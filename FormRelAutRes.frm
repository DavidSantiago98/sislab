VERSION 5.00
Begin VB.Form FormRelAutRes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormRelAutAna"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8685
   Icon            =   "FormRelAutRes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   8685
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtPesqRapFr 
      Height          =   315
      Left            =   3000
      Picture         =   "FormRelAutRes.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   21
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Exames"
      Top             =   1080
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaRapidaFrases 
      Height          =   315
      Left            =   7440
      Picture         =   "FormRelAutRes.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Exames"
      Top             =   1080
      Width           =   375
   End
   Begin VB.ListBox EcListaFrases 
      Height          =   645
      Left            =   4560
      TabIndex        =   18
      Top             =   1080
      Width           =   2895
   End
   Begin VB.TextBox EcPesqRapAux 
      Height          =   285
      Left            =   1680
      TabIndex        =   16
      Top             =   4680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   180
      TabIndex        =   9
      Top             =   6960
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   960
      TabIndex        =   8
      Top             =   6990
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   210
      TabIndex        =   7
      Top             =   6480
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   930
      TabIndex        =   6
      Top             =   6480
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   180
      TabIndex        =   5
      Top             =   2160
      Width           =   8295
   End
   Begin VB.TextBox EcResultado 
      Height          =   285
      Left            =   1500
      TabIndex        =   4
      Top             =   1080
      Width           =   1545
   End
   Begin VB.ComboBox EcDescricaoAnaCombo 
      Height          =   315
      Left            =   3120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   660
      Width           =   4755
   End
   Begin VB.TextBox EcCodigoAna 
      Height          =   285
      Left            =   1500
      TabIndex        =   2
      Top             =   660
      Width           =   1545
   End
   Begin VB.ComboBox EcDescricaoAutoCombo 
      Height          =   315
      Left            =   3120
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   210
      Width           =   4755
   End
   Begin VB.TextBox EcCodigoAuto 
      Height          =   285
      Left            =   1500
      TabIndex        =   0
      Top             =   240
      Width           =   1545
   End
   Begin VB.Label Label8 
      Caption         =   "Frases"
      Height          =   255
      Left            =   3840
      TabIndex        =   19
      Top             =   1080
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "EcPesqRapAux"
      Height          =   255
      Left            =   360
      TabIndex        =   17
      Top             =   4680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "Resultado "
      Height          =   225
      Left            =   6390
      TabIndex        =   15
      Top             =   1830
      Width           =   1065
   End
   Begin VB.Label Label6 
      Caption         =   "C�digo An�lise Simples "
      Height          =   225
      Left            =   2910
      TabIndex        =   14
      Top             =   1830
      Width           =   1755
   End
   Begin VB.Label Label4 
      Caption         =   "C�digo Automatiza��o "
      Height          =   225
      Left            =   180
      TabIndex        =   13
      Top             =   1830
      Width           =   1755
   End
   Begin VB.Label Label2 
      Caption         =   "Resultado "
      Height          =   225
      Left            =   240
      TabIndex        =   12
      Top             =   1110
      Width           =   1065
   End
   Begin VB.Label Label1 
      Caption         =   "An�lise Simples "
      Height          =   225
      Left            =   240
      TabIndex        =   11
      Top             =   720
      Width           =   1755
   End
   Begin VB.Label Label3 
      Caption         =   "Automatiza��o "
      Height          =   225
      Left            =   240
      TabIndex        =   10
      Top             =   240
      Width           =   1755
   End
End
Attribute VB_Name = "FormRelAutRes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub BtPesqRapFr_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_dicionario", _
                        "descr_frase", "seq_frase", _
                        EcPesqRapAux
End Sub

Private Sub BtPesquisaRapidaFrases_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_frase"
    CamposEcran(1) = "cod_frase"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_frase"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_dicionario "
    CWhere = ""
    CampoPesquisa = "descr_frase"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_frase ", _
                                                                           " Frases")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaFrases.ListCount = 0 Then
                EcListaFrases.AddItem BL_SelCodigo("sl_dicionario", "descr_frase", "seq_frase", resultados(i))
                EcListaFrases.ItemData(0) = resultados(i)
            Else
                EcListaFrases.AddItem BL_SelCodigo("sl_dicionario", "descr_frase", "seq_frase", resultados(i))
                EcListaFrases.ItemData(EcListaFrases.NewIndex) = resultados(i)
            End If
        Next i
        

    End If

End Sub

Private Sub EcCodigoAna_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodigoAna_LostFocus()
    Dim i As Integer
    Dim sql As String
    Dim rsAna As New ADODB.recordset
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigoAna.Text = UCase(EcCodigoAna.Text)
    
    sql = "SELECT t_result,seq_ana_s, descr_ana_s FROM sl_ana_s WHERE cod_ana_s =" & BL_TrataStringParaBD(EcCodigoAna)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    rsAna.Open sql, gConexao
    If rsAna.RecordCount > 0 Then
        For i = 0 To EcDescricaoAnaCombo.ListCount - 1
            If EcDescricaoAnaCombo.ItemData(i) = rsAna!seq_ana_s Then
                EcDescricaoAnaCombo.ListIndex = i
                Exit For
            End If
        Next
        
        Select Case rsAna!t_result
            Case gT_Numerico
                BtPesqRapFr.Enabled = False
                EcListaFrases.Enabled = False
                BtPesquisaRapidaFrases.Enabled = False
                EcResultado.Enabled = True
                EcListaFrases.Clear
            Case gT_Alfanumerico, gT_Auxiliar
                BtPesqRapFr.Enabled = True
                EcListaFrases.Enabled = False
                BtPesquisaRapidaFrases.Enabled = False
                EcResultado.Enabled = True
                EcListaFrases.Clear
            Case gT_Frase
                BtPesqRapFr.Enabled = False
                EcListaFrases.Enabled = True
                BtPesquisaRapidaFrases.Enabled = True
                EcResultado.Enabled = False
                EcResultado = ""
        End Select
    End If
    rsAna.Close
    Set rsAna = Nothing
    
End Sub


Private Sub EcCodigoAuto_GotFocus()

    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodigoAuto_LostFocus()

    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigoAuto.Text = UCase(EcCodigoAuto.Text)
    BL_ColocaTextoCombo "sl_auto_res", "seq_auto_res", "cod_auto_res", EcCodigoAuto, EcDescricaoAutoCombo
    
End Sub
Private Sub EcDescricaoAnaCombo_Click()
    Dim rsAna As ADODB.recordset
    Dim sql As String
    If EcDescricaoAnaCombo.ListIndex = mediComboValorNull Then
        Exit Sub
    End If
    
    sql = "SELECT t_result, cod_ana_s FROM sl_ana_s WHERE seq_ana_s =" & EcDescricaoAnaCombo.ItemData(EcDescricaoAnaCombo.ListIndex)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    rsAna.Open sql, gConexao
    If rsAna.RecordCount > 0 Then
        EcCodigoAna = rsAna!cod_ana_s
        Select Case rsAna!t_result
            Case gT_Numerico
                BtPesqRapFr.Enabled = False
                EcListaFrases.Enabled = False
                BtPesquisaRapidaFrases.Enabled = False
                EcResultado.Enabled = True
                EcListaFrases.Clear
            Case gT_Alfanumerico, gT_Auxiliar
                BtPesqRapFr.Enabled = True
                EcListaFrases.Enabled = False
                BtPesquisaRapidaFrases.Enabled = False
                EcResultado.Enabled = True
                EcListaFrases.Clear
            Case gT_Frase
                BtPesqRapFr.Enabled = False
                EcListaFrases.Enabled = True
                BtPesquisaRapidaFrases.Enabled = True
                EcResultado.Enabled = False
                EcResultado = ""
        End Select
    End If
    rsAna.Close
    Set rsAna = Nothing
End Sub




Private Sub EcDescricaoAnaCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcDescricaoAnaCombo.ListIndex = -1
End Sub


Private Sub EcDescricaoAutoCombo_Click()
    BL_ColocaComboTexto "sl_auto_res", "seq_auto_res", "cod_auto_res", EcCodigoAuto, EcDescricaoAutoCombo
End Sub

Private Sub EcDescricaoAutoCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcDescricaoAutoCombo.ListIndex = -1
End Sub


Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub


Private Sub EcPesqRapAux_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapAux.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_frase FROM sl_dicionario WHERE seq_frase = " & EcPesqRapAux, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da frase!", vbExclamation, "Pesquisa r�pida"
        Else
            EcResultado = "\\" & rsCodigo!cod_frase
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
End Sub

Private Sub EcResultado_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcResultado_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Rela��o entre An�lises e Resultados da Automatiza��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 8775
    Me.Height = 5000 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_ana_res"
    Set CampoDeFocus = EcCodigoAuto
    
    NumCampos = 3
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_auto_res"
    CamposBD(1) = "cod_ana_s"
    CamposBD(2) = "descr_res"
   
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigoAuto
    Set CamposEc(1) = EcCodigoAna
    Set CamposEc(2) = EcResultado
    
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo de Automatiza��o"
    TextoCamposObrigatorios(1) = "C�digo de An�lise Simples"
    'TextoCamposObrigatorios(2) = "Resultado"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_auto_res"
    Set ChaveEc = EcCodigoAuto
    
    CamposBDparaListBox = Array("cod_auto_res", "cod_ana_s", "descr_res")
    NumEspacos = Array(22, 32, 22)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    If Not rs Is Nothing Then
       rs.Close
       Set rs = Nothing
    End If
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormRelAutRes = Nothing
End Sub

Sub LimpaCampos()

    Me.SetFocus
    BG_LimpaCampo_Todos CamposEc
    EcDescricaoAutoCombo.ListIndex = mediComboValorNull
    EcDescricaoAnaCombo.ListIndex = mediComboValorNull
    EcPesqRapAux.Text = ""
    EcListaFrases.Clear
End Sub

Sub DefTipoCampos()
    EcCodigoAuto.Tag = adVarChar
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub
Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_ana_s", "seq_ana_s", "descr_ana_s", EcDescricaoAnaCombo
    BG_PreencheComboBD_ADO "sl_auto_res", "seq_auto_res", "descr_auto_res", EcDescricaoAutoCombo
    
    'trazer codigo do form de automatizacoes
    'caso a chamada a este form tenha sido efectuada de l�
    If gF_AUTORES = 1 Then
        EcCodigoAuto = FormAutoRes.EcCodigo
        BL_ColocaTextoCombo "sl_auto_res", "seq_auto_res", "cod_auto_res", EcCodigoAuto, EcDescricaoAutoCombo
    End If

End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_ColocaTextoCombo "sl_ana_s", "seq_ana_s", "cod_ana_s", EcCodigoAna, EcDescricaoAnaCombo
        BL_ColocaTextoCombo "sl_auto_res", "seq_auto_res", "cod_auto_res", EcCodigoAuto, EcDescricaoAutoCombo
        PreencheFrases
        
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_auto_res ASC,cod_ana_s ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        
        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'
        
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
        
    GravaFrases
        
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    On Error GoTo TrataErro
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BL_TrataStringParaBD(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    GravaFrases
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'

    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Modificar Registo: " & Err.Description, Me.Name, "BD_Update", True
    Exit Sub
    Resume Next
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    SQLQuery = "DELETE FROM sl_ana_res_frase WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub
Private Sub EcListaFrases_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaFrases.ListCount > 0 Then     'Delete
        EcListaFrases.RemoveItem (EcListaFrases.ListIndex)
    End If
End Sub


Private Sub GravaFrases()
    Dim i As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = "DELETE FROM sl_ana_res_frase WHERE cod_auto_res = " & BL_TrataStringParaBD(EcCodigoAuto)
    sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(EcCodigoAna)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcListaFrases.ListCount - 1
        sSql = "INSERT INTO sl_ana_res_frase (cod_auto_res, cod_ana_s, cod_frase, ordem) VALUES( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigoAuto) & ", "
        sSql = sSql & BL_TrataStringParaBD(EcCodigoAna) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("sl_dicionario", "cod_frase", "seq_frase", EcListaFrases.ItemData(i))) & ", "
        sSql = sSql & i & ")"
        BG_ExecutaQuery_ADO sSql
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao GravaFrases: " & Err.Description, Me.Name, "GravaFrases", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheFrases()
    Dim i As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsFrase As New ADODB.recordset
    EcListaFrases.Clear
    
    sSql = "SELECT x1.cod_auto_res, x1.cod_ana_s, x1.cod_frase, x1.ordem, x2.descr_frase, x2.seq_frase "
    sSql = sSql & " FROM sl_ana_res_frase x1, sl_dicionario x2 "
    sSql = sSql & " WHERE cod_auto_res = " & BL_TrataStringParaBD(EcCodigoAuto)
    sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(EcCodigoAna)
    sSql = sSql & " AND x1.cod_frase = x2.cod_frase "
    sSql = sSql & " ORDER BY ordem "
    rsFrase.CursorType = adOpenStatic
    rsFrase.CursorLocation = adUseServer
    If gModoDebug = 1 Then BG_LogFile_Erros sSql
    rsFrase.Open sSql, gConexao
    If rsFrase.RecordCount > 0 Then
        While Not rsFrase.EOF
            EcListaFrases.AddItem rsFrase!descr_frase
            EcListaFrases.ItemData(EcListaFrases.NewIndex) = rsFrase!seq_frase
            rsFrase.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PreencheFrases: " & Err.Description, Me.Name, "PreencheFrases", True
    Exit Sub
    Resume Next
End Sub



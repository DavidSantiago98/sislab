VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormChegadaPorTubosV2 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormChegadaPorTubosV2"
   ClientHeight    =   18960
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15405
   FillColor       =   &H8000000F&
   Icon            =   "FormChegadaPorTubosV2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   18960
   ScaleWidth      =   15405
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPrinterResumo 
      Height          =   285
      Left            =   10440
      TabIndex        =   68
      Top             =   9120
      Width           =   975
   End
   Begin VB.Frame FramePendentes 
      BorderStyle     =   0  'None
      Height          =   8895
      Left            =   120
      TabIndex        =   3
      Top             =   9600
      Width           =   15255
      Begin VB.CommandButton BtPesquisaGrupoAna 
         Height          =   315
         Left            =   4680
         Picture         =   "FormChegadaPorTubosV2.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   74
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox EcDescrCodGrupo 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   73
         TabStop         =   0   'False
         Top             =   1320
         Width           =   2535
      End
      Begin VB.TextBox EcCodGrupo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         TabIndex        =   72
         Top             =   1320
         Width           =   735
      End
      Begin VB.CheckBox cbDiscriminarAna 
         Caption         =   "Discriminar an�lises na impress�o"
         Height          =   255
         Left            =   9120
         TabIndex        =   71
         Top             =   1440
         Width           =   5175
      End
      Begin VB.CheckBox ChkColheita 
         Caption         =   "Apenas com colheita efetuada"
         Height          =   195
         Left            =   5640
         TabIndex        =   70
         Top             =   1440
         Width           =   3375
      End
      Begin VB.ListBox EcListaTubos 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   9120
         TabIndex        =   14
         Top             =   480
         Width           =   5175
      End
      Begin VB.ListBox EcListaLocais 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1440
         TabIndex        =   11
         Top             =   480
         Width           =   6015
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9120
         TabIndex        =   58
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9840
         Locked          =   -1  'True
         TabIndex        =   57
         TabStop         =   0   'False
         Top             =   120
         Width           =   4455
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   14280
         Picture         =   "FormChegadaPorTubosV2.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   56
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   120
         Width           =   375
      End
      Begin VB.CommandButton BtSairPendentes 
         Height          =   615
         Left            =   12480
         Picture         =   "FormChegadaPorTubosV2.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Voltar"
         Top             =   6960
         Width           =   615
      End
      Begin MSFlexGridLib.MSFlexGrid FGReq 
         Height          =   4935
         Left            =   120
         TabIndex        =   17
         Top             =   1920
         Width           =   13575
         _ExtentX        =   23945
         _ExtentY        =   8705
         _Version        =   393216
         Rows            =   4
         Cols            =   4
         FixedRows       =   0
         FixedCols       =   0
         BackColor       =   16777215
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin VB.CommandButton BtPesquisaTubos 
         Height          =   315
         Left            =   14280
         Picture         =   "FormChegadaPorTubosV2.frx":0F96
         Style           =   1  'Graphical
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaLocais 
         Height          =   315
         Left            =   7440
         Picture         =   "FormChegadaPorTubosV2.frx":1520
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   480
         Width           =   375
      End
      Begin VB.ComboBox cbUrgencia 
         Height          =   315
         Left            =   6240
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   120
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   1440
         TabIndex        =   5
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   169345025
         CurrentDate     =   39573
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   4200
         TabIndex        =   7
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   169345025
         CurrentDate     =   39573
      End
      Begin VB.Label lbgrAna 
         Caption         =   "Gr. An�lises"
         Height          =   255
         Left            =   120
         TabIndex        =   69
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Sala / Posto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   8040
         TabIndex        =   59
         Top             =   120
         Width           =   1155
      End
      Begin VB.Label Label2 
         Caption         =   "Tubos"
         Height          =   255
         Left            =   8040
         TabIndex        =   16
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Locais"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   13
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Urg�ncia"
         Height          =   255
         Left            =   5520
         TabIndex        =   10
         Top             =   165
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Dt.Previ. Final"
         Height          =   255
         Index           =   2
         Left            =   2880
         TabIndex        =   8
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Dt.Previ. Inicial"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   1215
      End
   End
   Begin VB.CommandButton BtInfo 
      Height          =   615
      Left            =   11160
      Picture         =   "FormChegadaPorTubosV2.frx":1AAA
      Style           =   1  'Graphical
      TabIndex        =   67
      ToolTipText     =   "Informa��es Associadas"
      Top             =   8160
      Width           =   615
   End
   Begin VB.CommandButton BtEtiq 
      Height          =   615
      Left            =   14160
      Picture         =   "FormChegadaPorTubosV2.frx":2214
      Style           =   1  'Graphical
      TabIndex        =   66
      ToolTipText     =   "Impress�o Etiquetas Prim�rias"
      Top             =   8160
      Width           =   615
   End
   Begin VB.CommandButton BtEtiqSeq 
      Height          =   615
      Left            =   13560
      Picture         =   "FormChegadaPorTubosV2.frx":2EDE
      Style           =   1  'Graphical
      TabIndex        =   65
      ToolTipText     =   "Impress�o Etiquetas Secund�rias"
      Top             =   8160
      Width           =   615
   End
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   360
      TabIndex        =   55
      Top             =   10080
      Width           =   615
   End
   Begin VB.TextBox EcArea 
      Height          =   285
      Left            =   6840
      TabIndex        =   54
      Text            =   "Text1"
      Top             =   11040
      Width           =   855
   End
   Begin VB.TextBox EcCodProven 
      Height          =   285
      Left            =   1320
      TabIndex        =   51
      Text            =   "Text1"
      Top             =   10800
      Width           =   855
   End
   Begin VB.TextBox EcDataNasc 
      Height          =   285
      Left            =   5880
      TabIndex        =   50
      Text            =   "Text1"
      Top             =   11040
      Width           =   855
   End
   Begin VB.Frame FrInfoComplementar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   44
      Top             =   8280
      Width           =   11175
      Begin VB.TextBox EcHoraEliminacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   8760
         TabIndex        =   49
         ToolTipText     =   "Hora do Cancelamento"
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox EcDataEliminacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   8760
         TabIndex        =   48
         ToolTipText     =   "Data do Cancelamento"
         Top             =   120
         Width           =   2055
      End
      Begin VB.TextBox EcUtilizadorEliminacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   47
         ToolTipText     =   "Utilizador do Cancelamento"
         Top             =   360
         Width           =   5895
      End
      Begin VB.TextBox EcMotivoEliminacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   46
         ToolTipText     =   "Motivo de Cancelamento"
         Top             =   120
         Width           =   5895
      End
      Begin VB.Label Label5 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Dados de Cancelamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   240
         TabIndex        =   45
         Top             =   120
         Width           =   2415
      End
      Begin VB.Shape Shape4 
         BackStyle       =   1  'Opaque
         BorderColor     =   &H8000000C&
         FillColor       =   &H00E0E0E0&
         FillStyle       =   0  'Solid
         Height          =   615
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   10935
      End
   End
   Begin VB.TextBox EcCodigoMotivo 
      Height          =   285
      Left            =   240
      TabIndex        =   41
      Top             =   11880
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Frame FrCabecalho 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   120
      TabIndex        =   28
      Top             =   0
      Width           =   15615
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1080
         MaxLength       =   9
         TabIndex        =   33
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox EcUtente 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1080
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   32
         Top             =   780
         Width           =   1695
      End
      Begin VB.TextBox EcPrescricao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   9000
         MaxLength       =   9
         TabIndex        =   64
         Top             =   840
         Width           =   975
      End
      Begin VB.OptionButton Opt 
         Appearance      =   0  'Flat
         Caption         =   "SAIDA DE TUBOS"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000006&
         Height          =   195
         Index           =   1
         Left            =   13560
         TabIndex        =   39
         Top             =   10200
         Width           =   1695
      End
      Begin VB.OptionButton Opt 
         Appearance      =   0  'Flat
         Caption         =   "ENTRADA DE TUBOS"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000006&
         Height          =   195
         Index           =   0
         Left            =   13560
         TabIndex        =   38
         Top             =   9960
         Width           =   1695
      End
      Begin VB.TextBox EcEstado 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   34
         Top             =   240
         Width           =   4935
      End
      Begin VB.TextBox EcNomeUtente 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   3000
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   840
         Width           =   4695
      End
      Begin VB.TextBox EcDescrLocal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   10080
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   240
         Width           =   5055
      End
      Begin VB.TextBox EcLocal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   9000
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   29
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Prescri��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   5
         Left            =   8040
         TabIndex        =   63
         Top             =   840
         Width           =   885
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H8000000C&
         Height          =   495
         Index           =   1
         Left            =   7920
         Shape           =   4  'Rounded Rectangle
         Top             =   720
         Width           =   7335
      End
      Begin VB.Label LbNumero 
         AutoSize        =   -1  'True
         Caption         =   "&Requisi��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   120
         TabIndex        =   37
         Top             =   240
         Width           =   900
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H8000000C&
         Height          =   495
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   7815
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H8000000C&
         Height          =   495
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   720
         Width           =   7815
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   36
         Top             =   840
         Width           =   540
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Local"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   8040
         TabIndex        =   35
         Top             =   240
         Width           =   435
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H8000000C&
         Height          =   495
         Index           =   0
         Left            =   7920
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   7335
      End
   End
   Begin VB.Frame FrGarrafa 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   21
      Top             =   8280
      Width           =   11175
      Begin VB.CommandButton BtCancGarrafa 
         Height          =   435
         Left            =   10080
         Picture         =   "FormChegadaPorTubosV2.frx":3BA8
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Cancelar"
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtGarrafaOk 
         Height          =   435
         Left            =   9600
         Picture         =   "FormChegadaPorTubosV2.frx":3FC8
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "Gravar identifica��o garrafa"
         Top             =   120
         Width           =   495
      End
      Begin VB.TextBox EcGarrafa 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   5040
         TabIndex        =   24
         Top             =   120
         Width           =   4575
      End
      Begin VB.Label Label3 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Introduza a identifica��o da garrafa de hemocultura"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   120
         Width           =   4575
      End
      Begin VB.Shape Shape5 
         BackStyle       =   1  'Opaque
         BorderColor     =   &H8000000C&
         FillColor       =   &H00E0E0E0&
         FillStyle       =   0  'Solid
         Height          =   615
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   10935
      End
   End
   Begin VB.Frame FrMotivo 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   18
      Top             =   8280
      Width           =   11055
      Begin VB.CommandButton BtCancMotivo 
         Height          =   435
         Left            =   9960
         Picture         =   "FormChegadaPorTubosV2.frx":43BB
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Cancelar"
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtMotivoOk 
         Height          =   435
         Left            =   9480
         Picture         =   "FormChegadaPorTubosV2.frx":47DB
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Gravar motivo de nova amostra"
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtPesquisaCanc 
         Height          =   435
         Left            =   9000
         Picture         =   "FormChegadaPorTubosV2.frx":4BCE
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Pesquisa de motivos de cancelamento"
         Top             =   120
         Width           =   495
      End
      Begin VB.TextBox EcMotivoNovaAmostra 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   19
         Top             =   120
         Width           =   6255
      End
      Begin VB.Label Label9 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Motivo de Nova Amostra"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   120
         Width           =   2535
      End
      Begin VB.Shape Shape6 
         BackStyle       =   1  'Opaque
         BorderColor     =   &H8000000C&
         FillColor       =   &H00E0E0E0&
         FillStyle       =   0  'Solid
         Height          =   615
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   10935
      End
   End
   Begin VB.TextBox TextAuxiliar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   3720
      TabIndex        =   1
      Text            =   "TextAuxiliar"
      Top             =   10800
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   375
      Left            =   2880
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   0
      Top             =   10800
      Width           =   495
   End
   Begin MSComctlLib.ImageList ImageListIcons 
      Left            =   240
      Top             =   10560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormChegadaPorTubosV2.frx":4FB1
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormChegadaPorTubosV2.frx":510B
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormChegadaPorTubosV2.frx":5265
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormChegadaPorTubosV2.frx":5665
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormChegadaPorTubosV2.frx":5A65
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame FrameSaida 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   720
      Width           =   3855
   End
   Begin VB.Frame FrTubos 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7575
      Left            =   120
      TabIndex        =   42
      Top             =   1320
      Width           =   15255
      Begin VB.CommandButton BtFaltaTubos 
         Height          =   615
         Left            =   14640
         Picture         =   "FormChegadaPorTubosV2.frx":5DFF
         Style           =   1  'Graphical
         TabIndex        =   52
         ToolTipText     =   "Tubos Pendentes"
         Top             =   6840
         Width           =   615
      End
      Begin VB.CommandButton BtResumo 
         Height          =   615
         Left            =   12840
         Picture         =   "FormChegadaPorTubosV2.frx":6343
         Style           =   1  'Graphical
         TabIndex        =   62
         ToolTipText     =   "Folha de Resumo"
         Top             =   6840
         Width           =   615
      End
      Begin VB.CommandButton BtEmiteFolhaNovaAmostra 
         Height          =   615
         Left            =   12240
         Picture         =   "FormChegadaPorTubosV2.frx":6AAD
         Style           =   1  'Graphical
         TabIndex        =   53
         ToolTipText     =   "Emite folha de pedido de nova amostra"
         Top             =   6840
         Width           =   615
      End
      Begin MSComctlLib.ListView LvTubos 
         Height          =   6615
         Left            =   0
         TabIndex        =   43
         Top             =   120
         Width           =   15255
         _ExtentX        =   26908
         _ExtentY        =   11668
         SortOrder       =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.CommandButton BtNotas 
         Enabled         =   0   'False
         Height          =   615
         Left            =   11640
         Picture         =   "FormChegadaPorTubosV2.frx":6F75
         Style           =   1  'Graphical
         TabIndex        =   60
         ToolTipText     =   "Notas"
         Top             =   6840
         Width           =   615
      End
      Begin VB.CommandButton BtNotasVRM 
         Enabled         =   0   'False
         Height          =   615
         Left            =   11640
         Picture         =   "FormChegadaPorTubosV2.frx":76DF
         Style           =   1  'Graphical
         TabIndex        =   61
         ToolTipText     =   "Notas"
         Top             =   6840
         Width           =   615
      End
   End
   Begin Crystal.CrystalReport Report 
      Left            =   360
      Top             =   11040
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
End
Attribute VB_Name = "FormChegadaPorTubosV2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit   ' Pada obrigar a definir as vari�veis.

Private NumCampos As Integer

Private CamposBD() As String
Private CamposEc() As Object
Private TextoCamposObrigatorios() As String

Private ChaveBD As String
Private ChaveEc As Object

Private NomeTabela As String
Private CriterioTabela As String

Private Estado As Integer  ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Private CampoActivo As Object
Private CampoDeFocus As Object

Private erroTmp As Integer
Private MarcaInicial As Variant   ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Private CamposBDparaListBox
Private NumEspacos
Private ListarRemovidos As Boolean

Private CmdReq As New ADODB.Command

Private g_codigo_tubo As String
Private g_codigo_produto As String

Private tipo_pesquisa As Integer
        
Private Enum LvTubosCores

    t_verde = &HC000&
    t_vermelho = &HC0&
    t_vermelho_claro = &H8080FF
    t_azul = &HC0C000
    t_cinzento = &H808080
    t_amarelo = &HC0C0&
    t_laranja = &H80C0FF
    t_verde_claro = &HC0FFC0
    t_violeta = &HFF00FF
    
End Enum
          
Private Enum LvTubosColunas
  
    t_numero_requisicao
    t_informacoes
    t_codigo_tubo
    t_descricao_tubo
    t_data_prevista
    t_data_chegada
    t_utilizador
    t_data_colheita
    t_inicio_transp
    t_fim_transp
    t_analise
    t_estado_tubo
    
End Enum

Private Enum AnaliseTag
  
    t_undef = 0
    t_perfil = 1
    t_complexa = 2
    t_simples = 3
    
End Enum

Private Enum MovimentosTubo
    
    t_entrada = 0
    t_saida = 1
    
End Enum

Private Enum ListViewIcons
    
    cIconCollapsed = 1
    cIconExpanded = 2
    cIconIn = 3
    cIconOut = 4
    
End Enum

Private Enum StockDirection
    
    cIn = 0
    cOut = 1
    
End Enum

'Private Type estruturaTubos
'    seq_req_tubo As Long
'    Index As Long
'    agrupador_estrutura As Long
'    requisicao As Long
'    codigo_tubo As String
'    data_prevista As String
'    data_chegada As String
'    hora_chegada  As String
'    utilizador_chegada  As String
'    data_colheita As String
'    hora_colheita As String
'    utilizador_colheita As String
'    data_eliminacao As String
'    hora_eliminacao As String
'    utilizador_eliminacao As String
'    codigo_local As Integer
'    codigo_analise As String
'    descricao_analise As String
'    estado_tubo As Integer
'    descricao_tubo As String
'    codigo_etiqueta As String
'    motivo_eliminacao As String
'    numero_garrafa As String
'    codigo_produto As String
'    descricao_produto As String
'    flag_transito As Boolean
'    num_obs_ana As Integer
'    dt_inicio_transp As String
'    hr_inicio_transp As String
'    user_inicio_transp As String
'    dt_fim_transp As String
'    hr_fim_transp As String
'    user_fim_transp As String
'    cod_local_ana As String
'    cod_local_extra As String
'    flg_informacao As Integer
'    indiceEstrutTubos As Integer
'End Type

'Dim eTubos() As estruturaTubos

Dim totalTubos As Long

' Keep handle to the list view.
Private lhwndListView As Long

' Keep handle to the text box.
Private lhwndTextBox As Long

' Keep list item index whose subItem is being edited.
Private lListItemIndex As Long

' Keep zero based index of LvTubos.ListItems(lListItemIndex).SubItem being edited.
Private lListSubItem As Long

Private Type Tubos
    cod_tubo As String
    descR_tubo As String
    dt_chega As String
    hr_chega As String
    dt_canc As String
    hr_canc As String
    cor As String
    estado_tubo As Integer
End Type

Private Type req
    n_req As String
    dt_previ As String
    dt_chega As String
    nome_ute As String
    dt_nasc_ute As String
    Utente As String
    t_utente As String
    cod_proven As String
    descr_proven As String
    t_urg As String
    TubosPend() As Tubos
    totalTubosPend As Long
    n_proc_1 As String
    n_proc_2 As String
End Type
Dim EstrutReq() As req
Dim totalReq As Long
Dim linhaActual As Integer
Dim reqAtual As req

Private Type tuboSec
    cod_tubo_sec As String
    etiq_tubo_sec As String
    cod_tubo_prim As String
    descr_tubo_sec As String
    Copias As Integer
    abr_ana As String
End Type
Dim estrutTubosSec() As tuboSec
Dim totalTubosSec As Integer
Dim flg_ReqBloqueada As Boolean

Private Type CodAnaRegras
    cod_ana As String
    cod_local As String
    cod_obrig As String
End Type
Dim estrutRegras() As CodAnaRegras
Dim totalRegras As Integer

Private Enum Motivos
    PedNovaAmostra = 1
    RemarcacaoTubo = 2
End Enum
Dim TipoMotivo As Integer
'BRUNODSANTOS CHUC-7934 - 22.04.2016
Dim arrReq() As Long
'
'NELSONPSILVA Glintt-HS-18011 09.02.2018
Dim campo_tabela As String
Dim campo_condicao As String
'

'NELSONPSILVA Glintt-HS-18011 04.04.2018 - Acesso contextualizado quando chamado pelo form anterior (Log RGPD)
Public ExternalAccess As Boolean
 
'Public Property Get ExternalAccess() As Boolean
'    ExternalAccess = ExtAccess
'End Property
'
'Public Property Let ExternalAccess(ByVal newValue As Boolean)
'    ExtAccess = newValue
'End Property
''


'Private Sub BtCancelaTubos_Click()
'
'    If (FGReq.rows < 2) Then: Exit Sub
'    If (FGReq.row = Empty) Then: Exit Sub
'    gMsgMsg = "Tem a certeza que quer cancelar a chegada do(s) tubo(s) para a requisi��o " & EstrutReq(FGReq.row).n_req & "?"
'    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
'    If gMsgResp = vbYes Then: If (ExecutaCancelamentoTubos(FGReq.row)) Then FuncaoProcurar
'
'End Sub

'edgar.parada IPO-P-13743 21.02.2019 - COnstante para controlo de impress�o de an�lies
Private ImprimeAna As Integer
'

Private Sub BtCancGarrafa_Click()

    Call ControlaFrames(FrGarrafa, False)
    
End Sub

Private Sub BtCancMotivo_Click()

    Call ControlaFrames(FrMotivo, False)
    
End Sub

Private Sub BtEmiteFolhaNovaAmostra_Click()

    Dim Item As Long
    Dim sSql As String
    
    sSql = "DELETE FROM sl_cr_pedido_nova_amostra WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
   
    For Item = 1 To UBound(eTubos)
        If (eTubos(Item).Index = Empty And eTubos(Item).motivo_eliminacao <> Empty) Then: If (Not PreencheTabela(Item)) Then Exit Sub
    Next
    Call EmitirPedidoNovaAmostra

End Sub

Private Sub BtEtiq_Click()
    If (gRequisicaoActiva = -1 Or gRequisicaoActiva = 0) And EcNumReq = "" Then Exit Sub
    'Limpar parametros
    BG_LimpaPassaParams
    'Passar parametros necessarios o ao form de impress�o
    gPassaParams.Param(0) = BL_HandleNull(EcNumReq.text, gRequisicaoActiva)
    '
    
    'Abrir form de impress�o
    'FormNEtiqNTubos.CarregaArray arrReq()
    FormNEtiqNTubos.Show
    DoEvents
    'FormGestaoRequisicaoPrivado.SetFocus

End Sub


Private Sub BtEtiqSeq_Click()
    Dim i As Integer
    For i = 1 To totalTubos
        If i > 1 Then
            If eTubos(i).codigo_tubo <> eTubos(i - 1).codigo_tubo Then
                VerificaEtiquetasSecundarias eTubos(i).codigo_tubo
            End If
        Else
            VerificaEtiquetasSecundarias eTubos(i).codigo_tubo
        End If
    Next i
End Sub
Private Sub BtFaltaTubos_Click()

    FramePendentes.top = 120
    FramePendentes.left = 120
    FramePendentes.Visible = True
    BtSairPendentes.top = BtFaltaTubos.top + 1200
    BtSairPendentes.left = BtFaltaTubos.left - 600
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
End Sub

Private Sub BtGarrafaOk_Click()

    Call ControlaFrames(FrGarrafa, False)
    If (LvTubos.ListItems.Count = Empty) Then: Exit Sub
    If (EcGarrafa.text = Empty) Then: BG_Mensagem mediMsgBox, "Tem que inserir o n�mero de identifica��o da garrafa!", vbExclamation, " Aten��o": Exit Sub
    Call InsereGarrafaHemocultura(LvTubos.SelectedItem.Index)
    Call RegistaMovimentoTubos(LvTubos.SelectedItem.Index, True)
    RefrescaLista
    
End Sub

Private Sub BtInfo_Click()
    Dim posicao As Long
    If LvTubos.ListItems.Count > 0 Then
        posicao = RetornaIndiceTubo(LvTubos.SelectedItem.Index)
        If posicao > 0 Then
            FormInfoAna.Show
            FormInfoAna.EcNReq.text = eTubos(posicao).requisicao
            FormInfoAna.EcCodTInformacao.text = "3"
            FormInfoAna.FuncaoProcurar
        End If
    End If
End Sub

Private Sub BtMotivoOk_Click()
    Dim posicao As Long
    
    If (EcMotivoNovaAmostra.text = Empty) Then: BG_Mensagem mediMsgBox, "Tem que inserir o motivo!", vbExclamation, " Aten��o": Exit Sub
    posicao = RetornaIndiceTubo(LvTubos.SelectedItem.Index)
    If (eTubos(posicao).estado_tubo = gEstadosTubo.cancelado) Then: Exit Sub
    If TipoMotivo = Motivos.PedNovaAmostra Then
        BL_RegistaPedidoNovaAmostra eTubos(posicao).requisicao, eTubos(posicao).codigo_tubo, EcCodigoMotivo.text, EcMotivoNovaAmostra.text
        Call ExecutaPedidoNovaAmostra(gRequisicaoActiva, LvTubos.SelectedItem.Index, EcCodigoMotivo.text)
    ElseIf TipoMotivo = Motivos.RemarcacaoTubo Then
        ExecutaRemarcacaoTubo LvTubos.SelectedItem.Index, EcCodigoMotivo.text
    End If
        Call ControlaFrames(FrMotivo, False)
    
End Sub

Private Sub BtPesquisacanc_Click()
    Dim codigo_frase As String
    If BL_HandleNull(gAnaliseCancelamentoAmostra, "-1") <> "-1" Then
        PesquisaFrases gAnaliseCancelamentoAmostra, codigo_frase
        EcCodigoMotivo.text = codigo_frase
        EcMotivoNovaAmostra.text = BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", codigo_frase, "V")
    Else
        PesquisaMotivosCanc
    End If
End Sub
Private Sub PesquisaMotivosCanc()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_canc"
    CamposEcran(1) = "descr_canc"
    Tamanhos(1) = 5000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_t_canc"
    CamposEcran(2) = "cod_t_canc"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_t_canc"
    CWhere = "t_util = " & BL_TrataStringParaBD(TIPOS_CANCELAMENTO.t_TUBO)
    CampoPesquisa = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Motivos de pedido de nova amostra")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcMotivoNovaAmostra.text = UCase(Resultados(1))
            EcCodigoMotivo.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem motivos de pedido de nova amostra", vbExclamation, "ATEN��O"
    End If

End Sub

Private Sub BtPesquisaGrupoAna_Click()
    'CHUC-12894
    PA_PesquisaGrAna EcCodGrupo, EcDescrCodGrupo
End Sub

Private Sub BtResumo_Click()
    If EcNumReq <> "" Or gRequisicaoActiva > 0 Then
        If gTipoInstituicao = gTipoInstituicaoHospitalar Then
            FR_ImprimeResumoCrystal_HOSP BL_HandleNull(EcNumReq, gRequisicaoActiva)
        ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Then
        
        End If
    End If
End Sub

Private Sub BtSairPendentes_Click()
    FramePendentes.Visible = False
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
End Sub

'edgar.parada IPO-P-13743 19.02.2019
Private Sub cbDiscriminarAna_Click()
   If cbDiscriminarAna.value = mediSim Then
      ImprimeAna = mediSim
   Else
      ImprimeAna = mediNao
   End If
End Sub
'

'Private Sub cbGrAna_KeyDown(KeyCode As Integer, Shift As Integer)
'    On Error GoTo TrataErro
'
'    BG_LimpaOpcao cbGrAna, KeyCode
'
'Exit Sub
'TrataErro:
'    BG_LogFile_Erros "cbGrAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "cbGrAna_KeyDown"
'    Exit Sub
'    Resume Next
'End Sub

'CHUC-12894
Public Sub EcCodGrupo_Validate(cancel As Boolean)

    cancel = PA_ValidateGrAna(EcCodGrupo, EcDescrCodGrupo)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodGrupo_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodGrupo_Validate"
    Exit Sub
    Resume Next
End Sub
'

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (KeyCode)
        Case KeyCodeConstants.vbKeyReturn:
            'rcoelho 21.05.2013 chvng-4118
            If Len(EcNumReq) > 7 Then
                EcNumReq = (Right("00" & EcNumReq, 9))
            End If
            If (Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)) Then: Exit Sub
            Call IniciaMecanismoTubos
            'EcNumReq.SetFocus
            EcNumReq.SelLength = Len(EcNumReq)
            

        Case Else: ' Empty
    End Select
    
End Sub



Private Sub EcPrescricao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcPrescricao_Validate False
    End If
End Sub

Private Sub EcPrescricao_Validate(cancel As Boolean)
    Dim sSql As String
    Dim rsPrescr As New ADODB.recordset
    Dim requisicoes As String
    If EcPrescricao <> "" Then
        If IsNumeric(EcPrescricao) Then
            If gTipoInstituicao = gTipoInstituicaoHospitalar Then
                sSql = "SELECT distinct x1.n_req from sl_requis x1, sl_Req_episodio x2 WHERE x1.n_Req = x2.n_req "
                sSql = sSql & " AND ( x1.n_prescricao = " & EcPrescricao & " OR x2.n_prescricao = " & EcPrescricao & ")"
            Else
                sSql = "SELECT distinct x1.n_req from sl_requis x1 WHERE x1.n_prescricao = " & EcPrescricao
            End If
            rsPrescr.CursorLocation = adUseServer
            rsPrescr.CursorType = adOpenStatic
            rsPrescr.Open sSql, gConexao
            If rsPrescr.RecordCount = 1 Then
                EcNumReq = BL_HandleNull(rsPrescr!n_req, "")
                EcNumReq_KeyDown vbKeyReturn, 0
            ElseIf rsPrescr.RecordCount > 1 Then
                requisicoes = "("
                While Not rsPrescr.EOF
                    requisicoes = requisicoes & BL_HandleNull(rsPrescr!n_req, "") & ","
                    rsPrescr.MoveNext
                Wend
                requisicoes = Mid(requisicoes, 1, Len(requisicoes) - 1) & ")"
                FormPesquisaRapida.InitPesquisaRapida "SL_REQUIS", _
                                    "n_req", "n_req", _
                                     EcNumReq, , "n_Req IN " & requisicoes
                If EcNumReq <> "" Then
                    EcNumReq_KeyDown vbKeyReturn, 0
                End If
            End If
            rsPrescr.Close
            Set rsPrescr = Nothing
        End If
    End If
        
End Sub



Public Sub Form_Load()
    EventoLoad
End Sub

Public Sub Form_Activate()
    EventoActivate
End Sub

Public Sub Form_Unload(cancel As Integer)
    EventoUnload
End Sub

Private Sub Inicializacoes()
    If gColheitaChegada = gEnumColheitaChegada.ChegadaTubo Then
        Me.caption = " Chegada de Tubos"
    ElseIf gColheitaChegada = gEnumColheitaChegada.ColheitaTubo Then
        Me.caption = " Colheita de Tubos"
    End If
    Me.left = 5
    Me.top = 5
    Me.Width = 15570
    Me.Height = 9390 ' Normal
    'Me.Height = 7500 ' Campos Extras
        
    EcNumReq.Tag = adVarChar
    Set CampoDeFocus = EcNumReq
    EcEstado.locked = True
    EcUtente.locked = True
    EcNomeUtente.locked = True
    EcLocal.locked = True
    EcDescrLocal.locked = True
    
    totalTubos = 0
    ReDim eTubos(totalTubos)
    DefineLvTubos
    lhwndListView = LvTubos.hwnd
    
    ReDim reqColh(0)
    TotalReqColh = 0
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
   ' campo_tabela = ""
   ' campo_condicao = ""
  '  If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
  '      campo_tabela = ", slv_identif"
  '      campo_condicao = "AND sl_requis.seq_utente = slv_identif.seq_utente"
  '  End If
    '
    'Inicializa o comando ADO para selec��o do estado do utente da requisi��o
   ' Set CmdReq.ActiveConnection = gConexao
  '  CmdReq.CommandType = adCmdText
  '  CmdReq.CommandText = "SELECT estado_req, sl_requis.seq_utente, cod_local, cod_proven, tipo_urgencia, dt_previ, dt_chega, t_urg, cod_proven, "
  '  CmdReq.CommandText = CmdReq.CommandText & " sl_tbf_t_urg.cor cor_urg "
   ' CmdReq.CommandText = CmdReq.CommandText & " FROM sl_requis, sl_tbf_t_urg " & campo_tabela & " "
  '  CmdReq.CommandText = CmdReq.CommandText & "  WHERE estado_req not in (" & BL_TrataStringParaBD(gEstadoReqBloqueada)
  '  CmdReq.CommandText = CmdReq.CommandText & "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ") AND n_req =?"
   ' CmdReq.CommandText = CmdReq.CommandText & "  AND sl_requis.t_urg = sl_tbf_t_urg.cod_t_urg " & campo_condicao & " "
  '  CmdReq.Parameters.Append CmdReq.CreateParameter("N_REQ", adInteger, adParamInput, 20)
  '  CmdReq.Prepared = True

    Opt(0).value = True
    Opt(1).value = False
    
    MDIFormInicio.IDM_TUBO(1).Visible = True
    FrMotivo.Visible = False
    FrGarrafa.Visible = False
    FrInfoComplementar.Visible = False
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    
    'BRUNODSANTOS CHUC-7934 - 22.04.2016
    PassaParam.PrinterEtiq = EcPrinterEtiq.text
    EcPrinterResumo = BL_SelImpressora("ResumoRequisicoes.rpt")
    ReDim arrReq(0)
    '
        'edgar.parada IPO-P-13743 21.02.2019
    ImprimeAna = mediNao
    '
End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    gF_CHEGA_TUBOS = 1
    Inicializacoes
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    Estado = 0
    BG_StackJanelas_Push Me
    
End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    If (CampoDeFocus.Enabled) Then: CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "COPIANREQ", "Activo"
        
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    If UBound(eTubos) > 0 And LvTubos.ListItems.Count > 0 Then
        RefrescaLista
    End If
End Sub

Private Sub EventoUnload()
    gF_CHEGA_TUBOS = 0
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormChegadaPorTubosV2 = Nothing
    
End Sub

Private Sub LimpaCampos(Optional limpa_apenas_estrutura As Boolean)
    
    If flg_ReqBloqueada = False And Trim(EcNumReq) <> "" Then
        'BL_DesbloqueiaReq Trim(EcNumReq), Me.Name
    End If
    If (Not limpa_apenas_estrutura) Then
        EcNumReq.text = Empty
        EcEstado.text = Empty
        EcUtente.text = Empty
        EcNomeUtente.text = Empty
        EcLocal.text = Empty
        EcDescrLocal.text = Empty
        EcCodProven.text = Empty
        LimpaReqAtual
    End If
    totalTubos = 0
   
    Dim temp(2) As estruturaTubos
    temp(0) = eTubos(totalTubos)
    ReDim eTubos(totalTubos)
    LvTubos.ListItems.Clear
   
    Opt(0).value = True
    Opt(1).value = False
    EcDtFim.value = Bg_DaData_ADO
    EcDtIni.value = Bg_DaData_ADO
    cbUrgencia.ListIndex = mediComboValorNull
    EcListaLocais.Clear
    EcListaTubos.Clear
    FramePendentes.Visible = False
    FGReq.Cols = 2
    FGReq.rows = 2
    FGReq.TextMatrix(1, 0) = ""
    FGReq.TextMatrix(1, 1) = ""
    FrMotivo.Visible = False
    FrGarrafa.Visible = False
    FrInfoComplementar.Visible = False
    EcMotivoNovaAmostra.text = Empty
    EcGarrafa.text = Empty
    EcCodigoMotivo.text = Empty
    EcDataNasc.text = Empty
    EcArea.text = Empty
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    BtNotas.Enabled = True
    BtNotasVRM.Enabled = False
    EcPrescricao = ""
    totalRegras = 0
    ReDim estrutRegras(0)
    'LimpaReqAtual
    'edgar.parada IPO-P-13743 19.02.2019
    'cbGrAna.ListIndex = mediComboValorNull
    ChkColheita.value = vbUnchecked
    cbDiscriminarAna.value = mediNao
    ImprimeAna = mediNao
    '
End Sub

Private Sub PreencheCampos()
    'nada
End Sub

Public Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        If (CampoDeFocus.Enabled) Then: CampoDeFocus.SetFocus
    End If

End Sub

Public Sub FuncaoImprimir()
    
    ImprimeListagemTubosPendentes
    
End Sub

Public Sub FuncaoEstadoAnterior()
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        If (CampoDeFocus.Enabled) Then: CampoDeFocus.SetFocus
        
    Else
        Unload Me
    End If
End Sub

Public Sub FuncaoProcurar()
    
    On Error GoTo TrataErro
    Estado = 2
    BL_InicioProcessamento Me, " A procurar registos..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    If (FramePendentes.Visible) Then: ProcuraReqPend
    If (Not FramePendentes.Visible) Then: Call IniciaMecanismoTubos(True)
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    'BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    'ConstroiBotoesDinamicos
    Exit Sub
        
TrataErro:
    BG_LogFile_Erros "Erro  ao Procurar: " & Err.Description, Me.Name, "FuncaoProcurar", True
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    Resume Next
End Sub

Public Sub FuncaoInserir()
    
End Sub

Public Sub FuncaoModificar()
    'nada
End Sub
    
Private Sub BD_Update()
    'nada
End Sub

Public Sub FuncaoRemover()
    'nada
End Sub

Private Sub BD_Delete()
    'nada
End Sub

Public Sub Funcao_DataActual()
    'nada
End Sub

Private Sub InsereItemEstrutura(Index As Long, agrupador_estrutura As Long, requisicao As Long, codigo_tubo As String, _
                                data_prevista As String, data_chegada As String, hora_chegada As String, _
                                utilizador_chegada As String, data_colheita As String, hora_colheita As String, _
                                data_eliminacao As String, hora_eliminacao As String, utilizador_eliminacao As String, _
                                codigo_local As Integer, codigo_analise As String, descricao_analise As String, _
                                descricao_tubo As String, codigo_etiqueta As String, motivo_eliminacao As String, _
                                numero_garrafa As String, codigo_produto As String, descricao_produto As String, _
                                flag_transito As Boolean, num_obs_ana As Integer, dt_ini_transp As String, hr_ini_transp As String, _
                                user_ini_transp As String, dt_fim_transp As String, hr_fim_transp As String, user_fim_transp As String, _
                                utilizador_colheita As String, cod_local_ana As String, seq_req_tubo As Long, estado_tubo As Integer, _
                                seq_req_tubo_local As Integer, flg_nota As Integer, etiqueta As String)
    Dim i As Integer
    totalTubos = totalTubos + 1
    ReDim Preserve eTubos(totalTubos)
    eTubos(totalTubos).Index = Index
    eTubos(totalTubos).agrupador_estrutura = agrupador_estrutura
    eTubos(totalTubos).requisicao = requisicao
    eTubos(totalTubos).codigo_tubo = codigo_tubo
    eTubos(totalTubos).data_prevista = data_prevista
    eTubos(totalTubos).data_chegada = data_chegada
    eTubos(totalTubos).hora_chegada = hora_chegada
    eTubos(totalTubos).utilizador_chegada = utilizador_chegada
    eTubos(totalTubos).codigo_local = codigo_local
    eTubos(totalTubos).codigo_analise = codigo_analise
    eTubos(totalTubos).data_colheita = data_colheita
    eTubos(totalTubos).hora_colheita = hora_colheita
    eTubos(totalTubos).utilizador_colheita = utilizador_colheita
    eTubos(totalTubos).data_eliminacao = data_eliminacao
    eTubos(totalTubos).hora_eliminacao = hora_eliminacao
    eTubos(totalTubos).utilizador_eliminacao = utilizador_eliminacao
    eTubos(totalTubos).descricao_tubo = descricao_tubo
    eTubos(totalTubos).codigo_etiqueta = codigo_etiqueta
    eTubos(totalTubos).descricao_analise = descricao_analise
    eTubos(totalTubos).motivo_eliminacao = motivo_eliminacao
    eTubos(totalTubos).numero_garrafa = numero_garrafa
    eTubos(totalTubos).codigo_produto = codigo_produto
    eTubos(totalTubos).descricao_produto = descricao_produto
    eTubos(totalTubos).flag_transito = flag_transito
    eTubos(totalTubos).num_obs_ana = num_obs_ana
    eTubos(totalTubos).dt_inicio_transp = dt_ini_transp
    eTubos(totalTubos).hr_inicio_transp = hr_ini_transp
    eTubos(totalTubos).user_inicio_transp = user_ini_transp
    eTubos(totalTubos).dt_fim_transp = dt_fim_transp
    eTubos(totalTubos).hr_fim_transp = hr_fim_transp
    eTubos(totalTubos).user_fim_transp = user_fim_transp
    eTubos(totalTubos).cod_local_ana = cod_local_ana
    eTubos(totalTubos).seq_req_tubo = seq_req_tubo
    eTubos(totalTubos).indiceEstrutTubos = mediComboValorNull
    eTubos(totalTubos).estado_tubo = estado_tubo
    eTubos(totalTubos).seq_req_tubo_local = seq_req_tubo_local
    eTubos(totalTubos).flg_nota = flg_nota
    'NELSONPSILVA CHVNG-7461 29.10.2018
    eTubos(totalTubos).etiqueta = etiqueta
    '
    If flg_nota = mediSim Then
        eTubos(totalTubos).descricao_tubo = eTubos(totalTubos).descricao_tubo & " (*)"
    End If
    
    If codigo_analise = "" And codigo_tubo <> "" Then
        eTubos(totalTubos).flg_informacao = VerificaInfoTubos(seq_req_tubo)
    End If
    If codigo_analise <> "" And codigo_analise <> "N/D" Then
        VerificaRegrasLocais codigo_analise, cod_local_ana
        For i = 1 To totalRegras
            If codigo_analise = estrutRegras(i).cod_ana And estrutRegras(i).cod_local <> cod_local_ana Then
                ActualizaLocalAnaAcrescentadas codigo_analise, estrutRegras(i).cod_local, estrutRegras(i).cod_obrig, CInt(totalTubos)
            End If
        Next
    End If
    If cod_local_ana <> CStr(gCodLocal) And cod_local_ana <> "" Then
        eTubos(agrupador_estrutura).cod_local_extra = cod_local_ana
    End If
End Sub

Private Sub ActualizaItemEstrutura(Index As Long, agrupador_estrutura As Long, requisicao As Long, codigo_tubo As String, _
                                   data_prevista As String, data_chegada As String, hora_chegada As String, _
                                   utilizador_chegada As String, data_colheita As String, hora_colheita As String, _
                                   data_eliminacao As String, hora_eliminacao As String, utilizador_eliminacao As String, _
                                   codigo_local As Integer, codigo_analise As String, descricao_analise As String, _
                                   descricao_tubo As String, codigo_etiqueta As String, motivo_eliminacao As Integer, _
                                   numero_garrafa As String, codigo_produto As String, descricao_produto As String)

    eTubos(Index).agrupador_estrutura = agrupador_estrutura
    eTubos(Index).requisicao = requisicao
    eTubos(Index).codigo_tubo = codigo_tubo
    eTubos(Index).data_prevista = data_prevista
    eTubos(Index).data_chegada = data_chegada
    eTubos(Index).hora_chegada = hora_chegada
    eTubos(Index).utilizador_chegada = utilizador_chegada
    eTubos(Index).codigo_local = codigo_local
    eTubos(Index).codigo_analise = codigo_analise
    eTubos(Index).data_colheita = data_colheita
    eTubos(Index).hora_colheita = hora_colheita
    eTubos(Index).data_eliminacao = data_eliminacao
    eTubos(Index).hora_eliminacao = hora_eliminacao
    eTubos(Index).utilizador_eliminacao = utilizador_eliminacao
    eTubos(Index).descricao_tubo = descricao_tubo
    eTubos(Index).codigo_etiqueta = codigo_etiqueta
    eTubos(Index).descricao_analise = descricao_analise
    eTubos(Index).motivo_eliminacao = motivo_eliminacao
    eTubos(Index).numero_garrafa = numero_garrafa
    eTubos(Index).codigo_produto = codigo_produto
    eTubos(Index).descricao_produto = descricao_produto
    
End Sub

Private Sub LvTubos_DblClick()

    Dim bParent As Boolean
    Dim lStructureIndex As Long
    
    On Error GoTo TrataErro
    lStructureIndex = RetornaIndiceTubo(LvTubos.SelectedItem.Index, bParent, "")
    If (LvTubos.SelectedItem.Index <> Empty) Then
        If (bParent) Then
            BeforeHandlePipeExecution LvTubos.SelectedItem
        Else
        End If
        
        If (CampoDeFocus.Enabled) Then: CampoDeFocus.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao LvTubos_DblClick: " & Err.Description, Me.Name, "LvTubos_DblClick", True
    Exit Sub
    Resume Next
End Sub

Private Sub HandleListViewTree(ByVal Item As MSComctlLib.ListItem, iManualKey As Variant)
   
    On Error GoTo TrataErro
    If (Item.SmallIcon = ListViewIcons.cIconCollapsed And BL_HandleNull(iManualKey, vbKeyRight) = vbKeyRight) Then: ExpandirLV Item: Exit Sub
    If (Item.SmallIcon = ListViewIcons.cIconExpanded And BL_HandleNull(iManualKey, vbKeyLeft) = vbKeyLeft) Then: ComprimirLV Item: Exit Sub
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao HandleListViewTree: " & Err.Description, Me.Name, "HandleListViewTree", True
    Exit Sub
    Resume Next
End Sub

Private Sub ComprimirLV(ByVal Item As MSComctlLib.ListItem)
   
    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim lStructureIndex As Long
    
    On Error GoTo TrataErro
    lStructureIndex = RetornaIndiceTubo(Item.Index)
    Call LimitesTubos(Item.text, Item.ListSubItems(LvTubosColunas.t_codigo_tubo).text, eTubos(lStructureIndex).agrupador_estrutura, lLowerBound, lUpperBound)
    For lItem = lLowerBound To lUpperBound: Call RemoveItemLV(Item): Next
    Item.SmallIcon = ListViewIcons.cIconCollapsed

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao ComprimirLV: " & Err.Description, Me.Name, "ComprimirLV", True
    Exit Sub
    Resume Next
End Sub

Private Sub ExpandirLV(ByVal Item As MSComctlLib.ListItem)

    Dim lItem As Long
    Dim iShift As Integer
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim lStructure As Long
    
    On Error GoTo TrataErro
    lStructure = RetornaIndiceTubo(Item.Index)
    Call LimitesTubos(Item.text, Item.ListSubItems(LvTubosColunas.t_codigo_tubo).text, eTubos(lStructure).agrupador_estrutura, lLowerBound, lUpperBound)
    iShift = Item.Index + 1
    For lItem = lLowerBound To lUpperBound: Call AdicionaItemLV(lItem, CLng(iShift), , , True): iShift = iShift + 1: Next
    Item.SmallIcon = ListViewIcons.cIconExpanded

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao ExpandirLV: " & Err.Description, Me.Name, "ExpandirLV", True
    Exit Sub
    Resume Next
End Sub

Private Sub LimitesTubos(requisicao As String, codigo_tubo As String, agrupador_estrutura As Long, Optional ByRef lLowerBound As Long, Optional ByRef lUpperBound As Long)

    Dim lItem As Long
    
    On Error GoTo TrataErro
    For lItem = 1 To UBound(eTubos)
       If (eTubos(lItem).requisicao = requisicao And eTubos(lItem).codigo_tubo = codigo_tubo And eTubos(lItem).agrupador_estrutura = agrupador_estrutura) Then
            If (eTubos(lItem).Index = Empty) Then: lLowerBound = lItem + 1: lUpperBound = lLowerBound
            If (eTubos(lItem).Index <> Empty) Then: lUpperBound = lItem
        End If
    Next
    Exit Sub
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao LimitesTubos: " & Err.Description, Me.Name, "LimitesTubos", True
    Exit Sub
    Resume Next
End Sub

Private Function RetornaIndiceTubo(lItem As Long, Optional ByRef bParent As Boolean, Optional cod_etiqueta As String) As Integer
    Dim i As Integer

    On Error GoTo TrataErro
    If lItem > 0 Then
        RetornaIndiceTubo = lItem
        RetornaIndiceTubo = CInt(Split(Split(LvTubos.ListItems(lItem).key, "KEY_")(1), "_")(1))
        bParent = CBool(eTubos(RetornaIndiceTubo).Index = Empty)
    
    Else
        For i = 0 To totalTubos
            If gSeqReqTuboLocal <> mediSim Then
                If Right("00" & eTubos(i).codigo_etiqueta, 2) = cod_etiqueta And cod_etiqueta <> "" Then
                    RetornaIndiceTubo = i
                End If
            Else
                If Right("00" & eTubos(i).seq_req_tubo_local, 2) = cod_etiqueta And cod_etiqueta <> "" Then
                    RetornaIndiceTubo = i
                End If
            End If
        Next
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao RetornaIndiceTubo: " & Err.Description, Me.Name, "RetornaIndiceTubo", True
    Exit Function
    Resume Next
End Function

Private Function AdicionaItemLV(lItem As Long, lIndex As Long, Optional bParent As Boolean, Optional bAutomatic As Boolean, Optional bShowDirection As Boolean) As Boolean

    Dim produto_info As Collection
    Dim i As Integer
    On Error GoTo TrataErro
    With LvTubos.ListItems.Add(lIndex, "KEY_" & eTubos(lItem).agrupador_estrutura & "_" & lItem, IIf(bParent, eTubos(lItem).requisicao, Empty), Empty, IIf(bParent, IIf(bAutomatic, ListViewIcons.cIconExpanded, ListViewIcons.cIconCollapsed), Empty))
        If eTubos(lItem).flg_informacao = mediSim Then
            .ListSubItems.Add , , "", 5, "Tubo com informa��es"
        Else
            .ListSubItems.Add , , "", Empty, Empty
        End If
        .ListSubItems.Add , , IIf(bParent, eTubos(lItem).codigo_tubo, Empty)
        If bParent Then
            If gColheitaChegada = gEnumColheitaChegada.ColheitaTubo And eTubos(lItem).data_colheita = "" And gEstruturaTubos(eTubos(lItem).indiceEstrutTubos).hora_colheita <> "" Then
                .ListSubItems.Add , , eTubos(lItem).descricao_tubo & "(" & gEstruturaTubos(eTubos(lItem).indiceEstrutTubos).hora_colheita & ")", Empty, Empty
            Else
                .ListSubItems.Add , , eTubos(lItem).descricao_tubo, Empty, Empty
            End If
        Else
            If eTubos(lItem).num_obs_ana > 0 Then
                .ListSubItems.Add , , eTubos(lItem).codigo_analise & Space(1) & eTubos(lItem).descricao_analise, 5, "Identifica��o Garrafa: " & eTubos(lItem).numero_garrafa
            Else
                .ListSubItems.Add , , Space(5) & eTubos(lItem).codigo_analise & Space(1) & eTubos(lItem).descricao_analise, Empty, "Identifica��o Garrafa: " & eTubos(lItem).numero_garrafa
            End If
            If eTubos(lItem).cod_local_ana <> "" And gMultiLocalAmbito = mediSim Then
                For i = 1 To gTotalLocais
                    If eTubos(lItem).cod_local_ana = gEstruturaLocais(i).cod_local Then
                        .ListSubItems.Add , , gEstruturaLocais(i).descr_local, Empty, Empty
                        ' martelan�o para gaia
                        If gEstruturaLocais(i).cod_local = 2 Then
                            .ListSubItems.Item(3).ForeColor = vbRed
                        Else
                            .ListSubItems.Item(3).ForeColor = .ListSubItems.Item(2).ForeColor
                        End If
                        Exit For
                    End If
                Next
            End If
        End If
        .ListSubItems.Add , , IIf(bParent, eTubos(lItem).data_prevista, Empty)
        .ListSubItems.Add , , IIf(bParent, eTubos(lItem).data_chegada & Space(1) & eTubos(lItem).hora_chegada, Empty)
        .ListSubItems.Add , , IIf(bParent, IIf(eTubos(lItem).utilizador_chegada <> Empty, BL_DevolveNomeUtilizador(CStr(eTubos(lItem).utilizador_chegada), "", ""), Empty), Empty)
        .ListSubItems.Add , , IIf(bParent, eTubos(lItem).data_colheita & Space(1) & eTubos(lItem).hora_colheita, Empty)
        .ListSubItems.Add , , IIf(bParent, IIf(eTubos(lItem).utilizador_colheita <> Empty, IIf(CStr(eTubos(lItem).utilizador_colheita) = "", "", BL_DevolveNomeUtilizadorColheita(CStr(eTubos(lItem).utilizador_colheita))), ""), "")
        .ListSubItems.Add , , IIf(bParent, eTubos(lItem).dt_inicio_transp & Space(1) & eTubos(lItem).hr_inicio_transp, Empty)
        .ListSubItems.Add , , IIf(bParent, eTubos(lItem).dt_fim_transp & Space(1) & eTubos(lItem).hr_fim_transp, Empty)
        If eTubos(lItem).estado_tubo <> mediComboValorNull Then
            .ListSubItems.Add , , IIf(Not bParent, eTubos(lItem).estado_tubo, Empty)
        End If
        '.ListSubItems.Add , , IIf(bParent, BL_DevolveDescrLocal(CStr(eTubos(lItem).codigo_local)), Empty)
    End With
    LvTubos.ListItems(lIndex).Bold = True
    Call LimpaSubItems(lIndex)
    AdicionaItemLV = True
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao AdicionaItemLV: " & Err.Description, Me.Name, "AdicionaItemLV", True
    Exit Function
    Resume Next
End Function

Private Sub RemoveItemLV(ByVal Item As MSComctlLib.ListItem)
   
    On Error GoTo TrataErro
    LvTubos.ListItems.Remove Item.Index + 1
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao RemoveItemLV: " & Err.Description, Me.Name, "RemoveItemLV", True
    Exit Sub
    Resume Next
End Sub

Private Function ProcuraRegistos(ByVal requicao As Long) As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim sql As String
    Dim RsTubos As ADODB.recordset
    Dim rsAna As ADODB.recordset
    Dim Index As Long
    Dim agrupador_estrutura As Long
    Dim RsM As New ADODB.recordset
    Dim fgrid As MSFlexGrid
    On Error GoTo TrataErro
    TB_ProcuraTubos Nothing, requicao, False
    
    Set RsTubos = New ADODB.recordset
    RsTubos.CursorLocation = adUseServer
    RsTubos.CursorType = adOpenStatic
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    sql = ConstroiQuery1(requicao)
    RsTubos.Open sql, gConexao
    totalTubosSec = 0
    ReDim estrutTubosSec(0)
    agrupador_estrutura = 1
    
    While (Not RsTubos.EOF)
        Index = Empty
        PreencheTubosSec RsTubos!cod_tubo, "", BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), BL_HandleNull(RsTubos!cod_etiq, ""), BL_HandleNull(RsTubos!n_req, "")
        'NELSONPSILVA CHVNG-7461 29.10.2018 Adicionado a "etiqueta" � estrutura
        InsereItemEstrutura Index, agrupador_estrutura, BL_HandleNull(RsTubos!n_req, Empty), BL_HandleNull(RsTubos!cod_tubo, Empty), _
                            BL_HandleNull(RsTubos!dt_previ, Empty), BL_HandleNull(RsTubos!dt_chega, Empty), _
                            BL_HandleNull(RsTubos!hr_chega, Empty), BL_HandleNull(RsTubos!user_chega, Empty), _
                            BL_HandleNull(RsTubos!dt_colheita, Empty), BL_HandleNull(RsTubos!hr_colheita, Empty), _
                            BL_HandleNull(RsTubos!dt_eliminacao, Empty), BL_HandleNull(RsTubos!hr_eliminacao, Empty), _
                            BL_HandleNull(RsTubos!user_eliminacao, Empty), BL_HandleNull(RsTubos!local_chega, Empty), _
                            Empty, Empty, BL_HandleNull(RsTubos!descR_tubo, Empty), BL_HandleNull(RsTubos!cod_etiq, Empty), _
                            BL_HandleNull(RsTubos!mot_novo, Empty), Empty, BL_HandleNull(RsTubos!cod_produto, Empty), _
                            BL_HandleNull(RsTubos!descr_produto, Empty), CBool(BL_HandleNull(RsTubos!flg_transito, Empty) = mediSim), 0, _
                            BL_HandleNull(RsTubos!dt_inicio_transporte, Empty), BL_HandleNull(RsTubos!hr_inicio_transporte, Empty), _
                            BL_HandleNull(RsTubos!user_inicio_transporte, Empty), BL_HandleNull(RsTubos!dt_fim_transporte, Empty), _
                            BL_HandleNull(RsTubos!hr_fim_transporte, Empty), BL_HandleNull(RsTubos!user_fim_transporte, Empty), _
                            BL_HandleNull(RsTubos!user_colheita, Empty), Empty, BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), _
                            BL_HandleNull(RsTubos!estado_tubo, mediComboValorNull), BL_HandleNull(RsTubos!seq_req_tubo_local, mediComboValorNull), _
                            BL_HandleNull(RsTubos!flg_nota, 0), BL_HandleNull(RsTubos!etiqueta, Empty)
                            
        sql = ConstroiQuery2(requicao, BL_HandleNull(RsTubos!cod_tubo, Empty), BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull))
        rsAna.Open sql, gConexao
        If (rsAna.RecordCount = Empty) Then
            Index = Index + 1
            InsereItemEstrutura Index, agrupador_estrutura, BL_HandleNull(RsTubos!n_req, Empty), BL_HandleNull(RsTubos!cod_tubo, Empty), _
                                BL_HandleNull(RsTubos!dt_previ, Empty), Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, _
                                "N/D", Empty, Empty, Empty, Empty, BL_HandleNull(RsTubos!id_garrafa, Empty), Empty, Empty, Empty, 0, Empty, Empty, _
                                Empty, Empty, Empty, Empty, Empty, Empty, BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), mediComboValorNull, _
                                mediComboValorNull, 0, BL_HandleNull(RsTubos!etiqueta, Empty)
        End If
        While (Not rsAna.EOF)
        
            'TUBOS SECUNDARIOS
            If BL_HandleNull(rsAna!cod_tubo_sec, "") <> "" Then
                'RGONCALVES 16.07.2013 CHVNG-4306
                If Inibir_Impressao_Tubo_Sec(BL_HandleNull(rsAna!cod_tubo_sec, "")) = False Then
                '
                    PreencheTubosSec RsTubos!cod_tubo, BL_HandleNull(rsAna!cod_tubo_sec, ""), BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), BL_HandleNull(RsTubos!cod_etiq, ""), BL_HandleNull(RsTubos!n_req, "")
                End If
            Else
                If Mid(BL_HandleNull(rsAna!cod_ana, Empty), 1, 1) = "C" Then
                    sql = "SELECT distinct cod_tubo_sec FROM sl_ana_s WHERE cod_ana_s in (Select cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_ana, Empty)) & ")"
                ElseIf Mid(BL_HandleNull(rsAna!cod_ana, Empty), 1, 1) = "P" Then
                    sql = "SELECT distinct cod_tubo_sec FROM sl_ana_s WHERE cod_ana_s in (Select cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_ana, Empty)) & ")"
                End If
                RsM.CursorLocation = adUseServer
                RsM.CursorType = adOpenStatic
                RsM.Open sql, gConexao
                If RsM.RecordCount >= 1 Then
                    While Not RsM.EOF
                        If BL_HandleNull(RsM!cod_tubo_sec, "") <> "" Then
                            'RGONCALVES 16.07.2013 CHVNG-4306
                            If Inibir_Impressao_Tubo_Sec(BL_HandleNull(RsM!cod_tubo_sec, "")) = False Then
                                PreencheTubosSec RsTubos!cod_tubo, BL_HandleNull(RsM!cod_tubo_sec, ""), BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), BL_HandleNull(RsTubos!cod_etiq, ""), BL_HandleNull(RsTubos!n_req, "")
                            End If
                        End If
                        RsM.MoveNext
                    Wend
                End If
                RsM.Close
                
            End If
            
            Index = Index + 1
            InsereItemEstrutura Index, agrupador_estrutura, BL_HandleNull(RsTubos!n_req, Empty), BL_HandleNull(RsTubos!cod_tubo, Empty), _
                                BL_HandleNull(RsTubos!dt_previ, Empty), Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, _
                                BL_HandleNull(rsAna!cod_ana, Empty), BL_HandleNull(rsAna!descr_ana, Empty), _
                                Empty, Empty, Empty, BL_HandleNull(RsTubos!id_garrafa, Empty), Empty, Empty, Empty, BL_HandleNull(rsAna!conta, 0), _
                                Empty, Empty, Empty, Empty, Empty, Empty, Empty, BL_HandleNull(rsAna!cod_local_ana, Empty), _
                                BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), mediComboValorNull, _
                                mediComboValorNull, 0, BL_HandleNull(RsTubos!etiqueta, Empty)
            rsAna.MoveNext
        Wend
        If (rsAna.state = adStateOpen) Then: rsAna.Close
        RsTubos.MoveNext
        agrupador_estrutura = agrupador_estrutura + 1
    Wend
    
    For i = 1 To totalTubos
        For j = 1 To gTotalTubos
            If eTubos(i).codigo_tubo = gEstruturaTubos(j).CodTubo And eTubos(i).seq_req_tubo = gEstruturaTubos(j).seq_req_tubo Then
                eTubos(i).indiceEstrutTubos = j
                Exit For
            End If
        Next j
    Next i

    If (RsTubos.RecordCount = Empty) Then: BG_Mensagem mediMsgBox, "N�o encontrou registos!", vbInformation, " Procura"
    If (RsTubos.state = adStateOpen) Then: RsTubos.Close
    ProcuraRegistos = True
    
Exit Function
TrataErro:
    ProcuraRegistos = False
    BG_LogFile_Erros "Erro  ProcuraRegistos : " & Err.Description & vbCrLf & sql, Me.Name, "ProcuraRegistos", True
    Exit Function
    Resume Next
End Function

Private Sub LimpaSubItems(lItem As Long)
    
    Dim vSubItem As Variant
    
    On Error GoTo TrataErro
    For Each vSubItem In LvTubos.ListItems(lItem).ListSubItems
        If (vSubItem.text = "0") Then: vSubItem.text = Empty
    Next
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao LimpaSubItems: " & Err.Description, Me.Name, "LimpaSubItems", True
    Exit Sub
    Resume Next
End Sub

Private Sub ExpandirLvAutom()
    
    Dim lItem As Long
            
    On Error GoTo TrataErro
    For lItem = 1 To UBound(eTubos)
        Call AdicionaItemLV(lItem, LvTubos.ListItems.Count + 1, (eTubos(lItem).Index = Empty), True)
    Next
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao ExpandirLvAutom: " & Err.Description, Me.Name, "ExpandirLvAutom", True
    Exit Sub
    Resume Next
End Sub

Private Sub LvTubos_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If (LvTubos.ListItems.Count = Empty) Then: Exit Sub
    If (VisualizaInformacaoComplementar(Item.Index)) Then: FrInfoComplementar.Visible = True: Exit Sub
    FrInfoComplementar.Visible = False
    
End Sub

Private Sub LvTubos_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo TrataErro
    If (LvTubos.ListItems.Count = Empty) Then: Exit Sub
    Select Case KeyCode
        Case vbKeyLeft, vbKeyRight: HandleListViewTree LvTubos.SelectedItem, KeyCode
        Case Else: ' Empty
    End Select
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao LvTubos_KeyDown: " & Err.Description, Me.Name, "LvTubos_KeyDown", True
    Exit Sub
    Resume Next
End Sub

Private Function GeraLV() As Boolean

    Dim lItem As Long
    Dim vSubItem As Variant
    Dim bAutomatic As Boolean
        
    On Error GoTo TrataErro
    bAutomatic = CBool(gExpandeAutoTubosV2)
    For lItem = 1 To UBound(eTubos)
        If (eTubos(lItem).Index = Empty And Not bAutomatic) Then: If (Not AdicionaItemLV(lItem, LvTubos.ListItems.Count + 1, True, bAutomatic, False)) Then Exit Function
        If (bAutomatic) Then: If (Not AdicionaItemLV(lItem, LvTubos.ListItems.Count + 1, CBool(eTubos(lItem).Index = Empty), bAutomatic, False)) Then Exit Function
    Next
    GeraLV = True
    
Exit Function
TrataErro:
    GeraLV = False
    BG_LogFile_Erros "Erro  GeraLV : " & Err.Description, Me.Name, "GeraLV", True
    Exit Function
    Resume Next
End Function

Private Function ConstroiQuery1(requicao As Long) As String

    Dim sql As String

    On Error GoTo TrataErro
    sql = "select x1.seq_req_tubo, x1.cod_tubo, x1.cod_local, x1.n_req, x1.dt_previ, x1.dt_chega, x1.hr_chega, x1.dt_eliminacao, x1.hr_eliminacao, x1.user_eliminacao,"
    sql = sql & " x1.user_chega, x1.local_chega, x1.dt_saida, x1.hr_saida, x1.user_saida, x1.local_saida, x1.dt_colheita, x1.hr_colheita, "
    sql = sql & " x1.mot_novo, x1.id_garrafa,x1.flg_transito, "
    sql = sql & " x2.descr_tubo, x2.cod_etiq, "
    sql = sql & " x3.cod_produto, x3.descr_produto, x1.dt_inicio_Transporte, x1.hr_inicio_transporte, x1.user_inicio_transporte , "
    sql = sql & " x1.dt_fim_Transporte, x1.hr_fim_transporte, x1.user_fim_transporte, x1.user_colheita, x1.estado_tubo, x1.seq_req_tubo_local, x1.flg_nota, x1.etiqueta "
    sql = sql & " FROM sl_req_tubo x1, sl_tubo x2 LEFT OUTER JOIN sl_produto x3 ON x2.cod_prod= x3.cod_produto WHERE n_req =" & requicao & " AND x1.cod_tubo = x2.cod_tubo"
    sql = sql & " ORDER BY cod_tubo"
    ConstroiQuery1 = sql
    
Exit Function
TrataErro:
    ConstroiQuery1 = ""
    BG_LogFile_Erros "Erro  ConstroiQuery1 : " & Err.Description, Me.Name, "ConstroiQuery1", True
    Exit Function
    Resume Next
End Function

Private Function ConstroiQuery2(requisicao As Long, codigo_tubo As String, seq_req_tubo As Long) As String

    Dim sSql As String
    Dim Item As Variant
    Dim rsAna As ADODB.recordset
    Dim devolve_tubo As Boolean
    
    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    ConstroiQuery2 = "select distinct slv_analises.cod_ana, descr_ana, cod_tubo_sec,"
    ConstroiQuery2 = ConstroiQuery2 & " (Select count(*) from sl_obs_ana_req where slv_analises.cod_ana = sl_obs_ana_req.cod_agrup and sl_obs_ana_req.n_req = " & requisicao & ") conta, sl_ana_acrescentadas.cod_local cod_local_ana"
    ConstroiQuery2 = ConstroiQuery2 & " from slv_analises, sl_ana_acrescentadas where slv_analises.cod_ana in ("
    sSql = "select cod_agrup from sl_marcacoes where n_req = " & requisicao & " AND seq_req_tubo = " & seq_req_tubo
    sSql = sSql & " union select cod_agrup from sl_realiza where n_req = " & requisicao & " AND seq_req_tubo = " & seq_req_tubo
    rsAna.Open sSql, gConexao
    If (rsAna.RecordCount = Empty) Then:  ConstroiQuery2 = ConstroiQuery2 & "null)": Exit Function
    While (Not rsAna.EOF)
        ConstroiQuery2 = ConstroiQuery2 & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_agrup, Empty)) & ","
        devolve_tubo = True
        rsAna.MoveNext
    Wend
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    If (Not devolve_tubo) Then
        ConstroiQuery2 = ConstroiQuery2 & "null)"
    Else
        ConstroiQuery2 = Mid(ConstroiQuery2, 1, Len(ConstroiQuery2) - 1) & ")"
    End If
    ConstroiQuery2 = ConstroiQuery2 & " AND slv_analises.cod_ana = sl_ana_acrescentadas.cod_agrup and sl_ana_acrescentadas.n_req =" & requisicao
    ConstroiQuery2 = ConstroiQuery2 & " AND (flg_eliminada IS NULL or flg_eliminada = 0 )"
    
    Exit Function
    
TrataErro:
    ConstroiQuery2 = ""
    BG_LogFile_Erros "Erro  ConstroiQuery2 : " & Err.Description, Me.Name, "ConstroiQuery2", True
    Exit Function
    Resume Next
    
End Function

' pferreira 2010.06.07
' Handle list view double click.
Private Sub HandleListViewDoubleClick()
  
    Dim lvhti As LVHITTESTINFO
    Dim rc As RECT
    Dim lListItem As ListItem
      
    On Error GoTo TrataErro
    Call GetCursorPos(lvhti.pt)
    Call ScreenToClient(lhwndListView, lvhti.pt)
    If (ListView_SubItemHitTest(lhwndListView, lvhti) = LVI_NOITEM) Then: Exit Sub
    If (lvhti.iSubItem = Empty) Then: Exit Sub
    If (ListView_GetSubItemRect(lhwndListView, lvhti.iItem, lvhti.iSubItem, LVIR_LABEL, rc)) Then
        Call MapWindowPoints(lhwndListView, hwnd, rc, 2)
        lListItemIndex = lvhti.iItem + 1
        lListSubItem = lvhti.iSubItem
        If (Not ValidateColumnToEdit(LvTubos.ColumnHeaders(lListSubItem + 1).key)) Then: Exit Sub
        TextAuxiliar.text = LvTubos.ListItems(lListItemIndex).SubItems(lListSubItem)
        TextAuxiliar.Tag = LvTubos.ColumnHeaders(lListSubItem + 1).key
        LvTubos.ListItems(lListItemIndex).SubItems(lListSubItem) = ""
        Call SubClass(lhwndTextBox, AddressOf WndProc)
        EnableTextAuxiliar True, rc
        If (TextAuxiliar.Enabled) Then: TextAuxiliar.SetFocus
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao HandleListViewDoubleClick: " & Err.Description, Me.Name, "HandleListViewDoubleClick", True
    Exit Sub
    Resume Next
End Sub

' pferreira 2010.06.07
' Validate column to edit.
Private Function ValidateColumnToEdit(sColumnKey As String) As Boolean

    On Error GoTo TrataErro
    Select Case (sColumnKey)
        Case Else: ValidateColumnToEdit = False
    End Select
Exit Function
TrataErro:
    ValidateColumnToEdit = False
    BG_LogFile_Erros "Erro  ValidateColumnToEdit : " & Err.Description, Me.Name, "ValidateColumnToEdit", True
    Exit Function
    Resume Next
End Function

' pferreira 2010.06.07
' Handle editable columns.
Private Sub HandleEditableColumns(sColumnKey As String, sValue As String, lStructureIndex As Long)

    On Error GoTo TrataErro
    If (sValue = Empty) Then: Exit Sub
    Select Case (sColumnKey)
        'Case "OBSERVACAO": eProdutos(lStructureIndex).observacao = sValue
        Case Else:  ' Empty
    End Select
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao HandleEditableColumns: " & Err.Description, Me.Name, "HandleEditableColumns", True
    Exit Sub
    Resume Next
End Sub

Private Sub LvTubos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If (linhaActual = Empty) Then: Exit Sub
    LvTubos.ListItems.Item(linhaActual).Selected = True
    DoEvents
    Select Case (Button)
        Case MouseButtonConstants.vbRightButton: Call LvTubos_ItemClick(LvTubos.SelectedItem): BeforeHandlePopMenu
        Case MouseButtonConstants.vbLeftButton: Call BeforeHandleListViewTree(X, Y)
        Case Else: ' Empty
    End Select
    
End Sub

Private Sub LvTubos_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Integer
    linhaActual = 0
    For i = 1 To LvTubos.ListItems.Count
        If Y >= LvTubos.ListItems(i).top And Y < LvTubos.ListItems(i).top + LvTubos.ListItems(i).Height Then
            linhaActual = i
            Exit For
        End If
    Next
        
End Sub

Private Sub Opt_Click(Index As Integer)

    If (Opt(0).value = True) Then: tipo_pesquisa = MovimentosTubo.t_entrada
    If (Opt(1).value = True) Then: tipo_pesquisa = MovimentosTubo.t_saida
    
End Sub



' pferreira 2010.06.07
' Keypress event for the object TextAuxiliar.
Private Sub TextAuxiliar_KeyPress(KeyAscii As Integer)

    Dim lStructureIndex As Long
    Dim rc As RECT
    
    On Error GoTo TrataErro
    Select Case (KeyAscii)
        Case vbKeyEscape:
            Call HideTextBox(False):
            Call EnableTextAuxiliar(False, rc)
        Case vbKeyReturn:
            lStructureIndex = RetornaIndiceTubo(LvTubos.SelectedItem.Index)
            HandleEditableColumns TextAuxiliar.Tag, TextAuxiliar.text, lStructureIndex
            Call HideTextBox(True)
            Call EnableTextAuxiliar(False, rc)
    End Select
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao TextAuxiliar_KeyPress: " & Err.Description, Me.Name, "TextAuxiliar_KeyPress", True
    Exit Sub
    Resume Next
End Sub

' pferreira 2010.06.07
' LostFocus event for the object TextAuxiliar.
Private Sub TextAuxiliar_LostFocus()
    
    Dim rc As RECT
    
    On Error GoTo TrataErro
    If (TextAuxiliar.Visible) Then: Call HideTextBox(False): Call EnableTextAuxiliar(False, rc)
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao TextAuxiliar_LostFocus: " & Err.Description, Me.Name, "TextAuxiliar_LostFocus", True
    Exit Sub
    Resume Next
End Sub

' pferreira 2010.06.07
' Enable or disable TextAuxiliar.
Private Sub EnableTextAuxiliar(bEnable As Boolean, rc As RECT)

    On Error GoTo TrataErro
    If (Not bEnable) Then
        TextAuxiliar.Visible = False
        TextAuxiliar.text = Empty
        If (LvTubos.Enabled) Then: LvTubos.SetFocus
    Else
        TextAuxiliar.Alignment = AlignmentConstants.vbLeftJustify
        TextAuxiliar.Move ((rc.left + 1) * Screen.TwipsPerPixelX), (rc.top * Screen.TwipsPerPixelY) + (2 * Screen.TwipsPerPixelY), ((rc.Right - rc.left) * Screen.TwipsPerPixelX) - 10, (rc.Bottom - rc.top) * Screen.TwipsPerPixelY - 100
        TextAuxiliar.ForeColor = &H808080
        If (LvTubos.SelectedItem.Index Mod 2 <> 0) Then: TextAuxiliar.BackColor = &H8000000F
        If (LvTubos.SelectedItem.Index Mod 2 = 0) Then: TextAuxiliar.BackColor = vbWhite
        TextAuxiliar.SelStart = 0
        TextAuxiliar.SelLength = Len(TextAuxiliar)
        TextAuxiliar.Visible = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao EnableTextAuxiliar: " & Err.Description, Me.Name, "EnableTextAuxiliar", True
    Exit Sub
    Resume Next
End Sub

' pferreira 2010.06.07
' Hide auxiliar text box.
Friend Sub HideTextBox(bApplyChanges As Boolean)

    On Error GoTo TrataErro
    If (bApplyChanges) Then: LvTubos.ListItems(lListItemIndex).SubItems(lListSubItem) = TextAuxiliar.text
    If (Not bApplyChanges) Then: LvTubos.ListItems(lListItemIndex).ListSubItems(lListSubItem).ReportIcon = Empty
    Call UnSubClass(lhwndTextBox): lListItemIndex = Empty

     
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao HideTextBox: " & Err.Description, Me.Name, "HideTextBox", True
    Exit Sub
    Resume Next
End Sub

Private Sub DefineLvTubos()
   
    On Error GoTo TrataErro
    With LvTubos
        .ColumnHeaders.Add(, , "Requisi��o", 1100, lvwColumnLeft).key = "REQUISICAO"
        .ColumnHeaders.Add(, , "", 200, lvwColumnLeft).key = ""
        .ColumnHeaders.Add(, , "Tubo", 600, lvwColumnLeft).key = "TUBO"
        .ColumnHeaders.Add(, , "Descr. Tubo", 1950, lvwColumnLeft).key = "DESCRICAO_TUBO"
        .ColumnHeaders.Add(, , "Dt. Prevista", 1950, lvwColumnLeft).key = "DATA_PREVISTA"
        .ColumnHeaders.Add(, , "Data Chegada", 1450, lvwColumnLeft).key = "DATA_CHEGADA"
        .ColumnHeaders.Add(, , "Util. Chegada", 1000, lvwColumnLeft).key = "UTILIZADOR"
        .ColumnHeaders.Add(, , "Data Colheita", 1450, lvwColumnLeft).key = "DATA_COLHEITA"
        .ColumnHeaders.Add(, , "Util. Colheita", 1500, lvwColumnLeft).key = "UTIL_COLHEITA"
        .ColumnHeaders.Add(, , "Inicio Transp", 1500, lvwColumnLeft).key = "INI_TRANSP"
        .ColumnHeaders.Add(, , "Fim Transp", 1500, lvwColumnLeft).key = "FIM_TRANSP"
        .ColumnHeaders.Add(, , "Estado", 1000, lvwColumnLeft).key = "ESTADO"
        '.ColumnHeaders.Add(, , "Local", 2000, lvwColumnLeft).Key = "LOCAL"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .SmallIcons = ImageListIcons
        .FullRowSelect = True
        .Checkboxes = False
        .ForeColor = LvTubosCores.t_cinzento
        .MultiSelect = False
    End With
    'BG_SetListViewColor LvTubos, PictureListColor, vbWhite, vbWhite
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao DefineLvTubos: " & Err.Description, Me.Name, "DefineLvTubos", True
    Exit Sub
    Resume Next
End Sub

Sub PreencheDadosUtente(SeqUtente As String)
    Dim Campos(8) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "n_proc_1"
    Campos(4) = "n_proc_2"
    Campos(5) = "n_cartao_ute"
    Campos(6) = "dt_nasc_ute"

    iret = BL_DaDadosUtente(Campos, retorno, SeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        EcUtente.text = ""
        EcNomeUtente.text = ""
        Exit Sub
    End If
    
    EcUtente.text = retorno(1) & Space(1) & retorno(0)
    EcNomeUtente.text = retorno(2)
    EcDataNasc.text = retorno(6)
    
    reqAtual.dt_nasc_ute = retorno(6)
    reqAtual.Utente = retorno(0)
    reqAtual.t_utente = retorno(1)
    reqAtual.nome_ute = retorno(2)
    reqAtual.n_proc_1 = retorno(3)
    reqAtual.n_proc_2 = retorno(4)
    
   'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        Dim requestJson As String
        Dim responseJson As String
        Dim context As String
        Dim CamposEcra() As Object
        Dim NumCampos As Integer
        NumCampos = 4
        ReDim CamposEcra(0 To NumCampos - 1)
         Set CamposEcra(0) = EcNumReq
         Set CamposEcra(1) = EcUtente
         Set CamposEcra(2) = EcLocal
         Set CamposEcra(3) = EcPrescricao
         requestJson = "{" & Chr(34) & "n_req" & Chr(34) & ":" & Chr(34) & EcNumReq.text & Chr(34) & "}"
         responseJson = BL_TrataCamposRGPD(CamposEcra)
         responseJson = left$(responseJson, Len(responseJson) - 1)
         responseJson = responseJson & ", " & Chr(34) & "nome_ute" & Chr(34) & ":" & Chr(34) & EcNomeUtente.text & Chr(34) & "}"
        
        If Me.ExternalAccess = True Then
            context = "{" & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & ", " & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & Chr(34) & "}"
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & Me.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, context, IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        Else
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & Me.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        End If
    End If
    
End Sub

Private Function ValidaRequisicao() As Boolean
    
    Dim rsReq As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsReq = New ADODB.recordset
    Dim i As Integer
    Dim n_requis As String
    Dim tabela_aux As String
    Dim sSql As String
    n_requis = ""
    
    If (EcNumReq = Empty) Then: BG_Mensagem mediMsgBox, "� obrigat�rio introduzir o n�mero de requisi��o!", vbExclamation, " Aten��o": Exit Function
    If (Len(EcNumReq.text) > 9) Then: Exit Function
    gRequisicaoActiva = Right(EcNumReq.text, 7)
    
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Reutilizacao_Colheitas = mediSim Then
        'Call COLH_EstrutReutil(EcNumReq, COLH_DevolveNumColheita(EcNumReq))
        Call COLH_DevolveReqColheita(COLH_DevolveNumColheita(gRequisicaoActiva))
        If UBound(reqColh) > 0 Then
            For i = 1 To UBound(reqColh)
                n_requis = n_requis & reqColh(i).n_req & "','"
            Next i
            n_requis = "('" & Mid(n_requis, 1, Len(n_requis) - 2) & ")"
        Else
            n_requis = gRequisicaoActiva
        End If
        'CmdReq.Parameters(0).value = n_requis
    Else
        'CmdReq.Parameters(0).value = gRequisicaoActiva
        n_requis = gRequisicaoActiva
    End If
    '
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    campo_tabela = ""
    campo_condicao = ""
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        campo_tabela = ", slv_identif"
        campo_condicao = "AND sl_requis.seq_utente = slv_identif.seq_utente"
    Else
        campo_tabela = ", sl_identif"
        campo_condicao = "AND sl_requis.seq_utente = sl_identif.seq_utente"
    End If
    '
    sSql = "SELECT estado_req, sl_requis.seq_utente, cod_local, cod_proven, tipo_urgencia, dt_previ, dt_chega, t_urg, cod_proven, sl_requis.n_req, "
    sSql = sSql & " sl_tbf_t_urg.cor cor_urg "
    sSql = sSql & " FROM sl_requis, sl_tbf_t_urg " & campo_tabela & " "
    sSql = sSql & "  WHERE estado_req not in (" & BL_TrataStringParaBD(gEstadoReqBloqueada)
    sSql = sSql & "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ") "
    sSql = sSql & "  AND sl_requis.t_urg = sl_tbf_t_urg.cod_t_urg " & campo_condicao & " AND n_req in " & n_requis & " order by n_req asc"
    
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    
    rsReq.Open sSql, gConexao
    'Set rsReq = CmdReq.Execute
    If (Not rsReq.EOF) Then
       While Not rsReq.EOF
            If BL_HandleNull(rsReq!n_req, "") = gRequisicaoActiva Then
                Call PreencheDadosUtente(BL_HandleNull(rsReq!seq_utente, Empty))
                reqAtual.cod_proven = BL_HandleNull(rsReq!cod_proven, "")
                reqAtual.dt_previ = BL_HandleNull(rsReq!dt_previ, "")
                reqAtual.dt_chega = BL_HandleNull(rsReq!dt_chega, "")
                reqAtual.t_urg = BL_HandleNull(rsReq!t_urg, "")
                reqAtual.n_req = CStr(gRequisicaoActiva)
                
                Call PreencheLocal(BL_HandleNull(rsReq!cod_local, Empty))
                Call Preenche_Estado(BL_HandleNull(rsReq!estado_req, Empty))
                Call PreencheNotas
            End If
            Call ProcuraRegistos(rsReq!n_req)
            If BL_HandleNull(rsReq!cor_urg, vbBlack) = vbWhite Then
                EcNumReq.ForeColor = vbBlack
                EcEstado.ForeColor = vbBlack
            Else
                EcNumReq.ForeColor = BL_HandleNull(rsReq!cor_urg, vbBlack)
                EcEstado.ForeColor = BL_HandleNull(rsReq!cor_urg, vbBlack)
            End If
            EcCodProven.text = BL_HandleNull(rsReq!cod_proven, Empty)
            EcArea.text = BL_HandleNull(rsReq!tipo_urgencia, Empty)
            rsReq.MoveNext
        Wend
            ValidaRequisicao = True
    Else
        BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Chegada de Tubos"
        If (EcNumReq.Enabled) Then: EcNumReq.SetFocus
        Sendkeys ("{HOME}+{END}")
        ValidaRequisicao = False
    End If
    rsReq.Close
    Set rsReq = Nothing
    
Exit Function
TrataErro:
    ValidaRequisicao = False
    BG_LogFile_Erros "Erro  ao validar req : " & Err.Description, Me.Name, "ValidaRequisicao", True
    Exit Function
    Resume Next
End Function

Private Sub Preenche_Estado(EstadoReq As String)
    EcEstado = BL_DevolveEstadoReq(EstadoReq)
End Sub

Private Sub PreencheLocal(codigo_local As String)

    If (codigo_local = Empty) Then: Exit Sub
    EcLocal.text = codigo_local
    EcDescrLocal.text = BL_DevolveDescrLocal(codigo_local)

End Sub

Private Function ExecutaChegada(codigo_etiqueta As String, requisicao As Long, Index As Long, Optional codigo_tubo As String, Optional numero_garrafa As String) As Boolean

    Dim sql As String
    Dim data_chegada As String
    Dim hora_chegada As String
    Dim user_chegada As String
    Dim i As Integer
    Dim sSql As String
    Dim rsTubo As ADODB.recordset
    Dim rsAna As ADODB.recordset
    On Error GoTo TrataErro
    
    If (ValidaChegadaTubo(codigo_etiqueta, requisicao, codigo_tubo)) Then
        data_chegada = Bg_DaData_ADO
        hora_chegada = Bg_DaHora_ADO
        user_chegada = gCodUtilizador
        eTubos(Index).estado_tubo = gEstadosTubo.chegado
        If (Not ExecutaChegadaTubo(Index, requisicao, codigo_tubo, codigo_etiqueta, data_chegada, hora_chegada, user_chegada, numero_garrafa)) Then: Exit Function
        If (Not ExecutaChegadaProduto(requisicao, g_codigo_produto, data_chegada)) Then: Exit Function
                
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If gAtiva_Reutilizacao_Colheitas = mediSim And UBound(reqColh) > 0 Then
            For i = 1 To UBound(reqColh)
                If reqColh(i).n_req <> requisicao Then
                    sSql = "SELECT COUNT(*) conta FROM sl_req_tubo WHERE n_req = " & reqColh(i).n_req & " AND dt_chega IS NOT NULL"
                    Set rsTubo = New ADODB.recordset
                    rsTubo.CursorLocation = adUseServer
                    rsTubo.CursorType = adOpenStatic
                    rsTubo.Open sSql, gConexao
                    
                    If BL_HandleNull(rsTubo!conta, 0) = 0 Then
                        requisicao = reqColh(i).n_req
                    End If
                    rsTubo.Close
                    Set rsTubo = Nothing
                End If
                
                If (Not ExecutaChegadaRequisicao(requisicao, data_chegada, hora_chegada, user_chegada)) Then: Exit Function
                If (Not ExecutaChegadaMarcacao(requisicao, data_chegada, hora_chegada, codigo_tubo, codigo_etiqueta)) Then: Exit Function
                If (Not EnviaFacturacao(requisicao)) Then: Exit Function
                If (Not ActualizaComentarioFinal(requisicao, codigo_tubo, eTubos(Index).agrupador_estrutura)) Then: Exit Function
                Call BL_MudaEstadoReq(requisicao)
                'FG ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, "Tubo: " & eTubos(Index).descricao_tubo & " Chegou"
                ExecutaChegada = True
            Next i
        Else
             If (Not ExecutaChegadaRequisicao(requisicao, data_chegada, hora_chegada, user_chegada)) Then: Exit Function
                If (Not ExecutaChegadaMarcacao(requisicao, data_chegada, hora_chegada, codigo_tubo, codigo_etiqueta)) Then: Exit Function
                If (Not EnviaFacturacao(requisicao)) Then: Exit Function
                If (Not ActualizaComentarioFinal(requisicao, codigo_tubo, eTubos(Index).agrupador_estrutura)) Then: Exit Function
                Call BL_MudaEstadoReq(requisicao)
                'FG ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, "Tubo: " & eTubos(Index).descricao_tubo & " Chegou"
                ExecutaChegada = True
        End If
        '
    Else
        ExecutaChegada = False
    End If
'    EcNumReq.SetFocus
'    EcNumReq.SelStart = 0
'    EcNumReq.SelLength = Len(EcNumReq.Text)
    'BRUNODSANTOS 22.11.2017
    EcNumReq.text = ""
    '
Exit Function
TrataErro:
    ExecutaChegada = False
    BG_LogFile_Erros "Erro  ao dar chegada tubo : " & Err.Description, Me.Name, "ExecutaChegada", True
    Exit Function
    Resume Next
End Function



Private Function ValidaChegadaTubo(codigo_etiqueta As String, requisicao As Long, Optional codigo_tubo As String) As Boolean
    
    Dim sql As String
    Dim rsTubo As ADODB.recordset
    On Error GoTo TrataErro
    
    Set rsTubo = New ADODB.recordset
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    
    If (codigo_etiqueta <> Empty) Then
        If gSeqReqTuboLocal <> mediSim Then
            sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & " and  substr('00'|| sl_tubo.cod_etiq,length('00'|| sl_tubo.cod_etiq)-1,2)  = " & BL_TrataStringParaBD(codigo_etiqueta) & " and dt_chega is null "
        Else
            sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & " and  substr('00'|| sl_req_tubo.seq_req_tubo_local,length('00'|| sl_req_tubo.seq_req_tubo_local)-1,2)  = " & BL_TrataStringParaBD(codigo_etiqueta) & " and dt_chega is null "
        End If
    ElseIf (codigo_tubo <> Empty) Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & "  and sl_tubo.cod_tubo = " & BL_TrataStringParaBD(codigo_tubo) & " and dt_chega is null "
    End If
    rsTubo.Open sql, gConexao
    If (rsTubo.RecordCount = Empty) Then: Exit Function
    If (BL_HandleNull(rsTubo!dt_chega, Empty) = Empty) Then
        'codigo_tubo = Empty
        'g_codigo_produto = Empty
        ValidaChegadaTubo = True
    Else
        ValidaChegadaTubo = False
        'codigo_tubo = BL_HandleNull(RsTubo!cod_tubo, "")
        'g_codigo_produto = BL_HandleNull(RsTubo!cod_prod, "")
    End If
    If (rsTubo.state = adStateOpen) Then: rsTubo.Close
    
Exit Function
TrataErro:
    ValidaChegadaTubo = False
    BG_LogFile_Erros "Erro  ao ValidaChegadaTubo: " & Err.Description, Me.Name, "ValidaChegadaTubo", True
    Exit Function
    Resume Next
End Function

Private Function ValidaColheitaTubo(codigo_etiqueta As String, requisicao As Long, Optional codigo_tubo As String) As Boolean
    
    Dim sql As String
    Dim rsTubo As ADODB.recordset
    On Error GoTo TrataErro
    
    Set rsTubo = New ADODB.recordset
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    
    If (codigo_etiqueta <> Empty) Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_colheita,dt_chega, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & " and sl_tubo.cod_etiq = " & BL_TrataStringParaBD(codigo_etiqueta) & " and dt_chega is null "
    ElseIf (codigo_tubo <> Empty) Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_colheita,dt_chega, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & "  and sl_tubo.cod_tubo = " & BL_TrataStringParaBD(codigo_tubo) & " and dt_chega is null "
    End If
    rsTubo.Open sql, gConexao
    If (rsTubo.RecordCount = Empty) Then: Exit Function
    If BL_HandleNull(rsTubo!dt_chega, Empty) = Empty And BL_HandleNull(rsTubo!dt_colheita, Empty) = Empty Then
        ValidaColheitaTubo = True
    Else
        ValidaColheitaTubo = False
    End If
    If (rsTubo.state = adStateOpen) Then: rsTubo.Close
    
Exit Function
TrataErro:
    ValidaColheitaTubo = False
    BG_LogFile_Erros "Erro  ao ValidaColheitaTubo: " & Err.Description, Me.Name, "ValidaColheitaTubo", True
    Exit Function
    Resume Next
End Function

Private Function EnviaFacturacao(requisicao As Long) As Boolean
    
    Dim sql As String
    Dim RsFact As ADODB.recordset

    On Error GoTo TrataErro
    If (gEnviaFact_ChegaTubo <> mediSim) Or gTipoInstituicao <> gTipoInstituicaoHospitalar Then
        EnviaFacturacao = True:
        Exit Function
    End If

    If (gHIS_Import = 1 And HIS.nome = "SONHO") Then: If (BL_Abre_Conexao_HIS(gConnHIS, gOracle) <> mediSim) Then Exit Function
    'gConnHIS.BeginTrans
    Set RsFact = New ADODB.recordset
    RsFact.CursorLocation = adUseServer
    RsFact.CursorType = adOpenStatic
    sql = " select r.n_req,i.t_utente,i.utente, i.n_proc_1,  r.t_sit, r.n_epis,r.cod_efr,r.cod_proven, r.dt_chega,m.cod_perfil,m.cod_ana_c,m.cod_ana_s,m.cod_agrup, m.flg_facturado, r.cod_med, m.ord_ana, m.ord_ana_compl, m.ord_ana_perf  " & " from sl_requis r, sl_marcacoes m, sl_identif i " & " Where r.n_req = m.n_req " & " and r.seq_utente = i.seq_utente " & " and r.n_req = " & requisicao
    sql = sql & " UNION select r.n_req,i.t_utente,i.utente, i.n_proc_1,  r.t_sit, r.n_epis,r.cod_efr,r.cod_proven, r.dt_chega,m.cod_perfil,m.cod_ana_c,m.cod_ana_s,m.cod_agrup, m.flg_facturado, r.cod_med, m.ord_ana, m.ord_ana_compl, m.ord_ana_perf  " & " from sl_requis r, sl_realiza m, sl_identif i " & " Where r.n_req = m.n_req " & " and r.seq_utente = i.seq_utente " & " and r.n_req = " & requisicao & " order by cod_perfil, cod_ana_c,cod_ana_s, ord_ana, ord_ana_compl, ord_ana_perf "
    RsFact.Open sql, gConexao
    While Not RsFact.EOF
        If (BL_HandleNull(RsFact!Flg_Facturado, Empty) = 0 And (Len(BL_HandleNull(RsFact!n_epis, Empty)) > 0) And (BL_HandleNull(RsFact!t_sit, mediComboValorNull) <> -1)) Then
            If (IF_Factura_Analise(BL_HandleNull(RsFact!n_req, Empty), BL_HandleNull(RsFact!cod_efr, Empty), BL_HandleNull(RsFact!t_sit, mediComboValorNull), BL_HandleNull(RsFact!n_epis, Empty), BL_HandleNull(RsFact!dt_chega, Empty), BL_HandleNull(RsFact!n_proc_1, Empty), BL_HandleNull(RsFact!cod_proven, Empty), BL_HandleNull(RsFact!Cod_Perfil, Empty), BL_HandleNull(RsFact!cod_ana_c, Empty), BL_HandleNull(RsFact!cod_ana_s, Empty), BL_HandleNull(RsFact!cod_agrup, Empty), "", BL_HandleNull(RsFact!t_utente, mediComboValorNull), BL_HandleNull(RsFact!Utente, Empty), Empty, Empty, Empty, 1, BL_HandleNull(RsFact!cod_med, ""), "", "")) Then
                sql = "update sl_marcacoes set flg_facturado = 1 where n_req = " & BL_HandleNull(RsFact!n_req, Empty) & " and cod_perfil = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!Cod_Perfil, Empty)) & " and cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_c, Empty)) & " and cod_ana_s = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_s, Empty))
                BG_ExecutaQuery_ADO sql
                sql = "update sl_realiza set flg_facturado = 1 where n_req = " & BL_HandleNull(RsFact!n_req, Empty) & " and cod_perfil = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!Cod_Perfil, Empty)) & " and cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_c, Empty)) & " and cod_ana_s = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_s, Empty))
                BG_ExecutaQuery_ADO sql
            ElseIf (gAnalise_Ja_Facturada) Then
                sql = "update sl_marcacoes set flg_facturado = 1 where n_req = " & BL_HandleNull(RsFact!n_req, Empty) & " and cod_perfil = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!Cod_Perfil, Empty)) & " and cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_c, Empty)) & " and cod_ana_s = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_s, Empty))
                BG_ExecutaQuery_ADO sql
                sql = "update sl_realiza set flg_facturado = 1 where n_req = " & BL_HandleNull(RsFact!n_req, Empty) & " and cod_perfil = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!Cod_Perfil, Empty)) & " and cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_c, Empty)) & " and cod_ana_s = " & BL_TrataStringParaBD(BL_HandleNull(RsFact!cod_ana_s, Empty))
                BG_ExecutaQuery_ADO sql
            End If
        End If
        RsFact.MoveNext
        Wend
    If gHIS_Import = 1 And HIS.nome = "SONHO" Then
        If (RsFact.state = adStateOpen) Then: RsFact.Close
    End If
    BL_Fecha_conexao_HIS
    'gConnHIS.CommitTrans
    EnviaFacturacao = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao EnviaFacturacao: " & Err.Description, Me.Name, "EnviaFacturacao", True
    Exit Function
    Resume Next
End Function

Private Function ValidaSaidaTubo(codigo_etiqueta As String, requisicao As Long, Optional ByVal codigo_tubo As String) As Boolean
    
    Dim sql As String
    Dim rsTubo As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsTubo = New ADODB.recordset
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    
    If (codigo_etiqueta <> Empty) Then
        If gSeqReqTuboLocal <> mediSim Then
            sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_saida, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & " and sl_tubo.cod_etiq = " & BL_TrataStringParaBD(codigo_etiqueta) & " and dt_chega is not null "
        End If
    ElseIf (codigo_tubo <> Empty) Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_saida, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & "  and sl_tubo.cod_tubo = " & BL_TrataStringParaBD(codigo_tubo) & " and dt_chega is not null "
    End If
    rsTubo.Open sql, gConexao
    If (rsTubo.RecordCount = Empty) Then: Exit Function
    If (BL_HandleNull(rsTubo!dt_saida, Empty) = Empty) Then
        codigo_tubo = Empty
        g_codigo_produto = Empty
        ValidaSaidaTubo = True
    Else
        ValidaSaidaTubo = True
        codigo_tubo = BL_HandleNull(rsTubo!cod_tubo, "")
        g_codigo_produto = BL_HandleNull(rsTubo!cod_prod, "")
    End If
    If (rsTubo.state = adStateOpen) Then: rsTubo.Close
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao ValidaSaidaTubo: " & Err.Description, Me.Name, "ValidaSaidaTubo", True
    Exit Function
    Resume Next
End Function

Private Sub IniciaMecanismoTubos(Optional funcao_procurar As Boolean)
    
    Dim aux As String
    
    On Error GoTo TrataErro
    If (Not funcao_procurar) Then
        Estado = 2
        BL_InicioProcessamento Me, " A procurar registos..."
        BL_ToolbarEstadoN 2
        Me.MousePointer = vbArrowHourglass
        MDIFormInicio.MousePointer = vbArrowHourglass
    End If
    
    'BRUNODSANTOS CHUC-7934 - 27.04.2016
    PassaParam.NumReq = Right(EcNumReq.text, 7)
'
    aux = EcNumReq
    FuncaoLimpar
    EcNumReq = aux
    If (Not ValidaRequisicao) Then: GoTo TrataErro
    If (Not GeraLV) Then: GoTo TrataErro
    If (Not ActualizaEstadoTubos) Then: GoTo TrataErro
    If (Not RegistaMovimentoTubos) Then: GoTo TrataErro
    If (Not ActualizaEstadoTubos) Then: GoTo TrataErro
  
    If (Not funcao_procurar) Then
        BL_FimProcessamento Me
        BL_ToolbarEstadoN 1
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        Me.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    End If
Exit Sub
TrataErro:

    If (Not funcao_procurar) Then
            BL_FimProcessamento Me
            BL_ToolbarEstadoN 1
            BL_Toolbar_BotaoEstado "Inserir", "InActivo"
            Me.MousePointer = vbArrow
            MDIFormInicio.MousePointer = vbArrow
    End If
    Exit Sub
    Resume Next
End Sub

Private Function RegistaMovimentoTubos(Optional Item As Long, Optional regista_movimento_tubos_parcial As Boolean) As Boolean
    
    Dim lStructureIndex As Long
    Dim codigo_etiqueta As String
    
    On Error GoTo TrataErro
    RegistaMovimentoTubos = False
    
    If (Item = Empty) Then
        If (Not Len(EcNumReq) = 9) Or (Len(EcNumReq.text) = 9 And Mid(EcNumReq.text, 1, 2) = "00") Then: RegistaMovimentoTubos = True: Exit Function
    End If
    If Len(EcNumReq) = 9 Then
        codigo_etiqueta = left(EcNumReq, 2)
    End If
    lStructureIndex = RetornaIndiceTubo(Item, Empty, codigo_etiqueta)
    If lStructureIndex <= 0 And codigo_etiqueta <> "" Then
        BG_Mensagem mediMsgBox, "O tubo indicado n�o existe."
        Exit Function
    ElseIf lStructureIndex <= 0 Then
        BG_Mensagem mediMsgBox, "A requisi��o indicada n�o existe."
        Exit Function
    End If
    If (Item = Empty) Then
        If (Not Len(EcNumReq) = 9) Then: RegistaMovimentoTubos = True: Exit Function
    End If
    
    
    'If (Not ExisteMapeamento(lStructureIndex)) Then: Call ActualizaTuboEmTransito(lStructureIndex, True): RegistaMovimentoTubos = True: Exit Function
    If (Not regista_movimento_tubos_parcial) Then: If (IdentificaGarrafa(lStructureIndex)) Then Exit Function
    BG_BeginTransaction
    If (Not RemarcacaoAnalises(lStructureIndex)) Then
        GoTo TrataErro
    End If
    'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o gRequisicaoActiva para eTubos(lStructureIndex).requisicao
        If (Not ExecutaChegada(codigo_etiqueta, eTubos(lStructureIndex).requisicao, lStructureIndex, eTubos(lStructureIndex).codigo_tubo, eTubos(lStructureIndex).numero_garrafa)) Then
        BG_Mensagem mediMsgBox, "O tubo " & eTubos(lStructureIndex).descricao_tubo & " da requisi��o " & gRequisicaoActiva & " j� deu entrada anteriormente!"
        GoTo TrataErro
    End If
    'RefrescaLista
    Call InsereMovimentoTracking(lStructureIndex, "ENTRADA")
    BG_CommitTransaction
    RegistaMovimentoTubos = True
    
Exit Function
TrataErro:
    BG_RollbackTransaction
    RegistaMovimentoTubos = False
    BG_LogFile_Erros "Erro  ao RegistaMovimentoTubos: " & Err.Description, Me.Name, "RegistaMovimentoTubos", False
    Exit Function
    Resume Next
End Function

Private Function RegistaColheitasTubos(Optional Item As Long) As Boolean
    Dim lStructureIndex As Long
    Dim codigo_etiqueta As String
    On Error GoTo TrataErro
    RegistaColheitasTubos = False
    
    If (Item = Empty) Then
        If (Not Len(EcNumReq.text) = 9) Then: RegistaColheitasTubos = True: Exit Function
    End If
    If Len(EcNumReq) = 9 Then
        codigo_etiqueta = left(EcNumReq, 2)
    End If
    lStructureIndex = RetornaIndiceTubo(Item, Empty, codigo_etiqueta)
    If lStructureIndex <= 0 And codigo_etiqueta <> "" Then
        BG_Mensagem mediMsgBox, "O tubo indicado n�o existe."
        Exit Function
    ElseIf lStructureIndex <= 0 Then
        BG_Mensagem mediMsgBox, "A requisi��o indicada n�o existe."
        Exit Function
    End If
    If (Item = Empty) Then
        If (Not Len(EcNumReq) = 9) Then: RegistaColheitasTubos = True: Exit Function
    End If
    BG_BeginTransaction
    
    'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o gRequisicaoActiva para eTubos(lStructureIndex).requisicao
    If (ValidaColheitaTubo(codigo_etiqueta, eTubos(lStructureIndex).requisicao, eTubos(lStructureIndex).codigo_tubo)) Then
    'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o gRequisicaoActiva para eTubos(lStructureIndex).requisicao
        If (Not TB_ExecutaColheita(codigo_etiqueta, eTubos(lStructureIndex).requisicao, eTubos(lStructureIndex).codigo_tubo)) Then
            BG_Mensagem mediMsgBox, "O tubo " & eTubos(lStructureIndex).descricao_tubo & " da requisi��o " & eTubos(lStructureIndex).requisicao & " j� deu entrada anteriormente!"
            GoTo TrataErro
            'FG ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, "Tubo: " & eTubos(lStructureIndex).descricao_tubo & " Colhido"
        End If
    End If
    
    Call InsereMovimentoTracking(lStructureIndex, "COLHEITA")
    BG_CommitTransaction
    RegistaColheitasTubos = True
Exit Function
TrataErro:
    BG_RollbackTransaction
    RegistaColheitasTubos = False
    BG_LogFile_Erros "Erro  ao RegistaColheitasTubos: " & Err.Description, Me.Name, "RegistaColheitasTubos", False
    Exit Function
    Resume Next
End Function
 
Private Function ExecutaChegadaTubo(indice As Long, requisicao As Long, codigo_tubo As String, codigo_etiqueta As String, data_chegada As String, hora_chegada As String, user_chegada As String, numero_garrafa As String) As Boolean
    Dim sql As String
    On Error GoTo TrataErro
    
    If (Len(CStr(codigo_etiqueta)) = 1) Then: codigo_etiqueta = "0" & codigo_etiqueta
    If (codigo_tubo <> Empty) Then
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.chegado & ", dt_chega = " & BL_TrataDataParaBD(data_chegada) & ", hr_chega = " & BL_TrataStringParaBD(hora_chegada) & ", user_chega = " & BL_TrataStringParaBD(user_chegada) & ", local_chega = " & gCodLocal & ", id_garrafa = " & BL_TrataStringParaBD(numero_garrafa) & " where n_req = " & requisicao & " and cod_tubo = " & BL_TrataStringParaBD(codigo_tubo) & " AND dt_chega IS NULL AND dt_eliminacao IS NULL "
    ElseIf (Mid(codigo_etiqueta, 1, 1) = "0") Then
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.chegado & ", dt_chega = " & BL_TrataDataParaBD(data_chegada) & ", hr_chega = " & BL_TrataStringParaBD(hora_chegada) & ", user_chega = " & BL_TrataStringParaBD(user_chegada) & ", local_chega = " & gCodLocal & ", id_garrafa = " & BL_TrataStringParaBD(numero_garrafa) & " where n_req = " & requisicao & " and cod_tubo in (" & BL_TrataStringParaBD(codigo_etiqueta) & "," & BL_TrataStringParaBD(Mid(codigo_etiqueta, 2, 1)) & " AND dt_chega IS NULL AND dt_eliminacao IS NULL "
    Else
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.chegado & ", dt_chega = " & BL_TrataDataParaBD(data_chegada) & ", hr_chega = " & BL_TrataStringParaBD(hora_chegada) & ", user_chega = " & BL_TrataStringParaBD(user_chegada) & ", local_chega = " & gCodLocal & ", id_garrafa = " & BL_TrataStringParaBD(numero_garrafa) & " where n_req = " & requisicao & " and cod_tubo = " & BL_TrataStringParaBD(codigo_etiqueta) & " AND dt_chega IS NULL AND dt_eliminacao IS NULL "
    End If
    ' IMPRIME ETIQUETAS PARA TUBO PRIMARIO
    VerificaEtiquetasPrimarias CStr(requisicao), indice
    
    'IMPRIME ETIQUETAS DO TUBO SECUNDARIO.
    VerificaEtiquetasSecundarias codigo_tubo
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    ExecutaChegadaTubo = CBool(gSQLError = 0)
    
Exit Function
TrataErro:
    ExecutaChegadaTubo = False
    BG_LogFile_Erros "Erro  ao ExecutaChegadaTubo: " & Err.Description, Me.Name, "ExecutaChegadaTubo", True
    Exit Function
    Resume Next
End Function

Private Function DesactivaChegadaTubo(posicao As Long, requisicao As Long, seq_req_tubo As Long, dt_colheita As String) As Boolean
    Dim sql As String
    Dim iReg As Integer
    Dim Estado As Integer
    On Error GoTo TrataErro
    'CHMA-1594
    Dim rs As New ADODB.recordset
    '
    DesactivaChegadaTubo = False
    BG_BeginTransaction
    If dt_colheita = "" Then
        Estado = gEstadosTubo.Pendente
    Else
        Estado = gEstadosTubo.colhido
    End If
    
    'CHMA-1594
    'Verifica se h� outros tubos com chegada
    sql = "SELECT * FROM sl_req_tubo WHERE n_req = " & requisicao & " AND seq_req_tubo <> " & seq_req_tubo & " AND dt_chega IS NOT NULL"
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    
    If rs.RecordCount = 0 Then
        sql = "UPDATE sl_requis SET dt_chega = null WHERE n_req = " & requisicao
        iReg = BG_ExecutaQuery_ADO(sql)
        If iReg <= 0 Then
            GoTo TrataErro
        End If
    End If
    
    sql = "update sl_req_tubo set estado_tubo = " & Estado & ", dt_chega = null, hr_chega = null, user_chega = null, local_chega = null "
    sql = sql & " WHERE seq_req_tubo = " & seq_req_tubo
    sql = sql & " AND dt_chega IS NOT NULL AND dt_eliminacao IS NULL "
    iReg = BG_ExecutaQuery_ADO(sql)
    If iReg <= 0 Then
        GoTo TrataErro
    End If
    If InsereMovimentoTracking(posicao, "ANUL CHEG.") = False Then
        GoTo TrataErro
    End If
    'FG ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, "Tubo: " & eTubos(posicao).descricao_tubo & " Desactivado"
    DoEvents
    DesactivaChegadaTubo = True
    BG_CommitTransaction
    Exit Function
TrataErro:
    BG_RollbackTransaction
    DesactivaChegadaTubo = False
    BG_LogFile_Erros "Erro  ao DesactivaChegadaTubo: " & Err.Description & sql, Me.Name, "DesactivaChegadaTubo", True
    Exit Function
    Resume Next
    
End Function

Private Function ExecutaChegadaProduto(requisicao As Long, codigo_produto As String, data_chegada As String) As Boolean
    
    Dim sql As String
    
    On Error GoTo TrataErro
    If (codigo_produto <> Empty) Then
        sql = "update sl_req_prod set dt_chega = " & BL_TrataDataParaBD(data_chegada) & " where n_req = " & requisicao & " and cod_prod = " & BL_TrataStringParaBD(codigo_produto) & " and dt_chega is null "
    Else
        sql = "update sl_req_prod set dt_chega = " & BL_TrataDataParaBD(data_chegada) & " where n_req = " & requisicao & " and dt_chega is null"
    End If
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    ExecutaChegadaProduto = CBool(gSQLError = 0)
    
Exit Function
TrataErro:
    ExecutaChegadaProduto = False
    BG_LogFile_Erros "Erro  ao ExecutaChegadaProduto: " & Err.Description, Me.Name, "ExecutaChegadaProduto", True
    Exit Function
    Resume Next
End Function

Private Function ExecutaChegadaRequisicao(requisicao As Long, data_chegada As String, hora_chegada As String, user_chegada As String) As Boolean
    
    Dim sql As String
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    Dim i As Integer
    
    On Error GoTo TrataErro
    sSql = "SELECT COUNT(*) conta FROM sl_req_tubo WHERE n_req = " & requisicao & " AND dt_chega IS NOT NULL"
    Set rsTubo = New ADODB.recordset
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount = 1 Then
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If BL_HandleNull(rsTubo!conta, 0) > 0 Or (gAtiva_Reutilizacao_Colheitas = mediSim And UBound(reqColh) > 0) Then
        '
            sql = "update sl_requis set dt_chega = " & BL_TrataDataParaBD(data_chegada) & ", hr_chega = " & BL_TrataStringParaBD(hora_chegada) & " where n_req = " & requisicao
            'CHVNG-9820
            sql = sql & " AND dt_chega IS NULL"
            '
            BG_ExecutaQuery_ADO sql
            BG_Trata_BDErro
            If gEnviaRelatorioEresultsChegada = mediSim Then
                BL_InsereEResults CStr(requisicao), True
            End If
        End If
        rsTubo.Close
        Set rsTubo = Nothing
    End If
    ExecutaChegadaRequisicao = CBool(gSQLError = 0)
    
Exit Function
TrataErro:
    ExecutaChegadaRequisicao = False
    BG_LogFile_Erros "Erro  ao ExecutaChegadaRequisicao: " & Err.Description, Me.Name, "ExecutaChegadaRequisicao", True
    Exit Function
    Resume Next
End Function

Private Function ExecutaChegadaMarcacao(requisicao As Long, data_chegada As String, hora_chegada As String, codigo_tubo As String, codigo_etiqueta As String) As Boolean
    
    Dim sql As String
    Dim rsAna   As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    sql = "select m.n_req, m.cod_perfil, m.cod_ana_c, m.cod_ana_s " & " from sl_ana_s s right outer join sl_marcacoes m on m.cod_ana_s = s.cod_ana_s "
    sql = sql & " left outer join sl_tubo t on s.cod_tubo = t.cod_tubo " & " where m.n_req = " & requisicao & " and " & " (t.cod_tubo = "
    sql = sql & IIf(codigo_tubo <> "", BL_TrataStringParaBD(codigo_tubo), BL_TrataStringParaBD(codigo_etiqueta)) & " or t.cod_etiq is null) and dt_chega is null"
    rsAna.Open sql, gConexao
    While (Not rsAna.EOF)
        sql = "update sl_marcacoes set dt_chega = " & BL_TrataDataParaBD(data_chegada) & "," & " hr_chega = " & BL_TrataStringParaBD(hora_chegada) & " where n_req = " & rsAna!n_req & " and cod_perfil = " & BL_TrataStringParaBD(rsAna!Cod_Perfil) & " and cod_ana_c = " & BL_TrataStringParaBD(rsAna!cod_ana_c) & " and cod_ana_s = " & BL_TrataStringParaBD(rsAna!cod_ana_s)
        BG_ExecutaQuery_ADO sql, gConexao
        BG_Trata_BDErro
        If (gSQLError <> Empty) Then: Exit Function
        rsAna.MoveNext
    Wend
    ExecutaChegadaMarcacao = CBool(gSQLError = 0)
    
Exit Function
TrataErro:
    ExecutaChegadaMarcacao = False
    BG_LogFile_Erros "Erro  ao ExecutaChegadaMarcacao: " & Err.Description, Me.Name, "ExecutaChegadaMarcacao", True
    Exit Function
    Resume Next
End Function


Private Sub DefTipoCampos()
    
    If gTipoInstituicao = gTipoInstituicaoHospitalar Then
        BtResumo.Visible = True
    Else
        BtResumo.Visible = False
    End If
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", cbUrgencia
        'edgar.parada IPO-P-13743
    'BG_PreencheComboBD_ADO "sl_gr_ana", "cod_gr_ana", "descr_gr_ana", cbGrAna
    '
    If gUsaSaidaTubos = mediSim Then
        FrameSaida.Visible = True
    Else
        FrameSaida.Visible = False
    End If
    LimpaCampos

    With FGReq
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 1000
        .Col = 0
        .ColAlignment(0) = flexAlignCenterCenter
        .TextMatrix(0, 0) = "Requis."
        
        .ColWidth(1) = 1000
        .Col = 1
        .ColAlignment(1) = flexAlignCenterCenter
        .TextMatrix(0, 1) = "Data"
        
    End With
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    BtNotas.Enabled = True
    BtNotasVRM.Enabled = False

    EcGarrafa.Font.Bold = True
    EcMotivoEliminacao.Font.Bold = True
    EcUtilizadorEliminacao.Font.Bold = True
    EcDataEliminacao.Font.Bold = True
    EcHoraEliminacao.Font.Bold = True
    EcMotivoNovaAmostra.Font.Bold = True
    If gColheitaChegada = gEnumColheitaChegada.ColheitaTubo Then
        BtFaltaTubos.Visible = False
        BtEmiteFolhaNovaAmostra.Visible = False
    Else
        BtEmiteFolhaNovaAmostra.Visible = True
        BtFaltaTubos.Visible = True
    End If
    LimpaReqAtual
End Sub



Private Function ActualizaEstadoTubos(Optional Item As Long) As Boolean

    Dim vItem As Variant
    
    On Error GoTo TrataErro
    Select Case (Item)
        Case Empty:
            For Each vItem In LvTubos.ListItems
                If (Not ActualizaEstadoTubo(vItem.Index)) Then: Exit Function
            Next
        Case Else:
            If (Not ActualizaEstadoTubo(Item)) Then: Exit Function
    End Select
    ActualizaEstadoTubos = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao ActualizaEstadoTubos: " & Err.Description, Me.Name, "ActualizaEstadoTubos", True
    Exit Function
    Resume Next
End Function

Private Function ActualizaEstadoTubo(Item As Long) As Boolean
 
    Dim cor_tubo As String
    Dim coluna_lista As Integer
    Dim bParent As Boolean
    Dim lStructureIndex As Long
    Dim estado_tubo As String
    
    On Error GoTo TrataErro
    lStructureIndex = RetornaIndiceTubo(Item, bParent)
    If (Not bParent) Then: ActualizaEstadoTubo = True: Exit Function
    Select Case (eTubos(lStructureIndex).estado_tubo)
        Case gEstadosTubo.cancelado: cor_tubo = LvTubosCores.t_amarelo
        Case gEstadosTubo.chegado: cor_tubo = LvTubosCores.t_verde
        Case gEstadosTubo.colhido: cor_tubo = LvTubosCores.t_laranja
        Case gEstadosTubo.Pendente: cor_tubo = LvTubosCores.t_vermelho
    End Select
    estado_tubo = TB_DevolveEstadoTubo(eTubos(lStructureIndex).estado_tubo)
    LvTubos.ListItems(Item).ForeColor = cor_tubo
    For coluna_lista = LvTubosColunas.t_codigo_tubo To LvTubosColunas.t_estado_tubo
        If (coluna_lista = LvTubosColunas.t_estado_tubo) Then: LvTubos.ListItems(Item).ListSubItems(coluna_lista).text = estado_tubo
        LvTubos.ListItems(Item).ListSubItems(coluna_lista).ForeColor = cor_tubo
    Next
    ActualizaEstadoTubo = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao ActualizaEstadoTubo: " & Err.Description, Me.Name, "ActualizaEstadoTubo", True
    Exit Function
    Resume Next
End Function

Private Function RefrescaLista() As Boolean
    On Error GoTo TrataErro

    Dim i As Integer
    LimpaCampos True
    
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Reutilizacao_Colheitas = mediSim And UBound(reqColh) > 0 Then
        For i = 1 To UBound(reqColh)
            If (Not ProcuraRegistos(reqColh(i).n_req)) Then: Exit Function
        Next i
    Else
    '
        If (Not ProcuraRegistos(gRequisicaoActiva)) Then: Exit Function
    End If
    PreencheNotas
    GeraLV
    If (Not ActualizaEstadoTubos) Then: Exit Function
    RefrescaLista = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao RefrescaLista: " & Err.Description, Me.Name, "RefrescaLista", True
    Exit Function
    Resume Next
End Function

Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", Resultados(i))
                EcListaLocais.ItemData(0) = Resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", Resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub
Private Sub BtPesquisaTubos_click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_tubo"
    CamposEcran(1) = "cod_tubo"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_tubo"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tubo"
    CWhere = ""
    CampoPesquisa = "descr_tubo"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_tubo ", _
                                                                           " Tubos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaTubos.ListCount = 0 Then
                EcListaTubos.AddItem BL_SelCodigo("sl_tubo", "descr_tubo", "seq_tubo", Resultados(i))
                EcListaTubos.ItemData(0) = Resultados(i)
            Else
                EcListaTubos.AddItem BL_SelCodigo("sl_tubo", "descr_tubo", "seq_tubo", Resultados(i))
                EcListaTubos.ItemData(EcListaTubos.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub



Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaLocais.ListCount > 0 Then     'Delete
        If EcListaLocais.ListIndex > mediComboValorNull Then
            EcListaLocais.RemoveItem (EcListaLocais.ListIndex)
        End If
    End If
End Sub


Private Sub EclistaTubos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaTubos.ListCount > 0 Then     'Delete
        If EcListaTubos.ListIndex > mediComboValorNull Then
            EcListaTubos.RemoveItem (EcListaTubos.ListIndex)
        End If
    End If
End Sub

Private Sub ProcuraReqPend()
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    Dim i As Integer
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    
    On Error GoTo TrataErro
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    'edgar.parada IPO-P-13743 19.02.2019 add x2.gr_ana
    sSql = "SELECT distinct x1.t_utente, x1.utente, x1.nome_ute, x1.dt_nasc_ute, x2.n_Req, x2.dt_previ, x2.dt_chega,x2.t_urg, x2.cod_proven, x3.descr_proven, "
    sSql = sSql & " x4.cod_tubo, x4.dt_chega dt_chega_tubo, x4.hr_chega hr_chega_tubo, x4.dt_eliminacao,x4.hr_eliminacao, x5.descR_tubo, x4.estado_tubo, x6.cor cor_urg, x2.gr_ana  "
    sSql = sSql & " FROM " & tabela_aux & " x1, sl_Requis x2, sl_proven x3, sl_req_tubo x4, sl_tubo x5, sl_tbf_t_urg x6 "
    'If cbGrAna.ListIndex > mediComboValorNull Then
    If EcCodGrupo <> "" Then
       sSql = sSql & ",sl_marcacoes x7,slv_analises x8"
    End If
    sSql = sSql & " WHERE x1.seq_utente = x2.seq_utente AND x2.cod_proven = x3.cod_proven(+) AND x2.n_Req = x4.n_req AND x4.cod_tubo = x5.cod_tubo "
    sSql = sSql & " AND x2.dt_previ BETWEEN " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    sSql = sSql & " AND estado_req <> 'C' AND x2.n_req IN (SELECT n_Req from sl_req_tubo WHERE dt_chega IS NULL )"
    sSql = sSql & " AND x2.t_urg = x6.cod_t_urg"
    
    ' URGENCIA
    If cbUrgencia.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND x2.t_urg = " & cbUrgencia.ItemData(cbUrgencia.ListIndex)
    End If
    
    ' SALA
    If EcCodSala <> "" Then
        sSql = sSql & " AND x2.cod_sala = " & EcCodSala
    End If
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        sSql = sSql & " AND X2.cod_local IN ("
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------------------------------------------------
    ' TUBO PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaTubos.ListCount > 0 Then
        sSql = sSql & " AND X5.SEQ_TUBO IN ("
        For i = 0 To EcListaTubos.ListCount - 1
            sSql = sSql & EcListaTubos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
        ' ------------------------------------------------------------------------------
    ' GRUPO DE ANALISES EDGAR PARADA IPO-P-13743 19.02.219
    ' ------------------------------------------------------------------------------
    'If cbGrAna.ListIndex > mediComboValorNull Then
    If EcCodGrupo <> "" Then
    
    'sSql = sSql & " AND (x2.gr_ana LIKE '%;" & cbGrAna.ItemData(cbGrAna.ListIndex) & "' OR x2.gr_ana LIKE '%;" & cbGrAna.ItemData(cbGrAna.ListIndex) & ";%'"
   ' sSql = sSql & " OR x2.gr_ana LIKE '" & cbGrAna.ItemData(cbGrAna.ListIndex) & ";%' OR x2.gr_ana LIKE '" & cbGrAna.ItemData(cbGrAna.ListIndex) & "')"
   ' ssql = ssql & " AND x2.n_req = x7.n_req AND x7.cod_agrup = x8.cod_ana AND x7.seq_req_tubo = x4.seq_req_tubo AND x8.gr_ana = " & cbGrAna.ItemData(cbGrAna.ListIndex)
     sSql = sSql & " AND x2.n_req = x7.n_req AND x7.cod_agrup = x8.cod_ana AND x7.seq_req_tubo = x4.seq_req_tubo AND x8.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo)
    End If
    
    ' ------------------------------------------------------------------------------
    ' Apenas Tubos com colheita IPO-P-13743 19.02.219
    ' ------------------------------------------------------------------------------
    If ChkColheita.value = vbChecked Then
      sSql = sSql & " AND x4.dt_colheita IS NOT NULL"
    End If
    '
    sSql = sSql & " ORDER BY x2.n_Req, x5.descr_tubo "
    
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    If gModoDebug Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount > 0 Then
        totalReq = 0
        ReDim EstrutReq(totalReq)
        While (Not rsTubo.EOF)
            PreencheEstrutReq BL_HandleNull(rsTubo!t_utente, ""), BL_HandleNull(rsTubo!Utente, ""), BL_HandleNull(rsTubo!nome_ute, ""), _
                              BL_HandleNull(rsTubo!dt_nasc_ute, ""), BL_HandleNull(rsTubo!n_req, ""), BL_HandleNull(rsTubo!dt_previ, ""), _
                              BL_HandleNull(rsTubo!dt_chega, ""), BL_HandleNull(rsTubo!t_urg, ""), BL_HandleNull(rsTubo!cod_proven, ""), _
                              BL_HandleNull(rsTubo!descr_proven, ""), BL_HandleNull(rsTubo!cod_tubo, ""), BL_HandleNull(rsTubo!dt_chega_tubo, ""), _
                              BL_HandleNull(rsTubo!hr_chega_tubo, ""), BL_HandleNull(rsTubo!dt_eliminacao, ""), BL_HandleNull(rsTubo!hr_eliminacao, ""), _
                              BL_HandleNull(rsTubo!descR_tubo, ""), BL_HandleNull(rsTubo!estado_tubo, mediComboValorNull)
            rsTubo.MoveNext
        Wend
    Else
        ReDim EstrutReq(0)
        totalReq = 0
        FGReq.Cols = 2
        FGReq.rows = 2
        FGReq.TextMatrix(1, 0) = ""
        FGReq.TextMatrix(1, 1) = ""
        BG_Mensagem mediMsgBox, "N�o foi encontrado nenhum resultado para estas condi��es de pesquisa!", vbExclamation, App.ProductName
    End If
    rsTubo.Close
    Set rsTubo = Nothing
    PreencheFGReq
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Procurar Tubos Pendentes: " & Err.Description, Me.Name, "ProcuraReqPend", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheEstrutReq(t_utente As String, Utente As String, nome_ute As String, dt_nasc_ute As String, _
                              n_req As String, dt_previ As String, dt_chega As String, t_urg As String, cod_proven As String, _
                              descr_proven As String, cod_tubo As String, dt_chega_tubo As String, hr_chega_tubo As String, _
                              dt_eliminacao As String, hr_eliminacao As String, descR_tubo As String, estado_tubo As Integer)
    Dim flg_encontrou As Boolean
    Dim i As Long
    Dim indice As Integer
    flg_encontrou = False
    On Error GoTo TrataErro
    
    For i = 1 To totalReq
        If EstrutReq(i).n_req = n_req Then
            indice = i
            flg_encontrou = True
            Exit For
        End If
    Next
    If flg_encontrou = False Then
        totalReq = totalReq + 1
        ReDim Preserve EstrutReq(totalReq)
        indice = totalReq
        EstrutReq(indice).n_req = n_req
        EstrutReq(indice).t_utente = t_utente
        EstrutReq(indice).Utente = Utente
        EstrutReq(indice).nome_ute = nome_ute
        EstrutReq(indice).dt_nasc_ute = dt_nasc_ute
        EstrutReq(indice).dt_previ = dt_previ
        EstrutReq(indice).dt_chega = dt_chega
        EstrutReq(indice).t_urg = t_urg
        EstrutReq(indice).cod_proven = descr_proven
        EstrutReq(indice).totalTubosPend = 0
        ReDim EstrutReq(indice).TubosPend(0)
    End If
     EstrutReq(indice).totalTubosPend = EstrutReq(indice).totalTubosPend + 1
     ReDim Preserve EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend)
     EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend).cod_tubo = cod_tubo
     EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend).descR_tubo = descR_tubo
     EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend).dt_canc = dt_eliminacao
     EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend).hr_canc = hr_eliminacao
     EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend).dt_chega = dt_chega_tubo
     EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend).hr_chega = hr_chega_tubo
     EstrutReq(indice).TubosPend(EstrutReq(indice).totalTubosPend).estado_tubo = estado_tubo
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher estrutura Tubos Pendentes: " & Err.Description, Me.Name, "PreencheEstrutReq", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheFGReq()
    Dim i As Long
    Dim j As Long
    Dim colunas As Integer
    On Error GoTo TrataErro
    colunas = 0
    FGReq.Cols = 2
    FGReq.rows = 2
    FGReq.TextMatrix(1, 0) = ""
    FGReq.TextMatrix(1, 1) = ""
    
    For i = 1 To totalReq
        If EstrutReq(i).totalTubosPend > colunas Then
            colunas = EstrutReq(i).totalTubosPend
        End If
    Next
    FGReq.Cols = colunas + 2
    For i = 2 To FGReq.Cols - 1
        FGReq.ColWidth(i) = 1100
        FGReq.Col = i
        FGReq.ColAlignment(i) = flexAlignLeftCenter
        FGReq.TextMatrix(0, i) = "Tubo"
    Next i
    
    For i = 1 To totalReq
        FGReq.row = i
        FGReq.TextMatrix(i, 0) = EstrutReq(i).n_req
        FGReq.TextMatrix(i, 1) = EstrutReq(i).dt_previ
        For j = 1 To EstrutReq(i).totalTubosPend
            FGReq.Col = j + 1
            FGReq.TextMatrix(i, j + 1) = EstrutReq(i).TubosPend(j).descR_tubo
            If EstrutReq(i).TubosPend(j).estado_tubo = gEstadosTubo.cancelado Then
                FGReq.CellBackColor = t_amarelo
                EstrutReq(i).TubosPend(j).cor = t_amarelo
            ElseIf EstrutReq(i).TubosPend(j).estado_tubo = gEstadosTubo.chegado Then
                FGReq.CellBackColor = t_verde_claro
                EstrutReq(i).TubosPend(j).cor = t_verde_claro
            ElseIf EstrutReq(i).TubosPend(j).estado_tubo = gEstadosTubo.colhido Then
                FGReq.CellBackColor = t_laranja
                EstrutReq(i).TubosPend(j).cor = t_laranja
            Else
                FGReq.CellBackColor = t_vermelho_claro
                EstrutReq(i).TubosPend(j).cor = t_vermelho_claro
            End If
        Next j
        FGReq.AddItem ""
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher FGReq: " & Err.Description, Me.Name, "PreencheFgReq", True
    Exit Sub
    Resume Next
End Sub

Public Sub DesactivaColheitaTubo()
    Dim posicao As Long
    Dim sql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    posicao = RetornaIndiceTubo(LvTubos.SelectedItem.Index)
    gMsgMsg = "Tem a certeza que quer cancelar a colheita do tubo " & eTubos(posicao).descricao_tubo & "?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BG_BeginTransaction

        
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.Pendente & ", dt_colheita = null, hr_colheita = null, user_colheita = null "
        sql = sql & " WHERE seq_req_tubo = " & eTubos(posicao).seq_req_tubo
        sql = sql & " AND dt_chega IS NULL AND dt_eliminacao IS NULL "
        iReg = BG_ExecutaQuery_ADO(sql)
        If iReg <= 0 Then
            GoTo TrataErro
        End If
        If TB_AnulaHoraPrevistaColheita(eTubos(posicao).requisicao) = True Then
            If InsereMovimentoTracking(posicao, "ANUL COLH.") = False Then
                GoTo TrataErro
            End If
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
    RefrescaLista
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao Desactivar Colheita Tubos: " & Err.Description, Me.Name, "DesactivaColheitaTubo", True
    Exit Sub
    Resume Next
End Sub

Public Sub Desactiva_ChegadaTubo()
    Dim posicao As Long
    On Error GoTo TrataErro
    
    posicao = RetornaIndiceTubo(LvTubos.SelectedItem.Index)
    gMsgMsg = "Tem a certeza que quer cancelar a chegada do tubo " & eTubos(posicao).descricao_tubo & "?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        If DesactivaChegadaTubo(posicao, gRequisicaoActiva, eTubos(posicao).seq_req_tubo, eTubos(posicao).data_colheita) = False Then
            GoTo TrataErro
        Else
            RefrescaLista
        End If
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Desactivar Chegada Tubos: " & Err.Description, Me.Name, "Desactiva_ChegadaTubo", True
    Exit Sub
    Resume Next
End Sub

Public Sub PedidoNovaAmostra()
    
    On Error GoTo TrataErro
    Dim Index As Long
    
    On Error GoTo TrataErro
    Index = RetornaIndiceTubo(LvTubos.SelectedItem.Index)
    gMsgMsg = "Tem a certeza que quer pedir nova amostra " & eTubos(Index).descricao_tubo & "?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbYes) Then: Call ControlaFrames(FrMotivo, True)
    TipoMotivo = Motivos.PedNovaAmostra
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro  ao Pedir Nova Amostra: " & Err.Description, Me.Name, "PedidoNovaAmostra", True
    Exit Sub
    Resume Next
    Exit Sub
    
End Sub

Private Function IdentificaGarrafa(Index As Long) As Boolean
    
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim Item As Variant
    On Error GoTo TrataErro
    
    For Each Item In DevolveAnalisesTuboEstrutura(Index)
        If Mid(Item, 1, 1) = "P" Then
            sSql = "select flg_garrafa from sl_perfis where cod_perfis = " & BL_TrataStringParaBD(CStr(Item))
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If (gModoDebug = mediSim) Then: BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If (rsAna.RecordCount = 1) Then
                IdentificaGarrafa = CBool(BL_HandleNull(rsAna!flg_garrafa, Empty))
                If (IdentificaGarrafa) Then: Exit For
            End If
            If (rsAna.state = adStateOpen) Then: rsAna.Close
        End If
    Next
    If (IdentificaGarrafa) Then: Call ControlaFrames(FrGarrafa, True)
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Sub ControlaFrames(Frame As Frame, Enabled As Boolean)

    BL_Toolbar_BotaoEstado "Limpar", IIf(Enabled, "InActivo", "Activo")
    FrCabecalho.Enabled = Not Enabled
    FrTubos.Enabled = Not Enabled
    BtEtiq.Enabled = Not Enabled
    BtFaltaTubos.Enabled = Not Enabled
    Frame.Visible = Enabled
    
End Sub

Private Function DevolveAnalisesTuboEstrutura(Index As Long) As Collection

    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim Item As Long
    
    On Error GoTo TrataErro
    Set DevolveAnalisesTuboEstrutura = New Collection
    Call LimitesTubos(CStr(eTubos(Index).requisicao), eTubos(Index).codigo_tubo, eTubos(Index).agrupador_estrutura, lLowerBound, lUpperBound)
    For Item = lLowerBound To lUpperBound
        DevolveAnalisesTuboEstrutura.Add eTubos(Item).codigo_analise
    Next
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Sub InsereGarrafaHemocultura(Index As Long)
 
   Dim lStructureIndex As Long
    
    On Error GoTo TrataErro
    lStructureIndex = RetornaIndiceTubo(Index)
    eTubos(lStructureIndex).numero_garrafa = EcGarrafa.text
    Exit Sub
    
TrataErro:
    Exit Sub
    
End Sub

Private Function ExecutaPedidoNovaAmostra(requisicao As Long, Index As Long, codigo_motivo As String) As Boolean
    Dim codAna() As String
    Dim sql As String
    Dim lStructureIndex As Long
    Dim seqReqTubo As Long
    On Error GoTo TrataErro
    lStructureIndex = RetornaIndiceTubo(Index)
    eTubos(lStructureIndex).motivo_eliminacao = codigo_motivo
    If (Not CancelaChegadaTubo(lStructureIndex, codigo_motivo)) Then Exit Function
    seqReqTubo = mediComboValorNull
    seqReqTubo = TB_InsereTuboBD(mediComboValorNull, CStr(eTubos(lStructureIndex).requisicao), eTubos(lStructureIndex).codigo_tubo, _
                                eTubos(lStructureIndex).data_prevista, "", "", _
                                "", "", "", codAna, gEstadosTubo.Pendente, mediComboValorNull, "")
    If seqReqTubo > mediComboValorNull Then
        sql = "UPDATE sl_marcacoes SET seq_req_tubo = " & seqReqTubo & " WHERE seq_req_tubo = " & eTubos(lStructureIndex).seq_req_tubo
        BG_ExecutaQuery_ADO sql
        sql = "UPDATE sl_realiza SET seq_req_tubo = " & seqReqTubo & " WHERE seq_req_tubo = " & eTubos(lStructureIndex).seq_req_tubo
        BG_ExecutaQuery_ADO sql
        
        ExecutaPedidoNovaAmostra = True
    Else
        ExecutaPedidoNovaAmostra = False
    End If
    If (ExecutaPedidoNovaAmostra) Then
        'FG ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, "Tubo: " & eTubos(lStructureIndex).descricao_tubo & " Nova Amostra"
        'EmitirPedidoNovaAmostra lStructureIndex
        RefrescaLista
    End If
    Exit Function
    
TrataErro:
    ExecutaPedidoNovaAmostra = False
    BG_LogFile_Erros "Erro  ao ExecutaPedidoNovaAmostra: " & Err.Description, Me.Name, "ExecutaPedidoNovaAmostra", True
    Exit Function
    Resume Next
End Function

Private Function LocalDiferenteTubo(Index As Long, codigo_local As String) As Boolean

    On Error GoTo TrataErro
    LocalDiferenteTubo = CBool(CStr(gCodLocal) <> codigo_local)
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Erro  ao LocalDiferenteTubo: " & Err.Description, Me.Name, "LocalDiferenteTubo", True
    Exit Function
    Resume Next
    
End Function

'Private Function ExecutaRemarcacaoAnalise(Index As Long) As Boolean
'    Dim cod_agrup As String
'    Dim cod_agrup_transf As String
'    Dim tokens() As String
'    Dim i As Integer
'    On Error GoTo TrataErro
'
'    cod_agrup = eTubos(Index).codigo_analise
'    'MAPEIA COD_AGRUP
'    cod_agrup_transf = MapeiaCodigoAnalise(cod_agrup, gCodLocal)
'    If cod_agrup_transf = "-1" Then
'        ExecutaRemarcacaoAnalise = True
'        Exit Function
'    End If
'    If cod_agrup_transf <> "" Then
'        BL_Tokenize cod_agrup_transf, ";", tokens
'        For i = 0 To UBound(tokens)
'            If tokens(i) <> "" Then
'                If (AnaliseSemResultado(eTubos(Index).requisicao, eTubos(Index).codigo_analise, eTubos(Index).codigo_local)) Then: BG_Mensagem mediMsgBox, "An�lise " & eTubos(Index).codigo_analise & " com resultados!", vbExclamation, " Aten��o": Exit Function
'                ExecutaRemarcacaoAnalise = RemarcaAnalise3(CStr(eTubos(Index).requisicao), cod_agrup, tokens(i))
'                If ExecutaRemarcacaoAnalise = False Then
'                    GoTo TrataErro
'                End If
'            End If
'        Next
'    Else
'        If (AnaliseSemResultado(eTubos(Index).requisicao, eTubos(Index).codigo_analise, eTubos(Index).codigo_local)) Then: BG_Mensagem mediMsgBox, "An�lise " & eTubos(Index).codigo_analise & " com resultados!", vbExclamation, " Aten��o": Exit Function
'        ExecutaRemarcacaoAnalise = RemarcaAnalise3(CStr(eTubos(Index).requisicao), cod_agrup, "")
'        If ExecutaRemarcacaoAnalise = False Then
'            GoTo TrataErro
'        End If
'    End If
'
'    'ExecutaRemarcacaoAnalise = True
'    Exit Function
'
'TrataErro:
'    BG_LogFile_Erros "Erro  ao ExecutaRemarcacaoAnalise: " & Err.Description, Me.Name, "ExecutaRemarcacaoAnalise", True
'    Exit Function
'    Resume Next
'
'End Function

Private Function MapeiaCodigoAnalise(ByVal codigo_analise As String, codigo_local As Integer, Optional ByVal codigo_agrupador As String)

    Dim sql As String
    Dim Tabela As String
    Dim campo As String
    Dim tabela_membro As String
    Dim campo_membro As String
    Dim campo_pai As String
    Dim rsAna As ADODB.recordset
     
    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    If (codigo_analise = Empty) Then: Exit Function

    sql = " Select slv_analises_apenas.cod_ana from slv_analises_apenas, sl_ana_locais_exec where slv_analises_apenas.cod_ana  in (select cod_map "
    sql = sql & " from sl_mapeamento_analises where sl_mapeamento_analises.cod_ana = " & BL_TrataStringParaBD(codigo_analise) & " AND sl_mapeamento_analises.cod_local = " & gCodLocal & ") "
    sql = sql & "and sl_ana_locais_exec.cod_local = " & codigo_local & " and sl_ana_locais_exec.cod_ana = slv_analises_apenas.cod_ana "
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    rsAna.Open sql, gConexao
    If (rsAna.RecordCount >= 1) Then
        MapeiaCodigoAnalise = ""
        rsAna.MoveFirst
        While Not rsAna.EOF
            MapeiaCodigoAnalise = MapeiaCodigoAnalise & BL_HandleNull(rsAna(0), Empty) & ";"
            rsAna.MoveNext
        Wend
        If MapeiaCodigoAnalise <> "" Then
            MapeiaCodigoAnalise = Mid(MapeiaCodigoAnalise, 1, Len(MapeiaCodigoAnalise) - 1)
        End If
    Else
        rsAna.Close
        sql = "SELECT * from sl_mapeamento_analises WHERE cod_ana = " & BL_TrataStringParaBD(codigo_analise) & " AND cod_map IS NULL"
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If (rsAna.RecordCount >= 1) Then
            MapeiaCodigoAnalise = ""
        Else
            MapeiaCodigoAnalise = "-1"
        End If
    End If
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Exit Function
    
TrataErro:
    MapeiaCodigoAnalise = "-1"
    BG_LogFile_Erros "Erro  ao MapeiaCodigoAnalise: " & Err.Description, Me.Name, "MapeiaCodigoAnalise", True
    Exit Function
    Resume Next
    
End Function

Private Function AnaliseSemResultado(requisicao As Long, codigo_analise As String, codigo_local As Integer) As Boolean

    Dim sql As String
    Dim RsRes As ADODB.recordset
     
    On Error GoTo TrataErro
    Set RsRes = New ADODB.recordset
    If (codigo_analise = Empty) Then: Exit Function
    sql = "select * from sl_realiza where n_req = " & requisicao & " and cod_agrup = " & _
          BL_TrataStringParaBD(codigo_analise)
    RsRes.CursorLocation = adUseServer
    RsRes.CursorType = adOpenStatic
    RsRes.Open sql, gConexao
    AnaliseSemResultado = CBool(RsRes.RecordCount <> Empty)
    If (RsRes.state = adStateOpen) Then: RsRes.Close
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Erro  ao AnaliseSemResultado: " & Err.Description, Me.Name, "AnaliseSemResultado", True
    Exit Function
    Resume Next
    
End Function
'Private Function RemarcaAnalise3(n_req As String, cod_agrup As String, cod_agrup_transf As String) As Boolean
'    On Error GoTo TrataErro
'    Dim sSql As String
'    Dim iRegistos As Integer
'
'    If cod_agrup = cod_agrup_transf Then
'        RemarcaAnalise3 = True
'        Exit Function
'    End If
'    sSql = "DELETE FROM sl_marcacoes WHERE n_Req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
'    iRegistos = BG_ExecutaQuery_ADO(sSql)
'    If iRegistos < 0 Then
'        GoTo TrataErro
'    End If
'    If cod_agrup_transf <> "" Then
'        sSql = "DELETE FROM sl_marcacoes WHERE n_Req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup_transf)
'        iRegistos = BG_ExecutaQuery_ADO(sSql)
'        If iRegistos < 0 Then
'            GoTo TrataErro
'        End If
'    End If
'
'    If Mid(cod_agrup_transf, 1, 1) = "P" Then
'        RemarcaAnalise3 = InserePerfil(n_req, cod_agrup_transf, cod_agrup_transf)
'    ElseIf Mid(cod_agrup_transf, 1, 1) = "C" Then
'        RemarcaAnalise3 = InsereComplexa(n_req, cod_agrup_transf, "0", cod_agrup_transf)
'    ElseIf Mid(cod_agrup_transf, 1, 1) = "S" Then
'        RemarcaAnalise3 = InsereSimples(n_req, cod_agrup_transf, "0", "0", cod_agrup_transf)
'    ElseIf cod_agrup_transf = "" Then
'        RemarcaAnalise3 = True
'    End If
'    If RemarcaAnalise3 = True Then
'        RegistaTransformacao n_req, cod_agrup, cod_agrup_transf
'    Else
'        GoTo TrataErro
'    End If
'    Exit Function
'TrataErro:
'    RemarcaAnalise3 = False
'    BG_LogFile_Erros "Erro  ao RemarcaAnalise3: " & Err.Description, Me.Name, "RemarcaAnalise3", True
'    Exit Function
'    Resume Next
'End Function


Private Function RemarcacaoAnalises(Index As Long) As Boolean
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    Dim iReg As Integer
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim lItem As Long
    On Error GoTo TrataErro
    
    
    'VERIFICA SE EXISTEM ANALISES PARA MARCAR AUTOMATICAMENTE
    Call LimitesTubos(CStr(eTubos(Index).requisicao), eTubos(Index).codigo_tubo, eTubos(Index).agrupador_estrutura, lLowerBound, lUpperBound)
     For lItem = lLowerBound To lUpperBound
        If (Not MarcaAnalisesAutomaticas(eTubos(Index).requisicao, eTubos(lItem).codigo_analise, Index)) Then: Exit Function
    Next
    
    If (LocalDiferenteTubo(Index, EcLocal.text)) Then
    
        'MARCA REQUISICAO COMO TRANSFORMADA
        sSql = "UPDATE sl_requis SET flg_transformada = 1 WHERE n_req = " & eTubos(Index).requisicao
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
        
        MudaLocalAnaAcrescentadaTubo Index, eTubos(Index).codigo_tubo
            
        sSql = "SELECT * FROM sl_Requis WHERE n_req = " & eTubos(Index).requisicao & " AND dt_chega IS NULL AND cod_local <> " & gCodLocal
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsReq.Open sSql, gConexao
        If rsReq.RecordCount = 1 Then
            sSql = "UPDATE sl_Requis set cod_local = " & gCodLocal & " WHERE  n_req = " & eTubos(Index).requisicao
            iReg = BG_ExecutaQuery_ADO(sSql)
            If iReg <> 1 Then
                GoTo TrataErro
            End If
            EcLocal.text = gCodLocal
            EcDescrLocal.text = BL_DevolveDescrLocal(CStr(gCodLocal))
            
            sSql = "UPDATE sl_ana_acrescentadas set cod_local = " & gCodLocal & " WHERE  n_req = " & eTubos(Index).requisicao
            sSql = sSql & " AND cod_agrup in (SELECT Cod_ana FROM sl_ana_locais_exec WHERE cod_local = " & gCodLocal & ")"
            iReg = BG_ExecutaQuery_ADO(sSql)
            If iReg < 1 Then
                GoTo TrataErro
            End If
            rsReq.Close
            Set rsReq = Nothing
            'RefrescaLista
            If InsereMovimentoTracking(Index, "REMARCACAO") = False Then
                GoTo TrataErro
            End If
        End If
        
    End If
    RemarcacaoAnalises = True
    Exit Function
    
TrataErro:
    RemarcacaoAnalises = False
    BG_LogFile_Erros "Erro  ao RemarcacaoAnalises: " & Err.Description, Me.Name, "RemarcacaoAnalises", True
    Exit Function
    Resume Next
    
End Function

Private Sub BeforeHandleListViewTree(X As Single, Y As Single)
    
    Dim lvhti As LVHITTESTINFO
    
    lvhti.pt.X = X / Screen.TwipsPerPixelX
    lvhti.pt.Y = Y / Screen.TwipsPerPixelY
    If (ListView_ItemHitTest(lhwndListView, lvhti) = LVI_NOITEM) Then: Exit Sub
    If (lvhti.flags = LVHT_ONITEMICON) Then: Call HandleListViewTree(LvTubos.SelectedItem, Empty)

End Sub

Private Sub BeforeHandlePopMenu()

    Dim bParent As Boolean
    Dim lStructureIndex As Integer
    
    lStructureIndex = RetornaIndiceTubo(LvTubos.SelectedItem.Index, bParent)
    If (lStructureIndex = Empty) Then: Exit Sub
    If bParent = True Then
        If gColheitaChegada = gEnumColheitaChegada.ChegadaTubo Then
            MDIFormInicio.IDM_TUBO(0).Visible = True
            MDIFormInicio.IDM_TUBO(1).Visible = True
            MDIFormInicio.IDM_TUBO(2).Visible = False
            MDIFormInicio.IDM_TUBO(2).Visible = False
            MDIFormInicio.IDM_TUBO(3).Visible = True
            Select Case (eTubos(lStructureIndex).estado_tubo)
                Case gEstadosTubo.chegado, gEstadosTubo.Pendente, gEstadosTubo.colhido:
                    DoEvents
                    MDIFormInicio.PopupMenu MDIFormInicio.HIDE_TUBOS
                    If (LvTubos.Enabled) Then: LvTubos.SetFocus
            End Select
        ElseIf gColheitaChegada = gEnumColheitaChegada.ColheitaTubo Then
            MDIFormInicio.IDM_TUBO(0).Visible = False
            MDIFormInicio.IDM_TUBO(1).Visible = False
            MDIFormInicio.IDM_TUBO(2).Visible = True
            MDIFormInicio.IDM_TUBO(3).Visible = False
            Select Case (eTubos(lStructureIndex).estado_tubo)
                Case gEstadosTubo.colhido:
                    DoEvents
                    MDIFormInicio.PopupMenu MDIFormInicio.HIDE_TUBOS
                    If (LvTubos.Enabled) Then: LvTubos.SetFocus
            End Select
    
        End If
    Else
        If gColheitaChegada = gEnumColheitaChegada.ChegadaTubo Then
            MudarLocalAnaAcrescentada CLng(lStructureIndex)
        End If
    End If
End Sub

Private Sub BeforeHandlePipeExecution(ByVal Item As MSComctlLib.ListItem)
    
    Dim bParent As Boolean
    Dim posicao As Long
    Dim hora As String
'    ' mais um martelan�o jeitoso...
'    If gLAB = "CHVNG" And gCodLocal = 2 Then
'        hora = Bg_DaHora_ADO
'        If Weekday(Bg_DaData_ADO) <> vbSaturday And Weekday(Bg_DaData_ADO) <> vbSunday Then
'            If hora > "09:00" And hora < "19:00" Then
'                gMsgMsg = "Local: Urg�ncia." & vbCrLf & "Tem a certeza que quer dar chegada dos tubos neste local?"
'                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
'                If (gMsgResp <> vbYes) Then: Exit Sub
'
'            End If
'        End If
'    End If
    
    If (LvTubos.ListItems.Count = Empty) Then: Exit Sub
    posicao = RetornaIndiceTubo(Item.Index, bParent)
    If (Not bParent) Then: Exit Sub
    
    If gColheitaChegada = gEnumColheitaChegada.ChegadaTubo Then
        If (eTubos(posicao).estado_tubo = gEstadosTubo.cancelado Or eTubos(posicao).estado_tubo = gEstadosTubo.chegado) Then
            If gLAB = "HSJ" Then
                BG_Mensagem mediMsgBox, "Tubo j� deu entrada anteriormente.", vbInformation, "Entrada de Tubos"
                Exit Sub
            End If
        End If
        If (LocalDiferenteTubo(posicao, BL_HandleNull(EcLocal.text, mediComboValorNull))) And gLAB = "CHVNG" Then
            gMsgMsg = "Tem a certeza que quer dar chegada do(s) tubo(s) neste local?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If (gMsgResp <> vbYes) Then: Exit Sub
        End If
        If (Not RegistaMovimentoTubos(Item.Index)) Then: Exit Sub
    Else
        If eTubos(posicao).estado_tubo = gEstadosTubo.cancelado Then
            BG_Mensagem mediMsgBox, "Tubo cancelado.", vbInformation, "Entrada de Tubos"
            Exit Sub
        ElseIf eTubos(posicao).estado_tubo = gEstadosTubo.chegado Then
            BG_Mensagem mediMsgBox, "Tubo j� deu entrada.", vbInformation, "Entrada de Tubos"
            Exit Sub
        ElseIf eTubos(posicao).estado_tubo = gEstadosTubo.colhido Then
            BG_Mensagem mediMsgBox, "Tubo j� colhido.", vbInformation, "Entrada de Tubos"
            Exit Sub
        End If
        If (Not RegistaColheitasTubos(Item.Index)) Then: Exit Sub
    End If
    If (Not RefrescaLista) Then: Exit Sub
    
End Sub



Private Function VisualizaInformacaoComplementar(Index As Long) As Boolean
 
    Dim posicao As Long
    Dim bParent As Boolean
    
    On Error GoTo TrataErro
    posicao = RetornaIndiceTubo(Index, bParent)
    If (Not bParent) Then: Exit Function
    If (eTubos(posicao).estado_tubo <> gEstadosTubo.cancelado) Then: Exit Function
    EcMotivoEliminacao.text = Empty
    EcDataEliminacao.text = Empty
    EcHoraEliminacao.text = Empty
    EcUtilizadorEliminacao.text = Empty
    EcMotivoEliminacao.text = UCase(BL_SelCodigo("sl_t_canc", "descr_canc", "cod_t_canc", eTubos(posicao).motivo_eliminacao, "V"))
    EcUtilizadorEliminacao.text = BL_DevolveNomeUtilizador(UCase(eTubos(posicao).utilizador_eliminacao), "", "")
    EcDataEliminacao.text = eTubos(posicao).data_eliminacao
    EcHoraEliminacao.text = eTubos(posicao).hora_eliminacao
    VisualizaInformacaoComplementar = True
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Erro  ao VisualizaInformacaoComplementar: " & Err.Description, Me.Name, "VisualizaInformacaoComplementar", True
    Exit Function
    Resume Next
    
End Function

Private Sub EmitirPedidoNovaAmostra()

    
    Dim sSql As String
    Dim continua As Boolean
    
    Dim Report As CrystalReport
     
    BL_InicioProcessamento Me, "A imprimir folha do pedido..."
    
    continua = BL_IniciaReport("PedidoNovaAmostra", "Pedido de Nova amostra", crptToWindow)
    If continua = False Then
        BL_FimProcessamento Me
        Exit Sub
    End If
    
    Set Report = forms(0).Controls("Report")
  
    Report.formulas(0) = "NomeUtente=" & BL_TrataStringParaBD(EcNomeUtente.text)
    Report.formulas(1) = "DtNascUtente=" & BL_TrataStringParaBD(EcDataNasc.text)
    Report.formulas(2) = "NumReq=" & BL_TrataStringParaBD(CStr(gRequisicaoActiva))
    Report.formulas(3) = "NumUtente=" & BL_TrataStringParaBD(EcUtente.text)
    Report.formulas(4) = "Processo=" & BL_TrataStringParaBD(EcUtente.text)
    Report.formulas(5) = "Utilizador=" & BL_TrataStringParaBD(gNomeUtilizador)
    Report.formulas(6) = "TituloUtil=" & BL_TrataStringParaBD(gTituloUtilizador)
    Report.formulas(7) = "ProvReq=" & BL_TrataStringParaBD(BL_SelCodigo("sl_proven", "descr_proven", "cod_proven", EcCodProven.text, "V"))
    Report.formulas(8) = "DtEmissao=" & BL_TrataStringParaBD(Bg_DaData_ADO & Space(1) & Bg_DaHora_ADO)
    Report.formulas(9) = "Area=" & BL_TrataStringParaBD(EcArea.text)
    Report.SelectionFormula = "{sl_cr_pedido_nova_amostra.nome_computador} = " & BL_TrataStringParaBD(gComputador)
    Me.SetFocus

    Call BL_ExecutaReport
    BL_FimProcessamento Me
    
End Sub



Private Function GeraSequenciaRealiza(ByRef sequencia_realiza As Long) As Boolean

    Dim tamanho As Integer
    
    On Error GoTo TrataErro
    sequencia_realiza = -1
    tamanho = Empty
    While (sequencia_realiza = -1 And tamanho <= 10)
        sequencia_realiza = BL_GeraNumero("SEQ_REALIZA")
        tamanho = tamanho + 1
    Wend
    GeraSequenciaRealiza = CBool(sequencia_realiza <> -1)
    Exit Function
    
TrataErro:
    GeraSequenciaRealiza = False
    Exit Function

End Function

Private Function InsereRealiza(sequencia_realiza As Long, requisicao As String, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String) As Boolean
    
    Dim sSql As String
    Dim flag_estado As String
    Dim rsAna As ADODB.recordset
    
    On Error GoTo TrataErro
    flag_estado = DevolveFlagEstado
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    sSql = "insert into sl_realiza (seq_realiza,flg_estado, dt_cri,hr_cri,user_cri, n_req, cod_perfil, cod_ana_c, cod_ana_s, ord_ana," & _
           "ord_ana_compl, ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,ord_marca,flg_facturado,hr_colheita,dt_colheita,hr_ult_admin," & _
           "dt_ult_admin) (select " & sequencia_realiza & "," & flag_estado & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & _
           BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ",n_req, cod_perfil, " & _
           "cod_ana_c, cod_ana_s, ord_ana, ord_ana_compl, ord_ana_perf, cod_agrup, n_folha_trab, " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", ord_marca, flg_facturado, " & _
           "hr_colheita, dt_colheita, hr_ult_admin, dt_ult_admin from sl_marcacoes where n_req = " & _
           BL_TrataStringParaBD(requisicao) & " and cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil) & " and cod_ana_c = " & _
           BL_TrataStringParaBD(cod_ana_c) & " and cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s) & ")"
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    InsereRealiza = CBool(gSQLError = Empty)
    Exit Function
    
TrataErro:
    InsereRealiza = False
    Exit Function

End Function
Private Function ApagaMarcacoes(requisicao As String, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String) As Boolean
    
    Dim sSql As String
    Dim cod_agrup As String
    On Error GoTo TrataErro
    sSql = "DELETE FROM sl_marcacoes WHERE n_req =" & requisicao & " And Cod_Perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c)
    sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    If Cod_Perfil <> "0" Then
        cod_agrup = Cod_Perfil
    ElseIf cod_ana_c <> "0" Then
        cod_agrup = cod_ana_c
    Else
        cod_agrup = cod_ana_s
    End If
    BG_ExecutaQuery_ADO sSql
    BL_RegistaAnaEliminadas requisicao, cod_agrup, Cod_Perfil, cod_ana_c, cod_ana_s
    BG_Trata_BDErro
    ApagaMarcacoes = CBool(gSQLError = Empty)
    Exit Function
    
TrataErro:
    ApagaMarcacoes = False
    Exit Function

End Function

Private Function InsereResultado(sequencia_realiza As Long, cod_ana_c As String, cod_ana_s As String) As Boolean
    
    Dim sSql As String
    
    On Error GoTo TrataErro
    If (cod_ana_c = gGHOSTMEMBER_C Or cod_ana_s = gGHOSTMEMBER_S) Then: InsereResultado = True: Exit Function
    sSql = "insert into sl_res_alfan (seq_realiza, n_res, result) values (" & sequencia_realiza & "," & 1 & "," & _
           BL_TrataStringParaBD(cSemResultado) & ")"
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    InsereResultado = CBool(gSQLError = Empty)
 Exit Function
    
TrataErro:
    InsereResultado = False
    Exit Function

End Function

Private Function DevolveFlagEstado() As String

    If (PermiteValidacao) Then
        DevolveFlagEstado = "3"
    Else
        DevolveFlagEstado = "1"
    End If
    
End Function

Private Function PermiteValidacao() As Boolean
    
    PermiteValidacao = CBool(gPermResUtil = cValMedRes)
    
End Function

Private Function EliminaMarcacaoAnalise(requisicao As Long, codigo_agrupador As String, codigo_analise As String) As Boolean
 
    Dim sql As String
    
    On Error GoTo TrataErro
    Select Case Mid(codigo_agrupador, 1, 1)
        Case "P":
            sql = "delete from sl_marcacoes where n_req = " & requisicao & " and cod_agrup = " & BL_TrataStringParaBD(codigo_agrupador) & _
                  " and cod_ana_c = " & BL_TrataStringParaBD(codigo_analise)
        Case "C":
            sql = "delete from sl_marcacoes where n_req = " & requisicao & " and cod_agrup = " & BL_TrataStringParaBD(codigo_agrupador) & _
                  " and cod_ana_s = " & BL_TrataStringParaBD(codigo_analise)
        Case Else:
            EliminaMarcacaoAnalise = False
    End Select
    BG_ExecutaQuery_ADO sql
    BL_RegistaAnaEliminadas requisicao, codigo_agrupador, "0", "0", "0"
    BG_Trata_BDErro
    EliminaMarcacaoAnalise = CBool(gSQLError = 0)
    Exit Function
    
TrataErro:
    EliminaMarcacaoAnalise = False
    Exit Function
    
End Function

Private Function DevolveTuboAnaliseSimples(codigo_analise As String, Tubos As Collection) As Collection
    
    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    sql = "select cod_tubo from sl_ana_s where cod_ana_s = " & BL_TrataStringParaBD(codigo_analise) & " and cod_tubo is not null"
    rsAna.Open sql, gConexao
    If (rsAna.RecordCount > Empty) Then: Tubos.Add BL_HandleNull(rsAna!cod_tubo, Empty)
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Set DevolveTuboAnaliseSimples = Tubos
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Function DevolveTuboAnaliseComplexa(codigo_analise As String, Tubos As Collection) As Collection
    
    Dim sql As String
    Dim rsAna As ADODB.recordset
    Dim rsMem As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    Set DevolveTuboAnaliseComplexa = New Collection
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    sql = "select cod_tubo from sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(codigo_analise) & " and cod_tubo is not null"
    rsAna.Open sql, gConexao
    If (rsAna.RecordCount > Empty) Then
        Tubos.Add BL_HandleNull(rsAna!cod_tubo, Empty)
    Else
        Set rsMem = New ADODB.recordset
        rsMem.CursorLocation = adUseServer
        rsMem.CursorType = adOpenStatic
        sql = "select cod_membro, cod_ana_s, cod_tubo from sl_membro, sl_Ana_s where cod_membro = cod_ana_s AND cod_ana_c = " & BL_TrataStringParaBD(codigo_analise)
        rsMem.Open sql, gConexao
        While (Not rsMem.EOF)
            If (rsMem.RecordCount > Empty) Then: Tubos.Add BL_HandleNull(rsMem!cod_tubo, Empty)
            
            'Set Tubos = DevolveTuboAnaliseSimples(BL_HandleNull(rsMem!cod_membro, Empty), Tubos)
            rsMem.MoveNext
        Wend
        If (rsMem.state = adStateOpen) Then: rsMem.Close
    End If
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Set DevolveTuboAnaliseComplexa = Tubos
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Function DevolveTuboAnalisePerfil(codigo_analise As String) As Collection
    
    Dim sql As String
    Dim rsAna As ADODB.recordset
    Dim rsMem As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    Set DevolveTuboAnalisePerfil = New Collection
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    sql = "select cod_tubo from sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codigo_analise) & " and cod_tubo is not null"
    rsAna.Open sql, gConexao
    If (rsAna.RecordCount > Empty) Then
        DevolveTuboAnalisePerfil.Add BL_HandleNull(rsAna!cod_tubo, Empty)
        Exit Function
    Else
        Set rsMem = New ADODB.recordset
        rsMem.CursorLocation = adUseServer
        rsMem.CursorType = adOpenStatic
        sql = "select cod_analise from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(codigo_analise)
        rsMem.Open sql, gConexao
        While (Not rsMem.EOF)
            Select Case (Mid(BL_HandleNull(rsMem!cod_analise, Empty), 1, 1))
                Case "C": Set DevolveTuboAnalisePerfil = DevolveTuboAnaliseComplexa(BL_HandleNull(rsMem!cod_analise, Empty), DevolveTuboAnalisePerfil)
                Case "S": Set DevolveTuboAnalisePerfil = DevolveTuboAnaliseSimples(BL_HandleNull(rsMem!cod_analise, Empty), DevolveTuboAnalisePerfil)
            End Select
            rsMem.MoveNext
        Wend
        If (rsMem.state = adStateOpen) Then: rsMem.Close
    End If
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Function DevolveTubosAnalise(codigo_analise As String) As Collection
     
    On Error GoTo TrataErro
    Select Case (Mid(codigo_analise, 1, 1))
        Case "P": Set DevolveTubosAnalise = DevolveTuboAnalisePerfil(codigo_analise)
        Case "C": Set DevolveTubosAnalise = DevolveTuboAnaliseComplexa(codigo_analise, New Collection)
        Case "S": Set DevolveTubosAnalise = DevolveTuboAnaliseSimples(codigo_analise, New Collection)
        Case Else: Set DevolveTubosAnalise = New Collection
    End Select
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Function DevolvePrimeiroTuboAnalise(codigo_analise As String, Optional cod_tubo As String) As String
    
    Dim Tubos As Collection
    Dim i As Integer
    On Error GoTo TrataErro
    Set Tubos = DevolveTubosAnalise(codigo_analise)
    If cod_tubo = "" Then
        If (Tubos.Count > Empty) Then: DevolvePrimeiroTuboAnalise = Tubos.Item(1)
    Else
        For i = 1 To Tubos.Count
            If Tubos.Item(i) = cod_tubo Then
                DevolvePrimeiroTuboAnalise = Tubos.Item(i)
                Exit For
            End If
        Next
    End If
    Exit Function
    
TrataErro:
    Exit Function
    
End Function
   
Private Function RemarcaTubosAnalises(requisicao As Long, data_prevista As String, codigo_analise As String, ByRef novos_tubos As Boolean) As Boolean

    Dim codigo_tubo As Variant
    
    On Error GoTo TrataErro
    For Each codigo_tubo In DevolveTubosAnalise(codigo_analise)
        If (codigo_tubo <> Empty) Then
            If (Not TuboExistente(requisicao, CStr(codigo_tubo))) Then
                If (Not InsereTubo(requisicao, data_prevista, CStr(codigo_tubo), True)) Then: Exit Function
                novos_tubos = True
            End If
        End If
    Next
    RemarcaTubosAnalises = True
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Function InsereTubo(requisicao As Long, data_prevista As String, codigo_tubo As String, Optional executa_chegada) As Boolean

    Dim sql As String
    Dim codAna() As String
    
    On Error GoTo TrataErro
    If (executa_chegada) Then
        TB_InsereTuboBD mediComboValorNull, CStr(requisicao), codigo_tubo, data_prevista, Bg_DaData_ADO, Bg_DaHora_ADO, "", CStr(gCodUtilizador), _
                        CStr(gCodLocal), codAna, gEstadosTubo.chegado, mediComboValorNull, ""
    Else
        TB_InsereTuboBD mediComboValorNull, CStr(requisicao), codigo_tubo, data_prevista, "", "", "", "", "", codAna, gEstadosTubo.Pendente, mediComboValorNull, ""
    End If
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    InsereTubo = CBool(gSQLError = 0)
    Exit Function
    
TrataErro:
    InsereTubo = False
    BG_LogFile_Erros "Erro  ao InsereTubo: " & Err.Description, Me.Name, "InsereTubo", True
    Exit Function
    Resume Next
    
End Function

Private Function TuboExistente(requisicao As Long, codigo_tubo As String) As Boolean
    
    Dim sql As String
    Dim rsTubo As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsTubo = New ADODB.recordset
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, cod_prod " & " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & " and n_req = " & requisicao & "  and sl_tubo.cod_tubo = " & BL_TrataStringParaBD(codigo_tubo)
    rsTubo.Open sql, gConexao
    If (rsTubo.RecordCount > Empty) Then: TuboExistente = True
    If (rsTubo.state = adStateOpen) Then: rsTubo.Close
    Exit Function
    
TrataErro:
    TuboExistente = False
    BG_LogFile_Erros "Erro  ao TuboExistente: " & Err.Description, Me.Name, "TuboExistente", True
    Exit Function
    Resume Next
    
End Function

Private Function DevolveAnalisesMarcacaoAutomatica(codigo_analise As String) As Collection
    
    Dim sSql As String
    Dim rsAna As ADODB.recordset
    
    On Error GoTo TrataErro
    Set DevolveAnalisesMarcacaoAutomatica = New Collection
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    sSql = "select cod_ana_n from sl_marcacao_automatica where cod_ana_1 = " & BL_TrataStringParaBD(codigo_analise)
    sSql = sSql & " and cod_local = " & gCodLocal
    rsAna.Open sSql, gConexao
    While (Not rsAna.EOF)
        DevolveAnalisesMarcacaoAutomatica.Add BL_HandleNull(rsAna!cod_ana_n, Empty)
        rsAna.MoveNext
    Wend
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Exit Function
    
TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function DevolveMembrosAnalise(codigo_analise As String, marcacao_membros As Boolean) As Collection
    
    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    On Error GoTo TrataErro
    Set DevolveMembrosAnalise = New Collection
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    Select Case (Mid(codigo_analise, 1, 1))
        Case "P": sql = "select cod_analise cod_ana from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(codigo_analise)
        Case "C":
            If (Not marcacao_membros) Then
                sql = "select cod_ana_s cod_ana from sl_ana_s where flg_marc_auto = 1 and cod_ana_s in ("
                sql = sql & "select cod_membro cod_ana from sl_membro where cod_ana_c = " & BL_TrataStringParaBD(codigo_analise) & ")"
            Else
                sql = "select cod_membro cod_ana from sl_membro where cod_ana_c = " & BL_TrataStringParaBD(codigo_analise)
            End If
        Case Else: Exit Function
    End Select
    rsAna.Open sql, gConexao
    While (Not rsAna.EOF)
        DevolveMembrosAnalise.Add BL_HandleNull(rsAna!cod_ana, Empty)
        rsAna.MoveNext
    Wend
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Exit Function
    
TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function MarcaAnalisesAutomaticas(requisicao As Long, codigo_analise As String, indice As Long) As Boolean

    Dim analise As Variant
    Dim Membro As Variant
    Dim ordem_analise As Integer
   
    On Error GoTo TrataErro
    For Each analise In DevolveAnalisesMarcacaoAutomatica(codigo_analise)
        If (AnaliseMarcada(requisicao, CStr(analise))) Then: MarcaAnalisesAutomaticas = True: Exit Function
        ordem_analise = DevolveOrdemAnalise(CStr(analise))
        Select Case (Mid(analise, 1, 1))
            Case "P":
                If (Not MarcaMembros(requisicao, CStr(analise), ordem_analise, CStr(analise), indice)) Then: GoTo TrataErro
            Case "C":
                If (Not MarcaMembros(requisicao, CStr(analise), ordem_analise, , indice)) Then: GoTo TrataErro
            Case "S":
                If (Not MarcaAnalise(requisicao, CStr(analise), ordem_analise, , , CStr(analise), , , indice)) Then: GoTo TrataErro
        End Select
    Next
    MarcaAnalisesAutomaticas = True
    Exit Function

TrataErro:
    MarcaAnalisesAutomaticas = False
    Exit Function

End Function

Private Function VerificaMarcacaoMembros(codigo_analise As String) As Boolean

    Dim sql As String
    Dim rsAna As ADODB.recordset

    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    Select Case (Mid(codigo_analise, 1, 1))
        Case "P": sql = "select flg_activo from sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codigo_analise)
        Case "C": sql = "select flg_marc_memb from sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(codigo_analise)
        Case Else: Exit Function
    End Select
    rsAna.Open sql, gConexao
    If (rsAna.RecordCount = Empty) Then: Exit Function
    Select Case (Mid(codigo_analise, 1, 1))
        Case "P": VerificaMarcacaoMembros = CBool(BL_HandleNull(rsAna!flg_activo, 1) = 0)
        Case "C": VerificaMarcacaoMembros = CBool(BL_HandleNull(rsAna!flg_marc_memb, 0) = 1)
        Case Else: Exit Function
    End Select
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Exit Function

TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function MarcaMembros(requisicao As Long, codigo_analise As String, ordem_analise As Integer, _
                              Optional codigo_perfil As String, Optional indice As Long) As Boolean
    
    Dim codigo_membro As Variant
    Dim ordem_membro As Integer
    Dim analise_tag As AnaliseTag
    Dim ghost_perfil_marcado As Boolean
    Dim ghost_complexa_marcado As Boolean
    Dim marcacao_membros As Boolean
    
    On Error GoTo TrataErro
    marcacao_membros = VerificaMarcacaoMembros(codigo_analise)
    If (codigo_perfil <> Empty And Not ghost_perfil_marcado) Then
        If (Not MarcaGhostMember(requisicao, codigo_analise, ordem_analise, codigo_perfil, , , indice)) Then: Exit Function
        ghost_perfil_marcado = True
    End If
    If (Not ghost_complexa_marcado) Then
        If (Not MarcaGhostMember(requisicao, codigo_analise, ordem_analise, codigo_perfil, codigo_analise, , indice)) Then: Exit Function
        ghost_complexa_marcado = True
    End If
    For Each codigo_membro In DevolveMembrosAnalise(codigo_analise, marcacao_membros)
        Select Case (Mid(codigo_membro, 1, 1))
            Case "P":
                If (VerificaMarcacaoMembros(CStr(codigo_membro))) Then
                    If (Not MarcaMembros(requisicao, codigo_perfil, CStr(codigo_membro), DevolveOrdemAnalise(CStr(codigo_membro)), indice)) Then: Exit Function
                    If (DevolveOrdemAnaliseMembro(codigo_analise, analise_tag, ordem_membro, CStr(codigo_membro))) Then
                        Call MarcaAnalise(requisicao, codigo_analise, ordem_analise, CStr(codigo_membro), , , analise_tag, ordem_membro, indice)
                    End If
                End If
            Case "C":
                If (VerificaMarcacaoMembros(CStr(codigo_membro))) Then
                    If (Not MarcaMembros(requisicao, codigo_perfil, CStr(codigo_membro), DevolveOrdemAnalise(CStr(codigo_membro)), indice)) Then: Exit Function
                    If (DevolveOrdemAnaliseMembro(codigo_analise, analise_tag, ordem_membro, CStr(codigo_membro))) Then
                        Call MarcaAnalise(requisicao, codigo_analise, ordem_analise, , CStr(codigo_membro), , analise_tag, ordem_membro, indice)
                    End If
                End If
            Case "S":
                If (DevolveOrdemAnaliseMembro(codigo_analise, analise_tag, ordem_membro, , CStr(codigo_membro))) Then
                    Call MarcaAnalise(requisicao, codigo_analise, ordem_analise, , , CStr(codigo_membro), analise_tag, ordem_membro, indice)
                End If
            Case Else: Exit Function
        End Select
    Next
    MarcaMembros = True
    Exit Function

TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function MarcaAnalise(requisicao As Long, codigo_agrupador As String, ordem_analise As Integer, _
                              Optional codigo_perfil As String, Optional codigo_complexa As String, _
                              Optional codigo_simples As String, Optional analise_tag As AnaliseTag, _
                              Optional ordem_membro As Integer, Optional indice As Long) As Boolean
    
    Dim sSql As String
    Dim ordem_perfil As Integer
    Dim ordem_complexa As Integer
    
    On Error GoTo TrataErro
    Select Case analise_tag
        Case AnaliseTag.t_perfil: ordem_perfil = ordem_membro: codigo_perfil = codigo_agrupador
        Case AnaliseTag.t_complexa: ordem_complexa = ordem_membro: codigo_complexa = codigo_agrupador
    End Select
    sSql = "insert into sl_marcacoes (n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,flg_apar_trans,flg_facturado, seq_req_tubo) values ("
    sSql = sSql & requisicao & "," & BL_TrataStringParaBD(IIf(codigo_perfil = Empty, "0", codigo_perfil)) & "," & BL_TrataStringParaBD(IIf(codigo_complexa = Empty, "0", codigo_complexa)) & ","
    sSql = sSql & BL_TrataStringParaBD(IIf(codigo_simples = Empty, "0", codigo_simples)) & "," & ordem_analise & "," & ordem_complexa & "," & ordem_perfil & ","
    sSql = sSql & BL_TrataStringParaBD(codigo_agrupador) & ",0,0,0,"
    sSql = sSql & eTubos(indice).seq_req_tubo & ")"
    BG_ExecutaQuery_ADO sSql
    
    BL_RegistaAnaAcrescentadas BL_SelCodigo("sl_requis", "SEQ_UTENTE", "N_REQ", requisicao), CStr(requisicao), "", "", "", _
                               codigo_agrupador, Bg_DaData_ADO, Bg_DaHora_ADO, mediComboValorNull, mediComboValorNull
    BG_Trata_BDErro
    MarcaAnalise = CBool(gSQLError = Empty)
    Exit Function

TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function DevolveOrdemAnaliseMembro(codigo_agrupador As String, ByRef analise_tag As AnaliseTag, ByRef ordem_membro As Integer, Optional codigo_perfil As String, Optional codigo_complexa As String, Optional codigo_simples As String) As Boolean

    Dim sSql As String
    Dim RsOrd As ADODB.recordset
     
    On Error GoTo TrataErro
    Set RsOrd = New ADODB.recordset
    RsOrd.CursorType = adOpenStatic
    RsOrd.CursorLocation = adUseServer
    Select Case (Mid(codigo_agrupador, 1, 1))
        Case "P":
            analise_tag = AnaliseTag.t_perfil
            sSql = "SELECT ordem from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(codigo_agrupador)
            sSql = sSql & " and cod_analise = " & BL_TrataStringParaBD(IIf(codigo_perfil <> Empty, codigo_perfil, (IIf(codigo_complexa <> codigo_complexa, codigo_complexa, codigo_simples))))
        Case "C":
            analise_tag = AnaliseTag.t_complexa
            sSql = "SELECT ordem from sl_membro where cod_ana_c = " & BL_TrataStringParaBD(codigo_agrupador)
            sSql = sSql & " and cod_membro = " & BL_TrataStringParaBD(IIf(codigo_complexa <> Empty, codigo_complexa, codigo_simples))
        Case "S":
            analise_tag = AnaliseTag.t_simples
            sSql = "SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(codigo_agrupador)
            sSql = sSql & " UNION SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(codigo_agrupador)
            sSql = sSql & " UNION SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codigo_agrupador)
        Case Else:
            analise_tag = AnaliseTag.t_undef
            DevolveOrdemAnaliseMembro = False
            Exit Function
    End Select
    RsOrd.Open sSql, gConexao
    If (RsOrd.RecordCount > Empty) Then: ordem_membro = BL_HandleNull(RsOrd!ordem, -1)
    If (RsOrd.state = adStateOpen) Then: RsOrd.Close
    DevolveOrdemAnaliseMembro = True
    Exit Function

TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function DevolveOrdemAnalise(codigo_analise As String) As Integer

    Dim sSql As String
    Dim RsOrd As ADODB.recordset
     
    On Error GoTo TrataErro
    Set RsOrd = New ADODB.recordset
    RsOrd.CursorType = adOpenStatic
    RsOrd.CursorLocation = adUseServer
    sSql = "SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(codigo_analise)
    sSql = sSql & " UNION SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(codigo_analise)
    sSql = sSql & " UNION SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codigo_analise)
    RsOrd.Open sSql, gConexao
    If (RsOrd.RecordCount > Empty) Then: DevolveOrdemAnalise = BL_HandleNull(RsOrd!ordem, -1)
    If (RsOrd.state = adStateOpen) Then: RsOrd.Close
    Exit Function

TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function MarcaGhostMember(requisicao As Long, codigo_agrupador As String, ordem_analise As Integer, _
                                  Optional codigo_perfil As String, Optional codigo_complexa As String, _
                                  Optional codigo_simples As String, Optional indice As Long) As Boolean
   
    Dim sSql As String
    
    On Error GoTo TrataErro
    Select Case (Mid(codigo_agrupador, 1, 1))
        Case "P":
            sSql = "insert into sl_marcacoes (n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,flg_apar_trans,flg_facturado, seq_req_tubo) values ("
            sSql = sSql & requisicao & "," & BL_TrataStringParaBD(IIf(codigo_perfil = Empty, "0", codigo_perfil)) & ","
            sSql = sSql & BL_TrataStringParaBD(gGHOSTMEMBER_C) & "," & BL_TrataStringParaBD(gGHOSTMEMBER_S) & ","
            sSql = sSql & ordem_analise & ",-1,-1," & BL_TrataStringParaBD(codigo_agrupador) & ",0,0,0, "
            sSql = sSql & eTubos(indice).seq_req_tubo & ")"
        Case "C":
            sSql = "insert into sl_marcacoes (n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,"
            sSql = sSql & " cod_agrup,n_folha_trab,flg_apar_trans,flg_facturado, seq_req_tubo) values ("
            sSql = sSql & requisicao & "," & BL_TrataStringParaBD(IIf(codigo_perfil = Empty, "0", codigo_perfil)) & ","
            sSql = sSql & BL_TrataStringParaBD(IIf(codigo_complexa = Empty, "0", codigo_complexa)) & ","
            sSql = sSql & BL_TrataStringParaBD(gGHOSTMEMBER_S) & "," & ordem_analise & ",-1,0,"
            sSql = sSql & BL_TrataStringParaBD(codigo_agrupador) & ",0,0,0,"
            sSql = sSql & eTubos(indice).seq_req_tubo & ")"
        Case Else:
            MarcaGhostMember = True
            Exit Function
    End Select
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    MarcaGhostMember = CBool(gSQLError = Empty)
    Exit Function

TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function AnaliseMarcada(requisicao As Long, codigo_analise As String) As Boolean
   
    Dim sSql As String
    Dim RsOrd As ADODB.recordset
     
    On Error GoTo TrataErro
    Set RsOrd = New ADODB.recordset
    RsOrd.CursorType = adOpenStatic
    RsOrd.CursorLocation = adUseServer
    sSql = "select * from sl_marcacoes where n_req = " & requisicao & " and cod_agrup = " & BL_TrataStringParaBD(codigo_analise)
    RsOrd.Open sSql, gConexao
    If (RsOrd.RecordCount > Empty) Then
        AnaliseMarcada = True
    Else
        AnaliseMarcada = False
    End If
    If (RsOrd.state = adStateOpen) Then: RsOrd.Close
    Exit Function

TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function CancelaChegadaTubo(posicao As Long, codigo_motivo As String, Optional cancelamento_automatico As Boolean) As Boolean

    Dim sql As String
    Dim descricao_motivo As String
    
    On Error GoTo TrataErro
    descricao_motivo = BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", codigo_motivo, "V")
    Select Case (eTubos(posicao).estado_tubo)
        Case gEstadosTubo.chegado:
            sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.cancelado & ", dt_eliminacao = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", hr_eliminacao = " & _
                  BL_TrataStringParaBD(Bg_DaHora_ADO) & ", user_eliminacao = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & _
                  ", mot_novo = " & BL_TrataStringParaBD(CStr(codigo_motivo)) & " where n_req = " & eTubos(posicao).requisicao & _
                  " and cod_tubo = " & BL_TrataStringParaBD(eTubos(posicao).codigo_tubo) & " and dt_chega = " & _
                  BL_TrataDataParaBD(eTubos(posicao).data_chegada) & " and hr_chega = " & BL_TrataStringParaBD(eTubos(posicao).hora_chegada) & _
                  " and user_chega = " & BL_TrataStringParaBD(eTubos(posicao).utilizador_chegada) & " and local_chega = " & _
                  eTubos(posicao).codigo_local & " and dt_eliminacao is null "
        Case gEstadosTubo.Pendente, gEstadosTubo.colhido, gEstadosTubo.cancelado:
            sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.cancelado & ", dt_eliminacao = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", hr_eliminacao = " & _
                  BL_TrataStringParaBD(Bg_DaHora_ADO) & ", user_eliminacao = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & _
                  ", mot_novo = " & BL_TrataStringParaBD(CStr(codigo_motivo)) & " where n_req = " & eTubos(posicao).requisicao & _
                  " and cod_tubo = " & BL_TrataStringParaBD(eTubos(posicao).codigo_tubo) & " and dt_eliminacao is null "
        Case Else:
            Exit Function
    End Select
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
'    If (Not cancelamento_automatico) Then: If (Not ActualizaComentarioFinal(eTubos(posicao).requisicao, eTubos(posicao).codigo_tubo, eTubos(posicao).agrupador_estrutura, descricao_motivo)) Then Exit Function
'    sql = "update sl_requis set obs_req =  obs_req || " & BL_TrataStringParaBD(Space(2) & descricao_motivo) & _
'          " where n_req = " & eTubos(posicao).requisicao & " and obs_req not like " & BL_TrataStringParaBD("%" & descricao_motivo & "%")
'    BG_ExecutaQuery_ADO sql
'    BG_Trata_BDErro
    If (Not cancelamento_automatico) Then: If (Not InsereAnalisePredefinidaCancelamento(eTubos(posicao).requisicao, eTubos(posicao).motivo_eliminacao)) Then Exit Function
    CancelaChegadaTubo = CBool(gSQLError = Empty)
    Exit Function
    
TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function PreencheTabela(Index As Long) As Boolean
  
    Dim sSql As String
  
    On Error GoTo TrataErro
    
    sSql = "INSERT INTO sl_cr_pedido_nova_amostra(nome_computador, n_req, cod_tubo, descr_tubo, cod_produto, descr_produto, motivo) VALUES ("
    sSql = sSql & BL_TrataStringParaBD(gComputador) & ","
    sSql = sSql & eTubos(Index).requisicao & ","
    sSql = sSql & BL_TrataStringParaBD(eTubos(Index).codigo_tubo) & ","
    sSql = sSql & BL_TrataStringParaBD(eTubos(Index).descricao_tubo) & ","
    sSql = sSql & BL_TrataStringParaBD(eTubos(Index).codigo_produto) & ","
    sSql = sSql & BL_TrataStringParaBD(eTubos(Index).descricao_produto) & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(eTubos(Index).motivo_eliminacao)) & ")"
    BG_ExecutaQuery_ADO sSql
    
    PreencheTabela = CBool(gSQLError = Empty)
    Exit Function
    
TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function ExisteMapeamento(Index As Long) As Boolean

    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim codMap As String
    On Error GoTo TrataErro
    If (Not LocalDiferenteTubo(Index, EcLocal.text)) Or gLAB <> "CHVNG" Then: ExisteMapeamento = True: Exit Function
    Call LimitesTubos(CStr(eTubos(Index).requisicao), eTubos(Index).codigo_tubo, eTubos(Index).agrupador_estrutura, lLowerBound, lUpperBound)
    For lItem = lLowerBound To lUpperBound
        codMap = MapeiaCodigoAnalise(eTubos(lItem).codigo_analise, gCodLocal)
        If (codMap <> "-1" And codMap <> "") Then: ExisteMapeamento = True: Exit Function
    Next
    ExisteMapeamento = False
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Erro  ao VerificaMapeamento: " & Err.Description, Me.Name, "VerificaMapeamento", True
    Exit Function
    Resume Next
    
End Function


Private Function VerificaTuboCancelado(requisicao As Long, codigo_tubo As String, agrupador_estrutura As Long) As Boolean
   
    Dim lItem As Long
    
    On Error GoTo TrataErro
    For lItem = 1 To UBound(eTubos)
        If (eTubos(lItem).Index = Empty And eTubos(lItem).codigo_tubo = codigo_tubo And eTubos(lItem).estado_tubo = gEstadosTubo.cancelado) Then: VerificaTuboCancelado = True: Exit Function
    Next
    VerificaTuboCancelado = False
    Exit Function
    
TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function PesquisaFrases(codigo_analise As String, ByRef codigo_frase As String) As Boolean

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim res(100) As Variant
    Dim PesqRapida As Boolean
    Dim RsRelacao As ADODB.recordset
    Dim TotalElementosSel As Integer
    
    On Error GoTo TrataErro
   
    Set RsRelacao = New ADODB.recordset
    With RsRelacao
        .Source = "SELECT COUNT(*) as contador FROM sl_ana_frase WHERE cod_ana_s =" & BL_TrataStringParaBD(codigo_analise)
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open , gConexao
    End With
    
    CWhere = ""
    If Not RsRelacao.EOF Then
        If BL_HandleNull(RsRelacao!contador, 0) <> 0 Then
            CWhere = " cod_frase IN ( SELECT cod_frase FROM sl_ana_frase WHERE cod_ana_s = " & BL_TrataStringParaBD(codigo_analise) & ")"
            CWhere = CWhere & " AND (flg_invisivel is null or flg_invisivel  = 0 ) "
        Else
            Exit Function
        End If
    End If
    RsRelacao.Close
    Set RsRelacao = Nothing
        
    FormPesqRapidaAvancada.Width = 10000
    FormPesqRapidaAvancada.LwPesquisa.Width = 9500
    PesqRapida = False
    
    ChavesPesq(1) = "descr_frase"
    CamposEcran(1) = "descr_frase"
    Tamanhos(1) = 8000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_frase"
    CamposEcran(2) = "cod_frase"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_dicionario"
    CampoPesquisa = "descr_frase"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_frase ", " Resultado frase pr�-definido")
    
    If (PesqRapida) Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados res, CancelouPesquisa, TotalElementosSel
        If (Not CancelouPesquisa) Then: PesquisaFrases = True: codigo_frase = res(2)
    Else
        BG_Mensagem mediMsgBox, "N�o existem frases no dicion�rio!", vbExclamation, "ATEN��O"
    End If
    Exit Function
         
TrataErro:
    Exit Function
    Resume Next
    
End Function

Private Function InsereAnalisePredefinidaNova() As Boolean

End Function

Private Function InsereAnalisePredefinidaCancelamento(requisicao As Long, codigo_frase As String) As Boolean
    
    Dim sql As String
    Dim sequencia_realiza As Long
    
    On Error GoTo TrataErro
    If (gAnaliseCancelamentoAmostra = Empty Or BL_HandleNull(gAnaliseCancelamentoAmostra, "-1") = mediComboValorNull) Then
        InsereAnalisePredefinidaCancelamento = True
        Exit Function
    End If
    If codigo_frase = "" Then
        If (Not PesquisaFrases(gAnaliseCancelamentoAmostra, codigo_frase)) Then: Exit Function
    End If
    gConexao.BeginTrans
    If (Not GeraSequenciaRealiza(sequencia_realiza)) Then: Exit Function
    sql = "delete from sl_realiza where n_req = " & requisicao & " and cod_agrup = " & BL_TrataStringParaBD(gAnaliseCancelamentoAmostra)
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    If (gSQLError <> Empty) Then: gConexao.RollbackTrans: Exit Function
    sql = "insert into sl_realiza (seq_realiza,n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,"
    sql = sql & "flg_estado,dt_cri,user_cri,hr_cri,dt_act,user_act,hr_act,dt_chega,seq_utente,"
    sql = sql & "flg_unid_act1,ord_marca,flg_facturado) values ("
    sql = sql & sequencia_realiza & "," & requisicao & "," & "'0','0'," & BL_TrataStringParaBD(gAnaliseCancelamentoAmostra) & ","
    sql = sql & 1 & "," & "null" & "," & "null" & "," & BL_TrataStringParaBD(gAnaliseCancelamentoAmostra) & "," & "null" & "," & gEstadoAnaComResultado & ","
    sql = sql & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & gCodUtilizador & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
    sql = sql & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & gCodUtilizador & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
    sql = sql & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & "null" & "," & BL_TrataStringParaBD(1) & "," & 1 & "," & 0 & ")"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    If (gSQLError <> Empty) Then: gConexao.RollbackTrans: Exit Function
    sql = "insert into sl_res_frase (seq_realiza,n_res,cod_frase,ord_frase,flg_imprimir,flg_apar) values ("
    sql = sql & sequencia_realiza & "," & 1 & "," & BL_TrataStringParaBD(codigo_frase) & "," & 0 & "," & 1 & "," & 0 & ")"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    If (gSQLError <> Empty) Then: gConexao.RollbackTrans: Exit Function
    InsereAnalisePredefinidaCancelamento = True
    gConexao.CommitTrans
    Exit Function
    
TrataErro:
    gConexao.RollbackTrans
    Exit Function
    Resume Next
    
End Function

Private Function ActualizaComentarioFinal(requisicao As Long, codigo_tubo As String, agrupador_estrutura As Long, Optional descricao_motivo As String) As Boolean
    
    Dim sql As String
    Dim frase As String
    Dim RsComentario As ADODB.recordset
    If gLAB <> "CHVNG" Then
        ActualizaComentarioFinal = True
        Exit Function
    End If

    On Error GoTo TrataErro
    If (descricao_motivo <> Empty) Then
        frase = "Foi pedido nova amostra no dia " & Bg_DaData_ADO & Space(1) & Bg_DaHora_ADO & " pelo motivo " & descricao_motivo
    Else
        If (Not VerificaTuboCancelado(requisicao, codigo_tubo, agrupador_estrutura)) Then: ActualizaComentarioFinal = True: Exit Function
        frase = "Recep��o de nova amostra no dia " & Bg_DaData_ADO & Space(1) & Bg_DaHora_ADO
    End If
    Set RsComentario = New ADODB.recordset
    With RsComentario
        .Source = "select * from sl_cm_fin where n_req = " & requisicao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open , gConexao
    End With
    If (RsComentario.RecordCount > Empty) Then
        sql = "update sl_cm_fin set descr_cm_fin = descr_cm_fin || chr(13)  || " & BL_TrataStringParaBD(frase) & " where n_req = " & requisicao
    Else
        sql = "insert into sl_cm_fin(seq_cm_fin,n_req,descr_cm_fin) values ((select nvl(max(seq_cm_fin),0) + 1 from sl_cm_fin)," & _
              requisicao & "," & BL_TrataStringParaBD(frase) & ")"
    End If
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    ActualizaComentarioFinal = CBool(gSQLError = Empty)
    If (RsComentario.state = adStateOpen) Then: RsComentario.Close
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ActualizaComentarioFinal", Me.Name, "ActualizaComentarioFinal", False
    Exit Function
    Resume Next
End Function

' Imprime listagem dos tubos pendentes.
Private Sub ImprimeListagemTubosPendentes()
    Dim continua As Boolean
    Dim sSql As String
    
    On Error GoTo TrataErro
    If totalReq <= 0 Then Exit Sub
    
    BG_Mensagem cr_mediMsgStatus, ("A imprimir..."), cr_mediMsgBeep + cr_mediMsgPermanece
    CR_BL_PoeMousePointer cr_mediMP_Espera
    sSql = "delete from sl_cr_tubos_pendentes where nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListagemTubosPendentes", "Listagem de Tubos Pendentes", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListagemTubosPendentes", "Listagem de Tubos Pendentes", crptToWindow)
    End If
    
    If continua = False Then Exit Sub
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    If (Empty <> gSQLError) Then: GoTo TrataErro
  
    If (False = PreencheTabelaTubosPendentes) Then: BG_Mensagem mediMsgBox, "Erro ao imprimir mensagem!", vbExclamation, " Erro": GoTo TrataErro
        'edgar.parada IPO-P-13743 21.02.2019 - Colocada ordena��o por cod_tubo
    Report.SQLQuery = "select * from sl_cr_tubos_pendentes where nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & " ORDER BY n_req, cod_tubo"
        
    Report.formulas(0) = "DtInicial=" & BL_TrataDataParaBD("" & EcDtIni.value)
    Report.formulas(1) = "DtFinal=" & BL_TrataDataParaBD("" & EcDtFim.value)
    Report.formulas(3) = "Urgencia=" & BL_TrataStringParaBD("" & cbUrgencia.text)
    Report.formulas(4) = "Locais=" & BL_TrataStringParaBD("" & DevolveFormulaListas(EcListaLocais))
    Report.formulas(5) = "Tubos=" & BL_TrataStringParaBD("" & DevolveFormulaListas(EcListaTubos))
    Report.formulas(6) = "Sala=" & BL_TrataStringParaBD("" & EcDescrSala)
        'edgar.parada IPO-P-13743 21.02.2019
    Report.formulas(7) = "Analises=" & BL_TrataStringParaBD("" & ImprimeAna)
    '
    Report.Destination = gImprimirDestino
    BL_ExecutaReport
    BG_Mensagem cr_mediMsgStatus, "", cr_mediMsgBeep + cr_mediMsgPermanece
    CR_BL_PoeMousePointer cr_mediMP_Activo
    Exit Sub
    
TrataErro:
    BG_Mensagem cr_mediMsgStatus, "", cr_mediMsgBeep + cr_mediMsgPermanece
    CR_BL_PoeMousePointer cr_mediMP_Activo
    Exit Sub
    
End Sub

' Preenche tabela tubos pendentes.
Private Function PreencheTabelaTubosPendentes()
    Dim rsAna As New ADODB.recordset
    Dim i_req As Integer
    Dim i_tub As Integer
    Dim analise As Variant
    Dim codigo As String
    Dim descricao As String
      
    Dim sql As String
    
    On Error GoTo TrataErro
    For i_req = 1 To UBound(EstrutReq)
        'sql = "SELECT distinct cod_agrup,descr_ana  FROM sl_marcacoes, slv_analises WHERE n_Req = " & EstrutReq(i_req).n_req & " AND cod_agrup = cod_ana "
        'edgar.parada IPO-P-13743 21.02.2019
        sql = "SELECT distinct a.cod_agrup, b.descr_ana,  e.descr_gr_ana, c.nome_ute  FROM sl_marcacoes a, slv_analises b, sl_identif c, sl_requis d, sl_gr_ana e "
        sql = sql & " WHERE a.n_Req = " & EstrutReq(i_req).n_req & " AND a.n_req = d.n_req AND b.cod_gr_ana = e.cod_gr_ana AND c.seq_utente = d.seq_utente AND a.cod_agrup = b.cod_ana"
        'CHUC-12894
        'If cbGrAna.ListIndex > mediComboValorNull Then
        If EcCodGrupo <> "" Then
           'sql = sql & " AND b.gr_ana = " & cbGrAna.ItemData(cbGrAna.ListIndex)
           sql = sql & " AND b.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo)
        End If
        '
        rsAna.CursorType = adOpenStatic
        rsAna.CursorLocation = adUseServer
        rsAna.Open sql, gConexao
        If rsAna.RecordCount >= 1 Then
            For i_tub = 1 To UBound(EstrutReq(i_req).TubosPend)
                 If (LvTubosCores.t_vermelho_claro = EstrutReq(i_req).TubosPend(i_tub).cor Or LvTubosCores.t_laranja = EstrutReq(i_req).TubosPend(i_tub).cor) Then
                    rsAna.MoveFirst
                    While Not rsAna.EOF
                        If DevolvePrimeiroTuboAnalise(BL_HandleNull(rsAna!cod_agrup, Empty), EstrutReq(i_req).TubosPend(i_tub).cod_tubo) = EstrutReq(i_req).TubosPend(i_tub).cod_tubo Then
                            sql = "insert into sl_cr_tubos_pendentes (nome_computador, n_req, dt_prevista,"
                            sql = sql & "cod_tubo,descr_tubo,estado,cod_ana,descr_ana, gr_ana, nome) values ("
                            sql = sql & BL_TrataStringParaBD(gComputador) & "," & EstrutReq(i_req).n_req & ","
                            sql = sql & BL_TrataDataParaBD(EstrutReq(i_req).dt_previ) & ","
                            sql = sql & BL_TrataStringParaBD(EstrutReq(i_req).TubosPend(i_tub).cod_tubo) & ","
                            sql = sql & BL_TrataStringParaBD(EstrutReq(i_req).TubosPend(i_tub).descR_tubo) & ","
                            sql = sql & BL_TrataStringParaBD(UCase(EstrutReq(i_req).TubosPend(i_tub).cor)) & ","
                            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_agrup, "")) & ","
                            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rsAna!descr_ana, "")) & ","
                                                        'edgar.parada IPO-P-13743 21.02.2019
                            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rsAna!descr_gr_ana, "")) & ","
                            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rsAna!nome_ute, "")) & ")"
                            '
                            BG_ExecutaQuery_ADO sql
                            BG_Trata_BDErro
                            If (Empty <> gSQLError) Then: Exit Function
                        End If
                        rsAna.MoveNext
                    Wend
                End If
            Next
        End If
        rsAna.Close
    Next
    PreencheTabelaTubosPendentes = True
    Exit Function
    
TrataErro:
    BG_Mensagem cr_mediMsgStatus, "", cr_mediMsgBeep + cr_mediMsgPermanece
    Exit Function
    Resume Next
End Function

Private Function DevolveAnalisesTubo(requisicao As String, codigo_tubo As String) As Collection
    
    Dim sSql As String
    Dim rsAnalises As ADODB.recordset
     
    On Error GoTo TrataErro
    Set DevolveAnalisesTubo = New Collection
    Set rsAnalises = New ADODB.recordset
    rsAnalises.CursorType = adOpenStatic
    rsAnalises.CursorLocation = adUseServer
    sSql = "select distinct cod_ana, descr_ana, "
    sSql = sSql & " (SELECT descR_obs FROM sl_obs_ana_Req WHERE n_Req = " & requisicao & " AND cod_Agrup = slv_analises.cod_ana  and rownum = 1) obs"
    sSql = sSql & " from slv_analises where cod_tubo = "
    sSql = sSql & BL_TrataStringParaBD(codigo_tubo) & " and cod_ana in "
    sSql = sSql & " (select cod_agrup from sl_marcacoes where n_req = " & requisicao & ")"
    rsAnalises.Open sSql, gConexao
    While (Not rsAnalises.EOF)
        DevolveAnalisesTubo.Add BL_HandleNull(rsAnalises!cod_ana, Empty) & ";" & _
                                BL_HandleNull(rsAnalises!descr_ana, Empty) & IIf(BL_HandleNull(rsAnalises!obs, "") = "", "", " *" & BL_HandleNull(rsAnalises!obs, "") & "*")
        rsAnalises.MoveNext
    Wend
    If (rsAnalises.state = adStateOpen) Then: rsAnalises.Close
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Function DevolveFormulaListas(lista As ListBox) As String

    Dim i As Integer
    If (Empty = lista.ListCount) Then: Exit Function
    For i = 0 To lista.ListCount
        If (Empty <> lista.List(i)) Then: DevolveFormulaListas = DevolveFormulaListas & lista.List(i) & "; "
    Next

End Function


' ------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM TUBOS SECUND�RIOS

' ------------------------------------------------------------------------------------------------------

Private Sub PreencheTubosSec(cod_tubo_prim As String, cod_tubo_sec As String, seq_req_tubo As Long, cod_etiq_prim As String, n_req As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim flg_existe As Boolean
    Dim i As Integer
    Dim rsSec As New ADODB.recordset
    If cod_tubo_sec <> "" Then
        sSql = "SELECT * FROM sl_tubo_sec WHERE cod_tubo = " & BL_TrataStringParaBD(cod_tubo_sec)
    Else
        sSql = "SELECT * FROM sl_tubo_sec WHERE cod_tubo_prim = " & BL_TrataStringParaBD(cod_tubo_prim)
    End If
    
    rsSec.CursorType = adOpenStatic
    rsSec.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSec.Open sSql, gConexao
    If rsSec.RecordCount > 0 Then
        While Not rsSec.EOF
            flg_existe = False
            For i = 1 To totalTubosSec
                If estrutTubosSec(i).cod_tubo_sec = BL_HandleNull(rsSec!cod_tubo, "") Then
                    flg_existe = True
                End If
            Next i
            If flg_existe = False Then
                totalTubosSec = totalTubosSec + 1
                ReDim Preserve estrutTubosSec(totalTubosSec)
                estrutTubosSec(totalTubosSec).cod_tubo_prim = cod_tubo_prim
                estrutTubosSec(totalTubosSec).cod_tubo_sec = BL_HandleNull(rsSec!cod_tubo, "")
                estrutTubosSec(totalTubosSec).descr_tubo_sec = BL_HandleNull(rsSec!descR_tubo, "")
                estrutTubosSec(totalTubosSec).etiq_tubo_sec = BL_HandleNull(rsSec!cod_etiq, "")
                estrutTubosSec(totalTubosSec).abr_ana = BL_RetornaAbrAnaTubo(CStr(n_req), BL_HandleNull(rsSec!cod_tubo, ""), False)
                estrutTubosSec(totalTubosSec).Copias = BL_HandleNull(rsSec!num_copias, 1)
                If estrutTubosSec(totalTubosSec).Copias = 0 Then
                    estrutTubosSec(totalTubosSec).Copias = 1
                End If
            End If
            rsSec.MoveNext
        Wend
    End If
    rsSec.Close
    
    sSql = "select x3.cod_meio, x3.descr_meio from sl_diario x1, sl_diario_meio x2, sl_cod_meio x3"
    sSql = sSql & " where x1.seq_diario  = x2.seq_diario and x2.cod_meio = x3.cod_meio and x1.cod_agrup IN "
    sSql = sSql & " (select cod_agrup FROM sl_marcacoes x4  WHERE x4.n_reQ = x1.n_req AND x4.seq_req_tubo = " & seq_req_tubo & ")"
    rsSec.CursorType = adOpenStatic
    rsSec.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSec.Open sSql, gConexao
    If rsSec.RecordCount > 0 Then
            flg_existe = False
            For i = 1 To totalTubosSec
                If cod_tubo_prim = estrutTubosSec(totalTubosSec).cod_tubo_prim Then
                    flg_existe = True
                End If
            Next i
            If flg_existe = False And gLAB = "CHNE" Then
                totalTubosSec = totalTubosSec + 1
                ReDim Preserve estrutTubosSec(totalTubosSec)
                estrutTubosSec(totalTubosSec).cod_tubo_prim = cod_tubo_prim
                estrutTubosSec(totalTubosSec).cod_tubo_sec = "Admin"
                estrutTubosSec(totalTubosSec).descr_tubo_sec = "Registo Microbiologia"
                estrutTubosSec(totalTubosSec).etiq_tubo_sec = ""
                estrutTubosSec(totalTubosSec).abr_ana = ""
                estrutTubosSec(totalTubosSec).Copias = 1
            End If
    
        While Not rsSec.EOF
            flg_existe = False
            For i = 1 To totalTubosSec
                If estrutTubosSec(i).cod_tubo_sec = "Meio" & BL_HandleNull(rsSec!cod_meio, "") And cod_tubo_prim = estrutTubosSec(totalTubosSec).cod_tubo_prim Then
                    flg_existe = True
                End If
            Next i
            If flg_existe = False Then
                totalTubosSec = totalTubosSec + 1
                ReDim Preserve estrutTubosSec(totalTubosSec)
                estrutTubosSec(totalTubosSec).cod_tubo_prim = cod_tubo_prim
                estrutTubosSec(totalTubosSec).cod_tubo_sec = "Meio" & BL_HandleNull(rsSec!cod_meio, "")
                estrutTubosSec(totalTubosSec).descr_tubo_sec = BL_HandleNull(rsSec!descr_meio, "")
                estrutTubosSec(totalTubosSec).etiq_tubo_sec = cod_etiq_prim
                estrutTubosSec(totalTubosSec).abr_ana = ""
                estrutTubosSec(totalTubosSec).Copias = 1
            End If
            rsSec.MoveNext
        Wend
    End If
    rsSec.Close
    Set rsSec = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao preencher tubos Sec: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheTubosSec"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------------

' VERIFICA QUE ETIQUETAS SECUNDARIAS SAO PARA IMPRIMIR

' ------------------------------------------------------------------------------------------------------
Private Sub VerificaEtiquetasSecundarias(cod_tubo_prim As String)
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To totalTubosSec
        If estrutTubosSec(i).cod_tubo_prim = cod_tubo_prim Then
            ImprimeEtiqSec i
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao verificar tubos Sec para imprimir: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaEtiquetasSecundarias"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------------

' IMPRESSAO DE ETIQUETAS PRIMARIAS

' ------------------------------------------------------------------------------------------------------

Private Sub VerificaEtiquetasPrimarias(requisicao As String, indice As Long)
    On Error GoTo TrataErro
    If gImprimeEtiqTuboPrimChegadaTubo = mediSim Then
        If eTubos(indice).indiceEstrutTubos > mediComboValorNull Then
            If gEstruturaTubos(eTubos(indice).indiceEstrutTubos).dt_imp = "" Then
                TB_ImprimeEtiqueta CStr(requisicao), CInt(eTubos(indice).indiceEstrutTubos), mediComboValorNull, "", ""
                'BRUNODSANTOS CHUC-7934 - 22.04.2016
                If gImprimirEtiqResumo = mediSim And gImprimeEtiqResumoEcraChegadaTubos = mediSim Then
                    Call BL_ImprimeEtiqResumo(eTubos(), PassaParam, True, arrReq())
                End If
                '
            End If
        End If
    End If
    If eTubos(indice).cod_local_extra <> "" Then
        If gImprimeEtiqTuboPrimChegadaTubo = mediSim Then
            TB_ImprimeEtiqueta CStr(requisicao), CInt(eTubos(indice).indiceEstrutTubos), mediComboValorNull, eTubos(indice).cod_local_extra, ""
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao verificar tubos primarios para imprimir: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaEtiquetasPrimarias", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------------

' IMPRIME DETERMINADA ETIQUETA SECUNDARIA

' ------------------------------------------------------------------------------------------------------
Private Sub ImprimeEtiqSec(indice As Integer)
    On Error GoTo TrataErro
    Dim i As Integer
    Dim N_req_bar As String
    Dim idade As String
    Dim descrTuboPrim As String
    Dim requisicao As String
    Dim etiqueta As String
    requisicao = ""
    descrTuboPrim = ""
    etiqueta = ""
    
    For i = 1 To totalTubos
        If eTubos(i).codigo_tubo = estrutTubosSec(indice).cod_tubo_prim Then
            descrTuboPrim = BL_HandleNull(eTubos(i).descricao_tubo, "")
            'NELSONPSILVA CHVNG-7461 29.10.2018
            requisicao = BL_HandleNull(eTubos(i).requisicao, "")
            etiqueta = BL_HandleNull(eTubos(i).etiqueta, "")
            '
        End If
    Next i
    
    'NELSONPSILVA CHVNG-7461 29.10.2018 Troca BL_HandleNull(reqAtual.n_req, gRequisicaoActiva) -> etiqueta
    If gAtiva_Nova_Numeracao_Tubo = mediSim Then
        N_req_bar = etiqueta
    Else
        N_req_bar = Right("0000000" & requisicao, 7)
        N_req_bar = estrutTubosSec(indice).etiq_tubo_sec & N_req_bar
    End If
    '
    ' ---------------------------------
    If BL_HandleNull(reqAtual.dt_nasc_ute, "") <> "" Then
        idade = CStr(BG_CalculaIdade(CDate(reqAtual.dt_nasc_ute), Bg_DaData_ADO))
    End If
        
    If Not BL_LerEtiqIni("etiq_sec.ini") Then
        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
        Exit Sub
    End If
    If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
        MsgBox "Imposs�vel abrir impressora etiquetas"
        Exit Sub
    End If
        
    'BL_HandleNull(reqAtual.n_req, gRequisicaoActiva),
    Call BL_EtiqSecPrint(reqAtual.t_utente, reqAtual.Utente, requisicao, _
                    reqAtual.nome_ute, N_req_bar, estrutTubosSec(indice).descr_tubo_sec, estrutTubosSec(indice).Copias, _
                    estrutTubosSec(indice).abr_ana, reqAtual.t_urg, reqAtual.n_proc_1, reqAtual.n_proc_2, reqAtual.dt_chega, descrTuboPrim, etiqueta)
    BL_EtiqClosePrinter
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao imprimir tubo Sec: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeEtiqSec"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodsala_Validate(cancel As Boolean)
    cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodsala_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodsala_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
End Sub


Private Sub BtNotas_Click()
    
    Notas
End Sub

Private Sub Notas()
    Dim lStructureIndex As Long
    Dim bParent As Boolean
    On Error GoTo TrataErro
    lStructureIndex = RetornaIndiceTubo(LvTubos.SelectedItem.Index, bParent, "")
    If lStructureIndex > mediComboValorNull Then
        FormChegadaPorTubosV2.Enabled = False
        FormNotasReq.Show
        FormNotasReq.EcNumReq.text = gRequisicaoActiva
        FormNotasReq.EcSeqReqTubo.text = eTubos(lStructureIndex).seq_req_tubo
        FormNotasReq.grupoNota = gGrupoNotasTubo
        FormNotasReq.FuncaoProcurar
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotas_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotas_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotasVRM_Click()
    Notas

End Sub
    
' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset
    
    BtNotas.Enabled = True
    BtNotasVRM.Enabled = True
    If gRequisicaoActiva <= 0 Then Exit Sub
    sSql = "SELECT count(*) total FROM sl_obs_ana_req WHERE seq_req_tubo is not null AND n_req = " & gRequisicaoActiva
    sSql = sSql & " AND (flg_invisivel is null or flg_invisivel = 0)"

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True
            
        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotas: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheNotas"
    Exit Sub
    Resume Next
End Sub



'' ------------------------------------------------------------------------------------------------------------
'
'' REGISTA TRANSFORMACAO DE ANALISES
'
'' ------------------------------------------------------------------------------------------------------------
'Private Function RegistaTransformacao(n_req As String, cod_agrup As String, cod_agrup_transf As String) As Boolean
'    On Error GoTo TrataErro
'    Dim iReg As Integer
'    Dim sSql As String
'
'    sSql = "INSERT INTO sl_ana_transformada (seq_ana_transf, n_req,  cod_agrup, cod_agrup_transf, user_cri , dt_cri, hr_cri)"
'    sSql = sSql & " VALUES( seq_ana_transf.nextval,"
'    sSql = sSql & n_req & " ,"
'    sSql = sSql & BL_TrataStringParaBD(cod_agrup) & " ,"
'    sSql = sSql & BL_TrataStringParaBD(cod_agrup_transf) & " ,"
'    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & " ,"
'    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & " ,"
'    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
'    iReg = BG_ExecutaQuery_ADO(sSql)
'    If iReg = 1 Then
'        RegistaTransformacao = True
'    Else
'        RegistaTransformacao = False
'    End If
'
'Exit Function
'TrataErro:
'    RegistaTransformacao = False
'    BG_LogFile_Erros "RegistaTransformacao: " & Err.Number & " - " & Err.Description, Me.Name, "RegistaTransformacao"
'    Exit Function
'    Resume Next
'End Function



Private Function InsereSimples(n_req As String, cod_agrup As String, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim ordem As Integer
    Dim ord_compl As Integer
    Dim ord_perf As Integer
    Dim iReg As Integer
    
    BL_RetornaOrdens Cod_Perfil, cod_ana_c, cod_ana_s, ordem, ord_compl, ord_perf
    If ord_compl = -1 And ordem = -1 And ord_perf = -1 Then
        GoTo TrataErro
    End If
    sSql = "INSERT INTO sl_marcacoes(n_req, cod_perfil, cod_ana_c, cod_ana_s, ord_ana, ord_ana_compl, ord_ana_perf, cod_agrup, n_folha_trab, "
    sSql = sSql & " flg_apar_trans, ord_marca, flg_facturado,dt_chega) VALUES ("
    sSql = sSql & n_req & "," & BL_TrataStringParaBD(Cod_Perfil) & "," & BL_TrataStringParaBD(cod_ana_c) & ","
    sSql = sSql & BL_TrataStringParaBD(cod_ana_s) & "," & ordem & "," & ord_compl & "," & ord_perf & ", " & BL_TrataStringParaBD(cod_agrup) & ",0,"
    sSql = sSql & "0,1,0," & BL_TrataStringParaBD(Bg_DaData_ADO) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    InsereSimples = True
Exit Function
TrataErro:
    InsereSimples = False
    BG_LogFile_Erros "InsereSimples: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "InsereSimples"
    Exit Function
    Resume Next
End Function

Private Function InsereComplexa(n_req As String, cod_agrup As String, Cod_Perfil As String, cod_ana_c As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsComp As New ADODB.recordset
    
    If InsereSimples(n_req, cod_agrup, Cod_Perfil, cod_ana_c, gGHOSTMEMBER_S) = False Then
        GoTo TrataErro
    End If
    sSql = "SELECT  * FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND t_membro = 'A'"
    rsComp.CursorType = adOpenStatic
    rsComp.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsComp.Open sSql, gConexao
    If rsComp.RecordCount > 0 Then
        While Not rsComp.EOF
            If InsereSimples(n_req, cod_agrup, Cod_Perfil, cod_ana_c, BL_HandleNull(rsComp!cod_membro, "")) = False Then
                GoTo TrataErro
            End If
            rsComp.MoveNext
        Wend
        rsComp.Close
        Set rsComp = Nothing
        InsereComplexa = True
    Else
        InsereComplexa = False
    End If
Exit Function
TrataErro:
    InsereComplexa = False
    BG_LogFile_Erros "InsereComplexa: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "InsereComplexa"
    Exit Function
    Resume Next
End Function

Private Function InserePerfil(n_req As String, cod_agrup As String, Cod_Perfil As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsPerf As New ADODB.recordset
    
    InsereComplexa n_req, cod_agrup, Cod_Perfil, gGHOSTMEMBER_C
    sSql = "SELECT  * FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(Cod_Perfil)
    RsPerf.CursorType = adOpenStatic
    RsPerf.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsPerf.Open sSql, gConexao
    If RsPerf.RecordCount > 0 Then
        While Not RsPerf.EOF
            If Mid(BL_HandleNull(RsPerf!cod_analise, ""), 1, 1) = "C" Then
                If InsereComplexa(n_req, cod_agrup, Cod_Perfil, RsPerf!cod_analise) = False Then
                    GoTo TrataErro
                End If
            ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, ""), 1, 1) = "S" Then
                If InsereSimples(n_req, cod_agrup, Cod_Perfil, "0", BL_HandleNull(RsPerf!cod_ana_s, "")) = False Then
                    GoTo TrataErro
                End If
            End If
            RsPerf.MoveNext
        Wend
        RsPerf.Close
        Set RsPerf = Nothing
        InserePerfil = True
    Else
        InserePerfil = False
    End If
Exit Function
TrataErro:
    InserePerfil = False
    BG_LogFile_Erros "InserePerfil: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "InserePerfil"
    Exit Function
    Resume Next
End Function



Private Sub MudarLocalAnaAcrescentada(indice As Long)
    Dim i As Integer
    On Error GoTo TrataErro

    If gMultiLocalAmbito <> mediSim Then
        Exit Sub
    End If
    
    For i = 0 To MDIFormInicio.IDM_LOCAL.Count - 1
        'NELSONPSILVA 24.01.2017 CHVNG-9009
        'If BL_DevolveLocalPretendidoExecAna(eTubos(indice).codigo_analise, gEstruturaLocais(i + 1).cod_local) = gEstruturaLocais(i + 1).cod_local Then
            'MDIFormInicio.IDM_LOCAL(i).Visible = True
            If eTubos(indice).cod_local_ana = gEstruturaLocais(i + 1).cod_local Then
                MDIFormInicio.IDM_LOCAL(i).checked = True
            Else
                MDIFormInicio.IDM_LOCAL(i).checked = False
            End If
        'Else
            'MDIFormInicio.IDM_LOCAL(i).Visible = False
        'End If
    Next
            
    MDIFormInicio.PopupMenu MDIFormInicio.HIDE_LOCAIS
Exit Sub
TrataErro:
    BG_LogFile_Erros "MudarLocalAnaAcrescentada: " & Err.Number & " - " & Err.Description, Me.Name, "MudarLocalAnaAcrescentada"
    Exit Sub
    Resume Next
End Sub

Public Sub MudaLocalAnaAcrescentadaTubo(indice As Long, cod_tubo As String)
    Dim sSql As String
    Dim lStructureIndex As Long
    Dim bParent As Boolean
    Dim i As Integer
    Dim iLocal As Integer
    On Error GoTo TrataErro
    
    For iLocal = 1 To gTotalLocais
        If gEstruturaLocais(iLocal).cod_local = gCodLocal Then
            Exit For
        End If
    Next iLocal
    If iLocal > gTotalLocais Then
        Exit Sub
    End If
    lStructureIndex = RetornaIndiceTubo(LvTubos.SelectedItem.Index, bParent, "")
    If gEstruturaLocais(iLocal + 1).cod_local <> eTubos(lStructureIndex).cod_local_ana Then

        If gMultiLocalAmbito <> mediSim Then
            gMsgMsg = "Deseja alterar o local de todas as an�lises para o local: " & gEstruturaLocais(iLocal).descr_local & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        End If
        If BL_HandleNull(gMsgResp, vbYes) = vbYes Or gMultiLocalAmbito = mediSim Then
            For i = 1 To totalTubos
                If eTubos(i).codigo_analise <> "" And (eTubos(i).codigo_tubo = cod_tubo Or (eTubos(i).codigo_analise = eTubos(indice).codigo_analise)) Then
                    If BL_DevolveLocalPretendidoExecAna(eTubos(i).codigo_analise, gEstruturaLocais(iLocal).cod_local) = gEstruturaLocais(iLocal).cod_local Then
                        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(Right(EcNumReq, 7), gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
                        sSql = "UPDATE sl_ana_Acrescentadas set cod_local = " & gEstruturaLocais(iLocal).cod_local & " WHERE n_req = " & eTubos(lStructureIndex).requisicao
                        sSql = sSql & " AND (flg_eliminada IS NULL or flg_eliminada = 0) and cod_Agrup = " & BL_TrataStringParaBD(eTubos(i).codigo_analise)
                        BG_ExecutaQuery_ADO sSql
                        eTubos(i).cod_local_ana = gEstruturaLocais(iLocal).cod_local
                        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(EcNumReq, gRequisicaoActiva)) para eTubos(lStructureIndex).requisicao
                        If RegistaAlteraLocalAnaAcrescentadas(eTubos(lStructureIndex).requisicao, eTubos(i).codigo_analise, eTubos(i).cod_local_ana, gEstruturaLocais(iLocal).cod_local) = False Then
                            GoTo TrataErro
                        End If
                    End If
                End If
            Next
        Else
            If BL_DevolveLocalPretendidoExecAna(eTubos(lStructureIndex).codigo_analise, gEstruturaLocais(iLocal).cod_local) = gEstruturaLocais(iLocal).cod_local Then
                'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(Right(EcNumReq, 7), gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
                sSql = "UPDATE sl_ana_Acrescentadas set cod_local = " & gEstruturaLocais(iLocal).cod_local & " WHERE n_req = " & eTubos(lStructureIndex).requisicao
                sSql = sSql & " AND (flg_eliminada IS NULL or flg_eliminada = 0) and cod_Agrup = " & BL_TrataStringParaBD(eTubos(lStructureIndex).codigo_analise)
                BG_ExecutaQuery_ADO sSql
                eTubos(lStructureIndex).cod_local_ana = gEstruturaLocais(iLocal).cod_local
                'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(EcNumReq, gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
                If RegistaAlteraLocalAnaAcrescentadas(eTubos(lStructureIndex).requisicao, eTubos(lStructureIndex).codigo_analise, eTubos(lStructureIndex).cod_local_ana, gEstruturaLocais(iLocal).cod_local) = False Then
                    GoTo TrataErro
                End If
            End If
        End If
        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(EcNumReq, gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
        sSql = "UPDATE sl_marcacoes SET flg_apar_trans = 0 WHERE n_Req = " & eTubos(lStructureIndex).requisicao
        BG_ExecutaQuery_ADO sSql
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "MudaLocalAnaAcrescentadaTubo: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "MudaLocalAnaAcrescentadaTubo"
    Exit Sub
    Resume Next
End Sub

Public Sub MudaLocalAnaAcrescentadaAnalise(indice As String)
    Dim sSql As String
    Dim lStructureIndex As Long
    Dim bParent As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    
    
    lStructureIndex = RetornaIndiceTubo(LvTubos.SelectedItem.Index, bParent, "")
    If gEstruturaLocais(indice + 1).cod_local <> eTubos(lStructureIndex).cod_local_ana Then
        'BRUNODSANTOS ULSNE-1871
        'Adicionada � condicao gMudaLocalAnaliseTubo
        If gMultiLocalAmbito <> mediSim Or gMudaLocalAnaliseTubo = mediSim Then
            'gMsgMsg = "Deseja alterar o local de todas as an�lises para o local: " & gEstruturaLocais(indice + 1).descr_local & "?"
            gMsgMsg = "Pretende alterar s� o local da an�lise " & eTubos(lStructureIndex).codigo_analise & " para o local: " & gEstruturaLocais(indice + 1).descr_local & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        End If
        
         'BRUNODSANTOS ULSNE-1871
        'Adicionada � condicao gMudaLocalAnaliseTubo
        'If BL_HandleNull(gMsgResp, vbYes) = vbYes Or (gMultiLocalAmbito = mediSim And gMudaLocalAnaliseTubo <> mediSim) Then
        If BL_HandleNull(gMsgResp, vbNo) = vbNo Or (gMultiLocalAmbito = mediSim And gMudaLocalAnaliseTubo <> mediSim) Then
            For i = 1 To totalTubos
                'NELSONPSILVA 24.01.2017 CHVNG-9009
                'If eTubos(i).codigo_analise <> "" And (eTubos(i).codigo_tubo = eTubos(lStructureIndex).codigo_tubo Or (eTubos(i).codigo_analise = eTubos(lStructureIndex).codigo_analise)) Then
                If eTubos(i).codigo_analise <> "" Then
                    If BL_DevolveLocalPretendidoExecAna(eTubos(i).codigo_analise, gEstruturaLocais(indice + 1).cod_local) = gEstruturaLocais(indice + 1).cod_local Then
                        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(Right(EcNumReq, 7), gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
                        sSql = "UPDATE sl_ana_Acrescentadas set cod_local = " & gEstruturaLocais(indice + 1).cod_local & " WHERE n_req = " & eTubos(lStructureIndex).requisicao
                        sSql = sSql & " AND (flg_eliminada IS NULL or flg_eliminada = 0) and cod_Agrup = " & BL_TrataStringParaBD(eTubos(i).codigo_analise)
                        BG_ExecutaQuery_ADO sSql
                        eTubos(i).cod_local_ana = gEstruturaLocais(indice + 1).cod_local
                        LvTubos.ListItems(LvTubos.SelectedItem.Index).SubItems(LvTubosColunas.t_data_prevista) = gEstruturaLocais(indice + 1).descr_local
                        
                        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(EcNumReq, gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
                        If RegistaAlteraLocalAnaAcrescentadas(eTubos(lStructureIndex).requisicao, eTubos(i).codigo_analise, eTubos(i).cod_local_ana, gEstruturaLocais(indice + 1).cod_local) = False Then
                            GoTo TrataErro
                        End If
                    End If
                End If
            Next i
        Else
            If BL_DevolveLocalPretendidoExecAna(eTubos(lStructureIndex).codigo_analise, gEstruturaLocais(indice + 1).cod_local) = gEstruturaLocais(indice + 1).cod_local Then
                'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(Right(EcNumReq, 7), gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
                sSql = "UPDATE sl_ana_Acrescentadas set cod_local = " & gEstruturaLocais(indice + 1).cod_local & " WHERE n_req = " & eTubos(lStructureIndex).requisicao
                sSql = sSql & " AND (flg_eliminada IS NULL or flg_eliminada = 0) and cod_Agrup = " & BL_TrataStringParaBD(eTubos(lStructureIndex).codigo_analise)
                BG_ExecutaQuery_ADO sSql
                eTubos(lStructureIndex).cod_local_ana = gEstruturaLocais(indice + 1).cod_local
                LvTubos.ListItems(LvTubos.SelectedItem.Index).SubItems(LvTubosColunas.t_data_prevista) = gEstruturaLocais(indice + 1).descr_local
                
                'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(EcNumReq, gRequisicaoActiva) para eTubos(lStructureIndex).requisicao
                If RegistaAlteraLocalAnaAcrescentadas(eTubos(lStructureIndex).requisicao, eTubos(lStructureIndex).codigo_analise, eTubos(lStructureIndex).cod_local_ana, gEstruturaLocais(indice + 1).cod_local) = False Then
                    GoTo TrataErro
                End If
            End If
        End If
    End If
    FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "MudaLocalAnaAcrescentadaTubo: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "MudaLocalAnaAcrescentadaTubo"
    Exit Sub
    Resume Next
End Sub


Private Sub VerificaRegrasLocais(cod_ana As String, cod_local As String)
    Dim sSql As String
    Dim ana() As String
    Dim i As Integer
    Dim j As Integer
    Dim RsLocal As New ADODB.recordset
    If cod_local <> "" Then
        sSql = "SELECT * FROM sl_ana_locais_exec WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana) & " AND cod_local = " & cod_local
        RsLocal.CursorType = adOpenStatic
        RsLocal.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsLocal.Open sSql, gConexao
        If RsLocal.RecordCount > 0 Then
            While Not RsLocal.EOF
                If BL_HandleNull(RsLocal!mesmo_local, "") <> "" Then
                    BL_Tokenize RsLocal!mesmo_local, ";", ana
                    For i = 0 To UBound(ana)
                        totalRegras = totalRegras + 1
                        ReDim Preserve estrutRegras(totalRegras)
                        estrutRegras(totalRegras).cod_ana = ana(i)
                        estrutRegras(totalRegras).cod_local = cod_local
                        estrutRegras(totalRegras).cod_obrig = cod_ana
                        For j = 1 To totalTubos
                            If estrutRegras(totalRegras).cod_ana = eTubos(j).codigo_analise Then
                                ActualizaLocalAnaAcrescentadas eTubos(j).codigo_analise, cod_local, cod_ana, j
                                
                            End If
                        Next
                    Next i
                End If
                RsLocal.MoveNext
            Wend
        Else
            'BG_Mensagem mediMsgBox, "An�lise " & BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", cod_ana, "V") & " n�o est� mapeada para o Local", vbExclamation, " Aten��o"
        End If
        RsLocal.Close
        Set RsLocal = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "VerificaRegrasLocais: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "VerificaRegrasLocais"
    Exit Sub
    Resume Next
End Sub

Private Sub ActualizaLocalAnaAcrescentadas(cod_ana As String, cod_local As String, cod_obrig As String, indice As Integer)
    Dim sSql As String
    On Error GoTo TrataErro
    If BL_DevolveLocalPretendidoExecAna(cod_ana, cod_local) = cod_local Then
        BG_Mensagem mediMsgBox, "O local da an�lise " & BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", cod_ana, "V") & " vai ser alterado para o local da an�lise:" & BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", cod_obrig, "V"), vbExclamation, " Aten��o"
        
        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(Right(EcNumReq, 7), gRequisicaoActiva) para eTubos(indice).requisicao
        sSql = "UPDATE sl_ana_Acrescentadas set cod_local = " & cod_local & " WHERE n_req = " & eTubos(indice).requisicao
        sSql = sSql & " AND (flg_eliminada IS NULL or flg_eliminada = 0) and cod_Agrup = " & BL_TrataStringParaBD(cod_ana)
        BG_ExecutaQuery_ADO sSql
        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(EcNumReq, gRequisicaoActiva) para eTubos(indice).requisicao
        sSql = "UPDATE sl_marcacoes SET flg_apar_trans = 0 WHERE n_Req = " & eTubos(indice).requisicao & " AND cod_agrup = " & BL_TrataStringParaBD(cod_ana)
        BG_ExecutaQuery_ADO sSql
        'NELSONPSILVA CHVNG-7461 29.10.2018 Altera��o BL_HandleNull(EcNumReq, gRequisicaoActiva) para eTubos(indice).requisicao
        If RegistaAlteraLocalAnaAcrescentadas(eTubos(indice).requisicao, eTubos(indice).codigo_analise, eTubos(indice).cod_local_ana, cod_local) = False Then
            GoTo TrataErro
        End If

        eTubos(indice).cod_local_ana = cod_local
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "ActualizaLocalAnaAcrescentadas: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "ActualizaLocalAnaAcrescentadas"
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------------------------------

' REGISTA DADOS SOBRE QUEM DESACTIVOU CHEGADA DE TUBO

' --------------------------------------------------------------------------------------------------------------------------------------------
Private Function RegistaDesactivacaoTubo(seq_req_tubo As Long) As Boolean
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    RegistaDesactivacaoTubo = False
        
    sSql = "INSERT INTO sl_req_tubo_desactivo( seq_desactivo, seq_req_tubo, user_cri, dt_cri, hr_cri) VALUES("
    sSql = sSql & " seq_desactivo.nextval, " & seq_req_tubo & "," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <= 0 Then
        GoTo TrataErro
    End If
    
    RegistaDesactivacaoTubo = True
Exit Function
TrataErro:
    RegistaDesactivacaoTubo = False
    BG_LogFile_Erros "RegistaDesactivacaoTubo: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "RegistaDesactivacaoTubo"
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------------------------------------------------------------------------------------------

' REGISTA DADOS SOBRE QUEM ALTEROU LOCAL DA TABELA SL_ANA_aCRESCENTADAS

' --------------------------------------------------------------------------------------------------------------------------------------------
Private Function RegistaAlteraLocalAnaAcrescentadas(n_req As Long, cod_agrup As String, cod_local As String, cod_local_novo As String) As Boolean
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    RegistaAlteraLocalAnaAcrescentadas = False
        
    sSql = "INSERT INTO sl_ana_acrescentadas_modif( seq_modif, n_Req, cod_agrup, cod_local, cod_local_novo, user_cri, dt_cri, hr_cri) VALUES("
    sSql = sSql & " seq_modif.nextval, " & n_req & "," & BL_TrataStringParaBD(CStr(cod_agrup)) & "," & cod_local & "," & cod_local_novo & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <= 0 Then
        GoTo TrataErro
    End If
    
    RegistaAlteraLocalAnaAcrescentadas = True
Exit Function
TrataErro:
    RegistaAlteraLocalAnaAcrescentadas = False
    BG_LogFile_Erros "RegistaAlteraLocalAnaAcrescentadas: " & Err.Number & " - " & Err.Description & " " & sSql, Me.Name, "RegistaAlteraLocalAnaAcrescentadas"
    Exit Function
    Resume Next
End Function



Private Function InsereMovimentoTracking(Index As Long, movimento As String) As Boolean

    Dim sql As String
    On Error GoTo TrataErro
    
    sql = "insert into sl_regista_movimentos (n_req,cod_tubo,movimento,user_cri,dt_cri,cod_local,estado) values(" & _
          eTubos(Index).requisicao & "," & BL_TrataStringParaBD(eTubos(Index).codigo_tubo) & "," & _
          BL_TrataStringParaBD(movimento) & "," & gCodUtilizador
    If gSGBD = gOracle Then
        sql = sql & ",sysdate,"
    ElseIf gSGBD = gPostGres Then
        sql = sql & ",now(),"
    End If
    sql = sql & gCodLocal & "," & _
          BL_TrataStringParaBD(TB_DevolveEstadoTubo(eTubos(Index).estado_tubo)) & ")"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    InsereMovimentoTracking = CBool(gSQLError = Empty)
    
     'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        Dim requestJson As String
        Dim responseJson As String
        Dim CamposEcra() As Object
        Dim NumCampos As Integer
        NumCampos = 4
        ReDim CamposEcra(0 To NumCampos - 1)
         Set CamposEcra(0) = EcNumReq
         Set CamposEcra(1) = EcLocal
         Set CamposEcra(2) = EcUtente
         Set CamposEcra(3) = EcPrescricao
        
        requestJson = "{"
        requestJson = requestJson & Chr(34) & "n_req" & Chr(34) & ":" & Chr(34) & eTubos(Index).requisicao & Chr(34) & ", " & Chr(34) & "cod_tubo" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(eTubos(Index).codigo_tubo) & Chr(34) & _
        ", " & Chr(34) & "movimento" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(movimento) & Chr(34) & ", " & Chr(34) & "user_cri" & Chr(34) & ":" & Chr(34) & gCodUtilizador & Chr(34) & _
        ", " & Chr(34) & "dt_cri" & Chr(34) & ":" & Chr(34) & Bg_DaData_ADO & " " & Bg_DaHora_ADO & Chr(34) & ", " & Chr(34) & "cod_local" & Chr(34) & ":" & Chr(34) & gCodLocal & Chr(34) & _
        ", " & Chr(34) & "estado" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(TB_DevolveEstadoTubo(eTubos(Index).estado_tubo)) & Chr(34) & ""
        
        requestJson = requestJson & "}"
        'requestJson = BL_TrataCamposRGPD(CamposEcra)
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Alteracao) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
    End If
    
Exit Function
TrataErro:
    InsereMovimentoTracking = False
    BG_LogFile_Erros "InsereMovimentoTracking: " & Err.Number & " - " & Err.Description & " " & sql, Me.Name, "InsereMovimentoTracking"
    Exit Function
    Resume Next
End Function

'RGONCALVES 16.07.2013 CHVNG-4306
Private Function Inibir_Impressao_Tubo_Sec(ByVal tubo_sec As String) As Boolean
    Dim sql As String
    Dim rs As ADODB.recordset
    
    On Error GoTo TrataErro
    
    tubo_sec = Trim(tubo_sec)
    If tubo_sec = "" Then
        Inibir_Impressao_Tubo_Sec = False
        Exit Function
    End If
     
    sql = "SELECT * FROM sl_tubo_sec WHERE cod_tubo = " & BL_TrataStringParaBD(tubo_sec)
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount = 1 Then
        If CStr(BL_HandleNull(rs!flg_inibir_impressao, "0")) = "1" Then
            Inibir_Impressao_Tubo_Sec = True
        Else
            Inibir_Impressao_Tubo_Sec = False
        End If
    Else
        Inibir_Impressao_Tubo_Sec = False
    End If
    
    rs.Close
    Set rs = Nothing
    
Exit Function
TrataErro:
    Inibir_Impressao_Tubo_Sec = False
    BG_LogFile_Erros "Inibir_Impressao_Tubo_Sec: " & Err.Number & " - " & Err.Description & " " & sql, Me.Name, "Inibir_Impressao_Tubo_Sec"
    Exit Function
    Resume Next
End Function



Public Sub RemarcaTubo()
    Dim bParent As Boolean
    Dim lStructureIndex As Integer
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    
    lStructureIndex = RetornaIndiceTubo(LvTubos.SelectedItem.Index, bParent)
    Select Case (eTubos(lStructureIndex).estado_tubo)
        Case gEstadosTubo.chegado, gEstadosTubo.cancelado, gEstadosTubo.colhido:
            BG_Mensagem mediMsgBox, "Tubo n�o pode ser alterado. ", vbExclamation, " Aten��o"
            Exit Sub
    End Select
    sSql = "SELECT * FROM sl_realiza WHERE seq_req_tubo = " & eTubos(lStructureIndex).seq_req_tubo

    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount >= 1 Then
        BG_Mensagem mediMsgBox, "Tubo j� tem resultados e n�o pode ser alterado.", vbExclamation, " Aten��o"
        Exit Sub
    End If
    rsTubo.Close
    Set rsTubo = Nothing
    
    gMsgMsg = "Tem a certeza que pretende remarcar outro tubo " & eTubos(lStructureIndex).descricao_tubo & "?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbYes) Then: Call ControlaFrames(FrMotivo, True)
    TipoMotivo = Motivos.RemarcacaoTubo
    Exit Sub
End Sub

Private Function ExecutaRemarcacaoTubo(Index As Integer, codigo_motivo As String) As Boolean
    Dim lStructureIndex As Integer
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim codAna() As String
    Dim TotalElementosSel As Integer
    Dim seq_req_tubo_old As Long
    Dim seq_Req_tubo_new As Long
    Dim sSql As String
    On Error GoTo TrataErro

    PesqRapida = False
    
    ChavesPesq(1) = "cod_tubo"
    CamposEcran(1) = "cod_tubo"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_tubo"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tubo "
    CWhere = ""
    CampoPesquisa = "descr_tubo"
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_tubo ", _
                                                                           " Tubos")

    FormPesqRapidaAvancada.Show vbModal
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
    If Not CancelouPesquisa Then
        
        lStructureIndex = RetornaIndiceTubo(CLng(Index))
        eTubos(lStructureIndex).motivo_eliminacao = codigo_motivo
        seq_req_tubo_old = eTubos(lStructureIndex).seq_req_tubo
        If (Not CancelaChegadaTubo(CLng(lStructureIndex), codigo_motivo)) Then Exit Function
        seq_Req_tubo_new = TB_InsereTuboBD(mediComboValorNull, CStr(eTubos(lStructureIndex).requisicao), Resultados(1), _
                                    eTubos(lStructureIndex).data_prevista, "", "", _
                                    "", "", "", codAna, gEstadosTubo.Pendente, mediComboValorNull, "")
        If seq_Req_tubo_new > mediComboValorNull Then
            ExecutaRemarcacaoTubo = True
            sSql = "UPDATE sl_marcacoes SET seq_req_tubo = " & seq_Req_tubo_new & " WHERE seq_req_tubo = " & seq_req_tubo_old
            BG_ExecutaQuery_ADO sSql
            RefrescaLista
        Else
            ExecutaRemarcacaoTubo = False
        End If
    End If
    Exit Function
    
Exit Function
TrataErro:
    ExecutaRemarcacaoTubo = False
    BG_LogFile_Erros "ExecutaRemarcacaoTubo: " & Err.Number & " - " & Err.Description, Me.Name, "ExecutaRemarcacaoTubo"
    Exit Function
    Resume Next
End Function


Private Sub LimpaReqAtual()
    reqAtual.cod_proven = ""
    reqAtual.descr_proven = ""
    reqAtual.dt_chega = ""
    reqAtual.dt_nasc_ute = ""
    reqAtual.dt_chega = ""
    reqAtual.dt_nasc_ute = ""
    reqAtual.dt_previ = ""
    reqAtual.n_proc_1 = ""
    reqAtual.n_proc_2 = ""
    reqAtual.n_req = ""
    reqAtual.nome_ute = ""
    reqAtual.t_urg = ""
    reqAtual.t_utente = ""
    reqAtual.Utente = ""
End Sub


Private Function VerificaInfoTubos(seq_req_tubo As Long) As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    
    sSql = "SELECT * from sl_ana_informacao where cod_ana in (Select cod_ana_s from sl_marcacoes where seq_req_tubo = " & seq_req_tubo
    sSql = sSql & " union select cod_ana_s from sl_realiza where seq_req_tubo = " & seq_req_tubo & ") AND cod_informacao IN ("
    sSql = sSql & " SELECT cod_informacao FROM sl_informacao where cod_t_informacao = 3)"
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount >= 1 Then
        VerificaInfoTubos = 1
    Else
        VerificaInfoTubos = 0
    End If
    rsTubo.Close
    Set rsTubo = Nothing
Exit Function
TrataErro:
    VerificaInfoTubos = 0
    BG_LogFile_Erros "VerificaInfoTubos: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaInfoTubos"
    Exit Function
    Resume Next
End Function

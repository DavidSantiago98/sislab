VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormChegadaPorTubos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   7965
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14100
   Icon            =   "FormChegadaPorTubos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   14100
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   1680
      TabIndex        =   45
      Top             =   6880
      Width           =   615
   End
   Begin VB.Frame FrameTubosFalta 
      Caption         =   "Tubos em Falta"
      Height          =   6015
      Left            =   120
      TabIndex        =   19
      Top             =   0
      Visible         =   0   'False
      Width           =   10815
      Begin MSFlexGridLib.MSFlexGrid FgTubos2 
         Height          =   3435
         Left            =   120
         TabIndex        =   32
         Top             =   1440
         Width           =   10620
         _ExtentX        =   18733
         _ExtentY        =   6059
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton BtActualizar 
         Caption         =   "Actualizar"
         Height          =   495
         Left            =   9480
         Picture         =   "FormChegadaPorTubos.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   31
         ToolTipText     =   "Actualizar a Pesquisa"
         Top             =   840
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker EcDtReq 
         Height          =   255
         Left            =   6720
         TabIndex        =   30
         Top             =   360
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   343539713
         CurrentDate     =   38998
      End
      Begin VB.CommandButton BtSair 
         Caption         =   "Voltar"
         Height          =   495
         Left            =   9480
         Picture         =   "FormChegadaPorTubos.frx":685E
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Voltar � Entrada de tubos"
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton BtPesquisaRapidaTubo 
         Height          =   375
         Left            =   4440
         Picture         =   "FormChegadaPorTubos.frx":D0B0
         Style           =   1  'Graphical
         TabIndex        =   28
         ToolTipText     =   "Pesquisa R�pida de Grupos de Trabalho"
         Top             =   840
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
         Height          =   375
         Left            =   4440
         Picture         =   "FormChegadaPorTubos.frx":D63A
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcDescrTubo 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   26
         Top             =   840
         Width           =   2415
      End
      Begin VB.TextBox EcCodTubo 
         Height          =   285
         Left            =   1560
         TabIndex        =   25
         Top             =   840
         Width           =   495
      End
      Begin VB.TextBox EcDescrGrAna 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   23
         Top             =   360
         Width           =   2415
      End
      Begin VB.TextBox EcCodGrAna 
         Height          =   285
         Left            =   1560
         TabIndex        =   22
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Tubo"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   24
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo An�lises"
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   21
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Data da Requisi��o"
         Height          =   255
         Index           =   0
         Left            =   5280
         TabIndex        =   20
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.CommandButton BtEtiq 
      Height          =   615
      Left            =   9600
      Picture         =   "FormChegadaPorTubos.frx":DBC4
      Style           =   1  'Graphical
      TabIndex        =   44
      ToolTipText     =   "Impress�o Etiquetas"
      Top             =   600
      Width           =   1215
   End
   Begin VB.CommandButton Command0 
      BackColor       =   &H00FFFFFF&
      Caption         =   "NAO APAGAR"
      Height          =   195
      Index           =   0
      Left            =   480
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   6600
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Frame FrAna 
      Height          =   3615
      Left            =   120
      TabIndex        =   40
      Top             =   2400
      Width           =   10815
      Begin VB.CommandButton BtFecharFrame 
         Caption         =   "Voltar"
         Height          =   495
         Left            =   9480
         Picture         =   "FormChegadaPorTubos.frx":E32E
         Style           =   1  'Graphical
         TabIndex        =   42
         ToolTipText     =   "Voltar � Entrada de tubos"
         Top             =   240
         Width           =   1215
      End
      Begin MSFlexGridLib.MSFlexGrid FgAna 
         Height          =   3435
         Left            =   120
         TabIndex        =   41
         Top             =   120
         Width           =   9180
         _ExtentX        =   16193
         _ExtentY        =   6059
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.TextBox EcNumReqAssoc 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1560
      MaxLength       =   9
      TabIndex        =   33
      Top             =   480
      Width           =   975
   End
   Begin VB.CommandButton BtFaltaTubos 
      Caption         =   "Tubos em Falta"
      Height          =   495
      Left            =   9600
      Picture         =   "FormChegadaPorTubos.frx":14B80
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "Listagem Tubos em Falta"
      Top             =   70
      Width           =   1215
   End
   Begin VB.TextBox EcEstado 
      Alignment       =   2  'Center
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   2520
      TabIndex        =   17
      Top             =   120
      Width           =   3255
   End
   Begin VB.Frame FrTubos 
      Height          =   3615
      Left            =   120
      TabIndex        =   15
      Top             =   2400
      Width           =   10815
      Begin MSFlexGridLib.MSFlexGrid FGTubos 
         Height          =   3435
         Left            =   120
         TabIndex        =   16
         Top             =   120
         Width           =   10620
         _ExtentX        =   18733
         _ExtentY        =   6059
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.TextBox EcNumReq 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1560
      MaxLength       =   9
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Caption         =   "Dados do Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   120
      TabIndex        =   1
      Top             =   1320
      Width           =   10815
      Begin VB.TextBox EcProcHosp1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6720
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Data de Entrada"
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox EcNome 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox EcUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   240
         Width           =   3015
      End
      Begin VB.TextBox EcNumCartaoUte 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8640
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox EcProcHosp2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8400
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6720
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   1215
      End
      Begin VB.ComboBox CbTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   13
         Top             =   600
         Width           =   405
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   12
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Processo "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5760
         TabIndex        =   11
         Top             =   240
         Width           =   735
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5760
         TabIndex        =   10
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   795
      End
      Begin VB.Label LbReqAssoci 
         AutoSize        =   -1  'True
         Caption         =   "Cart�o "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8040
         TabIndex        =   9
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   525
      End
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Left            =   6000
      TabIndex        =   35
      Top             =   0
      Width           =   3495
      Begin VB.OptionButton CbTipoPesquisa 
         Caption         =   "Utilizando N�mero Requisi��o"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   37
         Top             =   180
         Width           =   2775
      End
      Begin VB.OptionButton CbTipoPesquisa 
         Caption         =   "Utilizando N�mero Requisi��o Associado"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   36
         Top             =   600
         Width           =   3255
      End
   End
   Begin VB.OptionButton Opt 
      Caption         =   "Entada de tubos"
      Height          =   195
      Index           =   0
      Left            =   1560
      TabIndex        =   38
      Top             =   960
      Width           =   1695
   End
   Begin VB.OptionButton Opt 
      Caption         =   "Sa�da de tubos"
      Height          =   195
      Index           =   1
      Left            =   4200
      TabIndex        =   39
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Req Associada"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Index           =   1
      Left            =   240
      TabIndex        =   34
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Requisi��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Index           =   0
      Left            =   240
      TabIndex        =   14
      Top             =   120
      Width           =   900
   End
End
Attribute VB_Name = "FormChegadaPorTubos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

' Actualiza��o : / /
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer
Dim CamposBDparaListBox
Dim NumEspacos
Dim CampoDeFocus As Object

'Flag que evita os beep dos enters nas textbox
Dim Enter As Boolean

Public RegistosT As New Collection

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdReq As New ADODB.Command

Dim CodTubo As String
Dim CodProd As String

Const Entrada = 0
Const saida = 1
Dim lTipoPesquisa As Integer
Private Type tuboSec
    cod_tubo_sec As String
    etiq_tubo_sec As String
    cod_tubo_prim As String
    descr_tubo_sec As String
    n_req As String
    t_utente As String
    Utente As String
    nome_ute As String
    dt_nasc_ute As String
    Copias As Integer
    abr_ana As String
End Type
Dim estrutTubosSec() As tuboSec
Dim totalTubosSec As Integer

Private Type analises
    cod_ana As String
    descr_ana As String
End Type
Dim estrutAnalises() As analises
Dim totalAnalises As Long
Dim FlgMensagem As Boolean
Dim flg_ReqBloqueada As Boolean

Private Sub BtActualizar_Click()
    FaltaTubos
End Sub

Private Sub BtEtiq_Click()
    If gRequisicaoActiva <> -1 Then
        If IsNumeric(gRequisicaoActiva) Then
            'Limpar parametros
                BG_LimpaPassaParams
            'Passar parametros necessarios o ao form de impress�o
                gPassaParams.Param(0) = gRequisicaoActiva
            
            'Abrir form de impress�o
            FormNEtiqNTubos.Show
            DoEvents
        'FormGestaoRequisicaoPrivado.SetFocus
        End If
    End If


End Sub

Private Sub CbTipoPesquisa_Click(Index As Integer)
    Select Case Index
        Case 0
            CbTipoPesquisa(0).value = True
            EcNumReq.Locked = False
            EcNumReqAssoc.Locked = True
        Case 1
            CbTipoPesquisa(1).value = True
            EcNumReq.Locked = True
            EcNumReqAssoc.Locked = False
        EcNumReqAssoc.SetFocus
    End Select

End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim CodEtiqTubo As String
    Dim RsDescrT As ADODB.recordset
    Dim sql As String
    Dim rsReq As ADODB.recordset
    
    Enter = False
    
    If EcNumReq.Text <> "" Then
        If (KeyCode = 13 And Shift = 0) Then
            Enter = True
            CodTubo = ""
            CodProd = ""
            If BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
                If Len(EcNumReq.Text) = 9 Then
                        gRequisicaoActiva = Right(EcNumReq.Text, 7)
                        CmdReq.Parameters(0).value = gRequisicaoActiva
                        Set rsReq = CmdReq.Execute
                        If Not rsReq.EOF Then
                            
                            CodEtiqTubo = Mid(EcNumReq.Text, 1, 2)
                            
                            PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
                            FuncaoProcurar_PreencheTubo (gRequisicaoActiva)
                            If lTipoPesquisa = Entrada Then
                                If Da_ChegadaTubo(CodEtiqTubo, gRequisicaoActiva) = False Then
                                    
                                    Set RsDescrT = New ADODB.recordset
                                    RsDescrT.CursorLocation = adUseServer
                                    RsDescrT.CursorType = adOpenStatic
                                    sql = "select descr_tubo from sl_tubo where  cod_etiq = '" & CodEtiqTubo & "'"
                                    RsDescrT.Open sql, gConexao
                                    If RsDescrT.RecordCount > 0 Then
                                        BG_Mensagem mediMsgBox, "O Tubo " & BL_HandleNull(RsDescrT!descR_tubo, "") & " da requisi��o " & gRequisicaoActiva & " j� deu entrada anteriormente!"
                                    End If
                                    RsDescrT.Close
                                    Set RsDescrT = Nothing
                                End If
                            ElseIf lTipoPesquisa = saida Then
                                If Da_SaidaTubo(CodEtiqTubo, gRequisicaoActiva) = False Then
                                    
                                    Set RsDescrT = New ADODB.recordset
                                    RsDescrT.CursorLocation = adUseServer
                                    RsDescrT.CursorType = adOpenStatic
                                    sql = "select descr_tubo from sl_tubo where  cod_etiq = '" & CodEtiqTubo & "'"
                                    RsDescrT.Open sql, gConexao
                                    If RsDescrT.RecordCount > 0 Then
                                        BG_Mensagem mediMsgBox, "O Tubo " & BL_HandleNull(RsDescrT!descR_tubo, "") & " da requisi��o " & gRequisicaoActiva & " j� deu sa�da anteriormente!"
                                    End If
                                    RsDescrT.Close
                                    Set RsDescrT = Nothing
                                End If
                            End If
                            PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
                            FuncaoProcurar_PreencheTubo (gRequisicaoActiva)
                            Preenche_Estado (BL_HandleNull(rsReq!estado_req, ""))
                        Else
                            BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Chegada de Tubos"
                            LimpaCampos
                        End If
                        rsReq.Close
                        EcNumReq.SetFocus
                        Sendkeys ("{HOME}+{END}")
                
                ElseIf Len(EcNumReq.Text) = 8 Then
                        gRequisicaoActiva = Right(EcNumReq.Text, 7)
                        
                        CmdReq.Parameters(0).value = gRequisicaoActiva
                        Set rsReq = CmdReq.Execute
                        If Not rsReq.EOF Then
                            
                            CodEtiqTubo = CInt(Mid(EcNumReq.Text, 1, 1))
                            
                            If Da_ChegadaTubo(CodEtiqTubo, gRequisicaoActiva) = False Then
                                
                                Set RsDescrT = New ADODB.recordset
                                RsDescrT.CursorLocation = adUseServer
                                RsDescrT.CursorType = adOpenStatic
                                sql = "select descr_tubo from sl_tubo where cod_etiq = '" & CodEtiqTubo & "'"
                                RsDescrT.Open sql, gConexao
                                If RsDescrT.RecordCount > 0 Then
                                    BG_Mensagem mediMsgBox, "O Tubo " & BL_HandleNull(RsDescrT!descR_tubo, "") & " da requisi��o " & gRequisicaoActiva & " j� deu entrada enteriormente!"
                                End If
                                RsDescrT.Close
                                Set RsDescrT = Nothing
                            End If
                            
                            PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
                            FuncaoProcurar_PreencheTubo (gRequisicaoActiva)
                            Preenche_Estado (BL_HandleNull(rsReq!estado_req, ""))
                        Else
                            BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Chegada de Tubos"
                            LimpaCampos
                        End If
                        rsReq.Close
                        EcNumReq.SetFocus
                        Sendkeys ("{HOME}+{END}")
                Else
                    gRequisicaoActiva = EcNumReq.Text
                    CmdReq.Parameters(0).value = gRequisicaoActiva
                    Set rsReq = CmdReq.Execute
                    If Not rsReq.EOF Then
                        ' se o codigo recebido n tiver 9 digitos n � imposs�vel dar entrada do tubo
                        ' BG_Mensagem mediMsgBox, "Tubo n�o identificado!" & vbCrLf & "N�o � poss�vel dar chegada do Tubo da requisi��o " & gRequisicaoActiva, vbInformation, "Chegada de Tubos"
                        
                        PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
                        FuncaoProcurar_PreencheTubo (gRequisicaoActiva)
                        Preenche_Estado (BL_HandleNull(rsReq!estado_req, ""))
                    Else
                        BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Chegada de Tubos"
                        LimpaCampos
                    End If
                    rsReq.Close
                    EcNumReq.Text = ""
                    EcNumReq.SetFocus
                End If
            Else
                Sendkeys ("{HOME}+{END}")
                EcNumReq.SetFocus
            End If
        End If
    End If
 
End Sub

Private Sub EcEtiqTubo_KeyPress(KeyAscii As Integer)
    
    If Enter = True Then KeyAscii = 0

End Sub



Private Sub FGTubos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        If FGTubos.TextMatrix(FGTubos.row, 7) = "Entrou" Then
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_TUBOS
        End If
    End If
End Sub


' -------------------------------------------------------------------------------

' DAR ENTRADA DOS TUBOS ATRAVES DE CLICK

' -------------------------------------------------------------------------------
Private Sub FGTubos_click()
    Dim tubo As String
    Dim i As Integer
    Dim tamanho As Integer
    Dim rsReq As ADODB.recordset
    Dim RsDescrT As ADODB.recordset
    Dim sql As String
    Dim CodEtiqTubo As String
    
    ' -------------------------------------------------------------------------------
    ' APENAS SE A LINHA FOR > 0 E O TUBO ESTIVER PREENCHIDO
    ' -------------------------------------------------------------------------------
    If FGTubos.row = 0 Then
        Exit Sub
    End If
    If Trim(FGTubos.TextMatrix(FGTubos.row, 0)) = "" Then
        Exit Sub
    End If
    If lTipoPesquisa = Entrada Then
        If RegistosT(FGTubos.row).EstadoTubo = "Espera" Or RegistosT(FGTubos.row).EstadoTubo = "Falta" Then
            tubo = RegistosT(FGTubos.row).CodTubo
            CodEtiqTubo = RegistosT(FGTubos.row).CodEtiqTubo
            
            gRequisicaoActiva = Trim(FGTubos.TextMatrix(FGTubos.row, 0))
                            
            CmdReq.Parameters(0).value = gRequisicaoActiva
            Set rsReq = CmdReq.Execute
            If Not rsReq.EOF Then
                
                If lTipoPesquisa = Entrada Then
                    If Da_ChegadaTubo("", gRequisicaoActiva, tubo) = False Then
                        
                        Set RsDescrT = New ADODB.recordset
                        RsDescrT.CursorLocation = adUseServer
                        RsDescrT.CursorType = adOpenStatic
                        sql = "select descr_tubo from sl_tubo where cod_tubo = '" & tubo & "'"
                        RsDescrT.Open sql, gConexao
                        If RsDescrT.RecordCount > 0 Then
                            BG_Mensagem mediMsgBox, "O Tubo " & BL_HandleNull(RsDescrT!descR_tubo, "") & " da requisi��o " & gRequisicaoActiva & " j� deu entrada enteriormente!"
                        End If
                        RsDescrT.Close
                        Set RsDescrT = Nothing
                    End If
                End If
                
                FuncaoProcurar_PreencheTubo (gRequisicaoActiva)
                Preenche_Estado (BL_HandleNull(rsReq!estado_req, ""))
                PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
            Else
                BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Chegada de Tubos"
                LimpaCampos
            End If
            rsReq.Close
            EcNumReq.SetFocus
            Sendkeys ("{HOME}+{END}")
        End If
    ElseIf RegistosT(FGTubos.row).DtSaida = "" And lTipoPesquisa = saida Then
        tubo = RegistosT(FGTubos.row).CodTubo
        CodEtiqTubo = RegistosT(FGTubos.row).CodEtiqTubo
        
        gRequisicaoActiva = Trim(FGTubos.TextMatrix(FGTubos.row, 0))
                        
        CmdReq.Parameters(0).value = gRequisicaoActiva
        Set rsReq = CmdReq.Execute
        If Not rsReq.EOF Then
            
            If Da_SaidaTubo("", gRequisicaoActiva, tubo) = False Then
                
                Set RsDescrT = New ADODB.recordset
                RsDescrT.CursorLocation = adUseServer
                RsDescrT.CursorType = adOpenStatic
                sql = "select descr_tubo from sl_tubo where cod_tubo = '" & tubo & "'"
                RsDescrT.Open sql, gConexao
                If RsDescrT.RecordCount > 0 Then
                    BG_Mensagem mediMsgBox, "O Tubo " & BL_HandleNull(RsDescrT!descR_tubo, "") & " da requisi��o " & gRequisicaoActiva & " j� deu sa�da enteriormente!"
                End If
                RsDescrT.Close
                Set RsDescrT = Nothing
            End If
            
            FuncaoProcurar_PreencheTubo (gRequisicaoActiva)
            Preenche_Estado (BL_HandleNull(rsReq!estado_req, ""))
            PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
        Else
            BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Chegada de Tubos"
            LimpaCampos
        End If
        rsReq.Close
        'EcNumReq.text = ""
        EcNumReq.SetFocus
        Sendkeys ("{HOME}+{END}")
        
    End If
End Sub

Private Sub FGTubos_RowColChange()

    FGTubos.SelectionMode = flexSelectionByRow
    
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    If gF_REQPENDELECT = 1 Then
        FormReqPedElectr.Enabled = True
        Set gFormActivo = FormReqPedElectr
    Else
        Set gFormActivo = MDIFormInicio
    End If
    BL_ToolbarEstadoN 0
    
    Set FormChegadaPorTubos = Nothing

End Sub

Sub DefTipoCampos()
With FGTubos
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .TextMatrix(0, 0) = "Requis."
        .ColWidth(1) = 900
        .Col = 1
        .TextMatrix(0, 1) = "Tubo"
        .ColWidth(2) = 4800
        .Col = 2
        .TextMatrix(0, 2) = "Descri��o"
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(3) = 950
        .Col = 3
        .TextMatrix(0, 3) = "Previsto"
        .ColWidth(4) = 950
        .Col = 4
        .TextMatrix(0, 4) = "Data"
        .ColWidth(5) = 600
        .Col = 5
        .TextMatrix(0, 5) = "Hora"
        .ColWidth(6) = 900
        .Col = 6
        .TextMatrix(0, 6) = "Utiliz."
        .ColWidth(7) = 600
        .Col = 7
        .TextMatrix(0, 7) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    
With FgTubos2
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .TextMatrix(0, 0) = "N.Req"
        .ColWidth(1) = 900
        .Col = 1
        .TextMatrix(0, 1) = "Tubo"
        .ColWidth(2) = 4800
        .Col = 2
        .TextMatrix(0, 2) = "Descri��o"
        .ColWidth(3) = 950
        .Col = 3
        .TextMatrix(0, 3) = "Previsto"
        .ColWidth(4) = 950
        .Col = 4
        .TextMatrix(0, 4) = "Chegada"
        .ColWidth(5) = 950
        .Col = 5
        .TextMatrix(0, 5) = "Hr.Chegada"
        .ColWidth(6) = 900
        .Col = 6
        .TextMatrix(0, 6) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    
    'INDICA SE USA SAIDA DE TUBOS
    If gUsaSaidaTubos <> mediSim Then
        Opt(0).Visible = False
        Opt(1).Visible = False
    End If
    
    With FgAna
        .Width = 8300
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .Col = 0
        .ColWidth(0) = 1000
        .TextMatrix(0, 0) = "Codigo"
        .Col = 1
        .ColWidth(1) = 7000
        .TextMatrix(0, 1) = "Descricao"
        .ColAlignment(0) = flexAlignLeftCenter
        .ColAlignment(1) = flexAlignLeftCenter
    End With
    FrAna.Visible = False
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    EcDtReq.value = Bg_DaData_ADO
    lTipoPesquisa = Entrada
    If gUsaSaidaTubos = mediSim Then
        Opt(0).value = True
    End If
End Sub

Sub Inicializacoes()
    
    Me.caption = " Chegada de Tubos"
    Me.left = 440
    Me.top = 20
    Me.Width = 11130
    Me.Height = 6540 ' Normal
    'Me.Height = 5505 ' Campos Extras
    
    EcNumReq.Tag = adVarChar
    Set CampoDeFocus = EcNumReq
    EcEstado.Locked = True
    
    
    'Inicializa o comando ADO para selec��o do estado do utente da requisi��o
    Set CmdReq.ActiveConnection = gConexao
    CmdReq.CommandType = adCmdText
    CmdReq.CommandText = "SELECT estado_req, seq_utente FROM sl_requis WHERE estado_req not in (" & BL_TrataStringParaBD(gEstadoReqBloqueada)
    CmdReq.CommandText = CmdReq.CommandText & "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ") AND n_req =?"
    CmdReq.Parameters.Append CmdReq.CreateParameter("N_REQ", adInteger, adParamInput, 20)
    CmdReq.Prepared = True
    
    MDIFormInicio.IDM_TUBO(1).Visible = False
    FrameTubosFalta.top = 0
    FrameTubosFalta.left = 120
    LimpaCampos
    EcPrinterEtiq.Text = BL_SelImpressora("Etiqueta.rpt")
End Sub

Sub FuncaoProcurar()
    Dim rsReq As ADODB.recordset
    FlgMensagem = True
    If gRequisicaoActiva <> 0 And EcNumReq.Text <> "" Then
        EcNumReq = gRequisicaoActiva
    End If
    If EcNumReq.Text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
            gRequisicaoActiva = CLng(EcNumReq.Text)
            CmdReq.Parameters(0).value = gRequisicaoActiva
            Set rsReq = CmdReq.Execute
            If Not rsReq.EOF Then


            PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
                    FuncaoProcurar_PreencheTubo (gRequisicaoActiva)
                Preenche_Estado (BL_HandleNull(rsReq!estado_req, ""))
            Else
                BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Chegada de Tubos"
                LimpaCampos
                EcNumReq.SetFocus
            End If
        Else
            Sendkeys ("{HOME}+{END}")
            EcNumReq.SetFocus
        End If
    End If
    ConstroiBotoesDinamicos
End Sub

Sub LimpaCampos()
    FlgMensagem = True
    EcNumReq.Text = ""
    EcNumReqAssoc.Text = ""
    EcEstado.Text = ""
    LimpaFgTubos
    LimpaFgTubos2
    
    EcProcHosp1.Text = ""
    EcProcHosp2.Text = ""
    EcNumCartaoUte.Text = ""
    EcDataNasc.Text = ""
    EcNome.Text = ""
    EcUtente.Text = ""
    CbTipoUtente.ListIndex = mediComboValorNull

    CodTubo = ""
    CodProd = ""
    FrameTubosFalta.Visible = False
    CbTipoPesquisa_Click (0)
    gRequisicaoActiva = 0
    
    FrAna.Visible = False
    DestroiBotoesDinamicos
    totalTubosSec = 0
    ReDim estrutTubosSec(0)
    
End Sub

Sub Preenche_Estado(EstadoReq As String)
    EcEstado = BL_DevolveEstadoReq(EstadoReq)
End Sub

Sub FuncaoLimpar()
    
    LimpaCampos
    EcNumReq.SetFocus
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Function FuncaoProcurar_PreencheTubo(NumReq As Long)
    
    ' Selecciona todos os tubos da requisi��o e preenche a lista

    Dim i As Long
    Dim rsDescr As ADODB.recordset
    Dim rsTubo As ADODB.recordset
    Dim CmdDescrT As New ADODB.Command
      
    Set rsTubo = New ADODB.recordset
    With rsTubo
        .Source = "SELECT cod_tubo, dt_previ, dt_chega, hr_chega, dt_eliminacao, user_chega, local_chega,  " & _
                   " dt_saida, hr_saida,  user_saida, local_saida " & _
                   " FROM sl_req_tubo " & _
                   " WHERE n_req =" & gRequisicaoActiva
                   
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        .Open
    End With

    'Inicializa o comando ADO para selec��o da descri��o do tubo
    Set CmdDescrT.ActiveConnection = gConexao
    CmdDescrT.CommandType = adCmdText
    CmdDescrT.CommandText = "SELECT descr_tubo, cod_etiq FROM sl_tubo WHERE " & _
                            "cod_tubo =?"
    CmdDescrT.Prepared = True
     
    i = 1
    LimpaFgTubos
    totalTubosSec = 0
    ReDim estrutTubosSec(0)
    While Not rsTubo.EOF
        
        RegistosT(i).CodTubo = rsTubo!cod_tubo
        CmdDescrT.Parameters(0).value = Trim(UCase(rsTubo!cod_tubo))
        Set rsDescr = CmdDescrT.Execute
        RegistosT(i).descrTubo = BL_HandleNull(rsDescr!descR_tubo, "")
        RegistosT(i).CodEtiqTubo = BL_HandleNull(rsDescr!cod_etiq, "")
        rsDescr.Close
        
        RegistosT(i).DtPrev = BL_HandleNull(rsTubo!dt_previ, "")
        RegistosT(i).DtChega = BL_HandleNull(rsTubo!dt_chega, "")
        RegistosT(i).HrChega = BL_HandleNull(rsTubo!hr_chega, "")
        RegistosT(i).UserChega = BL_HandleNull(rsTubo!user_chega, "")
        RegistosT(i).LocalChega = BL_HandleNull(rsTubo!local_chega, "")
        
        RegistosT(i).DtSaida = BL_HandleNull(rsTubo!dt_saida, "")
        RegistosT(i).HrSaida = BL_HandleNull(rsTubo!hr_saida, "")
        RegistosT(i).UserSaida = BL_HandleNull(rsTubo!user_saida, "")
        RegistosT(i).LocalSaida = BL_HandleNull(rsTubo!local_saida, "")
        
        RegistosT(i).DtElim = BL_HandleNull(rsTubo!dt_eliminacao, "")
        RegistosT(i).EstadoTubo = BL_Coloca_Estado(RegistosT(i).DtPrev, RegistosT(i).DtChega, RegistosT(i).DtElim)
        
        If RegistosT(i).EstadoTubo = "Entrou" Then
            'Verde
            RegistosT(i).Cor_Estado = &H8000&
        ElseIf RegistosT(i).EstadoTubo = "Espera" Then
            'Laranja
            RegistosT(i).Cor_Estado = &HC0&
        ElseIf RegistosT(i).EstadoTubo = "Falta" Then
            'Vermelho
            RegistosT(i).Cor_Estado = &HC0&
        ElseIf RegistosT(i).EstadoTubo = "Cancelado" Then
            'Cinzento
            RegistosT(i).Cor_Estado = &H808080
        End If
        
        PreencheTubosSec RegistosT(i).CodTubo
        
        rsTubo.MoveNext
        i = i + 1
        CriaNovaClasse RegistosT, i, "TUBO"
        FGTubos.AddItem "", i
        FGTubos.row = i
     Wend
     PreencheFGTubo NumReq
     Set CmdDescrT = Nothing
     Set rsDescr = Nothing
     Set rsTubo = Nothing
    ConstroiBotoesDinamicos
    
End Function

Sub PreencheDadosUtente(SeqUtente As String)
    
    Dim Campos(7) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "n_proc_1"
    Campos(4) = "n_proc_2"
    Campos(5) = "n_cartao_ute"
    Campos(6) = "dt_nasc_ute"

    iret = BL_DaDadosUtente(Campos, retorno, SeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.Text = ""
        Exit Sub
    End If
    
    EcUtente.Text = retorno(0)
    CbTipoUtente.Text = retorno(1)
    EcNome.Text = retorno(2)
    EcProcHosp1.Text = retorno(3)
    EcProcHosp2.Text = retorno(4)
    EcNumCartaoUte.Text = retorno(5)
    EcDataNasc.Text = retorno(6)
    
End Sub

Sub LimpaFgTubos()
    
    Dim j As Long
    Dim i As Long
    j = FGTubos.rows - 1
    While j > 0
        If j > 1 Then
            FGTubos.RemoveItem j
        Else
             For i = 0 To FGTubos.Cols - 1
                FGTubos.TextMatrix(j, i) = ""
            Next
        End If
        
        j = j - 1
    Wend
    
    CriaNovaClasse RegistosT, 1, "TUBO", True

End Sub

Function Coloca_Estado(DtPrev, DtChega) As String
    
    If DateValue(DtPrev) < DateValue(Bg_DaData_ADO) And DtChega = "" Then
        Coloca_Estado = "Falta"
    ElseIf DateValue(DtPrev) >= DateValue(Bg_DaData_ADO) And DtChega = "" Then
        Coloca_Estado = "Espera"
    ElseIf DtChega <> "" Then
        Coloca_Estado = "Entrou"
    Else
        Coloca_Estado = ""
    End If

End Function

Private Sub CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)

    ' Tipo = TUBO
    ' EmCascata = True : Cria todos elementos at� o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRegTubo As New ClRegTubos
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "TUBO" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegTubo
            Next i
        Else
            Coleccao.Add AuxRegTubo
        End If
    End If
    
    Set AuxRegTubo = Nothing
    
End Sub

Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)

    ' Index = -1 : Apaga todos elementos
    
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        Coleccao.Remove Index
    End If

End Sub

Function Da_ChegadaTubo(CodEtiqTubo As String, NumReq As Long, Optional CodTubo As String) As Boolean
    Dim sql As String
    'Dim CodTubo As String
    Dim DtChega As String
    Dim HrChega As String
    Dim rsAna As ADODB.recordset
    Dim tubo As String
    Dim UserChegada As String
    
    On Error GoTo TrataErro
    
    
    If Verifica_Tubo_ja_Chegou(CodEtiqTubo, NumReq, CodTubo) = False Then
    
        If FlgMensagem = True Then
            'VerificaAnaliseJaMarcada CStr(NumReq)
        End If
        
        DtChega = Bg_DaData_ADO
        HrChega = Bg_DaHora_ADO
        UserChegada = gCodUtilizador
        
        If Len(CStr(CodEtiqTubo)) = 1 Then
            If gLAB = "HPOVOA" Then
                tubo = CodEtiqTubo
            Else
                tubo = "0" & CodEtiqTubo    'verificar????  codetiqtubo ou codtubo???
            End If
        Else
            tubo = CodEtiqTubo
        End If
        If Mid(tubo, 1, 1) = "0" Then
            tubo = "(" & BL_TrataStringParaBD(Mid(CStr(tubo), 2, 1)) & ", " & BL_TrataStringParaBD(CStr(tubo)) & ")"
        End If
        If Mid(tubo, 1, 1) <> "(" Then
            tubo = "(" & BL_TrataStringParaBD(tubo) & ")"
        End If
        
        'imprime tubo secundario
        If CodTubo <> "" Then
            VerificaEtiquetasSecundarias CodTubo
        Else
            VerificaEtiquetasSecundarias BL_SelCodigo("SL_TUBO", "COD_TUBO", "substr('00'||cod_etiq,-2)", CodEtiqTubo, "V")
        End If
        
        If CodTubo <> "" Then
            sql = "update sl_req_tubo set dt_chega = " & BL_TrataDataParaBD(DtChega) & _
              ", hr_chega = " & BL_TrataStringParaBD(HrChega) & _
              ", user_chega = " & BL_TrataStringParaBD(UserChegada) & _
              ", local_chega = " & gCodLocal & _
              ", estado_tubo = " & gEstadosTubo.chegado & _
              " where n_req = " & NumReq & " and cod_tubo = '" & CodTubo & "'"
        Else
            sql = "update sl_req_tubo set dt_chega = " & BL_TrataDataParaBD(DtChega) & _
                  ", hr_chega = " & BL_TrataStringParaBD(HrChega) & _
                  ", user_chega = " & BL_TrataStringParaBD(UserChegada) & _
                  ", local_chega = " & gCodLocal & _
                  ", estado_tubo = " & gEstadosTubo.chegado & _
                  " where n_req = " & NumReq & " and cod_tubo in " & tubo
        End If
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
        If CodProd <> "" Then
        
            'D� chegada do produto associado ao tubo
            sql = "update sl_req_prod set dt_chega = " & BL_TrataDataParaBD(DtChega) & _
                  " where n_req = " & NumReq & " and cod_prod = " & BL_TrataStringParaBD(CodProd) & _
                  " and dt_chega is null "
              
        Else
        
            'D� chegada dos produtos da requisi��o que ainda n tenham chegado
            sql = "update sl_req_prod set dt_chega = " & BL_TrataDataParaBD(DtChega) & _
                  " where n_req = " & NumReq & " and dt_chega is null "
        
        End If
        
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
        
        'D� chegada da requisi��o a partir do primeiro tubo recebido
        sql = "update sl_requis set dt_chega = " & BL_TrataDataParaBD(DtChega) & ", hr_chega = " & BL_TrataStringParaBD(HrChega) & " where n_req = " & NumReq & " and dt_chega is null "
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
        
        'D� chegada das an�lises associadas a esse tubo (dt_chega) na sl_marcacoes
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gSGBD = gOracle Then
            sql = "SELECT m.n_req, m.cod_perfil, m.cod_ana_c, m.cod_ana_s " & _
                             "FROM sl_marcacoes m, sl_ana_s s, sl_tubo t " & _
                             "WHERE m.cod_ana_s = s.cod_ana_s(+) and " & _
                             "s.cod_tubo = t.cod_tubo(+) and " & _
                             "m.n_req = " & gRequisicaoActiva & " and " & _
                             "(t.cod_tubo = '" & IIf(CodTubo <> "", CodTubo, CodEtiqTubo) & "' or t.cod_etiq is null) and dt_chega is null"
        ElseIf gSGBD = gSqlServer Then
            sql = "SELECT m.n_req, m.cod_perfil, m.cod_ana_c, m.cod_ana_s " & _
                             " FROM sl_ana_s s RIGHT OUTER JOIN sl_marcacoes m on m.cod_ana_s = s.cod_ana_s " & _
                             " LEFT OUTER JOIN sl_tubo t ON s.cod_tubo = t.cod_tubo " & _
                             " WHERE m.n_req = " & gRequisicaoActiva & " and " & _
                             " (t.cod_tubo = " & IIf(CodTubo <> "", CodTubo, CodEtiqTubo) & " or t.cod_etiq is null) and dt_chega is null"
                        
        End If
        rsAna.Open sql, gConexao
        While Not rsAna.EOF
            sql = "UPDATE sl_marcacoes set dt_chega = " & BL_TrataDataParaBD(DtChega) & "," & _
                    " hr_chega = " & BL_TrataStringParaBD(HrChega) & _
                    " WHERE n_req = " & rsAna!n_req & " and cod_perfil = " & BL_TrataStringParaBD(rsAna!Cod_Perfil) & _
                    " and cod_ana_c = " & BL_TrataStringParaBD(rsAna!cod_ana_c) & " and cod_ana_s = " & BL_TrataStringParaBD(rsAna!cod_ana_s)
            BG_ExecutaQuery_ADO sql, gConexao
            BG_Trata_BDErro
            rsAna.MoveNext
        Wend
        
        Envia_Facturacao (NumReq)
 
        Da_ChegadaTubo = True
        
        Call BL_MudaEstadoReq(NumReq)
        
    Else
        Da_ChegadaTubo = False
    End If
    
    Exit Function
TrataErro:
    BG_Mensagem mediMsgBeep, "Erro ao dar chegada do tubo " & CodEtiqTubo & " da requisi��o " & NumReq, vbError, "Chegada de Tubos"
    Da_ChegadaTubo = False
    Exit Function
    Resume Next
End Function

Function Da_SaidaTubo(CodEtiqTubo As String, NumReq As Long, Optional CodTubo As String) As Boolean
    Dim sql As String
    'Dim CodTubo As String
    Dim DtSaida As String
    Dim HrSaida As String
    Dim rsAna As ADODB.recordset
    Dim tubo As String
    Dim UserSaida As String
    
    On Error GoTo TrataErro
    
    
    If Verifica_Tubo_ja_Saiu(CodEtiqTubo, NumReq, CodTubo) = False Then
    
        DtSaida = Bg_DaData_ADO
        HrSaida = Bg_DaHora_ADO
        UserSaida = gCodUtilizador
        
        If Len(CStr(CodEtiqTubo)) = 1 Then
            If gLAB = "HPOVOA" Then
                tubo = CodEtiqTubo
            Else
                tubo = "0" & CodEtiqTubo    'verificar????  codetiqtubo ou codtubo???
            End If
        Else
            tubo = CodEtiqTubo
        End If
        If Mid(tubo, 1, 1) = "0" Then
            tubo = "(" & BL_TrataStringParaBD(Mid(CStr(tubo), 2, 1)) & ", " & BL_TrataStringParaBD(CStr(tubo)) & ")"
        End If
        If Mid(tubo, 1, 1) <> "(" Then
            tubo = "(" & BL_TrataStringParaBD(tubo) & ")"
        End If
        If CodTubo <> "" Then
            sql = "update sl_req_tubo set dt_saida = " & BL_TrataDataParaBD(DtSaida) & _
              ", hr_saida = " & BL_TrataStringParaBD(HrSaida) & _
              ", user_saida = " & BL_TrataStringParaBD(UserSaida) & _
              ", local_saida = " & gCodLocal & _
              " where n_req = " & NumReq & " and cod_tubo = '" & CodTubo & "'"
        Else
            sql = "update sl_req_tubo set dt_saida = " & BL_TrataDataParaBD(DtSaida) & _
                  ", hr_saida = " & BL_TrataStringParaBD(HrSaida) & _
                  ", user_saida = " & BL_TrataStringParaBD(UserSaida) & _
                  ", local_saida = " & gCodLocal & _
                  " where n_req = " & NumReq & " and cod_tubo in " & tubo
        End If
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
        
 
        Da_SaidaTubo = True
        
    Else
        Da_SaidaTubo = False
    End If
    
    Exit Function
TrataErro:
    BG_Mensagem mediMsgBeep, "Erro ao dar Sa�da do tubo " & CodEtiqTubo & " da requisi��o " & NumReq, vbError, "Chegada de Tubos"
    Da_SaidaTubo = False
    Exit Function
    Resume Next
End Function

Function Verifica_Tubo_ja_Chegou(CodEtiqTubo As String, NumReq As Long, Optional CodTubo As String) As Boolean
    ' Verifica se tubo j� chegou
    ' devolve o codigo do tubo a dar chegada, caso n tenha chegado
    
    Dim sql As String
    Dim rsTubo As ADODB.recordset
    
    Set rsTubo = New ADODB.recordset
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    
    If CodEtiqTubo <> "" Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, cod_prod " & _
              " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & _
              " and n_req = " & NumReq & "  and sl_tubo.cod_etiq = '" & CodEtiqTubo & "'" & _
              " and dt_chega is null "
    ElseIf CodTubo <> "" Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, cod_prod " & _
          " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & _
          " and n_req = " & NumReq & "  and sl_tubo.cod_tubo = '" & CodTubo & "'" & _
          " and dt_chega is null "
    End If
          
    rsTubo.Open sql, gConexao
    If rsTubo.RecordCount > 0 Then
        If BL_HandleNull(rsTubo!dt_chega, "") = "" Then
            Verifica_Tubo_ja_Chegou = False
            CodTubo = BL_HandleNull(rsTubo!cod_tubo, "")
            CodProd = BL_HandleNull(rsTubo!cod_prod, "")
        Else
            Verifica_Tubo_ja_Chegou = True
            CodTubo = ""
            CodProd = ""
        End If
        
    'VERIFICAR NECESSIDADE DO CODIGO ABAIXO!!
    '-------------------------------------------------------------------------------
    Else
        'situa��o HPOVOA : cod_etiq vazio : confirmar noutros clientes se nao falha
         Set rsTubo = New ADODB.recordset
         rsTubo.CursorLocation = adUseServer
         rsTubo.CursorType = adOpenStatic
         sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, cod_prod " & _
                " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & _
                " and n_req = " & NumReq & "  and sl_tubo.cod_tubo = " & BL_TrataStringParaBD(CodEtiqTubo)
         rsTubo.Open sql, gConexao
        If rsTubo.RecordCount > 0 Then
            If BL_HandleNull(rsTubo!dt_chega, "") = "" Then
                Verifica_Tubo_ja_Chegou = False
                'BL_HandleNull(RsTubo!Cod_Tubo, "")
                CodTubo = BL_HandleNull(rsTubo!cod_tubo, "")
                CodProd = BL_HandleNull(rsTubo!cod_prod, "")
            Else
                Verifica_Tubo_ja_Chegou = True
                CodTubo = ""
                CodProd = ""
            End If
        End If
    End If

End Function

Function Verifica_Tubo_ja_Saiu(CodEtiqTubo As String, NumReq As Long, Optional CodTubo As String) As Boolean
    ' Verifica se tubo j� chegou
    ' devolve o codigo do tubo a dar chegada, caso n tenha chegado
    
    Dim sql As String
    Dim rsTubo As ADODB.recordset
    
    Set rsTubo = New ADODB.recordset
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    
    If CodEtiqTubo <> "" Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_saida, cod_prod " & _
              " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & _
              " and n_req = " & NumReq & "  and sl_tubo.cod_etiq = '" & CodEtiqTubo & "'" & _
              " and dt_chega is null "
    ElseIf CodTubo <> "" Then
        sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_saida, cod_prod " & _
          " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & _
          " and n_req = " & NumReq & "  and sl_tubo.cod_tubo = '" & CodTubo & "'" & _
          " and dt_saida is null "
    End If
          
    rsTubo.Open sql, gConexao
    If rsTubo.RecordCount > 0 Then
        If BL_HandleNull(rsTubo!dt_saida, "") = "" Then
            Verifica_Tubo_ja_Saiu = False
            CodTubo = BL_HandleNull(rsTubo!cod_tubo, "")
            CodProd = BL_HandleNull(rsTubo!cod_prod, "")
        Else
            Verifica_Tubo_ja_Saiu = True
            CodTubo = ""
            CodProd = ""
        End If
        
    'VERIFICAR NECESSIDADE DO CODIGO ABAIXO!!
    '-------------------------------------------------------------------------------
    Else
        'situa��o HPOVOA : cod_etiq vazio : confirmar noutros clientes se nao falha
         Set rsTubo = New ADODB.recordset
         rsTubo.CursorLocation = adUseServer
         rsTubo.CursorType = adOpenStatic
         sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_saida, cod_prod " & _
                " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & _
                " and n_req = " & NumReq & "  and sl_tubo.cod_tubo = " & CodEtiqTubo
         rsTubo.Open sql, gConexao
        If rsTubo.RecordCount > 0 Then
            If BL_HandleNull(rsTubo!dt_saida, "") = "" Then
                Verifica_Tubo_ja_Saiu = False
                'BL_HandleNull(RsTubo!Cod_Tubo, "")
                CodTubo = BL_HandleNull(rsTubo!cod_tubo, "")
                CodProd = BL_HandleNull(rsTubo!cod_prod, "")
            Else
                Verifica_Tubo_ja_Saiu = True
                CodTubo = ""
                CodProd = ""
            End If
        End If
    End If

End Function



Private Sub BtPesquisaRapidaGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrAna.Text = resultados(1)
            EcDescrGrAna.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub



Private Sub BtPesquisaRapidaTubo_Click()
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_tubo"
    CamposEcran(1) = "cod_tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_tubo"
    CamposEcran(2) = "descr_tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_tubo"
    CampoPesquisa1 = "descr_tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubos")
    
    mensagem = "N�o foi encontrada nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTubo.Text = resultados(1)
            EcDescrTubo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodTubo_Validate(Cancel As Boolean)
    Dim RsDescrTubo As ADODB.recordset
    
    If Trim(EcCodTubo.Text) <> "" Then
        Set RsDescrTubo = New ADODB.recordset
        
        With RsDescrTubo
            .Source = "SELECT descr_tubo FROM sl_tubo WHERE cod_tubo= " & BL_TrataStringParaBD(EcCodTubo.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrTubo.RecordCount > 0 Then
            EcDescrTubo.Text = RsDescrTubo!descR_tubo
            RsDescrTubo.Close
            Set RsDescrTubo = Nothing
        Else
            RsDescrTubo.Close
            Set RsDescrTubo = Nothing
            EcDescrTubo.Text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrTubo.Text = ""
    End If
    
End Sub

Private Sub Eccodgrana_Validate(Cancel As Boolean)
    Dim RsDescrGrAnalises As ADODB.recordset
    
    If Trim(EcCodGrAna.Text) <> "" Then
        Set RsDescrGrAnalises = New ADODB.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcCodGrAna.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount > 0 Then
            EcDescrGrAna.Text = RsDescrGrAnalises!descr_gr_ana
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrGrAna.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo de An�lise indicado n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrAna.Text = ""
    End If
    
End Sub


Private Sub LimpaFrameTubosFalta()
    EcCodGrAna = ""
    Eccodgrana_Validate False
    EcCodTubo = ""
    EcCodTubo_Validate False
    EcDtReq.value = Bg_DaData_ADO
End Sub
Private Sub BtSair_Click()
    FrameTubosFalta.Visible = False
    LimpaFrameTubosFalta
End Sub

Private Sub BtFaltaTubos_Click()
    FrameTubosFalta.Visible = True
    FaltaTubos
End Sub



Private Sub FaltaTubos()
    
    ' Selecciona todos os tubos da requisi��o e preenche a lista
    Dim sSql As String
    Dim i As Long
    Dim rsTubo As ADODB.recordset
    Dim CmdDescrT As New ADODB.Command
          
    ' SIMPLES
    sSql = " SELECT x1.n_req, x5.cod_tubo,x1.dt_previ, x1.dt_chega, x1.hr_chega, x5.descr_tubo "
    sSql = sSql & " FROM sl_req_tubo x1, sl_marcacoes x2, sl_ana_s x3, "
    sSql = sSql & " sl_gr_ana x4, sl_tubo x5, sl_requis x6 "
    sSql = sSql & " Where x1.n_req = x2.n_req"
    sSql = sSql & " AND x2.cod_ana_s = x3.cod_ana_s"
    sSql = sSql & " AND x3.gr_ana = x4.cod_gr_ana"
    sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x2.cod_ana_s <> 'S99999'"
    sSql = sSql & " AND x3.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x1.n_req = x6.n_req "
    sSql = sSql & " AND x1.dt_chega IS NULL "
    
    If EcCodGrAna <> "" Then
        sSql = sSql & " AND x3.gr_ana = " & EcCodGrAna
    End If
    
    If EcCodTubo <> "" Then
        sSql = sSql & " AND x5.cod_tubo = " & EcCodTubo
    End If
    
    If EcDtReq <> "" Then
        sSql = sSql & " AND x6.dt_cri = " & BL_TrataDataParaBD(EcDtReq)
    End If
    
    
    ' COMPLEXAS
    sSql = sSql & " UNION SELECT x1.n_req, x5.cod_tubo,x1.dt_previ, x1.dt_chega, x1.hr_chega, x5.descr_tubo "
    sSql = sSql & " FROM sl_req_tubo x1, sl_marcacoes x2, sl_ana_c x3, "
    sSql = sSql & " sl_gr_ana x4, sl_tubo x5, sl_requis x6 "
    sSql = sSql & " Where x1.n_req = x2.n_req"
    sSql = sSql & " AND x2.cod_ana_c = x3.cod_ana_c"
    sSql = sSql & " AND x3.gr_ana = x4.cod_gr_ana"
    sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x2.cod_ana_c <> 'C99999'"
    sSql = sSql & " AND x3.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x1.n_req = x6.n_req "
    sSql = sSql & " AND x1.dt_chega IS NULL "
    
    If EcCodGrAna <> "" Then
        sSql = sSql & " AND x3.gr_ana = " & EcCodGrAna
    End If
    
    If EcCodTubo <> "" Then
        sSql = sSql & " AND x5.cod_tubo = " & EcCodTubo
    End If
    
    If EcDtReq <> "" Then
        sSql = sSql & " AND x6.dt_cri = " & BL_TrataDataParaBD(EcDtReq)
    End If
        
    ' PERFIS
    sSql = sSql & " UNION SELECT x1.n_req, x5.cod_tubo,x1.dt_previ, x1.dt_chega, x1.hr_chega, x5.descr_tubo "
    sSql = sSql & " FROM sl_req_tubo x1, sl_marcacoes x2, sl_perfis x3, "
    sSql = sSql & " sl_gr_ana x4, sl_tubo x5, sl_requis x6 "
    sSql = sSql & " Where x1.n_req = x2.n_req"
    sSql = sSql & " AND x2.cod_perfil = x3.cod_perfis"
    sSql = sSql & " AND x3.gr_ana = x4.cod_gr_ana"
    sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x3.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x1.n_req = x6.n_req "
    sSql = sSql & " AND x1.dt_chega IS NULL "
    
    If EcCodGrAna <> "" Then
        sSql = sSql & " AND x3.gr_ana = " & EcCodGrAna
    End If
    
    If EcCodTubo <> "" Then
        sSql = sSql & " AND x5.cod_tubo = " & EcCodTubo
    End If
    
    If EcDtReq <> "" Then
        sSql = sSql & " AND x6.dt_cri = " & BL_TrataDataParaBD(EcDtReq)
    End If
    
    ' SIMPLES
    sSql = sSql & " UNION SELECT x1.n_req, x5.cod_tubo,x1.dt_previ, x1.dt_chega, x1.hr_chega, x5.descr_tubo "
    sSql = sSql & " FROM sl_req_tubo x1, sl_realiza x2, sl_ana_s x3, "
    sSql = sSql & " sl_gr_ana x4, sl_tubo x5, sl_requis x6 "
    sSql = sSql & " Where x1.n_req = x2.n_req"
    sSql = sSql & " AND x2.cod_ana_s = x3.cod_ana_s"
    sSql = sSql & " AND x3.gr_ana = x4.cod_gr_ana"
    sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x2.cod_ana_s <> 'S99999'"
    sSql = sSql & " AND x3.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x1.n_req = x6.n_req "
    sSql = sSql & " AND x1.dt_chega IS NULL "
    
    If EcCodGrAna <> "" Then
        sSql = sSql & " AND x3.gr_ana = " & EcCodGrAna
    End If
    
    If EcCodTubo <> "" Then
        sSql = sSql & " AND x5.cod_tubo = " & EcCodTubo
    End If
    
    If EcDtReq <> "" Then
        sSql = sSql & " AND x6.dt_cri = " & BL_TrataDataParaBD(EcDtReq)
    End If
    
    
    ' COMPLEXAS
    sSql = sSql & " UNION SELECT x1.n_req, x5.cod_tubo,x1.dt_previ, x1.dt_chega, x1.hr_chega, x5.descr_tubo "
    sSql = sSql & " FROM sl_req_tubo x1, sl_realiza x2, sl_ana_c x3, "
    sSql = sSql & " sl_gr_ana x4, sl_tubo x5, sl_requis x6 "
    sSql = sSql & " Where x1.n_req = x2.n_req"
    sSql = sSql & " AND x2.cod_ana_c = x3.cod_ana_c"
    sSql = sSql & " AND x3.gr_ana = x4.cod_gr_ana"
    sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x2.cod_ana_c <> 'C99999'"
    sSql = sSql & " AND x3.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x1.n_req = x6.n_req "
    sSql = sSql & " AND x1.dt_chega IS NULL "
    
    If EcCodGrAna <> "" Then
        sSql = sSql & " AND x3.gr_ana = " & EcCodGrAna
    End If
    
    If EcCodTubo <> "" Then
        sSql = sSql & " AND x5.cod_tubo = " & EcCodTubo
    End If
    
    If EcDtReq <> "" Then
        sSql = sSql & " AND x6.dt_cri = " & BL_TrataDataParaBD(EcDtReq)
    End If
        
    ' PERFIS
    sSql = sSql & " UNION SELECT x1.n_req, x5.cod_tubo,x1.dt_previ, x1.dt_chega, x1.hr_chega, x5.descr_tubo "
    sSql = sSql & " FROM sl_req_tubo x1, sl_realiza x2, sl_perfis x3, "
    sSql = sSql & " sl_gr_ana x4, sl_tubo x5, sl_requis x6 "
    sSql = sSql & " Where x1.n_req = x2.n_req"
    sSql = sSql & " AND x2.cod_perfil = x3.cod_perfis"
    sSql = sSql & " AND x3.gr_ana = x4.cod_gr_ana"
    sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x3.cod_tubo = x5.cod_tubo"
    sSql = sSql & " AND x1.n_req = x6.n_req "
    sSql = sSql & " AND x1.dt_chega IS NULL "
    
    If EcCodGrAna <> "" Then
        sSql = sSql & " AND x3.gr_ana = " & EcCodGrAna
    End If
    
    If EcCodTubo <> "" Then
        sSql = sSql & " AND x5.cod_tubo = " & EcCodTubo
    End If
    
    If EcDtReq <> "" Then
        sSql = sSql & " AND x6.dt_cri = " & BL_TrataDataParaBD(EcDtReq)
    End If
    
    sSql = sSql & " GROUP BY  x1.n_req, x5.cod_tubo,x1.dt_previ, x1.dt_chega, x1.hr_chega, x5.descr_tubo "
    If gSGBD = gOracle Then
        sSql = sSql & " ORDER BY n_req "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " ORDER BY x1.n_req "
    End If
    
    Set rsTubo = New ADODB.recordset
    With rsTubo
        .Source = sSql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        .Open
    End With
    
    LimpaFgTubos2
    
    If rsTubo.RecordCount = 0 Then
       rsTubo.Close
       Set rsTubo = Nothing
       Exit Sub
    End If
     
     i = 1
     While Not rsTubo.EOF
        FgTubos2.row = i
        FgTubos2.Col = 0
        FgTubos2.TextMatrix(i, 0) = BL_HandleNull(rsTubo!n_req, "")
        
        FgTubos2.Col = 1
        FgTubos2.TextMatrix(i, 1) = BL_HandleNull(rsTubo!cod_tubo, "")
        
        FgTubos2.Col = 2
        FgTubos2.TextMatrix(i, 2) = BL_HandleNull(rsTubo!descR_tubo, "")
        
        FgTubos2.Col = 3
        FgTubos2.TextMatrix(i, 3) = BL_HandleNull(rsTubo!dt_previ, "")
        
        FgTubos2.Col = 4
        FgTubos2.TextMatrix(i, 4) = BL_HandleNull(rsTubo!dt_chega, "")
        
        FgTubos2.Col = 5
        FgTubos2.TextMatrix(i, 5) = BL_HandleNull(rsTubo!hr_chega, "")
        
        FgTubos2.Col = 6
        FgTubos2.TextMatrix(i, 6) = "ESPERA"
        
        rsTubo.MoveNext
        i = i + 1
        
        FgTubos2.AddItem "", i
        FgTubos2.row = i
     Wend
            
     Set rsTubo = Nothing

End Sub

Sub LimpaFgTubos2()
    
    Dim j As Long
    Dim i As Long
    j = FgTubos2.rows - 1
    While j > 0
        If j > 1 Then
            FgTubos2.RemoveItem j
        Else
             For i = 0 To FGTubos.Cols - 1
                FGTubos.TextMatrix(j, i) = ""
            Next
        End If
        
        j = j - 1
    Wend
    
    CriaNovaClasse RegistosT, 1, "TUBO", True

End Sub

Private Sub EcNumReqAssoc_Validate(Cancel As Boolean)
    Dim RsAnalise As ADODB.recordset
    Dim sql As String
    
    EcNumReqAssoc.Text = UCase(EcNumReqAssoc.Text)
    If EcNumReqAssoc.Text <> "" Then
        sql = "Select n_req from sl_requis where n_req_assoc = '" & EcNumReqAssoc.Text & "'"
        Set RsAnalise = New ADODB.recordset
        RsAnalise.CursorType = adOpenStatic
        RsAnalise.CursorLocation = adUseServer
        RsAnalise.Open sql, gConexao
        If RsAnalise.RecordCount > 0 Then
            EcNumReq.Text = RsAnalise!n_req
            EcNumReq_KeyDown 13, False
        Else
            BG_Mensagem mediMsgBox, "Requisi��o Associada inexistente!", vbOKOnly + vbExclamation, "SISLAB"
            LimpaCampos
        End If
        RsAnalise.Close
        Set RsAnalise = Nothing
    End If

End Sub

Sub Desactiva_ChegadaTubo(RowTubo As Integer)
    Dim sql As String
    Dim rv As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    If FGTubos.TextMatrix(RowTubo, 7) = "Entrou" Then
        FGTubos.TextMatrix(RowTubo, 4) = ""
        FGTubos.TextMatrix(RowTubo, 5) = ""
        FGTubos.TextMatrix(RowTubo, 7) = "Espera"
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.Pendente & ", dt_chega = null, hr_chega = null, user_chega = null, "
        sql = sql & " user_colheita = null, dt_colheita = null, hr_colheita = null, where n_req = " & _
                gRequisicaoActiva & " and cod_tubo = " & BL_TrataStringParaBD(FGTubos.TextMatrix(RowTubo, 1))
        BG_ExecutaQuery_ADO sql
        
        RegistosT(RowTubo).DtChega = ""
        RegistosT(RowTubo).HrChega = ""
        RegistosT(RowTubo).EstadoTubo = BL_Coloca_Estado(RegistosT(RowTubo).DtPrev, RegistosT(RowTubo).DtChega, RegistosT(RowTubo).DtElim)
        
        If RegistosT(RowTubo).EstadoTubo = "Entrou" Then
            'Verde
            RegistosT(RowTubo).Cor_Estado = &H8000&
        ElseIf RegistosT(RowTubo).EstadoTubo = "Espera" Then
            'Laranja
            RegistosT(RowTubo).Cor_Estado = &HC0&
        ElseIf RegistosT(RowTubo).EstadoTubo = "Falta" Then
            'Vermelho
            RegistosT(RowTubo).Cor_Estado = &HC0&
        ElseIf RegistosT(RowTubo).EstadoTubo = "Cancelado" Then
            'Cinzento
            RegistosT(RowTubo).Cor_Estado = &H808080
        End If
        
        For i = 0 To FGTubos.Cols - 1
            FGTubos.row = RowTubo
            FGTubos.Col = i
            FGTubos.CellForeColor = RegistosT(RowTubo).Cor_Estado
        Next i
        
        If gSQLError <> 0 Then
            BG_Mensagem mediMsgBox, "Erro a desactivar chegada do tubo " & FGTubos.TextMatrix(RowTubo, 1) & "!", vbCritical + vbOKOnly, "Erro"
        Else
            BG_Mensagem mediMsgBox, "Chegada do tubo " & FGTubos.TextMatrix(RowTubo, 1) & " desactivada com sucesso!", vbInformation, "Desactivar chegada"
        End If
    End If
    Exit Sub
TrataErro:
    MsgBox "Erro a desactivar chegada do tubo " & FGTubos.TextMatrix(RowTubo, 1)
End Sub

Sub Envia_Facturacao(NReq As Long)
    Dim sql As String
    Dim RsFact As ADODB.recordset
    Dim HisAberto As Integer
    
    'soliveira 15.05.2008 CHVNG-Espinho
    'Envio para a facturacao na chegada de tubos
    
    On Error GoTo TrataErro
    
    If gEnviaFact_ChegaTubo = 1 Then

        If UCase(HIS.nome) = "SONHO" Then
            If gHIS_Import = 1 Then
                HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
            Else
                HisAberto = 0
            End If
            gConnHIS.BeginTrans
        ElseIf UCase(HIS.nome) = "GH" Then
            HisAberto = 1
        End If
        
        Set RsFact = New ADODB.recordset
        RsFact.CursorLocation = adUseServer
        RsFact.CursorType = adOpenStatic
        sql = " select r.n_req,i.t_utente,i.utente, i.n_proc_1,  r.t_sit, r.n_epis,r.cod_efr,r.cod_proven, r.dt_chega,m.cod_perfil,m.cod_ana_c,m.cod_ana_s,m.cod_agrup, m.flg_facturado, r.cod_med " & _
                " from sl_requis r, sl_marcacoes m, sl_identif i " & _
                " Where r.n_req = m.n_req " & _
                " and r.seq_utente = i.seq_utente " & _
                " and r.n_req = " & NReq & " order by cod_perfil, cod_ana_c,cod_ana_s, m.ord_ana, m.ord_ana_compl, m.ord_ana_perf "
        RsFact.Open sql, gConexao
        While Not RsFact.EOF
            If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
                    
                If RsFact!Flg_Facturado = 0 And ANALISE_Verifica_MICRO(RsFact!cod_ana_s) = False And ANALISE_Verifica_AnaNaoFacturaGESREQ(RsFact!cod_ana_s) = False And _
                    (Len(BL_HandleNull(RsFact!n_epis, "")) > 0) And (BL_HandleNull(RsFact!t_sit, -1) <> -1) Then
                    If IF_Factura_Analise(RsFact!n_req, _
                                                BL_HandleNull(RsFact!cod_efr, ""), _
                                                RsFact!t_sit, _
                                                RsFact!n_epis, _
                                                RsFact!dt_chega, _
                                                RsFact!n_proc_1, _
                                                RsFact!cod_proven, _
                                                RsFact!Cod_Perfil, _
                                                RsFact!cod_ana_c, _
                                                RsFact!cod_ana_s, _
                                                RsFact!cod_agrup, "", RsFact!t_utente, RsFact!Utente, Empty, Empty, Empty, 1, BL_HandleNull(RsFact!cod_med, ""), "", "") = True Then
                        
                        sql = "update sl_marcacoes set flg_facturado = 1 where n_req = " & RsFact!n_req & " and cod_perfil = '" & RsFact!Cod_Perfil & "'" & _
                               " and cod_ana_c = '" & RsFact!cod_ana_c & "' and cod_ana_s = '" & RsFact!cod_ana_s & "'"
                        BG_ExecutaQuery_ADO sql
                        
                    Else
                        If gAnalise_Ja_Facturada = True Then
                            sql = "update sl_marcacoes set flg_facturado = 1 where n_req = " & RsFact!n_req & " and cod_perfil = '" & RsFact!Cod_Perfil & "'" & _
                                    " and cod_ana_c = '" & RsFact!cod_ana_c & "' and cod_ana_s = '" & RsFact!cod_ana_s & "'"
                            BG_ExecutaQuery_ADO sql
                        End If
                    End If
                End If

            End If
            RsFact.MoveNext
        Wend
        RsFact.Close
        Set RsFact = Nothing
        
        'necess�rio quando existe database links (HDM)
        gConnHIS.CommitTrans

        
    End If
    Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro Envia_Facturacao na Chegada de Tubos " & Err.Description & " " & sql
    gConnHIS.RollbackTrans
End Sub

Private Sub Opt_Click(Index As Integer)
    lTipoPesquisa = Index
    FuncaoProcurar
End Sub




Private Sub PreencheFGTubo(NumReq As Long)
    Dim i As Integer
    
    For i = 1 To RegistosT.Count - 1
        FGTubos.row = i
        FGTubos.Col = 0
        FGTubos.TextMatrix(i, 0) = NumReq
        FGTubos.CellForeColor = RegistosT(i).Cor_Estado
        
        FGTubos.Col = 1
        FGTubos.TextMatrix(i, 1) = RegistosT(i).CodTubo
        FGTubos.CellForeColor = RegistosT(i).Cor_Estado
        
        FGTubos.Col = 2
        FGTubos.TextMatrix(i, 2) = RegistosT(i).descrTubo
        FGTubos.CellForeColor = RegistosT(i).Cor_Estado
        
        FGTubos.Col = 3
        FGTubos.TextMatrix(i, 3) = RegistosT(i).DtPrev
        FGTubos.CellForeColor = RegistosT(i).Cor_Estado
        
        If lTipoPesquisa = Entrada Then
            FGTubos.Col = 4
            FGTubos.TextMatrix(i, 4) = RegistosT(i).DtChega
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
            
            FGTubos.Col = 5
            FGTubos.TextMatrix(i, 5) = RegistosT(i).HrChega
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
            
            FGTubos.Col = 6
            FGTubos.TextMatrix(i, 6) = BL_SelCodigo("SL_IDUTILIZADOR", "UTILIZADOR", "COD_UTILIZADOR", BL_HandleNull(RegistosT(i).UserChega, ""), "V")
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
        
            FGTubos.Col = 7
            FGTubos.TextMatrix(i, 7) = RegistosT(i).EstadoTubo
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
        
        ElseIf lTipoPesquisa = saida Then
            FGTubos.Col = 4
            FGTubos.TextMatrix(i, 4) = RegistosT(i).DtSaida
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
            
            FGTubos.Col = 5
            FGTubos.TextMatrix(i, 5) = RegistosT(i).HrSaida
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
            
            FGTubos.Col = 6
            FGTubos.TextMatrix(i, 6) = BL_SelCodigo("SL_IDUTILIZADOR", "UTILIZADOR", "COD_UTILIZADOR", BL_HandleNull(RegistosT(i).UserSaida, ""), "V")
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
            
            FGTubos.Col = 7
            FGTubos.TextMatrix(i, 7) = ""
            FGTubos.CellForeColor = RegistosT(i).Cor_Estado
        End If
        
    Next
End Sub

' ----------------------------------------------------------------------------------------------

' PROCURA ANALISES CODIFICADAS PARA UM DETERMINADO TUBO

' ----------------------------------------------------------------------------------------------

Private Sub ProcuraAnalises(linha As Long)
    
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "select cod_ana, descr_ana from slv_analises where cod_tubo = " & BL_TrataStringParaBD(RegistosT(linha).CodTubo) & _
           "and cod_ana in (select cod_agrup from sl_marcacoes where n_req = " & gRequisicaoActiva & _
           "union select cod_agrup from sl_realiza where n_req = " & gRequisicaoActiva & ")"
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount = 0 Then
        rsAna.Close
        sSql = "SELECT cod_ana_s cod_ana, descr_ana_s descr_ana FROM sl_ana_s WHERE cod_tubo = " & BL_TrataStringParaBD(RegistosT(linha).CodTubo) & _
           "and cod_ana_s in (select cod_ana_s from sl_marcacoes where n_req = " & gRequisicaoActiva & _
           "union select cod_ana_s from sl_realiza where n_req = " & gRequisicaoActiva & ")"
        sSql = sSql & " UNION SELECT cod_ana_c cod_ana, descr_ana_c descr_ana FROM sl_ana_c WHERE cod_tubo = " & BL_TrataStringParaBD(RegistosT(linha).CodTubo) & _
           "and cod_ana_c in (select cod_ana_c from sl_marcacoes where n_req = " & gRequisicaoActiva & _
           "union select cod_ana_s from sl_realiza where n_req = " & gRequisicaoActiva & ")"
        sSql = sSql & " UNION SELECT cod_perfis cod_ana, descr_perfis descr_ana FROM sl_perfis WHERE cod_tubo = " & BL_TrataStringParaBD(RegistosT(linha).CodTubo) & _
           "and cod_perfis in (select cod_perfil from sl_marcacoes where n_req = " & gRequisicaoActiva & _
           "union select cod_ana_s from sl_realiza where n_req = " & gRequisicaoActiva & ")"
        rsAna.CursorType = adOpenStatic
        rsAna.CursorLocation = adUseServer
        rsAna.Open sSql, gConexao
    End If
    If (rsAna.RecordCount > 0) Then
        While Not rsAna.EOF
            totalAnalises = totalAnalises + 1
            ReDim Preserve estrutAnalises(totalAnalises)
            LimpaFGAna
            PreencheEstrutAna BL_HandleNull(rsAna!cod_ana, ""), BL_HandleNull(rsAna!descr_ana, "")
            rsAna.MoveNext
        Wend
        PreencheFGAna
        FrAna.Visible = True
    End If
    rsAna.Close
    Set rsAna = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher An�lises : " & sSql & Err.Description, Me.Name, "ProcuraAnalises"
    BG_Mensagem mediMsgBox, "Erro ao Preencher An�lises !", vbExclamation, "ProcuraAnalises"
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ANALISES

' ----------------------------------------------------------------------------------------------

Private Sub PreencheEstrutAna(cod_ana As String, descr_ana As String)
    On Error GoTo TrataErro

    estrutAnalises(totalAnalises).cod_ana = cod_ana
    estrutAnalises(totalAnalises).descr_ana = descr_ana
  
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Estrutura de An�lises " & Err.Description, Me.Name, "PreencheEstrutAna"
    BG_Mensagem mediMsgBox, "Erro ao Preencher Estrutura de An�lises !", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaFGAna()
    On Error GoTo TrataErro
    FgAna.rows = 1
    FgAna.rows = 2
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Limpar Grelha An�lises" & Err.Description, Me.Name, "LimpaFGAna"
    BG_Mensagem mediMsgBox, "Erro ao Limpar Grelha An�lises!", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------------------

' PREENCHE FLEX GRID DE ANALISES

' ----------------------------------------------------------------------------------------------

Private Sub PreencheFGAna()
    Dim estado As String
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To totalAnalises
        FgAna.TextMatrix(i, 0) = estrutAnalises(i).cod_ana
        FgAna.TextMatrix(i, 1) = estrutAnalises(i).descr_ana
        FgAna.AddItem ""
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Grelha de An�lises " & Err.Description, Me.Name, "PreencheFGAna"
    BG_Mensagem mediMsgBox, "Erro ao Preencher Grelha de An�lises !", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
    
End Sub

Private Sub BtFecharFrame_Click()
    FrAna.Visible = False
End Sub

Private Sub Command0_Click(Index As Integer)

    On Error GoTo TrataErro
    totalAnalises = 0
    ReDim estrutAnalises(0)
    ProcuraAnalises CLng(Index)
    Exit Sub

TrataErro:
Exit Sub

End Sub

Private Sub DestroiBotoesDinamicos()
    
    Dim j As Integer
    
    On Error GoTo TrataErro
    For j = Command0.LBound + 1 To Command0.UBound
        Controls.Remove "Command" & CStr(j)
        Unload Command0(j)
    Next
    Exit Sub

TrataErro:
    Exit Sub

End Sub

Private Sub ConstroiBotoesDinamicos()
    
    Dim i As Integer
    
    On Error GoTo TrataErro
    Set Command0(Command0.UBound).Container = Controls("FrTubos")
    Command0(Empty).top = 0
    Command0(Empty).Visible = False
    DestroiBotoesDinamicos
    Set Command0(0).Container = Controls("FrTubos")
    For i = 1 To RegistosT.Count - 1
        Call Controls.Add("vb.commandbutton", "Command" & CStr(Command0.UBound + 1), Controls("FrTubos"))
        Load Command0(Command0.UBound + 1)
        Set Command0(Command0.UBound).Container = Controls("FrTubos")
        Command0(Command0.UBound).Width = 800 '500
        Command0(Command0.UBound).Height = 200
        Command0(Command0.UBound).top = (285 * (i + 1)) - 120
        Command0(Command0.UBound).left = 5900
        Command0(Command0.UBound).caption = "An�lises"
        Command0(Command0.UBound).ToolTipText = "Consulta an�lises associadas ao tubo " & RegistosT(i).descrTubo
        Command0(Command0.UBound).Visible = True
    Next
    Exit Sub

TrataErro:
    Exit Sub

End Sub

' --------------------------------------------------------

' VERIFICA SE ANALISE JA FOI MARCADA NOUTRA REQUUISICAO NO PROPRIO DIA

' --------------------------------------------------------
Private Function VerificaAnaliseJaMarcada(n_req As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim rsReq As New ADODB.recordset
    Dim data As Integer
    Dim Data2 As String
    Dim rsAnalises As New ADODB.recordset
    Dim p_seq_utente As String
    Dim p_cod_agrup As String
    Dim p_user_cri As String
    
    ' verifica se requisicao nao tem data de chegada e procura seq_utente
    sSql = "SELECT * FROM SL_REQUIS WHERE n_req = " & n_req
    rsReq.CursorType = adOpenStatic
    rsReq.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao
    If rsReq.RecordCount > 0 Then
        p_seq_utente = BL_HandleNull(rsReq!seq_utente, "")
        p_user_cri = BL_HandleNull(rsReq!user_cri, "")
        If BL_HandleNull(BL_SelCodigo("sl_idutilizador", "flg_electronico", "cod_utilizador", p_user_cri, "V"), "") = "1" Then
            FlgMensagem = True
            VerificaAnaliseJaMarcada = False
            Exit Function
        End If
    End If
    rsReq.Close
    Set rsReq = Nothing
    
    ' AN�LISES MARCADAS
    sSql = "SELECT distinct cod_agrup FROM sl_marcacoes WHERE n_req = " & n_req
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            ' PARA CADA ANALISE VERIFICA O PRAZO_VAL
            data = -1
            sSql = "SELECT prazo_Val FROM slv_analises WHERE cod_ana = " & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_agrup, "")) & " AND prazo_val IS NOT NULL "
            rsAnalises.CursorType = adOpenStatic
            rsAnalises.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnalises.Open sSql, gConexao
            If rsAnalises.RecordCount > 0 Then
                data = BL_HandleNull(rsAna!prazo_val, 0)
            End If
            rsAnalises.Close
            If data = -1 Then
                data = 0
            End If
            Data2 = CStr(CDate(Bg_DaData_ADO) - data)
            
            'verifica se analise ja foi marcada naquele intervalo de data
            sSql = "SELECT distinct x2.n_Req FROM  sl_requis x2, sl_marcacoes x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
            sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(Bg_DaData_ADO)
            sSql = sSql & " AND x2.seq_utente = " & p_seq_utente & " AND x2.n_Req = x3.n_req   AND x3.cod_agrup = " & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_agrup, ""))
            If n_req <> "" Then
                sSql = sSql & " AND x2.n_req <> " & n_req
            End If
            sSql = sSql & " UNION SELECT distinct x2.n_Req FROM  sl_requis x2, sl_realiza x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
            sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(Bg_DaData_ADO)
            sSql = sSql & " AND x2.seq_utente = " & p_seq_utente & " AND x2.n_Req = x3.n_req AND x3.cod_agrup = " & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_agrup, ""))
            If n_req <> "" Then
                sSql = sSql & " AND x2.n_req <> " & n_req
            End If
            rsAnalises.CursorType = adOpenStatic
            rsAnalises.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnalises.Open sSql, gConexao
            If rsAnalises.RecordCount > 0 Then
                VerificaAnaliseJaMarcada = True
                FlgMensagem = False
                MsgBox "An�lise(s) j� foi marcada(s)  para o Utente na requisi��o: " & rsAnalises!n_req & " !"
                rsAnalises.Close
                Exit Function
            Else
                VerificaAnaliseJaMarcada = False
            End If
            rsAnalises.Close
            
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaAnaliseJaMarcada: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaAnaliseJaMarcada"
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM TUBOS SECUND�RIOS

' ------------------------------------------------------------------------------------------------------

Private Sub PreencheTubosSec(cod_tubo_prim As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsSec As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_tubo_sec WHERE cod_tubo_prim = " & BL_TrataStringParaBD(cod_tubo_prim)
    rsSec.CursorType = adOpenStatic
    rsSec.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSec.Open sSql, gConexao
    If rsSec.RecordCount > 0 Then
        While Not rsSec.EOF
            totalTubosSec = totalTubosSec + 1
            ReDim Preserve estrutTubosSec(totalTubosSec)
            estrutTubosSec(totalTubosSec).cod_tubo_prim = cod_tubo_prim
            estrutTubosSec(totalTubosSec).cod_tubo_sec = BL_HandleNull(rsSec!cod_tubo, "")
            estrutTubosSec(totalTubosSec).descr_tubo_sec = BL_HandleNull(rsSec!descR_tubo, "")
            estrutTubosSec(totalTubosSec).etiq_tubo_sec = BL_HandleNull(rsSec!cod_etiq, "")
            estrutTubosSec(totalTubosSec).nome_ute = EcNome
            estrutTubosSec(totalTubosSec).n_req = gRequisicaoActiva
            estrutTubosSec(totalTubosSec).t_utente = CbTipoUtente
            estrutTubosSec(totalTubosSec).Utente = EcUtente
            estrutTubosSec(totalTubosSec).dt_nasc_ute = EcDataNasc
            estrutTubosSec(totalTubosSec).Copias = BL_HandleNull(rsSec!num_copias, 1)
            estrutTubosSec(totalTubosSec).abr_ana = BL_RetornaAbrAnaTubo(CStr(gRequisicaoActiva), BL_HandleNull(rsSec!cod_tubo, ""), False)
            If estrutTubosSec(totalTubosSec).Copias = 0 Then
                estrutTubosSec(totalTubosSec).Copias = 1
            End If
            rsSec.MoveNext
        Wend
    End If
    rsSec.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao preencher tubos Sec: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheTubosSec"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------------

' VERIFICA QUE ETIQUETAS SECUNDARIAS SAO PARA IMPRIMIR

' ------------------------------------------------------------------------------------------------------
Private Sub VerificaEtiquetasSecundarias(cod_tubo_prim As String)
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To totalTubosSec
        If estrutTubosSec(i).cod_tubo_prim = cod_tubo_prim Then
            ImprimeEtiqSec i
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao verificar tubos Sec para imprimir: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaEtiquetasSecundarias"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------------

' IMPRIME DETERMINADA ETIQUETA SECUNDARIA

' ------------------------------------------------------------------------------------------------------
Private Sub ImprimeEtiqSec(indice As Integer)
    On Error GoTo TrataErro
    Dim i As Integer
    Dim N_req_bar As String
    Dim idade As String
    
    
    N_req_bar = Right("0000000" & estrutTubosSec(indice).n_req, 7)
    N_req_bar = estrutTubosSec(indice).etiq_tubo_sec & N_req_bar
    
    ' ---------------------------------
    If BL_HandleNull(estrutTubosSec(indice).dt_nasc_ute, "") <> "" Then
        idade = CStr(BG_CalculaIdade(CDate(estrutTubosSec(indice).dt_nasc_ute), Bg_DaData_ADO))
    End If
        
    If Not BL_LerEtiqIni("etiq_sec.ini") Then
        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
        Exit Sub
    End If
    If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
        MsgBox "Imposs�vel abrir impressora etiquetas"
        Exit Sub
    End If
        
    Call EtiqPrint(estrutTubosSec(indice).t_utente, estrutTubosSec(indice).Utente, estrutTubosSec(indice).n_req, _
                    estrutTubosSec(indice).nome_ute, N_req_bar, estrutTubosSec(indice).descr_tubo_sec, estrutTubosSec(indice).Copias, _
                    estrutTubosSec(indice).abr_ana)
    BL_EtiqClosePrinter
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao imprimir tubo Sec: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeEtiqSec"
    Exit Sub
    Resume Next
End Sub

Private Function EtiqPrint( _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_req As String, _
    ByVal nome_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal Copias As Integer, _
    ByVal abr_ana As String) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_RemovePortuguese(StrConv(nome_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{DT_IMP}", CStr(Bg_DaData_ADO))
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", "")

    sWrittenData = Replace(sWrittenData, "{GR_ANA}", "")
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", "")
    sWrittenData = Replace(sWrittenData, "{T_URG}", "")
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", "")
    sWrittenData = Replace(sWrittenData, "{NOME1}", "")
    sWrittenData = Replace(sWrittenData, "{NOME2}", "")
    sWrittenData = Replace(sWrittenData, "{NOME3}", "")
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", "")
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", "")
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", "")
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", "")
    sWrittenData = Replace(sWrittenData, "{QUANTIDADE}", Copias)
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", Copias)
    sWrittenData = Replace(sWrittenData, "{COD_ETIQ}", "")

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", Copias)
        EtiqEndJob = Replace(EtiqEndJob, "{QUANTIDADE}", Copias)
    End If

End Function



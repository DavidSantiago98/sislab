VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormListagemPedNovaAmostra 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormListagemPedNovaAmostra"
   ClientHeight    =   6135
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7800
   Icon            =   "FormListagemPedNovaAmostra.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6135
   ScaleWidth      =   7800
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkAgruparProven 
      Caption         =   "Agrupar por Proveni�ncia"
      Height          =   255
      Left            =   360
      TabIndex        =   24
      Top             =   5160
      Width           =   2175
   End
   Begin VB.CheckBox CkAgruparSala 
      Caption         =   "Agrupar por Sala/Posto"
      Height          =   255
      Left            =   2640
      TabIndex        =   23
      Top             =   5160
      Width           =   2055
   End
   Begin VB.CheckBox CkAgruparMedico 
      Caption         =   "Agrupar por M�dico"
      Height          =   255
      Left            =   4800
      TabIndex        =   22
      Top             =   5160
      Width           =   2055
   End
   Begin VB.ListBox EcListaTubos 
      Height          =   645
      Left            =   1440
      TabIndex        =   16
      Top             =   1200
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaRapidaTubos 
      Height          =   375
      Left            =   7200
      Picture         =   "FormListagemPedNovaAmostra.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1200
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaProveniencia 
      Height          =   315
      Left            =   7200
      Picture         =   "FormListagemPedNovaAmostra.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   2040
      Width           =   375
   End
   Begin VB.ListBox EcListaProven 
      Height          =   645
      Left            =   1440
      TabIndex        =   13
      Top             =   2040
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   7200
      Picture         =   "FormListagemPedNovaAmostra.frx":0B20
      Style           =   1  'Graphical
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   2760
      Width           =   375
   End
   Begin VB.ListBox EcListaSala 
      Height          =   645
      Left            =   1440
      TabIndex        =   11
      Top             =   2760
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaLocais 
      Height          =   315
      Left            =   7200
      Picture         =   "FormListagemPedNovaAmostra.frx":10AA
      Style           =   1  'Graphical
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   3480
      Width           =   375
   End
   Begin VB.ListBox EcListaLocais 
      Height          =   645
      Left            =   1440
      TabIndex        =   9
      Top             =   3480
      Width           =   5775
   End
   Begin VB.ListBox EcListaMedicos 
      Height          =   645
      Left            =   1440
      TabIndex        =   8
      Top             =   4320
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaMedicos 
      Height          =   315
      Left            =   7200
      Picture         =   "FormListagemPedNovaAmostra.frx":1634
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   4320
      Width           =   375
   End
   Begin VB.Frame Frame3 
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   7575
      Begin VB.ComboBox cbUrgencia 
         Height          =   315
         Left            =   6120
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   315
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   1440
         TabIndex        =   2
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   158990337
         CurrentDate     =   39588
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3120
         TabIndex        =   3
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   158990337
         CurrentDate     =   39588
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   6
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   5
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label4 
         Caption         =   "Urg�ncia"
         Height          =   255
         Index           =   0
         Left            =   5400
         TabIndex        =   4
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Tubos"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   21
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Proveni�ncias"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   20
      Top             =   2040
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Salas / Postos"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   19
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Locais"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   18
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "M�dicos"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   17
      Top             =   4320
      Width           =   1095
   End
End
Attribute VB_Name = "FormListagemPedNovaAmostra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Listagem de Pedidos de Nova Amostra"
    
    Me.Left = 540
    Me.Top = 450
    Me.Width = 7890
    Me.Height = 6555 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de An�lises")
    
    Set FormListagemPedNovaAmostra = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    EcListaTubos.Clear
    EcListaProven.Clear
    EcListaSala.Clear
    EcListaLocais.Clear
    EcListaMedicos.Clear
    CkAgruparMedico.Value = vbUnchecked
    CkAgruparProven.Value = vbUnchecked
    CkAgruparSala.Value = vbUnchecked
    CbUrgencia.ListIndex = mediComboValorNull
    EcDtFim.Value = Bg_DaData_ADO
    EcDtInicio.Value = Bg_DaData_ADO
End Sub

Sub DefTipoCampos()
        
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    EcDtFim.Value = Bg_DaData_ADO
    EcDtInicio.Value = Bg_DaData_ADO
    CkAgruparMedico.Value = vbUnchecked
    CkAgruparProven.Value = vbUnchecked
    CkAgruparSala.Value = vbUnchecked

End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub

Private Sub EclistaTubos_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaTubos, KeyCode, Shift
End Sub

Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProven, KeyCode, Shift
End Sub

Private Sub EclistaSala_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaSala, KeyCode, Shift
End Sub
Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocais, KeyCode, Shift
End Sub

Private Sub EcListaMedicos_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaMedicos, KeyCode, Shift
End Sub



Private Sub BtPesquisaRapidatubos_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_tubo"
    CamposEcran(1) = "cod_tubo"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_tubo"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tubo"
    CWhere = ""
    CampoPesquisa = "descr_tubo"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_tubo ", _
                                                                           " Tubos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(Resultados(i) <> "") Then
                If EcListaTubos.ListCount = 0 Then
                    EcListaTubos.AddItem BL_SelCodigo("sl_tubo", "descr_tubo", "seq_tubo", Resultados(i))
                    EcListaTubos.ItemData(0) = Resultados(i)
                Else
                    EcListaTubos.AddItem BL_SelCodigo("sl_tubo", "descr_tubo", "seq_tubo", Resultados(i))
                    EcListaTubos.ItemData(EcListaTubos.NewIndex) = Resultados(i)
                End If
            End If
        Next i
    End If
End Sub
Private Sub BtPesquisaMedicos_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_medicos"
    CWhere = ""
    CampoPesquisa = "nome_med"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_med ", _
                                                                           " M�dicos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(Resultados(i) <> "") Then
                If EcListaMedicos.ListCount = 0 Then
                    EcListaMedicos.AddItem BL_SelCodigo("sl_medicos", "nome_med", "seq_med", Resultados(i))
                    EcListaMedicos.ItemData(0) = Resultados(i)
                Else
                    EcListaMedicos.AddItem BL_SelCodigo("sl_medicos", "nome_med", "seq_med", Resultados(i))
                    EcListaMedicos.ItemData(EcListaMedicos.NewIndex) = Resultados(i)
                End If
            End If
        Next i
    End If
End Sub


Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                EcListaProven.ItemData(0) = Resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSalaMultiSel EcListaSala
End Sub

Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", Resultados(i))
                EcListaLocais.ItemData(0) = Resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", Resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub
Sub Preenche_Estatistica()
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim nomeReport As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    If CkAgruparSala = vbChecked Then
        nomeReport = "ListagemPedNovaAmostra_Sala"
    ElseIf CkAgruparProven = vbChecked Then
        nomeReport = "ListagemPedNovaAmostra_Proven"
    ElseIf CkAgruparMedico = vbChecked Then
        nomeReport = "ListagemPedNovaAmostra_Medico"
    Else
        nomeReport = "ListagemPedNovaAmostra"
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Estat�stica de An�lises", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Estat�stica de An�lises", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    PreencheTabelaTemporaria
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT  nome_computador, num_sessao, seq_utente, utente,t_utente, dt_nasc_ute, nome_ute, dt_chega, n_req,cod_proven, cod_sala, cod_med, cod_tubo, descr_tubo,descr_motivo, dt_cri, user_cri, hr_cri"
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_ped_nova_amostra WHERE sl_cr_ped_nova_amostra.nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & " AND sl_cr_ped_nova_amostra.num_sessao = " & gNumeroSessao
    
    If CkAgruparMedico.Value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_ped_nova_amostra.nome_med, sl_cr_ped_nova_amostra.dt_cri "
    ElseIf CkAgruparProven.Value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_ped_nova_amostra.descr_proven ASC ,sl_cr_ped_nova_amostra.dt_cri ASC"
    ElseIf CkAgruparSala.Value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_ped_nova_amostra.descr_sala ASC ,sl_cr_ped_nova_amostra.dt_cri ASC"
    Else
        Report.SQLQuery = Report.SQLQuery & " ORDER BY sl_cr_ped_nova_amostra.dt_cri ASC"
    End If
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.Value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Value)
    
    ' TUBOS
    StrTemp = ""
    For i = 0 To EcListaTubos.ListCount - 1
        StrTemp = StrTemp & EcListaTubos.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(2) = "Tubos=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(2) = "Tubos=" & BL_TrataStringParaBD("Todos")
    End If
           
    'PROVENIENCIAS
    StrTemp = ""
    For i = 0 To EcListaProven.ListCount - 1
        StrTemp = StrTemp & EcListaProven.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(3) = "Proven=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(3) = "Proven=" & BL_TrataStringParaBD("Todas")
    End If
    
    'SALAS
    StrTemp = ""
    For i = 0 To EcListaSala.ListCount - 1
        StrTemp = StrTemp & EcListaSala.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(4) = "Salas=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(4) = "Salas=" & BL_TrataStringParaBD("Todas")
    End If
    
    'LOCAIS
    StrTemp = ""
    For i = 0 To EcListaLocais.ListCount - 1
        StrTemp = StrTemp & EcListaLocais.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(5) = "Locais=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(5) = "Locais=" & BL_TrataStringParaBD("Todos")
    End If
    
   
    'AGRUPAR PROVENI�NCIA
    If CkAgruparProven.Value = vbChecked Then
        Report.formulas(6) = "AgruparProven=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(6) = "AgruparProven=" & BL_TrataStringParaBD("N")
    End If
    
    'AGRUPAR SALA
    If CkAgruparSala.Value = vbChecked Then
        Report.formulas(7) = "AgruparSala=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(7) = "AgruparSala=" & BL_TrataStringParaBD("N")
    End If
    
    
    'MEDICOS
    StrTemp = ""
    For i = 0 To EcListaMedicos.ListCount - 1
        StrTemp = StrTemp & EcListaMedicos.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(8) = "MEDICOS=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(8) = "MEDICOS=" & BL_TrataStringParaBD("Todos")
    End If
    
    'AGRUPAR MEDICO
    If CkAgruparMedico.Value = vbChecked Then
        Report.formulas(9) = "AgruparMedico=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(9) = "AgruparMedico=" & BL_TrataStringParaBD("N")
    End If
    
    Call BL_ExecutaReport
 
    
End Sub




Private Sub PreencheTabelaTemporaria()
    Dim sSql As String
    Dim sSqlCONDICOES As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = " DELETE FROM sl_cr_ped_nova_amostra WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
    
    sSql = "INSERT INTO sl_cr_ped_nova_amostra (nome_computador, num_sessao, seq_utente, utente, t_utente, dt_nasc_ute, nome_ute, dt_chega, n_req, "
    sSql = sSql & " cod_proven, descr_proven, cod_sala, descr_sala, cod_med, nome_med, cod_tubo, descr_tubo, descr_motivo, dt_cri, user_cri, hr_cri)"
    sSql = sSql & "SELECT" & BL_TrataStringParaBD(CStr(gComputador)) & "," & gNumeroSessao & "," & " ute.seq_utente, ute.utente, ute.t_utente, ute.dt_nasc_ute, "
    sSql = sSql & " ute.nome_ute, req.dt_chega, req.n_Req, req.cod_proven,descr_proven, req.cod_sala, descr_sala, req.cod_med, nome_med, t.cod_tubo, t.descr_tubo, ped.descr_motivo, ped.dt_cri, ped.user_Cri, ped.hr_cri "
    sSql = sSql & ConstroiFROM
    sSql = sSql & ConstroiWHERE
    sSql = sSql & ConstroiORDER
    If BG_ExecutaQuery_ADO(sSql) = 0 Then
        BG_Mensagem mediMsgBox, "N�o existem registos.", vbCritical, ""
    End If
        
Exit Sub
TrataErro:
    BG_Mensagem mediMsgBox, "N�o � poss�vel gerar a estat�stica de an�lises.", vbCritical, "Erro de Base Dados"
    BG_LogFile_Erros "FormEstatAna - PreencheTabelaTemporaria: " & Err.Number & " (" & Err.Description & ")"
    BG_LogFile_Erros sSql
    Exit Sub
    Resume Next
End Sub

Private Function ConstroiFROM() As String
    Dim sSql As String
    Dim i As Integer
    sSql = ""
    sSql = sSql & " FROM sl_identif ute, sl_requis req, sl_pedido_nova_amostra ped, sl_tubo t "
    

    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_proven proven"
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_proven proven ON req.cod_proven = proven.cod_proven"
    End If

    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_cod_salas sala"
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_cod_salas sala ON req.cod_sala = sala.cod_sala "
    End If
        
    ' ------------------------------------------------------------------------------
    ' M�DICO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_medicos med "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_medicos med ON req.cod_med = med.cod_med "
    End If
    
    
    ConstroiFROM = sSql
End Function


Private Function ConstroiWHERE() As String
    Dim sSql As String
    Dim i As Integer
    sSql = ""
    sSql = sSql & " WHERE ute.seq_utente = req.seq_utente AND req.n_req = ped.n_req "
    sSql = sSql & " AND ped.cod_tubo = t.cod_tubo"
    sSql = sSql & " AND (ped.dt_cri BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Value) & " AND " & BL_TrataDataParaBD(EcDtFim.Value) & " )"
    
    ' ------------------------------------------------------------------------------
    ' SE URGENCIA PREENCHIDA
    ' ------------------------------------------------------------------------------
    If CbUrgencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND req.t_urg = " & BG_DaComboSel(CbUrgencia)
    End If
    
    ' ------------------------------------------------------------------------------
    ' MEDICOS PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & " AND req.cod_med = med.cod_med (+) "
    End If
    
    If EcListaMedicos.ListCount > 0 Then
        sSql = sSql & " AND med.seq_med  IN ("
        For i = 0 To EcListaMedicos.ListCount - 1
            sSql = sSql & EcListaMedicos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & "  AND req.cod_proven= proven.cod_proven  (+) "
    End If
    If EcListaProven.ListCount > 0 Then
        sSql = sSql & " AND proven.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & EcListaProven.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & "  AND req.cod_sala= sala.cod_sala (+) "
    End If
    If EcListaSala.ListCount > 0 Then
        sSql = sSql & " AND sala.seq_sala  IN ("
        For i = 0 To EcListaSala.ListCount - 1
            sSql = sSql & EcListaSala.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        sSql = sSql & " AND req.cod_local IN ("
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ConstroiWHERE = sSql
End Function


Private Function ConstroiGROUPBY() As String
    Dim sSql As String
    If gSGBD = gOracle Then
        sSql = "GROUP BY utente, nome_ute, req.n_req, req.dt_chega, proven.cod_proven,proven.descr_proven, "
        sSql = sSql & "grAna.cod_gr_ana, grAna.descr_gr_ana, sala.cod_sala , sala.descr_sala, ana.cod_ana, ana.descr_ana,"
        sSql = sSql & " ana.peso_estatistico, req.cod_efr, req.n_epis, req.t_sit, res.ord_ana, efr.descr_efr, med.cod_med,med.nome_med "
    End If
    ConstroiGROUPBY = sSql
    
End Function

Private Function ConstroiORDER() As String
    Dim sSql As String
    sSql = ""
    If gSGBD = gOracle Then
        sSql = sSql & " ORDER BY   dt_cri ASC, hr_Cri ASC "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " ORDER BY  PED.dt_cri ASC, PED.hr_cri ASC "
    End If
    ConstroiORDER = sSql

End Function





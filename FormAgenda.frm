VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormAgenda 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8055
   Icon            =   "FormAgenda.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   8055
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbAgenda 
      Height          =   315
      Left            =   1320
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   120
      Width           =   4935
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   7815
      Begin VB.CommandButton Command1 
         Height          =   255
         Left            =   120
         Picture         =   "FormAgenda.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton Command2 
         Height          =   255
         Left            =   6120
         Picture         =   "FormAgenda.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Left            =   1800
         TabIndex        =   5
         Top             =   240
         Width           =   4215
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   960
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   1
      Top             =   8160
      Width           =   495
   End
   Begin MSComctlLib.ListView EcLista 
      Height          =   5415
      Left            =   120
      TabIndex        =   0
      Top             =   1560
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   9551
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   7800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAgenda.frx":0720
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAgenda.frx":6F82
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   " Impress�o de dias agendados "
      Height          =   975
      Left            =   120
      TabIndex        =   6
      Top             =   6960
      Width           =   7815
      Begin MSComCtl2.DTPicker EcDataFinal 
         Height          =   375
         Left            =   4680
         TabIndex        =   8
         Top             =   360
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         _Version        =   393216
         Format          =   147062785
         CurrentDate     =   39538
      End
      Begin MSComCtl2.DTPicker EcDataInicial 
         Height          =   375
         Left            =   1440
         TabIndex        =   9
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   147062785
         CurrentDate     =   39538
      End
      Begin VB.CommandButton BtProcura 
         Height          =   615
         Left            =   6960
         Picture         =   "FormAgenda.frx":D7E4
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Pr�-visualiza Impress�o"
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Data Inicial"
         Height          =   255
         Left            =   3480
         TabIndex        =   11
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Agenda"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "FormAgenda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2005
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const cIconYellow = 1
Const cIconRed = 2

Dim mes As String
Dim Diasmes As Integer
Dim data As Date

Public rs As ADODB.recordset

' pferreira 2008.03.31
Private Sub BtProcura_Click()
    
    If EcDataFinal.value = "" Or EcDataFinal = "" Then
        BG_Mensagem mediMsgBox, "Deve introduzir um intervalo de datas!", vbExclamation, " Impress�o"
        Exit Sub
    End If
    gImprimirDestino = 0
    FuncaoImprimir
    
End Sub


Private Sub CbAgenda_Click()
PreencheAgenda Bg_DaData_ADO
End Sub

' pferreira 2008.10.09
Private Sub Command1_Click()

    Dim ano As Long
    
    If (mes > 1) Then
        mes = CStr(IIf(Len(CStr(mes - 1)) = 1, "0", Empty) & mes - 1)
        ano = Year(data)
    Else
        mes = 12
        ano = Year(data) - 1
    End If
    
    
    ActualizaLabelMes "01-" & mes & "-" & ano
    EcLista.ListItems.Clear
    PreencheAgenda CStr("01-" & mes & "-" & ano)
    
End Sub

' pferreira 2008.10.09
Private Sub Command2_Click()
 
    Dim ano As Long
   
    If (mes < 12) Then
        mes = CStr(IIf(Len(CStr(mes + 1)) = 1, "0", Empty) & mes + 1)
        ano = Year(data)
    Else
        mes = 1
        ano = Year(data) + 1
    End If
    
    ActualizaLabelMes CDate("01-" & mes & "-" & ano)
        EcLista.ListItems.Clear
    PreencheAgenda CStr("01-" & mes & "-" & ano)
    
End Sub

Private Sub EcLista_ItemClick(ByVal item As MSComctlLib.ListItem)
    
    On Error GoTo ErrorHandler
    EcLista.SetFocus
    item.Selected = True
    item.EnsureVisible
ErrorHandler:
   Exit Sub
End Sub

Private Sub EcLista_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    On Error GoTo ErrorHandler
    
    ' Commence sorting
    With EcLista
    
        ' Display the hourglass cursor whilst sorting
        
        Dim lngCursor As Long
        lngCursor = .MousePointer
        .MousePointer = vbHourglass
        
        ' Prevent the ListView control from updating on screen -
        ' this is to hide the changes being made to the listitems
        ' and also to speed up the sort
        
        LockWindowUpdate .hwnd
        
        ' Check the data type of the column being sorted,
        ' and act accordingly
        
        Dim l As Long
        Dim strFormat As String
        Dim strData() As String
        
        Dim lngIndex As Long
        lngIndex = ColumnHeader.Index - 1
    
        Select Case UCase$(ColumnHeader.Tag)
        Case "DATA"
        
            ' Sort by date.
            
            strFormat = "YYYYMMDDHhNnSs"
        
            ' Loop through the values in this column. Re-format
            ' the dates so as they can be sorted alphabetically,
            ' having already stored their visible values in the
            ' tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
            
             
        Case Else
            
        End Select

        ' Unlock the list window so that the OCX can update it
        LockWindowUpdate 0&
        ' Restore the previous cursor
        .MousePointer = lngCursor
        
            
    End With
    Exit Sub
    
ErrorHandler:
    
    LockWindowUpdate 0&
    
End Sub

Private Sub EcLista_DblClick()
    
    If (EcLista.ListItems.Count = 0) Then: Exit Sub
    'If (EcLista.SelectedItem.SmallIcon <> cIconRed) Then
        If gF_REQCONS = mediSim Then
            If CbAgenda.ListIndex > mediComboValorNull Then
                FormGesReqCons.EcDtMarcacao.Text = EcLista.SelectedItem.Text
                FormGesReqCons.EcTAgenda.Text = CbAgenda.ItemData(CbAgenda.ListIndex)
                Unload Me
            End If
        ElseIf gF_REQUIS = mediSim Then
            If CbAgenda.ListIndex > mediComboValorNull Then
                FormGestaoRequisicao.EcDataPrevista.Text = EcLista.SelectedItem.Text
                FormGestaoRequisicao.EcTAgenda.Text = CbAgenda.ItemData(CbAgenda.ListIndex)
                Unload Me
            End If
        End If
    'End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
      
End Sub

Sub Inicializacoes()

    Me.caption = " Agenda "
    Me.left = 540
    Me.top = 450
    Me.Width = 8145
    Me.Height = 8490 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    'Set CampoDeFocus = EcDtIni
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
        
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    

    Set FormAgenda = Nothing
    
    If gF_REQCONS = mediSim Then
        FormGesReqCons.Enabled = True
        Set gFormActivo = FormGesReqCons
    ElseIf gF_REQUIS = mediSim Then
        FormGestaoRequisicao.Enabled = True
        Set gFormActivo = FormGestaoRequisicao
    End If

    
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    EcLista.ListItems.Clear
    CbAgenda.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()

    IniciaLista
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    
    ' pferreira 2008.10.09
    EcDataInicial.value = Bg_DaData_ADO
    EcDataFinal.value = Bg_DaData_ADO
    BG_PreencheComboBD_ADO "sl_t_agenda", "cod_t_agenda", "descr_t_agenda", CbAgenda
    
End Sub

Sub PreencheAgenda(DtMarcacao As String, Optional TAgenda As String)
    
    Dim lngCursor As Long
    
    lngCursor = EcLista.MousePointer
    EcLista.MousePointer = vbHourglass
    LockWindowUpdate EcLista.hwnd
    EcLista.ListItems.Clear
    ActualizaLabelMes BL_HandleNull(DtMarcacao, Bg_DaData_ADO)
    InicializaLista DtMarcacao
    ActualizaLista
    If (EcLista.ListItems.Count > 0 And TAgenda <> Empty) Then: EcLista_ItemClick EcLista.FindItem(data, , 1, lvwWhole)
    If (EcLista.ListItems.Count > 0 And TAgenda = Empty) Then: EcLista_ItemClick EcLista.FindItem(EcLista.ListItems(1).Text, , 1, lvwWhole)
    LockWindowUpdate 0&
    EcLista.MousePointer = lngCursor
            
End Sub

Private Sub InicializaLista(DtMarcacao As String)
    Dim sql As String
    Dim rsAgenda As ADODB.recordset
    Dim i As Integer
    data = Empty
    Set rsAgenda = New ADODB.recordset
    rsAgenda.CursorLocation = adUseServer
    rsAgenda.CursorType = adOpenStatic
    For i = 1 To Diasmes
        If gSGBD = gOracle Then
            sql = "select * from sl_agenda_indisponiveis where dia_indisp = " & _
                  BL_TrataDataParaBD(Format$(i & "-" & mes & "-" & Year(DtMarcacao), "dd-mm-yyyy")) & _
                  " and ( TO_CHAR (dia_indisp, 'D') <> '7' or TO_CHAR (dia_indisp, 'D') <> '8')"
            rsAgenda.Open sql, gConexao
            If (rsAgenda.RecordCount = 0) Then
                data = CDate(Format$(i & "-" & mes & "-" & Year(DtMarcacao), "dd-mm-yyyy"))
                With EcLista.ListItems.Add(, Format$(i & "-" & mes & "-" & Year(DtMarcacao), "dd-mm-yyyy"), Format$(i & "-" & mes & "-" & Year(DtMarcacao), "dd-mm-yyyy"))
                    .ListSubItems.Add , , DiaSemana(Weekday(Format$(i & "-" & mes & "-" & Year(DtMarcacao), "dd-mm-yyyy")))
                    .ListSubItems.Add , , Empty
                    .ListSubItems.Add , , Empty
                    .ListSubItems.Add , , Empty
                End With
            End If
            If (rsAgenda.state = adStateOpen) Then: rsAgenda.Close
        End If
    Next

End Sub

Private Sub IniciaLista()
           
    With EcLista
        .ColumnHeaders.Add(, , "Data", 1500, lvwColumnLeft).Tag = "DATA"
        .ColumnHeaders.Add(, , "Dia", 2000, lvwColumnLeft).Tag = "DIA"
        .ColumnHeaders.Add(, , "Agenda", 2000, lvwColumnLeft).Tag = "AGENDA"
        .ColumnHeaders.Add(, , "Marca��es", 1020, lvwColumnLeft).Tag = "MARCACOES"
        .ColumnHeaders.Add(, , "Limite", 1020, lvwColumnLeft).Tag = "LIMITE"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .SmallIcons = ImageList1
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = False
        .MultiSelect = False
    End With
    SetListViewColor EcLista, Picture1, RGB(253, 245, 230), vbWhite
 
End Sub

Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Function DiaSemana(dia As Integer) As String
    
    Select Case (dia)
        Case vbSunday: DiaSemana = "Domingo"
        Case vbMonday: DiaSemana = "Segunda"
        Case vbTuesday: DiaSemana = "Ter�a"
        Case vbWednesday: DiaSemana = "Quarta"
        Case vbThursday: DiaSemana = "Quinta"
        Case vbFriday: DiaSemana = "Sexta"
        Case vbSaturday: DiaSemana = "S�bado"
        Case Else: ' Empty
    End Select
    
End Function

Private Sub ActualizaLabelMes(data As Date)
    If mes = "" Then mes = Month(data)
    Select Case (CInt(mes))
        Case 1: Label1.caption = " Janeiro " & Year(data): Diasmes = 31
        Case 2: Label1.caption = " Fevereiro " & Year(data): Diasmes = IIf(Year(data) Mod 4 = 0, 29, 28)
        Case 3: Label1.caption = " Mar�o " & Year(data): Diasmes = 31
        Case 4: Label1.caption = " Abril " & Year(data): Diasmes = 30
        Case 5: Label1.caption = " Maio " & Year(data): Diasmes = 31
        Case 6: Label1.caption = " Junho " & Year(data): Diasmes = 30
        Case 7: Label1.caption = " Julho " & Year(data): Diasmes = 31
        Case 8: Label1.caption = " Agosto " & Year(data): Diasmes = 31
        Case 9: Label1.caption = " Setembro " & Year(data): Diasmes = 30
        Case 10: Label1.caption = " Outubro " & Year(data): Diasmes = 31
        Case 11: Label1.caption = " Novembro " & Year(data):: Diasmes = 30
        Case 12: Label1.caption = " Dezembro " & Year(data): Diasmes = 31
        Case Else:  Label1.caption = " Janeiro " & Year(data): Diasmes = 31
    End Select
    
End Sub

' pferreira 2008.10.09
Private Sub ActualizaLista()

    Dim vItem As Variant
    Dim sql As String
    Dim rsAgenda As ADODB.recordset
    Dim marcacoes As Integer
    Dim limite As Integer
    Dim bBold As Boolean
    Dim Icon As Integer
    Dim toolTip As String
    Dim descrAgenda As String
    Dim FColor As Long
    Dim limiteAgenda As String
    'EcLista.ListItems.Clear
    
    If (EcLista.ListItems.Count = 0) Then: Exit Sub
    If CbAgenda.ListIndex = mediComboValorNull Then Exit Sub
    
    Set rsAgenda = New ADODB.recordset
    rsAgenda.CursorLocation = adUseServer
    rsAgenda.CursorType = adOpenStatic
    sql = "select * from sl_t_agenda where cod_t_agenda = " & BL_TrataStringParaBD(CbAgenda.ItemData(CbAgenda.ListIndex))
    rsAgenda.Open sql, gConexao
    If (rsAgenda.RecordCount > 0) Then: descrAgenda = BL_HandleNull(rsAgenda!descr_t_agenda, ""): limiteAgenda = BL_HandleNull(rsAgenda!limite, 0)
    If (rsAgenda.state = adStateOpen) Then: rsAgenda.Close
    For Each vItem In EcLista.ListItems
        sql = " SELECT dt_marcacao,descr_t_agenda,limite, count(*) marcacoes " & _
              " FROM sl_t_agenda, sl_agenda " & _
              " WHERE sl_t_agenda.cod_t_agenda = sl_agenda.cod_t_agenda and " & _
              " sl_t_agenda.cod_t_agenda = " & BL_TrataStringParaBD(CbAgenda.ItemData(CbAgenda.ListIndex)) & " and dt_marcacao = " & BL_TrataDataParaBD(vItem.Text) & _
              " and sl_agenda.cod_local = " & gCodLocal & _
              " and dt_marcacao not in (select dia_indisp from sl_agenda_indisponiveis where user_cri = " & gCodUtilizador & ")" & _
              " group by dt_marcacao, descr_t_agenda, limite " & _
              " order by dt_marcacao "
        rsAgenda.Open sql, gConexao
        If (rsAgenda.RecordCount > 0) Then
            marcacoes = BL_HandleNull(rsAgenda!marcacoes, 0)
            limite = BL_HandleNull(rsAgenda!limite, 0)
            If (marcacoes >= limite) Then
                FColor = vbRed
                bBold = False
                Icon = cIconRed
                toolTip = "N�o pode marcar mais consultas nesta data!"
            ElseIf (marcacoes >= Abs(limite) - Abs(5)) Then
                FColor = vbBlack
                Icon = cIconRed
                bBold = False
                toolTip = "S� pode marcar mais " & Abs(limite - marcacoes) & " consultas nesta data!"
            ElseIf (marcacoes >= Abs(limite) - Abs(10)) Then
                FColor = vbBlack
                Icon = cIconYellow
                bBold = False
                toolTip = "S� pode marcar mais " & Abs(limite - marcacoes) & " consultas nesta data!"
            ElseIf (Weekday(rsAgenda!dt_marcacao) = vbSaturday Or Weekday(rsAgenda!dt_marcacao) = vbSunday) Then
                FColor = vbBlack
                Icon = Empty
                bBold = True
                toolTip = "Esta marca��o est� efectuada a um " & DiaSemana(Weekday(rsAgenda!dt_marcacao)) & "!"
            Else
                FColor = vbBlack
                Icon = Empty
                bBold = False
                toolTip = "Esta marca��o est� efectuada a uma " & DiaSemana(Weekday(rsAgenda!dt_marcacao)) & "!"
            End If
            
            vItem.SmallIcon = Icon: vItem.ForeColor = FColor: vItem.Bold = bBold
            vItem.ListSubItems(1).ToolTipText = toolTip: vItem.ListSubItems(1).ForeColor = FColor: vItem.ListSubItems(1).Bold = bBold
            vItem.ListSubItems(2).ToolTipText = toolTip: vItem.ListSubItems(2).Text = BL_HandleNull(rsAgenda!descr_t_agenda, ""): vItem.ListSubItems(2).ForeColor = FColor:: vItem.ListSubItems(2).Bold = bBold
            vItem.ListSubItems(3).ToolTipText = toolTip: vItem.ListSubItems(3).Text = BL_HandleNull(rsAgenda!marcacoes, 0): vItem.ListSubItems(3).ForeColor = FColor: vItem.ListSubItems(3).Bold = bBold
            vItem.ListSubItems(4).ToolTipText = toolTip: vItem.ListSubItems(4).Text = BL_HandleNull(rsAgenda!limite, 0): vItem.ListSubItems(4).ForeColor = FColor: vItem.ListSubItems(4).Bold = bBold
            
        Else
            If (Weekday(vItem.Text) = vbSaturday Or Weekday(vItem.Text) = vbSunday) Then
                Icon = Empty
                bBold = True
                toolTip = "Esta marca��o est� efectuada a um " & DiaSemana(Weekday(vItem.Text)) & "!"
            Else
                Icon = Empty
                bBold = False
                toolTip = "Esta marca��o est� efectuada a uma " & DiaSemana(Weekday(vItem.Text)) & "!"
            End If
            vItem.SmallIcon = Icon: vItem.Bold = bBold
            vItem.ListSubItems(1).ToolTipText = toolTip:: vItem.ListSubItems(1).Bold = bBold
            vItem.ListSubItems(2).ToolTipText = toolTip: vItem.ListSubItems(2).Text = descrAgenda: vItem.ListSubItems(2).Bold = bBold
            vItem.ListSubItems(3).ToolTipText = toolTip: vItem.ListSubItems(3).Text = 0: vItem.ListSubItems(3).Bold = bBold
            vItem.ListSubItems(4).ToolTipText = toolTip: vItem.ListSubItems(4).Text = limiteAgenda: vItem.ListSubItems(4).Bold = bBold
        End If
        If (rsAgenda.state = adStateOpen) Then: rsAgenda.Close
    Next
End Sub

' pferreira 2008.10.09
Public Sub FuncaoImprimir()
    
    Dim sql As String
    Dim continua As Boolean
          
    On Error Resume Next
    sql = "delete from sl_cr_agenda"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    sql = "insert into sl_cr_agenda (cod_t_agenda, dt_marcacao, marcacoes, n_req,"
    sql = sql & " flg_activo, t_utente, utente, nome) select ag.cod_t_agenda, ag.dt_ini, ag.marcacoes, ag.n_req, ag.flg_activo, "
    sql = sql & " id.t_utente, id.utente, id.nome_ute from sl_agenda ag, sl_requis_consultas rc, sl_identif id where ag.dt_marcacao between" & _
          BL_TrataDataParaBD(EcDataInicial.value) & " and " & BL_TrataDataParaBD(EcDataFinal.value) & _
          " AND tipo_req = 'C' "
    sql = sql & " AND ag.n_Req = rc.n_req and rc.seq_utente  = id.seq_utente "
    If CbAgenda.ListIndex > mediComboValorNull Then
        sql = sql & " AND cod_t_agenda = " & CbAgenda.ItemData(CbAgenda.ListIndex)
    End If
    'sql = sql & ")"
    BG_ExecutaQuery_ADO sql
    
    sql = "insert into sl_cr_agenda (cod_t_agenda, dt_marcacao, marcacoes, n_req,"
    sql = sql & " flg_activo, t_utente, utente, nome) select ag.cod_t_agenda, ag.dt_ini, ag.marcacoes, ag.n_req, ag.flg_activo, "
    sql = sql & " id.t_utente, id.utente, id.nome_ute from sl_agenda ag, sl_requis rc, sl_identif id where ag.dt_marcacao between" & _
          BL_TrataDataParaBD(EcDataInicial.value) & " and " & BL_TrataDataParaBD(EcDataFinal.value) & _
          "  AND tipo_req = 'R' "
    sql = sql & " AND ag.n_Req = rc.n_req and rc.seq_utente  = id.seq_utente "
    If CbAgenda.ListIndex > mediComboValorNull Then
        sql = sql & " AND cod_t_agenda = " & CbAgenda.ItemData(CbAgenda.ListIndex)
    End If
    'sql = sql & ")"
    BG_ExecutaQuery_ADO sql
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("Agenda", " Dias Agendados", crptToPrinter)
    Else
        continua = BL_IniciaReport("Agenda", " Dias Agendados", crptToWindow)
    End If
    
    If continua = False Then
        Exit Sub
    End If
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Me.SetFocus
    Report.Connect = "DSN=" & gDSN
    Call BL_ExecutaReport
    Report.Reset
    Exit Sub
End Sub

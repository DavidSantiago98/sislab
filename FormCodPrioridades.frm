VERSION 5.00
Begin VB.Form FormCodPrioridades 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Codifica��o de Prioridades"
   ClientHeight    =   5670
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7200
   Icon            =   "FormCodPrioridades.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5670
   ScaleWidth      =   7200
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAjuda2 
      Height          =   450
      Left            =   6600
      Picture         =   "FormCodPrioridades.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   29
      ToolTipText     =   " Ajuda na constru��o da f�rmula "
      Top             =   120
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.Frame Frame1 
      Caption         =   " Ajuda "
      Height          =   2415
      Left            =   240
      TabIndex        =   27
      Top             =   1320
      Visible         =   0   'False
      Width           =   6885
      Begin VB.Label LbAjuda 
         Caption         =   "Label2"
         Height          =   1935
         Left            =   120
         TabIndex        =   28
         Top             =   360
         Visible         =   0   'False
         Width           =   6615
      End
   End
   Begin VB.CommandButton BtAjuda1 
      Height          =   450
      Left            =   6600
      Picture         =   "FormCodPrioridades.frx":0776
      Style           =   1  'Graphical
      TabIndex        =   26
      ToolTipText     =   " Ajuda na constru��o da f�rmula "
      Top             =   120
      Width           =   450
   End
   Begin VB.ComboBox CbCodigo 
      Height          =   315
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   615
   End
   Begin VB.TextBox EcNome 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   4080
      TabIndex        =   2
      Top             =   120
      Width           =   2415
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      ItemData        =   "FormCodPrioridades.frx":0EE0
      Left            =   240
      List            =   "FormCodPrioridades.frx":0EE2
      TabIndex        =   15
      Top             =   1320
      Width           =   6855
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6600
      TabIndex        =   14
      Top             =   4320
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1080
      MaxLength       =   100
      TabIndex        =   3
      Top             =   720
      Width           =   6015
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   13
      Top             =   4800
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4320
      TabIndex        =   12
      Top             =   4800
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   11
      Top             =   4440
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4320
      TabIndex        =   10
      Top             =   4440
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   240
      TabIndex        =   0
      Top             =   3480
      Width           =   6885
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   7
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   6
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   5
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   4
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Label Label13 
      Caption         =   "Nome"
      Height          =   225
      Left            =   3360
      TabIndex        =   25
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2880
      TabIndex        =   24
      Top             =   4800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2880
      TabIndex        =   23
      Top             =   4440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   4800
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   4440
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   5040
      TabIndex        =   20
      Top             =   4320
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   225
      Left            =   240
      TabIndex        =   18
      Top             =   120
      Width           =   615
   End
   Begin VB.Label LbNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label LbDescricao 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   2760
      TabIndex        =   16
      Top             =   1080
      Width           =   855
   End
End
Attribute VB_Name = "FormCodPrioridades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''
' Actualiza��o: 23/03/2007 '''''''''''''''''
' T�cnico: Paulo Ferreira ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de indices ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CI_SEQ_PRIO = 0
Private Const CI_COD_PRIO = 1
Private Const CI_NOME_PRIO = 2
Private Const CI_DESCR_PRIO = 3
Private Const CI_USER_CRI = 4
Private Const CI_DT_CRI = 5
Private Const CI_USER_ACT = 6
Private Const CI_DT_ACT = 7

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de estado do ecra '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CS_INACTIVE = 0
Private Const CS_ACTIVE = 1
Private Const CS_WAIT = 2

''''''''''''''''''''''''''''''''''''''''''''
' Variaveis de BD ''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String
Dim ChaveBD As String
Dim ChaveEc As Object
Dim NomeTabela As String
Dim CriterioTabela As String
Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean
Dim rs As ADODB.recordset

''''''''''''''''''''''''''''''''''''''''''''
' Variaveis do ecra ''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim estado As Integer
Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim MarcaInicial As Variant
Dim bAjuda As Boolean

''''''''''''''''''''''''''''''''''''''''''''
' Evento do botao ajuda 1 ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtAjuda1_Click()
    
    If (bAjuda = False) Then
        bAjuda = True
        BtAjuda1.Visible = False
        BtAjuda2.Visible = True
        LbNome.Visible = False
        LbDescricao.Visible = False
        EcLista.Visible = False
        Frame1.Visible = True
        Frame2.Visible = False
        LbAjuda.Visible = True
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento do botao ajuda 1 ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtAjuda2_Click()
    
    If (bAjuda = True) Then
        bAjuda = False
        BtAjuda1.Visible = True
        BtAjuda2.Visible = False
        LbNome.Visible = True
        LbDescricao.Visible = True
        EcLista.Visible = True
        Frame1.Visible = False
        Frame2.Visible = True
        LbAjuda.Visible = False
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Valida campo codigo ''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub CbCodigo_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Valida campo descricao '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcDescricao_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de 'click' da lista '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcLista_Click()
    
    ' Se a lista contiver dados preeche campos do ecra
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de 'focus' da combobox codigo '''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub CbCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de 'lostfocus' da combobox codigo '
''''''''''''''''''''''''''''''''''''''''''''
'Private Sub CbCodigo_LostFocus()
'    CbCodigo.text = UCase(CbCodigo.text)
'
'End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de 'focus' do campo descricao '''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Load do ecra '''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Load()
    EventoLoad
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Activate do ecra '''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Activate()
    EventoActivate
End Sub
''''''''''''''''''''''''''''''''''''''''''''
' Unload do ecra '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Inicializacoes do ecra '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Inicializacoes()

    ' Definicoes do ecra
    Me.caption = " Codifica��o de Prioridades"
    Me.left = 540
    Me.top = 450
    Me.Width = 7290
    Me.Height = 4845
        
    ' Definicoes da BD
    NomeTabela = "sl_cod_prioridades"
    Set CampoDeFocus = CbCodigo
    NumCampos = 8
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(CI_SEQ_PRIO) = "seq_prio"
    CamposBD(CI_COD_PRIO) = "cod_prio"
    CamposBD(CI_NOME_PRIO) = "nome_prio"
    CamposBD(CI_DESCR_PRIO) = "descr_prio"
    CamposBD(CI_USER_CRI) = "user_cri"
    CamposBD(CI_DT_CRI) = "dt_cri"
    CamposBD(CI_USER_ACT) = "user_act"
    CamposBD(CI_DT_ACT) = "dt_act"
        
    ' Campos do Ecr�
    Set CamposEc(CI_SEQ_PRIO) = EcCodSequencial
    Set CamposEc(CI_COD_PRIO) = CbCodigo
    Set CamposEc(CI_NOME_PRIO) = EcNome
    Set CamposEc(CI_DESCR_PRIO) = EcDescricao
    Set CamposEc(CI_USER_CRI) = EcUtilizadorCriacao
    Set CamposEc(CI_DT_CRI) = EcDataCriacao
    Set CamposEc(CI_USER_ACT) = EcUtilizadorAlteracao
    Set CamposEc(CI_DT_ACT) = EcDataAlteracao
        
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(CI_COD_PRIO) = "C�digo"
    TextoCamposObrigatorios(CI_NOME_PRIO) = "Nome da Prioridade"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_prio"
    Set ChaveEc = CbCodigo
    CamposBDparaListBox = Array("cod_prio", "nome_prio")
    NumEspacos = Array(22, 32)
    BG_PreencheCombo Array(1, 1, 2, 2, 3, 3, 4, 4), CbCodigo
    
    ' Ajuda
    bAjuda = False
    PreencheAjuda
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento load do ecra ''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    ' Carrega ecra
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    estado = CS_ACTIVE
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento activate do ecra ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoActivate()

    ' Activa ecra
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_ToolbarEstadoN estado
    If (estado = CS_WAIT) Then: BL_Toolbar_BotaoEstado "Remover", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento unload do ecra ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoUnload()

    ' Desactiva ecra
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN CS_INACTIVE
    If (Not rs Is Nothing) Then
        If (rs.state = adStateOpen) Then: rs.Close
        Set rs = Nothing
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Limpa campos do ecra '''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub LimpaCampos()

    Me.SetFocus
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Define tipo de campos do ecra ''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Preenche campos do ecra ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub PreencheCampos()
    
    Me.SetFocus
    If (rs.RecordCount = 0) Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao limpar do ecra ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoLimpar()
        
    ' Se estado for de espera passa para o estado anterior
    If (estado = CS_WAIT) Then: FuncaoEstadoAnterior
    
    ' Se o estado for diferente de espera limpa os campos do ecra
    If (estado <> CS_WAIT) Then: LimpaCampos: CampoDeFocus.SetFocus
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao limpar do ecra ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoEstadoAnterior()

    If (estado = CS_WAIT) Then
        estado = CS_ACTIVE
        BL_ToolbarEstadoN estado
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then: rs.Close: Set rs = Nothing
    Else
        Unload Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que valida campos EC ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Function ValidaCamposEc() As Integer
    
    Dim iRes
    Dim i As Integer
    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    ValidaCamposEc = True
End Function

''''''''''''''''''''''''''''''''''''''''''''
' Funcao procurar do ecra ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY " & ChaveBD & " ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = CS_WAIT
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao registo anterior do ecra ''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao registo seguinte do ecra ''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoSeguinte()

    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao inserir do ecra '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoInserir()

    Dim iRes As Integer
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que insere um registo na BD '''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BD_Insert()
    
    Dim SQLQuery As String
    gSQLError = 0
    gSQLISAM = 0
    
    BG_ExecutaQuery_ADO "DELETE FROM " & NomeTabela & " WHERE cod_prio = " & CamposEc(0)
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_prio") + 1
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    BG_Trata_BDErro
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao modificar do ecra '''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoModificar()

    Dim iRes As Integer
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que actualiza um registo na BD ''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
        
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao remover do ecra '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoRemover()

    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que remove um registo na BD '''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant

    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & ChaveBD & "=" & BG_DaComboSel(CbCodigo)
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que escreve ajuda no ecra '''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub PreencheAjuda()
    
    LbAjuda.Font.Bold = True
    LbAjuda.Font.Size = 4
    LbAjuda.caption = "   � necess�rio definir SEMPRE quatro prioridades:" & vbCrLf & vbCrLf & _
    "       Codigo 1: Prioridade desconhecida;" & vbCrLf & _
    "       Codigo 2: Prioridade normal;" & vbCrLf & _
    "       Codigo 3: Prioridade alta;" & vbCrLf & _
    "       Codigo 4: Prioridade muito alta." & vbCrLf & vbCrLf & _
    "   As prioridades devem ser definidas de acordo com o codigo, ou seja, " & vbCrLf & _
    "   quanto maior o codigo maior o grau de prioridade."
End Sub

VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClRegRecibosAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Dados para recibos
Public ReciboPerfilMarcacao As Variant 'usado apenas no publico
Public ReciboCodAna As Variant  'usado apenas no privado
Public ReciboCodFacturavel As Variant  'usado apenas no privado
Public ReciboEntidade As String
Public ReciboCodEfrEnvio As String
Public ReciboDescrEntidade As String
Public ReciboCodAnaEFR As Variant
Public ReciboDescrAnaEFR As Variant
Public ReciboP1 As Variant
Public ReciboCorP1 As Variant
Public ReciboTaxa As Variant
Public ReciboTaxaUnitario As Variant
Public ReciboValorEFR As Variant
Public ReciboValorUnitEFR As Variant
Public ReciboNumDoc As String
Public ReciboSerieDoc As String
Public ReciboUserCri As String
Public ReciboDtCri As String
Public ReciboHrCri As String
Public ReciboUserEmi As String
Public ReciboDtEmi As String
Public ReciboHrEmi As String
Public ReciboFlgImpresso As String
Public ReciboFlgFacturado As String
Public ReciboFlgAdicionado As String
Public ReciboFlgRetirado As String
Public ReciboFlgMarcacaoRetirada As String
Public ReciboFlgReciboManual As String
Public ReciboIsencao As String
Public ReciboOrdemMarcacao As Long
Public ReciboCodMedico As String
Public ReciboQuantidade As Integer
Public ReciboQuantidadeEFR As Integer
Public ReciboAnaNaoFact As String
Public ReciboNrBenef As String
Public ReciboFDS As String
Public ReciboCodigoPai As String
Public ReciboFlgAutomatico As String
Public ReciboSeqRecibo As Long
Public flgCodBarras As Integer


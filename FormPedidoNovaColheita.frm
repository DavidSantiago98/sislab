VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormPedidoNovaAmostra 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8610
   Icon            =   "FormPedidoNovaColheita.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5415
   ScaleWidth      =   8610
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcObsReq 
      Height          =   285
      Left            =   3120
      TabIndex        =   26
      Top             =   6600
      Width           =   1095
   End
   Begin VB.TextBox EcPesqRapTubo 
      Height          =   285
      Left            =   1440
      TabIndex        =   24
      Top             =   6480
      Width           =   1095
   End
   Begin VB.TextBox EcEstadoReq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3600
      TabIndex        =   12
      Top             =   5880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcSeqUtente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   11
      Top             =   5880
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5160
      TabIndex        =   1
      Top             =   5880
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Height          =   5175
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8355
      Begin VB.CheckBox CkApagarMarcadas 
         Caption         =   "Apagar An�lises Marcadas"
         Height          =   255
         Left            =   1200
         TabIndex        =   27
         Top             =   4800
         Width           =   3855
      End
      Begin VB.CommandButton BtPedidoNovaAmostra 
         Height          =   495
         Left            =   7560
         Picture         =   "FormPedidoNovaColheita.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Gerar pedido de nova amostra"
         Top             =   4440
         Width           =   615
      End
      Begin VB.TextBox EcDataChegada 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7080
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   240
         Width           =   1095
      End
      Begin VB.Frame Frame3 
         Caption         =   "Amostra a pedir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2415
         Left            =   120
         TabIndex        =   17
         Top             =   1800
         Width           =   8175
         Begin MSFlexGridLib.MSFlexGrid FgTubo 
            Height          =   1875
            Left            =   75
            TabIndex        =   18
            Top             =   330
            Width           =   8025
            _ExtentX        =   14155
            _ExtentY        =   3307
            _Version        =   393216
            BackColorBkg    =   -2147483633
            GridLines       =   0
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label LaProduto 
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   2040
            TabIndex        =   25
            Top             =   120
            Width           =   3975
         End
      End
      Begin VB.TextBox EcMotivoNovaAmostra 
         Height          =   315
         Left            =   1200
         TabIndex        =   15
         Top             =   4380
         Width           =   6015
      End
      Begin VB.ComboBox CbEstadoReq 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   315
         Left            =   2640
         Style           =   1  'Simple Combo
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   240
         Width           =   3495
      End
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H002C7124&
         Height          =   315
         Left            =   1440
         TabIndex        =   8
         Top             =   240
         Width           =   1215
      End
      Begin VB.Frame Frame2 
         Caption         =   "Dados do Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   975
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   8175
         Begin VB.TextBox EcDataNasc 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6960
            TabIndex        =   19
            TabStop         =   0   'False
            ToolTipText     =   "Data de Nascimento"
            Top             =   240
            Width           =   1095
         End
         Begin VB.TextBox EcNome 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1320
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   600
            Width           =   6735
         End
         Begin VB.TextBox EcUtente 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   4
            Top             =   240
            Width           =   3495
         End
         Begin VB.ComboBox CbTipoUtente 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "FormPedidoNovaColheita.frx":08D6
            Left            =   1320
            List            =   "FormPedidoNovaColheita.frx":08D8
            Locked          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label LbData 
            AutoSize        =   -1  'True
            Caption         =   "Data Nasc."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   6120
            TabIndex        =   20
            ToolTipText     =   "Data de Nascimento"
            Top             =   240
            Width           =   795
         End
         Begin VB.Label LbNome 
            AutoSize        =   -1  'True
            Caption         =   "Nome"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   240
            TabIndex        =   7
            Top             =   600
            Width           =   405
         End
         Begin VB.Label LbNrDoe 
            AutoSize        =   -1  'True
            Caption         =   "&Utente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   240
            TabIndex        =   6
            Top             =   240
            Width           =   465
         End
      End
      Begin VB.Label LaDtChegada 
         Caption         =   "Dt &chegada"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   6240
         TabIndex        =   22
         Top             =   225
         Width           =   855
      End
      Begin VB.Label Label9 
         Caption         =   "Motivo de Nova Amostra"
         Height          =   495
         Left            =   120
         TabIndex        =   16
         Top             =   4260
         Width           =   1095
      End
      Begin VB.Label LbNumero 
         AutoSize        =   -1  'True
         Caption         =   "&Requisi��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   900
      End
   End
   Begin VB.Label Label31 
      Caption         =   "EcEstadoReq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2520
      TabIndex        =   14
      Top             =   5880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label19 
      Caption         =   "EcSeqUtente"
      Enabled         =   0   'False
      Height          =   255
      Left            =   600
      TabIndex        =   13
      Top             =   5880
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "FormPedidoNovaAmostra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 19/04/2006
' T�cnico Sandra Oliveira
' Vari�veis Globais para este Form.

Dim estado As Integer
Dim CamposBDparaListBox
Dim NumEspacos
Dim CampoDeFocus As Object

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Public rs As ADODB.recordset

'Estrutura que guarda tubos
Private Type Tubos
    seq_req_tubo As Long
    n_req As Long
    CodProduto As String
    DescrProduto As String
    CodTubo As String
    descrTubo As String
    dt_previ As String
    dt_chega As String
    hr_chega As String
    dt_eliminacao As String
    hr_eliminacao As String
    estado As String
    Motivo As String
    flg_Apagar As Boolean
End Type
Dim estrutTubos() As Tubos
Dim totalTubos As Integer


Private Sub BtPedidoNovaamostra_Click()
    Dim codAna() As String
    Dim iRes As Boolean
    Dim sql As String
    Dim i As Integer
    Dim req As Long
    Dim flg_apaga As Boolean
    Dim mensagem As String
    Dim seq_req_tubo As Long
    mensagem = ""
    On Error GoTo Trata_Erro
    iRes = ValidaCamposEc
    If iRes = True Then
        
        ' CAMPOS OBRIGAT�RIOS PARA APAGAR TUBOS
        ' -----------------------------------------------------------
        If EcMotivoNovaAmostra.Text = "" Then
            BG_Mensagem mediMsgBox, "Campo obrigat�rio: Motivo pedido de nova amostra !", vbError, "SISLAB"
            Exit Sub
        End If
        
        If EcDataChegada = "" Then
            BG_Mensagem mediMsgBox, "Requisi��o sem data de chegada !", vbError, "SISLAB"
            Exit Sub
        End If
        
        
        For i = 1 To totalTubos
            If estrutTubos(i).flg_Apagar = True And estrutTubos(i).dt_eliminacao = "" Then
                flg_apaga = True
                
                ' VERIFICA SE ESTAO REUNIDAS TODAS CONDICOES PARA APAGAR TUBO
                ' -----------------------------------------------------------
                If estrutTubos(i).CodTubo = "" Then
                    BG_Mensagem mediMsgBox, "Campo obrigat�rio: Tubo !", vbError, "SISLAB"
                    estrutTubos(i).flg_Apagar = False
                    flg_apaga = False
                ElseIf estrutTubos(i).dt_chega = "" Or EcDataChegada.Text = "" Then
                    BG_Mensagem mediMsgBox, "N�o � poss�vel pedir nova amostra de um tubo em falta! " & estrutTubos(i).descrTubo, vbError, "SISLAB"
                    estrutTubos(i).flg_Apagar = False
                    flg_apaga = False
                ElseIf estrutTubos(i).DescrProduto = "" Then
                    BG_Mensagem mediMsgBox, "Obrigat�ria a identifica��o do produto associado ao tubo! " & estrutTubos(i).descrTubo, vbError, "SISLAB"
                    estrutTubos(i).flg_Apagar = False
                    flg_apaga = False
                End If
                
                ' SE PUDER APAGAR TUBO
                ' -----------------------------------------------------------
                If flg_apaga = True Then
                    If estrutTubos(i).estado = "Cancelado" Then
                        'If BG_Mensagem(mediMsgBox, "Pedido j� efectuado a " & estrutTubos(i).dt_eliminacao & " " & estrutTubos(i).hr_eliminacao & "." & vbCrLf & _
                                                "Deseja emitir de novo o documento?", vbYesNo + vbInformation) = vbYes Then
                                                
                        '    Call Emitir_PedidoNovaAmostra(estrutTubos(i).descrProduto, EcMotivoNovaAmostra, gRequisicaoActiva)
                        'End If
                    Else
                        
                        '   CANCELAMENTO DO TUBO
                        '   ----------------------
                        gMsgTitulo = "Pedido de nova amostra"
                        gMsgMsg = "Deseja pedir nova tubo de " & estrutTubos(i).descrTubo & " pelo motivo de " & EcMotivoNovaAmostra & " ?" & vbCrLf
                        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                        If gMsgResp = vbYes Then
                        
                            
                            gSQLError = 0
                            gSQLISAM = 0
                            BG_BeginTransaction
                    
                            ' GUARDA OBSERVACAO NA TABELA DE REQUISI��ES
                            ' -----------------------------------------------------------
                            If gSGBD = gOracle Then
                                sql = "update sl_requis set obs_req = substr(obs_req || '" & EcObsReq.Text & " " & " Nova amostra de " & estrutTubos(i).DescrProduto & " pedida a " & Bg_DaData_ADO & " pelo motivo " & EcMotivoNovaAmostra & ".',1,400)" & " where n_req = " & estrutTubos(i).n_req
                            ElseIf gSGBD = gSqlServer Then
                                sql = "update sl_requis set obs_req = substring(obs_req + '" & EcObsReq.Text & " " & " Nova amostra de " & estrutTubos(i).DescrProduto & " pedida a " & Bg_DaData_ADO & " pelo motivo " & EcMotivoNovaAmostra & ".',1,400)" & " where n_req = " & estrutTubos(i).n_req
                            End If
                            BG_ExecutaQuery_ADO sql
                            If gSQLError <> 0 Then
                                mensagem = "Erro a gravar obs da requisi��o actual !" & gSQLError
                                GoTo Trata_Erro
                            End If
                            
                            ' CANCELA O TUBO CORRENTE
                            ' -----------------------------------------------------------
                            estrutTubos(i).dt_eliminacao = Bg_DaData_ADO
                            estrutTubos(i).hr_eliminacao = Bg_DaHora_ADO
                            estrutTubos(i).Motivo = EcMotivoNovaAmostra
                            estrutTubos(i).estado = BL_Coloca_Estado(estrutTubos(i).dt_previ, estrutTubos(i).dt_chega, estrutTubos(i).dt_eliminacao)
                            
                            sql = "update sl_req_tubo set mot_novo = " & BL_TrataStringParaBD(EcMotivoNovaAmostra) & _
                                    ", user_eliminacao = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & _
                                    ", dt_eliminacao = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
                                    ", hr_eliminacao = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & _
                                    ", estado_tubo = " & gEstadosTubo.cancelado & _
                                    " where n_req = " & estrutTubos(i).n_req & " and cod_tubo = " & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & _
                                    " AND dt_eliminacao is null "
                            BG_ExecutaQuery_ADO sql
                            BG_Trata_BDErro
                            If gSQLError <> 0 Then
                                'Erro a gravar motivo
                                mensagem = "Erro a gravar motivo de pedido de nova amostra !" & gSQLError
                                GoTo Trata_Erro
                            End If
                            
                            If CkApagarMarcadas.value = vbChecked Then
                                
                                ' SE ELIMINAR ANALISES - INSERE EM MARCACOES_APAGADAS
                                ' -----------------------------------------------------------
                                sql = "insert into sl_marcacoes_apagadas select * from sl_marcacoes " & _
                                        " where n_req = " & estrutTubos(i).n_req & " and (cod_ana_s in ( " & _
                                        " select cod_ana_s from sl_ana_s where cod_tubo = " & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & ")" & _
                                        " or cod_ana_c in ( " & _
                                        " select cod_ana_c from sl_ana_c where cod_tubo = " & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & ")" & _
                                        " or cod_perfil in ( " & _
                                        " select cod_perfis from sl_perfis where cod_tubo = " & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & ")" & ")"
                                BG_ExecutaQuery_ADO sql
                                BG_Trata_BDErro
                                If gSQLError <> 0 Then
                                    mensagem = "Erro a inserir na tabela sl_marcacoes_apagadas !" & gSQLError
                                    GoTo Trata_Erro
                                End If
                                
                                ' SE ELIMINAR ANALISES - APAGA DE MARCACOES
                                ' -----------------------------------------------------------
                                sql = "delete from sl_marcacoes " & _
                                        " where n_req = " & estrutTubos(i).n_req & " and (cod_ana_s in ( " & _
                                        " select cod_ana_s from sl_ana_s where cod_tubo = " & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & ")" & _
                                        " or cod_ana_c in ( " & _
                                        " select cod_ana_c from sl_ana_c where cod_tubo = " & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & ")" & _
                                        " or cod_perfil in ( " & _
                                        " select cod_perfis from sl_perfis where cod_tubo = " & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & ")" & ")"
                                'BG_ExecutaQuery_ADO sql
                                BG_Trata_BDErro
                                If gSQLError <> 0 Then
                                    mensagem = "Erro a apagar na tabela sl_marcacoes !" & gSQLError
                                    GoTo Trata_Erro
                                End If
                            Else
                            
                                ' SE NAO ELIMINA ANALISES POE NOVO TUBO A ESPERA
                                ' -----------------------------------------------------------
                                seq_req_tubo = TB_InsereTuboBD(mediComboValorNull, CStr(estrutTubos(i).n_req), UCase(estrutTubos(i).CodTubo), Bg_DaData_ADO, "", "", "", "", "", codAna, gEstadosTubo.Pendente, mediComboValorNull, "")
                                If seq_req_tubo = mediComboValorNull Then
                                    mensagem = "Erro marcar novo tubo!" & gSQLError
                                    GoTo Trata_Erro
                                End If
                                
                            End If
                            sql = "UPDATE sl_marcacoes SET seq_req_tubo = " & seq_req_tubo & " WHERE seq_req_tubo = " & estrutTubos(i).seq_req_tubo
                            BG_ExecutaQuery_ADO sql
                            sql = "UPDATE sl_realiza SET seq_req_tubo = " & seq_req_tubo & " WHERE seq_req_tubo = " & estrutTubos(i).seq_req_tubo
                            BG_ExecutaQuery_ADO sql
                            BG_CommitTransaction
                            
                            estrutTubos(i).estado = BL_Coloca_Estado(estrutTubos(i).dt_previ, estrutTubos(i).dt_chega, Bg_DaData_ADO)
                            
                            'REGITA O MOTIVO DE PEDIDO DE NOVA AMOSTRA
                            BL_RegistaPedidoNovaAmostra estrutTubos(i).n_req, estrutTubos(i).CodTubo, "", EcMotivoNovaAmostra
                            Call Emitir_PedidoNovaAmostra(" " & estrutTubos(i).DescrProduto & " (" & estrutTubos(i).descrTubo & ") ", EcMotivoNovaAmostra, gRequisicaoActiva)
                        End If
                    End If
                End If
            End If
        Next
        FuncaoProcurar
     End If
     
     Exit Sub

Trata_Erro:
    BG_LogFile_Erros "BtPedidoNovaAmostra: " & sql & " - " & Err.Description, Me.Name, "BtPedidoNovaAmostra"
    BG_RollbackTransaction
    LimpaCampos
    Exit Sub
    Resume Next
End Sub

Private Sub cmdOK_Click()

    Call FuncaoProcurar

End Sub


Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim CodEtiqTubo As Integer
    Dim CriterioTabelaAux As String
    
    totalTubos = 0
    ReDim estrutTubos(0)
    
    If EcNumReq.Text <> "" Then
        If (KeyCode = 13 And Shift = 0) Then
            If BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
                gRequisicaoActiva = Right(EcNumReq.Text, 7)
                Set rs = New ADODB.recordset
                If gSGBD = gOracle Then
                    CriterioTabela = "SELECT rt.seq_req_tubo, r.n_req, r.estado_req, r.seq_utente, r.dt_chega, t.cod_tubo, t.descr_tubo, "
                    CriterioTabela = CriterioTabela & "rt.dt_previ, rt.dt_chega dt_chega_tubo, rt.hr_chega hr_chega_tubo ,r.obs_req, "
                    CriterioTabela = CriterioTabela & "rt.dt_eliminacao, rt.hr_eliminacao, rt.user_eliminacao, rt.mot_novo, t.cod_prod, p.descr_produto "
                    CriterioTabela = CriterioTabela & "FROM sl_requis r, sl_req_tubo rt, sl_tubo t, sl_produto p WHERE "
                    CriterioTabela = CriterioTabela & "r.n_req = rt.n_req(+) and rt.cod_tubo = t.cod_tubo(+) and "
                    CriterioTabela = CriterioTabela & "t.cod_prod = p.cod_produto(+) and "
                    CriterioTabela = CriterioTabela & "r.n_req = " & gRequisicaoActiva
                    CriterioTabela = CriterioTabela & " ORDER BY dt_previ, cod_tubo "
                ElseIf gSGBD = gSqlServer Then
                    CriterioTabela = "SELECT rt.seq_req_tubo, r.n_req, r.estado_req, r.seq_utente, r.dt_chega, t.cod_tubo, t.descr_tubo, "
                    CriterioTabela = CriterioTabela & " rt.dt_previ, rt.dt_chega dt_chega_tubo, rt.hr_chega hr_chega_tubo,r.obs_req, "
                    CriterioTabela = CriterioTabela & " rt.dt_eliminacao, rt.hr_eliminacao, rt.user_eliminacao, rt.mot_novo, t.cod_prod, p.descr_produto "
                    CriterioTabela = CriterioTabela & " FROM sl_requis r LEFT OUTER JOIN sl_req_tubo rt ON r.n_req = rt.n_req "
                    CriterioTabela = CriterioTabela & " LEFT OUTER JOIN   sl_tubo t ON rt.cod_tubo = t.cod_tubo "
                    CriterioTabela = CriterioTabela & " LEFT OUTER JOIN sl_produto p ON t.cod_prod = p.cod_produto "
                    CriterioTabela = CriterioTabela & " WHERE r.n_req = " & gRequisicaoActiva
                End If
                rs.CursorLocation = adUseServer
                rs.CursorType = adOpenStatic
                                
                If Len(EcNumReq.Text) = 9 Then
                        
                    CodEtiqTubo = Mid(EcNumReq.Text, 1, 2)

                    CriterioTabelaAux = CriterioTabela & " and t.cod_etiq = " & CodEtiqTubo
                    rs.Open CriterioTabelaAux, gConexao
        
                    If rs.RecordCount <= 0 Then
                        BL_FimProcessamento Me
                        BG_Mensagem mediMsgBox, "N�o foi encontrado o tubo identificado!", vbInformation, "Procurar"
                        FuncaoLimpar
                    Else
                        estado = 2
                        BL_ToolbarEstadoN estado
            
                        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
                        gRequisicaoActiva = CLng(EcNumReq.Text)
           
                        PreencheEstadoRequisicao
                        PreencheDadosUtente
                        
                        LimpaFgTubo
                        PreencheFGTubo
                                                                
                        EcNumReq.locked = True
                        
                        Set CampoDeFocus = EcNumReq
                        
                        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
                        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
                        BL_Toolbar_BotaoEstado "Remover", "InActivo"
                        
                        BL_FimProcessamento Me
                    
                    End If
                        
                Else
                    gRequisicaoActiva = Right(EcNumReq.Text, 7)
                    
                    rs.Open CriterioTabela, gConexao
                    
            
                     If rs.RecordCount <= 0 Then
                         BL_FimProcessamento Me
                         BG_Mensagem mediMsgBox, "N�o foi encontrado a requisi��o identificada!", vbInformation, "Procurar"
                         FuncaoLimpar
                     Else
                         estado = 2
                         BL_ToolbarEstadoN estado
             
                         BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
                         gRequisicaoActiva = CLng(EcNumReq.Text)
            
                         PreencheEstadoRequisicao
                         PreencheDadosUtente
                         LimpaFgTubo
                         
                         If rs.RecordCount >= 1 Then
                            While Not rs.EOF
                                PreencheEstrutTubos BL_HandleNull(rs!seq_req_tubo, mediComboValorNull), rs!n_req, BL_HandleNull(rs!cod_prod, ""), BL_HandleNull(rs!descr_produto, ""), BL_HandleNull(rs!cod_tubo, ""), _
                                                    BL_HandleNull(rs!descR_tubo, ""), BL_HandleNull(rs!dt_previ, ""), BL_HandleNull(rs!dt_chega_tubo, ""), BL_HandleNull(rs!hr_chega_tubo, ""), BL_HandleNull(rs!dt_eliminacao, ""), BL_HandleNull(rs!hr_eliminacao, "")
                                rs.MoveNext
                            Wend
                            PreencheFGTubo

                         End If
                             
                         EcNumReq.locked = True
                         
                         Set CampoDeFocus = EcNumReq
                         
                         BL_Toolbar_BotaoEstado "Modificar", "InActivo"
                         BL_Toolbar_BotaoEstado "Inserir", "InActivo"
                         BL_Toolbar_BotaoEstado "Remover", "InActivo"
                         
                         BL_FimProcessamento Me
                     End If
                End If
            End If
        End If
    End If
 
End Sub


Private Sub EcNumReq_LostFocus()

    cmdOK.Default = False

End Sub

Private Sub EcPesqRapTubo_Change()
    Dim indice As Integer
    Dim CriterioTabelaAux As String
    
    If EcPesqRapTubo.Text <> "" Then
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        
        CriterioTabelaAux = CriterioTabela & " and dt_eliminacao is null and t.seq_tubo = " & EcPesqRapTubo
        rs.Open CriterioTabelaAux, gConexao
        If rs.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do tubo!", vbExclamation, "Pesquisa r�pida"
        Else
            Call PreencheFGTubo
        End If
        EcPesqRapTubo = ""
    End If
End Sub



Private Sub FgTubo_Click()
    If FgTubo.row >= 1 And FgTubo.row <= totalTubos Then
        LaProduto = estrutTubos(FgTubo.row).DescrProduto
        EcMotivoNovaAmostra = estrutTubos(FgTubo.row).Motivo
    End If
    
End Sub

Private Sub FgTubo_DblClick()
    If FgTubo.row >= 1 And FgTubo.row <= totalTubos Then
        If estrutTubos(FgTubo.row).flg_Apagar = False Then
            estrutTubos(FgTubo.row).flg_Apagar = True
            FgTubo.TextMatrix(FgTubo.row, 6) = "SIM"
        Else
            estrutTubos(FgTubo.row).flg_Apagar = False
            FgTubo.TextMatrix(FgTubo.row, 6) = "N�O"
        End If
    End If
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormPedidoNovaAmostra = Nothing

End Sub

Sub DefTipoCampos()
    
    
    'Preenche combo estado da requisi��o
    CbEstadoReq.AddItem "Sem an�lises"
    CbEstadoReq.AddItem "Espera de produtos"
    CbEstadoReq.AddItem "Espera de resultados"
    CbEstadoReq.AddItem "Espera de valida��o m�dica"
    CbEstadoReq.AddItem "Cancelada"
    CbEstadoReq.AddItem "Valida��o m�dica completa"
    CbEstadoReq.AddItem "N�o passar a hist�rico"
    CbEstadoReq.AddItem "Todas impressas"
    CbEstadoReq.AddItem "Hist�rico"
    CbEstadoReq.AddItem "Resultados parciais"
    CbEstadoReq.AddItem "Valida��o m�dica parcial"
    CbEstadoReq.AddItem "Impress�o parcial"
    EcDataChegada.Tag = adDate
    With FgTubo
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 500
        .Col = 0
        .TextMatrix(0, 0) = "Tubo"
        .ColWidth(1) = 1980
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 1200
        .Col = 2
        .TextMatrix(0, 2) = "Data Prevista"
        .ColWidth(3) = 1200
        .Col = 3
        .TextMatrix(0, 3) = "Data Chegada"
        .ColWidth(4) = 1200
        .Col = 4
        .TextMatrix(0, 4) = "Hora Chegada"
        .ColWidth(5) = 800
        .Col = 5
        .TextMatrix(0, 5) = "Estado"
        .ColWidth(6) = 800
        .Col = 6
        .TextMatrix(0, 6) = "Apagar"
        .row = 1
        .Col = 0
    End With
    

    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
       
End Sub

Sub Inicializacoes()
    
    Me.caption = " Pedido de nova amostra"
    Me.left = 440
    Me.top = 350
    Me.Width = 8700
    Me.Height = 5895 ' Normal
    'Me.Height = 6330 ' Campos Extras
    
    NomeTabela = "sl_requis"
    Set CampoDeFocus = EcNumReq
    
    NumCampos = 5
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "seq_utente"
    CamposBD(2) = "estado_req"
    CamposBD(3) = "dt_chega"
    CamposBD(4) = "obs_req"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcSeqUtente
    Set CamposEc(2) = EcEstadoReq
    Set CamposEc(3) = EcDataChegada
    Set CamposEc(4) = EcObsReq
    
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "Requisi��o"
    TextoCamposObrigatorios(1) = "Utente"
    
    
    EcNumReq.Tag = 5
    Set CampoDeFocus = EcNumReq


End Sub

Sub FuncaoProcurar()
    
    Call EcNumReq_KeyDown(13, 0)
    Exit Sub
    
Trata_Erro:
    
    BG_LogFile_Erros Me.Name & ": FuncaoProcurar (" & Err.Number & "-" & Err.Description & ")"
    Err.Clear
    BL_FimProcessamento Me
    BG_Mensagem mediMsgBox, "Erro ao seleccionar registo !", vbInformation, "Procurar"
    FuncaoLimpar
    Resume Next


End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    CbEstadoReq.ListIndex = mediComboValorNull
    CbTipoUtente.ListIndex = mediComboValorNull
    EcUtente.Text = ""
    EcNome.Text = ""
    EcDataNasc.Text = ""
    EcMotivoNovaAmostra.Text = ""
    EcNumReq.locked = False
    LaProduto.caption = ""
    EcNumReq.SetFocus
    totalTubos = 0
    ReDim estrutTubos(0)
End Sub


Sub FuncaoLimpar()
    
    EcNumReq.Text = ""
    LimpaFgTubo
    LimpaCampos

End Sub



Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub


Sub Emitir_PedidoNovaAmostra(Produto As String, Motivo As String, NrRequis As Long)
    Dim sql As String
    Dim ObsReq As String
    Dim continua As Boolean
    Dim RsEstudo As ADODB.recordset
    Dim Estudo As String
    Dim NomeMed As String
    Dim ServicoMed As String
    Dim CSaudeMed As String
    Dim MoradaMed As String
    Dim codPostal As String
    Dim Medico As String
    Dim RsMed As ADODB.recordset
    Dim sSql As String
    Dim i As Integer
     BL_InicioProcessamento Me, "A imprimir fax do pedido..."
    
    'Printer Common Dialog
    continua = BL_IniciaReport("PedidoNovaAmostra", "Pedido de Nova amostra", crptToWindow)
    If continua = False Then
        BL_FimProcessamento Me
        Exit Sub
    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    sSql = "DELETE FROM sl_cr_pedido_nova_amostra WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalTubos
        If estrutTubos(i).flg_Apagar = True Then
            sSql = "INSERT INTO sl_cr_pedido_nova_amostra(nome_computador, n_req, cod_tubo, descr_tubo, cod_produto, "
            sSql = sSql & " descr_produto, motivo) VALUES ("
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ","
            sSql = sSql & estrutTubos(i).n_req & ","
            sSql = sSql & BL_TrataStringParaBD(estrutTubos(i).CodTubo) & ","
            sSql = sSql & BL_TrataStringParaBD(estrutTubos(i).descrTubo) & ","
            sSql = sSql & BL_TrataStringParaBD(estrutTubos(i).CodProduto) & ","
            sSql = sSql & BL_TrataStringParaBD(estrutTubos(i).DescrProduto) & ","
            sSql = sSql & BL_TrataStringParaBD(estrutTubos(i).Motivo) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    
    'Report.SQLQuery = ""
    'F�rmulas do Report
    Report.formulas(0) = "NomeUtente=" & BL_TrataStringParaBD("" & EcNome.Text)
    Report.formulas(1) = "DtNascUtente=" & BL_TrataStringParaBD("" & EcDataNasc.Text)
    Report.formulas(2) = "NumReq=" & BL_TrataStringParaBD("" & gRequisicaoActiva)
    Report.formulas(3) = "NumUtente=" & BL_TrataStringParaBD("" & CbTipoUtente.Text & "/" & EcUtente.Text)
    Report.formulas(4) = "DtChegada=" & BL_TrataStringParaBD("" & EcDataChegada.Text)
    Report.formulas(5) = "Processo=" & BL_TrataStringParaBD("" & EcUtente.Text)
    Report.formulas(6) = "ProvReq=" & BL_TrataStringParaBD("" & "")
    Report.formulas(7) = "ObsProven=" & BL_TrataStringParaBD("" & "")
    Report.formulas(9) = "Motivo=" & BL_TrataStringParaBD("" & EcMotivoNovaAmostra)
    Report.formulas(10) = "Utilizador=" & BL_TrataStringParaBD("" & gNomeUtilizador)
    Report.formulas(11) = "TituloUtil=" & BL_TrataStringParaBD("" & gTituloUtilizador)
    
    Report.SelectionFormula = "{sl_cr_pedido_nova_amostra.nome_computador} = " & BL_TrataStringParaBD(gComputador)
    Me.SetFocus
'    Report.SubreportToChange = ""
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
    
    BL_FimProcessamento Me
End Sub



Sub PreencheDadosUtente()
    
    Dim Campos(4) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "dt_nasc_ute"

    iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.Text = ""
        Exit Sub
    End If
    
    EcUtente.Text = retorno(0)
    CbTipoUtente.Text = retorno(1)
    EcNome.Text = retorno(2)
    EcDataNasc.Text = retorno(3)
    

    
End Sub


Sub PreencheEstadoRequisicao()
    

    Select Case Trim(BL_HandleNull(rs!estado_req, ""))
        Case gEstadoReqSemAnalises
            CbEstadoReq.ListIndex = 0   '"Sem an�lises"
        Case gEstadoReqEsperaProduto
            CbEstadoReq.ListIndex = 1   '"Espera de produtos"
        Case gEstadoReqEsperaResultados
            CbEstadoReq.ListIndex = 2   '"Espera de resultados"
        Case gEstadoReqEsperaValidacao
            CbEstadoReq.ListIndex = 3   '"Espera de valida��o m�dica"
        Case gEstadoReqCancelada
            CbEstadoReq.ListIndex = 4   '"Cancelada"
        Case gEstadoReqValicacaoMedicaCompleta
            CbEstadoReq.ListIndex = 5   '"Valida��o m�dica completa"
        Case gEstadoReqNaoPassarHistorico
            CbEstadoReq.ListIndex = 6   '"N�o passar a hist�rico"
        Case gEstadoReqTodasImpressas
            CbEstadoReq.ListIndex = 7   '"Todas impressas"
        Case gEstadoReqHistorico
            CbEstadoReq.ListIndex = 8   '"Hist�rico"
        Case gEstadoReqResultadosParciais
            CbEstadoReq.ListIndex = 9   '"Resultados parciais"
        Case gEstadoReqValidacaoMedicaParcial
            CbEstadoReq.ListIndex = 10  '"Valida��o m�dica parcial"
        Case gEstadoReqImpressaoParcial
            CbEstadoReq.ListIndex = 11  '"Impress�o parcial"
        Case Else
            CbEstadoReq.ListIndex = mediComboValorNull
    End Select
    
End Sub




Sub LimpaFgTubo()
    
    Dim j As Long
    
    
    j = FgTubo.rows - 1
    While j > 0
        If j > 1 Then
            FgTubo.RemoveItem j
        Else
            FgTubo.TextMatrix(j, 0) = ""
            FgTubo.TextMatrix(j, 1) = ""
            FgTubo.TextMatrix(j, 2) = ""
            FgTubo.TextMatrix(j, 3) = ""
            FgTubo.TextMatrix(j, 4) = ""
            FgTubo.TextMatrix(j, 5) = ""
        End If
        
        j = j - 1
    Wend
    
End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function


Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        
    End If
End Sub


Sub PreencheFGTubo()
    Dim i As Integer
    LimpaFgTubo
    
    For i = 1 To totalTubos
        FgTubo.TextMatrix(i, 0) = estrutTubos(i).CodTubo
        FgTubo.TextMatrix(i, 1) = estrutTubos(i).descrTubo
        FgTubo.TextMatrix(i, 2) = estrutTubos(i).dt_previ
        FgTubo.TextMatrix(i, 3) = estrutTubos(i).dt_chega
        FgTubo.TextMatrix(i, 4) = estrutTubos(i).hr_chega
        FgTubo.TextMatrix(i, 5) = estrutTubos(i).estado
        If estrutTubos(i).flg_Apagar = False Then
            FgTubo.TextMatrix(i, 6) = "N�O"
        Else
            FgTubo.TextMatrix(i, 6) = "SIM"
        End If
        FgTubo.AddItem ""
    Next
    FgTubo.row = 1
    LaProduto.caption = estrutTubos(1).DescrProduto
End Sub

Private Sub PreencheEstrutTubos(seq_req_tubo As Long, n_req As Long, cod_produto As String, descr_produto As String, cod_tubo As String, _
                                descR_tubo As String, dt_previ As String, dt_chega As String, hr_chega As String, _
                                dt_eliminacao As String, hr_eliminacao As String)
                                

    totalTubos = totalTubos + 1
    ReDim Preserve estrutTubos(totalTubos)
    
    estrutTubos(totalTubos).seq_req_tubo = seq_req_tubo
    estrutTubos(totalTubos).n_req = n_req
    estrutTubos(totalTubos).CodProduto = cod_produto
    estrutTubos(totalTubos).DescrProduto = descr_produto
    estrutTubos(totalTubos).CodTubo = cod_tubo
    estrutTubos(totalTubos).descrTubo = descR_tubo
    estrutTubos(totalTubos).dt_previ = dt_previ
    estrutTubos(totalTubos).dt_chega = dt_chega
    estrutTubos(totalTubos).hr_chega = hr_chega
    estrutTubos(totalTubos).dt_eliminacao = dt_eliminacao
    estrutTubos(totalTubos).hr_eliminacao = hr_eliminacao
    estrutTubos(totalTubos).flg_Apagar = False
    estrutTubos(totalTubos).Motivo = EcMotivoNovaAmostra
    estrutTubos(totalTubos).estado = BL_Coloca_Estado(dt_previ, dt_chega, dt_eliminacao)
    
End Sub


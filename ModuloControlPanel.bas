Attribute VB_Name = "ModuloControlPanel"
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

'---------------------------------------------------------------------------
' Modulo para adquirir parametros do sistema que possam nao estar
' defenidos no registo do Windows:
'
'   DefLocal.DatasFormato As String
'   DefLocal.DatasSeparador As String
'   DefLocal.DatasOrdem As Integer
'   DefLocal.HorasFormato As String
'   DefLocal.HorasSeparador As String
'   DefLocal.MoedaSDecimal As String
'   DefLocal.MoedaSMilhares As String
'   DefLocal.MoedaSMoeda As String
'   DefLocal.MoedaNDecimais As Integer
'   DefLocal.MoedaNGrupo As Integer
'   DefLocal.ValoresSDecimal As String
'   DefLocal.ValoresSMilhares As String
'   DefLocal.ValoresNDecimais As Integer
'   DefLocal.ValoresNGrupo As Integer
'
'
'---------------------------------------------------------------------------

Private Const LOCALE_SCURRENCY = &H14
Private Const LOCALE_ICURRDIGITS = &H19
Private Const LOCALE_ICURRENCY = &H1B
Private Const LOCALE_IDATE = &H21
Private Const LOCALE_IDAYLZERO = &H26
Private Const LOCALE_IDIGITS = &H11
Private Const LOCALE_IINTLCURRDIGITS = &H1A
Private Const LOCALE_SDATE = &H1D
Private Const LOCALE_SDECIMAL = &HE
Private Const LOCALE_SGROUPING = &H10
Private Const LOCALE_SMONDECIMALSEP = &H16
Private Const LOCALE_SMONGROUPING = &H18
Private Const LOCALE_SMONTHOUSANDSEP = &H17
Private Const LOCALE_SSHORTDATE = &H1F
Private Const LOCALE_STHOUSAND = &HF
Private Const LOCALE_STIME = &H1E
Private Const LOCALE_STIMEFORMAT = &H1003

Private Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long) As Long
Private Declare Function GetUserDefaultLCID% Lib "kernel32" ()

Type Definicoes
   DatasFormato As String
   DatasSeparador As String
   DatasOrdem As Integer
   HorasFormato As String
   HorasSeparador As String
   MoedaSDecimal As String
   MoedaSMilhares As String
   MoedaSMoeda As String
   MoedaNDecimais As Integer
   MoedaNGrupo As Integer
   ValoresSDecimal As String
   ValoresSMilhares As String
   ValoresNDecimais As Integer
   ValoresNGrupo As Integer
End Type

Public DefLocal As Definicoes

Sub ControlPanel()

Dim Symbol As String
Dim Iret1 As Long
Dim Iret2 As Long
Dim Lplcdatavar As String
Dim Pos As Integer
Dim locale As Long

'HKey - Informa��es que n�o est�o no Registry se ainda n�o foram iniciadas!
locale = GetUserDefaultLCID()

'Datas - Formato
Iret1 = GetLocaleInfo(locale, LOCALE_SSHORTDATE, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_SSHORTDATE, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.DatasFormato = Symbol

'Datas - Separador
Iret1 = GetLocaleInfo(locale, LOCALE_SDATE, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_SDATE, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.DatasSeparador = Symbol

'Datas - Ordem
Iret1 = GetLocaleInfo(locale, LOCALE_IDATE, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_IDATE, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.DatasOrdem = Symbol

'Horas - Formato
Iret1 = GetLocaleInfo(locale, LOCALE_STIMEFORMAT, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_STIMEFORMAT, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.HorasFormato = Symbol

'Horas - Separador
Iret1 = GetLocaleInfo(locale, LOCALE_STIME, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_STIME, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.HorasSeparador = Symbol
'-------
'Moeda - Simbolo Decimal
Iret1 = GetLocaleInfo(locale, LOCALE_SMONDECIMALSEP, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_SMONDECIMALSEP, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.MoedaSDecimal = Symbol

'Moedas - Simbolo Milhares
Iret1 = GetLocaleInfo(locale, LOCALE_SMONTHOUSANDSEP, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_SMONTHOUSANDSEP, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.MoedaSMilhares = Symbol

'Moeda - Simbolo Moeda
Iret1 = GetLocaleInfo(locale, LOCALE_SCURRENCY, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_SCURRENCY, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.MoedaSMoeda = Symbol

'Moeda - N� decimais
Iret1 = GetLocaleInfo(locale, LOCALE_IINTLCURRDIGITS, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_IINTLCURRDIGITS, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.MoedaNDecimais = Val(Symbol)

'Moeda - N� Grupo
Iret1 = GetLocaleInfo(locale, LOCALE_ICURRENCY, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_ICURRENCY, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.MoedaNGrupo = Symbol

'Valores - Separador decimal
Iret1 = GetLocaleInfo(locale, LOCALE_SDECIMAL, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_SDECIMAL, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.ValoresSDecimal = Symbol

'Valores - Separador Milhares
Iret1 = GetLocaleInfo(locale, LOCALE_STHOUSAND, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_STHOUSAND, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.ValoresSMilhares = Symbol

'Valores - N� Decimais
Iret1 = GetLocaleInfo(locale, LOCALE_IDIGITS, Lplcdatavar, 0)
Symbol = String$(Iret1, 0)
Iret2 = GetLocaleInfo(locale, LOCALE_IDIGITS, Symbol, Iret1)
Pos = InStr(Symbol, Chr$(0))
If Pos <> 0 Then Symbol = Left$(Symbol, Pos - 1)

DefLocal.ValoresNDecimais = Symbol

End Sub


VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form FormEstatTemposEstapas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   9855
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15255
   Icon            =   "FormEstatTemposEstapas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9855
   ScaleWidth      =   15255
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PictureListColor 
      Height          =   255
      Left            =   840
      ScaleHeight     =   195
      ScaleWidth      =   435
      TabIndex        =   37
      Top             =   8760
      Width           =   495
   End
   Begin VB.Frame FrRodape 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   36
      Top             =   7200
      Width           =   13815
      Begin VB.CheckBox CkAgrupadores 
         Appearance      =   0  'Flat
         Caption         =   "Agrupar Prioridade"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   2
         Left            =   4200
         TabIndex        =   20
         Top             =   240
         Width           =   1695
      End
      Begin VB.CheckBox CkAgrupadores 
         Appearance      =   0  'Flat
         Caption         =   "Agrupar Proveni�ncia"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   1
         Left            =   2040
         TabIndex        =   19
         Top             =   240
         Width           =   1935
      End
      Begin VB.CheckBox CkAgrupadores 
         Appearance      =   0  'Flat
         Caption         =   "Agrupar Operador"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   18
         Top             =   240
         Width           =   1695
      End
      Begin VB.CheckBox CkSupDetalhes 
         Appearance      =   0  'Flat
         Caption         =   "Suprimir Detalhes"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   12000
         TabIndex        =   21
         Top             =   240
         Width           =   1695
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H8000000C&
         Height          =   615
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   13815
      End
   End
   Begin VB.Frame FrResultados 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4335
      Left            =   120
      TabIndex        =   35
      Top             =   2760
      Width           =   13815
      Begin MSComctlLib.ListView LvPercentagens 
         Height          =   4335
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   7646
         SortOrder       =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   4335
         Left            =   5400
         TabIndex        =   39
         Top             =   0
         Width           =   8415
         _ExtentX        =   14843
         _ExtentY        =   7646
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "Tempo m�dio - Colheita"
         TabPicture(0)   =   "FormEstatTemposEstapas.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "MscColheita"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "BtExpande1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Tempo m�dio - Valida��o"
         TabPicture(1)   =   "FormEstatTemposEstapas.frx":03B7
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "MscValidacao"
         Tab(1).Control(1)=   "BtExpande2"
         Tab(1).ControlCount=   2
         Begin VB.CommandButton BtExpande2 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   -67080
            Picture         =   "FormEstatTemposEstapas.frx":0771
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   0
            Width           =   495
         End
         Begin VB.CommandButton BtExpande1 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   7920
            Picture         =   "FormEstatTemposEstapas.frx":0B1C
            Style           =   1  'Graphical
            TabIndex        =   17
            Top             =   0
            Width           =   495
         End
         Begin MSChart20Lib.MSChart MscColheita 
            Height          =   3855
            Left            =   120
            OleObjectBlob   =   "FormEstatTemposEstapas.frx":0EC7
            TabIndex        =   40
            Top             =   360
            Width           =   8175
         End
         Begin MSChart20Lib.MSChart MscValidacao 
            Height          =   3855
            Left            =   -74880
            OleObjectBlob   =   "FormEstatTemposEstapas.frx":27C7
            TabIndex        =   41
            Top             =   360
            Width           =   8175
         End
      End
   End
   Begin VB.Frame FrFiltros 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   120
      TabIndex        =   25
      Top             =   960
      Width           =   13815
      Begin VB.ListBox EcLocais 
         Appearance      =   0  'Flat
         Height          =   1380
         Left            =   9120
         Style           =   1  'Checkbox
         TabIndex        =   11
         Top             =   120
         Width           =   4455
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   1200
         Width           =   4215
      End
      Begin VB.TextBox EcDescrAnalise 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   840
         Width           =   4215
      End
      Begin VB.TextBox EcDescrTubo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   480
         Width           =   4215
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   285
         Left            =   6960
         Picture         =   "FormEstatTemposEstapas.frx":40C7
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Pesquisa requisi��es pendentes"
         Top             =   1200
         Width           =   600
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1440
         TabIndex        =   9
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CommandButton BtPesquisaAnalise 
         Height          =   285
         Left            =   6960
         Picture         =   "FormEstatTemposEstapas.frx":47B1
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Pesquisa requisi��es pendentes"
         Top             =   840
         Width           =   600
      End
      Begin VB.TextBox EcCodAnalise 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1440
         TabIndex        =   7
         Top             =   840
         Width           =   1335
      End
      Begin VB.CommandButton BtPesquisaTubo 
         Height          =   285
         Left            =   6960
         Picture         =   "FormEstatTemposEstapas.frx":4E9B
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Pesquisa requisi��es pendentes"
         Top             =   480
         Width           =   600
      End
      Begin VB.TextBox EcCodTubo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcDescrGrupo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   120
         Width           =   4215
      End
      Begin VB.CommandButton BtPesquisaGrupo 
         Height          =   285
         Left            =   6960
         Picture         =   "FormEstatTemposEstapas.frx":5585
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Pesquisa requisi��es pendentes"
         Top             =   120
         Width           =   600
      End
      Begin VB.TextBox EcCodGrupo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1440
         TabIndex        =   3
         Top             =   120
         Width           =   1335
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "&Locais"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8280
         TabIndex        =   34
         Top             =   120
         Width           =   540
      End
      Begin VB.Label Label1 
         Caption         =   "&Proveni�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   33
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "&An�lise"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   31
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "&Tubo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   29
         Top             =   480
         Width           =   855
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H8000000C&
         Height          =   1695
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   13815
      End
      Begin VB.Label Label1 
         Caption         =   "&Grupo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   27
         Top             =   120
         Width           =   855
      End
   End
   Begin VB.Frame FrCabecalho 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Caption         =   "FrFiltros"
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   120
      TabIndex        =   22
      Top             =   0
      Width           =   13815
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   6360
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton OptAgrupamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Por ano"
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   2
         Left            =   12360
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton OptAgrupamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Por M�s"
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   11280
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton OptAgrupamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Por Hora"
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   3
         Left            =   10200
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton OptAgrupamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Por dia"
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   9120
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   360
         Width           =   1095
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   285
         Left            =   1440
         TabIndex        =   0
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   503
         _Version        =   393216
         Format          =   303300609
         CurrentDate     =   39694
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   285
         Left            =   3960
         TabIndex        =   1
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   503
         _Version        =   393216
         Format          =   303300609
         CurrentDate     =   39694
      End
      Begin VB.Label Label1 
         Caption         =   "&Epis�dio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   5520
         TabIndex        =   43
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "&Agrupar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8280
         TabIndex        =   38
         Top             =   360
         Width           =   675
      End
      Begin VB.Label Label1 
         Caption         =   "&Data Inicial"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   24
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "&Data Final"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   3000
         TabIndex        =   23
         Top             =   360
         Width           =   855
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H8000000C&
         Height          =   735
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   13815
      End
   End
   Begin MSComctlLib.ImageList ImageListIcons 
      Left            =   120
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   17
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":5C6F
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":5DC9
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":5F23
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":6323
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":6723
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":6ABD
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":6E57
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":71F1
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":8ECB
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":928D
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":9987
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":101E9
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":16A4B
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":16DC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":1D626
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":23E88
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEstatTemposEstapas.frx":24243
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormEstatTemposEstapas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2011.06.14   .
'     .       � 2011 Glintt-HS      .
'      .............................

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Long

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'
Dim linhaActual As Long
Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean
' Keep handle to the list view.
Private lhwndListView As Long

Private Enum ListViewIcons
    
    e_IconCollapsed = 1
    e_IconExpanded = 2
    e_IconLeftGray = 3
    e_IconRightGray = 4
    e_IconRightYellow = 5
    e_IconCantDrop = 6
    e_IconRightBlue = 7
    e_IconForbidden = 8
    e_IconReturn = 9
    e_IconSearch = 10
    e_IconForbidden2 = 11
    e_IconOk = 12
    e_IconDown = 13
    e_IconIn = 14
    e_IconOut = 15
    e_IconGraphExpanded = 16
    e_IconGraphCollapsed = 17
    
End Enum

Private Enum Agrupadores

    e_Operador = 0
    e_Proveniencia = 1
    e_Prioridade = 2

End Enum

Private Type DadosReq
    t_index As Long
    n_req As String
    dt_cri As String
    hr_cri As String
    dt_chega As String
    hr_chega As String
    dt_val As String
    hr_val As String
    cod_agrup As String
    tempo_max_colheita As Long
    tempo_max_validacao As Long
    tempo_colheita As Long
    tempo_validacao As Long
    cod_proven As String
    t_urg As String
    user_val As String
    
End Type

Dim estrutRequis() As DadosReq
Dim totalRequis As Long

Private Type dados
    Label As String
    soma As Double
    media As Double
    NReq As Long
End Type

Dim estrutGraficoColheita() As dados
Dim TotalGraficoColheita As Long

Dim estrutGraficoValidacao() As dados
Dim TotalGraficoValidacao As Long

Const lAgrupDia = 0
Const lAgrupHora = 3
Const lAgrupMes = 1
Const lAgrupAno = 2

Private Sub BtExpande1_Click()
    
    On Error GoTo ErrorHandler
    If (BtExpande1.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphCollapsed).Picture) Then
        ColapsaGrafico
    Else
        ExpandeGrafico
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtExpande1_Click' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub BtExpande2_Click()

    On Error GoTo ErrorHandler
    If (BtExpande2.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphCollapsed).Picture) Then
        ColapsaGrafico
    Else
        ExpandeGrafico
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtExpande2_Click' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub BtPesquisaProveniencia_Click()

    Dim i As Long
    Dim locais As String
    
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    On Error GoTo ErrorHandler
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    ClausulaWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

 Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtPesquisaProveniencia_Click' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub


Private Sub CkAgrupadores_Click(Index As Integer)

    If (CkAgrupadores(Index).value <> vbChecked) Then: Exit Sub
    Select Case (Index)
        Case Agrupadores.e_Operador:
            CkAgrupadores(Agrupadores.e_Prioridade).value = vbUnchecked
            CkAgrupadores(Agrupadores.e_Proveniencia).value = vbUnchecked
        Case Agrupadores.e_Prioridade:
            CkAgrupadores(Agrupadores.e_Operador).value = vbUnchecked
            CkAgrupadores(Agrupadores.e_Proveniencia).value = vbUnchecked
        Case Agrupadores.e_Proveniencia:
            CkAgrupadores(Agrupadores.e_Operador).value = vbUnchecked
            CkAgrupadores(Agrupadores.e_Prioridade).value = vbUnchecked
    End Select

End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)

    Dim Tabela As ADODB.recordset
    Dim sql As String

    On Error GoTo ErrorHandler
    If (EcCodProveniencia.Text <> Empty) Then
        EcCodProveniencia.Text = UCase(EcCodProveniencia.Text)
        Set Tabela = New ADODB.recordset

        sql = "select descr_proven from sl_proven where cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao

        If (Tabela.RecordCount <= 0) Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.Text = Empty
            EcDescrProveniencia.Text = Empty
        Else
            EcDescrProveniencia.Text = BL_HandleNull(Tabela!descr_proven, Empty)
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.Text = Empty
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EcCodProveniencia_Validate' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub BtPesquisaAnalise_Click()

       
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    On Error GoTo ErrorHandler
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_ana"
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "slv_analises"
    CampoPesquisa1 = "descr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhuma an�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnalise.Text = resultados(1)
            EcDescrAnalise.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtPesquisaAnalise_Click' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub EcCodAnalise_Validate(Cancel As Boolean)

    Dim Tabela As ADODB.recordset
    Dim sql As String

    On Error GoTo ErrorHandler
    If (EcCodAnalise.Text <> Empty) Then
        EcCodAnalise.Text = UCase(EcCodAnalise.Text)
        Set Tabela = New ADODB.recordset

        sql = "select descr_ana from slv_analises where cod_ana = " & BL_TrataStringParaBD(EcCodAnalise.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao

        If (Tabela.RecordCount <= 0) Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodAnalise.Text = Empty
            EcDescrAnalise.Text = Empty
        Else
            EcDescrAnalise.Text = BL_HandleNull(Tabela!descr_ana, Empty)
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrAnalise.Text = Empty
    End If
 Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EcCodAnalise_Validate' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub BtPesquisaTubo_Click()

    
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    On Error GoTo ErrorHandler
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTubo.Text = resultados(1)
            EcDescrTubo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtPesquisaTubo_Click' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub EcCodTubo_Validate(Cancel As Boolean)

    Dim Tabela As ADODB.recordset
    Dim sql As String

    On Error GoTo ErrorHandler
    If (EcCodTubo.Text <> Empty) Then
        EcCodTubo.Text = UCase(EcCodTubo.Text)
        Set Tabela = New ADODB.recordset

        sql = "select descr_tubo from sl_tubo where cod_tubo = " & BL_TrataStringParaBD(EcCodTubo.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao

        If (Tabela.RecordCount <= 0) Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodTubo.Text = Empty
            EcDescrTubo.Text = Empty
        Else
            EcDescrTubo.Text = BL_HandleNull(Tabela!descR_tubo, Empty)
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrTubo.Text = Empty
    End If
 Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EcCodTubo_Validate' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub BtPesquisaGrupo_Click()

      Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    On Error GoTo ErrorHandler
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
   
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.Text = resultados(1)
            EcDescrGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtPesquisaGrupo_Click' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub EcCodGrupo_Validate(Cancel As Boolean)

    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    On Error GoTo ErrorHandler
    If (EcCodGrupo.Text <> Empty) Then
        EcCodGrupo.Text = UCase(EcCodGrupo.Text)
        Set Tabela = New ADODB.recordset

        sql = "select descr_gr_ana from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao

        If (Tabela.RecordCount <= 0) Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodGrupo.Text = Empty
            EcDescrGrupo.Text = Empty
        Else
            EcDescrGrupo.Text = BL_HandleNull(Tabela!descr_gr_ana, Empty)
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrGrupo.Text = Empty
    End If
 Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EcCodGrupo_Validate' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Estat�stica de tempos m�dios - Colheita/Valida��o"
    Me.left = 5
    Me.top = 5
    Me.Width = 14160
    Me.Height = 8370
    Set CampoDeFocus = EcDtInicio
    lhwndListView = LvPercentagens.hwnd
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbSituacao, KeyCode

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    PreencheValoresDefeito
    DefTipoCampos
    
    BG_ParametrizaPermissoes_ADO Me.Name

    estado = 0
    BG_StackJanelas_Push Me

End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0


    Set FormEstatTempoColheita = Nothing

End Sub

Sub LimpaCampos(Optional apenas_estrutura As Boolean)
    
    On Error GoTo ErrorHandler
    Me.SetFocus

    If (Not apenas_estrutura) Then
        EcDtInicio.value = Bg_DaData_ADO
        EcDtFim.value = Bg_DaData_ADO
        BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
        EcCodGrupo.Text = Empty: EcDescrGrupo.Text = Empty
        EcCodTubo.Text = Empty: EcDescrTubo.Text = Empty
        EcCodAnalise.Text = Empty: EcDescrAnalise.Text = Empty
        EcCodProveniencia.Text = Empty: EcDescrProveniencia.Text = Empty
        CbSituacao.ListIndex = mediComboValorNull
        Call LimpaListaLocais
    End If
    
    totalRequis = Empty
    ReDim estrutRequis(Empty)
    TotalGraficoColheita = Empty
    ReDim estrutGraficoColheita(TotalGraficoColheita)
    TotalGraficoValidacao = Empty
    ReDim estrutGraficoValidacao(TotalGraficoValidacao)
    LvPercentagens.ListItems.Clear
    MscColheita.ColumnCount = Empty
    MscValidacao.ColumnCount = Empty
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LimpaCampos' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub LimpaListaLocais()
    
    Dim i As Long
    
    On Error GoTo ErrorHandler
    For i = 0 To EcLocais.ListCount - 1
        If (EcLocais.ItemData(i) = gCodLocal) Then
            EcLocais.Selected(i) = True
        Else
            EcLocais.Selected(i) = False
        End If
    Next
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LimpaListaLocais' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Sub DefTipoCampos()

    On Error GoTo ErrorHandler
    totalRequis = 0
    ReDim estrutRequis(totalRequis)
    DefineLista
    LimpaListaLocais
    
    BtExpande1.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphExpanded).Picture
    BtExpande1.ToolTipText = "Expande gr�fico"
    BtExpande2.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphExpanded).Picture
    BtExpande2.ToolTipText = "Expande gr�fico"
    MscColheita.title.Text = "Tempo M�dio de Colheita"
    MscValidacao.title.Text = "Tempo M�dio de Valida��o"
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'DefTipoCampos' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Sub PreencheCampos()
    'nada
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    'nada
End Sub

Sub FuncaoProcurar()

    Dim i As Long
    Dim sSql As String
    Dim rsFE As New ADODB.recordset
    Dim Index As Long
    Dim tempo_colheita As Double
    Dim tempo_validacao As Double
    
    On Error GoTo ErrorHandler
    
    LimpaCampos (True)
    
    If (Not ValidaProcura) Then: Exit Sub

    If (Not ConstroiCriterio(sSql)) Then: Exit Sub
    
    BL_InicioProcessamento Me, " A procurar registos..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    
    Set rsFE = New ADODB.recordset
    rsFE.CursorLocation = adUseServer
    rsFE.CursorType = adOpenStatic
    rsFE.Open sSql, gConexao
    
    If rsFE.RecordCount > 0 Then
        While Not rsFE.EOF
            If (Index = Empty) Then: PreencheEstrutReq Index, BL_HandleNull(rsFE!n_req, Empty)
            Index = Index + 1
            
            ' CALCULA TEMPO DE COLHEITA
            tempo_colheita = CalculaTempoColheita(BL_HandleNull(rsFE!dt_cri, Empty), BL_HandleNull(rsFE!hr_cri, Empty), _
                                                  BL_HandleNull(rsFE!dt_chega, Empty), BL_HandleNull(rsFE!hr_chega, Empty))
            ' CALCULA TEMPO DE VALIDACAO
            tempo_validacao = CalculaTempoValidacao(BL_HandleNull(rsFE!dt_chega, Empty), BL_HandleNull(rsFE!hr_chega, Empty), _
                                                    BL_HandleNull(rsFE!dt_val, Empty), BL_HandleNull(rsFE!hr_val, Empty))
                                                    
            PreencheEstrutReq Index, BL_HandleNull(rsFE!n_req, Empty), BL_HandleNull(rsFE!dt_cri, Empty), _
                              BL_HandleNull(rsFE!hr_cri, Empty), BL_HandleNull(rsFE!dt_chega, Empty), _
                              BL_HandleNull(rsFE!hr_chega, Empty), BL_HandleNull(rsFE!dt_val, Empty), _
                              BL_HandleNull(rsFE!hr_val, Empty), BL_HandleNull(rsFE!cod_agrup, Empty), _
                              BL_HandleNull(rsFE!t_colheita, Empty), BL_HandleNull(rsFE!t_validacao, Empty), _
                              CStr(tempo_colheita), CStr(tempo_validacao), BL_HandleNull(rsFE!cod_proven, Empty), _
                              BL_HandleNull(rsFE!t_urg, Empty), BL_HandleNull(rsFE!user_val, Empty)
            rsFE.MoveNext
        Wend
        
        Call AdicionaItemLV(1, LvPercentagens.ListItems.Count + 1, True, False, False)
        
        BL_Toolbar_BotaoEstado "Imprimir", "Activo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
        
    Else
        BG_Mensagem mediMsgBox, "N�o existem registos para abrir a estat�stica!", vbInformation, "Estat�stica de Tempo M�dio de colheitas"
    
    End If
    
    rsFE.Close
    Set rsFE = Nothing
    
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoProcurar' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Function ValidaProcura() As Boolean
    
    On Error GoTo ErrorHandler
    If EcDtInicio.value > EcDtFim.value Then
        Call BG_Mensagem(mediMsgBox, "Data final tem de ser igual ou superior � data inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Function
    End If

    ValidaProcura = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ValidaProcura' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function ConstroiCriterio(ByRef sSql As String) As Boolean
 
    Dim i As Integer
    
    On Error GoTo ErrorHandler
    sSql = "SELECT t_urg,cod_proven,n_req,slv_req_etapas.dt_cri,hr_cri,dt_chega,hr_chega,dt_val,hr_val,cod_agrup,t_colheita,t_validacao, USER_VAL " & _
           " FROM slv_req_etapas,sl_ana_tempos_maximos" & _
           " WHERE dt_chega_req BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & _
           " AND slv_req_etapas.cod_agrup=sl_ana_tempos_maximos.cod_ana" & _
           " AND slv_req_etapas.cod_local = sl_ana_tempos_maximos.cod_local "

    If (EcLocais.SelCount > Empty) Then
        sSql = sSql & " and sl_ana_tempos_maximos.cod_local in ("
        For i = 0 To EcLocais.ListCount - 1
            If (EcLocais.Selected(i)) Then: sSql = sSql & EcLocais.ItemData(i) & ","
        Next
        sSql = Mid(sSql, 1, Len(sSql) - 1) & ")"
    End If
    
    sSql = sSql & " AND sl_ana_tempos_maximos.t_sit = slv_req_etapas.t_sit "
    If (CbSituacao.ListIndex <> mediComboValorNull) Then
        sSql = sSql & " AND slv_req_etapas.t_sit = " & BL_TrataStringParaBD(BG_DaComboSel(CbSituacao))
    End If

    If (EcCodGrupo <> Empty) Then
        sSql = sSql & " AND gr_ana = " & BL_TrataStringParaBD(EcCodGrupo)
    End If

    If (EcCodTubo <> Empty) Then
        sSql = sSql & " AND cod_tubo = " & BL_TrataStringParaBD(EcCodTubo)
    End If

    If (EcCodAnalise <> Empty) Then
        sSql = sSql & " AND cod_agrup = " & BL_TrataStringParaBD(EcCodAnalise)
    End If

    If (EcCodProveniencia <> Empty) Then
        sSql = sSql & " AND cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia)
    End If

    'sSql = sSql & " group by t_urg,cod_proven,n_req,slv_req_etapas.dt_cri,hr_cri,dt_chega,hr_chega,dt_val,hr_val,cod_agrup,t_colheita,t_validacao"
    sSql = sSql & " order by dt_chega desc,hr_chega desc,dt_val desc,hr_val desc,cod_agrup"
    ConstroiCriterio = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConstroiCriterio' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BG_Mensagem mediMsgBox, "Erro ao contruir crit�rio de pesquisa", vbExclamation, " Procura"
    Exit Function
    
End Function

Private Sub LvPercentagens_ItemClick(ByVal item As MSComctlLib.ListItem)
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A gerar gr�fico - colheitas..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    
    Call GeraGraficoColheita(item.Key, CBool(item.Index = 1))
    
    BL_InicioProcessamento Me, " A gerar gr�fico - valida��o..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    
    Call GeraGraficoValidacao(item.Key, CBool(item.Index = 1))
    
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
  Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvPercentagens_ItemClick' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
  
End Sub

Private Sub LvPercentagens_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Long
    
    On Error GoTo ErrorHandler
    linhaActual = 0
    For i = 1 To LvPercentagens.ListItems.Count
        If Y >= LvPercentagens.ListItems(i).top And Y < LvPercentagens.ListItems(i).top + LvPercentagens.ListItems(i).Height Then
            linhaActual = i
            Exit For
        End If
    Next
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvPercentagens_MouseMove' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub LvPercentagens_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (linhaActual = Empty) Then: Exit Sub
    LvPercentagens.ListItems.item(linhaActual).Selected = True
    DoEvents
    Select Case (Button)
        Case MouseButtonConstants.vbLeftButton: Call BeforeHandleListViewTree(X, Y)
        Case Else: ' Empty
    End Select
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvPercentagens_MouseDown' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Function GeraLV() As Boolean

    Dim lItem As Long
    Dim vSubItem As Variant
        
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(estrutRequis)
        If (Not AdicionaItemLV(lItem, LvPercentagens.ListItems.Count + 1, True, False, False)) Then: Exit Function
    Next
    GeraLV = True
    
Exit Function
ErrorHandler:
    GeraLV = False
    BG_LogFile_Erros "Error in function 'GeraLV' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next
End Function

Private Sub PreencheEstrutReq(t_index As Long, n_req As String, Optional dt_cri As String, Optional hr_cri As String, _
                              Optional dt_chega As String, Optional hr_chega As String, Optional dt_val As String, _
                              Optional hr_val As String, Optional cod_agrup As String, Optional tempo_max_colheita As Long, _
                              Optional tempo_max_validacao As Long, Optional tempo_colheita As Long, _
                              Optional tempo_validacao As Long, Optional cod_proven As String, Optional t_urg As String, Optional user_val As String)

    On Error GoTo ErrorHandler
    totalRequis = totalRequis + 1
    ReDim Preserve estrutRequis(totalRequis)
    estrutRequis(totalRequis).t_index = t_index
    estrutRequis(totalRequis).n_req = n_req
    estrutRequis(totalRequis).dt_cri = dt_cri
    estrutRequis(totalRequis).hr_cri = hr_cri
    estrutRequis(totalRequis).dt_chega = dt_chega
    estrutRequis(totalRequis).hr_chega = hr_chega
    estrutRequis(totalRequis).dt_val = dt_val
    estrutRequis(totalRequis).hr_val = hr_val
    estrutRequis(totalRequis).cod_agrup = cod_agrup
    estrutRequis(totalRequis).tempo_max_colheita = tempo_max_colheita
    estrutRequis(totalRequis).tempo_max_validacao = tempo_max_validacao
    estrutRequis(totalRequis).tempo_colheita = tempo_colheita
    estrutRequis(totalRequis).tempo_validacao = tempo_validacao
    estrutRequis(totalRequis).cod_proven = cod_proven
    estrutRequis(totalRequis).t_urg = t_urg
    estrutRequis(totalRequis).user_val = user_val
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheEstrutReq' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Sub FuncaoAnterior()
    'nada
End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub BD_Insert()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub BD_Update()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub

Sub BD_Delete()
    'nada
End Sub

Sub PreencheValoresDefeito()
    
    On Error GoTo ErrorHandler
    'OptAgrupamento(0).Value = True
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao, mediAscComboCodigo
    SSTab1.Tab = 0
    OptAgrupamento(0).value = True
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheValoresDefeito' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub


Private Sub GeraGraficoColheita(cod_ana As String, Optional total As Boolean)
    Dim modo As Long
    Dim i As Long
    Dim Label As String
    
    On Error GoTo ErrorHandler
    TotalGraficoColheita = Empty
    ReDim estrutGraficoColheita(TotalGraficoColheita)
    
    If OptAgrupamento(lAgrupAno).value = True Then
        modo = lAgrupAno
    ElseIf OptAgrupamento(lAgrupMes).value = True Then
        modo = lAgrupMes
    ElseIf OptAgrupamento(lAgrupDia).value = True Then
        modo = lAgrupDia
    ElseIf OptAgrupamento(lAgrupHora).value = True Then
        modo = lAgrupHora
    End If
    
    For i = totalRequis To 1 Step -1
        If (estrutRequis(i).t_index <> Empty And (estrutRequis(i).cod_agrup = cod_ana Or total = True)) Then
            If modo = lAgrupAno Then
                Label = DatePart("YYYY", CDate(estrutRequis(i).dt_cri))
            ElseIf modo = lAgrupMes Then
                Label = DatePart("m", CDate(estrutRequis(i).dt_cri)) & "-" & DatePart("YYYY", CDate(estrutRequis(i).dt_cri))
            ElseIf modo = lAgrupDia Then
                Label = CDate(estrutRequis(i).dt_cri)
            ElseIf modo = lAgrupHora Then
                Label = DatePart("h", estrutRequis(i).hr_cri)
            End If
            PreencheEstrutGraficoColheita Label, estrutRequis(i).dt_cri & " " & estrutRequis(i).hr_cri, estrutRequis(i).dt_chega & " " & estrutRequis(i).hr_chega
            DoEvents
        End If
    Next
    
    With MscColheita
        
        .RowCount = TotalGraficoColheita
        .ColumnCount = 1
        
        'Quando se atribui valores ao gr�fico (.data) incrementa a linha na coluna activa
        'ou a coluna se a linha da coluna for a �ltima
        .AutoIncrement = False
    
        .Column = 1
        For i = 1 To TotalGraficoColheita
            .row = i
            .data = BG_newRound(BL_String2Double(estrutGraficoColheita(i).media), 1)
            DoEvents
        Next i
        '.ChartData = Matriz
        
        'LABELS (S� DEPOIS DA ATRIBUI��O=>1 n�vel de indenta��o por Label!)
        For i = 1 To TotalGraficoColheita
            .row = i
            .RowLabelCount = 2
            .RowLabelIndex = 1
            .RowLabel = estrutGraficoColheita(i).Label
            .RowLabelIndex = 2
            If OptAgrupamento(lAgrupDia) Then
                .RowLabel = "Dias"
            ElseIf OptAgrupamento(lAgrupHora) Then
                .RowLabel = "Horas"
            ElseIf OptAgrupamento(lAgrupMes) Then
                .RowLabel = "Meses"
            ElseIf OptAgrupamento(lAgrupAno) Then
                .RowLabel = "Anos"
            End If
            DoEvents
        Next i
        
        .Column = 1
        .ColumnLabelCount = 1
        .ColumnLabelIndex = 1
                     
    End With
    MscColheita.Visible = True
      
    Dim color As RGB
    color.t_R = 255
    color.t_G = 64
    color.t_B = 64
    If (UBound(estrutGraficoColheita) > Empty) Then: Call MostraMarcadores(MscColheita, True, color)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GeraGraficoColheita' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
  
End Sub

Private Sub GeraGraficoValidacao(cod_ana As String, Optional total As Boolean)
    Dim modo As Long
    Dim i As Long
    Dim Label As String
    
    On Error GoTo ErrorHandler
    TotalGraficoValidacao = Empty
    ReDim estrutGraficoValidacao(TotalGraficoValidacao)
    
    If OptAgrupamento(lAgrupAno).value = True Then
        modo = lAgrupAno
    ElseIf OptAgrupamento(lAgrupMes).value = True Then
        modo = lAgrupMes
    ElseIf OptAgrupamento(lAgrupDia).value = True Then
        modo = lAgrupDia
    ElseIf OptAgrupamento(lAgrupHora).value = True Then
        modo = lAgrupHora
    End If
    
    For i = totalRequis To 1 Step -1
        If (estrutRequis(i).t_index <> Empty And (estrutRequis(i).cod_agrup = cod_ana Or total = True)) Then
            If modo = lAgrupAno Then
                Label = DatePart("YYYY", CDate(estrutRequis(i).dt_chega))
            ElseIf modo = lAgrupMes Then
                Label = DatePart("m", CDate(estrutRequis(i).dt_chega)) & "-" & DatePart("YYYY", CDate(estrutRequis(i).dt_chega))
                If Len(Label) = 6 Then
                    Label = "0" & Label
                End If
            ElseIf modo = lAgrupDia Then
                Label = CDate(estrutRequis(i).dt_chega)
            ElseIf modo = lAgrupHora Then
                Label = DatePart("h", estrutRequis(i).hr_chega)
            End If
            PreencheEstrutGraficoValidacao Label, estrutRequis(i).dt_chega & " " & estrutRequis(i).hr_chega, estrutRequis(i).dt_val & " " & estrutRequis(i).hr_val
            
        End If
    Next
    DoEvents
    With MscValidacao
        
        .RowCount = TotalGraficoValidacao
        .ColumnCount = 1
        
        'Quando se atribui valores ao gr�fico (.data) incrementa a linha na coluna activa
        'ou a coluna se a linha da coluna for a �ltima
        .AutoIncrement = False
    
        .Column = 1
        For i = 1 To TotalGraficoValidacao
            .row = i
            .data = BG_newRound(BL_String2Double(estrutGraficoValidacao(i).media), 1)
            DoEvents
        Next i
        '.ChartData = Matriz
        
        'LABELS (S� DEPOIS DA ATRIBUI��O=>1 n�vel de indenta��o por Label!)
        For i = 1 To TotalGraficoValidacao
            .row = i
            .RowLabelCount = 2
            .RowLabelIndex = 1
            .RowLabel = estrutGraficoValidacao(i).Label
            .RowLabelIndex = 2
            If OptAgrupamento(lAgrupDia) Then
                .RowLabel = "Dias"
            ElseIf OptAgrupamento(lAgrupHora) Then
                .RowLabel = "Horas"
            ElseIf OptAgrupamento(lAgrupMes) Then
                .RowLabel = "Meses"
            ElseIf OptAgrupamento(lAgrupAno) Then
                .RowLabel = "Anos"
            End If
            DoEvents
        Next i
        
        .Column = 1
        .ColumnLabelCount = 1
        .ColumnLabelIndex = 1
        
    End With
    MscValidacao.Visible = True
        
    Dim color As RGB
    color.t_R = 72
    color.t_G = 72
    color.t_B = 255
    If (UBound(estrutGraficoColheita) > Empty) Then: Call MostraMarcadores(MscValidacao, True, color)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GeraGraficoValidacao' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub PreencheEstrutGraficoColheita(Label As String, hr_cri As String, hr_act As String)
    Dim i As Long
    Dim indice As Long
    
    
    On Error GoTo ErrorHandler
    
'    If DateDiff("n", hr_cri, hr_act) < 0 Then
'        Exit Sub
'    End If
    indice = -1
    For i = 1 To TotalGraficoColheita
        If estrutGraficoColheita(i).Label = Label Then
            indice = i
            Exit For
        End If
    Next
    If indice = -1 Then
        TotalGraficoColheita = TotalGraficoColheita + 1
        ReDim Preserve estrutGraficoColheita(TotalGraficoColheita)
        estrutGraficoColheita(TotalGraficoColheita).NReq = 0
        estrutGraficoColheita(TotalGraficoColheita).soma = 0
        indice = TotalGraficoColheita
    End If
    estrutGraficoColheita(indice).Label = Label
    estrutGraficoColheita(indice).NReq = estrutGraficoColheita(indice).NReq + 1
    estrutGraficoColheita(indice).soma = estrutGraficoColheita(indice).soma + DateDiff("n", hr_cri, hr_act)
    estrutGraficoColheita(indice).media = estrutGraficoColheita(indice).soma / estrutGraficoColheita(indice).NReq
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheEstrutGraficoColheita' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub PreencheEstrutGraficoValidacao(Label As String, hr_cri As String, hr_act As String)
    Dim i As Long
    Dim indice As Long
    
    On Error GoTo ErrorHandler
    indice = -1
    indice = DevolveIndiceInserirValidacao(Label)
    estrutGraficoValidacao(indice).Label = Label
    estrutGraficoValidacao(indice).NReq = estrutGraficoValidacao(indice).NReq + 1
    estrutGraficoValidacao(indice).soma = estrutGraficoValidacao(indice).soma + DateDiff("n", hr_cri, hr_act)
    estrutGraficoValidacao(indice).media = estrutGraficoValidacao(indice).soma / estrutGraficoValidacao(indice).NReq
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheEstrutGraficoValidacao' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Public Sub FuncaoImprimir()

    Dim sSql As String
    Dim continua As Boolean
    Dim Label As String
    Dim i As Long
    Dim total_in_c As Double
    Dim total_out_c As Double
    Dim total_in_v As Double
    Dim total_out_v As Double
    Dim tempo_colheita As Double
    Dim tempo_validacao As Double
    Dim nomeReport As String
    Dim campoOrder As String
    'edgar.parada HDES-6802 04.10.2018
    Dim mediaTempov As Double
    Dim mediaTempoc As Double
    '
    If (totalRequis = Empty) Then: Exit Sub

    'Printer Common Dialog
    gImprimirDestino = Empty
    
    If (CkAgrupadores(Agrupadores.e_Operador).value = vbChecked) Then
        nomeReport = "EstatTemposEstapas_OrderOperador"
        campoOrder = "sl_idutilizador.nome "
    ElseIf (CkAgrupadores(Agrupadores.e_Prioridade).value = vbChecked) Then
        nomeReport = "EstatTemposEstapas_OrderPrioridade"
        campoOrder = "sl_cr_estat_tempos_etapas.t_urg"
    ElseIf (CkAgrupadores(Agrupadores.e_Proveniencia).value = vbChecked) Then
        nomeReport = "EstatTemposEstapas_OrderProveniencia"
        campoOrder = "sl_cr_estat_tempos_etapas.cod_proven"
    Else
        nomeReport = "EstatTemposEstapas"
        campoOrder = "sl_cr_estat_tempos_etapas.cod_agrup"
    End If
    
    continua = BL_IniciaReport(nomeReport, "Estat�stica de Colheitas", crptToWindow)
    If (Not continua) Then: Exit Sub

    sSql = "delete from sl_cr_estat_tempos_etapas where nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    If (gSQLError <> Empty) Then: Exit Sub

    For i = 1 To totalRequis
        If (estrutRequis(i).t_index <> Empty) Then
        
            tempo_colheita = CalculaTempoColheita(estrutRequis(i).dt_cri, estrutRequis(i).hr_cri, estrutRequis(i).dt_chega, estrutRequis(i).hr_chega)
            tempo_validacao = CalculaTempoValidacao(estrutRequis(i).dt_chega, estrutRequis(i).hr_chega, estrutRequis(i).dt_val, estrutRequis(i).hr_val)
            'edgar.parada HDES-6802 04.10.2018
            'Call CalculaPercentagensColheitaTotal(total_in_c, total_out_c, estrutRequis(i).cod_agrup)
            Call CalculaPercentagensColheitaAnalise(estrutRequis(i).cod_agrup, mediaTempoc, total_in_c, total_out_c)
            'Call CalculaPercentagensValidacaoTotal(total_in_v, total_out_v, estrutRequis(i).cod_agrup)
            Call CalculaPercentagensValidacaoAnalise(estrutRequis(i).cod_agrup, mediaTempov, total_in_v, total_out_v)
            '
            sSql = "insert into sl_cr_estat_tempos_etapas (nome_computador,num_sessao,n_req,cod_agrup,t_m_colheita,t_m_validacao," & _
                   "t_p_colheita_out,t_p_colheita_in,t_p_validacao_out,t_p_validacao_in,dt_cri,dt_act,user_cri,user_act,cod_local,cod_proven,t_urg, user_val) values (" & _
                   BL_TrataStringParaBD(gComputador) & "," & _
                   gNumeroSessao & "," & _
                   estrutRequis(i).n_req & "," & _
                   BL_TrataStringParaBD(estrutRequis(i).cod_agrup) & "," & _
                   BL_TrataStringParaBD(CStr(tempo_colheita)) & "," & _
                   BL_TrataStringParaBD(CStr(tempo_validacao)) & "," & _
                   BL_TrataStringParaBD(CStr(total_out_c)) & "," & _
                   BL_TrataStringParaBD(CStr(total_in_c)) & "," & _
                   BL_TrataStringParaBD(CStr(total_out_v)) & "," & _
                   BL_TrataStringParaBD(CStr(total_in_v)) & "," & _
                   "sysdate" & "," & _
                   "null" & "," & _
                   gCodUtilizador & "," & _
                   "null" & "," & _
                   gCodLocal & "," & _
                   BL_TrataStringParaBD(estrutRequis(i).cod_proven) & "," & _
                   estrutRequis(i).t_urg & ", " & _
                   BL_TrataStringParaBD(estrutRequis(i).user_val) & ")"

            BG_ExecutaQuery_ADO sSql
            If (gSQLError <> Empty) Then: Exit Sub
        End If
    Next

    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.SQLQuery = "select sl_cr_estat_tempos_etapas.t_urg,sl_cr_estat_tempos_etapas.nome_computador,sl_cr_estat_tempos_etapas.num_sessao,sl_cr_estat_tempos_etapas.n_req," & _
                      "sl_cr_estat_tempos_etapas.cod_agrup,sl_cr_estat_tempos_etapas.t_m_colheita,sl_cr_estat_tempos_etapas.t_m_validacao," & _
                      "sl_cr_estat_tempos_etapas.t_p_colheita_out,sl_cr_estat_tempos_etapas.t_p_colheita_in,sl_cr_estat_tempos_etapas.t_p_validacao_out," & _
                      "sl_cr_estat_tempos_etapas.t_p_validacao_in,sl_cr_estat_tempos_etapas.dt_cri,sl_cr_estat_tempos_etapas.dt_act," & _
                      "sl_cr_estat_tempos_etapas.user_cri,sl_cr_estat_tempos_etapas.user_act,sl_cr_estat_tempos_etapas.cod_local,sl_cr_estat_tempos_etapas.user_val, sl_idutilizador.nome " & _
                      "from sl_tbf_t_urg, sl_proven, sl_cr_estat_tempos_etapas, sl_idutilizador,gr_empr_inst,slv_analises where sl_cr_estat_tempos_etapas.nome_computador = " & _
                      BL_TrataStringParaBD(BG_SYS_GetComputerName) & " and sl_cr_estat_tempos_etapas.num_sessao = " & gNumeroSessao & " and " & _
                      "sl_idutilizador.cod_utilizador = sl_cr_estat_tempos_etapas.user_Val And gr_empr_inst.cod_empr = " & gCodLocal & " and cod_agrup = cod_ana " & _
                      " and sl_proven.cod_proven = sl_cr_estat_tempos_etapas.cod_proven " & _
                      " and sl_tbf_t_urg.cod_t_urg = sl_cr_estat_tempos_etapas.t_urg" & _
                      " order by " & campoOrder
                      
    'F�rmulas do Report ...
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.value)
    Report.formulas(2) = "TipoEstat=" & BL_TrataStringParaBD("tempo m�dio colheitas/valida��o")
    Report.formulas(3) = "GrAna=" & BL_TrataStringParaBD(EcDescrGrupo.Text)
    Report.formulas(4) = "Tubo=" & BL_TrataStringParaBD(EcDescrTubo.Text)
    Report.formulas(5) = "Analises=" & BL_TrataStringParaBD(EcDescrAnalise.Text)
    Report.formulas(6) = "Proven=" & BL_TrataStringParaBD(EcDescrProveniencia.Text)
    Report.formulas(7) = "Locais=" & BL_TrataStringParaBD(DevolveLocaisSel)
         
    ' ...
    Report.formulas(8) = "AgruparOperador=" & BL_TrataStringParaBD(IIf(CkAgrupadores(Agrupadores.e_Operador) = vbChecked, "Sim", "N�o"))
    Report.formulas(9) = "AgruparProven=" & BL_TrataStringParaBD(IIf(CkAgrupadores(Agrupadores.e_Proveniencia) = vbChecked, "Sim", "N�o"))
    Report.formulas(10) = "AgruparPrioridade=" & BL_TrataStringParaBD(IIf(CkAgrupadores(Agrupadores.e_Prioridade) = vbChecked, "Sim", "N�o"))
    Report.formulas(11) = "SuprimirDetalhes=" & BL_TrataStringParaBD(IIf(CkSupDetalhes = vbChecked, "Sim", "N�o"))
    
    Call BL_ExecutaReport

    sSql = "DELETE FROM sl_cr_estat_tempos_etapas WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    If (gSQLError <> Empty) Then: Exit Sub

End Sub


Private Sub DefineLista()

    On Error GoTo ErrorHandler
    With LvPercentagens
        .ColumnHeaders.Add(, , "An�lise", 1420, lvwColumnLeft).Key = "ANALISE"
        .ColumnHeaders.Add(, , "Col%OUT", 900, lvwColumnLeft).Key = "COL%NAO"
        .ColumnHeaders.Add(, , "Col%IN", 900, lvwColumnLeft).Key = "COL%SIM"
        .ColumnHeaders.Add(, , "Val%OUT", 900, lvwColumnLeft).Key = "VAL%SIM"
        .ColumnHeaders.Add(, , "Val%IN", 900, lvwColumnLeft).Key = "VAL%NAO"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .SmallIcons = ImageListIcons
        .FullRowSelect = True
        .Checkboxes = False
        .ForeColor = ApplicationHexadecimalColors.e_Gray
        .MultiSelect = False
    End With
    BG_SetListViewColor LvPercentagens, PictureListColor, ApplicationHexadecimalColors.e_LightGray, vbWhite
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'DefineLista' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

Private Sub OptAgrupamento_Click(Index As Integer)
    
    Dim i As Long
    
    On Error GoTo ErrorHandler
    For i = 0 To 3
        If (i = Index) Then
            OptAgrupamento(i).BackColor = ApplicationHexadecimalColors.e_LightGray
            If (Me.Visible) Then: EcLocais.SetFocus
        Else
            OptAgrupamento(i).BackColor = ApplicationHexadecimalColors.e_White
            If (Me.Visible) Then: EcLocais.SetFocus
        End If
    Next
    If (Me.Visible) Then
        TotalGraficoColheita = Empty: ReDim estrutGraficoColheita(TotalGraficoColheita)
        TotalGraficoValidacao = Empty: ReDim estrutGraficoValidacao(TotalGraficoValidacao)
        If (LvPercentagens.ListItems.Count > 0) Then: LvPercentagens_ItemClick LvPercentagens.SelectedItem
    End If
      Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'OptAgrupamento_Click' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub BeforeHandleListViewTree(X As Single, Y As Single)
    
    Dim lvhti As LVHITTESTINFO
    
    On Error GoTo ErrorHandler
    lvhti.pt.X = X / Screen.TwipsPerPixelX
    lvhti.pt.Y = Y / Screen.TwipsPerPixelY
    If (ListView_ItemHitTest(lhwndListView, lvhti) = LVI_NOITEM) Then: Exit Sub
    If (lvhti.flags = LVHT_ONITEMICON) Then: Call HandleListViewTree(LvPercentagens.SelectedItem, Empty)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BeforeHandleListViewTree' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub HandleListViewTree(ByVal item As MSComctlLib.ListItem, iManualKey As Variant)
   
    On Error GoTo ErrorHandler
    If (item.SmallIcon = ListViewIcons.e_IconCollapsed And BL_HandleNull(iManualKey, vbKeyRight) = vbKeyRight) Then: ExpandirLV item: Exit Sub
    If (item.SmallIcon = ListViewIcons.e_IconExpanded And BL_HandleNull(iManualKey, vbKeyLeft) = vbKeyLeft) Then: ComprimirLV item: Exit Sub
Exit Sub
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HandleListViewTree' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
End Sub

Private Function AdicionaItemLV(lItem As Long, lIndex As Long, Optional bParent As Boolean, Optional bAutomatic As Boolean, Optional bShowDirection As Boolean) As Boolean

    On Error GoTo ErrorHandler
    If (bParent) Then
        Dim total_colheita_in As Double, total_colheita_out As Double
        Dim total_validacao_in As Double, total_validacao_out As Double
        Call CalculaPercentagensColheitaTotal(total_colheita_in, total_colheita_out)
        Call CalculaPercentagensValidacaoTotal(total_validacao_in, total_validacao_out)
        With LvPercentagens.ListItems.Add(, "KEY_" & estrutRequis(lItem).n_req & "_" & estrutRequis(lItem).t_index & "_" & lItem, "Total", Empty, IIf(bParent, IIf(bAutomatic, ListViewIcons.e_IconExpanded, ListViewIcons.e_IconCollapsed), Empty))
            .ListSubItems.Add(, , total_colheita_out).ForeColor = ApplicationHexadecimalColors.e_LightRed
            .ListSubItems.item(1).Bold = True
            .ListSubItems.Add(, , total_colheita_in).ForeColor = ApplicationHexadecimalColors.e_Green
            .ListSubItems.item(2).Bold = True
            .ListSubItems.Add(, , total_validacao_out).ForeColor = ApplicationHexadecimalColors.e_LightRed
            .ListSubItems.item(3).Bold = True
            .ListSubItems.Add(, , total_validacao_in).ForeColor = ApplicationHexadecimalColors.e_Green
            .ListSubItems.item(4).Bold = True
        End With
        LvPercentagens.ListItems(1).Bold = True
    Else
        If (Not ItemJaExiste(estrutRequis(lItem).cod_agrup)) Then
            Dim analise_colheita_in As Double, analise_colheita_out As Double
            Dim analise_validacao_in As Double, analise_validacao_out As Double
            Dim media_tempo_colheita As Double, media_tempo_validacao As Double
            Call CalculaPercentagensColheitaAnalise(estrutRequis(lItem).cod_agrup, media_tempo_colheita, analise_colheita_in, analise_colheita_out)
            Call CalculaPercentagensValidacaoAnalise(estrutRequis(lItem).cod_agrup, media_tempo_validacao, analise_validacao_in, analise_validacao_out)
            Dim descr_ana() As String
            descr_ana = Split(BL_DevolveDescricaoAnalise(estrutRequis(lItem).cod_agrup, True), "_")
            With LvPercentagens.ListItems.Add(, estrutRequis(lItem).cod_agrup, descr_ana(1), IIf(bParent, IIf(bAutomatic, ListViewIcons.e_IconExpanded, ListViewIcons.e_IconCollapsed), Empty))
                .ListSubItems.Add(, , analise_colheita_out, , "Tempo colheita: " & media_tempo_colheita & " min. [" & estrutRequis(lItem).tempo_max_colheita & " min.]").ForeColor = ApplicationHexadecimalColors.e_LightRed
                .ListSubItems.Add(, , analise_colheita_in, , "Tempo colheita: " & media_tempo_colheita & " min. [" & estrutRequis(lItem).tempo_max_colheita & " min.]").ForeColor = ApplicationHexadecimalColors.e_Green
                .ListSubItems.Add(, , analise_validacao_out, , "Tempo valida��o: " & media_tempo_validacao & " min. [" & estrutRequis(lItem).tempo_max_validacao & "] min.").ForeColor = ApplicationHexadecimalColors.e_LightRed
                .ListSubItems.Add(, , analise_validacao_in, , "Tempo valida��o: " & media_tempo_validacao & " min. [" & estrutRequis(lItem).tempo_max_validacao & "] min.").ForeColor = ApplicationHexadecimalColors.e_Green
                .ToolTipText = "[ " & estrutRequis(lItem).cod_agrup & " ]" & descr_ana(0)
            End With
        End If
    End If
    DoEvents
    AdicionaItemLV = True
Exit Function
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AdicionaItemLV' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next
End Function

Private Sub RemoveItemLV(ByVal item As MSComctlLib.ListItem)
   
    On Error GoTo ErrorHandler
    LvPercentagens.ListItems.Remove item.Index + 1
    
Exit Sub
ErrorHandler:
    BG_LogFile_Erros "Error in function 'RemoveItemLV' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
End Sub

Private Sub ExpandirLV(ByVal item As MSComctlLib.ListItem)

    Dim lItem As Long
    Dim iShift As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim lStructure As Long
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A expandir arvore..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    'lStructure = RetornaIndiceTubo(Item.index)
    iShift = item.Index + 1
    For lItem = 2 To UBound(estrutRequis): Call AdicionaItemLV(lItem, CLng(iShift), , , True): iShift = iShift + 1: Next
    LvPercentagens.ListItems(1).SmallIcon = ListViewIcons.e_IconExpanded
    DoEvents
    LvPercentagens.Refresh
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

Exit Sub
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExpandirLV' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
End Sub

Private Sub ComprimirLV(ByVal item As MSComctlLib.ListItem)
   
    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim lStructureIndex As Long
    
    On Error GoTo ErrorHandler
    For lItem = 2 To LvPercentagens.ListItems.Count: Call RemoveItemLV(item): Next
    item.SmallIcon = ListViewIcons.e_IconCollapsed
    DoEvents
    LvPercentagens.Refresh

Exit Sub
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ComprimirLV' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
End Sub

Private Function CalculaTempoColheita(dt_requisicao As String, hr_requisicao As String, dt_chegada As String, hr_chegada As String) As Double
    
    Dim date1 As Date
    Dim date2 As Date
    
    On Error GoTo ErrorHandler
    date1 = CDate(dt_requisicao & Space(1) & hr_requisicao)
    date2 = CDate(dt_chegada & Space(1) & hr_chegada)
    CalculaTempoColheita = DateDiff("n", date1, date2)
   Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CalculaTempoColheita' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function CalculaTempoValidacao(dt_chegada As String, hr_chegada As String, dt_Validacao As String, hr_Validacao As String) As Double
    
    Dim date1 As Date
    Dim date2 As Date
    
    On Error GoTo ErrorHandler
    date1 = CDate(dt_chegada & Space(1) & hr_chegada)
    date2 = CDate(dt_Validacao & Space(1) & hr_Validacao)
    CalculaTempoValidacao = DateDiff("n", date1, date2)
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CalculaTempoValidacao' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function CalculaPercentagensColheitaTotal(ByRef total_in As Double, total_out As Double, Optional cod_ana As String) As Boolean
    
    Dim i As Long
    Dim n_colheita_in As Long
    Dim n_colheita_out As Long
    
    On Error GoTo ErrorHandler
    For i = 1 To totalRequis
        If (estrutRequis(i).t_index <> Empty Or (cod_ana <> Empty And estrutRequis(i).cod_agrup = cod_ana)) Then
            If (estrutRequis(i).tempo_colheita > estrutRequis(i).tempo_max_colheita) Then: n_colheita_out = n_colheita_out + 1
            If (estrutRequis(i).tempo_colheita <= estrutRequis(i).tempo_max_colheita) Then: n_colheita_in = n_colheita_in + 1
        End If
    Next
    total_in = BG_newRound((n_colheita_in * 100) / (totalRequis - 1), 1)
    total_out = BG_newRound((n_colheita_out * 100) / (totalRequis - 1), 1)
    CalculaPercentagensColheitaTotal = True
    Exit Function
    
ErrorHandler:
BG_LogFile_Erros "Error in function 'CalculaPercentagensColheitaTotal' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function CalculaPercentagensValidacaoTotal(ByRef total_in As Double, total_out As Double, Optional cod_ana As String) As Boolean
  
    Dim i As Long
    Dim n_validacao_in As Long
    Dim n_validacao_out As Long
    
    On Error GoTo ErrorHandler
    For i = 1 To totalRequis
        If (estrutRequis(i).t_index <> Empty Or (cod_ana <> Empty And estrutRequis(i).cod_agrup = cod_ana)) Then
            If (estrutRequis(i).tempo_validacao > estrutRequis(i).tempo_max_validacao) Then: n_validacao_out = n_validacao_out + 1
            If (estrutRequis(i).tempo_validacao <= estrutRequis(i).tempo_max_validacao) Then: n_validacao_in = n_validacao_in + 1
        End If
    Next
    total_in = BG_newRound((n_validacao_in * 100) / (totalRequis - 1), 1)
    total_out = BG_newRound((n_validacao_out * 100) / (totalRequis - 1), 1)
    CalculaPercentagensValidacaoTotal = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CalculaPercentagensValidacaoTotal' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
End Function

Private Function AnaliseJaExiste(n_req As String, cod_ana As String) As Boolean
    AnaliseJaExiste = False
    Exit Function
    Dim i As Long
    On Error GoTo ErrorHandler
    For i = 1 To totalRequis
        If (estrutRequis(i).t_index <> Empty) Then
            If (estrutRequis(i).n_req = n_req And estrutRequis(i).cod_agrup = cod_ana) Then
                AnaliseJaExiste = True
                Exit Function
            End If
        End If
    Next
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AnaliseJaExiste' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
End Function

Private Function ItemJaExiste(cod_ana As String) As Boolean
    
    Dim item As Variant
    
     On Error GoTo ErrorHandler
    For Each item In LvPercentagens.ListItems
        If (item.Key = cod_ana) Then
            ItemJaExiste = True
            Exit Function
        End If
    Next
   Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ItemJaExiste' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
End Function

Private Function CalculaPercentagensColheitaAnalise(cod_ana As String, ByRef media_tempo As Double, ByRef total_in As Double, total_out As Double) As Boolean
    
    Dim i As Long
    Dim n_colheita_in As Long
    Dim n_colheita_out As Long
    Dim total_ana As Long
    
    On Error GoTo ErrorHandler
    For i = 1 To totalRequis
        If (estrutRequis(i).t_index <> Empty And estrutRequis(i).cod_agrup = cod_ana) Then
            total_ana = total_ana + 1
            media_tempo = media_tempo + estrutRequis(i).tempo_colheita
            If (estrutRequis(i).tempo_colheita > estrutRequis(i).tempo_max_colheita) Then: n_colheita_out = n_colheita_out + 1
            If (estrutRequis(i).tempo_colheita <= estrutRequis(i).tempo_max_colheita) Then: n_colheita_in = n_colheita_in + 1
        End If
    Next
    total_in = BG_newRound((n_colheita_in * 100) / total_ana, 1)
    total_out = BG_newRound((n_colheita_out * 100) / total_ana, 1)
    media_tempo = BG_newRound(media_tempo / total_ana, 1)
    CalculaPercentagensColheitaAnalise = True
    Exit Function
    
ErrorHandler:
BG_LogFile_Erros "Error in function 'CalculaPercentagensColheitaAnalise' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function CalculaPercentagensValidacaoAnalise(cod_ana As String, ByRef media_tempo As Double, ByRef total_in As Double, total_out As Double) As Boolean
  
    Dim i As Long
    Dim n_validacao_in As Long
    Dim n_validacao_out As Long
    Dim total_ana As Long
     
    On Error GoTo ErrorHandler
    For i = 1 To totalRequis
        If (estrutRequis(i).t_index <> Empty And estrutRequis(i).cod_agrup = cod_ana) Then
            total_ana = total_ana + 1
            media_tempo = media_tempo + estrutRequis(i).tempo_validacao
            If (estrutRequis(i).tempo_validacao > estrutRequis(i).tempo_max_validacao) Then: n_validacao_out = n_validacao_out + 1
            If (estrutRequis(i).tempo_validacao <= estrutRequis(i).tempo_max_validacao) Then: n_validacao_in = n_validacao_in + 1
        End If
    Next
    total_in = BG_newRound((n_validacao_in * 100) / total_ana, 1)
    total_out = BG_newRound((n_validacao_out * 100) / total_ana, 1)
    media_tempo = BG_newRound(media_tempo / total_ana, 1)
    CalculaPercentagensValidacaoAnalise = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CalculaPercentagensValidacaoAnalise' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
End Function

Private Sub LvPercentagens_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ErrorHandler
    If (LvPercentagens.ListItems.Count = Empty) Then: Exit Sub
    Select Case KeyCode
        Case vbKeyLeft, vbKeyRight: HandleListViewTree LvPercentagens.SelectedItem, KeyCode
        Case Else: ' Empty
    End Select
    Exit Sub
    
ErrorHandler:
BG_LogFile_Erros "Error in function 'LvPercentagens_KeyDown' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Private Sub MostraMarcadores(Grafico As MSChart, mostra As Boolean, color As RGB)
    
    Dim series As series

    On Error GoTo ErrorHandler
    
    For Each series In Grafico.Plot.SeriesCollection
        With series
            .Pen.VtColor.Set color.t_R, color.t_G, color.t_B
            .SeriesMarker.Show = True
            '.Pen.Style = VtPenStyleNull
            .SeriesMarker.Show = mostra
            .SeriesMarker.Auto = False
            .DataPoints(-1).Marker.Visible = mostra
            .DataPoints(-1).Marker.Size = 250
            '.DataPoints(-1).Marker.Style = VtMarkerStyleDiamond
            '.DataPoints(-1).Marker.Style = VtMarkerStyleCircle
            '.DataPoints(-1).Marker.Style = VtMarkerStyleUpTriangle
            '.DataPoints(-1).Marker.Style = VtMarkerStyleSquare
            .DataPoints(-1).Marker.Style = VtMarkerStyle3dBall
            '.DataPoints(-1).Marker.Style = VtMarkerStyleStar
            .DataPoints(-1).Marker.Pen.Width = 25
            .DataPoints(-1).Marker.FillColor.Set color.t_R, color.t_G, color.t_B
        End With
    Next
    
    With Grafico.Plot.SeriesCollection(1)
        With .DataPoints(-1).DataPointLabel
            '.LocationType = VtChLabelLocationTypeAbovePoint
            .LocationType = VtChLabelLocationTypeAbovePoint
            .Custom = mostra
            .Component = IIf(mostra, VtChLabelComponent.VtChLabelComponentValue, VtChLabelComponent.VtChLabelComponentSeriesName)
            '.VtFont.VtColor.Set 0, 0, 255
        End With
    End With
     Exit Sub
    
ErrorHandler:
BG_LogFile_Erros "Error in function 'MostraMarcadores' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

Private Sub ExpandeGrafico()

    On Error GoTo ErrorHandler
    FrResultados.top = 120
    FrResultados.left = 120
    FrResultados.Height = 6975
    LvPercentagens.Visible = False
    SSTab1.left = 0
    SSTab1.top = 0
    SSTab1.Width = 13815
    SSTab1.Height = 6975
    MscColheita.Height = 6495
    MscColheita.Width = 13575
    MscValidacao.Height = 6495
    MscValidacao.Width = 13575
    BtExpande1.left = 13320
    BtExpande1.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphCollapsed).Picture
    BtExpande1.ToolTipText = "Colapsa gr�fico"
    BtExpande2.left = 13320
    BtExpande2.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphCollapsed).Picture
    BtExpande2.ToolTipText = "Colapsa gr�fico"
    Exit Sub
    
ErrorHandler:
BG_LogFile_Erros "Error in function 'ExpandeGrafico' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

Private Sub ColapsaGrafico()

    On Error GoTo ErrorHandler
    FrResultados.top = 2760
    FrResultados.left = 120
    LvPercentagens.Visible = True
    SSTab1.left = 5400
    SSTab1.top = 0
    FrResultados.Height = 4335
    MscColheita.Height = 3855
    MscColheita.Width = 8175
    MscValidacao.Height = 3855
    MscValidacao.Width = 8175
    SSTab1.Width = 8415
    SSTab1.Height = 4335
    BtExpande1.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphExpanded).Picture
    BtExpande1.ToolTipText = "Expande gr�fico"
    BtExpande1.top = 0
    BtExpande1.left = 7920
    BtExpande2.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconGraphExpanded).Picture
    BtExpande2.ToolTipText = "Expande gr�fico"
    BtExpande2.top = 0
    BtExpande2.left = 7920
    Exit Sub
    
ErrorHandler:
BG_LogFile_Erros "Error in function 'ColapsaGrafico' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Function DevolveLocaisSel() As String
 
    Dim i As Integer
    On Error GoTo ErrorHandler
    If (EcLocais.SelCount > Empty) Then
        For i = 0 To EcLocais.ListCount - 1
           If (EcLocais.Selected(i)) Then: DevolveLocaisSel = DevolveLocaisSel & EcLocais.List(i) & ";"
        Next
        DevolveLocaisSel = Mid(DevolveLocaisSel, 1, Len(DevolveLocaisSel) - 1)
    End If
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'DevolveLocaisSel' in form FormEstatTemposEstapas (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function DevolveIndiceInserirValidacao(ByVal Label As String) As Integer
    Dim i As Integer
    Dim modo As Integer
    On Error GoTo TrataErro
    
    If OptAgrupamento(lAgrupAno).value = True Then
        modo = lAgrupAno
    ElseIf OptAgrupamento(lAgrupMes).value = True Then
        modo = lAgrupMes
    ElseIf OptAgrupamento(lAgrupDia).value = True Then
        modo = lAgrupDia
    ElseIf OptAgrupamento(lAgrupHora).value = True Then
        modo = lAgrupHora
    End If
    
    If modo = lAgrupAno Then
        Label = "01-01" & Label
    ElseIf modo = lAgrupMes Then
        Label = "01-" & Label
    End If
    For i = 1 To TotalGraficoValidacao
        If modo = lAgrupAno Then
            If CDate(Label) < CDate("01-01" & BL_HandleNull(estrutGraficoValidacao(i).Label, "9999")) Then
                ActualizaEstruturaValidacao i
                DevolveIndiceInserirValidacao = i
                Exit Function
            ElseIf CDate(Label) = CDate("01-01" & BL_HandleNull(estrutGraficoValidacao(i).Label, "9999")) Then
                DevolveIndiceInserirValidacao = i
                Exit Function
            End If
        ElseIf modo = lAgrupMes Then
            If CDate(Label) < CDate("01-" & BL_HandleNull(estrutGraficoValidacao(i).Label, "01-9999")) Then
                ActualizaEstruturaValidacao i
                DevolveIndiceInserirValidacao = i
                Exit Function
            ElseIf CDate(Label) = CDate("01-" & BL_HandleNull(estrutGraficoValidacao(i).Label, "01-9999")) Then
                DevolveIndiceInserirValidacao = i
                Exit Function
            End If
        ElseIf modo = lAgrupDia Then
            If CDate(Label) < CDate(BL_HandleNull(estrutGraficoValidacao(i).Label, "01-01-9999")) Then
                ActualizaEstruturaValidacao i
                DevolveIndiceInserirValidacao = i
                Exit Function
            ElseIf CDate(Label) = CDate(BL_HandleNull(estrutGraficoValidacao(i).Label, "01-01-9999")) Then
                DevolveIndiceInserirValidacao = i
                Exit Function
            End If
        ElseIf modo = lAgrupHora Then
            If CInt(Label) < CInt(BL_HandleNull(estrutGraficoValidacao(i).Label, "24")) Then
                ActualizaEstruturaValidacao i
                DevolveIndiceInserirValidacao = i
                Exit Function
            ElseIf CInt(Label) = CInt(BL_HandleNull(estrutGraficoValidacao(i).Label, "24")) Then
                DevolveIndiceInserirValidacao = i
                Exit Function
            End If
        End If
    Next
    TotalGraficoValidacao = TotalGraficoValidacao + 1
    ReDim Preserve estrutGraficoValidacao(TotalGraficoValidacao)
    DevolveIndiceInserirValidacao = TotalGraficoValidacao
Exit Function
TrataErro:
    BG_LogFile_Erros "DevolveIndiceInserirValidacao: " & Err.Description, Me.Name, "DevolveIndiceInserirValidacao", False
    Exit Function
    Resume Next
End Function

Private Sub ActualizaEstruturaValidacao(ByVal indice As Integer)
    Dim i As Integer
    On Error GoTo TrataErro
    
    TotalGraficoValidacao = TotalGraficoValidacao + 1
    ReDim Preserve estrutGraficoValidacao(TotalGraficoValidacao)
    
    For i = TotalGraficoValidacao To indice + 1 Step -1
        estrutGraficoValidacao(i).Label = estrutGraficoValidacao(i - 1).Label
        estrutGraficoValidacao(i).media = estrutGraficoValidacao(i - 1).media
        estrutGraficoValidacao(i).NReq = estrutGraficoValidacao(i - 1).NReq
        estrutGraficoValidacao(i).soma = estrutGraficoValidacao(i - 1).soma
        
        estrutGraficoValidacao(i - 1).Label = ""
        estrutGraficoValidacao(i - 1).media = 0
        estrutGraficoValidacao(i - 1).NReq = 0
        estrutGraficoValidacao(i - 1).soma = 0
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ActualizaEstruturaValidacao: " & Err.Description, Me.Name, "ActualizaEstruturaValidacao", False
    Exit Sub
    Resume Next
End Sub



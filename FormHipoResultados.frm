VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Object = "{EE757A1F-B0AC-40BC-9E72-B8651740F53E}#1.0#0"; "ARProgBar.ocx"
Begin VB.Form FormHipoResultados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "&H8000000A&"
   ClientHeight    =   11370
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   19095
   Icon            =   "FormHipoResultados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   11370
   ScaleWidth      =   19095
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrObservacao 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   120
      TabIndex        =   34
      Top             =   120
      Width           =   13815
      Begin VB.CommandButton Command1 
         Height          =   295
         Left            =   13200
         Picture         =   "FormHipoResultados.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   38
         ToolTipText     =   "Elimina observa��o"
         Top             =   680
         Width           =   495
      End
      Begin VB.CommandButton BtRet 
         Height          =   295
         Left            =   13200
         Picture         =   "FormHipoResultados.frx":03F7
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Volta ao ecr�"
         Top             =   400
         Width           =   495
      End
      Begin VB.TextBox EcObs 
         Appearance      =   0  'Flat
         Height          =   885
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   37
         Top             =   120
         Width           =   12975
      End
      Begin VB.CommandButton BtGuardaObs 
         Height          =   295
         Left            =   13200
         Picture         =   "FormHipoResultados.frx":07A9
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Actualiza observa��o da requisi��o"
         Top             =   120
         Width           =   495
      End
      Begin VB.Shape Shape6 
         BorderColor     =   &H8000000C&
         Height          =   1095
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   13815
      End
   End
   Begin VB.TextBox EcSeqUtente 
      Height          =   375
      Left            =   1080
      TabIndex        =   30
      Text            =   "Text1"
      Top             =   10680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame FrPendentes 
      BorderStyle     =   0  'None
      Height          =   7935
      Left            =   120
      TabIndex        =   20
      Top             =   1320
      Width           =   13815
      Begin VB.ListBox EcLocais 
         Appearance      =   0  'Flat
         Height          =   705
         Left            =   9480
         Style           =   1  'Checkbox
         TabIndex        =   27
         Top             =   120
         Width           =   4215
      End
      Begin MSComCtl2.DTPicker DtFinal 
         Height          =   255
         Left            =   4680
         TabIndex        =   22
         Top             =   120
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   450
         _Version        =   393216
         Format          =   106102785
         CurrentDate     =   40694
      End
      Begin MSComctlLib.ListView LvPendentes 
         Height          =   6855
         Left            =   0
         TabIndex        =   21
         Top             =   1080
         Width           =   13815
         _ExtentX        =   24368
         _ExtentY        =   12091
         SortOrder       =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
      Begin MSComCtl2.DTPicker DtInicial 
         Height          =   255
         Left            =   1080
         TabIndex        =   24
         Top             =   120
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   450
         _Version        =   393216
         Format          =   106102785
         CurrentDate     =   40694
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "&Locais"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8520
         TabIndex        =   28
         Top             =   120
         Width           =   540
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "&Data Final"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3840
         TabIndex        =   26
         Top             =   120
         Width           =   765
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "&Data Inicial"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   25
         Top             =   120
         Width           =   855
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H8000000C&
         Height          =   975
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   13815
      End
   End
   Begin MSComCtl2.MonthView MvAgenda 
      Height          =   2310
      Left            =   120
      TabIndex        =   17
      Top             =   6480
      Width           =   2460
      _ExtentX        =   4339
      _ExtentY        =   4075
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      MonthBackColor  =   16777215
      StartOfWeek     =   106102786
      TitleBackColor  =   -2147483644
      TitleForeColor  =   0
      TrailingForeColor=   -2147483644
      CurrentDate     =   40674
   End
   Begin MSChart20Lib.MSChart MSResultados 
      Height          =   1695
      Left            =   120
      OleObjectBlob   =   "FormHipoResultados.frx":089E
      TabIndex        =   15
      Top             =   4680
      Width           =   13815
   End
   Begin VB.ComboBox CbPeriodo 
      Height          =   315
      Left            =   5760
      TabIndex        =   13
      Top             =   10080
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.TextBox EcAuxiliar 
      Alignment       =   2  'Center
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   9480
      TabIndex        =   12
      Top             =   9960
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Frame FrCabecalho 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   13815
      Begin VB.CommandButton BtProcuraPendentes 
         Height          =   300
         Left            =   6120
         Picture         =   "FormHipoResultados.frx":22F1
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Pesquisa requisi��es pendentes"
         Top             =   210
         Width           =   600
      End
      Begin VB.CommandButton BtObservacao 
         BackColor       =   &H00FFFFFF&
         Height          =   300
         Left            =   5880
         MaskColor       =   &H00FFFFFF&
         Picture         =   "FormHipoResultados.frx":29DB
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   210
         Width           =   255
      End
      Begin VB.TextBox EcEstadoReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   240
         Width           =   3735
      End
      Begin VB.CommandButton BtPesqUtente 
         Height          =   300
         Left            =   6120
         Picture         =   "FormHipoResultados.frx":2D51
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Pesquisa dados do utente"
         Top             =   840
         Width           =   600
      End
      Begin VB.TextBox EcDtChega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   840
         Width           =   2415
      End
      Begin VB.TextBox EcDescrLocal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox EcNomeUtente 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   840
         Width           =   4095
      End
      Begin VB.TextBox EcUtente 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   720
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   4
         Top             =   840
         Width           =   1215
      End
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   720
         MaxLength       =   9
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
      Begin MSComctlLib.ListView LvDiagnoticos 
         Height          =   855
         Left            =   7080
         TabIndex        =   31
         Top             =   240
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   1508
         SortOrder       =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H8000000C&
         Height          =   495
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   6855
      End
      Begin VB.Shape Shape4 
         BorderColor     =   &H8000000C&
         Height          =   495
         Left            =   10320
         Shape           =   4  'Rounded Rectangle
         Top             =   720
         Width           =   3495
      End
      Begin VB.Shape Shape7 
         BorderColor     =   &H8000000C&
         Height          =   495
         Left            =   10320
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   3495
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Chegada"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   10440
         TabIndex        =   11
         Top             =   840
         Width           =   720
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H8000000C&
         Height          =   1095
         Left            =   6960
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   3255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Local"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   10440
         TabIndex        =   9
         Top             =   240
         Width           =   435
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   8
         Top             =   840
         Width           =   540
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H8000000C&
         Height          =   495
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   720
         Width           =   6855
      End
      Begin VB.Label LbNumero 
         AutoSize        =   -1  'True
         Caption         =   "&Req"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   315
      End
   End
   Begin VB.ComboBox CbMedicamento 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   11640
      TabIndex        =   1
      Top             =   9840
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   255
      Left            =   10800
      ScaleHeight     =   195
      ScaleWidth      =   435
      TabIndex        =   0
      Top             =   10080
      Width           =   495
   End
   Begin MSComctlLib.ImageList ImageListIcons 
      Left            =   8280
      Top             =   9480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":95A3
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":96FD
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":9857
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":9C57
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":A057
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":A3F1
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":A78B
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":AB25
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":C7FF
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":CBC1
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":D2BB
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":13B1D
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":1A37F
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormHipoResultados.frx":1A919
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView LvTerapeutica 
      Height          =   1815
      Left            =   120
      TabIndex        =   14
      Top             =   1320
      Width           =   13815
      _ExtentX        =   24368
      _ExtentY        =   3201
      SortOrder       =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin MSComctlLib.ListView LvResultados 
      Height          =   1335
      Left            =   120
      TabIndex        =   16
      Top             =   3240
      Width           =   13815
      _ExtentX        =   24368
      _ExtentY        =   2355
      SortOrder       =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin MSComctlLib.ListView LvAgenda 
      Height          =   2670
      Left            =   2640
      TabIndex        =   18
      Top             =   6480
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   4710
      SortOrder       =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin ARProgBarCtrl.ARProgressBar BarMarcacoes 
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   8880
      Width           =   2460
      _ExtentX        =   4339
      _ExtentY        =   450
      Value           =   100
      ForeColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CaptionForeColor=   16777215
      UseGradient     =   -1  'True
      IniColor        =   8454016
      EndColor        =   192
   End
End
Attribute VB_Name = "FormHipoResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2011.05.04   .
'     .       � 2011 Glintt-HS      .
'      .............................

Option Explicit

' GUARDA O IDENTIFICADOR DA LISTA (TERAPEUTICA).
Private lhwndLvTerapeutica As Long

' GUARDA INFORMACAO SOBRE O HIT NUMA LISTVIEW.
Private lvhitLvTerapeutica As LVHITTESTINFO

' GUARDA O INDICE DO ITEM DA LISTA CUJO SUBITEM ESTA SENDO EDITADO (TERAPEUTICA).
Private lidxLvTerapeutica As Long

' GUARDA O INDICE ZERO DE BASE SUBITEM.SUBITEM SENDO EDITADO (TERAPEUTICA).
Private lsubidxLvTerapeutica As Long

' GUARDA O IDENTIFICADOR DA LISTA (RESULTADOS).
Private lhwndLvResultados As Long

' GUARDA O INDICE DO ITEM DA LISTA CUJO SUBITEM ESTA SENDO EDITADO (RESULTADOS).
Private lidxLvResultados As Long

' GUARDA O INDICE ZERO DE BASE SUBITEM.SUBITEM SENDO EDITADO (RESULTADOS).
Private lsubidxLvResultados As Long

' GUARDA O IDENTIFICADOR DA COMBOBOX (MEDICAMENTO)
Private lhwndCbMedicamento As Long

' GUARDA O IDENTIFICADOR DA COMBOBOX (PERIODO)
Private lhwndCbPeriodo As Long

' GUARDA O IDENTIFICADOR DA TEXTBOX (AUXILIAR)
Private lhwndEcAuxiliar As Long

' GUARDA O ULTIMO INDEX DA COLUNA (TERAPEUTICA).
Private ilastidxLvTerapeutica As Integer

' GUARDA a ULTIMA COLUNA DA LISTA (TERAPEUTICA).
Private ilastCidxLvTerapeutica As Integer

' GUARDA a ULTIMA LINHA DA LISTA (TERAPEUTICA).
Private ilastRidxLvTerapeutica As Integer

' KEEP THE SOURCE ITEM OF DRAGGING.
Private oSourceItem As ListItem

' ESTADO DO ECRA.
Private Estado As Integer

' CAMPO ACTIVO DO ECRA.
Private CampoDeFocus As Object

Private Enum EstadoRequisicao
    
    e_EstadoReqAceite = 0
    e_EstadoReqNaoAceite = 1
    
End Enum

' ENUMERACAO LISTVIEW ICONS.
Private Enum ListViewIcons
    
    e_IconCollapsed = 1
    e_IconExpanded = 2
    e_IconLeftGray = 3
    e_IconRightGray = 4
    e_IconRightYellow = 5
    e_IconCantDrop = 6
    e_IconRightBlue = 7
    e_IconForbidden = 8
    e_IconReturn = 9
    e_IconSearch = 10
    e_IconForbidden2 = 11
    e_IconOk = 12
    e_IconDiag = 13
    
End Enum

' ENUMERACAO PARA DISTINGUIR LISTAS DO ECRA.
Private Enum ListasEcra
    
    e_LvTerapeutica
    e_LvResultados
    e_LvAgenda
    e_LvPendentes
    e_LvDiagnosticos
    
End Enum

' COMMANDO ADODB PARA RETORNAR DADOS PARA UMA REQUISICAO.
Private CmdReq As New ADODB.Command

' ENUMERACAO PARA AS COLUNAS DA LISTA (TERAPEUTICA).
Private Enum LvTerapeuticaColunas
  
    e_cod_tera_med = 1
    e_res_ana = 2
    e_periodo = 3
    e_dia_1 = 4
    e_dia_2 = 5
    e_dia_3 = 6
    e_dia_4 = 7
    e_dia_5 = 8
    e_dia_6 = 9
    e_dia_7 = 10
    e_user_cri = 11
    e_dt_cri = 12
    
End Enum

' ENUMERACAO PARA AS COLUNAS DA LISTA (AGENDA).
Private Enum LvAgendaColunas
  
    e_n_req = 1
    e_estado_req = 2
    e_dt_prevista = 3
    e_tipo_utente = 4
    e_num_utente = 5
    e_nome_utente = 6
    e_user_cri = 7
    e_dt_cri = 8
    
End Enum

' ESTRUTURA DA AGENDA (SL_AGENDA).
Private Type EstruturaAgenda

    t_index As Long
    t_n_req As String
    t_estado_req As String
    t_dt_prevista As String
    t_seq_utente As String
    t_tipo_utente As String
    t_num_utente As String
    t_nome_utente As String
    t_user_cri As String
    t_dt_cri As String
    t_hr_cri As String

End Type

' ESTRUTURA DA TERAPEUTICA (SL_TM_UTE).
Private Type EstruturaTerapeutica

    t_index As Long
    t_res_ana As String
    t_n_req As String
    t_cod_tera_med As String
    t_periodo As String
    t_dia_1 As String
    t_dia_2 As String
    t_dia_3 As String
    t_dia_4 As String
    t_dia_5 As String
    t_dia_6 As String
    t_dia_7 As String
    t_user_cri As String
    t_dt_cri As String

End Type

' ESTRUTURA DOS RESULTADOS (SL_REALIZA/SL_RES_ALFAN).
Private Type EstruturaResultados
    
    t_index As Long
    t_n_req As String
    t_seq_realiza As String
    t_dt_cri As String
    t_hr_cri As String
    t_user_cri As String
    t_seq_ana As String
    t_descr_ana As String
    t_resultado As String
    
End Type

' ARRAY TERAPEUTICA COM A ESTRUTURA 'ESTRUTURATERAPEUTICA'.
Private eTerapeutica() As EstruturaTerapeutica

' TOTAL REGISTOS DO ARRAY DA TERAPEUTICA.
Private TotalTerapeutica As Long

' ARRAY RESULTADOS COM A ESTRUTURA 'ESTRUTURARESULTADOS'.
Private eResultados() As EstruturaResultados

' TOTAL REGISTOS DO ARRAY DOS RESULTADOS.
Private TotalResultados As Long

' ARRAY AGENDA COM A ESTRUTURA 'ESTRUTURAAGENDA'.
Private eAgenda() As EstruturaAgenda

' TOTAL REGISTOS DO ARRAY DA AGENDA.
Private TotalAgenda As Long

' GUARDA ANO ANTERIOR (CALENDARIO).
Private AnoAnterior As Long

' GUARDA MES ANTERIOR (CALENDARIO).
Private MesAnterior As MonthConstants

' GUARDA DIA ANTERIOR (CALENDARIO).
Private DiaAnterior As DayConstants

' CONSTROI SQL TERAPEUTICA.
Private Function ConstroiSqlTerapeutica(requisicao As String) As String

    Dim sql As String
    
    On Error GoTo ErrorHandler
    
    If (gAnaliseDefeitoHipocoagulado = Empty) Then: BG_Mensagem mediMsgBox, "An�lise por defeito n�o definida!", vbExclamation, " Aten��o"
    
    ' SQL BASE
    sql = "select sl_requis.n_req,sl_tm_ute.cod_tera_med,sl_tm_ute.dia_1,sl_tm_ute.dia_2,sl_tm_ute.dia_3,sl_tm_ute.dia_4," & _
          "sl_tm_ute.dia_5,sl_tm_ute.dia_6,sl_tm_ute.dia_7,sl_tm_ute.user_cri,sl_tm_ute.dt_cri,sl_tm_ute.periodo,sl_res_alfan.result " & _
          "from sl_tm_ute, sl_requis,sl_realiza,sl_res_alfan " & _
          "where sl_requis.n_req = sl_tm_ute.n_req and sl_requis.flg_hipo = 1 and sl_tm_ute.flg_hipo = 1 " & _
          "and sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(gAnaliseDefeitoHipocoagulado) & _
          "and sl_realiza.n_req = sl_requis.n_req and sl_realiza.seq_realiza = sl_res_alfan.seq_realiza " & _
          "and sl_res_alfan.n_res = 1"
    
    ' SQL FILTROS
    If (Empty <> requisicao) Then: sql = sql & "and sl_requis.seq_utente = (select seq_utente from sl_requis where n_req = " & requisicao & ")"
    
    ' SQL ORDENACAO
    sql = sql & " order by sl_tm_ute.dt_cri desc"
    ConstroiSqlTerapeutica = sql
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConstroiSqlTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function DevolveTipoTabela(tabela As String)

    On Error GoTo ErrorHandler
    Select Case (gTipoActividade)
        Case TiposActividade.e_Marcacao: DevolveTipoTabela = tabela & "_consultas"
        Case TiposActividade.e_Requisicao: DevolveTipoTabela = tabela
        Case Else: ' Empty
    End Select
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'DevolveTipoTabela' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' CONSTROI SQL PENDENTES.
Private Function ConstroiSqlPendentes() As String

    Dim sql As String
    Dim i As Integer
    
    On Error GoTo ErrorHandler
    
    ' SQL BASE
    sql = "select * from sl_requis r where r.flg_hipo = 1 and r.dt_chega is not null " & _
          "and trim(r.estado_req) is not null and r.n_req not in (select t.n_req from sl_tm_ute t " & _
          "where t.flg_hipo = 1) and r.dt_chega between " & BL_TrataDataParaBD(DtInicial.Value) & _
          " and " & BL_TrataDataParaBD(DtFinal.Value)
 
    ' SQL FILTROS
    If (EcLocais.SelCount > Empty) Then
        sql = sql & " and r.cod_local in ("
        For i = 0 To EcLocais.ListCount - 1
            If (EcLocais.Selected(i)) Then: sql = sql & EcLocais.ItemData(i) & ","
        Next
        sql = Mid(sql, 1, Len(sql) - 1) & ")"
    End If
    
    ' SQL ORDENACAO
    sql = sql & " order by r.dt_chega asc, r.hr_chega asc"
    ConstroiSqlPendentes = sql
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConstroiSqlPendentes' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' CONSTROI SQL AGENDA.
Private Function ConstroiSqlAgenda(mes As MonthConstants) As String

    Dim sql As String
    
    On Error GoTo ErrorHandler
    
    ' SQL BASE
    sql = "select r.n_req,r.estado_req,r.dt_previ,r.seq_utente,i.t_utente,i.utente,i.nome_ute,r.user_cri,r.dt_cri,r.hr_cri " & _
          "from " & DevolveTipoTabela("sl_requis") & " r, sl_identif i,sl_agenda a " & _
          "where r.flg_hipo = 1 and a.n_req = r.n_req and r.seq_utente = i.seq_utente and " & _
          "a.cod_t_agenda = " & TiposAgenda.e_Hipocoagulados & " and " & _
          "r.dt_chega is null"
 
    ' SQL FILTROS
    If (Empty <> mes) Then: sql = sql & " and to_numeric (to_char (r.dt_previ, 'mm'))= " & mes
    
    ' SQL ORDENACAO
    sql = sql & " order by r.n_req desc"
    ConstroiSqlAgenda = sql
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConstroiSqlAgenda' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' CONSTROI SQL RESULTADOS.
Private Function ConstroiSqlResultados(requisicao As String) As String
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    
    ' SQL BASE
    sql = "select sl_requis.n_req,sl_realiza.seq_realiza,sl_realiza.dt_cri,sl_ana_s.seq_ana_s,sl_ana_s.descr_ana_s,sl_realiza.hr_cri,sl_realiza.user_cri,sl_res_alfan.result " & _
          "from sl_realiza, sl_res_alfan, sl_requis, sl_ana_s " & _
          "where sl_requis.n_req = sl_realiza.n_req " & _
          "and sl_requis.flg_hipo = 1 " & _
          "and sl_realiza.seq_realiza = sl_res_alfan.seq_realiza " & _
          "and sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
          "and sl_realiza.flg_estado in (" & gEstadoAnaValidacaoMedica & "," & gEstadoAnaImpressa & ") "
    
    ' SQL FILTROS
    If (Empty <> requisicao) Then: sql = sql & "and sl_realiza.seq_utente in (select seq_utente from sl_realiza where n_req=" & requisicao & ")"
    
    ' SQL ORDENACAO
    sql = sql & " order by sl_realiza.dt_cri desc,sl_realiza.hr_cri desc"
    ConstroiSqlResultados = sql
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConstroiSqlResultados' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Sub ActualizaObservacaoReq(numero_requisicao As String, observacao As String)

    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "update sl_requis set obs_req = " & BL_TrataStringParaBD(observacao) & " where n_req = " & numero_requisicao
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    If (gSQLError = Empty) Then: BG_Mensagem mediMsgBox, "Observa��o da requisi��o alterada", vbInformation, " Observa��o"
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Observa��o da requisi��o n�o alterada", vbExclamation, " Observa��o"
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaObservacaoReq' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub BtGuardaObs_Click()
        
    gMsgTitulo = " Observa��o"
    gMsgMsg = "Tem a certeza que quer actualizar a observa��o da requisi��o " & EcNumReq.text & "?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbNo) Then: Exit Sub
    Call ActualizaObservacaoReq(EcNumReq.text, EcObs.text)
    
End Sub

Private Sub BtObservacao_Click()

    Call GereFrameObservacao(FrObservacao.Visible)
    
End Sub

Private Sub BtPesqUtente_Click()
    
    If (EcSeqUtente.text = Empty) Then: BG_Mensagem mediMsgBox, "N�mero sequencial do utente n�o definido!", vbExclamation, " Aten��o": Exit Sub
    Call ConsultaUtente(EcSeqUtente)
    
End Sub

Private Sub BtProcuraPendentes_Click()

    Call GereFramePendentes(FrPendentes.Visible)

End Sub

' GESTAO DA FRAME DA OBSERVACAO
Private Sub GereFrameObservacao(visivel As Boolean)

    On Error GoTo ErrorHandler
    If (Not visivel) Then
        FrObservacao.Visible = True
        DoEvents
        FrObservacao.Width = 13815
        FrObservacao.Height = 1095
        FrObservacao.left = 120
        FrObservacao.top = 120
        DoEvents
    Else
        FrObservacao.Visible = False
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GereFrameObservacao' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' GESTAO DA FRAME DE REQUISICOES DE PENDENTES.
Private Sub GereFramePendentes(visivel As Boolean)

    On Error GoTo ErrorHandler
    If (Not visivel) Then
        FrPendentes.Visible = True
        DoEvents
        FrPendentes.Width = 13815
        FrPendentes.Height = 7935
        FrPendentes.left = 120
        FrPendentes.top = 1320
        BtProcuraPendentes.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconReturn).Picture
        BtProcuraPendentes.ToolTipText = "Volta ao ecr�"
        DoEvents
        LimpaListaLocais
        DoEvents
        FuncaoProcurar
        DoEvents
    Else
        BtProcuraPendentes.Picture = ImageListIcons.ListImages(ListViewIcons.e_IconSearch).Picture
        BtProcuraPendentes.ToolTipText = "Pesquisa requisi��es pendentes"
        FrPendentes.Visible = False
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GereFramePendentes' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub CbMedicamento_Click()
    
    Dim rc As RECT
    
    If (Empty = CbMedicamento.text) Then: Exit Sub
    eTerapeutica(lidxLvTerapeutica).t_cod_tera_med = BG_DaComboSel(CbMedicamento)
    LvTerapeutica.ListItems(lidxLvTerapeutica).text = CbMedicamento.text
    EnableCbMedicamento False, rc
    
End Sub

Private Sub CbMedicamento_LostFocus()
    
    Dim rc As RECT
    EnableCbMedicamento False, rc
    
End Sub

Private Sub CbPeriodo_LostFocus()
    
    Dim rc As RECT
    EnableCbPeriodo False, rc
    
End Sub
Private Sub CbPeriodo_Click()
    
    Dim rc As RECT
   
    If (Empty = CbPeriodo.text) Then: Exit Sub
    eTerapeutica(lidxLvTerapeutica).t_periodo = BG_DaComboSel(CbPeriodo)
    LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(lsubidxLvTerapeutica - 1).text = CbPeriodo.text
    EnableCbPeriodo False, rc
    Call DefinePeriodo(CInt(LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(lsubidxLvTerapeutica - 1).text))
  
End Sub

' DEFINE O PERIODO E FAZ A GESTAO DOS CAMPOS DA DOSE.
Private Sub DefinePeriodo(periodo As Integer)

    Dim i As Integer
    
    On Error GoTo ErrorHandler
    For i = LvTerapeuticaColunas.e_dia_1 To LvTerapeuticaColunas.e_dia_7
        If (Empty = periodo) Then
            LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(i - 1).ReportIcon = ListViewIcons.e_IconForbidden
            LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(i - 1).text = Empty
        Else
            periodo = periodo - 1
            LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(i - 1).ReportIcon = Empty
        End If
    Next
    Exit Sub
      
ErrorHandler:
    BG_LogFile_Erros "Error in function 'DefinePeriodo' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub
    
Private Sub MostraMarcadores(mostra As Boolean)
    
    Dim series As series

    For Each series In MSResultados.Plot.SeriesCollection
        With series
            .SeriesMarker.Show = True
            '.Pen.Style = VtPenStyleNull
            .SeriesMarker.Show = mostra
            .SeriesMarker.Auto = False
            .DataPoints(-1).Marker.Visible = mostra
            .DataPoints(-1).Marker.Size = 250
            '.DataPoints(-1).Marker.Style = VtMarkerStyleDiamond
            '.DataPoints(-1).Marker.Style = VtMarkerStyleCircle
            '.DataPoints(-1).Marker.Style = VtMarkerStyleUpTriangle
            '.DataPoints(-1).Marker.Style = VtMarkerStyleSquare
            .DataPoints(-1).Marker.Style = VtMarkerStyle3dBall
            '.DataPoints(-1).Marker.Style = VtMarkerStyleStar
            .DataPoints(-1).Marker.Pen.Width = 25
            .DataPoints(-1).Marker.FillColor.Set 112, 146, 190
        End With
    Next
    
    With MSResultados.Plot.SeriesCollection(1)
        With .DataPoints(-1).DataPointLabel
            '.LocationType = VtChLabelLocationTypeAbovePoint
            .LocationType = VtChLabelLocationTypeAbovePoint
            .Custom = mostra
            .Component = IIf(mostra, VtChLabelComponent.VtChLabelComponentValue, VtChLabelComponent.VtChLabelComponentSeriesName)
            '.VtFont.VtColor.Set 0, 0, 255
        End With
    End With
    'If (mostra = False) Then: ActualizaGraficoResultados
    
End Sub

Private Sub BtRet_Click()

    Call GereFrameObservacao(FrObservacao.Visible)

End Sub

Private Sub Command1_Click()

    gMsgTitulo = " Observa��o"
    gMsgMsg = "Tem a certeza que quer eliminar a observa��o da requisi��o " & EcNumReq.text & "?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbNo) Then: Exit Sub
    EcObs.text = Empty
    Call ActualizaObservacaoReq(EcNumReq.text, EcObs.text)
    
End Sub

Private Sub EcAuxiliar_LostFocus()

    Dim rc As RECT
    EnableEcAuxiliar False, rc
    
End Sub

Private Sub EcNumReq_KeyPress(KeyAscii As Integer)

    Select Case (KeyAscii)
        Case KeyCodeConstants.vbKeyReturn: FuncaoProcurar
        Case Else: ' Empty
    End Select
    
End Sub

Private Sub EcNumReq_LostFocus()

    If (EcNumReq.text = Empty) Then: Exit Sub
    Call ValidaRequisicao(EcNumReq.text)

End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)

End Sub

Private Sub Form_Load()
    
    EventoLoad
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload
    
End Sub

Private Sub Form_Activate()

    EventoActivate

End Sub

' DEFINE PROPRIEDAS DE UMA LISTA.
Private Sub DefineLista(lista As ListasEcra)
   
    On Error GoTo ErrorHandler
    Select Case (lista)
        Case ListasEcra.e_LvTerapeutica:
            Dim descr_ana() As String
            descr_ana = Split(BL_DevolveDescricaoAnalise(gAnaliseDefeitoHipocoagulado, True), "_")
            With LvTerapeutica
                .ColumnHeaders.Add(, , "Medicamento", 2800, lvwColumnLeft).Key = "E_COD_TERA_MED"
                .ColumnHeaders.Add(, , descr_ana(1), 1000, lvwColumnLeft).Key = "E_RES_ANA"
                .ColumnHeaders.Add(, , "Periodo", 800, lvwColumnLeft).Key = "E_PERIODO"
                .ColumnHeaders.Add(, , "Seg", 600, lvwColumnLeft).Key = "E_DIA_1"
                .ColumnHeaders.Add(, , "Ter", 600, lvwColumnLeft).Key = "E_DIA_2"
                .ColumnHeaders.Add(, , "Qua", 600, lvwColumnLeft).Key = "E_DIA_3"
                .ColumnHeaders.Add(, , "Qui", 600, lvwColumnLeft).Key = "E_DIA_4"
                .ColumnHeaders.Add(, , "Sex", 600, lvwColumnLeft).Key = "E_DIA_5"
                .ColumnHeaders.Add(, , "Sab", 600, lvwColumnLeft).Key = "E_DIA_6"
                .ColumnHeaders.Add(, , "Dom", 600, lvwColumnLeft).Key = "E_DIA_7"
                .ColumnHeaders.Add(, , "Data", 1700, lvwColumnLeft).Key = "E_DT_CRI"
                .ColumnHeaders.Add(, , "Utilizador", 2800, lvwColumnLeft).Key = "E_USER_CRI"
                .View = lvwReport
                .FlatScrollBar = False
                .LabelEdit = lvwManual
                .AllowColumnReorder = False
                .SmallIcons = ImageListIcons
                .FullRowSelect = True
                .Checkboxes = False
                .ForeColor = ApplicationHexadecimalColors.e_Gray
                .MultiSelect = False
            End With
            BG_SetListViewColor LvTerapeutica, PictureListColor, ApplicationHexadecimalColors.e_LightGray, vbWhite
        Case ListasEcra.e_LvResultados:
            With LvResultados
                .ColumnHeaders.Add(, , "An�lises", 4000, lvwColumnLeft).Key = "ANALISES"
                .View = lvwReport
                .FlatScrollBar = False
                .LabelEdit = lvwManual
                .AllowColumnReorder = False
                .SmallIcons = ImageListIcons
                .FullRowSelect = True
                .Checkboxes = False
                .ForeColor = ApplicationHexadecimalColors.e_Gray
                .MultiSelect = False
            End With
            BG_SetListViewColor LvResultados, PictureListColor, ApplicationHexadecimalColors.e_LightGray, vbWhite
        Case ListasEcra.e_LvAgenda:
            With LvAgenda
                .ColumnHeaders.Add(, , "Requisi��o", 1000, lvwColumnLeft).Key = "E_N_REQ"
                .ColumnHeaders.Add(, , "Data Prevista", 1200, lvwColumnLeft).Key = "E_DT_PREVISTA"
                .ColumnHeaders.Add(, , "Tipo Utente", 1100, lvwColumnLeft).Key = "E_TIPO_UTENTE"
                .ColumnHeaders.Add(, , "N�mero Utente", 1400, lvwColumnLeft).Key = "E_NUM_UTENTE"
                .ColumnHeaders.Add(, , "Nome Utente", 2600, lvwColumnLeft).Key = "E_NOME_UTENTE"
                .ColumnHeaders.Add(, , "Utilizador", 2000, lvwColumnLeft).Key = "E_USER_CRI"
                .ColumnHeaders.Add(, , "Data", 1500, lvwColumnLeft).Key = "E_DT_CRI"
                .View = lvwReport
                .FlatScrollBar = False
                .LabelEdit = lvwManual
                .AllowColumnReorder = False
                .SmallIcons = ImageListIcons
                .FullRowSelect = True
                .Checkboxes = False
                .ForeColor = ApplicationHexadecimalColors.e_Gray
                .MultiSelect = False
            End With
            BG_SetListViewColor LvAgenda, PictureListColor, ApplicationHexadecimalColors.e_LightGray, vbWhite
        Case ListasEcra.e_LvPendentes:
            With LvPendentes
                .ColumnHeaders.Add(, , "Requisi��o", 1000, lvwColumnLeft).Key = "E_N_REQ"
                .ColumnHeaders.Add(, , "Estado", 2500, lvwColumnLeft).Key = "E_REQ_ESTADO"
                .ColumnHeaders.Add(, , "Data Prevista", 1200, lvwColumnLeft).Key = "E_DT_PREVISTA"
                .ColumnHeaders.Add(, , "Data Chegada", 1500, lvwColumnLeft).Key = "E_DT_CHEGADA"
                .ColumnHeaders.Add(, , "Utilizador", 2000, lvwColumnLeft).Key = "E_USER_CRI"
                .ColumnHeaders.Add(, , "Data", 1500, lvwColumnLeft).Key = "E_DT_CRI"
                .ColumnHeaders.Add(, , "Local", 2500, lvwColumnLeft).Key = "E_COD_LOCAL"
                .View = lvwReport
                .FlatScrollBar = False
                .LabelEdit = lvwManual
                .AllowColumnReorder = False
                .SmallIcons = ImageListIcons
                .FullRowSelect = True
                .Checkboxes = False
                .ForeColor = ApplicationHexadecimalColors.e_Gray
                .MultiSelect = False
            End With
            BG_SetListViewColor LvPendentes, PictureListColor, ApplicationHexadecimalColors.e_LightGray, vbWhite
        Case ListasEcra.e_LvDiagnosticos:
            With LvDiagnoticos
                .ColumnHeaderIcons = ImageListIcons
                .SmallIcons = ImageListIcons
                .ColumnHeaders.Add(, , , 400, lvwColumnLeft, ListViewIcons.e_IconDiag).Key = "ICON_DIAG"
                .ColumnHeaders.Add(, , "Diagn�stico", 2360, lvwColumnLeft).Key = "DIAG"
                .View = lvwReport
                .FlatScrollBar = False
                .LabelEdit = lvwManual
                .AllowColumnReorder = False
                .FullRowSelect = True
                .Checkboxes = False
                .ForeColor = ApplicationHexadecimalColors.e_Gray
                .MultiSelect = False
            End With
        Case Else: ' Empty
    End Select
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'DefineLista' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' CONTROLA O EVENTO CLICK DE UMA LISTA.
Private Sub HandleListViewClick(lista As ListasEcra, Optional Shift As Boolean)
  
    Dim lvhti As LVHITTESTINFO
    Dim lListItem As ListItem
    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    Call GetCursorPos(lvhti.pt)
    If (Shift = True) Then
        lvhti.pt.X = lvhitLvTerapeutica.pt.X
        lvhti.pt.Y = lvhitLvTerapeutica.pt.Y
    End If
    
    Select Case (lista)
        Case ListasEcra.e_LvTerapeutica:
            Call ScreenToClient(lhwndLvTerapeutica, lvhti.pt)
            If (ListView_SubItemHitTest(lhwndLvTerapeutica, lvhti) = LVI_NOITEM) Then: Exit Sub
            If (Shift) Then
                lvhti.iItem = lvhitLvTerapeutica.iItem
                lvhti.iSubItem = lvhitLvTerapeutica.iSubItem + 1
            End If
            'If (lvhti.iSubItem = Empty) Then: Exit Sub
            If (ListView_GetSubItemRect(lhwndLvTerapeutica, lvhti.iItem, lvhti.iSubItem, LVIR_LABEL, rc)) Then
                Call MapWindowPoints(lhwndLvTerapeutica, hWnd, rc, 2)
                
                If (Shift = False) Then
                    lidxLvTerapeutica = lvhti.iItem + 1
                    lsubidxLvTerapeutica = lvhti.iSubItem + 1
                Else
                    lsubidxLvTerapeutica = lsubidxLvTerapeutica + 1
                End If
                 
                lvhitLvTerapeutica.pt = lvhti.pt
                lvhitLvTerapeutica.iItem = lvhti.iItem
                lvhitLvTerapeutica.iSubItem = lvhti.iSubItem
                
                Select Case (lsubidxLvTerapeutica)
                
                    ' CASO COLUNA MEDICAMENTO
                    Case LvTerapeuticaColunas.e_cod_tera_med:
                        CbMedicamento.text = LvTerapeutica.ListItems(lidxLvTerapeutica).text
                        CbMedicamento.tag = LvTerapeutica.ColumnHeaders(lsubidxLvTerapeutica).Key
                        'LvTerapeutica.ListItems(lidxLvTerapeutica).text = Empty
                        Call SubClass(lhwndCbMedicamento, AddressOf WndProc)
                        EnableCbMedicamento True, rc
                    
                    ' CASO COLUNA PERIODO
                    Case LvTerapeuticaColunas.e_periodo:
                        CbPeriodo.text = LvTerapeutica.ListItems(lidxLvTerapeutica).SubItems(lsubidxLvTerapeutica - 1)
                        CbPeriodo.tag = LvTerapeutica.ColumnHeaders(lsubidxLvTerapeutica).Key
                        'LvTerapeutica.ListItems(lidxLvTerapeutica).SubItems(lsubidxLvTerapeutica) = Empty
                        Call SubClass(lhwndCbMedicamento, AddressOf WndProc)
                        EnableCbPeriodo True, rc
                    
                    ' CASO COLUNAS DIA*
                    Case LvTerapeuticaColunas.e_dia_1, LvTerapeuticaColunas.e_dia_2, LvTerapeuticaColunas.e_dia_3, LvTerapeuticaColunas.e_dia_4, LvTerapeuticaColunas.e_dia_5, LvTerapeuticaColunas.e_dia_6, LvTerapeuticaColunas.e_dia_7:
                        If (LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(lsubidxLvTerapeutica - 1).ReportIcon <> Empty) Then: Exit Sub
                        EcAuxiliar.text = LvTerapeutica.ListItems(lidxLvTerapeutica).SubItems(lsubidxLvTerapeutica - 1)
                        EcAuxiliar.tag = DevolveTagColunaId(LvTerapeutica.ColumnHeaders(lsubidxLvTerapeutica).Key)
                        Call SubClass(lhwndEcAuxiliar, AddressOf WndProc)
                        EnableEcAuxiliar True, rc
                        
                    Case Else: ' Empty
                End Select
            End If
    End Select
    Exit Sub
  
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HandleListViewDoubleClick' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

' CONTROLA O EVENTO DOUBLE-CLICK DE UMA LISTA.
Private Sub HandleListViewDoubleClick(lista As ListasEcra)
  
    Dim lvhti As LVHITTESTINFO
    Dim lListItem As ListItem
    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    Call GetCursorPos(lvhti.pt)
    Select Case (lista)
        Case ListasEcra.e_LvResultados:
            Call ScreenToClient(lhwndLvResultados, lvhti.pt)
            If (ListView_SubItemHitTest(lhwndLvResultados, lvhti) = LVI_NOITEM) Then: Exit Sub
            If (lvhti.iSubItem = Empty) Then: Exit Sub
            If (ListView_GetSubItemRect(lhwndLvResultados, lvhti.iItem, lvhti.iSubItem, LVIR_LABEL, rc)) Then
                Call MapWindowPoints(lhwndLvResultados, hWnd, rc, 2)
                lidxLvResultados = lvhti.iItem + 1
                lsubidxLvResultados = lvhti.iSubItem + 1
                Call ConsultaResultados(CStr(Split(LvResultados.ColumnHeaders(lsubidxLvResultados).text, Space(1))(0)))
            End If
        Case Else:
            ' Empty
    End Select
    Exit Sub
  
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HandleListViewDoubleClick' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

Private Function DevolveTagColunaId(tag As String) As String
    
    Select Case (UCase(tag))
        Case "E_COD_TERA_MED": DevolveTagColunaId = LvTerapeuticaColunas.e_cod_tera_med
        Case "E_PERIODO": DevolveTagColunaId = LvTerapeuticaColunas.e_periodo
        Case "E_DIA_1": DevolveTagColunaId = LvTerapeuticaColunas.e_dia_1
        Case "E_DIA_2": DevolveTagColunaId = LvTerapeuticaColunas.e_dia_2
        Case "E_DIA_3": DevolveTagColunaId = LvTerapeuticaColunas.e_dia_3
        Case "E_DIA_4": DevolveTagColunaId = LvTerapeuticaColunas.e_dia_4
        Case "E_DIA_5": DevolveTagColunaId = LvTerapeuticaColunas.e_dia_5
        Case "E_DIA_6": DevolveTagColunaId = LvTerapeuticaColunas.e_dia_6
        Case "E_DIA_7": DevolveTagColunaId = LvTerapeuticaColunas.e_dia_7
        Case "E_USER_CRI": DevolveTagColunaId = LvTerapeuticaColunas.e_user_cri
        Case "E_DT_CRI": DevolveTagColunaId = LvTerapeuticaColunas.e_dt_cri
        Case Else: ' Empty
    End Select
    
End Function

Private Sub LvAgenda_DblClick()

    If (LvAgenda.ListItems.Count = Empty) Then: Exit Sub
    Call ConsultaRequisicao(LvAgenda.SelectedItem.text)
    
End Sub

Private Sub LvDiagnoticos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    If (ColumnHeader.icon = ListViewIcons.e_IconDiag) Then: ConsultaInformacaoClinica
    
End Sub

Private Sub LvPendentes_DblClick()
    
    If (LvPendentes.ListItems.Count = Empty) Then: Exit Sub
    Select Case (LvPendentes.SelectedItem.SmallIcon)
        Case ListViewIcons.e_IconForbidden2:
            BG_Mensagem mediMsgBox, "N�o pode inserir terap�utica para esta requisi��o!", vbExclamation, " Aten��o"
        Case ListViewIcons.e_IconOk:
            Call GereFramePendentes(True)
            EcNumReq.text = LvPendentes.SelectedItem.text
            FuncaoProcurar
    End Select
    
End Sub

Private Sub LvPendentes_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If (LvPendentes.ListItems.Count = Empty) Then: Exit Sub
    Call ValidaRequisicao(LvPendentes.SelectedItem.text, True)
    
End Sub

Private Sub LvResultados_DblClick()
    
    On Error GoTo ErrorHandler
    If (LvResultados.ListItems.Count = Empty) Then: Exit Sub
    Call HandleListViewDoubleClick(ListasEcra.e_LvResultados)
    Exit Sub
  
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvResultados_DblClick' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

Private Sub LvResultados_ItemClick(ByVal Item As MSComctlLib.ListItem)
    
    On Error GoTo ErrorHandler
    If (LvResultados.ListItems.Count = Empty) Then: Exit Sub
    Call ActualizaGraficoResultados(CStr(Split(LvResultados.SelectedItem.Key, "KEY_")(1)))
    Exit Sub
  
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HandleListViewDoubleClick' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Private Sub LvTerapeutica_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
    
    Call BeforeHandleListViewTree(X, Y)
    If (False = CanDropItem(ListasEcra.e_LvTerapeutica)) Then
        LvTerapeutica.DragIcon = ImageListIcons.ListImages(ListViewIcons.e_IconCantDrop).Picture
        LvTerapeutica.ListItems(1).SmallIcon = ListViewIcons.e_IconRightBlue
    Else
        LvTerapeutica.DragIcon = ImageListIcons.ListImages(ListViewIcons.e_IconRightBlue).Picture
        LvTerapeutica.ListItems(1).SmallIcon = ListViewIcons.e_IconRightYellow
    End If
    
End Sub

Private Sub LvTerapeutica_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If (Empty = LvTerapeutica.ListItems.Count) Then: Exit Sub
    Set oSourceItem = LvTerapeutica.HitTest(X, Y)
    If (False = CanDropItem(ListasEcra.e_LvTerapeutica)) Then
        LvTerapeutica.DragIcon = ImageListIcons.ListImages(ListViewIcons.e_IconForbidden).Picture
        LvTerapeutica.ListItems(1).SmallIcon = ListViewIcons.e_IconRightBlue
    Else
        LvTerapeutica.DragIcon = ImageListIcons.ListImages(ListViewIcons.e_IconRightBlue).Picture
        LvTerapeutica.ListItems(1).SmallIcon = ListViewIcons.e_IconRightYellow
    End If
    Select Case (Button)
        Case MouseButtonConstants.vbRightButton: BeforeHandlePopMenu LvTerapeutica.SelectedItem.Index
        Case Else: ' Empty
    End Select
    
End Sub

' ACTIVA/INACTIVA CBMEDICAMENTO.
Private Sub EnableCbMedicamento(bEnable As Boolean, rc As RECT)

    On Error GoTo ErrorHandler
    If (Not bEnable) Then
        CbMedicamento.Visible = False
        CbMedicamento.text = Empty
        ilastidxLvTerapeutica = Empty
        LvTerapeutica.SetFocus
    Else
        'CbMedicamento.Move ((rc.left + 1) * Screen.TwipsPerPixelX), (rc.top * Screen.TwipsPerPixelY) + (2 * Screen.TwipsPerPixelY), ((rc.right - rc.left) * Screen.TwipsPerPixelX) - 10
        CbMedicamento.Move ((rc.left) * Screen.TwipsPerPixelX), (rc.top * Screen.TwipsPerPixelY) + (Screen.TwipsPerPixelY), ((rc.right - rc.left) * Screen.TwipsPerPixelX) - 10
        CbMedicamento.ForeColor = ApplicationHexadecimalColors.e_Gray
        If (LvTerapeutica.SelectedItem.Index Mod 2 <> 0) Then: CbMedicamento.BackColor = ApplicationHexadecimalColors.e_LightGray
        If (LvTerapeutica.SelectedItem.Index Mod 2 = 0) Then: CbMedicamento.BackColor = ApplicationHexadecimalColors.e_White
        'CbMedicamento.SelStart = 0
        'CbMedicamento.SelLength = Len(CbMedicamento)
        CbMedicamento.Visible = True
        CbMedicamento.SetFocus
    End If
    Exit Sub
        
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EnableCbMedicamento' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' ACTIVA/INACTIVA CBPERIODO.
Private Sub EnableCbPeriodo(bEnable As Boolean, rc As RECT)

    On Error GoTo ErrorHandler
    If (Not bEnable) Then
        CbPeriodo.Visible = False
        CbPeriodo.text = Empty
        ilastidxLvTerapeutica = Empty
        LvTerapeutica.SetFocus
    Else
        'CbMedicamento.Move ((rc.left + 1) * Screen.TwipsPerPixelX), (rc.top * Screen.TwipsPerPixelY) + (2 * Screen.TwipsPerPixelY), ((rc.right - rc.left) * Screen.TwipsPerPixelX) - 10
        CbPeriodo.Move ((rc.left) * Screen.TwipsPerPixelX), (rc.top * Screen.TwipsPerPixelY) + (Screen.TwipsPerPixelY), ((rc.right - rc.left) * Screen.TwipsPerPixelX) - 10
        CbPeriodo.ForeColor = ApplicationHexadecimalColors.e_Gray
        If (LvTerapeutica.SelectedItem.Index Mod 2 <> 0) Then: CbPeriodo.BackColor = ApplicationHexadecimalColors.e_LightGray
        If (LvTerapeutica.SelectedItem.Index Mod 2 = 0) Then: CbPeriodo.BackColor = ApplicationHexadecimalColors.e_White
        'CbPeriodo.SelStart = 0
        'CbPeriodo.SelLength = Len(CbMedicamento)
        CbPeriodo.Visible = True
        CbPeriodo.SetFocus
    End If
    Exit Sub
        
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EnableCbPeriodo' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' ACTIVA/INACTIVA ECAUXILIAR.
Private Sub EnableEcAuxiliar(bEnable As Boolean, rc As RECT)

    On Error GoTo ErrorHandler
    If (LvTerapeutica.SelectedItem = Empty) Then: Exit Sub
    If (Not bEnable) Then
        EcAuxiliar.Visible = False
        EcAuxiliar.text = Empty
        ilastidxLvTerapeutica = Empty
        LvTerapeutica.SetFocus
    Else
        EcAuxiliar.Alignment = AlignmentConstants.vbLeftJustify
        EcAuxiliar.Move ((rc.left + 1) * Screen.TwipsPerPixelX), (rc.top * Screen.TwipsPerPixelY) + (2 * Screen.TwipsPerPixelY), ((rc.right - rc.left) * Screen.TwipsPerPixelX) - 10, (rc.bottom - rc.top) * Screen.TwipsPerPixelY - 100
        EcAuxiliar.ForeColor = ApplicationHexadecimalColors.e_Gray
        If (LvTerapeutica.SelectedItem.Index Mod 2 <> 0) Then: EcAuxiliar.BackColor = ApplicationHexadecimalColors.e_LightGray
        If (LvTerapeutica.SelectedItem.Index Mod 2 = 0) Then: EcAuxiliar.BackColor = ApplicationHexadecimalColors.e_White
        EcAuxiliar.SelStart = 0
        EcAuxiliar.SelLength = Len(EcAuxiliar)
        EcAuxiliar.Visible = True
        EcAuxiliar.SetFocus
    End If
    Exit Sub
        
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EnableEcAuxiliar' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' INICIALIZACOES DO ECRA.
Private Sub Inicializacoes()
    
    On Error GoTo ErrorHandler
    Me.caption = " Gest�o de Hipocoagulados"
    Me.left = 15
    Me.top = 5
    Me.Width = 14145
    Me.Height = 9780
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'Inicializacoes' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_tera_med", "seq_tera_med", "descr_tera_med", CbMedicamento, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_tbf_periodo", "cod_periodo", "sigla_periodo", CbPeriodo, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    
End Sub

Private Sub DefTipoCampos()
    
    EcNumReq.tag = adVarChar
    Set CampoDeFocus = EcNumReq
    EcEstadoReq.locked = True
    EcUtente.locked = True
    EcNomeUtente.locked = True
    'EcLocal.locked = True
    EcDescrLocal.locked = True
    EcDtChega.locked = True
    Call DefineLista(ListasEcra.e_LvTerapeutica)
    Call DefineLista(ListasEcra.e_LvResultados)
    Call DefineLista(ListasEcra.e_LvAgenda)
    Call DefineLista(ListasEcra.e_LvPendentes)
    Call DefineLista(ListasEcra.e_LvDiagnosticos)
    lhwndLvTerapeutica = LvTerapeutica.hWnd
    lhwndLvResultados = LvResultados.hWnd
    lhwndCbMedicamento = CbMedicamento.hWnd
    lhwndEcAuxiliar = EcAuxiliar.hWnd
    lhwndCbPeriodo = CbPeriodo.hWnd
    
    Set CmdReq.ActiveConnection = gConexao
    CmdReq.CommandType = adCmdText
    CmdReq.CommandText = "select estado_req, seq_utente, cod_local, cod_proven, tipo_urgencia, obs_req, dt_chega, hr_chega, obs_req FROM sl_requis where n_req =? and flg_hipo=1"
    CmdReq.Parameters.Append CmdReq.CreateParameter("N_REQ", adInteger, adParamInput, 20)
    CmdReq.Prepared = True
    InicializaGraficoResultados
    BarMarcacoes.caption = Empty
    BarMarcacoes.Max = 0
    BarMarcacoes.Value = 0
    
    FrPendentes.Visible = False
    FrObservacao.Visible = False
    
    Call LimpaListaLocais
    
    DtInicial.Value = Bg_DaData_ADO
    DtFinal.Value = Bg_DaData_ADO
    
    BtObservacao.BackColor = ApplicationHexadecimalColors.e_White
    BtObservacao.ToolTipText = Empty
    BtObservacao.Enabled = False
    
End Sub

Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    Estado = 0
    BG_StackJanelas_Push Me
    gF_HIPO_RESULTADOS = mediSim
    
End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    If (CampoDeFocus.Enabled) Then: CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
        
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormHipoResultados = Nothing
    gF_HIPO_RESULTADOS = mediNao
    
End Sub
    
Private Sub LimpaCampos(Optional limpa_apenas_estrutura As Boolean)

    Dim rc As RECT
    Dim i As Integer
    If (False = limpa_apenas_estrutura) Then
        EcNumReq.text = Empty
        EcUtente.text = Empty
        EcNomeUtente.text = Empty: EcNomeUtente.ToolTipText = Empty
        'EcLocal.text = Empty
        EcDescrLocal.text = Empty
        EcDtChega.text = Empty
        EcEstadoReq.text = Empty: EcEstadoReq.ToolTipText = Empty
        EcObs.text = Empty: EcObs.ToolTipText = Empty
        LvDiagnoticos.ListItems.Clear
        BtObservacao.BackColor = ApplicationHexadecimalColors.e_White
        BtObservacao.ToolTipText = Empty
        BtObservacao.Enabled = False
    End If
    LvTerapeutica.ListItems.Clear
    LvResultados.ListItems.Clear
    LvAgenda.ListItems.Clear
    Call RemoveColunasLista(ListasEcra.e_LvResultados)
    TotalTerapeutica = Empty
    ReDim eTerapeutica(TotalTerapeutica)
    TotalAgenda = Empty
    ReDim eAgenda(TotalAgenda)
    EnableCbPeriodo False, rc
    EnableCbMedicamento False, rc
    InicializaGraficoResultados
    BarMarcacoes.caption = Empty
    BarMarcacoes.Max = 0
    BarMarcacoes.Value = 0
    MvAgenda.Value = Bg_DaData_ADO
    AnoAnterior = Empty
    MesAnterior = Empty
    DiaAnterior = Empty
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    LvPendentes.ListItems.Clear
    EcSeqUtente.text = Empty
    
End Sub

' LIMPA A LISTA DE LOCAIS.
Private Sub LimpaListaLocais()
    
    Dim i As Integer
    
    For i = 0 To EcLocais.ListCount - 1
        If (EcLocais.ItemData(i) = gCodLocal) Then
            EcLocais.Selected(i) = True
        Else
            EcLocais.Selected(i) = False
        End If
    Next
    
End Sub

' DEVOLVE INDEX DE UMA COLUNA NA LISTA (RESULTADOS).
Private Function DevolveIndexColuna(lista As ListasEcra, Key As String) As Integer

    Dim i As Integer
    
    On Error GoTo ErrorHandler
    Select Case (lista)
        Case ListasEcra.e_LvResultados:
            For i = 2 To LvResultados.ColumnHeaders.Count
                If (LvResultados.ColumnHeaders(i).Key = Key) Then: DevolveIndexColuna = LvResultados.ColumnHeaders(i).Index: Exit Function
            Next
    End Select
    DevolveIndexColuna = -1
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'DevolveIndexColuna' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    DevolveIndexColuna = -1
    Exit Function

End Function

' DEVOLVE INDEX DE UMA LINHA NA LISTA (RESULTADOS).
Private Function DevolveIndexLinha(lista As ListasEcra, Key As String) As Integer

    Dim i As Integer
    
    On Error GoTo ErrorHandler
    Select Case (lista)
        Case ListasEcra.e_LvResultados:
            For i = 1 To LvResultados.ListItems.Count
                If (LvResultados.ListItems.Item(i).Key = Key) Then: DevolveIndexLinha = LvResultados.ListItems.Item(i).Index: Exit Function
            Next
    End Select
    DevolveIndexLinha = -1
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'DevolveIndexLinha' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    DevolveIndexLinha = -1
    Exit Function

End Function

' REMOVE COLUNAS DE UMA LISTA.
Private Sub RemoveColunasLista(lista As ListasEcra)

    Dim i As Integer
    
    On Error GoTo ErrorHandler
    Select Case (lista)
        Case ListasEcra.e_LvResultados:
            For i = 2 To LvResultados.ColumnHeaders.Count
                LvResultados.ColumnHeaders.Remove i
            Next
    End Select
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'RemoveColunasLista' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

Private Sub PreencheCampos()
    'nada
End Sub

Private Sub CbMedicamento_KeyPress(Key As Integer)

    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    Select Case (Key)
        Case vbKeyEscape:
            Call EnableCbMedicamento(False, rc)
        Case Else
            ' Empty
    End Select
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'CbMedicamento_KeyPress' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

Private Sub CbPeriodo_KeyPress(Key As Integer)

    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    Select Case (Key)
        Case vbKeyEscape:
            Call EnableCbPeriodo(False, rc)
        Case Else
            ' Empty
    End Select
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'CbPeriodo_KeyPress' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

Private Sub EcAuxiliar_KeyPress(Key As Integer)

    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    Select Case (Key)
        Case vbKeyEscape:
            Call HideTextBox(False)
            Call EnableEcAuxiliar(False, rc)
        Case vbKeyReturn:
            Call HideTextBox(True, EcAuxiliar.tag)
            DoEvents
            Call EnableEcAuxiliar(False, rc)
            DoEvents
            lvhitLvTerapeutica.pt.X = lvhitLvTerapeutica.pt.X + (60)
            HandleListViewClick e_LvTerapeutica, True
            DoEvents
    End Select
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'EcAuxiliar_KeyPress' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' FAZ A GESTAO DA CAIXA DE TEXTO AUXILIAR.
Private Sub HideTextBox(bApplyChanges As Boolean, Optional tag As String)

    On Error GoTo ErrorHandler
    If (bApplyChanges) Then
        LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(lsubidxLvTerapeutica - 1).text = EcAuxiliar.text
        LvTerapeutica.ListItems(lidxLvTerapeutica).ListSubItems(lsubidxLvTerapeutica - 1).ToolTipText = EcAuxiliar.text
        Select Case (tag)
             Case LvTerapeuticaColunas.e_dia_1: eTerapeutica(1).t_dia_1 = EcAuxiliar.text
             Case LvTerapeuticaColunas.e_dia_2: eTerapeutica(1).t_dia_2 = EcAuxiliar.text
             Case LvTerapeuticaColunas.e_dia_3: eTerapeutica(1).t_dia_3 = EcAuxiliar.text
             Case LvTerapeuticaColunas.e_dia_4: eTerapeutica(1).t_dia_4 = EcAuxiliar.text
             Case LvTerapeuticaColunas.e_dia_5: eTerapeutica(1).t_dia_5 = EcAuxiliar.text
             Case LvTerapeuticaColunas.e_dia_6: eTerapeutica(1).t_dia_6 = EcAuxiliar.text
             Case LvTerapeuticaColunas.e_dia_7: eTerapeutica(1).t_dia_7 = EcAuxiliar.text
        End Select
    End If
    Call UnSubClass(lhwndEcAuxiliar) ' : lListItemIndex = Empty
    Exit Sub
     
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HideTextBox' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' PROCURA REGISTOS RELACIONADOS COM A REQUISICAO HIPOCOAGULADOS.
Private Sub ProcuraRegistos(requisicao As String)

    Call ProcuraRegistosTerapeutica(requisicao)
    Call ProcuraResultadosAnteriores(requisicao)
    Call ProcuraRegistosAgenda(Month(Now))
    
End Sub

' ACTUALIZA OPS CONTROLOS ASSOCIADOS A REQUISICAO HIPOCOAGULADOS.
Private Sub ActualizaEcra()
    
    On Error GoTo ErrorHandler
    Call ActualizaHistTerapeutica
    Call ActualizaHistResultados
    Call ActualizaGraficoResultados
    Call ActualizaListaAgenda(Format(Now, "dd-mm-yyyy"))
    Call ActualizaPercentagemMarcacoes(Format(Now, "dd-mm-yyyy"))
    Call ActualizaCalendarioBold
    Exit Sub
        
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaEcra' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub


Public Sub FuncaoInserir()

    Call InsereTerapeutica
    
End Sub

Public Sub FuncaoProcurar()
    
    On Error GoTo ErrorHandler
    Estado = 2
    BL_InicioProcessamento Me, " A procurar registos..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    LimpaCampos True
    
    Select Case (FrPendentes.Visible)
        Case True:
            ProcuraRequisicoesPendentes
        Case False:
            If (Not ValidaRequisicao(EcNumReq.text)) Then: GoTo ErrorHandler
            ProcuraRegistos EcNumReq
            ActualizaEcra
    End Select
    
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    'BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    Exit Sub
        
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoProcurar' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
End Sub

Public Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        If (CampoDeFocus.Enabled) Then: CampoDeFocus.SetFocus
    End If

End Sub

' PROCURA REGISTOS AGENDA (POR MES).
Private Sub ProcuraRegistosAgenda(mes As MonthConstants)

    Dim sql As String
    Dim rsAgenda As ADODB.recordset
    Dim Index As Long
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A procurar agenda para hoje..."
    Set rsAgenda = New ADODB.recordset
    rsAgenda.CursorLocation = adUseClient
    rsAgenda.CursorType = adOpenStatic
    sql = ConstroiSqlAgenda(mes)
    rsAgenda.Open sql, gConexao
    TotalAgenda = Empty
    ReDim eAgenda(Empty)
    Index = Index + 1
    While (Not rsAgenda.EOF)
        InsereItemAgenda Index, BL_HandleNull(rsAgenda!n_req, Empty), BL_HandleNull(rsAgenda!estado_req, Empty), _
                         BL_HandleNull(rsAgenda!dt_previ, Empty), BL_HandleNull(rsAgenda!seq_utente, Empty), _
                         BL_HandleNull(rsAgenda!t_utente, Empty), BL_HandleNull(rsAgenda!utente, Empty), _
                         BL_HandleNull(rsAgenda!nome_ute, Empty), BL_HandleNull(rsAgenda!user_cri, Empty), _
                         BL_HandleNull(rsAgenda!dt_cri, Empty), BL_HandleNull(rsAgenda!hr_cri, Empty)
        Index = Index + 1
        rsAgenda.MoveNext
    Wend
    If (rsAgenda.State = adStateOpen) Then: rsAgenda.Close
    DoEvents
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ProcuraRegistosAgenda' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' PROCURA REQUISICOES PENDENTES.
Private Sub ProcuraRequisicoesPendentes()

    Dim sql As String
    Dim Estado As EstadoRequisicao
    Dim rsReq As ADODB.recordset
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A procurar requisi��es pendentes..."
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseClient
    rsReq.CursorType = adOpenStatic
    sql = ConstroiSqlPendentes
    rsReq.Open sql, gConexao
    While (Not rsReq.EOF)
        If (BL_HandleNull(rsReq!estado_req, Empty) = gEstadoReqValicacaoMedicaCompleta Or _
            BL_HandleNull(rsReq!estado_req, Empty) = gEstadoReqTodasImpressas) Then
            Estado = EstadoRequisicao.e_EstadoReqAceite
        Else
            Estado = EstadoRequisicao.e_EstadoReqNaoAceite
        End If
        Call AdicionaItemPendentes(rsReq, Estado)
        rsReq.MoveNext
    Wend
    If (rsReq.RecordCount = Empty) Then: BG_Mensagem mediMsgBox, "N�o encontrou requisi��es pendentes!", vbInformation, " Procura"
    If (rsReq.State = adStateOpen) Then: rsReq.Close
    DoEvents
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ProcuraRequisicoesPendentes' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' ADICIONA ITEM NA LISTA DE PENDENTES.
Private Function AdicionaItemPendentes(rs As ADODB.recordset, Estado As EstadoRequisicao) As Boolean

    Dim cor As ApplicationHexadecimalColors
    Dim icon As ListViewIcons
    
    On Error GoTo ErrorHandler
    Select Case (Estado)
        Case EstadoRequisicao.e_EstadoReqAceite: cor = e_Green: icon = ListViewIcons.e_IconOk
        Case EstadoRequisicao.e_EstadoReqNaoAceite: cor = e_LightRed: icon = ListViewIcons.e_IconForbidden2
        Case Else: ' Empty
    End Select
    With LvPendentes.ListItems.Add(, , BL_HandleNull(rs!n_req, Empty))
        .ListSubItems.Add(, , BL_DevolveEstadoReq(BL_HandleNull(rs!estado_req, Empty))).ForeColor = cor
        .ListSubItems.Add(, , BL_HandleNull(rs!dt_previ, Empty)).ForeColor = cor
        .ListSubItems.Add(, , BL_HandleNull(rs!dt_chega, Empty) & Space(1) & BL_HandleNull(rs!hr_chega, Empty)).ForeColor = cor
        .ListSubItems.Add(, , BL_DevolveNomeUtilizador(BL_HandleNull(rs!user_cri, Empty), "")).ForeColor = cor
        .ListSubItems.Add(, , BL_HandleNull(rs!dt_cri, Empty) & Space(1) & BL_HandleNull(rs!hr_cri, Empty)).ForeColor = cor
        .ListSubItems.Add(, , BL_DevolveDescrLocal(BL_HandleNull(rs!cod_local, Empty))).ForeColor = cor
        .ForeColor = cor
        .SmallIcon = icon
    End With
    AdicionaItemPendentes = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AdicionaItemPendentes' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    AdicionaItemPendentes = False
    Exit Function
    Resume Next

End Function

' PROCURA REGISTOS TERAPEUTICA (HISTORICO).
Private Sub ProcuraRegistosTerapeutica(requisicao As String)

    Dim sql As String
    Dim rsTerap As ADODB.recordset
    Dim Index As Long
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A procurar hist�rico de terap�utica..."
    Set rsTerap = New ADODB.recordset
    rsTerap.CursorLocation = adUseClient
    rsTerap.CursorType = adOpenStatic
    sql = ConstroiSqlTerapeutica(requisicao)
    rsTerap.Open sql, gConexao
    If (TotalTerapeutica <> Empty) Then: TotalTerapeutica = Empty: ReDim eTerapeutica(Empty)
    InsereItemTerapeutica Index, requisicao
    Index = Index + 1
    While (Not rsTerap.EOF)
        InsereItemTerapeutica Index, BL_HandleNull(rsTerap!n_req, Empty), BL_HandleNull(rsTerap!cod_tera_med, Empty), _
                              BL_HandleNull(rsTerap!periodo, Empty), BL_HandleNull(rsTerap!dia_1, Empty), _
                              BL_HandleNull(rsTerap!dia_2, Empty), BL_HandleNull(rsTerap!dia_3, Empty), _
                              BL_HandleNull(rsTerap!dia_4, Empty), BL_HandleNull(rsTerap!dia_5, Empty), _
                              BL_HandleNull(rsTerap!dia_6, Empty), BL_HandleNull(rsTerap!dia_7, Empty), _
                              BL_HandleNull(rsTerap!user_cri, Empty), BL_HandleNull(rsTerap!dt_cri, Empty), _
                              BL_HandleNull(rsTerap!result, Empty)
         Index = Index + 1
         rsTerap.MoveNext
    Wend
    If (rsTerap.RecordCount = Empty) Then: BG_Mensagem mediMsgBox, "N�o encontrou hist�rico de terap�utica!", vbInformation, " Procura"
    If (rsTerap.State = adStateOpen) Then: rsTerap.Close
    DoEvents
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ProcuraRegistosTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' PROCURA RESULTADOS ANTERIORES.
Private Sub ProcuraResultadosAnteriores(requisicao As String)

    Dim sql As String
    Dim RsRes As ADODB.recordset
    Dim Index As Long
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A procurar hist�rico de resultados..."
    Set RsRes = New ADODB.recordset
    RsRes.CursorLocation = adUseClient
    RsRes.CursorType = adOpenStatic
    sql = ConstroiSqlResultados(requisicao)
    RsRes.Open sql, gConexao
    TotalResultados = Empty
    ReDim eResultados(Empty)
    While (Not RsRes.EOF)
        InsereItemResultado Index, BL_HandleNull(RsRes!n_req, Empty), BL_HandleNull(RsRes!seq_realiza, Empty), _
                            BL_HandleNull(RsRes!dt_cri, Empty), BL_HandleNull(RsRes!hr_cri, Empty), _
                            BL_HandleNull(RsRes!user_cri, Empty), BL_HandleNull(RsRes!seq_ana_s, Empty), _
                            BL_HandleNull(RsRes!descr_ana_s, Empty), BL_HandleNull(RsRes!result, Empty)
         Index = Index + 1
         RsRes.MoveNext
    Wend
    If (RsRes.RecordCount = Empty) Then: BG_Mensagem mediMsgBox, "N�o encontrou hist�rico de resultados!", vbInformation, " Procura"
    If (RsRes.State = adStateOpen) Then: RsRes.Close
    DoEvents
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ProcuraResultadosAnteriores' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub
Private Sub LimpaSubItems(lItem As Long)
    
    Dim vSubItem As Variant
    
    On Error GoTo ErrorHandler
    For Each vSubItem In LvTerapeutica.ListItems(lItem).ListSubItems
        If (vSubItem.text = "0") Then: vSubItem.text = Empty
    Next
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LimpaSubItems' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' INSERE ITEM NA ESTRUTURA DE TERAPEUTICA.
Private Sub InsereItemTerapeutica(Index As Long, n_req As String, Optional cod_tera_med As String, _
                                  Optional periodo As String, Optional dia_1 As String, _
                                  Optional dia_2 As String, Optional dia_3 As String, _
                                  Optional dia_4 As String, Optional dia_5 As String, _
                                  Optional dia_6 As String, Optional dia_7 As String, _
                                  Optional user_cri As String, Optional dt_cri As String, _
                                  Optional res_ana As String)

    On Error GoTo ErrorHandler
    TotalTerapeutica = TotalTerapeutica + 1
    ReDim Preserve eTerapeutica(TotalTerapeutica)
    eTerapeutica(TotalTerapeutica).t_index = Index
    eTerapeutica(TotalTerapeutica).t_n_req = n_req
    eTerapeutica(TotalTerapeutica).t_cod_tera_med = cod_tera_med
    eTerapeutica(TotalTerapeutica).t_periodo = periodo
    eTerapeutica(TotalTerapeutica).t_dia_1 = dia_1
    eTerapeutica(TotalTerapeutica).t_dia_2 = dia_2
    eTerapeutica(TotalTerapeutica).t_dia_3 = dia_3
    eTerapeutica(TotalTerapeutica).t_dia_4 = dia_4
    eTerapeutica(TotalTerapeutica).t_dia_5 = dia_5
    eTerapeutica(TotalTerapeutica).t_dia_6 = dia_6
    eTerapeutica(TotalTerapeutica).t_dia_7 = dia_7
    eTerapeutica(TotalTerapeutica).t_user_cri = user_cri
    eTerapeutica(TotalTerapeutica).t_dt_cri = dt_cri
    eTerapeutica(TotalTerapeutica).t_res_ana = res_ana
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InsereItemTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' INSERE ITEM NA ESTRUTURA DE RESULTADOS.
Private Sub InsereItemResultado(Index As Long, n_req As String, Optional seq_realiza As String, _
                                Optional dt_cri As String, Optional hr_cri As String, _
                                Optional user_cri As String, Optional seq_ana As String, _
                                Optional descr_ana As String, Optional resultado As String)

    On Error GoTo ErrorHandler
    TotalResultados = TotalResultados + 1
    ReDim Preserve eResultados(TotalResultados)
    eResultados(TotalResultados).t_index = Index
    eResultados(TotalResultados).t_n_req = n_req
    eResultados(TotalResultados).t_seq_realiza = seq_realiza
    eResultados(TotalResultados).t_dt_cri = dt_cri
    eResultados(TotalResultados).t_hr_cri = hr_cri
    eResultados(TotalResultados).t_user_cri = user_cri
    eResultados(TotalResultados).t_seq_ana = seq_ana
    eResultados(TotalResultados).t_descr_ana = descr_ana
    eResultados(TotalResultados).t_resultado = resultado
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InsereItemResultado' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' INSERE ITEM NA ESTRUTURA DE AGENDA.
Private Sub InsereItemAgenda(Index As Long, Optional n_req As String, Optional estado_req As String, Optional dt_prevista As String, _
                             Optional seq_utente As String, Optional tipo_utente As String, Optional num_utente As String, _
                             Optional nome_utente As String, Optional user_cri As String, Optional dt_cri As String, _
                             Optional hr_cri As String)

    On Error GoTo ErrorHandler
    TotalAgenda = TotalAgenda + 1
    ReDim Preserve eAgenda(TotalAgenda)
    eAgenda(TotalAgenda).t_index = Index
    eAgenda(TotalAgenda).t_n_req = n_req
    eAgenda(TotalAgenda).t_estado_req = estado_req
    eAgenda(TotalAgenda).t_dt_prevista = dt_prevista
    eAgenda(TotalAgenda).t_seq_utente = seq_utente
    eAgenda(TotalAgenda).t_tipo_utente = tipo_utente
    eAgenda(TotalAgenda).t_num_utente = num_utente
    eAgenda(TotalAgenda).t_nome_utente = nome_utente
    eAgenda(TotalAgenda).t_user_cri = user_cri
    eAgenda(TotalAgenda).t_dt_cri = dt_cri
    eAgenda(TotalAgenda).t_hr_cri = hr_cri
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InsereItemAgenda' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Public Sub FuncaoEstadoAnterior()
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        If (CampoDeFocus.Enabled) Then: CampoDeFocus.SetFocus
        
    Else
        Unload Me
    End If
End Sub

Private Function ContaRequisicoes() As Integer
    
    Dim reqs As New CollectionKEY
    Dim Item As Long
    
    On Error GoTo ErrorHandler
    For Item = 1 To TotalResultados
        If (reqs.Count = Empty) Then: Call reqs.Add(eResultados(Item).t_n_req, eResultados(Item).t_n_req)
        If (reqs.ItemKEY(eResultados(Item).t_n_req) = Empty) Then: Call reqs.Add(eResultados(Item).t_n_req, eResultados(Item).t_n_req)
    Next
    ContaRequisicoes = reqs.Count
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ContaRequisicoes' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next

End Function

' ACTUALIZA LISTA AGENDA (LISTA).
Private Sub ActualizaListaAgenda(data As String)
    
    Dim Item As Long
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A actualizar agenda para hoje..."
    LvAgenda.ListItems.Clear
    For Item = 1 To TotalAgenda
         If (eAgenda(Item).t_dt_prevista = data) Then: Call AdicionaItemAgenda(Item)
    Next
    DoEvents
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaListaAgenda' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

' ACTUALIZA HISTORICO RESULTADOS (LISTA).
Private Sub ActualizaHistResultados()
    
    Dim Item As Long
    Dim numero_requisicoes As Integer
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A actualizar hist�rico de resultados..."
    numero_requisicoes = ContaRequisicoes
    For Item = 1 To TotalResultados
        Call AdicionaItemResultado(Item, numero_requisicoes)
    Next
    DoEvents
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaHistResultados' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

' ACTUALIZA HISTORICO TERAPEUTICA (LISTA).
Private Sub ActualizaHistTerapeutica()
    
    Dim Item As Long
    Dim existe_terapeutica As Boolean
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A actualizar hist�rico de terap�utica..."
    For Item = 1 To TotalTerapeutica
        Call AdicionaItemTerapeutica(Item)
        If (eTerapeutica(Item).t_n_req = EcNumReq) Then: existe_terapeutica = True
    Next
    Call InsereLinhaTerapeutica(1, existe_terapeutica)
    DoEvents
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaHistTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

' INSERE NOVA LINHA PARA REGISTAR A TERAPEUTICA.
Private Sub InsereLinhaTerapeutica(Index As Long, existe_terapeutica As Boolean)

    If (True = existe_terapeutica) Then
        With LvTerapeutica.ListItems(1)
            .Bold = True
            .SmallIcon = ListViewIcons.e_IconRightBlue
        End With
        Exit Sub
    End If
    
    With LvTerapeutica.ListItems.Add(1, "KEY_" & LvTerapeuticaColunas.e_cod_tera_med & "_" & 1, Empty, , ListViewIcons.e_IconRightBlue)
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .ListSubItems.Add , , Empty
        .Bold = True
    End With
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InsereLinhaTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' ADICIONA ITEM NA LISTA DE AGENDA.
Private Function AdicionaItemAgenda(Item As Long) As Boolean
    
    On Error GoTo ErrorHandler
    With LvAgenda.ListItems.Add(, "KEY_" & LvAgendaColunas.e_n_req & "_" & eAgenda(Item).t_index & "_" & eAgenda(Item).t_n_req, eAgenda(Item).t_n_req)
        .ListSubItems.Add , , eAgenda(Item).t_dt_prevista
        .ListSubItems.Add , , eAgenda(Item).t_tipo_utente
        .ListSubItems.Add , , eAgenda(Item).t_num_utente
        .ListSubItems.Add , , eAgenda(Item).t_nome_utente
        .ListSubItems.Add , , BL_DevolveNomeUtilizador(eAgenda(Item).t_user_cri, "")
        .ListSubItems.Add , , eAgenda(Item).t_dt_cri & " " & eAgenda(Item).t_hr_cri
    End With
    Call LimpaSubItems(eAgenda(Item).t_index)
    AdicionaItemAgenda = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AdicionaItemAgenda' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    AdicionaItemAgenda = False
    Exit Function
    Resume Next

End Function

' ADICIONA ITEM NA LISTA DE TERAPEUTICA.
Private Function AdicionaItemTerapeutica(Item As Long) As Boolean

    Dim Index As Long
    
    On Error GoTo ErrorHandler
    Index = eTerapeutica(Item).t_index + 1
    With LvTerapeutica.ListItems.Add(, "KEY_" & LvTerapeuticaColunas.e_cod_tera_med & "_" & Index & "_" & eTerapeutica(Item).t_n_req, BL_SelCodigo("sl_tera_med", "descr_tera_med", "seq_tera_med", eTerapeutica(Item).t_cod_tera_med))
        .ListSubItems.Add(, , eTerapeutica(Item).t_res_ana).ToolTipText = gAnaliseDefeitoHipocoagulado
        .ListSubItems.Add , , eTerapeutica(Item).t_periodo
        .ListSubItems.Add , , eTerapeutica(Item).t_dia_1
        .ListSubItems.Add , , eTerapeutica(Item).t_dia_2
        .ListSubItems.Add , , eTerapeutica(Item).t_dia_3
        .ListSubItems.Add , , eTerapeutica(Item).t_dia_4
        .ListSubItems.Add , , eTerapeutica(Item).t_dia_5
        .ListSubItems.Add , , eTerapeutica(Item).t_dia_6
        .ListSubItems.Add , , eTerapeutica(Item).t_dia_7
        .ListSubItems.Add , , eTerapeutica(Item).t_dt_cri
        .ListSubItems.Add , , IIf(eTerapeutica(Item).t_user_cri <> Empty, BL_DevolveNomeUtilizador(eTerapeutica(Item).t_user_cri, ""), Empty)
    End With
    Call LimpaSubItems(eTerapeutica(Item).t_index)
    AdicionaItemTerapeutica = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AdicionaItemTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    AdicionaItemTerapeutica = False
    Exit Function
    Resume Next

End Function

' ADICIONA ITEM NA LISTA DE RESULTADOS.
Private Function AdicionaItemResultado(Item As Long, numero_requisicoes As Integer) As Boolean

    Dim i As Integer
    Dim coluna_index As Integer
    Dim linha_index As Integer
    
    On Error GoTo ErrorHandler

    coluna_index = DevolveIndexColuna(ListasEcra.e_LvResultados, "KEY_" & eResultados(Item).t_n_req)
    linha_index = DevolveIndexLinha(ListasEcra.e_LvResultados, "KEY_" & eResultados(Item).t_seq_ana)
    
    If (coluna_index = -1) Then
        Call LvResultados.ColumnHeaders.Add(, "KEY_" & eResultados(Item).t_n_req, eResultados(Item).t_n_req & " " & eResultados(Item).t_dt_cri, 1500, lvwColumnCenter)
        coluna_index = DevolveIndexColuna(ListasEcra.e_LvResultados, "KEY_" & eResultados(Item).t_n_req)
    End If
    
    If (linha_index = -1) Then
        With LvResultados.ListItems.Add(, "KEY_" & eResultados(Item).t_seq_ana, eResultados(Item).t_descr_ana)
            For i = 1 To numero_requisicoes
                .ListSubItems.Add , , , , eResultados(Item).t_n_req
            Next
            coluna_index = DevolveIndexColuna(ListasEcra.e_LvResultados, "KEY_" & eResultados(Item).t_n_req)
            LvResultados.ListItems.Item(LvResultados.ListItems.Count).ListSubItems.Item(coluna_index - 1).text = eResultados(Item).t_resultado
            LvResultados.ListItems.Item(LvResultados.ListItems.Count).ListSubItems.Item(coluna_index - 1).ToolTipText = eResultados(Item).t_n_req
        End With
    Else
        LvResultados.ListItems.Item(linha_index).ListSubItems.Item(coluna_index - 1).text = eResultados(Item).t_resultado
        LvResultados.ListItems.Item(linha_index).ListSubItems.Item(coluna_index - 1).ToolTipText = eResultados(Item).t_n_req
    End If
    
    Call LimpaSubItems(eResultados(Item).t_index)
    AdicionaItemResultado = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AdicionaItemResultado' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    AdicionaItemResultado = False
    Exit Function
    Resume Next

End Function

' VALIDA A REQUISICAO DE PESQUISA.
Private Function ValidaRequisicao(numero_requisicao As String, Optional pendentes As Boolean) As Boolean
    
    Dim rsReq As ADODB.recordset
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A validar requisi��o..."
    LvDiagnoticos.ListItems.Clear
    EcEstadoReq.text = Empty: EcEstadoReq.ToolTipText = Empty
    EcObs.text = Empty: EcObs.ToolTipText = Empty
    EcNomeUtente.ToolTipText = Empty
    BtObservacao.BackColor = ApplicationHexadecimalColors.e_White
    BtObservacao.ToolTipText = Empty
    BtObservacao.Enabled = False
    Set rsReq = New ADODB.recordset
    If (numero_requisicao = Empty) Then: BG_Mensagem mediMsgBox, "� obrigat�rio introduzir o n�mero de requisi��o!", vbExclamation, " Aten��o": Exit Function
    If (pendentes = True) Then: EcNumReq.text = numero_requisicao
    If (Not IsNumeric(EcNumReq.text)) Then: Exit Function
    gRequisicaoActiva = right(numero_requisicao, 7)
    CmdReq.Parameters(0).Value = gRequisicaoActiva
    Set rsReq = CmdReq.Execute
    If (Not rsReq.EOF) Then
        If (Not pendentes And BL_HandleNull(rsReq!estado_req, Empty) <> gEstadoReqValicacaoMedicaCompleta And BL_HandleNull(rsReq!estado_req, Empty) <> gEstadoReqTodasImpressas) Then
            BG_Mensagem mediMsgBox, "O estado da requisi��o � inv�lido: " & BL_DevolveEstadoReq(BL_HandleNull(rsReq!estado_req, Empty)), vbExclamation, " Validar"
            If (EcNumReq.Enabled) Then: EcNumReq.SetFocus
            SendKeys ("{HOME}+{END}")
            ValidaRequisicao = False
            BL_FimProcessamento Me
            Exit Function
        End If
        EcSeqUtente.text = BL_HandleNull(rsReq!seq_utente, Empty)
        Call PreencheDadosUtente(BL_HandleNull(rsReq!seq_utente, Empty))
        Call PreencheLocal(BL_HandleNull(rsReq!cod_local, Empty))
        EcEstadoReq.text = BL_DevolveEstadoReq(BL_HandleNull(rsReq!estado_req, Empty)): EcEstadoReq.ToolTipText = EcEstadoReq.text
        EcObs.text = BL_HandleNull(rsReq!obs_req, Empty): EcObs.ToolTipText = EcObs.text
        If (EcObs.text <> Empty) Then
            BtObservacao.BackColor = ApplicationHexadecimalColors.e_LightRed
            BtObservacao.ToolTipText = "Requisi��o com observa��o"
            BtObservacao.Enabled = True
        Else
            BtObservacao.BackColor = ApplicationHexadecimalColors.e_White
            BtObservacao.ToolTipText = "Requisi��o sem observa��o"
            BtObservacao.Enabled = True
        End If
        ValidaRequisicao = True
        EcDtChega.text = BL_HandleNull(rsReq!dt_chega, Empty) & " " & BL_HandleNull(rsReq!hr_chega, Empty)
    Else
        BG_Mensagem mediMsgBox, "Erro ao validar requisi��o", vbExclamation, " Validar"
        If (EcNumReq.Enabled) Then: EcNumReq.SetFocus
        SendKeys ("{HOME}+{END}")
        ValidaRequisicao = False
    End If
    BL_FimProcessamento Me
    Exit Function
    
ErrorHandler:
    ValidaRequisicao = False
    BG_LogFile_Erros "Error in function 'ValidaRequisicao' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BL_FimProcessamento Me
    Exit Function
    Resume Next
    
End Function

' PREENCHE LOCAL.
Private Sub PreencheLocal(codigo_local As String)

    If (codigo_local = Empty) Then: Exit Sub
    'EcLocal.text = codigo_local
    EcDescrLocal.text = BL_DevolveDescrLocal(codigo_local)

End Sub

' PREENCHE OS DADOS DO UTENTE.
Private Sub PreencheDadosUtente(seq_utente As String)
    
    Dim Campos(7) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "n_proc_1"
    Campos(4) = "n_proc_2"
    Campos(5) = "n_cartao_ute"
    Campos(6) = "dt_nasc_ute"

    iret = BL_DaDadosUtente(Campos, retorno, seq_utente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        EcUtente.text = ""
        EcNomeUtente.text = ""
        Exit Sub
    End If
    
    EcUtente.text = retorno(1) & Space(1) & retorno(0)
    EcNomeUtente.text = retorno(2): EcNomeUtente.ToolTipText = EcNomeUtente.text
    Call PreencheDiagnostico(seq_utente)
   ' EcDataNasc.text = Retorno(6)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheDadosUtente' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' FAZ GESTAO DO MENU PARA A LISTA DE TERAPEUTICA.
Private Sub BeforeHandlePopMenu(Index As Integer)

    If (1 <> Index) Then: Exit Sub
    MDIFormInicio.PopupMenu MDIFormInicio.HIDE_HIPO
    
End Sub

' INSERE NOVA TERAPEUTICA NA BASE DE DADOS.
Public Sub InsereTerapeutica(Optional anterior As String)
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    
    gMsgTitulo = " Terap�utica"
    gMsgMsg = "Tem a certeza que quer inserir a terap�utica " & anterior & "?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbNo) Then: Exit Sub
    
    BL_InicioProcessamento Me, " A inserir registos..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    
    sql = "delete from sl_tm_ute where flg_hipo = 1 and n_Req = " & eTerapeutica(1).t_n_req
    
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    If (Empty <> gSQLError) Then: BG_Mensagem mediMsgBox, "Erro ao inserir nova terap�utica", vbExclamation, " Gravar": Exit Sub
    
    sql = "insert into sl_tm_ute (n_req,cod_tera_med,user_cri,dt_cri,periodo,dia_1,dia_2,dia_3,dia_4,dia_5,dia_6,dia_7,flg_hipo) values (" & _
          eTerapeutica(1).t_n_req & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_cod_tera_med) & "," & _
          BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & _
          "sysdate" & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_periodo) & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_dia_1) & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_dia_2) & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_dia_3) & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_dia_4) & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_dia_5) & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_dia_6) & "," & _
          BL_TrataStringParaBD(eTerapeutica(1).t_dia_7) & "," & _
          "1)"
      
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    If (Empty <> gSQLError) Then: BG_Mensagem mediMsgBox, "Erro ao inserir nova terap�utica", vbExclamation, " Gravar": Exit Sub
    BG_Mensagem mediMsgBox, "Nova terap�utica inserida", vbInformation, " Gravar"
    LvTerapeutica.ListItems.Clear
    TotalTerapeutica = Empty
    ReDim eTerapeutica(Empty)
    Call ProcuraRegistosTerapeutica(EcNumReq.text)
    Call ActualizaHistTerapeutica
    
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InsereTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' ACTUALIZA TERAPEUTICA NA BASE DE DADOS (NAO ESTA A SER UTILIZADA).
Private Sub ActualizaTerapeutica()
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "update sl_tm_ute set cod_tera_med = " & BL_TrataStringParaBD(eTerapeutica(1).t_cod_tera_med) & ", " & _
          "user_cri = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", " & _
          "dt_cri = sysdate" & ", " & _
          "periodo = " & eTerapeutica(1).t_periodo & ", " & _
          "dia_1 = " & BL_TrataStringParaBD(eTerapeutica(1).t_dia_1) & ", " & _
          "dia_2 = " & BL_TrataStringParaBD(eTerapeutica(1).t_dia_2) & ", " & _
          "dia_3 = " & BL_TrataStringParaBD(eTerapeutica(1).t_dia_3 & ", " & _
          "dia_4 = " & BL_TrataStringParaBD(eTerapeutica(1).t_dia_4)) & ", " & _
          "dia_5 = " & BL_TrataStringParaBD(eTerapeutica(1).t_dia_5) & ", " & _
          "dia_6 = " & BL_TrataStringParaBD(eTerapeutica(1).t_dia_6) & ", " & _
          "dia_7 = " & BL_TrataStringParaBD(eTerapeutica(1).t_dia_7) & ", " & _
          "where n_req = " & eTerapeutica(1).t_n_req & _
          "and flg_hipo = 1"
          
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    If (Empty <> gSQLError) Then: BG_Mensagem mediMsgBox, "Erro ao actualizar nova terap�utica", vbExclamation, " Gravar": Exit Sub
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Private Sub LvTerapeutica_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (Button = vbLeftButton And Not LvTerapeutica.SelectedItem Is Nothing And LvTerapeutica.SelectedItem.Index <> 1) Then
        Set LvTerapeutica.SelectedItem = oSourceItem
        LvTerapeutica.DragIcon = ImageListIcons.ListImages(ListViewIcons.e_IconRightBlue).Picture
        LvTerapeutica.Drag vbBeginDrag
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvTerapeutica_MouseMove' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub LvTerapeutica_DragDrop(Source As Control, X As Single, Y As Single)
   
    On Error GoTo ErrorHandler
    If (Nothing Is oSourceItem) Then: Exit Sub
    If (False = CanDropItem(ListasEcra.e_LvTerapeutica)) Then: Exit Sub
    LvTerapeutica.ListItems(1).SmallIcon = ListViewIcons.e_IconRightBlue
    Call CopiaTerapeutica(oSourceItem.Index)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvTerapeutica_DragDrop' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' COPIA TERAPEUTICA (INDEX -> 1) - ACTUALIZA LISTA E ESTRUTURA.
Public Sub CopiaTerapeutica(Optional Index As Long)
    
    Dim undragged As Boolean
    
    On Error GoTo ErrorHandler
    If (LvTerapeutica.ListItems.Count < 2) Then: Exit Sub
    If (Empty = Index) Then: undragged = True: Index = 2
    With LvTerapeutica.ListItems(1)
        .text = BL_SelCodigo("sl_tera_med", "descr_tera_med", "seq_tera_med", eTerapeutica(Index).t_cod_tera_med)
        .ListSubItems.Item(LvTerapeuticaColunas.e_periodo - 1).text = eTerapeutica(Index).t_periodo
        .ListSubItems.Item(LvTerapeuticaColunas.e_res_ana - 1).text = eTerapeutica(Index).t_res_ana
        .ListSubItems.Item(LvTerapeuticaColunas.e_dia_1 - 1).text = eTerapeutica(Index).t_dia_1
        .ListSubItems.Item(LvTerapeuticaColunas.e_dia_2 - 1).text = eTerapeutica(Index).t_dia_2
        .ListSubItems.Item(LvTerapeuticaColunas.e_dia_3 - 1).text = eTerapeutica(Index).t_dia_3
        .ListSubItems.Item(LvTerapeuticaColunas.e_dia_4 - 1).text = eTerapeutica(Index).t_dia_4
        .ListSubItems.Item(LvTerapeuticaColunas.e_dia_5 - 1).text = eTerapeutica(Index).t_dia_5
        .ListSubItems.Item(LvTerapeuticaColunas.e_dia_6 - 1).text = eTerapeutica(Index).t_dia_6
        .ListSubItems.Item(LvTerapeuticaColunas.e_dia_7 - 1).text = eTerapeutica(Index).t_dia_7
    End With
    Call DefinePeriodo(CInt(eTerapeutica(Index).t_periodo))
    eTerapeutica(1).t_cod_tera_med = eTerapeutica(Index).t_cod_tera_med
    eTerapeutica(1).t_res_ana = eTerapeutica(Index).t_res_ana
    eTerapeutica(1).t_periodo = eTerapeutica(Index).t_periodo
    eTerapeutica(1).t_dia_1 = eTerapeutica(Index).t_dia_1
    eTerapeutica(1).t_dia_2 = eTerapeutica(Index).t_dia_2
    eTerapeutica(1).t_dia_3 = eTerapeutica(Index).t_dia_3
    eTerapeutica(1).t_dia_4 = eTerapeutica(Index).t_dia_4
    eTerapeutica(1).t_dia_5 = eTerapeutica(Index).t_dia_5
    eTerapeutica(1).t_dia_6 = eTerapeutica(Index).t_dia_6
    eTerapeutica(1).t_dia_7 = eTerapeutica(Index).t_dia_7
    If (undragged) Then: Call InsereTerapeutica("anterior")
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CopiaTerapeutica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub
Private Sub BeforeHandleListViewTree(X As Single, Y As Single)
    
    Dim lvhti As LVHITTESTINFO
    
    lvhti.pt.X = X / Screen.TwipsPerPixelX
    lvhti.pt.Y = Y / Screen.TwipsPerPixelY
    If (ListView_ItemHitTest(lhwndLvTerapeutica, lvhti) = LVI_NOITEM) Then: Exit Sub
    If (lvhti.flags = LVHT_ONITEMICON) Then: Exit Sub

End Sub

Private Sub LvTerapeutica_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (Empty = LvTerapeutica.ListItems.Count) Then: Exit Sub
    If (1 <> LvTerapeutica.SelectedItem.Index) Then: Exit Sub
    Select Case (Button)
        Case MouseButtonConstants.vbLeftButton: Call HandleListViewClick(ListasEcra.e_LvTerapeutica)
        Case Else: ' Empty
    End Select
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvTerapeutica_MouseUp' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Function CanDropItem(lista As ListasEcra) As Boolean
  
    Dim lvhti As LVHITTESTINFO
    Dim lListItem As ListItem
    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    Call GetCursorPos(lvhti.pt)
    Select Case (lista)
        Case ListasEcra.e_LvTerapeutica:
            Call ScreenToClient(lhwndLvTerapeutica, lvhti.pt)
            If (ListView_SubItemHitTest(lhwndLvTerapeutica, lvhti) = LVI_NOITEM) Then: Exit Function
            'If (lvhti.iSubItem = Empty) Then: Exit Sub
            If (ListView_GetSubItemRect(lhwndLvTerapeutica, lvhti.iItem, lvhti.iSubItem, LVIR_LABEL, rc)) Then
                Call MapWindowPoints(lhwndLvTerapeutica, hWnd, rc, 2)
                lidxLvTerapeutica = lvhti.iItem + 1
                CanDropItem = CBool(lidxLvTerapeutica = 1)
            End If
        Case Else:  ' Empty
    End Select
    Exit Function
  
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CanDropItem' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next

End Function

' INICIALIZA GRAFICO DE RESULTAODOS.
Private Sub InicializaGraficoResultados()
    
    On Error GoTo ErrorHandler
    With MSResultados
        .ColumnCount = 1
        .RowCount = 1
        .Plot.SeriesCollection(1).SeriesMarker.Show = False
        .row = 1
        .RowLabel = ""
        .Column = 1
        .ColumnLabel = Empty
    End With
    Exit Sub
  
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InicializaGraficoResultados' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

' DEFINE PROPRIEDADES DO GRAFICO.
Private Sub DefineGraficoResultados()
    
    On Error GoTo ErrorHandler
    With MSResultados
        .Plot.SeriesCollection.Item(1).Pen.Style = VtPenStyleSolid
        .Plot.SeriesCollection.Item(1).Pen.Width = 25
        .Plot.SeriesCollection(1).DataPoints(-1).Brush.FillColor.Set 112, 146, 190
        '.Plot.Axis(VtChAxisIdY).AxisTitle = "An�lise"
        '.Plot.Axis(VtChAxisIdX).AxisTitle = "Hist�rico"
        .chartType = VtChChartType2dLine
        .AllowDithering = False
        .AllowDynamicRotation = False
        .AllowSelections = False
        .AllowSeriesSelection = False
        .AutoIncrement = False
        .ShowLegend = False
    End With
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'DefineGraficoResultados' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' DEVOLVE DESCRICAO ANALISE POR SEQ_ANA.
Private Function DevolveDescricaoAnalise(seq_ana As Long)
    
    Dim i As Long
    
    On Error GoTo ErrorHandler
    For i = 1 To TotalResultados
        If (eResultados(i).t_seq_ana = seq_ana) Then: DevolveDescricaoAnalise = eResultados(i).t_descr_ana
    Next
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'DevolveDescricaoAnalise' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' ACTUALIZA GRAFICO RESULTADOS.
Private Sub ActualizaGraficoResultados(Optional seq_ana As Long)
    
    Dim row As Long
    Dim results As New CollectionKEY
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A actualizar gr�fico de resultados..."
    If (Empty = seq_ana) Then: seq_ana = eResultados(1).t_seq_ana
    Set results = DevolveItems(seq_ana)
    
    If (results.Count = Empty) Then: Exit Sub
    With MSResultados
        .Plot.Axis(VtChAxisIdY).AxisTitle = DevolveDescricaoAnalise(seq_ana)
        .ColumnCount = 1
        .RowCount = results.Count
        For row = 1 To results.Count
            .row = row
            .RowLabel = Format$(eResultados(results.Item(row)).t_dt_cri, "dd-mm-yyyy") & Space(1) & eResultados(results.Item(row)).t_hr_cri
            .data = BG_CvDecimalParaDisplay(eResultados(results.Item(row)).t_resultado)
        Next
    End With
    DefineGraficoResultados
    MostraMarcadores True
    MSResultados.Repaint = True
    DoEvents
    BL_FimProcessamento Me
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaGraficoResultados' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BL_FimProcessamento Me
    Exit Sub

End Sub

' DEVOLVE ITEMS DA ESTRUTURA DE RESULTADOS PARA DETERMINADO SEQ_ANA.
Private Function DevolveItems(seq_ana) As CollectionKEY
     Dim Item As Long
    
    On Error GoTo ErrorHandler
    Set DevolveItems = New CollectionKEY
    For Item = 1 To TotalResultados
        If (eResultados(Item).t_seq_ana = seq_ana) Then: Call DevolveItems.Add(Item, eResultados(Item).t_seq_ana & "_" & Item)
    Next
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'DevolveItems' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Sub MSResultados_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
   
    Dim iPart As Integer
    Dim iSeries As Integer
    Dim iDatapoint As Integer
    Dim iIndex3 As Integer
    Dim iIndex4 As Integer
    Dim dblValue As Double, iNullflag As Integer
    
    On Error GoTo ErrorHandler
    With MSResultados
        .TwipsToChartPart X, Y, iPart, iDatapoint, iSeries, iIndex3, iIndex4
        If (iPart = VtChPartTypePoint) Then
            .DataGrid.GetData iSeries, iDatapoint, dblValue, iNullflag
            .Column = iDatapoint
            .ToolTipText = dblValue
        Else
            .ToolTipText = ""
        End If
    End With
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'MSResultados_MouseMove' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' ACTUALIZA A BARRA DE PERCENTAGEM DE OCUPACOES.
Private Sub ActualizaPercentagemMarcacoes(data_prevista As String)

    Dim sql As String
    Dim rsAgenda As ADODB.recordset
    
    On Error GoTo ErrorHandler
    BarMarcacoes.caption = Empty
    BarMarcacoes.Max = 0
    BarMarcacoes.Value = 0
    Set rsAgenda = New ADODB.recordset
    rsAgenda.CursorLocation = adUseClient
    rsAgenda.CursorType = adOpenStatic
    sql = "select dt_marcacao, limite, count (0) curr from sl_agenda, sl_t_agenda where dt_marcacao = " & BL_TrataDataParaBD(data_prevista) & _
          " and sl_agenda.cod_t_agenda = " & TiposAgenda.e_Hipocoagulados & " and sl_agenda.cod_t_agenda = sl_t_agenda.cod_t_agenda " & _
          "group by dt_marcacao, limite"
    rsAgenda.Open sql, gConexao
    If (rsAgenda.RecordCount > Empty) Then
        BarMarcacoes.Max = BL_HandleNull(rsAgenda!limite, Empty)
        BarMarcacoes.Value = BL_HandleNull(rsAgenda!curr, Empty)
        If (BarMarcacoes.Value <= (BarMarcacoes.Max / 2)) Then
            BarMarcacoes.CaptionForeColor = vbBlack
        Else
            BarMarcacoes.CaptionForeColor = vbWhite
        End If
    End If
    BarMarcacoes.ToolTipText = "A taxa de ocupa��o para o dia " & data_prevista & " � de " & Round(Split(BarMarcacoes.caption, "%")(0), 0) & "%"
    If (rsAgenda.State = adStateOpen) Then: rsAgenda.Close
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaPercentagemMarcacoes' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub ActualizaCalendarioBold()
    
    Dim Item As Long
    Dim i As Date
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A actualizar agenda para este m�s..."
    For Item = 1 To TotalAgenda
        If (Month(eAgenda(Item).t_dt_prevista) = Month(MvAgenda.Value)) Then
            If (Not MvAgenda.DayBold(eAgenda(Item).t_dt_prevista)) Then: MvAgenda.DayBold(eAgenda(Item).t_dt_prevista) = True
        End If
    Next
    DoEvents
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaCalendarioBold' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Private Sub MvAgenda_SelChange(ByVal StartDate As Date, ByVal EndDate As Date, Cancel As Boolean)

    On Error GoTo ErrorHandler
    If (AnoAnterior <> Year(StartDate) Or MesAnterior <> Month(StartDate)) Then
        LvAgenda.ListItems.Clear
        TotalAgenda = Empty
        ReDim eAgenda(TotalAgenda)
        ProcuraRegistosAgenda Month(StartDate)
        DiaAnterior = Empty
        BL_FimProcessamento Me
        Me.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    End If
    
    If (DiaAnterior <> Day(StartDate)) Then
        LvAgenda.ListItems.Clear
        ActualizaListaAgenda CStr(StartDate)
        ActualizaCalendarioBold
        ActualizaPercentagemMarcacoes CStr(StartDate)
        BL_FimProcessamento Me
        Me.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    End If
    
    AnoAnterior = Year(StartDate)
    MesAnterior = Month(StartDate)
    DiaAnterior = Day(StartDate)
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'MvAgenda_SelChange' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub MvAgenda_DateDblClick(ByVal DateDblClicked As Date)

    Dim mensagem As String
    
    gMsgTitulo = " Marca��o"
    gMsgMsg = "Tem a certeza que quer efecutar marca��o para a data " & CStr(DateDblClicked) & " ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbNo) Then: Exit Sub
    
    If (Not PermiteMarcacao(DateDblClicked, mensagem)) Then
        BG_Mensagem mediMsgBox, "N�o � poss�vel executar a marca��o: " & mensagem, vbExclamation, " Marca��o"
        Exit Sub
    End If
   
    If (Not ExecutaMarcacao(CStr(DateDblClicked), mensagem)) Then
        BG_Mensagem mediMsgBox, "N�o � poss�vel executar a marca��o: " & mensagem, vbExclamation, " Marca��o"
        Exit Sub
    End If
    
End Sub

' EXECUTA MARCACAO PARA DETERMINADO DATA.
Private Function ExecutaMarcacao(data_prevista As String, ByRef mensagem As String) As Boolean

    Dim numero_requisicao As Long
    
    On Error GoTo ErrorHandler
    If (EcNumReq.text = Empty) Then: mensagem = "Campo de requisi��o vazio": Exit Function
    gConexao.BeginTrans
    If (Not GeraNumeroRequisicao(numero_requisicao)) Then: gConexao.RollbackTrans: mensagem = "Erro ao gerar n� requisi��o": Exit Function
    If (Not ExecutaMarcacaoRequisicao(numero_requisicao, data_prevista)) Then: gConexao.RollbackTrans: mensagem = "Erro ao inserir requisi��o": Exit Function
    If (Not ExecutaMarcacaoAnalises(numero_requisicao)) Then: gConexao.RollbackTrans: mensagem = "Erro ao inserir an�lises": Exit Function
    If (Not ExecutaMarcacaoTubos(numero_requisicao, data_prevista)) Then: gConexao.RollbackTrans: mensagem = "Erro ao inserir tubos": Exit Function
    If (Not ExecutaMarcacaoProdutos(numero_requisicao, data_prevista)) Then: gConexao.RollbackTrans: mensagem = "Erro ao inserir produtos": Exit Function
    If (Not ExecutaMarcacaoAgenda(numero_requisicao, data_prevista)) Then: gConexao.RollbackTrans: mensagem = "Erro ao inserir agenda": Exit Function
    gConexao.CommitTrans
    ExecutaMarcacao = True
    BG_Mensagem mediMsgBox, "Marca��o efectuada: " & numero_requisicao, vbInformation, " Marca��o"
    Call ActualizaAgenda(data_prevista)
    Exit Function

ErrorHandler:
    gConexao.RollbackTrans
    BG_LogFile_Erros "Error in function 'ExecutaMarcacao' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' EXECUTA MARCACAO REQUISICAO.
Private Function ExecutaMarcacaoRequisicao(ByRef numero_requisicao As Long, data_prevista As String) As Boolean
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "insert into " & DevolveTipoTabela("sl_requis") & " (n_req,n_req_assoc,seq_utente,n_epis,dt_previ,dt_imp,t_sit,t_urg,cod_efr,n_benef," & _
          "cod_proven,cod_med,obs_proven,pag_tax,pag_ent,pag_ute,obs_req,estado_req,user_cri,dt_cri,hr_cri,user_act,dt_act,hr_act,dt_chega,hr_imp," & _
          "user_imp,dt_imp2,hr_imp2,user_imp2,cod_isencao,cod_urbano,km,flg_facturado,gr_ana,req_aux,cod_med2,ficha,arquivo,dt_fact,hr_chega," & _
          "mot_fact_rej,dt_fact_rej,hr_fact_rej,user_fact_rej,dt_validade,cod_local,dt_fecho,hr_fecho,user_fecho,user_colheita,dt_colheita," & _
          "hr_colheita,inf_complementar,cod_sala,cod_t_colheita,cod_destino,hemodialise,descr_medico,morada,cod_postal,t_urbano,tipo_urgencia," & _
          "convencao,fim_semana,flg_cobra_domicilio,peso_ute,altura_ute,seq_utente_fact,requisicao_alert,n_prescricao,flg_req_grupo_especial," & _
          "flg_transf,dt_pretend,flg_aviso_sms,flg_sms_enviada,user_confirm,dt_confirm,hr_confirm,dt_conclusao,cod_valencia,flg_hipo) " & _
          "(select " & numero_requisicao & ",n_req_assoc,seq_utente,n_epis," & BL_TrataDataParaBD(data_prevista) & ",dt_imp,t_sit,t_urg,cod_efr," & _
          "n_benef,cod_proven,cod_med,obs_proven,pag_tax,pag_ent,pag_ute," & "obs_req,'I'," & gCodUtilizador & "," & _
          BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ",null,null,null,null,hr_imp,user_imp,dt_imp2," & _
          "null,null,cod_isencao,cod_urbano,km,null,gr_ana,req_aux,cod_med2,ficha,arquivo,null,null,null,null," & _
          "null,null,dt_validade,cod_local,null,null,null,user_colheita,dt_colheita,hr_colheita,inf_complementar," & _
          "cod_sala,cod_t_colheita,cod_destino,hemodialise,descr_medico,morada,cod_postal,t_urbano,tipo_urgencia,convencao,fim_semana," & _
          "flg_cobra_domicilio,peso_ute,altura_ute,null,requisicao_alert,n_prescricao,flg_req_grupo_especial,flg_transf,dt_pretend," & _
          "flg_aviso_sms,flg_sms_enviada,null,null,null,null,cod_valencia,flg_hipo from sl_requis " & _
          "where n_req = " & EcNumReq & ")"
    BG_ExecutaQuery_ADO sql
    ExecutaMarcacaoRequisicao = CBool(gSQLError = Empty)
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExecutaMarcacaoRequisicao' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' EXECUTA MARCACAO ANALISES.
Private Function ExecutaMarcacaoAnalises(numero_requisicao As Long) As Boolean
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "insert into " & DevolveTipoTabela("sl_marcacoes") & " (n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup," & _
          "n_folha_trab,dt_chega,ord_marca,flg_facturado,hr_colheita,dt_colheita,hr_ult_admin,dt_ult_admin,flg_apar_trans) (select " & _
          numero_requisicao & ",cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,null,ord_marca," & _
          "flg_facturado,hr_colheita,dt_colheita,hr_ult_admin,dt_ult_admin, 0 from sl_marcacoes where n_req = " & numero_requisicao & ") union (" & _
          "select " & numero_requisicao & ",cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,null," & _
          "ord_marca,flg_facturado,hr_colheita,dt_colheita,hr_ult_admin,dt_ult_admin, 0 from sl_realiza where n_req = " & EcNumReq & ")"
    BG_ExecutaQuery_ADO sql
    ExecutaMarcacaoAnalises = CBool(gSQLError = Empty)
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExecutaMarcacaoAnalises' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' EXECUTA MARCACAO TUBOS.
Private Function ExecutaMarcacaoTubos(numero_requisicao As Long, data_prevista As String) As Boolean
     
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "insert into " & DevolveTipoTabela("sl_req_tubo") & " (n_req,cod_tubo,dt_previ,dt_chega,hr_chega,dt_eliminacao,hr_eliminacao,mot_novo," & _
          "user_eliminacao,id_garrafa,dt_imp,hr_imp,user_imp,dt_ult_imp,hr_ult_imp,user_ult_imp,user_chega,local_chega,dt_saida,hr_saida,user_saida," & _
          "local_saida,dt_colheita,hr_colheita,cod_local,user_colheita,flg_transito) (select " & numero_requisicao & ",cod_tubo," & _
          BL_TrataDataParaBD(data_prevista) & ",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null," & _
          "local_saida,dt_colheita,hr_colheita,null,user_colheita,null from sl_req_tubo where n_req = " & EcNumReq & ")"
    BG_ExecutaQuery_ADO sql
    ExecutaMarcacaoTubos = CBool(gSQLError = Empty)
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExecutaMarcacaoTubos' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' EXECUTA MARCACAO PRODUTOS.
Private Function ExecutaMarcacaoProdutos(numero_requisicao As Long, data_prevista As String) As Boolean
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "insert into " & DevolveTipoTabela("sl_req_prod") & " (n_req,cod_prod,dt_previ,dt_chega,cod_especif,volume,obs_especif) (select " & _
          numero_requisicao & ",cod_prod," & BL_TrataDataParaBD(data_prevista) & ",null,cod_especif,volume,obs_especif from sl_req_prod " & _
          "where n_req = " & EcNumReq & ")"
    BG_ExecutaQuery_ADO sql
    ExecutaMarcacaoProdutos = CBool(gSQLError = Empty)
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExecutaMarcacaoProdutos' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' GERA NOVO NUMERO DE REQUISICAO.
Private Function GeraNumeroRequisicao(ByRef numero_requisicao As Long) As Boolean

    Dim tamanho As Integer
    
    On Error GoTo ErrorHandler
    numero_requisicao = -1
    tamanho = Empty
    While (numero_requisicao = -1 And tamanho <= 10)
        numero_requisicao = BL_GeraNumero(BL_HandleNull(DevolveTipoTabela("N_REQUIS"), "N_REQUIS"))
        tamanho = tamanho + 1
    Wend
    GeraNumeroRequisicao = CBool(numero_requisicao <> -1)
    Exit Function
    
ErrorHandler:
    GeraNumeroRequisicao = False
    BG_LogFile_Erros "Error in function 'GeraNumeroRequisicao' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function

End Function

' EXECUTA A MARCACOA NA AGENDA (SL_AGENDA).
Private Function ExecutaMarcacaoAgenda(numero_requisicao As Long, data_prevista As String)

    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "insert into sl_agenda (cod_t_agenda,dt_marcacao,marcacoes,n_req,flg_activo,cod_local) values (" & _
          TiposAgenda.e_Hipocoagulados & "," & BL_TrataDataParaBD(data_prevista) & ",null," & numero_requisicao & ",null," & gCodLocal & ")"
    BG_ExecutaQuery_ADO sql
    ExecutaMarcacaoAgenda = CBool(gSQLError = Empty)
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExecutaMarcacaoAgenda' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' VERIFICA SE A MARCACAO PODE SER EFECTUADA.
Private Function PermiteMarcacao(data_prevista As Date, ByRef mensagem As String) As Boolean
    
    On Error GoTo ErrorHandler
    PermiteMarcacao = CBool(BarMarcacoes.Max <> BarMarcacoes.Value)
    If (Not PermiteMarcacao) Then: mensagem = "Agenda completa": Exit Function
    PermiteMarcacao = CBool(BL_VerficaDiasIndisponiveis(1, CDate(data_prevista - 1)) = data_prevista)
    If (Not PermiteMarcacao) Then: mensagem = "Dia indisponivel": Exit Function
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PermiteMarcacao' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' ACTUALIZA AGENDA (ESTRUTURA,LISTA,PROGRESS BAR).
Private Sub ActualizaAgenda(data_prevista As String)
    
    On Error GoTo ErrorHandler
    Call ProcuraRegistosAgenda(Month(data_prevista))
    Call ActualizaListaAgenda(data_prevista)
    Call ActualizaPercentagemMarcacoes(data_prevista)
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ActualizaAgenda' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' CONSULTA REQUISICAO.
Private Sub ConsultaRequisicao(numero_requisicao As String)

    Dim form_consulta As Form
    
    On Error GoTo ErrorHandler
    If (numero_requisicao = Empty) Then: Exit Sub
    Select Case (gTipoActividade)
        Case TiposActividade.e_Marcacao: Set form_consulta = FormGesReqCons
        Case TiposActividade.e_Requisicao:
            Select Case (gTipoInstituicao)
                Case gTipoInstituicaoHospitalar: Set form_consulta = FormGestaoRequisicao
                Case gTipoInstituicaoPrivada: Set form_consulta = FormGestaoRequisicaoPrivado
                Case Else: ' Empty
            End Select
        Case Else: ' Empty
    End Select
    If (form_consulta Is Nothing) Then: Exit Sub
    
    form_consulta.Show
    DoEvents
    form_consulta.EcNumReq = numero_requisicao
    form_consulta.FuncaoProcurar
    DoEvents
    Set gFormActivo = form_consulta
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConsultaRequisicao' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' CONSULTA RESULTADOS DA REQUISI��O.
Private Sub ConsultaResultados(numero_requisicao As String)

    Dim form_consulta As Form
    Dim campo_pesquisa As TextBox
    
    On Error GoTo ErrorHandler
    If (numero_requisicao = Empty) Then: Exit Sub
    Select Case (gNovoEcraResultados)
        Case (mediSim):
            Set form_consulta = FormResultadosNovo
            Set campo_pesquisa = form_consulta.EcPesqNumReq
        Case Else:
            Set form_consulta = FormResultados
            Set campo_pesquisa = form_consulta.EcNumReq
    End Select
    form_consulta.Show
    DoEvents
    campo_pesquisa = numero_requisicao
    form_consulta.FuncaoProcurar
    DoEvents
    Set gFormActivo = form_consulta
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConsultaResultados' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' CONSULTA UTENTE.
Private Sub ConsultaUtente(sequencial_utente As String)

    Dim form_consulta As Form
    
    On Error GoTo ErrorHandler
    If (sequencial_utente = Empty) Then: Exit Sub
    Set form_consulta = FormIdentificaUtente
    form_consulta.Show
    DoEvents
    form_consulta.EcCodSequencial = sequencial_utente
    form_consulta.FuncaoProcurar
    DoEvents
    Set gFormActivo = form_consulta
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConsultaUtente' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub PreencheDiagnostico(seq_utente As String)

    Dim sql As String
    Dim rsDiag As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rsDiag = New ADODB.recordset
    rsDiag.CursorLocation = adUseClient
    rsDiag.CursorType = adOpenStatic
    sql = "select sl_diag.cod_diag, sl_diag.descr_diag from sl_diag_pri, sl_diag where seq_utente = " & seq_utente & " and sl_diag_pri.cod_diag = sl_diag.cod_diag"
    rsDiag.Open sql, gConexao
    While (Not rsDiag.EOF)
        With LvDiagnoticos.ListItems.Add()
            .ListSubItems.Add , , BL_HandleNull(rsDiag!descr_diag, Empty), , "[" & BL_HandleNull(rsDiag!cod_diag, Empty) & "] " & BL_HandleNull(rsDiag!descr_diag, Empty)
        End With
        rsDiag.MoveNext
    Wend
    If (rsDiag.State = adStateOpen) Then: rsDiag.Close
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheDiagnostico' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub ConsultaInformacaoClinica()
    
    Dim form_consulta As Form
    
    On Error GoTo ErrorHandler
    If (EcSeqUtente.text = Empty) Then: Exit Sub
    BL_PreencheDadosUtente EcSeqUtente
    Me.Enabled = False
    Set form_consulta = FormInformacaoClinica
    form_consulta.Show
    DoEvents
    form_consulta.FuncaoProcurar
    DoEvents
    Set gFormActivo = form_consulta
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConsultaInformacaoClinica' in form FormHipoResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

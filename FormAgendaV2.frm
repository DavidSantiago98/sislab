VERSION 5.00
Object = "{79EB16A5-917F-4145-AB5F-D3AEA60612D8}#16.3#0"; "CODEJO~1.OCX"
Begin VB.Form FormAgendaV2 
   BackColor       =   &H00FFFFFF&
   Caption         =   "FormAgendaV2"
   ClientHeight    =   8580
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   13005
   Icon            =   "FormAgendaV2.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8580
   ScaleWidth      =   13005
   Begin XtremeCalendarControl.DatePicker calendario 
      Height          =   7935
      Left            =   10680
      TabIndex        =   1
      Top             =   360
      Width           =   2535
      _Version        =   1048579
      _ExtentX        =   4471
      _ExtentY        =   13996
      _StockProps     =   64
      Show3DBorder    =   2
      RowCount        =   3
      MaxSelectionCount=   5
      TextTodayButton =   "Hoje"
      VisualTheme     =   3
   End
   Begin XtremeCalendarControl.CalendarControl CalAgenda 
      Height          =   8055
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   10455
      _Version        =   1048579
      _ExtentX        =   18441
      _ExtentY        =   14208
      _StockProps     =   64
      ViewType        =   1
      VisualTheme     =   3
      ShowCaptionBar  =   -1  'True
      ShowTimelineButton=   0   'False
      ShowMultiColumnsButton=   0   'False
      ShowExpandButton=   -1  'True
   End
   Begin VB.Frame FrameControlos 
      BackColor       =   &H00F7F1DC&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   0
      TabIndex        =   2
      Top             =   -120
      Width           =   11295
      Begin VB.CommandButton BtImprimir 
         Height          =   375
         Left            =   5160
         Picture         =   "FormAgendaV2.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Imprimir"
         Top             =   120
         Width           =   495
      End
      Begin VB.ComboBox CbTAgenda 
         Height          =   315
         Left            =   1680
         TabIndex        =   3
         Text            =   "Combo1"
         Top             =   120
         Width           =   3135
      End
      Begin VB.Label Label1 
         BackColor       =   &H00F7F1DC&
         Caption         =   "Agenda:"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   165
         Width           =   1455
      End
   End
End
Attribute VB_Name = "FormAgendaV2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim agenda As ClAgenda
Dim estado As Integer
Dim NReq As Long
Dim tReq As String

Private Sub BtImprimir_Click()
    CalAgenda.PrintPreview True
End Sub

Private Sub CalAgenda_DblClick()
    Dim HitTest As CalendarHitTestInfo
    Set HitTest = CalAgenda.ActiveView.HitTest
    
    If HitTest.ViewEvent Is Nothing Then
        NovoEvento
    Else
        ModificarEvento HitTest.ViewEvent.Event
    End If
    
End Sub

Private Sub CalAgenda_IsEditOperationDisabled(ByVal OpParams As XtremeCalendarControl.CalendarEditOperationParameters, DisableOperation As Boolean)
    
    If OpParams.Operation = xtpCalendarEO_DragCopy Or _
        OpParams.Operation = xtpCalendarEO_DragResizeBegin Or _
        OpParams.Operation = xtpCalendarEO_DragResizeEnd Or _
        OpParams.Operation = xtpCalendarEO_EditSubject_AfterEventResize Or _
        OpParams.Operation = xtpCalendarEO_EditSubject_ByF2 Or _
        OpParams.Operation = xtpCalendarEO_EditSubject_ByMouseClick Or _
        OpParams.Operation = xtpCalendarEO_EditSubject_ByTab _
    Then
        DisableOperation = True
    ElseIf OpParams.Operation = xtpCalendarEO_DeleteEvent Then
        gMsgTitulo = "Remover Agendamento"
        gMsgMsg = "Tem a certeza que quer apagar o agendamento?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            If agenda.AG_ApagaventoAgenda(OpParams.EventViews(0).Event) = True Then
                DisableOperation = False
            Else
                DisableOperation = True
            End If
        Else
            DisableOperation = True
        End If
    ElseIf OpParams.Operation = xtpCalendarEO_DragMove Then
'        gMsgTitulo = "Altera��o de Agendamento"
'        gMsgMsg = "Tem a certeza que quer alterar o agendamento?"
'        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
'        If gMsgResp = vbYes Then
'            If 1 = 1 Then
'                DisableOperation = False
'            Else
'                DisableOperation = True
'            End If
'        Else
'            DisableOperation = True
'        End If
'
    End If
        

End Sub



Private Sub CbTAgenda_Click()
    InicializaRequisicao NReq, tReq
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.WindowState = 2
    Me.caption = " Emiss�o de Refer�ncias Multibanco"
    Me.left = 50
    Me.top = 50
    Me.Width = 13140
    Me.Height = 6945 ' Normal
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    PreencheValoresDefeito
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormAgendaV2 = Nothing
    If gF_REQCONS = mediSim Then
        FormGesReqCons.Enabled = True
        Set gFormActivo = FormGesReqCons
    ElseIf gF_REQUIS = mediSim Then
        FormGestaoRequisicao.Enabled = True
        Set gFormActivo = FormGestaoRequisicao
    End If
End Sub



Sub DefTipoCampos()
End Sub

Sub PreencheCampos()
    

End Sub

Sub PreencheValoresDefeito()
    Dim i As Integer
    BG_PreencheComboBD_ADO "sl_t_agenda", "cod_t_agenda", "descr_t_agenda", CbTAgenda
    If gCodTAgendaDefeito <> "" Then
        For i = 0 To CbTAgenda.ListCount - 1
            If CbTAgenda.ItemData(i) = gCodTAgendaDefeito Then
                CbTAgenda.ListIndex = i
                Exit For
            End If
        Next i
    End If
    'inicializa_calendario
End Sub


Sub FuncaoLimpar()
    

End Sub

Sub FuncaoEstadoAnterior()

End Sub



Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Dim nHeight As Long, LabelWidth As Long
    
    nHeight = Height - CalAgenda.top - 12 * Screen.TwipsPerPixelY
    
    
    If nHeight < 0 Then Height = 0

    
    calendario.left = Me.ScaleWidth - calendario.Width - 3
    calendario.Height = nHeight
    FrameControlos.left = 0
    FrameControlos.Width = calendario.left + calendario.Width
    'wndDatePicker.Move Me.ScaleWidth - wndDatePicker.Width, CalendarControl.Top, wndDatePicker.Left, nHeight
    'CalendarControl.Move 0, CalendarControl.Top, Me.ScaleWidth, nHeight
    CalAgenda.Move 0, CalAgenda.top, calendario.left, nHeight

    'lblDate.Move lblCaption.Width, lblDate.Top, LabelWidth, lblDate.Height
End Sub

Public Sub InicializaRequisicao(n_req As Long, tipo As String)
    NReq = n_req
    tReq = tipo
    Set agenda = Nothing
    CalAgenda.DataProvider.RemoveAllEvents
    CalAgenda.RedrawControl
    CalAgenda.Visible = False
    If CbTAgenda.ListIndex > mediComboValorNull Then
        Set agenda = New ClAgenda
        agenda.Ag_InicializaAgendaByCod CbTAgenda.ItemData(CbTAgenda.ListIndex)
        agenda.Ag_InicializaReq n_req, tipo
        agenda.Ag_inicializaControlo CalAgenda, calendario
        agenda.AG_CarregaEventosAgenda
        CalAgenda.Visible = True
    End If
End Sub

Public Sub NovoEvento()
    Dim aux As String
    On Error GoTo TrataErro
    Dim m_pEditingEvent As CalendarEvent
    Set m_pEditingEvent = agenda.Ag_novoEvento
    If m_pEditingEvent.CustomProperties.Property("N_REQ") <> "" Then
        aux = agenda.AG_ExisteRequisicaoAgendada
        If aux = "" Then
            Me.Enabled = False
            Set gFormActivo = FormAgendaNovoEvento
            FormAgendaNovoEvento.Show
            FormAgendaNovoEvento.CkImprimirMarcacao.value = vbUnchecked
            FormAgendaNovoEvento.CkImprimirMarcacao.Visible = True
            FormAgendaNovoEvento.InicializaEvento m_pEditingEvent, CalAgenda, agenda
        Else
            BG_Mensagem mediMsgBox, "Requisi��o j� tem marca��o agendada. Ter� que re-agendar ou apagar marca��o existente:" & vbCrLf & aux
            Exit Sub
        End If
    End If
Exit Sub
TrataErro:
End Sub

Public Sub ModificarEvento(m_pEditingEvent As CalendarEvent)
    Me.Enabled = False
    Set gFormActivo = FormAgendaNovoEvento
    FormAgendaNovoEvento.Show
    FormAgendaNovoEvento.CkImprimirMarcacao.value = vbUnchecked
    FormAgendaNovoEvento.CkImprimirMarcacao.Visible = False
    FormAgendaNovoEvento.InicializaEvento m_pEditingEvent, CalAgenda, agenda
End Sub


Public Sub CalculaTempos(BeginSelection As Date, EndSelection As Date, AllDay As Boolean)
    Dim StartDate As Date, StartTime As Date, EndDate As Date, EndTime As Date

    StartDate = DateValue(BeginSelection)
    StartTime = TimeValue(BeginSelection)

    EndDate = DateValue(EndSelection)
    EndTime = TimeValue(EndSelection)
    
 
End Sub

Private Sub calagenda_EventChanged(ByVal EventID As Long)
    Dim pEvent As CalendarEvent
    Set pEvent = CalAgenda.DataProvider.GetEvent(EventID)
    
    ' pEvent Is Nothing for recurrence Ocurrence and Exception.
    ' See CalendarControl_PatternChanged for recurrence events changes.
    If Not pEvent Is Nothing Then
        agenda.AG_ActualizaEventoAgenda pEvent
    End If
End Sub

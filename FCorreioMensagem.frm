VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form FormCorreioMensagem 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionFormCorreioMensagem"
   ClientHeight    =   8415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11160
   Icon            =   "FCorreioMensagem.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8415
   ScaleWidth      =   11160
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtImprimir 
      Height          =   855
      Left            =   9720
      Picture         =   "FCorreioMensagem.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Imprimir mensagem"
      Top             =   4320
      Width           =   1320
   End
   Begin VB.CommandButton BtEnviar 
      Height          =   855
      Left            =   8400
      Picture         =   "FCorreioMensagem.frx":1CD6
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Enviar mensagem"
      Top             =   4320
      Width           =   1320
   End
   Begin VB.CommandButton BtAnexo 
      Height          =   855
      Left            =   7080
      Picture         =   "FCorreioMensagem.frx":39A0
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Adicionar Anexo"
      Top             =   4320
      Width           =   1320
   End
   Begin VB.PictureBox Picture1 
      Height          =   495
      Left            =   120
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   20
      Top             =   6360
      Width           =   615
   End
   Begin VB.Frame Frame3 
      Height          =   4215
      Left            =   7080
      TabIndex        =   11
      Top             =   0
      Width           =   3975
      Begin VB.CommandButton BtRetirarTodos 
         Height          =   1095
         Left            =   3360
         Picture         =   "FCorreioMensagem.frx":566A
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Remover Destinat�rios"
         Top             =   2760
         Width           =   495
      End
      Begin VB.CommandButton BtRetirar 
         Height          =   1095
         Left            =   3360
         Picture         =   "FCorreioMensagem.frx":5DD4
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   "Remover Destinat�rio Seleccionado"
         Top             =   1680
         Width           =   495
      End
      Begin VB.CommandButton BtAdicionar 
         Height          =   1095
         Left            =   3360
         Picture         =   "FCorreioMensagem.frx":653E
         Style           =   1  'Graphical
         TabIndex        =   18
         ToolTipText     =   "Adicionar Destinat�rio"
         Top             =   600
         Width           =   495
      End
      Begin VB.OptionButton EcOptListas 
         Caption         =   "Listas de utilizadores"
         Height          =   255
         Left            =   2040
         TabIndex        =   15
         Top             =   240
         Width           =   1815
      End
      Begin VB.OptionButton EcOptUtilizadores 
         Caption         =   "Utilizadores"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.ComboBox EcListas 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   600
         Width           =   3255
      End
      Begin VB.CheckBox EcChkOcultarDest 
         Caption         =   "Ocultar destinat�rios"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   3840
         Width           =   3135
      End
      Begin MSComctlLib.ListView EcDestinatarios 
         Height          =   2895
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   5106
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList2"
         SmallIcons      =   "ImageList2"
         ColHdrIcons     =   "ImageList2"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame2 
      Height          =   5175
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   6855
      Begin VB.Frame Frame1 
         Height          =   735
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Width           =   6495
         Begin VB.ComboBox EcFonte 
            Height          =   315
            ItemData        =   "FCorreioMensagem.frx":6CA8
            Left            =   120
            List            =   "FCorreioMensagem.frx":6CAA
            Sorted          =   -1  'True
            TabIndex        =   9
            Top             =   240
            Width           =   2415
         End
         Begin VB.ComboBox EcTamanho 
            Height          =   315
            Left            =   2520
            TabIndex        =   8
            Top             =   240
            Width           =   975
         End
         Begin MSComctlLib.Toolbar ToolbarFormatacao 
            Height          =   390
            Left            =   3600
            TabIndex        =   10
            Top             =   240
            Width           =   2775
            _ExtentX        =   4895
            _ExtentY        =   688
            ButtonWidth     =   609
            ButtonHeight    =   582
            ImageList       =   "ImageListFormatacao"
            DisabledImageList=   "ImageListFormatacao"
            HotImageList    =   "ImageListFormatacao"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "bold"
                  ImageIndex      =   1
                  Style           =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "italic"
                  ImageIndex      =   2
                  Style           =   1
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "underline"
                  ImageIndex      =   3
                  Style           =   1
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "left"
                  ImageIndex      =   4
                  Style           =   2
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "center"
                  ImageIndex      =   5
                  Style           =   2
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "right"
                  ImageIndex      =   6
                  Style           =   2
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "corrector"
                  Object.ToolTipText     =   "Verificar ortografia"
                  ImageIndex      =   7
               EndProperty
            EndProperty
         End
      End
      Begin VB.TextBox EcAssunto 
         Height          =   285
         Left            =   840
         TabIndex        =   4
         Top             =   240
         Width           =   5775
      End
      Begin RichTextLib.RichTextBox EcTexto 
         Height          =   2535
         Left            =   120
         TabIndex        =   3
         Top             =   1320
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   4471
         _Version        =   393217
         Enabled         =   -1  'True
         TextRTF         =   $"FCorreioMensagem.frx":6CAC
      End
      Begin MSComctlLib.ListView EcLtVwAnexos 
         Height          =   1215
         Left            =   120
         TabIndex        =   5
         Top             =   3840
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   2143
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ColHdrIcons     =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Label1 
         Caption         =   "Assunto"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   1575
      End
   End
   Begin MSComctlLib.ImageList ImageListFormatacao 
      Left            =   600
      Top             =   5640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":6D2E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":6E88
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":6FE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":713C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":7296
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":73F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":754A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CmdDlgPrint 
      Left            =   120
      Top             =   5640
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1800
      Top             =   5640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":7DDC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin Crystal.CrystalReport Report 
      Left            =   240
      Top             =   7080
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   1200
      Top             =   5640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":84D9
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":8873
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":8C0D
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":8F27
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreioMensagem.frx":933D
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormCorreioMensagem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

' pferreira 2009.03.03
' Constantes para identificar o icon de mensagem lida/nao-lida.
Const c_MESSAGE_NOT_READED = 4
Const c_MESSAGE_READED = 5

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim bFonte As Boolean
Dim bTamanho As Boolean

Dim sFonte As String
Dim iTamanho As Integer
'Emanuel Sousa - 02.06.2009
Dim userList() As Integer

Private Sub ConfiguraListView()
    EcDestinatarios.View = lvwReport
    
    ' pferreira 2009.03.03
    ' Nova coluna para icon de lido/n�o-lido.
    EcDestinatarios.ColumnHeaders.Add , , Empty, 350
    EcDestinatarios.ColumnHeaders.Add , , "Tipo", 650
    EcDestinatarios.ColumnHeaders.Add , , "Descri��o", 2000
    EcLtVwAnexos.View = lvwIcon
    SetListViewColor EcDestinatarios, Picture1, &H80000018, &HFFFFFF
End Sub



Private Sub DefTipoCampos()
    EcTexto.Tag = adVarWChar
End Sub

Private Sub PreencheDados(iCodMsg As Integer)
    Dim sSql As String
    Dim rs As ADODB.recordset
    
    On Error GoTo Trata_Erro

    'Dados do mail
    sSql = "SELECT * FROM " & gTabelaCorreio & " WHERE codigo=" & iCodMsg
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    
    'Anexos
    

    Exit Sub
Trata_Erro:
        
End Sub

Public Sub PreencheMensagem(sCodMensagem As String)
    
    Dim msg_lida As Integer
    Dim msg_lida_tooltip As String
    Dim sSql As String
    Dim rsMensagem As ADODB.recordset
    
    'Detalhes da mensagem
    sSql = "SELECT * FROM " & gTabelaCorreio & " WHERE codigo=" & sCodMensagem
    Set rsMensagem = New ADODB.recordset
    rsMensagem.CursorLocation = adUseServer
    rsMensagem.Open sSql, gConexao, adOpenStatic
    If rsMensagem.RecordCount = 0 Then
        Exit Sub
    Else
        If Not IsNull(rsMensagem!assunto) Then
            'EcAssunto = BG_CryptStringDecode(RsMensagem!assunto)
            EcAssunto = rsMensagem!assunto
        End If
        EcAssunto.Enabled = False
        ' EcTexto.TextRTF = BG_CryptStringDecode(RsMensagem!Texto)
        EcTexto.TextRTF = rsMensagem!texto
        EcTexto.Locked = True
        EcFonte.Enabled = False
        EcTamanho.Enabled = False
        ToolbarFormatacao.Enabled = False
    End If
    
    BtAdicionar.Enabled = False
    BtRetirar.Enabled = False
    BtRetirarTodos.Enabled = False
    EcListas.Enabled = False
    EcOptListas.Enabled = False
    EcOptUtilizadores.Enabled = False
    EcChkOcultarDest.Enabled = False
    BtEnviar.Enabled = False
    BtAnexo.Enabled = False
    If rsMensagem!flg_ocultar = 1 Then
        'Ocultar destinat�rios
        EcDestinatarios.ListItems.Clear
        EcDestinatarios.Enabled = False
        rsMensagem.Close
        Set rsMensagem = Nothing
    Else
        'preencher destinat�rios
        rsMensagem.Close
        Set rsMensagem = Nothing
        
        sSql = "SELECT * FROM " & gTabelaCorreioUtil & " WHERE cod_mensagem=" & sCodMensagem
        Set rsMensagem = New ADODB.recordset
        rsMensagem.CursorLocation = adUseServer
        rsMensagem.Open sSql, gConexao, adOpenStatic
        If rsMensagem.RecordCount > 0 Then
            Do While Not rsMensagem.EOF
            
                ' pferreira 2009.03.03
                ' Reformulado, para dar a informa��o de mensagem lida/n�o-lida
                ' -------------------------------------------------------------------------------------------
                If (BL_HandleNull(rsMensagem!msg_lida, Empty) = 0) Then: msg_lida = c_MESSAGE_NOT_READED: msg_lida_tooltip = "Mensagem n�o lida"
                If (BL_HandleNull(rsMensagem!msg_lida, Empty) = 1) Then: msg_lida = c_MESSAGE_READED: msg_lida_tooltip = "Mensagem lida"
                With EcDestinatarios.ListItems.Add(, "U-" & rsMensagem!cod_util, Empty, , msg_lida)
                    .ListSubItems.Add , , Empty, 2, msg_lida_tooltip
                    .ListSubItems.Add , , BL_PesquisaCodDes(gTabelaUtilizadores, gCampoCodUtilizador, gCampoNomeUtilizador, rsMensagem!cod_util, "DESC"), , msg_lida_tooltip
                End With
                ' -------------------------------------------------------------------------------------------
                rsMensagem.MoveNext
            Loop
        End If
        rsMensagem.Close
        Set rsMensagem = Nothing
    End If
    
    
    'verificar anexos
    EcLtVwAnexos.ListItems.Clear
    
    sSql = "SELECT * FROM " & gTabelaCorreioAnexos & " WHERE cod_mensagem=" & sCodMensagem
    Set rsMensagem = New ADODB.recordset
    rsMensagem.CursorLocation = adUseServer
    rsMensagem.Open sSql, gConexao, adOpenStatic
    If rsMensagem.RecordCount > 0 Then
        rsMensagem.MoveFirst
        Do While Not rsMensagem.EOF
            EcLtVwAnexos.ListItems.Add , gPathAnexos & sCodMensagem & "_" & rsMensagem!cod_anexo & "_" & rsMensagem!nome, rsMensagem!nome, 1
            rsMensagem.MoveNext
        Loop
    End If
    rsMensagem.Close
    Set rsMensagem = Nothing
    
    Exit Sub

Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro : " & Err.Number & " - " & Err.Description, vbCritical, "..."

End Sub

Private Sub TestaValoresDefeitoLetra()
    Dim sParam As String
    Dim i As Integer
    Dim iValor As Integer
    
    iValor = -1
    
    sParam = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "RelatFonte")
    For i = 0 To EcFonte.ListCount - 1
        If EcFonte.List(i) = sParam Then
            iValor = i
            Exit For
        ElseIf EcFonte.List(i) = "Tahoma" Then
            iValor = i
        End If
    Next i
    
    EcFonte.ListIndex = iValor
        
    iValor = -1
    
    sParam = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "RelatTamanho")
    For i = 0 To EcTamanho.ListCount - 1
        If EcTamanho.List(i) = sParam Then
            iValor = i
            Exit For
        ElseIf EcTamanho.List(i) = "12" Then
            iValor = i
        End If
    Next i
    
    EcTamanho.ListIndex = iValor
End Sub

Private Sub BtAdicionar_Click()
    
    Dim sList As String
    
    If EcListas.ListIndex <> mediComboValorNull Then
        On Error GoTo TrataErro
    
        Dim iTipo As Integer
        If EcOptUtilizadores.value = True Then
            ' sList = IsUserInList(EcListas.ItemData(EcListas.ListIndex))
            ' If (sList <> Empty) Then: BG_Mensagem mediMsgBox, "O utilizador encontra-se configurado na lista " & sList, vbInformation, "...": Exit Sub
            iTipo = 2
             
            ' pferreira 2009.03.03
            ' Reformulado, para dar a informa��o de mensagem lida/n�o-lida
            ' -------------------------------------------------------------------------------------------
            With EcDestinatarios.ListItems.Add(, "U-" & EcListas.ItemData(EcListas.ListIndex), Empty, , c_MESSAGE_NOT_READED)
                .ListSubItems.Add , , Empty, iTipo, "Mensagem n�o lida"
                .ListSubItems.Add , , EcListas.List(EcListas.ListIndex), , "Mensagem n�o lida"
            End With
            ' -------------------------------------------------------------------------------------------
            
        Else
            iTipo = 1
            
            ' pferreira 2009.03.03
            ' Reformulado, para dar a informa��o de mensagem lida/n�o-lida
            ' -------------------------------------------------------------------------------------------
            With EcDestinatarios.ListItems.Add(, "L-" & EcListas.ItemData(EcListas.ListIndex), Empty, , c_MESSAGE_NOT_READED)
                .ListSubItems.Add , , Empty, iTipo, "Mensagem n�o lida"
                .ListSubItems.Add , , EcListas.List(EcListas.ListIndex), , "Mensagem n�o lida"
            End With
            ' -------------------------------------------------------------------------------------------
            
        End If
    
    End If
    Exit Sub
TrataErro:
    If Err.Number = 35602 Then
        'item j� existe na lista
        BG_Mensagem mediMsgBox, "O item j� se encontra seleccionado", vbInformation, "..."
    Else
        BG_Mensagem mediMsgBox, "Erro: " & Err.Number & " - " & Err.Description, vbCritical, "..."
    End If
End Sub

Private Sub BtAnexo_Click()
    On Error GoTo TrataErro
    
    Me.CmdDlgPrint.FilterIndex = 1
    Me.CmdDlgPrint.CancelError = True
    Me.CmdDlgPrint.InitDir = gDirCliente & "\"
    Me.CmdDlgPrint.FileName = ""
    On Error Resume Next
    
    Me.CmdDlgPrint.DialogTitle = "Ficheiro"
    Me.CmdDlgPrint.ShowOpen
    
    If Me.CmdDlgPrint.FileTitle <> "" Then
        'Anexar ficheiro
        EcLtVwAnexos.ListItems.Add , Me.CmdDlgPrint.FileName, Me.CmdDlgPrint.FileTitle, 1
    End If
    Exit Sub
TrataErro:
   BG_Mensagem mediMsgBox, "Erro: " & Err.Number & " - " & Err.Description, vbCritical, "..."
End Sub

Private Sub BtEnviar_Click()
    Dim sSql As String
    Dim sCod() As String
    Dim lCodigoMsg As Long
    Dim i As Integer
    Dim cListaFinal As Collection
    Dim item As Variant
    
    If EcDestinatarios.ListItems.Count = 0 Then
        BG_Mensagem mediMsgBox, "Campo Obrigat�rio: Destinat�rio", vbInformation, "..."
        Exit Sub
    End If
    
    If EcAssunto.Text = "" Then
        BG_Mensagem mediMsgBox, "Campo Obrigat�rio: Assunto", vbInformation, "..."
        Exit Sub
    End If
    
    If Len(EcAssunto) > 50 Then
        BG_Mensagem mediMsgBox, "O Assunto n�o pode ter mais de 50 caracteres.", vbInformation, "..."
        Exit Sub
    End If
    
    If EcTexto.Text = "" Then
        gMsgResp = BG_Mensagem(mediMsgBox, "O texto da mensagem est� vazio." & vbCrLf & vbCrLf & "Deseja continuar?", vbQuestion + vbYesNo, "...")
        If gMsgResp = vbNo Then
            Exit Sub
        End If
    End If
    
    lCodigoMsg = BG_DaMAX(gTabelaCorreio, "codigo") + 1
    
    'Inserir a mensagem
    sSql = "INSERT INTO " & gTabelaCorreio & " (codigo,assunto,texto,flg_ocultar,user_cri,dt_cri,hr_cri) VALUES("
    sSql = sSql & lCodigoMsg & ","
    'sSql = sSql & BL_TrataStringParaBD(BG_CryptStringEncode(EcAssunto.text)) & ","
    sSql = sSql & BL_TrataStringParaBD(EcAssunto.Text) & ","
    'sSql = sSql & BL_TrataStringParaBD(BG_CryptStringEncode(EcTexto.TextRTF)) & ","
    sSql = sSql & BL_TrataStringParaBD(EcTexto.TextRTF) & ","
    sSql = sSql & EcChkOcultarDest.value & ","
    sSql = sSql & gCodUtilizador & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    If (gSQLError <> 0) Then: BG_RollbackTransaction: Unload Me
    
    'inserir os anexos
    If EcLtVwAnexos.ListItems.Count > 0 Then
        For i = 1 To EcLtVwAnexos.ListItems.Count
        
            sSql = "INSERT INTO " & gTabelaCorreioAnexos & " (cod_mensagem,cod_anexo,nome) VALUES("
            sSql = sSql & lCodigoMsg & ","
            sSql = sSql & i & ","
            sSql = sSql & BG_VfValor(EcLtVwAnexos.ListItems(i).Text, "'") & ")"
            
            On Error Resume Next
            FileCopy EcLtVwAnexos.ListItems.item(i).Key, gPathAnexos & lCodigoMsg & "_" & i & "_" & EcLtVwAnexos.ListItems.item(i).Text
            
            
            On Error GoTo TrataErro
            
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro
            If (gSQLError <> 0) Then: BG_RollbackTransaction: Unload Me
        Next i
    End If
    'Construir lista de destinatarios
    Set cListaFinal = ConstroiListaDestinatarios
    'Remover os destinatarios repetidos
    Set cListaFinal = CleanRepeated(cListaFinal)
    
    'Inserir os destinat�rios - Emanuel Sousa - 02.06.2009
    For Each item In cListaFinal
    
        sSql = "INSERT INTO " & gTabelaCorreioUtil & " (cod_mensagem,cod_util,cod_estado) VALUES("
        sSql = sSql & lCodigoMsg & ","
        sSql = sSql & item & ","
        sSql = sSql & "1)"
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
        If (gSQLError <> 0) Then: BG_RollbackTransaction: Unload Me
    Next
    
    Unload Me

    Exit Sub
TrataErro:
    BG_Mensagem mediMsgBox, "Erro: " & Err.Number & " - " & Err.Description, "..."
End Sub
'Emanuel Sousa - 02.06.2009
Private Function CleanRepeated(�collection� As Collection) As Collection
    
    Dim cleanCollection As Collection
    Dim item As Variant
    
    On Error GoTo ErrorHandler
    Set cleanCollection = New Collection
    Set CleanRepeated = New Collection
    For Each item In �collection�
        If (Not ContainsUser(cleanCollection, item)) Then: cleanCollection.Add item
    Next
    Set CleanRepeated = cleanCollection
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function
'Emanuel Sousa - 02.06.2009
Private Function ContainsUser(�collection� As Collection, �user� As Variant) As Boolean

    Dim item As Variant
    
    On Error GoTo ErrorHandler
    For Each item In �collection�
        If (item = �user�) Then: ContainsUser = True: Exit Function
    Next
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

'Emanuel Sousa - 02.06.2009
Private Function ConstroiListaDestinatarios() As Collection
    
    Dim cRes As Collection
    Dim cod As String
    Dim codUser As String
    Dim RsList As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    
    Set cRes = New Collection
    
    For i = 1 To EcDestinatarios.ListItems.Count
        cod = Mid$(EcDestinatarios.ListItems(i).Key, 3)
        
        'Caso seja um utilizador
        If (Mid$(EcDestinatarios.ListItems(i).Key, 1, 2) = "U-") Then
        
            'Adiciona o user � colec��o
            cRes.Add (cod)
            
        Else
        'Caso seja uma lista de utilizadores
                sql = "SELECT cod_lista, cod_membro FROM sb_cod_listas_membros WHERE cod_lista= " & cod
                    Set RsList = New ADODB.recordset
                    RsList.CursorLocation = adUseServer
                    RsList.CursorType = adOpenStatic
                    RsList.Open sql, gConexao
                    If RsList.RecordCount > 0 Then
                            RsList.MoveFirst
                            Do While Not RsList.EOF
                                codUser = RsList!cod_membro
                                
                                'Adiciona utilizador � Collection
                                cRes.Add (codUser)
                                
                                RsList.MoveNext
                            Loop
                    End If
                    RsList.Close
                    Set RsList = Nothing
        End If
    Next
    Set ConstroiListaDestinatarios = cRes
    
End Function

Private Sub BtImprimir_Click()

    gImprimirDestino = 0
    FuncaoImprimir
End Sub

Private Sub BtRetirar_Click()
    If EcDestinatarios.ListItems.Count > 0 Then
        EcDestinatarios.ListItems.Remove EcDestinatarios.SelectedItem.Index
    End If
End Sub

Private Sub BtRetirarTodos_Click()
    If EcDestinatarios.ListItems.Count > 0 Then
        EcDestinatarios.ListItems.Clear
    End If
End Sub

Sub EcFonte_Change()

    On Error Resume Next
    
    If bFonte = True Then EcTexto.SelFontName = EcFonte.Text
    
End Sub

Sub EcFonte_Click()

    On Error Resume Next
    
    If bFonte = True Then EcTexto.SelFontName = EcFonte.Text
    
End Sub


Private Sub EcLtVwAnexos_DblClick()
    If EcLtVwAnexos.ListItems.Count > 0 Then
        Dim sComando As String
        Dim sSeparador() As String
        Dim sExtensao As String
        Dim sFileName As String
        Dim aSplitFileName() As String
        
        On Error GoTo Trata_Erro
        
        If BtAnexo.Enabled = True Then
            Exit Sub
        End If
        
        'Copiar o ficheiro para um destino
        If Dir(EcLtVwAnexos.SelectedItem.Key) = "" Then
            BG_Mensagem mediMsgBox, "N�o � poss�vel localizar o anexo selecionado." & vbCrLf & "Pode ter sido apagado." & vbCrLf & "Por favor contacte o administrador da aplica��o.", vbCritical, "..."
            Exit Sub
        End If
        
        sSeparador = Split(EcLtVwAnexos.SelectedItem.Key, "\")
        sSeparador = Split(sSeparador(UBound(sSeparador)), ".")
        If UBound(sSeparador) > 0 Then
            sExtensao = sSeparador(1)
        End If
        
        aSplitFileName = Split(EcLtVwAnexos.SelectedItem.Key, "_")
        sFileName = aSplitFileName(UBound(aSplitFileName))
        
        Me.CmdDlgPrint.FilterIndex = 1
        Me.CmdDlgPrint.CancelError = True
        Me.CmdDlgPrint.InitDir = gDirCliente & "\"
        Me.CmdDlgPrint.FileName = sFileName
        On Error GoTo Trata_Erro
        
        Me.CmdDlgPrint.DialogTitle = "Guardar anexo"
        Me.CmdDlgPrint.ShowSave
        
        If Me.CmdDlgPrint.FileTitle <> "" Then
              FileCopy EcLtVwAnexos.SelectedItem.Key, Me.CmdDlgPrint.FileName & "." & sExtensao
              If Dir(Me.CmdDlgPrint.FileName & "." & sExtensao) = "" Then
                    BG_Mensagem mediMsgBox, "N�o foi poss�vel guardar o anexo.", vbCritical, "..."
              End If
        End If
    End If
    Exit Sub
Trata_Erro:
    If Err.Number = 32755 Then
        'cancelou o gravar local
    Else
       BG_Mensagem mediMsgBox, "Erro: " & Err.Number & " - " & Err.Description, vbCritical, "..."
    End If
End Sub


Private Sub EcLtVwAnexos_KeyDown(KeyCode As Integer, Shift As Integer)
    If EcLtVwAnexos.ListItems.Count > 0 Then
        If KeyCode = 46 And Shift = 0 Then
            If BtAnexo.Enabled = True Then
                EcLtVwAnexos.ListItems.Remove EcLtVwAnexos.SelectedItem.Index
            End If
        End If
    End If
End Sub


Sub EcTamanho_Change()

    On Error Resume Next
    
    If bTamanho = True Then EcTexto.SelFontSize = EcTamanho.Text
    
End Sub

Sub EcTamanho_Click()

    On Error Resume Next
    
    If bTamanho = True Then EcTexto.SelFontSize = EcTamanho.Text
    
End Sub


Private Sub EcOptListas_Click()
     If EcOptListas.value = True Then
        Dim sSql As String
            
        sSql = "SELECT * FROM " & gTabelaCodListas
        BG_PreencheComboBD_ADO sSql, "codigo", "descricao", EcListas, mediAscComboDesignacao
    End If
End Sub

Private Sub EcOptUtilizadores_Click()
     If EcOptUtilizadores.value = True Then
        Dim sSql As String
        
        sSql = "SELECT * FROM " & gTabelaUtilizadores & " WHERE flg_removido=0 or flg_removido is null"
        BG_PreencheComboBD_ADO sSql, gCampoCodUtilizador, gCampoNomeUtilizador, EcListas, mediAscComboDesignacao
           
    End If
End Sub

Sub EcTexto_GotFocus()
    Dim Control As Control
    
    Set CampoActivo = Me.ActiveControl
    
    ' Ignore errors for controls without the TabStop property.
    On Error Resume Next
    ' Switch off the change of focus when pressing TAB.
    For Each Control In Controls
        Control.TabStop = False
    Next Control
    
    'EcFonte.text = "Arial"
        
End Sub

Sub EcTexto_SelChange()

        
    'If IsNull(EcTexto.SelFontName) Then
    '    bFonte = False
    '    EcFonte.text = ""
    'Else
    '    bFonte = True
    '    EcFonte.text = EcTexto.SelFontName
    'End If
    
    If IsNull(EcTexto.SelFontSize) Then
        bTamanho = False
        EcTamanho.Text = ""
    Else
        bTamanho = True
        EcTamanho.Text = EcTexto.SelFontSize
    End If
    
    If EcTexto.SelBold Then
        ToolbarFormatacao.Buttons.item("bold").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("bold").value = tbrUnpressed
    End If
    
    If EcTexto.SelItalic Then
        ToolbarFormatacao.Buttons.item("italic").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("italic").value = tbrUnpressed
    End If
    
    If EcTexto.SelUnderline Then
        ToolbarFormatacao.Buttons.item("underline").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("underline").value = tbrUnpressed
    End If
    
    If IsNull(EcTexto.SelAlignment) Then
        ToolbarFormatacao.Buttons.item("left").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("center").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("right").value = tbrUnpressed
    ElseIf EcTexto.SelAlignment = rtfLeft Then
        ToolbarFormatacao.Buttons.item("left").value = tbrPressed
    ElseIf EcTexto.SelAlignment = rtfCenter Then
        ToolbarFormatacao.Buttons.item("center").value = tbrPressed
    ElseIf EcTexto.SelAlignment = rtfRight Then
        ToolbarFormatacao.Buttons.item("right").value = tbrPressed
    End If
    
End Sub



Sub EcTexto_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub




Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Mensagem"

    Me.Width = 11145
    Me.Height = 5790
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
End Sub

Sub EventoActivate()
    Set gFormActivo = Me
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    Set gFormActivo = FormCorreio
    FormCorreio.BtRefresh_Click
End Sub


Private Sub PreencheValoresDefeito()
    
    Dim i As Integer
    Dim sSql As String
    
    ConfiguraListView
         
    EcChkOcultarDest.value = vbUnchecked
    
    For i = 0 To Screen.FontCount - 1
        EcFonte.AddItem Screen.Fonts(i)
    Next
    

    For i = 8 To 12
        EcTamanho.AddItem i
    Next i
    
    For i = 14 To 28 Step 2
        EcTamanho.AddItem i
    Next i
    
    EcTamanho.AddItem 36
    EcTamanho.AddItem 48
    EcTamanho.AddItem 72
    
    'BL_RichText_Inicializacao EcTexto
    
     EcListas.Clear
    
    sSql = "SELECT * FROM " & gTabelaUtilizadores & " WHERE flg_removido=0 or flg_removido is null"
    BG_PreencheComboBD_ADO sSql, gCampoCodUtilizador, gCampoNomeUtilizador, EcListas, mediAscComboDesignacao
    
    
    TestaValoresDefeitoLetra
End Sub


Private Sub ToolbarFormatacao_ButtonClick(ByVal Button As MSComctlLib.Button)
     Select Case Button.Key
        Case "bold"
            If Button.value = tbrUnpressed Then
                If Not IsNull(EcTexto.SelBold) Then EcTexto.SelBold = False
            Else
                EcTexto.SelBold = True
            End If
        Case "italic"
            If Button.value = tbrUnpressed Then
                If Not IsNull(EcTexto.SelItalic) Then EcTexto.SelItalic = False
            Else
                EcTexto.SelItalic = True
            End If
        Case "underline"
            If Button.value = tbrUnpressed Then
                If Not IsNull(EcTexto.SelUnderline) Then EcTexto.SelUnderline = False
            Else
                EcTexto.SelUnderline = True
            End If
        Case "left"
            If Button.value <> tbrUnpressed Then EcTexto.SelAlignment = rtfLeft
        Case "center"
            If Button.value <> tbrUnpressed Then EcTexto.SelAlignment = rtfCenter
        Case "right"
            If Button.value <> tbrUnpressed Then EcTexto.SelAlignment = rtfRight
        Case "corrector"
            If Me.ActiveControl.Name <> "EcAssunto" And Me.ActiveControl.Name <> "EcTexto" Then
                Exit Sub
            End If
            Dim Corrector As Corrector
            
            Set Corrector = New Corrector
            Corrector.InicializaCorrector gDirServidor & "\bin" & "\Corrector.cor"
            If Corrector.LocalizacaoCorrector <> "" Then
                Corrector.VerificaTextoObjecto Me.ActiveControl
            End If
            Set Corrector = Nothing
            
            DoEvents
            Me.SetFocus
        Case Else
            'Nada
    End Select
End Sub


' pferreira 2007.08.27
' Paint a list view with colored bars.
Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SetListViewColor' in form FormCorreioMensagem (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2007.11.14
Private Function IsUserInList(�lUserCode� As Long) As String

    Dim sSql As String
    Dim oItem As Variant
    Dim rsUser As ADODB.recordset

    On Error GoTo ErrorHandler
    For Each oItem In EcDestinatarios.ListItems
        If (oItem.Key Like "L-*") Then
            sSql = "select descricao from " & gTabelaCodListas & "," & gTabelaCodListasMembros & _
                   " where codigo = cod_lista and cod_membro = " & �lUserCode�
            Set rsUser = New ADODB.recordset
            rsUser.CursorLocation = adUseServer
            rsUser.CursorType = adOpenStatic
            rsUser.Open sSql, gConexao
            If (rsUser.RecordCount > 0) Then: IsUserInList = BL_HandleNull(rsUser!descricao, ""): Exit Function
            If (rsUser.state = adStateOpen) Then: rsUser.Close
        End If
    Next
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'IsUserInList' in form FormCorreioMensagem (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    IsUserInList = ""
    Exit Function

End Function

Public Sub FuncaoImprimir()
    
    ImprimeMensagem
    
End Sub

Private Sub ImprimeMensagem()
    
    BG_Mensagem mediMsgStatus, ("A imprimir regisos..."), mediMsgBeep + mediMsgPermanece
    BL_MudaCursorRato mediMP_Espera, Me
    PreencheTabelas
    DoEvents
    Report.Connect = "DSN=" & gDSN
    Report.ReportFileName = gDirServidor & "\bin\Mensagem_Correio.rpt"
    Report.SubreportToChange = "SubReportAnexos"
    Report.SubreportToChange = "SubReportDestinatarios"
    Report.Destination = gImprimirDestino
    Report.Action = 1
    Report.Reset
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    BL_MudaCursorRato mediMP_Activo, Me

End Sub

Private Sub PreencheTabelas()

    Dim sSqlDestinatarios As String
    Dim sSqlMensagem As String
    Dim sSqlAnexos As String
    Dim oItem As Variant
    Dim rsMensagem As ADODB.recordset
    
    On Error GoTo ErrorHandler
    BG_ExecutaQuery_ADO "delete from " & gTabelaCrMensagem
    BG_Trata_BDErro
    BG_ExecutaQuery_ADO "delete from " & gTabelaCrDestinatarios
    BG_Trata_BDErro
    BG_ExecutaQuery_ADO "delete from " & gTabelaCrAnexos
    BG_Trata_BDErro
    For Each oItem In EcDestinatarios.ListItems
        sSqlDestinatarios = "insert into " & gTabelaCrDestinatarios & " (destinatario, user_cri, dt_cri,hr_cri,cod_local) values (" & _
                            BL_TrataStringParaBD(oItem.SubItems(2)) & "," & gCodUtilizador & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & gCodLocal & ")"
        BG_ExecutaQuery_ADO sSqlDestinatarios
        BG_Trata_BDErro
    Next
    For Each oItem In EcLtVwAnexos.ListItems
        sSqlAnexos = "insert into " & gTabelaCrAnexos & " (anexo, user_cri, dt_cri,hr_cri,cod_local) values (" & _
                    BL_TrataStringParaBD(oItem.Text) & "," & gCodUtilizador & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & gCodLocal & ")"
        BG_ExecutaQuery_ADO sSqlAnexos
        BG_Trata_BDErro
    Next
    sSqlMensagem = "insert into " & gTabelaCrMensagem & " (mensagem, assunto,user_cri, dt_cri,hr_cri,cod_local) values (" & _
                   BL_TrataStringParaBD(EcTexto.Text) & "," & BL_TrataStringParaBD(EcAssunto.Text) & "," & gCodUtilizador & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & gCodLocal & ")"
    BG_ExecutaQuery_ADO sSqlMensagem
    BG_Trata_BDErro
    
ErrorHandler:

End Sub

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormAnaInterferencias 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAnaInterferencias"
   ClientHeight    =   6825
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8235
   Icon            =   "FormAnaInterferencias.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6825
   ScaleWidth      =   8235
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcFraseBlq 
      Height          =   1095
      Left            =   240
      TabIndex        =   19
      Top             =   1080
      Width           =   7815
   End
   Begin VB.TextBox EcFraseObs 
      Height          =   855
      Left            =   240
      TabIndex        =   18
      Top             =   2520
      Width           =   7815
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Left            =   120
      TabIndex        =   5
      Top             =   5880
      Width           =   8055
      Begin VB.Label LaHrCri 
         Height          =   255
         Left            =   6840
         TabIndex        =   11
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Hora Cria��o:"
         Height          =   255
         Index           =   3
         Left            =   5640
         TabIndex        =   10
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDtCri 
         Height          =   255
         Left            =   1320
         TabIndex        =   9
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LbUserCri 
         Height          =   255
         Left            =   1320
         TabIndex        =   8
         Top             =   120
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Data Cria��o:"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Utilizador:"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   240
      TabIndex        =   2
      Top             =   360
      Width           =   615
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   960
      TabIndex        =   1
      Top             =   360
      Width           =   7095
   End
   Begin MSComctlLib.ListView EcList1 
      Height          =   1455
      Left            =   240
      TabIndex        =   0
      Top             =   3360
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   2566
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label Label7 
      Caption         =   "%3 - Evento da interfer�ncia"
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   5400
      Width           =   7935
   End
   Begin VB.Label Label6 
      Caption         =   "%2 - Substituido pelo nome da interfer�ncia"
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   5160
      Width           =   7935
   End
   Begin VB.Label Label5 
      Caption         =   "%1 - Substituido pelo nome do teste"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   4920
      Width           =   7935
   End
   Begin VB.Label Label4 
      Caption         =   "Frase para Observa��o"
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   2160
      Width           =   2175
   End
   Begin VB.Label Label3 
      Caption         =   "Frase para Bloqueio"
      Height          =   255
      Left            =   240
      TabIndex        =   13
      Top             =   840
      Width           =   2175
   End
   Begin VB.Label LaUser 
      Height          =   255
      Left            =   360
      TabIndex        =   12
      Top             =   6720
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   0
      Left            =   960
      TabIndex        =   4
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormAnaInterferencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'======================

' FERNANDO GONCALVES
' 10-04-2006
' CHVNG

'======================

Option Explicit

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String
Dim CriterioTabelaF As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'
Dim rs As New ADODB.recordset






Private Sub EcList1_Click()
    Dim i As Long
    If rs.state = 1 And EcList1.ListItems.Count > 0 Then
        rs.MoveFirst
        i = EcList1.SelectedItem
        While Not rs!cod_Interferencia = i
            rs.MoveNext
        Wend
        LimpaCampos
        PreencheCampos
    End If
End Sub


Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Codifica��o de Interfer�ncias."
    Me.left = 540
    Me.top = 450
    Me.Width = 8325
    Me.Height = 7305 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    NomeTabela = "sl_interferencias"
    Set CampoDeFocus = EcDescricao
    
    NumCampos = 7
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_interferencia"
    CamposBD(1) = "descr_interferencia"
    CamposBD(2) = "dt_cri"
    CamposBD(3) = "hr_cri"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "frase_bloq"
    CamposBD(6) = "frase_obs"
        
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = LaDtCri
    Set CamposEc(3) = LaHrCri
    Set CamposEc(4) = LaUser
    Set CamposEc(5) = EcFraseBlq
    Set CamposEc(6) = EcFraseObs
    
    ' Texto para a mensagem nos campos obrigat�rios
    TextoCamposObrigatorios(0) = "C�digo Obrigat�rio."
    
    ChaveBD = "cod_interferencia"
    Set ChaveEc = EcCodigo
        
End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAnaInterferencias = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    BG_LimpaCampo_Todos CamposEc
    LbUserCri.caption = ""
    EcList1.ListItems.Clear
    
End Sub

Sub DefTipoCampos()
    LaDtCri.Tag = adDate
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub
Sub PreencheValoresDefeito()
    ConfiguraListView
    EcList1.FullRowSelect = True
End Sub
Sub PreencheCampos()
    Dim i As Integer
    
    Me.SetFocus
    
    BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    
    MarcaInicial = rs.Bookmark
    
    PreencheUserCri
    PreencheLista
    If Trim(EcCodigo) <> "" Then
        For i = 1 To EcList1.ListItems.Count
            If EcCodigo = EcList1.ListItems(i).Text Then
                EcList1.SelectedItem = EcList1.ListItems(i)
            End If
        Next
    End If
        
End Sub


Sub FuncaoLimpar()
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
End Sub
Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If Trim(EcCodigo) = "" Then
    Else
    End If
              
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
        
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    CriterioTabela = CriterioTabela & " ORDER BY cod_interferencia "
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        rs.MoveFirst
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
End Sub


' CONFIGURA OS CABE�ALHOS DAS LISTS VIEWS
Private Sub ConfiguraListView()
    
    EcList1.View = lvwReport
    EcList1.ColumnHeaders.Add , , "C�digo", 1000
    EcList1.ColumnHeaders.Add , , "Descricao", EcList1.Width - 1100
    
End Sub


'PREENCHE A LISTVIEW COM TODAS AS INTERFERENCIAS RESULTANTES DA PROCURA
Private Sub PreencheLista()
    rs.MoveFirst
    While Not rs.EOF
        EcList1.ListItems.Add , "I" & CStr(rs!cod_Interferencia), rs!cod_Interferencia
        EcList1.ListItems(EcList1.ListItems.Count).SubItems(1) = rs!descr_interferencia
        rs.MoveNext
    Wend
    rs.Bookmark = MarcaInicial
End Sub


' PREENCHE NOME DO UTILIZADOR SEGUNDO O CODIGO QUE ESTA NA LABEL LaUser
Private Sub PreencheUserCri()
    Dim sSql As String
    Dim rsUtil As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador = " & LaUser
    rsUtil.CursorType = adOpenStatic
    rsUtil.CursorLocation = adUseServer
    rsUtil.Open CriterioTabela, gConexao
    If rsUtil.RecordCount = 1 Then
        LbUserCri = BL_HandleNull(rsUtil!nome, "")
    End If
    rsUtil.Close
    Set rsUtil = Nothing
    Exit Sub

TrataErro:
    BG_LogFile_Erros "FormAnaInterferencias -> PreencheUserCri: " & Err.Description
    Exit Sub
    Resume Next
End Sub
Sub FuncaoInserir()
    Dim iRes As Integer
    Dim SQLQuery As String
    Dim rsInsAux As ADODB.recordset
    Dim rsAux As ADODB.recordset
    Dim SQLAux As String


    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
            
        SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & ChaveBD & "='" & EcCodigo & "'"
        Set rsInsAux = New ADODB.recordset
        rsInsAux.CursorLocation = adUseServer
        rsInsAux.CursorType = adOpenStatic
        rsInsAux.Open SQLQuery, gConexao
    
        If rsInsAux.RecordCount = (-1) Then
            BG_Mensagem mediMsgBox, "Erro a verificar c�digo!", vbExclamation, "Inserir"
        ElseIf rsInsAux.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
        Else
            BL_InicioProcessamento Me, "A inserir registo."
            iRes = ValidaCamposEc
        
            If iRes = True Then
                BD_Insert
            End If
            BL_FimProcessamento Me
        End If
        
        rsInsAux.Close
        Set rsInsAux = Nothing
    End If
End Sub
Sub BD_Insert()
    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLAux As String
    Dim SQLAux2 As String
    Dim MarcaLocal As Variant
    Dim i As Integer

    On Error GoTo TrataErro
    
    LaDtCri = Bg_DaData_ADO
    LaHrCri = Bg_DaHora_ADO
    LaUser = gCodUtilizador
    PreencheUserCri
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro

    
    LimpaCampos
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "FormAnaInterferencias -> BD_INSERT: " & Err.Description
    Exit Sub
    Resume Next
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub


Sub BD_Update()
    Dim condicao As String
    Dim SQLQuery As String
    Dim Codigo As String
    On Error GoTo TrataErro
    
    LaDtCri = Bg_DaData_ADO
    LaUser = gCodUtilizador
    PreencheUserCri
    
    condicao = " cod_interferencia = " & EcCodigo
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro

    Codigo = EcCodigo
    LimpaCampos
    EcCodigo = Codigo
    rs.Requery
    PreencheCampos
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "FormAnaInterferencias -> BD_UPDATE: " & Err.Description
    Exit Sub
    Resume Next
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant

    On Error GoTo TrataErro
    
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro
    
    SQLQuery = "DELETE FROM sl_rel_interferencias WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If

    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If

    If rs.EOF Then rs.MovePrevious

    LimpaCampos
    PreencheCampos
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "FormAnaInterferencias -> BD_DELETE: " & Err.Description
    Exit Sub
    Resume Next

End Sub




VERSION 5.00
Begin VB.Form FormGHNovasAnalises 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFacturacaoNovasAnalises"
   ClientHeight    =   7725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6975
   Icon            =   "FormGHNovasAnalises.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7725
   ScaleWidth      =   6975
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "N�o facturar a r�brica para "
      Height          =   2480
      Left            =   0
      TabIndex        =   29
      Top             =   8640
      Width           =   6735
      Begin VB.ListBox ListaEFR 
         Height          =   1635
         Left            =   240
         Style           =   1  'Checkbox
         TabIndex        =   32
         Top             =   315
         Width           =   6015
      End
      Begin VB.CommandButton ButSelTudo 
         Caption         =   "Todas"
         Height          =   315
         Left            =   4200
         TabIndex        =   31
         Top             =   2040
         Width           =   975
      End
      Begin VB.CommandButton ButSelNada 
         Caption         =   "Nenhuma"
         Height          =   315
         Left            =   5280
         TabIndex        =   30
         Top             =   2040
         Width           =   975
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "R�brica "
      Height          =   5775
      Left            =   120
      TabIndex        =   13
      Top             =   0
      Width           =   6735
      Begin VB.TextBox EcNumMedioMarcadores 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5880
         TabIndex        =   49
         Top             =   5400
         Width           =   735
      End
      Begin VB.TextBox EcNumMinMarcadores 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3240
         TabIndex        =   47
         Top             =   5400
         Width           =   735
      End
      Begin VB.CheckBox CkMarcadores 
         Caption         =   "An�lise Sujeita a Regra de Marcadores"
         Height          =   195
         Left            =   240
         TabIndex        =   45
         Top             =   5160
         Width           =   3135
      End
      Begin VB.ListBox EcLocais 
         Appearance      =   0  'Flat
         Height          =   705
         Left            =   1080
         Style           =   1  'Checkbox
         TabIndex        =   43
         Top             =   2280
         Width           =   5175
      End
      Begin VB.TextBox EcCentroCusto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5880
         TabIndex        =   41
         Top             =   4440
         Width           =   735
      End
      Begin VB.TextBox EcQtd 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5880
         TabIndex        =   39
         Top             =   4080
         Width           =   735
      End
      Begin VB.TextBox EcPercent 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3240
         TabIndex        =   37
         Top             =   4800
         Width           =   735
      End
      Begin VB.TextBox EcCodAnaFacturar 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3240
         TabIndex        =   36
         Top             =   4440
         Width           =   735
      End
      Begin VB.CheckBox CkFlgfacturarAna 
         Caption         =   "Pergunta se pretende facturar an�lise"
         Height          =   195
         Left            =   240
         TabIndex        =   35
         Top             =   4440
         Width           =   3015
      End
      Begin VB.CheckBox CkFlgRegra 
         Caption         =   "S� factura se n�o marcada a an�lise"
         Height          =   195
         Left            =   240
         TabIndex        =   34
         Top             =   4080
         Width           =   2895
      End
      Begin VB.TextBox EcCodAnaRegra 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3240
         TabIndex        =   33
         Top             =   4080
         Width           =   735
      End
      Begin VB.CheckBox CkContaMembros 
         Caption         =   "Envia Membros para FACTUS"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   3705
         Width           =   3855
      End
      Begin VB.CommandButton BtListaAnaNaoMapeadas 
         BackColor       =   &H80000004&
         Height          =   525
         Left            =   6000
         Picture         =   "FormGHNovasAnalises.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Pr�-Visualizar an�lises n�o mapeadas"
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6240
         Picture         =   "FormGHNovasAnalises.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   26
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1800
         Width           =   375
      End
      Begin VB.TextBox EcDescrEfr 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   24
         Top             =   1800
         Width           =   4095
      End
      Begin VB.TextBox EcCodEfr 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   23
         Top             =   1800
         Width           =   1095
      End
      Begin VB.TextBox EcDescrGH 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         TabIndex        =   22
         Top             =   840
         Width           =   4095
      End
      Begin VB.TextBox EcCodGH 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   21
         Top             =   840
         Width           =   1095
      End
      Begin VB.CheckBox CkSoFacturacao 
         Caption         =   "Rubrica usada s� na factura��o (N�o entra na passagem da GH para o SISLAB)"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   3360
         Width           =   6015
      End
      Begin VB.TextBox EcSeqAna 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   16
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcCodAna 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   15
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox EcDescrAna 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         TabIndex        =   14
         Top             =   1320
         Width           =   4095
      End
      Begin VB.Label Label6 
         Caption         =   "N� M�dio Marcadores"
         Height          =   255
         Left            =   4320
         TabIndex        =   48
         Top             =   5400
         Width           =   1575
      End
      Begin VB.Label Label5 
         Caption         =   "N� Min.Marcadores"
         Height          =   255
         Left            =   1800
         TabIndex        =   46
         Top             =   5400
         Width           =   1455
      End
      Begin VB.Label LbLocal 
         AutoSize        =   -1  'True
         Caption         =   "Locais"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   44
         Top             =   2325
         Width           =   465
      End
      Begin VB.Label Label4 
         Caption         =   "Centro Custo"
         Height          =   255
         Index           =   1
         Left            =   4680
         TabIndex        =   42
         Top             =   4440
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Quantidade"
         Height          =   255
         Index           =   0
         Left            =   4680
         TabIndex        =   40
         Top             =   4080
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Valor do desconto"
         Height          =   255
         Left            =   480
         TabIndex        =   38
         Top             =   4800
         Width           =   2535
      End
      Begin VB.Label Label2 
         Caption         =   "Entidade"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   25
         Top             =   1800
         Width           =   855
      End
      Begin VB.Label LaSistemaFacturacao 
         Caption         =   "GH"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Factura��o"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Sislab"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   17
         Top             =   1320
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pesquisa R�pida "
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   5880
      Width           =   6735
      Begin VB.CommandButton BtPesqRapP 
         Caption         =   "..."
         Height          =   320
         Left            =   5880
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapC 
         Caption         =   "..."
         Height          =   320
         Left            =   5880
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   840
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapS 
         Caption         =   "..."
         Height          =   320
         Left            =   5880
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcDescrAnaP 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2280
         TabIndex        =   9
         Top             =   1320
         Width           =   3615
      End
      Begin VB.TextBox EcDescrAnaC 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2280
         TabIndex        =   8
         Top             =   840
         Width           =   3615
      End
      Begin VB.TextBox EcCodAnaP 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1440
         TabIndex        =   7
         Top             =   1320
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaC 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1440
         TabIndex        =   6
         Top             =   840
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaS 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox EcDescrAnaS 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2280
         TabIndex        =   4
         Top             =   360
         Width           =   3615
      End
      Begin VB.OptionButton OptPerfis 
         Caption         =   "Perfis"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   1320
         Width           =   1095
      End
      Begin VB.OptionButton OptComplexas 
         Caption         =   "Complexas"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   1095
      End
      Begin VB.OptionButton OptSimples 
         Caption         =   "Simples"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   855
      End
   End
End
Attribute VB_Name = "FormGHNovasAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim nomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub BtListaAnaNaoMapeadas_Click()
    Dim continua As Boolean
    Dim sql As String
    
    'Printer Common Dialog
    continua = BL_IniciaReport("ListaAnaNaoMapeadas", "Listagem de An�lises N�o Mapeadas", crptToWindow)
    If continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me

    BG_BeginTransaction
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    sql = "DELETE FROM sl_cr_anafacturacao where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql
    
    sql = "insert into sl_cr_anafacturacao(cod_ana,descr_ana,cod_rubr,flg_regra,cod_ana_regra,factura_membros,cod_efr,nome_computador) " & _
            " select cod_ana_s cod_ana, descr_ana_s descr_ana,cod_ana_gh,flg_regra,cod_ana_regra, factura_membros,cod_efr,'" & BG_SYS_GetComputerName & "'" & _
            " from sl_ana_facturacao f, sl_ana_s s " & _
            " where s.cod_ana_s = f.cod_ana(+) and flg_facturar = 1 " & _
            " Union " & _
            " select cod_ana_c cod_ana, descr_ana_c descr_ana,cod_ana_gh,flg_regra,cod_ana_regra, factura_membros,cod_efr,'" & BG_SYS_GetComputerName & "'" & _
            " from sl_ana_facturacao f, sl_ana_c c " & _
            " where c.cod_ana_c = f.cod_ana(+) and flg_facturar = 1 " & _
            " Union " & _
            " select cod_perfis cod_ana, descr_perfis descr_ana,cod_ana_gh,flg_regra,cod_ana_regra, factura_membros,cod_efr,'" & BG_SYS_GetComputerName & "'" & _
            " from sl_ana_facturacao f, sl_perfis p " & _
            " where p.cod_perfis = f.cod_ana(+) and flg_facturar = 1 " & _
            " order by descr_ana "
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
    BG_CommitTransaction
    
    Report.SelectionFormula = "{sl_cr_anafacturacao.nome_computador} = '" & BG_SYS_GetComputerName & "'"

    
    Report.Connect = "DSN=" & gDSN
    
    Call BL_ExecutaReport

    BL_MudaCursorRato mediMP_Activo, Me

End Sub

Private Sub BtPesqRapC_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_c"
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_c"
    CampoPesquisa = "descr_ana_c"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaC.text = Resultados(2)
            EcCodAnaC.text = Resultados(1)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaC.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises complexas", vbExclamation, "ATEN��O"
        EcCodAnaC.SetFocus
    End If

End Sub

Private Sub BtPesqRapP_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_perfis"
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis"
    CampoPesquisa = "descr_perfis"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaP.text = Resultados(1)
            EcDescrAnaP.text = Resultados(2)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaP.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem perfis", vbExclamation, "ATEN��O"
        EcCodAnaP.SetFocus
    End If

End Sub

Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.text = Resultados(2)
            EcCodAnaS.text = Resultados(1)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If

End Sub


Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub ButSelNada_Click()
   
    On Error Resume Next
    
    Dim i As Integer
    
    Me.ListaEFR.Visible = False
    For i = Me.ListaEFR.ListCount To 0 Step -1
        Me.ListaEFR.Selected(i) = False
    Next
    Me.ListaEFR.ListIndex = 0
    Me.ListaEFR.Visible = True

End Sub

Private Sub ButSelTudo_Click()
    
    On Error Resume Next
    
    Dim i As Integer
    
    Me.ListaEFR.Visible = False
    For i = Me.ListaEFR.ListCount To 0 Step -1
        Me.ListaEFR.Selected(i) = True
    Next
    Me.ListaEFR.ListIndex = 0
    Me.ListaEFR.Visible = True

End Sub



Private Sub CkMarcadores_Click()
    If CkMarcadores.Value = vbChecked Then
        EcNumMedioMarcadores.Enabled = True
        EcNumMinMarcadores.Enabled = True
    Else
        EcNumMedioMarcadores.Enabled = False
        EcNumMinMarcadores.Enabled = False
        EcNumMedioMarcadores.text = ""
        EcNumMinMarcadores.text = ""
    End If
End Sub

Private Sub EcCodana_Validate(Cancel As Boolean)
    
    Dim descrAna As String
    
    If Trim(EcCodAna.text) <> "" Then
        EcCodAna.text = UCase(EcCodAna.text)
        EcDescrAna = ValidaAnalise(EcCodAna.text)
        If Trim(EcDescrAna.text) = "" Then
            Cancel = True
            SendKeys ("{HOME}+{END}")
        End If
    End If

End Sub

Private Sub EcCodAnaC_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    If EcCodAnaC.text <> "" Then
        sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c=" & BL_TrataStringParaBD(EcCodAnaC.text)
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaC.text = BL_HandleNull(rsAna!Descr_Ana_C, "")
            EcCodAna.text = EcCodAnaC.text
            EcDescrAna.text = EcDescrAnaC.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaC.text = ""
            EcCodAnaC.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
End Sub

Private Sub EcCodAnaP_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    If EcCodAnaP.text <> "" Then
        sql = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis=" & BL_TrataStringParaBD(EcCodAnaP.text)
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaP.text = BL_HandleNull(rsAna!descr_perfis, "")
            EcCodAna.text = EcCodAnaP.text
            EcDescrAna.text = EcDescrAnaP.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaP.text = ""
            EcCodAnaP.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
End Sub



Private Sub EcCodAnaS_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    If EcCodAnaS.text <> "" Then
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(EcCodAnaS.text)
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaS.text = BL_HandleNull(rsAna!descr_ana_s, "")
            EcCodAna.text = EcCodAnaS.text
            EcDescrAna.text = EcDescrAnaS.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaS.text = ""
            EcCodAnaS.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
End Sub

Private Sub EcCodGH_Validate(Cancel As Boolean)
    
    Dim sql As String
    Dim RsDescrGH As ADODB.recordset
    Dim iBD_Aberta As Long
    If HIS.nome = cFACTURACAO_NOVAHIS Then
        Exit Sub
    End If
    If Trim(EcCodGH.text) <> "" Then
        iBD_Aberta = BL_Abre_Conexao_Secundaria(gOracle)
    
        If iBD_Aberta = 0 Then
            BL_FimProcessamento Me
            Exit Sub
        End If
        
        sql = "SELECT descr_rubr FROM fa_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(EcCodGH.text)
        sql = sql & " AND trim(user_rem) IS NULL "
        Set RsDescrGH = New ADODB.recordset
        RsDescrGH.CursorLocation = adUseClient
        RsDescrGH.CursorType = adOpenStatic
        RsDescrGH.Open sql, gConexaoSecundaria
        If RsDescrGH.RecordCount > 0 Then
            EcDescrGH.text = BL_HandleNull(RsDescrGH!descr_rubr, "")
        Else
            If gConfirmaRubrica = mediSim Then
                EcCodGH = ""
                gMsgMsg = "A r�brica introduzida n�o existe!"
                BG_Mensagem mediMsgBox, gMsgMsg, vbOKOnly, gMsgTitulo
                Exit Sub
            End If
        End If
        RsDescrGH.Close
        Set RsDescrGH = Nothing
        BL_Fecha_Conexao_Secundaria
    End If

End Sub

Private Sub EcSeqAna_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcSeqAna)

End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate
    
End Sub

Sub Form_Unload(Cancel As Integer)

    EventoUnload
    
End Sub

Sub Inicializacoes()
    
    Me.caption = " Cria��o de C�digo de R�brica para a Factura��o - GH"
    
    Me.Left = 400
    Me.Top = 20
    Me.Width = 7065
    Me.Height = 8160 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    nomeTabela = "sl_ana_facturacao"
    Set CampoDeFocus = EcSeqAna
    
    NumCampos = 16
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_ana"
    CamposBD(1) = "cod_ana"
    CamposBD(2) = "cod_ana_gh"
    CamposBD(3) = "flg_so_facturacao"
    CamposBD(4) = "cod_efr"
    CamposBD(5) = "flg_conta_membros"
    CamposBD(6) = "cod_ana_regra"
    CamposBD(7) = "flg_regra"
    CamposBD(8) = "cod_ana_Facturar"
    CamposBD(9) = "flg_Ana_Facturar"
    CamposBD(10) = "perc_Facturar"
    CamposBD(11) = "qtd"
    CamposBD(12) = "centro_custo"
    CamposBD(13) = "flg_marcador"
    CamposBD(14) = "min_marcadores"
    CamposBD(15) = "media_marcadores"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeqAna
    Set CamposEc(1) = EcCodAna
    Set CamposEc(2) = EcCodGH
    Set CamposEc(3) = CkSoFacturacao
    Set CamposEc(4) = EcCodEFR
    Set CamposEc(5) = CkContaMembros
    Set CamposEc(6) = EcCodAnaRegra
    Set CamposEc(7) = CkFlgRegra
    Set CamposEc(8) = EcCodAnaFacturar
    Set CamposEc(9) = CkFlgfacturarAna
    Set CamposEc(10) = EcPercent
    Set CamposEc(11) = EcQtd
    Set CamposEc(12) = EcCentroCusto
    Set CamposEc(13) = CkMarcadores
    Set CamposEc(14) = EcNumMinMarcadores
    Set CamposEc(15) = EcNumMedioMarcadores
    
    TextoCamposObrigatorios(0) = "Sequencial"
    TextoCamposObrigatorios(1) = "C�digo da An�lise"
    TextoCamposObrigatorios(2) = "C�digo da R�brica"
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    '...
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_ana"
    Set ChaveEc = EcSeqAna
    
    EcDescrAnaS.Enabled = False
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    EcDescrAnaC.Enabled = False
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    EcDescrAnaP.Enabled = False
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    If gSISTEMA_FACTURACAO = "GH" Then
        LaSistemaFacturacao.caption = "GH"
    ElseIf gSISTEMA_FACTURACAO = "FACTUS" Then
        LaSistemaFacturacao.caption = "Factus"
    End If
    CkSoFacturacao.Value = vbGrayed
    CkFlgfacturarAna.Value = vbGrayed
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormFACTUSNovasAnalises = Nothing
    
End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next
    BG_LimpaCampo_Todos CamposEc
    
    EcCodAnaS.text = ""
    EcCodAnaC.text = ""
    EcCodAnaP.text = ""
    
    EcDescrAnaS.text = ""
    EcDescrAnaC.text = ""
    EcDescrAnaP.text = ""
    EcDescrGH.text = ""
    EcDescrAna.text = ""
    
    EcDescrAnaS.Enabled = False
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    EcDescrAnaC.Enabled = False
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    EcDescrAnaP.Enabled = False
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    EcDescrEFR = ""
    OptSimples.Value = vbUnchecked
    OptComplexas.Value = vbUnchecked
    OptPerfis.Value = vbUnchecked
    CkMarcadores.Value = vbGrayed
    Me.ListaEFR.Clear
    DoEvents
    
End Sub

Sub DefTipoCampos()
    
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    BG_DefTipoCampoEc_Todos_ADO nomeTabela, CamposBD, CamposEc, mediTipoDefeito
    CkContaMembros.Value = vbGrayed
    CkFlgRegra.Value = vbGrayed
    CkMarcadores.Value = vbGrayed
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
       ' BL_Toolbar_BotaoEstado "Remover", "InActivo"
       
       EcCodana_Validate (False)
       EcCodGH_Validate (False)
    
        ' Preenche a listbox com entidades a n�o facturar.
        Me.ListaEFR.Visible = False
        Call Carrega_EFRs(Me.ListaEFR, Trim(EcCodAna))
        Me.ListaEFR.Visible = True
        CarregaLocaisMapeam EcSeqAna
        
        If CkMarcadores.Value = vbChecked Then
            EcNumMedioMarcadores.Enabled = True
            EcNumMinMarcadores.Enabled = True
        Else
            EcNumMedioMarcadores.Enabled = False
            EcNumMinMarcadores.Enabled = False
            EcNumMedioMarcadores.text = ""
            EcNumMinMarcadores.text = ""
        End If
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
'    If ((Len(Trim(Me.EcSeqAna.text)) = 0) And _
'        (Len(Trim(Me.EcDescrAna.text)) = 0) And _
'        (Len(Trim(Me.EcCodGH.text)) = 0) And _
'        (Len(Trim(Me.EcCodAna.text)) = 0)) Then
'
'        MsgBox "Deve Indicar um crit�rio de pesquisa.    ", vbInformation, Me.caption
'        Exit Sub
'    End If
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(nomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY seq_ana ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open CriterioTabela, gConexao
    
    If (rs.RecordCount <= 0) Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        Call LimpaCampos
        Call PreencheCampos
    
        BL_ToolbarEstadoN Estado
'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
        
End Sub

Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
    
        If CkSoFacturacao.Value = vbGrayed Then
            CkSoFacturacao.Value = vbUnchecked
        End If
   
        If CkSoFacturacao.Value = vbUnchecked Then
            sql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE (cod_ana_gh=" & BL_TrataStringParaBD(EcCodGH.text) & _
                " AND cod_ana=" & BL_TrataStringParaBD(EcCodAna.text) & ") OR (" & _
                " cod_ana_gh=" & BL_TrataStringParaBD(EcCodGH.text) & _
                " AND flg_so_facturacao <> 1)"
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            rsAna.Open sql, gConexao
            If rsAna.RecordCount > 0 Then
                BG_Mensagem mediMsgBox, "J� existe uma rubrica para essa an�lise.", vbExclamation, App.ProductName
                'rsAna.Close
                'Set rsAna = Nothing
                'Exit Sub
            End If
            rsAna.Close
            Set rsAna = Nothing
        End If
    
        BL_InicioProcessamento Me, "A inserir registo."
        EcSeqAna.text = BG_DaMAX(nomeTabela, "seq_ana") + 1
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
            GravaLocaisMapeam CLng(EcSeqAna)
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim rv As Integer
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcSeqAna.text = BG_DaMAX(nomeTabela, "seq_ana") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(nomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    rv = Grava_EFRs_Nao_Facturar(EcCodAna.text, Me.ListaEFR)
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        
        If CkSoFacturacao.Value = vbGrayed Then
            CkSoFacturacao.Value = vbUnchecked
        End If
   
        If CkSoFacturacao.Value = vbUnchecked Then
            sql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE ((cod_ana_gh=" & BL_TrataStringParaBD(EcCodGH.text) & _
                " AND cod_ana=" & BL_TrataStringParaBD(EcCodAna.text) & ") OR (" & _
                " cod_ana_gh=" & BL_TrataStringParaBD(EcCodGH.text) & _
                " AND flg_so_facturacao <> 1)) AND seq_ana <> " & EcSeqAna.text
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            rsAna.Open sql, gConexao
            If rsAna.RecordCount > 0 Then
                'BG_Mensagem mediMsgBox, "J� existe uma rubrica para essa an�lise.", vbExclamation, App.ProductName
                'rsAna.Close
                'Set rsAna = Nothing
                'Exit Sub
            End If
            rsAna.Close
            Set rsAna = Nothing
        End If
        
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
            GravaLocaisMapeam CLng(EcSeqAna)
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim rv As Integer
        
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(nomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
   
    rv = Grava_EFRs_Nao_Facturar(EcCodAna.text, Me.ListaEFR)

End Sub

Sub FuncaoRemover()

    Dim iRes As Integer
    
    gMsgTitulo = "Apagar"
    gMsgMsg = "Tem a certeza que quer Apagar estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
     
        BL_InicioProcessamento Me, "A apagar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Delete
        End If
        BL_FimProcessamento Me
    End If


End Sub

Sub BD_Delete()
   
    Dim condicao As String
    Dim SQLQuery As String
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & nomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    FuncaoLimpar

End Sub






Private Sub OptComplexas_Click()
    
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Enabled = True
    BtPesqRapC.Enabled = True
    
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaC.SetFocus
    
End Sub

Private Sub OptPerfis_Click()

    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Enabled = True
    BtPesqRapP.Enabled = True
    
    EcCodAnaP.SetFocus
    
End Sub

Private Sub OptSimples_Click()
    
    EcCodAnaS.Enabled = True
    BtPesqRapS.Enabled = True
    
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaS.SetFocus
    
End Sub

Private Function Carrega_EFRs(lista As ListBox, _
                              cod_ana As String) As Integer

    ' Carrega a lista de EFRs.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As String
    Dim cod_efr As String
    
    ret = ""
    
    cod_ana = UCase(Trim(cod_ana))
    
    If (Len(cod_ana) = 0) Then
        Carrega_EFRs = -2
        Exit Function
    End If
        
    sql = "SELECT " & _
          "     cod_efr, " & _
          "     descr_efr " & _
          "FROM " & _
          "     sl_efr " & _
          "ORDER BY " & _
          "     descr_efr"
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.Open sql, gConexao
        
    lista.Clear
    DoEvents
    DoEvents
    
    While Not (rs_aux.EOF)
        If (Not IsNull(rs_aux(1))) Then
            
            If (Not IsNull(rs_aux(0))) Then
               cod_efr = Trim(rs_aux(0))
               
               lista.AddItem Trim(rs_aux(1))
               lista.ItemData(lista.ListCount - 1) = CLng(cod_efr)
               
               'If (ANALISE_Para_Facturar(Trim(EcCodAna.text), cod_efr) = True) Then
               '     lista.Selected(lista.ListCount - 1) = False
               'Else
                    lista.Selected(lista.ListCount - 1) = True
               'End If
               
            End If
        
        End If
        rs_aux.MoveNext
    Wend
    lista.ListIndex = 0
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    Carrega_EFRs = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Carrega_EFRs (FormFACTUSNovasAnalises) -> " & Err.Description)
            Carrega_EFRs = -1
            Exit Function
            Resume Next
    End Select
End Function

Private Function Grava_EFRs_Nao_Facturar(cod_ana As String, _
                                         lista As ListBox) As Integer
    
    On Error GoTo ErrorHandler
    
    Dim i As Integer
    Dim sql As String
    Dim cmd As ADODB.Command

    cod_ana = Trim(cod_ana)
    If (Len(cod_ana) = 0) Then
        Grava_EFRs_Nao_Facturar = -2
        Exit Function
    End If
    
    Call BG_BeginTransaction
    
    ' Apaga as refer�ncias existentes na tabela para a an�lise.
    
    Set cmd = New ADODB.Command
    
    sql = "DELETE " & _
          "FROM " & _
          "     sl_nao_facturar " & _
          "WHERE " & _
          "     cod_ana = '" & cod_ana & "'"
    
    cmd.ActiveConnection = gConexao
    cmd.CommandText = sql
    cmd.Execute
        
    
    ' Insere cada uma das linhas da lista.
    
    For i = 0 To lista.ListCount - 1
        If (lista.Selected(i)) Then
            sql = "INSERT INTO sl_nao_facturar " & vbCrLf & _
                  "(cod_ana, cod_efr) " & vbCrLf & _
                  "VALUES " & vbCrLf & _
                  "('" & cod_ana & "', " & lista.ItemData(i) & ")"
        
            cmd.CommandText = sql
            cmd.Execute
        End If
    Next
    
    Set cmd = Nothing
    
    Call BG_CommitTransaction
    
    Grava_EFRs_Nao_Facturar = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Grava_EFRs_Nao_Facturar = 1 (FormFACTUSNovasAnalises) -> " & Err.Description)
            Call BG_RollbackTransaction
            Grava_EFRs_Nao_Facturar = -1
            Exit Function
    End Select
End Function

Function ValidaAnalise(codAna As String) As String
    
    Dim sql As String
    Dim rsAna As ADODB.recordset

    ValidaAnalise = ""
    sql = "(SELECT descr_ana_s descricao FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(codAna) & _
    ") UNION " & _
    "(SELECT descr_ana_c descricao FROM sl_ana_c WHERE cod_ana_c=" & BL_TrataStringParaBD(codAna) & _
    ") UNION " & _
    "(SELECT descr_perfis descricao FROM sl_perfis WHERE cod_perfis=" & BL_TrataStringParaBD(codAna) & ")"
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    rsAna.Open sql, gConexao
    If rsAna.RecordCount > 0 Then
        ValidaAnalise = BL_HandleNull(rsAna!descricao, "Sem Descricao")
    End If
    rsAna.Close
    Set rsAna = Nothing
    
End Function
Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub



' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------
Public Sub CarregaLocaisMapeam(seq_ana As String)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ass_ana_fact_locais WHERE seq_ana = " & seq_ana
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    EcLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar locais: " & sSql & " " & Err.Description, Me.Name, "CarregaLocaisMapeam", False
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A

' ------------------------------------------------------------------------------------------------

Public Sub GravaLocaisMapeam(seq_ana As Long)
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_ass_ana_fact_locais WHERE seq_ana = " & seq_ana
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            sSql = "INSERT into sl_ass_ana_fact_locais (seq_ana, cod_local) VALUES("
            sSql = sSql & (seq_ana) & ","
            sSql = sSql & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Gravar locais: " & sSql & " " & Err.Description, Me.Name, "GravaLocaisMapeam", False
    Exit Sub
    Resume Next
End Sub




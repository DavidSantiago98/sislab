VERSION 5.00
Begin VB.Form FormCancelarRecibos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCancelarRecibos"
   ClientHeight    =   4110
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7920
   Icon            =   "FormCancelamentoRecibos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4110
   ScaleWidth      =   7920
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox LbNumAnulacao 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1320
      TabIndex        =   32
      Top             =   240
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      Locked          =   -1  'True
      TabIndex        =   31
      Text            =   "/"
      Top             =   200
      Width           =   120
   End
   Begin VB.TextBox EcSerieDoc 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4560
      Locked          =   -1  'True
      TabIndex        =   30
      Top             =   200
      Width           =   495
   End
   Begin VB.TextBox EcNumCaucao 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4440
      Locked          =   -1  'True
      TabIndex        =   29
      Top             =   600
      Width           =   1215
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   1440
      TabIndex        =   25
      Top             =   5280
      Width           =   855
   End
   Begin VB.CommandButton BtImprimirRecibo 
      Height          =   495
      Left            =   6240
      Picture         =   "FormCancelamentoRecibos.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   24
      ToolTipText     =   "Imprimir Recibo Anula��po"
      Top             =   2520
      Width           =   615
   End
   Begin VB.TextBox EcDescricao 
      Height          =   525
      Left            =   1320
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   16
      Top             =   840
      Width           =   5750
   End
   Begin VB.TextBox EcObsCanc 
      Height          =   975
      Left            =   1320
      MultiLine       =   -1  'True
      TabIndex        =   15
      Top             =   1440
      Width           =   6255
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   120
      TabIndex        =   8
      Top             =   3120
      Width           =   7490
      Begin VB.Label Label5 
         Caption         =   "Data"
         Height          =   255
         Left            =   3960
         TabIndex        =   14
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label6 
         Caption         =   "Hora"
         Height          =   255
         Left            =   5640
         TabIndex        =   13
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label7 
         Caption         =   "Utilizador"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   735
      End
      Begin VB.Label LbDataCanc 
         Height          =   255
         Left            =   4440
         TabIndex        =   11
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label LbHoraCanc 
         Height          =   255
         Left            =   6120
         TabIndex        =   10
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label LbUtilizadorCanc 
         Height          =   255
         Left            =   960
         TabIndex        =   9
         Top             =   240
         Width           =   2895
      End
   End
   Begin VB.CommandButton BtPesquisacanc 
      Height          =   525
      Left            =   7080
      Picture         =   "FormCancelamentoRecibos.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Pesquisa R�pida de tipos cancelamento"
      Top             =   840
      Width           =   495
   End
   Begin VB.CommandButton BtConfirmarCanc 
      Height          =   495
      Left            =   5520
      Picture         =   "FormCancelamentoRecibos.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Confirmar Anulamento"
      Top             =   2520
      Width           =   615
   End
   Begin VB.CommandButton BtCancelarCanc 
      Height          =   495
      Left            =   6960
      Picture         =   "FormCancelamentoRecibos.frx":266A
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Voltar"
      Top             =   2520
      Width           =   615
   End
   Begin VB.TextBox EcTipoCanc 
      Height          =   285
      Left            =   2040
      TabIndex        =   4
      Top             =   4200
      Width           =   615
   End
   Begin VB.TextBox EcNumRec 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5160
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   200
      Width           =   1095
   End
   Begin VB.TextBox EcHoraCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Top             =   4920
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.TextBox EcDataCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   4560
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.TextBox EcUtilizadorCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4560
      TabIndex        =   0
      Top             =   4560
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label Label9 
      Caption         =   "da  cau��o com n�"
      Height          =   255
      Left            =   2760
      TabIndex        =   28
      Top             =   600
      Width           =   1455
   End
   Begin VB.Label LaAnulacao 
      Caption         =   "do documento com  n� "
      Height          =   255
      Left            =   2760
      TabIndex        =   27
      Top             =   240
      Width           =   2175
   End
   Begin VB.Label Label13 
      Caption         =   "EcNumReq"
      Height          =   255
      Index           =   1
      Left            =   360
      TabIndex        =   26
      Top             =   5280
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Anula��o N�"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "Observa��es"
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label8 
      Caption         =   "Cod tipo cancelamento"
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   4200
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCanc"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   4560
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label13 
      Caption         =   "EcHoraCanc"
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   18
      Top             =   4920
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label14 
      Caption         =   "EcUtilizadorCanc"
      Height          =   255
      Left            =   3120
      TabIndex        =   17
      Top             =   4560
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormCancelarRecibos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim ListarRemovidos As Boolean

Dim rsCanc As ADODB.recordset

Dim numAnulacao As Long

'Flag que indica se � a 1� vez ou n�o que se faz o activate da form
Public Flg_Passou As Boolean
Const vermelho = &HC0C0FF

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    Dim sql As String
    Dim Tabela As New ADODB.recordset
    
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    
    If Flg_Passou = False Then
        If FormGestaoRequisicaoPrivado.RegistosR(FormGestaoRequisicaoPrivado.FGRecibos.row).Caucao = 0 And _
            FormGestaoRequisicaoPrivado.RegistosR(FormGestaoRequisicaoPrivado.FGRecibos.row).ValorPagar <> 0 Then
            EcNumRec.Text = FormGestaoRequisicaoPrivado.RegistosR(FormGestaoRequisicaoPrivado.FGRecibos.row).NumDoc
            EcSerieDoc.Text = FormGestaoRequisicaoPrivado.RegistosR(FormGestaoRequisicaoPrivado.FGRecibos.row).SerieDoc
        ElseIf FormGestaoRequisicaoPrivado.RegistosR(FormGestaoRequisicaoPrivado.FGRecibos.row).NumDoc <> 0 Then
            EcSerieDoc.Text = FormGestaoRequisicaoPrivado.RegistosR(FormGestaoRequisicaoPrivado.FGRecibos.row).SerieDoc
            EcNumRec.Text = FormGestaoRequisicaoPrivado.RegistosR(FormGestaoRequisicaoPrivado.FGRecibos.row).NumDoc
        End If
        
        EcNumReq = gRequisicaoActiva
        sql = "SELECT n_req FROM sl_recibos_canc WHERE serie_rec = '" & EcSerieDoc & "' and  n_rec=" & EcNumRec
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount > 0 Then
            FuncaoProcurar
        Else
            BtImprimirRecibo.Enabled = False
        End If
        Tabela.Close
        Set Tabela = Nothing
        Flg_Passou = True
    End If

    BL_ToolbarEstadoN estado
        
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    On Error Resume Next
    
    
    BL_ToolbarEstadoN 2
    
    If Not rsCanc Is Nothing Then
        rsCanc.Close
        Set rsCanc = Nothing
    End If
    
    Set FormCancelarRecibos = Nothing
    
    Set gFormActivo = FormGestaoRequisicaoPrivado
    
    FormGestaoRequisicaoPrivado.Enabled = True
    'FormGestaoRequisicaoPrivado.RS.Requery
    'FormGestaoRequisicaoPrivado.RS.Bookmark = FormGestaoRequisicaoPrivado.MarcaLocal
        
End Sub

Private Sub BtCancelarCanc_Click()
    FormGestaoRequisicaoPrivado.PreencheFgRecibos
    Unload Me
End Sub

Private Sub BtConfirmarCanc_Click()
    
    FuncaoInserir
End Sub

Private Sub BtImprimirRecibo_Click()
    If Trim(LbNumAnulacao) <> "" Then
        FormGestaoRequisicaoPrivado.ImprimeAnulRecibo LbNumAnulacao
    End If
End Sub

Private Sub BtPesquisacanc_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_canc"
    CamposEcran(1) = "descr_canc"
    Tamanhos(1) = 5000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_t_canc"
    CamposEcran(2) = "cod_t_canc"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_t_canc"
    CWhere = "t_util = " & BL_TrataStringParaBD(TIPOS_CANCELAMENTO.t_RECIBO)
    CampoPesquisa = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Cancelamentos")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescricao.Text = resultados(1)
            EcTipoCanc.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem tipos de cancelamentos para requisi��es", vbExclamation, "ATEN��O"
    End If

End Sub

Private Sub EcTipoCanc_Change()
    
    Dim sql As String
    Dim Tabela As New ADODB.recordset
    
    If EcTipoCanc.Text <> "" Then
        sql = "SELECT descr_canc FROM sl_t_canc WHERE cod_t_canc=" & BL_TrataStringParaBD(EcTipoCanc.Text)
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            EcDescricao.Text = Tabela!descr_canc
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub

Private Sub EcUtilizadorCanc_LostFocus()
    
    LbUtilizadorCanc = BL_SelNomeUtil(EcUtilizadorCanc.Text)

End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Anula��o de Recibos"
    Me.left = 520
    Me.top = 1680
    Me.Width = 7770
    Me.Height = 4425 ' Normal
    'Me.Height = 3990 ' Campos Extras
    
    NomeTabela = "sl_recibos_canc"
    Set CampoDeFocus = EcNumRec
    
    NumCampos = 9
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_rec"
    CamposBD(1) = "serie_rec"
    CamposBD(2) = "dt_canc"
    CamposBD(3) = "hr_canc"
    CamposBD(4) = "t_canc"
    CamposBD(5) = "obs_canc"
    CamposBD(6) = "user_canc"
    CamposBD(7) = "n_req"
    CamposBD(8) = "n_anulacao"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumRec
    Set CamposEc(1) = EcSerieDoc
    Set CamposEc(2) = EcDataCanc
    Set CamposEc(3) = EcHoraCanc
    Set CamposEc(4) = EcTipoCanc
    Set CamposEc(5) = EcObsCanc
    Set CamposEc(6) = EcUtilizadorCanc
    Set CamposEc(7) = EcNumReq
    Set CamposEc(8) = LbNumAnulacao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(4) = "Tipo de cancelamento"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_rec"
    Set ChaveEc = EcNumRec
    
    Flg_Passou = False

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_canc", EcDataCanc, mediTipoData
    'BG_DefTipoCampoEc_ADO NomeTabela, "hr_canc", EcHoraCanc, mediTipoHora
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub Funcao_DataActual()
    
    If Me.ActiveControl.Name = "EcDataCanc" Then
        BL_PreencheData EcDataCanc, Me.ActiveControl
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    Dim i As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        If EcDescricao.Text = "" Then
            MsgBox "Descri��o obrigat�ria"
            EcDescricao.SetFocus
            Exit Sub
        End If
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        
        numAnulacao = -1
        If BG_DaComboSel(FormGestaoRequisicaoPrivado.CbSala) <> mediComboValorNull Then
            If BL_SelCodigo("SL_COD_SALAS", "flg_seq_recibos", "COD_SALA", BG_DaComboSel(FormGestaoRequisicaoPrivado.CbSala)) = mediSim Then
                While numAnulacao = -1 And i <= 10
                    numAnulacao = BL_GeraAnulacao(BG_DaComboSel(FormGestaoRequisicaoPrivado.CbSala))
                    i = i + 1
                Wend
            Else
                While numAnulacao = -1 And i <= 10
                    numAnulacao = BL_GeraNumero("N_ANULACAO")
                    i = i + 1
                Wend
            End If
        Else
            While numAnulacao = -1 And i <= 10
                numAnulacao = BL_GeraNumero("N_ANULACAO")
                i = i + 1
            Wend
        
        End If
        If numAnulacao <> -1 Then
            LbNumAnulacao = numAnulacao
            RECIBO_Anulacao numAnulacao, FormGestaoRequisicaoPrivado.FGRecibos.row, FormGestaoRequisicaoPrivado.EcNumReq, _
                                        EcTipoCanc, EcObsCanc, FormGestaoRequisicaoPrivado.RegistosA, FormGestaoRequisicaoPrivado.RegistosR, _
                                        FormGestaoRequisicaoPrivado.RegistosRM
            FormGestaoRequisicaoPrivado.ImprimeAnulRecibo CLng(numAnulacao)
            FormGestaoRequisicaoPrivado.RetiraAnalisesApagadas
            FormGestaoRequisicaoPrivado.PreencheFgRecibos
            Unload Me
        Else
            BL_FimProcessamento Me
            Unload Me
            FormGestaoRequisicaoPrivado.PreencheFgRecibos
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero de anula��o!", vbError, "ERRO"
        End If
    End If

End Sub

Sub BD_Insert()
    ' nada
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rsCanc = New ADODB.recordset
    LbNumAnulacao = Trim(LbNumAnulacao)
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY n_req"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rsCanc.CursorType = adOpenStatic
    rsCanc.CursorLocation = adUseServer
    
    rsCanc.Open CriterioTabela, gConexao
    
    If rsCanc.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub
Sub PreencheCampos()
    
    If rsCanc.RecordCount = 0 Then
        FuncaoLimpar
        BtImprimirRecibo.Enabled = False
    Else
        BG_PreencheCampoEc_Todos_ADO rsCanc, CamposBD, CamposEc
        LbDataCanc.caption = EcDataCanc.Text
        LbHoraCanc.caption = EcHoraCanc.Text
        LbUtilizadorCanc.caption = EcUtilizadorCanc.Text
        BtConfirmarCanc.Enabled = False
        BtPesquisaCanc.Enabled = False
        EcDescricao.Enabled = False
        EcObsCanc.Enabled = False
        BtImprimirRecibo.Enabled = True
    End If

End Sub

Sub FuncaoLimpar()
    
    LimpaCampos

End Sub

Sub LimpaCampos()
    
    BG_LimpaCampo_Todos CamposEc

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

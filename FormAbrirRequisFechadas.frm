VERSION 5.00
Begin VB.Form FormAbrirRequisFechadas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAbrirRequisFechadas"
   ClientHeight    =   645
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3045
   Icon            =   "FormAbrirRequisFechadas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   645
   ScaleWidth      =   3045
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAbrir 
      Height          =   375
      Left            =   2400
      Picture         =   "FormAbrirRequisFechadas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   120
      Width           =   495
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   150
      Width           =   855
   End
End
Attribute VB_Name = "FormAbrirRequisFechadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim argumentos() As String
Public rs As ADODB.recordset



Private Sub BtAbrir_Click()
    Dim flg_executou As Boolean
    Dim sql As String
    On Error GoTo TrataErro
    Dim iReg As Integer
    Dim traducao As String
    flg_executou = False
    
    If EcNumReq.Text <> "" Then
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        rs.Source = "select dt_fact, dt_fecho from sl_requis where n_req = " & EcNumReq.Text
        rs.ActiveConnection = gConexao
        rs.Open
        If rs.RecordCount > 0 Then
            If BL_HandleNull(rs!dt_fecho, "") = "" Then
            ElseIf BL_HandleNull(rs!dt_fact, "") <> "" Then
            Else
                flg_executou = True
                sql = "update sl_requis set user_fecho = null, dt_fecho = null, hr_fecho = null where n_req = " & EcNumReq.Text
                BG_ExecutaQuery_ADO sql
                BG_Trata_BDErro
            End If
        End If
        ' ASSINATURA
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        rs.Source = "select * from sl_req_assinatura where n_req = " & EcNumReq.Text
        rs.ActiveConnection = gConexao
        rs.Open
        If rs.RecordCount > 0 Then
            flg_executou = True
            BG_BeginTransaction
            sql = "INSERT INTO sl_req_assinatura_apagadas (n_req, user_cri, dt_cri, hr_cri, user_Assin, dt_assin, hr_assin, flg_res_mv) VALUES ("
            sql = sql & EcNumReq & "," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", " & BL_TrataDataParaBD(Bg_DaData_ADO)
            sql = sql & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", " & BL_TrataStringParaBD(CStr(BL_HandleNull(rs!user_cri, "")))
            sql = sql & ", " & BL_TrataDataParaBD(BL_HandleNull(rs!dt_cri, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(rs!hr_cri, ""))
            sql = sql & ", " & BL_HandleNull(rs!flg_res_mv, 0) & ")"
            
            iReg = BG_ExecutaQuery_ADO(sql)
            If iReg <= 0 Then GoTo TrataErro
            
            sql = "UPDATE sl_realiza SET flg_assinado = 0, user_assinado = null, dt_assinado = null, hr_assinado = null WHERE n_req =" & EcNumReq
            iReg = BG_ExecutaQuery_ADO(sql)
            If iReg <= 0 Then GoTo TrataErro
            
            sql = "DELETE FROM sl_req_Assinatura WHERE n_req = " & EcNumReq
            iReg = BG_ExecutaQuery_ADO(sql)
            If iReg <= 0 Then GoTo TrataErro
            BG_CommitTransaction
            

        End If
    End If
    
    
    If flg_executou = True Then
        ' opera��o efectuada com sucesso
        BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation
    Else
        ' opera��o nao efectuada
        BG_Mensagem mediMsgBox, "Opera��o n�o efectuada.", vbInformation
    End If
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao validar: " & Err.Description, Me.Name, "BtAbrir_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub


Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub


Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()
    Me.caption = " Abir requisi��es( fechadas / assinadas)"
    DIC_Inicializacao_Idioma Me, gIdioma
    Me.left = 540
    Me.top = 450
    Me.Width = 3135
    Me.Height = 1125 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormAbrirRequisFechadas = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcNumReq.Text = ""
    
End Sub

Sub DefTipoCampos()

    EcNumReq.Tag = adInteger
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    
End Sub

Sub FuncaoProcurar()
          
End Sub

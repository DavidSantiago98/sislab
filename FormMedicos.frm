VERSION 5.00
Begin VB.Form FormMedicos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMedicos"
   ClientHeight    =   9030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7320
   Icon            =   "FormMedicos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9030
   ScaleWidth      =   7320
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Registo Cancelado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   5040
      TabIndex        =   52
      Top             =   0
      Width           =   2055
   End
   Begin VB.TextBox EcUserCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   51
      Top             =   8040
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcHrCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3360
      TabIndex        =   50
      Top             =   8040
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDtCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2880
      TabIndex        =   49
      Top             =   8040
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.ListBox EcLocais 
      Appearance      =   0  'Flat
      Height          =   705
      Left            =   1440
      Style           =   1  'Checkbox
      TabIndex        =   47
      Top             =   3240
      Width           =   5535
   End
   Begin VB.TextBox EcCodExt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   255
      Left            =   3720
      MaxLength       =   4
      TabIndex        =   45
      Top             =   2880
      Width           =   855
   End
   Begin VB.TextBox EcCodOrdem 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   255
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   44
      Top             =   2880
      Width           =   855
   End
   Begin VB.TextBox EcSeqMed 
      Height          =   285
      Left            =   6480
      TabIndex        =   41
      Top             =   8280
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcEmail 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   39
      Top             =   2520
      Width           =   5655
   End
   Begin VB.TextBox EcServico 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   36
      Top             =   720
      Width           =   5655
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   4440
      TabIndex        =   34
      Top             =   9240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcRuaPostal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2040
      MaxLength       =   3
      TabIndex        =   33
      Top             =   1800
      Width           =   495
   End
   Begin VB.TextBox EcCodPostal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   32
      Top             =   1800
      Width           =   615
   End
   Begin VB.TextBox EcDescrPostal 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2520
      TabIndex        =   31
      Top             =   1800
      Width           =   4215
   End
   Begin VB.CommandButton BtPesqCodPostal 
      Height          =   315
      Left            =   6720
      Picture         =   "FormMedicos.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   30
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
      Top             =   1800
      Width           =   375
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      ItemData        =   "FormMedicos.frx":0596
      Left            =   240
      List            =   "FormMedicos.frx":0598
      TabIndex        =   27
      Top             =   4320
      Width           =   6885
   End
   Begin VB.TextBox EcCodPostalCompleto 
      Height          =   285
      Left            =   2280
      TabIndex        =   25
      Top             =   9240
      Width           =   495
   End
   Begin VB.TextBox EcMorada 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Top             =   1440
      Width           =   5655
   End
   Begin VB.TextBox EcCSaude 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Top             =   1080
      Width           =   5655
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2280
      TabIndex        =   17
      Top             =   8760
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4320
      TabIndex        =   16
      Top             =   8760
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2280
      TabIndex        =   15
      Top             =   8310
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   14
      Top             =   8280
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   240
      TabIndex        =   7
      Top             =   6600
      Width           =   6885
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   10
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   9
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   8
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.TextBox EcObs 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   2160
      Width           =   5655
   End
   Begin VB.TextBox EcNome 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      MaxLength       =   20
      TabIndex        =   1
      Top             =   360
      Width           =   4815
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   0
      Top             =   360
      Width           =   855
   End
   Begin VB.Label LbLocal 
      AutoSize        =   -1  'True
      Caption         =   "Locais"
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   48
      Top             =   3285
      Width           =   465
   End
   Begin VB.Label Label20 
      Caption         =   "C�digo Ext."
      Height          =   255
      Index           =   1
      Left            =   2520
      TabIndex        =   46
      Top             =   2880
      Width           =   1095
   End
   Begin VB.Label Label20 
      Caption         =   "C�d. Ordem"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   43
      Top             =   2880
      Width           =   1095
   End
   Begin VB.Label Label19 
      Caption         =   "EcSeqMed"
      Height          =   255
      Left            =   5640
      TabIndex        =   42
      Top             =   8280
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label18 
      Caption         =   "Email"
      Height          =   255
      Left            =   240
      TabIndex        =   40
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label17 
      Caption         =   "Servi�o"
      Height          =   255
      Left            =   4200
      TabIndex        =   38
      Top             =   4080
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Servi�o"
      Height          =   255
      Left            =   240
      TabIndex        =   37
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "EcCodPostalAux"
      Height          =   255
      Left            =   3000
      TabIndex        =   35
      Top             =   9240
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label16 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   270
      TabIndex        =   29
      Top             =   4080
      Width           =   645
   End
   Begin VB.Label Label15 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   1440
      TabIndex        =   28
      Top             =   4080
      Width           =   855
   End
   Begin VB.Label Label14 
      Caption         =   "EcCodPostalCompleto"
      Height          =   255
      Left            =   600
      TabIndex        =   26
      Top             =   9240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Cod Postal"
      Height          =   255
      Left            =   240
      TabIndex        =   24
      Top             =   1800
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "Morada"
      Height          =   255
      Left            =   240
      TabIndex        =   23
      Top             =   1440
      Width           =   615
   End
   Begin VB.Label Label7 
      Caption         =   "C. Sa�de"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   720
      TabIndex        =   21
      Top             =   8280
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   600
      TabIndex        =   20
      Top             =   8760
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3000
      TabIndex        =   19
      Top             =   8280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   3000
      TabIndex        =   18
      Top             =   8760
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "Observa��es"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "M�dico"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   360
      Width           =   615
   End
End
Attribute VB_Name = "FormMedicos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 20/09/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset
Dim CodPostalInexistente As Boolean


Private Sub BtPesqCodPostal_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_postal"
    CamposEcran(1) = "descr_postal"
    Tamanhos(1) = 2000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_postal"
    CamposEcran(2) = "cod_postal"
    Tamanhos(2) = 2000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_postal"
    CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    CampoPesquisa = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, resultados(2), "-") - 1
            If i = -1 Then
                s1 = resultados(2)
                s2 = ""
            Else
                s1 = Mid(resultados(2), 1, i)
                s2 = Mid(resultados(2), InStr(1, resultados(2), "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcCodPostalAux.Text = Trim(resultados(2))
            EcDescrPostal.Text = resultados(1)
            EcDescrPostal.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If

    
End Sub

Private Sub EcCodPostal_GotFocus()

    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodPostal_LostFocus()

    If CodPostalInexistente = True Then
        EcCodPostal.SetFocus
    End If
    CodPostalInexistente = False
    
End Sub

Private Sub EcCodPostalCompleto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalCompleto.Text <> "" Then
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     seq_postal = " & EcCodPostalCompleto
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcCodPostal.SetFocus
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
            EcCodPostalAux.Text = Trim(rsCodigo!cod_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    If gF_REQUIS_PRIVADO = 1 Then
        EcCodigo.Text = FormGestaoRequisicaoPrivado.EcCodMedico.Text
        EcNome.SetFocus
    End If
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormMedicos = Nothing
    
    If gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.Enabled = True
        Set gFormActivo = FormGestaoRequisicaoPrivado
    ElseIf gF_REQUIS = 1 Then
        FormGestaoRequisicao.Enabled = True
        Set gFormActivo = FormGestaoRequisicao
    End If

    
End Sub

Sub Inicializacoes()
    
    Me.caption = "M�dicos"
    Me.left = 540
    Me.top = 450
    Me.Width = 7410
    Me.Height = 8055 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_medicos"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 19
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_med"
    CamposBD(1) = "nome_med"
    CamposBD(2) = "csaude"
    CamposBD(3) = "morada"
    CamposBD(4) = "codpostal"
    CamposBD(5) = "obs_med"
    CamposBD(6) = "user_cri"
    CamposBD(7) = "dt_cri"
    CamposBD(8) = "user_act"
    CamposBD(9) = "dt_act"
    CamposBD(10) = "servico"
    CamposBD(11) = "email"
    CamposBD(12) = "seq_med"
    CamposBD(13) = "cod_ordem"
    CamposBD(14) = "cod_ext"
    CamposBD(15) = "flg_invisivel"
    CamposBD(16) = "user_canc"
    CamposBD(17) = "dt_canc"
    CamposBD(18) = "hr_canc"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcNome
    Set CamposEc(2) = EcCSaude
    Set CamposEc(3) = EcMorada
    Set CamposEc(4) = EcCodPostalAux
    Set CamposEc(5) = EcObs
    Set CamposEc(6) = EcUtilizadorCriacao
    Set CamposEc(7) = EcDataCriacao
    Set CamposEc(8) = EcUtilizadorAlteracao
    Set CamposEc(9) = EcDataAlteracao
    Set CamposEc(10) = EcServico
    Set CamposEc(11) = EcEmail
    Set CamposEc(12) = EcSeqMed
    Set CamposEc(13) = EcCodOrdem
    Set CamposEc(14) = EcCodExt
    Set CamposEc(15) = CkInvisivel
    Set CamposEc(16) = EcUserCanc
    Set CamposEc(17) = EcDtCanc
    Set CamposEc(18) = EcHrCanc
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo do m�dico"
    TextoCamposObrigatorios(1) = "Nome do m�dico"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_med"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("cod_med", "nome_med", "servico")
    NumEspacos = Array(7, 25, 30)
        
End Sub

Sub DefTipoCampos()
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_canc", EcDtCanc, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    CkInvisivel.Visible = False
    CkInvisivel.value = vbGrayed
End Sub

Private Sub EcCodigo_LostFocus()
    
    EcCodigo.Text = UCase(EcCodigo.Text)

End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    
    EcCodPostal.Text = ""
    EcRuaPostal.Text = ""
    EcDescrPostal.Text = ""
    EcCodPostalCompleto.Text = ""
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next
    CkInvisivel.Visible = False
    CkInvisivel.value = vbGrayed
    
End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        If gLAB = "BIO" Or gLAB = "GM" Then
            EcCodigo = BG_DaMAX(NomeTabela, "cod_med") + 1
        End If
        EcSeqMed = BG_DaMAX(NomeTabela, "seq_med") + 1
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
            GravaLocaisMedicos EcCodigo
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            GravaLocaisMedicos EcCodigo
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    
    
    condicao = ChaveBD & " = " & BL_TrataStringParaBD(BG_CvPlica(rs(ChaveBD)))
    
    
    
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'

    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
            
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    If gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.EcNomeMedico.Text = EcNome.Text
        Unload Me
    End If
    
End Sub

Sub FuncaoRemover()
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer desactivar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        CkInvisivel.value = vbChecked
        EcUserCanc = CStr(gCodUtilizador)
        EcDtCanc = Bg_DaData_ADO
        EcHrCanc = Bg_DaHora_ADO
        BD_Update
        BL_FimProcessamento Me
    End If
    
End Sub


Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim str_aux As String
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If EcNome.Text <> "" Then
        str_aux = EcNome.Text
        EcNome.Text = "*" & UCase(EcNome.Text) & "*"
        DoEvents
        CamposBD(1) = "UPPER(nome_med)"
    End If
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & " ORDER BY cod_med ASC, nome_med ASC"
    Else
    End If
              
    CamposBD(1) = "nome_med"
    EcNome.Text = str_aux
    DoEvents
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        
        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'
        
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        PreencheCodigoPostal
        CarregaLocaisMedicos EcCodigo
        If CkInvisivel.value = 1 Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Private Sub EcRuaPostal_GotFocus()

    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcRuaPostal_Validate(Cancel As Boolean)
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcRuaPostal.Text = UCase(EcRuaPostal.Text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.Text) & "-" & Trim(EcRuaPostal.Text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            CodPostalInexistente = True
            EcCodPostal.SetFocus
        Else
            EcCodPostalAux.Text = Postal
            EcDescrPostal.Text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.Text = ""
    End If
    
End Sub

Sub PreencheCodigoPostal()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostalAux)
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcEmail_Validate(Cancel As Boolean)
    If EcEmail <> "" Then
        If InStr(1, EcEmail, "@") = 0 Then
            BG_Mensagem mediMsgBox, "Email inv�lido.", vbExclamation, "Valida��o de email"
            EcEmail = ""
            EcEmail.SetFocus
            Exit Sub
        End If
    End If
End Sub

' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------
Public Sub CarregaLocaisMedicos(cod_med As String)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ass_med_locais WHERE cod_med = " & BL_TrataStringParaBD(cod_med)
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    EcLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar locais: " & sSql & " " & Err.Description, Me.Name, "CarregaLocaisMedicos", False
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------

Public Sub GravaLocaisMedicos(cod_med As String)
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "DELETE FROM sl_ass_med_locais WHERE cod_med = " & BL_TrataStringParaBD(cod_med)
    BG_ExecutaQuery_ADO sSql
        
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            sSql = "INSERT into sl_ass_med_locais (cod_med, cod_local) VALUES("
            sSql = sSql & BL_TrataStringParaBD(cod_med) & ","
            sSql = sSql & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Gravar locais: " & sSql & " " & Err.Description, Me.Name, "GravaLocaisMedicos", False
    Exit Sub
    Resume Next
End Sub



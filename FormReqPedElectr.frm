VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormReqPedElectr 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormReqPedElectr"
   ClientHeight    =   6870
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15255
   Icon            =   "FormReqPedElectr.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   15255
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcSNS 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   10680
      TabIndex        =   31
      Top             =   1440
      Width           =   2775
   End
   Begin VB.TextBox EcCodProveniencia 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   29
      ToolTipText     =   "Proveni�ncia"
      Top             =   1920
      Width           =   735
   End
   Begin VB.CommandButton BtPesquisaProveniencia 
      Height          =   315
      Left            =   9000
      Picture         =   "FormReqPedElectr.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   28
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1920
      Width           =   375
   End
   Begin VB.TextBox EcDescrProveniencia 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   1920
      Width           =   6975
   End
   Begin VB.CheckBox CkConsSemMarcacao 
      Appearance      =   0  'Flat
      Caption         =   "Consultas Sem Marca��o"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   9840
      TabIndex        =   26
      Top             =   480
      Width           =   2175
   End
   Begin VB.TextBox EcNome 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   24
      Top             =   1440
      Width           =   7935
   End
   Begin VB.CheckBox CkRefresh 
      Appearance      =   0  'Flat
      Caption         =   "Actualizar autom�ticamente"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      TabIndex        =   23
      Top             =   6600
      Width           =   2415
   End
   Begin VB.TextBox EcUtente 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2760
      TabIndex        =   21
      Top             =   960
      Width           =   2415
   End
   Begin VB.ComboBox CbTUtente 
      Height          =   315
      Left            =   1440
      TabIndex        =   22
      Text            =   "CbSituacao"
      Top             =   960
      Width           =   1335
   End
   Begin VB.CheckBox CkMarc 
      Appearance      =   0  'Flat
      Caption         =   "Incluir Consultas"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7800
      TabIndex        =   19
      Top             =   480
      Width           =   1575
   End
   Begin VB.CheckBox CkReq 
      Appearance      =   0  'Flat
      Caption         =   "Incluir Requisi��es"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5520
      TabIndex        =   18
      Top             =   480
      Width           =   1935
   End
   Begin VB.CheckBox CkDatas 
      Appearance      =   0  'Flat
      Caption         =   "Incluir Intervalo de Datas"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   480
      TabIndex        =   17
      Top             =   120
      Width           =   2415
   End
   Begin VB.TextBox EcPrescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   10680
      TabIndex        =   15
      Top             =   960
      Width           =   1095
   End
   Begin VB.TextBox EcEpisodio 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   7920
      TabIndex        =   13
      Top             =   960
      Width           =   1455
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   6360
      TabIndex        =   12
      Text            =   "CbSituacao"
      Top             =   960
      Width           =   1575
   End
   Begin VB.ComboBox CbLocal 
      Height          =   315
      Left            =   10680
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   1920
      Width           =   2775
   End
   Begin VB.CommandButton BtImprimir 
      BackColor       =   &H0080FFFF&
      Height          =   615
      Left            =   14520
      Picture         =   "FormReqPedElectr.frx":0396
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Imprimir"
      Top             =   720
      Width           =   615
   End
   Begin VB.TextBox EcEstado 
      Height          =   285
      Left            =   1080
      TabIndex        =   8
      Top             =   7320
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   9480
      Top             =   2160
   End
   Begin MSFlexGridLib.MSFlexGrid FgReqPend 
      Height          =   4335
      Left            =   120
      TabIndex        =   7
      Top             =   2400
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   7646
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin VB.CommandButton BtEntrTubos 
      BackColor       =   &H00C0C0FF&
      Height          =   615
      Left            =   13920
      Picture         =   "FormReqPedElectr.frx":1060
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Entrada Tubos"
      Top             =   720
      Width           =   615
   End
   Begin VB.CommandButton BtGestReq 
      BackColor       =   &H00C0FFC0&
      Height          =   615
      Left            =   14520
      Picture         =   "FormReqPedElectr.frx":1D2A
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Gest�o Requisi��es"
      Top             =   120
      Width           =   615
   End
   Begin VB.CommandButton BtRefresh 
      BackColor       =   &H00FFC0C0&
      Height          =   615
      Left            =   13920
      Picture         =   "FormReqPedElectr.frx":29F4
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Actualizar"
      Top             =   120
      Width           =   615
   End
   Begin MSComCtl2.DTPicker EcDtIni 
      Height          =   300
      Left            =   1440
      TabIndex        =   0
      Top             =   480
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   529
      _Version        =   393216
      Format          =   170786817
      CurrentDate     =   39555
   End
   Begin MSComCtl2.DTPicker EcDtFinal 
      Height          =   300
      Left            =   3840
      TabIndex        =   1
      Top             =   480
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   529
      _Version        =   393216
      Format          =   170786817
      CurrentDate     =   39555
   End
   Begin VB.Label Label2 
      Caption         =   "SNS"
      Height          =   255
      Index           =   5
      Left            =   9840
      TabIndex        =   32
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Proveni�ncia"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   30
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Nome"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   25
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Utente"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   20
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Prescri��o"
      Height          =   255
      Index           =   1
      Left            =   9840
      TabIndex        =   16
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Epis�dio"
      Height          =   255
      Index           =   0
      Left            =   5520
      TabIndex        =   14
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Local"
      Height          =   255
      Index           =   2
      Left            =   9840
      TabIndex        =   11
      Top             =   1920
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Data Final"
      Height          =   255
      Index           =   1
      Left            =   2880
      TabIndex        =   3
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicial"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   975
   End
End
Attribute VB_Name = "FormReqPedElectr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Dim Estado As Integer
Dim CamposBDparaListBox
Dim NumEspacos
Dim CampoDeFocus As Object


Private Type requisicoes
    n_req As String
    estado_req As String
    dt_previ As String
    dt_cri As String
    hr_cri As String
    Proven As String
    obs_proven As String
    nome As String
    episoido As String
    t_episodio As String
    t_urg As String
    cod_medico As String
    estado_etiq As String
    Produtos As String
    tipo_marcacao As Integer
    n_prescricao As Long
    user_cri As String
    nome_user As String
    gr_ana As String
    descr_gr_ana As String
End Type

Dim EstrutDadosRequis() As requisicoes
Dim TotalDadosRequis As Integer

Const lColNReq = 0
Const lcolEpis = 1
Const lColNome = 2
Const lColProven = 3
Const lColObsProven = 4
Const lColDtPrevi = 5
Const lColDtCri = 6
Const lColHrCri = 7
Const lColUserCri = 8
Const lColEstadoEtiq = 9
Const lColProdutos = 10
Const lColPrescricao = 11
Const lColGrAna = 12

Const lVermelho = 12632319

Const lTipoPedidoMarcacao = 1
Const lTipoPedidoRequisicao = 2



Private Sub BtEntrTubos_Click()
    If FgReqPend.row > 0 And FgReqPend.row <= TotalDadosRequis Then
        'FormReqPedElectr.Enabled = False
        
        ' pferreira 2010.09.29
        Select Case (gVersaoChegadaTubos)
            Case ("V1")
                FormChegadaPorTubos.Show
                FormChegadaPorTubos.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
                FormChegadaPorTubos.FuncaoProcurar
            Case ("V2")
                'NELSONPSILVA 08.03.2018 Glintt-HS-18730
                If gGestao_Tubos_Default = mediNao Then
                    gColheitaChegada = gEnumColheitaChegada.ColheitaTubo
                ElseIf gGestao_Tubos_Default = mediSim Then
                    gColheitaChegada = gEnumColheitaChegada.ChegadaTubo
                End If
                '
                'NELSONPSILVA 04.04.2018 Glintt-HS-18011 (passagem de vari�vel global para registo de Log RGPD por context_info)
                'Dim frtubo As New FormChegadaPorTubosV2
                FormChegadaPorTubosV2.ExternalAccess = True
                FormChegadaPorTubosV2.Show
                FormChegadaPorTubosV2.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
                FormChegadaPorTubosV2.FuncaoProcurar
            Case Else
                FormChegadaPorTubos.Show
                FormChegadaPorTubos.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
                FormChegadaPorTubos.FuncaoProcurar
            End Select
    End If
End Sub

' pferreira 2010.11.09
Private Sub BtGestReq_Click()

    If FgReqPend.row > 0 And FgReqPend.row <= TotalDadosRequis Then
        If EstrutDadosRequis(FgReqPend.row).tipo_marcacao = lTipoPedidoRequisicao Then
            FormReqPedElectr.Enabled = False
            'NELSONPSILVA 04.04.2018 Glintt-HS-18011 (passagem de vari�vel global para registo de Log RGPD por context_info)
            FormGestaoRequisicao.ExternalAccess = True
            FormGestaoRequisicao.Show
            FormGestaoRequisicao.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
            FormGestaoRequisicao.FuncaoProcurar
            If gLAB = "HCVP" Then
                FormGestaoRequisicao.EcDataChegada = Bg_DaData_ADO
            End If
        ElseIf EstrutDadosRequis(FgReqPend.row).tipo_marcacao = lTipoPedidoMarcacao Then
            FormReqPedElectr.Enabled = False
            'NELSONPSILVA 04.04.2018 Glintt-HS-18011 (passagem de vari�vel global para registo de Log RGPD por context_info)
            FormGesReqCons.ExternalAccess = True
            FormGesReqCons.Show
            FormGesReqCons.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
            FormGesReqCons.FuncaoProcurar
        End If
    End If
    
End Sub


Private Sub BtImprimir_Click()
    Dim sql As String
    Dim continua As Boolean
    Dim i As Integer
    Dim totalMicro As Long
            
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Requisi��es Previstas") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFinal.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFinal.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    gImprimirDestino = 0
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ReqPendElectr", "Requisi��es Previstas", crptToPrinter)
    Else
        continua = BL_IniciaReport("ReqPendElectr", "Requisi��es Previstas", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    If TotalDadosRequis = 0 Then
        BG_Mensagem mediMsgBox, "N�o existem registos para imprimir!", vbInformation, Me.caption
        Exit Sub
    End If
    
    PreencheTabelaTemporaria
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = " SELECT SL_CR_REQPENDELECTR.N_REQ, SL_CR_REQPENDELECTR.DESCR_PROVEN, SL_CR_REQPENDELECTR.OBS_PROVEN, "
    Report.SQLQuery = Report.SQLQuery & " SL_CR_REQPENDELECTR.NOME, SL_CR_REQPENDELECTR.EPISODIO, SL_CR_REQPENDELECTR.T_EPISODIO "
    Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_REQPENDELECTR WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
    'F�rmulas do Report
    'Report.Formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.text)

    'Me.SetFocus
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Call BL_RemoveTabela("SL_CR_ESTATMICRORG" & gNumeroSessao)
    
End Sub



Private Sub BtPesquisaProveniencia_Click()
    PA_PesquisaProven EcCodProveniencia, EcDescrProveniencia, CStr(gCodLocal)
End Sub

Private Sub BtRefresh_Click()
    FuncaoProcurar False
End Sub



Private Sub Combo1_Change()

End Sub




Private Sub CkConsSemMarcacao_Click()
    If CkConsSemMarcacao.value = vbChecked Then
        CkReq.value = vbUnchecked
        CkReq.Enabled = False
        CkMarc.value = vbUnchecked
        CkMarc.Enabled = False
    Else
        CkReq.Enabled = True
        CkMarc.Enabled = True
    End If
End Sub
    
Private Sub CkDatas_Click()
    If CkDatas = vbChecked Then
        EcDtFinal.Enabled = True
        EcDtIni.Enabled = True
    Else
        EcDtFinal.Enabled = False
        EcDtIni.Enabled = False
    End If
    
End Sub



Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    PA_ValidateProven EcCodProveniencia, EcDescrProveniencia, CStr(gCodLocal)
End Sub

Private Sub EcNome_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FuncaoProcurar
    End If
End Sub

Private Sub EcPrescricao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And EcPrescricao <> "" Then
        FuncaoProcurar
    End If
End Sub

Private Sub FgReqPend_DblClick()
    If EstrutDadosRequis(FgReqPend.row).tipo_marcacao = lTipoPedidoRequisicao Then
        BtEntrTubos_Click
    ElseIf EstrutDadosRequis(FgReqPend.row).tipo_marcacao = lTipoPedidoMarcacao Then
        BtGestReq_Click
    End If
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    gF_REQPENDELECT = 1
    FuncaoProcurar True
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormReqPedElectr = Nothing
    gF_REQPENDELECT = 0
    CkDatas.value = vbUnchecked
    CkDatas_Click
    CkReq.value = vbChecked
    CkMarc.value = vbChecked
End Sub

Sub DefTipoCampos()
With FgReqPend
        .rows = 2
        .FixedRows = 1
        .Cols = 13
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = lColNReq
        
        .ColWidth(lColNReq) = 900
        .Col = lColNReq
        .TextMatrix(0, lColNReq) = "N.Req"
        
        .ColWidth(lcolEpis) = 1300
        .Col = lcolEpis
        .TextMatrix(0, lcolEpis) = "Epis�dio"
        
        .ColWidth(lColNome) = 2000
        .Col = lColNome
        .TextMatrix(0, lColNome) = "Nome"
        
        .ColWidth(lColProven) = 2000
        .Col = lColProven
        .TextMatrix(0, lColProven) = "Proveni�ncia"
        
        .ColWidth(lColObsProven) = 800
        .Col = lColObsProven
        .TextMatrix(0, lColObsProven) = "Obs. Proven"
        
        .ColWidth(lColDtPrevi) = 900
        .Col = lColDtPrevi
        .TextMatrix(0, lColDtPrevi) = "Dt Prevista"
        
        .ColWidth(lColDtCri) = 900
        .Col = lColDtCri
        .TextMatrix(0, lColDtCri) = "Dt Pedido"
        
        .ColWidth(lColHrCri) = 600
        .Col = lColHrCri
        .TextMatrix(0, lColHrCri) = "Hora"
        
        .ColWidth(lColUserCri) = 1400
        .Col = lColUserCri
        .TextMatrix(0, lColUserCri) = "Origem"
        
        .ColWidth(lColProdutos) = 1000
        .Col = lColProdutos
        .TextMatrix(0, lColProdutos) = "Produtos"
        
        .ColWidth(lColEstadoEtiq) = 1400
        .Col = lColEstadoEtiq
        .TextMatrix(0, lColEstadoEtiq) = "Etiquetas"
        
        .ColWidth(lColPrescricao) = 1000
        .Col = lColPrescricao
        .TextMatrix(0, lColPrescricao) = "Prescri��o"
        
        .ColWidth(lColGrAna) = 700
        .Col = lColGrAna
        .TextMatrix(0, lColGrAna) = "Gr.Ana."
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    If gNaoRefrescaReqPedElectr = mediSim Then
        CkRefresh.value = vbUnchecked
    Else
        CkRefresh.value = vbChecked
    End If
    EcNome.text = ""
End Sub

Sub PreencheValoresDefeito()
    Dim i As Integer
    EcDtIni.value = Bg_DaData_ADO
    EcDtFinal.value = Bg_DaData_ADO
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTUtente, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal, mediAscComboCodigo
    If gLAB <> "CHNE" Then
        For i = 0 To CbLocal.ListCount - 1
            If CbLocal.ItemData(i) = gCodLocal Then
                CbLocal.ListIndex = i
                Exit For
            End If
        Next
    End If
End Sub

Sub Inicializacoes()
    
    Me.caption = "Requisi��es Electr�nicas Pendentes"
    Me.left = 5
    Me.top = 5
    Me.Width = 15345
    Me.Height = 7015 ' Normal
    'Me.Height = 5505 ' Campos Extras
    
    Set CampoDeFocus = EcPrescricao
    EcEstado.locked = True
        
    LimpaCampos
End Sub

Sub FuncaoProcurar(Optional InibeMensagem As Boolean)
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    TotalDadosRequis = 0
    ReDim EstrutDadosRequis(0)
    sSql = ""
    If EcNome.text = "" And CkDatas.value = vbUnchecked And EcEpisodio = "" And EcPrescricao = "" And (CbTUtente.ListIndex = mediComboValorNull Or EcUtente.text = "") Then
        If InibeMensagem = False Then
            BG_Mensagem cr_mediMsgBox, "Ter� que introduzir um crit�rio para pesquisa", vbExclamation, "Procuar"
        End If
        Exit Sub
    End If
    If CkReq.value = vbChecked Then
        sSql = "SELECT x1.n_req, x1.estado_req, x1.dt_previ, x1.dt_cri, x1.hr_cri, x1.user_cri,x2.nome nome_user,x3.descr_proven,x1.obs_proven, x4.nome_ute, x1.n_epis, x1.t_sit, "
        sSql = sSql & " x1.t_urg, x1.cod_med, 'R' marcacao, x1.n_prescricao, x1.gr_ana  FROM sl_requis x1 LEFT OUTER JOIN sl_proven x3 ON x1.cod_proven = x3.cod_proven, sl_idutilizador x2,  " & tabela_aux & " x4 "
        sSql = sSql & " WHERE "
        sSql = sSql & " x1.dt_chega IS NULL "
        If gSGBD = gOracle Then
            'CHMA-1580 colocado to_char
            'CHUC -12855
            sSql = sSql & " AND x1.user_cri = to_char(x2.cod_utilizador) and x2.flg_electronico = 1 "
        ElseIf gSGBD = gPostGres Then
            sSql = sSql & " AND x1.user_cri =to_char(x2.cod_utilizador,'99999') and x2.flg_electronico = 1 "
        End If
        
        sSql = sSql & " AND  x1.seq_utente = x4.seq_utente AND x1.estado_req in(" & BL_TrataStringParaBD(gEstadoReqEsperaProduto) & "," & BL_TrataStringParaBD(gEstadoReqEsperaResultados) & " ) "
        If CkDatas.value = vbChecked Then
            sSql = sSql & " AND (dt_previ BETWEEN " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFinal.value) & " )"
        End If
        If EcCodProveniencia.text <> "" Then
            sSql = sSql & " AND x1.cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia.text)
        End If
        If EcNome.text <> "" Then
            sSql = sSql & " AND upper(x4.nome_ute) like '%" & UCase(EcNome) & "%'"
        End If
        If EcEpisodio <> "" Then
            sSql = sSql & " AND x1.t_sit = " & BG_DaComboSel(CbSituacao)
            sSql = sSql & " AND x1.n_epis = " & BL_TrataStringParaBD(EcEpisodio)
        End If
        If CbTUtente.ListIndex <> mediComboValorNull And EcUtente <> "" Then
            sSql = sSql & " AND x4.t_utente =" & BL_TrataStringParaBD(CbTUtente.List(CbTUtente.ListIndex))
            sSql = sSql & " AND x4.utente =" & BL_TrataStringParaBD(EcUtente)
        End If
        If EcSNS.text <> "" Then
            sSql = sSql & " AND n_cartao_ute = " & EcSNS.text
        End If
        If EcPrescricao <> "" Then
            If IsNumeric(EcPrescricao) Then
                sSql = sSql & " AND (x1.t_sit, x1.n_epis) IN (select x5.t_sit, x5.n_epis FROM sl_requis x5, sl_idutilizador x6, sl_req_episodio x7 WHERE x5.n_req = x7.n_req AND  (x5.n_prescricao = " & EcPrescricao & " OR x7.n_prescricao = " & EcPrescricao & ")"
                sSql = sSql & " AND x5.dt_chega IS NULL AND x5.user_cri = x6.cod_utilizador and x6.flg_electronico = 1)"
            End If
        End If
        If CbLocal.ListIndex > mediComboValorNull And gLAB <> "CHVNG" Then
            sSql = sSql & " AND x1.cod_local = " & BG_DaComboSel(CbLocal)
        End If
    End If
    If CkMarc.value = vbChecked Or CkConsSemMarcacao.value = vbChecked Then
        If sSql <> "" Then
            sSql = sSql & " UNION "
        End If
        
        sSql = sSql & " SELECT x1.n_req, x1.estado_req, x1.dt_previ, x1.dt_cri, x1.hr_cri, x1.user_cri,x2.nome nome_user,x3.descr_proven,x1.obs_proven, x4.nome_ute, x1.n_epis, x1.t_sit, "
        sSql = sSql & " x1.t_urg, x1.cod_med, 'M' marcacao, x1.n_prescricao, x1.gr_ana FROM SL_REQUIS_CONSULTAS x1 LEFT OUTER JOIN sl_proven x3 ON x1.cod_proven = x3.cod_proven, sl_idutilizador x2, " & tabela_aux & " x4 "
        sSql = sSql & " WHERE x1.dt_chega IS NULL "
        If gSGBD = gOracle Then
            'CHUC -12855
            sSql = sSql & " AND x1.user_cri = to_char(x2.cod_utilizador) and x2.flg_electronico = 1 "
        ElseIf gSGBD = gPostGres Then
            sSql = sSql & " AND x1.user_cri =to_char(x2.cod_utilizador,'99999') and x2.flg_electronico = 1 "
        End If
        sSql = sSql & " AND x1.seq_utente = x4.seq_utente AND x1.estado_req in(" & BL_TrataStringParaBD(gEstadoReqEsperaProduto) & "," & BL_TrataStringParaBD(gEstadoReqEsperaResultados) & " ) "
        If CkDatas.value = vbChecked Then
            sSql = sSql & " AND (dt_previ BETWEEN " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFinal.value) & " )"
        End If
        If EcCodProveniencia.text <> "" Then
            sSql = sSql & " AND x1.cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia.text)
        End If
        If EcNome.text <> "" Then
            sSql = sSql & " AND upper(x4.nome_ute) like '%" & UCase(EcNome) & "%'"
        End If
        If EcEpisodio <> "" Then
            sSql = sSql & " AND x1.t_sit = " & BG_DaComboSel(CbSituacao)
            sSql = sSql & " AND x1.n_epis = " & BL_TrataStringParaBD(EcEpisodio)
        End If
        If CbTUtente.ListIndex <> mediComboValorNull And EcUtente <> "" Then
            sSql = sSql & " AND x4.t_utente =" & BL_TrataStringParaBD(CbTUtente.List(CbTUtente.ListIndex))
            sSql = sSql & " AND x4.utente =" & BL_TrataStringParaBD(EcUtente)
        End If
        If EcSNS.text <> "" Then
            sSql = sSql & " AND n_cartao_ute = " & EcSNS.text
        End If
        If EcPrescricao <> "" Then
            If IsNumeric(EcPrescricao) Then
                sSql = sSql & " AND (x1.t_sit, x1.n_epis) IN (select x5.t_sit, x5.n_epis FROM sl_requis_consultas x5, sl_idutilizador x6, sl_req_episodio x7 WHERE x5.n_req = x7.n_req "
                sSql = sSql & " AND  (x5.n_prescricao = " & EcPrescricao & " OR x7.n_prescricao = " & EcPrescricao & ") AND x5.dt_chega IS NULL AND x5.user_cri = x6.cod_utilizador and x6.flg_electronico = 1)"
            End If
        End If
        If CbLocal.ListIndex > mediComboValorNull And gLAB <> "CHVNG" Then
            sSql = sSql & " AND x1.cod_local = " & BG_DaComboSel(CbLocal)
        End If
        If CkConsSemMarcacao.value = vbChecked Then
            sSql = sSql & " AND x1.dt_marcacao IS NULL "
        End If
                'CHMA-1580 09.04.2019
        sSql = sSql & " AND x1.flg_invisivel is null"
        '
    End If
    sSql = sSql & " ORDER BY dt_cri desc, hr_cri desc, n_prescricao"
    Set rsReq = BG_ExecutaSELECT(sSql)
    
    If rsReq.RecordCount > 0 Then
        While Not rsReq.EOF
            PreencheEstrutRequis BL_HandleNull(rsReq!n_req, ""), BL_DevolveEstadoReq(BL_HandleNull(rsReq!estado_req, " ")), _
                                 BL_HandleNull(rsReq!dt_previ, ""), BL_HandleNull(rsReq!dt_cri, ""), BL_HandleNull(rsReq!hr_cri, ""), _
                                 BL_HandleNull(rsReq!nome_user, ""), BL_HandleNull(rsReq!descr_proven, ""), BL_HandleNull(rsReq!obs_proven, ""), _
                                 BL_HandleNull(rsReq!nome_ute, ""), BL_HandleNull(rsReq!t_sit, ""), BL_HandleNull(rsReq!n_epis, ""), _
                                 BL_HandleNull(rsReq!t_urg, ""), BL_HandleNull(rsReq!cod_med, ""), BL_HandleNull(rsReq!marcacao, ""), _
                                 BL_HandleNull(rsReq!marcacao, ""), BL_HandleNull(rsReq!n_prescricao, mediComboValorNull), _
                                 BL_HandleNull(rsReq!nome_user, ""), BL_HandleNull(rsReq!gr_ana, "")
            rsReq.MoveNext
        Wend
        FgReqPend.Visible = False
        PreencheFgReqPend
        FgReqPend.Visible = True
        
        'NELSONPSILVA 04.04.2018 Glintt-HS-18011
        If gATIVA_LOGS_RGPD = mediSim Then
            Dim responseJson As String
            Dim requestJson As String
            Dim CamposEcra() As Object
            Dim NumCampos As Integer
            NumCampos = 6
            ReDim CamposEcra(0 To NumCampos - 1)
         Set CamposEcra(0) = CbTUtente
         Set CamposEcra(1) = EcUtente
         Set CamposEcra(2) = CbSituacao
         Set CamposEcra(3) = EcEpisodio
         Set CamposEcra(4) = EcNome
         Set CamposEcra(5) = EcSNS
            
            requestJson = BL_TrataCamposRGPD(CamposEcra)
            If CkDatas.value <> vbUnchecked Or requestJson <> "" Then
                If requestJson <> "" Then
                    requestJson = left$(requestJson, Len(requestJson) - 1)
                    requestJson = requestJson & ", " & Chr(34) & "EcDtIni" & Chr(34) & ":" & Chr(34) & EcDtIni.value & Chr(34) & ", " & Chr(34) & "EcDtFim" & Chr(34) & ":" & Chr(34) & EcDtFinal.value & Chr(34) & "}"
                Else
                    requestJson = "{" & Chr(34) & "EcDtIni" & Chr(34) & ":" & Chr(34) & EcDtIni.value & Chr(34) & ", " & Chr(34) & "EcDtFim" & Chr(34) & ":" & Chr(34) & EcDtFinal.value & Chr(34) & "}"
                End If
            End If
            'requestJson = left$(requestJson, Len(requestJson) - 1)
            'requestJson = requestJson & ", " & Chr(34) & "EcDtIni" & Chr(34) & ":" & Chr(34) & EcDtIni.value & Chr(34) & ", " & Chr(34) & "EcDtFinal" & Chr(34) & ":" & Chr(34) & EcDtFinal.value & Chr(34) & "} "

            responseJson = BL_TrataListBoxRGPD(Nothing, rsReq, False)
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        End If
        rsReq.Close
    Else
        LimpaFgReqPend
    End If
End Sub

Sub LimpaCampos()
    Dim i As Integer
    
    LimpaFgReqPend
    EcNome.text = ""
    EcDtIni.value = Bg_DaData_ADO
    EcDtFinal.value = Bg_DaData_ADO
    CbSituacao.ListIndex = mediComboValorNull
    CbTUtente.ListIndex = mediComboValorNull
    EcUtente = ""
    EcEpisodio.text = ""
    EcPrescricao = ""
    For i = 0 To CbLocal.ListCount - 1
        If CbLocal.ItemData(i) = gCodLocal Then
            CbLocal.ListIndex = i
            Exit For
        End If
    Next
    TotalDadosRequis = 0
    ReDim EstrutDadosRequis(0)
    
    'FuncaoProcurar
    CkDatas.value = vbUnchecked
    CkDatas_Click
    CkReq.value = vbChecked
    CkMarc.value = vbChecked

End Sub



Sub FuncaoLimpar()
    
    LimpaCampos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub LimpaFgReqPend()
    
    Dim j As Long
    Dim i As Long
    j = FgReqPend.rows - 1
    While j > 0
        If j > 1 Then
            FgReqPend.RemoveItem j
        Else
             For i = 0 To FgReqPend.Cols - 1
                FgReqPend.TextMatrix(j, i) = ""
            Next
        End If
        
        j = j - 1
    Wend
End Sub


Private Sub PreencheEstrutRequis(n_req As String, Estado As String, dt_previ As String, _
                                 dt_cri As String, hr_cri As String, user_cri As String, _
                                 Proven As String, obs_proven As String, nome_ute As String, _
                                 t_sit As String, n_epis As String, t_urg As String, _
                                 cod_medico As String, tipo_marcacao As String, marcacao As String, _
                                 n_prescricao As Long, nome_user As String, gr_ana As String)
                                 
    TotalDadosRequis = TotalDadosRequis + 1
    ReDim Preserve EstrutDadosRequis(TotalDadosRequis)
    EstrutDadosRequis(TotalDadosRequis).n_req = n_req
    EstrutDadosRequis(TotalDadosRequis).estado_req = Estado
    EstrutDadosRequis(TotalDadosRequis).Proven = Proven
    EstrutDadosRequis(TotalDadosRequis).obs_proven = obs_proven
    EstrutDadosRequis(TotalDadosRequis).dt_previ = dt_previ
    EstrutDadosRequis(TotalDadosRequis).dt_cri = dt_cri
    EstrutDadosRequis(TotalDadosRequis).hr_cri = hr_cri
    EstrutDadosRequis(TotalDadosRequis).nome = nome_ute
    EstrutDadosRequis(TotalDadosRequis).episoido = n_epis
    EstrutDadosRequis(TotalDadosRequis).t_episodio = t_sit
    EstrutDadosRequis(TotalDadosRequis).t_urg = t_urg
    EstrutDadosRequis(TotalDadosRequis).cod_medico = cod_medico
    EstrutDadosRequis(TotalDadosRequis).estado_etiq = BL_EstadoEtiq(n_req)
    EstrutDadosRequis(TotalDadosRequis).n_prescricao = n_prescricao
    EstrutDadosRequis(TotalDadosRequis).user_cri = user_cri
    EstrutDadosRequis(TotalDadosRequis).nome_user = nome_user
    EstrutDadosRequis(TotalDadosRequis).gr_ana = gr_ana
    PreencheGrAna TotalDadosRequis
    If marcacao = "M" Then
        EstrutDadosRequis(TotalDadosRequis).tipo_marcacao = lTipoPedidoMarcacao
    Else
        EstrutDadosRequis(TotalDadosRequis).tipo_marcacao = lTipoPedidoRequisicao
    End If
    EstrutDadosRequis(TotalDadosRequis).Produtos = PreencheProdutos(n_req, CStr(EstrutDadosRequis(TotalDadosRequis).tipo_marcacao))
End Sub


Private Sub PreencheFgReqPend()
    Dim i As Integer
    LimpaFgReqPend
    
    For i = 1 To TotalDadosRequis
        FgReqPend.TextMatrix(i, lColNReq) = EstrutDadosRequis(i).n_req
        FgReqPend.TextMatrix(i, lcolEpis) = Mid(BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", EstrutDadosRequis(i).t_episodio), 1, 3) & _
                                            "  " & EstrutDadosRequis(i).episoido
        FgReqPend.TextMatrix(i, lColNome) = EstrutDadosRequis(i).nome
        FgReqPend.TextMatrix(i, lColDtPrevi) = EstrutDadosRequis(i).dt_previ
        FgReqPend.TextMatrix(i, lColDtCri) = EstrutDadosRequis(i).dt_cri
        FgReqPend.TextMatrix(i, lColHrCri) = EstrutDadosRequis(i).hr_cri
        FgReqPend.TextMatrix(i, lColUserCri) = EstrutDadosRequis(i).nome_user
        FgReqPend.TextMatrix(i, lColProven) = EstrutDadosRequis(i).Proven
        FgReqPend.TextMatrix(i, lColObsProven) = EstrutDadosRequis(i).obs_proven
        FgReqPend.TextMatrix(i, lColProdutos) = EstrutDadosRequis(i).Produtos
        FgReqPend.TextMatrix(i, lColPrescricao) = EstrutDadosRequis(i).n_prescricao
        FgReqPend.TextMatrix(i, lColGrAna) = EstrutDadosRequis(i).descr_gr_ana
        If EstrutDadosRequis(i).estado_etiq = gEstadoEtiqNenhumaImpressa Then
            FgReqPend.TextMatrix(i, lColEstadoEtiq) = "Nenhuma Imp."
        ElseIf EstrutDadosRequis(i).estado_etiq = gEstadoEtiqAlgumasImpressas Then
            FgReqPend.TextMatrix(i, lColEstadoEtiq) = "Algumas Imp."
        ElseIf EstrutDadosRequis(i).estado_etiq = gEstadoEtiqTodasImpressas Then
            FgReqPend.TextMatrix(i, lColEstadoEtiq) = "Todas Imp."
        ElseIf EstrutDadosRequis(i).estado_etiq = gEstadoEtiqSemTubo Then
            FgReqPend.TextMatrix(i, lColEstadoEtiq) = "Sem Tubos"
        End If
        
        If EstrutDadosRequis(i).t_urg = "1" Then
            BL_MudaCorFg FgReqPend, CLng(i), lVermelho
        Else
            BL_MudaCorFg FgReqPend, CLng(i), vbWhite
        End If
        FgReqPend.AddItem ""
    Next
End Sub

Private Sub timer1_Timer()
    If CkRefresh.value = vbChecked Then
        FuncaoProcurar True
    End If
End Sub

Private Sub PreencheTabelaTemporaria()
    Dim sSql As String
    Dim i As Integer
    
    sSql = "DELETE FROM SL_CR_REQPENDELECTR WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To TotalDadosRequis
        sSql = "INSERT INTO SL_CR_REQPENDELECTR ( nome_computador, n_req, estado_req, dt_previ, dt_cri, hr_cri, user_cri, "
        sSql = sSql & " descr_proven, obs_proven, nome, episodio, t_episodio,descr_medico) VALUES( " & BL_TrataStringParaBD(gComputador) & ", "
        sSql = sSql & EstrutDadosRequis(i).n_req & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutDadosRequis(i).estado_req) & ", "
        sSql = sSql & BL_TrataDataParaBD(EstrutDadosRequis(i).dt_previ) & ", "
        sSql = sSql & BL_TrataDataParaBD(EstrutDadosRequis(i).dt_cri) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutDadosRequis(i).hr_cri) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutDadosRequis(i).user_cri) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutDadosRequis(i).Proven) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutDadosRequis(i).obs_proven) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutDadosRequis(i).nome) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutDadosRequis(i).episoido) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", EstrutDadosRequis(i).t_episodio)) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("sl_medicos", "descr_med", "cod_med", EstrutDadosRequis(i).cod_medico)) & ") "
        BG_ExecutaQuery_ADO sSql
    Next
End Sub

' ----------------------------------------------------------------------------------

' PREENCHER PRODUTOS ASSOCIADOS A REQUISICAO

' ----------------------------------------------------------------------------------
Private Function PreencheProdutos(n_req As String, tipo As String) As String
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    'NELSONPSILVA 16.01.2018 CHVNG-8946 (As query's estavam trocas quando era uma marca��o ou uma requisi��o)
    If tipo = lTipoPedidoMarcacao Then
        sSql = "SELECT cod_prod FROM sl_req_prod_consultas WHERE n_req = " & n_req
    ElseIf tipo = lTipoPedidoRequisicao Then
        sSql = "SELECT cod_prod FROM sl_req_prod WHERE n_req = " & n_req
    End If
    '
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    If RsProd.RecordCount > 0 Then
        PreencheProdutos = ""
        While Not RsProd.EOF
            PreencheProdutos = PreencheProdutos & BL_HandleNull(RsProd!cod_prod, "") & ";"
            RsProd.MoveNext
        Wend
    End If
    RsProd.Close
    Set RsProd = Nothing
Exit Function
TrataErro:
    PreencheProdutos = ""
    BG_LogFile_Erros "Erro  ao Preencher Produtos: " & Err.Description, Me.Name, "PreencheProdutos", True
    Exit Function
    Resume Next
End Function


Private Sub PreencheGrAna(indice As Integer)
    Dim grupos() As String
    Dim i As Integer
    Dim descr_gr_ana As String
    ReDim grupos(0)
    BL_Tokenize EstrutDadosRequis(indice).gr_ana, ";", grupos
    EstrutDadosRequis(indice).descr_gr_ana = ""
    For i = 0 To UBound(grupos)
        If grupos(i) <> "" Then
            If grupos(i) > 0 Then
                'NELSONPSILVA 23.01.2018 CHVNG-9008
                'EstrutDadosRequis(indice).descr_gr_ana = EstrutDadosRequis(indice).gr_ana
                EstrutDadosRequis(indice).descr_gr_ana = EstrutDadosRequis(indice).descr_gr_ana & Mid(BL_SelCodigo("SL_GR_ANA", "DESCR_GR_ANA", "COD_GR_ANA", grupos(i), "V"), 1, 3) & ";"
            End If
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PreencheGrAna: " & Err.Description, Me.Name, "PreencheGrAna", True
    Exit Sub
    Resume Next
End Sub

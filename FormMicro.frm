VERSION 5.00
Begin VB.Form FormMicro 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMicro"
   ClientHeight    =   6390
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8190
   Icon            =   "FormMicro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6390
   ScaleWidth      =   8190
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodSINAVE 
      Height          =   285
      Left            =   6960
      TabIndex        =   51
      Top             =   2160
      Width           =   945
   End
   Begin VB.ComboBox CbFraseMicro 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   49
      Top             =   2520
      Width           =   4185
   End
   Begin VB.CheckBox CkAlerta 
      Caption         =   "Emitir Alerta "
      Height          =   255
      Left            =   5760
      TabIndex        =   48
      Top             =   1800
      Width           =   2055
   End
   Begin VB.ComboBox CbMicrorgAssoc 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   46
      Top             =   2160
      Width           =   4185
   End
   Begin VB.TextBox EcCodMicroAssoc 
      Height          =   285
      Left            =   4080
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Cancelada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   305
      Left            =   4320
      TabIndex        =   43
      Top             =   120
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.TextBox EcAbrev 
      Height          =   285
      Left            =   5280
      TabIndex        =   41
      Top             =   120
      Width           =   2715
   End
   Begin VB.TextBox EcCodGrupo 
      Height          =   285
      Left            =   1560
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   8160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.ComboBox EcGrupoDesc 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   37
      Top             =   1680
      Width           =   4185
   End
   Begin VB.TextBox EcProva 
      Height          =   285
      Left            =   6600
      TabIndex        =   35
      Top             =   7800
      Width           =   1095
   End
   Begin VB.ComboBox CbProva 
      Height          =   315
      Left            =   6360
      Style           =   2  'Dropdown List
      TabIndex        =   33
      Top             =   480
      Width           =   1635
   End
   Begin VB.ComboBox EcMetodoTSQ 
      Height          =   315
      Left            =   6330
      Style           =   2  'Dropdown List
      TabIndex        =   31
      Top             =   1250
      Width           =   1635
   End
   Begin VB.ComboBox EcTipoMicro 
      Height          =   315
      Left            =   3840
      Style           =   2  'Dropdown List
      TabIndex        =   29
      Top             =   1250
      Width           =   1755
   End
   Begin VB.ComboBox EcIdProva 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   27
      Top             =   1250
      Width           =   1515
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   22
      Top             =   7800
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   21
      Top             =   7800
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   20
      Top             =   7440
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3720
      TabIndex        =   19
      Top             =   7440
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   120
      TabIndex        =   12
      Top             =   5520
      Width           =   7845
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   16
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   15
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   14
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   13
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.ComboBox EcGrMicroCombo 
      Height          =   315
      Left            =   2640
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   840
      Width           =   5355
   End
   Begin VB.TextBox EcCodGrMicro 
      Height          =   315
      Left            =   1440
      TabIndex        =   3
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   1425
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Top             =   480
      Width           =   4395
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6720
      TabIndex        =   0
      Top             =   7440
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   240
      TabIndex        =   5
      Top             =   3120
      Width           =   7845
   End
   Begin VB.Label Label20 
      Caption         =   "C�d. SINAVE"
      Height          =   225
      Left            =   5760
      TabIndex        =   52
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label18 
      Caption         =   "Frase Relat�rio"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   50
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label Label18 
      Caption         =   "Micro.Associado"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   47
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label Label39 
      Caption         =   "EcCodMicroAssoc"
      Height          =   255
      Index           =   1
      Left            =   2640
      TabIndex        =   45
      Top             =   8280
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label19 
      Caption         =   "Abreviatura"
      Height          =   255
      Left            =   4320
      TabIndex        =   42
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label39 
      Caption         =   "EcCodGrupo"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   40
      Top             =   8160
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label18 
      Caption         =   "Grupo"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   38
      Top             =   1680
      Width           =   735
   End
   Begin VB.Label Label17 
      Caption         =   "EcProva"
      Height          =   225
      Left            =   5880
      TabIndex        =   36
      Top             =   7800
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.Label Label16 
      Caption         =   "Prova"
      Height          =   255
      Left            =   5880
      TabIndex        =   34
      Top             =   480
      Width           =   495
   End
   Begin VB.Label Label15 
      Caption         =   "M�todo p/ TSQ"
      Height          =   495
      Index           =   0
      Left            =   5745
      TabIndex        =   32
      Top             =   1215
      Width           =   615
   End
   Begin VB.Label Label14 
      Caption         =   "Tipo Micro"
      Height          =   255
      Left            =   3000
      TabIndex        =   30
      Top             =   1260
      Width           =   855
   End
   Begin VB.Label Label13 
      Caption         =   "Id. Prova"
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   1260
      Width           =   855
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2520
      TabIndex        =   26
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2640
      TabIndex        =   25
      Top             =   7440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   0
      TabIndex        =   24
      Top             =   7800
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   0
      TabIndex        =   23
      Top             =   7440
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Grupo"
      Height          =   465
      Left            =   120
      TabIndex        =   11
      Top             =   840
      Width           =   645
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   3000
      TabIndex        =   10
      Top             =   2880
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   2880
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo "
      Height          =   225
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o "
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   5280
      TabIndex        =   6
      Top             =   7440
      Visible         =   0   'False
      Width           =   1365
   End
End
Attribute VB_Name = "FormMicro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub ChRemovidos_Click()
    If ListarRemovidos = False Then
        ListarRemovidos = True
    Else
        ListarRemovidos = False
    End If
End Sub

Private Sub EcCodGrMicro_GotFocus()

    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodGrMicro_LostFocus()

    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodGrMicro.Text = UCase(EcCodGrMicro.Text)
    BL_ColocaTextoCombo "sl_gr_microrg", "seq_gr_microrg", "cod_gr_microrg", EcCodGrMicro, EcGrMicroCombo
    
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()

    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()

    BG_ValidaTipoCampo_ADO Me, CampoActivo
    
End Sub

Private Sub EcAbrev_GotFocus()

    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcAbrev_LostFocus()

    BG_ValidaTipoCampo_ADO Me, CampoActivo
    
End Sub

Private Sub EcGrMicroCombo_Click()

    BL_ColocaComboTexto "sl_gr_microrg", "seq_gr_microrg", "cod_gr_microrg", EcCodGrMicro, EcGrMicroCombo

End Sub

Private Sub EcGrMicroCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcGrMicroCombo.ListIndex = -1
End Sub


Private Sub EcLista_Click()
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub


Private Sub CbProva_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbProva, KeyCode

End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Microrganismos"
    Me.left = 540
    Me.top = 450
    Me.Width = 8415
    Me.Height = 6810 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_microrg"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 19
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_microrg"
    CamposBD(1) = "cod_microrg"
    CamposBD(2) = "descr_microrg"
    CamposBD(3) = "cod_gr_microrg"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "id_prova"
    CamposBD(9) = "tipo_microrg"
    CamposBD(10) = "prova"
    CamposBD(11) = "metodo_tsq"
    CamposBD(12) = "gr_ana"
    CamposBD(13) = "abrev_microrg"
    CamposBD(14) = "flg_invisivel"
    CamposBD(15) = "cod_microrg_assoc"
    CamposBD(16) = "flg_alerta"
    CamposBD(17) = "cod_frase_micro"
    'BRUNODSANTOS 30.03.2017 - Glintt-HS-14510
    CamposBD(18) = "cod_sinave"
    '
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcCodGrMicro
    Set CamposEc(4) = EcUtilizadorCriacao
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = EcIdProva
    Set CamposEc(9) = EcTipoMicro
    Set CamposEc(10) = CbProva
    Set CamposEc(11) = EcMetodoTSQ
    Set CamposEc(12) = EcCodGrupo
    Set CamposEc(13) = EcAbrev
    Set CamposEc(14) = CkInvisivel
    Set CamposEc(15) = EcCodMicroAssoc
    Set CamposEc(16) = CkAlerta
    Set CamposEc(17) = CbFraseMicro
    'BRUNODSANTOS 30.03.2017 - Glintt-HS-14510
    Set CamposEc(18) = EcCodSinave
    '
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Microrganismo"
    TextoCamposObrigatorios(2) = "Descri��o do Microrganismo"
    TextoCamposObrigatorios(10) = "Prova"
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_microrg"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_microrg", "descr_microrg")
    NumEspacos = Array(22, 43)
        
    CkInvisivel.value = vbGrayed
    CkInvisivel.Visible = False
End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    
     If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormMicro = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcGrMicroCombo.ListIndex = mediComboValorNull
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcIdProva.ListIndex = mediComboValorNull
    EcTipoMicro.ListIndex = mediComboValorNull
    CbProva.ListIndex = mediComboValorNull
    EcMetodoTSQ.ListIndex = mediComboValorNull
    EcGrupoDesc.ListIndex = mediComboValorNull
    CkInvisivel.value = vbGrayed
    CkInvisivel.Visible = False
    CbMicrorgAssoc.ListIndex = mediComboValorNull
    CkAlerta.value = vbGrayed
End Sub

Sub DefTipoCampos()
       
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData

    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub
Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_gr_microrg", "seq_gr_microrg", "descr_gr_microrg", EcGrMicroCombo, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_tbf_t_microrg", "cod_t_microrg", "descr_t_microrg", EcTipoMicro
    BG_PreencheComboBD_ADO "sl_tbf_id_prova", "cod_id_prova", "descr_id_prova", EcIdProva
    BG_PreencheComboBD_ADO "sl_tbf_metodo_tsq", "cod_metodo_tsq", "descr_metodo_tsq", EcMetodoTSQ
    
    BG_PreencheComboBD_ADO "sl_tbf_t_prova", "cod_t_prova", "descr_t_prova", CbProva
    BG_PreencheComboBD_ADO "sl_cod_frase_micro", "cod_frase_micro", "descr_frase_micro", CbFraseMicro
    
    PreencheComboGrupoDef
    CkAlerta.value = vbGrayed
End Sub
Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_ColocaTextoCombo "sl_gr_microrg", "seq_gr_microrg", "cod_gr_microrg", EcCodGrMicro, EcGrMicroCombo
        BL_ColocaTextoCombo "sl_microrg", "seq_microrg", "cod_microrg", EcCodMicroAssoc, CbMicrorgAssoc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        PreencheComboGrupo
        If CkInvisivel.value = 1 Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
    End If
    
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        EcLista.Clear
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_microrg ASC, descr_microrg ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        EcLista.Clear
        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
'        EcProva.Text = Mid(CbProva.Text, 1, 1)
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0

    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_microrg") + 1
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
        
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
'        EcProva.Text = Mid(CbProva.Text, 1, 1)
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub


Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    'Para os valores de refer�ncia
    BG_RollbackTransaction
    BG_BeginTransaction
    
    'Apaga os registos das Tabelas relacionadas
    'Call ApagaRelacoes(rs!cod_ana_s)
    
    'Apaga o registo da an�lise
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = "UPDATE sl_microrg SET flg_invisivel = 1, " & _
                " user_act = '" & gCodUtilizador & "'," & _
                " dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
                "  WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    'Confirma a elimina��o
    BG_BeginTransaction
    
    'Temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
     'A Tabela Ficou Vazia?
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    
    'Era o �ltimo Registo?
    If rs.EOF Then
        rs.MovePrevious
    Else
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    End If
    
    LimpaCampos
    PreencheCampos

End Sub
Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

'Sub BD_Delete()
'
'    Dim condicao As String
'    Dim SQLQuery As String
'    Dim MarcaLocal As Variant
'
'    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
'    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
'    BG_ExecutaQuery_ADO SQLQuery
'
'    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
'    MarcaLocal = rs.Bookmark
'    rs.Requery
'
'    If rs.BOF And rs.EOF Then
'        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
'        FuncaoEstadoAnterior
'        Exit Sub
'    End If
'
'    ' Inicio do preenchimento de 'EcLista'
'    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
'    ' Fim do preenchimento de 'EcLista'
'
'    If MarcaLocal <= EcLista.ListCount Then
'        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
'        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
'    Else
'        rs.MoveLast
'    End If
'
'    If rs.EOF Then rs.MovePrevious
'
'    LimpaCampos
'    PreencheCampos
'End Sub

Sub PreencheComboGrupo()
    
    BL_ColocaTextoCombo "sl_gr_ana", "seq_gr_ana", "cod_gr_ana", EcCodGrupo, EcGrupoDesc

End Sub

Sub PreencheComboGrupoDef()
    
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", EcGrupoDesc, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_microrg", "seq_microrg", "descr_microrg", CbMicrorgAssoc, mediAscComboDesignacao

End Sub
Private Sub EcGrupoDesc_Click()
    
    BL_ColocaComboTexto "sl_gr_ana", "seq_gr_ana", "cod_gr_ana", EcCodGrupo, EcGrupoDesc

End Sub

Private Sub EcGrupoDesc_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcGrupoDesc.ListIndex = -1

End Sub


Private Sub CbMicrorgAssoc_click()
    If CbMicrorgAssoc.ListIndex > mediComboValorNull Then
        EcCodMicroAssoc = BL_SelCodigo("sl_microrg", "cod_microrg", "seq_microrg", CbMicrorgAssoc.ItemData(CbMicrorgAssoc.ListIndex))
    Else
        EcCodMicroAssoc = ""
    End If

End Sub
Private Sub CbMicrorgAssoc_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then
        CbMicrorgAssoc.ListIndex = -1
        EcCodMicroAssoc = ""
    End If

End Sub


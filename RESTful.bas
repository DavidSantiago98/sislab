Attribute VB_Name = "RESTful"
Option Explicit

Public Function REST_webservice(strQuery As String) As String
    
    Dim response As String
    Dim sURL As String
    Dim seq_log_rnu As Long
    'Dim xmlhttp As XMLHTTP60
    Dim xmlhttp As WinHttp.WinHttpRequest
    
    
    sURL = BL_DevolveURLWebService("PORTAL_ERESULTS")
    
    If sURL = "" Then Exit Function
    
    'Set xmlhttp = New XMLHTTP60
    Set xmlhttp = New WinHttp.WinHttpRequest
    
    xmlhttp.Open "POST", sURL, False
    xmlhttp.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    If Len(strQuery) > 0 Then
        xmlhttp.SetRequestHeader "Content-Length", Len(strQuery)
        seq_log_rnu = RNU_InsereLog(strQuery, sURL)
        If seq_log_rnu > mediComboValorNull Then
            xmlhttp.Send strQuery
        End If
    End If
    
    Do While Not xmlhttp.WaitForResponse
        DoEvents
    Loop
    response = xmlhttp.ResponseText
    RNU_ActualizaLog seq_log_rnu, response
    REST_webservice = response
    Set xmlhttp = Nothing
End Function

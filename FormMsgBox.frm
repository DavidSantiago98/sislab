VERSION 5.00
Begin VB.Form FormMsgBox 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMsgBox"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5415
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "FormMsgBox.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   5415
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Bt 
      Caption         =   "9"
      Height          =   375
      Index           =   9
      Left            =   3600
      TabIndex        =   10
      Top             =   3720
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "8"
      Height          =   375
      Index           =   8
      Left            =   2760
      TabIndex        =   9
      Top             =   3720
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "7"
      Height          =   375
      Index           =   7
      Left            =   1920
      TabIndex        =   8
      Top             =   3720
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "6"
      Height          =   375
      Index           =   6
      Left            =   1080
      TabIndex        =   7
      Top             =   3720
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "5"
      Height          =   375
      Index           =   5
      Left            =   240
      TabIndex        =   6
      Top             =   3720
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "4"
      Height          =   375
      Index           =   4
      Left            =   3600
      TabIndex        =   5
      Top             =   3360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "3"
      Height          =   375
      Index           =   3
      Left            =   2760
      TabIndex        =   4
      Top             =   3360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "2"
      Height          =   375
      Index           =   2
      Left            =   1920
      TabIndex        =   3
      Top             =   3360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "1"
      Height          =   375
      Index           =   1
      Left            =   1080
      TabIndex        =   2
      Top             =   3360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Bt 
      Caption         =   "0"
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   3360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcTexto 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   1080
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   4125
   End
   Begin VB.Image ImgError 
      Height          =   480
      Left            =   120
      Picture         =   "FormMsgBox.frx":000C
      Top             =   4440
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image ImgExclamation 
      Height          =   480
      Left            =   120
      Picture         =   "FormMsgBox.frx":044E
      Top             =   5040
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image ImgInfo 
      Height          =   480
      Left            =   840
      Picture         =   "FormMsgBox.frx":0890
      Top             =   4440
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image ImgQuestion 
      Height          =   480
      Left            =   840
      Picture         =   "FormMsgBox.frx":0CD2
      Top             =   5040
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image ImgIcon 
      Height          =   480
      Left            =   180
      Picture         =   "FormMsgBox.frx":1114
      Top             =   120
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "FormMsgBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 24/10/2002
' T�cnico Paulo Costa

Dim Opt As Integer

Public Function CaixaMensagem(Texto As String, _
                              NBotoes As Integer, _
                              CapBotoes() As String, _
                              Optional Titulo As Variant, _
                              Optional Tipo As Variant, _
                              Optional NLinhasTexto As Variant) As Integer

    ' Texto : Mensagem a transmitir
    ' NBotoes : N�mero de bot�es (1 a 10)
    ' CapBot�es() : Array de strings com as captions dos bot�es (tamanho = NBot�es)
    ' Titulo : titulo do form
    ' Tipo :  vbExclamation , vbInformation, vbError, vbQuestion
    ' NLinhasTexto: permite superar a margem de erro no calculo da altura da caixa de texto

    Dim i As Integer
    Dim Icon As Integer
    Dim NLinhas As Integer

    FormMsgBox.Width = 5500
    FormMsgBox.Left = 1500
    FormMsgBox.Top = 1500

    'Titulo da message box
    If IsMissing(Titulo) Then
        FormMsgBox.Caption = "..."
    Else
        FormMsgBox.Caption = Titulo
    End If

    'Ajustar Icon
    If IsMissing(Tipo) Then
        Icon = -1
    Else
        Icon = Tipo
    End If
    ImgIcon.Visible = True
    Select Case Icon
        Case vbExclamation
            ImgIcon.Picture = ImgExclamation.Picture
        Case vbInformation
            ImgIcon.Picture = ImgInfo.Picture
        Case vbError
            ImgIcon.Picture = ImgError.Picture
        Case vbQuestion
            ImgIcon.Picture = ImgQuestion.Picture
        Case Else
            ImgIcon.Visible = False
            EcTexto.Left = EcTexto.Left - 800
            FormMsgBox.Width = FormMsgBox.Width - 850
    End Select

    'Ajustar caixa de texto
    EcTexto.Text = Texto
    EcTexto.Width = 4000
    If IsMissing(NLinhasTexto) Then
        '   Altura : 4000 de Width suporta +/- 22 caracteres, assim calcula-se +/- o n� de linhas
        '            O calculo falha quando se utiliza textos compridos em letra maiuscula
        EcTexto.Height = 200 + (100 * (Len(Texto) / 22))
    Else
        EcTexto.Height = NLinhasTexto * 200
    End If

    'Ajustar bot�es
    NBotoes = NBotoes - 1
    If NBotoes = -1 Then
        Bt(0).Visible = True
        Bt(0).Caption = "    Sair    "
    End If
    For i = 0 To NBotoes
        If i = 10 Then Exit For
        Bt(i).Visible = True
        If i < UBound(CapBotoes) Then
            If CapBotoes(i + 1) = "" Then CapBotoes(i + 1) = "(" & i & ")"
            Bt(i).Caption = CapBotoes(i + 1)
            Bt(i).Width = 150 + (150 * Len(CapBotoes(i + 1)))
            If Bt(i).Width > FormMsgBox.Width - 1000 Then
                Bt(i).Width = FormMsgBox.Width - 1000
            End If
        End If
    Next i

    ' defenir posi��o dos bot�es
    i = 0
    NLinhas = 0
    If NBotoes = 0 Then
        'apenas 1 bot�o (centra-se)
        Bt(i).Left = (FormMsgBox.Width / 2) - (Bt(i).Width / 2)
        Bt(i).Top = EcTexto.Top + EcTexto.Height + 100
    Else
        Bt(i).Left = (FormMsgBox.Width / 4) - 500
        Bt(i).Top = EcTexto.Top + EcTexto.Height + 100
        While i < NBotoes
            If (Bt(i).Left + Bt(i).Width + 100 + Bt(i + 1).Width) > FormMsgBox.Width - 300 Then
                NLinhas = NLinhas + 1
                Bt(i + 1).Left = (FormMsgBox.Width / 4) - 500
            Else
                Bt(i + 1).Left = Bt(i).Left + Bt(i).Width + 100
            End If
            Bt(i + 1).Top = EcTexto.Top + EcTexto.Height + 100 + (400 * NLinhas)
            i = i + 1
        Wend
    End If
    FormMsgBox.Height = Bt(i).Top + Bt(i).Height + 500
    FormMsgBox.Show vbModal

    CaixaMensagem = Opt

End Function

Private Sub Bt_Click(Index As Integer)
    
    Opt = Index + 1
    Unload FormMsgBox

End Sub

Private Sub EcTexto_GotFocus()
    
    Bt(0).SetFocus

End Sub

Private Sub Form_Activate()
    
    Bt(0).SetFocus

End Sub




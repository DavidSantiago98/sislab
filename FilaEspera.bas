Attribute VB_Name = "FilaEspera"
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''
' Constante que define menor de idade ''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Const FE_MINORAGE = 13
''''''''''''''''''''''''''''''''''''''''''''
' Constantes de prioridades ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Const FE_UNKNOWN = 1
Public Const FE_NORMAL = 2
Public Const FE_HIGH = 3
Public Const FE_HIGHEST = 4

''''''''''''''''''''''''''''''''''''''''''''
' Estrutura do utente ''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Type filaEspera
    n_req As String
    nome As String
    t_utente As String
    Utente As String
    seq_utente As String
    cod_local As String
    cod_sala As String
    prioridade As String
    dt_cri As String
    dt_estado As String
    user_cri As String
    user_estado As String
    flg_estado As String
    dt_nasc As String
    idade As String
    Sexo As String
    morada As String
    destino As String
    Entidade As String
    Medico As String
    hora_nova_colheita As String
End Type

''''''''''''''''''''''''''''''''''''''''''''
' Vector de utentes ''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Global gEstrutUteFilaEspera() As filaEspera
Global gTotalFilaEspera As Integer

Public Enum gEstadoFilaEspera
    espera = 0
    bloqueado = 1
    cancelado = 2
    concluido = 3
    novaColheita = 4
End Enum

Public Function FE_InsereFilaEspera(ByVal n_req As String, ByVal seq_utente As Long, ByVal cod_sala As String, _
                                    ByVal cod_t_colheita As String, ByVal CodProven As String) As Boolean
    Dim sql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    FE_InsereFilaEspera = True
    
    If gUsaFilaEspera <> mediSim Then
        Exit Function
    End If
    
    If FE_VerificaSalaFilaEspera(cod_sala) = False Then
        Exit Function
    End If

    FE_InsereFilaEspera = False
    
    
    sql = "INSERT INTO sl_fila_espera (n_req, seQ_utente, cod_local, cod_sala, prioridade, dt_cri, user_cri, flg_estado, dt_estado, user_estado)"
    sql = sql & " VALUES (" & n_req & "," & seq_utente & "," & gCodLocal & "," & BL_HandleNull(cod_sala, "NULL") & "," & cod_t_colheita & ", "
    sql = sql & "sysdate," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & gEstadoFilaEspera.espera & ","
    sql = sql & "sysdate," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    iReg = BG_ExecutaQuery_ADO(sql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    FE_InsereFilaEspera = True

Exit Function
TrataErro:
    FE_InsereFilaEspera = False
    BG_LogFile_Erros "Erro ao inserir em filaEspera: -> " & Err.Description & " " & sql, "FE", "FE_InsereFilaEspera ", True
    Exit Function
    Resume Next
End Function

Private Function FE_VerificaSalaFilaEspera(cod_sala As String) As Boolean
    On Error GoTo Trata_Erro
    Dim rsSala As New ADODB.recordset
    Dim sSql As String
    If (cod_sala = "") Then
        FE_VerificaSalaFilaEspera = True
    Else
        sSql = "select flg_colheita from sl_cod_salas where seq_sala = " & cod_sala
        rsSala.CursorLocation = adUseServer
        rsSala.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSala.Open sSql, gConexao
        FE_VerificaSalaFilaEspera = IIf(rsSala.RecordCount > 0, CBool(rsSala!flg_colheita), False)
        If (rsSala.state = adStateOpen) Then: rsSala.Close
    End If
Exit Function
Trata_Erro:
    FE_VerificaSalaFilaEspera = False
    BG_LogFile_Erros "Erro ao Verificar Sala para filaEspera: -> " & Err.Description & " ", "FE", "FE_VerificaSalaFilaEspera", True
    If (rsSala.state = adStateOpen) Then: rsSala.Close
    Exit Function
    Resume Next
End Function

Private Function FE_VerificaProvenFilaEspera(cod_proven As String) As Boolean
    On Error GoTo Trata_Erro
    Dim rsSala As New ADODB.recordset
    Dim sSql As String
    If (cod_proven = "") Then
        FE_VerificaProvenFilaEspera = True
    Else
        sSql = "select flg_filaespera from sl_proven where cod_proven = " & cod_proven
        rsSala.CursorLocation = adUseServer
        rsSala.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSala.Open sSql, gConexao
        FE_VerificaProvenFilaEspera = IIf(rsSala.RecordCount > 0, CBool(BL_HandleNull(rsSala!flg_filaespera, 0)), False)
        If (rsSala.state = adStateOpen) Then: rsSala.Close
    End If
Exit Function
Trata_Erro:
    FE_VerificaProvenFilaEspera = False
    BG_LogFile_Erros "Erro ao Verificar Proven para filaEspera: -> " & Err.Description & " ", "FE", "FE_VerificaProvenFilaEspera", True
    If (rsSala.state = adStateOpen) Then: rsSala.Close
    Exit Function
    Resume Next
End Function


Public Function FE_AtualizaEstadoFilaEspera(indice As Integer, flg_estado As Integer, cod_sala As String) As Boolean
    Dim iReg As String
    Dim sSql As String
    On Error GoTo TrataErro
    FE_AtualizaEstadoFilaEspera = False
    gEstrutUteFilaEspera(indice).flg_estado = flg_estado
    gEstrutUteFilaEspera(indice).user_estado = CStr(gCodUtilizador)
    gEstrutUteFilaEspera(indice).dt_estado = Bg_DaDataHora_ADO
    gEstrutUteFilaEspera(indice).cod_sala = cod_sala
    
    sSql = "UPDATE sl_fila_espera SET flg_estado = " & flg_estado & ", dt_estado = sysdate, user_estado = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
    sSql = sSql & ", cod_sala = " & cod_sala & " WHERE n_req = " & gEstrutUteFilaEspera(indice).n_req
    
    Select Case flg_estado
        Case gEstadoFilaEspera.bloqueado
            sSql = sSql & " AND flg_estado = " & gEstadoFilaEspera.espera
        Case gEstadoFilaEspera.concluido
            sSql = sSql & " AND flg_estado in( " & gEstadoFilaEspera.espera & ", " & gEstadoFilaEspera.bloqueado & ", " & gEstadoFilaEspera.novaColheita & ")"
    End Select
    iReg = BG_ExecutaQuery_ADO(sSql)
    
    If iReg <> 1 Then
        FE_AtualizaEstadoFilaEspera = False
    Else
        FE_InsereTracking gEstrutUteFilaEspera(indice).n_req, flg_estado
        FE_AtualizaEstadoFilaEspera = True
        If flg_estado = gEstadoFilaEspera.concluido Then
            sSql = "UPDATE SL_REQUIS set cod_sala = " & cod_sala & " WHERE n_req = " & gEstrutUteFilaEspera(indice).n_req & " AND cod_sala IS NULL"
            BG_ExecutaQuery_ADO sSql
        End If
    End If
    
Exit Function
TrataErro:
    FE_AtualizaEstadoFilaEspera = False
    BG_LogFile_Erros "Erro ao atualizar estado em filaEspera: -> " & Err.Description & " " & sSql, "FE", "FE_AtualizaEstadoFilaEspera ", True
    Exit Function
    Resume Next
End Function


Public Function FE_AtualizaEstadoFilaEspera2(n_req As String, flg_estado As Integer, cod_sala As String, nova_colheita As String) As Boolean
    Dim iReg As String
    Dim sSql As String
    On Error GoTo TrataErro
    
    FE_AtualizaEstadoFilaEspera2 = False
    
    
    sSql = "UPDATE sl_fila_espera SET flg_estado = " & flg_estado & ", dt_estado = sysdate, user_estado = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
    sSql = sSql & ", cod_sala = " & BL_HandleNull(cod_sala, "NULL") & ", hora_nova_colheita = " & BL_TrataStringParaBD(nova_colheita)
    sSql = sSql & " WHERE n_req = " & n_req
    
    Select Case flg_estado
        Case gEstadoFilaEspera.bloqueado
            sSql = sSql & " AND flg_estado = " & gEstadoFilaEspera.espera
        Case gEstadoFilaEspera.concluido
            sSql = sSql & " AND flg_estado in( " & gEstadoFilaEspera.espera & ", " & gEstadoFilaEspera.bloqueado & ", " & gEstadoFilaEspera.novaColheita & ")"
    End Select
    iReg = BG_ExecutaQuery_ADO(sSql)
    
    If iReg = 1 Then
        FE_InsereTracking n_req, flg_estado
        If flg_estado = gEstadoFilaEspera.concluido And cod_sala <> "" Then
            sSql = "UPDATE SL_REQUIS set cod_sala = " & cod_sala & " WHERE n_req = " & n_req & " AND cod_sala IS NULL"
            BG_ExecutaQuery_ADO sSql
        End If
    End If
    FE_AtualizaEstadoFilaEspera2 = True
    
Exit Function
TrataErro:
    FE_AtualizaEstadoFilaEspera2 = False
    BG_LogFile_Erros "Erro ao atualizar estado em filaEspera2: -> " & Err.Description & " " & sSql, "FE", "FE_AtualizaEstadoFilaEspera2 ", True
    Exit Function
    Resume Next
End Function

Public Function FE_AtualizaProximaColheita(n_req As String) As Boolean
    Dim sSql As String
    Dim rsT As New ADODB.recordset
    On Error GoTo TrataErro
    
    FE_AtualizaProximaColheita = False
    
    sSql = "SELECT * FROM sl_req_tubo WHERE n_req = " & n_req & " AND dt_colheita IS NULL and hr_previ_colheita IS NOT NULL ORDER BY hr_previ_colheita ASC "
    rsT.CursorLocation = adUseServer
    rsT.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsT.Open sSql, gConexao
    If rsT.RecordCount >= 1 Then
        'gMsgTitulo = "Nova Colheita "
        'gMsgMsg = "Tem tubos n�o colhidos em agenda. Pretende manter Utente em fila de espera?"
        'gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        'If gMsgResp = vbYes Then
            FE_AtualizaProximaColheita = FE_AtualizaEstadoFilaEspera2(n_req, gEstadoFilaEspera.novaColheita, "", BL_HandleNull(rsT!hr_previ_colheita, ""))
        'Else
        '    FE_AtualizaProximaColheita = True
        'End If
    End If
    rsT.Close
    Set rsT = Nothing
Exit Function
TrataErro:
    FE_AtualizaProximaColheita = False
    BG_LogFile_Erros "Erro ao atualizar hora da proxima colheita: -> " & Err.Description & " " & sSql, "FE", "FE_AtualizaProximaColheita"
    Exit Function
    Resume Next
End Function
Private Function FE_InsereTracking(n_req As String, flg_estado As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = "INSERT INTO sl_fila_espera_tracking (n_req, dt_cri, user_cri, flg_estado) VALUES("
    sSql = sSql & n_req & ", sysdate, " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & flg_estado & ")"
    BG_ExecutaQuery_ADO sSql
    FE_InsereTracking = True
Exit Function
TrataErro:
    FE_InsereTracking = False
    BG_LogFile_Erros "Erro ao atualizar inserir tracking: -> " & Err.Description & " " & sSql, "FE", "FE_InsereTracking ", True
    Exit Function
    Resume Next
End Function
Public Function FE_DevolveQueryFilaEspera(modo As Integer, cod_sala As String) As String
    Dim sSql As String
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    On Error GoTo TrataErro
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    sSql = "SELECT x3.n_req, x1.dt_nasc_ute, x1.nome_ute, x3.prioridade, x3.seq_utente, x3.cod_local, x2.cod_sala, x3.dt_cri"
    sSql = sSql & ", x3.dt_estado, x3.flg_estado, X1.sexo_ute , X1.descr_mor_ute, X2.cod_destino, X2.cod_efr, X2.cod_med,x3.user_cri, x3.user_estado, "
    sSql = sSql & " x3.hora_nova_colheita "
    sSql = sSql & " FROM " & tabela_aux & " x1, sl_requis x2, sl_fila_espera x3 WHERE x1.seq_utente = x3.seq_utente and x2.n_req = x3.n_req "
    sSql = sSql & " AND trunc(x3.dt_cri)  =" & BL_TrataDataParaBD(Bg_DaData_ADO) & " And x3.cod_local = " & gCodLocal
    sSql = sSql & " AND (x2.cod_sala IS NULL OR x2.cod_sala = " & cod_sala & " ) "
    
    ' Filtra o modo de procura
    Select Case modo
        ' Caso em que os utentes ja estao ordenados
        Case 0
            sSql = sSql & " AND flg_estado IN( " & gEstadoFilaEspera.espera & "," & gEstadoFilaEspera.bloqueado & "," & gEstadoFilaEspera.novaColheita & ")"
            sSql = sSql & " ORDER BY prioridade DESC, dt_cri ASC "
                
        ' Caso em que os utentes ja foram chamados
        Case 1
            sSql = sSql & " AND flg_estado IN( " & gEstadoFilaEspera.cancelado & "," & gEstadoFilaEspera.concluido & ")"
            sSql = sSql & " ORDER BY prioridade DESC, dt_estado ASC "
    End Select
    FE_DevolveQueryFilaEspera = sSql
Exit Function
TrataErro:
    FE_DevolveQueryFilaEspera = ""
    BG_LogFile_Erros "Erro ao construir fila espera: -> " & Err.Description & " " & sSql, "FE", "FE_DevolveQueryFilaEspera ", True
    Exit Function
    Resume Next
End Function


''''''''''''''''''''''''''''''''''''''''''''
' Verifica se esta sala � de crian�as ''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Function FE_CheckSalaCriancas(cod_sala As String) As Boolean

    Dim rsSala As ADODB.recordset
    Set rsSala = New ADODB.recordset
    FE_CheckSalaCriancas = CBool(0)
    
    ' Procura flag de sala de criancas
    rsSala.Open "SELECT flg_criancas FROM sl_cod_salas where cod_sala = " & cod_sala, _
                gConexao, adOpenStatic, adLockReadOnly
   If (rsSala.RecordCount > 0) Then: FE_CheckSalaCriancas = CBool(BL_HandleNull(rsSala!flg_criancas, 0))
    
    ' Fecha recordset
    If (rsSala.state = adStateOpen) Then: rsSala.Close
End Function

''''''''''''''''''''''''''''''''''''''''''''
' Verifica se corresponde a uma crianca ''''
''''''''''''''''''''''''''''''''''''''''''''
Private Function IsChild(data As String) As Boolean
    
    ' Data actual menos a data de nascimento tem de ser menor ou igual a CG_MINORAGE
    IsChild = Mid(BG_CalculaIdade(CDate(data)), 1, InStr(1, BG_CalculaIdade(CDate(data)), " ") - 1) <= FE_MINORAGE
End Function

''''''''''''''''''''''''''''''''''''''''''''
' Verifica analises em determinadas salas ''
''''''''''''''''''''''''''''''''''''''''''''
Private Function FE_VerifySalaAnalises(NReq As String) As Boolean

    Dim rsAna As ADODB.recordset
    Set rsAna = New ADODB.recordset
    Dim rsSala As ADODB.recordset
    Set rsSala = New ADODB.recordset
    Dim res As Boolean
    res = False
    
    ' Consulta analises para esta sala
    rsSala.Open "SELECT analises FROM sl_cod_salas WHERE seq_sala = " & gCodSala, _
                gConexao, adOpenStatic, adLockReadOnly
    
    ' Se nao existir analises especificadas para esta sala said a funcao (TRUE)
    If (rsSala.RecordCount = 0 Or IsNull(rsSala!analises)) Then: res = True: GoTo Exit_function
    
    ' Consulta analises para esta requisicao
    rsAna.Open "SELECT DISTINCT cod_agrup FROM sl_marcacoes WHERE n_req = " & NReq, _
                gConexao, adOpenStatic, adLockReadOnly
    
    ' Se encontrar analises que estejam especificadas para esta sala sai da funcao (TRUE)
    While (Not rsAna.EOF)
        If (InStr(1, BL_HandleNull(rsSala!analises, ""), _
            BL_HandleNull(rsAna!cod_agrup, ""), vbTextCompare) <> 0) Then
            res = True
            GoTo Exit_function
        End If
        rsAna.MoveNext
    Wend
    
Exit_function:

    ' Fecha conexoes
    If (rsSala.state = adStateOpen) Then: rsSala.Close
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    FE_VerifySalaAnalises = res
End Function

''''''''''''''''''''''''''''''''''''''''''''
' Preenche a estrutura de fila de espera '''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FE_PopulateEstrutura(RsStack As ADODB.recordset, cod_sala As String)
    
    Dim position As Integer
    Dim Index As Integer
    Dim prioridade
    gTotalFilaEspera = 0
    ReDim gEstrutUteFilaEspera(0)
    Index = 0
    RsStack.MoveFirst

jump:
    ' Percorre o recordset
    While (Not RsStack.EOF)
        prioridade = BL_HandleNull(RsStack!prioridade, FE_NORMAL)
        
        ' Se a sala nao for de criancas exclui utentes criancas
        If (Not FE_CheckSalaCriancas(BL_HandleNull(RsStack!cod_sala, cod_sala)) And IsChild(BL_HandleNull(RsStack!dt_nasc_ute, ""))) Then
            RsStack.MoveNext
            GoTo jump
        ElseIf FE_CheckSalaCriancas(BL_HandleNull(RsStack!cod_sala, cod_sala)) And IsChild(BL_HandleNull(RsStack!dt_nasc_ute, "")) Then
            prioridade = FE_HIGHEST
        End If
        
        ' Se existirem analises definidas nesta sala exclui utentes que nao tenham marcacoes para essas analises
        If (Not FE_VerifySalaAnalises(BL_HandleNull(RsStack!n_req, ""))) Then
            RsStack.MoveNext
            GoTo jump
        End If
        
        
        ' Insere na estrutura de utentes
        FE_InsertUtente BL_HandleNull(RsStack!n_req, ""), BL_HandleNull(RsStack!nome_ute, ""), _
                     BL_HandleNull(RsStack!seq_utente, ""), CStr(prioridade), _
                     BL_HandleNull(RsStack!cod_local, ""), BL_HandleNull(RsStack!cod_sala, ""), _
                     BL_HandleNull(RsStack!dt_cri, ""), BL_HandleNull(RsStack!dt_estado, ""), _
                     BL_HandleNull(RsStack!user_cri, ""), BL_HandleNull(RsStack!user_estado, ""), _
                     BL_HandleNull(RsStack!dt_nasc_ute, ""), IIf(BL_HandleNull(RsStack!sexo_ute, 0) = 0, "F", "M"), _
                     BL_HandleNull(RsStack!descr_mor_ute, ""), BL_HandleNull(RsStack!cod_destino, ""), _
                     BL_HandleNull(RsStack!cod_efr, ""), BL_HandleNull(RsStack!cod_med, ""), _
                     BL_HandleNull(RsStack!flg_estado, 0), BL_HandleNull(RsStack!hora_nova_colheita, "")

        ' Avan�a no recordset
        Index = Index + 1
        RsStack.MoveNext
        
    Wend
End Sub


Private Sub FE_InsertUtente(req As String, nome As String, seq_utente As String, prioridade As String, _
                        cod_local As String, cod_sala As String, dt_cri As String, _
                        dt_estado As String, user_cri As String, user_estado As String, _
                        DtNasc As String, Sexo As String, _
                        morada As String, destino As String, Entidade As String, Medico As String, _
                        flg_estado As String, hora_nova_colheita As String)
    

    
    ' Aumenta o tamanho da estrutura
    gTotalFilaEspera = gTotalFilaEspera + 1
    ReDim Preserve gEstrutUteFilaEspera(gTotalFilaEspera)
    
    ' Insere utente na posicao
    gEstrutUteFilaEspera(gTotalFilaEspera).n_req = req
    gEstrutUteFilaEspera(gTotalFilaEspera).nome = nome
    gEstrutUteFilaEspera(gTotalFilaEspera).seq_utente = seq_utente
    gEstrutUteFilaEspera(gTotalFilaEspera).Utente = BL_SelCodigo("SL_IDENTIF", "UTENTE", "SEQ_UTENTE", seq_utente)
    gEstrutUteFilaEspera(gTotalFilaEspera).t_utente = BL_SelCodigo("SL_IDENTIF", "T_UTENTE", "SEQ_UTENTE", seq_utente)
    gEstrutUteFilaEspera(gTotalFilaEspera).cod_local = cod_local
    gEstrutUteFilaEspera(gTotalFilaEspera).cod_sala = cod_sala
    gEstrutUteFilaEspera(gTotalFilaEspera).prioridade = prioridade
    gEstrutUteFilaEspera(gTotalFilaEspera).dt_cri = dt_cri
    gEstrutUteFilaEspera(gTotalFilaEspera).dt_estado = dt_estado
    gEstrutUteFilaEspera(gTotalFilaEspera).user_cri = user_cri
    gEstrutUteFilaEspera(gTotalFilaEspera).user_estado = user_estado
    gEstrutUteFilaEspera(gTotalFilaEspera).flg_estado = flg_estado
    gEstrutUteFilaEspera(gTotalFilaEspera).dt_nasc = DtNasc
    gEstrutUteFilaEspera(gTotalFilaEspera).idade = BG_CalculaIdade(CDate(DtNasc), CDate(dt_cri))
    gEstrutUteFilaEspera(gTotalFilaEspera).Sexo = Sexo
    gEstrutUteFilaEspera(gTotalFilaEspera).morada = morada
    gEstrutUteFilaEspera(gTotalFilaEspera).Entidade = Entidade
    gEstrutUteFilaEspera(gTotalFilaEspera).destino = destino
    gEstrutUteFilaEspera(gTotalFilaEspera).Medico = Medico
    gEstrutUteFilaEspera(gTotalFilaEspera).hora_nova_colheita = hora_nova_colheita
    
End Sub




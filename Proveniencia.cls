VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Proveniencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private seq_proven As Long
Private cod_proven As String
Private descr_proven As String

Public Property Get DevolveDescricao()
    DevolveDescricao = descr_proven
End Property

Public Property Get DevolveCodigo()
    DevolveCodigo = cod_proven
End Property

Public Property Get DevolveSeq()
    DevolveSeq = seq_proven
End Property

Public Property Set PreencheSeq(ByVal SEQ As Long)
    seq_proven = SEQ
End Property

Public Property Set PreencheCod(ByVal cod As String)
    cod_proven = cod
End Property

Public Property Let PreencheDescr(ByVal descr As String)
    descr_proven = descr
End Property



VERSION 5.00
Begin VB.Form FormReqPrev 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mapa de Requisi��es Previstas"
   ClientHeight    =   1335
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6345
   Icon            =   "FormReqPrev.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1335
   ScaleWidth      =   6345
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbUrgencia 
      Height          =   315
      Left            =   3720
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   840
      Width           =   1335
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   840
      Width           =   1335
   End
   Begin VB.TextBox EcDtFim 
      Height          =   285
      Left            =   3720
      TabIndex        =   1
      Top             =   240
      Width           =   1455
   End
   Begin VB.TextBox EcDtIni 
      Height          =   285
      Left            =   1080
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "Prioridade"
      Height          =   255
      Left            =   2880
      TabIndex        =   7
      Top             =   840
      Width           =   735
   End
   Begin VB.Label Label6 
      Caption         =   "Situa��o"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   840
      Width           =   735
   End
   Begin VB.Label lblDataFim 
      Caption         =   "Data Fim"
      Height          =   255
      Left            =   2880
      TabIndex        =   5
      Top             =   240
      Width           =   735
   End
   Begin VB.Label lblDataInicio 
      Caption         =   "Data Inicio"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   240
      Width           =   855
   End
End
Attribute VB_Name = "FormReqPrev"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 19/07/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.
Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Type GR
    cod_grupo As String
    conta As Integer
    tamanho As Integer
End Type

Private Sub DefTipoCampos()

    'Tipo Data
    EcDtIni.Tag = adDate
    EcDtFim.Tag = adDate
    
    'Tipo Inteiro
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    CbSituacao.AddItem ""
    
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    CbUrgencia.AddItem ""
    
End Sub

Public Function Funcao_DataActual()
       
    Select Case CampoActivo.Name
        Case "EcDtIni", "EcDtFim"
            CampoActivo.Text = Bg_DaData_ADO
    End Select

End Function


Private Sub CbSituacao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
        
    If KeyCode = 46 And Shift = 0 Then CbSituacao.ListIndex = -1

End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1

End Sub

Private Sub EcDtFim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDtIni_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtIni)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub



Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar ecran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Private Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    EcDtIni.SetFocus
    Set CampoActivo = EcDtIni
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormReqPrev = Nothing
    
    Call BL_FechaPreview("Mapa Requisi��es Previstas")
     
End Sub

Private Sub Inicializacoes()

    Me.caption = " Mapa de Requisi��es Previstas"
    Me.left = 540
    Me.top = 450
    Me.Width = 6375
    Me.Height = 1815
   
    Set CampoDeFocus = EcDtIni
       
End Sub

Private Sub PreencheValoresDefeito()
       
    EcDtIni.Text = Bg_DaData_ADO
    EcDtFim.Text = Bg_DaData_ADO
        
End Sub

Public Sub FuncaoLimpar()
    
    Me.SetFocus
    EcDtIni.SetFocus
    
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    
End Sub

Private Sub CriaTabReqPrev()

    Dim TmpReqPrev(1 To 16) As DefTable
    
    TmpReqPrev(1).NomeCampo = "Empresa_Id"
    TmpReqPrev(1).tipo = "STRING"
    TmpReqPrev(1).tamanho = 8
    
    TmpReqPrev(2).NomeCampo = "N_req"
    TmpReqPrev(2).tipo = "INTEGER"
    
    TmpReqPrev(3).NomeCampo = "Nome_ute"
    TmpReqPrev(3).tipo = "STRING"
    TmpReqPrev(3).tamanho = 80
    
    TmpReqPrev(4).NomeCampo = "Tipo_ute"
    TmpReqPrev(4).tipo = "STRING"
    TmpReqPrev(4).tamanho = 25
    
    TmpReqPrev(5).NomeCampo = "Contagem_grupos"
    TmpReqPrev(5).tipo = "STRING"
    TmpReqPrev(5).tamanho = 300
    
    TmpReqPrev(6).NomeCampo = "Proveniencia"
    TmpReqPrev(6).tipo = "STRING"
    TmpReqPrev(6).tamanho = 40
    
    TmpReqPrev(7).NomeCampo = "Situacao"
    TmpReqPrev(7).tipo = "STRING"
    TmpReqPrev(7).tamanho = 1
    
    TmpReqPrev(8).NomeCampo = "Urgencia"
    TmpReqPrev(8).tipo = "STRING"
    TmpReqPrev(8).tamanho = 1
    
    TmpReqPrev(9).NomeCampo = "Produtos"
    TmpReqPrev(9).tipo = "STRING"
    TmpReqPrev(9).tamanho = 100
    
    TmpReqPrev(10).NomeCampo = "Data_ent"
    TmpReqPrev(10).tipo = "DATE"

    TmpReqPrev(11).NomeCampo = "Motivo"
    TmpReqPrev(11).tipo = "STRING"
    TmpReqPrev(11).tamanho = 100
    
    TmpReqPrev(12).NomeCampo = "Req_aux"
    TmpReqPrev(12).tipo = "STRING"
    TmpReqPrev(12).tamanho = 20
    
    TmpReqPrev(13).NomeCampo = "Obs"
    TmpReqPrev(13).tipo = "STRING"
    TmpReqPrev(13).tamanho = 10
    
    TmpReqPrev(14).NomeCampo = "Data_saida"
    TmpReqPrev(14).tipo = "STRING"
    TmpReqPrev(14).tamanho = 10
    
    TmpReqPrev(15).NomeCampo = "Resultado"
    TmpReqPrev(15).tipo = "STRING"
    TmpReqPrev(15).tamanho = 60
    
    TmpReqPrev(16).NomeCampo = "Responsavel"
    TmpReqPrev(16).tipo = "STRING"
    TmpReqPrev(16).tamanho = 20
    
    Call BL_CriaTabela("SL_CR_RP" & gNumeroSessao, TmpReqPrev)

End Sub

Public Sub FuncaoImprimir()
    
    'Requisi��es
    Dim rs As ADODB.recordset
    
    'Produtos
    Dim CmdProd As ADODB.Command
    Dim PmtProd As ADODB.Parameter
    Dim RsProd As ADODB.recordset
    'Vari�vel (STRING) que guarda os produtos de uma requisi��o
    Dim STRProduto As String
    
    'Marca��es
    Dim CmdReq As ADODB.Command
    Dim PmtReq As ADODB.Parameter
    Dim rsReq As ADODB.recordset
    
    'Grupos
    'Descri��o do grupo das an�lises Simples
    Dim CmdS As ADODB.Command
    Dim PmtS As ADODB.Parameter
    'Descri��o do grupo das an�lises Complexas
    Dim CmdC As ADODB.Command
    Dim PmtC As ADODB.Parameter
    'Descri��o do grupo das an�lises Perfis
    Dim CmdP As ADODB.Command
    Dim PmtP As ADODB.Parameter
    'Descri��o do Grupo da Marca��o
    Dim RsGr As ADODB.recordset
    'Descri��o dos Grupos (Cabe�alho)
    Dim rsGrupo As ADODB.recordset
    'Grupos
    Dim grupos() As GR
    Dim ContaGR As Integer
    'Vari�vel (STRING) que guarda os grupos
    Dim STRGrupos As String
    'Vari�vel (STRING) que guarda a contagem dos grupos para uma requisi��o
    Dim STRContagrupos As String
    
    'Dados do utente
    Dim CmdNome As ADODB.Command
    Dim PmtNome As ADODB.Parameter
    Dim RsNome As ADODB.recordset
    Dim TipoUtente As String
    
    'Situa��o do Utente
    Dim CmdSit As ADODB.Command
    Dim PmtSit As ADODB.Parameter
    Dim RsSit As ADODB.recordset
    
    'Proveni�ncia do Utente
    Dim CmdProven As ADODB.Command
    Dim PmtProven As ADODB.Parameter
    Dim RsProven As ADODB.recordset
    Dim Proven As String
    
    'Diagn�sticos da requisi��o (Motivos)
    Dim CmdDiag As ADODB.Command
    Dim PmtDiag As ADODB.Parameter
    Dim RsDiag As ADODB.recordset
    'Vari�vel (STRING) que guarda os diagn�sticos de uma requisi��o (motivos)
    Dim STRDiag As String
    
    'Resultado do tipo frase da an�lise cari�tipo (apenas para o IGM)
    Dim CmdResFrase As ADODB.Command
    Dim PmtResFrase As ADODB.Parameter
    Dim RsResFrase As ADODB.recordset
    'Resultado do tipo alfanumerico da an�lise cari�tipo(data de saida) (apenas para o IGM)
    Dim CmdResAlfan As ADODB.Command
    Dim PmtResAlfan As ADODB.Parameter
    Dim RsResAlfan As ADODB.recordset
    'Observa��es do Resultado da an�lise cari�tipo (apenas para o IGM)
    Dim CmdResObs As ADODB.Command
    Dim PmtResObs As ADODB.Parameter
    Dim RsResObs As ADODB.recordset
    'Vari�veis que guardam os resultados das an�lises
    Dim ResCariotipo As String
    Dim DataSaidaCar As String
    
    'M�dico responsavel pelo cariotipo (apenas para o IGM)
    Dim CmdMed As ADODB.Command
    Dim PmtMed As ADODB.Parameter
    Dim RsMed As ADODB.recordset
    'Vari�vel que guarda o respons�vel pela an�lise
    Dim MedResp As String
    
    Dim continua As Boolean
    Dim i As Integer
    Dim TotalLinha As Integer
    Dim LenCodigo As Integer
    Dim LenConta As Integer
    Dim SQLQuery As String
    Dim sql As String
    

    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Mapa Requisi��es Previstas") = True Then Exit Sub
    
    '2.Verifica se os campos obrigat�rios est�o nulos
    
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da Requisi��o.", vbOKOnly + vbInformation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    
     If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da Requisi��o.", vbOKOnly + vbInformation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
        
    '3.C�lculo da SqlQuery para inser��o dos registos na tabela tempor�ria
    SQLQuery = "SELECT sl_requis.n_req,req_aux,Seq_utente,sl_requis.Dt_previ,sl_requis.dt_chega,Cod_Proven,T_sit,T_urg,obs_proven,cod_med From sl_requis "
    
    If gMultiReport = 1 Then
        SQLQuery = SQLQuery & " WHERE sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & _
                                BL_TrataDataParaBD(EcDtFim.Text) & " AND estado_req<>'C'"
    Else
        SQLQuery = SQLQuery & " WHERE sl_requis.dt_previ between " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & _
                                BL_TrataDataParaBD(EcDtFim.Text) & " AND estado_req<>'C'"
    End If
    
    If gMultiReport <> 1 Then
        SQLQuery = SQLQuery & " AND dt_chega IS NULL "
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> CbUrgencia.ListCount - 1) And (CbUrgencia.ListIndex <> -1) Then
        SQLQuery = SQLQuery & " AND T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
    End If
    
    'Tipo de Situa��o preenchido?
    If (CbSituacao.ListIndex <> CbSituacao.ListCount - 1) And (CbSituacao.ListIndex <> -1) Then
        SQLQuery = SQLQuery & " AND T_sit=" & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    
    If gMultiReport = 1 Then
        SQLQuery = SQLQuery & " ORDER BY req_aux"
    End If
    
            
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.ActiveConnection = gConexao
    rs.Source = SQLQuery
    rs.Open
    If rs.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbOKOnly + vbInformation, App.ProductName
'        FuncaoLimpar
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
           
    '4.Inicia o processo de impress�o do relat�rio
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        If gMultiReport <> 1 Then
            continua = BL_IniciaReport("MapaRequisPrev", "Mapa Requisi��es Previstas", crptToPrinter)
        Else
            continua = BL_IniciaReport("MapaRequisPrevIGM", "Mapa Requisi��es Previstas", crptToPrinter)
        End If
    Else
        If gMultiReport <> 1 Then
            continua = BL_IniciaReport("MapaRequisPrev", "Mapa Requisi��es Previstas", crptToWindow)
        Else
            continua = BL_IniciaReport("MapaRequisPrevIGM", "Mapa Requisi��es Previstas", crptToWindow)
        End If
    End If
    If continua = False Then Exit Sub
     
    'O utilizador decidiu continuar o processo de impress�o
     
    '*************************************************************************
   '.Defini��o dos Comandos e Par�metros
    'Inicializa��o do comando que vai buscar o cod_prod � tabela sl_req_prod para uma determinada requisi��o
    Set CmdProd = New ADODB.Command
    Set CmdProd.ActiveConnection = gConexao
    CmdProd.CommandType = adCmdText
    CmdProd.CommandText = "SELECT cod_prod FROM sl_req_prod WHERE n_req=?"
    CmdProd.Prepared = True
    Set PmtProd = CmdProd.CreateParameter("N_REQ", adInteger, adParamInput)
    CmdProd.Parameters.Append PmtProd
    
    'Inicializa��o do comando que vai buscar o codigo de grupo(an�lise simples,complexa ou perfil)(cod_agrup) �s marca��es
    Set CmdReq = New ADODB.Command
    Set CmdReq.ActiveConnection = gConexao
    CmdReq.CommandType = adCmdText
    CmdReq.CommandText = "SELECT cod_agrup FROM sl_marcacoes WHERE n_req=?"
    CmdReq.Prepared = True
    Set PmtReq = CmdReq.CreateParameter("N_REQ", adInteger, adParamInput)
    CmdReq.Parameters.Append PmtReq
    
    'Inicializa��o de comando que vai buscar o grupo de uma an�lise simples
    Set CmdS = New ADODB.Command
    Set CmdS.ActiveConnection = gConexao
    CmdS.CommandType = adCmdText
    CmdS.CommandText = "SELECT gr_ana FROM sl_ana_s WHERE cod_ana_s=?"
    CmdS.Prepared = True
    Set PmtS = CmdS.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    CmdS.Parameters.Append PmtS
    
    'Inicializa��o de comando que vai buscar o grupo de uma an�lise complexa
    Set CmdC = New ADODB.Command
    Set CmdC.ActiveConnection = gConexao
    CmdC.CommandType = adCmdText
    CmdC.CommandText = "SELECT gr_ana FROM sl_ana_c WHERE cod_ana_c=?"
    CmdC.Prepared = True
    Set PmtC = CmdC.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    CmdC.Parameters.Append PmtC
    
    'Inicializa��o de comando que vai buscar o grupo de um perfil
    Set CmdP = New ADODB.Command
    Set CmdP.ActiveConnection = gConexao
    CmdP.CommandType = adCmdText
    CmdP.CommandText = "SELECT gr_ana FROM sl_perfis WHERE cod_perfis=?"
    CmdP.Prepared = True
    Set PmtP = CmdP.CreateParameter("COD_PERFIS", adVarChar, adParamInput, 7)
    CmdP.Parameters.Append PmtP
    
    'Inicializa��o do Comando que vai buscar o nome do utente usando o sequencial do utente na requisi��o
    Set CmdNome = New ADODB.Command
    Set CmdNome.ActiveConnection = gConexao
    CmdNome.CommandType = adCmdText
    CmdNome.CommandText = "SELECT nome_ute,utente,t_utente FROM sl_identif WHERE seq_utente=?"
    CmdNome.Prepared = True
    Set PmtNome = CmdNome.CreateParameter("SEQ_UTENTE", adInteger, adParamInput)
    CmdNome.Parameters.Append PmtNome
    
    'Inicializa��o do Comando da descri��o da Situa��o
    Set CmdSit = New ADODB.Command
    Set CmdSit.ActiveConnection = gConexao
    CmdSit.CommandType = adCmdText
    CmdSit.CommandText = "SELECT descr_t_sit FROM sl_tbf_t_sit WHERE cod_t_sit=?"
    CmdSit.Prepared = True
    Set PmtSit = CmdSit.CreateParameter("COD_T_SIT", adInteger, adParamInput)
    CmdSit.Parameters.Append PmtSit
        
    'Inicializa��o do Comando da descri��o da Proveni�ncia
    Set CmdProven = New ADODB.Command
    Set CmdProven.ActiveConnection = gConexao
    CmdProven.CommandType = adCmdText
    CmdProven.CommandText = "SELECT descr_proven FROM sl_proven WHERE cod_proven=?"
    CmdProven.Prepared = True
    Set PmtProven = CmdProven.CreateParameter("COD_PROVEN", adVarChar, adParamInput, 10)
    CmdProven.Parameters.Append PmtProven
    
    'Inicializa��o do comando que vai buscar o descr_diag � tabela sl_diag_sec para uma determinada requisi��o
    Set CmdDiag = New ADODB.Command
    Set CmdDiag.ActiveConnection = gConexao
    CmdDiag.CommandType = adCmdText
    CmdDiag.CommandText = "SELECT descr_diag FROM sl_diag, sl_diag_sec WHERE " & _
                         " sl_diag.cod_diag = sl_diag_sec.cod_diag and n_req=?"
    CmdDiag.Prepared = True
    Set PmtDiag = CmdDiag.CreateParameter("N_REQ", adInteger, adParamInput)
    CmdDiag.Parameters.Append PmtDiag
    
    'Inicializa��o do comando que vai buscar os resultados �s tabela sl_res_frase da an�lise cariotipo   'Apenas usado pra o IGM
    Set CmdResFrase = New ADODB.Command
    Set CmdResFrase.ActiveConnection = gConexao
    CmdResFrase.CommandType = adCmdText
    CmdResFrase.CommandText = "SELECT descr_frase " & _
                         " FROM sl_dicionario, sl_res_frase,sl_realiza WHERE " & _
                         " sl_realiza.seq_realiza = sl_res_frase.seq_realiza(+) and " & _
                         " sl_dicionario.cod_frase = sl_res_frase.cod_frase and " & _
                         " sl_realiza.cod_ana_s = 'SC' and n_req=?"
    CmdResFrase.Prepared = True
    Set PmtResFrase = CmdResFrase.CreateParameter("N_REQ", adInteger, adParamInput)
    CmdResFrase.Parameters.Append PmtResFrase
    
    'Inicializa��o do comando que vai buscar os resultados �s tabela sl_res_alfan da an�lise cariotipo   'Apenas usado pra o IGM
    Set CmdResAlfan = New ADODB.Command
    Set CmdResAlfan.ActiveConnection = gConexao
    CmdResAlfan.CommandType = adCmdText
    CmdResAlfan.CommandText = "SELECT result " & _
                         " FROM sl_res_alfan, sl_realiza WHERE " & _
                         " sl_realiza.seq_realiza = sl_res_alfan.seq_realiza and " & _
                         " sl_realiza.cod_ana_s = 'SC' and n_req=?"
    CmdResAlfan.Prepared = True
    Set PmtResAlfan = CmdResAlfan.CreateParameter("N_REQ", adInteger, adParamInput)
    CmdResAlfan.Parameters.Append PmtResAlfan
    
    'Inicializa��o do comando que vai buscar as observa��es dos resultados �s tabela sl_obs_ana da an�lise cariotipo   'Apenas usado pra o IGM
    Set CmdResObs = New ADODB.Command
    Set CmdResObs.ActiveConnection = gConexao
    CmdResObs.CommandType = adCmdText
    CmdResObs.CommandText = "SELECT descr_obs_ana " & _
                         " FROM sl_obs_ana, sl_realiza WHERE " & _
                         " sl_realiza.seq_realiza = sl_obs_ana.seq_realiza and " & _
                         " sl_realiza.cod_ana_s = 'SC' and n_req=?"
    CmdResObs.Prepared = True
    Set PmtResObs = CmdResObs.CreateParameter("N_REQ", adInteger, adParamInput)
    CmdResObs.Parameters.Append PmtResObs
    
    'Inicializa��o do comando que vai buscar o respons�vel pelo cariotipo � tabela sl_medicos   'Apenas usado pra o IGM
    Set CmdMed = New ADODB.Command
    Set CmdMed.ActiveConnection = gConexao
    CmdMed.CommandType = adCmdText
    CmdMed.CommandText = "SELECT nome_med FROM sl_medicos WHERE cod_med=?"
    CmdMed.Prepared = True
    Set PmtMed = CmdProven.CreateParameter("COD_MED", adVarChar, adParamInput, 10)
    CmdMed.Parameters.Append PmtMed
    
    
    '*************************************************************************
    'Inicializa��o da estrutura para o cabe�alho=>STRING GRUPOS
     'Concatenar os Grupos de an�lises
    sql = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana where cod_local = 1 and cod_gr_ana <> '99'"
    Set rsGrupo = New ADODB.recordset
    rsGrupo.CursorLocation = adUseServer
    rsGrupo.CursorType = adOpenStatic
    rsGrupo.Source = sql
    rsGrupo.ActiveConnection = gConexao
    rsGrupo.Open
    ContaGR = rsGrupo.RecordCount
    ReDim Preserve grupos(1 To ContaGR)
    STRGrupos = ""
    ContaGR = 0
    For i = 1 To rsGrupo.RecordCount
        STRGrupos = STRGrupos + Mid(rsGrupo!descr_gr_ana, 1, 6) + "   "
        ContaGR = ContaGR + 1
    
        grupos(ContaGR).cod_grupo = Trim(rsGrupo!cod_gr_ana)
        grupos(ContaGR).conta = 0
        grupos(ContaGR).tamanho = Len(Trim(Mid(rsGrupo!descr_gr_ana, 1, 6)))
        
        rsGrupo.MoveNext
    Next i
    STRGrupos = STRGrupos + "TOTAL"
    rsGrupo.Close
    Set rsGrupo = Nothing
    
'************************************************************************
    
    'Cria a tabela tempor�ria para a inser��o dos registos
    Call CriaTabReqPrev
    
    While Not rs.EOF
        '****************************************************************
        'Para cada Requisi��o calcula relativamente ao cabe�alho os contadores do Grupo
        
        'Limpa o contador do Vector Grupos a 0
        For i = 1 To ContaGR
            grupos(i).conta = 0
        Next i
        'Abre o RecordSet para os Grupos de uma determinada requisi��o
        CmdReq.Parameters(0).value = rs!n_req
        Set rsReq = CmdReq.Execute
        'Para cada Grupo da Marca��o devolve a respectiva descri��o (1 registo)
        While Not rsReq.EOF
            Select Case Mid(rsReq!cod_agrup, 1, 1)
                Case "S"
                    'Simples
                    CmdS.Parameters(0).value = rsReq!cod_agrup
                    Set RsGr = CmdS.Execute
                Case "C"
                    'Complexa
                    CmdC.Parameters(0).value = rsReq!cod_agrup
                    Set RsGr = CmdC.Execute
                Case "P"
                    'Perfil
                    CmdP.Parameters(0).value = rsReq!cod_agrup
                    Set RsGr = CmdP.Execute
            End Select
            i = 1
            While RsGr!gr_ana <> grupos(i).cod_grupo And i < ContaGR - 1
                i = i + 1
            Wend
            If i > ContaGR Then
                'Grupo novo
            Else
                grupos(i).conta = grupos(i).conta + 1
            End If
            rsReq.MoveNext
            If Not RsGr Is Nothing Then
                If RsGr.state <> adStateClosed Then
                    RsGr.Close
                End If
                Set RsGr = Nothing
            End If
        Wend
    
        'Construir a string de contagem de grupos e o total por linha
        STRContagrupos = ""
        TotalLinha = 0
        For i = 1 To ContaGR
            STRContagrupos = STRContagrupos + Trim(str(grupos(i).conta))
            STRContagrupos = STRContagrupos + Space((grupos(i).tamanho - Len(Trim(str(grupos(i).conta)))) + 3)
            TotalLinha = TotalLinha + grupos(i).conta
        Next i
        STRContagrupos = STRContagrupos + str(TotalLinha)
        
        '****************************************************************
            
        'Constroi a string de produtos
        CmdProd.Parameters(0).value = rs!n_req
        Set RsProd = CmdProd.Execute
        STRProduto = ""
        While Not RsProd.EOF
            STRProduto = STRProduto + RsProd!cod_prod + ";"
            RsProd.MoveNext
        Wend
        
        '****************************************************************
        
        'Calcula o Nome e o Tipo de Utente
        CmdNome.Parameters(0).value = rs!seq_utente
        Set RsNome = CmdNome.Execute
        If gMultiReport <> 1 Then
            TipoUtente = RsNome!Utente + "/" + RsNome!t_utente
        Else
            TipoUtente = RsNome!t_utente + "/" + RsNome!Utente
        End If
                
        '****************************************************************
        
        'Calcula a Situa��o
        CmdSit.Parameters(0).value = rs!t_sit
        Set RsSit = CmdSit.Execute
        
        '****************************************************************
        
        'Calcula a Proveni�ncia
        If BL_HandleNull(rs!cod_proven, "") <> "" Then
            CmdProven.Parameters(0).value = rs!cod_proven
            Set RsProven = CmdProven.Execute
            If Not RsProven.EOF Then
                Proven = RsProven!descr_proven
            Else
                Proven = "NULO"
            End If
        Else
            Proven = "NULO"
        End If
        
        '****************************************************************
        
        'Constroi a string de diagn�sticos secund�rios
        CmdDiag.Parameters(0).value = rs!n_req
        Set RsDiag = CmdDiag.Execute
        STRDiag = ""
        While Not RsDiag.EOF
            STRDiag = STRDiag + RsDiag!Descr_Diag + ";"
            RsDiag.MoveNext
        Wend
        
        '****************************************************************
        
        'Calcula resultado frase da an�lise cariotipo(para o IGM)
        CmdResFrase.Parameters(0).value = rs!n_req
        Set RsResFrase = CmdResFrase.Execute
        ResCariotipo = ""
        If Not RsResFrase.EOF Then
            ResCariotipo = ResCariotipo + BL_HandleNull(RsResFrase!descr_frase, "") + ";"
        End If
        
        '****************************************************************
        
        'Calcula resultado alfanum�rico da an�lise cariotipo(para o IGM)
        CmdResAlfan.Parameters(0).value = rs!n_req
        Set RsResAlfan = CmdResAlfan.Execute
        DataSaidaCar = ""
        While Not RsResAlfan.EOF
            DataSaidaCar = BL_HandleNull(RsResAlfan!result, "")
            RsResAlfan.MoveNext
        Wend
        
        '****************************************************************
        
        'Calcula observa��o do resultado da an�lise cariotipo(para o IGM)
        CmdResObs.Parameters(0).value = rs!n_req
        Set RsResObs = CmdResObs.Execute
        If Not RsResObs.EOF Then
            ResCariotipo = ResCariotipo + BL_HandleNull(RsResObs!descr_obs_ana, "") + ";"
        End If
        
        '****************************************************************
        
        'Calcula o M�dico respons�vel pelo cariotipo
        If BL_HandleNull(rs!cod_med, "") <> "" Then
            CmdMed.Parameters(0).value = rs!cod_med
            Set RsMed = CmdMed.Execute
            MedResp = RsMed!nome_med
        Else
            MedResp = "NULO"
        End If
        
        '****************************************************************
        
        sql = "INSERT INTO SL_CR_RP" & gNumeroSessao & _
              "(Empresa_Id, N_req,           req_aux,      Nome_ute, " & _
              " Tipo_ute,   Contagem_Grupos, Proveniencia, Situacao, " & _
              " Urgencia,   Produtos,        data_ent,     motivo, " & _
              " obs,        data_saida,      resultado,    responsavel) " & _
              "VALUES " & _
              "(Null," & rs!n_req & ",'" & BL_HandleNull(rs!req_aux, "null") & "'," & BL_TrataStringParaBD(RsNome!nome_ute) & "," & BL_TrataStringParaBD(TipoUtente) & "," & BL_TrataStringParaBD(STRContagrupos) & "," & IIf((Proven = "NULO"), "Null", BL_TrataStringParaBD(Proven)) & "," & _
              BL_TrataStringParaBD(Mid(RsSit!descr_t_sit, 1, 1)) & "," & BL_TrataStringParaBD(rs!t_urg) & "," & BL_TrataStringParaBD(STRProduto) & "," & IIf((BL_HandleNull(rs!dt_chega, "") = ""), "null", BL_TrataDataParaBD(BL_HandleNull(rs!dt_chega, Bg_DaData_ADO))) & _
              "," & BL_TrataStringParaBD(STRDiag) & ",'" & BL_HandleNull(rs!obs_proven, "") & "','" & DataSaidaCar & "','" & ResCariotipo & "'," & IIf((MedResp = "NULO"), "Null", BL_TrataStringParaBD(Mid(MedResp, 1, 20))) & ")"
        
        BG_ExecutaQuery_ADO sql
            
        
        'Liberta os RecordSet alocados pelos Comandos
            
        'GRupo
        'J� libertado anteriormente
            
        'Requisi��es
        If Not rsReq Is Nothing Then
            If rsReq.state <> adStateClosed Then
                rsReq.Close
            End If
            Set rsReq = Nothing
        End If
            
        'Produtos
        If Not RsProd Is Nothing Then
            If RsProd.state <> adStateClosed Then
                RsProd.Close
            End If
            Set RsProd = Nothing
        End If
                
        'Situa��o
        If Not RsSit Is Nothing Then
            If RsSit.state <> adStateClosed Then
                RsSit.Close
            End If
            Set RsSit = Nothing
        End If
                
        'Utente
        If Not RsNome Is Nothing Then
            If RsNome.state <> adStateClosed Then
                RsNome.Close
            End If
            Set RsNome = Nothing
        End If
        
        'Proveni�ncia
        If Not RsProven Is Nothing Then
            If RsProven.state <> adStateClosed Then
                RsProven.Close
            End If
            Set RsProven = Nothing
        End If
        
        'Diagn�stico Secund�rio(Motivo da requisi��o)
        If Not RsDiag Is Nothing Then
            If RsDiag.state <> adStateClosed Then
                RsDiag.Close
            End If
            Set RsDiag = Nothing
        End If
        
        'Resultado frase da an�lise cari�tipo
        If Not RsResFrase Is Nothing Then
            If RsResFrase.state <> adStateClosed Then
                RsResFrase.Close
            End If
            Set RsResFrase = Nothing
        End If
        
        'Resultado alfanum�rico da an�lise cari�tipo
        If Not RsResAlfan Is Nothing Then
            If RsResAlfan.state <> adStateClosed Then
                RsResAlfan.Close
            End If
            Set RsResAlfan = Nothing
        End If
        
        'Observa��es do Resultado da an�lise cari�tipo
        If Not RsResObs Is Nothing Then
            If RsResObs.state <> adStateClosed Then
                RsResObs.Close
            End If
            Set RsResObs = Nothing
        End If
        
        'M�dico respons�vel pelo cariotipo
        If Not RsMed Is Nothing Then
            If RsMed.state <> adStateClosed Then
                RsMed.Close
            End If
            Set RsMed = Nothing
        End If
        
        rs.MoveNext
        
    Wend
    
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
        
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD(EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD(EcDtFim.Text)
    Report.formulas(2) = "Grupos=" & BL_TrataStringParaBD("" & STRGrupos)
            
    Report.SQLQuery = "SELECT SL_CR_RP.N_REQ,SL_CR_RP.NOME_UTE,SL_CR_RP.CONTAGEM_GRUPOS,SL_CR_RP.PROVENIENCIA,SL_CR_RP.SITUACAO,SL_CR_RP.URGENCIA,SL_CR_RP.PRODUTOS," & _
                      " SL_CR_RP.REQ_AUX,SL_CR_RP.DATA_ENT,SL_CR_RP.MOTIVO,SL_CR_RP.OBS From SL_CR_RP" & gNumeroSessao & " SL_CR_RP"
                      
    Call BL_ExecutaReport
    
    Call BL_RemoveTabela("SL_CR_RP" & gNumeroSessao)
        
    'Desaloca Comandos e RecordSet
    Set CmdProd = Nothing
    Set CmdReq = Nothing
    Set CmdS = Nothing
    Set CmdC = Nothing
    Set CmdP = Nothing
    Set CmdNome = Nothing
    Set CmdSit = Nothing
    Set CmdProven = Nothing
    Set CmdDiag = Nothing
    Set CmdResFrase = Nothing
    Set CmdResAlfan = Nothing
    Set CmdResObs = Nothing
    Set CmdMed = Nothing
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
End Sub




VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormInformacaoClinica 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormInformacaoClinica"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9630
   Icon            =   "FormInformacaoClinica.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPesqRapEstatuto 
      Height          =   285
      Left            =   120
      TabIndex        =   86
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox EcSeqReferencia 
      Height          =   285
      Left            =   6960
      TabIndex        =   74
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox EcSeqMae 
      Height          =   285
      Left            =   6240
      TabIndex        =   73
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox EcSeqPai 
      Height          =   285
      Left            =   5520
      TabIndex        =   72
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox EcAscite 
      Height          =   285
      Left            =   4680
      TabIndex        =   49
      Top             =   7200
      Width           =   495
   End
   Begin VB.TextBox EcHep 
      Height          =   285
      Left            =   4080
      TabIndex        =   48
      Top             =   7200
      Width           =   495
   End
   Begin VB.TextBox EcEdema 
      Height          =   285
      Left            =   3480
      TabIndex        =   47
      Top             =   7200
      Width           =   495
   End
   Begin VB.TextBox EcCard 
      Height          =   285
      Left            =   2880
      TabIndex        =   46
      Top             =   7200
      Width           =   495
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   1800
      TabIndex        =   45
      Top             =   7200
      Width           =   855
   End
   Begin VB.TextBox EcIdadeDias 
      Height          =   285
      Left            =   840
      TabIndex        =   44
      Top             =   7200
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   2880
      TabIndex        =   31
      Top             =   5880
      Visible         =   0   'False
      Width           =   6135
      Begin VB.CommandButton BtAlbuminaAux 
         Height          =   305
         Left            =   1080
         Picture         =   "FormInformacaoClinica.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   39
         ToolTipText     =   " Ajuda "
         Top             =   600
         Width           =   400
      End
      Begin VB.TextBox EcAlbumina 
         Height          =   285
         Left            =   1440
         TabIndex        =   38
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton BtCreatinaAux 
         Height          =   305
         Left            =   1080
         Picture         =   "FormInformacaoClinica.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   37
         ToolTipText     =   " Ajuda "
         Top             =   120
         Width           =   400
      End
      Begin VB.CheckBox CkAscite 
         Alignment       =   1  'Right Justify
         Caption         =   "Ascite"
         Height          =   315
         Left            =   4920
         TabIndex        =   36
         Top             =   600
         Width           =   1100
      End
      Begin VB.CheckBox CkEdema 
         Alignment       =   1  'Right Justify
         Caption         =   "Edema"
         Height          =   315
         Left            =   4920
         TabIndex        =   35
         Top             =   120
         Width           =   1100
      End
      Begin VB.CheckBox CkHep 
         Alignment       =   1  'Right Justify
         Caption         =   "I. Hep."
         Height          =   315
         Left            =   3240
         TabIndex        =   34
         Top             =   600
         Width           =   1100
      End
      Begin VB.CheckBox CkCard 
         Alignment       =   1  'Right Justify
         Caption         =   "I. Card."
         Height          =   315
         Left            =   3240
         TabIndex        =   33
         Top             =   120
         Width           =   1100
      End
      Begin VB.TextBox EcCreatinina 
         Height          =   285
         Left            =   1440
         TabIndex        =   32
         Top             =   120
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "g/dL"
         Height          =   255
         Left            =   2340
         TabIndex        =   43
         Top             =   600
         Width           =   615
      End
      Begin VB.Label Label9 
         Caption         =   "Albumina"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "�mol/L"
         Height          =   255
         Left            =   2370
         TabIndex        =   41
         Top             =   120
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Creatinina"
         Height          =   255
         Left            =   240
         TabIndex        =   40
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.ComboBox EcTipoIdade 
      Height          =   315
      Left            =   1680
      TabIndex        =   30
      Top             =   5895
      Width           =   975
   End
   Begin VB.TextBox EcIdade 
      Height          =   285
      Left            =   840
      TabIndex        =   28
      Top             =   5895
      Width           =   855
   End
   Begin VB.TextBox EcPeso 
      Height          =   285
      Left            =   840
      TabIndex        =   23
      Top             =   6255
      Width           =   855
   End
   Begin VB.TextBox EcAltura 
      Height          =   285
      Left            =   840
      TabIndex        =   22
      Top             =   6600
      Width           =   855
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5655
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   9975
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "&Diagn�sticos"
      TabPicture(0)   =   "FormInformacaoClinica.frx":0B20
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label17"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "BtRetiraDiagP"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "BtInsereDiagP"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "EcListaPrim"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "EcListaDiag"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "EcPesqDiag"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "EcListaSec"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "BtInsereDiagS"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "BtRetiraDiagS"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Command2"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "EcInfCompl"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "&Terap�uticas/Medica��es"
      TabPicture(1)   =   "FormInformacaoClinica.frx":0B3C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Command1"
      Tab(1).Control(1)=   "EcPesqTeraMed"
      Tab(1).Control(2)=   "BtRetiraTeraMed"
      Tab(1).Control(3)=   "BtInsereTeraMed"
      Tab(1).Control(4)=   "EcListaTM"
      Tab(1).Control(5)=   "EcListaTeraMed"
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "&Motivos Cl�nicos"
      TabPicture(2)   =   "FormInformacaoClinica.frx":0B58
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Command3"
      Tab(2).Control(1)=   "EcListaSC"
      Tab(2).Control(2)=   "EcListaSClinica"
      Tab(2).Control(3)=   "EcPesqSClinica"
      Tab(2).Control(4)=   "BtInsereSClinica"
      Tab(2).Control(5)=   "BtRetiraSClinica"
      Tab(2).ControlCount=   6
      TabCaption(3)   =   "Fam�lia"
      TabPicture(3)   =   "FormInformacaoClinica.frx":0B74
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "EcCasoReferencia"
      Tab(3).Control(1)=   "EcCasoMae"
      Tab(3).Control(2)=   "EcCasoPai"
      Tab(3).Control(3)=   "EcMembro"
      Tab(3).Control(4)=   "BtPesquisaEstatuto"
      Tab(3).Control(5)=   "EcFeto"
      Tab(3).Control(6)=   "BtGeraFamilia"
      Tab(3).Control(7)=   "BtPesquisaFamilias"
      Tab(3).Control(8)=   "CbTipoReferencia"
      Tab(3).Control(9)=   "CbTipoMae"
      Tab(3).Control(10)=   "CbTipoPai"
      Tab(3).Control(11)=   "FGFamilias"
      Tab(3).Control(12)=   "BtPesquisaGrDiag"
      Tab(3).Control(13)=   "EcDescrGrDiag"
      Tab(3).Control(14)=   "EcPai"
      Tab(3).Control(15)=   "EcMae"
      Tab(3).Control(16)=   "EcNomeMae"
      Tab(3).Control(17)=   "EcNomePai"
      Tab(3).Control(18)=   "EcNomeReferencia"
      Tab(3).Control(19)=   "EcReferencia"
      Tab(3).Control(20)=   "EcAnoEstudo"
      Tab(3).Control(21)=   "EcParentesco"
      Tab(3).Control(22)=   "EcDescrEstatuto"
      Tab(3).Control(23)=   "EcCodEstatuto"
      Tab(3).Control(24)=   "EcCodGrDiag"
      Tab(3).Control(25)=   "EcCodFamilia"
      Tab(3).Control(26)=   "EcObs"
      Tab(3).Control(27)=   "Label6"
      Tab(3).Control(28)=   "Label16"
      Tab(3).Control(29)=   "Label15"
      Tab(3).Control(30)=   "Label14"
      Tab(3).Control(31)=   "Label7"
      Tab(3).Control(32)=   "Label13"
      Tab(3).Control(33)=   "Label12"
      Tab(3).Control(34)=   "Label11"
      Tab(3).Control(35)=   "LaEstatuto"
      Tab(3).Control(36)=   "Label4"
      Tab(3).Control(37)=   "Label3"
      Tab(3).ControlCount=   38
      Begin VB.TextBox EcInfCompl 
         Height          =   975
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   94
         Top             =   4560
         Width           =   8415
      End
      Begin VB.TextBox EcCasoReferencia 
         Height          =   315
         Left            =   -70320
         TabIndex        =   93
         Top             =   3000
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox EcCasoMae 
         Height          =   315
         Left            =   -70320
         TabIndex        =   92
         Top             =   2280
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox EcCasoPai 
         Height          =   315
         Left            =   -70320
         TabIndex        =   91
         Top             =   1560
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox EcMembro 
         Height          =   285
         Left            =   -68400
         TabIndex        =   87
         Top             =   1200
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton BtPesquisaEstatuto 
         Height          =   315
         Left            =   -66120
         Picture         =   "FormInformacaoClinica.frx":0B90
         Style           =   1  'Graphical
         TabIndex        =   85
         ToolTipText     =   "Pesquisa de Estatutos"
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox EcFeto 
         Height          =   285
         Left            =   -68400
         TabIndex        =   83
         Top             =   2280
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton BtGeraFamilia 
         Height          =   315
         Left            =   -73080
         Picture         =   "FormInformacaoClinica.frx":111A
         Style           =   1  'Graphical
         TabIndex        =   82
         ToolTipText     =   "Gera��o de nova fam�lia da patologia"
         Top             =   840
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaFamilias 
         Height          =   315
         Left            =   -72720
         Picture         =   "FormInformacaoClinica.frx":1524
         Style           =   1  'Graphical
         TabIndex        =   81
         ToolTipText     =   "Pesquisa de Familias"
         Top             =   840
         Width           =   375
      End
      Begin VB.ComboBox CbTipoReferencia 
         Height          =   315
         Left            =   -74040
         Style           =   2  'Dropdown List
         TabIndex        =   80
         Top             =   3000
         Width           =   975
      End
      Begin VB.ComboBox CbTipoMae 
         Height          =   315
         Left            =   -74040
         Style           =   2  'Dropdown List
         TabIndex        =   79
         Top             =   2280
         Width           =   975
      End
      Begin VB.ComboBox CbTipoPai 
         Height          =   315
         Left            =   -74040
         Style           =   2  'Dropdown List
         TabIndex        =   78
         Top             =   1560
         Width           =   975
      End
      Begin VB.CommandButton Command3 
         Height          =   375
         Left            =   -70560
         Picture         =   "FormInformacaoClinica.frx":1AAE
         Style           =   1  'Graphical
         TabIndex        =   77
         ToolTipText     =   "Listar todos as terap�uticas e medica��o."
         Top             =   480
         Width           =   375
      End
      Begin MSFlexGridLib.MSFlexGrid FGFamilias 
         Height          =   1500
         Left            =   -74880
         TabIndex        =   76
         Top             =   4080
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   2646
         _Version        =   393216
         FixedCols       =   0
      End
      Begin VB.CommandButton Command1 
         Height          =   375
         Left            =   -70560
         Picture         =   "FormInformacaoClinica.frx":2038
         Style           =   1  'Graphical
         TabIndex        =   75
         ToolTipText     =   "Listar todos as terap�uticas e medica��o."
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaGrDiag 
         Height          =   315
         Left            =   -69720
         Picture         =   "FormInformacaoClinica.frx":25C2
         Style           =   1  'Graphical
         TabIndex        =   71
         ToolTipText     =   "Pesquisa de Patologias"
         Top             =   1200
         Width           =   375
      End
      Begin VB.TextBox EcDescrGrDiag 
         Height          =   285
         Left            =   -73080
         TabIndex        =   70
         Top             =   1200
         Width           =   3375
      End
      Begin VB.TextBox EcPai 
         Height          =   315
         Left            =   -73080
         TabIndex        =   69
         Top             =   1560
         Width           =   3735
      End
      Begin VB.TextBox EcMae 
         Height          =   315
         Left            =   -73080
         TabIndex        =   68
         Top             =   2280
         Width           =   3735
      End
      Begin VB.TextBox EcNomeMae 
         Height          =   285
         Left            =   -74040
         TabIndex        =   67
         Top             =   2640
         Width           =   4695
      End
      Begin VB.TextBox EcNomePai 
         Height          =   285
         Left            =   -74040
         TabIndex        =   65
         Top             =   1920
         Width           =   4695
      End
      Begin VB.TextBox EcNomeReferencia 
         Height          =   285
         Left            =   -74040
         TabIndex        =   63
         Top             =   3360
         Width           =   4695
      End
      Begin VB.TextBox EcReferencia 
         Height          =   315
         Left            =   -73080
         TabIndex        =   62
         Top             =   3000
         Width           =   3735
      End
      Begin VB.TextBox EcAnoEstudo 
         Height          =   285
         Left            =   -70320
         TabIndex        =   61
         Top             =   840
         Width           =   615
      End
      Begin VB.TextBox EcParentesco 
         Height          =   285
         Left            =   -68400
         TabIndex        =   58
         Top             =   3360
         Width           =   1335
      End
      Begin VB.TextBox EcDescrEstatuto 
         Height          =   285
         Left            =   -67920
         TabIndex        =   56
         Top             =   840
         Width           =   2055
      End
      Begin VB.TextBox EcCodEstatuto 
         Height          =   285
         Left            =   -68400
         TabIndex        =   55
         Top             =   840
         Width           =   495
      End
      Begin VB.TextBox EcCodGrDiag 
         Height          =   285
         Left            =   -74040
         TabIndex        =   53
         Top             =   1200
         Width           =   975
      End
      Begin VB.TextBox EcCodFamilia 
         Height          =   285
         Left            =   -74040
         TabIndex        =   51
         Top             =   840
         Width           =   975
      End
      Begin VB.ListBox EcListaSC 
         Height          =   3765
         Left            =   -69360
         TabIndex        =   21
         Top             =   900
         Width           =   2775
      End
      Begin VB.ListBox EcListaSClinica 
         Height          =   3960
         Left            =   -74880
         TabIndex        =   20
         Top             =   900
         Width           =   4695
      End
      Begin VB.TextBox EcPesqTeraMed 
         Height          =   285
         Left            =   -74880
         TabIndex        =   19
         Top             =   540
         Width           =   4215
      End
      Begin VB.TextBox EcPesqSClinica 
         Height          =   285
         Left            =   -74880
         TabIndex        =   18
         Top             =   540
         Width           =   4215
      End
      Begin VB.CommandButton BtInsereSClinica 
         Height          =   495
         Left            =   -70080
         Picture         =   "FormInformacaoClinica.frx":2B4C
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton BtRetiraSClinica 
         Height          =   495
         Left            =   -70080
         Picture         =   "FormInformacaoClinica.frx":2E56
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2940
         Width           =   615
      End
      Begin VB.CommandButton Command2 
         Height          =   375
         Left            =   4440
         Picture         =   "FormInformacaoClinica.frx":3160
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Listar todos os diagn�sticos."
         Top             =   500
         Width           =   375
      End
      Begin VB.CommandButton BtRetiraTeraMed 
         Height          =   495
         Left            =   -70080
         Picture         =   "FormInformacaoClinica.frx":36EA
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   2940
         Width           =   615
      End
      Begin VB.CommandButton BtInsereTeraMed 
         Height          =   495
         Left            =   -70080
         Picture         =   "FormInformacaoClinica.frx":39F4
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   2160
         Width           =   615
      End
      Begin VB.ListBox EcListaTM 
         Height          =   3765
         ItemData        =   "FormInformacaoClinica.frx":3CFE
         Left            =   -69360
         List            =   "FormInformacaoClinica.frx":3D00
         TabIndex        =   3
         Top             =   900
         Width           =   2775
      End
      Begin VB.ListBox EcListaTeraMed 
         Height          =   3960
         ItemData        =   "FormInformacaoClinica.frx":3D02
         Left            =   -74880
         List            =   "FormInformacaoClinica.frx":3D04
         TabIndex        =   0
         Top             =   900
         Width           =   4695
      End
      Begin VB.CommandButton BtRetiraDiagS 
         Height          =   615
         Left            =   4920
         Picture         =   "FormInformacaoClinica.frx":3D06
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   3600
         Width           =   735
      End
      Begin VB.CommandButton BtInsereDiagS 
         Height          =   615
         Left            =   4920
         Picture         =   "FormInformacaoClinica.frx":4010
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2880
         Width           =   735
      End
      Begin VB.ListBox EcListaSec 
         Height          =   1815
         ItemData        =   "FormInformacaoClinica.frx":431A
         Left            =   5760
         List            =   "FormInformacaoClinica.frx":431C
         TabIndex        =   11
         Top             =   2520
         Width           =   2775
      End
      Begin VB.TextBox EcPesqDiag 
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   540
         Width           =   4215
      End
      Begin VB.ListBox EcListaDiag 
         Height          =   3375
         ItemData        =   "FormInformacaoClinica.frx":431E
         Left            =   120
         List            =   "FormInformacaoClinica.frx":4320
         TabIndex        =   5
         Top             =   900
         Width           =   4695
      End
      Begin VB.ListBox EcListaPrim 
         Height          =   1425
         ItemData        =   "FormInformacaoClinica.frx":4322
         Left            =   5760
         List            =   "FormInformacaoClinica.frx":4324
         TabIndex        =   8
         Top             =   900
         Width           =   2775
      End
      Begin VB.CommandButton BtInsereDiagP 
         Height          =   615
         Left            =   4920
         Picture         =   "FormInformacaoClinica.frx":4326
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   960
         Width           =   735
      End
      Begin VB.CommandButton BtRetiraDiagP 
         Height          =   615
         Left            =   4920
         Picture         =   "FormInformacaoClinica.frx":4630
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox EcObs 
         Height          =   285
         Left            =   -74040
         TabIndex        =   89
         Top             =   3720
         Width           =   4695
      End
      Begin VB.Label Label17 
         Caption         =   "Informa��o Complementar"
         Height          =   255
         Left            =   120
         TabIndex        =   95
         Top             =   4320
         Width           =   1935
      End
      Begin VB.Label Label6 
         Caption         =   "Observa��o"
         Height          =   255
         Left            =   -74910
         TabIndex        =   90
         Top             =   3720
         Width           =   975
      End
      Begin VB.Label Label16 
         Caption         =   "Membro"
         Height          =   255
         Left            =   -69120
         TabIndex        =   88
         Top             =   1200
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label Label15 
         Caption         =   "FETO"
         Height          =   255
         Left            =   -69120
         TabIndex        =   84
         Top             =   2280
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label14 
         Caption         =   "M�e"
         Height          =   255
         Left            =   -74880
         TabIndex        =   66
         Top             =   2280
         Width           =   375
      End
      Begin VB.Label Label7 
         Caption         =   "Pai"
         Height          =   255
         Left            =   -74880
         TabIndex        =   64
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label Label13 
         Caption         =   "Ano Estudo"
         Height          =   255
         Left            =   -71280
         TabIndex        =   60
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Refer�ncia"
         Height          =   255
         Left            =   -74880
         TabIndex        =   59
         Top             =   3000
         Width           =   855
      End
      Begin VB.Label Label11 
         Caption         =   "Parentesco"
         Height          =   255
         Left            =   -69240
         TabIndex        =   57
         Top             =   3360
         Width           =   855
      End
      Begin VB.Label LaEstatuto 
         Caption         =   "Estatuto"
         Height          =   255
         Left            =   -69120
         TabIndex        =   54
         Top             =   840
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Gr. Diagn."
         Height          =   255
         Left            =   -74880
         TabIndex        =   52
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Familia"
         Height          =   255
         Left            =   -74880
         TabIndex        =   50
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Secund�rios"
         Height          =   255
         Left            =   5760
         TabIndex        =   14
         Top             =   2325
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Prim�rios"
         Height          =   255
         Left            =   5760
         TabIndex        =   13
         Top             =   720
         Width           =   735
      End
   End
   Begin VB.Label LaIdade 
      Caption         =   "Idade"
      Height          =   255
      Left            =   240
      TabIndex        =   29
      Top             =   5895
      Width           =   495
   End
   Begin VB.Label LaPeso 
      Caption         =   "Peso"
      Height          =   255
      Left            =   240
      TabIndex        =   27
      Top             =   6255
      Width           =   375
   End
   Begin VB.Label LaAltura 
      Caption         =   "Altura"
      Height          =   255
      Left            =   240
      TabIndex        =   26
      Top             =   6600
      Width           =   735
   End
   Begin VB.Label LaUnidPeso 
      Caption         =   "Kg"
      Height          =   255
      Left            =   1725
      TabIndex        =   25
      Top             =   6255
      Width           =   375
   End
   Begin VB.Label LaUnidAltura 
      Caption         =   "cm"
      Height          =   255
      Left            =   1725
      TabIndex        =   24
      Top             =   6600
      Width           =   375
   End
End
Attribute VB_Name = "FormInformacaoClinica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 17/09/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim CamposBDparaListBoxP
Dim CamposBDparaListBoxS
Dim CamposBDparaListBoxTM

Dim NumEspacos
Dim ListarRemovidos As Boolean
Dim FlgGravou As Boolean
Dim rs As ADODB.recordset

'BRUNODSANTOS 21.10.2016 LACTO-366
Public flg_passou_no_ecra_ID As Boolean
'


Private Sub BtAlbuminaAux_Click()
Dim AlbuminaAux As String
    AlbuminaAux = InputBox("Introduza o valor da Albumina em g/L", "Albumina")
    AlbuminaAux = Replace(AlbuminaAux, ".", ",")
    If (IsNumeric(AlbuminaAux)) Then
        EcAlbumina.text = Format(AlbuminaAux, "############0.0####") / 10
        EcAlbumina.text = Replace(EcAlbumina, ",", ".")
    Else
        If AlbuminaAux <> "" Then
            MsgBox "O valor introduzido n�o � num�rico"
        End If
    End If
    
End Sub

Private Sub BtCreatinaAux_Click()
    Dim CreatinaAux As String
    CreatinaAux = InputBox("Introduza o valor da Creatina em mg/dL", "Creatina")
    If (IsNumeric(CreatinaAux)) Then
        CreatinaAux = Replace(CreatinaAux, ".", ",")
        EcCreatinina.text = Format(CreatinaAux, "############0.0####") * Format(88.4, "############0.0####")
        EcCreatinina.text = Replace(EcCreatinina, ",", ".")
    Else
        If CreatinaAux <> "" Then
            MsgBox "O valor introduzido n�o � num�rico"
        End If
    End If
    
End Sub

Private Sub BtGeraFamilia_Click()
    Dim sql As String
    Dim RsFamilias As ADODB.recordset
    Dim MaxFamPat As Long
    Dim CriterioPesquisa As String
    
    CriterioPesquisa = ""
    If EcCodGrDiag.text <> "" Then
        CriterioPesquisa = ""
        Set RsFamilias = New ADODB.recordset
        If gLAB = "GM" Then
            CriterioPesquisa = "where cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag.text)
        End If
        sql = "select max(nr_familia) max_familia from sl_familias " & CriterioPesquisa
        RsFamilias.CursorLocation = adUseServer
        RsFamilias.CursorType = adOpenStatic
        RsFamilias.Open sql, gConexao
        If RsFamilias.RecordCount > 0 Then
            MaxFamPat = BL_HandleNull(RsFamilias!max_familia, 0) + 1
            EcCodFamilia.text = MaxFamPat
        Else
            MaxFamPat = 1
            EcCodFamilia.text = MaxFamPat
        End If
        
        EcMembro = 1
        If gF_REQUIS = 1 Then
            If InStr(1, FormGestaoRequisicao.EcNome.text, "FETO") <> 0 Then
                EcFeto.text = "1"
            End If
        ElseIf gF_IDENTIF = 1 Then
            If InStr(1, FormIdentificaUtente.EcNome.text, "FETO") <> 0 Then
                EcFeto.text = "1"
            End If
        ElseIf gF_IDENTIF_VET = 1 Then
            If InStr(1, FormIdentificaVet.EcNome.text, "FETO") <> 0 Then
                EcFeto.text = "1"
            End If
        ElseIf gF_REQUTE = 1 Then
            If InStr(1, FormReqUte.EcNome.text, "FETO") <> 0 Then
                EcFeto.text = "1"
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "Patologia obrigat�ria!", vbInformation
        Exit Sub
    End If
End Sub

Private Sub BtInsereDiagP_Click()
    
    Dim indice, i As Integer
    Dim ListaOrigem As Object
    Dim ListaDestino As Object
    Dim aux As String
    Dim CountAntes As Integer
    Dim CountDepois As Integer
    
    
    Set ListaDestino = EcListaPrim
    Set ListaOrigem = EcListaDiag
   
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i

End Sub

Private Sub BtInsereDiagS_Click()
    
    Dim indice, i As Integer
    Dim ListaOrigem As Object
    Dim ListaDestino As Object
    Dim aux As String
    Dim CountAntes As Integer
    Dim CountDepois As Integer
    
    Set ListaDestino = EcListaSec
    Set ListaOrigem = EcListaDiag
   
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i

End Sub

Private Sub BtInsereSClinica_Click()
    
    Dim indice, i As Integer
    Dim ListaOrigem As Object
    Dim ListaDestino As Object
    Dim aux As String
    Dim CountAntes As Integer
    Dim CountDepois As Integer
    
    
    Set ListaDestino = EcListaSC
    Set ListaOrigem = EcListaSClinica
   
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i

End Sub

Private Sub BtInsereTeraMed_Click()
    
    Dim indice, i As Integer
    Dim ListaOrigem As Object
    Dim ListaDestino As Object
    Dim aux As String
    Dim CountAntes As Integer
    Dim CountDepois As Integer
    
    Set ListaDestino = EcListaTM
    Set ListaOrigem = EcListaTeraMed
   
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i

End Sub

Private Sub BtPesquisaEstatuto_Click()
        FormPesquisaRapida.InitPesquisaRapida "sl_tbf_estatuto", _
                    "descr_estat", "cod_estat", _
                    EcPesqRapEstatuto
End Sub


Private Sub BtPesquisaGrDiag_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_gr_diag"
    CamposEcran(1) = "descr_gr_diag"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_gr_diag"
    CamposEcran(2) = "cod_gr_diag"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_diag"
    CampoPesquisa = "descr_gr_diag"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "descr_gr_diag", " Patologias")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrDiag.text = Resultados(2)
            EcDescrGrDiag.text = Resultados(1)
            EcCodGrDiag.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem grupos de diagn�sticos", vbExclamation, "Proveni�ncias"
        EcCodGrDiag.SetFocus
    End If

End Sub

Private Sub BtRetiraDiagS_Click()
    
    If EcListaSec.ListIndex <> -1 Then
        EcListaSec.RemoveItem (EcListaSec.ListIndex)
    End If

End Sub

Private Sub BtRetiraDiagP_Click()
    
    If EcListaPrim.ListIndex <> -1 Then
        EcListaPrim.RemoveItem (EcListaPrim.ListIndex)
    End If

End Sub

Private Sub BtRetiraSClinica_Click()
    
    If EcListaSC.ListIndex <> -1 Then
        EcListaSC.RemoveItem (EcListaSC.ListIndex)
    End If

End Sub

Private Sub BtRetiraTeraMed_Click()
    
    If EcListaTM.ListIndex <> -1 Then
        EcListaTM.RemoveItem (EcListaTM.ListIndex)
    End If

End Sub

Private Sub Command1_Click()

    EcPesqTeraMed.text = "*"
    
End Sub

Private Sub Command2_Click()

    EcPesqDiag.text = "*"
    
End Sub

Private Sub Command3_Click()
    
    EcPesqSClinica.text = "*"
    
End Sub

Private Sub EcCodEstatuto_Validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
'    If gLAB = "ENZ" Then
        If EcCodEstatuto.text <> "" Then
            Set Tabela = New ADODB.recordset
        
            sql = "SELECT descr_estat FROM sl_tbf_estatuto WHERE cod_estat ='" & Trim(EcCodEstatuto.text) & "'"
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            Tabela.Open sql, gConexao
        
            If Tabela.RecordCount = 0 Then
                Cancel = True
                BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
                EcCodEstatuto.text = ""
                EcDescrEstatuto.text = ""
            Else
                EcDescrEstatuto = Tabela!descr_estat
            End If
            Tabela.Close
            Set Tabela = Nothing
        Else
            EcDescrEstatuto.text = ""
        End If
'    End If
End Sub

Private Sub EcMembro_Validate(Cancel As Boolean)
    Dim RsFeto  As ADODB.recordset
    Dim sql As String
    Dim Tabela As ADODB.recordset
    
    If gLAB = "GM" Then
        If EcFeto.text = "" And EcMembro.text <> "" And IsNumeric(EcCodFamilia) And EcCodGrDiag.text <> "" Then
            If gF_REQUIS = 1 Then
                If InStr(1, FormGestaoRequisicao.EcNome.text, "FETO") <> 0 Then
                    Set RsFeto = New ADODB.recordset
                    RsFeto.CursorLocation = adUseServer
                    RsFeto.CursorType = adOpenStatic
                    sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                    RsFeto.Open sql, gConexao
                    If RsFeto.RecordCount > 0 Then
                        EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                    Else
                        EcFeto = "1"
                    End If
                    RsFeto.Close
                    Set RsFeto = Nothing
                End If
            ElseIf gF_IDENTIF = 1 Then
                If InStr(1, FormIdentificaUtente.EcNome.text, "FETO") <> 0 Then
                    Set RsFeto = New ADODB.recordset
                    RsFeto.CursorLocation = adUseServer
                    RsFeto.CursorType = adOpenStatic
                    sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                    RsFeto.Open sql, gConexao
                    If RsFeto.RecordCount > 0 Then
                        EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                    Else
                        EcFeto = "1"
                    End If
                    RsFeto.Close
                    Set RsFeto = Nothing
                End If
            ElseIf gF_IDENTIF_VET = 1 Then
                If InStr(1, FormIdentificaVet.EcNome.text, "FETO") <> 0 Then
                    Set RsFeto = New ADODB.recordset
                    RsFeto.CursorLocation = adUseServer
                    RsFeto.CursorType = adOpenStatic
                    sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                    RsFeto.Open sql, gConexao
                    If RsFeto.RecordCount > 0 Then
                        EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                    Else
                        EcFeto = "1"
                    End If
                    RsFeto.Close
                    Set RsFeto = Nothing
                End If
            ElseIf gF_REQUTE = 1 Then
                If InStr(1, FormReqUte.EcNome.text, "FETO") <> 0 Then
                    Set RsFeto = New ADODB.recordset
                    RsFeto.CursorLocation = adUseServer
                    RsFeto.CursorType = adOpenStatic
                    sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                    RsFeto.Open sql, gConexao
                    If RsFeto.RecordCount > 0 Then
                        EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                    Else
                        EcFeto = "1"
                    End If
                    RsFeto.Close
                    Set RsFeto = Nothing
                End If
            End If
        End If
    End If
End Sub

Private Sub EcCodFamilia_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodFamilia_LostFocus()
    Dim RsMembro As ADODB.recordset
    Dim RsFeto As ADODB.recordset
    Dim sql As String
    Dim RsPatologia As ADODB.recordset
    If EcCodFamilia.text <> "" Then
    BG_ValidaTipoCampo_ADO Me, EcCodFamilia
    
    If gLAB = "ENZ" Then
        
        Set RsPatologia = New ADODB.recordset
        RsPatologia.CursorLocation = adUseServer
        RsPatologia.CursorType = adOpenStatic
        sql = "select cod_gr_diag from sl_familias where nr_familia = " & EcCodFamilia
        RsPatologia.Open sql, gConexao
        If RsPatologia.RecordCount > 0 Then
            EcCodGrDiag.text = BL_HandleNull(RsPatologia!cod_gr_diag, "")
            EcCodGrDiag_Validate False
        End If
    End If
    
    If EcMembro.text = "" Then
        If gF_REQUIS = 1 Then
            If InStr(1, FormGestaoRequisicao.EcNome.text, "FETO") = 0 And IsNumeric(EcCodFamilia) And EcCodGrDiag.text <> "" Then
                Set RsMembro = New ADODB.recordset
                RsMembro.CursorLocation = adUseServer
                RsMembro.CursorType = adOpenStatic
                sql = "select max(membro) max_membro from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag)
                RsMembro.Open sql, gConexao
                If RsMembro.RecordCount > 0 Then
                    EcMembro.text = BL_HandleNull(RsMembro!max_membro, 0) + 1
                End If
                RsMembro.Close
                Set RsMembro = Nothing
            End If
        ElseIf gF_IDENTIF = 1 Then
            If InStr(1, FormIdentificaUtente.EcNome.text, "FETO") = 0 And IsNumeric(EcCodFamilia) And EcCodGrDiag.text <> "" Then
                Set RsMembro = New ADODB.recordset
                RsMembro.CursorLocation = adUseServer
                RsMembro.CursorType = adOpenStatic
                sql = "select max(membro) max_membro from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag)
                RsMembro.Open sql, gConexao
                If RsMembro.RecordCount > 0 Then
                    EcMembro.text = BL_HandleNull(RsMembro!max_membro, 0) + 1
                End If
                RsMembro.Close
                Set RsMembro = Nothing
            End If
        ElseIf gF_REQUTE = 1 Then
            If InStr(1, FormReqUte.EcNome.text, "FETO") = 0 And IsNumeric(EcCodFamilia) And EcCodGrDiag.text <> "" Then
                Set RsMembro = New ADODB.recordset
                RsMembro.CursorLocation = adUseServer
                RsMembro.CursorType = adOpenStatic
                sql = "select max(membro) max_membro from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag)
                RsMembro.Open sql, gConexao
                If RsMembro.RecordCount > 0 Then
                    EcMembro.text = BL_HandleNull(RsMembro!max_membro, 0) + 1
                End If
                RsMembro.Close
                Set RsMembro = Nothing
            End If
        End If
    End If
    
    If EcFeto.text = "" And EcMembro.text <> "" And IsNumeric(EcCodFamilia) And EcCodGrDiag.text <> "" Then
        If gF_REQUIS = 1 Then
            If InStr(1, FormGestaoRequisicao.EcNome.text, "FETO") <> 0 Then
                Set RsFeto = New ADODB.recordset
                RsFeto.CursorLocation = adUseServer
                RsFeto.CursorType = adOpenStatic
                sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                RsFeto.Open sql, gConexao
                If RsFeto.RecordCount > 0 Then
                    EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                Else
                    EcFeto = "1"
                End If
                RsFeto.Close
                Set RsFeto = Nothing
            End If
        ElseIf gF_IDENTIF = 1 Then
            If InStr(1, FormIdentificaUtente.EcNome.text, "FETO") <> 0 Then
                Set RsFeto = New ADODB.recordset
                RsFeto.CursorLocation = adUseServer
                RsFeto.CursorType = adOpenStatic
                sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                RsFeto.Open sql, gConexao
                If RsFeto.RecordCount > 0 Then
                    EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                Else
                    EcFeto = "1"
                End If
                RsFeto.Close
                Set RsFeto = Nothing
            End If
        ElseIf gF_REQUTE = 1 Then
            If InStr(1, FormReqUte.EcNome.text, "FETO") <> 0 Then
                Set RsFeto = New ADODB.recordset
                RsFeto.CursorLocation = adUseServer
                RsFeto.CursorType = adOpenStatic
                sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                RsFeto.Open sql, gConexao
                If RsFeto.RecordCount > 0 Then
                    EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                Else
                    EcFeto = "1"
                End If
                RsFeto.Close
                Set RsFeto = Nothing
            End If
        End If
    End If
    End If
    
End Sub

Private Sub EcCodGrDiag_LostFocus()
    Dim RsMembro As ADODB.recordset
    Dim RsFeto As ADODB.recordset
    Dim sql As String
   
    If gF_REQUIS = 1 Then
        If IsNumeric(EcCodFamilia) And EcMembro.text = "" And EcCodGrDiag.text <> "" And InStr(1, FormGestaoRequisicao.EcNome.text, "FETO") = 0 Then
            Set RsMembro = New ADODB.recordset
            RsMembro.CursorLocation = adUseServer
            RsMembro.CursorType = adOpenStatic
            sql = "select max(membro) max_membro from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag)
            RsMembro.Open sql, gConexao
            If RsMembro.RecordCount > 0 Then
                EcMembro.text = BL_HandleNull(RsMembro!max_membro, 0) + 1
            End If
            RsMembro.Close
            Set RsMembro = Nothing
        End If
    ElseIf gF_IDENTIF = 1 Then
        If IsNumeric(EcCodFamilia) And EcMembro.text = "" And EcCodGrDiag.text <> "" And InStr(1, FormIdentificaUtente.EcNome.text, "FETO") = 0 Then
            Set RsMembro = New ADODB.recordset
            RsMembro.CursorLocation = adUseServer
            RsMembro.CursorType = adOpenStatic
            sql = "select max(membro) max_membro from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag)
            RsMembro.Open sql, gConexao
            If RsMembro.RecordCount > 0 Then
                EcMembro.text = BL_HandleNull(RsMembro!max_membro, 0) + 1
            End If
            RsMembro.Close
            Set RsMembro = Nothing
        End If
    ElseIf gF_REQUTE = 1 Then
        If IsNumeric(EcCodFamilia) And EcMembro.text = "" And EcCodGrDiag.text <> "" And InStr(1, FormReqUte.EcNome.text, "FETO") = 0 Then
            Set RsMembro = New ADODB.recordset
            RsMembro.CursorLocation = adUseServer
            RsMembro.CursorType = adOpenStatic
            sql = "select max(membro) max_membro from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag)
            RsMembro.Open sql, gConexao
            If RsMembro.RecordCount > 0 Then
                EcMembro.text = BL_HandleNull(RsMembro!max_membro, 0) + 1
            End If
            RsMembro.Close
            Set RsMembro = Nothing
        End If
    End If
    
    If EcFeto.text = "" And EcMembro.text <> "" And IsNumeric(EcCodFamilia) And EcCodGrDiag.text <> "" Then
        If gF_REQUIS = 1 Then
            If InStr(1, FormGestaoRequisicao.EcNome.text, "FETO") <> 0 Then
                Set RsFeto = New ADODB.recordset
                RsFeto.CursorLocation = adUseServer
                RsFeto.CursorType = adOpenStatic
                sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                RsFeto.Open sql, gConexao
                If RsFeto.RecordCount > 0 Then
                    EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                Else
                    EcFeto = "1"
                End If
                RsFeto.Close
                Set RsFeto = Nothing
            End If
        ElseIf gF_IDENTIF = 1 Then
            If InStr(1, FormIdentificaUtente.EcNome.text, "FETO") <> 0 Then
                Set RsFeto = New ADODB.recordset
                RsFeto.CursorLocation = adUseServer
                RsFeto.CursorType = adOpenStatic
                sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                RsFeto.Open sql, gConexao
                If RsFeto.RecordCount > 0 Then
                    EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                Else
                    EcFeto = "1"
                End If
                RsFeto.Close
                Set RsFeto = Nothing
            End If
        ElseIf gF_REQUTE = 1 Then
            If InStr(1, FormReqUte.EcNome.text, "FETO") <> 0 Then
                Set RsFeto = New ADODB.recordset
                RsFeto.CursorLocation = adUseServer
                RsFeto.CursorType = adOpenStatic
                sql = "select max(feto) max_feto from sl_familias where nr_familia = " & EcCodFamilia & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag) & " and membro = " & EcMembro.text
                RsFeto.Open sql, gConexao
                If RsFeto.RecordCount > 0 Then
                    EcFeto = BL_HandleNull(RsFeto!max_feto, 0) + 1
                Else
                    EcFeto = "1"
                End If
                RsFeto.Close
                Set RsFeto = Nothing
            End If
        End If
    End If
    
End Sub
Private Sub EcCodGrDiag_Validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String

        If EcCodGrDiag.text <> "" Then
            Set Tabela = New ADODB.recordset
            
            EcCodGrDiag.text = UCase(EcCodGrDiag)
        
            sql = "SELECT cod_gr_diag,descr_gr_diag FROM sl_gr_diag WHERE cod_gr_diag=" & BL_TrataStringParaBD(EcCodGrDiag.text)
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            Tabela.Open sql, gConexao
        
            If Tabela.RecordCount = 0 Then
                Cancel = True
                BG_Mensagem mediMsgBox, "N�o existe patologia com esse c�digo !", vbInformation, ""
                EcCodGrDiag.text = ""
                EcDescrGrDiag.text = ""
            Else
                EcDescrGrDiag.text = Tabela!descr_gr_diag
            End If
            Tabela.Close
            Set Tabela = Nothing
     
        Else
            EcDescrGrDiag.text = ""
        End If
End Sub

Private Sub EcPesqDiag_Change()

    RefinaPesquisa EcPesqDiag, "sl_diag", "descr_diag", "seq_diag", EcListaDiag
    
End Sub

Private Sub EcPesqRapEstatuto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If EcPesqRapEstatuto.text <> "" Then
        Set rsCodigo = New ADODB.recordset
       
        sql = "SELECT cod_estat,descr_estat FROM sl_tbf_estatuto WHERE cod_estat= '" & EcPesqRapEstatuto & "'"
        
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do estatuto!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapEstatuto.text = ""
        Else
            EcCodEstatuto.text = Trim(rsCodigo!Cod_estat)
            EcDescrEstatuto.text = Trim(rsCodigo!descr_estat)
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcPesqTeraMed_Change()
    
    RefinaPesquisa EcPesqTeraMed, "sl_tera_med", "descr_tera_med", "seq_tera_med", EcListaTeraMed

End Sub

Private Sub EcPesqSClinica_Change()
    
    RefinaPesquisa EcPesqSClinica, "sl_motivo", "descr_motivo", "seq_motivo", EcListaSClinica

End Sub

Sub RefinaPesquisa(Origem As Control, NomeTabela As String, CampoPesquisa As String, CampoChave As String, CampoControlDestino As Control)

    Dim i As Integer
    Dim TSQLQueryAux As String
    Dim Str1 As String
    Dim TTabelaAux As ADODB.recordset
        
    If Origem.text <> "" Then
        TSQLQueryAux = "SELECT " & CampoChave & "," & CampoPesquisa & " FROM " & NomeTabela & _
                      " WHERE UPPER(" & CampoPesquisa & ") LIKE '" & UCase(BG_CvWilcard(Origem.text)) & "%'"
        TSQLQueryAux = TSQLQueryAux & " ORDER BY " & CampoPesquisa
        
        Set TTabelaAux = New ADODB.recordset
        TTabelaAux.CursorType = adOpenStatic
        TTabelaAux.CursorLocation = adUseServer
        TTabelaAux.Open TSQLQueryAux, gConexao
    
        CampoControlDestino.ListIndex = mediComboValorNull
        CampoControlDestino.Clear
        i = 0
        Do Until TTabelaAux.EOF
            Str1 = ""
            Str1 = Str1 & TTabelaAux(CampoPesquisa)
            CampoControlDestino.AddItem Str1
            CampoControlDestino.ItemData(i) = TTabelaAux(CampoChave)
            TTabelaAux.MoveNext
            i = i + 1
        Loop
        TTabelaAux.Close
        Set TTabelaAux = Nothing
    ElseIf Origem.text = "" Then
        CampoControlDestino.Clear
    End If

End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    BG_ParametrizaPermissoes_ADO Me.Name
    PreencheValoresDefeito
    
    Estado = 2
    BL_FimProcessamento Me
    
    gF_INFCLI = 1

    
End Sub

Sub EventoActivate()

    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    SSTab1.Tab = 0
    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    
    'Se a form que abriu esta � a de identifica��o de utente ent�o n�o � poss�vel inserir
            'diagn�sticos secund�rios nem terap�uticas e medica��es

    If gF_REQUIS_PRIVADO = 1 Then
        'A form que abriu esta foi a form FormGestaoRequisicao
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BtInsereDiagS.Enabled = True
        BtRetiraDiagS.Enabled = True
        SSTab1.TabEnabled(1) = True
        If Trim(FormGestaoRequisicaoPrivado.EcNumReq) <> "" Then
            EcNumReq = FormGestaoRequisicaoPrivado.EcNumReq
        Else
            EcNumReq = "-" & gNumeroSessao
        End If
    ElseIf gF_REQUIS = 1 Then
        'A form que abriu esta foi a form FormGestaoRequisicao
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BtInsereDiagS.Enabled = True
        BtRetiraDiagS.Enabled = True
        SSTab1.TabEnabled(1) = True
        If Trim(FormGestaoRequisicao.EcNumReq) <> "" Then
            EcNumReq = FormGestaoRequisicao.EcNumReq
        Else
            EcNumReq = "-" & gNumeroSessao
        End If
    ElseIf gF_REQUTE = 1 Then
        'A form que abriu esta foi a form FormConsReqUte
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Procurar", "InActivo"
        BL_Toolbar_BotaoEstado "Limpar", "InActivo"
        SSTab1.TabEnabled(1) = True
        If Trim(FormReqUte.EcNumReq) <> "" Then
            EcNumReq = FormReqUte.EcNumReq
        Else
            EcNumReq = "-" & gNumeroSessao
        End If
    ElseIf gF_IDENTIF = 1 And (gF_REQUIS = 0 And gF_REQUIS_PRIVADO = 0) Then
        BtInsereDiagS.Enabled = False
        BtRetiraDiagS.Enabled = False
        SSTab1.TabEnabled(1) = False
        SSTab1.TabEnabled(2) = False
    ElseIf gF_IDENTIF_VET = 1 And (gF_REQUIS = 0 And gF_REQUIS_PRIVADO = 0) Then
        BtInsereDiagS.Enabled = False
        BtRetiraDiagS.Enabled = False
        SSTab1.TabEnabled(1) = False
        SSTab1.TabEnabled(2) = False
    
    ElseIf gF_HIPO_RESULTADOS = 1 Then
        'A form que abriu esta foi a form FormHipoResultados
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BtInsereDiagS.Enabled = True
        BtRetiraDiagS.Enabled = True
        SSTab1.TabEnabled(1) = True

    End If
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If gLAB = "ENZ" Then
        EcCasoPai.Visible = True
        EcCasoMae.Visible = True
        EcCasoReferencia.Visible = True
    End If
    
    If gUsaFamilias = 1 Then
        SSTab1.TabVisible(3) = True
    Else
        SSTab1.TabVisible(3) = False
    End If
    
    
End Sub

Sub EventoUnload()

'    If gF_REQUIS = 1 Then
        If FlgGravou = False Then
            gMsgTitulo = "Modificar"
            gMsgMsg = "Quer validar as altera��es efectuadas ?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            
            If gMsgResp = vbYes Then
                BD_Update
            End If
        End If
'    End If
    
    
    If gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.EcinfComplementar = EcInfCompl
        If EcListaSec.ListCount > 0 Then
            FormGestaoRequisicaoPrivado.EcFlgDiagSec.text = "1"
        Else
            FormGestaoRequisicaoPrivado.EcFlgDiagSec.text = "0"
        End If
        FormGestaoRequisicaoPrivado.Enabled = True
        Set gFormActivo = FormGestaoRequisicaoPrivado
    ElseIf gF_REQUIS = 1 Then
        FormGestaoRequisicao.EcinfComplementar = EcInfCompl
        If EcListaSec.ListCount > 0 Then
            FormGestaoRequisicao.EcFlgDiagSec.text = "1"
        Else
            FormGestaoRequisicao.EcFlgDiagSec.text = "0"
        End If
        FormGestaoRequisicao.Enabled = True
        Set gFormActivo = FormGestaoRequisicao
    ElseIf gF_REQUIS_VET = 1 Then
        FormGestaoRequisicaoVet.EcinfComplementar = EcInfCompl
        If EcListaSec.ListCount > 0 Then
            FormGestaoRequisicaoVet.EcFlgDiagSec.text = "1"
        Else
            FormGestaoRequisicaoVet.EcFlgDiagSec.text = "0"
        End If
        FormGestaoRequisicaoVet.Enabled = True
        Set gFormActivo = FormGestaoRequisicaoVet
    ElseIf gF_REQUTE = 1 Then
        FormReqUte.Enabled = True
        Set gFormActivo = FormReqUte
    ElseIf gF_IDENTIF = 1 And (gF_REQUIS = 0 And gF_REQUIS_PRIVADO = 0) Then
        'BRUNODSANTOS 21.06.2016 - LACTO-336
        flg_passou_no_ecra_ID = True
        FormIdentificaUtente.InfoComplementarID = EcInfCompl.text
        '
        If EcListaPrim.ListCount > 0 Then
            FormIdentificaUtente.EcFlgDiagPrim.text = "1"
        Else
            FormIdentificaUtente.EcFlgDiagPrim.text = "0"
        End If
        FormIdentificaUtente.Enabled = True
        Set gFormActivo = FormIdentificaUtente
    ElseIf gF_IDENTIF_VET = 1 And (gF_REQUIS = 0 And gF_REQUIS_PRIVADO = 0) Then
        If EcListaPrim.ListCount > 0 Then
            FormIdentificaVet.EcFlgDiagPrim.text = "1"
        Else
            FormIdentificaVet.EcFlgDiagPrim.text = "0"
        End If
        FormIdentificaVet.Enabled = True
        Set gFormActivo = FormIdentificaVet
    End If
    
    gF_INFCLI = 0
 
End Sub

Sub Inicializacoes()
    
    Me.caption = " Informa��o Clinica"
    Me.left = 540
    Me.top = 450
    Me.Width = 9720
    Me.Height = 6300 ' Normal
    If gF_REQUIS = 1 Or gF_REQUIS_PRIVADO = 1 Then
        Select Case gLAB
            Case "HSMARIA"
                Me.Height = 7425
                Frame1.Visible = True
            Case "BIO", "GM", "ENZ", "CITO", "CHVNG"
                Me.Height = 7425
            Case "HPD_GEN"
                Me.Height = "6250"
        End Select
    End If
    Set CampoDeFocus = EcPesqDiag
        
    NomeTabela = "sl_inf_clinica"
    
    NumCampos = 12
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "peso"
    CamposBD(2) = "altura"
    CamposBD(3) = "idade"
    CamposBD(4) = "t_idade"
    CamposBD(5) = "idade_dias"
    CamposBD(6) = "creatinina"
    CamposBD(7) = "albumina"
    CamposBD(8) = "flg_i_card"
    CamposBD(9) = "flg_i_hep"
    CamposBD(10) = "flg_edema"
    CamposBD(11) = "flg_ascite"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcPeso
    Set CamposEc(2) = EcAltura
    Set CamposEc(3) = EcIdade
    Set CamposEc(4) = EcTipoIdade
    Set CamposEc(5) = EcIdadeDias
    Set CamposEc(6) = EcCreatinina
    Set CamposEc(7) = EcAlbumina
    Set CamposEc(8) = EcCard
    Set CamposEc(9) = EcHep
    Set CamposEc(10) = EcEdema
    Set CamposEc(11) = EcAscite
        
    CamposBDparaListBoxP = Array("descr_diag")
    CamposBDparaListBoxS = Array("descr_diag")
    CamposBDparaListBoxTM = Array("descr_tera_med")
    NumEspacos = Array(50)
    
    FlgGravou = False
    
    EcCodFamilia.Tag = adInteger
    EcMembro.Tag = adInteger
    EcFeto.Tag = adInteger
    EcAnoEstudo.Tag = adInteger
    EcMembro.Tag = adInteger
    EcAnoEstudo.MaxLength = 4
    
    If gLAB = "GM" Then
        EcCodEstatuto.Visible = False
        EcDescrEstatuto.Visible = False
        BtPesquisaEstatuto.Visible = False
        LaEstatuto.Visible = False
    End If
  'edgar.parada Glintt-HS-19165 * vem de FormInformacaoClinica
    If gPassaParams.id = "FormGestaoRequisicaoPrivado" And gPassaParams.Param(0) = True Then
       EcInfCompl.locked = True
    End If
        '

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
  
    Me.SetFocus
        
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        BD_Insert
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()

    Dim i As Integer
    Dim SQLQuery As String
    Dim rsCodigo As ADODB.recordset
    Dim NReq As Long

    gSQLError = 0
    
    Set rsCodigo = New ADODB.recordset
    With rsCodigo
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .ActiveConnection = gConexao
    End With
        
    'Insere diagn�sticos prim�rios
    If EcListaPrim.ListCount > 0 Then
        For i = 0 To EcListaPrim.ListCount - 1
            rsCodigo.Source = "SELECT cod_diag FROM sl_diag WHERE seq_diag = " & EcListaPrim.ItemData(i)
            rsCodigo.Open
            
            If rsCodigo.RecordCount <> 0 Then
                SQLQuery = "INSERT INTO sl_diag_pri(seq_utente,cod_diag) VALUES(" & CDbl(gDUtente.seq_utente) & "," & BL_TrataStringParaBD(BL_HandleNull(rsCodigo!cod_diag, "0")) & ")"
                BG_ExecutaQuery_ADO SQLQuery
            End If
            
            rsCodigo.Close
        Next i
        SQLQuery = "UPDATE sl_identif SET flg_diag_pri = 1 WHERE seq_utente =" & (gDUtente.seq_utente)
        BG_ExecutaQuery_ADO SQLQuery
    Else
        SQLQuery = "UPDATE sl_identif SET flg_diag_pri = 0 WHERE seq_utente =" & (gDUtente.seq_utente)
        BG_ExecutaQuery_ADO SQLQuery
    End If
    
    'Insere dados da familia do utente
    If EcCodFamilia.text <> "" Then
        If EcCodGrDiag.text <> "" And EcCodFamilia.text <> "" Then
            SQLQuery = "insert into sl_familias(seq_utente,nr_familia,cod_gr_diag,ano_estudo,membro,parentesco,seq_pai,seq_mae,seq_referencia,feto, estatuto) values ( " & _
                        CInt(gDUtente.seq_utente) & "," & EcCodFamilia & "," & BL_TrataStringParaBD(EcCodGrDiag) & "," & BL_TrataStringParaBD(BL_HandleNull(EcAnoEstudo.text, "")) & "," & _
                        BL_TrataStringParaBD(BL_HandleNull(EcMembro, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(EcParentesco, "")) & "," & _
                        BL_HandleNull(EcSeqPai, "NULL") & "," & BL_HandleNull(EcSeqMae, "NULL") & "," & BL_HandleNull(EcSeqReferencia, "NULL") & "," & BL_HandleNull(EcFeto, "NULL") & ", " & BL_TrataStringParaBD(BL_HandleNull(EcCodEstatuto.text, "")) & " )"
            BG_ExecutaQuery_ADO SQLQuery
            
        Else
            BG_Mensagem mediMsgBox, "Grupo de Diagn�stico obrigat�rio! Dados da fam�lia n�o gravados!" & vbCrLf & "N�mero de caso n�o gerado automaticamente!", vbInformation
        End If
    End If

    
    If gF_REQUIS = 1 Then
        If Trim(FormGestaoRequisicao.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicao.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
    ElseIf gF_REQUIS_PRIVADO = 1 Then
        If Trim(FormGestaoRequisicaoPrivado.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicaoPrivado.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
    ElseIf gF_REQUIS_VET = 1 Then
        If Trim(FormGestaoRequisicaoVet.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicaoVet.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
    End If

    'Insere diagn�sticos secund�rios
    If EcListaSec.ListCount > 0 Then
        For i = 0 To EcListaSec.ListCount - 1
            rsCodigo.Source = "SELECT cod_diag FROM sl_diag WHERE seq_diag = " & EcListaSec.ItemData(i)
            rsCodigo.Open
            
            If rsCodigo.RecordCount <> 0 Then
                SQLQuery = "INSERT INTO sl_diag_sec(n_req,cod_diag) VALUES(" & NReq & "," & BL_TrataStringParaBD(BL_HandleNull(rsCodigo!cod_diag, "0")) & ")"
                BG_ExecutaQuery_ADO SQLQuery
            End If
            
            rsCodigo.Close
        Next i
        SQLQuery = "UPDATE sl_requis SET flg_diag_sec = 1 WHERE n_req =" & (NReq)
        BG_ExecutaQuery_ADO SQLQuery
    Else
        SQLQuery = "UPDATE sl_requis SET flg_diag_sec = 0 WHERE n_req =" & (NReq)
        BG_ExecutaQuery_ADO SQLQuery
    End If
                    
    
    'Insere terap�uticas e medica��es
    If EcListaTM.ListCount > 0 Then
        For i = 0 To EcListaTM.ListCount - 1
            ' pferreira 2011.05.05
            'rsCodigo.Source = "SELECT cod_tera_med FROM sl_tera_med WHERE seq_tera_med = " & EcListaTM.ItemData(i)
            'rsCodigo.Open
            
            'If rsCodigo.RecordCount <> 0 Then
            '    SQLQuery = "INSERT INTO sl_tm_ute(n_req,cod_tera_med) VALUES(" & nReq & "," & BL_TrataStringParaBD(BL_HandleNull(rsCodigo!cod_tera_med, "0")) & ")"
            '    BG_ExecutaQuery_ADO SQLQuery
            'End If
            
            'rsCodigo.Close
            SQLQuery = "INSERT INTO sl_tm_ute(n_req,cod_tera_med,dt_cri,user_cri) VALUES(" & NReq & "," & EcListaTM.ItemData(i) & ",sysdate," & gCodUtilizador & ")"
            BG_ExecutaQuery_ADO SQLQuery
        Next i
    End If
    
    'Insere suspeitas cl�nicas
    If EcListaSC.ListCount > 0 Then
        For i = 0 To EcListaSC.ListCount - 1
            rsCodigo.Source = "SELECT cod_motivo FROM sl_motivo WHERE seq_motivo = " & EcListaSC.ItemData(i)
            rsCodigo.Open
            
            If rsCodigo.RecordCount <> 0 Then
                SQLQuery = "INSERT INTO sl_motivo_req(n_req,cod_motivo) VALUES(" & NReq & "," & BL_TrataStringParaBD(BL_HandleNull(rsCodigo!cod_motivo, "0")) & ")"
                BG_ExecutaQuery_ADO SQLQuery
            End If
            
            rsCodigo.Close
        Next i
    End If
    
    If gF_REQUIS = 1 Or gF_REQUIS_PRIVADO = 1 Or gF_REQUIS_VET = mediSim Then
    Select Case gLAB
        Case "HSMARIA", "BIO", "GM", "CHVNG"
        If CkCard.value = 0 Then
            EcCard = "N"
        Else
            EcCard = "S"
        End If
        If CkHep.value = 0 Then
            EcHep = "N"
        Else
            EcHep = "S"
        End If
        If CkEdema.value = 0 Then
            EcEdema = "N"
        Else
            EcEdema = "S"
        End If
        If CkAscite.value = 0 Then
            EcAscite = "N"
        Else
            EcAscite = "S"
        End If
        
        SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
        BG_ExecutaQuery_ADO SQLQuery
        
'        SQLQuery = "INSERT INTO sl_inf_clinica(n_req,peso,altura,creatinina,flg_i_card,flg_i_hep,flg_edema,flg_ascite,albumina,idade,t_idade,idade_dias) VALUES (" & _
'                    NReq & "," & BL_TrataStringParaBD(EcPeso) & "," & BL_TrataStringParaBD(EcAltura) & "," & _
'                    BL_TrataStringParaBD(EcCreatinina) & "," & BL_TrataStringParaBD(flg_i_card) & "," & _
'                    BL_TrataStringParaBD(flg_i_hep) & "," & BL_TrataStringParaBD(flg_edema) & "," & _
'                    BL_TrataStringParaBD(flg_ascite) & "," & BL_TrataStringParaBD(EcAlbumina) & "," & _
'                    EcIdade.Text & "," & EcTipoIdade.ListIndex & ",'" & Replace(EcIdadeDias, ",", ".") & "')"
'        BG_ExecutaQuery_ADO SQLQuery
    End Select
    End If
    
    Set rsCodigo = Nothing
    
End Sub

Sub BD_Update()
    FlgGravou = True
    Call EcTipoIdade_Validate(False)
    UPDATE_DiagSec_TM_SC_Req
    Unload Me

End Sub

Sub UPDATE_DiagSec_TM_SC_Req()
    
    Dim sql As String
    Dim NReq As String
    'Delete dos diagnosticos prim�rios
    sql = "DELETE FROM sl_diag_pri WHERE seq_utente=" & gDUtente.seq_utente
    BG_ExecutaQuery_ADO sql
    
    'Delete dos dados da familia do utente
    If EcCodGrDiag.text <> "" And EcCodFamilia.text <> "" Then
        sql = "DELETE FROM sl_familias WHERE seq_utente=" & gDUtente.seq_utente & " and cod_gr_diag= " & BL_TrataStringParaBD(EcCodGrDiag.text)
        BG_ExecutaQuery_ADO sql
    End If
    
    If gF_REQUIS = 1 Then
        If Trim(FormGestaoRequisicao.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicao.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
        If NReq <> "" Then
            Select Case gLAB
                Case "HSMARIA", "BIO", "GM", "CITO", "ENZ", "CHVNG"
                sql = "DELETE FROM sl_inf_clinica WHERE n_req  = " & NReq
                BG_ExecutaQuery_ADO sql
            End Select
            
            'Delete dos diagnosticos secund�rios
            sql = "DELETE FROM sl_diag_sec WHERE n_req=" & NReq
            BG_ExecutaQuery_ADO sql
            
            'Delete das terap�uticas e medica��es
            sql = "DELETE FROM sl_tm_ute WHERE n_req=" & NReq
            BG_ExecutaQuery_ADO sql
            
            'Delete das suspeitas cl�nicas
            sql = "DELETE FROM sl_motivo_req WHERE n_req=" & NReq
            BG_ExecutaQuery_ADO sql
        End If
    ElseIf gF_REQUIS_PRIVADO = 1 Then
        If Trim(FormGestaoRequisicaoPrivado.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicaoPrivado.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
        
        If NReq <> "" Then
            Select Case gLAB
                Case "HSMARIA", "BIO", "GM", "CITO", "ENZ", "CHVNG"
                sql = "DELETE FROM sl_inf_clinica WHERE n_req  = " & NReq
                BG_ExecutaQuery_ADO sql
            End Select
            
            'Delete dos diagnosticos secund�rios
            sql = "DELETE FROM sl_diag_sec WHERE n_req=" & NReq
            BG_ExecutaQuery_ADO sql
            
            'Delete das terap�uticas e medica��es
            sql = "DELETE FROM sl_tm_ute WHERE n_req=" & NReq
            BG_ExecutaQuery_ADO sql
            
            'Delete das suspeitas cl�nicas
            sql = "DELETE FROM sl_motivo_req WHERE n_req=" & NReq
            BG_ExecutaQuery_ADO sql
        End If
    ElseIf gF_REQUIS_VET = mediSim Then
        If FormGestaoRequisicaoVet.EcNumReq.text <> "" Then
            Select Case gLAB
                Case "HSMARIA", "BIO", "GM", "CITO", "ENZ", "CHVNG"
                sql = "DELETE FROM sl_inf_clinica WHERE n_req  = " & FormGestaoRequisicaoVet.EcNumReq.text
                BG_ExecutaQuery_ADO sql
            End Select
            
            'Delete dos diagnosticos secund�rios
            sql = "DELETE FROM sl_diag_sec WHERE n_req=" & FormGestaoRequisicaoVet.EcNumReq.text
            BG_ExecutaQuery_ADO sql
            
            'Delete das terap�uticas e medica��es
            sql = "DELETE FROM sl_tm_ute WHERE n_req=" & FormGestaoRequisicaoVet.EcNumReq.text
            BG_ExecutaQuery_ADO sql
            
            'Delete das suspeitas cl�nicas
            sql = "DELETE FROM sl_motivo_req WHERE n_req=" & FormGestaoRequisicaoVet.EcNumReq.text
            BG_ExecutaQuery_ADO sql
        End If
    End If
    
    BD_Insert

End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    PreencheListas_Prim_Sec_TM_SC
    If gLAB = "ENZ" Or gLAB = "GM" Or gLAB = "BIO" Or gLAB = "CITO" Then
        Preenche_Familia
    End If
    BL_FimProcessamento Me
    
    If gLAB = "ENZ" Then
        EcCasoPai.Visible = True
        EcCasoMae.Visible = True
        EcCasoReferencia.Visible = True
    End If
        
End Sub

Sub FuncaoModificar()
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Update
    End If

End Sub

Sub PreencheListas_Prim_Sec_TM_SC()
    
    On Error GoTo ErrorHandler
    
    Dim Tabela As ADODB.recordset
    Dim tabelaAux As ADODB.recordset
    Dim sql As String
    Dim SQLAux As String
    Dim i As Integer
    Dim NReq As Long
    Dim SelTotal As Boolean
    
    Set Tabela = New ADODB.recordset
    Set tabelaAux = New ADODB.recordset
    
    'Lista diagn�sticos prim�rios
    sql = ""
    If gF_REQUIS = 1 Then
        If Trim(FormGestaoRequisicao.EcSeqUtente.text) <> "" Then
            sql = "SELECT seq_utente,cod_diag FROM sl_diag_pri WHERE "
            sql = sql & "seq_utente=" & Trim(FormGestaoRequisicao.EcSeqUtente.text)
        End If
    ElseIf gF_REQUIS_PRIVADO = 1 Then
        If Trim(FormGestaoRequisicaoPrivado.EcSeqUtente.text) <> "" Then
            sql = "SELECT seq_utente,cod_diag FROM sl_diag_pri WHERE "
            sql = sql & "seq_utente=" & Trim(FormGestaoRequisicaoPrivado.EcSeqUtente.text)
        End If
    ElseIf gF_REQUIS_VET = 1 Then
        If Trim(FormGestaoRequisicaoVet.EcSeqUtente.text) <> "" Then
            sql = "SELECT seq_utente,cod_diag FROM sl_diag_pri WHERE "
            sql = sql & "seq_utente=" & Trim(FormGestaoRequisicaoVet.EcSeqUtente.text)
        End If
    ElseIf gF_IDENTIF = 1 Then
        If Trim(FormIdentificaUtente.EcCodSequencial.text) <> "" Then
            sql = "SELECT seq_utente,cod_diag FROM sl_diag_pri WHERE "
            sql = sql & "seq_utente=" & Trim(FormIdentificaUtente.EcCodSequencial.text)
        End If
    ElseIf gF_IDENTIF_VET = 1 Then
        If Trim(FormIdentificaVet.EcCodSequencial.text) <> "" Then
            sql = "SELECT seq_utente,cod_diag FROM sl_diag_pri WHERE "
            sql = sql & "seq_utente=" & Trim(FormIdentificaVet.EcCodSequencial.text)
        End If
    ElseIf gF_REQUTE = 1 Then
        If Trim(FormReqUte.EcCodUteSeq.text) <> "" Then
            sql = "SELECT seq_utente,cod_diag FROM sl_diag_pri WHERE "
            sql = sql & "seq_utente=" & Trim(FormReqUte.EcCodUteSeq.text)
        End If
    End If
    
    If sql <> "" Then
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
        
        If Tabela.RecordCount > 0 Then
            i = 0
            While Not Tabela.EOF
                SQLAux = "SELECT seq_diag, descr_diag FROM sl_diag WHERE cod_diag='" & Trim(Tabela!cod_diag) & "'"
                tabelaAux.CursorType = adOpenStatic
                tabelaAux.CursorLocation = adUseServer
                tabelaAux.Open SQLAux, gConexao
                If tabelaAux.RecordCount > 0 Then
                    EcListaPrim.AddItem Trim("" & tabelaAux!Descr_Diag)
                    EcListaPrim.ItemData(i) = BL_HandleNull(tabelaAux!seq_diag, 0)
                    i = i + 1
                End If
                tabelaAux.Close
                Tabela.MoveNext
            Wend
        End If
        Tabela.Close
    End If
    
    If gF_REQUIS = 1 Then
        If Trim(FormGestaoRequisicao.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicao.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
    ElseIf gF_REQUIS_PRIVADO = 1 Then
        If Trim(FormGestaoRequisicaoPrivado.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicaoPrivado.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
    ElseIf gF_REQUIS_VET = 1 Then
        If Trim(FormGestaoRequisicaoVet.EcNumReq) <> "" Then
            NReq = FormGestaoRequisicaoVet.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
    ElseIf gF_REQUTE = 1 Then
        If Trim(FormReqUte.EcNumReq) <> "" Then
            NReq = FormReqUte.EcNumReq
        Else
            NReq = "-" & gNumeroSessao
        End If
    End If
    
    'Lista diagn�sticos secund�rios, a form onde estava era a de requisi��es
    If gF_REQUIS = 1 Or gF_REQUIS_PRIVADO = 1 Or gF_REQUIS_VET = 1 Or gF_REQUTE = 1 Then
        sql = "SELECT n_req,cod_diag FROM sl_diag_sec WHERE "
        'If Trim(FormGestaoRequisicao.EcNumReq.Text) <> "" Then
            sql = sql & "n_req=" & Trim(NReq)
        'Else
        '    Sql = Sql & "n_req=" & gNumeroSessao
        'End If
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount > 0 Then
            i = 0
            While Not Tabela.EOF
                SQLAux = "SELECT seq_diag, descr_diag FROM sl_diag WHERE cod_diag='" & Trim(Tabela!cod_diag) & "'"
                tabelaAux.CursorType = adOpenStatic
                tabelaAux.CursorLocation = adUseServer
                tabelaAux.Open SQLAux, gConexao
                If tabelaAux.RecordCount > 0 Then
                    EcListaSec.AddItem Trim("" & tabelaAux!Descr_Diag)
                    EcListaSec.ItemData(i) = BL_HandleNull(tabelaAux!seq_diag, 0)
                    i = i + 1
                End If
                tabelaAux.Close
                Tabela.MoveNext
            Wend
        End If
        Tabela.Close
    End If
    
    'Lista de terap�uticas e medica��es, a form onde estava era a de requisi��es
    If gF_REQUIS = 1 Or gF_REQUIS_PRIVADO = 1 Or gF_REQUTE = 1 Then
        sql = "SELECT n_req,cod_tera_med FROM sl_tm_ute WHERE "
        'If Trim(FormGestaoRequisicao.EcNumReq.Text) <> "" Then
            sql = sql & "n_req=" & Trim(NReq)
        'Else
        '    Sql = Sql & "n_req=" & gNumeroSessao
        'End If
            
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            i = 0
            While Not Tabela.EOF
                ' pferreira 2011.05.05
                SQLAux = "SELECT seq_tera_med, descr_tera_med FROM sl_tera_med WHERE seq_tera_med='" & Trim(Tabela!cod_tera_med) & "'"
                tabelaAux.CursorType = adOpenStatic
                tabelaAux.CursorLocation = adUseServer
                tabelaAux.Open SQLAux, gConexao
                If tabelaAux.RecordCount > 0 Then
                    EcListaTM.AddItem Trim("" & tabelaAux!descr_tera_med)
                    EcListaTM.ItemData(i) = BL_HandleNull(tabelaAux!seq_tera_med, 0)
                    i = i + 1
                End If
                tabelaAux.Close
                Tabela.MoveNext
            Wend
        End If
        Tabela.Close
      End If
      
    'Lista de suspeitas cl�nicas, a form onde estava era a de requisi��es
    If gF_REQUIS = 1 Or gF_REQUIS_PRIVADO = 1 Or gF_REQUTE = 1 Then
        sql = "SELECT n_req,cod_motivo FROM sl_motivo_req WHERE "
        'If Trim(FormGestaoRequisicao.EcNumReq.Text) <> "" Then
            sql = sql & "n_req=" & Trim(NReq)
'        Else
'            Sql = Sql & "n_req=" & gNumeroSessao
'        End If
            
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            i = 0
            While Not Tabela.EOF
                SQLAux = "SELECT seq_motivo, descr_motivo FROM sl_motivo WHERE cod_motivo='" & Trim(Tabela!cod_motivo) & "'"
                tabelaAux.CursorType = adOpenStatic
                tabelaAux.CursorLocation = adUseServer
                tabelaAux.Open SQLAux, gConexao
                If tabelaAux.RecordCount > 0 Then
                    EcListaSC.AddItem Trim("" & tabelaAux!descr_motivo)
                    EcListaSC.ItemData(i) = BL_HandleNull(tabelaAux!seq_motivo, 0)
                    i = i + 1
                End If
                tabelaAux.Close
                Tabela.MoveNext
            Wend
        End If
        Tabela.Close
    End If
    
    'Informa��o cl�nica
    If gF_REQUIS = 1 Or gF_REQUIS_PRIVADO = 1 Or gF_REQUTE = 1 Then
        If gF_REQUIS = 1 Then
            If Trim(FormGestaoRequisicao.EcNumReq) <> "" Then
                EcNumReq = FormGestaoRequisicao.EcNumReq
            Else
                EcNumReq = "-" & gNumeroSessao
            End If
        ElseIf gF_REQUIS_PRIVADO = 1 Then
            If Trim(FormGestaoRequisicaoPrivado.EcNumReq) <> "" Then
                EcNumReq = FormGestaoRequisicaoPrivado.EcNumReq
            Else
                EcNumReq = "-" & gNumeroSessao
            End If
        ElseIf gF_REQUTE = 1 Then
            If Trim(FormReqUte.EcNumReq) <> "" Then
                EcNumReq = FormReqUte.EcNumReq
            Else
                EcNumReq = "-" & gNumeroSessao
            End If
        End If
    
        CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open CriterioTabela, gConexao
        If Tabela.RecordCount > 0 Then
            BG_PreencheCampoEc_Todos_ADO Tabela, CamposBD, CamposEc
            If EcCard = "S" Then
                CkCard.value = 1
            Else
                CkCard.value = 0
            End If
            If EcHep = "S" Then
                CkHep.value = 1
            Else
                CkHep.value = 0
            End If
            If EcEdema = "S" Then
                CkEdema.value = 1
            Else
                CkEdema.value = 0
            End If
            If EcAscite = "S" Then
                CkAscite.value = 1
            Else
                CkAscite.value = 0
            End If
            If EcIdade.text = "" And gF_REQUIS = 1 Then
                If gLAB = "BIO" Or gLAB = "GM" Or gLAB = "CITO" Or gLAB = "ENZ" Then
                    Call CalculaIdade(BL_HandleNull(FormGestaoRequisicao.EcDataNasc.text, Null), BL_HandleNull(FormGestaoRequisicao.EcDataPrevista.text, ""))
                Else
                    Call CalculaIdade(BL_HandleNull(FormGestaoRequisicao.EcDataNasc.text, Null), BL_HandleNull(FormGestaoRequisicao.EcDataChegada.text, ""))
                End If
            ElseIf EcIdade.text = "" And gF_REQUIS_PRIVADO = 1 Then
                Call CalculaIdade(BL_HandleNull(FormGestaoRequisicaoPrivado.EcDataNasc.text, Null), BL_HandleNull(FormGestaoRequisicaoPrivado.EcDataChegada.text, ""))
            End If
        Else
            If gF_REQUIS = 1 Then
                Call CalculaIdade(BL_HandleNull(FormGestaoRequisicao.EcDataNasc.text, Null), BL_HandleNull(FormGestaoRequisicao.EcDataPrevista.text, ""))
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                Call CalculaIdade(BL_HandleNull(FormGestaoRequisicaoPrivado.EcDataNasc.text, Null), BL_HandleNull(FormGestaoRequisicaoPrivado.EcDataPrevista.text, ""))
            End If
        End If
        Tabela.Close
    End If
    

      
    Set Tabela = Nothing
    If Not tabelaAux Is Nothing Then
        Set tabelaAux = Nothing
    End If
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : PreencheListas_Prim_Sec_TM_SC (FormInformacaoClinica) -> " & Err.Description)
            Exit Sub
    End Select
End Sub


Sub FuncaoLimpar()
    
    LimpaCampos
    CampoDeFocus.SetFocus

End Sub

Sub FuncaoEstadoAnterior()
    
    Unload Me

End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    EcPesqDiag.text = ""
    EcPesqTeraMed.text = ""
    EcPesqSClinica.text = ""
    EcListaDiag.Clear
    EcListaPrim.Clear
    EcListaSec.Clear
    EcListaTeraMed.Clear
    EcListaTM.Clear
    EcListaSClinica.Clear
    EcListaSC.Clear
    EcInfCompl = ""
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_t_data", "cod_t_data", "descr_t_data", EcTipoIdade
    
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoPai
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoMae
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoReferencia
    
    Inicializa_FGFamilias
    
End Sub
Private Sub EcTipoIdade_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcTipoIdade.ListIndex = -1
End Sub



Private Sub EcTipoIdade_Validate(Cancel As Boolean)
    
    If EcTipoIdade.ListIndex = CInt(gT_Crianca) Or EcTipoIdade.ListIndex = CInt(gT_Adulto) Then
        EcIdade.text = "999"
        EcIdadeDias.text = "999"
    End If
    
    If EcIdade.text <> "999" And Trim(EcIdade.text) <> "" Then
        EcIdadeDias.text = BL_Converte_Para_Dias(EcIdade, BL_SelCodigo("sl_tbf_t_data", "cod_t_data", "descr_t_data", EcTipoIdade))
    End If
    
End Sub
Sub CalculaIdade(DataNasc As Date, Optional DataChega As Date)
    Dim mes As Integer
    Dim dia As Integer
    Dim idade As Integer
    Dim data As Date
    
    If IsNull(DataChega) Then
        data = Now
    Else
        data = DataChega
    End If
    
    If Month(DataNasc) = Month(data) Then
        mes = -1
    ElseIf Month(DataNasc) < Month(data) Then
        mes = 1
    Else
        mes = 0
    End If
    
    If Day(DataNasc) <= Day(data) Then
        dia = 1
    Else
        dia = 0
    End If
    
    Select Case 2 * mes + dia
        Case -2, 0, 1
            idade = DateDiff("yyyy", DataNasc, data) - 1
            EcTipoIdade.ListIndex = 2   'anos
            If idade < 1 Then
                If dia = 1 Then
                    idade = DateDiff("m", DataNasc, data)
                Else
                    idade = DateDiff("m", DataNasc, data) - 1
                End If
                EcTipoIdade.ListIndex = 1   'meses
            End If
            If idade < 1 Then
                idade = DateDiff("d", DataNasc, data) - 1
                EcTipoIdade.ListIndex = 0   'dias
            End If
            EcIdade.text = idade
        Case -1, 2, 3
            idade = DateDiff("yyyy", DataNasc, data)
            EcTipoIdade.ListIndex = 2   'anos
            If idade < 1 Then
                If dia = 0 Then
                    idade = DateDiff("m", DataNasc, data) - 1
                Else
                    idade = DateDiff("m", DataNasc, data)
                End If
                EcTipoIdade.ListIndex = 1   'meses
            End If
            If idade < 1 Then
                idade = DateDiff("d", DataNasc, data)
                EcTipoIdade.ListIndex = 0   'dias
            End If
            EcIdade.text = idade
    End Select
    
End Sub

Private Sub CbTipoPai_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoPai, KeyCode

End Sub
Private Sub CbTipoMae_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoMae, KeyCode

End Sub
Private Sub CbTipoReferencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoReferencia, KeyCode

End Sub

Private Sub CbTipoPai_Validate(Cancel As Boolean)
    
    Call Valida_Utente(Cancel, CbTipoPai.text, EcPai.text, "PAI")
    
End Sub
Private Sub CbTipoMae_Validate(Cancel As Boolean)
    
    Call Valida_Utente(Cancel, CbTipoMae.text, EcMae.text, "MAE")
    
End Sub
Private Sub CbTipoReferencia_Validate(Cancel As Boolean)
    
    Call Valida_Utente(Cancel, CbTipoReferencia.text, EcReferencia.text, "REFERENCIA")
    
End Sub
Private Sub EcPai_Validate(Cancel As Boolean)
    
    Call Valida_Utente(Cancel, CbTipoPai.text, EcPai.text, "PAI")
    
End Sub

Private Sub EcMae_Validate(Cancel As Boolean)
    
    Call Valida_Utente(Cancel, CbTipoMae.text, EcMae.text, "MAE")
    
End Sub
Private Sub EcReferencia_Validate(Cancel As Boolean)
    
    Call Valida_Utente(Cancel, CbTipoReferencia.text, EcReferencia.text, "REFERENCIA")
    
End Sub
Private Sub Valida_Utente(Cancel As Boolean, Optional CbTipoUtente As String, Optional EcUtente As String, Optional Utente As String, Optional SeqUtente As Long)
    
    Dim sql As String
    Dim RsUtente As ADODB.recordset
    
    'Limpa a estrutura de dados do utente
    If CbTipoUtente <> "" And EcUtente <> "" Then
        Set RsUtente = New ADODB.recordset
        RsUtente.CursorLocation = adUseServer
        RsUtente.CursorType = adOpenStatic
        
        sql = "select seq_utente,t_utente,utente,nome_ute, n_proc_2 from sl_identif where " & _
              " t_utente = " & BL_TrataStringParaBD(CbTipoUtente) & " And Utente = " & BL_TrataStringParaBD(EcUtente) & ""
                
        RsUtente.Open sql, gConexao
        If RsUtente.RecordCount > 0 Then
            If Utente = "PAI" Then
                EcSeqPai.text = RsUtente!seq_utente
                EcNomePai.text = RsUtente!nome_ute
                EcCasoPai.text = BL_HandleNull(RsUtente!n_proc_2, "")
            ElseIf Utente = "MAE" Then
                EcSeqMae.text = RsUtente!seq_utente
                EcNomeMae.text = RsUtente!nome_ute
                EcCasoMae.text = BL_HandleNull(RsUtente!n_proc_2, "")
            ElseIf Utente = "REFERENCIA" Then
                EcSeqReferencia.text = RsUtente!seq_utente
                EcNomeReferencia.text = RsUtente!nome_ute
                EcCasoReferencia.text = BL_HandleNull(RsUtente!n_proc_2, "")
            End If
        Else
            Cancel = True
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbInformation + vbOKOnly, "Pesquisa ao utente!"
            If Utente = "PAI" Then
                EcNomePai.text = ""
                EcCasoPai.text = ""
            ElseIf Utente = "MAE" Then
                EcNomeMae.text = ""
                EcCasoMae.text = ""
            ElseIf Utente = "REFERENCIA" Then
                EcNomeReferencia.text = ""
                EcCasoReferencia.text = ""
            End If
        End If
    ElseIf SeqUtente <> 0 Then
        Set RsUtente = New ADODB.recordset
        RsUtente.CursorLocation = adUseServer
        RsUtente.CursorType = adOpenStatic
        
        sql = "select seq_utente,t_utente,utente,nome_ute, n_proc_2 from sl_identif where seq_utente = " & SeqUtente
                
        RsUtente.Open sql, gConexao
        If RsUtente.RecordCount > 0 Then
            If Utente = "PAI" Then
                CbTipoPai.text = RsUtente!t_utente
                EcPai.text = RsUtente!Utente
                EcNomePai.text = RsUtente!nome_ute
                EcCasoPai.text = BL_HandleNull(RsUtente!n_proc_2, "")
            ElseIf Utente = "MAE" Then
                CbTipoMae.text = RsUtente!t_utente
                EcMae.text = RsUtente!Utente
                EcNomeMae.text = RsUtente!nome_ute
                EcCasoMae.text = BL_HandleNull(RsUtente!n_proc_2, "")
            ElseIf Utente = "REFERENCIA" Then
                CbTipoReferencia.text = RsUtente!t_utente
                EcReferencia.text = RsUtente!Utente
                EcNomeReferencia.text = RsUtente!nome_ute
                EcCasoReferencia.text = BL_HandleNull(RsUtente!n_proc_2, "")
            End If
        Else
            Cancel = True
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbInformation + vbOKOnly, "Pesquisa ao utente!"
            If Utente = "PAI" Then
                EcNomePai.text = ""
                EcCasoPai.text = ""
            ElseIf Utente = "MAE" Then
                EcNomeMae.text = ""
                EcCasoMae.text = ""
            ElseIf Utente = "REFERENCIA" Then
                EcNomeReferencia.text = ""
                EcCasoReferencia.text = ""
            End If
        End If
    End If
End Sub

Sub Inicializa_FGFamilias()

    With FGFamilias
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 1450
        .Col = 0
        .TextMatrix(0, 0) = "Utente"
        .ColWidth(1) = 800
        .Col = 1
        .TextMatrix(0, 1) = "Fam�lia"
        .ColWidth(2) = 800
        .Col = 2
        .TextMatrix(0, 2) = "Patologia"
        .ColWidth(3) = 700
        .Col = 3
        .TextMatrix(0, 3) = "Membro"
        .ColWidth(4) = 900
        .Col = 4
        .TextMatrix(0, 4) = "Ano Estudo"
        .ColWidth(5) = 1450
        .Col = 5
        .TextMatrix(0, 5) = "Estatuto"
        .ColWidth(6) = 1450
        .Col = 6
        .TextMatrix(0, 6) = "Parentesco"
        .ColWidth(7) = 1450
        .Col = 7
        .TextMatrix(0, 7) = "Refer�ncia"
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
End Sub

Sub Preenche_Familia()
    On Error GoTo ErrorHandler
    Dim sql As String
    Dim i As Integer

    
    Set rs = New ADODB.recordset
    
    'Dados da familia do utente
    sql = ""
    If gF_REQUIS = 1 Then
        If Trim(FormGestaoRequisicao.EcSeqUtente.text) <> "" Then

            sql = "select nr_familia,id_u.seq_utente seq_utente, id_u.t_utente t_utente,id_u.utente utente ,f.seq_pai ,id_p.t_utente t_pai,id_p.utente pai,f.seq_mae,id_m.t_utente t_mae,id_m.utente mae,f.seq_referencia, id_r.t_utente t_referencia ,id_r.utente referencia, " & _
                " cod_gr_diag,ano_estudo,membro,estatuto, parentesco, feto, f.obs, descr_estat " & _
                " from sl_familias f, sl_identif id_u, sl_identif id_p, sl_identif id_m, sl_identif id_r, sl_tbf_estatuto e where " & _
                " f.seq_utente = id_u.seq_utente and f.seq_mae = id_m.seq_utente(+) and f.seq_pai = id_p.seq_utente(+) and f.seq_referencia = id_r.seq_utente(+) and f.estatuto = e.cod_estat and " & _
                " f.seq_utente=" & Trim(FormGestaoRequisicao.EcSeqUtente.text)
                
        End If
    ElseIf gF_IDENTIF = 1 Then
        If Trim(FormIdentificaUtente.EcCodSequencial.text) <> "" Then
            sql = "select nr_familia,id_u.seq_utente seq_utente, id_u.t_utente t_utente,id_u.utente utente ,f.seq_pai ,id_p.t_utente t_pai,id_p.utente pai,f.seq_mae,id_m.t_utente t_mae,id_m.utente mae,f.seq_referencia, id_r.t_utente t_referencia ,id_r.utente referencia, " & _
                " cod_gr_diag,ano_estudo,membro,estatuto, parentesco, feto, f.obs, descr_estat " & _
                " from sl_familias f, sl_identif id_u, sl_identif id_p, sl_identif id_m, sl_identif id_r, sl_tbf_estatuto e where " & _
                " f.seq_utente = id_u.seq_utente and f.seq_mae = id_m.seq_utente(+) and f.seq_pai = id_p.seq_utente(+) and f.seq_referencia = id_r.seq_utente(+) and f.estatuto = e.cod_estat and " & _
                " f.seq_utente=" & Trim(FormIdentificaUtente.EcCodSequencial.text)
        End If
    ElseIf gF_IDENTIF_VET = 1 Then
        If Trim(FormIdentificaVet.EcCodSequencial.text) <> "" Then
            sql = "select nr_familia,id_u.seq_utente seq_utente, id_u.t_utente t_utente,id_u.utente utente ,f.seq_pai ,id_p.t_utente t_pai,id_p.utente pai,f.seq_mae,id_m.t_utente t_mae,id_m.utente mae,f.seq_referencia, id_r.t_utente t_referencia ,id_r.utente referencia, " & _
                " cod_gr_diag,ano_estudo,membro,estatuto, parentesco, feto, f.obs, descr_estat " & _
                " from sl_familias f, sl_identif id_u, sl_identif id_p, sl_identif id_m, sl_identif id_r, sl_tbf_estatuto e where " & _
                " f.seq_utente = id_u.seq_utente and f.seq_mae = id_m.seq_utente(+) and f.seq_pai = id_p.seq_utente(+) and f.seq_referencia = id_r.seq_utente(+) and f.estatuto = e.cod_estat and " & _
                " f.seq_utente=" & Trim(FormIdentificaVet.EcCodSequencial.text)
        End If
    ElseIf gF_REQUTE = 1 Then
        If Trim(FormReqUte.EcCodUteSeq.text) <> "" Then
            sql = "select nr_familia,id_u.seq_utente seq_utente, id_u.t_utente t_utente,id_u.utente utente ,f.seq_pai ,id_p.t_utente t_pai,id_p.utente pai,f.seq_mae,id_m.t_utente t_mae,id_m.utente mae,f.seq_referencia, id_r.t_utente t_referencia ,id_r.utente referencia, " & _
                " cod_gr_diag,ano_estudo,membro,estatuto, parentesco, feto, f.obs,descr_estat " & _
                " from sl_familias f, sl_identif id_u, sl_identif id_p, sl_identif id_m, sl_identif id_r, sl_tbf_estatuto e where " & _
                " f.seq_utente = id_u.seq_utente and f.seq_mae = id_m.seq_utente(+) and f.seq_pai = id_p.seq_utente(+) and f.seq_referencia = id_r.seq_utente(+) and f.estatuto = e.cod_estat and " & _
                " f.seq_utente=" & Trim(FormReqUte.EcCodUteSeq.text)
        End If
    End If
    
    If sql <> "" Then
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
        rs.Open sql, gConexao
        
        If rs.RecordCount > 0 Then
                
            For i = 1 To rs.RecordCount
                FGFamilias.TextMatrix(i, 0) = BL_HandleNull(rs!t_utente, "") & "/" & BL_HandleNull(rs!Utente, "")
                FGFamilias.TextMatrix(i, 1) = BL_HandleNull(rs!nr_familia, "")
                FGFamilias.TextMatrix(i, 2) = BL_HandleNull(rs!cod_gr_diag, "")
                FGFamilias.TextMatrix(i, 3) = BL_HandleNull(rs!Membro, "") & IIf(BL_HandleNull(rs!feto, "") <> "", "-F" & BL_HandleNull(rs!feto, ""), "")
                FGFamilias.TextMatrix(i, 4) = BL_HandleNull(rs!ano_estudo, "")
                FGFamilias.TextMatrix(i, 5) = BL_HandleNull(rs!descr_estat, "")
                FGFamilias.TextMatrix(i, 6) = BL_HandleNull(rs!parentesco, "")
                FGFamilias.TextMatrix(i, 7) = IIf(BL_HandleNull(rs!t_referencia, "") <> "", BL_HandleNull(rs!t_referencia, "") & "/" & BL_HandleNull(rs!referencia, ""), "")
                If i < rs.RecordCount Then
                    FGFamilias.rows = FGFamilias.rows + 1
                End If
                If gF_REQUIS = 1 Then
                    'If (gLAB = "GM" And InStr(1, FormGestaoRequisicao.EcReqAux.Text, FGFamilias.TextMatrix(i, 1)) > 0) Or gLAB = "ENZ" Then
                    
                        EcCodFamilia.text = BL_HandleNull(rs!nr_familia, "")
                        EcCodGrDiag.text = BL_HandleNull(rs!cod_gr_diag, "")
                        Call EcCodGrDiag_Validate(True)
                        EcAnoEstudo.text = BL_HandleNull(rs!ano_estudo, "")
                        EcCodEstatuto.text = BL_HandleNull(rs!estatuto, "")
                        EcCodEstatuto_Validate False
                        EcMembro.text = BL_HandleNull(rs!Membro, "")
                        EcFeto.text = BL_HandleNull(rs!feto, "")
                        EcSeqMae.text = BL_HandleNull(rs!seq_mae, "")
                        EcParentesco.text = BL_HandleNull(rs!parentesco, "")
                        EcObs.text = BL_HandleNull(rs!obs, "")
                        If BL_HandleNull(rs!t_mae, "") = "" Then
                            CbTipoMae.ListIndex = mediComboValorNull
                        Else
                            CbTipoMae.text = BL_HandleNull(rs!t_mae, "")
                        End If
                        EcMae.text = BL_HandleNull(rs!mae, "")
                        Call Valida_Utente(False, CbTipoMae.text, EcMae.text, "MAE")
                        EcSeqPai.text = BL_HandleNull(rs!seq_pai, "")
                        If BL_HandleNull(rs!t_pai, "") = "" Then
                            CbTipoPai.ListIndex = -1
                        Else
                            CbTipoPai.text = BL_HandleNull(rs!t_pai, "")
                        End If
                        EcPai.text = BL_HandleNull(rs!Pai, "")
                        Call Valida_Utente(False, CbTipoPai.text, EcPai.text, "PAI")
                        EcSeqReferencia.text = BL_HandleNull(rs!seq_referencia, "")
                        If BL_HandleNull(rs!t_referencia, "") = "" Then
                            CbTipoReferencia.ListIndex = mediComboValorNull
                        Else
                            CbTipoReferencia.text = BL_HandleNull(rs!t_referencia, "")
                        End If
                        EcReferencia.text = BL_HandleNull(rs!referencia, "")
                        Call Valida_Utente(False, CbTipoReferencia.text, EcReferencia.text, "REFERENCIA")
                        
                    'End If
                ElseIf gF_REQUTE = 1 Then
                    If gLAB = "ENZ" Or (gLAB = "GM" And InStr(1, FormReqUte.EcReqAux.text, FGFamilias.TextMatrix(i, 1)) > 0) Then
                        EcCodFamilia.text = BL_HandleNull(rs!nr_familia, "")
                        EcCodGrDiag.text = BL_HandleNull(rs!cod_gr_diag, "")
                        Call EcCodGrDiag_Validate(True)
                        EcAnoEstudo.text = BL_HandleNull(rs!ano_estudo, "")
                        EcCodEstatuto.text = BL_HandleNull(rs!estatuto, "")
                        EcCodEstatuto_Validate (False)
                        EcMembro.text = BL_HandleNull(rs!Membro, "")
                        EcFeto.text = BL_HandleNull(rs!feto, "")
                        EcSeqMae.text = BL_HandleNull(rs!seq_mae, "")
                        EcParentesco.text = BL_HandleNull(rs!parentesco, "")
                        EcObs.text = BL_HandleNull(rs!obs, "")
                        If BL_HandleNull(rs!t_mae, "") = "" Then
                            CbTipoMae.ListIndex = mediComboValorNull
                        Else
                            CbTipoMae.text = BL_HandleNull(rs!t_mae, "")
                        End If
                        EcMae.text = BL_HandleNull(rs!mae, "")
                        Call Valida_Utente(False, CbTipoMae.text, EcMae.text, "MAE")
                        EcSeqPai.text = BL_HandleNull(rs!seq_pai, "")
                        If BL_HandleNull(rs!t_pai, "") = "" Then
                            CbTipoPai.ListIndex = -1
                        Else
                            CbTipoPai.text = BL_HandleNull(rs!t_pai, "")
                        End If
                        EcPai.text = BL_HandleNull(rs!Pai, "")
                        Call Valida_Utente(False, CbTipoPai.text, EcPai.text, "PAI")
                        EcSeqReferencia.text = BL_HandleNull(rs!seq_referencia, "")
                        If BL_HandleNull(rs!t_referencia, "") = "" Then
                            CbTipoReferencia.ListIndex = mediComboValorNull
                        Else
                            CbTipoReferencia.text = BL_HandleNull(rs!t_referencia, "")
                        End If
                        EcReferencia.text = BL_HandleNull(rs!referencia, "")
                        Call Valida_Utente(False, CbTipoReferencia.text, EcReferencia.text, "REFERENCIA")
                    End If
                ElseIf gF_IDENTIF = 1 Then
                
                        EcCodFamilia.text = BL_HandleNull(rs!nr_familia, "")
                        EcCodGrDiag.text = BL_HandleNull(rs!cod_gr_diag, "")
                        Call EcCodGrDiag_Validate(True)
                        EcAnoEstudo.text = BL_HandleNull(rs!ano_estudo, "")
                        EcCodEstatuto.text = BL_HandleNull(rs!estatuto, "")
                        EcCodEstatuto_Validate (False)
                        EcMembro.text = BL_HandleNull(rs!Membro, "")
                        EcFeto.text = BL_HandleNull(rs!feto, "")
                        EcSeqMae.text = BL_HandleNull(rs!seq_mae, "")
                        EcParentesco.text = BL_HandleNull(rs!parentesco, "")
                        EcObs.text = BL_HandleNull(rs!obs, "")
                        If BL_HandleNull(rs!t_mae, "") = "" Then
                            CbTipoMae.ListIndex = mediComboValorNull
                        Else
                            CbTipoMae.text = BL_HandleNull(rs!t_mae, "")
                        End If
                        EcMae.text = BL_HandleNull(rs!mae, "")
                        Call Valida_Utente(False, CbTipoMae.text, EcMae.text, "MAE")
                        EcSeqPai.text = BL_HandleNull(rs!seq_pai, "")
                        If BL_HandleNull(rs!t_pai, "") = "" Then
                            CbTipoPai.ListIndex = -1
                        Else
                            CbTipoPai.text = BL_HandleNull(rs!t_pai, "")
                        End If
                        EcPai.text = BL_HandleNull(rs!Pai, "")
                        Call Valida_Utente(False, CbTipoPai.text, EcPai.text, "PAI")
                        EcSeqReferencia.text = BL_HandleNull(rs!seq_referencia, "")
                        If BL_HandleNull(rs!t_referencia, "") = "" Then
                            CbTipoReferencia.ListIndex = mediComboValorNull
                        Else
                            CbTipoReferencia.text = BL_HandleNull(rs!t_referencia, "")
                        End If
                        EcReferencia.text = BL_HandleNull(rs!referencia, "")
                        Call Valida_Utente(False, CbTipoReferencia.text, EcReferencia.text, "REFERENCIA")
                End If
            

                rs.MoveNext
            Next i
'            rs.MoveFirst
'
'            EcCodFamilia.Text = BL_HandleNull(rs!nr_familia, "")
'            EcCodGrDiag.Text = BL_HandleNull(rs!cod_gr_diag, "")
'            Call EcCodGrDiag_Validate(True)
'            EcAnoEstudo.Text = BL_HandleNull(rs!ano_estudo, "")
'            EcMembro.Text = BL_HandleNull(rs!Membro, "")
'            EcFeto.Text = BL_HandleNull(rs!feto, "")
'            EcSeqMae.Text = BL_HandleNull(rs!seq_mae, "")
'            If BL_HandleNull(rs!t_mae, "") = "" Then
'                CbTipoMae.ListIndex = mediComboValorNull
'            Else
'                CbTipoMae.Text = BL_HandleNull(rs!t_mae, "")
'            End If
'            EcMae.Text = BL_HandleNull(rs!mae, "")
'            EcSeqPai.Text = BL_HandleNull(rs!seq_pai, "")
'            If BL_HandleNull(rs!t_pai, "") = "" Then
'                CbTipoPai.ListIndex = -1
'            Else
'                CbTipoPai.Text = BL_HandleNull(rs!t_pai, "")
'            End If
'            EcPai.Text = BL_HandleNull(rs!Pai, "")
'            EcSeqReferencia.Text = BL_HandleNull(rs!seq_referencia, "")
'            If BL_HandleNull(rs!t_referencia, "") = "" Then
'                CbTipoReferencia.ListIndex = mediComboValorNull
'            Else
'                CbTipoReferencia.Text = BL_HandleNull(rs!t_referencia, "")
'            End If
'            EcReferencia.Text = BL_HandleNull(rs!referencia, "")
        End If
        
    End If
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Preenche_Familia (FormInformacaoClinica) -> " & Err.Description)
            Exit Sub
    End Select
    
End Sub

Sub PreencheLista_Familia()
    On Error GoTo ErrorHandler
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim i As Integer

    
    Set Tabela = New ADODB.recordset
    
    'Lista da familia do utente
    sql = ""
    If EcCodFamilia.text <> "" Then
        sql = "select nr_familia,id_u.seq_utente seq_utente, id_u.t_utente t_utente,id_u.utente utente ,id_p.t_utente t_pai,id_p.utente pai,id_m.t_utente t_mae,id_m.utente mae,id_r.t_utente t_referencia ,id_r.utente referencia, " & _
                " cod_gr_diag,ano_estudo,membro,descr_estat,parentesco " & _
                " from sl_familias f, sl_identif id_u, sl_identif id_p, sl_identif id_m, sl_identif id_r, sl_tbf_estatuto e where " & _
                " f.seq_utente = id_u.seq_utente and f.seq_mae = id_m.seq_utente(+) and f.seq_pai = id_p.seq_utente(+) and f.seq_referencia = id_r.seq_utente(+) and " & _
                " f.estatuto = e.cod_estat(+) and " & _
                " nr_familia = " & EcCodFamilia.text & " and cod_gr_diag = " & BL_TrataStringParaBD(EcCodGrDiag.text)
    End If
    
    If sql <> "" Then
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
        
        If Tabela.RecordCount > 0 Then
            For i = 1 To Tabela.RecordCount
                FGFamilias.TextMatrix(i, 0) = BL_HandleNull(Tabela!t_utente, "") & "/" & BL_HandleNull(Tabela!Utente, "")
                FGFamilias.TextMatrix(i, 1) = BL_HandleNull(Tabela!nr_familia, "")
                FGFamilias.TextMatrix(i, 2) = BL_HandleNull(Tabela!cod_gr_diag, "")
                FGFamilias.TextMatrix(i, 3) = BL_HandleNull(Tabela!Membro, "")
                FGFamilias.TextMatrix(i, 4) = BL_HandleNull(Tabela!ano_estudo, "")
                FGFamilias.TextMatrix(i, 5) = BL_HandleNull(Tabela!descr_estatuto, "")
                FGFamilias.TextMatrix(i, 6) = BL_HandleNull(Tabela!parentesco, "")
                FGFamilias.TextMatrix(i, 7) = IIf(BL_HandleNull(Tabela!t_referencia, "") <> "", BL_HandleNull(Tabela!t_referencia, "") & "/" & BL_HandleNull(Tabela!referencia, ""), "")
                If i < Tabela.RecordCount Then
                    FGFamilias.rows = FGFamilias.rows + 1
                End If
                Tabela.MoveNext
            Next i
        End If
        'Tabela.Close
        'Set Tabela = Nothing
    End If
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : PreencheLista_Familia (FormInformacaoClinica) -> " & Err.Description)
            Exit Sub
    End Select
    
End Sub



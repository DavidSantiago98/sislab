VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormAssociacaoProfiles 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Associa��o de Profiles"
   ClientHeight    =   6165
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9645
   Icon            =   "FAssociacaoProfiles.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6165
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ImageList ImageListToolBar 
      Left            =   1080
      Top             =   4560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":000C
            Key             =   "KeyAdministracaoProfiles"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":05A6
            Key             =   "KeyGravar"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton BtGravar 
      Caption         =   "&Guardar"
      Height          =   540
      Left            =   8760
      Picture         =   "FAssociacaoProfiles.frx":0B40
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   5520
      Width           =   735
   End
   Begin VB.CommandButton BtDessasociar 
      Height          =   615
      Left            =   5160
      Picture         =   "FAssociacaoProfiles.frx":10CA
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Desassociar Profile"
      Top             =   2520
      Width           =   615
   End
   Begin VB.CommandButton BtAssociar 
      Height          =   615
      Left            =   5160
      Picture         =   "FAssociacaoProfiles.frx":1221
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Associar Profile"
      Top             =   1800
      Width           =   615
   End
   Begin MSComctlLib.ImageList ImageListUtilizadores 
      Left            =   3120
      Top             =   4080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":1371
            Key             =   "KeyGrupoUtilizadores"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":190B
            Key             =   "KeyUtilizadorComProfile"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":1EA5
            Key             =   "KeyUtilizadorSemProfile"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":243F
            Key             =   "KeyProfile"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListProfiles 
      Left            =   7440
      Top             =   4080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":29D9
            Key             =   "Key1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAssociacaoProfiles.frx":2F73
            Key             =   "Key2"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeViewUtilizadores 
      Height          =   5055
      Left            =   120
      TabIndex        =   4
      Top             =   360
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   8916
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   530
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
   End
   Begin VB.CommandButton BtAdmProfiles 
      Caption         =   "&Adm. Profiles"
      Height          =   540
      Left            =   5760
      Picture         =   "FAssociacaoProfiles.frx":337D
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Administra��o de Profiles"
      Top             =   5520
      Width           =   1095
   End
   Begin MSComctlLib.TreeView TreeViewProfiles 
      Height          =   5055
      Left            =   5760
      TabIndex        =   0
      Top             =   360
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   8916
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   354
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
   End
   Begin VB.Label LabelProfile 
      Caption         =   "Profiles"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   2
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label LabelUtilizadores 
      Caption         =   "Utilizadores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "FormAssociacaoProfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const KeyGrupoUtilizadores = "KeyGR"
Const KeyUtilizadores = "KeyU"
Const KeyProfile = "KeyP"

Const ImKeyGrupoUtilizadores = 0
Const ImKeyUtilizadorComProfile = 1
Const ImKeyUtilizadorSemProfile = 2
Const ImKeyProfile = 3

' --------------------------------------
' ESTRUTURA COM OS GRUPOS E UTILIZADORES
' --------------------------------------
Private Type Utilizadores
    tipo As String
    Codigo As String
    descricao As String
    id As String
    cod_pai As String
    cod_profile As String
End Type
Dim totalEstrutUtilizadores As Integer
Dim estrutUtilizadores() As Utilizadores

' --------------------------------------
' ESTRUTURA COM OS PROFILES
' --------------------------------------
Private Type profile
    Codigo As String
    ordem As Long
    descricao As String
    codigo_pai As String

End Type
Dim totalEstrutProfile As Long
Dim estrutProfile() As profile

Private Sub BtAdmProfiles_Click()
    FormAdministracaoProfiles.Show
    Set gFormActivo = FormAdministracaoProfiles
End Sub

Private Sub BtAssociar_Click()
    If TreeViewUtilizadores.SelectedItem Is Nothing Or TreeViewProfiles.SelectedItem Is Nothing Then
        Exit Sub
    End If
    AssociaUtilizador TreeViewUtilizadores.SelectedItem.Key, TreeViewProfiles.SelectedItem.Key
End Sub

Private Sub BtDessasociar_Click()
    If TreeViewUtilizadores.SelectedItem Is Nothing Or TreeViewProfiles.SelectedItem Is Nothing Then
        Exit Sub
    End If
    DesasociaUtilizador TreeViewUtilizadores.SelectedItem.Key, TreeViewProfiles.SelectedItem.Key
End Sub

Private Sub BtGravar_Click()
    GravaAssocProfiles
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = "Associa��o de Profiles"
    Me.left = 5
    Me.top = 5
    Me.Width = 9735
    Me.Height = 6555 ' Normal
    Set CampoDeFocus = BtGravar
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAssociacaoProfiles = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    

End Sub

Sub DefTipoCampos()
    

End Sub

Sub PreencheCampos()

End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    

End Function

Sub FuncaoProcurar()
      
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()
    

End Sub

Sub FuncaoInserir()
    
End Sub

Sub BD_Insert()
    
    
End Sub

Sub FuncaoModificar()
    

End Sub

Sub BD_Update()


End Sub

Sub FuncaoRemover()
    
    
End Sub

Sub BD_Delete()
    

End Sub


Sub PreencheValoresDefeito()
    InicializaTreeProfiles
    preencheTreeUtilizadores
End Sub




Private Sub preencheTreeUtilizadores()
  TreeViewUtilizadores.LineStyle = tvwRootLines
  TreeViewUtilizadores.ImageList = ImageListUtilizadores
  
  totalEstrutUtilizadores = 0
  ReDim estrutUtilizadores(0)
  PreencheGrupos
  PreencheUtilizadores
End Sub

' ----------------------------------------------------------------------------------

' PREENCHE GRUPOS

' ----------------------------------------------------------------------------------
Private Sub PreencheGrupos()
    Dim sSql As String
    Dim RsGr As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "gr_util"
    RsGr.CursorLocation = adUseServer
    RsGr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsGr.Open sSql, gConexao
    If RsGr.RecordCount >= 1 Then
        While Not RsGr.EOF
            totalEstrutUtilizadores = totalEstrutUtilizadores + 1
            ReDim Preserve estrutUtilizadores(totalEstrutUtilizadores)
            estrutUtilizadores(totalEstrutUtilizadores).Codigo = BL_HandleNull(RsGr!cod_gr_util, "")
            estrutUtilizadores(totalEstrutUtilizadores).descricao = BL_HandleNull(RsGr!descr_gr_util, "")
            estrutUtilizadores(totalEstrutUtilizadores).tipo = KeyGrupoUtilizadores
            estrutUtilizadores(totalEstrutUtilizadores).id = KeyGrupoUtilizadores & estrutUtilizadores(totalEstrutUtilizadores).Codigo
            estrutUtilizadores(totalEstrutUtilizadores).cod_profile = ""
            estrutUtilizadores(totalEstrutUtilizadores).cod_pai = ""
            RsGr.MoveNext
            Dim root As Node
            Set root = TreeViewUtilizadores.Nodes.Add(, tvwChild, estrutUtilizadores(totalEstrutUtilizadores).id, estrutUtilizadores(totalEstrutUtilizadores).descricao, DevolveImagemUtilizadores(ImKeyGrupoUtilizadores))
            root.Expanded = False
            
        Wend
    End If
    RsGr.Close
    Set RsGr = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheGrupoas " & Err.Description, Me.Name, "PreencheGrupoas"
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------

' DADO O CODIGO E TIPO(UTILIZADOR/GRUPO) DEVOLVE INDICE DA ESTRUTURA

' ----------------------------------------------------------------------------------
Private Function DevolveIndiceUtilizadoresDadoCodigo(tipo As String, Codigo As String) As Long
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To totalEstrutUtilizadores
        If estrutUtilizadores(i).Codigo = Codigo And estrutUtilizadores(i).tipo = tipo Then
            DevolveIndiceUtilizadoresDadoCodigo = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  DevolveIndiceUtilizadoresDadoCodigo " & Err.Description, Me.Name, "DevolveIndiceUtilizadoresDadoCodigo"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' DADO O ID DEVOLVE INDICE DA ESTRUTURA

' ----------------------------------------------------------------------------------
Private Function DevolveIndiceUtilizadoresDadoID(id As String) As Long
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To totalEstrutUtilizadores
        If estrutUtilizadores(i).id = id Then
            DevolveIndiceUtilizadoresDadoID = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  DevolveIndiceUtilizadoresDadoID " & Err.Description, Me.Name, "DevolveIndiceUtilizadoresDadoID"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' DADO O ID DEVOLVE TIPO DE NO

' ----------------------------------------------------------------------------------
Private Function DevolveTipoUtilizadoresDadoID(id As String) As Long
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To totalEstrutUtilizadores
        If estrutUtilizadores(i).id = id Then
            DevolveTipoUtilizadoresDadoID = estrutUtilizadores(i).tipo
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  DevolveTipoUtilizadoresDadoID " & Err.Description, Me.Name, "DevolveTipoUtilizadoresDadoID"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' PREENCHE UTILIZADORES

' ----------------------------------------------------------------------------------
Private Sub PreencheUtilizadores()
    Dim sSql As String
    Dim rsUtil As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "idutilizador WHERE flg_removido is null or flg_removido = 0"
    rsUtil.CursorLocation = adUseServer
    rsUtil.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsUtil.Open sSql, gConexao
    If rsUtil.RecordCount >= 1 Then
        While Not rsUtil.EOF
            totalEstrutUtilizadores = totalEstrutUtilizadores + 1
            ReDim Preserve estrutUtilizadores(totalEstrutUtilizadores)
            estrutUtilizadores(totalEstrutUtilizadores).Codigo = BL_HandleNull(rsUtil!cod_utilizador, "")
            estrutUtilizadores(totalEstrutUtilizadores).descricao = BL_HandleNull(rsUtil!nome, "")
            estrutUtilizadores(totalEstrutUtilizadores).tipo = KeyUtilizadores
            estrutUtilizadores(totalEstrutUtilizadores).id = KeyUtilizadores & estrutUtilizadores(totalEstrutUtilizadores).Codigo
            estrutUtilizadores(totalEstrutUtilizadores).cod_profile = BL_HandleNull(rsUtil!cod_profile, "")
            
            Dim root As Node
            If BL_HandleNull(rsUtil!cod_gr_util, "0") <> "0" Then
                estrutUtilizadores(totalEstrutUtilizadores).cod_pai = BL_HandleNull(rsUtil!cod_gr_util, "0")
                Set root = TreeViewUtilizadores.Nodes.Add(DevolveIndiceUtilizadoresDadoCodigo(KeyGrupoUtilizadores, estrutUtilizadores(totalEstrutUtilizadores).cod_pai), tvwChild, estrutUtilizadores(totalEstrutUtilizadores).id, estrutUtilizadores(totalEstrutUtilizadores).descricao, DevolveImagemUtilizadores(ImKeyUtilizadorSemProfile))
                root.Expanded = False
            Else
                estrutUtilizadores(totalEstrutUtilizadores).cod_pai = "0"
                Set root = TreeViewUtilizadores.Nodes.Add(, tvwChild, estrutUtilizadores(totalEstrutUtilizadores).id, estrutUtilizadores(totalEstrutUtilizadores).descricao, DevolveImagemUtilizadores(ImKeyUtilizadorSemProfile))
                root.Expanded = False
            End If
            
            If estrutUtilizadores(totalEstrutUtilizadores).cod_profile <> "" Then
                totalEstrutUtilizadores = totalEstrutUtilizadores + 1
                ReDim Preserve estrutUtilizadores(totalEstrutUtilizadores)
                
                estrutUtilizadores(totalEstrutUtilizadores).Codigo = estrutUtilizadores(totalEstrutUtilizadores - 1).cod_profile
                estrutUtilizadores(totalEstrutUtilizadores).descricao = DevolveNomeProfile(estrutUtilizadores(totalEstrutUtilizadores - 1).cod_profile)
                estrutUtilizadores(totalEstrutUtilizadores).tipo = KeyProfile
                estrutUtilizadores(totalEstrutUtilizadores).id = KeyProfile & estrutUtilizadores(totalEstrutUtilizadores - 1).cod_profile
                estrutUtilizadores(totalEstrutUtilizadores).cod_profile = ""
                estrutUtilizadores(totalEstrutUtilizadores).cod_pai = estrutUtilizadores(totalEstrutUtilizadores - 1).Codigo
                

                root.Image = DevolveImagemUtilizadores(ImKeyUtilizadorComProfile)
                Set root = TreeViewUtilizadores.Nodes.Add(DevolveIndiceUtilizadoresDadoCodigo(KeyUtilizadores, estrutUtilizadores(totalEstrutUtilizadores).cod_pai), tvwChild, _
                           KeyUtilizadores & estrutUtilizadores(totalEstrutUtilizadores).cod_pai & "_" & estrutUtilizadores(totalEstrutUtilizadores).id, _
                           estrutUtilizadores(totalEstrutUtilizadores).descricao, DevolveImagemUtilizadores(ImKeyProfile))
                root.Expanded = False
            End If
            rsUtil.MoveNext
        Wend
    End If
    rsUtil.Close
    Set rsUtil = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheUtilizadores " & Err.Description, Me.Name, "PreencheUtilizadores"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' DEVOLVE IMAGEM CORRECTA

' ----------------------------------------------------------------------------------
Private Function DevolveImagemUtilizadores(estado As Long) As String
    On Error GoTo TrataErro
    If estado = ImKeyGrupoUtilizadores Then
        DevolveImagemUtilizadores = "KeyGrupoUtilizadores"
    ElseIf estado = ImKeyUtilizadorComProfile Then
        DevolveImagemUtilizadores = "KeyUtilizadorComProfile"
    ElseIf estado = ImKeyUtilizadorSemProfile Then
        DevolveImagemUtilizadores = "KeyUtilizadorSemProfile"
    ElseIf estado = ImKeyProfile Then
        DevolveImagemUtilizadores = "KeyProfile"
    End If
Exit Function
TrataErro:
    DevolveImagemUtilizadores = ""
    BG_LogFile_Erros "Erro  DevolveImagemUtilizadores " & Err.Description, Me.Name, "DevolveImagemUtilizadores"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' CARREGA A ARVORE DOS PROFILES

' ----------------------------------------------------------------------------------
Private Sub InicializaTreeProfiles()
    Dim sSql As String
    Dim rsProfiles As New ADODB.recordset
    On Error GoTo TrataErro
    TreeViewProfiles.LineStyle = tvwRootLines
    TreeViewProfiles.ImageList = ImageListProfiles
    
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles ORDER BY cod_profile"
    rsProfiles.CursorLocation = adUseServer
    rsProfiles.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfiles.Open sSql, gConexao
    If rsProfiles.RecordCount >= 1 Then
        totalEstrutProfile = 0
        ReDim estrutProfile(0)
        
        While Not rsProfiles.EOF
            totalEstrutProfile = totalEstrutProfile + 1
            ReDim Preserve estrutProfile(totalEstrutProfile)
            estrutProfile(totalEstrutProfile).Codigo = BL_HandleNull(rsProfiles!cod_profile, "")
            estrutProfile(totalEstrutProfile).codigo_pai = BL_HandleNull(rsProfiles!cod_pai, "0")
            estrutProfile(totalEstrutProfile).descricao = BL_HandleNull(rsProfiles!descr_profile, "")
            estrutProfile(totalEstrutProfile).ordem = BL_HandleNull(rsProfiles!ordem, "")
            
            If (estrutProfile(totalEstrutProfile).codigo_pai = "0") Then
              Dim root As Node
              Set root = TreeViewProfiles.Nodes.Add(, tvwChild, "KeyPR" & estrutProfile(totalEstrutProfile).Codigo, estrutProfile(totalEstrutProfile).descricao, "Key1")
              root.Expanded = True
            Else
              Set root = TreeViewProfiles.Nodes.Add(DevolvePaiProfile(totalEstrutProfile), tvwChild, "KeyPR" & estrutProfile(totalEstrutProfile).Codigo, estrutProfile(totalEstrutProfile).descricao, "Key1")
            End If
            rsProfiles.MoveNext
        Wend
    End If
    rsProfiles.Close

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InicializaTreeProfiles " & Err.Description, Me.Name, "InicializaTreeProfiles", True
    Exit Sub
    Resume Next
End Sub



' ----------------------------------------------------------------------------------

' DEVOLVE O INDICE DO NO PAI

' ----------------------------------------------------------------------------------
Private Function DevolvePaiProfile(indice As Long) As Long
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To indice
        If estrutProfile(i).Codigo = estrutProfile(indice).codigo_pai Then
            DevolvePaiProfile = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolvePaiProfile = -1
    BG_LogFile_Erros "Erro  DevolvePaiProfile " & Err.Description, Me.Name, "DevolvePaiProfile"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' DEVOLVE O INDICE DO NO PAI

' ----------------------------------------------------------------------------------
Private Function DevolveNomeProfile(Codigo As String) As String
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To totalEstrutProfile
        If estrutProfile(i).Codigo = Codigo Then
            DevolveNomeProfile = estrutProfile(i).descricao
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolveNomeProfile = ""
    BG_LogFile_Erros "Erro  DevolveNomeProfile " & Err.Description, Me.Name, "DevolveNomeProfile"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' DEVOLVE O CODIGO DO NO PAI

' ----------------------------------------------------------------------------------
Private Function DevolveCodigoProfileDadoId(id As String) As String
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To totalEstrutProfile
        If "KeyPR" & estrutProfile(i).Codigo = id Then
            DevolveCodigoProfileDadoId = estrutProfile(i).Codigo
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolveCodigoProfileDadoId = ""
    BG_LogFile_Erros "Erro  DevolveCodigoProfileDadoId " & Err.Description, Me.Name, "DevolveCodigoProfileDadoId"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' DESASOCIA UM NO DA ARVORE

' ----------------------------------------------------------------------------------
Private Sub DesasociaUtilizador(idU As String, idP As String)
    Dim i As Long
    Dim indiceU As Long
    On Error GoTo TrataErro
    indiceU = DevolveIndiceUtilizadoresDadoID(idU)
    
    If estrutUtilizadores(indiceU).tipo = KeyUtilizadores Then
        If estrutUtilizadores(indiceU).cod_profile <> "" And (estrutUtilizadores(indiceU).cod_profile = DevolveCodigoProfileDadoId(idP) Or idP = "") Then
            For i = indiceU To totalEstrutUtilizadores
                If estrutUtilizadores(i).cod_pai = estrutUtilizadores(indiceU).Codigo Then
                    TreeViewUtilizadores.Nodes.item(indiceU).Image = DevolveImagemUtilizadores(ImKeyUtilizadorSemProfile)
                    estrutUtilizadores(indiceU).cod_profile = ""
                    TreeViewUtilizadores.Nodes.Remove i
                    RemoveEstrutUtilizadores i
                    Exit Sub
                End If
            Next
        End If
    ElseIf estrutUtilizadores(indiceU).tipo = KeyGrupoUtilizadores Then
        For i = indiceU To totalEstrutUtilizadores
            If estrutUtilizadores(i).cod_pai = estrutUtilizadores(indiceU).Codigo And estrutUtilizadores(i).tipo = KeyUtilizadores Then
                DesasociaUtilizador estrutUtilizadores(i).id, idP
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  DesasociaUtilizador " & Err.Description, Me.Name, "DesasociaUtilizador"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' ASSOCIA UM NO DA ARVORE

' ----------------------------------------------------------------------------------
Private Sub AssociaUtilizador(idU As String, idP As String)
    Dim i As Long
    Dim indiceU As Long
    On Error GoTo TrataErro
    indiceU = DevolveIndiceUtilizadoresDadoID(idU)
    
    If estrutUtilizadores(indiceU).tipo = KeyUtilizadores Then
    
        ' SE UTILIZADOR J� TIVER PERFIL, RETIRA-O
        If estrutUtilizadores(indiceU).cod_profile <> "" Then
            DesasociaUtilizador idU, ""
        End If

        estrutUtilizadores(indiceU).cod_profile = DevolveCodigoProfileDadoId(idP)
        
        totalEstrutUtilizadores = totalEstrutUtilizadores + 1
        ReDim Preserve estrutUtilizadores(totalEstrutUtilizadores)
        
        estrutUtilizadores(totalEstrutUtilizadores).Codigo = estrutUtilizadores(indiceU).cod_profile
        estrutUtilizadores(totalEstrutUtilizadores).descricao = DevolveNomeProfile(estrutUtilizadores(indiceU).cod_profile)
        estrutUtilizadores(totalEstrutUtilizadores).tipo = KeyProfile
        estrutUtilizadores(totalEstrutUtilizadores).id = KeyProfile & estrutUtilizadores(indiceU).cod_profile
        estrutUtilizadores(totalEstrutUtilizadores).cod_profile = ""
        estrutUtilizadores(totalEstrutUtilizadores).cod_pai = estrutUtilizadores(indiceU).Codigo
        
        Dim root As Node
        TreeViewUtilizadores.Nodes.item(indiceU).Image = DevolveImagemUtilizadores(ImKeyUtilizadorComProfile)
        Set root = TreeViewUtilizadores.Nodes.Add(DevolveIndiceUtilizadoresDadoCodigo(KeyUtilizadores, estrutUtilizadores(totalEstrutUtilizadores).cod_pai), tvwChild, _
                   KeyUtilizadores & estrutUtilizadores(totalEstrutUtilizadores).cod_pai & "_" & estrutUtilizadores(totalEstrutUtilizadores).id, _
                   estrutUtilizadores(totalEstrutUtilizadores).descricao, DevolveImagemUtilizadores(ImKeyProfile))
        root.Expanded = False
    ElseIf estrutUtilizadores(indiceU).tipo = KeyGrupoUtilizadores Then
        For i = indiceU To totalEstrutUtilizadores
            If estrutUtilizadores(i).cod_pai = estrutUtilizadores(indiceU).Codigo And estrutUtilizadores(i).tipo = KeyUtilizadores Then
                Dim idUtilizador As String
                idUtilizador = estrutUtilizadores(i).id
                AssociaUtilizador idUtilizador, idP
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  associaUtilizador " & Err.Description, Me.Name, "associaUtilizador"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' REMOVE ITEM DA ESTRUTURA

' ----------------------------------------------------------------------------------
Private Sub RemoveEstrutUtilizadores(indice As Long)
    Dim i As Long
    On Error GoTo TrataErro
    For i = indice To totalEstrutUtilizadores - 1
        estrutUtilizadores(i).cod_pai = estrutUtilizadores(i + 1).cod_pai
        estrutUtilizadores(i).cod_profile = estrutUtilizadores(i + 1).cod_profile
        estrutUtilizadores(i).Codigo = estrutUtilizadores(i + 1).Codigo
        estrutUtilizadores(i).descricao = estrutUtilizadores(i + 1).descricao
        estrutUtilizadores(i).id = estrutUtilizadores(i + 1).id
        estrutUtilizadores(i).tipo = estrutUtilizadores(i + 1).tipo
    Next
    
    estrutUtilizadores(totalEstrutUtilizadores).cod_pai = ""
    estrutUtilizadores(totalEstrutUtilizadores).cod_profile = ""
    estrutUtilizadores(totalEstrutUtilizadores).Codigo = ""
    estrutUtilizadores(totalEstrutUtilizadores).descricao = ""
    estrutUtilizadores(totalEstrutUtilizadores).id = ""
    estrutUtilizadores(totalEstrutUtilizadores).tipo = ""
    totalEstrutUtilizadores = totalEstrutUtilizadores - 1
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RemoveEstrutUtilizadores " & Err.Description, Me.Name, "RemoveEstrutUtilizadores"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' GRAVA ASSOCIA��O DE PROFILES AOS UTILIZADORES

' ----------------------------------------------------------------------------------
Private Sub GravaAssocProfiles()
    Dim sSql As String
    Dim i As Long
    On Error GoTo TrataErro
    BG_BeginTransaction
    For i = 1 To totalEstrutUtilizadores
        If estrutUtilizadores(i).tipo = KeyUtilizadores Then
            sSql = "UPDATE " & gBD_PREFIXO_TAB & "idutilizador SET cod_profile = " & BL_HandleNull(estrutUtilizadores(i).cod_profile, "NULL")
            sSql = sSql & " WHERE cod_utilizador = " & estrutUtilizadores(i).Codigo
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  GravaAssocProfiles " & Err.Description, Me.Name, "GravaAssocProfiles"
    Exit Sub
    Resume Next
End Sub

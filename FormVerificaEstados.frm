VERSION 5.00
Begin VB.Form FormVerificaEstados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormVerificaEstados"
   ClientHeight    =   2025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4470
   Icon            =   "FormVerificaEstados.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2025
   ScaleWidth      =   4470
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Bt_inicia 
      Caption         =   "&Verificar estados das requisi��es"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   1560
      Width           =   4215
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      Height          =   735
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      TabStop         =   0   'False
      Text            =   "FormVerificaEstados.frx":000C
      Top             =   120
      Width           =   4215
   End
   Begin VB.TextBox EcDtInicio 
      Height          =   285
      Left            =   840
      TabIndex        =   2
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox EcDtFim 
      Height          =   285
      Left            =   2640
      TabIndex        =   4
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "De"
      Height          =   255
      Left            =   480
      TabIndex        =   1
      Top             =   1080
      Width           =   255
   End
   Begin VB.Label Label2 
      Caption         =   "at�"
      Height          =   255
      Left            =   2280
      TabIndex        =   3
      Top             =   1080
      Width           =   375
   End
End
Attribute VB_Name = "FormVerificaEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoDeFocus As Object
Dim CampoActivo As Object

Private Sub Bt_inicia_Click()
    
    Dim rs As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    If EcDtFim.Text = "" Or EcDtInicio.Text = "" Then
        BG_Mensagem mediMsgBox, "Deve indicar o intervalo de datas das requisi��es!", vbExclamation + vbOKOnly, App.ProductName
        Exit Sub
    End If
    
    BL_InicioProcessamento Me, "A processar... aguarde um momento, se faz favor."
    
    sql = "SELECT n_req FROM sl_requis WHERE estado_req not in( 'F', 'H') AND dt_cri BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
    Set rs = New ADODB.recordset
    With rs
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .ActiveConnection = gConexao
        .Open
    
        i = 0
        While Not .EOF
            i = i + 1
            Call BL_MudaEstadoReq(BL_HandleNull(!n_req, 0))
            .MoveNext
        Wend
            
        .Close
    End With
    
    Set rs = Nothing
    
    BL_FimProcessamento Me, "Processamento completo."

End Sub

Private Sub EcDtFim_GotFocus()
    
    Set CampoActivo = EcDtFim
    
    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDtInicio_GotFocus()
    
    Set CampoActivo = EcDtInicio
    
    If Trim(EcDtInicio.Text) = "" Then
        EcDtInicio.Text = Bg_DaData_ADO
    End If
    
End Sub

Private Sub EcDtInicio_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtInicio)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub Form_Activate()

    Call EventoActivate
    
End Sub

Public Function Funcao_DataActual()
       
    Select Case CampoActivo.Name
        Case "EcDtInicio", "EcDtFim"
            CampoActivo.Text = Bg_DaData_ADO
    End Select

End Function

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub

Public Sub EventoActivate()
    
    Set gFormActivo = Me
    
    If CampoDeFocus.Enabled = True Then
        CampoDeFocus.SetFocus
    End If

    BL_ToolbarEstadoN 0
    
    Set CampoActivo = EcDtInicio
    
    EcDtInicio.SetFocus
    
    Me.MousePointer = vbArrow
    
    MDIFormInicio.MousePointer = vbArrow
      
End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    
End Sub

Private Sub DefTipoCampos()

    'Tipo Data
    EcDtInicio.Tag = adDate
    EcDtFim.Tag = adDate
    
    'Tipo Inteiro
    EcDtInicio.MaxLength = 10
    EcDtFim.MaxLength = 10
    
End Sub

Public Sub FuncaoLimpar()
    
    EcDtInicio.SetFocus
    EcDtInicio.Text = ""
    EcDtFim.Text = ""
    
End Sub

Private Sub EventoUnload()
        
    BL_ToolbarEstadoN 0
    
    Set FormVerificaEstados = Nothing
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()

    Me.caption = " Verificar Estados de rRequisi��es"
    
    Me.Width = 4560
    Me.Height = 2400
    
    Call DefTipoCampos
    
    'Campo com Focus
    Set CampoDeFocus = EcDtInicio
      
End Sub


Attribute VB_Name = "BillingQRcode"
Option Explicit


'GX-62545
Private Const CRYPT_STRING_BASE64 As Long = &H1&
Private Declare Function CryptStringToBinaryW Lib "Crypt32.dll" ( _
    ByVal pszString As Long, _
    ByVal cchString As Long, _
    ByVal dwFlags As Long, _
    ByVal pbBinary As Long, _
    ByRef pcbBinary As Long, _
    ByVal pdwSkip As Long, _
    ByVal pdwFlags As Long) As Long

Public Function DecodeBase64(ByVal strData As String) As Byte()
    On Error GoTo TrataErro
    Dim Buffer() As Byte
    Dim dwBinaryBytes As Long
    dwBinaryBytes = LenB(strData)
    ReDim Buffer(dwBinaryBytes - 1) As Byte
    DoEvents
    If CryptStringToBinaryW(StrPtr(strData), LenB(strData), CRYPT_STRING_BASE64, _
        VarPtr(Buffer(0)), dwBinaryBytes, 0, 0) Then
        DoEvents
        ReDim Preserve Buffer(dwBinaryBytes - 1) As Byte
        DecodeBase64 = Buffer
    End If
    DoEvents
    Erase Buffer
    Exit Function
TrataErro:
    BG_LogFile_Erros "DecodeBase64:" & Err.Number & " - " & Err.Description, "DecodeBase64", "DecodeBase64", False
    Exit Function
    Resume Next
End Function

Public Function DecodeBase64_v2(ByVal B64 As String, ByVal path As String) As Byte()
    On Error GoTo TrataErro
    
    Dim PaddingX As Single
    Dim PaddingY As Single
    Dim F As Integer
    Dim ImageProcess As WIA.ImageProcess

'    PaddingX = Width - ScaleWidth
'    PaddingY = Height - ScaleHeight
        With New WIA.Vector
           .BinaryData = CryptoBase64.Decode(B64)
           B64 = vbNullString
           'Display it, size Form to fit:
'           Set Picture = .Picture
'           Width = PaddingX + ScaleX(.ImageFile.Width, vbPixels)
'           Height = PaddingY + ScaleY(.ImageFile.Height, vbPixels)
           'Convert to PNG format, save to disk:
           Set ImageProcess = New WIA.ImageProcess
           With ImageProcess
               .Filters.Add .FilterInfos("Convert").FilterID
               .Filters.Item(1).Properties("FormatID").value = wiaFormatPNG
           End With
           On Error Resume Next
           Kill path
           ImageProcess.Apply(.ImageFile).SaveFile path
        End With
    Exit Function
TrataErro:
    BG_LogFile_Erros "DecodeBase64_v2:" & Err.Number & " - " & Err.Description, "DecodeBase64_v2", "DecodeBase64_v2", False
    Exit Function
    Resume Next
End Function


Public Function BillQRCode_GetB64QRcodeString(ByVal i_string As String, ByRef o_msg As String) As String
    On Error GoTo TrataErro
    
    Dim CmdQr As New adodb.Command
    Dim PmtQrStr As adodb.Parameter
    Dim PmtRes As adodb.Parameter
    Dim PmtB64Str As adodb.Parameter
    
    Dim b64str As String
    Dim msg As String
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdQr
        .ActiveConnection = gConexao
        .CommandText = "pck_ws_qrcode.get_qrcode_sislab"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtQrStr = CmdQr.CreateParameter("i_string", adVarChar, adParamInput, 10000)
    Set PmtB64Str = CmdQr.CreateParameter("o_qr_str", adLongVarChar, adParamOutput, 32767)
    Set PmtRes = CmdQr.CreateParameter("o_msg", adLongVarChar, adParamOutput, 32767)
    
    
    CmdQr.Parameters.Append PmtQrStr
    CmdQr.Parameters.Append PmtB64Str
    CmdQr.Parameters.Append PmtRes
    
    CmdQr.Parameters("i_string") = i_string

    CmdQr.Execute
    
    b64str = CStr(BL_HandleNull(Trim(CmdQr.Parameters.Item("o_qr_str").value), ""))
    msg = BL_HandleNull(Trim(CmdQr.Parameters.Item("o_msg").value), "")
    
    o_msg = msg
    
    If msg = "" Then
        BillQRCode_GetB64QRcodeString = b64str
    Else
        BillQRCode_GetB64QRcodeString = "-1"
        BG_LogFile_Erros "Erro ao Gerar QR Code:" & msg, "BillingQRcode", "BillQRCode_GetB64QRcodeString", False
    End If
    
    Set CmdQr = Nothing
    Set PmtQrStr = Nothing
    Set PmtB64Str = Nothing
    Set PmtRes = Nothing
    

Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Gerar QR Code:" & Err.Number & " - " & Err.Description, "BillingQRcode", "BillQRCode_GetB64QRcodeString", True
    BillQRCode_GetB64QRcodeString = "-1"
    Exit Function
    Resume Next
End Function

Public Sub BillQRCode_insertQrBd(ByVal origemImagem As String, ByVal n_req As String)
    Dim ssql As String
    Dim rsGra As New adodb.recordset
    Dim sourcefile As Integer
    Dim ByteData() As Byte
    Dim FileLength As Long
    Dim NumBlocks As Integer
    Dim LeftOver As Long
    Const BlockSize = 100000
    Dim i As Integer

    On Error GoTo TrataErro
    
    ssql = "DELETE sl_cr_recibo_qr_code WHERE nome_computador = '" & gComputador & "'"
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    BG_ExecutaQuery_ADO ssql

    ssql = "INSERT INTO sl_cr_recibo_qr_code (n_req,n_sessao,nome_computador) "
    ssql = ssql & " VALUES ('" & n_req & "'," & gNumeroSessao & ",'" & gComputador & "')"
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    BG_ExecutaQuery_ADO ssql
    
   
    ssql = "SELECT * FROM  sl_cr_recibo_qr_code  WHERE n_req  = '" & n_req & "'  AND n_sessao = '" & gNumeroSessao & "'"
    Set rsGra = New adodb.recordset
    rsGra.CursorType = adOpenKeyset
    rsGra.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsGra.Open ssql, gConexao

    If rsGra.RecordCount > 0 Then

        rsGra.MoveFirst

            sourcefile = FreeFile
            Open origemImagem For Binary As sourcefile
            FileLength = LOF(sourcefile)  ' Get the length of the file.
            'Debug.Print "Filelength is " & FileLength
            If FileLength = 0 Then
                Call BG_LogFile_Erros(Now & " -> Ficheiro QR CODE com tamanho 0 : " & origemImagem)
                Close sourcefile
                'Kill origemficheiro
                Exit Sub
            Else
                'If gModoDebug = mediSim Then BG_LogFile_Erros "FileLength > 0"
                
                NumBlocks = FileLength / BlockSize
                LeftOver = FileLength Mod BlockSize
                
                'If gModoDebug = mediSim Then BG_LogFile_Erros "LeftOver: " & CStr(LeftOver)
                
                If LeftOver > 0 Then
                    ReDim ByteData(LeftOver - 1)
                    Get sourcefile, , ByteData()
                    rsGra.Fields("grafico_raw").AppendChunk ByteData()
                End If
                ReDim ByteData(BlockSize - 1)

                For i = 1 To NumBlocks
                    DoEvents
                    Get sourcefile, , ByteData()

                    rsGra.Fields("grafico_raw").AppendChunk ByteData()

                Next i
                
                'If gModoDebug = mediSim Then BG_LogFile_Erros "BEFORE rsGra.Update"
                
                rsGra.Update   'Commit the new data.
                
                'If gModoDebug = mediSim Then BG_LogFile_Erros "AFTER rsGra.Update"
            End If
            DoEvents
            'If gModoDebug = mediSim Then BG_LogFile_Erros "BEFORE sourcefile"
            
            Close sourcefile
            
'            If gModoDebug = mediSim Then BG_LogFile_Erros "AFTER sourcefile"
'            If gModoDebug = mediSim Then BG_LogFile_Erros "BEFORE DeleteFileFromFileSystem"
            
            DeleteFileFromFileSystem (origemImagem)
            
            'If gModoDebug = mediSim Then BG_LogFile_Erros "AFTER DeleteFileFromFileSystem"
            DoEvents
    End If
    rsGra.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "BillQRCode_insertQrBd: " & Err.Description
    Exit Sub
    Resume Next
End Sub

Private Sub DeleteFileFromFileSystem(ByVal path As String)
    If Dir(path) <> "" Then
        Kill path
    End If
End Sub

'GX-62545
Public Function ManageBillingQRCode(ByVal strQrCode As String, ByVal n_req As String)

    Dim byteArray() As Byte
    Dim fnum As Integer
    Dim continua As Boolean
    Dim path As String
    Dim ret As Boolean
    ret = False
    path = gDirCliente & "\bin\" & "qr_code.png"
    
    If gModoDebug = mediSim Then BG_LogFile_Erros "Before DeleteFileFromFileSystem path"
    
    DeleteFileFromFileSystem (path)
    
    If gModoDebug = mediSim Then BG_LogFile_Erros "After DeleteFileFromFileSystem path"
    
    If strQrCode <> "" Then
    
        If gModoDebug = mediSim Then BG_LogFile_Erros "strQrCode: " & strQrCode
     
        Dim b64str As String
        Dim qrMsgOut As String
        
        If gModoDebug = mediSim Then BG_LogFile_Erros "Before BillQRCode_GetB64QRcodeString "
        
        b64str = BillQRCode_GetB64QRcodeString(strQrCode, qrMsgOut)
        If gModoDebug = mediSim Then BG_LogFile_Erros "AFTER  BillQRCode_GetB64QRcodeString "
        
        If b64str <> "-1" Then
            ret = True
            
            If gModoDebug = mediSim Then BG_LogFile_Erros "BEFORE  BillingQRcode.DecodeBase64_v2 "
            Call DecodeBase64_v2(b64str, path)
            If gModoDebug = mediSim Then BG_LogFile_Erros "AFTER  BillingQRcode.DecodeBase64_v2 "
            
'            If gModoDebug = mediSim Then BG_LogFile_Erros "BEFORE  BillingQRcode.DecodeBase64 "
'            byteArray = BillingQRcode.DecodeBase64(b64str)
'             If gModoDebug = mediSim Then BG_LogFile_Erros "AFTER  BillingQRcode.DecodeBase64 "
'            fnum = FreeFile
'            Open path For Binary As #fnum
'            Put #fnum, 1, byteArray
'            Close fnum
            
            'If gModoDebug = mediSim Then BG_LogFile_Erros "BEFORE  BillingQRcode.BillQRCode_insertQrBd "
            Call BillingQRcode.BillQRCode_insertQrBd(path, n_req)
            
            'If gModoDebug = mediSim Then BG_LogFile_Erros "AFTER  BillingQRcode.BillQRCode_insertQrBd "
        Else
            BG_Mensagem mediMsgBox, "Erro ao obter QR CODE: " & qrMsgOut, vbOKOnly + vbError, "ManageBillingQRCode"
            BG_LogFile_Erros "ManageBillingQRCode (ManageBillingQRCode) " & qrMsgOut, "ManageBillingQRCode", "ManageBillingQRCode"
        End If
    End If
    ManageBillingQRCode = ret
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Gerar QR Code:" & Err.Number & " - " & Err.Description, "ManageBillingQRCode", "ManageBillingQRCode", False
    ManageBillingQRCode = ret
    Exit Function
    Resume Next
End Function


Public Function BillQRCode_GenerateQRcodeString(ByVal i_serie_rec As String, ByVal i_n_rec As String) As String
    On Error GoTo TrataErro
    
    Dim CmdQr As New adodb.Command
    Dim PmtSerieRec As adodb.Parameter
    Dim PmtNRec As adodb.Parameter
    Dim PmtRes As adodb.Parameter

    Dim stringQRCode As String
    
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdQr
        .ActiveConnection = gConexao
        .CommandText = "pck_ws_qrcode.qrcode_gen"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtSerieRec = CmdQr.CreateParameter("p_serie_rec", adVarChar, adParamInput, 10000)
    Set PmtNRec = CmdQr.CreateParameter("p_n_rec", adVarChar, adParamInput, 10000)
    Set PmtRes = CmdQr.CreateParameter("o_msg_qrcode", adLongVarChar, adParamOutput, 32767)
    
    
    CmdQr.Parameters.Append PmtSerieRec
    CmdQr.Parameters.Append PmtNRec
    CmdQr.Parameters.Append PmtRes
    
    CmdQr.Parameters("p_serie_rec") = i_serie_rec
    CmdQr.Parameters("p_n_rec") = i_n_rec

    CmdQr.Execute
    
    stringQRCode = CStr(BL_HandleNull(Trim(CmdQr.Parameters.Item("o_msg_qrcode").value), ""))
    
    If gModoDebug = mediSim Then BG_LogFile_Erros "BillQRCode_GenerateQRcodeString QR CODE STRING -> " & stringQRCode
    
    BillQRCode_GenerateQRcodeString = stringQRCode
    
    If stringQRCode = "-1" Then
        BillQRCode_GenerateQRcodeString = "-1"
        BG_LogFile_Erros "Erro ao Gerar String QR Code!", "BillingQRcode", "BillQRCode_GenerateQRcodeString", True
    End If
    
    Set CmdQr = Nothing
    Set PmtSerieRec = Nothing
    Set PmtNRec = Nothing
    Set PmtRes = Nothing
    

Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Gerar QR Code:" & Err.Number & " - " & Err.Description, "BillingQRcode", "BillQRCode_GenerateQRcodeString", True
    BillQRCode_GenerateQRcodeString = stringQRCode
    Exit Function
    Resume Next
End Function

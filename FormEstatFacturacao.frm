VERSION 5.00
Begin VB.Form FormEstatFacturacao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5880
   Icon            =   "FormEstatFacturacao.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   5880
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox Flg_DescrAna 
      Caption         =   "Indicar An�lises"
      Height          =   255
      Left            =   3840
      TabIndex        =   16
      Top             =   4560
      Value           =   1  'Checked
      Width           =   3735
   End
   Begin VB.TextBox EcCodEFR 
      Height          =   315
      Left            =   1440
      TabIndex        =   13
      Top             =   6000
      Width           =   735
   End
   Begin VB.TextBox EcDescrEFR 
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   6000
      Width           =   3735
   End
   Begin VB.CommandButton BtPesquisaEntFin 
      Height          =   315
      Left            =   5880
      Picture         =   "FormEstatFacturacao.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
      Top             =   6000
      Width           =   375
   End
   Begin VB.TextBox EcCodProveniencia 
      Height          =   315
      Left            =   1440
      TabIndex        =   10
      Top             =   5520
      Width           =   735
   End
   Begin VB.TextBox EcDescrProveniencia 
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   5520
      Width           =   3735
   End
   Begin VB.CommandButton BtPesquisaProveniencia 
      Height          =   315
      Left            =   5880
      Picture         =   "FormEstatFacturacao.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   5520
      Width           =   375
   End
   Begin VB.CheckBox Flg_PesoEstat 
      Caption         =   "Peso Estat�stico"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   4920
      Width           =   2655
   End
   Begin VB.CheckBox Flg_DetAna 
      Caption         =   "Detalhe das An�lises"
      Height          =   195
      Left            =   240
      TabIndex        =   6
      Top             =   4560
      Width           =   2175
   End
   Begin VB.Frame Frame3 
      Height          =   975
      Left            =   120
      TabIndex        =   3
      Top             =   40
      Width           =   3615
      Begin VB.ComboBox CbMes 
         Height          =   315
         Left            =   2040
         TabIndex        =   18
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox EcAno 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   720
         TabIndex        =   17
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "M�s"
         Height          =   255
         Left            =   1560
         TabIndex        =   5
         Top             =   400
         Width           =   375
      End
      Begin VB.Label Label3 
         Caption         =   "Ano"
         Height          =   255
         Left            =   250
         TabIndex        =   4
         Top             =   400
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   1335
      Left            =   240
      TabIndex        =   0
      Top             =   1920
      Width           =   5295
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   480
         Width           =   735
      End
   End
   Begin VB.Label Label10 
      Caption         =   "E.&F.R."
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   6000
      Width           =   495
   End
   Begin VB.Label Label9 
      Caption         =   "&Proveni�ncia"
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   5520
      Width           =   975
   End
End
Attribute VB_Name = "FormEstatFacturacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : /02/2003
' T�cnico Paulo Costa

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim NRegistos As Long
Dim NAnaSMarc As Long
Dim NAnaCMarc As Long
Dim NAnaSReal As Long
Dim NAnaCReal As Long
Dim NAnaSReq As Long
Dim NAnaCReq As Long
Dim NAnaTabela As Long

Dim DataIni As String
Dim DataFim As String

Dim n_fact_ini As String
Dim n_fact_fim As String

Public rs As ADODB.recordset

Sub Constroi_Estatistica_Facturacao()
    
    Dim sql As String
    Dim continua As Boolean
    Dim rsAux As ADODB.recordset
        
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Servi�o Requisitante") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcAno.text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique o Ano.", vbOKOnly + vbExclamation, App.ProductName)
        EcAno.SetFocus
        Exit Sub
    End If
'    If EcDtFim.Text = "" Then
'        Call BG_Mensagem(mediMsgBox, "Indique o M�s.", vbOKOnly + vbExclamation, App.ProductName)
'        EcDtFim.SetFocus
'        Exit Sub
'    End If
      
    ' Um report por sistema de factura��o.
    Select Case gSISTEMA_FACTURACAO
    
        Case cFACTURACAO_FACTUS
            'Printer Common Dialog
                If gImprimirDestino = 1 Then
                    continua = BL_IniciaReport("EstatisticaServico_FACTUS", "Estat�stica por Servi�o Requisitante", crptToPrinter)
                Else
                    continua = BL_IniciaReport("EstatisticaServico_FACTUS", "Estat�stica por Servi�o Requisitante", crptToWindow)
                End If
                If continua = False Then Exit Sub
        Case Else
            'Printer Common Dialog
                If gImprimirDestino = 1 Then
                    continua = BL_IniciaReport("EstatisticaServico", "Estat�stica por Servi�o Requisitante", crptToPrinter)
                Else
                    continua = BL_IniciaReport("EstatisticaServico", "Estat�stica por Servi�o Requisitante", crptToWindow)
                End If
                If continua = False Then Exit Sub
    End Select
    
    BL_MudaCursorRato mediMP_Espera, Me
    
    ' Cria as tabelas tempor�rias.
    
    Call Cria_Tmp_SL_CR_ESTAT_FACTURACAO
    
    ' -------------------------------------------------------------------------
    
    ' Preenche as tabelas temporarias.
    
    Dim mes As String
    
    DataIni = "01-01-2003"
    DataFim = "01-02-2003"
    
    mes = Me.CbMes.ItemData(Me.CbMes.ListIndex)
    
    Call Preenche_Tabelas_Temporarias(DataIni, DataFim)
    
    ' --------------------------------------------------------------------
    
    ' Report Crystal.
    
    ' --------------------------------------------------------------------

    ' Elimina as tabelas tempor�rias.
    
    Set rsAux = New ADODB.recordset
    rsAux.CursorLocation = adUseClient
    rsAux.CursorType = adOpenForwardOnly
    rsAux.LockType = adLockReadOnly
    rsAux.ActiveConnection = gConexao
        
    gSQLError = 0
    gSQLISAM = 0
    sql = "DROP TABLE " & "SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao
    rsAux.Open sql
    BG_Trata_BDErro
        
    Set rsAux = Nothing
                
    ' --------------------------------------------------------------------
End Sub

Sub Preenche_Tabelas_Temporarias(dt_ini As String, _
                                 dt_fim As String)
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rsAux As ADODB.recordset
    Dim rv As Integer
    
    dt_ini = Trim(dt_ini)
    dt_fim = Trim(dt_fim)
    
    ' Constroi a estat�stica (tabela auxiliar 3).
    rv = Processa_Tudo(dt_ini, dt_fim)
    
    ' PROVIS�RIO : Mostra o relat�rio num ficheiro de texto.
    rv = Mostra_Relatorio_TXT()

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Preenche_Tabelas_Temporarias (FormEstatFACTURACAO) -> " & Err.Number & " : " & Err.Description)
            Set rsAux = Nothing
            Exit Sub
    End Select
End Sub

Sub Cria_Tmp_SL_CR_ESTAT_FACTURACAO()

    Dim TmpRec(8) As DefTable
    
    
    TmpRec(1).NomeCampo = "COD_PROVEN"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 10
    
    TmpRec(2).NomeCampo = "PROVEN"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 50
    
    TmpRec(3).NomeCampo = "DESCRICAO"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 50
    
    TmpRec(4).NomeCampo = "COD_EFR"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 10
    
    TmpRec(5).NomeCampo = "EFR"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 100
    
    TmpRec(6).NomeCampo = "COD_ANALISE"
    TmpRec(6).tipo = "STRING"
    TmpRec(6).tamanho = 10
    
    TmpRec(7).NomeCampo = "VAL_EFR"
    TmpRec(7).tipo = "DOUBLE"
    
    TmpRec(8).NomeCampo = "CONTA"
    TmpRec(8).tipo = "DOUBLE"
    
    If (gSGBD <> gSqlServer) Then
        
        Call BL_CriaTabela("SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao, TmpRec)
    
    Else
        
        'On Error Resume Next
        Dim sql As String
        
        sql = "CREATE TABLE SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & " " & vbCrLf & _
              "( " & vbCrLf & _
              "     cod_proven  VARCHAR(10), " & vbCrLf & _
              "     proven      VARCHAR(50), " & vbCrLf & _
              "     descricao   VARCHAR(50), " & vbCrLf & _
              "     cod_efr     VARCHAR(10), " & vbCrLf & _
              "     efr         VARCHAR(100), " & vbCrLf & _
              "     cod_analise VARCHAR(10), " & vbCrLf & _
              "     val_efr     NUMERIC(9,5), " & vbCrLf & _
              "     conta       DECIMAL " & vbCrLf & _
              ")"
    
        Call BG_ExecutaQuery_ADO(sql, -1)
    
    End If

End Sub

Private Sub BtPesquisaEntFin_Click()
    
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub BtPesquisaProveniencia_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.text = Resultados(1)
            EcDescrProveniencia.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcAno_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)

End Sub

Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_Validate(Cancel As Boolean)
    
    Dim RsDescrEFR As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodEFR.text) <> "" Then
        Set RsDescrEFR = New ADODB.recordset
        
        With RsDescrEFR
            .Source = "SELECT descr_efr FROM sl_efr WHERE cod_efr= " & BL_TrataStringParaBD(EcCodEFR.text)
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrEFR.RecordCount > 0 Then
            EcDescrEFR.text = "" & RsDescrEFR!descr_efr
        Else
            Cancel = True
            EcDescrEFR.text = ""
            BG_Mensagem mediMsgBox, "Entidade Financeira inexistente!", vbOKOnly + vbExclamation, App.ProductName
            SendKeys ("{HOME}+{END}")
        End If
        RsDescrEFR.Close
        Set RsDescrEFR = Nothing
    Else
        EcDescrEFR.text = ""
    End If

End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.text)
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            SendKeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.text = ""
    End If
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
      
End Sub

Sub Inicializacoes()

    Dim mes As Integer
    
    Me.caption = " Estat�sticas da Factura��o "
    Me.Left = 540
    Me.Top = 450
    Me.Width = 3945
    Me.Height = 1530 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodProveniencia
      
    ' Ano.
    Me.EcAno = CInt(Year(Date))
    
    ' Combo de meses.
    CbMes.AddItem "Janeiro"
    CbMes.ItemData(0) = 1
    CbMes.AddItem "Fevereiro"
    CbMes.ItemData(1) = 2
    CbMes.AddItem "Mar�o"
    CbMes.ItemData(2) = 3
    CbMes.AddItem "Abril"
    CbMes.ItemData(3) = 4
    CbMes.AddItem "Maio"
    CbMes.ItemData(4) = 5
    CbMes.AddItem "Junho"
    CbMes.ItemData(5) = 6
    CbMes.AddItem "Julho"
    CbMes.ItemData(6) = 7
    CbMes.AddItem "Agosto"
    CbMes.ItemData(7) = 8
    CbMes.AddItem "Setembro"
    CbMes.ItemData(8) = 9
    CbMes.AddItem "Outubro"
    CbMes.ItemData(9) = 10
    CbMes.AddItem "Novembro"
    CbMes.ItemData(10) = 11
    CbMes.AddItem "Dezembro"
    CbMes.ItemData(11) = 12
      
    mes = CInt(Month(Date))
    mes = mes - 1
    If (mes = 0) Then
        mes = 12
    End If
    
    CbMes.ListIndex = mes - 1

End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica por Servi�o Requisitante")
    
    Set FormEstatFacturacao = Nothing
    
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    CbSituacao.ListIndex = mediComboValorNull
    CbMes.ListIndex = mediComboValorNull
    EcCodProveniencia.text = ""
    EcDescrProveniencia.text = ""
    EcCodEFR.text = ""
    EcDescrEFR.text = ""
    Me.EcAno.text = ""
    Flg_DescrAna.Value = 0
    Flg_DetAna.Value = 0
    Flg_PesoEstat.Value = 0
    
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"
    EcCodEFR.Tag = "200"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodProveniencia.MaxLength = 5
    EcCodEFR.MaxLength = 9
'    EcNumFolhaTrab.MaxLength = 10
    
    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao

End Sub

Sub FuncaoProcurar()
       
End Sub

Sub FuncaoImprimir()

    Me.MousePointer = vbHourglass
    MDIFormInicio.MousePointer = vbHourglass
    Call Constroi_Estatistica_Facturacao
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub ImprimirVerAntes()
    
    Call Constroi_Estatistica_Facturacao

End Sub

Function Processa_Tudo(data_ini As String, _
                       data_fim As String) As Integer

    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim sql1 As String
    Dim Sql2 As String
    Dim Sql3 As String
    Dim Sql4 As String
    
    Dim ano As String
    Dim mes As String
    Dim serie_fac As String
    Dim rsAux As ADODB.recordset
    Dim rv As Integer
            
    Set rsAux = New ADODB.recordset
    
    rsAux.CursorLocation = adUseClient
    rsAux.CursorType = adOpenForwardOnly
    rsAux.LockType = adLockReadOnly
    rsAux.ActiveConnection = gConexao
            
    ' --------------------------------------------------------
    
    ano = EcAno.text
    ano = Trim(ano)
    mes = CbMes.ItemData(CbMes.ListIndex)
    
    If (Not (IsNumeric(ano))) Then
        ano = CStr(Year(Date))
    Else
        If (CLng(ano) > Year(Date)) Then
            ano = CStr(Year(Date))
        End If
    End If

    EcAno.text = ano
    DoEvents

    ' Dtermina��o do intervalo de facturas para este m�s.
    
    rv = Obtem_Intervalo_Facturas(ano, _
                                  mes, _
                                  n_fact_ini, _
                                  n_fact_fim)
    
    Select Case rv
        Case 1
            ' OK
        Case 2
            ' Erro.
            MsgBox " A factura��o para este m�s n�o est� conclu�da.      ", vbExclamation, " Aten��o "
        Case Else
            ' Erro.
            MsgBox " N�o � poss�vel efectuar a estat�stica.   ", vbExclamation, " Erro "
            Processa_Tudo = -2
            Exit Function
    End Select
    
    serie_fac = ano
    
    ' --------------------------------------------------------
    ' Efectua a estat�stica.
            
    ' Tabela Tempor�ria 3.

    ' Onde se desconte a TAXA.
    
    sql1 = "INSERT INTO SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & " " & vbCrLf & _
           "(  " & vbCrLf & _
           "     cod_proven, cod_efr, cod_analise, val_efr , conta  " & vbCrLf & _
           ")  " & vbCrLf

    Sql2 = "SELECT  " & vbCrLf & _
           "     cod_proven,  " & vbCrLf & _
           "     cod_efr,  " & vbCrLf & _
           "     seq_facturacao,  " & vbCrLf & _
           "     SUM(val_total - val_taxa) AS total,  " & vbCrLf & _
           "     COUNT(*)        AS conta  " & vbCrLf & _
           "FROM  " & vbCrLf & _
           "     sl_facturados_FACTUS  " & vbCrLf & _
           "WHERE  " & vbCrLf & _
           "     cod_efr <> 55 AND  " & vbCrLf & _
           "     serie_fac = '" & serie_fac & "' AND  " & vbCrLf & _
           "     n_fac >= " & n_fact_ini & " AND  " & vbCrLf & _
           "     n_fac <= " & n_fact_fim & " AND " & vbCrLf & _
           "     NOT( val_taxa IS NULL ) " & vbCrLf & _
           "GROUP BY  " & vbCrLf & _
           "     cod_proven,  " & vbCrLf & _
           "     cod_efr,  " & vbCrLf & _
           "     seq_facturacao  " & vbCrLf & _
           "HAVING  " & vbCrLf & _
           "     ((SUM(val_total))<>0)  " & vbCrLf

    ' Onde n�o se desconte a TAXA.
    
    Sql3 = "UNION " & vbCrLf & _
           "SELECT  " & vbCrLf & _
           "     cod_proven,  " & vbCrLf & _
           "     cod_efr,  " & vbCrLf & _
           "     seq_facturacao,  " & vbCrLf & _
           "     SUM(val_total)  AS total,  " & vbCrLf & _
           "     COUNT(*)        AS conta  " & vbCrLf & _
           "FROM  " & vbCrLf & _
           "     sl_facturados_FACTUS  " & vbCrLf & _
           "WHERE  " & vbCrLf & _
           "     cod_efr <> 55 AND  " & vbCrLf & _
           "     serie_fac = '" & serie_fac & "' AND  " & vbCrLf & _
           "     n_fac >= " & n_fact_ini & " AND  " & vbCrLf & _
           "     n_fac <= " & n_fact_fim & " AND " & vbCrLf & _
           "     val_taxa IS NULL " & vbCrLf & _
           "GROUP BY  " & vbCrLf & _
           "     cod_proven,  " & vbCrLf & _
           "     cod_efr,  " & vbCrLf & _
           "     seq_facturacao  " & vbCrLf & _
           "HAVING  " & vbCrLf & _
           "     ((SUM(val_total))<>0) "
          
    ' Mart. PSP
    
    Sql4 = "UNION " & vbCrLf & _
           "SELECT  " & vbCrLf & _
           "     cod_proven,  " & vbCrLf & _
           "     cod_efr,  " & vbCrLf & _
           "     seq_facturacao,  " & vbCrLf & _
           "     SUM(val_total * 0.75 )  AS total,  " & vbCrLf & _
           "     COUNT(*)        AS conta  " & vbCrLf & _
           "FROM  " & vbCrLf & _
           "     sl_facturados_FACTUS  " & vbCrLf & _
           "WHERE  " & vbCrLf & _
           "     cod_efr = 55 AND  " & vbCrLf & _
           "     serie_fac = '" & serie_fac & "' AND  " & vbCrLf & _
           "     n_fac >= " & n_fact_ini & " AND  " & vbCrLf & _
           "     n_fac <= " & n_fact_fim & " AND " & vbCrLf & _
           "     val_taxa IS NULL " & vbCrLf & _
           "GROUP BY  " & vbCrLf & _
           "     cod_proven,  " & vbCrLf & _
           "     cod_efr,  " & vbCrLf & _
           "     seq_facturacao  " & vbCrLf & _
           "HAVING  " & vbCrLf & _
           "     ((SUM(val_total))<>0) "
          
    gSQLError = 0
    gSQLISAM = 0
    Err.Clear
    
    rsAux.Open sql1 & Sql2 & Sql3 & Sql4
    
    BG_Trata_BDErro
    
    ' --------------------------------------------------------
    
    If (rv <> "1") Then
        n_fact_fim = ""
    End If
    
    ' --------------------------------------------------------
    
    ' Descri��o proveni�ncia.
            
    sql = "UPDATE SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & " " & vbCrLf & _
          "SET  " & vbCrLf & _
          "     SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & ".proven =  " & vbCrLf & _
          "     (SELECT  " & vbCrLf & _
          "             descr_proven  " & vbCrLf & _
          "      FROM  " & vbCrLf & _
          "             sl_proven  " & vbCrLf & _
          "      WHERE  " & vbCrLf & _
          "             cod_proven = SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & ".cod_proven) "

    gSQLError = 0
    gSQLISAM = 0
    Err.Clear
    rsAux.Open sql
    BG_Trata_BDErro
    
    ' --------------------------------------------------------
    
    ' Descri��o da EFR.
            
    sql = "UPDATE SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & " " & vbCrLf & _
          "SET  " & vbCrLf & _
          "     SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & ".efr =  " & vbCrLf & _
          "     (SELECT  " & vbCrLf & _
          "             descr_efr  " & vbCrLf & _
          "      FROM  " & vbCrLf & _
          "             sl_efr  " & vbCrLf & _
          "      WHERE  " & vbCrLf & _
          "             cod_efr = SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & ".cod_efr) "

    gSQLError = 0
    gSQLISAM = 0
    Err.Clear
    rsAux.Open sql
    BG_Trata_BDErro
    
    ' --------------------------------------------------------

    ' Descri��o das an�lises.
    
    sql = "UPDATE " & vbCrLf & _
          "     SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & "  " & vbCrLf & _
          "SET  " & vbCrLf & _
          "     descricao =   " & vbCrLf & _
          "(  " & vbCrLf & _
          "    SELECT   " & vbCrLf & _
          "         descr_ana " & vbCrLf & _
          "     FROM  " & vbCrLf & _
          "         sl_ana_facturacao " & vbCrLf & _
          "     WHERE  " & vbCrLf & _
          "         UPPER(seq_ana) = SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & ".cod_analise " & vbCrLf & _
          ") "
    
    gSQLError = 0
    gSQLISAM = 0
    Err.Clear
    rsAux.Open sql
    BG_Trata_BDErro
    
    ' C�digo das an�lises.
    
    sql = "UPDATE " & vbCrLf & _
          "     SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & " " & vbCrLf & _
          "SET  " & vbCrLf & _
          "     cod_analise =   " & vbCrLf & _
          "(  " & vbCrLf & _
          "       SELECT   " & vbCrLf & _
          "         UPPER(cod_ana) " & vbCrLf & _
          "       FROM  " & vbCrLf & _
          "         sl_ana_facturacao " & vbCrLf & _
          "       WHERE  " & vbCrLf & _
          "         seq_ana = SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & ".cod_analise " & vbCrLf & _
          ")  "
    
    gSQLError = 0
    gSQLISAM = 0
    Err.Clear
    rsAux.Open sql
    BG_Trata_BDErro
    
    ' --------------------------------------------------------
    
    Set rsAux = Nothing
    
    Processa_Tudo = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rsAux = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : Processa_Tudo (FormEstatFACTURACAO) -> " & Err.Number & ", " & Err.Description)
            Processa_Tudo = -1
            Exit Function
    End Select
End Function

Function Mostra_Relatorio_TXT() As Integer

    On Error GoTo ErrorHandler

    Dim sql As String
    Dim rsAux As ADODB.recordset
    
    Dim f As Integer
    
    Dim str_proven As String
    Dim str_proven_act As String
    
    Dim str_efr As String
    Dim str_efr_act As String
    
    Dim str_fich As String
    Dim total_count As Double
    Dim total_proven As Double
    Dim total_efr As Double
    
    Dim grande_total_count As Double
    Dim grande_total_proven As Double
    Dim grande_total_efr As Double
    
    Dim AuxP As String
    Dim AuxC As String
    Dim AuxS As String
            
    Set rsAux = New ADODB.recordset
    
    rsAux.CursorLocation = adUseClient
    rsAux.CursorType = adOpenForwardOnly
    rsAux.LockType = adLockReadOnly
    rsAux.ActiveConnection = gConexao
            
    ' --------------------------------------------------------
            
    sql = "SELECT " & vbCrLf & _
          "     proven, " & vbCrLf & _
          "     cod_efr, " & vbCrLf & _
          "     efr, " & vbCrLf & _
          "     cod_analise, " & vbCrLf & _
          "     descricao, " & vbCrLf & _
          "     val_efr, " & vbCrLf & _
          "     conta " & vbCrLf & _
          "FROM " & vbCrLf & _
          "     SL_CR_ESTAT_FACTURACAO_" & gNumeroSessao & " " & vbCrLf & _
          "ORDER BY " & vbCrLf & _
          "     proven, " & vbCrLf & _
          "     efr, " & vbCrLf & _
          "     descricao "
                
    gSQLError = 0
    gSQLISAM = 0
    Err.Clear
    rsAux.Open sql
    BG_Trata_BDErro
            
    If Not (rsAux.EOF) Then
                
        str_fich = ""
        str_proven = ""
        str_efr = ""
                
        total_count = 0
        total_proven = 0
                
        grande_total_count = 0
        grande_total_proven = 0
                
        str_fich = str_fich & "----------------------------------------------------------------------------" & vbCrLf
        str_fich = str_fich & vbCrLf
        str_fich = str_fich & "                      ESTAT�STICA POR PROVENI�NCIA" & vbCrLf
        str_fich = str_fich & vbCrLf
        str_fich = str_fich & "                      " & Me.CbMes.List(Me.CbMes.ListIndex) & " de " & Trim(Me.EcAno.text) & vbCrLf
        str_fich = str_fich & vbCrLf
        str_fich = str_fich & "                      Da factura " & Trim(n_fact_ini) & " � " & Trim(n_fact_fim) & "   " & vbCrLf
        str_fich = str_fich & vbCrLf
        str_fich = str_fich & "----------------------------------------------------------------------------" & vbCrLf
        str_fich = str_fich & " Data : " & Format(Date, gFormatoData) & vbCrLf
        str_fich = str_fich & " Hora : " & time & vbCrLf
        str_fich = str_fich & "----------------------------------------------------------------------------" & vbCrLf
        str_fich = str_fich & vbCrLf
                
        str_fich = str_fich & "            An�lise                                Cont.  Fact. EFR" & vbCrLf
                
        While Not (rsAux.EOF())
                    
            str_proven_act = Trim(BL_HandleNull(rsAux!Proven, ""))
            str_efr_act = Trim(BL_HandleNull(rsAux!efr, ""))
                    
            ' Proveni�ncia.
            
            If (StrComp(str_proven, str_proven_act) <> 0) Then
                        
                If (total_count > 0) Then
                    str_fich = str_fich & vbCrLf & _
                               " " & Left(str_proven & "                                                                   ", 35) & "   TOTAL   " & _
                               Right("                  " & total_count, 8) & "  " & _
                               Right("                                         " & Format(total_proven, "###0.00"), 10) & " �"
                               str_fich = str_fich & vbCrLf
                End If
                        
                    str_fich = str_fich & vbCrLf
                    str_fich = str_fich & "----------------------------------------------------------------------------" & vbCrLf
                    str_fich = str_fich & " " & str_proven_act & vbCrLf
                    str_fich = str_fich & "----------------------------------------------------------------------------" & vbCrLf
                        
                    str_proven = str_proven_act
                    total_count = 0
                    total_proven = 0
                    total_efr = 0
                        
            End If
                    
            ' EFR.
            
            If (StrComp(str_efr, str_efr_act) <> 0) Then
                        
                    str_fich = str_fich & vbCrLf
'                    str_fich = str_fich & " ------------------------------------------------------------------"
                    str_fich = str_fich & " " & str_efr_act
'                    str_fich = str_fich & " ------------------------------------------------------------------"
                    str_fich = str_fich & vbCrLf & vbCrLf
                        
                    str_efr = str_efr_act
                        
            End If

                str_fich = str_fich & " " & Left(BL_HandleNull(rsAux!descricao, "") & "                                                       ", 38) & " "
                               
                str_fich = str_fich & Left(BL_HandleNull(rsAux!cod_analise, "") & "                    ", 7)
                 
                str_fich = str_fich & Right("                  " & BL_HandleNull(rsAux!conta, ""), 8) & _
                           Right("                                         " & Format(BL_HandleNull(rsAux!val_efr, ""), "###0.00"), 12) & vbCrLf
                    
                grande_total_count = grande_total_count + CDbl(BL_HandleNull(rsAux!conta, "0"))
                grande_total_proven = grande_total_proven + CDbl(BL_HandleNull(rsAux!val_efr, "0"))
                grande_total_efr = grande_total_efr + CDbl(BL_HandleNull(rsAux!val_efr, "0"))
                    
                total_count = total_count + CDbl(BL_HandleNull(rsAux!conta, "0"))
                total_proven = total_proven + CDbl(BL_HandleNull(rsAux!val_efr, "0"))
                total_efr = total_efr + CDbl(BL_HandleNull(rsAux!val_efr, "0"))
                    
                rsAux.MoveNext
        Wend
            
        If (total_count > 0) Then
            str_fich = str_fich & vbCrLf & _
                       " " & Left(str_proven & "                                                                   ", 35) & "   TOTAL   " & _
                       Right("                  " & total_count, 8) & "  " & _
                       Right("                                         " & Format(total_proven, "###0.00"), 10) & " �"
                       str_fich = str_fich & vbCrLf
        End If
                
        If (grande_total_count > 0) Then
            str_fich = str_fich & vbCrLf & "----------------------------------------------------------------------------" & vbCrLf & _
                       " " & Left("TOTAL                                                      ", 38) & "        " & _
                       Right("                  " & grande_total_count, 8) & "" & _
                       Right("                                         " & Format(grande_total_proven, "###0.00"), 12) & " �"
        End If
                
        str_fich = str_fich & vbCrLf & "----------------------------------------------------------------------------" & vbCrLf

        ' ---------------------------

        f = FreeFile
        Open gDirCliente & "\Bin\Logs\ESTATISTICA_" & Trim(Me.EcAno.text) & "_" & Me.CbMes.List(Me.CbMes.ListIndex) & ".TXT" For Output As #f
        Print #f, str_fich & vbCrLf
        
        str_fich = ""
        
        ' ---------------------------
        
        ' Por EFRs.

        rsAux.Close
        
        sql = "SELECT " & vbCrLf & _
              "   T.efr as descr_efr,  " & vbCrLf & _
              "   SUM(T.val_efr) AS soma,  " & vbCrLf & _
              "   SUM(T.conta) AS conta " & vbCrLf & _
              "FROM " & vbCrLf & _
              "   sl_cr_estat_facturacao_" & gNumeroSessao & " T " & vbCrLf & _
              "GROUP BY " & vbCrLf & _
              "   T.cod_efr, " & vbCrLf & _
              "   T.efr " & vbCrLf & _
              "ORDER BY " & vbCrLf & _
              "   T.efr "
        
        gSQLError = 0
        gSQLISAM = 0
        Err.Clear
        rsAux.Open sql
        BG_Trata_BDErro
        
        Print #f, "----------------------------------------------------------------------------"
        Print #f, " POR ENTIDADE FINANCEIRA"
        Print #f, "----------------------------------------------------------------------------" & vbCrLf
        
        While Not rsAux.EOF
        
            Print #f, " " & Left(Trim(BL_HandleNull(rsAux!descr_efr, "")) & "                                                              ", 47) & _
                      Right("                  " & BL_HandleNull(rsAux!conta, ""), 8) & _
                      Right("                                         " & Format(BL_HandleNull(rsAux!soma, ""), "###0.00"), 10)
            
            rsAux.MoveNext
        Wend
        
        rsAux.Close
        
        sql = "SELECT " & vbCrLf & _
              "     SUM(T.val_efr) AS soma, " & vbCrLf & _
              "     SUM(T.conta) AS conta " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_cr_estat_facturacao_" & gNumeroSessao & " T "
        
        gSQLError = 0
        gSQLISAM = 0
        Err.Clear
        rsAux.Open sql
        BG_Trata_BDErro
        
        If Not rsAux.EOF Then
        
'            Print #f, vbCrLf
            Print #f, vbCrLf & "----------------------------------------------------------------------------"
            Print #f, " TOTAL                                          " & _
                      Right("                  " & BL_HandleNull(rsAux!conta, ""), 8) & _
                      Right("                                         " & Format(BL_HandleNull(rsAux!soma, ""), "###0.00"), 10) & " �"
            Print #f, "----------------------------------------------------------------------------"
        
        End If
        
        ' ---------------------------
        
        Close #f
                
        ' ---------------------------
        
        DoEvents
        DoEvents
        DoEvents
        DoEvents
        
        Dim X
        X = Shell("Notepad " & gDirCliente & "\Bin\Logs\ESTATISTICA_" & Trim(Me.EcAno.text) & "_" & Me.CbMes.List(Me.CbMes.ListIndex) & ".TXT", vbMaximizedFocus)
            
    End If
    
    ' -----------------------------------------------------------------------------
    
    Set rsAux = Nothing
    
    Mostra_Relatorio_TXT = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rsAux = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : Mostra_Relatorio_TXT (FormEstatFACTURACAO) -> " & Err.Number & ", " & Err.Description)
            Mostra_Relatorio_TXT = -1
            Exit Function
    End Select
End Function

Function Obtem_Intervalo_Facturas(ano As String, _
                                  mes As String, _
                                  ByRef nfact_ini As String, _
                                  ByRef nfact_fim As String) As Integer

    On Error GoTo ErrorHandler
    
    Dim rs As ADODB.recordset
    Dim sql As String
    
    nfact_ini = ""
    nfact_fim = ""
    
    ano = Right("0000" & Trim(ano), 4)
    mes = Right("00" & Trim(mes), 2)

    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    rs.ActiveConnection = gConexao
    
    sql = "SELECT " & vbCrLf & _
          "     n_fac " & vbCrLf & _
          "FROM " & vbCrLf & _
          "     sl_facturas_factus " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "     dt_emiss_fac > '01-" & mes & "-" & ano & "' AND " & vbCrLf & _
          "     cod_efr = '1' " & vbCrLf & _
          "ORDER BY " & vbCrLf & _
          "     serie_fac ASC, " & vbCrLf & _
          "     n_fac     ASC "

    rs.Open sql
    
    Err.Clear
    On Error Resume Next
    ' 1� Factura ARS do m�s.
    nfact_ini = Trim(CStr(CLng(rs(0))))
    If (Err.Number <> 0) Then
        ' Erro.
        Obtem_Intervalo_Facturas = -2
        Exit Function
    End If
    
    rs.MoveNext
    ' 1� Factura ARS do m�s seguinte, n�o entra.
    nfact_fim = Trim(rs(0))
    nfact_fim = Trim(CStr(CInt(nfact_fim) - 1))
    
    If (Err.Number <> 0) Then
        ' Erro.
        rs.Close
        Set rs = Nothing
        ' N�o encontra a factura do m�s seguinte.
        ' N�o foi processada ?
        nfact_fim = Trim(CStr(CLng(nfact_ini) + 500))
        
        Obtem_Intervalo_Facturas = 2
        Exit Function
    End If
    Err.Clear
    On Error GoTo ErrorHandler
    
    rs.Close
    Set rs = Nothing

    Obtem_Intervalo_Facturas = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            nfact_ini = ""
            nfact_fim = ""
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : Obtem_Intervalo_Facturas (FormEstatFACTURACAO) -> " & Err.Number & ", " & Err.Description)
            Obtem_Intervalo_Facturas = -1
            Exit Function
    End Select
End Function


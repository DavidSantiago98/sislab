Attribute VB_Name = "BibliotecaGenericaADO"
'Option Explicit

' Actualiza��o : 08/02/2002
' T�cnico Paulo Costa

'*-------------------------------------*
'| Biblioteca de Rotinas Gen�ricas ADO |
'*-------------------------------------*
'NELSONPSILVA Glintt-HS-18011 04.04.2018
    Dim requestJson As String
    Dim responseJson As String

Function Bg_DaData_ADO() As Date

    Dim CmdData As New ADODB.Command
    Dim PmtData As ADODB.Parameter
    Dim rsData As ADODB.recordset
    Dim i As Integer
    Dim data As Date
    On Error GoTo TrataErro
    
    data = -1
    i = 0
    While i < 10 And data = -1
        With CmdData
            .ActiveConnection = gConexao
            .CommandText = "DEVOLVE_DATA"
            .CommandType = adCmdStoredProc
        End With
        
        If gSGBD = gOracle Or gSGBD = gSqlServer Then
            Set PmtData = CmdData.CreateParameter("DATA", adVarChar, adParamOutput, 12)
            CmdData.Parameters.Append PmtData
            CmdData.Execute
            data = Format(Trim(CmdData.Parameters.item(0).value), gFormatoData)
        Else
            Set rsData = CmdData.Execute
            If rsData.EOF Then
                data = "-1"
            Else
                data = Format(Trim(CmdData.Parameters.item(0).value), gFormatoData)
            End If
        End If
        If data = -1 Then
            Set rsData = Nothing
            Set CmdData = Nothing
        End If
        
        i = i + 1
    Wend
    
    If data = "-1" Then
        'BG_LogFile_Erros "BG_DaData_ADO: ATEN��O - Data obtida localmente! Contactar o Administrador da aplica��o."
        Bg_DaData_ADO = Format(Date, gFormatoData)
    Else
        If gSGBD = gOracle Or gSGBD = gSqlServer Then
            Bg_DaData_ADO = Format(Trim(data), gFormatoData)
        Else
            Bg_DaData_ADO = Format(Trim(rsData.Fields(0).value), gFormatoData)
        End If
    End If
    
    Set CmdData = Nothing
    Set PmtData = Nothing
    Set rsData = Nothing

    Exit Function

TrataErro:
    'BG_TrataErro "BG_DaData_ADO", Err
    'BG_Mensagem mediMsgBox, "BG_DaData_ADO: " & Error(Err), vbExclamation, "ATEN��O: ERRO GRAVE"
    Bg_DaData_ADO = Format(Date, gFormatoData)
    'Resume Next
End Function


Function Bg_DaDataHora_ADO() As Date

    Dim CmdData As New ADODB.Command
    Dim PmtData As ADODB.Parameter
    Dim rsData As ADODB.recordset
    Dim i As Integer
    Dim data As Date
    On Error GoTo TrataErro
    
    data = -1
    i = 0
    While i < 10 And data = -1
        With CmdData
            .ActiveConnection = gConexao
            .CommandText = "devolve_data_hora"
            .CommandType = adCmdStoredProc
        End With
        
        If gSGBD = gOracle Or gSGBD = gSqlServer Then
            Set PmtData = CmdData.CreateParameter("DATA", adVarChar, adParamOutput, 30)
            CmdData.Parameters.Append PmtData
            CmdData.Execute
            data = CDate(Format(Trim(CmdData.Parameters.item(0).value), "dd-mm-yyyy hh:nn"))
        Else
            Set rsData = CmdData.Execute
            If rsData.EOF Then
                data = -1
            Else
                data = CDate(Format(Trim(CmdData.Parameters.item(0).value), "dd-mm-yyyy hh:nn"))
            End If
        End If
        If data = -1 Then
            Set rsData = Nothing
            Set CmdData = Nothing
        End If
        
        i = i + 1
    Wend
    
    If data = -1 Then
        'BG_LogFile_Erros "BG_DaData_ADO: ATEN��O - Data obtida localmente! Contactar o Administrador da aplica��o."
        Bg_DaDataHora_ADO = Format(Now, "dd-mm-yyyy hh:nn")
    Else
        If gSGBD = gOracle Or gSGBD = gSqlServer Then
            Bg_DaDataHora_ADO = Format(Trim(data), "dd-mm-yyyy hh:nn")
        Else
            Bg_DaDataHora_ADO = Format(Trim(rsData.Fields(0).value), "dd-mm-yyyy hh:nn")
        End If
    End If
    
    Set CmdData = Nothing
    Set PmtData = Nothing
    Set rsData = Nothing

    Exit Function

TrataErro:
    'BG_TrataErro "BG_DaData_ADO", Err
    'BG_Mensagem mediMsgBox, "BG_DaData_ADO: " & Error(Err), vbExclamation, "ATEN��O: ERRO GRAVE"
    Bg_DaDataHora_ADO = Format(Now, "dd-mm-yyyy hh:nn")
    Exit Function
    Resume Next
End Function
Function Bg_DaHora_ADO() As String

    Dim CmdHora As New ADODB.Command
    Dim PmtHora As ADODB.Parameter
    Dim RsHora As ADODB.recordset
    
    Dim s As String
    Dim horas As String
    Dim minutos As String
        
    On Error GoTo TrataErro
    
    With CmdHora
        .ActiveConnection = gConexao
        .CommandText = "DEVOLVE_HORA"
        .CommandType = adCmdStoredProc
    End With
    
    If gSGBD = gOracle Or gSGBD = gSqlServer Then
        Set PmtHora = CmdHora.CreateParameter("HORA", adVarChar, adParamOutput, 10)
        CmdHora.Parameters.Append PmtHora
    Else
        Set RsHora = CmdHora.Execute
        If RsHora.EOF Then Bg_DaHora_ADO = "-1"
    End If
    
    CmdHora.Execute
    If Bg_DaHora_ADO = "-1" Then
        BG_LogFile_Erros "bg_dahora_ado: ATEN��O - Hora obtida localmente! Contactar o Administrador da aplica��o."
        Bg_DaHora_ADO = Format(Date, "HH:MM")
    Else
        If gSGBD = gOracle Or gSGBD = gSqlServer Then
            s = Trim(CmdHora.Parameters.item(0).value)
            If Len(s) > 8 Then
                s = Mid(s, 1, Len(s) - 3)
            End If
            Bg_DaHora_ADO = Format(s, "HH:MM")
        Else
            Bg_DaHora_ADO = Mid(CStr(RsHora.Fields(0).value), 1, 5)

        End If
    End If
    
    Set CmdHora = Nothing
    Set PmtHora = Nothing
    Set RsHora = Nothing

    Exit Function

TrataErro:
    'BG_TrataErro "BG_DaHora_ADO", Err
    BG_Mensagem mediMsgBox, "BG_DaHora_ADO: " & Error(Err), vbExclamation, "ATEN��O: ERRO GRAVE"
     Bg_DaHora_ADO = "-1"
    'Resume Next
End Function

Public Function BG_Abre_ConexaoBD_ADO()
       
    'vari�vel ter� de ser global uma s� vez usada para a aplica��o em geral
    Set gConexao = New ADODB.Connection
    
    On Error GoTo TrataErro
    
    With gConexao
        'este � provider que indica que vamos usar ODBC
        'para utilizar o OLEDB directamente, ter� que se mudar o provider
        'para o respectivo Provider OLEDB (sqlserver, informix ou oracle)
        If gProvider <> "" Then
             .Provider = gProvider
            .Open gOracleServer, gOracleUID, gOraclePWD
        
        Else
             .Provider = "MSDASQL"
             ' gDSN = Data Source especificado no DNS do ODBC (criado no registo do Windows pelas Inicializa��es)
             If gFileDSN = "" Then
                 .ConnectionString = "DSN=" & gDSN
             Else
                 .ConnectionString = "FILEDSN=" & gFileDSN
             End If
             .CommandTimeout = "180"
            .Open
        End If
    End With
    
    ' Defini�ao dos comandos da aplica��o
    CarregaParamAmbiente
    BG_DefineComandos_ADO
    gSGBD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SGBD")
    gSGBD_SECUNDARIA = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SGBD_SECUNDARIA")
    gVersaoSGBD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VERSAO_gSGBD")
    
    'Quando o gSGBD � ORACLE ou SQL Server, torna-se necess�rio defenir o
    'formato data utilizado dentro da conex�o, de modo a uniformizar o mesmo
    'formato.
    If Trim(gSGBD) = gSqlServer Then
        gConexao.Execute "SET DATEFORMAT dmy"
    ElseIf Trim(gSGBD) = gPostGres Then
        gConexao.Execute "SET datestyle = ""ISO, DMY"""
    Else
       gConexao.Execute "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'"
    End If

    BG_Abre_ConexaoBD_ADO = 1
    
    Exit Function
    
TrataErro:
    If gProvider <> "" And Err.Number = 3706 Then
        gProvider = ""
        BG_Abre_ConexaoBD_ADO = BG_Abre_ConexaoBD_ADO()
    Else
        Beep
        BG_Mensagem mediMsgBox, "Erro a Abrir a Base de Dados", vbCritical, cAPLICACAO_NOME_CURTO
        'BG_LogFile_Erros "Abertura da BD (" & gSGBD & " - " & gDSN & ") : " & Err.Number & " (" & Err.Description & ")"
        BG_Abre_ConexaoBD_ADO = 0
        Exit Function
        Resume Next
    End If
End Function
Public Function BG_Abre_ConexaoBD_LOGS_ADO()
       
    'vari�vel ter� de ser global uma s� vez usada para a aplica��o em geral
    Set gConexaoLOGS = New ADODB.Connection
    
    On Error GoTo TrataErro
    
    With gConexaoLOGS
        'este � provider que indica que vamos usar ODBC
        'para utilizar o OLEDB directamente, ter� que se mudar o provider
        'para o respectivo Provider OLEDB (sqlserver, informix ou oracle)
        .Provider = "MSDASQL"
        ' gDSN = Data Source especificado no DNS do ODBC (criado no registo do Windows pelas Inicializa��es)
        If gFileDSN = "" Then
            .ConnectionString = "DSN=" & gDSN
        Else
            .ConnectionString = "FILEDSN=" & gFileDSN
        End If
       .Open
    End With
    
    If Trim(gSGBD) = gSqlServer Then
        gConexaoLOGS.Execute "SET DATEFORMAT dmy"
    ElseIf Trim(gSGBD) = gOracle Then
       gConexaoLOGS.Execute "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'"
    End If

    BG_Abre_ConexaoBD_LOGS_ADO = 1
    
    Exit Function
    
TrataErro:
    Beep
    BG_Mensagem mediMsgBox, "Erro a Abrir a Base de Dados LOGS", vbCritical, cAPLICACAO_NOME_CURTO
    'BG_LogFile_Erros "Abertura da BD LOGS(" & gSGBD & " - " & gDSN & ") : " & Err.Number & " (" & Err.Description & ")"
    BG_Abre_ConexaoBD_LOGS_ADO = 0
End Function

Public Function BG_DefineComandos_ADO()

    'inicializacao do comando ADO para seleccao de valores da
    'tabela de parametrizacao de ambiente em BG_DaParamAmbiente_ADO
    
    ' Geral
    Set gCmdParamAmbiente_G.ActiveConnection = gConexao
    gCmdParamAmbiente_G.CommandType = adCmdText
    gCmdParamAmbiente_G.CommandText = "SELECT codigo, conteudo FROM " & cParamAmbiente & " WHERE chave = ? AND ambito = ? ORDER BY codigo ASC"
    gCmdParamAmbiente_G.Prepared = True
    Set gPmtChave = gCmdParamAmbiente_G.CreateParameter("CHAVE", adVarChar, adParamInput, 40)
    gCmdParamAmbiente_G.Parameters.Append gPmtChave
    Set gPmtAmbito = gCmdParamAmbiente_G.CreateParameter("AMBITO", adInteger, adParamInput, 9)
    gCmdParamAmbiente_G.Parameters.Append gPmtAmbito
    
    ' Por computador
    Set gCmdParamAmbiente_C.ActiveConnection = gConexao
    gCmdParamAmbiente_C.CommandType = adCmdText
    gCmdParamAmbiente_C.CommandText = "SELECT codigo, conteudo " & _
                                       "FROM " & cParamAmbiente & " " & _
                                       "WHERE chave = ? AND ambito = ? AND ( cod_computador = ?  ) " & _
                                       "ORDER BY codigo ASC"
    gCmdParamAmbiente_C.Prepared = True
    gCmdParamAmbiente_C.Parameters.Append gPmtChave
    gCmdParamAmbiente_C.Parameters.Append gPmtAmbito
    Set gPmtComputador = gCmdParamAmbiente_C.CreateParameter("COD_COMPUTADOR", adInteger, adParamInput, 9)
    gCmdParamAmbiente_C.Parameters.Append gPmtComputador
    
    ' Por Utilizador
    Set gCmdParamAmbiente_U.ActiveConnection = gConexao
    gCmdParamAmbiente_U.CommandType = adCmdText
    gCmdParamAmbiente_U.CommandText = "SELECT codigo, conteudo FROM " & cParamAmbiente & " WHERE chave = ? AND ambito = ? AND ( cod_utilizador = ? ) ORDER BY codigo ASC"
    gCmdParamAmbiente_U.Prepared = True
    gCmdParamAmbiente_U.Parameters.Append gPmtChave
    gCmdParamAmbiente_U.Parameters.Append gPmtAmbito
    Set gPmtUtilizador = gCmdParamAmbiente_U.CreateParameter("COD_UTILIZADOR", adInteger, adParamInput, 9)
    gCmdParamAmbiente_U.Parameters.Append gPmtUtilizador
    
    'inicializacao do comando ADO para seleccao de valores da
    'tabela de parametrizacao de acessos em BG_ParametrizaPermissoes_ADO
    Set gCmdParamAcessos.ActiveConnection = gConexao
    gCmdParamAcessos.CommandType = adCmdText
    gCmdParamAcessos.CommandText = " SELECT * FROM " & cParamAcessos & " WHERE ( cod_grupo= ? OR cod_util= ? ) AND nome_form = ?  AND upper(descr_prop )not in( 'VISIBLE', 'ENABLED') ORDER BY seq_param_acess ASC "
    gCmdParamAcessos.Prepared = True
    Set gPmtGrupoUtil = gCmdParamAcessos.CreateParameter("GRUPO_UTIL", adInteger, adParamInput, 9)
    gCmdParamAcessos.Parameters.Append gPmtGrupoUtil
    Set gPmtCodUtil = gCmdParamAcessos.CreateParameter("COD_UTIL", adInteger, adParamInput, 9)
    gCmdParamAcessos.Parameters.Append gPmtCodUtil
    Set gPmtForm = gCmdParamAcessos.CreateParameter("FORM", adVarChar, adParamInput, 100)
    gCmdParamAcessos.Parameters.Append gPmtForm
    
End Function

Function BG_Fecha_ConexaoBD_ADO()

    'fecha a conex�o
    gConexao.Close
    
    'liberta os comandos e parametros
    Set gCmdParamAcessos = Nothing
    Set gCmdParamAmbiente_G = Nothing
    Set gCmdParamAmbiente_C = Nothing
    Set gCmdParamAmbiente_U = Nothing
    Set gPmtChave = Nothing
    Set gPmtCodUtil = Nothing
    Set gPmtForm = Nothing
    Set gPmtGrupoUtil = Nothing
    
    'liberta a vari�vel de conex�o
    Set gConexao = Nothing
    
End Function

Function BG_ConstroiCriterio_UPDATE_ADO(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant, condicao As String) As String

' Constroi um statment SQL para UPDATE baseado nos campos do Form,
' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)

    Dim Criterio As String
    Dim ValorCampo, ValorControl As Variant
    Dim i, nPassou, nTipo, pos As Integer

    i = 0
    nPassou = 0
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
    
    'criar a instru�ao de UPDATE
    Criterio = "UPDATE " & NomeTabela & " SET "
    
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf NomesControl(i) Is TextBox Or _
           TypeOf NomesControl(i) Is Label Or _
           TypeOf NomesControl(i) Is MaskEdBox Or _
           TypeOf NomesControl(i) Is RichTextBox Then
           
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = BG_DataComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'controlar o primeiro campo para nao levar virgula
        If nPassou <> 0 Then
            Criterio = Criterio & ", "
        End If
        
        'se o campo do form tem conteudo formata o conteudo para o statment SQL
        If ValorControl <> "" Then
            
            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = mediTipoCapitalizado Or nTipo = mediTipoData _
                   Or nTipo = mediTipoDefeito Or nTipo = mediTipoHora _
                   Or nTipo = mediTipoMaiusculas Or nTipo = mediTipoMaiusculas _
                   Or nTipo = mediTipoMinusculas Then
                    BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            End If

            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            If nTipo = adDecimal Or nTipo = adNumeric Or nTipo = adSingle Or nTipo = adVarNumeric Or nTipo = adDouble Or nTipo = adInteger Then
                Criterio = Criterio & ValorCampo & " = " & BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            ElseIf nTipo = adDate Or nTipo = adDBDate Or nTipo = adDBTimeStamp Or nTipo = adDBTime Then
                If NomesControl(i).Tag = CStr(mediTipoHora) Then
                    'hora
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'outro
            Else
                If ValorControl <> " " Then ValorControl = RTrim(ValorControl)
                Criterio = Criterio & ValorCampo & " = " & "'" & ValorControl & "'"
            End If
        'se o campo do form esta vazio, poe null
        Else
            Criterio = Criterio & ValorCampo & " = " & "null"
        End If
        nPassou = 1
        i = i + 1
    Next
    
    'acrescenta o criterio de UPDATE
    Criterio = Criterio & " WHERE " & condicao

    'Mensagens
'    BG_LogFile_Erros "BG_ConstroiCriterio_UPDATE_ADO:"
'    BG_LogFile_Erros "    " & Criterio
    
    'para debug:
    'BG_Mensagem mediMsgBox, Criterio, , "BG_ConstroiCriterio_UPDATE_ADO"

    'Retorno de resultados
    BG_ConstroiCriterio_UPDATE_ADO = Criterio
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        requestJson = BL_TrataCamposRGPD(NomesControl)
        responseJson = vbNullString
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Alteracao) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
    End If

End Function

Function BG_ConstroiCriterio_SELECT_ADO(NomeTabela As String, _
                                        NomesCampo As Variant, _
                                        NomesControl As Variant, _
                                        SelTotal As Boolean) As String
    
    ' Constroi um statment SQL para SELECT baseado nos campos do Form,
    ' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)
    
    Dim nTipo As Integer
    Dim i As Integer
    Dim nPassou As Integer
    Dim iCount As Integer
    Dim res As Integer
    Dim iTempDecimal As Integer
    Dim Criterio As String
    Dim sTempDecimal As String
    Dim ValorCampo, ValorControl As Variant
    Dim pos As Integer

    i = 0
    iCount = 0
    nPassou = 0
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("sl_identif") Then
            NomeTabela = "slv_identif"
        End If
    End If
    
    'cria a instru�ao de SELECT
    Criterio = "SELECT * FROM " & NomeTabela & " WHERE "
    
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        ElseIf TypeOf NomesControl(i) Is RichTextBox Then
        'RichtTextBox: Se estiver vazia (o TEXT) n�o inclui no crit�rio
        '(caso contr�rio entrava na query a formata��o do texto vazio!)
            If Trim(NomesControl(i).Text) <> "" Then
                ValorControl = NomesControl(i)
                ValorControl = BG_CvPlica(ValorControl)
            Else
                ValorControl = ""
            End If
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = BG_DataComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'se o campo do form esta vazio nao entra no criterio
        If ValorControl = "" Then
            iCount = iCount + 1
        'se o campo do form tem conteudo, formata o conteudo para o statment SQL
        Else
            'controlar o primeiro campo para nao levar virgula
            If nPassou <> 0 Then
                Criterio = Criterio & " AND "
            End If

            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = mediTipoCapitalizado Or nTipo = mediTipoData _
                   Or nTipo = mediTipoDefeito Or nTipo = mediTipoHora _
                   Or nTipo = mediTipoMaiusculas Or nTipo = mediTipoMaiusculas _
                   Or nTipo = mediTipoMinusculas Then
                    BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            End If

        
            Select Case nTipo
            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            Case adDecimal, adNumeric, adSingle, adVarNumeric, adDouble, adInteger
                Criterio = Criterio & ValorCampo & " = " & BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                If NomesControl(i).Tag = CStr(mediTipoHora) Then
                    'hora
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'string: adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar,
            '        adVarChar, adVariant, adVarWChar, adWChar
            '(controlar os wilcards - '*' e '?' )
            Case adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                If InStr(ValorControl, "*") <> 0 Or InStr(ValorControl, "?") <> 0 Then
                    ValorControl = BG_CvWilcard(ValorControl)
                    Criterio = Criterio & ValorCampo & " LIKE " & "'" & ValorControl & "'"
                Else
                    Criterio = Criterio & ValorCampo & " = " & "'" & ValorControl & "'"
                End If
            'outro
            Case Else
                Criterio = Criterio & ValorCampo & " = " & ValorControl
            End Select
            
            nPassou = 1
        End If
        
        i = i + 1
    Next

    'caso todos os campos estejam vazios, selecciona tudo
    If iCount = i Then
        Criterio = "SELECT * FROM " & NomeTabela
        SelTotal = True
    End If
    
    ' Mensagens
'    BG_LogFile_erros "BG_ConstroiCriterio_SELECT_ADO:"
'    BG_LogFile_erros "    " & Criterio
    
    'para debug:
    'BG_Mensagem mediMsgBox, Criterio, , "BG_ConstroiCriterio_SELECT_ADO"
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        BG_ConstroiCriterio_SELECT_ADO = BG_DevolveCriterioFiltrado(NomeTabela, Criterio)
    Else
    ' Retorno de resultados
    BG_ConstroiCriterio_SELECT_ADO = Criterio
    End If
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
  '      If gFormActivo <> FormIdentificaUtente Then
           requestJson = BL_TrataCamposRGPD(NomesControl)
  '      End If
    End If

End Function


Function BG_ConstroiCriterio_COUNT_ADO(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant, SelTotal As Boolean) As Integer
    
' Constroi um statment SQL para SELECT COUNT(*) baseado nos campos do Form,
' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)
' e retorna o pr�prio COUNT(*)
'
' O objectivo desta rotina, � permitir saber os registos selecionados num form que utilize
' o recordset aberto do lado do servidor (n�o retorna o recordcount!)
    
    Dim nTipo As Integer
    Dim i As Integer
    Dim nPassou As Integer
    Dim iCount As Integer
    Dim res As Integer
    Dim iTempDecimal As Integer
    Dim sTempDecimal As String
    Dim Criterio As String
    Dim ValorCampo As Variant
    Dim ValorControl As Variant
    Dim rsCount As ADODB.recordset
    Dim pos As Integer
    
    Set rsCount = New ADODB.recordset
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
    
    'cria a instru�ao de SELECT count(*)
    Criterio = "SELECT count(*) as conta FROM " & NomeTabela & " WHERE "
    
    ' exactamente igual ao BG_ConstroiCriterio_SELECT_ADO
    '-----------------------------------------------------
    i = 0
    iCount = 0
    nPassou = 0
    For Each ValorCampo In NomesCampo
        If Trim(NomesControl(i).Tag) = "" Then
            'retirar o tipo de campo da base de dados
            BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
            nTipo = gFieldObjectProperties(0).tipo
        Else
            'caso j� esteja defenido
            pos = InStr(NomesControl(i).Tag, "(")
            If pos <> 0 Then
                'tratar o decimal
                nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
            Else
                nTipo = CInt(NomesControl(i).Tag)
            End If
            
            'verificar se n�o se trata duma defini��o local
            If nTipo = mediTipoCapitalizado Or nTipo = mediTipoData _
               Or nTipo = mediTipoDefeito Or nTipo = mediTipoHora _
               Or nTipo = mediTipoMaiusculas Or nTipo = mediTipoMaiusculas _
               Or nTipo = mediTipoMinusculas Then
                BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            End If
        
        End If

        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        ElseIf TypeOf NomesControl(i) Is RichTextBox Then
            If Trim(NomesControl(i).Text) <> "" Then
                ValorControl = NomesControl(i)
                ValorControl = BG_CvPlica(ValorControl)
            End If
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = BG_DataComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        Else
        End If
        
        If ValorControl = "" Then
            iCount = iCount + 1
        Else
            If nPassou <> 0 Then
                Criterio = Criterio & " AND "
            End If
            Select Case nTipo
            Case adDecimal, adNumeric, adSingle, adVarNumeric, adDouble, adInteger
                Criterio = Criterio & ValorCampo & " = " & BG_CvDecimalParaWhere_ADO(ValorControl)
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                If NomesControl(i).Tag = CStr(mediTipoHora) Then
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            Case adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                If InStr(ValorControl, "*") <> 0 Or InStr(ValorControl, "?") <> 0 Then
                    ValorControl = BG_CvWilcard(ValorControl)
                    Criterio = Criterio & ValorCampo & " LIKE " & "'" & ValorControl & "'"
                Else
                    Criterio = Criterio & ValorCampo & " = " & "'" & ValorControl & "'"
                End If
            Case Else
                Criterio = Criterio & ValorCampo & " = " & ValorControl
            End Select
            nPassou = 1
        End If
        i = i + 1
    Next

    If iCount = i Then
        Criterio = "SELECT count(*) as conta FROM " & NomeTabela
        SelTotal = True
    End If
    
    'fim da parte igual ao BG_ConstroiCriterio_SELECT_ADO
    '-----------------------------------------------------
    
    ' Mensagens
'    BG_LogFile_Erros "BG_ConstroiCriterio_COUNT_ADO:"
'    BG_LogFile_Erros "    " & Criterio
    
    'executa o criterio
    rsCount.CursorType = adOpenStatic
    rsCount.CursorLocation = adUseServer
    
    If gModoDebug = mediSim Then BG_LogFile_Erros Criterio
    rsCount.Open Criterio, gConexao, adOpenStatic, adLockReadOnly
    
    ' Retorno de resultados
    If rsCount.RecordCount > 0 Then
        BG_ConstroiCriterio_COUNT_ADO = rsCount!conta
    Else
        BG_ConstroiCriterio_COUNT_ADO = 0
    End If
    
    'fecha o recordset
    rsCount.Close
    Set rsCount = Nothing
    
End Function

Function BG_DaParamAmbiente_ADO(iAmbito As Integer, sChave As String, Optional ByRef iNumReg As Variant, Optional ByRef n_esimo As Variant) As String

'   Argumentos de entrada:
'   ---------------------
'   iAmbito - �mbito a que corresponde a chave, que poder� ser:
'       . mediAmbitoGeral
'       . mediAmbitoComputador
'       . mediAmbitoUtilizador
'   sChave - chave a pesquisar
'   iNumReg - n�mero de registos encontrados para a chave pretendida
'   n_esimo - Se houver v�rios registos para a mesma chave � retornado o n �simo registo
'
'   Resultado:
'   ---------
'   O resultado � o conteudo pretendido para a chave indicada ou (-1) quando n�o existe
'
    Dim sSql As String
    Dim rsResult As ADODB.recordset
    Dim registos As Integer
    Dim sRes As String
    Dim Erro As Integer
    
    On Error GoTo TrataErro

    Erro = 0
    sRes = ""
    
    'gT_Geral
    If iAmbito = 0 Then
        gCmdParamAmbiente_G.Parameters(0).value = sChave
        gCmdParamAmbiente_G.Parameters(1).value = iAmbito
        Set rsResult = gCmdParamAmbiente_G.Execute
    'gT_Computador
    ElseIf iAmbito = 1 Then
        gCmdParamAmbiente_C.Parameters(0).value = sChave
        gCmdParamAmbiente_C.Parameters(1).value = iAmbito
        gCmdParamAmbiente_C.Parameters(2).value = BL_GetComputerCodigo()
        Set rsResult = gCmdParamAmbiente_C.Execute
    'gT_Utilizador
    ElseIf iAmbito = 2 Then
        gCmdParamAmbiente_U.Parameters(0).value = sChave
        gCmdParamAmbiente_U.Parameters(1).value = iAmbito
        gCmdParamAmbiente_U.Parameters(2).value = gCodUtilizador
        Set rsResult = gCmdParamAmbiente_U.Execute
    End If
    
    If Not rsResult.EOF Then
        If BL_HandleNull(rsResult!conteudo, "") <> "" Then
            If IsMissing(n_esimo) Then
                sRes = rsResult!conteudo
            Else
                rsResult.Move n_esimo - 1
                sRes = rsResult!conteudo
            End If
        End If
    End If
    
    If Not IsMissing(iNumReg) Then
        rsResult.Requery
        iNumReg = 0
        While Not rsResult.EOF
            iNumReg = iNumReg + 1
            rsResult.MoveNext
        Wend
    End If

    If sRes = "" Then
        sRes = "-1"
    End If
        
    BG_DaParamAmbiente_ADO = RTrim(sRes)
    rsResult.Close
    Set rsResult = Nothing
    
    Exit Function
    
TrataErro:
    BG_TrataErro "BG_DaParamAmbiente", Err, "Chave = '" & sChave & "'"
    Resume Next
End Function

Function BG_DaParamAmbiente_NEW(iAmbito As Integer, sChave As String, Optional ByRef iNumReg As Variant, Optional ByRef n_esimo As Variant) As String

'   Argumentos de entrada:
'   ---------------------
'   iAmbito - �mbito a que corresponde a chave, que poder� ser:
'       . mediAmbitoGeral
'       . mediAmbitoComputador
'       . mediAmbitoUtilizador
'   sChave - chave a pesquisar
'   iNumReg - n�mero de registos encontrados para a chave pretendida
'   n_esimo - Se houver v�rios registos para a mesma chave � retornado o n �simo registo
'
'   Resultado:
'   ---------
'   O resultado � o conteudo pretendido para a chave indicada ou (-1) quando n�o existe
'
    Dim registos As Long
    Dim sRes As String
    Dim Erro As Integer
        
    On Error GoTo TrataErro

    Erro = 0
    sRes = ""
    For registos = 1 To gTotalParamet
        If gEstrutParamet(registos).ambito = iAmbito And gEstrutParamet(registos).chave = sChave And _
            (gCodLocal = gEstrutParamet(registos).cod_local Or gEstrutParamet(registos).cod_local = mediComboValorNull) Then
                sRes = gEstrutParamet(registos).conteudo
                Exit For
        End If
    Next
    If sRes = "" Then
        sRes = "-1"
    End If
        
    BG_DaParamAmbiente_NEW = RTrim(sRes)
    
Exit Function
TrataErro:
    BG_LogFile_Erros "BG_DaParamAmbiente_NEW:" & Err.Description, "BG_ADO", "BG_DaParamAmbiente_NEW", True
    Resume Next
End Function

Function BG_Trata_BDErro() As Integer

'Verifica a validade da ultima opera��o SQL via ODBC e faz o display duma
'mensagem conforme o erro.
'O erro tratado � o erro nativo retornado pelo ODBC e corresponde
'ao erro emitido pelo gSGBD usado.
'As vari�veis gInsRegistoDuplo, gErroSyntax, ... s�o arrays com os erros correspondentes
'em SQL_SERVER, ORACLE, INFORMIX. Estas variaveis s�o globais ao projecto,
'e ter�o que ser criadas � medida que surgem os erros.
'Poder� haver a necessidade de fazer uma revis�o aos v�rios erros
'nativos para criar vari�veis que estejam em falta.
    Dim aux
    Dim retorno As Integer
    
    retorno = 0
    For Each aux In gValoresInsuf
        If aux = gSQLError Then
            If retorno = 0 Then
                BG_Mensagem mediMsgBox, "Valores insuficientes para inser��o !", vbExclamation, "Inserir"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gInsRegistoDuplo
        If aux = gSQLError Then
            If retorno = 0 Then
                BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gUpdRegistoDuplo
        If aux = gSQLError Then
            If retorno = 0 Then
                BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gInsRegistoNulo
        If aux = gSQLError Then
            If retorno = 0 Then
                BG_Mensagem mediMsgBox, "N�o se pode inserir valor vazio num dos campos da tabela!", vbExclamation, "Inserir"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gErroSyntax
        If aux = gSQLError Then
            If retorno = 0 Then
                BG_Mensagem mediMsgBox, "Erro de sintaxe !", vbExclamation, "Aten��o"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gChaveViolada
        If aux = gSQLError Then
            If retorno = 0 Then
                BG_Mensagem mediMsgBox, "N�o � poss�vel apagar registo. Chave violada!", vbExclamation, "Aten��o"
                retorno = -1
            End If
        End If
    Next
    
    If retorno = 0 And gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro de processamento na BD (Erro:" & gSQLError & ",Isam:" & gSQLISAM & ") !", vbExclamation, "Erro"
        retorno = -1
    End If
    
    BG_Trata_BDErro = retorno
End Function

Function BGAux_DaFieldObjectProperties_ADO(NomeTabela As String, _
                                           NomeCamposBD As Variant)

    'NOTA:
    '   Esta fun��o retorna o valor das propriedades dum ou ou todos campos duma tabela na estrutura:
    '       gFieldObjectProperties().tipo As String
    '       gFieldObjectProperties().Precisao As String
    '       gFieldObjectProperties().EscalaNumerica As String
    '       gFieldObjectProperties().TamanhoConteudo As String
    '       gFieldObjectProperties().TamanhoCampo As String
    '       gFieldObjectProperties().Nome As String

    Dim MyQuery As String
    Dim rs As ADODB.recordset
    Dim VersaoBD As Double
    Dim NomeCampoBD As Variant
    Dim Ind As Integer
    Dim CamposSelect As String
    
    Set rs = New ADODB.recordset
        
    On Error GoTo TrataErro
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
           
    ' reinizializar estrutura
    Ind = 0
    ReDim gFieldObjectProperties(0)
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    CamposSelect = ""
    If IsArray(NomeCamposBD) Then
        For Each NomeCampoBD In NomeCamposBD
            If Trim(CamposSelect) <> "" Then CamposSelect = CamposSelect & ","
            
            CamposSelect = CamposSelect & NomeCampoBD
        Next
    Else
        CamposSelect = CamposSelect & NomeCamposBD
    End If
    
    If Trim(gSGBD) = gInformix Then
        ' Verificar vers�o do INFORMIX
        ' (SELECT FIRST s� � suportado a partir da versao 7.30)
        VersaoBD = BL_String2Double(Trim(BG_CvDecimalParaDisplay_ADO(gVersaoSGBD)))
        
        If VersaoBD >= 7.3 Then
            MyQuery = "SELECT FIRST 1 " & CamposSelect & " FROM " & NomeTabela
        Else
            MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela
        End If
    ElseIf Trim(gSGBD) = gOracle Then
        MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela & _
                  " WHERE ROWNUM < 2 "
    ElseIf Trim(gSGBD) = gSqlServer Then
        MyQuery = "SELECT TOP 1 " & CamposSelect & " FROM " & NomeTabela
    ElseIf gSGBD = gPostGres Then
        MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela & " LIMIT 1"
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros MyQuery
    rs.Open MyQuery, gConexao
        
    If IsArray(NomeCamposBD) Then
        ' se o pedido de propriedades for para mais que um campo
        For Each NomeCampoBD In NomeCamposBD
            ' redimencionar a estrutura
            ReDim Preserve gFieldObjectProperties(Ind)
            
            ' atribuir valores
            gFieldObjectProperties(Ind).tipo = rs.Fields(NomeCampoBD).Type
            gFieldObjectProperties(Ind).Precisao = rs.Fields(NomeCampoBD).Precision
            gFieldObjectProperties(Ind).EscalaNumerica = rs.Fields(NomeCampoBD).NumericScale
            gFieldObjectProperties(Ind).TamanhoConteudo = rs.Fields(NomeCampoBD).ActualSize
            gFieldObjectProperties(Ind).TamanhoCampo = rs.Fields(NomeCampoBD).DefinedSize
            gFieldObjectProperties(Ind).nome = rs.Fields(NomeCampoBD).Name
        
            Ind = Ind + 1
        Next
    Else
        ' atribuir valores
        gFieldObjectProperties(Ind).tipo = rs.Fields(NomeCamposBD).Type
        gFieldObjectProperties(Ind).Precisao = rs.Fields(NomeCamposBD).Precision
        gFieldObjectProperties(Ind).EscalaNumerica = rs.Fields(NomeCamposBD).NumericScale
        gFieldObjectProperties(Ind).TamanhoConteudo = rs.Fields(NomeCamposBD).ActualSize
        gFieldObjectProperties(Ind).TamanhoCampo = rs.Fields(NomeCamposBD).DefinedSize
        gFieldObjectProperties(Ind).nome = rs.Fields(NomeCamposBD).Name
    End If
    
    Exit Function

TrataErro:

    If Err.Number <> 3021 Then
        BG_LogFile_Erros "BGAux_DaFieldObjectProperties: " & Err & "." & Err.Description & " - " & _
                                                             NomeTabela & " - " & NomeCampoBD
    End If
    
    Resume Next

End Function
Function BGAux_DaFieldObjectProperties_ADO_SEC(NomeTabela As String, _
                                           NomeCamposBD As Variant)

    'NOTA:
    '   Esta fun��o retorna o valor das propriedades dum ou ou todos campos duma tabela na estrutura:
    '       gFieldObjectProperties().tipo As String
    '       gFieldObjectProperties().Precisao As String
    '       gFieldObjectProperties().EscalaNumerica As String
    '       gFieldObjectProperties().TamanhoConteudo As String
    '       gFieldObjectProperties().TamanhoCampo As String
    '       gFieldObjectProperties().Nome As String

    Dim MyQuery As String
    Dim rs As ADODB.recordset
    Dim VersaoBD As Double
    Dim NomeCampoBD As Variant
    Dim Ind As Integer
    Dim CamposSelect As String
    
    Set rs = New ADODB.recordset
        
    On Error GoTo TrataErro
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
           
    ' reinizializar estrutura
    Ind = 0
    ReDim gFieldObjectProperties(0)
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    CamposSelect = ""
    If IsArray(NomeCamposBD) Then
        For Each NomeCampoBD In NomeCamposBD
            If Trim(CamposSelect) <> "" Then CamposSelect = CamposSelect & ","
            
            CamposSelect = CamposSelect & NomeCampoBD
        Next
    Else
        CamposSelect = CamposSelect & NomeCamposBD
    End If
    
    If Trim(gSGBD) = gInformix Then
        ' Verificar vers�o do INFORMIX
        ' (SELECT FIRST s� � suportado a partir da versao 7.30)
        VersaoBD = BL_String2Double(Trim(BG_CvDecimalParaDisplay_ADO(gVersaoSGBD)))
        
        If VersaoBD >= 7.3 Then
            MyQuery = "SELECT FIRST 1 " & CamposSelect & " FROM " & NomeTabela
        Else
            MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela
        End If
    ElseIf Trim(gSGBD) = gOracle Then
        MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela & _
                  " WHERE ROWNUM < 2 "
    ElseIf Trim(gSGBD) = gSqlServer Then
        MyQuery = "SELECT TOP 1 " & CamposSelect & " FROM " & NomeTabela
    Else
        MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros MyQuery
    rs.Open MyQuery, gConexaoSecundaria
        
    If IsArray(NomeCamposBD) Then
        ' se o pedido de propriedades for para mais que um campo
        For Each NomeCampoBD In NomeCamposBD
            ' redimencionar a estrutura
            ReDim Preserve gFieldObjectProperties(Ind)
            
            ' atribuir valores
            gFieldObjectProperties(Ind).tipo = rs.Fields(NomeCampoBD).Type
            gFieldObjectProperties(Ind).Precisao = rs.Fields(NomeCampoBD).Precision
            gFieldObjectProperties(Ind).EscalaNumerica = rs.Fields(NomeCampoBD).NumericScale
            gFieldObjectProperties(Ind).TamanhoConteudo = rs.Fields(NomeCampoBD).ActualSize
            gFieldObjectProperties(Ind).TamanhoCampo = rs.Fields(NomeCampoBD).DefinedSize
            gFieldObjectProperties(Ind).nome = rs.Fields(NomeCampoBD).Name
        
            Ind = Ind + 1
        Next
    Else
        ' atribuir valores
        gFieldObjectProperties(Ind).tipo = rs.Fields(NomeCamposBD).Type
        gFieldObjectProperties(Ind).Precisao = rs.Fields(NomeCamposBD).Precision
        gFieldObjectProperties(Ind).EscalaNumerica = rs.Fields(NomeCamposBD).NumericScale
        gFieldObjectProperties(Ind).TamanhoConteudo = rs.Fields(NomeCamposBD).ActualSize
        gFieldObjectProperties(Ind).TamanhoCampo = rs.Fields(NomeCamposBD).DefinedSize
        gFieldObjectProperties(Ind).nome = rs.Fields(NomeCamposBD).Name
    End If
    
    Exit Function

TrataErro:

    If Err.Number <> 3021 Then
        BG_LogFile_Erros "BGAux_DaFieldObjectProperties_ADO_SEC: " & Err & "." & Err.Description & " - " & _
                                                             NomeTabela & " - " & NomeCampoBD
    End If
    
    Resume Next

End Function



Sub BGAux_DefTipoCampoEc_ADO(ByVal NomeCampoBD As String, ByVal NomeControl As Control, TipoCampo As Integer, ObjectPropertiesIndex As Integer, Optional DecimalLength As Variant, Optional DecimalScale As Variant)
    
    Dim nTipo As Integer
    Dim nPrecision As Integer
    Dim nNumericScale As Integer
    
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        ' Continua a defini��o do tipo de campo.
    ElseIf TypeOf NomeControl Is ComboBox Then
        Exit Sub
    ElseIf TypeOf NomeControl Is CheckBox Then
        Exit Sub
    Else
        Exit Sub
    End If

    If NomeControl.Tag <> "" Then Exit Sub  ' Por causa de 'BG_DefTipoCampoEc_Todos_ADO
    
    If TypeOf NomeControl Is MaskEdBox Then
        NomeControl.AllowPrompt = True
        NomeControl.ClipMode = mskExcludeLiterals
        NomeControl.FontUnderline = False
        NomeControl.PromptInclude = False
    
        NomeControl.PromptChar = "_"
        NomeControl.AutoTab = False
    End If
    
    If gFieldObjectProperties(ObjectPropertiesIndex).tipo = "" Then gFieldObjectProperties(ObjectPropertiesIndex).tipo = "-1"
    nTipo = gFieldObjectProperties(ObjectPropertiesIndex).tipo
    
    If TipoCampo = mediTipoDefeito Then
        NomeControl.Tag = nTipo
    Else
        NomeControl.Tag = TipoCampo
    End If
    
    Select Case TipoCampo
    Case mediTipoDefeito
        Select Case nTipo
            ' Boolean: Tratado por defeito
            Case adBoolean
            ' Binarios: Tratado por defeito
            Case adBinary, adLongVarBinary, adVarBinary
            ' Currency: N�o tratado
            Case adCurrency
            ' Large integers: a precisao pode ir at� 38 digitos
            '          logo limito a 37 (n� digitos da precis�o -1)
            Case adBigInt, adUnsignedBigInt, adVarNumeric, adSingle
                NomeControl.MaxLength = 37
            ' Integer: a precisao pode ir at� +/-2147483647,
            '          logo limito a 10 (n� digitos da precis�o -1)
            Case adInteger, adUnsignedInt
                NomeControl.MaxLength = 10
            ' Small integers: a precisao pode ir at� +/-32767
            '          logo limito a 4 (n� digitos da precis�o -1)
            Case adSmallInt, adTinyInt, adUnsignedSmallInt, adUnsignedTinyInt
                NomeControl.MaxLength = 4
            ' Decimal: define pelos parametros da funcao ou pelas
            '          defini�oes da BD
            Case adDecimal, adDouble, adNumeric
                If (IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Or (Not IsMissing(DecimalLength) And IsMissing(DecimalScale)) Then
                    BG_Mensagem mediMsgBox, "Falta par�metro 'DecimalLength' ou 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
                ElseIf (Not IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Then
                    NomeControl.Tag = NomeControl.Tag & "(" & CInt(DecimalLength) & "," & CInt(DecimalScale) & ")"
                    NomeControl.MaxLength = DecimalLength + 1
                    If DecimalLength <= DecimalScale Then
                        BG_Mensagem mediMsgBox, "'DecimalLength' ter� de ser maior do que 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
                    End If
                Else
                    nPrecision = gFieldObjectProperties(ObjectPropertiesIndex).Precisao
                    nNumericScale = gFieldObjectProperties(ObjectPropertiesIndex).EscalaNumerica
                    NomeControl.Tag = NomeControl.Tag & "(" & CInt(nPrecision) & "," & CInt(nNumericScale) & ")"
                    NomeControl.MaxLength = nPrecision + nNumericScale + 1
                End If
            ' Data: Tratado por defeito
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                BG_Mensagem mediMsgBox, "Em vez de utilizar 'mediTipoDefeito' para o campo Data ou Hora, utilizar 'mediTipoData' ou 'mediTipoHora', consoante o caso!", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
            ' Strings: Usa a defenicao na BD
            Case adBSTR, adChar, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                If gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo <= 65535 Then
                    NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
                Else
                    NomeControl.MaxLength = 65535
                End If
            ' Outro!? N�o tratado.
            Case Else
            End Select
    
        Case mediTipoMaiusculas
            If nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case mediTipoMinusculas
            If nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case mediTipoCapitalizado
            If nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case mediTipoData
            If nTipo = adDate Or nTipo = adDBDate Or nTipo = adDBTimeStamp Then
                ' Tratado por defeito
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case mediTipoHora
            If nTipo = adDBTime Or nTipo = adDBTimeStamp Then
                ' Tratado por defeito
            ElseIf nTipo = adDBTimeStamp Or nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                ' para o caso de se guardar na BD a hora em strings
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case Else
            ' N�o tratado!
    
    End Select

End Sub

Sub BG_DefTipoCampoEc_ADO(NomeTabela As String, _
                          ByVal NomeCampoBD As String, _
                          ByVal NomeControl As Control, _
                          TipoCampo As Integer, _
                          Optional DecimalLength As Variant, _
                          Optional DecimalScale As Variant)

    ' Trata controlos dum form de forma a que os formatos e tamanhos
    ' sejam compativeis com os campos da BD.
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If

    BGAux_DaFieldObjectProperties_ADO NomeTabela, NomeCampoBD
    
    If Not IsMissing(DecimalLength) And Not IsMissing(DecimalScale) Then
        BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0, DecimalLength, DecimalScale
    ElseIf Not IsMissing(DecimalLength) Then
        BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0, DecimalLength
    ElseIf Not IsMissing(DecimalScale) Then
        BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0, , DecimalScale
    Else
        BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0
    End If
    
End Sub

Sub BG_DefTipoCampoEc_Todos_ADO(NomeTabela As String, _
                                CamposBD, _
                                CamposEc, _
                                TipoCampo As Integer)

    ' Trata todos controlos dum form

    Dim TamArray As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
    
    TamArray = UBound(CamposEc) - LBound(CamposEc) + 1
    BGAux_DaFieldObjectProperties_ADO NomeTabela, CamposBD

    For i = 0 To TamArray - 1
        BGAux_DefTipoCampoEc_ADO CamposBD(i), CamposEc(i), TipoCampo, i
    Next i

    Exit Sub
    
TrataErro:
    BG_TrataErro "BG_DefTipoCampoEc_Todos_ADO ('" & CamposEc(i).Name & "')", Err
    Resume Next

End Sub


Sub BG_PreencheCampoEc_Todos_ADO(NomeRecordset As ADODB.recordset, CamposBD, CamposEc)
' preenche todos os controlos com o conteudo dos campos da BD

    Dim TamArray As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    TamArray = UBound(CamposEc) - LBound(CamposEc) + 1

    For i = 0 To TamArray - 1
        BG_PreencheCampoEc_ADO NomeRecordset, CamposBD(i), CamposEc(i)
    Next i
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        If gFormActivo.Name = "FormReqPedElectr" Then
            Exit Sub
        End If
        If gFormActivo.Name = "FormPesquisaUtentes" Or gFormActivo.Name = "FormIdUtentePesq" Then
            responseJson = BL_TrataCamposRGPD(CamposEc)
        ElseIf gFormActivo.Name <> "FormIdentificaUtente" Then
            responseJson = BL_TrataCamposRGPD(CamposEc)
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        Else
            contextJson = requestJson
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, contextJson, IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        End If
    End If

    Exit Sub
    
TrataErro:
    BG_TrataErro "BG_PreencheCampoEc_Todos_ADO('" & CamposEc(i).Name & "')", Err
    Resume Next

End Sub

Function BG_PreencheCampoEc_ADO(NomeRecordset As ADODB.recordset, ByVal NomeCampoBD As String, ByVal NomeControl As Control) As Integer
' preenche um controlo com o conteudo dum campo da BD

    On Error GoTo TrataErro
    
    'TextBox, Label, MaskEdBox, RichTextBox
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl = ""
        Else
            NomeControl = RTrim(NomeRecordset(NomeCampoBD))
            
            If left(NomeControl.Tag, 2) = adDecimal Or left(NomeControl.Tag, 1) = adDouble Or left(NomeControl.Tag, 3) = adNumeric Then
                NomeControl = BG_CvDecimalParaDisplay_ADO(NomeControl)
            ElseIf NomeControl.Tag = mediTipoData Then
                NomeControl = NomeControl
            ElseIf NomeControl.Tag = mediTipoHora Then
                NomeControl = BG_CvHora(NomeControl)
            End If
        End If
    'ComboBox
    ElseIf TypeOf NomeControl Is ComboBox Then
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl.ListIndex = -1
        Else
            BG_MostraComboSel NomeRecordset(NomeCampoBD), NomeControl
        End If
    'DataCombo
    ElseIf TypeOf NomeControl Is DataCombo Then
            If IsNull(NomeRecordset(NomeCampoBD)) Then
                NomeControl.Index = -1
            Else
                BG_MostraDataComboSel NomeRecordset(NomeCampoBD), NomeControl
            End If
    'CheckBox
    ElseIf TypeOf NomeControl Is CheckBox Then
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl.value = vbGrayed
        Else
            NomeControl.value = NomeRecordset(NomeCampoBD)
        End If
    ' Nada
    Else
    End If
    
    BG_PreencheCampoEc_ADO = 0
    Exit Function

TrataErro:
    BG_PreencheCampoEc_ADO = -1
    Exit Function

End Function

Function BG_ExecutaQuery_ADO(SQLQuery As String, Optional iMensagem) As Long

'   O argumento 'iMensagem' serve para que seja apresentada uma mensagem caso a
'   opera��o que se pretende n�o seja efectuada.
'
'   Hip�teses:
'       <Missing>     -> Aparece uma MsgBox
'       mediMsgBox    -> Aparece uma MsgBox
'       mediMsgStatus -> Aparece uma MsgStatus
'       -1            -> N�o aparece mensagem

    Dim sMsg As String
    Dim iRes As Long
    Dim registos As Long
    
    gSQLError = 0
    gSQLISAM = 0
    On Error GoTo TrataErro
    If gModoDebug = 1 Then BG_LogFile_Erros (SQLQuery)
    If SQLQuery = "" Then
        BG_ExecutaQuery_ADO = -1
        Exit Function
    End If
    gConexao.Execute SQLQuery, registos, adCmdText + adExecuteNoRecords
        
    If (registos < 0) Then
        iRes = -1
        If gModoDebug = 1 Then BG_LogFile_Erros "(BG_ExecutaQuery: RecordsAffected = 0) -> " & SQLQuery
        sMsg = "Esta opera��o pode n�o ter sido efectuada." & vbCrLf & vbCrLf & "Raz�o prov�vel: Integridade dos Dados."
        If IsMissing(iMensagem) Then
            'If gModoDebug = 1 Then BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
        Else
            If iMensagem = mediMsgBox Then
                'If gModoDebug = 1 Then BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
            ElseIf iMensagem = mediMsgStatus Then
                If gModoDebug = 1 Then BG_Mensagem mediMsgStatus, sMsg, mediMsgBeep + 10
            ElseIf iMensagem = -1 Then
                ' N�o d� mensagem.
            End If
        End If
    Else
        iRes = registos
        
    End If
    
    BG_ExecutaQuery_ADO = iRes
    Exit Function

TrataErro:
BG_TrataErro vbCrLf & "Erro : BG_ExecutaQuery_ADO" & vbNewLine & "(" & SQLQuery & ")" & vbNewLine & vbCrLf, Err
    If IsMissing(iMensagem) Then
        BG_Trata_BDErro
    Else
        If iMensagem <> -1 Then BG_Trata_BDErro
    End If
    BG_ExecutaQuery_ADO = -1
    Resume Next
    Exit Function

End Function

Function BG_ConstroiCriterio_INSERT_ADO(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant) As String

' Constroi um statment SQL para INSERT baseado nos campos do Form,
' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)
    
    Dim Criterio As String
    Dim CriterioAux As String
    Dim ValorCampo As Variant
    Dim ValorControl As Variant
    Dim i As Integer
    Dim nPassou As Integer
    Dim nTipo As Integer
    Dim pos As Integer

    i = 0
    nPassou = 0
        
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
        
    'criar a instru�ao de INSERT
    Criterio = "INSERT INTO " & NomeTabela & " ("
    CriterioAux = ") VALUES ("
    
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = BG_DataComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'controlar o primeiro campo para nao levar virgula
        If nPassou <> 0 Then
            Criterio = Criterio & ", "
            CriterioAux = CriterioAux & ", "
        End If
        
        Criterio = Criterio & ValorCampo
        
        'se o campo do form tem conteudo formata o conteudo para o statment SQL
        If ValorControl <> "" Then
            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = mediTipoCapitalizado Or nTipo = mediTipoData _
                   Or nTipo = mediTipoDefeito Or nTipo = mediTipoHora _
                   Or nTipo = mediTipoMaiusculas Or nTipo = mediTipoMaiusculas _
                   Or nTipo = mediTipoMinusculas Then
                    BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            
            End If

            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            If nTipo = adDecimal Or nTipo = adNumeric Or nTipo = adSingle Or nTipo = adVarNumeric Or nTipo = adDouble Or nTipo = adInteger Then
                CriterioAux = CriterioAux & BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            ElseIf nTipo = adDate Or nTipo = adDBDate Or nTipo = adDBTimeStamp Or nTipo = adDBTime Then
                If NomesControl(i).Tag = CStr(mediTipoHora) Then
                    'hora
                    CriterioAux = CriterioAux & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    CriterioAux = CriterioAux & "'" & BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'outro
            Else
                If ValorControl <> " " Then ValorControl = RTrim(ValorControl)
                CriterioAux = CriterioAux & "'" & ValorControl & "'"
            End If
        'se o campo do form esta vazio, poe null
        Else
            CriterioAux = CriterioAux & "null"
        End If
        
        nPassou = 1
        i = i + 1
    Next
    Criterio = Criterio & CriterioAux & ")"

    'Mensagens
'    BG_LogFile_Erros "BG_ConstroiCriterio_INSERT_ADO:"
'    BG_LogFile_Erros "    " & Criterio

    'para debug:
    'BG_Mensagem mediMsgBox, Criterio, , "BG_ConstroiCriterio_INSERT_ADO"

    ' Retorno de resultados
    BG_ConstroiCriterio_INSERT_ADO = Criterio
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        requestJson = BL_TrataCamposRGPD(NomesControl)
        responseJson = vbNullString
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Insercao) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
    End If

End Function
Function BG_ConstroiCriterio_INSERT_ADO_SEC(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant) As String

' Constroi um statment SQL para INSERT baseado nos campos do Form,
' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)
    
    Dim Criterio As String
    Dim CriterioAux As String
    Dim ValorCampo As Variant
    Dim ValorControl As Variant
    Dim i As Integer
    Dim nPassou As Integer
    Dim nTipo As Integer
    Dim pos As Integer

    i = 0
    nPassou = 0
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
        
    'criar a instru�ao de INSERT
    Criterio = "INSERT INTO " & NomeTabela & " ("
    CriterioAux = ") VALUES ("
    
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = BG_DataComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'controlar o primeiro campo para nao levar virgula
        If nPassou <> 0 Then
            Criterio = Criterio & ", "
            CriterioAux = CriterioAux & ", "
        End If
        
        Criterio = Criterio & ValorCampo
        
        'se o campo do form tem conteudo formata o conteudo para o statment SQL
        If ValorControl <> "" Then
            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                BGAux_DaFieldObjectProperties_ADO_SEC NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = mediTipoCapitalizado Or nTipo = mediTipoData _
                   Or nTipo = mediTipoDefeito Or nTipo = mediTipoHora _
                   Or nTipo = mediTipoMaiusculas Or nTipo = mediTipoMaiusculas _
                   Or nTipo = mediTipoMinusculas Then
                    BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            
            End If

            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            If nTipo = adDecimal Or nTipo = adNumeric Or nTipo = adSingle Or nTipo = adVarNumeric Or nTipo = adDouble Or nTipo = adInteger Then
                CriterioAux = CriterioAux & BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            ElseIf nTipo = adDate Or nTipo = adDBDate Or nTipo = adDBTimeStamp Or nTipo = adDBTime Then
                If NomesControl(i).Tag = CStr(mediTipoHora) Then
                    'hora
                    CriterioAux = CriterioAux & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    CriterioAux = CriterioAux & "'" & BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'outro
            Else
                If ValorControl <> " " Then ValorControl = RTrim(ValorControl)
                CriterioAux = CriterioAux & "'" & ValorControl & "'"
            End If
        'se o campo do form esta vazio, poe null
        Else
            CriterioAux = CriterioAux & "null"
        End If
        
        nPassou = 1
        i = i + 1
    Next
    Criterio = Criterio & CriterioAux & ")"

    'Mensagens
'    BG_LogFile_Erros "BG_ConstroiCriterio_INSERT_ADO:"
'    BG_LogFile_Erros "    " & Criterio

    'para debug:
    'BG_Mensagem mediMsgBox, Criterio, , "BG_ConstroiCriterio_INSERT_ADO"

    ' Retorno de resultados
    BG_ConstroiCriterio_INSERT_ADO_SEC = Criterio

End Function

Public Sub BG_PreencheListBoxMultipla_ADO(NomeControl As Object, _
                                          NomeRecordset As ADODB.recordset, _
                                          CamposRequeridos, _
                                          NumEspacos, _
                                          CamposBD, _
                                          CamposEc, _
                                          Localizacao As String, _
                                          Optional ValoresCombo As Variant)

' Preenche uma listbox com varios campos da BD

    Dim NomeCampoBD As Variant
    Dim Marca As Variant
    Dim sLinha As String
    Dim bValorNaCombo As Boolean
    Dim sValorParaMostrar As String
    Dim i As Long
    Dim j, k, l, m As Integer
    Dim iAux As Integer
    Dim CampoAux As Variant
    Dim bEncontrou As Boolean
    Dim lPosicao, lNumTotal As Long

    On Error GoTo TrataErro

    lPosicao = NomeControl.ListIndex
    lNumTotal = NomeControl.ListCount
    
    NomeControl.Clear
    Marca = NomeRecordset.Bookmark
    
    i = 0
    Do Until NomeRecordset.EOF
        sLinha = ""
        j = 0
        For Each NomeCampoBD In CamposRequeridos
            If (IsNull(NomeRecordset(NomeCampoBD))) Or (NomeRecordset(NomeCampoBD) = "") Then
                sLinha = sLinha & String(NumEspacos(j), " ")
            Else
                bValorNaCombo = False

                If Not IsMissing(ValoresCombo) Then
                    'If ValoresCombo = mediOff Then GoTo IGNORA_COMBO
                    If ValoresCombo = 0 Then GoTo IGNORA_COMBO
                End If

                ' In�cio: Trata Campos do tipo ComboBox e DataCombo
                k = 0
                bEncontrou = False
                For Each CampoAux In CamposBD
                    If NomeCampoBD = CampoAux Then
                        bEncontrou = True
                        Exit For
                    End If
                    k = k + 1
                Next
                
                If bEncontrou = True Then
                    'ComboBox
                    If TypeOf CamposEc(k) Is ComboBox Then
                        iAux = CInt(NomeRecordset(NomeCampoBD))
                        For l = 0 To CamposEc(k).ListCount - 1
                            If iAux = CamposEc(k).ItemData(l) Then
                                bValorNaCombo = True
                                sValorParaMostrar = CamposEc(k).List(l)
                                Exit For
                            End If
                        Next
                    'DataCombo
                    ElseIf TypeOf CamposEc(k) Is DataCombo Then
                        iAux = CInt(NomeRecordset(NomeCampoBD))
                        If NomeControl.Index = -1 Then
                            bValorNaCombo = False
                        ElseIf iAux = CamposEc(k).ItemData(CamposEc(k).ListIndex) Then
                            bValorNaCombo = True
                            sValorParaMostrar = NomeRecordset!CamposEc(k).ListField(ListIndex)
                        End If
                     End If
                End If
                
                ' Fim: Trata Campos do tipo ComboBox e DataCombo
IGNORA_COMBO:
                If bValorNaCombo = False Then
                    sValorParaMostrar = NomeRecordset(NomeCampoBD)

                    ' In�cio: Trata convers�es para Decimal, Data e Hora
                    m = 0
                    For Each CampoAux In CamposBD
                        If NomeCampoBD = CampoAux Then

                            ' adDouble, adDecimal
                            If left(NomeControl.Tag, 2) = adDecimal Or left(NomeControl.Tag, 1) = adDouble Or left(NomeControl.Tag, 3) = adNumeric Then
                                sValorParaMostrar = BG_CvDecimalParaDisplay_ADO(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoData Then
                                sValorParaMostrar = BG_CvDataParaDisplay_ADO(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoHora Then
                                sValorParaMostrar = BG_CvHora(sValorParaMostrar)
                            End If
                            
                            Exit For
                        End If
                        m = m + 1
                    Next
                    ' Fim: Trata convers�es para Decimal, Data e Hora
                End If

                sLinha = sLinha & left(sValorParaMostrar, NumEspacos(j))
                If Len(sValorParaMostrar) < NumEspacos(j) Then
                    sLinha = sLinha & String(NumEspacos(j) - Len(sValorParaMostrar), " ")
                End If
            End If
            sLinha = sLinha & " "
            j = j + 1
        Next
        NomeControl.AddItem sLinha
        NomeRecordset.MoveNext
        i = i + 1
        If i > 32767 Then
            BG_Mensagem mediMsgBox, "N�o � poss�vel mostrar nesta lista mais do que " & i - 1 & " registos!", vbExclamation
            Exit Do
        End If
    Loop
    
    If Localizacao = "SELECT" Then
        NomeControl.ListIndex = 0
    ElseIf Localizacao = "UPDATE" Then
        NomeControl.ListIndex = lPosicao
    ElseIf Localizacao = "DELETE" Then
        If lNumTotal = lPosicao + 1 Then
            NomeControl.ListIndex = lPosicao - 1
        Else
            NomeControl.ListIndex = lPosicao
        End If
    End If
    
    If Localizacao <> "SELECT" And Localizacao <> "UPDATE" And Localizacao <> "DELETE" Then
        NomeControl.ListIndex = 0
        BG_Mensagem mediMsgBox, "BG_PreencheListBoxMultipla: Necess�rio indicar SELECT, UPDATE ou DELETE"
    End If
    
    NomeRecordset.Bookmark = Marca
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        If gFormActivo.Name = "FormPesquisaUtentes" Or gFormActivo.Name = "FormIdUtentePesq" Then
            responseJson = BL_TrataListBoxRGPD(CamposRequeridos, NomeRecordset, True)
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        Else
            responseJson = BL_TrataListBoxRGPD(CamposRequeridos, NomeRecordset, True)
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        End If
    End If
    Exit Sub
    
TrataErro:
    BG_Mensagem mediMsgBox, "Problema na rotina 'BG_PreencheListBoxMultipla'" & vbCrLf & vbCrLf & Err & " - " & Error(Err), vbExclamation, "BG " & bgVersao
    Err.Clear
    Resume Next

End Sub



Sub BG_MostraDataComboSel(ByVal ValorCampoBD As Variant, NomeControl As Control)
    
    Dim i As Integer
    Dim Seleccionado As Integer
        
    'Activa um elemento duma ComboBox onde o seu ItemData for igual ao ValorCampoBD
    
    If IsNull(ValorCampoBD) Or Not IsNumeric(ValorCampoBD) Then
        NomeControl.Index = -1
        Exit Sub
    End If
    
    i = 0
    Seleccionado = False
    While (Seleccionado = False) And (i < NomeControl.VisibleCount)
        If NomeControl.ItemData(i) = CLng(ValorCampoBD) Then
            NomeControl.Index = i
            Seleccionado = True
        End If
        i = i + 1
    Wend

    If Seleccionado = False Then
        NomeControl.Index = -1
    End If
    
End Sub

Function BG_DataComboSel(ByVal NomeControl As Control) As Long
    
    ' Retorna o ItemData do elemento seleccionado numa ComboBox

    If NomeControl.Index = -1 Then
        BG_DataComboSel = mediComboValorNull
    Else
        BG_DataComboSel = NomeControl.ItemData(NomeControl.ListIndex)
    End If

End Function

Sub BG_PreencheComboBD_ADO(NomeTabelaBD As String, _
                           NomeCampoBD_codigo As String, _
                           NomeCampoBD_Descr As String, _
                           NomeControl As Control, _
                           Optional TipoOrdenacao1 As Variant, _
                           Optional TipoOrdenacao2 As Variant)

    ' Preenche uma ComboBox com base numa query

    Dim obTabelaAux As ADODB.recordset
    Dim CriterioAux As String
    Dim CriterioInicial As String
    Dim iTabelaOuQuery As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    
    'Inicio: Verificar se trata sobre Tabela ou sobre Query
    iTabelaOuQuery = InStr(StrConv(NomeTabelaBD, vbUpperCase), "SELECT ")
    If iTabelaOuQuery = 1 Then
        CriterioInicial = NomeTabelaBD
    Else
        CriterioInicial = "SELECT " & NomeCampoBD_codigo & ", " & NomeCampoBD_Descr & " FROM " & NomeTabelaBD
    End If
    'Fim: Verificar se trata sobre Tabela ou sobre Query
        
    'Inicio: Crit�rio de Ordena��o
    If IsMissing(TipoOrdenacao1) Then
        CriterioAux = CriterioInicial
    Else
        If TipoOrdenacao1 = mediAscComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " DESC"
        ElseIf TipoOrdenacao1 = mediAscComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " DESC"
        End If
        
        If Not IsMissing(TipoOrdenacao2) Then
            If TipoOrdenacao2 = mediAscComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " DESC"
            ElseIf TipoOrdenacao2 = mediAscComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " DESC"
            End If
        End If
    End If
    'Fim: Crit�rio de Ordena��o

    Set obTabelaAux = New ADODB.recordset
    'obTabelaAux.Open CriterioAux, gConexao, adOpenDynamic, adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioAux
    obTabelaAux.Open CriterioAux, gConexao, adOpenStatic, adLockReadOnly

    NomeControl.Clear

    i = 0
    Do Until obTabelaAux.EOF
        NomeControl.AddItem RTrim(obTabelaAux(NomeCampoBD_Descr))
        NomeControl.ItemData(i) = CLng(obTabelaAux(NomeCampoBD_codigo))
        obTabelaAux.MoveNext
        i = i + 1
    Loop

    obTabelaAux.Close
    
    Exit Sub

TrataErro:
    If Err = 6 Then
        NomeControl.List(i) = "!!Sup. 32767: " & NomeControl.List(i)
        NomeControl.ItemData(i) = -10
        BG_TrataErro "BG_PreencheComboBD", Err, "'" & NomeTabelaBD & "': Valor Superior a 32767!"
        Resume Next
    Else
        BG_TrataErro "BG_PreencheComboBD " & NomeTabelaBD & ": ", Err
        Beep
        BG_Mensagem mediMsgStatus, "Erro a preencher " & NomeControl.Name & "!", vbCritical + vbOKOnly, "ERRO"
    End If

End Sub

Public Function BG_CvWilcard(ByVal sArg1 As String) As String

    ' Converter os '*' e '?' em '%' e '_' respectivamente
    ' Para manter compatibilidade entre SGBDs passa a usar-se sempre o LIKE (n�o o MATCHES)
    ' Isto permite o utilizador continuar a usar os WillCards a que est� habituado

    Dim texto As String
    Dim tam As Integer
    Dim pos As Integer

    tam = Len(sArg1)
    texto = ""
    pos = 1
    While pos <> tam + 1
        If Mid(sArg1, pos, 1) = "*" Then
            texto = texto + "%"
        ElseIf Mid(sArg1, pos, 1) = "?" Then
            texto = texto + "_"
        Else
            texto = texto + Mid(sArg1, pos, 1)
        End If
        pos = pos + 1
    Wend
    
    BG_CvWilcard = texto
    
End Function

Public Function BG_DaMAX(NomeTabela As String, NomeCampo As String) As Long

    Dim rsMax As ADODB.recordset
    Dim SQLQuery As String
    Dim retorno As Integer
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
    
    If gSGBD = gOracle Then
        SQLQuery = "SELECT MAX(" & NomeCampo & ") AS maximo FROM " & NomeTabela
    ElseIf gSGBD = gSqlServer Then
        SQLQuery = "SELECT MAX(convert(numeric," & NomeCampo & ")) AS maximo FROM " & NomeTabela
    ElseIf gSGBD = gPostGres Then
        SQLQuery = "SELECT MAX(" & NomeCampo & ") AS maximo FROM " & NomeTabela
    End If
    
    Set rsMax = New ADODB.recordset
    
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLQuery
    rsMax.Open SQLQuery, gConexao, adOpenStatic, adLockReadOnly
    
    ' Retorno de resultados
    If IsNull(rsMax!maximo) Then
        BG_DaMAX = 0
    Else
        BG_DaMAX = rsMax!maximo
    End If
    
    'fecha o recordset
    rsMax.Close
    Set rsMax = Nothing

End Function

Public Function BG_DaMAX_Secund�ria(NomeTabela As String, NomeCampo As String) As Long

    Dim rsMax As ADODB.recordset
    Dim SQLQuery As String
    Dim retorno As Integer
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        If UCase(NomeTabela) = UCase("slv_identif") Then
            NomeTabela = "sl_identif"
        End If
    End If
    
    If gSGBD = gOracle Then
        SQLQuery = "SELECT MAX(" & NomeCampo & ") AS maximo FROM " & NomeTabela
    Else
        SQLQuery = "SELECT MAX(convert(numeric," & NomeCampo & ")) AS maximo FROM " & NomeTabela
    End If
    
    Set rsMax = New ADODB.recordset
    
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLQuery
    rsMax.Open SQLQuery, gConexaoSecundaria, adOpenStatic, adLockReadOnly
    
    ' Retorno de resultados
    If IsNull(rsMax!maximo) Then
        BG_DaMAX_Secund�ria = 0
    Else
        BG_DaMAX_Secund�ria = rsMax!maximo
    End If
    
    'fecha o recordset
    rsMax.Close
    Set rsMax = Nothing

End Function

Public Sub BG_ParametrizaPermissoes_ADO(nomeForm As String)

    ' Parametriza os objectos dum form conforme
    ' as defeni�oes da tabela de parametriza�oes de acessos

    Dim rs As ADODB.recordset
    Dim sql As String
    Dim NomeControlo As String
    Dim indice As Integer
    Dim NomePropriedade As String
    Dim ValorPropriedade As String
    Dim PropBag As PropertyBag
        
    On Error GoTo Trata_Erro
    
    gCmdParamAcessos.Parameters(0).value = gCodGrupo
    gCmdParamAcessos.Parameters(1).value = gCodUtilizador
    gCmdParamAcessos.Parameters(2).value = UCase(nomeForm)
    Set rs = gCmdParamAcessos.Execute
    
    If Not rs.EOF Then
        With rs
            Do While Not .EOF
                NomeControlo = !nome_obj
                indice = BL_HandleNull(!indice, -1)
                NomePropriedade = !descr_prop
                ValorPropriedade = !val_prop
                BL_PoePermissao_ADO Trim(nomeForm), Trim(NomeControlo), Trim(NomePropriedade), Trim(ValorPropriedade), indice
                .MoveNext
            Loop
        End With
    End If
    
    rs.Close
    Set rs = Nothing
    
    BL_AplicaProfile2 gProfileUtilizador, nomeForm
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        If nomeForm <> MDIFormInicio.Name Then
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.acesso_form) & " - " & nomeForm, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", "{}", "{}"
        End If
    End If
    
Exit Sub
Trata_Erro:
BG_LogFile_Erros "Erro na BG_ParametrizaPermissoes_ADO :" & Err.Number & "/" & Err.Description
    
End Sub

Public Function BG_CvDataParaWhere_ADO(ByVal sArg1 As String) As String

    ' Altera o formato da data da aplica��o para o formato da base de dados
    
    If Trim(sArg1) <> "" Then
        BG_CvDataParaWhere_ADO = Format(CDate(sArg1), cFormatoDataBD)
    Else
        BG_CvDataParaWhere_ADO = ""
    End If
    
End Function

Public Sub BG_LimpaOpcao(ByRef NomeControlo As Control, KeyCode As Integer)

    If KeyCode = 46 Then
        If TypeOf NomeControlo Is ComboBox Then
            NomeControlo.ListIndex = -1
        ElseIf TypeOf NomeControlo Is OptionButton Then
            NomeControlo.value = 0
        ElseIf TypeOf NomeControlo Is CheckBox Then
            NomeControlo.value = vbGrayed
        End If
    End If

End Sub

Public Function BG_CvDecimalParaWhere_ADO(ByVal sArg1 As String) As String

    ' Converte o simbolo decimal utilizado pela aplica��o para o da BD
    
    Dim iTemp As Integer
    Dim sTemp As String
    
    If Trim(sArg1) = "" Then
        sTemp = sArg1
    Else
        iTemp = InStr(sArg1, gSimboloDecimal)
        If iTemp <> 0 Then
            sTemp = Mid(sArg1, 1, iTemp - 1) & cFormatoDecBD & Mid(sArg1, iTemp + 1)
        Else
            sTemp = sArg1
        End If
    End If

    BG_CvDecimalParaWhere_ADO = sTemp

End Function

Public Function BG_CvDecimalParaDisplay_ADO(ByVal sArg1 As String) As String

    ' Converte o simbolo decimal da BD para o utilizado pela aplica��o
    ' N�o � utilizado pois o WINDOWS/ODBC faz autom�ticamente a convers�o
    ' Mas caso venha a ser necess�rio...

    Dim iPos As Integer
    Dim sTemp As String
    
    iPos = InStr(sArg1, cFormatoDecBD)
    If iPos <> 0 Then
        sTemp = Mid(sArg1, 1, iPos - 1) & gSimboloDecimal & Mid(sArg1, iPos + 1)
    Else
        sTemp = sArg1
    End If

    BG_CvDecimalParaDisplay_ADO = sTemp
    
End Function

Public Function BG_CvDataParaDisplay_ADO(ByVal sArg1 As String) As String
        
    'Para garantir que n�o teremos problemas com a convers�o da data da base de dados
    'para a do sistema local utilizando a fun��o FORMAT (troca de dias com meses p.e.),
    'esta rotina faz o tratamento de strings necess�rio para a convers�o.
            
    Dim dia As String
    Dim mes As String
    Dim ano As String
    Dim Sepdata As String
    Dim Orddata As String
    Dim pos As Integer
    Dim tam As Integer
    Dim i As Integer
    Dim s As String
    Dim Formato As String
    
    'Calcula o tamanho da Data sem os espa�os
    
    s = Trim(sArg1)
    tam = Len(s)
    
    'EXTRAI O DIA, M�S E ANO DA DATA NA BD

    Formato = cFormatoDataBD

    'Ano no principio?
    If UCase(Mid(Formato, 1, 1)) = "Y" Then
        pos = 1
        While InStr(1, "0123456789", Mid(s, pos, 1)) <> 0
            pos = pos + 1
        Wend
        ano = Mid(s, 1, pos - 1)
        s = Trim(Mid(s, pos + 1))
    Else
        'Ano no fim
        pos = 1
        While InStr(1, "0123456789", Mid(s, tam - pos + 1, 1)) <> 0
            pos = pos + 1
        Wend
        ano = Right(s, pos - 1)
        s = Mid(s, 1, pos)
    End If
    
    
    'Calcula a posi��o do caracter n�o num�rico =>Separador da data
    pos = 1
    While InStr(1, "0123456789", Mid(s, pos, 1)) <> 0
        pos = pos + 1
    Wend
    
    If InStr(1, UCase(Formato), "D") < InStr(1, UCase(Formato), "M") Then
        'Dias antes do m�s
        dia = Mid(s, 1, pos - 1)
        mes = Mid(s, pos + 1)
    Else
        'Dias depois do m�s
        mes = Mid(s, 1, pos - 1)
        dia = Mid(s, pos + 1)
    End If
    
    'ADAPTA A DATA AO FORMATO DO CONTROLPANEL
    'Obt�m o separador para as datas
    Sepdata = DefLocal.DatasSeparador
    'Obt�m a ordem das datas
    Orddata = DefLocal.DatasOrdem
    
    Select Case Orddata
       Case 0
          BG_CvDataParaDisplay_ADO = mes & Sepdata & dia & Sepdata & ano
       Case 1
          BG_CvDataParaDisplay_ADO = dia & Sepdata & mes & Sepdata & ano
       Case 2
          BG_CvDataParaDisplay_ADO = ano & Sepdata & mes & Sepdata & dia
    End Select
   
End Function

Function BG_ValidaTipoCampo_ADO(nomeForm As Form, NomeControl As Control) As Boolean

    Dim nTipo, i As Integer
    Dim sTipos As Variant
    Dim Var1, Var2 As Variant
    Dim nCorrecto As Boolean
    Dim DecimalLength, DecimalScale As Integer
    Dim pos, pos1 As Integer
    Dim s1, s2 As String
    Dim Decimal_tag As String
    Dim ErroDecimal As Integer
    Dim ErroMsgDecimal As String

    Dim iComp As Integer
    Dim PrimeiraLetra, TemEspaco As Boolean

    '--------------------------------------
    
    On Error GoTo TrataErro
    
    nCorrecto = True
    sTipos = Array(adBoolean, "Booleano", adUnsignedTinyInt, "Byte (0..255)", adSmallInt, "Inteiro", _
            adUnsignedSmallInt, "Inteiro", adInteger, "Inteiro", adUnsignedInt, "Inteiro", adBigInt, "Longo Inteiro", _
            adUnsignedBigInt, "Longo Inteiro", adVarNumeric, "Longo Inteiro", adSingle, "Longo Inteiro", _
            adCurrency, "Currency", adDecimal, "Precis�o Simples", adDouble, "Precis�o Dupla", _
            adNumeric, "Precis�o Dupla", adDate, "Data (" & gFormatoData & ")", adDBDate, "Data (" & _
            gFormatoData & ")", adDBTimeStamp, "Data (" & gFormatoData & ")", adDBTime, "Hora (" & _
            gFormatoHora & ")", adBSTR, "Texto", adChar, "Texto", adLongVarChar, "Texto", adLongVarWChar, _
            "Texto", adVarChar, "Texto", adVariant, "Texto", adVarWChar, "Texto", adWChar, "Texto", adBinary, _
            "Bin�rio", adLongVarBinary, "Longo Bin�rio", adVarBinary, "Bin�rio", adTinyInt, "Bin�rio", _
            adChapter, "Memo", mediTipoMaiusculas, "Maiusculas", mediTipoMinusculas, "Minusculas", _
            mediTipoCapitalizado, "Capitalizado", mediTipoData, "Data(" & gFormatoData & ")", mediTipoHora, _
            "Hora (" & gFormatoHora & ")")
    
    '--------------------------------------
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        ' Continua o tratamento da valida��o do tipo de campo.
    ElseIf TypeOf NomeControl Is ComboBox Then
        GoTo FimDaValidacao
    ElseIf TypeOf NomeControl Is CheckBox Then
        GoTo FimDaValidacao
    Else
        GoTo FimDaValidacao
    End If
    '--------------------------------------
    
    If NomeControl.Tag = "" Then
        BG_Mensagem mediMsgBox, "N�o foi definido tipo de campo.", vbExclamation, "BG_ValidaTipoCampo_ADO - '" & NomeControl.Name & "'"
        nCorrecto = False
        GoTo FimDaValidacao
    End If
    
    If NomeControl = "" Then GoTo FimDaValidacao ' Campo sem nada n�o precisa de tratamento.
    
    '--------------------------------------
    
    ' In�cio: Tratamento do Campo Decimal, sub-tratar a string que indica tipo decimal.
    DecimalLength = 0 ' Tamanho da parte decimal e fraccion�ria
    DecimalScale = 0 ' Tamanho da parte fraccion�ria
    Decimal_tag = "" ' Utilizado como vari�vel tempor�ria para o tipo decimal

    pos = InStr(NomeControl.Tag, "(")
    If pos <> 0 Then
        s1 = Mid(NomeControl.Tag, pos + 1, 1)
        s2 = Mid(NomeControl.Tag, pos + 2, 1)
        If s2 = "," Then
            DecimalLength = CInt(s1)
        Else
            DecimalLength = CInt(s1 & s2)
        End If

        pos1 = InStr(NomeControl.Tag, ",")
        s1 = Mid(NomeControl.Tag, pos1 + 1, 1)
        s2 = Mid(NomeControl.Tag, pos1 + 2, 1)
        If s2 = ")" Then
            DecimalScale = CInt(s1)
        Else
            DecimalScale = CInt(s1 & s2)
        End If
        
        Decimal_tag = NomeControl.Tag
        NomeControl.Tag = left(NomeControl.Tag, pos - 1)
        
    End If
    ' Fim: Tratamento do Campo Decimal

    '--------------------------------------
    
    If IsNumeric(NomeControl.Tag) Then
        nTipo = CInt(NomeControl.Tag)
        Select Case nTipo
            Case adBoolean
                NomeControl = CBool(NomeControl)
            Case adUnsignedTinyInt
                NomeControl = CByte(NomeControl)
            Case adSmallInt, adUnsignedSmallInt
                NomeControl = CInt(NomeControl)
            Case adInteger, adUnsignedInt
                NomeControl = CLng(NomeControl)
            Case adBigInt, adUnsignedBigInt, adVarNumeric, adSingle
                NomeControl = CSng(NomeControl)
            Case adCurrency
                NomeControl = CCur(NomeControl)
            Case adDecimal
                NomeControl = BG_CvDecimalParaDisplay_ADO(NomeControl)
                GoSub TratarDecimal
            Case adDouble, adNumeric
                NomeControl = BG_CvDecimalParaDisplay_ADO(NomeControl)
                GoSub TratarDecimal
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                ' O tipo deste campo dever� ser mudado para 'mediTipoData', no entanto,
                ' � tratado caso entre aqui.
                NomeControl = BG_CvData(NomeControl)
            Case adBSTR, adChar, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                ' Tratado por defeito
            Case adBinary, adLongVarBinary, adVarBinary, adTinyInt
                ' N�o tratado!
            Case adChapter
                ' Tratado por defeito
            Case mediTipoMaiusculas
                NomeControl = UCase(NomeControl)
            Case mediTipoMinusculas
                NomeControl = LCase(NomeControl)
            Case mediTipoCapitalizado
                s1 = LCase(NomeControl)
                s1 = LTrim(s1)
                s1 = RTrim(s1)
                iComp = Len(s1)
                s2 = ""
                PrimeiraLetra = True
                TemEspaco = False
                For i = 1 To iComp
                    If (Mid(s1, i, 1) <> " ") Then
                        TemEspaco = False
                        If PrimeiraLetra = True Then
                            s2 = s2 & UCase(Mid(s1, i, 1))
                            PrimeiraLetra = False
                        Else
                            s2 = s2 & Mid(s1, i, 1)
                        End If
                    ElseIf (TemEspaco = False) Then
                        PrimeiraLetra = True
                        TemEspaco = True
                        s2 = s2 & Mid(s1, i, 1)
                    End If
                Next i
                NomeControl = s2
            Case mediTipoData
                NomeControl = BG_CvData(NomeControl)
            Case mediTipoHora
                NomeControl = BG_CvHora(NomeControl)
            Case Else
                ' N�o tratado!
        End Select
    Else
        BG_Mensagem mediMsgBox, "Programa��o: O tipo de campo foi alterado.", vbExclamation, "BG_ValidaTipoCampo_ADO - '" & NomeControl.Name & "'"
    End If

    '--------------------------------------
    If Decimal_tag <> "" Then
        NomeControl.Tag = Decimal_tag
        If ErroDecimal <> 0 Then
            Beep
            ErroMsgDecimal = ""
            If gModoDebug = 1 Then ErroMsgDecimal = "Erro Decimal (" & ErroDecimal & ")" & vbCrLf & vbCrLf
            BG_Mensagem mediMsgBox, ErroMsgDecimal & "Este campo dever� ter " & DecimalLength - DecimalScale & " n�mero(s) � esquerda do sinal decimal e " & DecimalScale & " n�mero(s) � direita!" & vbCrLf & vbCrLf & "Exemplo:   " & String(DecimalLength - DecimalScale, "x") & gSimboloDecimal & String(DecimalScale, "x"), vbExclamation, "Aten��o"
            nCorrecto = False
            NomeControl.SetFocus
        End If
    End If
    '--------------------------------------

    GoTo FimDaValidacao
    

FimDaValidacao:
    BG_ValidaTipoCampo_ADO = nCorrecto
    Exit Function
    
TratarDecimal:
    Dim iTemp As Single

    ErroDecimal = 0
    pos = InStr(NomeControl, gSimboloDecimal)
    
    If pos = 0 And Len(NomeControl) > (DecimalLength - DecimalScale) And (DecimalLength - DecimalScale) >= 0 Then ErroDecimal = -1
    If pos <> 0 And pos > (DecimalLength - DecimalScale + 1) And (DecimalLength - DecimalScale + 1) >= 0 Then ErroDecimal = -2
    If pos <> 0 And (Len(NomeControl) - pos) > DecimalScale Then ErroDecimal = -3
    iTemp = CSng(NomeControl)
    
    'A mensagem de erro � mostrada no fim da fun��o.
    Return
    
TrataErro:
    i = 0
    For Each Var1 In sTipos
        If Var1 = nTipo Then Var2 = sTipos(i + 1)
        i = i + 1
    Next
    Beep
    BG_Mensagem mediMsgBox, "O valor atribuido n�o � correcto para este campo !   " & vbCrLf & "Este campo � do tipo '" & Var2 & "'", vbExclamation, "Aten��o"
    If NomeControl.Visible = True Then NomeControl.SetFocus
    nCorrecto = False
    Resume Next

End Function

Sub BG_BeginTransaction()

    ' Gest�o de transac�oes de base de dados:
    ' ( Colocar no inicio do form gEstadoTransacao = False, no inicio da aplica��o,
    '   esta atribui��o � efectuada, e se as fun�oes de controlo de transac�oes forem
    '   bem utilizadas esta inicializacao nao � necess�ria, no entanto, mais vale prevenir... )
    '
    ' Quando se chamar a funcao com uma transaccao aberta, faz-se commit � anterior (grava na BD),
    ' e abre-se uma nova.
     
     If gEstadoTransacao = False Then
        gConexao.BeginTrans
        gEstadoTransacao = True
    ElseIf gEstadoTransacao = True Then
        BG_CommitTransaction
        BG_BeginTransaction
    End If
    
End Sub

Sub BG_RollbackTransaction()

    ' Desfaz as altera��es efectuadas desde a abertura da ultima transac��o
    
    If gEstadoTransacao = True Then
        gConexao.RollbackTrans
        gEstadoTransacao = False
    End If
    
End Sub

Sub BG_CommitTransaction()

    ' Grava as altera��es efectuadas desde a abertura da ultima transac��o
    
    If gEstadoTransacao = True Then
        gConexao.CommitTrans
        gEstadoTransacao = False
    End If
    
End Sub

Sub BG_PreencheComboBDSecundaria_ADO(NomeTabelaBD As String, _
                           NomeCampoBD_codigo As String, _
                           NomeCampoBD_Descr As String, _
                           NomeControl As Control, _
                           Optional TipoOrdenacao1 As Variant, _
                           Optional TipoOrdenacao2 As Variant)

    ' Preenche uma ComboBox com base numa query

    Dim obTabelaAux As ADODB.recordset
    Dim CriterioAux As String
    Dim CriterioInicial As String
    Dim iTabelaOuQuery As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    
    'Inicio: Verificar se trata sobre Tabela ou sobre Query
    iTabelaOuQuery = InStr(StrConv(NomeTabelaBD, vbUpperCase), "SELECT ")
    If iTabelaOuQuery = 1 Then
        CriterioInicial = NomeTabelaBD
    Else
        CriterioInicial = "SELECT " & NomeCampoBD_codigo & ", " & NomeCampoBD_Descr & " FROM " & NomeTabelaBD
    End If
    'Fim: Verificar se trata sobre Tabela ou sobre Query
        
    'Inicio: Crit�rio de Ordena��o
    If IsMissing(TipoOrdenacao1) Then
        CriterioAux = CriterioInicial
    Else
        If TipoOrdenacao1 = mediAscComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " DESC"
        ElseIf TipoOrdenacao1 = mediAscComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " DESC"
        End If
        
        If Not IsMissing(TipoOrdenacao2) Then
            If TipoOrdenacao2 = mediAscComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " DESC"
            ElseIf TipoOrdenacao2 = mediAscComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " DESC"
            End If
        End If
    End If
    'Fim: Crit�rio de Ordena��o

    Set obTabelaAux = New ADODB.recordset
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioAux
    obTabelaAux.Open CriterioAux, gConexaoSecundaria, adOpenStatic, adLockReadOnly

    NomeControl.Clear

    i = 0
    Do Until obTabelaAux.EOF
        NomeControl.AddItem RTrim(obTabelaAux(NomeCampoBD_Descr))
        NomeControl.ItemData(i) = CLng(obTabelaAux(NomeCampoBD_codigo))
        obTabelaAux.MoveNext
        i = i + 1
    Loop

    obTabelaAux.Close
    
    Exit Sub

TrataErro:
    If Err = 6 Then
        NomeControl.List(i) = "!!Sup. 32767: " & NomeControl.List(i)
        NomeControl.ItemData(i) = -10
        BG_TrataErro "BG_PreencheComboBDSecundaria_ADO", Err, "'" & NomeTabelaBD & "': Valor Superior a 32767!"
        Resume Next
    Else
        BG_TrataErro "BG_PreencheComboBDSecundaria_ADO " & NomeTabelaBD & ": ", Err
        Beep
        BG_Mensagem mediMsgStatus, "Erro a preencher " & NomeControl.Name & "!", vbCritical + vbOKOnly, "ERRO"
    End If

End Sub




Function BG_Retorna_BDErro() As String
'Verifica a validade da ultima opera��o SQL via ODBC e faz o display duma
'mensagem conforme o erro.
'O erro tratado � o erro nativo retornado pelo ODBC e corresponde
'ao erro emitido pelo gSGBD usado.
'As vari�veis gInsRegistoDuplo, gErroSyntax, ... s�o arrays com os erros correspondentes
'em SQL_SERVER, ORACLE, INFORMIX. Estas variaveis s�o globais ao projecto,
'e ter�o que ser criadas � medida que surgem os erros.
'Poder� haver a necessidade de fazer uma revis�o aos v�rios erros
'nativos para criar vari�veis que estejam em falta.
    Dim aux
    Dim retorno As Integer
    Dim mensagem As String
    
    retorno = 0
    For Each aux In gValoresInsuf
        If aux = gSQLError Then
            If retorno = 0 Then
                mensagem = "Valores insuficientes para inser��o !"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gInsRegistoDuplo
        If aux = gSQLError Then
            If retorno = 0 Then
                mensagem = "J� existe registo com esse c�digo !"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gUpdRegistoDuplo
        If aux = gSQLError Then
            If retorno = 0 Then
                mensagem = "J� existe registo com esse c�digo !"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gInsRegistoNulo
        If aux = gSQLError Then
            If retorno = 0 Then
                mensagem = "N�o se pode inserir valor vazio num dos campos da tabela!"
                retorno = -1
            End If
        End If
    Next
    aux = 0
    For Each aux In gErroSyntax
        If aux = gSQLError Then
            If retorno = 0 Then
                mensagem = "Erro de sintaxe !"
                retorno = -1
            End If
        End If
    Next
    
    If retorno = 0 And gSQLError <> 0 Then
        mensagem = "Erro de processamento na BD (Erro:" & gSQLError & ",Isam:" & gSQLISAM & ") !"
        retorno = -1
    End If
    
    BG_Retorna_BDErro = mensagem
End Function


Public Sub BG_CarregaParamAmbiente()
    Dim sSql As String
    Dim rsResult As New ADODB.recordset
    Dim registos As Integer
    Dim sRes As String
    Dim Erro As Integer
    
    On Error GoTo TrataErro

    Erro = 0
    sRes = ""
    gTotalParamet = 0
    ReDim gEstrutParamet(0)
    gVERSAO_PARAM_AMB = BG_DevolveVersaoParamAmb
    
    If gVERSAO_PARAM_AMB = "V1" Then
        sSql = "SELECT acesso,ambito,cod_computador,cod_utilizador,chave,conteudo, -1 cod_local  FROM sl_paramet WHERE ambito = 0 "
        sSql = sSql & " UNION SELECT acesso,ambito,cod_computador,cod_utilizador,chave,conteudo, -1 cod_local  FROM sl_paramet WHERE ambito = 1 AND cod_computador = " & BL_GetComputerCodigo()
        sSql = sSql & " UNION SELECT acesso,ambito,cod_computador,cod_utilizador,chave,conteudo, -1 cod_local  FROM sl_paramet WHERE ambito = 2 AND cod_utilizador = " & gCodUtilizador
    ElseIf gVERSAO_PARAM_AMB = "V2" Then
        sSql = "SELECT x1.acesso,x1.ambito,x1.cod_computador,x1.cod_utilizador,x1.chave,x2.conteudo,x2.cod_local FROM sl_paramet x1, sl_paramet_locais x2 WHERE x1.codigo = x2.cod_paramet AND ambito = 0 and x2.cod_local = " & gCodLocal
        sSql = sSql & " UNION SELECT x1.acesso,x1.ambito,x1.cod_computador,x1.cod_utilizador,x1.chave,x2.conteudo,x2.cod_local FROM sl_paramet x1, sl_paramet_locais x2 WHERE x1.codigo = x2.cod_paramet AND ambito = 1 AND cod_computador = " & BL_GetComputerCodigo() & " and x2.cod_local = " & gCodLocal
        sSql = sSql & " UNION SELECT x1.acesso,x1.ambito,x1.cod_computador,x1.cod_utilizador,x1.chave,x2.conteudo,x2.cod_local FROM sl_paramet x1, sl_paramet_locais x2 WHERE x1.codigo = x2.cod_paramet AND ambito = 2 AND cod_utilizador = " & gCodUtilizador & " and x2.cod_local = " & gCodLocal
    Else
        sSql = "SELECT acesso,ambito,cod_computador,cod_utilizador,chave,conteudo, -1 cod_local  FROM sl_paramet WHERE ambito = 0 "
        sSql = sSql & " UNION SELECT acesso,ambito,cod_computador,cod_utilizador,chave,conteudo, -1 cod_local  FROM sl_paramet WHERE ambito = 1 AND cod_computador = " & BL_GetComputerCodigo()
        sSql = sSql & " UNION SELECT acesso,ambito,cod_computador,cod_utilizador,chave,conteudo, -1 cod_local  FROM sl_paramet WHERE ambito = 2 AND cod_utilizador = " & gCodUtilizador
    End If
        rsResult.CursorLocation = adUseServer
        rsResult.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsResult.Open sSql, gConexao
        If rsResult.RecordCount >= 1 Then
            While Not rsResult.EOF
                gTotalParamet = gTotalParamet + 1
                ReDim Preserve gEstrutParamet(gTotalParamet)
                gEstrutParamet(gTotalParamet).acesso = BL_HandleNull(rsResult!acesso, -1)
                gEstrutParamet(gTotalParamet).ambito = BL_HandleNull(rsResult!ambito, -1)
                gEstrutParamet(gTotalParamet).cod_computador = BL_HandleNull(rsResult!cod_computador, -1)
                gEstrutParamet(gTotalParamet).cod_utilizador = BL_HandleNull(rsResult!cod_utilizador, "")
                gEstrutParamet(gTotalParamet).chave = BL_HandleNull(rsResult!chave, "")
                gEstrutParamet(gTotalParamet).conteudo = BL_HandleNull(rsResult!conteudo, "")
                gEstrutParamet(gTotalParamet).cod_local = BL_HandleNull(rsResult!cod_local, -1)
                rsResult.MoveNext
            Wend
        End If
        rsResult.Close
        Set rsResult = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BG_CarregaParamAmbiente:" & Err.Description, "BG_ADO", "BG_CarregaParamAmbiente", True
    Resume Next
End Sub

Private Function BG_DevolveVersaoParamAmb()
    
    Dim sSql As String
    Dim rsResult As New ADODB.recordset
    On Error GoTo TrataErro

    
    sSql = "SELECT conteudo from sl_paramet where chave = " & BL_TrataStringParaBD("VERSAO_PARAM_AMB")

    rsResult.CursorLocation = adUseServer
    rsResult.CursorType = adOpenStatic
    If (gModoDebug = mediSim) Then: BG_LogFile_Erros sSql
    rsResult.Open sSql, gConexao
    If (rsResult.RecordCount > Empty) Then: BG_DevolveVersaoParamAmb = BL_HandleNull(rsResult!conteudo, Empty)
    rsResult.Close
    Set rsResult = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "ParamAmbiente.BG_DevolveVersaoParamAmb:" & Err.Description
    Resume Next
End Function

'NELSONPSILVA Glintt-HS-18011 09.02.2018
Public Function BG_DevolveCriterioFiltrado(ByVal NomeTabela As String, ByVal Criterio As String) As String
    Dim sql As String
    
    Select Case UCase(NomeTabela)
        Case "SL_REQUIS", "SL_REQUIS_CONSULTAS"
            sql = "SELECT * from (SELECT a.* from (" & Criterio & ") a, slv_identif b" & _
                " WHERE a.seq_utente = b.seq_utente)"
        Case Else
            sql = Criterio
        End Select

    BG_DevolveCriterioFiltrado = sql
End Function


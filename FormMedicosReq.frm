VERSION 5.00
Begin VB.Form FormMedicosReq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4425
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6240
   Icon            =   "FormMedicosReq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4425
   ScaleWidth      =   6240
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcServico 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   33
      Top             =   600
      Width           =   4695
   End
   Begin VB.CheckBox CkMedRequisitante 
      Caption         =   "Marca��o como m�dico requisitante"
      Height          =   195
      Left            =   240
      TabIndex        =   32
      Top             =   2160
      Width           =   3975
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1200
      TabIndex        =   31
      Top             =   240
      Width           =   615
   End
   Begin VB.TextBox EcPesqRapMedico 
      Height          =   285
      Left            =   3960
      TabIndex        =   29
      Top             =   5760
      Width           =   735
   End
   Begin VB.CommandButton BtPesquisaMedico 
      Height          =   315
      Left            =   5520
      Picture         =   "FormMedicosReq.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "Pesquisa R�pida de Utentes"
      Top             =   240
      Width           =   375
   End
   Begin VB.TextBox EcCodPostal 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      MaxLength       =   4
      TabIndex        =   3
      Top             =   1580
      Width           =   855
   End
   Begin VB.TextBox EcRuaPostal 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      MaxLength       =   3
      TabIndex        =   4
      Top             =   1580
      Width           =   735
   End
   Begin VB.TextBox EcDescrPostal 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2760
      TabIndex        =   5
      Top             =   1580
      Width           =   3135
   End
   Begin VB.TextBox EcCSaude 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   1
      Top             =   930
      Width           =   4695
   End
   Begin VB.TextBox EcNome 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   0
      Top             =   240
      Width           =   3855
   End
   Begin VB.TextBox EcRequisicao 
      Height          =   285
      Left            =   4920
      TabIndex        =   19
      Top             =   4920
      Width           =   975
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   4920
      TabIndex        =   18
      Top             =   5400
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   270
      TabIndex        =   11
      Top             =   3480
      Width           =   5685
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   17
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   16
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   15
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   14
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   10
      Top             =   4920
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   9
      Top             =   4950
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   8
      Top             =   5400
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   7
      Top             =   5400
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   690
      Left            =   240
      TabIndex        =   6
      Top             =   2640
      Width           =   5655
   End
   Begin VB.TextBox EcMorada 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   2
      Top             =   1245
      Width           =   4695
   End
   Begin VB.Label Label7 
      Caption         =   "Servi�o"
      Height          =   255
      Left            =   240
      TabIndex        =   34
      Top             =   600
      Width           =   735
   End
   Begin VB.Label Label15 
      Caption         =   "EcPesqRapMedico"
      Height          =   255
      Left            =   2640
      TabIndex        =   30
      Top             =   5760
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "M�dico"
      Height          =   255
      Left            =   240
      TabIndex        =   27
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "C. Sa�de"
      Height          =   255
      Left            =   240
      TabIndex        =   26
      Top             =   930
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "Cod Postal"
      Height          =   255
      Left            =   240
      TabIndex        =   25
      Top             =   1580
      Width           =   855
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2640
      TabIndex        =   24
      Top             =   5400
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2640
      TabIndex        =   23
      Top             =   4920
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   5400
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   360
      TabIndex        =   21
      Top             =   4920
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Morada"
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   1245
      Width           =   975
   End
End
Attribute VB_Name = "FormMedicosReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim CriterioBase As String 'usado para os botoes de ordenacao
Dim CriterioAnterior As String 'usado para os botoes de ordenacao

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public obTabela As ADODB.recordset

' Indica os estados da ordena��o
    '10 - Codigo Ascendente     11 - Codigo Descendente
    '20 - sigla                 21 - sigla
    '30 - Descricao             31 - Descricao
Dim bTemCriterio As Boolean ' True - Com crit�rio de pesquisa; False - Sem crit�rio de pesquisa
Dim OrdenacaoAnterior As Byte
Dim TipoOrdenacao As Byte
Dim bLista As Boolean


Private Sub BtPesquisaMedico_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_medicos", _
'                        "nome_med", "cod_med", _
'                        EcPesqRapMedico

    Dim ChavesPesq(1 To 5) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 5) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 5) As Long
    Dim Headers(1 To 5) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 5)  As Variant
    Dim PesqRapida As Boolean
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    Headers(2) = "Nome"
    
    ChavesPesq(3) = "servico"
    CamposEcran(3) = "servico"
    Tamanhos(3) = 3000
    Headers(3) = "Servico"
    
    ChavesPesq(4) = "csaude"
    CamposEcran(4) = "csaude"
    Tamanhos(4) = 3000
    Headers(4) = "Centro Sa�de"
    
    ChavesPesq(5) = "morada"
    CamposEcran(5) = "morada"
    Tamanhos(5) = 3000
    Headers(5) = "Morada"
    
    CamposRetorno.InicializaResultados 5
    
    CFrom = "sl_medicos"
    If EcCodigo.Text <> "" Then
        CWhere = "cod_med = '" & EcCodigo.Text & "'"
    Else
        CWhere = ""
    End If
    CampoPesquisa = "nome_med"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " M�dicos")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FuncaoLimpar
            EcCodigo.Text = resultados(1)
            EcNome.Text = resultados(2)
            EcServico.Text = resultados(3)
            EcCSaude.Text = resultados(4)
            EcMorada.Text = resultados(5)
            
            Set rsCodigo = New ADODB.recordset
            sql = "SELECT * FROM sl_medicos WHERE cod_med = '" & resultados(1) & "'"
            rsCodigo.CursorLocation = adUseServer
            rsCodigo.CursorType = adOpenStatic
            
            rsCodigo.Open sql, gConexao
            If rsCodigo.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do m�dico!", vbExclamation, "Pesquisa r�pida"
                EcPesqRapMedico.Text = ""
            Else
                EcCodigo.Text = Trim(rsCodigo!cod_med)
                EcNome.Text = Trim(rsCodigo!nome_med)
                EcServico.Text = BL_HandleNull(Trim(rsCodigo!servico), "")
                EcCSaude.Text = BL_HandleNull(Trim(rsCodigo!csaude), "")
                EcMorada.Text = BL_HandleNull(Trim(rsCodigo!morada), "")
                EcCodPostalAux.Text = BL_HandleNull(Trim(rsCodigo!codPostal), "")
                PreencheCodigoPostal
            End If
            rsCodigo.Close
            Set rsCodigo = Nothing
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem m�dicos codificados", vbExclamation, "ATEN��O"
    End If
End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String

    If EcCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT * FROM sl_medicos WHERE cod_med=" & Trim(EcCodigo.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            LimpaCampos
        Else
            EcCodigo.Text = Trim(Tabela!cod_med)
            EcNome.Text = Trim(Tabela!nome_med)
            EcCSaude.Text = BL_HandleNull(Trim(Tabela!csaude), "")
            EcMorada.Text = BL_HandleNull(Trim(Tabela!morada), "")
            EcCodPostalAux.Text = BL_HandleNull(Trim(Tabela!codPostal), "")
            EcServico.Text = BL_HandleNull(Trim(Tabela!servico), "")
            PreencheCodigoPostal
        End If
        Tabela.Close
        Set Tabela = Nothing
        
    End If

End Sub
Private Sub EcPesqRapMedico_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If EcPesqRapMedico.Text <> "" Then
        FuncaoLimpar
        Set rsCodigo = New ADODB.recordset
        sql = "SELECT * FROM sl_medicos WHERE cod_med = '" & EcPesqRapMedico & "'"
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do m�dico!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapMedico.Text = ""
        Else
            EcCodigo.Text = Trim(rsCodigo!cod_med)
            EcNome.Text = Trim(rsCodigo!nome_med)
            EcCSaude.Text = BL_HandleNull(Trim(rsCodigo!csaude), "")
            EcMorada.Text = BL_HandleNull(Trim(rsCodigo!morada), "")
            EcCodPostalAux.Text = BL_HandleNull(Trim(rsCodigo!codPostal), "")
            EcServico.Text = BL_HandleNull(Trim(rsCodigo!servico), "")
            PreencheCodigoPostal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcCodPostal_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodPostal_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
    If EcRuaPostal <> "" And EcCodPostal <> "" Then
        EcCodPostalAux = EcCodPostal & "-" & EcRuaPostal
    Else
        EcCodPostalAux = EcCodPostal
    End If
End Sub

Private Sub EcRuaPostal_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcRuaPostal_LostFocus()
    'BG_ValidaTipoCampo Me, CampoActivo
    If EcRuaPostal <> "" And EcCodPostal <> "" Then
        EcCodPostalAux = EcCodPostal & "-" & EcRuaPostal
    End If
End Sub

Private Sub EcLista_Click()
    If EcLista.ListCount > 0 Then
        obTabela.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
        bLista = True
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " M�dicos da Requisi��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 6330
    Me.Height = 4800 ' Normal
    'Me.Height = 5775 ' Campos Extras
    
    NomeTabela = "sl_medicos_req"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 7
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_med"
    CamposBD(1) = "n_req"
    CamposBD(2) = "flg_med_requis"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"

    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcRequisicao
    Set CamposEc(2) = CkMedRequisitante
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcUtilizadorAlteracao
    Set CamposEc(6) = EcDataAlteracao

    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo do M�dico"
    TextoCamposObrigatorios(1) = "Requisi��o"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_med"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("cod_med", "nome_med")
    NumEspacos = Array(7, 40)
    
    bTemCriterio = False
    
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    If gMultiReport <> 1 Then
        BL_ToolbarEstadoN estado
    End If
  
    'A form que abriu esta foi a form FormGestaoRequisicao
    If gF_REQUIS = 1 Then
        If Trim(FormGestaoRequisicao.EcNumReq) <> "" Then
            EcRequisicao = FormGestaoRequisicao.EcNumReq
        Else
            EcRequisicao = "-" & gNumeroSessao
        End If
    
        FuncaoLimpar
        FuncaoProcurar
        'BtImpEtiqueta = True
    ElseIf gF_REQUTE = 1 Then
        If Trim(FormReqUte.EcNumReq) <> "" Then
            EcRequisicao = FormReqUte.EcNumReq
        Else
            EcRequisicao = "-" & gNumeroSessao
        End If
    
        FuncaoLimpar
        FuncaoProcurar
    End If
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If gF_REQUIS = 1 Then
        FormGestaoRequisicao.Enabled = True
    ElseIf gF_REQUTE = 1 Then
        FormReqUte.Enabled = True
    End If
    
    If Not obTabela Is Nothing Then
        obTabela.Close
        Set obTabela = Nothing
    End If
    Set FormMedicosReq = Nothing
    
End Sub

Sub LimpaCampos()
    Dim aux As String
    Me.SetFocus
    aux = EcRequisicao
    BG_LimpaCampo_Todos CamposEc
    
    EcRequisicao = aux
    EcCodPostal = ""
    EcRuaPostal = ""
    EcNome.Text = ""
    EcCSaude.Text = ""
    EcMorada.Text = ""
    EcCodPostalAux.Text = ""
    EcDescrPostal.Text = ""
    EcServico.Text = ""
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    
    bTemCriterio = False
End Sub

Sub DefTipoCampos()

    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    'Tipo Inteiro
    EcCodPostal.Tag = adInteger
    EcRuaPostal.Tag = adInteger
    
    CkMedRequisitante.value = vbGrayed
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheCampos()
    Me.SetFocus
    
    If obTabela.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO obTabela, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        bTemCriterio = True
        'EcCodigo_Validate (True)
        EcCodigo.Text = BL_HandleNull(obTabela!cod_med, "")
        EcNome.Text = BL_HandleNull(obTabela!nome_med, "")
        EcCSaude.Text = BL_HandleNull(obTabela!csaude, "")
        EcMorada.Text = BL_HandleNull(obTabela!morada, "")
        EcCodPostalAux.Text = BL_HandleNull(obTabela!codPostal, "")
        EcServico.Text = BL_HandleNull(obTabela!servico, "")
        PreencheCodigoPostal
    End If
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        
        If Not obTabela Is Nothing Then
            obTabela.Close
            Set obTabela = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes As Integer
    Dim i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set obTabela = New ADODB.recordset
    
    'CriterioBase = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    
    CriterioBase = "select r.n_req,r.cod_med,r.flg_med_requis,r.user_cri,r.dt_cri,r.user_act,r.dt_act,nome_med,servico,csaude,morada,codpostal,localidade,servico" & _
                    " from sl_medicos m,sl_medicos_req r where m.cod_med = r.cod_med" & _
                    " and r.n_req = " & EcRequisicao
    CriterioTabela = CriterioBase & " ORDER BY r.n_req, r.cod_med"
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    obTabela.CursorType = adOpenStatic
    obTabela.CursorLocation = adUseServer
    
    obTabela.Open CriterioTabela, gConexao
    
    If obTabela.RecordCount <= 0 Then
        BL_FimProcessamento Me
        'BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = obTabela.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, obTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        'PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub
Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    obTabela.MovePrevious
    
    If obTabela.BOF Then
        obTabela.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    obTabela.MoveNext
    
    If obTabela.EOF Then
        obTabela.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    Dim sql As String
    Dim rs As ADODB.recordset
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            If CkMedRequisitante.value = 2 Then
                CkMedRequisitante.value = 0
            Else
                If gF_REQUIS = 1 Then
                    FormGestaoRequisicao.EcCodMedico.Text = EcCodigo.Text
                    FormGestaoRequisicao.EcNomeMedico.Text = EcNome.Text
                End If
            End If
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    'EcCodSequencial = BG_DaMAX(NomeTabela, "seq_medico") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery

End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        
        If iRes = True Then
            If CkMedRequisitante.value = 2 Then
                CkMedRequisitante.value = 0
            ElseIf gF_REQUIS = 1 Then
                If FormGestaoRequisicao.EcCodMedico.Text = "" Then
                    FormGestaoRequisicao.EcCodMedico.Text = EcCodigo.Text
                    FormGestaoRequisicao.EcNomeMedico.Text = EcNome.Text
                End If
            End If
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & EcCodigo & " And " & CamposBD(1) & " = " & EcRequisicao
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    BG_Trata_BDErro
    
    
    'propriedades precisam de ser estas pois � necessario utilizar o Bookmark
    MarcaLocal = obTabela.Bookmark
    obTabela.Requery
    
    If obTabela.BOF And obTabela.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, obTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= obTabela.RecordCount Then
        obTabela.Move EcLista.ListIndex, MarcaLocal
        obTabela.Bookmark = MarcaLocal
    End If
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        If CkMedRequisitante.value = 1 And gF_REQUIS = 1 Then
            If FormGestaoRequisicao.EcCodMedico.Text = EcCodigo.Text Then
                FormGestaoRequisicao.EcCodMedico.Text = ""
                FormGestaoRequisicao.EcNomeMedico.Text = ""
            End If
        End If
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(obTabela(ChaveBD)) & "' and " & CamposBD(1) & " = " & EcRequisicao
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'Temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = obTabela.Bookmark
    obTabela.Requery
        
    If obTabela.BOF And obTabela.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, obTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= obTabela.RecordCount Then
            obTabela.Move EcLista.ListIndex, MarcaLocal
            obTabela.Bookmark = MarcaLocal
        End If
    Else
        obTabela.MoveLast
    End If
    
    If obTabela.EOF Then obTabela.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

Sub PreencheCodigoPostal()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostalAux)
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub





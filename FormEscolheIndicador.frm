VERSION 5.00
Begin VB.Form FormEscolheIndicador 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Escolha de Indicador"
   ClientHeight    =   330
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4695
   Icon            =   "FormEscolheIndicador.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   330
   ScaleWidth      =   4695
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbGrIndicador 
      Height          =   315
      Left            =   0
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   0
      Width           =   4695
   End
End
Attribute VB_Name = "FormEscolheIndicador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ExecutaClick As Boolean


Private Sub CbGrIndicador_Click()

Dim url As String
Dim IE As Object

    If ExecutaClick = True And CbGrIndicador.ListIndex <> mediComboValorNull Then
            url = Trim(BL_Devolve_URL_MIS_Data_Discovery(BG_DaComboSel(CbGrIndicador)))
            If url <> "" Then
                Set IE = CreateObject("internetexplorer.application")
                IE.Navigate url
                IE.Visible = True
                ShowWindow IE.hwnd, 3
            Else
                'BG_TrataErro
                BG_Mensagem mediMsgBox, "Erro ao aceder ao Data Discovery!", vbOKOnly
            End If

        FormEscolheIndicador.Visible = True
    End If
    
End Sub


Private Sub Form_Load()

    FormEscolheIndicador.left = 9000
    FormEscolheIndicador.top = 120
    
    ExecutaClick = False
    CbGrIndicador.Clear
    
    BG_PreencheComboBD_ADO "sl_tbf_indicador", "cod_indicador", "descr_indicador", CbGrIndicador
    BG_Trata_BDErro
    
    ExecutaClick = True
    
End Sub

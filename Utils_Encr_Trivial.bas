Attribute VB_Name = "Utils_Encr_Trivial"
Option Explicit

' Actualização : 23/01/2002
' Paulo Costa

Public Function Encripta_Trivial(in_str As String) As String

    On Error GoTo TrataErro
    
    Dim ret As String
    Dim i As Integer
    Dim cod_c As Long

    ret = ""
    
    For i = 1 To Len(in_str)
        
        cod_c = CLng(Asc((Mid(in_str, i, 1))) - 5)
        If ((cod_c < 0) Or (cod_c > 255)) Then
            cod_c = 32
        End If
        
        ret = ret & Chr(cod_c)
    
    Next
    
    Encripta_Trivial = ret

    Exit Function
TrataErro:
    Encripta_Trivial = ""
End Function

Public Function Desencripta_Trivial(in_str As String) As String

    On Error GoTo TrataErro
    
    Dim ret As String
    Dim i As Integer
    Dim cod_c As Long

    ret = ""
    
    For i = 1 To Len(in_str)
        
        cod_c = CLng(Asc((Mid(in_str, i, 1))) + 5)
        If ((cod_c < 0) Or (cod_c > 255)) Then
            cod_c = 32
        End If
        
        ret = ret & Chr(cod_c)
    
    Next
    
    Desencripta_Trivial = ret

    Exit Function
TrataErro:
    Desencripta_Trivial = ""
End Function


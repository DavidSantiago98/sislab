VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{2EF1D294-3C92-4B2E-9781-6205A0F17CDC}#1.0#0"; "FluxCtrl.ocx"
Begin VB.Form FormReqPendAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormReqPendAna"
   ClientHeight    =   17010
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15105
   Icon            =   "FormReqPendAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   17010
   ScaleWidth      =   15105
   ShowInTaskbar   =   0   'False
   Begin FluxCtrl.IDLETimer IDLETimerRefresh 
      Left            =   12120
      Top             =   3720
      _ExtentX        =   873
      _ExtentY        =   873
   End
   Begin VB.Frame FrOpcoes 
      Caption         =   "Op��es"
      Height          =   2655
      Left            =   120
      TabIndex        =   9
      Top             =   5280
      Width           =   15015
      Begin VB.CheckBox CkAssinatura 
         Caption         =   "Resultados � Espera de Assinatura"
         Height          =   255
         Left            =   3720
         TabIndex        =   95
         Top             =   1440
         Width           =   3495
      End
      Begin VB.Frame FrameOrdena 
         Caption         =   "Ordena��o"
         Height          =   2415
         Left            =   10680
         TabIndex        =   88
         Top             =   120
         Width           =   2055
         Begin VB.OptionButton OptOrdena 
            Caption         =   "Triagem"
            Height          =   375
            Index           =   0
            Left            =   360
            TabIndex        =   94
            Top             =   1920
            Width           =   1215
         End
         Begin VB.OptionButton OptOrdena 
            Caption         =   "Nome"
            Height          =   255
            Index           =   1
            Left            =   360
            TabIndex        =   93
            Top             =   600
            Width           =   1215
         End
         Begin VB.OptionButton OptOrdena 
            Caption         =   "Proveni�ncia"
            Height          =   375
            Index           =   2
            Left            =   360
            TabIndex        =   92
            Top             =   840
            Width           =   1575
         End
         Begin VB.OptionButton OptOrdena 
            Caption         =   "Chegada"
            Height          =   375
            Index           =   3
            Left            =   360
            TabIndex        =   91
            Top             =   1200
            Width           =   1575
         End
         Begin VB.OptionButton OptOrdena 
            Caption         =   "Requisi��o"
            Height          =   375
            Index           =   4
            Left            =   360
            TabIndex        =   90
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton OptOrdena 
            Caption         =   "Urg�ncia"
            Height          =   375
            Index           =   5
            Left            =   360
            TabIndex        =   89
            Top             =   1560
            Width           =   1215
         End
      End
      Begin VB.Frame FrameDatas 
         Height          =   855
         Left            =   8040
         TabIndex        =   63
         Top             =   1680
         Width           =   1815
         Begin VB.OptionButton OptData 
            Caption         =   "Data Tubo"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   65
            Top             =   480
            Width           =   1575
         End
         Begin VB.OptionButton OptData 
            Caption         =   "Data Requisi��o"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   64
            Top             =   120
            Width           =   1575
         End
      End
      Begin VB.TextBox EcDtPretend 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   9480
         TabIndex        =   55
         Top             =   240
         Width           =   975
      End
      Begin VB.CheckBox CkMostrarNome 
         Caption         =   "Mostrar Nome Utente"
         Height          =   255
         Left            =   3720
         TabIndex        =   46
         Top             =   720
         Width           =   3375
      End
      Begin VB.CheckBox CkApenasMarcacoes 
         Caption         =   "Apenas An�lises Sem Resultados"
         Height          =   255
         Left            =   240
         TabIndex        =   43
         Top             =   1800
         Width           =   3495
      End
      Begin VB.TextBox EcReq 
         Height          =   285
         Left            =   9480
         TabIndex        =   31
         Top             =   720
         Width           =   975
      End
      Begin VB.CheckBox CkApenasResultados 
         Caption         =   "Apenas Resultados"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   1440
         Width           =   2535
      End
      Begin VB.CommandButton BtSairFrOpcoes 
         Caption         =   "Confirmar"
         Height          =   495
         Left            =   13680
         Picture         =   "FormReqPendAna.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2040
         Width           =   855
      End
      Begin VB.CheckBox CkValidacaoSequencial 
         Caption         =   "Valida��o Sequencial"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   360
         Width           =   1935
      End
      Begin VB.CheckBox CkUrgentes 
         Caption         =   "Apenas Requisi��es Urgentes"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   720
         Width           =   2535
      End
      Begin VB.CheckBox CkCompletas 
         Caption         =   "Apenas Requisi��es com Todos Resultados"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   2160
         Width           =   3495
      End
      Begin VB.CheckBox CkUserAltert 
         Caption         =   "Apenas Requisi��es Electr�nicas"
         Height          =   255
         Left            =   3720
         TabIndex        =   12
         Top             =   1080
         Width           =   3495
      End
      Begin VB.CheckBox CkTodas 
         Caption         =   "Todas as requisi��es"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   1080
         Width           =   3495
      End
      Begin VB.CheckBox CkRequisComTubo 
         Caption         =   "Apenas Requisi��es com Tubo"
         Height          =   255
         Left            =   3720
         TabIndex        =   10
         Top             =   360
         Width           =   3495
      End
      Begin VB.Label Label16 
         Caption         =   "Data Pretendida"
         Height          =   240
         Index           =   1
         Left            =   7920
         TabIndex        =   56
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label16 
         Caption         =   "Procurar Requisi��o"
         Height          =   240
         Index           =   0
         Left            =   7920
         TabIndex        =   32
         Top             =   735
         Width           =   1455
      End
   End
   Begin VB.CommandButton BtLegenda 
      Height          =   615
      Left            =   14280
      Picture         =   "FormReqPendAna.frx":0156
      Style           =   1  'Graphical
      TabIndex        =   87
      ToolTipText     =   "Legenda"
      Top             =   840
      Width           =   735
   End
   Begin VB.Frame FrameLegenda 
      Height          =   2175
      Left            =   120
      TabIndex        =   71
      Top             =   10440
      Width           =   14055
      Begin VB.Label Lb1 
         BackColor       =   &H000080FF&
         Height          =   255
         Index           =   12
         Left            =   3960
         TabIndex        =   86
         Top             =   840
         Width           =   255
      End
      Begin VB.Label Lb1 
         Caption         =   "An�lise Pendente"
         Height          =   255
         Index           =   9
         Left            =   4440
         TabIndex        =   85
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label Lb1 
         BackColor       =   &H000000FF&
         Height          =   255
         Index           =   8
         Left            =   3960
         TabIndex        =   84
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Lb1 
         Caption         =   "An�lise Bloqueada"
         Height          =   255
         Index           =   5
         Left            =   4440
         TabIndex        =   83
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Lb1 
         Caption         =   "An�lise Sem Resultado"
         Height          =   255
         Index           =   10
         Left            =   600
         TabIndex        =   82
         Top             =   1440
         Width           =   1695
      End
      Begin VB.Label LbValidacao 
         Caption         =   "An�lise Validada"
         Height          =   255
         Left            =   600
         TabIndex        =   81
         Top             =   825
         Width           =   2295
      End
      Begin VB.Label Lb1 
         Caption         =   "An�lise Por Validar"
         Height          =   255
         Index           =   6
         Left            =   600
         TabIndex        =   80
         Top             =   1155
         Width           =   1335
      End
      Begin VB.Label Lb1 
         Caption         =   "An�lise Sem Tubo"
         Height          =   255
         Index           =   7
         Left            =   600
         TabIndex        =   79
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label Lb1 
         Caption         =   "An�lise Impressa"
         Height          =   255
         Index           =   4
         Left            =   600
         TabIndex        =   78
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Lb1 
         BackColor       =   &H00C0FFC0&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   77
         Top             =   825
         Width           =   255
      End
      Begin VB.Label Lb1 
         BackColor       =   &H00C0C0C0&
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   76
         Top             =   1155
         Width           =   255
      End
      Begin VB.Label Lb1 
         BackColor       =   &H0080FFFF&
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   75
         Top             =   1440
         Width           =   255
      End
      Begin VB.Label Lb1 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   74
         Top             =   1800
         Width           =   255
      End
      Begin VB.Label Lb1 
         BackColor       =   &H00FF8080&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   73
         Top             =   480
         Width           =   255
      End
      Begin VB.Label LbReq 
         Caption         =   "A cor na coluna da requisi��o define a prioridade de acordo com as codifica��es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   72
         Top             =   165
         Width           =   8415
      End
   End
   Begin RichTextLib.RichTextBox RTB 
      Height          =   495
      Left            =   5040
      TabIndex        =   66
      Top             =   9960
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   873
      _Version        =   393217
      TextRTF         =   $"FormReqPendAna.frx":0E20
   End
   Begin VB.TextBox EcHoraMax 
      Height          =   285
      Left            =   5760
      TabIndex        =   53
      Text            =   "23"
      Top             =   2280
      Width           =   300
   End
   Begin VB.TextBox EcMinutoMax 
      Height          =   285
      Left            =   6240
      TabIndex        =   51
      Text            =   "59"
      Top             =   2280
      Width           =   300
   End
   Begin VB.TextBox EcMinutoMin 
      Height          =   285
      Left            =   2640
      TabIndex        =   49
      Text            =   "00"
      Top             =   2280
      Width           =   315
   End
   Begin MSComCtl2.UpDown BtHoraMin 
      Height          =   285
      Left            =   2430
      TabIndex        =   48
      Top             =   2280
      Width           =   255
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      AutoBuddy       =   -1  'True
      BuddyControl    =   "EcHoraMin"
      BuddyDispid     =   196636
      OrigLeft        =   4560
      OrigTop         =   2280
      OrigRight       =   4815
      OrigBottom      =   2535
      Max             =   23
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox EcHoraMin 
      Height          =   285
      Left            =   2160
      TabIndex        =   47
      Text            =   "0"
      Top             =   2280
      Width           =   555
   End
   Begin VB.CommandButton BtActualizar 
      Height          =   615
      Left            =   14280
      Picture         =   "FormReqPendAna.frx":0EAB
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Actualizar"
      Top             =   1560
      Width           =   735
   End
   Begin VB.CommandButton BtResutadosNovo 
      Height          =   495
      Left            =   9360
      Picture         =   "FormReqPendAna.frx":1B75
      Style           =   1  'Graphical
      TabIndex        =   42
      ToolTipText     =   "Ecr� Resultados"
      Top             =   8400
      Width           =   615
   End
   Begin VB.TextBox EcGrAnalises 
      Height          =   375
      Left            =   3120
      TabIndex        =   33
      Top             =   9960
      Visible         =   0   'False
      Width           =   735
   End
   Begin TabDlg.SSTab TabFiltros 
      Height          =   2055
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   14055
      _ExtentX        =   24791
      _ExtentY        =   3625
      _Version        =   393216
      Style           =   1
      Tabs            =   8
      Tab             =   2
      TabsPerRow      =   8
      TabHeight       =   520
      TabCaption(0)   =   "Grp. An�lises"
      TabPicture(0)   =   "FormReqPendAna.frx":283F
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "EcListaGrAna"
      Tab(0).Control(1)=   "BtPesquisaRapidaGrAnalises"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Grp. Trabalho"
      TabPicture(1)   =   "FormReqPendAna.frx":285B
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "BtPesquisaRapidaGrTrab"
      Tab(1).Control(1)=   "EcListaGrTrab"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Proveni�ncias"
      TabPicture(2)   =   "FormReqPendAna.frx":2877
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "EcListaProven"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "BtPesquisaProveniencia"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Produtos"
      TabPicture(3)   =   "FormReqPendAna.frx":2893
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "EcListaProd"
      Tab(3).Control(1)=   "BtPesquisaProdutos"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Locais"
      TabPicture(4)   =   "FormReqPendAna.frx":28AF
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "EcListaLocais"
      Tab(4).Control(1)=   "BtPesquisaLocais"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Frame3"
      Tab(4).ControlCount=   3
      TabCaption(5)   =   "An�lises"
      TabPicture(5)   =   "FormReqPendAna.frx":28CB
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "BtPesquisaAnalises"
      Tab(5).Control(0).Enabled=   0   'False
      Tab(5).Control(1)=   "EcListaAnalises"
      Tab(5).Control(2)=   "Frame2"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "Aparelhos"
      TabPicture(6)   =   "FormReqPendAna.frx":28E7
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "EcListaApar"
      Tab(6).Control(1)=   "BtPesquisaApar"
      Tab(6).Control(1).Enabled=   0   'False
      Tab(6).ControlCount=   2
      TabCaption(7)   =   "Epis�dio/Urg."
      TabPicture(7)   =   "FormReqPendAna.frx":2903
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "BtPesquisaSit"
      Tab(7).Control(0).Enabled=   0   'False
      Tab(7).Control(1)=   "EcListaSit"
      Tab(7).Control(2)=   "BtPesquisaUrg"
      Tab(7).Control(2).Enabled=   0   'False
      Tab(7).Control(3)=   "EcListaUrg"
      Tab(7).ControlCount=   4
      Begin VB.ListBox EcListaUrg 
         Height          =   1230
         Left            =   -68760
         TabIndex        =   70
         Top             =   480
         Width           =   5055
      End
      Begin VB.CommandButton BtPesquisaUrg 
         Height          =   315
         Left            =   -63720
         Picture         =   "FormReqPendAna.frx":291F
         Style           =   1  'Graphical
         TabIndex        =   69
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Aparelhos"
         Top             =   480
         Width           =   375
      End
      Begin VB.ListBox EcListaSit 
         Height          =   1230
         Left            =   -74880
         TabIndex        =   68
         Top             =   480
         Width           =   5655
      End
      Begin VB.CommandButton BtPesquisaSit 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormReqPendAna.frx":2EA9
         Style           =   1  'Graphical
         TabIndex        =   67
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Aparelhos"
         Top             =   480
         Width           =   375
      End
      Begin VB.Frame Frame3 
         Height          =   855
         Left            =   -65280
         TabIndex        =   60
         Top             =   960
         Width           =   1455
         Begin VB.OptionButton Opt2 
            Caption         =   "Requisi��es"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   62
            Top             =   480
            Width           =   1215
         End
         Begin VB.OptionButton Opt2 
            Caption         =   "An�lises"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   61
            Top             =   120
            Width           =   1095
         End
      End
      Begin VB.Frame Frame2 
         Height          =   855
         Left            =   -64800
         TabIndex        =   57
         Top             =   1080
         Width           =   1335
         Begin VB.OptionButton Opt1 
            Caption         =   "Incluir"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   59
            Top             =   120
            Width           =   1095
         End
         Begin VB.OptionButton Opt1 
            Caption         =   "Excluir"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   58
            Top             =   480
            Width           =   975
         End
      End
      Begin VB.CommandButton BtPesquisaApar 
         Height          =   315
         Left            =   -64800
         Picture         =   "FormReqPendAna.frx":3433
         Style           =   1  'Graphical
         TabIndex        =   45
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Aparelhos"
         Top             =   600
         Width           =   375
      End
      Begin VB.ListBox EcListaApar 
         Height          =   1230
         Left            =   -74640
         TabIndex        =   44
         Top             =   600
         Width           =   9855
      End
      Begin VB.ListBox EcListaAnalises 
         Height          =   1230
         Left            =   -74760
         TabIndex        =   36
         Top             =   480
         Width           =   9495
      End
      Begin VB.CommandButton BtPesquisaAnalises 
         Height          =   315
         Left            =   -65280
         Picture         =   "FormReqPendAna.frx":39BD
         Style           =   1  'Graphical
         TabIndex        =   35
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de An�lises"
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaLocais 
         Height          =   315
         Left            =   -65520
         Picture         =   "FormReqPendAna.frx":3F47
         Style           =   1  'Graphical
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Locais"
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaProdutos 
         Height          =   315
         Left            =   -64200
         Picture         =   "FormReqPendAna.frx":44D1
         Style           =   1  'Graphical
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos"
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   10680
         Picture         =   "FormReqPendAna.frx":4A5B
         Style           =   1  'Graphical
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   480
         Width           =   375
      End
      Begin VB.ListBox EcListaLocais 
         Height          =   1425
         Left            =   -74640
         TabIndex        =   27
         Top             =   480
         Width           =   9135
      End
      Begin VB.ListBox EcListaProd 
         Height          =   1425
         Left            =   -74640
         TabIndex        =   26
         Top             =   480
         Width           =   10455
      End
      Begin VB.ListBox EcListaProven 
         Height          =   1425
         Left            =   360
         TabIndex        =   25
         Top             =   480
         Width           =   10335
      End
      Begin VB.ListBox EcListaGrTrab 
         Height          =   1425
         Left            =   -74640
         TabIndex        =   24
         Top             =   480
         Width           =   10695
      End
      Begin VB.CommandButton BtPesquisaRapidaGrTrab 
         Height          =   375
         Left            =   -63960
         Picture         =   "FormReqPendAna.frx":4FE5
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Pesquisa R�pida de Grupos de Trabalho"
         Top             =   480
         Width           =   375
      End
      Begin VB.ListBox EcListaGrAna 
         Height          =   1425
         Left            =   -74640
         TabIndex        =   22
         Top             =   480
         Width           =   10815
      End
      Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
         Height          =   375
         Left            =   -63840
         Picture         =   "FormReqPendAna.frx":556F
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
         Top             =   480
         Width           =   375
      End
   End
   Begin VB.TextBox EcCodSala 
      Height          =   285
      Left            =   1080
      TabIndex        =   18
      Top             =   9840
      Width           =   735
   End
   Begin VB.CommandButton BtOpcoes 
      Height          =   495
      Left            =   10080
      Picture         =   "FormReqPendAna.frx":5AF9
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Op��es"
      Top             =   8400
      Width           =   615
   End
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   720
      TabIndex        =   8
      Top             =   9840
      Width           =   255
   End
   Begin VB.CommandButton BtImprimeEtiq 
      Height          =   495
      Left            =   10800
      Picture         =   "FormReqPendAna.frx":67C3
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   8400
      Width           =   615
   End
   Begin VB.CommandButton BtImprimir 
      Height          =   495
      Left            =   11520
      Picture         =   "FormReqPendAna.frx":6ACD
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   8400
      Width           =   615
   End
   Begin VB.CommandButton BtSeguinte 
      Height          =   375
      Left            =   4920
      Picture         =   "FormReqPendAna.frx":7797
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "P�gina Seguinte"
      Top             =   8400
      Width           =   495
   End
   Begin VB.CommandButton BtAnterior 
      Height          =   375
      Left            =   3840
      Picture         =   "FormReqPendAna.frx":DFE9
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "P�gina Anterior"
      Top             =   8400
      Width           =   495
   End
   Begin VB.TextBox EcData 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   10200
      TabIndex        =   1
      Top             =   3600
      Visible         =   0   'False
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid FGReqPendRes 
      Height          =   5745
      Left            =   120
      TabIndex        =   0
      Top             =   2640
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   10134
      _Version        =   393216
      Rows            =   22
      Cols            =   10
      FixedRows       =   0
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin MSComCtl2.DTPicker EcDtIni 
      Height          =   255
      Left            =   840
      TabIndex        =   37
      Top             =   2280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   450
      _Version        =   393216
      Format          =   190054401
      CurrentDate     =   39573
   End
   Begin MSComCtl2.DTPicker EcDtFim 
      Height          =   255
      Left            =   4560
      TabIndex        =   38
      Top             =   2280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   450
      _Version        =   393216
      Format          =   190054401
      CurrentDate     =   39573
   End
   Begin MSComCtl2.UpDown BtMinutoMin 
      Height          =   285
      Left            =   2910
      TabIndex        =   50
      Top             =   2280
      Width           =   255
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      AutoBuddy       =   -1  'True
      BuddyControl    =   "EcMinutoMin"
      BuddyDispid     =   196635
      OrigLeft        =   5040
      OrigTop         =   2280
      OrigRight       =   5295
      OrigBottom      =   2535
      Max             =   59
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin MSComCtl2.UpDown BtHoraMax 
      Height          =   285
      Left            =   6030
      TabIndex        =   52
      Top             =   2280
      Width           =   255
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      BuddyControl    =   "EcHoraMax"
      BuddyDispid     =   196633
      OrigLeft        =   4560
      OrigTop         =   2280
      OrigRight       =   4815
      OrigBottom      =   2535
      Max             =   23
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin MSComCtl2.UpDown BtMinutoMax 
      Height          =   285
      Left            =   6510
      TabIndex        =   54
      Top             =   2280
      Width           =   255
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      BuddyControl    =   "EcMinutoMax"
      BuddyDispid     =   196634
      OrigLeft        =   5040
      OrigTop         =   2280
      OrigRight       =   5295
      OrigBottom      =   2535
      Max             =   59
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.Label LaTempo 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   6840
      TabIndex        =   41
      Top             =   14700
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label10 
      Caption         =   "Dt. Inicial"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   40
      Top             =   2325
      Width           =   855
   End
   Begin VB.Label Label10 
      Caption         =   "Dt. Final"
      Height          =   255
      Index           =   1
      Left            =   3840
      TabIndex        =   39
      Top             =   2280
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcGrAnalises"
      Height          =   255
      Left            =   2400
      TabIndex        =   34
      Top             =   10080
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label LbPagina 
      Alignment       =   2  'Center
      Height          =   255
      Left            =   4440
      TabIndex        =   5
      Top             =   8460
      Width           =   375
   End
End
Attribute VB_Name = "FormReqPendAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type
Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

' Vari�veis Globais para este Forr.
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Private Type analise
    abr_ana As String
    descr_ana As String
    flg_estado As String
    cod_tubo As String
    cod_ana As String
    Ord_Marca As Integer
End Type

Private Type requisicoes
    num_requis As String
    servico As String
    urgente As String
    dt_chega As String
    hr_chega As String
    tipo_urgencia As String
    nome As String
    dt_conclusao As String
    dt_pretendida As String
    dt_confirmacao As String
    dt_nascimento As String
    n_Req_assoc As String
    com_tec As String
    cor_urgencia As String
    limite As Long
    tempo As Double
    percentagem As Double
    analises() As analise
    totalAnalises As Integer
End Type

Dim EstrutDadosRequis() As requisicoes
Dim TotalDadosRequis As Integer

Dim EstrutDadosRequisImpr() As requisicoes
Dim TotalDadosRequisImpr As Integer

Private Type AnaImpr
    num_requis As String
    descr_ana As String
    tipo As String 'ANALISE VALIDADA - ANALISE SEM RESULTADO - ANALISE SEM VALIDACAO
    ordem As Integer
End Type

Dim EstrutAnaImpr() As AnaImpr
Dim TotalAnaImpr As Integer



Const vermelhoClaro = &H8080FF
Const Amarelo = &H80FFFF
Const Branco = &HFFFFFF

Const cinzento = &HC0C0C0              '&HE0E0E0
Const Verde = &HC0FFC0
Const azul = &HFEA381                          '&HFF8080
Const laranja = &H80FF&
Const vermelho = &HFF&
Const AzulClaro = &HFF8080

Const Validada = "ANALISE VALIDADA"
Const resultado = "ANALISE SEM RESULTADO"
Const Validar = "ANALISE SEM VALIDACAO"

Dim linhaActual As Long

Public rs As ADODB.recordset
Dim NReq As Long

Dim NumRequisicoes As Integer

Dim ValMinimo As Long
Dim ValMaximo As Long

'STRING QUE GUARDA OS ESTADOS QUE NAO SE CONSIDERA VALIDADOS (TEC/MED)
Dim EstadosNaoValidados As String

Public Flg_Resultados As Boolean
Public Flg_PassaProximaReq As Boolean

Dim tempo As Long
'Estruturas utilizadas para gerar as etiquetas
Private Type Tipo_Tubo
    descR_tubo As String
    inf_complementar As String
    cod_tubo As String
    n_req As String
End Type

Dim Tubos() As Tipo_Tubo

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

Dim lDataInicial As String
Dim lDataFinal As String
Dim lHoraInicial As String
Dim lHoraFinal As String

Const lColLimite = 0
Const lColNumReq = 1
Const lColProven = 2
Const lColDtChega = 3
Const lColHrChega = 4

Const colunas = 14

Private Type LimiteCor
    cod_limite_cor As Integer
    descr_limite_cor As String
    val_minimo As Integer
    val_maximo As Integer
    cor As Long
End Type
Dim EstrutLimiteCor() As LimiteCor
Dim totalLimiteCor As Integer

'R.G.: 26-03-2012
Dim EstrutDadosRequisOriginal() As requisicoes
Dim TotalDadosRequisOriginal As Integer

Const ordenaTUrg = "t_urg"
Const ordenaDtChega = "dt_chega"
Const ordenaTriagem = "triagem"
Const ordenaServico = "servico"
Const ordenaNumRequis = "num_requis"
Const ordenaNome = "nome"
Dim ordenacao As String

Private Sub BtActualizar_Click()
    FuncaoProcurar
    
End Sub

Private Sub BtAnterior_Click()
    Dim i As Integer
    If ValMinimo - NumRequisicoes >= 1 Then
        ValMaximo = ValMaximo - NumRequisicoes
        ValMinimo = ValMinimo - NumRequisicoes
    Else
        BG_Mensagem mediMsgBox, "N�o Existem mais P�ginas!", vbOKOnly + vbExclamation, "Aten��o!"
        Exit Sub
    End If
    LbPagina.caption = CLng(LbPagina) - 1
    DoEvents
    
    FGReqPendRes.Clear
    FGReqPendRes.Cols = colunas
    FGReqPendRes.rows = 23
    FGReqPendRes.row = 0
    linhaActual = 0
    
    FGReqPendRes.Visible = False
    For i = ValMinimo To ValMaximo
        If i <= TotalDadosRequis Then
            PreencheFlexGrid EstrutDadosRequis(i).num_requis, EstrutDadosRequis(i).urgente, EstrutDadosRequis(i).servico, _
                             EstrutDadosRequis(i).dt_chega, EstrutDadosRequis(i).hr_chega, EstrutDadosRequis(i).tipo_urgencia, _
                             EstrutDadosRequis(i).nome, EstrutDadosRequis(i).dt_confirmacao, EstrutDadosRequis(i).cor_urgencia, i
        End If
    Next
    FGReqPendRes.topRow = 0
    FGReqPendRes.Visible = True

End Sub





Private Sub BtImprimeEtiq_Click()
    Dim conta As Integer
    
    For conta = 1 To UBound(Tubos)
        ImprimeEtiq Tubos(conta).n_req, Tubos(conta).descR_tubo
    Next


End Sub

Private Sub BtImprimir_Click()
    FuncaoImprimir
End Sub



Private Sub BtLegenda_Click()
    If FrameLegenda.Visible = True Then
        FrameLegenda.Visible = False
    Else
        FrameLegenda.Visible = True
        FrameLegenda.top = 0
        FrameLegenda.left = 120
    End If
End Sub

Private Sub BtOpcoes_Click()
    FrOpcoes.left = 120
    FrOpcoes.top = 120
    FrOpcoes.Visible = True
End Sub

Private Sub BtPesquisaAnalises_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "slv_analises "
    'CWhere = "  (flg_invisivel is null or flg_invisivel = 0) "
    CampoPesquisa = "descr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaAnalises.ListCount = 0 Then
                EcListaAnalises.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                EcListaAnalises.ItemData(0) = resultados(i)
            Else
                EcListaAnalises.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                EcListaAnalises.ItemData(EcListaAnalises.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaApar_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer
    On Error GoTo TrataErro
    PesqRapida = False
    
    ChavesPesq(1) = "seq_apar"
    CamposEcran(1) = "seq_apar"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_apar"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    If gSGBD = gSqlServer Then
        CFrom = "gescom.dbo.gc_apar"
    ElseIf gSGBD = gOracle Then
        CFrom = "gc_apar"
    End If
    CWhere = ""
    CampoPesquisa = "descr_apar"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_apar ", _
                                                                           " Aparelhos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaApar.ListCount = 0 Then
                EcListaApar.AddItem BL_SelCodigo(CFrom, CamposEcran(2), CamposEcran(1), resultados(i))
                EcListaApar.ItemData(0) = resultados(i)
            Else
                EcListaApar.AddItem BL_SelCodigo(CFrom, CamposEcran(2), CamposEcran(1), resultados(i))
                EcListaApar.ItemData(EcListaApar.NewIndex) = resultados(i)
            End If
        Next i
    End If
    If EcListaApar.ListCount > 0 And gLAB = "LHL" Then
        CkApenasMarcacoes.value = vbChecked
    Else
        CkApenasMarcacoes.value = vbUnchecked
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaApar: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaApar", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisasit_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer
    On Error GoTo TrataErro
    PesqRapida = False
    
    ChavesPesq(1) = "cod_t_sit"
    CamposEcran(1) = "cod_t_sit"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_t_sit"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tbf_t_sit"
    
    CWhere = ""
    CampoPesquisa = "descr_t_sit"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_t_sit ", _
                                                                           " Situa��o")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaSit.ListCount = 0 Then
                EcListaSit.AddItem BL_SelCodigo(CFrom, CamposEcran(2), CamposEcran(1), resultados(i))
                EcListaSit.ItemData(0) = resultados(i)
            Else
                EcListaSit.AddItem BL_SelCodigo(CFrom, CamposEcran(2), CamposEcran(1), resultados(i))
                EcListaSit.ItemData(EcListaSit.NewIndex) = resultados(i)
            End If
        Next i
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisasit: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisasit", True
    Exit Sub
    Resume Next
End Sub
Private Sub BtPesquisaurg_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer
    On Error GoTo TrataErro
    PesqRapida = False
    
    ChavesPesq(1) = "cod_t_urg"
    CamposEcran(1) = "cod_t_urg"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_t_urg"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tbf_t_urg"
    
    CWhere = ""
    CampoPesquisa = "descr_t_urg"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_t_urg ", _
                                                                           " Tipo de Urg�ncia")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaUrg.ListCount = 0 Then
                EcListaUrg.AddItem BL_SelCodigo(CFrom, CamposEcran(2), CamposEcran(1), resultados(i))
                EcListaUrg.ItemData(0) = resultados(i)
            Else
                EcListaUrg.AddItem BL_SelCodigo(CFrom, CamposEcran(2), CamposEcran(1), resultados(i))
                EcListaUrg.ItemData(EcListaUrg.NewIndex) = resultados(i)
            End If
        Next i
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisasit: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisasit", True
    Exit Sub
    Resume Next
End Sub


Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(0) = resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaProdutos_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto"
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProd.ListCount = 0 Then
                EcListaProd.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                EcListaProd.ItemData(0) = resultados(i)
            Else
                EcListaProd.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                EcListaProd.ItemData(EcListaProd.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub




Private Sub BtSairFrOpcoes_Click()
    FrOpcoes.Visible = False
    FuncaoProcurar
End Sub

Private Sub BtSeguinte_Click()
    Dim i As Integer
    Dim limite As Long
    
    If ValMinimo + NumRequisicoes <= TotalDadosRequis Then
        ValMaximo = ValMaximo + NumRequisicoes
        ValMinimo = ValMinimo + NumRequisicoes
    Else
        BG_Mensagem mediMsgBox, "N�o Existem mais P�ginas!", vbOKOnly + vbExclamation, "Aten��o!"
        Exit Sub
    End If
    LbPagina.caption = CLng(LbPagina) + 1
    DoEvents
    FGReqPendRes.Visible = False
    FGReqPendRes.Clear
    FGReqPendRes.Cols = colunas
    FGReqPendRes.rows = 23
    FGReqPendRes.row = 0
    linhaActual = 0
    If ValMaximo < TotalDadosRequis Then
        limite = ValMaximo
    Else
        limite = TotalDadosRequis
    End If
    FGReqPendRes.Enabled = False
    For i = ValMinimo To limite
        If i <= TotalDadosRequis Then

            PreencheFlexGrid EstrutDadosRequis(i).num_requis, EstrutDadosRequis(i).urgente, EstrutDadosRequis(i).servico, _
                             EstrutDadosRequis(i).dt_chega, EstrutDadosRequis(i).hr_chega, EstrutDadosRequis(i).tipo_urgencia, _
                             EstrutDadosRequis(i).nome, EstrutDadosRequis(i).dt_confirmacao, EstrutDadosRequis(i).cor_urgencia, i
        End If
    Next
    FGReqPendRes.topRow = 0
    FGReqPendRes.Enabled = True
    FGReqPendRes.Visible = True
End Sub

Private Sub Check1_Click()

End Sub

Private Sub CkTodas_Click()
    
    If CkTodas.value = 0 Then
        CkTodas.caption = "Todas as requisi��es "
    Else
        CkAssinatura.value = vbUnchecked
        
        If EcDtIni.value <> "" Then
            If EcDtFim.value <> "" Then
                If EcDtIni.value <> EcDtFim.value Then
                    BG_Mensagem mediMsgBox, "Esta op��o s� permite visualizar um dia! " & vbCrLf & " Coloque a data inicial igual � data final", vbInformation, "Pendentes Detalhada"
                    CkTodas.value = 0
                    Exit Sub
                Else
                    EcData.Text = EcDtIni.value
                    CkTodas.caption = "Todas as requisi��es do dia " & EcData.Text
                End If
            Else
                EcData.Text = EcDtIni.value
                CkTodas.caption = "Todas as requisi��es do dia " & EcData.Text
            End If
        ElseIf EcData.Text = "ALL" Or EcData.Text = "" Then
            EcData.Text = Bg_DaData_ADO
            CkTodas.caption = "Todas as requisi��es do dia " & EcData.Text
        Else
            CkTodas.caption = "Todas as requisi��es do dia " & EcData.Text
        End If
        
    End If
    FuncaoProcurar
    
    
End Sub



Private Sub EcDtIni_Change()
    EcHoraMin.Text = "00"
    EcMinutoMin.Text = "00"
End Sub

Private Sub EcDtPretend_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
   
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtPretend)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDtPretend_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDtPretend_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcHoraMin_Validate(Cancel As Boolean)
    
    If Not IsNumeric(EcHoraMin) Then
        EcHoraMin = "0"
    Else
        EcHoraMin = CInt(EcHoraMin)
        If EcHoraMin < 0 Or EcHoraMin > 23 Then
            EcHoraMin = "0"
        End If
    End If
End Sub
Private Sub EcHoraMax_Validate(Cancel As Boolean)
    If Not IsNumeric(EcHoraMax) Then
        EcHoraMax = "23"
    Else
        EcHoraMax = CInt(EcHoraMax)
        If EcHoraMax < 0 Or EcHoraMax > 23 Then
            EcHoraMax = "23"
        End If
    End If
End Sub
Private Sub EcMinutoMin_Validate(Cancel As Boolean)
    If Not IsNumeric(EcMinutoMin) Then
        EcMinutoMin = "0"
    Else
        EcMinutoMin = CInt(EcMinutoMin)
        If EcMinutoMin < 0 Or EcMinutoMin > 59 Then
            EcMinutoMin = ""
        End If
    End If
End Sub
Private Sub EcMinutoMax_Validate(Cancel As Boolean)
    If Not IsNumeric(EcMinutoMax) Then
        EcMinutoMax = "59"
    Else
        EcMinutoMax = CInt(EcMinutoMax)
        If EcMinutoMax < 0 Or EcMinutoMax > 59 Then
            EcMinutoMax = "59"
        End If
    End If
End Sub
Private Sub EcReq_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    Dim j As Integer
    Dim flg_encontrou As Boolean
    
    flg_encontrou = False
    If KeyCode = 13 Then
        FGReqPendRes.topRow = 1
        If Trim(EcReq) <> "" Then
            For i = 1 To TotalDadosRequis
                If EstrutDadosRequis(i).num_requis = EcReq Then
                
                    ValMinimo = 1 + (NumRequisicoes * CInt(i \ NumRequisicoes))
                    ValMaximo = NumRequisicoes + (NumRequisicoes * CInt(i \ NumRequisicoes))
                    LbPagina.caption = CInt(i \ NumRequisicoes) + 1
                    DoEvents
                    
                    FGReqPendRes.Clear
                    FGReqPendRes.Cols = 10
                    FGReqPendRes.rows = 23
                    FGReqPendRes.row = 0
                    linhaActual = 0
                        
                    FGReqPendRes.Enabled = False
                    For j = ValMinimo To ValMaximo
                        If j > TotalDadosRequis Then
                            Exit For
                        End If
                        PreencheFlexGrid EstrutDadosRequis(j).num_requis, EstrutDadosRequis(j).urgente, _
                                         EstrutDadosRequis(j).servico, EstrutDadosRequis(j).dt_chega, _
                                         EstrutDadosRequis(j).hr_chega, EstrutDadosRequis(j).tipo_urgencia, _
                                         EstrutDadosRequis(j).nome, EstrutDadosRequis(j).dt_confirmacao, EstrutDadosRequis(j).cor_urgencia, j
                    Next
                    FGReqPendRes.Enabled = True
                    
                End If
            Next
            
            For i = 1 To FGReqPendRes.rows - 1
                If FGReqPendRes.TextMatrix(i, lColNumReq) = EcReq Then
                    FGReqPendRes.topRow = i
                    Exit For
                End If
            Next
            Sendkeys ("{HOME}+{END}")
        End If
    End If
    
End Sub

Private Sub FGReqPendRes_Click()
    FrOpcoes.Visible = False
End Sub

Private Sub FGReqPendRes_DblClick()
    If gCodGrupo = gGrupoSecretariado And gPermResUtil <> cValMedRes Then
        Exit Sub
    End If
    ' SE LISTA TIVER MAIS QUE UM ELEMENTO TRABALHA SEMPRE APENAS COM PRIMEIRO.
    EcGrAnalises = ""
    If EcListaGrAna.ListCount > 0 Then
        EcGrAnalises = BL_SelCodigo("SL_GR_ANA", "COD_GR_ANA", "SEQ_GR_ANA", EcListaGrAna.ItemData(0))
    End If
    If EcGrAnalises = CStr(gCodGrupoMicrobiologia) Or EcGrAnalises = CStr(gCodGrupoMicobacteriologia) Then

            ModoResultadosNovo


    Else
        If gNovoEcraResultados = mediSim Then
            ModoResultadosNovo
        Else
            ModoResultados
        End If
    End If
End Sub


Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    DefTipoCampos
    'PreencheValoresDefeito
    'OptSimples_Click
    If EcData.Text = "" Then
        EcData.Text = "ALL"
    End If
    'FuncaoProcurar

    BG_ParametrizaPermissoes_ADO Me.Name
    estado = 1
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    gF_REQPENDANA = 1
End Sub

Sub Inicializacoes()

    Me.caption = " Requisi��es Pendentes Por An�lise"
    Me.left = 5
    Me.top = 30
    Me.Width = 15300  ' 12235
    Me.Height = 9450  ' Normal
    
    Flg_Resultados = False
    Flg_PassaProximaReq = True
    
    linhaActual = 0
    
    tempo = gDiasReqPendAna
    LaTempo = "Per�odo de " & tempo & " Dias"
    
    FrOpcoes.Visible = False
    Set CampoDeFocus = EcReq

    
    If BL_SeleccionaGrupoAnalises = True Then
        ' FM
        'EcGrAnalises = gCodGrAnaUtilizador
        'EcGrAnalises_Validate False
    End If
    
    FGReqPendRes.ScrollTrack = True
    
    LbPagina.caption = 1
    NumRequisicoes = 30
    ValMinimo = 1
    ValMaximo = NumRequisicoes
    

    If gPermResUtil = "2" Then
        EstadosNaoValidados = " (sl_realiza.flg_estado = " & BL_TrataStringParaBD(gEstadoAnaComResultado) & " or sl_realiza.flg_estado =" & BL_TrataStringParaBD(gEstadoAnaValidacaoTecnica) & ")"
    Else
        EstadosNaoValidados = " (sl_realiza.flg_estado = " & BL_TrataStringParaBD(gEstadoAnaComResultado) & ")"
    End If
    EcPrinterEtiq.Text = BL_SelImpressora("Etiqueta.rpt")
    ReDim Tubos(0)
    Opt1(0).value = True

End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    gF_REQPENDANA = 1
    
    
    
    If Flg_Resultados = False Or CkValidacaoSequencial = False Then
        PreencheValoresDefeito
    Else
        If FGReqPendRes.row = FGReqPendRes.rows - 1 Then
            PreencheValoresDefeito
        ElseIf FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq) = "" Then
            PreencheValoresDefeito
        Else
            If Flg_PassaProximaReq = True Then
                While FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq) = FGReqPendRes.TextMatrix(FGReqPendRes.row + 1, lColNumReq)
                    FGReqPendRes.row = FGReqPendRes.row + 1
                Wend
                FGReqPendRes.row = FGReqPendRes.row + 1
                FGReqPendRes_DblClick
            Else
                PreencheValoresDefeito
            End If
        End If
    End If
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    gF_REQPENDANA = 0
    Set FormReqPendAna = Nothing
    
End Sub

Sub LimpaCampos()
    
    'Me.SetFocus
    FGReqPendRes.Clear
    FGReqPendRes.Cols = colunas
    FGReqPendRes.rows = 23
    FGReqPendRes.row = 0
    linhaActual = 0
    TotalDadosRequis = 0
    ReDim EstrutDadosRequis(TotalDadosRequis)
    'ValMinimo = 1
    'ValMaximo = NumRequisicoes
    'LbPagina.Caption = 1
    ReDim Tubos(0)
    TabFiltros.Tab = 0
    FrameLegenda.Visible = False
    'Opt(0).Value = True
    'Opt2(0).Value = True
    'OptOrdena(5).Value = True
End Sub

Sub DefTipoCampos()
    Dim i As Integer
    FrameLegenda.Visible = False
    If gLAB = "HOSPOR" Then
        Opt2(1).value = True
    Else
        Opt2(0).value = True
    End If
    
    With FGReqPendRes
        .rows = 23
        .FixedRows = 0
        .Cols = colunas
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(lColLimite) = 100
        .Col = lColLimite
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColNumReq) = 700
        .Col = lColNumReq
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColProven) = 3300
        .Col = lColProven
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColDtChega) = 900
        .Col = lColDtChega
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColHrChega) = 500
        .Col = lColHrChega
        .CellAlignment = flexAlignCenterCenter
        For i = lColHrChega + 1 To colunas - 1
            .ColWidth(i) = 990
            .Col = i
            .CellAlignment = flexAlignCenterCenter
        Next
        .row = 1
        .Col = 0
        
    End With
    ' FM
    'BG_PreencheComboBD_ADO "gr_empr_inst", "empresa_id", "nome_empr", CbLocal
    'Dim i As Integer
    'For i = 0 To CbLocal.ListCount - 1
    '    If gCodLocal = CbLocal.ItemData(i) Then
    '        CbLocal.text = CbLocal.List(i)
    '    End If
    'Next i
    
    If gImprimeEtiqPendentes <> mediSim Then
        BtImprimeEtiq.Visible = False
    End If
    
    If gTipoInstituicao = "HOSPITALAR" Then
        TabFiltros.TabCaption(2) = "Proveni�ncias"
    ElseIf gTipoInstituicao = "PRIVADA" Then
        TabFiltros.TabCaption(2) = "Postos"
    End If
    EcDtIni.value = CDate(Bg_DaData_ADO - tempo)
    EcDtFim.value = Bg_DaData_ADO
    EcHoraMin.Text = Format(time, "hh")
    EcMinutoMin.Text = Format(time, "nn")
    If gLAB <> "HOSPOR" Then
        EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "cod_empr", gCodLocal)
        EcListaLocais.ItemData(EcListaLocais.NewIndex) = gCodLocal
    End If
    If gCodGrAnaUtilizador <> "" And BL_SeleccionaGrupoAnalises = True Then
        'FN 31-08-2009
        Dim rsGrAna As ADODB.recordset
        Dim sql As String
        sql = "SELECT seq_gr_ana, cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana=" & BL_TrataStringParaBD(gCodGrAnaUtilizador)
        Set rsGrAna = New ADODB.recordset
        rsGrAna.CursorLocation = adUseServer
        rsGrAna.CursorType = adOpenStatic
        rsGrAna.Open sql, gConexao
        If rsGrAna.RecordCount > 0 Then
            While Not rsGrAna.EOF
                EcListaGrAna.AddItem BL_HandleNull(rsGrAna!descr_gr_ana, "")
                EcListaGrAna.ItemData(EcListaGrAna.NewIndex) = BL_HandleNull(rsGrAna!seq_gr_ana, -1)
                
                rsGrAna.MoveNext
            Wend
        End If
        rsGrAna.Close
        Set rsGrAna = Nothing
        
        '----- No caso do campo cod_gr_ana ser alfanumerico d� erro na fun��o BL_SelCodigo
        'EcListaGrAna.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "cod_gr_ana", gCodGrAnaUtilizador)
        'EcListaGrAna.ItemData(EcListaGrAna.NewIndex) = BL_SelCodigo("sl_gr_ana", "seq_gr_ana", "cod_gr_ana", gCodGrAnaUtilizador)
    End If
    
    If gCodGrTrabUtilizador <> "" And BL_SeleccionaGrupoAnalises = True Then
        Dim rsGrTrab As ADODB.recordset
        sql = "SELECT seq_gr_trab, cod_gr_trab, descr_gr_trab FROM sl_gr_trab WHERE cod_gr_trab=" & BL_TrataStringParaBD(gCodGrTrabUtilizador)
        Set rsGrTrab = New ADODB.recordset
        rsGrTrab.CursorLocation = adUseServer
        rsGrTrab.CursorType = adOpenStatic
        rsGrTrab.Open sql, gConexao
        If rsGrTrab.RecordCount > 0 Then
            While Not rsGrTrab.EOF
                EcListaGrTrab.AddItem BL_HandleNull(rsGrTrab!descr_gr_trab, "")
                EcListaGrTrab.ItemData(EcListaGrTrab.NewIndex) = BL_HandleNull(rsGrTrab!seq_gr_trab, -1)
                rsGrTrab.MoveNext
            Wend
        End If
        rsGrTrab.Close
        Set rsGrTrab = Nothing
    End If
    
    If gNovoEcraResultados <> mediSim Then
        BtResutadosNovo.Visible = False
    End If
    
    If gPermResUtil = cIntRes Then
        LbValidacao.caption = "An�lise Com Resultado/Validada"
    Else
        LbValidacao.caption = "An�lise Validada"
    End If
    If gUsaHoraPendentes = mediSim Then
        EcHoraMax.Visible = True
        EcHoraMin.Visible = True
        EcMinutoMax.Visible = True
        EcMinutoMin.Visible = True
        BtHoraMax.Visible = True
        BtMinutoMax.Visible = True
        BtHoraMin.Visible = True
        BtMinutoMin.Visible = True
    Else
        EcHoraMax.Visible = False
        EcHoraMin.Visible = False
        EcMinutoMax.Visible = False
        EcMinutoMin.Visible = False
        BtHoraMax.Visible = False
        BtMinutoMax.Visible = False
        BtHoraMin.Visible = False
        BtMinutoMin.Visible = False
    End If
    If gOrdenaPendentesNumReq = mediSim Then
        OptOrdena(4).value = True
    Else
        OptOrdena(5).value = True
    End If
    BG_DefTipoCampoEc_ADO "sl_requis", "dt_pretend", EcDtPretend, mediTipoData
    TabFiltros.Tab = 0
    
    If BL_HandleNull(gMinutosRefreshPendentes, 0) > 0 Then
        IDLETimerRefresh.MinutosDeIntervalo = BL_HandleNull(gMinutosRefreshPendentes, 0)
    Else
        IDLETimerRefresh.MinutosDeIntervalo = 99
    End If
End Sub

Sub FuncaoLimpar()
    ordenacao = ""
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        Me.caption = " Requisi��es Pendentes Por An�lise"
        
        ' pferreira 2010.04.22
        EcData.Text = "ALL"
        
        LimpaCampos
        'CampoDeFocus.SetFocus
        
        ' FM
        'If BL_SeleccionaGrupoAnalises = True Then
        '    EcGrAnalises = gCodGrAnaUtilizador
        '    EcGrAnalises_Validate False
        'End If
    End If
    CkCompletas.value = 0
    CkUrgentes.value = 0
    CkValidacaoSequencial.value = 0
    
    ' PFerreira 2007.07.27
    ' Limpa o controlo da checkbox 'CkUserAltert'
    CkUserAltert.value = vbUnchecked
    
    
    CkTodas.value = 0
    CkTodas.caption = "Todas as requisi��es "

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
    
End Sub

Sub PreencheValoresDefeito()
    'OptSimples_Click
    If EcData.Text = "" Then
        EcData.Text = "ALL"
    End If
    If gUsaDataTuboPendentes = mediSim Then
        OptData(1).value = True
    Else
        OptData(0).value = True
    End If
    
    PreencheSalaDefeito
    PreencheEstrutLimiteCor
End Sub

Sub FuncaoProcurar()
    'ValMinimo = 1
    'ValMaximo = NumRequisicoes
    'LbPagina.Caption = 1
    Me.Enabled = False
    EcReq = ""
    If EcData <> "" Then
        FGReqPendRes.Clear
        PreencheFGReqPendRes
    Else
        BG_Mensagem mediMsgBox, "Tem de Escolher uma data para a pesquisa!", vbOKOnly + vbExclamation, "Aten��o!"
    End If
    Me.Enabled = True
    'OptOrdena(5).Value = True
    
End Sub

Sub FuncaoImprimir()
    Dim DtChega As String
    Dim RsPendRes As New ADODB.recordset
    Dim i As Integer
    Dim sSql As String
    Dim continua As Boolean
    Dim descrImp As String
    Dim GrAna As String
    Dim GrTrab As String
    Dim Proven As String
    
    TotalAnaImpr = 0
    ReDim EstrutAnaImpr(TotalAnaImpr)
    
    TotalDadosRequisImpr = 0
    ReDim EstrutDadosRequisImpr(TotalDadosRequisImpr)
    
    If EcDtIni <> "" And EcDtFim <> "" And EcDtFim >= EcDtIni Then
        lDataInicial = EcDtIni
        
        
        ' FORMATO DA HORA INICIAL
        If Len(EcHoraMin) = 1 Then
            lHoraInicial = "0" & EcHoraMin
        Else
            lHoraInicial = EcHoraMin
        End If
        
        If Len(EcMinutoMin) = 1 Then
            lHoraInicial = lHoraInicial & ":0" & EcMinutoMin
        Else
            lHoraInicial = lHoraInicial & ":" & EcMinutoMin
        End If
        
        ' FORMATO DA HORA FINAL
        If Len(EcHoraMax) = 1 Then
            lHoraFinal = "0" & EcHoraMax
        Else
            lHoraFinal = EcHoraMax
        End If
        
        If Len(EcMinutoMax) = 1 Then
            lHoraFinal = lHoraFinal & ":0" & EcMinutoMax
        Else
            lHoraFinal = lHoraFinal & ":" & EcMinutoMax
        End If
        
        lDataFinal = EcDtFim
    Else
        lDataInicial = ""
        lDataFinal = ""
        lHoraInicial = "00:00"
        lHoraFinal = "23:59"
    End If

    
    RsPendRes.CursorType = adOpenStatic
    RsPendRes.CursorLocation = adUseServer
            
    sSql = ConstroiSQL("slv_analises", "cod_ana")

    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsPendRes.Open sSql, gConexao, adOpenStatic
    If RsPendRes.RecordCount > 0 Then
        i = 1
        While Not RsPendRes.EOF
            PreencheEstrutRequisImpr RsPendRes!n_req, RsPendRes!t_urg, BL_HandleNull(RsPendRes!descr_proven, ""), BL_HandleNull(RsPendRes!dt_chega, "---"), _
                                     BL_HandleNull(RsPendRes!hr_chega, "---"), BL_HandleNull(RsPendRes!dt_conclusao, ""), BL_HandleNull(RsPendRes!dt_pretend, ""), _
                                     BL_HandleNull(RsPendRes!dt_nasc_ute, ""), BL_HandleNull(RsPendRes!n_Req_assoc, "")
            RsPendRes.MoveNext
            i = i + 1
        Wend
        RsPendRes.Close
        Set RsPendRes = Nothing
        PreencheTabela
        
        descrImp = ""
        gImprimirDestino = 0
        If gImprimirDestino = 1 Then
            continua = BL_IniciaReport("ListagemPendAna", "Listagem Pendentes de An�lises", crptToPrinter, False, , , , , descrImp)
        Else
            continua = BL_IniciaReport("ListagemPendAna", "Listagem Pendentes de An�lises", crptToWindow, , , , , , descrImp)
        End If
        
        If continua = False Then Exit Sub
        
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        
        Report.SQLQuery = "SELECT GR_EMPR_INST.NOME_EMPR " & _
                        " FROM GR_EMPR_INST " & _
                        " WHERE estado = 'S'"
        If EcData <> "ALL" Then
            Report.formulas(0) = "Data=" & BL_TrataStringParaBD(EcData)
        Else
            Report.formulas(0) = "Data='(Todas as datas)'"
        End If

        If EcListaGrAna.ListCount > 0 Then
            For i = 0 To EcListaGrAna.ListCount - 1
                GrAna = GrAna & EcListaGrAna.List(i) & ";"
            Next i
            GrAna = Mid(GrAna, 1, Len(GrAna) - 1)
            Report.formulas(1) = "GrupoAna=" & BL_TrataStringParaBD(GrAna)
        Else
            Report.formulas(1) = "GrupoAna=" & BL_TrataStringParaBD("Todos")
        End If
        If EcListaGrTrab.ListCount > 0 Then
            For i = 0 To EcListaGrTrab.ListCount - 1
                GrTrab = GrTrab & EcListaGrTrab.List(i) & ";"
            Next i
            GrTrab = Mid(GrTrab, 1, Len(GrTrab) - 1)
            Report.formulas(2) = "GrupoTrab=" & BL_TrataStringParaBD(GrTrab)
        Else
            Report.formulas(2) = "GrupoTrab=" & BL_TrataStringParaBD("Todos")
        End If
        If EcListaProven.ListCount > 0 Then
            For i = 0 To EcListaProven.ListCount - 1
                Proven = Proven & EcListaProven.List(i) & ";"
            Next i
            Proven = Mid(Proven, 1, Len(Proven) - 1)
            Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD(Proven)
        Else
            Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
        End If
        Report.SubreportToChange = "SubListagemPendAna1"
        Report.SelectionFormula = "{sl_cr_pend_ana.nome_computador} = '" & BG_SYS_GetComputerName & "' AND {sl_cr_pend_ana.estado} ='ANALISE SEM RESULTADO'"
        Report.SubreportToChange = "SubListagemPendAna2"
        Report.SelectionFormula = "{sl_cr_pend_ana.nome_computador} = '" & BG_SYS_GetComputerName & "' AND {sl_cr_pend_ana.estado} ='ANALISE SEM VALIDACAO'"
        Report.SubreportToChange = ""
        Report.WindowState = crptMaximized
        Call BL_ExecutaReport(True)
        
    End If
End Sub
    



Sub PreencheFGReqPendRes()
    Dim RsPendRes As ADODB.recordset
    Dim sSql As String
    Dim totalReq As Long
    Dim i As Long
    On Error GoTo TrataErro
    Me.Enabled = False
    If EcDtIni <> "" And EcDtFim <> "" And EcDtFim >= EcDtIni Then
        lDataInicial = EcDtIni
        lDataFinal = EcDtFim
        
        ' FORMATO DA HORA INICIAL
        If Len(EcHoraMin) = 1 Then
            lHoraInicial = "0" & EcHoraMin
        Else
            lHoraInicial = EcHoraMin
        End If
        
        If Len(EcMinutoMin) = 1 Then
            lHoraInicial = lHoraInicial & ":0" & EcMinutoMin
        Else
            lHoraInicial = lHoraInicial & ":" & EcMinutoMin
        End If
        
        ' FORMATO DA HORA FINAL
        If Len(EcHoraMax) = 1 Then
            lHoraFinal = "0" & EcHoraMax
        Else
            lHoraFinal = EcHoraMax
        End If
        
        If Len(EcMinutoMax) = 1 Then
            lHoraFinal = lHoraFinal & ":0" & EcMinutoMax
        Else
            lHoraFinal = lHoraFinal & ":" & EcMinutoMax
        End If
    ElseIf EcData <> "ALL" And EcData <> "" Then
        lDataInicial = EcData
        lDataFinal = EcData
    Else
        lDataInicial = ""
        lDataFinal = ""
    End If
        
    Set RsPendRes = New ADODB.recordset
    RsPendRes.CursorType = adOpenStatic
    RsPendRes.CursorLocation = adUseServer
    
    LimpaCampos
        

    sSql = ConstroiSQL("slv_analises", "cod_ana")
    Set RsPendRes = New ADODB.recordset
    RsPendRes.CursorType = adOpenStatic
    RsPendRes.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsPendRes.Open sSql, gConexao
    If RsPendRes.RecordCount > 0 Then
        i = 1
        While Not RsPendRes.EOF
            PreencheEstrutRequis RsPendRes!n_req, RsPendRes!t_urg, BL_HandleNull(RsPendRes!descr_proven, ""), _
                                 BL_HandleNull(RsPendRes!dt_chega, "---"), BL_HandleNull(RsPendRes!hr_chega, "---"), _
                                 BL_HandleNull(RsPendRes!tipo_urgencia, ""), BL_HandleNull(RsPendRes!nome_ute, ""), _
                                 BL_HandleNull(RsPendRes!dt_conclusao, "---"), BL_HandleNull(RsPendRes!dt_pretend, "---"), _
                                 BL_HandleNull(RsPendRes!dt_confirm, ""), BL_HandleNull(RsPendRes!descr_cm_tec, ""), _
                                 BL_HandleNull(RsPendRes!cor, ""), BL_HandleNull(RsPendRes!limite, 0)
            RsPendRes.MoveNext
            i = i + 1
        Wend
        FGReqPendRes.Visible = False
        FGReqPendRes.Enabled = False
        For i = ValMinimo To ValMaximo
            If i <= TotalDadosRequis Then
                PreencheFlexGrid EstrutDadosRequis(i).num_requis, EstrutDadosRequis(i).urgente, EstrutDadosRequis(i).servico, _
                                 EstrutDadosRequis(i).dt_chega, EstrutDadosRequis(i).hr_chega, EstrutDadosRequis(i).tipo_urgencia, _
                                 EstrutDadosRequis(i).nome, EstrutDadosRequis(i).dt_confirmacao, EstrutDadosRequis(i).cor_urgencia, CInt(i)
            Else
                Exit For
            End If
        Next
        FGReqPendRes.Enabled = True
        FGReqPendRes.Visible = True
    
    End If
    Me.Enabled = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar Requisi��es:" & Err.Description, Me.Name, "PreencheFgReqPendRes", True
    Exit Sub
    Resume Next
End Sub



Private Sub Opt_Click(Index As Integer)
    EcListaAnalises.Clear
End Sub




Private Sub IDLETimerRefresh_Inactividade()
    If gMinutosRefreshPendentes >= 0 Then
        FuncaoProcurar
        IDLETimerRefresh.MinutosDeIntervalo = gMinutosRefreshPendentes
    End If
End Sub

Private Sub OptOrdena_Click(Index As Integer)
    Dim i As Integer
    Select Case Index
        Case 0
            Call OrdenaEstruturaRequisicoes(ordenaTriagem, cSortOrderAsc)
        Case 1
            Call OrdenaEstruturaRequisicoes(ordenaNome, cSortOrderAsc)
        Case 2
            Call OrdenaEstruturaRequisicoes(ordenaServico, cSortOrderAsc)
        Case 3
            Call OrdenaEstruturaRequisicoes(ordenaDtChega, cSortOrderAsc)
        Case 4
            Call OrdenaEstruturaRequisicoes(ordenaNumRequis, cSortOrderAsc)
        Case 5
            Call OrdenaEstruturaRequisicoes(ordenaTUrg, cSortOrderDesc)
    End Select
    FGReqPendRes.Visible = False
    FGReqPendRes.Enabled = False
    LimpaFGReq
    For i = ValMinimo To ValMaximo
        If i <= TotalDadosRequis Then
            PreencheFlexGrid EstrutDadosRequis(i).num_requis, EstrutDadosRequis(i).urgente, EstrutDadosRequis(i).servico, _
                             EstrutDadosRequis(i).dt_chega, EstrutDadosRequis(i).hr_chega, EstrutDadosRequis(i).tipo_urgencia, _
                             EstrutDadosRequis(i).nome, EstrutDadosRequis(i).dt_confirmacao, EstrutDadosRequis(i).cor_urgencia, CInt(i)
        Else
            Exit For
        End If
    Next
    FGReqPendRes.Enabled = True
    FGReqPendRes.Visible = True
End Sub

Private Sub PreencheFlexGrid(n_req As String, t_urg As String, servico As String, dt_chega As String, hr_chega As String, _
                             tipo_urgencia As String, nome As String, dt_confirmacao As String, cor_urgencia As String, indice As Integer)
    On Error GoTo TrataErro
    If linhaActual >= FGReqPendRes.rows - 1 Then
        FGReqPendRes.AddItem "", linhaActual
    End If
    FGReqPendRes.TextMatrix(linhaActual, lColNumReq) = n_req
    If CkMostrarNome.value = vbChecked Then
        FGReqPendRes.TextMatrix(linhaActual, lColProven) = nome
    Else
        FGReqPendRes.TextMatrix(linhaActual, lColProven) = servico
    End If
    If dt_confirmacao <> "" Then
        FGReqPendRes.TextMatrix(linhaActual, lColProven) = FGReqPendRes.TextMatrix(linhaActual, lColProven) & " - REQ.CONFIRMADA"
    End If
    FGReqPendRes.TextMatrix(linhaActual, lColDtChega) = dt_chega
    FGReqPendRes.TextMatrix(linhaActual, lColHrChega) = hr_chega
   
    ' pferreira 2007.10.24
    ' Coloca a cor de acordo com o tipo de urgencia
    If (tipo_urgencia <> "") Then
        FGReqPendRes.Col = lColProven
        FGReqPendRes.row = linhaActual
        FGReqPendRes.CellBackColor = BL_DevolveCorTipoUrgencia(tipo_urgencia)
    End If
    
    
    FGReqPendRes.Col = lColNumReq
    FGReqPendRes.row = linhaActual
    FGReqPendRes.CellBackColor = BL_HandleNull(cor_urgencia, vbWhite)
    FGReqPendRes.Col = lColLimite
    FGReqPendRes.CellBackColor = DevolveCorLimite(EstrutDadosRequis(indice).percentagem)
    
    PreencheFlexGrid_Analises indice
    
    'INCREMENTA A LINHA ACTUAL
    linhaActual = linhaActual + 1
    DoEvents
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "PreencheFlexGrid", False
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheAnalises(indice As Integer, n_req As String, tipo_urgencia As String)
    Dim sSql As String
    Dim rs As New ADODB.recordset
    Dim i As Integer
    Dim linhasUtilizadas As Integer
    On Error GoTo TrataErro
    
    sSql = ControiCriterioPreencheAna(n_req, tipo_urgencia)
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao, adOpenStatic
    
    If rs.RecordCount > 0 Then
        While Not rs.EOF
            EstrutDadosRequis(indice).totalAnalises = EstrutDadosRequis(indice).totalAnalises + 1
            ReDim Preserve EstrutDadosRequis(indice).analises(EstrutDadosRequis(indice).totalAnalises)
            EstrutDadosRequis(indice).analises(EstrutDadosRequis(indice).totalAnalises).abr_ana = BL_HandleNull(rs!Descr, "")
            EstrutDadosRequis(indice).analises(EstrutDadosRequis(indice).totalAnalises).cod_ana = BL_HandleNull(rs!cod_ana, "")
            EstrutDadosRequis(indice).analises(EstrutDadosRequis(indice).totalAnalises).cod_tubo = BL_HandleNull(rs!cod_tubo, "")
            EstrutDadosRequis(indice).analises(EstrutDadosRequis(indice).totalAnalises).descr_ana = BL_HandleNull(rs!descr_ana, "")
            EstrutDadosRequis(indice).analises(EstrutDadosRequis(indice).totalAnalises).flg_estado = BL_HandleNull(rs!flg_estado, "-1")
            EstrutDadosRequis(indice).analises(EstrutDadosRequis(indice).totalAnalises).Ord_Marca = BL_HandleNull(rs!Ord_Marca, 0)
                
            rs.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, Me.Name, "PreencheAnalises", True
    Exit Sub
    Resume Next
End Sub
Private Sub PreencheFlexGrid_Analises(indice As Integer)
    Dim linhasUtilizadas As Integer
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    linhasUtilizadas = 1
    i = 1
    If EstrutDadosRequis(indice).totalAnalises <= 0 Then
        PreencheAnalises indice, EstrutDadosRequis(indice).num_requis, EstrutDadosRequis(indice).tipo_urgencia
    End If
    
    For j = 1 To EstrutDadosRequis(indice).totalAnalises
        
        If i = (colunas - lColHrChega) Then
            ' SE O NUMERO DE ANALISES FOR MAIOR QUE O NUMERO DE COLUNAS MUDA DE LINHA E INCREMENTA
            linhasUtilizadas = linhasUtilizadas + 1
            linhaActual = linhaActual + 1
            If linhaActual >= 22 Then
                FGReqPendRes.AddItem "", linhaActual
            End If
            i = 1 'volta a por no primeira celula possivel
            FGReqPendRes.TextMatrix(linhaActual, lColLimite) = FGReqPendRes.TextMatrix(linhaActual - 1, lColLimite)
            FGReqPendRes.TextMatrix(linhaActual, lColNumReq) = FGReqPendRes.TextMatrix(linhaActual - 1, lColNumReq)
            FGReqPendRes.TextMatrix(linhaActual, lColProven) = FGReqPendRes.TextMatrix(linhaActual - 1, lColProven)
            FGReqPendRes.TextMatrix(linhaActual, lColDtChega) = FGReqPendRes.TextMatrix(linhaActual - 1, lColDtChega)
            FGReqPendRes.TextMatrix(linhaActual, lColHrChega) = FGReqPendRes.TextMatrix(linhaActual - 1, lColHrChega)
            
            
            ' coloca a cor do limite
            FGReqPendRes.row = linhaActual
            FGReqPendRes.Col = lColLimite
            FGReqPendRes.CellBackColor = DevolveCorLimite(EstrutDadosRequis(indice).percentagem)
            
            
            'coloca a cor de acordo com o tipo de urg�ncia
            If (EstrutDadosRequis(indice).tipo_urgencia <> "") Then
                FGReqPendRes.Col = lColProven
                FGReqPendRes.row = linhaActual
                FGReqPendRes.CellBackColor = BL_DevolveCorTipoUrgencia(EstrutDadosRequis(indice).tipo_urgencia)
            End If
            FGReqPendRes.MergeCol(lColNumReq) = True
            
            
        End If
        If gImprimeEtiqPendentes = mediSim Then
            PreenhceEstrutTubos EstrutDadosRequis(indice).num_requis, EstrutDadosRequis(indice).analises(j).cod_ana
        End If
        FGReqPendRes.TextMatrix(linhaActual, lColHrChega + i) = BL_HandleNull(EstrutDadosRequis(indice).analises(j).abr_ana, "---")
        FGReqPendRes.Col = lColHrChega + i
        FGReqPendRes.row = linhaActual
        
        
        If Trim(EstrutDadosRequis(indice).analises(j).flg_estado) = gEstadoAnaSemResultado Then
            
            If BL_HandleNull(EstrutDadosRequis(indice).analises(j).cod_tubo, "") <> "" And gMostraAnalisesSemTubo = mediSim Then
                If CkRequisComTubo.value = vbUnchecked Then
                    If VerificaTemTubo(EstrutDadosRequis(indice).num_requis, EstrutDadosRequis(indice).analises(j).cod_tubo) = False Then
                        If gMostraAnalisesSemTubo = mediSim Then
                            FGReqPendRes.CellBackColor = Branco
                        Else
                            FGReqPendRes.CellBackColor = Amarelo
                        End If
                    Else
                        FGReqPendRes.CellBackColor = Amarelo
                    End If
                Else
                    FGReqPendRes.CellBackColor = Amarelo
                End If
            ElseIf gMostraAnalisesSemTubo = mediSim Then
                'CHVNG-Espinho: Se a analise nao tem tubo, vai ver os seus membros se t�m tubo
                If Mid(EstrutDadosRequis(indice).analises(j).cod_ana, 1, 1) = "P" Then
                    Dim rsTubo As ADODB.recordset
                    Dim ChegouTuboMembro As Boolean
                    Set rsTubo = New ADODB.recordset
                    rsTubo.CursorLocation = adUseServer
                    rsTubo.CursorType = adOpenStatic
                    rsTubo.Source = "select cod_ana,cod_tubo from sl_ana_perfis, slv_analises where sl_ana_perfis.cod_analise = slv_analises.cod_ana and sl_ana_perfis.cod_perfis = '" & EstrutDadosRequis(indice).analises(j).cod_ana & "'"
                    rsTubo.Open , gConexao
                    If rsTubo.RecordCount > 0 Then
                        While Not rsTubo.EOF
                            If BL_HandleNull(rsTubo!cod_tubo, "") <> "" Then
                                If CkRequisComTubo.value = vbUnchecked Then
                                    If VerificaTemTubo(EstrutDadosRequis(indice).num_requis, rsTubo!cod_tubo) = True Then
                                        ChegouTuboMembro = True
                                    End If
                                Else
                                    ChegouTuboMembro = True
                                End If
                            ElseIf Mid(rsTubo!cod_ana, 1, 1) = "C" Then
                                Dim rsTubo2 As New ADODB.recordset
                                If rsTubo2.state = adStateOpen Then
                                    rsTubo2.Close
                                End If
                                rsTubo2.CursorLocation = adUseServer
                                rsTubo2.CursorType = adOpenStatic
                                rsTubo2.Source = "select cod_ana,cod_tubo from sl_membro, slv_analises where sl_membro.cod_ana_c = '" & EstrutDadosRequis(indice).analises(j).cod_ana & "'"
                                rsTubo2.Open , gConexao
                                If rsTubo2.RecordCount > 0 Then
                                    While Not rsTubo2.EOF
                                        If BL_HandleNull(rsTubo2!cod_tubo, "") <> "" Then
                                            If CkRequisComTubo.value = vbUnchecked Then
                                                If VerificaTemTubo(EstrutDadosRequis(indice).num_requis, rsTubo2!cod_tubo) = True Then
                                                    ChegouTuboMembro = True
                                                End If
                                            Else
                                                ChegouTuboMembro = True
                                            End If
                                        Else
                                            ChegouTuboMembro = True
                                        End If
                                        rsTubo2.MoveNext
                                    Wend
                                End If
                            Else
                                ChegouTuboMembro = True
                            End If
                            rsTubo.MoveNext
                        Wend
                    End If
                ElseIf Mid(EstrutDadosRequis(indice).analises(j).cod_ana, 1, 1) = "C" Then
                    Set rsTubo = New ADODB.recordset
                    rsTubo.CursorLocation = adUseServer
                    rsTubo.CursorType = adOpenStatic
                    rsTubo.Source = "select cod_ana,cod_tubo from sl_membro, slv_analises where sl_membro.cod_membro = slv_analises.cod_ana and sl_membro.cod_ana_c = '" & EstrutDadosRequis(indice).analises(j).cod_ana & "'"
                    rsTubo.Open , gConexao
                    While Not rsTubo.EOF
                        If BL_HandleNull(rsTubo!cod_tubo, "") <> "" Then
                            If CkRequisComTubo.value = vbUnchecked Then
                                If VerificaTemTubo(EstrutDadosRequis(indice).num_requis, rsTubo!cod_tubo) = True Then
                                    ChegouTuboMembro = True
                                End If
                            Else
                                ChegouTuboMembro = True
                            End If
                        Else
                            ChegouTuboMembro = True
                        End If
                        rsTubo.MoveNext
                    Wend
                Else
                    ChegouTuboMembro = True
                End If
                If ChegouTuboMembro = True Then
                    FGReqPendRes.CellBackColor = Amarelo
                Else
                    FGReqPendRes.CellBackColor = Branco
                End If
            Else
                FGReqPendRes.CellBackColor = Amarelo
            End If
        ElseIf EstrutDadosRequis(indice).analises(j).flg_estado = gEstadoAnaImpressa And gMostraAnalisesImpressas = mediSim Then
            FGReqPendRes.CellBackColor = AzulClaro
        ElseIf gPermResUtil = cValTecRes And (EstrutDadosRequis(indice).analises(j).flg_estado = gEstadoAnaComResultado) Then
            FGReqPendRes.CellBackColor = cinzento
        ElseIf gPermResUtil = cValMedRes And (EstrutDadosRequis(indice).analises(j).flg_estado = gEstadoAnaComResultado Or EstrutDadosRequis(indice).analises(j).flg_estado = gEstadoAnaValidacaoTecnica) Then
            FGReqPendRes.CellBackColor = cinzento
        ElseIf EstrutDadosRequis(indice).analises(j).flg_estado = gEstadoAnaBloqueada Then
            FGReqPendRes.CellBackColor = vermelho
        ElseIf EstrutDadosRequis(indice).analises(j).flg_estado = gEstadoAnaPendente Then
            FGReqPendRes.CellBackColor = laranja
        Else
            FGReqPendRes.CellBackColor = Verde
        
        End If
        i = i + 1
     Next
     
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher An�lises:" & Err.Description, Me.Name, "PreencheFlexGrid_Analises", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaRapidaGrTrab_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_trab"
    CamposEcran(1) = "cod_gr_trab"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_trab"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_trab"
    CWhere = ""
    CampoPesquisa = "descr_gr_trab"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_trab ", _
                                                                           " Grupo Trabalho")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If EcListaGrTrab.ListCount = 0 Then
                    EcListaGrTrab.AddItem BL_SelCodigo("sl_gr_trab", "descr_gr_trab", "seq_gr_trab", resultados(i))
                    EcListaGrTrab.ItemData(0) = resultados(i)
                Else
                    EcListaGrTrab.AddItem BL_SelCodigo("sl_gr_trab", "descr_gr_trab", "seq_gr_trab", resultados(i))
                    EcListaGrTrab.ItemData(EcListaGrTrab.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If
End Sub

Private Sub BtPesquisaProveniencia_Click()
    If gTipoInstituicao = "HOSPITALAR" Then
        PesquisaProven
    ElseIf gTipoInstituicao = "PRIVADA" Then
        PA_PesquisaSalaMultiSel EcListaProven
    End If
End Sub

Private Sub BtPesquisaRapidaGrAnalises_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_ana"
    CWhere = ""
    CampoPesquisa = "descr_gr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_ana ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(resultados(i) <> "") Then
                If EcListaGrAna.ListCount = 0 Then
                    EcListaGrAna.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                    EcListaGrAna.ItemData(0) = resultados(i)
                Else
                    EcListaGrAna.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                    EcListaGrAna.ItemData(EcListaGrAna.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If
End Sub

Private Function ConstroiSQL(Tabela As String, campoCodigo As String) As String
    Dim SqlSELECT1 As String
    Dim SqlSELECT2 As String
    Dim sqlFROM1 As String
    Dim sqlFROM2 As String
    Dim sqlWhere1 As String
    Dim sqlWhere2 As String
    Dim tipo As String
    Dim i As Integer
    Dim Flg_UsaTabela As Boolean
    Dim strData As String
        
    Flg_UsaTabela = False
    'CONSTROI SELECT
    '---------------
    
    If CkUserAltert.value = vbUnchecked Then
        SqlSELECT1 = "SELECT distinct sl_requis.n_req, sl_requis.n_req_assoc, sl_requis.t_urg,sl_tbf_t_urg.cor,  sl_requis.dt_conclusao, sl_requis.dt_pretend, "
        If gTipoInstituicao = gTipoInstituicaoHospitalar Or gTipoInstituicao = gTipoInstituicaoVet Then
            SqlSELECT1 = SqlSELECT1 & "descr_proven, sl_requis.dt_chega,sl_requis.hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, dt_nasc_ute,  sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
        Else
            SqlSELECT1 = SqlSELECT1 & " descr_sala descr_proven, sl_requis.dt_chega,sl_requis.hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
        End If
        
        If CkCompletas.value = 1 Then
            SqlSELECT2 = " SELECT distinct sl_requis.N_req, sl_requis.n_req_assoc, sl_requis.t_urg,sl_tbf_t_urg.cor, sl_realiza.cod_agrup , sl_requis.dt_conclusao, sl_requis.dt_pretend,  "
            If gTipoInstituicao = gTipoInstituicaoHospitalar Or gTipoInstituicao = gTipoInstituicaoVet Then
                SqlSELECT2 = SqlSELECT2 & " descr_proven,sl_requis.dt_chega,sl_requis.hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            Else
                SqlSELECT2 = SqlSELECT2 & " descr_sala descr_proven,sl_requis.dt_chega,sl_requis.hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            End If
        Else
            SqlSELECT2 = " UNION SELECT distinct sl_requis.N_req, sl_requis.n_req_assoc, sl_requis.t_urg, sl_tbf_t_urg.cor, sl_requis.dt_conclusao, sl_requis.dt_pretend,  "
            If gTipoInstituicao = gTipoInstituicaoHospitalar Or gTipoInstituicao = gTipoInstituicaoVet Then
                SqlSELECT2 = SqlSELECT2 & " descr_proven,sl_requis.dt_chega,sl_requis.hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            Else
                SqlSELECT2 = SqlSELECT2 & " descr_sala descr_proven,sl_requis.dt_chega,sl_requis.hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            End If
        End If
    Else
        SqlSELECT1 = "SELECT distinct sl_requis.n_req, sl_requis.n_req_assoc, sl_requis.t_urg,sl_tbf_t_urg.cor, sl_marcacoes.cod_agrup, sl_requis.dt_conclusao, sl_requis.dt_pretend,  "
        
        If gTipoInstituicao = gTipoInstituicaoHospitalar Or gTipoInstituicao = gTipoInstituicaoVet Then
            SqlSELECT1 = SqlSELECT1 & "descr_proven, sl_requis.dt_previ dt_chega ,null hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
        Else
            SqlSELECT1 = SqlSELECT1 & " descr_sala descr_proven, sl_requis.dt_previ dt_chega ,null hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
        End If
        
        If CkCompletas.value = 1 Then
            SqlSELECT2 = " SELECT distinct sl_requis.N_req, sl_requis.n_req_assoc, sl_requis.t_urg,sl_tbf_t_urg.cor, sl_realiza.cod_agrup, sl_requis.dt_conclusao, sl_requis.dt_pretend,  "
            If gTipoInstituicao = gTipoInstituicaoHospitalar Or gTipoInstituicao = gTipoInstituicaoVet Then
                SqlSELECT2 = SqlSELECT2 & " descr_proven,sl_requis.dt_previ dt_chega,null hr_chega, sl_requis.tipo_urgencia, nome_ute,dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            Else
                SqlSELECT2 = SqlSELECT2 & " descr_sala descr_proven,sl_requis.dt_previ dt_chega,null hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            End If
        Else
            SqlSELECT2 = " UNION SELECT distinct sl_requis.N_req, sl_requis.n_req_assoc, sl_requis.t_urg,sl_tbf_t_urg.cor, sl_realiza.cod_agrup, sl_requis.dt_conclusao, sl_requis.dt_pretend,  "
            If gTipoInstituicao = gTipoInstituicaoHospitalar Or gTipoInstituicao = gTipoInstituicaoVet Then
                SqlSELECT2 = SqlSELECT2 & " descr_proven,sl_requis.dt_previ dt_chega , null hr_chega, sl_requis.tipo_urgencia, nome_ute,dt_confirm, dt_nasc_ute, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            Else
                SqlSELECT2 = SqlSELECT2 & " descr_sala descr_proven,sl_requis.dt_previ dt_chega , null hr_chega, sl_requis.tipo_urgencia, nome_ute, dt_confirm, sl_cm_tec.descr_cm_tec, sl_tbf_t_urg.limite, sl_tbf_t_urg.ordem_urgencia "
            End If
        End If
    End If
    
    ' CONSTROI FROM
    ' -------------
    sqlFROM2 = " FROM sl_identif, sl_requis LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven = sl_proven.cod_proven "
    sqlFROM2 = sqlFROM2 & " LEFT OUTER JOIN sl_cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala "
    sqlFROM2 = sqlFROM2 & " LEFT OUTER JOIN sl_cm_tec ON sl_requis.n_req = sl_cm_tec.n_req, sl_realiza , slv_analises_apenas slv_analises, sl_tbf_t_urg "
    
    sqlFROM1 = " FROM sl_identif, sl_requis LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven = sl_proven.cod_proven "
    sqlFROM1 = sqlFROM1 & " LEFT OUTER JOIN sl_cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala "
    sqlFROM1 = sqlFROM1 & " LEFT OUTER JOIN sl_cm_tec ON sl_requis.n_req = sl_cm_tec.n_req, sl_marcacoes, slv_analises_apenas slv_analises, sl_tbf_t_urg "
    
    ' SE FOI SELECCIONADO OPCAO PARA PESQUISAR POR DATA DO TUBO
    If OptData(1).value = True Or CkRequisComTubo.value = vbChecked Then
        sqlFROM1 = sqlFROM1 & ", sl_req_tubo "
        sqlFROM2 = sqlFROM2 & ", sl_req_tubo "
    End If
    
    If EcListaGrTrab.ListCount > 0 Then                        ' SE FOI INDICADO ALGUM GRUPO DE TRABALHO
        sqlFROM1 = sqlFROM1 & ", sl_ana_trab, sl_gr_trab"
        sqlFROM2 = sqlFROM2 & ", sl_ana_trab, sl_gr_trab"
    End If
    

    
    If EcListaGrAna.ListCount > 0 Then    'SE FOI INDICADO ALGUMA ANALISE A EXCLUI
        sqlFROM1 = sqlFROM1 & ", sl_gr_ana "
        sqlFROM2 = sqlFROM2 & ", sl_gr_ana "
'        Flg_UsaTabela = True
    End If
    
    If EcListaProd.ListCount > 0 Then                           ' SE FOI INDICADO ALGUM PRODUTO
        sqlFROM1 = sqlFROM1 & ", sl_produto"
        sqlFROM2 = sqlFROM2 & ", sl_produto"
    End If
    
    
    ' If EcListaProven.ListCount > 0 And gTipoInstituicao = "PRIVADA" Then     ' SE FOI INDICADO ALGUM POSTO
    '    sqlFROM1 = sqlFROM1 & ", sl_cod_salas"
    '    sqlFROM2 = sqlFROM2 & ", sl_cod_salas"
    ' End If
    
    
    'CONSTROI WHERE
    '--------------
    sqlWhere1 = " WHERE sl_requis.n_req = sl_marcacoes.n_req "
    sqlWhere1 = sqlWhere1 & " AND sl_marcacoes.cod_agrup = slv_analises.cod_ana "
    sqlWhere1 = sqlWhere1 & " AND sl_requis.seq_utente = sl_identif.seq_utente "
    If gSGBD = gPostGres Then
        sqlWhere1 = sqlWhere1 & " AND TO_NUMBER(sl_requis.t_urg,'99') = sl_tbf_t_urg.cod_t_urg "
    Else
        sqlWhere1 = sqlWhere1 & " AND sl_requis.t_urg = sl_tbf_t_urg.cod_t_urg "
    End If
    
    
    ' SE FOI SELECCIONADO OPCAO PARA PESQUISAR POR DATA DO TUBO
    If OptData(1).value = True Or CkRequisComTubo.value = vbChecked Then
        sqlWhere1 = sqlWhere1 & " AND sl_requis.n_Req = sl_req_tubo.n_req AND slv_analises.cod_tubo = sl_req_tubo.cod_tubo "
        sqlWhere1 = sqlWhere1 & " AND sl_marcacoes.seq_req_tubo = sl_req_tubo.seq_req_tubo  "
    End If
    
    If CkTodas.value <> 1 Then
        sqlWhere1 = sqlWhere1 & " AND sl_requis.estado_req not in(" & BL_TrataStringParaBD(gEstadoReqTodasImpressas) & ","
        sqlWhere1 = sqlWhere1 & BL_TrataStringParaBD(gEstadoReqValicacaoMedicaCompleta) & "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ","
        sqlWhere1 = sqlWhere1 & BL_TrataStringParaBD(gEstadoReqBloqueada) & ")"
    End If

    If gLAB = "HPOVOA" Then
        sqlWhere1 = sqlWhere1 & " AND sl_marcacoes.cod_ana_s <> 'S99999'"
    End If

    sqlWhere2 = " WHERE  sl_requis.n_req = sl_realiza.n_req "
    sqlWhere2 = sqlWhere2 & " AND sl_realiza.cod_agrup = slv_analises.cod_ana "
    sqlWhere2 = sqlWhere2 & " AND sl_requis.seq_utente = sl_identif.seq_utente "
    sqlWhere2 = sqlWhere2 & " AND sl_Realiza.seq_realiza not in (Select seq_realiza FROM sl_obs_auto o "
    sqlWhere2 = sqlWhere2 & " WHERE o.seq_realiza = sl_realiza.seq_realiza AND o.flg_bloqueio = 0 and o.flg_activo = 1) "
    If gSGBD = gPostGres Then
        sqlWhere2 = sqlWhere2 & " AND TO_NUMBER(sl_requis.t_urg,'99') = sl_tbf_t_urg.cod_t_urg "
    Else
        sqlWhere2 = sqlWhere2 & " AND sl_requis.t_urg = sl_tbf_t_urg.cod_t_urg "
    End If
    If CkTodas.value <> 1 Then
        If CkAssinatura.value = vbChecked And gAssinaturaActivo = mediSim Then
            sqlWhere2 = sqlWhere2 & " AND sl_requis.n_Req NOT IN (select sl_Req_assinatura.n_req FROM sl_req_assinatura WHERE sl_req_assinatura.n_Req = sl_requis.n_req) "
        Else
            sqlWhere2 = sqlWhere2 & " AND sl_requis.estado_req not in(" & BL_TrataStringParaBD(gEstadoReqTodasImpressas) & ","
            sqlWhere2 = sqlWhere2 & BL_TrataStringParaBD(gEstadoReqValicacaoMedicaCompleta) & "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ","
            sqlWhere2 = sqlWhere2 & BL_TrataStringParaBD(gEstadoReqBloqueada) & ")"
        End If
    End If
    ' SE FOI SELECCIONADO OPCAO PARA PESQUISAR POR DATA DO TUBO
    If OptData(1).value = True Or CkRequisComTubo.value = vbChecked Then
        sqlWhere2 = sqlWhere2 & " AND sl_requis.n_Req = sl_req_tubo.n_req AND slv_analises.cod_tubo = sl_req_tubo.cod_tubo "
        sqlWhere2 = sqlWhere2 & " AND sl_realiza.seq_req_tubo = sl_req_tubo.seq_Req_tubo  "
    End If
    
    
    If CkTodas.value <> 1 Then
        If CkAssinatura.value = vbChecked And gAssinaturaActivo = mediSim Then
            sqlWhere2 = sqlWhere2 & " AND (sl_realiza.flg_assinado = 0 OR sl_Realiza.flg_assinado IS NULL)"
            sqlWhere2 = sqlWhere2 & " AND sl_realiza.flg_estado =" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica)
        Else
            sqlWhere2 = sqlWhere2 & " AND " & EstadosNaoValidados
        End If
    End If
    
    
    ' ------------------------------------
    ' SE  FOI INDICADO APENAS REQ URG
    ' ------------------------------------
    If CkUrgentes.value = vbChecked Then
        sqlWhere1 = sqlWhere1 & " AND sl_requis.t_urg in ('1','2') "
        sqlWhere2 = sqlWhere2 & " AND sl_requis.t_urg in ('1','2') "
    End If
    
    If EcDtPretend <> "" Then
        sqlWhere1 = sqlWhere1 & " AND (sl_requis.dt_pretend <= " & BL_TrataDataParaBD(EcDtPretend) & " OR ( sl_requis.dt_conclusao = " & BL_TrataDataParaBD(EcDtPretend) & "))"
        sqlWhere2 = sqlWhere2 & " AND (sl_requis.dt_pretend <= " & BL_TrataDataParaBD(EcDtPretend) & " OR ( sl_requis.dt_conclusao = " & BL_TrataDataParaBD(EcDtPretend) & "))"
    End If
    ' ------------------------------------
    ' SE  FOI INDICADO APENAS REQ COM TUBO
    ' ------------------------------------
    If CkRequisComTubo.value = vbChecked Then
        sqlWhere1 = sqlWhere1 & " AND sl_req_tubo.n_req = sl_requis.n_req AND " & Tabela & ".cod_tubo = sl_req_tubo.cod_tubo "
        sqlWhere1 = sqlWhere1 & " AND   sl_req_tubo.dt_chega is not null "
        'sqlWhere1 = sqlWhere1 & " AND slv_analises.cod_ana = sl_marcacoes.cod_Agrup "
    End If
    


    ' ------------------------------------
    ' SE  FOI INDICADO ALGUMGRUPO ANALISES
    ' ------------------------------------
    If EcListaGrAna.ListCount > 0 Then
        sqlWhere1 = sqlWhere1 & " AND sl_marcacoes.cod_agrup = " & Tabela & "." & campoCodigo
        sqlWhere1 = sqlWhere1 & " AND " & Tabela & ".cod_gr_ana = sl_gr_ana.cod_gr_ana AND sl_gr_ana.seq_gr_ana in ( "
        For i = 0 To EcListaGrAna.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaGrAna.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        sqlWhere2 = sqlWhere2 & " AND sl_realiza.cod_agrup = " & Tabela & "." & campoCodigo
        sqlWhere2 = sqlWhere2 & " AND " & Tabela & ".cod_gr_ana = sl_gr_ana.cod_gr_ana AND sl_gr_ana.seq_gr_ana in ( "
        For i = 0 To EcListaGrAna.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaGrAna.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
    End If
    
    
    If gSGBD = gOracle Then
        If lDataInicial <> "" And lDataFinal <> "" Then
            ' -----------------------------------
            ' SE FOI INDICADO ALGUMA DATA
            ' -----------------------------------
            If OptData(0).value = True Then
                strData = "sl_requis."
            ElseIf OptData(1).value = True Then
                strData = "sl_req_tubo."
            End If
            If gUsaHoraPendentes = mediSim Then
                sqlWhere1 = sqlWhere1 & " AND ( to_date(TO_CHAR (" & strData & "dt_chega || ' ' || (" & strData & "hr_chega)),'dd-mm-yyyy hh24:mi:ss') BETWEEN "
                sqlWhere1 = sqlWhere1 & BL_TrataDataParaBD(lDataInicial & " " & lHoraInicial, "dd-mm-yyyy hh24:mi:ss") & " AND "
                sqlWhere1 = sqlWhere1 & BL_TrataDataParaBD(lDataFinal & " " & lHoraFinal, "dd-mm-yyyy hh24:mi:ss") & " ) "
                sqlWhere1 = sqlWhere1 & " AND " & strData & "dt_chega >=  " & BL_TrataDataParaBD(lDataInicial)
                
                sqlWhere2 = sqlWhere2 & " AND ( to_date(TO_CHAR (" & strData & "dt_chega || ' ' || (" & strData & "hr_chega)),'dd-mm-yyyy hh24:mi:ss') BETWEEN "
                sqlWhere2 = sqlWhere2 & BL_TrataDataParaBD(lDataInicial & " " & lHoraInicial, "dd-mm-yyyy hh24:mi:ss") & " AND "
                sqlWhere2 = sqlWhere2 & BL_TrataDataParaBD(lDataFinal & " " & lHoraFinal, "dd-mm-yyyy hh24:mi:ss") & " ) "
                sqlWhere2 = sqlWhere2 & " AND " & strData & "dt_chega >=  " & BL_TrataDataParaBD(lDataInicial)
            Else
                sqlWhere1 = sqlWhere1 & " AND  " & strData & "dt_chega  BETWEEN "
                sqlWhere1 = sqlWhere1 & BL_TrataDataParaBD(lDataInicial) & " AND "
                sqlWhere1 = sqlWhere1 & BL_TrataDataParaBD(lDataFinal) & "  "
                
                sqlWhere2 = sqlWhere2 & " AND " & strData & "dt_chega  BETWEEN "
                sqlWhere2 = sqlWhere2 & BL_TrataDataParaBD(lDataInicial) & " AND "
                sqlWhere2 = sqlWhere2 & BL_TrataDataParaBD(lDataFinal)
            End If
        End If
    Else
        If lDataInicial <> "" And lDataFinal <> "" Then
            ' -----------------------------------
            ' SE FOI INDICADO ALGUMA DATA
            ' -----------------------------------
            If OptData(0).value = True Then
                sqlWhere1 = sqlWhere1 & " AND ((sl_requis.dt_chega > " & BL_TrataDataParaBD(lDataInicial) & " OR (sl_requis.dt_chega = " & BL_TrataDataParaBD(lDataInicial) & " AND sl_Requis.hr_chega >= " & BL_TrataStringParaBD(lHoraInicial) & "))"
                sqlWhere1 = sqlWhere1 & " AND (sl_requis.dt_chega <" & BL_TrataDataParaBD(lDataFinal) & " OR (sl_requis.dt_chega = " & BL_TrataDataParaBD(lDataFinal) & " AND sl_Requis.hr_chega <= " & BL_TrataStringParaBD(lHoraFinal) & ")))"
                
                sqlWhere2 = sqlWhere2 & " AND ((sl_requis.dt_chega > " & BL_TrataDataParaBD(lDataInicial) & " OR (sl_requis.dt_chega = " & BL_TrataDataParaBD(lDataInicial) & " AND sl_Requis.hr_chega >= " & BL_TrataStringParaBD(lHoraInicial) & "))"
                sqlWhere2 = sqlWhere2 & " AND (sl_requis.dt_chega <" & BL_TrataDataParaBD(lDataFinal) & " OR (sl_requis.dt_chega = " & BL_TrataDataParaBD(lDataFinal) & " AND sl_Requis.hr_chega <= " & BL_TrataStringParaBD(lHoraFinal) & ")))"
            ElseIf OptData(1).value = True Then
                sqlWhere1 = sqlWhere1 & " AND ((sl_req_tubo.dt_chega > " & BL_TrataDataParaBD(lDataInicial) & " OR (sl_requis.dt_chega = " & BL_TrataDataParaBD(lDataInicial) & " AND sl_Requis.hr_chega >= " & BL_TrataStringParaBD(lHoraInicial) & "))"
                sqlWhere1 = sqlWhere1 & " AND (sl_req_tubo.dt_chega <" & BL_TrataDataParaBD(lDataFinal) & " OR (sl_requis.dt_chega = " & BL_TrataDataParaBD(lDataFinal) & " AND sl_Requis.hr_chega <= " & BL_TrataStringParaBD(lHoraFinal) & ")))"
                
                sqlWhere2 = sqlWhere2 & " AND ((sl_req_tubo.dt_chega > " & BL_TrataDataParaBD(lDataInicial) & " OR (sl_req_tubo.dt_chega = " & BL_TrataDataParaBD(lDataInicial) & " AND sl_req_tubo.hr_chega >= " & BL_TrataStringParaBD(lHoraInicial) & "))"
                sqlWhere2 = sqlWhere2 & " AND (sl_req_tubo.dt_chega <" & BL_TrataDataParaBD(lDataFinal) & " OR (sl_req_tubo.dt_chega = " & BL_TrataDataParaBD(lDataFinal) & " AND sl_req_tubo.hr_chega <= " & BL_TrataStringParaBD(lHoraFinal) & ")))"
            End If
                
        Else
            ' -----------------------------------
            ' SE NAO FOI INDICADO ALGUMA DATA
            ' -----------------------------------
            If OptData(0).value = True Then
                sqlWhere1 = sqlWhere1 & " AND convert(datetime,sl_requis.dt_previ,105) >=  (convert(datetime,getdate()-" & tempo & " ,105))"
                If CkTodas.value = 1 Then
                    sqlWhere2 = sqlWhere2 & " AND convert(datetime,sl_requis.dt_previ,105) >=  (convert(datetime,getdate()-" & tempo & " ,105))"
                Else
                    sqlWhere2 = sqlWhere2 & " AND convert(datetime,sl_requis.dt_previ,105) >=  (convert(datetime,getdate()-" & tempo & " ,105))"
                End If
            ElseIf OptData(1).value = True Then
                sqlWhere1 = sqlWhere1 & " AND convert(datetime,sl_req_tubo.dt_chega,105) >=  (convert(datetime,getdate()-" & tempo & " ,105))"
                If CkTodas.value = 1 Then
                    sqlWhere2 = sqlWhere2 & " AND convert(datetime,sl_req_tubo.dt_chega,105) >=  (convert(datetime,getdate()-" & tempo & " ,105))"
                Else
                    sqlWhere2 = sqlWhere2 & " AND convert(datetime,sl_req_tubo.dt_chega,105) >=  (convert(datetime,getdate()-" & tempo & " ,105))"
                End If
            End If
                
        End If
    End If
        
    
    
    If EcListaProven.ListCount > 0 And gTipoInstituicao = "HOSPITALAR" Then
        ' -----------------------------------
        ' SE FOI INDICADO ALGUMA PROVINIENCIA
        ' -----------------------------------
        sqlWhere1 = sqlWhere1 & " AND sl_requis.cod_proven = sl_proven.cod_proven and sl_proven.seq_proven in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        sqlWhere2 = sqlWhere2 & " AND sl_requis.cod_proven = sl_proven.cod_proven and sl_proven.seq_proven in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
        
    ElseIf EcListaProven.ListCount > 0 And gTipoInstituicao = "PRIVADA" Then
        ' -----------------------------------
        ' SE FOI INDICADO ALGUMA SALA/POSTO
        ' -----------------------------------
        sqlWhere1 = sqlWhere1 & " AND sl_requis.cod_sala = sl_cod_salas.cod_sala and sl_cod_salas.seq_sala in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        sqlWhere2 = sqlWhere2 & " AND sl_requis.cod_sala = sl_cod_salas.cod_sala and sl_cod_salas.seq_sala in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
    End If
        
        
    If EcListaGrTrab.ListCount > 0 Then                          ' SE FOI INDICADO ALGUM GRUPO TRABALHO
        sqlWhere1 = sqlWhere1 & " AND sl_ana_trab.cod_gr_trab = sl_gr_trab.cod_gr_trab AND sl_gr_trab.seq_gr_trab in ( "
        For i = 0 To EcListaGrTrab.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaGrTrab.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        sqlWhere1 = sqlWhere1 & " AND sl_ana_trab.cod_analise = sl_marcacoes.cod_agrup "
        
        sqlWhere2 = sqlWhere2 & " AND sl_ana_trab.cod_gr_trab = sl_gr_trab.cod_gr_trab AND sl_gr_trab.seq_gr_trab in ( "
        For i = 0 To EcListaGrTrab.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaGrTrab.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
        sqlWhere2 = sqlWhere2 & " AND sl_ana_trab.cod_analise = sl_realiza.cod_agrup "
    End If
   
    'LOCAL CHVNG-Espinho aparece todas as requisi��es que tenham analises cujo local executante seja o seleccionado
    If EcListaLocais.ListCount > 0 Then
        If Opt2(0).value = True Then
            'SE USAR AMBITO ENTAO USA LOCAL DA ANALISE (SL_ANA_ACRESCENTADAS)
            If gMultiLocalAmbito = mediSim Then
                sqlWhere1 = sqlWhere1 & " AND (slv_analises.cod_ana IN (SELECT cod_agrup FROM sl_ana_acrescentadas WHERE n_req = sl_requis.n_req AND (cod_local IS NULL OR cod_local IN ( "
                For i = 0 To EcListaLocais.ListCount - 1
                    sqlWhere1 = sqlWhere1 & EcListaLocais.ItemData(i) & ", "
                Next i
                sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ")) and (flg_eliminada IS NULL or flg_eliminada = 0))) "
                
                sqlWhere2 = sqlWhere2 & " AND (slv_analises.cod_ana IN (SELECT cod_agrup FROM sl_ana_acrescentadas WHERE n_req = sl_requis.n_req AND (cod_local IS NULL OR cod_local IN ( "
                For i = 0 To EcListaLocais.ListCount - 1
                    sqlWhere2 = sqlWhere2 & EcListaLocais.ItemData(i) & ", "
                Next i
                sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ")) and (flg_eliminada IS NULL or flg_eliminada = 0))) "
            Else
                sqlWhere1 = sqlWhere1 & " AND (slv_analises.cod_ana IN (SELECT COD_ANA FROM sl_ana_locais_exec WHERE cod_local IN ( "
                For i = 0 To EcListaLocais.ListCount - 1
                    sqlWhere1 = sqlWhere1 & EcListaLocais.ItemData(i) & ", "
                Next i
                sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & "))) "
                
                sqlWhere2 = sqlWhere2 & " AND (slv_analises.cod_ana IN (SELECT COD_ANA FROM sl_ana_locais_exec WHERE cod_local IN ( "
                For i = 0 To EcListaLocais.ListCount - 1
                    sqlWhere2 = sqlWhere2 & EcListaLocais.ItemData(i) & ", "
                Next i
                sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & "))) "
            End If
        ElseIf Opt2(1).value = True Then
            sqlWhere1 = sqlWhere1 & " AND SL_REQUIS.cod_local IN (  "
            For i = 0 To EcListaLocais.ListCount - 1
                sqlWhere1 = sqlWhere1 & EcListaLocais.ItemData(i) & ", "
            Next i
            sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
            
            sqlWhere2 = sqlWhere2 & " AND SL_REQUIS.cod_local IN (  "
            For i = 0 To EcListaLocais.ListCount - 1
                sqlWhere2 = sqlWhere2 & EcListaLocais.ItemData(i) & ", "
            Next i
            sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
        End If
    End If

    If EcListaSit.ListCount > 0 Then                          ' SE FOI INDICADO ALGUMa SITUACAO
        sqlWhere1 = sqlWhere1 & " AND sl_requis.t_sit in ( "
        For i = 0 To EcListaSit.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaSit.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        
        sqlWhere2 = sqlWhere2 & " AND sl_requis.t_sit in ("
        For i = 0 To EcListaSit.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaSit.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
    End If
    
    If EcListaUrg.ListCount > 0 Then                          ' SE FOI INDICADO ALGUMa URGENCIA
        sqlWhere1 = sqlWhere1 & " AND sl_requis.t_urg in ( "
        For i = 0 To EcListaUrg.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaUrg.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        
        sqlWhere2 = sqlWhere2 & " AND sl_requis.t_urg in ("
        For i = 0 To EcListaUrg.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaUrg.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
    End If
    
    If EcListaProd.ListCount > 0 Then                  'SE FOI INDICADO ALGUM PRODUTO
        sqlWhere1 = sqlWhere1 & " AND sl_marcacoes.cod_agrup = " & Tabela & "." & campoCodigo
        sqlWhere1 = sqlWhere1 & " AND " & Tabela & ".cod_produto = sl_produto.cod_produto AND sl_produto.seq_produto in ( "
        For i = 0 To EcListaProd.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaProd.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        sqlWhere2 = sqlWhere2 & " AND sl_realiza.cod_agrup = " & Tabela & "." & campoCodigo
        sqlWhere2 = sqlWhere2 & " AND " & Tabela & ".cod_produto = sl_produto.cod_produto AND sl_produto.seq_produto in ( "
        For i = 0 To EcListaProd.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaProd.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
    End If
        
        
    If EcCodSala <> "" Then                    'SE FOI INDICADO ALGUMA SALA
        sqlWhere1 = sqlWhere1 & " AND sl_requis.cod_sala = " & BL_TrataStringParaBD(EcCodSala)
        sqlWhere2 = sqlWhere2 & " AND sl_requis.cod_sala = " & BL_TrataStringParaBD(EcCodSala)
    End If
    
    
    If EcListaAnalises.ListCount > 0 Then                  'SE FOI INDICADO ALGUMA ANALISE
        If Opt1(0).value = True Then
            sqlWhere1 = sqlWhere1 & " AND slv_analises.seq_ana in ( "
        Else
            sqlWhere1 = sqlWhere1 & " AND slv_analises.seq_ana not in ( "
        End If
        
        For i = 0 To EcListaAnalises.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaAnalises.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        If Opt1(0).value = True Then
            sqlWhere2 = sqlWhere2 & " AND slv_analises.seq_ana in ( "
        Else
            sqlWhere2 = sqlWhere2 & " AND slv_analises.seq_ana not in ( "
        End If
        For i = 0 To EcListaAnalises.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaAnalises.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
        
    End If
    
    
    'SE FILTROU POR APARELHO (WHERE)
    If EcListaApar.ListCount > 0 Then
        
        If gSGBD = gOracle Then
            sqlWhere1 = sqlWhere1 & " AND sl_marcacoes.cod_ana_s IN (SELECT cod_simpl FROM gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.NAO_entra_pendentes = 0 or gc_ana_apar.NAO_entra_pendentes IS NULL) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        ElseIf gSGBD = gSqlServer Then
            sqlWhere1 = sqlWhere1 & " AND sl_marcacoes.cod_ana_S IN (SELECT cod_simpl FROM gescom.dbo.gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.nao_entra_pendentes = 0 or gc_ana_apar.nao_entra_pendentes is null) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        End If
        For i = 0 To EcListaApar.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaApar.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ")) "
        
        
        If gSGBD = gOracle Then
            sqlWhere2 = sqlWhere2 & " AND sl_Realiza.cod_ana_s IN (SELECT cod_simpl FROM gc_ana_apar gc_ana_apar WHERE gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        ElseIf gSGBD = gSqlServer Then
            sqlWhere2 = sqlWhere2 & " AND sl_realiza.cod_ana_S IN (SELECT cod_simpl FROM gescom.dbo.gc_ana_apar gc_ana_apar WHERE gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        End If
        For i = 0 To EcListaApar.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaApar.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ")) "
    End If
    

    
    
    ' FGONCALVES - HMILITAR
    ' OUT 2006  - APENAS APARECE REQUISICOES COM RESULTADOS POR VALIDAR
    If CkCompletas.value = 1 Then
        ConstroiSQL = Replace(SqlSELECT2, "UNION", "") & sqlFROM2 & sqlWhere2
        ConstroiSQL = ConstroiSQL & " AND sl_requis.n_req NOT IN (SELECT n_req FROM sl_marcacoes where cod_ana_s <> 'S99999' ) "
    ElseIf CkAssinatura.value = vbChecked And gAssinaturaActivo = mediSim Then
        ConstroiSQL = Replace(SqlSELECT2, "UNION", "") & sqlFROM2 & sqlWhere2
        ConstroiSQL = ConstroiSQL & " AND sl_requis.n_req NOT IN (SELECT n_req FROM sl_marcacoes where cod_ana_s <> 'S99999' ) "
    ElseIf CkApenasMarcacoes.value = 1 Then
        ConstroiSQL = SqlSELECT1 & sqlFROM1 & sqlWhere1 & ""
    ElseIf CkApenasResultados.value = vbChecked Then
        ConstroiSQL = Replace(SqlSELECT2, "UNION", "") & sqlFROM2 & sqlWhere2
    Else
        ConstroiSQL = SqlSELECT1 & sqlFROM1 & sqlWhere1 & SqlSELECT2 & sqlFROM2 & sqlWhere2 & ""
    End If
    If ordenacao = "" Then
        ConstroiSQL = ConstroiSQL & " ORDER BY "
        If gLAB = "ICIL" Then
            ConstroiSQL = ConstroiSQL & " N_REQ ASC,"
        End If
        ConstroiSQL = ConstroiSQL & " t_urg desc,"
        
        If gOrdenaPendentesNumReq = mediSim Then
            ConstroiSQL = ConstroiSQL & " n_req ASC "
        Else
            ConstroiSQL = ConstroiSQL & " dt_chega ASC, hr_chega ASC "
        End If
    Else
        Select Case UCase(ordenacao)
        
            Case UCase(ordenaTUrg)
                ConstroiSQL = ConstroiSQL & " ORDER BY  ordem_urgencia desc"
            Case UCase(ordenaDtChega)
                ConstroiSQL = ConstroiSQL & " ORDER BY  dt_chega ASC, hr_chega ASC"
            Case UCase(ordenaNome)
                ConstroiSQL = ConstroiSQL & " ORDER BY  nome_ute ASC"
            Case UCase(ordenaNumRequis)
                ConstroiSQL = ConstroiSQL & " ORDER BY  n_req ASC"
            Case UCase(ordenaServico)
                ConstroiSQL = ConstroiSQL & " ORDER BY  descr_proven ASC"
            Case UCase(ordenaTriagem)
                ConstroiSQL = ConstroiSQL & " ORDER BY  tipo_urg ASC"
        End Select
        If gOrdenaPendentesNumReq = mediSim Then
            ConstroiSQL = ConstroiSQL & ", n_req ASC "
        End If
    End If

End Function



Private Sub PreencheEstrutRequis(n_req As String, t_urg As String, servico As String, dt_chega As String, hr_chega As String, _
                                 tipo_urgencia As String, nome_ute As String, dt_conclusao As String, dt_pretend As String, _
                                 dt_confirmacao As String, com_tec As String, cor As String, limite As Long)
    Dim i As Long
    On Error GoTo TrataErro
    
    'VERIFICA SE NAO EXISTE A REQUISICAO NA ESTRUTURA
    For i = 1 To TotalDadosRequis
        If EstrutDadosRequis(i).num_requis = n_req Then
            Exit Sub
        End If
    Next
    
    TotalDadosRequis = TotalDadosRequis + 1
    ReDim Preserve EstrutDadosRequis(TotalDadosRequis)
    
    EstrutDadosRequis(TotalDadosRequis).num_requis = n_req
    EstrutDadosRequis(TotalDadosRequis).urgente = t_urg
    EstrutDadosRequis(TotalDadosRequis).servico = servico
    EstrutDadosRequis(TotalDadosRequis).dt_chega = dt_chega
    EstrutDadosRequis(TotalDadosRequis).hr_chega = hr_chega
    EstrutDadosRequis(TotalDadosRequis).tipo_urgencia = tipo_urgencia
    EstrutDadosRequis(TotalDadosRequis).nome = nome_ute
    EstrutDadosRequis(TotalDadosRequis).dt_conclusao = dt_conclusao
    EstrutDadosRequis(TotalDadosRequis).dt_pretendida = dt_pretend
    EstrutDadosRequis(TotalDadosRequis).dt_confirmacao = dt_confirmacao
    EstrutDadosRequis(TotalDadosRequis).cor_urgencia = cor
    EstrutDadosRequis(TotalDadosRequis).limite = limite
    If EstrutDadosRequis(TotalDadosRequis).dt_chega <> "---" And EstrutDadosRequis(TotalDadosRequis).hr_chega <> "---" Then
        EstrutDadosRequis(TotalDadosRequis).tempo = 1440 * (CDate(Now) - CDate(EstrutDadosRequis(TotalDadosRequis).dt_chega & " " & EstrutDadosRequis(TotalDadosRequis).hr_chega))
    End If
    If EstrutDadosRequis(TotalDadosRequis).limite > 0 Then
        EstrutDadosRequis(TotalDadosRequis).percentagem = (100 * EstrutDadosRequis(TotalDadosRequis).tempo) / EstrutDadosRequis(TotalDadosRequis).limite
    End If
    
    EstrutDadosRequis(TotalDadosRequis).totalAnalises = 0
    ReDim EstrutDadosRequis(TotalDadosRequis).analises(EstrutDadosRequis(TotalDadosRequis).totalAnalises)
    RTB.TextRTF = com_tec
    EstrutDadosRequis(TotalDadosRequis).com_tec = RTB.Text
    If TotalDadosRequis >= ValMinimo And TotalDadosRequis <= ValMaximo Then
        PreencheAnalises TotalDadosRequis, n_req, tipo_urgencia
    End If
    'R.G.: 26-03-2012
    EstrutDadosRequisOriginal = EstrutDadosRequis
    TotalDadosRequisOriginal = TotalDadosRequis
Exit Sub
TrataErro:
    MsgBox "a"
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheEstrutRequisImpr(n_req As String, t_urg As String, servico As String, dt_chega As String, hr_chega As String, _
                                     dt_pretend As String, dt_conclusao As String, dt_nasc As String, n_Req_assoc As String)
    Dim i As Long
    
    'VERIFICA SE NAO EXISTE A REQUISICAO NA ESTRUTURA
    For i = 1 To TotalDadosRequisImpr
        If EstrutDadosRequisImpr(i).num_requis = n_req Then
            Exit Sub
        End If
    Next
    
    TotalDadosRequisImpr = TotalDadosRequisImpr + 1
    ReDim Preserve EstrutDadosRequisImpr(TotalDadosRequisImpr)
    
    EstrutDadosRequisImpr(TotalDadosRequisImpr).num_requis = n_req
    EstrutDadosRequisImpr(TotalDadosRequisImpr).urgente = t_urg
    EstrutDadosRequisImpr(TotalDadosRequisImpr).servico = servico
    EstrutDadosRequisImpr(TotalDadosRequisImpr).dt_chega = dt_chega
    EstrutDadosRequisImpr(TotalDadosRequisImpr).hr_chega = hr_chega
    EstrutDadosRequisImpr(TotalDadosRequisImpr).dt_conclusao = dt_conclusao
    EstrutDadosRequisImpr(TotalDadosRequisImpr).dt_pretendida = dt_pretend
    EstrutDadosRequisImpr(TotalDadosRequisImpr).dt_nascimento = dt_nasc
    EstrutDadosRequisImpr(TotalDadosRequisImpr).n_Req_assoc = n_Req_assoc
   
    PreencheAnalisesImpr n_req
End Sub


Private Sub PreencheAnalisesImpr(n_req As String)
    Dim sSql As String
    Dim rs As New ADODB.recordset
    Dim i As Integer
    sSql = ControiCriterioPreencheAna(n_req, "")
    
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        i = 1 ' poe o indice no primeiro campo possivel para inserir resultado na primeira celula possivel
        While Not rs.EOF
            TotalAnaImpr = TotalAnaImpr + 1
            ReDim Preserve EstrutAnaImpr(TotalAnaImpr)

            EstrutAnaImpr(TotalAnaImpr).descr_ana = BL_HandleNull(rs!descr_ana, "---")
            If VerificaTemTubo(BL_HandleNull(rs!n_req, ""), BL_HandleNull(rs!cod_tubo, "")) = False Then
                If gMostraAnalisesSemTubo = mediSim Then
                    EstrutAnaImpr(TotalAnaImpr).descr_ana = EstrutAnaImpr(TotalAnaImpr).descr_ana & " (Sem tubo)"
                End If
            End If
            EstrutAnaImpr(TotalAnaImpr).ordem = BL_HandleNull(rs!Ord_Marca, 0)
            EstrutAnaImpr(TotalAnaImpr).num_requis = BL_HandleNull(rs!n_req, "")

            If Trim(rs!flg_estado) = gEstadoAnaSemResultado Then
                EstrutAnaImpr(TotalAnaImpr).tipo = resultado
            ElseIf rs!flg_estado = gEstadoAnaComResultado Or rs!flg_estado = gEstadoAnaValidacaoTecnica Then
                EstrutAnaImpr(TotalAnaImpr).tipo = Validar
            Else
                EstrutAnaImpr(TotalAnaImpr).tipo = Validada
            End If
            rs.MoveNext
        Wend
    End If
    rs.Close
    Set rs = Nothing
End Sub


Private Sub PreencheTabela()
    Dim i As Integer
    Dim j As Integer
    Dim sSql As String
    Dim RsIdentif As New ADODB.recordset
    Dim servico As String
    Dim data As String
    Dim hora As String
    Dim nome As String
    Dim idade As String
    Dim dt_pretendida As String
    Dim dt_conclusao As String
    Dim dt_nascimento As String
    Dim n_Req_assoc As String
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_cr_pend_ana WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    For i = 1 To TotalAnaImpr
        servico = ""
        data = ""
        hora = ""
        n_Req_assoc = ""
        For j = 1 To TotalDadosRequisImpr
            If EstrutDadosRequisImpr(j).num_requis = EstrutAnaImpr(i).num_requis Then
                servico = EstrutDadosRequisImpr(j).servico
                data = EstrutDadosRequisImpr(j).dt_chega
                hora = EstrutDadosRequisImpr(j).hr_chega
                dt_pretendida = EstrutDadosRequisImpr(j).dt_pretendida
                dt_conclusao = EstrutDadosRequisImpr(j).dt_conclusao
                dt_nascimento = EstrutDadosRequisImpr(j).dt_nascimento
                n_Req_assoc = EstrutDadosRequisImpr(j).n_Req_assoc
                Exit For
            End If
        Next
    
        nome = ""
        idade = ""
        
        sSql = "SELECT nome_ute, dt_nasc_ute FROM sl_identif, sl_requis WHERE sl_identif.seq_utente = sl_requis.seq_utente"
        sSql = sSql & " AND n_req  = " & BL_TrataStringParaBD(EstrutAnaImpr(i).num_requis)
        Set RsIdentif = New ADODB.recordset
        RsIdentif.CursorType = adOpenStatic
        RsIdentif.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsIdentif.Open sSql, gConexao, adOpenStatic
        If RsIdentif.RecordCount > 0 Then
            nome = BL_HandleNull(RsIdentif!nome_ute, "")
            idade = BG_CalculaIdade(BL_HandleNull(RsIdentif!dt_nasc_ute, Bg_DaData_ADO))
        End If
        RsIdentif.Close
        Set RsIdentif = Nothing
        
        sSql = "INSERT INTO sl_cr_pend_ana (nome_computador, n_req, servico, dt_chega, hr_chega, descr_ana, estado, nome, idade, "
        sSql = sSql & " dt_conclusao, dt_pretendida, dt_nascimento, n_req_assoc, ord_marca) VALUES("
        sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", " & BL_TrataStringParaBD(EstrutAnaImpr(i).num_requis)
        sSql = sSql & ", " & BL_TrataStringParaBD(servico) & ", " & BL_TrataDataParaBD(data) & ", " & BL_TrataStringParaBD(hora)
        sSql = sSql & ", " & BL_TrataStringParaBD(EstrutAnaImpr(i).descr_ana) & "," & BL_TrataStringParaBD(EstrutAnaImpr(i).tipo)
        sSql = sSql & ", " & BL_TrataStringParaBD(nome) & ", " & BL_TrataStringParaBD(idade) & ","
        sSql = sSql & BL_TrataDataParaBD(dt_conclusao) & ", " & BL_TrataDataParaBD(dt_pretendida) & ", "
        sSql = sSql & BL_TrataDataParaBD(dt_nascimento) & ", "
        sSql = sSql & BL_TrataStringParaBD(n_Req_assoc) & ", "
        sSql = sSql & EstrutAnaImpr(i).ordem & ") "
        
        
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
    Next
    Exit Sub
TrataErro:
    BG_LogFile_Erros "========================================================="
    BG_LogFile_Erros "FormReqPendAna -> PreencheTabela " & Err.Description
    BG_LogFile_Erros "========================================================="
    Exit Sub
    Resume Next
End Sub

Private Sub PreenhceEstrutTubos(n_req As String, cod_ana As String)
              
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim conta As Integer
    If Mid(cod_ana, 1, 1) = "P" Then
        sql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenForwardOnly
        rs.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rs.Open sql, gConexao
        If rs.RecordCount > 0 Then
            While Not rs.EOF
                PreenhceEstrutTubos n_req, rs!cod_analise
                rs.MoveNext
            Wend
        End If
        rs.Close
    ElseIf Mid(cod_ana, 1, 1) = "C" Then
        sql = "SELECT cod_membro FROM sl_membro WHERE T_MEMBRO = 'A' AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenForwardOnly
        rs.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rs.Open sql, gConexao
        If rs.RecordCount > 0 Then
            While Not rs.EOF
                PreenhceEstrutTubos n_req, rs!cod_membro
                rs.MoveNext
            Wend
        End If
        rs.Close
    End If
    
    ' -----------------------------------------------
    ' Preenche a grid.
    
    sql = "SELECT DISTINCT " & vbCrLf & _
          "    T.descr_tubo, " & vbCrLf & _
          "    A.cod_tubo,  " & vbCrLf & _
          "    A.flg_etiq_prod, " & vbCrLf & _
          "    A.flg_etiq_ord " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_marcacoes M, " & vbCrLf & _
          "    sl_ana_s     A, " & vbCrLf & _
          "    sl_tubo  T " & vbCrLf
    sql = sql & "WHERE " & vbCrLf & _
          "    M.n_req        = " & n_req & " AND " & vbCrLf & _
          "    A.cod_ana_s        = " & BL_TrataStringParaBD(cod_ana) & " AND " & vbCrLf & _
          "    A.cod_ana_s    = M.cod_ana_s AND " & vbCrLf & _
          "    A.cod_tubo = T.cod_tubo "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    

    While (Not rs.EOF)
        If BL_HandleNull(rs!cod_tubo, "") <> "" Then
            For conta = 1 To UBound(Tubos)
                If Tubos(conta).cod_tubo = rs!cod_tubo And Tubos(conta).n_req = n_req Then
                    rs.Close
                    Set rs = Nothing
                    Exit Sub
                End If
            Next
            ReDim Preserve Tubos(UBound(Tubos) + 1)
            Tubos(UBound(Tubos)).cod_tubo = rs!cod_tubo
            Tubos(UBound(Tubos)).descR_tubo = rs!descR_tubo
            Tubos(UBound(Tubos)).n_req = n_req
        End If
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
     
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : PreencheEstrutTubos (FormFolhasTrab) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
            Resume Next
    End Select
End Sub



Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String _
    ) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", RemovePortuguese(StrConv(nome_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function


Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function



Sub ImprimeEtiq(NReq As String, descR_tubo As String)
              
    On Error GoTo ErrorHandler
    
    Dim i As Integer
    Dim Report As CrystalReport
    Dim continua As Boolean
    Dim sql As String
    Dim Sql2 As String
    Dim rs As ADODB.recordset
    Dim RS2 As ADODB.recordset
    Dim n_req As String
    Dim N_req_bar As String
    Dim situacao As String
    Dim episodio As String
    Dim Criterio As String
    Dim idade As String
    Dim EtqCrystal As String
    
    If Trim(NReq) = "" Then
        Exit Sub
    End If
    
    NReq = Trim(NReq)
    N_req_bar = Right("0000000" & NReq, 7)
        
    Criterio = ""
    
    If (Len(descR_tubo) > 0) Then
        Criterio = " AND T.descr_tubo = '" & descR_tubo & "'"
    End If
    
    ' Preenche a tabela secund�ria.
        
    sql = "SELECT DISTINCT " & vbCrLf & _
          "    U.t_utente, U.utente, U.n_proc_1, R.n_req, " & vbCrLf & _
          "    R.t_urg, R.dt_chega, 'ORDEM', U.nome_ute, " & vbCrLf & _
          "    'N_REQ_BAR', " & vbCrLf & _
          "    T.descr_tubo,R.t_sit, R.n_epis, R.req_aux, U.dt_nasc_ute, R.dt_previ, T.cod_etiq, T.inf_complementar,T.cod_tubo  " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_marcacoes M, " & vbCrLf & _
          "    sl_ana_s     A, " & vbCrLf & _
          "    sl_requis    R, " & vbCrLf & _
          "    sl_identif   U, " & vbCrLf & _
          "    sl_gr_ana    G, " & vbCrLf & ""
        sql = sql & "   sl_tubo  T " & vbCrLf & ""
    
    sql = sql & _
          "WHERE " & vbCrLf & _
          "    M.n_req        = " & NReq & "          AND " & vbCrLf & _
          "    R.n_req        = M.n_req       AND " & vbCrLf & _
          "    A.cod_ana_s    = M.cod_ana_s   AND " & vbCrLf & _
          "    R.seq_utente   = U.seq_utente  AND " & vbCrLf & _
          "    A.gr_ana       = G.cod_gr_ana  AND " & vbCrLf

    sql = sql & "   A.cod_tubo = T.cod_tubo     " & vbCrLf & ""
    sql = sql & Criterio
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    rs.MoveFirst
    While (Not rs.EOF)
            
        ' ---------------------------------
        If BL_HandleNull(rs!dt_nasc_ute, "") <> "" And BL_HandleNull(rs!dt_previ, "") <> "" And BL_TrataDataParaBD(BL_HandleNull(rs!dt_nasc_ute, "")) <> BL_TrataDataParaBD("01/01/1800") Then
            idade = CStr(BG_CalculaIdade(rs!dt_nasc_ute, rs!dt_previ))
        End If
        
        ' Se o epis�dio for igual ao n�mero de requisi��o, n�o sai na etiqueta.
        n_req = Trim(BL_HandleNull(rs!n_req, ""))
        situacao = Trim(BL_HandleNull(rs!t_sit, ""))
        episodio = Trim(BL_HandleNull(rs!n_epis, ""))
        
        If (n_req = episodio) Then
            situacao = ""
            episodio = ""
        End If
        
        ' ---------------------------------
            
        Select Case situacao
        
            Case gT_Urgencia
                situacao = "URG"
            Case gT_Consulta
                situacao = "CON"
            Case gT_Internamento
                situacao = "INT"
            Case gT_Externo
                situacao = "HDI"
            Case gT_LAB
                situacao = "LAB"
            Case gT_RAD
                situacao = "RAD"
            Case gT_Bloco
                situacao = "BLO"
            Case Else
                situacao = ""
        
        End Select
        
        ' ---------------------------------
        If Not LerEtiqInI Then
            MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
            Exit Sub
        End If
        If Not EtiqOpenPrinter(EcPrinterEtiq) Then
            MsgBox "Imposs�vel abrir impressora etiquetas"
            Exit Sub
        End If
        
        
        Call EtiqPrint("", "", _
                BL_HandleNull(rs!t_utente, ""), BL_HandleNull(rs!Utente, ""), _
                BL_HandleNull(rs!n_proc_1, ""), BL_HandleNull(rs!n_req, ""), _
                BL_HandleNull(rs!t_urg, ""), BL_HandleNull(rs!dt_chega, ""), "", _
                BL_HandleNull(rs!nome_ute, ""), BL_HandleNull(rs!cod_etiq, "") & N_req_bar, BL_HandleNull(rs!descR_tubo, ""), BL_HandleNull(rs!inf_complementar, ""))
        
        EtiqClosePrinter
        
        BL_RegistaImprEtiq BL_HandleNull(rs!n_req, ""), BL_HandleNull(rs!cod_tubo, "")
        
        rs.MoveNext
        
    Wend
    
    rs.Close
    Set rs = Nothing
    
     
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : ImprimeEtiq (FormSepSoros) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub



Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function

Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function


Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo Erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While aux <> "P1"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function
Erro:
End Function

' pferreira 2010.09.27
' Alteracao da query para n�o retornar tubos cancelados.
Private Function VerificaTemTubo(n_req As String, cod_tubo As String) As Boolean
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_req_tubo WHERE n_req = " & BL_TrataStringParaBD(n_req)
    sSql = sSql & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    sSql = sSql & " AND dt_eliminacao is null "
    
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    rsTubo.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount >= 1 Then
        If BL_HandleNull(rsTubo!dt_chega, "") <> "" Then
            VerificaTemTubo = True
        Else
            VerificaTemTubo = False
        End If
    Else
        VerificaTemTubo = False
    End If
    rsTubo.Close
    Set rsTubo = Nothing
End Function

' --------------------------------------------------------

' UTILIZADORES DOS POSTOS PREENCHE POR DEFEITO A SALA

' --------------------------------------------------------
Private Sub PreencheSalaDefeito()
    EcCodSala.Enabled = True
    If gCodSalaAssocUser > 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodSala.Enabled = False
    Else
        EcCodSala = ""
    End If
End Sub

Private Sub EclistaGrAna_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaGrAna, KeyCode, Shift
End Sub
Private Sub Eclistasit_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaSit, KeyCode, Shift
End Sub
Private Sub Eclistaurg_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaUrg, KeyCode, Shift
End Sub
Private Sub EclistaGrtrab_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaGrTrab, KeyCode, Shift
End Sub

Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProven, KeyCode, Shift
End Sub

Private Sub EclistaProd_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProd, KeyCode, Shift
End Sub
Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocais, KeyCode, Shift
End Sub
Private Sub Eclistaapar_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaApar, KeyCode, Shift
End Sub

Private Sub PesquisaProven()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(0) = resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub Eclistaanalises_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaAnalises.ListCount > 0 Then     'Delete
        If EcListaAnalises.ListIndex > mediComboValorNull Then
            EcListaAnalises.RemoveItem (EcListaAnalises.ListIndex)
        End If
    End If
End Sub


Sub Funcao_DataActual()
    If Me.ActiveControl.Name = "EcDtIni" Then
        BL_PreencheData EcDtIni, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtFim" Then
        BL_PreencheData EcDtFim, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtPretend" Then
        BL_PreencheData EcDtPretend, Me.ActiveControl
    End If
    
End Sub
Private Sub EcDtini_Validate(Cancel As Boolean)
    
    If EcDtIni.value <> "" Then
        Cancel = Not ValidaDataInicial
    End If
    tempo = Bg_DaData_ADO - EcDtIni.value
End Sub
Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    If EcDtFim.value <> "" Then
        Cancel = Not ValidaDataFinal
    End If
        
End Sub
Function ValidaDataInicial() As Boolean

    ValidaDataInicial = False
    If EcDtIni.value <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDtIni) Then
            ValidaDataInicial = True
        End If
    End If
    
End Function
Function ValidaDataFinal() As Boolean

    ValidaDataFinal = False
    If EcDtFim.value <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDtFim) Then
            ValidaDataFinal = True
        End If
    End If
    
End Function



' --------------------------------------------------------

' ENTRA NO ECRA ANTIGO DE RESULTADOS

' --------------------------------------------------------
Private Sub ModoResultados()
    Dim sql As String
    Dim sqlWhere1 As String
    Dim sqlWhere2 As String
    Dim DtChega As String
    Dim i As Integer
    gSqlSelRes = ""
    gSqlSelResSELECT1 = ""
    gSqlSelResSELECT2 = ""
    gSqlSelResFROM1 = ""
    gSqlSelResFROM2 = ""
    gSqlSelResWHERE1 = ""
    gSqlSelResWHERE2 = ""
    gSqlSelResORDER = ""
    
    ' SE LISTA TIVER MAIS QUE UM ELEMENTO TRABALHA SEMPRE APENAS COM PRIMEIRO.
    EcGrAnalises = ""
    If EcListaGrAna.ListCount > 0 Then
        EcGrAnalises = BL_SelCodigo("SL_GR_ANA", "COD_GR_ANA", "SEQ_GR_ANA", EcListaGrAna.ItemData(0))
    End If
    If EcGrAnalises = gCodGrupoMicrobiologia Then
        gModoRes = cModoResMicro
    End If
    If EcData.Text <> "" And EcData.Text <> "ALL" Then
        DtChega = EcData.Text
    ElseIf EcData = "ALL" Then
        DtChega = ""
    End If
    
    NReq = BL_HandleNull(FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq), 0)
    
    sqlWhere1 = ""
    sqlWhere2 = ""
    
    If CkUrgentes.value = vbChecked Then               ' SE FOI INDICADO APENAS REQ URGENTES
        sqlWhere1 = sqlWhere1 & " AND req.t_urg in ('1','2') "
        sqlWhere2 = sqlWhere2 & " AND req.t_urg in ('1','2') "
    End If
    
    sqlWhere1 = sqlWhere1 & " AND marca.cod_agrup = slv_analises.cod_ana "
    sqlWhere2 = sqlWhere2 & " AND realiza.cod_agrup =slv_analises.cod_ana "
    
    ' FM
    If EcListaGrAna.ListCount > 0 Then                         ' SE FOI INDICADO ALGUM GRUPO
        sqlWhere1 = sqlWhere1 & " AND slv_analises.cod_gr_ana = sl_gr_ana.cod_gr_ana "
        sqlWhere1 = sqlWhere1 & " AND sl_gr_ana.seq_gr_ana in ( "
        For i = 0 To EcListaGrAna.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaGrAna.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
    
        sqlWhere2 = sqlWhere2 & " AND slv_analises.cod_gr_ana = sl_gr_ana.cod_gr_ana "
        sqlWhere2 = sqlWhere2 & " AND sl_gr_ana.seq_gr_ana in ( "
        For i = 0 To EcListaGrAna.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaGrAna.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
    End If
    
    
    'FM
    If EcListaProven.ListCount > 0 And gTipoInstituicao = "HOSPITALAR" Then                    'SE FOI INDICADO ALGUMA PROVINIENCIA
        sqlWhere1 = sqlWhere1 & " AND req.cod_proven= sl_proven.cod_proven "
        sqlWhere1 = sqlWhere1 & " AND sl_proven.seq_proven in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
    
        sqlWhere2 = sqlWhere2 & " AND req.cod_proven= sl_proven.cod_proven "
        sqlWhere2 = sqlWhere2 & " AND sl_proven.seq_proven in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
        
    ElseIf EcListaProven.ListCount > 0 And gTipoInstituicao = "PRIVADA" Then       'SE FOI INDICADO ALGUMA SALA
        sqlWhere1 = sqlWhere1 & " AND req.cod_sala= sl_cod_salas.cod_sala "
        sqlWhere1 = sqlWhere1 & " AND sl_cod_Salas.seq_sala in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
    
        sqlWhere2 = sqlWhere2 & " AND req.cod_sala= sl_cod_salas.cod_sala "
        sqlWhere2 = sqlWhere2 & " AND sl_cod_salas.seq_sala in ( "
        For i = 0 To EcListaProven.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaProven.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
    End If
                
    ' FM
    If EcListaGrTrab.ListCount > 0 Then                          ' SE FOI INDICADO ALGUM GRUPO TRABALHO
        sqlWhere1 = sqlWhere1 & " AND sl_ana_trab.cod_gr_trab = sl_gr_trab.cod_gr_trab AND sl_gr_trab.seq_gr_trab in ( "
        For i = 0 To EcListaGrTrab.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaGrTrab.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & ") "
        
        sqlWhere1 = sqlWhere1 & " AND sl_ana_trab.cod_analise = marca.cod_agrup "
        
        sqlWhere2 = sqlWhere2 & " AND sl_ana_trab.cod_gr_trab = sl_gr_trab.cod_gr_trab AND sl_gr_trab.seq_gr_trab in ( "
        For i = 0 To EcListaGrTrab.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaGrTrab.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ") "
        sqlWhere2 = sqlWhere2 & " AND sl_ana_trab.cod_analise = realiza.cod_agrup "
    End If
    
    'LOCAL CHVNG-Espinho quando entra nos resultados filtra apenas as analises do local executante seleccionado
    If EcListaLocais.ListCount > 0 Then

        sqlWhere1 = sqlWhere1 & " AND (slv_analises.cod_ana IN (SELECT COD_ANA FROM sl_ana_locais_exec WHERE cod_local IN ( "
        For i = 0 To EcListaLocais.ListCount - 1
            sqlWhere1 = sqlWhere1 & EcListaLocais.ItemData(i) & ", "
        Next i
        sqlWhere1 = Mid(sqlWhere1, 1, Len(sqlWhere1) - 2) & "))) "
        
        sqlWhere2 = sqlWhere2 & " AND (slv_analises.cod_ana IN (SELECT COD_ANA FROM sl_ana_locais_exec WHERE cod_local IN ( "
        For i = 0 To EcListaLocais.ListCount - 1
            sqlWhere2 = sqlWhere2 & EcListaLocais.ItemData(i) & ", "
        Next i
        sqlWhere2 = Mid(sqlWhere2, 1, Len(sqlWhere2) - 2) & ")) "
    End If
    

    'Introdu��o e Valida��o de Resultados, consoante permiss�o do utilizador
    ' FM
    If EcGrAnalises <> CStr(gCodGrupoMicrobiologia) And EcGrAnalises <> CStr(gCodGrupoMicobacteriologia) Then
        If gSGBD = gOracle Then
            gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.n_folha_trab, NULL AS user_cri, to_date(Null) AS dt_cri, NULL AS hr_cri, NULL AS user_act, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "to_date(Null) AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, to_date(Null) AS dt_val, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "NULL AS hr_val, NULL AS user_tec_val, to_date(Null) AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "req.dt_chega AS dt_chega_req, req.hr_chega as hr_chega_req, req.n_req_assoc, req.estado_req, id.seq_utente, id.t_utente, id.utente, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado,req.inf_complementar , pr.ordem ord_produto, slv_analises.cod_gr_ana,req.cod_proven, req.tipo_urgencia, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2,req.cod_efr, req.user_fecho, marca.seq_Req_tubo "
            gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id, "
            gSqlSelResFROM1 = gSqlSelResFROM1 & " sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto pr "
        ElseIf gSGBD = gSqlServer Then
            gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.n_folha_trab, NULL AS user_cri, Null  AS dt_cri, NULL AS hr_cri, NULL AS user_act, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "Null  AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, Null  AS dt_val, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "NULL AS hr_val, NULL AS user_tec_val, Null  AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "req.dt_chega AS dt_chega_req, req.hr_chega as hr_chega_req, req.n_req_assoc, req.estado_req, id.seq_utente, id.t_utente, id.utente, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado,req.inf_complementar , pr.ordem ord_produto, slv_analises.cod_gr_ana,req.cod_proven, req.tipo_urgencia, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2,req.cod_efr, req.user_fecho, marca.seq_Req_tubo "
            gSqlSelResFROM1 = " FROM sl_marcacoes marca LEFT OUTER JOIN sl_perfis p on (marca.cod_perfil = p.cod_perfis) " & _
                          " LEFT OUTER JOIN sl_ana_c c on (marca.cod_ana_c = c.cod_ana_c) " & _
                          " LEFT OUTER JOIN sl_ana_s s on (marca.cod_ana_s = s.cod_ana_s) " & _
                          " LEFT OUTER JOIN sl_segundo_res s2 on (marca.cod_ana_s = s2.cod_ana_s), " & _
                          " sl_requis req, sl_identif id "
        End If
    
    Else
        If gSGBD = gOracle Then
            gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.n_folha_trab, NULL AS user_cri, to_date(Null) AS dt_cri, NULL AS hr_cri, NULL AS user_act, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "to_date(Null) AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, to_date(Null) AS dt_val, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "NULL AS hr_val, NULL AS user_tec_val, to_date(Null) AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "req.dt_chega AS dt_chega_req,  req.hr_chega as hr_chega_req,  req.n_req_assoc, req.estado_req, id.seq_utente, id.t_utente, id.utente, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "null as n_res_frase, null as cod_frase, -1 as ord_frase, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "null n_res_micro, null as cod_micro, null as quantif,null as flg_imp,null as flg_tsq, null as flg_testes, null as prova,null as cod_gr_antibio, req.inf_complementar , pr.ordem ord_produto, slv_analises.cod_gr_ana,req.cod_proven, req.tipo_urgencia, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2,req.cod_efr, req.user_fecho, marca.seq_Req_tubo "
            gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id, "
            gSqlSelResFROM1 = gSqlSelResFROM1 & " sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto pr "
        ElseIf gSGBD = gSqlServer Then
            gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "marca.n_folha_trab, NULL AS user_cri, Null  AS dt_cri, NULL AS hr_cri, NULL AS user_act, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "Null  AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, Null  AS dt_val, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "NULL AS hr_val, NULL AS user_tec_val, Null  AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "req.dt_chega AS dt_chega_req,  req.hr_chega as hr_chega_req,  req.n_req_assoc, req.estado_req, id.seq_utente, id.t_utente, id.utente, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "null as n_res_frase, null as cod_frase, -1 as ord_frase, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & "null n_res_micro, null as cod_micro, null as quantif,null as flg_imp,null as flg_tsq, null as flg_testes, null as prova,null as cod_gr_antibio, req.inf_complementar , pr.ordem ord_produto, slv_analises.cod_gr_ana,req.cod_proven, req.tipo_urgencia, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, "
            gSqlSelResSELECT1 = gSqlSelResSELECT1 & " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2,req.cod_efr, req.user_fecho, marca.seq_Req_tubo "
            gSqlSelResFROM1 = " FROM sl_marcacoes marca LEFT OUTER JOIN sl_perfis p on (marca.cod_perfil = p.cod_perfis) " & _
                          " LEFT OUTER JOIN sl_ana_c c on (marca.cod_ana_c = c.cod_ana_c) " & _
                          " LEFT OUTER JOIN sl_ana_s s on (marca.cod_ana_s = s.cod_ana_s) " & _
                          " LEFT OUTER JOIN sl_segundo_res s2 on (marca.cod_ana_s = s2.cod_ana_s), " & _
                          " sl_requis req, sl_identif id,  "
        End If
    End If
    If EcListaGrAna.ListCount > 0 Then                         ' SE FOI INDICADO ALGUM GRUPO
        gSqlSelResFROM1 = gSqlSelResFROM1 & ", sl_gr_ana "
    End If
    
    If EcListaProven.ListCount > 0 Then                         ' SE FOI INDICADO ALGUMA PROVENIENCIA
        If gTipoInstituicao = "HOSPITALAR" Then
            gSqlSelResFROM1 = gSqlSelResFROM1 & ", sl_proven "
        ElseIf gTipoInstituicao = "PRIVADA" Then
            gSqlSelResFROM1 = gSqlSelResFROM1 & ", sl_cod_salas "
        End If
    End If
    
    If EcListaGrTrab.ListCount > 0 Then                         ' SE FOI INDICADO ALGUM GRUPO TRABALHO
        gSqlSelResFROM1 = gSqlSelResFROM1 & ", sl_gr_trab, sl_ana_trab "
    End If
    
    If gSGBD = gOracle Then
        gSqlSelResFROM1 = gSqlSelResFROM1 & ", slv_analises "
    ElseIf gSGBD = gSqlServer Then
        gSqlSelResFROM1 = gSqlSelResFROM1 & ", slv_analises  LEFT OUTER JOIN sl_produto pr ON slv_analises.cod_produto = pr.cod_produto "
    End If

    gSqlSelResWHERE1 = gSqlSelResWHERE1 & "WHERE req.n_req = marca.n_req AND "
    gSqlSelResWHERE1 = gSqlSelResWHERE1 & "id.seq_utente = req.seq_utente AND marca.dt_chega IS NOT NULL AND marca.n_req = " & NReq
    
    If gSGBD = gOracle Then
        gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.cod_perfil = p.cod_perfis(+) " & _
                                " AND marca.cod_ana_c = c.cod_ana_c(+) " & _
                                " AND marca.cod_ana_s = s.cod_ana_s(+) " & _
                                " AND marca.cod_ana_s = s2.cod_ana_s(+) "
        gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND slv_analises.cod_produto = pr.cod_produto (+)"
    End If
    gSqlSelResWHERE1 = gSqlSelResWHERE1 & sqlWhere1
    
    gSqlSelResSELECT2 = " UNION "
    
    ' FM
    If EcGrAnalises <> CStr(gCodGrupoMicrobiologia) And EcGrAnalises <> CStr(gCodGrupoMicobacteriologia) Then
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "SELECT realiza.seq_realiza, realiza.n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req,  "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "req.n_req_assoc, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado, req.inf_complementar, pr.ordem ord_produto, slv_analises.cod_gr_ana,req.cod_proven, req.tipo_urgencia, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2,req.cod_efr, req.user_fecho, realiza.seq_Req_tubo "
        If gSGBD = gOracle Then
            gSqlSelResFROM2 = " FROM sl_realiza realiza, sl_requis req, sl_identif id, "
            gSqlSelResFROM2 = gSqlSelResFROM2 & " sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto pr "
        Else
            gSqlSelResFROM2 = " FROM sl_realiza realiza LEFT OUTER JOIN sl_perfis p on (realiza.cod_perfil = p.cod_perfis) " & _
                  " LEFT OUTER JOIN sl_ana_c c on (realiza.cod_ana_c = c.cod_ana_c) " & _
                  " LEFT OUTER JOIN sl_ana_s s on (realiza.cod_ana_s = s.cod_ana_s) " & _
                  " LEFT OUTER JOIN sl_segundo_res s2 on (realiza.cod_ana_s = s2.cod_ana_s), " & _
                  " sl_requis req, sl_identif id "
        End If
    Else
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "SELECT realiza.seq_realiza, realiza.n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req,  "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "req.n_req_assoc, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "frase.n_res n_res_frase, frase.cod_frase, frase.ord_frase, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & "micro.N_Res n_res_micro, micro.Cod_Micro, micro.Quantif, micro.flg_imp, micro.flg_tsq, micro.flg_testes, micro.Prova, micro.cod_gr_antibio, req.inf_complementar, pr.ordem ord_produto, slv_analises.cod_gr_ana,req.cod_proven, req.tipo_urgencia, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, "
        gSqlSelResSELECT2 = gSqlSelResSELECT2 & " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2,req.cod_efr, req.user_fecho, realiza.seq_Req_tubo  "
        If gSGBD = gOracle Then
            gSqlSelResFROM2 = " FROM sl_realiza realiza, sl_requis req, sl_identif id, sl_res_micro micro, sl_res_frase frase, "
            gSqlSelResFROM2 = gSqlSelResFROM2 & " sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto pr "
        Else
            gSqlSelResFROM2 = " FROM sl_realiza realiza LEFT OUTER JOIN sl_perfis p on (realiza.cod_perfil = p.cod_perfis) " & _
                  " LEFT OUTER JOIN sl_ana_c c on (realiza.cod_ana_c = c.cod_ana_c) " & _
                  " LEFT OUTER JOIN sl_ana_s s on (realiza.cod_ana_s = s.cod_ana_s) " & _
                  " LEFT OUTER JOIN sl_segundo_res s2 on (realiza.cod_ana_s = s2.cod_ana_s), " & _
                  " sl_requis req, sl_identif id "
        End If
    End If
    If EcListaGrAna.ListCount > 0 Then                         ' SE FOI INDICADO ALGUM GRUPO
        gSqlSelResFROM2 = gSqlSelResFROM2 & ", sl_gr_ana "
    End If
    
    If EcListaProven.ListCount > 0 Then                         ' SE FOI INDICADO ALGUMA PROVENIENCIA
        If gTipoInstituicao = "HOSPITALAR" Then
            gSqlSelResFROM2 = gSqlSelResFROM2 & ", sl_proven "
        ElseIf gTipoInstituicao = "PRIVADA" Then
            gSqlSelResFROM2 = gSqlSelResFROM2 & ", sl_cod_salas "
        End If
    End If
    
    If EcListaGrTrab.ListCount > 0 Then                         ' SE FOI INDICADO ALGUM GRUPO TRABALHO
        gSqlSelResFROM2 = gSqlSelResFROM2 & ", sl_gr_trab, sl_ana_trab "
    End If
    If gSGBD = gOracle Then
        gSqlSelResFROM2 = gSqlSelResFROM2 & ", slv_analises "
    ElseIf gSGBD = gSqlServer Then
        gSqlSelResFROM2 = gSqlSelResFROM2 & ", slv_analises  LEFT OUTER JOIN sl_produto pr ON slv_analises.cod_produto = pr.cod_produto "
    End If
' FM
    If EcGrAnalises <> CStr(gCodGrupoMicrobiologia) And EcGrAnalises <> CStr(gCodGrupoMicobacteriologia) Then
        gSqlSelResWHERE2 = "WHERE req.n_req = realiza.n_req AND "
        gSqlSelResWHERE2 = gSqlSelResWHERE2 & "id.seq_utente = req.seq_utente AND realiza.dt_chega IS NOT NULL AND realiza.n_req = " & NReq
    Else
        gSqlSelResWHERE2 = "WHERE req.n_req = realiza.n_req AND  "
        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " realiza.seq_realiza = frase.seq_realiza(+) and realiza.seq_realiza = micro.seq_realiza(+) and "
        gSqlSelResWHERE2 = gSqlSelResWHERE2 & "id.seq_utente = req.seq_utente AND realiza.dt_chega IS NOT NULL AND realiza.n_req = " & NReq
    End If
    
    If gSGBD = gOracle Then
        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND realiza.cod_perfil = p.cod_perfis(+) " & _
                                " AND realiza.cod_ana_c = c.cod_ana_c(+) " & _
                                " AND realiza.cod_ana_s = s.cod_ana_s(+) " & _
                                " AND realiza.cod_ana_s = s2.cod_ana_s(+) "
    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND slv_analises.cod_produto = pr.cod_produto (+)"
    End If
    gSqlSelResWHERE2 = gSqlSelResWHERE2 & sqlWhere2
    
    
    If CkTodas.value = 1 Then
        gSqlSelResWHERE2 = gSqlSelResWHERE2
    ElseIf gPermResUtil = 0 Or gPermResUtil = 1 Then
        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado = " & gEstadoAnaComResultado & ") "
    ElseIf gPermResUtil = 2 Then
        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado = " & gEstadoAnaComResultado & " OR realiza.flg_estado = " & gEstadoAnaValidacaoTecnica & ") "
    End If
    gSqlSelResWHERE2 = gSqlSelResWHERE2 & "))"
    
    
'    If gSGBD = gOracle Then
                
        gSqlSelResORDER = " ORDER BY n_req, ord_produto,cod_gr_ana, ord_ana, cod_agrup, cod_perfil, ord_ana_perf, " & _
                     "ana_c, ord_ana_compl, ana_s "
                     
        gSqlSelRes = gSqlSelResSELECT1 & gSqlSelResFROM1 & gSqlSelResWHERE1 & gSqlSelResSELECT2 & gSqlSelResFROM2 & gSqlSelResWHERE2 & gSqlSelResORDER
'    End If
    

    If CkTodas.value = 1 Then
        gModoRes = cModoResGeral
    Else
        gModoRes = cModoResInsVal
    End If
                
    If EcGrAnalises <> CStr(gCodGrupoMicrobiologia) And EcGrAnalises <> CStr(gCodGrupoMicobacteriologia) Then
    
        FormResultados.Show
        If CkApenasResultados.value = vbChecked Then
            FormResultados.ApenasResultados = True
        Else
            FormResultados.ApenasResultados = False
        End If
    Else
        FormResMicro.Show
        gModoRes = cModoResMicro
    End If


End Sub

' --------------------------------------------------------

' ENTRA NO NOVO ECRA DE RESULTADOS

' --------------------------------------------------------
Private Sub ModoResultadosNovo()

    ' CRITERIO PARA REQUISICOES
    ' --------------------------------------------------------
    gSqlSelReq = "SELECT ute.seq_utente, ute.t_utente, ute.utente, ute.nome_ute, ute.dt_nasc_ute, ute.sexo_ute, "
    gSqlSelReq = gSqlSelReq & " req.n_req, req.n_req_assoc, req.n_epis, req.t_sit, req.estado_req, req.dt_chega,req.dt_chega, "
    gSqlSelReq = gSqlSelReq & " req.hr_chega, req.inf_complementar, req.cod_proven, req.t_urg, req.obs_req, req.peso_ute, req.altura_ute, tipo_urgencia, "
    gSqlSelReq = gSqlSelReq & " req.user_fecho, req.dt_fecho, req.hr_fecho, ute.cod_genero, req.cod_efr, req.n_prescricao "
    gSqlSelReq = gSqlSelReq & ", req.flg_cm_fin, req.flg_diag_sec, ute.flg_cm_Tec, ute.flg_diag_pri,ute.cod_raca, req.cod_destino, sl_tbf_t_destino.flg_email, "
    gSqlSelReq = gSqlSelReq & " req.flg_hipo, ute.flg_ve, req.flg_seroteca,"
    If gRegistaColheitaAtiva = mediSim Then
        gSqlSelReq = gSqlSelReq & " colh.seq_colheita "
    Else
        gSqlSelReq = gSqlSelReq & " -1 seq_colheita "
    End If
    gSqlSelReq = gSqlSelReq & " FROM sl_requis req "
    If gRegistaColheitaAtiva = mediSim Then
        gSqlSelReq = gSqlSelReq & " JOIN sl_req_colheitas colh on req.n_Req = colh.n_req"
    End If
    gSqlSelReq = gSqlSelReq & " LEFT OUTER JOIN sl_tbf_t_destino ON sl_tbf_t_destino.cod_t_dest = req.cod_destino, sl_identif ute "
    gSqlSelReq = gSqlSelReq & " WHERE  ute.seq_utente = req.seq_utente AND req.n_req in ("
    gSqlSelReq = gSqlSelReq & BL_TrataStringParaBD(BL_HandleNull(FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq), 0)) & ", "
    gSqlSelReq = Mid(gSqlSelReq, 1, Len(gSqlSelReq) - 2) & ")"
    gSqlSelReq = gSqlSelReq & " ORDER BY ute.seq_utente, n_req "


    ' CRITERIO PARA RESULTADOS
    ' --------------------------------------------------------
    If gSGBD = gOracle Then
        gSqlSelRes = "SELECT req.seq_utente,req.n_req, r.seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, cod_agrup, flg_estado, r.user_cri, "
        gSqlSelRes = gSqlSelRes & " r.dt_cri, r.hr_cri, r.dt_act, r.hr_act, r.user_act, r.user_val, r.dt_val, r.hr_val, r.user_tec_val, r.dt_tec_val, "
        gSqlSelRes = gSqlSelRes & " r.hr_tec_val, '1' flg_apar_trans, r.flg_facturado, ord_marca, n_folha_trab,ord_ana, "
        gSqlSelRes = gSqlSelRes & " ord_ana_compl, ord_ana_perf,  RA.n_res, RA.result, r.dt_chega, 'REALIZA' tabela, cod_frase, ord_frase, r.flg_apar, "
        gSqlSelRes = gSqlSelRes & " rm.cod_micro, rm.quantif, rm.flg_imp, rm.flg_tsq, rm.cod_gr_antibio, rm.prova, rm.flg_testes "
        gSqlSelRes = gSqlSelRes & " , ra.flg_imprimir flg_imprimir_ra, rf.flg_imprimir flg_imprimir_rf, rm.flg_imprimir flg_imprimir_rm, rf.n_res N_RES_FRASE "
        gSqlSelRes = gSqlSelRes & ", ra.res_ant1 ra_res_ant1, ra.res_ant2 ra_res_ant2, ra.res_ant3 ra_res_ant3, ra.dt_res_ant1 ra_dt_res_ant1 , "
        gSqlSelRes = gSqlSelRes & " ra.dt_res_ant2 ra_dt_res_ant2,ra.dt_res_ant3 ra_dt_res_ant3, "
        gSqlSelRes = gSqlSelRes & " ra.seq_obs_ant1 ra_seq_obsant1, ra.seq_obs_ant2 ra_seq_obsant2, ra.seq_obs_ant3 ra_seq_obsant3, "
        gSqlSelRes = gSqlSelRes & " rf.seq_obs_ant1 rf_seq_obsant1, rf.seq_obs_ant2 rf_seq_obsant2, rf.seq_obs_ant3 rf_seq_obsant3 "
        gSqlSelRes = gSqlSelRes & ", r.flg_assinado, r.dt_assinado, r.hr_assinado, r.user_assinado, 1 transmit_psm, 1 n_envio,r.flg_anexo, r.flg_obs_ana, "
        gSqlSelRes = gSqlSelRes & " r.seq_req_tubo, r.flg_grafico, rm.flg_ve  flg_ve_micro, ra.flg_ve flg_ve, rf.flg_ve flg_ve_frase, rm.cod_carac_micro , r.user_apar, r.dt_apar"
        gSqlSelRes = gSqlSelRes & " , rm.flg_gr_antib_facturado "
        gSqlSelRes = gSqlSelRes & " FROM sl_realiza r,sl_res_alfan ra, sl_res_frase rf,sl_res_micro rm, sl_requis req, slv_analises ana {TUBO_FROM1} WHERE r.seq_realiza = ra.seq_realiza(+) "
        gSqlSelRes = gSqlSelRes & " AND r.seq_realiza = rf.seq_realiza (+) "
        gSqlSelRes = gSqlSelRes & " AND r.seq_realiza = rm.seq_realiza (+) "
        gSqlSelRes = gSqlSelRes & " AND r.n_req = req.n_req AND req.n_req in ( "
        gSqlSelRes = gSqlSelRes & BL_TrataStringParaBD(BL_HandleNull(FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq), 0))
        gSqlSelRes = gSqlSelRes & ")"
        gSqlSelRes = gSqlSelRes & " AND r.cod_agrup = ana.cod_ana"
        gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE1} "
    Else
        gSqlSelRes = "SELECT req.seq_utente,req.n_req, r.seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, cod_agrup, flg_estado, r.user_cri, "
        gSqlSelRes = gSqlSelRes & " r.dt_cri, r.hr_cri, r.dt_act, r.hr_act, r.user_act, r.user_val, r.dt_val, r.hr_val, r.user_tec_val, r.dt_tec_val, "
        gSqlSelRes = gSqlSelRes & " r.hr_tec_val, '1' flg_apar_trans, r.flg_facturado, ord_marca, n_folha_trab,ord_ana, "
        gSqlSelRes = gSqlSelRes & " ord_ana_compl, ord_ana_perf,  RA.n_res n_res, RA.result, r.dt_chega, 'REALIZA' tabela, cod_frase, ord_frase, r.flg_apar, "
        gSqlSelRes = gSqlSelRes & " rm.cod_micro, rm.quantif, rm.flg_imp, rm.flg_tsq, rm.cod_gr_antibio, rm.prova, rm.flg_testes "
        gSqlSelRes = gSqlSelRes & " , ra.flg_imprimir flg_imprimir_ra, rf.flg_imprimir flg_imprimir_rf, rm.flg_imprimir flg_imprimir_rm , rf.n_res N_RES_FRASE "
        gSqlSelRes = gSqlSelRes & ", ra.res_ant1 ra_res_ant1, ra.res_ant2 ra_res_ant2, ra.res_ant3 ra_res_ant3, ra.dt_res_ant1 ra_dt_res_ant1 , "
        gSqlSelRes = gSqlSelRes & " ra.dt_res_ant2 ra_dt_res_ant2,ra.dt_res_ant3 ra_dt_res_ant3, "
        gSqlSelRes = gSqlSelRes & " ra.seq_obs_ant1 ra_seq_obsant1, ra.seq_obs_ant2 ra_seq_obsant2, ra.seq_obs_ant3 ra_seq_obsant3, "
        gSqlSelRes = gSqlSelRes & " rf.seq_obs_ant1 rf_seq_obsant1, rf.seq_obs_ant2 rf_seq_obsant2, rf.seq_obs_ant3 rf_seq_obsant3 "
        gSqlSelRes = gSqlSelRes & ", r.flg_assinado, r.dt_assinado, r.hr_assinado, r.user_assinado, 1 transmit_psm, 1 n_envio, r.flg_anexo, r.flg_obs_ana,"
        gSqlSelRes = gSqlSelRes & " r.seq_req_tubo, r.flg_grafico, rm.flg_ve flg_ve_micro, ra.flg_ve flg_ve, rf.flg_ve flg_ve_frase ,rm.cod_carac_micro , r.user_apar, r.dt_apar"
        gSqlSelRes = gSqlSelRes & " , rm.flg_gr_antib_facturado "
        gSqlSelRes = gSqlSelRes & " FROM sl_realiza r LEFT OUTER JOIN sl_res_alfan ra ON r.seq_realiza = ra.seq_realiza "
        gSqlSelRes = gSqlSelRes & " LEFT OUTER JOIN  sl_res_frase rf ON r.seq_realiza = rf.seq_realiza LEFT OUTER JOIN  sl_reS_micro rm ON r.seq_realiza = rm.seq_realiza {TUBO_FROM1}, sl_requis req, slv_analises ana WHERE  "
        gSqlSelRes = gSqlSelRes & " r.n_req = req.n_req AND req.n_req in ( "
        gSqlSelRes = gSqlSelRes & BL_TrataStringParaBD(BL_HandleNull(FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq), 0))
        gSqlSelRes = gSqlSelRes & ")"
        gSqlSelRes = gSqlSelRes & " AND r.cod_agrup = ana.cod_ana"
        gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE1} "
    End If
    
    If CkTodas.value <> 1 Then
        If gPermResUtil = cValMedRes Then
            gSqlSelRes = gSqlSelRes & " AND r.flg_estado in (" & BL_TrataStringParaBD(gEstadoAnaValidacaoTecnica) & ","
            gSqlSelRes = gSqlSelRes & BL_TrataStringParaBD(gEstadoAnaComResultado) & ")"
        ElseIf gPermResUtil = cValTecRes Then
            gSqlSelRes = gSqlSelRes & " AND r.flg_estado in (" & BL_TrataStringParaBD(gEstadoAnaComResultado) & ")"
        End If
    End If
    
    If CkApenasResultados <> mediSim Then
        If gSGBD = gOracle Then
            gSqlSelRes = gSqlSelRes & " UNION SELECT req.seq_utente,req.n_req, -1 seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, r.cod_agrup, '-1' flg_estado, NULL user_cri, "
            gSqlSelRes = gSqlSelRes & " NULL dt_cri, NULL hr_cri, NULL dt_act, NULL hr_act, NULL user_act, null user_val, null dt_val, null hr_val, null user_tec_val, null dt_tec_val, "
            gSqlSelRes = gSqlSelRes & " null hr_tec_val, r.flg_apar_trans, r.flg_facturado, r.ord_marca,r.n_folha_trab,r.ord_ana, "
            gSqlSelRes = gSqlSelRes & " r.ord_ana_compl, r.ord_ana_perf, -1 n_res, NULL result, r.dt_chega, 'MARCACOES' tabela , null cod_frase, -1 ord_frase,0 flg_apar, "
            gSqlSelRes = gSqlSelRes & " NULL cod_micro, NULL quantif, NULL flg_imp, NULL flg_tsq, NULL cod_gr_antibio, NULL prova, NULL flg_testes "
            gSqlSelRes = gSqlSelRes & " , 1 flg_imprimir_ra, 1 flg_imprimir_rf, 1 flg_imprimir_rm, -11 N_RES_FRASE "
            gSqlSelRes = gSqlSelRes & ", null ra_res_ant1, null ra_res_ant2, null ra_res_ant3, null ra_dt_res_ant1 , "
            gSqlSelRes = gSqlSelRes & " null ra_dt_res_ant2,null ra_dt_res_ant3, "
            gSqlSelRes = gSqlSelRes & " null ra_seq_obsant1, null ra_seq_obsant2, null ra_seq_obsant3, "
            gSqlSelRes = gSqlSelRes & " null rf_seq_obsant1, null rf_seq_obsant2, null rf_seq_obsant3 "
            gSqlSelRes = gSqlSelRes & ", 0 flg_assinado, null dt_assinado,null hr_assinado, null user_assinado, r.transmit_psm, r.n_envio, 0 flg_anexo, "
            gSqlSelRes = gSqlSelRes & " 0 flg_obs_ana, r.seq_req_tubo, 0 flg_grafico, null flg_ve_micro, null flg_ve, null flg_ve_frase , null cod_carac_micro, null user_apar, null dt_apar  "
            gSqlSelRes = gSqlSelRes & " , 0 flg_gr_antib_facturado "
            gSqlSelRes = gSqlSelRes & " FROM sl_marcacoes r,  sl_requis req, slv_analises ana {TUBO_FROM2}WHERE "
            gSqlSelRes = gSqlSelRes & " r.n_req = req.n_req AND req.n_req in ( "
            gSqlSelRes = gSqlSelRes & BL_TrataStringParaBD(BL_HandleNull(FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq), 0))
            gSqlSelRes = gSqlSelRes & ")"
            gSqlSelRes = gSqlSelRes & " AND r.cod_agrup = ana.cod_ana"
            gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE2}"
        Else
            gSqlSelRes = gSqlSelRes & " UNION SELECT req.seq_utente,req.n_req, -1 seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, r.cod_agrup, '-1' flg_estado, NULL user_cri, "
            gSqlSelRes = gSqlSelRes & " NULL dt_cri, NULL hr_cri, NULL dt_act, NULL hr_act, NULL user_act, null user_val, null dt_val, null hr_val, null user_tec_val, null dt_tec_val, "
            gSqlSelRes = gSqlSelRes & " null hr_tec_val, r.flg_apar_trans, r.flg_facturado, r.ord_marca,r.n_folha_trab,r.ord_ana, "
            gSqlSelRes = gSqlSelRes & " r.ord_ana_compl, r.ord_ana_perf, -1 n_res, NULL result, r.dt_chega, 'MARCACOES' tabela , null cod_frase, -1 ord_frase,0 flg_apar, "
            gSqlSelRes = gSqlSelRes & " NULL cod_micro, NULL quantif, NULL flg_imp, NULL flg_tsq, NULL cod_gr_antibio, NULL prova, NULL flg_testes "
            gSqlSelRes = gSqlSelRes & " , 1 flg_imprimir_ra,1 flg_imprimir_rf, 1 flg_imprimir_rm, -1 N_RES_FRASE "
            gSqlSelRes = gSqlSelRes & ", null ra_res_ant1, null ra_res_ant2, null ra_res_ant3, null ra_dt_res_ant1 , "
            gSqlSelRes = gSqlSelRes & " null ra_dt_res_ant2,null ra_dt_res_ant3, "
            gSqlSelRes = gSqlSelRes & " null ra_seq_obsant1, null ra_seq_obsant2, null ra_seq_obsant3, "
            gSqlSelRes = gSqlSelRes & " null rf_seq_obsant1, null rf_seq_obsant2, null rf_seq_obsant3 "
            gSqlSelRes = gSqlSelRes & ", 0 flg_assinado, null dt_assinado,null hr_assinado, null user_assinado, r.transmit_psm, r.n_envio, 0 flg_anexo, "
            gSqlSelRes = gSqlSelRes & " 0 flg_obs_ana, r.seq_req_tubo, 0 flg_grafico, null flg_ve_micro, null flg_ve, null flg_ve_frase , null cod_carac_micro, null user_apar, null dt_apar  "
            gSqlSelRes = gSqlSelRes & " , 0 flg_gr_antib_facturado "
            gSqlSelRes = gSqlSelRes & " FROM sl_marcacoes r {TUBO_FROM2},  sl_requis req, slv_analises ana WHERE "
            gSqlSelRes = gSqlSelRes & " r.n_req = req.n_req AND req.n_req in ( "
            gSqlSelRes = gSqlSelRes & BL_TrataStringParaBD(BL_HandleNull(FGReqPendRes.TextMatrix(FGReqPendRes.row, lColNumReq), 0))
            gSqlSelRes = gSqlSelRes & ")"
            gSqlSelRes = gSqlSelRes & " AND r.cod_agrup = ana.cod_ana"
            gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE2}"
        End If
    End If
    If gSGBD = gOracle Then
        gSqlSelRes = gSqlSelRes & " ORDER BY seq_utente, n_req,ord_ana, ord_ana_perf, ord_ana_compl, n_res, ord_frase"
    ElseIf gSGBD = gSqlServer Then
        gSqlSelRes = gSqlSelRes & " ORDER BY req.seq_utente, req.n_req,ord_ana, ord_ana_perf, ord_ana_compl, n_res, ord_frase"
    End If
    FormResultadosNovo.Show
    FormResultadosNovo.FuncaoProcurar

End Sub

Private Function ConstroiCriterio() As String
    Dim sSql As String
    Dim i As Integer
    
    If EcListaGrAna.ListCount > 0 Then                         ' SE FOI INDICADO ALGUM GRUPO
        sSql = sSql & " AND ana.cod_gr_ana in ( "
        For i = 0 To EcListaGrAna.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_GR_ANA", "COD_GR_ANA", "SEQ_GR_ANA", EcListaGrAna.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If


    If EcListaGrTrab.ListCount > 0 Then                          ' SE FOI INDICADO ALGUM GRUPO TRABALHO
        sSql = sSql & " AND ana.cod_ana IN (SELECT cod_analise FROM sl_ana_trab WHERE cod_gr_trab IN ( "
        For i = 0 To EcListaGrTrab.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_GR_TRAB", "COD_GR_TRAB", "SEQ_GR_TRAB", EcListaGrTrab.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
        
    End If
    
        
    If gMultiLocalAmbito = mediSim Then
        sSql = sSql & " AND (ana.cod_ana IN (SELECT cod_agrup FROM sl_ana_acrescentadas WHERE  n_req = r.n_req AND (cod_local = " & gCodLocal & " OR cod_local IS NULL))) "
    Else
        sSql = sSql & " AND (ana.cod_ana IN (SELECT COD_ANA FROM sl_ana_locais_exec WHERE cod_local = " & gCodLocal & ")) "
    End If
    
    ' ------------------------------------
    ' SE FOI INDICADO ALGUMA ANALISE
    ' ------------------------------------
    If EcListaAnalises.ListCount > 0 Then
        If Opt1(0).value = True Then
            sSql = sSql & " AND ana.cod_ana IN ( "
        Else
            sSql = sSql & " AND ana.cod_ana NOT IN ( "
        End If
        For i = 0 To EcListaAnalises.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("slv_analises", "COD_ANA", "SEQ_ANA", EcListaAnalises.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------
    ' SE FOI INDICADO ALGUM PRODUTO
    ' ------------------------------------
    If EcListaProd.ListCount > 0 Then
        sSql = sSql & " AND ana.cod_produto  in ( "
        For i = 0 To EcListaProd.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_PRODUTO", "COD_PRODUTO", "SEQ_PRODUTO", EcListaProd.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    'SE FILTROU ALGUM APARELHO
    If EcListaApar.ListCount > 0 Then
        If gSGBD = gOracle Then
            sSql = sSql & " AND r.cod_agrup IN (SELECT cod_agrup FROM gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.NAO_entra_pendentes = 0 or gc_ana_apar.NAO_entra_pendentes IS NULL) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        ElseIf gSGBD = gSqlServer Then
            sSql = sSql & " AND r.cod_agrup IN (SELECT cod_agrup FROM gescom.dbo.gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.nao_entra_pendentes = 0 or gc_ana_apar.nao_entra_pendentes is null) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        End If
        
        For i = 0 To EcListaApar.ListCount - 1
            sSql = sSql & EcListaApar.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
    End If
    
    ConstroiCriterio = sSql
End Function


Private Function constroiCriterioGERAL() As String
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = ""
    
    ' -----------------------------------
    ' SE NAO FOI INDICADO ALGUMA DATA
    ' -----------------------------------
    sSql = sSql & " AND (req.dt_chega  BETWEEN " & BL_TrataDataParaBD(lDataInicial)
    sSql = sSql & " AND " & BL_TrataDataParaBD(lDataFinal) & " )"
    'ssql = ssql & " AND req.estado_req not in('D','F','C') "
    
    
    ' ------------------------------------
    ' SE  FOI INDICADO APENAS REQ URG
    ' ------------------------------------
    If CkUrgentes.value = vbChecked Then
        sSql = sSql & " AND req.t_urg in ('1','2') "
    End If
    
    ' ------------------------------------
    ' SE  FOI INDICADO APENAS REQ COM TUBO
    ' ------------------------------------
    If CkRequisComTubo.value = vbChecked Then
        sSql = sSql & " AND ana.cod_tubo NOT IN (SELECT cod_tubo FROM sl_req_tubo WHERE n_Req = req.n_req and dt_chega is null)"
    End If
    
    ' ------------------------------------
    ' SE  FOI INDICADO APENAS REQ ELECTRON
    ' ------------------------------------
    If (CkUserAltert.value = vbChecked) Then
        sSql = sSql & " AND req.user_cri = " & BL_TrataStringParaBD(gCodUtilizadorHL7)
    End If
    
    ' -------------------------------------------------------
    ' SE FOI INDICADO REQUISICOES SEM ANALISES SEM RESULTADOS
    ' -------------------------------------------------------
    If CkCompletas.value = 1 Then
        sSql = sSql & " AND req.n_req NOT IN (SELECT n_req FROM sl_marcacoes where cod_ana_s <> 'S99999' ) "
    End If
    
    If EcListaProven.ListCount > 0 And gTipoInstituicao = "HOSPITALAR" Then
        ' -----------------------------------
        ' SE FOI INDICADO ALGUMA PROVINIENCIA
        ' -----------------------------------
        sSql = sSql & " AND req.cod_proven IN ( "
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & BL_SelCodigo("SL_PROVEN", "COD_PROVEN", "SEQ_PROVEN", EcListaProven.ItemData(i)) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        
    ElseIf EcListaProven.ListCount > 0 And gTipoInstituicao = "PRIVADA" Then
    
        ' -----------------------------------
        ' SE FOI INDICADO ALGUMA SALA/POSTO
        ' -----------------------------------
        sSql = sSql & " AND req.cod_sala IN ( "
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & BL_SelCodigo("SL_COD_SALAS", "COD_SALA", "SEQ_SALA", EcListaProven.ItemData(i)) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    
    ' ------------------------------------
    ' SE  FOI INDICADO ALGUM GRUPO ANA
    ' ------------------------------------
    If EcListaGrAna.ListCount > 0 Then
        sSql = sSql & " AND ana.cod_gr_ana in ( "
        For i = 0 To EcListaGrAna.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_GR_ANA", "COD_GR_ANA", "SEQ_GR_ANA", EcListaGrAna.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------
    ' SE FOI INDICADO ALGUM GRUPO TRABALHO
    ' ------------------------------------
    If EcListaGrTrab.ListCount > 0 Then
        sSql = sSql & " AND ana.cod_ana IN (SELECT cod_analise FROM sl_ana_trab WHERE cod_gr_trab IN ( "
        For i = 0 To EcListaGrTrab.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_GR_TRAB", "COD_GR_TRAB", "SEQ_GR_TRAB", EcListaGrTrab.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
        
    End If
    
    ' ------------------------------------
    ' SE FOI INDICADO ALGUM LOCAL
    ' ------------------------------------
    If EcListaLocais.ListCount > 0 Then
        If Opt2(0).value = True Then
            'SE USA AMBITO USA O LOCAL QUE FOI MARCARDO (SL_ANA_ACRESCENTADAS)
            If gMultiLocalAmbito = mediSim Then
                sSql = sSql & " AND (ana.cod_ana IN (SELECT COD_AGRUP FROM SL_ANA_ACRESCENTADAS WHERE N_REQ = req.N_REQ and (cod_local IS NULL OR cod_local IN ( "
                For i = 0 To EcListaLocais.ListCount - 1
                    sSql = sSql & EcListaLocais.ItemData(i) & ", "
                Next i
                sSql = Mid(sSql, 1, Len(sSql) - 2) & ")))) "
            Else
                sSql = sSql & " AND (ana.cod_ana IN (SELECT COD_ANA FROM sl_ana_locais_exec WHERE cod_local IN ( "
                For i = 0 To EcListaLocais.ListCount - 1
                    sSql = sSql & EcListaLocais.ItemData(i) & ", "
                Next i
                sSql = Mid(sSql, 1, Len(sSql) - 2) & "))) "
            End If
        ElseIf Opt2(1).value = True Then
            sSql = sSql & " AND (req.cod_local IN ( "
            For i = 0 To EcListaLocais.ListCount - 1
                sSql = sSql & EcListaLocais.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & "))"
        End If
    End If
    
    
    ' ------------------------------------
    ' SE FOI INDICADO ALGUM PRODUTO
    ' ------------------------------------
    If EcListaProd.ListCount > 0 Then
        sSql = sSql & " AND ana.cod_produto  in ( "
        For i = 0 To EcListaProd.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_PRODUTO", "COD_PRODUTO", "SEQ_PRODUTO", EcListaProd.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
            
    
    ' ------------------------------------
    ' SE FOI INDICADO ALGUMA ANALISE
    ' ------------------------------------
    If EcListaAnalises.ListCount > 0 Then
        If Opt1(0).value = True Then
            sSql = sSql & " AND ana.cod_ana IN ( "
        Else
            sSql = sSql & " AND ana.cod_ana NOT IN ( "
        End If
        For i = 0 To EcListaAnalises.ListCount - 1
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("slv_analises", "COD_ANA", "SEQ_ANA", EcListaAnalises.ItemData(i))) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
        
    'SE FILTROU ALGUM APARELHO
    If EcListaApar.ListCount > 0 Then
        If gSGBD = gOracle Then
            sSql = sSql & " AND res.cod_agrup IN (SELECT cod_agrup FROM gc_ana_apar gc_ana_apar wHERE (gc_ana_apar.NAO_entra_pendentes = 0 or gc_ana_apar.NAO_entra_pendentes IS NULL) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        ElseIf gSGBD = gSqlServer Then
            sSql = sSql & " AND res.cod_agrup IN (SELECT cod_agrup FROM gescom.dbo.gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.nao_entra_pendentes = 0 or gc_ana_apar.nao_entra_pendentes is null) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
        End If

        For i = 0 To EcListaApar.ListCount - 1
            sSql = sSql & EcListaApar.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
    End If
        
    constroiCriterioGERAL = sSql
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "constroiCriterioGERAL", False
    Exit Function
    Resume Next

End Function



Private Sub BtResutadosNovo_Click()
   On Error GoTo TrataErro
    If gCodGrupo = gGrupoSecretariado Then
        Exit Sub
    End If

    ' CRITERIO PARA REQUISICOES
    ' --------------------------------------------------------
    gSqlSelReq = "SELECT ute.seq_utente, ute.t_utente, ute.utente, ute.nome_ute, ute.dt_nasc_ute, ute.sexo_ute, "
    gSqlSelReq = gSqlSelReq & " req.n_req, req.n_req_assoc, req.n_epis, req.t_sit, req.estado_req, req.dt_chega,req.dt_chega, "
    gSqlSelReq = gSqlSelReq & " req.hr_chega, req.inf_complementar, req.cod_proven, req.t_urg, req.obs_req, req.altura_ute, req.peso_ute, tipo_urgencia, "
    gSqlSelReq = gSqlSelReq & " req.user_fecho, req.dt_fecho, req.hr_fecho,ute.cod_genero, req.cod_efr, req.n_prescricao"
    gSqlSelReq = gSqlSelReq & ", req.flg_cm_fin, req.flg_diag_sec, ute.flg_cm_Tec, ute.flg_diag_pri,ute.cod_raca, req.cod_destino, sl_tbf_t_destino.flg_email, "
    gSqlSelReq = gSqlSelReq & " req.flg_hipo, ute.flg_ve, req.flg_seroteca, "
    
    If gRegistaColheitaAtiva = mediSim Then
        gSqlSelReq = gSqlSelReq & " colh.seq_colheita "
    Else
        gSqlSelReq = gSqlSelReq & " -1 seq_colheita "
    End If
    
    gSqlSelReq = gSqlSelReq & " FROM sl_requis req "
    If gRegistaColheitaAtiva = mediSim Then
        gSqlSelReq = gSqlSelReq & " JOIN sl_req_colheitas colh on req.n_Req = colh.n_req"
    End If
    gSqlSelReq = gSqlSelReq & " LEFT OUTER JOIN sl_tbf_t_destino ON sl_tbf_t_destino.cod_t_dest = req.cod_destino, sl_identif ute, sl_realiza res, slv_analises ana"
    gSqlSelReq = gSqlSelReq & " WHERE  ute.seq_utente = req.seq_utente  AND req.n_Req = res.n_Req AND res.cod_agrup = ana.cod_ana"
    If gPermResUtil = cValMedRes Then
        gSqlSelReq = gSqlSelReq & " AND res.flg_estado in ('1','5' ) "
    ElseIf gPermResUtil = cValTecRes Then
        gSqlSelReq = gSqlSelReq & " AND res.flg_estado in ('1') "
    End If
    gSqlSelReq = gSqlSelReq & constroiCriterioGERAL
    If CkApenasResultados <> mediSim And CkCompletas.value <> vbChecked Then
        gSqlSelReq = gSqlSelReq & " UNION SELECT ute.seq_utente, ute.t_utente, ute.utente, ute.nome_ute, ute.dt_nasc_ute, ute.sexo_ute, "
        gSqlSelReq = gSqlSelReq & " req.n_req, req.n_req_assoc, req.n_epis, req.t_sit, req.estado_req, req.dt_chega,req.dt_chega, "
        gSqlSelReq = gSqlSelReq & " req.hr_chega, req.inf_complementar, req.cod_proven, req.t_urg, req.obs_req, req.altura_ute, req.peso_ute, tipo_urgencia, "
        gSqlSelReq = gSqlSelReq & " req.user_fecho, req.dt_fecho, req.hr_fecho,ute.cod_genero, req.cod_efr, req.n_prescricao"
        gSqlSelReq = gSqlSelReq & ", req.flg_cm_fin, req.flg_diag_sec, ute.flg_cm_Tec, ute.flg_diag_pri,ute.cod_raca, req.cod_destino, sl_tbf_t_destino.flg_email, "
        gSqlSelReq = gSqlSelReq & " req.flg_hipo, ute.flg_ve, req.flg_seroteca, "
        If gRegistaColheitaAtiva = mediSim Then
            gSqlSelReq = gSqlSelReq & " colh.seq_colheita "
        Else
            gSqlSelReq = gSqlSelReq & " -1 seq_colheita "
        End If
        gSqlSelReq = gSqlSelReq & " FROM sl_requis req "
        If gRegistaColheitaAtiva = mediSim Then
            gSqlSelReq = gSqlSelReq & " JOIN sl_req_colheitas colh on req.n_Req = colh.n_req"
        End If
        gSqlSelReq = gSqlSelReq & " LEFT OUTER JOIN sl_tbf_t_destino ON sl_tbf_t_destino.cod_t_dest = req.cod_destino, sl_identif ute, sl_marcacoes res, slv_analises ana"
        gSqlSelReq = gSqlSelReq & " WHERE  ute.seq_utente = req.seq_utente  AND req.n_Req = res.n_Req AND res.cod_agrup = ana.cod_ana "
        gSqlSelReq = gSqlSelReq & constroiCriterioGERAL
    End If
    If gSGBD = gOracle Then
        gSqlSelReq = gSqlSelReq & " ORDER BY n_req "
    Else
        gSqlSelReq = gSqlSelReq & " ORDER BY req.n_req "
    End If

    ' CRITERIO PARA RESULTADOS
    ' --------------------------------------------------------
    If gSGBD = gOracle Then
        gSqlSelRes = "SELECT req.seq_utente,req.n_req, r.seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, cod_agrup, flg_estado, r.user_cri, "
        gSqlSelRes = gSqlSelRes & " r.dt_cri, r.hr_cri, r.dt_act, r.hr_act, r.user_act, r.user_val, r.dt_val, r.hr_val, r.user_tec_val, r.dt_tec_val, "
        gSqlSelRes = gSqlSelRes & " r.hr_tec_val, '1' flg_apar_trans, r.flg_facturado, ord_marca, n_folha_trab,ord_ana, "
        gSqlSelRes = gSqlSelRes & " ord_ana_compl, ord_ana_perf,  RA.n_res, RA.result, r.dt_chega, 'REALIZA' tabela, cod_frase, ord_frase, r.flg_apar, "
        gSqlSelRes = gSqlSelRes & " rm.cod_micro, rm.quantif, rm.flg_imp, rm.flg_tsq, rm.cod_gr_antibio, rm.prova, rm.flg_testes "
        gSqlSelRes = gSqlSelRes & " , ra.flg_imprimir flg_imprimir_ra, rf.flg_imprimir flg_imprimir_rf, rm.flg_imprimir flg_imprimir_rm, rf.n_res N_RES_FRASE "
        gSqlSelRes = gSqlSelRes & ", ra.res_ant1 ra_res_ant1, ra.res_ant2 ra_res_ant2, ra.res_ant3 ra_res_ant3, ra.dt_res_ant1 ra_dt_res_ant1 , "
        gSqlSelRes = gSqlSelRes & " ra.dt_res_ant2 ra_dt_res_ant2,ra.dt_res_ant3 ra_dt_res_ant3, "
        gSqlSelRes = gSqlSelRes & " ra.seq_obs_ant1 ra_seq_obsant1, ra.seq_obs_ant2 ra_seq_obsant2, ra.seq_obs_ant3 ra_seq_obsant3, "
        gSqlSelRes = gSqlSelRes & " rf.seq_obs_ant1 rf_seq_obsant1, rf.seq_obs_ant2 rf_seq_obsant2, rf.seq_obs_ant3 rf_seq_obsant3 "
        gSqlSelRes = gSqlSelRes & ", r.flg_assinado, r.dt_assinado, r.hr_assinado, r.user_assinado, 1 transmit_psm, 1 n_envio, r.flg_anexo, r.flg_obs_ana, r.seq_req_tubo, r.flg_grafico, rm.flg_ve "
        gSqlSelRes = gSqlSelRes & " flg_ve_micro, ra.flg_ve flg_ve, rf.flg_ve flg_ve_frase , rm.cod_carac_micro, r.user_apar, r.dt_apar  "
        gSqlSelRes = gSqlSelRes & " , rm.flg_gr_antib_facturado "
        gSqlSelRes = gSqlSelRes & " FROM sl_realiza r,sl_res_alfan ra, sl_res_frase rf, sl_res_micro rm, sl_requis req, slv_analises ana {TUBO_FROM1} WHERE r.seq_realiza = ra.seq_realiza(+) "
        gSqlSelRes = gSqlSelRes & " AND r.seq_realiza = rf.seq_realiza (+) AND r.seq_realiza = rm.seq_realiza (+) AND r.cod_agrup = ana.cod_ana "
        gSqlSelRes = gSqlSelRes & " AND r.n_req = req.n_req AND req.n_req = {N_REQ}  "
        gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE1}"
    ElseIf gSGBD = gSqlServer Then
        gSqlSelRes = "SELECT req.seq_utente,req.n_req, r.seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, cod_agrup, flg_estado, r.user_cri, "
        gSqlSelRes = gSqlSelRes & " r.dt_cri, r.hr_cri, r.dt_act, r.hr_act, r.user_act, r.user_val, r.dt_val, r.hr_val, r.user_tec_val, r.dt_tec_val, "
        gSqlSelRes = gSqlSelRes & " r.hr_tec_val, '1' flg_apar_trans, r.flg_facturado, ord_marca, n_folha_trab,ord_ana, "
        gSqlSelRes = gSqlSelRes & " ord_ana_compl, ord_ana_perf,  RA.n_res n_res, RA.result, r.dt_chega, 'REALIZA' tabela, cod_frase, ord_frase, r.flg_apar, "
        gSqlSelRes = gSqlSelRes & " rm.cod_micro, rm.quantif, rm.flg_imp, rm.flg_tsq, rm.cod_gr_antibio, rm.prova, rm.flg_testes "
        gSqlSelRes = gSqlSelRes & " , ra.flg_imprimir flg_imprimir_ra, rf.flg_imprimir flg_imprimir_rf, rm.flg_imprimir flg_imprimir_rm, rf.n_res N_RES_FRASE "
        gSqlSelRes = gSqlSelRes & ", ra.res_ant1 ra_res_ant1, ra.res_ant2 ra_res_ant2, ra.res_ant3 ra_res_ant3, ra.dt_res_ant1 ra_dt_res_ant1 , "
        gSqlSelRes = gSqlSelRes & " ra.dt_res_ant2 ra_dt_res_ant2,ra.dt_res_ant3 ra_dt_res_ant3, "
        gSqlSelRes = gSqlSelRes & " ra.seq_obs_ant1 ra_seq_obsant1, ra.seq_obs_ant2 ra_seq_obsant2, ra.seq_obs_ant3 ra_seq_obsant3, "
        gSqlSelRes = gSqlSelRes & " rf.seq_obs_ant1 rf_seq_obsant1, rf.seq_obs_ant2 rf_seq_obsant2, rf.seq_obs_ant3 rf_seq_obsant3 "
        gSqlSelRes = gSqlSelRes & ", r.flg_assinado, r.dt_assinado, r.hr_assinado, r.user_assinado, 1 transmit_psm, 1 n_envio, r.flg_anexo, r.flg_obs_ana, r.seq_req_tubo, r.flg_grafico , "
        gSqlSelRes = gSqlSelRes & " rm.flg_ve flg_ve_micro, ra.flg_ve flg_ve, rf.flg_ve flg_ve_frase , rm.cod_carac_micro, r.user_apar, r.dt_apar  "
        gSqlSelRes = gSqlSelRes & " FROM sl_realiza r LEFT OUTER JOIN sl_res_alfan ra ON r.seq_realiza = ra.seq_realiza "
        gSqlSelRes = gSqlSelRes & " LEFT OUTER JOIN  sl_res_frase rf ON r.seq_realiza = rf.seq_realiza LEFT OUTER JOIN  sl_res_micro rm ON r.seq_realiza = rm.seq_realiza {TUBO_FROM1}, "
        gSqlSelRes = gSqlSelRes & " sl_requis req, slv_analises ana WHERE  "
        gSqlSelRes = gSqlSelRes & " r.cod_agrup = ana.cod_ana AND "
        gSqlSelRes = gSqlSelRes & " r.n_req = req.n_req AND req.n_req = {N_REQ}"
        gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE1}"
    End If
    
    If CkTodas.value <> 1 Then
        If gPermResUtil = cValMedRes Then
            gSqlSelRes = gSqlSelRes & " AND r.flg_estado in (" & BL_TrataStringParaBD(gEstadoAnaValidacaoTecnica) & ","
            gSqlSelRes = gSqlSelRes & BL_TrataStringParaBD(gEstadoAnaComResultado) & ")"
        ElseIf gPermResUtil = cValTecRes Then
            gSqlSelRes = gSqlSelRes & " AND r.flg_estado in (" & BL_TrataStringParaBD(gEstadoAnaComResultado) & ")"
        End If
    End If
    If CkApenasResultados.value <> vbChecked And CkCompletas.value <> vbChecked Then
    
        If gSGBD = gOracle Then
            gSqlSelRes = gSqlSelRes & " UNION SELECT req.seq_utente,req.n_req, -1 seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, r.cod_agrup, '-1' flg_estado, NULL user_cri, "
            gSqlSelRes = gSqlSelRes & " NULL dt_cri, NULL hr_cri, NULL dt_act, NULL hr_act, NULL user_act, null user_val, null dt_val, null hr_val, null user_tec_val, null dt_tec_val, "
            gSqlSelRes = gSqlSelRes & " null hr_tec_val, r.flg_apar_trans, r.flg_facturado, r.ord_marca,r.n_folha_trab,r.ord_ana, "
            gSqlSelRes = gSqlSelRes & " r.ord_ana_compl, r.ord_ana_perf, NULL n_res, NULL result, r.dt_chega, 'MARCACOES' tabela , null cod_frase, null ord_frase, 0 flg_apar, "
            gSqlSelRes = gSqlSelRes & " null cod_micro, null quantif, null flg_imp, null flg_tsq, null cod_gr_antibio, null prova, null flg_testes "
            gSqlSelRes = gSqlSelRes & " , 1 flg_imprimir_ra, 1 flg_imprimir_rf, 1 flg_imprimir_rm, null N_RES_FRASE "
            gSqlSelRes = gSqlSelRes & ", null ra_res_ant1, null ra_res_ant2, null ra_res_ant3, null ra_dt_res_ant1, "
            gSqlSelRes = gSqlSelRes & " null ra_dt_res_ant2, null ra_dt_res_ant3, null ra_seq_obsant1, null ra_seq_obsant2, null ra_seq_obsant3, null rf_seq_obsant1, null rf_seq_obsant2, null rf_seq_obsant3 "
            gSqlSelRes = gSqlSelRes & ", 0 flg_assinado, null dt_assinado,null hr_assinado, null user_assinado, r.transmit_psm, r.n_envio, 0 flg_anexo, 0 flg_obs_ana, r.seq_req_tubo, 0 flg_grafico, "
            gSqlSelRes = gSqlSelRes & " null flg_ve_micro, null flg_ve, null flg_ve_frase , null cod_carac_micro, null user_apar, null dt_apar "
            gSqlSelRes = gSqlSelRes & " , 0 flg_gr_antib_facturado "
            gSqlSelRes = gSqlSelRes & " FROM sl_marcacoes r,  sl_requis req, slv_analises ana {TUBO_FROM2} WHERE "
            gSqlSelRes = gSqlSelRes & " r.cod_agrup = ana.cod_ana AND "
            gSqlSelRes = gSqlSelRes & " r.n_req = req.n_req AND req.n_req = {N_REQ}"
            gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE2}"
        ElseIf gSGBD = gSqlServer Then
            gSqlSelRes = gSqlSelRes & " UNION SELECT req.seq_utente,req.n_req, -1 seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, r.cod_agrup, '-1' flg_estado, NULL user_cri, "
            gSqlSelRes = gSqlSelRes & " NULL dt_cri, NULL hr_cri, NULL dt_act, NULL hr_act, NULL user_act, null user_val, null dt_val, null hr_val, null user_tec_val, null dt_tec_val, "
            gSqlSelRes = gSqlSelRes & " null hr_tec_val, r.flg_apar_trans, r.flg_facturado, r.ord_marca,r.n_folha_trab,r.ord_ana, "
            gSqlSelRes = gSqlSelRes & " r.ord_ana_compl, r.ord_ana_perf, NULL n_res, NULL result, r.dt_chega, 'MARCACOES' tabela , null cod_frase, null ord_frase, 0 flg_apar, "
            gSqlSelRes = gSqlSelRes & " null cod_micro, null quantif, null flg_imp, null flg_tsq, null cod_gr_antibio, null prova, null flg_testes "
            gSqlSelRes = gSqlSelRes & " , 1 flg_imprimir_ra, 1 flg_imprimir_rf, 1 flg_imprimir_rm, null N_RES_FRASE "
            gSqlSelRes = gSqlSelRes & ", null ra_res_ant1, null ra_res_ant2, null ra_res_ant3, null ra_dt_res_ant1 , "
            gSqlSelRes = gSqlSelRes & " null ra_dt_res_ant2, null ra_dt_res_ant3,null ra_seq_obsant2, null ra_seq_obsant3, null rf_seq_obsant1, null rf_seq_obsant2, null rf_seq_obsant3  "
            gSqlSelRes = gSqlSelRes & ", 0 flg_assinado, null dt_assinado,null hr_assinado, null user_assinado, r.transmit_psm , r.n_envio, 0 flg_anexo, 0 flg_obs_ana, r.seq_req_tubo, 0 flg_grafico, "
            gSqlSelRes = gSqlSelRes & " null flg_ve_micro, null flg_ve, null flg_ve_frase , null cod_carac_micro, null user_apar, null dt_apar "
            gSqlSelRes = gSqlSelRes & " , 0 flg_gr_antib_facturado "
            gSqlSelRes = gSqlSelRes & " FROM sl_marcacoes r {TUBO_FROM2},  sl_requis req, slv_analises ana  WHERE "
            gSqlSelRes = gSqlSelRes & " r.cod_agrup = ana.cod_ana AND "
            gSqlSelRes = gSqlSelRes & " r.n_req = req.n_req AND req.n_req = {N_REQ}"
            gSqlSelRes = gSqlSelRes & ConstroiCriterio & "{TUBO_WHERE2}"
        End If
    End If
    If gSGBD = gOracle Then
        gSqlSelRes = gSqlSelRes & " ORDER BY n_req, ord_ana, ORD_ANA_PERF, ord_ana_compl, n_res"
    ElseIf gSGBD = gSqlServer Then
        gSqlSelRes = gSqlSelRes & " ORDER BY req.n_req,ord_ana, ord_ana_compl, ord_ana_perf, n_res"
    End If
    FormResultadosNovo.Show
    FormResultadosNovo.OptAgrupamento(4).value = True
    FormResultadosNovo.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "BtResutadosNovo_Click", False
    Exit Sub
    Resume Next
End Sub
Private Function RetornaComTec(n_req As String) As String
    Dim i As Integer
    For i = 1 To TotalDadosRequis
        If EstrutDadosRequis(i).num_requis = n_req Then
            RetornaComTec = EstrutDadosRequis(i).com_tec
            Exit Function
        End If
    Next
End Function

Private Sub FGReqPendRes_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim linhaActual As Integer
    
    linhaActual = FGReqPendRes_DevolveLinhaActual(Y)
    
    If linhaActual > 0 Then
        If FGReqPendRes.TextMatrix(linhaActual, lColNumReq) <> "" Then
            FGReqPendRes.ToolTipText = RetornaComTec(FGReqPendRes.TextMatrix(linhaActual, lColNumReq))
        End If
    End If
End Sub


' ----------------------------------------------------------------------

' RETORNA A LINHA ACTUAL

' ----------------------------------------------------------------------
Private Function FGReqPendRes_DevolveLinhaActual(Y As Single) As Integer
    Dim linha As Integer
        For linha = 0 To FGReqPendRes.rows - 1
            If FGReqPendRes.RowPos(linha) <= Y And FGReqPendRes.RowPos(linha) + FGReqPendRes.RowHeight(linha) >= Y Then
                FGReqPendRes_DevolveLinhaActual = linha
                Exit Function
            End If
        Next
        FGReqPendRes_DevolveLinhaActual = -1
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "FGReqPendRes_DevolveLinhaActual", False
    Exit Function
    Resume Next
End Function


Private Function ControiCriterioPreencheAna(n_req As String, tipo_urgencia As String) As String
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    If CkApenasResultados.value <> vbChecked Then
        sSql = " SELECT distinct sl_requis.n_req, abr_ana descr,descr_ana, '-1' flg_estado, slv_analises.cod_tubo, slv_analises.cod_ana cod_ana, ord_marca"
        sSql = sSql & " FROM sl_requis,sl_marcacoes, SLV_ANALISES_APENAS slv_analises "
        
        'SE FILTROU POR GRUPO TRABALHO (FROM)
        If EcListaGrTrab.ListCount > 0 Then
            sSql = sSql & ", sl_ana_trab, sl_gr_trab "
        End If
    
        'SE FILTROU POR GRUPO DE ANALISES (FROM)
        If EcListaGrAna.ListCount > 0 Then
            sSql = sSql & ", sl_gr_ana "
        End If
        
        'SE FILTROU POR PRODUTO (FROM)
        If EcListaProd.ListCount > 0 Then
            sSql = sSql & ", sl_produto "
        End If
        
        ' SE PRETENDE PESQUISAR POR DATAS DO TUBO
        If OptData(1).value = True Or CkRequisComTubo.value = vbChecked Then
            sSql = sSql & ", sl_req_tubo "
        End If
        
        sSql = sSql & " WHERE sl_requis.n_req = sl_marcacoes.n_req AND " & _
               " sl_requis.n_req = " & BL_TrataStringParaBD(n_req) & _
               " AND sl_marcacoes.Cod_agrup = slv_analises.cod_ana "
        If gLAB = "HPOVOA" Then
            sSql = sSql & " AND sl_marcacoes.cod_ana_s <> 'S99999'"
        End If
        
        ' SE PRETENDE PESQUISAR POR DATAS DO TUBO
        If OptData(1).value = True Then
            sSql = sSql & " AND sl_requis.n_req = sl_req_tubo.n_req AND slv_analises.cod_tubo = sl_req_tubo.cod_tubo "
            sSql = sSql & " AND (sl_req_tubo.dt_chega BETWEEN " & BL_TrataDataParaBD(lDataInicial)
            sSql = sSql & " AND " & BL_TrataDataParaBD(lDataFinal) & " )"
        End If
        
        'SE FILTROU POR APENAS ANALISES EM QUE TUBO CHEGOU
        If CkRequisComTubo.value = vbChecked Then
            sSql = sSql & " AND sl_req_tubo.n_req = " & n_req & " And sl_req_tubo.cod_tubo = slv_analises.cod_tubo "
            sSql = sSql & " AND sl_req_tubo.dt_chega is not null "
        End If
        
        'SE FILTROU POR GRUPO ANALISES
        If EcListaGrAna.ListCount > 0 Then
            sSql = sSql & " AND sl_gr_ana.cod_gr_ana = slv_analises.cod_gr_ana AND sl_gr_ana.seq_gr_ana IN ( "
            For i = 0 To EcListaGrAna.ListCount - 1
                sSql = sSql & EcListaGrAna.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
         
        
        'SE FILTROU POR GRUPO TRABALHO (WHERE)
        If EcListaGrTrab.ListCount > 0 Then
            sSql = sSql & " AND sl_ana_trab.cod_gr_trab = sl_gr_trab.cod_gr_trab "
            sSql = sSql & " AND sl_gr_trab.seq_gr_trab IN ("
            For i = 0 To EcListaGrTrab.ListCount - 1
                sSql = sSql & EcListaGrTrab.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
            sSql = sSql & " AND sl_ana_trab.cod_analise = sl_marcacoes.cod_agrup"
        End If
        
        'SE FILTROU POR PRODUTO (WHERE)
        If EcListaProd.ListCount > 0 Then
            sSql = sSql & " AND sl_produto.cod_produto = slv_analises.cod_produto AND sl_produto.seq_produto IN ( "
            For i = 0 To EcListaProd.ListCount - 1
                sSql = sSql & EcListaProd.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
        
        'LOCAL
        If EcListaLocais.ListCount > 0 Then
            ' pferreira 2010.04.22
            If gMultiLocalAmbito = mediSim Then
                sSql = sSql & " AND slv_analises.cod_ana IN (SELECT sl_ana_acrescentadas.cod_Agrup FROM sl_ana_acrescentadas WHERE sl_Ana_acrescentadas.n_req = " & n_req
                sSql = sSql & " AND (flg_eliminada = 0 OR flg_eliminada IS NULL) AND (cod_local IS NULL OR cod_local in( "
            Else
            
                sSql = sSql & " AND slv_analises.cod_ana IN (SELECT cod_Ana FROM sl_ana_locais_exec WHERE (cod_local in( "
            End If
            For i = 0 To EcListaLocais.ListCount - 1
                sSql = sSql & EcListaLocais.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & "))) "
        End If
        
        'SE FILTROU POR APARELHO (WHERE)
        If EcListaApar.ListCount > 0 Then
            If gSGBD = gOracle Then
                sSql = sSql & " AND sl_marcacoes.cod_agrup IN (SELECT cod_agrup FROM gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.NAO_entra_pendentes = 0 or gc_ana_apar.NAO_entra_pendentes IS NULL) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
            ElseIf gSGBD = gSqlServer Then
                sSql = sSql & " AND sl_marcacoes.cod_agrup IN (SELECT cod_agrup FROM gescom.dbo.gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.nao_entra_pendentes = 0 or gc_ana_apar.nao_entra_pendentes is null) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
            End If
            
            For i = 0 To EcListaApar.ListCount - 1
                sSql = sSql & EcListaApar.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
        End If
        
        If EcListaAnalises.ListCount > 0 Then                  'SE FOI INDICADO ALGUMA ANALISE
            If Opt1(0).value = True Then
                sSql = sSql & " AND slv_analises.seq_ana in ( "
            ElseIf Opt1(1).value = True Then
                sSql = sSql & " AND slv_analises.seq_ana NOT in ( "
            End If
            For i = 0 To EcListaAnalises.ListCount - 1
                sSql = sSql & EcListaAnalises.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
    End If

'------------------------ Union com tabela sl_realiza --------------------------------------


    If CkApenasMarcacoes.value <> 1 Then
    
        If CkApenasResultados.value <> vbChecked Then
            sSql = sSql & " UNION ALL"
        End If
        
        sSql = sSql & "  SELECT distinct sl_requis.N_req, abr_ana descr,descr_ana, sl_realiza.flg_estado, slv_analises.cod_tubo, slv_analises.cod_ana cod_ana, ord_marca " & _
               " FROM sl_requis,sl_realiza, SLV_ANALISES_APENAS slv_analises "
        
        'SE FILTROU POR GRUPO DE TRABALHO (FROM)
        If EcListaGrTrab.ListCount > 0 Then
            sSql = sSql & ", sl_ana_trab, sl_gr_trab "
        End If
    
        'SE FILTROU POR GRUPO DE ANALISES (FROM)
        If EcListaGrAna.ListCount > 0 Then
            sSql = sSql & ", sl_gr_ana "
        End If
        
        'SE FILTROU POR PRODUTO (FROM)
        If EcListaProd.ListCount > 0 Then
            sSql = sSql & ", sl_produto "
        End If
        ' SE PRETENDE PESQUISAR POR DATAS DO TUBO
        If OptData(1).value = True Then
            sSql = sSql & ", sl_req_tubo "
        End If
        
        sSql = sSql & " WHERE  sl_requis.n_req = sl_realiza.n_req AND " & _
           " sl_requis.n_req = " & BL_TrataStringParaBD(n_req) & _
           " AND sl_realiza.cod_agrup = slv_analises.cod_ana "
        
        If gLAB = "HPOVOA" Then
            sSql = sSql & " AND sl_realiza.cod_ana_s <> 'S99999'"
        End If
        
        ' SE PRETENDE PESQUISAR POR DATAS DO TUBO
        If OptData(1).value = True Then
            sSql = sSql & " AND sl_requis.n_req = sl_req_tubo.n_req AND slv_analises.cod_tubo = sl_req_tubo.cod_tubo "
            sSql = sSql & " AND (sl_req_tubo.dt_chega BETWEEN " & BL_TrataDataParaBD(lDataInicial)
            sSql = sSql & " AND " & BL_TrataDataParaBD(lDataFinal) & " )"
        End If
            
        'SE FILTROU POR GRUPO ANALISES
        If EcListaGrAna.ListCount > 0 Then
            sSql = sSql & " AND sl_gr_ana.cod_gr_ana = slv_analises.cod_gr_ana AND sl_gr_ana.seq_gr_ana IN ( "
            For i = 0 To EcListaGrAna.ListCount - 1
                sSql = sSql & EcListaGrAna.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
        
        'SE FILTROU POR GRUPO TRABALHO (WHERE)
        If EcListaGrTrab.ListCount > 0 Then
            sSql = sSql & " AND sl_ana_trab.cod_gr_trab = sl_gr_trab.cod_gr_trab "
            sSql = sSql & " AND sl_gr_trab.seq_gr_trab IN ("
            For i = 0 To EcListaGrTrab.ListCount - 1
                sSql = sSql & EcListaGrTrab.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
            sSql = sSql & " AND sl_ana_trab.cod_analise = sl_realiza.cod_agrup"
        End If
     
        'SE FILTROU POR PRODUTO (WHERE)
        If EcListaProd.ListCount > 0 Then
            sSql = sSql & " AND sl_produto.cod_produto = slv_analises.cod_produto AND sl_produto.seq_produto IN ( "
            For i = 0 To EcListaProd.ListCount - 1
                sSql = sSql & EcListaProd.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
        
        'LOCAL
        If EcListaLocais.ListCount > 0 Then
            ' pferreira 2010.04.22
            If gMultiLocalAmbito = mediSim Then
                sSql = sSql & " AND slv_analises.cod_ana IN (SELECT sl_ana_acrescentadas.cod_Agrup FROM sl_ana_acrescentadas WHERE sl_Ana_acrescentadas.n_req = " & n_req
                sSql = sSql & " AND (flg_eliminada = 0 OR flg_eliminada IS NULL) AND (cod_local IS NULL OR cod_local in( "
            Else
            
                sSql = sSql & " AND slv_analises.cod_ana IN (SELECT cod_Ana FROM sl_ana_locais_exec WHERE (cod_local in( "
            End If
            For i = 0 To EcListaLocais.ListCount - 1
                sSql = sSql & EcListaLocais.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & "))) "
        End If
        
        'SE FILTROU POR APARELHO (WHERE)
        If EcListaApar.ListCount > 0 Then
            If gSGBD = gOracle Then
                sSql = sSql & " AND sl_realiza.cod_Agrup IN (SELECT cod_agrup FROM gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.NAO_entra_pendentes = 0 or gc_ana_apar.NAO_entra_pendentes IS NULL)  AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
            ElseIf gSGBD = gSqlServer Then
                sSql = sSql & " AND sl_realiza.cod_ana_s IN (SELECT cod_agrup FROM gescom.dbo.gc_ana_apar gc_ana_apar WHERE (gc_ana_apar.nao_entra_pendentes = 0 or gc_ana_apar.nao_entra_pendentes is null) AND gc_ana_apar.cod_agrup IS NOT NULL AND seq_apar IN ( "
            End If
            
            For i = 0 To EcListaApar.ListCount - 1
                sSql = sSql & EcListaApar.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
        End If
        
        If EcListaAnalises.ListCount > 0 Then                  'SE FOI INDICADO ALGUMA ANALISE SIMPLES
            If Opt1(0).value = True Then
                sSql = sSql & " AND slv_analises.seq_ana in ( "
            ElseIf Opt1(1).value = True Then
                sSql = sSql & " AND slv_analises.seq_ana NOT in ( "
            End If
            For i = 0 To EcListaAnalises.ListCount - 1
                sSql = sSql & EcListaAnalises.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
    End If
    
    '-----------------------------------------------------------------------------------------
    sSql = sSql & " ORDER BY ord_marca ASC "
    ControiCriterioPreencheAna = sSql
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "PreencheFgReqPendRes", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' CARREGA ESTRUTURA DE CORES

' ----------------------------------------------------------------------
Private Sub PreencheEstrutLimiteCor()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsCor As New ADODB.recordset
    totalLimiteCor = 0
    ReDim EstrutLimiteCor(0)
    sSql = "SELECT * FROM sl_tbf_limite_cor ORDER by val_min ASC"
    RsCor.CursorLocation = adUseServer
    RsCor.CursorType = adOpenStatic
    RsCor.Open sSql, gConexao
    If RsCor.RecordCount > 0 Then
        While Not RsCor.EOF
            totalLimiteCor = totalLimiteCor + 1
            ReDim Preserve EstrutLimiteCor(totalLimiteCor)
            
            EstrutLimiteCor(totalLimiteCor).cod_limite_cor = BL_HandleNull(RsCor!cod_limite_cor, mediComboValorNull)
            EstrutLimiteCor(totalLimiteCor).cor = BL_HandleNull(RsCor!cor, vbWhite)
            EstrutLimiteCor(totalLimiteCor).descr_limite_cor = BL_HandleNull(RsCor!descr_limite_cor, "")
            EstrutLimiteCor(totalLimiteCor).val_maximo = BL_HandleNull(RsCor!val_max, mediComboValorNull)
            EstrutLimiteCor(totalLimiteCor).val_minimo = BL_HandleNull(RsCor!val_min, mediComboValorNull)
            RsCor.MoveNext
        Wend
    End If
    RsCor.Close
    Set RsCor = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "PreencheEstrutLimiteCor", False
    Exit Sub
    Resume Next
End Sub

Private Function DevolveCorLimite(percentagem As Double) As Long
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To totalLimiteCor
        If EstrutLimiteCor(i).val_minimo <= percentagem And EstrutLimiteCor(i).val_maximo > percentagem Then
            DevolveCorLimite = EstrutLimiteCor(i).cor
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolveCorLimite = vbWhite
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "DevolveCorLimite", False
    Exit Function
    Resume Next
End Function

Public Sub OrdenaEstruturaRequisicoes(ByVal coluna As String, ByVal ordem As String)
    'R.G.: 26-03-2012
    
    On Error GoTo TrataErro
    ordenacao = coluna
'    'Ex:
'
'        Case 0
'            Call OrdenaEstruturaRequisicoes("dt_chega", cSortOrderAsc)
'        Case 1
'            Call OrdenaEstruturaRequisicoes("tipo_urgencia", cSortOrderDesc)
'        Case 2
'            Call OrdenaEstruturaRequisicoes("servico", cSortOrderAsc)
'        Case 3
'            Call OrdenaEstruturaRequisicoes("num_requis", cSortOrderAsc)
'        Case 4
'            Call OrdenaEstruturaRequisicoes("nome", cSortOrderAsc)
'
    
    Dim rv As Integer
    Dim l As Long
    Dim sortType As String
    Dim sortOrder As String
    Dim dataArray() As String
    Dim indexArray() As String
    Dim EstrutDadosRequisAux() As requisicoes
    
    If TotalDadosRequisOriginal <= 0 Then
        'MsgBox "N�o foi poss�vel efectuar a ordena��o."
        Exit Sub
    End If
    
    ReDim dataArray(UBound(EstrutDadosRequisOriginal) - 1) As String
    ReDim indexArray(UBound(EstrutDadosRequisOriginal) - 1) As String
    ReDim EstrutDadosRequisAux(UBound(EstrutDadosRequisOriginal) - 1) As requisicoes
    
    If ordem <> cSortOrderAsc And ordem <> cSortOrderDesc Then
        'MsgBox "N�o foi poss�vel efectuar a ordena��o."
        Exit Sub
    End If
    
    Select Case UCase(coluna)
    
        Case UCase(ordenaDtChega)
            For l = LBound(EstrutDadosRequisOriginal) + 1 To UBound(EstrutDadosRequisOriginal)
                dataArray(l - 1) = EstrutDadosRequisOriginal(l).dt_chega & " " & EstrutDadosRequisOriginal(l).hr_chega
            Next l
            sortType = cSortTypeDate
            sortOrder = ordem
        Case UCase(ordenaTUrg)
            For l = LBound(EstrutDadosRequisOriginal) + 1 To UBound(EstrutDadosRequisOriginal)
                dataArray(l - 1) = EstrutDadosRequisOriginal(l).urgente
            Next l
            sortType = cSortTypeLong
            sortOrder = ordem
        Case UCase(ordenaTriagem)
            For l = LBound(EstrutDadosRequisOriginal) + 1 To UBound(EstrutDadosRequisOriginal)
                dataArray(l - 1) = EstrutDadosRequisOriginal(l).tipo_urgencia
            Next l
            sortType = cSortTypeLong
            sortOrder = ordem
        Case UCase(ordenaServico)
            For l = LBound(EstrutDadosRequisOriginal) + 1 To UBound(EstrutDadosRequisOriginal)
                dataArray(l - 1) = EstrutDadosRequisOriginal(l).servico
            Next l
            sortType = cSortTypeString
            sortOrder = ordem
        Case UCase(ordenaNumRequis)
            For l = LBound(EstrutDadosRequisOriginal) + 1 To UBound(EstrutDadosRequisOriginal)
                dataArray(l - 1) = EstrutDadosRequisOriginal(l).num_requis
            Next l
            sortType = cSortTypeLong
            sortOrder = ordem
        Case UCase(ordenaNome)
            For l = LBound(EstrutDadosRequisOriginal) + 1 To UBound(EstrutDadosRequisOriginal)
                dataArray(l - 1) = EstrutDadosRequisOriginal(l).nome
            Next l
            sortType = cSortTypeString
            sortOrder = ordem
            
        Case Else
            Exit Sub
    End Select
    
    'carrega o array de �ndices
    For l = LBound(indexArray) To UBound(indexArray)
        indexArray(l) = l
    Next l
    
'    'inverte se for necess�rio
'    If chkInv.Value = vbChecked Then
'        If sortOrder = cSortOrderAsc Then
'            sortOrder = cSortOrderDesc
'        Else
'            sortOrder = cSortOrderAsc
'        End If
'    End If
    If SORT_Array(dataArray, indexArray, sortType, sortOrder) = 1 Then
        'ordena toda a estrutura baseado apenas no array de �ndices retornado
        For l = LBound(indexArray) To UBound(indexArray)
            EstrutDadosRequisAux(l) = EstrutDadosRequisOriginal(indexArray(l) + 1)
        Next l
        
        For l = LBound(EstrutDadosRequisAux) To UBound(EstrutDadosRequisAux)
            EstrutDadosRequis(l + 1) = EstrutDadosRequisAux(l)
        Next l
        'PreencheListBox1 EstrutDadosRequis
        'Preencher FlexGrid aqui
    Else
        'MsgBox "N�o foi poss�vel efectuar a ordena��o."
    End If
    Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub


Private Sub LimpaFGReq()
    FGReqPendRes.Clear
    FGReqPendRes.Cols = colunas
    FGReqPendRes.rows = 23
    FGReqPendRes.row = 0
    linhaActual = 0
End Sub

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormListagemCaixa 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormListagemCaixa"
   ClientHeight    =   7125
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6840
   Icon            =   "FormListagemCaixa.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   6840
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkExcluirBorlas 
      Caption         =   "Excluir Borlas"
      Height          =   255
      Left            =   120
      TabIndex        =   36
      Top             =   6360
      Width           =   2175
   End
   Begin VB.CheckBox CkNaoConvformidades 
      Caption         =   "Incluir N�o Confomidades"
      Height          =   255
      Left            =   4800
      TabIndex        =   35
      Top             =   6000
      Width           =   2175
   End
   Begin VB.CheckBox CkAgruparMes 
      Caption         =   "Agrupar por M�s"
      Height          =   255
      Left            =   2520
      TabIndex        =   34
      Top             =   6000
      Width           =   2175
   End
   Begin VB.Frame FrameDatas 
      Height          =   1095
      Left            =   2760
      TabIndex        =   29
      Top             =   7440
      Width           =   3375
      Begin VB.OptionButton OptData 
         Caption         =   "Data Chegada da Requisi��o"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   32
         Top             =   720
         Width           =   2895
      End
      Begin VB.OptionButton OptData 
         Caption         =   "Data da Opera��o"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   31
         Top             =   495
         Width           =   2415
      End
      Begin VB.OptionButton OptData 
         Caption         =   "Data Cria��o da requisi��o"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   30
         Top             =   240
         Width           =   3135
      End
   End
   Begin VB.CheckBox CkAgruparData 
      Caption         =   "Agrupar por Data"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   6000
      Width           =   2175
   End
   Begin VB.CheckBox CkSupDetalhes 
      Caption         =   "Suprimir Detalhes"
      Height          =   255
      Left            =   4800
      TabIndex        =   22
      Top             =   5640
      Width           =   1935
   End
   Begin VB.CheckBox CkOrderRecibo 
      Caption         =   "Ordenar por N� Fact. Rec."
      Height          =   255
      Left            =   2520
      TabIndex        =   21
      Top             =   5640
      Width           =   2295
   End
   Begin VB.CheckBox CkDescrAnalises 
      Caption         =   "Descriminar An�lises"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   5640
      Width           =   2175
   End
   Begin VB.CheckBox CkAgrupOperador 
      Caption         =   "Agrupar por Operador"
      Height          =   255
      Left            =   4800
      TabIndex        =   16
      Top             =   5280
      Width           =   1935
   End
   Begin VB.CheckBox CkAgrupPosto 
      Caption         =   "Agrupar por Posto"
      Height          =   255
      Left            =   2520
      TabIndex        =   15
      Top             =   5280
      Width           =   2295
   End
   Begin VB.CheckBox CkAgrupEntidade 
      Caption         =   "Agrupar por Entidade"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   5280
      Width           =   2175
   End
   Begin VB.Frame Frame2 
      Height          =   3855
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   6615
      Begin VB.CommandButton BtPesqLocal 
         Height          =   315
         Left            =   6120
         Picture         =   "FormListagemCaixa.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   40
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Locais"
         Top             =   1080
         Width           =   375
      End
      Begin VB.TextBox EcCodLocal 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1320
         TabIndex        =   39
         Top             =   1080
         Width           =   975
      End
      Begin VB.TextBox EcDescrLocal 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   1080
         Width           =   3855
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6120
         Picture         =   "FormListagemCaixa.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   3120
         Width           =   375
      End
      Begin VB.ListBox EcListaProven 
         Height          =   645
         Left            =   1320
         TabIndex        =   26
         Top             =   3120
         Width           =   4815
      End
      Begin VB.ComboBox CbRequis 
         Height          =   315
         Left            =   4680
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   360
         Width           =   1455
      End
      Begin VB.ListBox ListaPostos 
         Height          =   645
         Left            =   1320
         MultiSelect     =   2  'Extended
         TabIndex        =   12
         Top             =   2400
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaPostos 
         Height          =   315
         Left            =   6120
         Picture         =   "FormListagemCaixa.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   2400
         Width           =   375
      End
      Begin VB.ListBox ListaEntidades 
         Height          =   645
         Left            =   1320
         MultiSelect     =   2  'Extended
         TabIndex        =   9
         Top             =   1560
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaEntidades 
         Height          =   315
         Left            =   6120
         Picture         =   "FormListagemCaixa.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   1560
         Width           =   375
      End
      Begin VB.ComboBox CbOperador 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   360
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Local"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncias"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   28
         Top             =   3120
         Width           =   1095
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Requisi��es"
         Height          =   195
         Left            =   3720
         TabIndex        =   25
         Top             =   360
         Width           =   870
      End
      Begin VB.Label Label1 
         Caption         =   "Postos"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   13
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Entidades"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   10
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Operador"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6615
      Begin VB.CommandButton BtDatas 
         Height          =   375
         Left            =   6000
         Picture         =   "FormListagemCaixa.frx":1634
         Style           =   1  'Graphical
         TabIndex        =   33
         ToolTipText     =   "Datas"
         Top             =   840
         Width           =   495
      End
      Begin VB.CommandButton BtReportLista 
         Height          =   480
         Left            =   6000
         Picture         =   "FormListagemCaixa.frx":19BE
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Gerar Listagem"
         Top             =   240
         Width           =   495
      End
      Begin VB.ComboBox CbListagem 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   240
         Width           =   3735
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   300
         Left            =   1200
         TabIndex        =   1
         Top             =   840
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   173146113
         CurrentDate     =   39300
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   300
         Left            =   3720
         TabIndex        =   3
         Top             =   840
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   173146113
         CurrentDate     =   39300
      End
      Begin VB.Label Label1 
         Caption         =   "Listagem"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Data Final"
         Height          =   255
         Index           =   1
         Left            =   2760
         TabIndex        =   4
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   840
         Width           =   975
      End
   End
   Begin MSComctlLib.ProgressBar ProgressBar 
      Height          =   375
      Left            =   120
      TabIndex        =   37
      Top             =   6720
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
   End
End
Attribute VB_Name = "FormListagemCaixa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim ListaOrigem As Control
Dim ListaDestino As Control
Public CamposBDparaListBoxF
Dim NumEspacos
Public RSListaDestino As ADODB.recordset
Public RSListaOrigem As ADODB.recordset

Const lListagemCaixa = 1
Const lAnulados = 2
Const lBorlas = 3
Const lCobrancas = 4
Const lDividas = 5
Const lMargensPostos = 6
Const lFactRecibos = 7
Const lDiario = 8

Private Type preco
    cod_rubr As String
    cod_rubr_efr As String
    descr_rubr As String
    val_Taxa As String
    val_pag_ent As String
    val_pag_doe As String
    perc_pag_ent As String
End Type
Private Type Portaria
    cod_portaria As String
    dt_ini As String
    dt_fim As String
    precos() As preco
    totalPrecos As Integer
End Type
Private Type Entidade
    cod_efr As String
    descr_efr As String
    tab_utilizada As String
    flg_ars As Integer
    flg_usa_preco_ent As Integer
    portarias() As Portaria
    totalPortarias  As Integer
End Type
Dim entidades() As Entidade
Dim totalEntidades As Integer

Sub Preenche_Estatistica()
    Dim sql As String
    Dim continua As Boolean
    Dim i As Integer
    Dim TotalRegistos As Long
    Dim nomeReport As String
    
    sql = "DELETE FROM sl_cr_listagem_caixa WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sql = sql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    If CbListagem.ListIndex = mediComboValorNull Then
        Call BG_Mensagem(mediMsgBox, "Indique a Estatistica Pretendida", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    End If
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Listagem de Caixa") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    If CkAgruparMes.value = vbChecked Then
        nomeReport = BL_HandleNull(BL_SelCodigo("sl_t_list_caixa", "order_mes", "cod_list_caixa", BG_DaComboSel(CbListagem)), BL_SelCodigo("sl_t_list_caixa", "nome_report", "cod_list_caixa", BG_DaComboSel(CbListagem)))
    ElseIf CkAgruparData.value = vbChecked Then
        nomeReport = BL_HandleNull(BL_SelCodigo("sl_t_list_caixa", "order_data", "cod_list_caixa", BG_DaComboSel(CbListagem)), BL_SelCodigo("sl_t_list_caixa", "nome_report", "cod_list_caixa", BG_DaComboSel(CbListagem)))
    ElseIf CkAgrupPosto.value = vbChecked Then
        nomeReport = BL_HandleNull(BL_SelCodigo("sl_t_list_caixa", "order_cod_posto", "cod_list_caixa", BG_DaComboSel(CbListagem)), BL_SelCodigo("sl_t_list_caixa", "nome_report", "cod_list_caixa", BG_DaComboSel(CbListagem)))
    ElseIf CkAgrupEntidade.value = vbChecked Then
        nomeReport = BL_HandleNull(BL_SelCodigo("sl_t_list_caixa", "order_cod_efr", "cod_list_caixa", BG_DaComboSel(CbListagem)), BL_SelCodigo("sl_t_list_caixa", "nome_report", "cod_list_caixa", BG_DaComboSel(CbListagem)))
    ElseIf CkAgrupOperador.value = vbChecked Then
        nomeReport = BL_HandleNull(BL_SelCodigo("sl_t_list_caixa", "order_cod_operador", "cod_list_caixa", BG_DaComboSel(CbListagem)), BL_SelCodigo("sl_t_list_caixa", "nome_report", "cod_list_caixa", BG_DaComboSel(CbListagem)))
    Else
        nomeReport = BL_SelCodigo("sl_t_list_caixa", "nome_report", "cod_list_caixa", BG_DaComboSel(CbListagem))
    End If
    
    If nomeReport = "" Then
        Call BG_Mensagem(mediMsgBox, "Listagem pretendida n�o tem nenhum report associado(SL_T_LIST_CAIXA). ", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    End If
    
    'Printer Common Dialog
    'gImprimirDestino = 0
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Listagem de Caixa", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Listagem de Caixa", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    
    If BG_DaComboSel(CbListagem) = lListagemCaixa Then
        PreencheTabelaTemporaria_ListagemCaixa
    ElseIf BG_DaComboSel(CbListagem) = lAnulados Then
        PreencheTabelaTemporaria_ANULADOS
    ElseIf BG_DaComboSel(CbListagem) = lBorlas Then
        PreencheTabelaTemporaria_BORLAS
    ElseIf BG_DaComboSel(CbListagem) = lCobrancas Then
        PreencheTabelaTemporaria_COBRANCAS
    ElseIf BG_DaComboSel(CbListagem) = lDividas Then
        PreencheTabelaTemporaria_dividas
    ElseIf BG_DaComboSel(CbListagem) = lMargensPostos Then
        PreencheTabelaTemporaria_MARGENS
    ElseIf BG_DaComboSel(CbListagem) = lFactRecibos Then
        PreencheTabelaTemporaria_FacturaRecibos
    ElseIf BG_DaComboSel(CbListagem) = lDiario Then
        PreencheTabelaTemporaria_Diario
    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.WindowState = crptMaximized
    
    If BG_DaComboSel(CbListagem) = lListagemCaixa Then
        Report.SQLQuery = " SELECT N_REQ, N_REC, COD_ENTIDADE, DESCR_ENTIDADE, COD_USER_EMI, DESCR_USER_EMI, COD_SALA, DESCR_SALA, VALOR, NOME, DESCR_ANALISE, DOCUMENTO, DATA, MES"
        Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_LISTAGEM_CAIXA "
        Report.SQLQuery = Report.SQLQuery & " WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    ElseIf BG_DaComboSel(CbListagem) = lAnulados Then
        Report.SQLQuery = "SELECT SL_CR_LISTAGEM_CAIXA.N_REQ, SL_CR_LISTAGEM_CAIXA.N_REC, SL_CR_LISTAGEM_CAIXA.NUM_ANALISES, "
        Report.SQLQuery = Report.SQLQuery & " SL_CR_LISTAGEM_CAIXA.VALOR, SL_CR_LISTAGEM_CAIXA.NOME"
        Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_LISTAGEM_CAIXA  "
        Report.SQLQuery = Report.SQLQuery & " WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    ElseIf BG_DaComboSel(CbListagem) = lBorlas Then
        Report.SQLQuery = "SELECT SL_CR_LISTAGEM_CAIXA.N_REQ, SL_CR_LISTAGEM_CAIXA.N_REC, SL_CR_LISTAGEM_CAIXA.NUM_ANALISES, "
        Report.SQLQuery = Report.SQLQuery & " SL_CR_LISTAGEM_CAIXA.VALOR, SL_CR_LISTAGEM_CAIXA.NOME"
        Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_LISTAGEM_CAIXA  "
        Report.SQLQuery = Report.SQLQuery & " WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    ElseIf BG_DaComboSel(CbListagem) = lCobrancas Then
        Report.SQLQuery = "SELECT SL_CR_LISTAGEM_CAIXA.N_REQ, SL_CR_LISTAGEM_CAIXA.N_REC, SL_CR_LISTAGEM_CAIXA.NUM_ANALISES, "
        Report.SQLQuery = Report.SQLQuery & " SL_CR_LISTAGEM_CAIXA.VALOR, SL_CR_LISTAGEM_CAIXA.NOME"
        Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_LISTAGEM_CAIXA  "
        Report.SQLQuery = Report.SQLQuery & " WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    ElseIf BG_DaComboSel(CbListagem) = lDividas Then
        Report.SQLQuery = "SELECT SL_CR_LISTAGEM_CAIXA.N_REQ, SL_CR_LISTAGEM_CAIXA.N_REC, SL_CR_LISTAGEM_CAIXA.NUM_ANALISES, "
        Report.SQLQuery = Report.SQLQuery & " SL_CR_LISTAGEM_CAIXA.VALOR, SL_CR_LISTAGEM_CAIXA.NOME"
        Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_LISTAGEM_CAIXA  "
        Report.SQLQuery = Report.SQLQuery & " WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    ElseIf BG_DaComboSel(CbListagem) = lMargensPostos Then
        Report.SQLQuery = "SELECT n_req , COD_ENTIDADE, DESCR_ENTIDADE, COD_USER_EMI, DESCR_USER_EMI, cod_sala, DESCR_SALA, nome, TOTAL_UTENTE, JA_RECEBIDO, POR_RECEBER, descr_analise, total_facturar, total_Percentagem, data "
        Report.SQLQuery = Report.SQLQuery & " From SL_CR_LISTAGEM_CAIXA SL_CR_LISTAGEM_CAIXA  WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    Else
        Report.SQLQuery = "SELECT * "
        Report.SQLQuery = Report.SQLQuery & " From SL_CR_LISTAGEM_CAIXA SL_CR_LISTAGEM_CAIXA  WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    End If
    
    Report.SQLQuery = Report.SQLQuery & "  ORDER BY "
    
    'AGRUPADO POR MES
    If CkAgruparMes.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " mes ASC, "
    End If
    
    'AGRUPADO POR DATA
    If CkAgruparData.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " data ASC, "
    End If
    
    'AGRUPADO POR POSTO
    If CkAgrupPosto.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " cod_sala ASC,"
    End If
    
    ' AGRUPADO POR ENTIDADE
    If CkAgrupEntidade.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " cod_entidade ASC, "
    End If
    
    'AGRUPADO POR OPERADOR
    If CkAgrupOperador.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " cod_user_emi ASC,"
    End If
    ' SE INDICAR ORDENADO POR RECIBO
    If CkOrderRecibo.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " n_rec ASC,"
    End If
    'edgar.Parada LJMANSO-288 09.05.2019
    If CkAgruparData.value = vbChecked And gPassaRecFactus <> mediSim Then
        Report.SQLQuery = Report.SQLQuery & " n_req ASC, documento DESC"
    Else
        ' Edgar.Parada LJMANSO-389 18.04.2019
        If gPassaRecFactus <> mediSim Then
            If CkOrderRecibo.value <> vbChecked Then
               Report.SQLQuery = Report.SQLQuery & " data ASC, n_req ASC,n_rec asc, documento DESC"
            Else
               Report.SQLQuery = Report.SQLQuery & " data ASC, n_req asc, documento DESC"
            End If
        End If
        '
    End If
    ' Edgar.Parada LJMANSO-288 18.04.2019
    If gPassaRecFactus <> mediSim Then
       Report.SQLQuery = Report.SQLQuery & ", flg_facturado DESC"
    Else
       Report.SQLQuery = Report.SQLQuery & " N_REQ ASC"
    End If
    
    If Right(Trim(Report.SQLQuery), 1) = "," Then
      Report.SQLQuery = Mid(Report.SQLQuery, "1", Len(Trim(Report.SQLQuery)) - 1)
    End If
    '
    'F�rmulas do Report
    'Report.SelectionFormula = "{sl_CR_LISTAGEM_CAIXA.NOME_COMPUTADOR} = '" & gComputador & "'"
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.value)
    
    ' ---------------------------------------------------------------------------------
    ' ENTIDADES
    ' ---------------------------------------------------------------------------------
    Dim StrEntidades As String
    For i = 0 To ListaEntidades.ListCount - 1
        StrEntidades = StrEntidades & ListaEntidades.List(i) & "; "
    Next i
    If StrEntidades <> "" Then
        StrEntidades = Mid(StrEntidades, 1, Len(StrEntidades) - 2)
        If Len(StrEntidades) > 200 Then
            StrEntidades = left(StrEntidades, 200) & "..."
        End If
        Report.formulas(3) = "Entidades=" & BL_TrataStringParaBD("" & StrEntidades)
    Else
        Report.formulas(3) = "Entidades=" & BL_TrataStringParaBD("Todas")
    End If
    
    ' ---------------------------------------------------------------------------------
    ' POSTOS
    ' ---------------------------------------------------------------------------------
    Dim StrPostos As String
    For i = 0 To ListaPostos.ListCount - 1
        StrPostos = StrPostos & ListaPostos.List(i) & "; "
    Next i
    If StrPostos <> "" Then
        StrPostos = Mid(StrPostos, 1, Len(StrPostos) - 2)
        If Len(StrPostos) > 200 Then
            StrPostos = left(StrPostos, 200) & "..."
        End If
        Report.formulas(4) = "Postos=" & BL_TrataStringParaBD("" & StrPostos)
    Else
        Report.formulas(4) = "Postos=" & BL_TrataStringParaBD("Todos")
    End If
    
    ' ---------------------------------------------------------------------------------
    ' OPERADOR
    ' ---------------------------------------------------------------------------------
    If CbOperador.ListIndex = -1 Then
        Report.formulas(5) = "Operador=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(5) = "Operador=" & BL_TrataStringParaBD("" & CbOperador.text)
    End If
        
    If CkAgrupEntidade.value = 1 Then
        Report.formulas(6) = "AgruparEntidade=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(6) = "AgruparEntidade=" & BL_TrataStringParaBD("N")
    End If
    
    If CkAgrupPosto.value = 1 Then
        Report.formulas(7) = "AgruparPosto=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(7) = "AgruparPosto=" & BL_TrataStringParaBD("N")
    End If
    
    If CkAgrupOperador.value = 1 Then
        Report.formulas(8) = "AgruparOperador=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(8) = "AgruparOperador=" & BL_TrataStringParaBD("N")
    End If
    
    Report.formulas(9) = "Listagem=" & BL_TrataStringParaBD(CbListagem)
    
    If CkDescrAnalises.value = 1 Then
        Report.formulas(10) = "DescriminaAna=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(10) = "DescriminaAna=" & BL_TrataStringParaBD("N")
    End If
    
    If CkSupDetalhes.value = 1 Then
        Report.formulas(11) = "SuprimirDetalhes=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(11) = "SuprimirDetalhes=" & BL_TrataStringParaBD("N")
    End If
    
    If CkAgruparData.value = 1 Then
        Report.formulas(12) = "AgruparData=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(12) = "AgruparData=" & BL_TrataStringParaBD("N")
    End If
    
    If CkAgruparMes.value = 1 Then
        Report.formulas(13) = "AgruparMes=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(13) = "AgruparMes=" & BL_TrataStringParaBD("N")
    End If
    
    Call BL_ExecutaReport
 

End Sub

Private Sub BtDatas_Click()
    If FrameDatas.Visible Then
        FrameDatas.Visible = False
    Else
        FrameDatas.Visible = True
        FrameDatas.top = 1080
        FrameDatas.left = 2760
    End If
End Sub

Private Sub BtPesquisaentidades_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_efr "
    CWhere = ""
    CampoPesquisa = "descr_efr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_efr ", _
                                                                           "Entidades")
    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaEntidades.ListCount = 0 Then
                ListaEntidades.AddItem BL_SelCodigo("sl_efr", "descr_efr", "seq_efr", Resultados(i))
                ListaEntidades.ItemData(0) = Resultados(i)
            Else
                ListaEntidades.AddItem BL_SelCodigo("sl_efr", "descr_efr", "seq_efr", Resultados(i))
                ListaEntidades.ItemData(ListaEntidades.NewIndex) = Resultados(i)
            End If
        Next i
        

    End If

End Sub


Private Sub BtPesquisaPostos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CWhere = ""
    CampoPesquisa = "descr_sala"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_sala ", _
                                                                           " Postos Colheita")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPostos.ListCount = 0 Then
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", Resultados(i))
                ListaPostos.ItemData(0) = Resultados(i)
            Else
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", Resultados(i))
                ListaPostos.ItemData(ListaPostos.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub



Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")
    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                EcListaProven.ItemData(0) = Resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub CbListagem_Click()
    If CbListagem.ListIndex <> mediComboValorNull Then
        If gCodSalaAssocUser > 0 And gCodSalaAssocUser <> gCodSalaDefeito Then
            If BL_SelCodigo("SL_T_LIST_CAIXA", "FLG_INIBE_POSTOS", "COD_LIST_CAIXA", BG_DaComboSel(CbListagem), "V") = CStr(mediSim) Then
                BG_Mensagem mediMsgBox, "Utilizador n�o tem permiss�es para visualizar a listagem.", vbOKOnly + vbCritical, "Erro"
                CbListagem.ListIndex = mediComboValorNull
                Exit Sub
            End If
        End If
        If CbListagem.ItemData(CbListagem.ListIndex) = lDividas Then
            CkNaoConvformidades.Visible = True
        Else
            CkNaoConvformidades.Visible = False
            CkNaoConvformidades.value = vbUnchecked
        End If
        If CbListagem.ItemData(CbListagem.ListIndex) = lListagemCaixa Then
            CkExcluirBorlas.Visible = True
            CkExcluirBorlas.value = vbUnchecked
        Else
            CkExcluirBorlas.Visible = False
        End If
        
        If gPassaRecFactus = mediSim And BG_DaComboSel(CbListagem) = lMargensPostos Then
             CkExcluirBorlas.Enabled = False
             CkNaoConvformidades.Enabled = False
             CkAgrupOperador.Enabled = False
             CbOperador.Enabled = False
             CbRequis.Enabled = False
             Label3.Enabled = False
             Label6.Enabled = False
        Else
             CkExcluirBorlas.Enabled = True
             CkNaoConvformidades.Enabled = True
             CkAgrupOperador.Enabled = True
             CbOperador.Enabled = True
             CbRequis.Enabled = True
             Label3.Enabled = True
             Label6.Enabled = True
        End If
    Else
        CkNaoConvformidades.Visible = False
        CkNaoConvformidades.value = vbUnchecked
    End If
End Sub

Private Sub CbOperador_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbOperador_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbOperador.ListIndex = -1
    
End Sub


Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_UnLoad(cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Listagem de Caixa - Valores Pagos"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 6975
    Me.Height = 6930 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    If Not RSListaOrigem Is Nothing Then
        RSListaOrigem.Close
        Set RSListaOrigem = Nothing
    End If
    
    Call BL_FechaPreview("Listagem de Caixa - Valores Pagos")
    
    Set FormListagemCaixa = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    CbOperador.ListIndex = mediComboValorNull
    ListaEntidades.Clear
    ListaPostos.Clear
    CbListagem.ListIndex = mediComboValorNull
    
    CkAgrupEntidade.value = vbUnchecked
    CkAgrupOperador.value = vbUnchecked
    CkAgrupPosto.value = vbUnchecked
    CkAgruparData.value = vbUnchecked
    CkAgruparMes.value = vbUnchecked
    CkNaoConvformidades.value = vbUnchecked
    CkNaoConvformidades.Visible = False
End Sub

Sub DefTipoCampos()
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    FrameDatas.Visible = False
    CkNaoConvformidades.Visible = False
    CkExcluirBorlas.Visible = False
    totalEntidades = 0
    ReDim entidades(totalEntidades)
    'ProgressBar.ScaleWidth = 100
    ProgressBar.Min = 0
    ProgressBar.Max = 10
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    If gLAB = "LHL" Then
        OptData(2).value = True
    Else
        OptData(0).value = True
    End If
    BG_PreencheComboBD_ADO "sl_idutilizador", "cod_utilizador", "nome", CbOperador
    BG_PreencheComboBD_ADO "sl_t_list_caixa", "cod_list_caixa", "descr_list_caixa", CbListagem
    BG_PreencheComboBD_ADO "SL_TBF_T_REQ_FACT", "COD_T_REQ", "DESCR_T_REQ", CbRequis, mediAscComboDesignacao
    If BL_HandleNull(gCodSalaAssocUser, "-1") <> "-1" And gCodSalaAssocUser <> 0 Then
        ListaPostos.AddItem BL_SelCodigo("SL_COD_SALAS", "DESCR_SALA", "COD_SALA", gCodSalaAssocUser)
        ListaPostos.ItemData(ListaPostos.NewIndex) = BL_SelCodigo("SL_COD_SALAS", "SEQ_SALA", "COD_SALA", gCodSalaAssocUser)
        ListaPostos.Enabled = False
        BtPesquisaPostos.Enabled = False
    Else
        ListaPostos.Enabled = True
        BtPesquisaPostos.Enabled = True
    End If
    EcCodLocal.text = gCodLocal
    EcCodlocal_Validate False
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub

Sub PreencheTabelaTemporaria_ListagemCaixa()
    Dim sSqlRecibos As String
    Dim sSqlRecibos1 As String
    Dim sSqlRecibos2 As String
    Dim sSqlRecibos3 As String
    Dim sSqlRecibos4 As String
    Dim sSqlRecibos5 As String
    Dim ssql As String
    Dim sSqlC As String
    Dim rsRecibos As New ADODB.recordset
    Dim i As Integer
    Dim ult_n_rec As String
    Dim ult_estado_rec As String
    Dim ult_total_Pagar As String
    
    ApagaTabelaCrystal
    
    If gPassaRecFactus <> mediSim Then
        ' O QUE EST� PAGO
        sSqlRecibos1 = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos1 = sSqlRecibos1 & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
        sSqlRecibos1 = sSqlRecibos1 & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos1 = sSqlRecibos1 & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x1.total_pagar, x1.estado, "
        sSqlRecibos1 = sSqlRecibos1 & " x1.flg_doc_caixa, 'POS' tipo, x4.estado_req, x7.flg_facturado, x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos1 = sSqlRecibos1 & ", x1.nao_conformidade"
        sSqlRecibos1 = sSqlRecibos1 & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det x7 "
        sSqlRecibos1 = sSqlRecibos1 & ", sl_proven proven "
        sSqlRecibos1 = sSqlRecibos1 & " WHERE x1.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos1 = sSqlRecibos1 & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos1 = sSqlRecibos1 & " AND x1.estado in ('A','P') AND (x1.dt_pagamento BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos1 = sSqlRecibos1 & " AND x1.n_req = x7.n_req AND x1.serie_rec =x7.serie_rec AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr "
        sSqlRecibos1 = sSqlRecibos1 & " AND x4.cod_isencao <> " & BL_TrataStringParaBD(CStr(gTipoIsencaoBorla))
        sSqlRecibos1 = sSqlRecibos1 & "  AND x4.cod_proven= proven.cod_proven  (+) "
        If CkExcluirBorlas.value = vbChecked Then
            sSqlRecibos1 = sSqlRecibos1 & " AND (x1.flg_borla = 0 or x1.flg_borla IS NULL)"
        End If
    
        ' CONTA MESMO COM OS ANULADOS
        sSqlRecibos2 = " UNION SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos2 = sSqlRecibos2 & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
        sSqlRecibos2 = sSqlRecibos2 & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos2 = sSqlRecibos2 & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x1.total_pagar, 'P' estado, "
        sSqlRecibos2 = sSqlRecibos2 & " x1.flg_doc_caixa, 'POS' tipo, x4.estado_req, x7.flg_facturado, x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos2 = sSqlRecibos2 & ", x1.nao_conformidade"
        sSqlRecibos2 = sSqlRecibos2 & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det_canc x7 "
        sSqlRecibos2 = sSqlRecibos2 & ", sl_proven proven "
        sSqlRecibos2 = sSqlRecibos2 & " WHERE x1.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos2 = sSqlRecibos2 & " x4.seq_utente = x5.seq_utente   And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos2 = sSqlRecibos2 & " AND x1.estado = 'A' AND (x1.dt_pagamento BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos2 = sSqlRecibos2 & " AND x1.n_req = x7.n_req AND x1.serie_rec =x7.serie_rec AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr "
        sSqlRecibos2 = sSqlRecibos2 & " AND x4.cod_isencao <> " & BL_TrataStringParaBD(CStr(gTipoIsencaoBorla))
        sSqlRecibos2 = sSqlRecibos2 & "  AND x4.cod_proven= proven.cod_proven  (+) "
        sSqlRecibos2 = sSqlRecibos2 & " AND (x1.flg_borla IS NULL OR x1.flG_borla = 0) "
        If CkExcluirBorlas.value = vbChecked Then
            sSqlRecibos2 = sSqlRecibos2 & " AND (x1.flg_borla = 0 or x1.flg_borla IS NULL)"
        End If
        
        'MENOS O QUE FOI ANULADO
        sSqlRecibos3 = " UNION SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos3 = sSqlRecibos3 & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x8.user_canc user_emi, "
        sSqlRecibos3 = sSqlRecibos3 & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos3 = sSqlRecibos3 & " x5.nome_ute, x1.venda_dinheiro,x8.dt_canc dt_emi, x7.cod_facturavel, 0 -  x1.total_pagar total_pagar, x1.estado,"
        sSqlRecibos3 = sSqlRecibos3 & " x1.flg_doc_caixa, 'NEG' tipo, x4.estado_req, x7.flg_facturado, x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos3 = sSqlRecibos3 & ", x1.nao_conformidade"
        sSqlRecibos3 = sSqlRecibos3 & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det_canc x7,sl_recibos_canc x8 "
        sSqlRecibos3 = sSqlRecibos3 & ", sl_proven proven "
        sSqlRecibos3 = sSqlRecibos3 & " WHERE x1.cod_efr = x2.cod_efr and x8.user_canc = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos3 = sSqlRecibos3 & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos3 = sSqlRecibos3 & " AND x1.estado = 'A' AND (x8.dt_canc BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos3 = sSqlRecibos3 & " AND x1.n_req = x7.n_req AND x1.serie_rec =x7.serie_rec AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr "
        sSqlRecibos3 = sSqlRecibos3 & " AND x1.serie_rec =x8.serie_rec AND x1.n_rec = x8.n_rec "
        sSqlRecibos3 = sSqlRecibos3 & " AND x4.cod_isencao <> " & BL_TrataStringParaBD(CStr(gTipoIsencaoBorla))
        sSqlRecibos3 = sSqlRecibos3 & "  AND x4.cod_proven= proven.cod_proven  (+) "
        sSqlRecibos3 = sSqlRecibos3 & " AND (x1.flg_borla IS NULL OR x1.flG_borla = 0) "
        If CkExcluirBorlas.value = vbChecked Then
            sSqlRecibos3 = sSqlRecibos3 & " AND (x1.flg_borla = 0 or x1.flg_borla IS NULL)"
        End If
    
        'ADIANTAMENTOS
        sSqlRecibos4 = " UNION SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos4 = sSqlRecibos4 & " x1.n_adiantamento num_doc , x4.cod_efr, x2.descr_efr, x1.user_emi, "
        sSqlRecibos4 = sSqlRecibos4 & " x3.Nome , x4.cod_Sala, x6.descr_sala, 0 num_analises,"
        sSqlRecibos4 = sSqlRecibos4 & " x5.nome_ute, '0' venda_dinheiro , x1.dt_emi, null cod_facturavel, x1.valor total_pagar, x1.estado,"
        sSqlRecibos4 = sSqlRecibos4 & " 2 flg_doc_caixa, 'POS' tipo, x4.estado_req, 0 flg_facturado, x1.dt_emi dt_pagamento ,0 desconto, null cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos4 = sSqlRecibos4 & ", 0 nao_conformidade"
        sSqlRecibos4 = sSqlRecibos4 & " FROM sl_adiantamentos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6"
        sSqlRecibos4 = sSqlRecibos4 & ", sl_proven proven "
        sSqlRecibos4 = sSqlRecibos4 & " WHERE x4.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos4 = sSqlRecibos4 & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) And x4.cod_proven = proven.cod_proven(+)"
        sSqlRecibos4 = sSqlRecibos4 & " AND (x1.dt_emi BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos4 = sSqlRecibos4 & " AND x4.cod_isencao <> " & BL_TrataStringParaBD(CStr(gTipoIsencaoBorla))
        sSqlRecibos4 = sSqlRecibos4 & "  AND x4.cod_proven= proven.cod_proven  (+) "
    
        'ADIANTAMENTOS - Cancelamentos
        sSqlRecibos5 = " UNION SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos5 = sSqlRecibos5 & " x1.n_adiantamento num_doc , x4.cod_efr, x2.descr_efr, x1.user_anul user_emi, "
        sSqlRecibos5 = sSqlRecibos5 & " x3.Nome , x4.cod_Sala, x6.descr_sala, 0 num_analises,"
        sSqlRecibos5 = sSqlRecibos5 & " x5.nome_ute, '0' venda_dinheiro , x1.dt_anul dt_emi, null cod_facturavel,0 - x1.valor total_pagar, x1.estado,"
        sSqlRecibos5 = sSqlRecibos5 & " 3 flg_doc_caixa, 'NEG' tipo, x4.estado_req, 0 flg_facturado, x1.dt_anul dt_pagamento ,0 desconto, null cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos5 = sSqlRecibos5 & ", 0 nao_conformidade"
        sSqlRecibos5 = sSqlRecibos5 & " FROM sl_adiantamentos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6"
        sSqlRecibos5 = sSqlRecibos5 & ", sl_proven proven "
        sSqlRecibos5 = sSqlRecibos5 & " WHERE x4.cod_efr = x2.cod_efr and x1.user_anul = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos5 = sSqlRecibos5 & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) And x4.cod_proven = proven.cod_proven(+)"
        sSqlRecibos5 = sSqlRecibos5 & " AND x1.estado = 'A' AND (x1.dt_anul BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos5 = sSqlRecibos5 & " AND x4.cod_isencao <> " & BL_TrataStringParaBD(CStr(gTipoIsencaoBorla))
        sSqlRecibos5 = sSqlRecibos5 & "  AND x4.cod_proven= proven.cod_proven  (+) "
        sSqlC = sSqlC & RetornaClausulaWhere
        
        ssql = sSqlRecibos1 & sSqlC & sSqlRecibos2 & sSqlC & sSqlRecibos3 & sSqlC & sSqlRecibos4 & sSqlC & sSqlRecibos5 & sSqlC
    Else
        sSqlRecibos = " SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_fac num_doc , x1.cod_efr, x1.obs_efr descr_efr, x1.user_cri user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_fac dt_emi, cod_rubr cod_facturavel, x1.val_total total_pagar, x1.flg_estado estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_emiss_fac dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade, x4.estado_req, 'POS' tipo, x9.tipo tipo2 "
        sSqlRecibos = sSqlRecibos & " FROM fa_fact x1 join sl_efr x2 ON substr(x1.obs_efr,1,instr(x1.obs_efr,'-',1)-1) = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_idutilizador x3 on x1.user_cri = to_char(x3.cod_utilizador)"
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x1.episodio = x4.n_req"
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " JOIN slv_documentos_doente x9 on x1.serie_fac = x9.serie_doc and x1.n_fac = x9.n_doc "
        sSqlRecibos = sSqlRecibos & " WHERE  x9.dt_pagamento BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        sSqlRecibos = sSqlRecibos & " AND x9.dt_pagamento is not null "
        If CkExcluirBorlas.value = vbChecked Then
            sSqlRecibos = sSqlRecibos & " AND (x9.flg_borla = 'N' or x9.flg_borla IS NULL)"
        End If
        sSqlRecibos = sSqlRecibos & RetornaClausulaWhere
        
        sSqlRecibos = sSqlRecibos & " UNION SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_fac num_doc , x1.cod_efr, x1.obs_efr descr_efr, x1.user_cri user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_fac dt_emi, cod_rubr cod_facturavel, 0 - x1.val_total total_pagar, x1.flg_estado estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_emiss_fac dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade, x4.estado_req, 'NEG' tipo, x9.tipo tipo2 "
        sSqlRecibos = sSqlRecibos & " FROM fa_fact x1 join sl_efr x2 ON substr(x1.obs_efr,1,instr(x1.obs_efr,'-',1)-1) = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_idutilizador x3 on x1.user_cri = to_char(x3.cod_utilizador)"
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x1.episodio = x4.n_req"
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " JOIN slv_documentos_doente x9 on x1.serie_fac = x9.serie_doc and x1.n_fac = x9.n_doc "
        sSqlRecibos = sSqlRecibos & " WHERE  trunc(x1.dt_canc) BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        sSqlRecibos = sSqlRecibos & " AND x9.dt_pagamento is not null and x1.dt_canc is not null "
        If CkExcluirBorlas.value = vbChecked Then
            sSqlRecibos = sSqlRecibos & " AND (x9.flg_borla = 'N' or x9.flg_borla IS NULL)"
        End If
        sSqlRecibos = sSqlRecibos & RetornaClausulaWhere
        sSqlRecibos = sSqlRecibos & " UNION SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_nota_cred num_doc , x1.cod_efr, x10.obs_efr descr_efr, x1.user_cri user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_nota_cred dt_emi, cod_rubr cod_facturavel, 0 - x1.val_total total_pagar, 'NNC' estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_emiss_nota_cred dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade, x4.estado_req, 'NEG' tipo, x9.tipo tipo2 "
        sSqlRecibos = sSqlRecibos & " FROM fa_nota_cred x1 "
        sSqlRecibos = sSqlRecibos & " JOIN sl_idutilizador x3 on x1.user_cri = x3.cod_utilizador"
        sSqlRecibos = sSqlRecibos & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac "
        sSqlRecibos = sSqlRecibos & "  JOIN FA_FACT x10 on x1.serie_fac = x10.serie_fac and x1.n_fac = x10.n_fac "
        sSqlRecibos = sSqlRecibos & " Join sl_efr x2 ON substr(x10.obs_efr,1,instr(x10.obs_efr,'-',1)-1) = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x7.episodio = x4.n_req "
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente "
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " JOIN slv_documentos_doente x9 on x1.serie_nota_cred = x9.serie_doc and x1.n_nota_cred = x9.n_doc "
        sSqlRecibos = sSqlRecibos & " WHERE  x1.dt_emiss_nota_cred BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        If CkExcluirBorlas.value = vbChecked Then
            sSqlRecibos = sSqlRecibos & " AND (x9.flg_borla = 'N' or x9.flg_borla IS NULL)"
        End If
        sSqlRecibos = sSqlRecibos & RetornaClausulaWhere
        ssql = sSqlRecibos
    End If
        
    If gSGBD = gOracle Then
        ssql = ssql & " ORDER BY dt_emi ASC, n_req ASC, num_doc ASC, estado ASC,total_pagar ASC, tipo DESC"
    ElseIf gSGBD = gSqlServer Then
        ssql = ssql & " ORDER BY x1.dt_emi ASC, x1.n_req ASC, x1.num_doc ASC, x1.estado ASC,total_pagar asc, tipo DESC"
    End If
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    
    If rsRecibos.RecordCount > 0 Then
        ult_n_rec = ""
        ult_estado_rec = ""
        While Not rsRecibos.EOF
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR, NUM_SESSAO, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE "
            ssql = ssql & " ,COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES,VALOR ,NOME, VENDA_DINHEIRO,"
            ssql = ssql & " DATA, DOCUMENTO, DESCR_ANALISE,FLG_FACTURADO, DATA_REC, DESCONTO,COD_FORMA_PAG, MES, nao_conformidade)  VALUES("
                    
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!num_doc, -1)) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_emi, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            
            If BL_HandleNull(rsRecibos!flg_doc_caixa, -1) <= 1 Then
                If ult_n_rec <> BL_HandleNull(rsRecibos!num_doc, -1) Or ult_estado_rec <> rsRecibos!Estado Or ult_total_Pagar <> BL_HandleNull(rsRecibos!total_pagar, 0) Then
                    ssql = ssql & Replace(BL_HandleNull(rsRecibos!total_pagar, 0), ",", ".") & ", "
                Else
                    ssql = ssql & " 0, "
                End If
            Else
                ssql = ssql & Replace(BL_HandleNull(rsRecibos!total_pagar, 0), ",", ".") & ", "
            End If
            ult_n_rec = BL_HandleNull(rsRecibos!num_doc, -1)
            ult_estado_rec = BL_HandleNull(rsRecibos!Estado, gEstadoReciboPago)
            ult_total_Pagar = BL_HandleNull(rsRecibos!total_pagar, 0)
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
                        
            'TIPO DE DOCUMENTO
            If BL_HandleNull(rsRecibos!Estado, gEstadoReciboPago) = "NC" Then
                If gPassaRecFactus = mediSim Then
                    If BL_HandleNull(rsRecibos!Tipo2, "") = "C" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("CC(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("CC") & ", "
                        End If
                    ElseIf BL_HandleNull(rsRecibos!Tipo2, "") = "R" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("FAC REC(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("FAC REC") & ", "
                        End If
                    ElseIf BL_HandleNull(rsRecibos!Tipo2, "") = "NC" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("NOTA CRED(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("NOTA CRED") & ", "
                        End If
                    End If
                Else
                    If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                        ssql = ssql & BL_TrataStringParaBD("RECIBO(*)") & ", "
                    Else
                        ssql = ssql & BL_TrataStringParaBD("RECIBO") & ", "
                    End If
                End If
            ElseIf BL_HandleNull(rsRecibos!Estado, gEstadoReciboPago) = "NNC" Then
                If gPassaRecFactus = mediSim Then
                    If BL_HandleNull(rsRecibos!Tipo2, "") = "C" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("CC(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("CC") & ", "
                        End If
                    ElseIf BL_HandleNull(rsRecibos!Tipo2, "") = "R" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("FAC REC(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("FAC REC") & ", "
                        End If
                    ElseIf BL_HandleNull(rsRecibos!Tipo2, "") = "NC" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("NOTA CRED(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("NOTA CRED") & ", "
                        End If
                    End If
                Else
                    If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                        ssql = ssql & BL_TrataStringParaBD("NOTA CRED(*)") & ", "
                    Else
                        ssql = ssql & BL_TrataStringParaBD("NOTA CRED") & ", "
                    End If
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And BL_HandleNull(rsRecibos!Estado, gEstadoReciboPago) <> gEstadoReciboAnulado Then
                If gPassaRecFactus = mediSim Then
                    If BL_HandleNull(rsRecibos!Tipo2, "") = "C" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("CC(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("CC") & ", "
                        End If
                    ElseIf BL_HandleNull(rsRecibos!Tipo2, "") = "R" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("FAC REC(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("FAC REC") & ", "
                        End If
                    ElseIf BL_HandleNull(rsRecibos!Tipo2, "") = "NC" Then
                        If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                            ssql = ssql & BL_TrataStringParaBD("NOTA CRED(*)") & ", "
                        Else
                            ssql = ssql & BL_TrataStringParaBD("NOTA CRED") & ", "
                        End If
                    End If
                Else
                    If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                        ssql = ssql & BL_TrataStringParaBD("RECIBO (*)") & ", "
                    Else
                        ssql = ssql & BL_TrataStringParaBD("RECIBO") & ", "
                    End If
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And BL_HandleNull(rsRecibos!Estado, gEstadoReciboPago) <> gEstadoReciboAnulado Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("DOC. CAIXA (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("DOC. CAIXA") & ", "
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And BL_HandleNull(rsRecibos!Estado, gEstadoReciboPago) = gEstadoReciboAnulado Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("ANUL. RECIBO (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("ANUL. RECIBO") & ", "
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And BL_HandleNull(rsRecibos!Estado, gEstadoReciboPago) = gEstadoReciboAnulado Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("ANUL. DOC. CAIXA (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("ANUL. DOC. CAIXA") & ", "
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 2 Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("ADIANTAMENTO (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("ADIANTAMENTO") & ", "
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 3 Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("ANUL. ADIANTAMENTO (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("ANUL. ADIANTAMENTO") & ", "
                End If
            End If
            
            'DESCRICAO DA ANALISE
            If UCase(Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1)) = "P" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf UCase(Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1)) = "C" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf UCase(Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1)) = "S" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            Else
                ssql = ssql & BL_HandleNull(BL_TrataStringParaBD(BL_SelCodigo("FA_RUBR", "DESCR_RUBR", "COD_RUBR", BL_HandleNull(rsRecibos!cod_facturavel, ""))), "null ") & ", "
            End If
            ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ","
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ") "
            
            
            BG_ExecutaQuery_ADO ssql
            
            rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos
    
End Sub




Private Sub Listaentidades_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaEntidades.ListCount > 0 Then     'Delete
        ListaEntidades.RemoveItem (ListaEntidades.ListIndex)
    End If
End Sub

Private Sub ListaPostos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPostos.ListCount > 0 Then     'Delete
        ListaPostos.RemoveItem (ListaPostos.ListIndex)
    End If
End Sub

Sub PreencheTabelaTemporaria_ANULADOS()

    Dim sSqlRecibos As String
    Dim sSqlCaucoes As String
    Dim sSqlCaucoesAnuladas As String
    Dim ssql As String
    Dim sSqlC As String
    Dim rsRecibos As New ADODB.recordset
    Dim i As Integer
    
    ApagaTabelaCrystal
    
    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    ' ------------------------------------------------------------------------------
    ' RECIBOS EMITIDOS OU POR EMITIR
    ' ------------------------------------------------------------------------------
    If gPassaRecFactus <> mediSim Then
        sSqlRecibos = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x8.user_canc user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x7.taxa, x1.estado,x1.flg_doc_caixa, x7.flg_facturado, x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos = sSqlRecibos & ", x1.nao_conformidade"
        sSqlRecibos = sSqlRecibos & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det_canc x7 "
        sSqlRecibos = sSqlRecibos & ", sl_proven proven, sl_recibos_canc x8 "
        sSqlRecibos = sSqlRecibos & " WHERE x1.cod_efr = x2.cod_efr and x8.user_canc= x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos = sSqlRecibos & " x4.seq_utente = x5.seq_utente  And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos = sSqlRecibos & " AND x1.estado = 'A' AND x8.dt_canc BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        sSqlRecibos = sSqlRecibos & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr and x1.serie_rec = x8.serie_rec and x1.n_rec = x8.n_rec "
        sSqlRecibos = sSqlRecibos & " AND (x1.flg_doc_caixa IS NULL OR x1.flg_doc_caixa = 0)"
        sSqlRecibos = sSqlRecibos & "  AND x4.cod_proven= proven.cod_proven  (+) "
    Else
        sSqlRecibos = " SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_fac num_doc , x1.cod_efr, x8.obs_efr descr_efr, x1.user_cri user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_nota_cred dt_emi, null cod_facturavel, x1.val_total, x1.flg_estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_emiss_nota_cred dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade"
        sSqlRecibos = sSqlRecibos & " FROM fa_nota_cred x1 join sl_efr x2 ON x1.cod_efr = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_idutilizador x3 on x1.user_cri = x3.cod_utilizador"
        sSqlRecibos = sSqlRecibos & " JOIN fa_fact x8 on x1.serie_fac = x8.serie_fac and x1.n_fac = x8.n_fac "
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x8.episodio = x4.n_req"
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " WHERE x1.tipo_nc = 'FACT' AND x1.dt_emiss_nota_cred BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    End If
        sSqlC = sSqlC & RetornaClausulaWhere
    
        
    
    ssql = sSqlRecibos & sSqlC
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    
    If rsRecibos.RecordCount > 0 Then
    
        Dim valPagar As Double
        While Not rsRecibos.EOF
        
        'BRUNODSANTOS - UALIA-569 15.05.2016
        If gPassaRecFactus <> mediSim Then
            valPagar = BL_HandleNull(rsRecibos!taxa, 0) - (BL_HandleNull(rsRecibos!taxa, 0) * (BL_HandleNull(rsRecibos!Desconto, 0)) / 100)
        Else
            valPagar = BL_HandleNull(rsRecibos!val_total, 0)
        End If
        
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR,NUM_SESSAO, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE "
            ssql = ssql & " ,COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES,VALOR ,NOME, VENDA_DINHEIRO,"
            ssql = ssql & " DATA,DESCR_ANALISE,FLG_FACTURADO,DATA_REC, DEScONTO,COD_FORMA_PAG, MES, nao_conformidade)  VALUES("
                    
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!num_doc, -1)) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_emi, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            'BRUNODSANTOS - UALIA-569 15.05.2016
            ssql = ssql & Replace(BL_HandleNull(valPagar, 0), ",", ".") & ", "
            'sSql = sSql & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
            '
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
                        
            'DESCRICAO DA ANALISE
            If Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "P" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "C" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "S" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            Else
                ssql = ssql & "null,"
            End If
            ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ","
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ") "
            BG_ExecutaQuery_ADO ssql
            
            rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos
    
End Sub




Sub PreencheTabelaTemporaria_BORLAS()

    Dim sSqlRecibos As String
    Dim sSqlRecibos1 As String
    Dim sSqlRecibos2 As String
    Dim sSqlCaucoes As String
    Dim sSqlCaucoesAnuladas As String
    Dim ssql As String
    Dim sSqlC As String
    Dim rsRecibos As New ADODB.recordset
    Dim i As Integer
    
    ApagaTabelaCrystal
    sSqlC = sSqlC & RetornaClausulaWhere
    
    ' ------------------------------------------------------------------------------
    ' RECIBOS EMITIDOS OU POR EMITIR
    ' ------------------------------------------------------------------------------
    If gPassaRecFactus <> mediSim Then
        sSqlRecibos1 = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos1 = sSqlRecibos1 & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
        sSqlRecibos1 = sSqlRecibos1 & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos1 = sSqlRecibos1 & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x7.taxa, x1.estado,x1.flg_doc_caixa, x7.flg_facturado, x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos1 = sSqlRecibos1 & ", x1.nao_conformidade "
        sSqlRecibos1 = sSqlRecibos1 & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det x7 "
        sSqlRecibos1 = sSqlRecibos1 & ", sl_proven proven "
        sSqlRecibos1 = sSqlRecibos1 & " WHERE x1.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos1 = sSqlRecibos1 & " x4.seq_utente = x5.seq_utente AND x1.flg_borla = 1  And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos1 = sSqlRecibos1 & " AND x1.estado <>'A' AND (NVL(x1.dt_pagamento,x4.dt_cri) BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos1 = sSqlRecibos1 & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr "
        sSqlRecibos1 = sSqlRecibos1 & " AND x7.flg_adicionada = 0 "
        sSqlRecibos1 = sSqlRecibos1 & "  AND x4.cod_proven= proven.cod_proven  (+) "
        ssql = sSqlRecibos1 & sSqlC & sSqlRecibos2 & sSqlC
    Else
        sSqlRecibos = " SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_fac num_doc , x1.cod_efr, x1.obs_efr descr_efr, x1.user_cri user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_fac dt_emi, null cod_facturavel, x1.val_total taxa, x1.flg_estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_emiss_fac dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade"
        sSqlRecibos = sSqlRecibos & " FROM fa_fact x1 join sl_efr x2 ON x1.cod_efr = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_idutilizador x3 on x1.user_cri = x3.cod_utilizador"
        sSqlRecibos = sSqlRecibos & " JOIN fa_fact x8 on x1.serie_fac = x8.serie_fac and x1.n_fac = x8.n_fac "
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x8.episodio = x4.n_req"
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " JOIN slv_documentos_doente x9 on x1.serie_fac = x9.serie_doc and x1.n_fac = x9.n_doc "
        sSqlRecibos = sSqlRecibos & " WHERE x9.flg_borla= 'S' AND x1.dt_emiss_fac BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        ssql = sSqlRecibos & sSqlC
    End If
        
    
    
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    
    If rsRecibos.RecordCount > 0 Then
        While Not rsRecibos.EOF
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR,num_sessao, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE "
            ssql = ssql & " ,COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES,VALOR ,NOME, VENDA_DINHEIRO,"
            ssql = ssql & " DATA,DESCR_ANALISE,FLG_FACTURADO, DATA_REC, DESCONTO,COD_FORMA_PAG, MES, nao_conformidade)  VALUES("
                    
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!num_doc, -1)) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_emi, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            'sSql = sSql & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
            ssql = ssql & Replace((BL_HandleNull(rsRecibos!taxa, 0) * (100 - BL_HandleNull(rsRecibos!Desconto, 0))) / 100, ",", ".") & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
                        
            'DESCRICAO DA ANALISE
            If Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "P" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "C" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "S" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            Else
                ssql = ssql & "'ANALISES',"
            End If
            ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ","
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ") "
            BG_ExecutaQuery_ADO ssql
            
            rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos
    
End Sub

Sub PreencheTabelaTemporaria_COBRANCAS()

    Dim sSqlRecibos As String
    Dim sSqlCaucoes As String
    Dim sSqlCaucoesAnuladas As String
    Dim ssql As String
    Dim sSqlC As String
    Dim rsRecibos As New ADODB.recordset
    Dim i As Integer
    
    ApagaTabelaCrystal
    ' ------------------------------------------------------------------------------
    ' RECIBOS EMITIDOS OU POR EMITIR
    ' ------------------------------------------------------------------------------
    If gPassaRecFactus <> mediSim Then
        sSqlRecibos = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel,"
        sSqlRecibos = sSqlRecibos & " x7.taxa, x1.estado,x1.flg_doc_caixa, x7.flg_facturado,x1.dt_pagamento, x1.total_pagar,x1.desconto,estado_req,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos = sSqlRecibos & ", x1.nao_conformidade"
        sSqlRecibos = sSqlRecibos & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det x7 "
        sSqlRecibos = sSqlRecibos & ", sl_proven proven "
        sSqlRecibos = sSqlRecibos & " WHERE x1.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos = sSqlRecibos & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos = sSqlRecibos & " AND x1.estado IN ('C') AND (x1.dt_emi BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos = sSqlRecibos & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr and (x7.flg_adicionada = 0 OR x7.flg_adicionada IS NULL)"
        sSqlRecibos = sSqlRecibos & "  AND x4.cod_proven= proven.cod_proven  (+) "
        sSqlC = sSqlC & " AND (x1.flg_borla IS NULL OR x1.flG_borla = 0) "
    Else
        sSqlRecibos = " SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_fac num_doc , x1.cod_efr, x1.obs_efr descr_efr, x1.user_cri user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_fac dt_emi, null cod_facturavel, x1.val_total taxa, x1.flg_estado estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_emiss_fac dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade, x4.estado_req "
        sSqlRecibos = sSqlRecibos & " FROM fa_fact x1 join sl_efr x2 ON x1.cod_efr = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_idutilizador x3 on x1.user_cri = x3.cod_utilizador"
        sSqlRecibos = sSqlRecibos & " JOIN fa_fact x8 on x1.serie_fac = x8.serie_fac and x1.n_fac = x8.n_fac "
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x8.episodio = x4.n_req"
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " JOIN slv_documentos_doente x9 on x1.serie_fac = x9.serie_doc and x1.n_fac = x9.n_doc "
        sSqlRecibos = sSqlRecibos & " WHERE x9.flg_borla= 'N' and x9.dt_pagamento is null AND x1.dt_emiss_fac BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    End If
        
    sSqlC = sSqlC & RetornaClausulaWhere
    
    ssql = sSqlRecibos & sSqlC
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    
    If rsRecibos.RecordCount > 0 Then
        While Not rsRecibos.EOF
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR,NUM_SESSAO, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE "
            ssql = ssql & " ,COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES,VALOR ,NOME, VENDA_DINHEIRO, "
            ssql = ssql & " DATA,DESCR_ANALISE,FLG_FACTURADO, DATA_REC, DOCUMENTO, DESCONTO,COD_FORMA_PAG, nao_conformidade)  VALUES("
                    
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!num_doc, -1)) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_emi, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            ssql = ssql & Replace((BL_HandleNull(rsRecibos!taxa, 0) * (100 - BL_HandleNull(rsRecibos!Desconto, 0))) / 100, ",", ".") & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
                        
            'DESCRICAO DA ANALISE
            If Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "P" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "C" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "S" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            Else
                ssql = ssql & "'ANALISES CLINICAS',"
            End If
            ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
            
            'TIPO DE DOCUMENTO
            If BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And rsRecibos!Estado <> gEstadoReciboAnulado Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("RECIBO (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("RECIBO") & ", "
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And rsRecibos!Estado <> gEstadoReciboAnulado Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("DOC. CAIXA (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("DOC. CAIXA") & ", "
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And rsRecibos!Estado = gEstadoReciboAnulado Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("ANUL. RECIBO (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("ANUL. RECIBO") & ", "
                End If
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And rsRecibos!Estado = gEstadoReciboAnulado Then
                If BL_HandleNull(rsRecibos!estado_req, "") = "C" Then
                    ssql = ssql & BL_TrataStringParaBD("ANUL. DOC. CAIXA (*)") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("ANUL. DOC. CAIXA") & ", "
                End If
            End If
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ") "
            BG_ExecutaQuery_ADO ssql
            
            rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos
    
End Sub



Sub PreencheTabelaTemporaria_dividas()
    Dim sSqlRecibos As String
    Dim sSqlCaucoes As String
    Dim sSqlCaucoesAnuladas As String
    Dim ssql As String
    Dim sSqlC As String
    Dim rsRecibos As New ADODB.recordset
    Dim i As Integer
    Dim data As String
    
    ' DATA QUE UTILIZADOR ESCOLHEU
    If OptData(0).value = True Then
        data = "x4.dt_cri "
    ElseIf OptData(1).value = True Then
        data = "x1.dt_cri "
    ElseIf OptData(2).value = True Then
        data = "x4.dt_chega "
    End If
    
    ApagaTabelaCrystal
    
    ' ------------------------------------------------------------------------------
    ' RECIBOS EMITIDOS OU POR EMITIR
    ' ------------------------------------------------------------------------------
    If gPassaRecFactus <> mediSim Then
        sSqlRecibos = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_cri, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x7.taxa, x1.estado,x1.flg_doc_caixa,x2.flg_usa_preco_ent, x7.flg_facturado,x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos = sSqlRecibos & ", x1.nao_conformidade"
        sSqlRecibos = sSqlRecibos & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det x7 "
        sSqlRecibos = sSqlRecibos & ", sl_proven proven "
        sSqlRecibos = sSqlRecibos & " WHERE x1.cod_efr = x2.cod_efr and x1.user_cri = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos = sSqlRecibos & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos = sSqlRecibos & " AND (x1.estado ='N' or x1.cod_efr = " & gCodEFRSemCredencial & " OR x1.nao_conformidade = 1)  AND "
        sSqlRecibos = sSqlRecibos & " (" & data & " BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos = sSqlRecibos & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr "
        sSqlRecibos = sSqlRecibos & " AND x4.estado_Req <>'C' AND x2.FLG_MOSTRA_DIVIDA = 1 AND (x7.flg_adicionada = 0 or x7.flg_adicionada is null) "
        sSqlRecibos = sSqlRecibos & "  AND x4.cod_proven= proven.cod_proven  (+) "
        sSqlRecibos = sSqlRecibos & " AND (x1.flg_borla IS NULL OR x1.flG_borla = 0) "
        
        sSqlC = sSqlC & " AND (x1.flg_borla IS NULL OR x1.flG_borla = 0) "
    Else
        sSqlRecibos = " SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_fac num_doc , x0.cod_efr, x2.descr_efr, null user_cri, "
        sSqlRecibos = sSqlRecibos & " null Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_ini_real dt_emi, x1.cod_rubr cod_facturavel, (x1.val_pag_doe +x1.val_taxa) taxa, x1.flg_estado estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_ini_real dt_pagamento, 0 desconto, null cod_forma_pag ,  null mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade, x4.estado_req "
        sSqlRecibos = sSqlRecibos & " FROM fa_movi_fact x1 join fa_movi_resp x0 on x1.t_episodio = x0.t_episodio and x1.episodio = x0.episodio and x1.n_ord = x0.n_ord"
        sSqlRecibos = sSqlRecibos & " join sl_efr x2 ON x0.cod_efr = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x1.episodio = x4.n_req"
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " WHERE  x1.dt_ini_real BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        sSqlRecibos = sSqlRecibos & " AND (nvl(x1.flg_estado_doe,0) <= 1 or nvl(x1.flg_estado_tx,0) = 1)"
    
    End If
    sSqlC = sSqlC & RetornaClausulaWhere

    
    ssql = sSqlRecibos & sSqlC
    
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    
    If rsRecibos.RecordCount > 0 Then
        While Not rsRecibos.EOF
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR, NUM_SESSAO, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE "
            ssql = ssql & " ,COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES,VALOR ,NOME, VENDA_DINHEIRO,"
            ssql = ssql & " DATA,DESCR_ANALISE,FLG_FACTURADO,DATA_REC,DESCONTO,COD_FORMA_PAG,MES, nao_conformidade)  VALUES("
                    
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            If BL_HandleNull(BL_HandleNull(rsRecibos!n_req, -1)) = mediSim Then
                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!num_doc, -1) & "*") & ", "
            Else
                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!num_doc, -1)) & ", "
            End If
            
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_cri, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
                        
            'DESCRICAO DA ANALISE
            If Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "P" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "C" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "S" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            Else
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("FA_RUBR", "DESCR_RUBR", "COD_RUBR", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            End If
            ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ","
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ") "
            
            BG_ExecutaQuery_ADO ssql
            
            rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos

End Sub


Sub PreencheTabelaTemporaria_MARGENS()
    Dim sSqlRecibos As String
    Dim sSqlRecibos2 As String
    Dim sSqlCaucoes As String
    Dim sSqlCaucoesAnuladas As String
    Dim ssql As String
    Dim sSqlC As String
    Dim sSqlO As String
    Dim i As Integer
    Dim rsRecibos As New ADODB.recordset
    Dim percentagem As String
    Dim totalFacturar As String
    Dim TotalUnitario As Double
    Dim JaRecebido As String
    Dim PorReceber As String
    Dim codRubrEfr As String
    Dim descrRubrEfr As String
    Dim data As String
    Dim ma As String
    'edgar.parada LJMANSO-288 03.05.2019
    Dim valDoe As String
    Dim valTx  As String
    '
    On Error GoTo TrataErro
    
    ApagaTabelaCrystal
    
    ' DATA QUE UTILIZADOR ESCOLHEU
    If OptData(0).value = True Then
        data = "x4.dt_cri "
    ElseIf OptData(1).value = True Then
        data = "x1.dt_cri "
    ElseIf OptData(2).value = True Then
        data = "x4.dt_chega "
    End If
    
    
    ' ------------------------------------------------------------------------------
    ' RECIBOS EMITIDOS OU POR EMITIR
        ' Edgar.Parada LJMANSO-288 18.04.2019
    ' Obter dados do FACTUS
    ' ------------------------------------------------------------------------------
        If gPassaRecFactus <> mediSim Then
                If gSGBD = gOracle Then
                        sSqlRecibos = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
                        sSqlRecibos = sSqlRecibos & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
                        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
                        sSqlRecibos = sSqlRecibos & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x7.taxa, "
                        sSqlRecibos = sSqlRecibos & " x1.estado,x1.flg_doc_caixa, x5.t_utente, x5.utente, x1.total_pagar, x7.quantidade, x1.desconto, x7.flg_Facturado,"
                        sSqlRecibos = sSqlRecibos & " x1.dt_pagamento,x1.desconto, x7.isencao, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
                        sSqlRecibos = sSqlRecibos & ", x1.nao_conformidade"
                        sSqlRecibos = sSqlRecibos & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det x7 "
                        sSqlRecibos = sSqlRecibos & ", sl_proven proven "
                        sSqlRecibos = sSqlRecibos & " WHERE x1.n_req = x4.n_req and x1.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and "
                        sSqlRecibos = sSqlRecibos & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) "
                        sSqlRecibos = sSqlRecibos & " AND x1.estado IN ('P','N', 'NL','C') AND (" & data & " BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
                        sSqlRecibos = sSqlRecibos & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.cod_efr = x7.cod_efr "
                        sSqlRecibos = sSqlRecibos & " AND x4.cod_proven= proven.cod_proven  (+) "
                        
                        gMsgMsg = "Deseja incluir an�lises retiradas?"
                        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                        If gMsgResp = vbNo Then
                                sSqlRecibos = sSqlRecibos & " and (flg_retirado is null or flg_retirado = 0 or x7.cod_efr = " & gEfrParticular & ")"
                        End If
                        
                        sSqlRecibos = sSqlRecibos & " AND  x7.isencao <> 3 and x4.estado_req <>'C' and (flg_adicionada is null or flg_adicionada = 0) "
                
                
                End If
                        
                sSqlC = ""
                
                sSqlC = sSqlC & RetornaClausulaWhere
                sSqlO = ""
                If gSGBD = gSqlServer Then
                        sSqlO = " GROUP BY x1.n_req,  x1.n_rec  , x1.cod_efr, x2.descr_efr, x1.user_emi,x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
                        sSqlO = sSqlO & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri ,x7.cod_facturavel, x7.taxa,  x1.estado,x1.flg_doc_caixa,x5.t_utente, x5.utente,"
                        sSqlO = sSqlO & " x1.total_pagar, x7.quantidade, x1.desconto, x7.flg_facturado,x1.dt_pagamento,x1.desconto,x7.isencao,x1.cod_forma_pag "
                End If
                
                'sSqlC = sSqlC & " AND x4.n_Req = 1151722 "
                ssql = sSqlRecibos & sSqlC & sSqlRecibos2 & sSqlC & sSqlO
        Else
                 sSqlRecibos = "SELECT a.*,"
                 sSqlRecibos = sSqlRecibos & "(SELECT dt_pagamento From sl_dados_doc WHERE serie_doc = a.doc_serie_efr AND n_doc = a.doc_efr) dt_pag_efr, "
                 sSqlRecibos = sSqlRecibos & "NVl((SELECT dt_pagamento From sl_dados_doc WHERE serie_doc = a.doc_serie_doe AND n_doc = a.doc_doe), "
                 sSqlRecibos = sSqlRecibos & "(SELECT dt_pagamento From sl_dados_doc WHERE serie_doc = a.doc_serie_tx AND n_doc = a.doc_tx)) dt_pag_doe  FROM ( "
                 
                 sSqlRecibos = sSqlRecibos & "SELECT " & BL_TrataStringParaBD(gComputador) & " nome_computador, x1.episodio n_req, x1.descr_lin_fac cod_facturavel, x2.cod_efr, x2.descr_efr, x4.cod_sala ,x4.descr_sala, "
                 sSqlRecibos = sSqlRecibos & "x6.t_utente, x6.nome_ute, x1.flg_estado estado_efr, x1.val_total pag_efr, x1.flg_estado_doe estado_doe, x1.flg_estado_tx estado_taxa, x1.val_pag_doe, x1.val_taxa, x1.serie_fac doc_serie_efr, x1.n_fac doc_efr, "
                 sSqlRecibos = sSqlRecibos & "x1.serie_fac_doe doc_serie_doe, x1.n_fac_doe doc_doe, x1.serie_fac_tx doc_serie_tx, x1.n_fac_tx doc_tx, x1.qtd, x3.dt_cri, TO_CHAR (x3.dt_cri, 'mm-yyyy') mes "
                 sSqlRecibos = sSqlRecibos & "FROM fa_movi_fact x1, fa_movi_resp x2, sl_requis x3, sl_cod_salas x4, sl_efr x5, sl_identif x6, sl_proven x7 "
                 sSqlRecibos = sSqlRecibos & "Where X1.episodio = X2.episodio AND x1.n_ord = x2.n_ord AND x1.episodio = x3.n_req AND x2.episodio = x3.n_req AND x3.cod_sala = x4.cod_sala(+)AND x2.cod_efr = x5.cod_efr "
                 sSqlRecibos = sSqlRecibos & "AND x6.seq_utente = x3.seq_utente AND x3.cod_proven = x7.cod_proven (+)"
                 sSqlRecibos = sSqlRecibos & "AND (x3.dt_cri BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ") AND x1.flg_estado <> 2 "
                 
                 sSqlC = ""
                
                 sSqlC = sSqlC & RetornaClausulaWhere
                  
                 ssql = sSqlRecibos & sSqlC & ") a"
     
                                                                                                                                                                                                                                                               
    End If
        
        Call BG_LogFile_Erros("Query Listagem Margens (TEMP)-> " & ssql)
        
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    If rsRecibos.RecordCount > 0 Then
        While Not rsRecibos.EOF
           'edgar.parada LJMANSO-288 03.05.2019
            valDoe = "0"
            valTx = "0"
            '
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR, NUM_SESSAO, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE, "
            ssql = ssql & " COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES, NOME, VENDA_DINHEIRO,"
            ssql = ssql & " DATA, DOCUMENTO,TOTAL_UTENTE,JA_RECEBIDO, POR_RECEBER,DESCR_ANALISE,TOTAL_FACTURAR, "
            ssql = ssql & " TOTAL_PERCENTAGEM, FLG_FACTURADO, DATA_REC, DESCONTO, cod_isencao,COD_FORMA_PAG,MES, "
            'rcoelho 17.05.2013 lrs-146
            ssql = ssql & " nao_conformidade, margem_posto) VALUES ("
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            If gPassaRecFactus <> mediSim Then
              ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!num_doc, -1)) & ", "
            Else
              ssql = ssql & BL_TrataStringParaBD("") & ", "
            End If
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            If gPassaRecFactus <> mediSim Then
              ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_emi, "")) & ", "
              ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            Else
              ssql = ssql & BL_TrataStringParaBD("") & ", " & BL_TrataStringParaBD("") & ", " 'operador de emissao
            End If
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            If gPassaRecFactus <> mediSim Then
              ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
              ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
            Else
              ssql = ssql & BL_TrataStringParaBD("") & ", " & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_cri, "")) & ", "
            End If
            
            'TIPO DE DOCUMENTO
            If gPassaRecFactus <> mediSim Then
                If BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 Then
                    ssql = ssql & BL_TrataStringParaBD("RECIBO") & ", "
                Else
                    ssql = ssql & BL_TrataStringParaBD("DOC CAIXA") & ", "
                End If
            Else
              ssql = ssql & BL_TrataStringParaBD("") & ", "
            End If
            
            'VALOR TOTAL DO UTENTE - SE TIVER ANULADO � NEGATIVO
            If gPassaRecFactus <> mediSim Then
                If BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboNaoEmitido Or BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboPago Or BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboNulo Or BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboCobranca Then
                
                    If BL_HandleNull(rsRecibos!Desconto, 1) > 1 Then
                        ssql = ssql & Replace(CalculaValorDesconto(BL_HandleNull(rsRecibos!taxa, 0), BL_HandleNull(rsRecibos!Desconto, 0)), ",", ".") & ", "
                    Else
                        ssql = ssql & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
                    End If
                ElseIf BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboAnulado Then
                    ssql = ssql & "0 - " & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
                End If
            Else
               If rsRecibos!val_pag_doe <> 0 Then
                                  'edgar.parada LJMANSO-389
                  'valDoe = Replace((CDbl(rsRecibos!val_pag_doe) * CInt(rsRecibos!qtd)), ",", ".")
                                  valDoe = Replace(rsRecibos!val_pag_doe, ",", ".")
                  ssql = ssql & valDoe & ", "
               ElseIf rsRecibos!val_Taxa <> 0 Then
                  valTx = Replace((CDbl(rsRecibos!val_Taxa) * CInt(rsRecibos!qtd)), ",", ".")
                  ssql = ssql & valTx & ", "
               Else
                   ssql = ssql & "0,"
               End If
            End If
            
            'VALOR JA RECEBIDO. SE ESTIVER NAO EMITIDO = TAXA SENAO � 0
            If gPassaRecFactus <> mediSim Then
                If BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboNaoEmitido Or BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboNulo Or BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboCobranca Then
                   ssql = ssql & "0, "
                   JaRecebido = "0"
                ElseIf BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboPago Then
                    If BL_HandleNull(rsRecibos!Desconto, 1) > 1 Then
                        ssql = ssql & Replace(CalculaValorDesconto(BL_HandleNull(rsRecibos!taxa, 0), BL_HandleNull(rsRecibos!Desconto, 0)), ",", ".") & ", "
                    Else
                        ssql = ssql & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
                    End If
                    JaRecebido = Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".")
                ElseIf BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboAnulado Then
                    ssql = ssql & "0, "
                    JaRecebido = "0"
                End If
                                                                                                                                                                
                                                                                                                                                                  
                                                                   
                                                                
                                  
            
                'VALOR FALTA PAGAR
                If BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboNaoEmitido Or BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboNulo Or BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboCobranca Then
                    If BL_HandleNull(rsRecibos!Desconto, 1) > 1 Then
                        ssql = ssql & Replace(CalculaValorDesconto(BL_HandleNull(rsRecibos!taxa, 0), BL_HandleNull(rsRecibos!Desconto, 0)), ",", ".") & ", "
                    Else
                        ssql = ssql & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
                    End If
                    
                    'sSql = sSql & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
                    PorReceber = Replace(CalculaValorDesconto(BL_HandleNull(rsRecibos!taxa, 0), BL_HandleNull(rsRecibos!Desconto, 0)), ",", ".")
                ElseIf BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboAnulado Then
                    ssql = ssql & "0 - " & Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".") & ", "
                    PorReceber = CStr(0 - CDbl(Replace(BL_HandleNull(rsRecibos!taxa, 0), ",", ".")))
                ElseIf BL_HandleNull(rsRecibos!Estado, "") = gEstadoReciboPago Then
                    ssql = ssql & "0, "
                    PorReceber = "0"
                End If
            Else
               If ((BL_HandleNull(rsRecibos!doc_serie_doe, "") <> "" And BL_HandleNull(rsRecibos!doc_doe, "") <> "") Or (BL_HandleNull(rsRecibos!doc_serie_tx, "") <> "" And BL_HandleNull(rsRecibos!doc_tx, "") <> "")) And BL_HandleNull(rsRecibos!dt_pag_doe, "") <> "" Then
               
                  If rsRecibos!val_pag_doe <> 0 Then
                         ssql = ssql & valDoe & ", " 'Recebido
                         ssql = ssql & "0" & ", "  'Por Receber
                   ElseIf rsRecibos!val_Taxa <> 0 Then
                         ssql = ssql & valTx & ", " 'Recebido
                         ssql = ssql & "0" & ", "  'Por Receber
                   Else
                         ssql = ssql & "0" & ", " 'Recebido
                         ssql = ssql & "0" & ", "  'Por Receber
                   End If
                     
               Else
                   If rsRecibos!val_pag_doe <> 0 Then
                        ssql = ssql & "0" & ", "  'Recebido
                        ssql = ssql & valDoe & ", " 'Por Receber
                   ElseIf rsRecibos!val_Taxa <> 0 Then
                        ssql = ssql & "0" & ", "  'Recebido
                        ssql = ssql & valTx & ", " 'Por Receber
                   Else
                        ssql = ssql & "0" & ", " 'Recebido
                        ssql = ssql & "0" & ", "  'Por Receber
                   End If
               End If
               
            End If
                        
            'DESCRICAO DA ANALISE
            If gPassaRecFactus <> mediSim Then
                If Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "P" Then
                    ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
                ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "C" Then
                    ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
                ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "S" Then
                    ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
                Else
                    ssql = ssql & "NULL" & ", "
                End If
            Else
                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_facturavel, "")) & ", "
            End If
            
            'TOTAL A FACTURAR
            If gPassaRecFactus <> mediSim Then
                totalFacturar = IF_RetornaTaxaAnaliseEntidade(BL_HandleNull(rsRecibos!cod_facturavel, ""), BL_HandleNull(rsRecibos!cod_efr, ""), _
                                BL_HandleNull(rsRecibos!dt_emi, ""), codRubrEfr, rsRecibos!t_utente, rsRecibos!Utente, TotalUnitario, _
                                BL_HandleNull(rsRecibos!quantidade, "1"), "", False, descrRubrEfr, mediComboValorNull)
                
                
                If gLAB = "LHL" Then
                    If rsRecibos!cod_efr = gEfrParticular Or rsRecibos!cod_efr = "54" Then
                        If totalFacturar = "0" Then
                            totalFacturar = CDbl(Replace(PorReceber, ".", ",")) + CDbl(Replace(JaRecebido, ".", ","))
                        End If
                    End If
                End If
                
                If Mid(totalFacturar, 1, 1) = "." Then
                    totalFacturar = "0" & totalFacturar
                End If
                If CDbl(totalFacturar) <= 0 Then
                    totalFacturar = "0.0"
                End If
                ssql = ssql & Replace(BL_HandleNull(totalFacturar, "0.0"), ",", ".") & ","
            
            Else
                ssql = ssql & Replace(BL_HandleNull(rsRecibos!pag_efr, "0.0"), ",", ".") & ","
                                  
                                                                                        
                                                                         
            End If
                                                                                                                                                                          
            
            'TOTAL DA PERCENTAGEM DO POSTO
            If gPassaRecFactus <> mediSim Then
                If BL_HandleNull(rsRecibos!cod_sala, "") <> "" Then
                    percentagem = BL_CalculaPercentagemPosto(rsRecibos!n_req, rsRecibos!cod_sala, Replace(JaRecebido, ".", ","), Replace(PorReceber, ".", ","), Replace(BL_HandleNull(totalFacturar, "0"), ".", ","))
                End If
                If percentagem = "" Then
                    percentagem = "0.0"
                End If
                ssql = ssql & Replace(percentagem, ",", ".") & ","
            Else
                ssql = ssql & "0" & ","
            End If
            
            If gPassaRecFactus <> mediSim Then
              ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            Else
              ssql = ssql & CInt("0") & ", "
            End If
            If gPassaRecFactus <> mediSim Then
                                                                                                                                                  
               ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
                                                                                                                                                                           
                                                                                                                           
                                                                                                                                                                                                                                         
            Else
               ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pag_doe, "0")) & ", "
            End If
            If gPassaRecFactus <> mediSim Then
              ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            Else
              ssql = ssql & CInt("0") & ","
            End If
            If gPassaRecFactus <> mediSim Then
              ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!isencao, "")) & ","
            Else
               ssql = ssql & "NULL" & ","
            End If
            If gPassaRecFactus <> mediSim Then
              ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ","
              ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ", "
              'rcoelho 17.05.2013 lrs-146
              ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ", "
              'rcoelho 20.08.2013 ualia-289
            Else
              ssql = ssql & "NULL" & ","
              ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ","
              ssql = ssql & CInt("0") & ","
            End If
            If gPassaRecFactus <> mediSim Then
                If BL_HandleNull(rsRecibos!cod_sala, "") <> "" Then
                    ma = BL_SelCodigo("SL_COD_SALAS", "MARGEM", "COD_SALA", BL_HandleNull(rsRecibos!cod_sala, "-1"), "V")
                Else
                    ma = ""
                End If
                If ma = "" Then
                    ssql = ssql & "NULL" & ") "
                Else
                    ssql = ssql & CInt(ma) & ") "
                End If
           Else
              ssql = ssql & CInt("0") & ") "
           End If
           BG_ExecutaQuery_ADO ssql
            
           rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos
Exit Sub
TrataErro:
    BG_Mensagem mediMsgBox, "Erro Gerar Estatistica Margens a Postos", vbOKOnly + vbCritical, "Erro"
    Call BG_LogFile_Erros("Erro Gerar Estatistica Margens a Postos  -> " & Err.Description)
    Call BG_LogFile_Erros(ssql)
    Exit Sub
    Resume Next

End Sub


Sub PreencheTabelaTemporaria_FacturaRecibos()
    Dim sSqlRecibos As String
    Dim sSqlRecibos1 As String
    Dim sSqlRecibos2 As String
    Dim sSqlRecibos3 As String
    Dim ssql As String
    Dim sSqlC As String
    Dim rsRecibos As New ADODB.recordset
    Dim i As Integer
    Dim ult_n_rec As String
    Dim ult_estado_rec As String
    
    ApagaTabelaCrystal
    
    
    If gPassaRecFactus <> mediSim Then
        'edgar.parada LJMANSO-254 11.04.2018 colocar a condi��o 'OR x1.flg_borla = 1'
        ' O QUE EST� PAGO
        sSqlRecibos1 = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos1 = sSqlRecibos1 & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
        sSqlRecibos1 = sSqlRecibos1 & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos1 = sSqlRecibos1 & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x1.total_pagar, x1.estado,x1.flg_doc_caixa, 'POS' tipo, x7.flg_facturado,x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        sSqlRecibos1 = sSqlRecibos1 & ", x1.nao_conformidade"
        sSqlRecibos1 = sSqlRecibos1 & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det x7 "
        sSqlRecibos1 = sSqlRecibos1 & ", sl_proven proven "
        sSqlRecibos1 = sSqlRecibos1 & " WHERE x1.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos1 = sSqlRecibos1 & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos1 = sSqlRecibos1 & " AND x1.estado in ('A','P','C') AND (x1.dt_emi BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos1 = sSqlRecibos1 & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.serie_rec = x7.serie_rec  AND x1.cod_efr = x7.cod_efr AND (x1.flg_doc_caixa = 0 OR x1.flg_doc_caixa is null) "
        sSqlRecibos1 = sSqlRecibos1 & "  AND x4.cod_proven= proven.cod_proven(+) AND (x1.flg_borla IS NULL OR x1.flg_borla = 0 OR x1.flg_borla = 1) "
        '
        'edgar.parada LJMANSO-254 11.04.2018 colocar a condi��o 'OR x1.flg_borla = 1'
        ' CONTA MESMO COM OS ANULADOS
        sSqlRecibos2 = " UNION SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos2 = sSqlRecibos2 & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
        sSqlRecibos2 = sSqlRecibos2 & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos2 = sSqlRecibos2 & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x1.total_pagar, 'P' estado,x1.flg_doc_caixa, 'POS' tipo, x7.flg_facturado,x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes   "
        sSqlRecibos2 = sSqlRecibos2 & ", x1.nao_conformidade"
        sSqlRecibos2 = sSqlRecibos2 & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det_canc x7 "
        sSqlRecibos2 = sSqlRecibos2 & ", sl_proven proven "
        sSqlRecibos2 = sSqlRecibos2 & " WHERE x1.cod_efr = x2.cod_efr and x1.user_emi = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos2 = sSqlRecibos2 & " x4.seq_utente = x5.seq_utente   And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos2 = sSqlRecibos2 & " AND x1.estado = 'A' AND (x1.dt_emi BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos2 = sSqlRecibos2 & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.serie_rec = x7.serie_rec AND x1.cod_efr = x7.cod_efr AND (x1.flg_doc_caixa = 0 OR x1.flg_doc_caixa is null)"
        sSqlRecibos2 = sSqlRecibos2 & "  AND x4.cod_proven= proven.cod_proven(+) AND (x1.flg_borla IS NULL OR x1.flg_borla = 0 OR x1.flg_borla = 1) "
        '
        'edgar.parada LJMANSO-254 11.04.2018 colocar a condi��o 'OR x1.flg_borla = 1'
        'MENOS O QUE FOI ANULADO
        sSqlRecibos3 = " UNION SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        sSqlRecibos3 = sSqlRecibos3 & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x8.user_canc user_emi, "
        sSqlRecibos3 = sSqlRecibos3 & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        sSqlRecibos3 = sSqlRecibos3 & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, 0 -  x1.total_pagar total_pagar, x1.estado,x1.flg_doc_caixa, 'NEG' tipo, x7.flg_facturado, x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes   "
        sSqlRecibos3 = sSqlRecibos3 & ", x1.nao_conformidade"
        sSqlRecibos3 = sSqlRecibos3 & " FROM sl_recibos x1, sl_efr x2, sl_idutilizador x3, sl_requis x4, sl_identif x5, sl_cod_salas x6, sl_recibos_det_canc x7,sl_recibos_canc x8 "
        sSqlRecibos3 = sSqlRecibos3 & ", sl_proven proven "
        sSqlRecibos3 = sSqlRecibos3 & " WHERE x1.cod_efr = x2.cod_efr and x8.user_canc  = x3.cod_utilizador(+) and x1.n_req = x4.n_req and"
        sSqlRecibos3 = sSqlRecibos3 & " x4.seq_utente = x5.seq_utente And x4.cod_Sala = x6.cod_Sala(+) "
        sSqlRecibos3 = sSqlRecibos3 & " AND x1.estado = 'A' AND (x8.dt_canc BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        sSqlRecibos3 = sSqlRecibos3 & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.serie_rec = x7.serie_rec AND x1.cod_efr = x7.cod_efr "
        sSqlRecibos3 = sSqlRecibos3 & " AND x1.n_rec = x8.n_rec AND x1.serie_rec = x8.serie_rec AND (x1.flg_doc_caixa = 0 OR x1.flg_doc_caixa is null)"
        sSqlRecibos3 = sSqlRecibos3 & "  AND x4.cod_proven= proven.cod_proven(+) AND (x1.flg_borla IS NULL OR x1.flg_borla = 0 OR x1.flg_borla = 1) "
        sSqlC = sSqlC & RetornaClausulaWhere

    
        ssql = sSqlRecibos1 & sSqlC & sSqlRecibos2 & sSqlC & sSqlRecibos3 & sSqlC
    Else
        sSqlRecibos = " SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        sSqlRecibos = sSqlRecibos & " x1.n_fac num_doc , x1.cod_efr, x2.descr_efr, x1.user_cri user_emi, "
        sSqlRecibos = sSqlRecibos & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        sSqlRecibos = sSqlRecibos & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_fac dt_emi, x7.cod_rubr cod_facturavel, x1.val_total total_pagar, x1.flg_estado estado, null flg_doc_caixa, null flg_facturado, "
        sSqlRecibos = sSqlRecibos & "  x1.dt_emiss_fac dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        sSqlRecibos = sSqlRecibos & ", null nao_conformidade, x4.estado_req, 'POS' tipo "
        sSqlRecibos = sSqlRecibos & " FROM fa_fact x1 join sl_efr x2 ON x1.cod_efr = x2.cod_efr"
        sSqlRecibos = sSqlRecibos & " JOIN sl_idutilizador x3 on x1.user_cri = x3.cod_utilizador"
        sSqlRecibos = sSqlRecibos & " JOIN sl_requis x4 on x1.episodio = x4.n_req"
        sSqlRecibos = sSqlRecibos & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        sSqlRecibos = sSqlRecibos & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac"
        sSqlRecibos = sSqlRecibos & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        sSqlRecibos = sSqlRecibos & " JOIN slv_documentos_doente x9 on x1.serie_fac = x9.serie_doc and x1.n_fac = x9.n_doc "
        sSqlRecibos = sSqlRecibos & " WHERE  x1.dt_emiss_fac BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        sSqlC = sSqlC & RetornaClausulaWhere

    
        ssql = sSqlRecibos & sSqlC

    
    End If
        
    If gSGBD = gOracle Then
        ssql = ssql & " ORDER BY dt_emi ASC, n_req ASC, num_doc ASC, tipo DESC"
    ElseIf gSGBD = gSqlServer Then
        ssql = ssql & " ORDER BY x1.dt_emi ASC, x1.n_req ASC, x1.n_rec ASC, tipo DESC"
    End If
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    
    If rsRecibos.RecordCount > 0 Then
        ult_n_rec = ""
        ult_estado_rec = ""
        While Not rsRecibos.EOF
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR, NUM_SESSAO, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE "
            ssql = ssql & " ,COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES,VALOR ,NOME, VENDA_DINHEIRO,"
            ssql = ssql & " DATA, DOCUMENTO, DESCR_ANALISE,FLG_FACTURADO, DATA_REC, DESCONTO,COD_FORMA_PAG, MES, nao_conformidade)  VALUES("
                    
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!num_doc, -1) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_emi, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            
            If ult_n_rec <> BL_HandleNull(rsRecibos!num_doc, -1) Or ult_estado_rec <> rsRecibos!Estado Then
                ssql = ssql & Replace(BL_HandleNull(rsRecibos!total_pagar, 0), ",", ".") & ", "
            Else
                ssql = ssql & " 0, "
            End If
            ult_n_rec = BL_HandleNull(rsRecibos!num_doc, -1)
            ult_estado_rec = rsRecibos!Estado
            
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
                        
            'TIPO DE DOCUMENTO
            If BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And rsRecibos!Estado <> gEstadoReciboAnulado Then
                ssql = ssql & BL_TrataStringParaBD("RECIBO") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And rsRecibos!Estado <> gEstadoReciboAnulado Then
                ssql = ssql & BL_TrataStringParaBD("DOC. CAIXA") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And rsRecibos!Estado = gEstadoReciboAnulado Then
                ssql = ssql & BL_TrataStringParaBD("ANUL. RECIBO") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And rsRecibos!Estado = gEstadoReciboAnulado Then
                ssql = ssql & BL_TrataStringParaBD("ANUL. DOC. CAIXA") & ", "
            End If
            
            'DESCRICAO DA ANALISE
            If Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "P" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "C" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            ElseIf Mid(BL_HandleNull(rsRecibos!cod_facturavel, ""), 1, 1) = "S" Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            Else
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("FA_RUBR", "DESCR_RUBR", "COD_RUBR", BL_HandleNull(rsRecibos!cod_facturavel, ""))) & ", "
            End If
            ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ","
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ") "
            
            
            BG_ExecutaQuery_ADO ssql
            
            rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos
    
End Sub


Sub PreencheTabelaTemporaria_Diario()
    Dim ssql As String
    Dim rsRecibos As New ADODB.recordset
    Dim i As Integer
    Dim ult_n_rec As String
    Dim ult_estado_rec As String
    
    ApagaTabelaCrystal
    

    
    If gPassaRecFactus <> mediSim Then
        'TABELA DE RECIBOS
        ssql = " SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        ssql = ssql & " x1.n_rec num_doc , x1.cod_efr, x2.descr_efr, x1.user_emi, "
        ssql = ssql & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.num_analises,"
        ssql = ssql & " x5.nome_ute, x1.venda_dinheiro,x4.dt_cri dt_emi, x7.cod_facturavel, x1.total_pagar, x1.estado,x1.flg_doc_caixa, 'POS' tipo, "
        ssql = ssql & " x7.flg_facturado,x1.dt_pagamento,x1.desconto, x1.cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes "
        ssql = ssql & ", x1.nao_conformidade, x8.descr_isencao, x9.descr_forma_pag, x1.flg_borla, x9.descr_ana, x1.dt_anul "
        ssql = ssql & " FROM sl_recibos x1 LEFT OUTER JOIN sl_forma_pag x9 ON x1.cod_forma_pag = x9.cod_forma_pag "
        ssql = ssql & " LEFT OUTER JOIN sl_idutilizador x3 ON x1.user_emi = x3.cod_utilizador, "
        ssql = ssql & " sl_requis x4 LEFT OUTER JOIN sl_cod_salas x6 ON  x4.cod_Sala = x6.cod_Sala "
        ssql = ssql & " LEFT OUTER JOIN sl_proven proven  ON  x4.cod_proven = proven.cod_proven, "
        ssql = ssql & " sl_efr x2, sl_identif x5,   sl_recibos_det x7, sl_isencao x8, slv_analises_factus x9 "
        ssql = ssql & " WHERE x1.cod_efr = x2.cod_efr  and x1.n_req = x4.n_req and"
        ssql = ssql & " x4.seq_utente = x5.seq_utente "
        ssql = ssql & " AND x1.n_req = x7.n_req AND x1.n_rec = x7.n_rec AND x1.serie_rec = x7.serie_rec  AND x1.cod_efr = x7.cod_efr AND (x1.flg_doc_caixa = 0 OR x1.flg_doc_caixa is null) "
        ssql = ssql & " AND ( x4.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        ssql = ssql & " AND x8.cod_isencao = x7.isencao "
        ssql = ssql & " AND x7.cod_facturavel = x9.cod_ana "
        ssql = ssql & RetornaClausulaWhere
        
        'TABELA DE ADIANTAMENTOS
        ssql = ssql & " UNION SELECT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x1.n_req, "
        ssql = ssql & " x1.n_adiantamento num_doc , x4.cod_efr, x2.descr_efr, x1.user_emi, "
        ssql = ssql & " x3.Nome , x4.cod_Sala, x6.descr_sala, 0 num_analises,"
        ssql = ssql & " x5.nome_ute, null venda_dinheiro, x4.dt_cri dt_emi, null cod_facturavel, x1.valor total_pagar, x1.estado , NULL flg_doc_caixa, 'A' tipo, "
        ssql = ssql & " 0 flg_facturado, x1.dt_emi dt_pagamento, null desconto, null cod_forma_pag, to_char(x4.dt_cri, 'mm-yyyy') mes  "
        ssql = ssql & ", null nao_conformidade , x8.descr_isencao, null descr_forma_pag, null flg_borla, null descr_ana, x1.dt_anul"
        ssql = ssql & " FROM sl_adiantamentos x1 LEFT OUTER JOIN  sl_idutilizador x3 ON x1.user_emi = x3.cod_utilizador, "
        ssql = ssql & " sl_efr x2,   sl_requis x4 LEFT OUTER JOIN sl_cod_salas x6 ON  x4.cod_Sala = x6.cod_Sala "
        ssql = ssql & " LEFT OUTER JOIN sl_proven proven  ON  x4.cod_proven = proven.cod_proven, "
        ssql = ssql & " sl_identif x5, sl_isencao x8 "
        ssql = ssql & " WHERE x4.cod_efr = x2.cod_efr and x1.n_req = x4.n_req and"
        ssql = ssql & " x4.seq_utente = x5.seq_utente "
        ssql = ssql & " AND ( x4.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
        ssql = ssql & " AND x8.cod_isencao = x4.cod_isencao "
        ssql = ssql & RetornaClausulaWhere
    Else
        ssql = " SELECT DISTINCT " & BL_TrataStringParaBD(gComputador) & "  nome_computador, x4.n_req, "
        ssql = ssql & " x1.n_fac num_doc , x1.cod_efr, x2.descr_efr, x1.user_cri user_emi, "
        ssql = ssql & " x3.Nome , x4.cod_Sala, x6.descr_sala, x1.qtd,"
        ssql = ssql & " x5.nome_ute, null venda_dinheiro,x1.dt_emiss_fac dt_emi, null cod_facturavel, x1.val_total taxa, x1.flg_estado estado, null flg_doc_caixa, null flg_facturado, "
        ssql = ssql & "  x1.dt_emiss_fac dt_pagamento, 0 desconto, x1.cod_frm_pag cod_forma_pag, x1.mes  "
        ssql = ssql & ", null nao_conformidade, x4.estado_req "
        ssql = ssql & " FROM fa_fact x1 join sl_efr x2 ON x1.cod_efr = x2.cod_efr"
        ssql = ssql & " JOIN sl_idutilizador x3 on x1.user_cri = x3.cod_utilizador"
        ssql = ssql & " JOIN fa_fact x8 on x1.serie_fac = x8.serie_fac and x1.n_fac = x8.n_fac "
        ssql = ssql & " JOIN sl_requis x4 on x8.episodio = x4.n_req"
        ssql = ssql & " JOIN sl_identif x5 on x4.seq_utente = x5.seq_utente"
        ssql = ssql & " LEFT OUTER JOIN sl_cod_salas x6 on x4.cod_sala = x6.cod_sala"
        ssql = ssql & " JOIN fa_lin_fact x7 on x1.serie_fac = x7.serie_fac and x1.n_fac = x7.n_fac"
        ssql = ssql & " LEFT OUTER JOIN sl_proven proven on x4.cod_proven = proven.cod_proven "
        ssql = ssql & " JOIN slv_documentos_doente x9 on x1.serie_fac = x9.serie_doc and x1.n_fac = x9.n_doc "
        ssql = ssql & " WHERE   x9.dt_pagamento is null AND x1.dt_emiss_fac BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        ssql = ssql & RetornaClausulaWhere
    End If
    
    


    If gSGBD = gOracle Then
        ssql = ssql & " ORDER BY dt_emi ASC, n_req ASC, num_doc ASC, tipo DESC"
    ElseIf gSGBD = gSqlServer Then
        ssql = ssql & " ORDER BY x1.dt_emi ASC, x1.n_req ASC, x1.n_rec ASC, tipo DESC"
    End If
    Set rsRecibos = BG_ExecutaSELECT(ssql)
    
    If rsRecibos.RecordCount > 0 Then
        ult_n_rec = ""
        ult_estado_rec = ""
        While Not rsRecibos.EOF
            ssql = "INSERT INTO SL_CR_LISTAGEM_CAIXA (NOME_COMPUTADOR, NUM_SESSAO, N_REQ ,N_REC ,COD_ENTIDADE, DESCR_ENTIDADE "
            ssql = ssql & " ,COD_USER_EMI,DESCR_USER_EMI,COD_SALA,DESCR_SALA,NUM_ANALISES,VALOR ,NOME, VENDA_DINHEIRO,"
            ssql = ssql & " DATA, DOCUMENTO, DESCR_ANALISE,FLG_FACTURADO, DATA_REC, DESCONTO,COD_FORMA_PAG, MES, nao_conformidade,"
            ssql = ssql & "  descr_forma_pag, descr_isencao)  VALUES("
                    
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_computador, "")) & ", "
            ssql = ssql & BL_HandleNull(gNumeroSessao, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!n_req, -1) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!num_doc, -1) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_efr, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!user_emi, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_sala, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_sala, "")) & ", "
            ssql = ssql & "1, "
            
            If ult_n_rec <> BL_HandleNull(rsRecibos!num_doc, -1) Or ult_estado_rec <> rsRecibos!Estado Then
                ssql = ssql & Replace(BL_HandleNull(rsRecibos!total_pagar, 0), ",", ".") & ", "
            Else
                ssql = ssql & " 0, "
            End If
            ult_n_rec = BL_HandleNull(rsRecibos!num_doc, -1)
            ult_estado_rec = rsRecibos!Estado
            
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!nome_ute, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!venda_dinheiro, "")) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_emi, "")) & ", "
                        
            'TIPO DE DOCUMENTO
            If BL_HandleNull(rsRecibos!tipo, "") = "A" And BL_HandleNull(rsRecibos!dt_anul, "") = "" Then
                ssql = ssql & BL_TrataStringParaBD("ADIANTAMENTO") & ", "
            ElseIf BL_HandleNull(rsRecibos!tipo, "") = "A" And BL_HandleNull(rsRecibos!dt_anul, "") <> "" Then
                ssql = ssql & BL_TrataStringParaBD("ADIANT. ANULADO") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And rsRecibos!Estado = gEstadoReciboPago Then
                ssql = ssql & BL_TrataStringParaBD("RECIBO") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And rsRecibos!Estado = gEstadoReciboCobranca Then
                ssql = ssql & BL_TrataStringParaBD("COBRAN�A") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And rsRecibos!Estado <> gEstadoReciboAnulado Then
                ssql = ssql & BL_TrataStringParaBD("DOC. CAIXA") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 0 And rsRecibos!Estado = gEstadoReciboAnulado Then
                ssql = ssql & BL_TrataStringParaBD("REC ANULADO") & ", "
            ElseIf BL_HandleNull(rsRecibos!flg_doc_caixa, 0) = 1 And rsRecibos!Estado = gEstadoReciboAnulado Then
                ssql = ssql & BL_TrataStringParaBD("DOC. CAIXA ANULADO") & ", "
            ElseIf rsRecibos!Estado = gEstadoReciboNaoEmitido Or rsRecibos!Estado = gEstadoReciboNulo Or rsRecibos!Estado = gEstadoReciboPerdido Then
                ssql = ssql & BL_TrataStringParaBD("DIVIDA") & ", "
            End If
            
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_ana, "")) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!Flg_Facturado, 0) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsRecibos!dt_pagamento, "")) & ", "
            ssql = ssql & Replace(BL_HandleNull(rsRecibos!Desconto, 0), ",", ".") & ","
            ssql = ssql & BL_HandleNull(rsRecibos!cod_forma_pag, 0) & ","
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!mes, "")) & ", "
            ssql = ssql & BL_HandleNull(rsRecibos!nao_conformidade, 0) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_forma_pag, "")) & ", "
            
            If BL_HandleNull(rsRecibos!flg_borla, 0) = 1 Then
                ssql = ssql & BL_TrataStringParaBD(BL_SelCodigo("SL_ISENCAO", "DESCR_ISENCAO", "COD_ISENCAO", gTipoIsencaoBorla))
            Else
                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!descr_isencao, ""))
            End If
            ssql = ssql & ")"
            
            BG_ExecutaQuery_ADO ssql
            
            rsRecibos.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsRecibos
    
End Sub

Private Sub BtReportLista_Click()
    gImprimirDestino = 0
    Call Preenche_Estatistica
End Sub




Private Function CalculaValorDesconto(taxa As String, Desconto As String) As String
    CalculaValorDesconto = (taxa * (100 - Desconto)) / 100
End Function
Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaProven.ListCount > 0 Then     'Delete
        If EcListaProven.ListIndex > mediComboValorNull Then
            EcListaProven.RemoveItem (EcListaProven.ListIndex)
        End If
    End If
End Sub


Private Function RetornaClausulaWhere() As String
     Dim sSqlC As String
    Dim i As Integer
    On Error GoTo TrataErro
    sSqlC = ""
    'sSqlC = sSqlC & " AND (x1.flg_borla IS NULL OR x1.flG_borla = 0) "
    
    ' ------------------------------------------------------------- ----------------
    ' ENTIDADE PREENCHIDA
    ' ------------------------------------------------------------------------------
    If ListaEntidades.ListCount > 0 Then
       'edgar.parada LJMANSO-288 18.04.2019
       If gPassaRecFactus = mediSim And BG_DaComboSel(CbListagem) = lMargensPostos Then
          sSqlC = sSqlC & " AND x5.seq_efr IN ("
       Else
          sSqlC = sSqlC & " AND x2.seq_efr IN ("
       End If
       '
       For i = 0 To ListaEntidades.ListCount - 1
         sSqlC = sSqlC & ListaEntidades.ItemData(i) & ", "
       Next i
       sSqlC = Mid(sSqlC, 1, Len(sSqlC) - 2) & ") "
       
    End If
    
    ' ------------------------------------------------------------------------------
    ' POSTOS PREENCHIDA
    ' ------------------------------------------------------------------------------
    If ListaPostos.ListCount > 0 Then
        'edgar.parada LJMANSO-288 18.04.2019
        If gPassaRecFactus = mediSim And BG_DaComboSel(CbListagem) = lMargensPostos Then
          sSqlC = sSqlC & " AND x4.seq_sala IN ("
          
        Else
          sSqlC = sSqlC & " AND x6.seq_sala IN ("
        End If
        
        For i = 0 To ListaPostos.ListCount - 1
            sSqlC = sSqlC & ListaPostos.ItemData(i) & ", "
        Next i
        sSqlC = Mid(sSqlC, 1, Len(sSqlC) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' OPERADOR PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gPassaRecFactus <> mediSim Then
        If (CbOperador.ListIndex <> -1) Then
            If gPassaRecFactus = mediSim And BG_DaComboSel(CbListagem) = lMargensPostos Then
                sSqlC = sSqlC & " AND x1.user_cri=" & BL_TrataStringParaBD(BG_DaComboSel(CbOperador))
            Else
                sSqlC = sSqlC & " AND x1.user_emi=" & BL_TrataStringParaBD(BG_DaComboSel(CbOperador))
            End If
        End If
    End If
        
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If (EcCodLocal.text <> "") Then
        'edgar.parada LJMANSO-288 18.04.2019
        If gPassaRecFactus = mediSim And BG_DaComboSel(CbListagem) = lMargensPostos Then
          sSqlC = sSqlC & " AND x3.cod_local=" & EcCodLocal.text
        Else
          sSqlC = sSqlC & " AND x4.cod_local=" & EcCodLocal.text
        End If
    End If
    
    ' ------------------------------------------------------------------------------
    ' ESTADO DE FACTURACAO
    ' ------------------------------------------------------------------------------
    If gPassaRecFactus <> mediSim Then
        If CbRequis.ListIndex <> mediComboValorNull Then
            If (CbRequis.ItemData(CbRequis.ListIndex) = gEstadoReqFactFacturada) Then
                sSqlC = sSqlC & " AND x7.flg_facturado = 1 "
            ElseIf (CbRequis.ItemData(CbRequis.ListIndex) = gEstadoReqFactNaoFacturada) Then
                sSqlC = sSqlC & " AND (x7.flg_facturado = 0 or x7.flg_facturado IS NULL or x7.flg_facturado = 2) "
            End If
        End If
    End If

    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaProven.ListCount > 0 Then
        'edgar.parada LJMANSO-288 18.04.2019
        If gPassaRecFactus = mediSim And BG_DaComboSel(CbListagem) = lMargensPostos Then
           sSqlC = sSqlC & " AND x7.seq_proven  IN ("
        Else
           sSqlC = sSqlC & " AND proven.seq_proven  IN ("
        End If
        
        For i = 0 To EcListaProven.ListCount - 1
            sSqlC = sSqlC & EcListaProven.ItemData(i) & ", "
        Next i
        sSqlC = Mid(sSqlC, 1, Len(sSqlC) - 2) & ") "
    End If
    RetornaClausulaWhere = sSqlC
Exit Function
TrataErro:
    RetornaClausulaWhere = ""
    BG_LogFile_Erros "Erro ao construir WHERE: " & Err.Description, Me.Name, RetornaClausulaWhere, True
    Exit Function
    Resume Next
End Function

Private Sub ApagaTabelaCrystal()
    Dim ssql As String
    ssql = "DELETE FROM SL_CR_LISTAGEM_CAIXA WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO ssql
    BG_Trata_BDErro

End Sub


Private Sub PreencheEntidades(cod_efr As String)
    Dim ssql As String
    Dim RsEFR As New ADODB.recordset
    Dim iAberta As Integer
    Dim i As Integer
    Dim incr As Double
    On Error GoTo TrataErro
    iAberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iAberta = mediSim Then
    For i = 1 To totalEntidades
        If entidades(i).cod_efr = cod_efr Then
            Exit Sub
        End If
    Next
    
    ssql = "SELECT * FROM fa_efr  WHERE cod_efr = " & cod_efr
    RsEFR.CursorType = adOpenStatic
    RsEFR.CursorLocation = adUseServer
    RsEFR.Open ssql, gConexaoSecundaria
    While Not RsEFR.EOF
        
        totalEntidades = totalEntidades + 1
        ReDim Preserve entidades(totalEntidades)
        entidades(totalEntidades).cod_efr = BL_HandleNull(RsEFR!cod_efr, "")
        entidades(totalEntidades).descr_efr = BL_HandleNull(RsEFR!descr_efr, "")
        entidades(totalEntidades).tab_utilizada = BL_HandleNull(RsEFR!tab_utilizada, "")
        entidades(totalEntidades).flg_usa_preco_ent = BL_HandleNull(RsEFR!flg_usa_preco_ent, 0)
        If BL_HandleNull(RsEFR!saida_mapa, 0) = 4 Then
            entidades(totalEntidades).flg_ars = mediSim
        Else
            entidades(totalEntidades).flg_ars = mediNao
        End If
        
        entidades(totalEntidades).totalPortarias = 0
        ReDim entidades(totalEntidades).portarias(entidades(totalEntidades).totalPortarias)
        PreenchePortarias totalEntidades
        RsEFR.MoveNext
    Wend
    RsEFR.Close
    Set RsEFR = Nothing
    ProgressBar.Visible = False
    End If
Exit Sub
TrataErro:
    Call BG_LogFile_Erros("Erro PreencheEntidades -> " & Err.Description & ssql, Me.Name, "PreencheEntidades", True)
    Exit Sub
    Resume Next
End Sub


Private Sub PreenchePortarias(linha As Integer)
    Dim ssql As String
    Dim rsTab As New ADODB.recordset
    Dim rsPort As New ADODB.recordset
    On Error GoTo TrataErro
    ssql = "SELECT * FROM fa_portarias WHERE cod_efr = " & entidades(linha).tab_utilizada
    ssql = ssql & " AND dt_ini >= " & BL_TrataStringParaBD(EcDtInicio.value)
    ssql = ssql & " AND dt_ini < " & BL_TrataStringParaBD(EcDtFim.value)
    ssql = ssql & " AND (dt_fim IS NULL "
    ssql = ssql & " OR (dt_fim < " & BL_TrataStringParaBD(EcDtFim.value)
    ssql = ssql & " AND dt_fim >= " & BL_TrataStringParaBD(EcDtInicio.value) & "))"
    ssql = ssql & " ORDER BY dt_ini ASC "
    
    rsPort.CursorType = adOpenStatic
    rsPort.CursorLocation = adUseServer
    rsPort.Open ssql, gConexaoSecundaria
    While Not rsPort.EOF
        entidades(linha).totalPortarias = entidades(linha).totalPortarias + 1
        ReDim Preserve entidades(linha).portarias(entidades(linha).totalPortarias)
        entidades(linha).portarias(entidades(linha).totalPortarias).cod_portaria = BL_HandleNull(rsPort!Portaria, "")
        entidades(linha).portarias(entidades(linha).totalPortarias).dt_fim = BL_HandleNull(rsPort!dt_fim, "")
        entidades(linha).portarias(entidades(linha).totalPortarias).dt_ini = BL_HandleNull(rsPort!dt_ini, "")
        entidades(linha).portarias(entidades(linha).totalPortarias).totalPrecos = 0
        ReDim entidades(linha).portarias(entidades(linha).totalPortarias).precos(entidades(linha).portarias(entidades(linha).totalPortarias).totalPrecos)
        PreenchePrecos linha, entidades(linha).totalPortarias
        rsPort.MoveNext
    Wend
    rsPort.Close
    Set rsPort = Nothing
    
Exit Sub
TrataErro:
    Call BG_LogFile_Erros("Erro PreenchePortarias -> " & Err.Description & ssql, Me.Name, "PreenchePortarias", True)
    Exit Sub
    Resume Next
End Sub

Private Sub PreenchePrecos(iEfr As Integer, iPortaria As Integer)
    Dim ssql As String
    Dim rsRubr As New ADODB.recordset
    On Error GoTo TrataErro
    ssql = "SELECT * FROM fa_pr_rubr WHERE portaria = " & entidades(iEfr).portarias(iPortaria).cod_portaria
    rsRubr.CursorType = adOpenStatic
    rsRubr.CursorLocation = adUseServer
    rsRubr.Open ssql, gConexaoSecundaria
    While Not rsRubr.EOF
        entidades(iEfr).portarias(iPortaria).totalPrecos = entidades(iEfr).portarias(iPortaria).totalPrecos + 1
        ReDim Preserve entidades(iEfr).portarias(iPortaria).precos(entidades(iEfr).portarias(iPortaria).totalPrecos)
        entidades(iEfr).portarias(iPortaria).precos(entidades(iEfr).portarias(iPortaria).totalPrecos).cod_rubr = BL_HandleNull(rsRubr!cod_rubr, "")
        rsRubr.MoveNext
    Wend
    rsRubr.Close
    Set rsRubr = Nothing
    
Exit Sub
TrataErro:
    Call BG_LogFile_Erros("Erro PreenchePrecos -> " & Err.Description & ssql, Me.Name, "PreenchePrecos", True)
    Exit Sub
    Resume Next
End Sub


Private Sub EcCodlocal_Validate(cancel As Boolean)
    cancel = PA_ValidateLocal(EcCodLocal, EcDescrLocal, "")
End Sub
Private Sub BtPesqLocal_Click()
    PA_PesquisaLocal EcCodLocal, EcDescrLocal
End Sub



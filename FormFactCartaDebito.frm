VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormFactCartaDebito 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactCartaDebito"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   8070
   Icon            =   "FormFactCartaDebito.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6360
   ScaleWidth      =   8070
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtImprimir 
      Height          =   615
      Left            =   6840
      Picture         =   "FormFactCartaDebito.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Imprimir"
      Top             =   5520
      Width           =   975
   End
   Begin VB.CheckBox CkDataPagVencida 
      Caption         =   "Data de Pagamento Vencida"
      Height          =   255
      Left            =   4800
      TabIndex        =   5
      Top             =   1320
      Width           =   2415
   End
   Begin VB.CheckBox CkApenasNovas 
      Caption         =   "Apenas Novas Cartas"
      Height          =   255
      Left            =   1440
      TabIndex        =   4
      Top             =   1320
      Width           =   2055
   End
   Begin MSComctlLib.TreeView Tv 
      Height          =   3735
      Left            =   240
      TabIndex        =   3
      Top             =   1680
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   6588
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox EcListaEFR 
      Appearance      =   0  'Flat
      Height          =   1005
      Left            =   1440
      TabIndex        =   0
      Top             =   240
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaEFR 
      Height          =   315
      Left            =   7200
      Picture         =   "FormFactCartaDebito.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   240
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "Entidades Finac."
      Height          =   255
      Index           =   5
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "FormFactCartaDebito"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim indice As Integer
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.
Private Type n_cred
    seq_carta_debito_cred As Long
    Serie_nota_cred As String
    nota_cred As String
    dt_emiss_nota_cred As String
    valor_cred As Double
    indice As Integer
End Type
Private Type fatura
    seq_carta_debito_det As Long
    serie_fac As String
    n_fac As String
    dt_emissao As String
    valor As Double
    indice As Integer
    notaCred() As n_cred
    totalNotaCred As Integer
End Type
Private Type Entidade
    seq_carta_debito As Long
    cod_efr As String
    descr_efr As String
    morada As String
    cod_postal As String
    descr_postal As String
    indice As Integer
    nr_dias_venc As Integer
    faturas() As fatura
    totalFaturas As Integer
End Type
Dim entidades() As Entidade
Dim totalEntidades As Integer
Public rs As ADODB.recordset


Private Sub BtImprimir_Click()
    Dim iEnt As Integer
    Dim iFac As Integer
    Dim iNCred As Integer
    Dim sSql As String
    Dim rsSeq As New ADODB.recordset
    Dim Report As CrystalReport
    Dim continua As Boolean
    
    For iEnt = 1 To totalEntidades
        continua = BL_IniciaReport("CartaDivida", "Emiss�o de Carta Divida", crptToPrinter)
        If continua = False Then Exit Sub
        
        If entidades(iEnt).seq_carta_debito = mediComboValorNull Then
            sSql = "SELECT seq_carta_debito.nextval  proximo FROM dual"
            rsSeq.CursorType = adOpenStatic
            rsSeq.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsSeq.Open sSql, gConexao
            If rsSeq.RecordCount = 1 Then
                entidades(iEnt).seq_carta_debito = BL_HandleNull(rsSeq!proximo, mediComboValorNull)
            End If
            rsSeq.Close
            Set rsSeq = Nothing
            sSql = "INSERT INTO sl_fact_carta_debito(SEQ_CARTA_DEBITO, COD_EFR, DESCR_EFR, MORADA, COD_POSTAL, "
            sSql = sSql & " DESCR_POSTAL, NR_DIAS_VENC, DT_CRI, USER_CRI) VALUES ("
            sSql = sSql & entidades(iEnt).seq_carta_debito & ","
            sSql = sSql & entidades(iEnt).cod_efr & ","
            sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).descr_efr) & ","
            sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).morada) & ","
            sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).cod_postal) & ","
            sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).descr_postal) & ","
            sSql = sSql & entidades(iEnt).nr_dias_venc & ","
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
            BG_ExecutaQuery_ADO sSql
        
            For iFac = 1 To entidades(iEnt).totalFaturas
                sSql = "SELECT seq_carta_debito_det.nextval  proximo FROM dual"
                rsSeq.CursorType = adOpenStatic
                rsSeq.CursorLocation = adUseServer
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsSeq.Open sSql, gConexao
                If rsSeq.RecordCount = 1 Then
                    entidades(iEnt).faturas(iFac).seq_carta_debito_det = BL_HandleNull(rsSeq!proximo, mediComboValorNull)
                End If
                rsSeq.Close
                Set rsSeq = Nothing
                sSql = "INSERT INTO sl_fact_carta_debito_det(SEQ_CARTA_DEBITO_DET, SEQ_CARTA_DEBITO, SERIE_FAC, N_FAC, "
                sSql = sSql & " DT_EMISSAO, VALOR_TOTAL ) VALUES("
                sSql = sSql & entidades(iEnt).faturas(iFac).seq_carta_debito_det & ","
                sSql = sSql & entidades(iEnt).seq_carta_debito & ","
                sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).faturas(iFac).serie_fac) & ","
                sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).faturas(iFac).n_fac) & ","
                sSql = sSql & BL_TrataDataParaBD(entidades(iEnt).faturas(iFac).dt_emissao) & ","
                sSql = sSql & Replace(entidades(iEnt).faturas(iFac).valor, ",", ".") & ")"
                BG_ExecutaQuery_ADO sSql
                
                For iNCred = 1 To entidades(iEnt).faturas(iFac).totalNotaCred
                    sSql = "SELECT seq_carta_debito_cred.nextval  proximo FROM dual"
                    rsSeq.CursorType = adOpenStatic
                    rsSeq.CursorLocation = adUseServer
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    rsSeq.Open sSql, gConexao
                    If rsSeq.RecordCount = 1 Then
                        entidades(iEnt).faturas(iFac).notaCred(iNCred).seq_carta_debito_cred = BL_HandleNull(rsSeq!proximo, mediComboValorNull)
                    End If
                    rsSeq.Close
                    Set rsSeq = Nothing
                    sSql = "INSERT INTO sl_fact_carta_debito_cred(SEQ_CARTA_DEBITO_CRED, SEQ_CARTA_DEBITO_DET, SERIE_NOTA_CRED,"
                    sSql = sSql & "NOTA_CRED, DT_EMISS_NOTA_CRED, VALOR_ACERTO) VALUES ("
                    sSql = sSql & entidades(iEnt).faturas(iFac).notaCred(iNCred).seq_carta_debito_cred & ","
                    sSql = sSql & entidades(iEnt).faturas(iFac).seq_carta_debito_det & ","
                    sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).faturas(iFac).notaCred(iNCred).Serie_nota_cred) & ","
                    sSql = sSql & BL_TrataStringParaBD(entidades(iEnt).faturas(iFac).notaCred(iNCred).nota_cred) & ","
                    sSql = sSql & BL_TrataDataParaBD(entidades(iEnt).faturas(iFac).notaCred(iNCred).dt_emiss_nota_cred) & ","
                    sSql = sSql & Replace(entidades(iEnt).faturas(iFac).notaCred(iNCred).valor_cred, ",", ".") & ")"
                    BG_ExecutaQuery_ADO sSql
                Next iNCred
            Next iFac
        End If
        
        Set Report = forms(0).Controls("Report")
        Report.SQLQuery = "SELECT sl_fact_carta_debito.SEQ_CARTA_DEBITO, sl_fact_carta_debito.COD_EFR, sl_fact_carta_debito.DESCR_EFR, sl_fact_carta_debito.MORADA, sl_fact_carta_debito.COD_POSTAL, sl_fact_carta_debito.DESCR_POSTAL, sl_fact_carta_debito.NR_DIAS_VENC, "
        Report.SQLQuery = Report.SQLQuery & " sl_fact_carta_debito_det.seq_carta_debito_det , sl_fact_carta_debito_det.serie_fac, sl_fact_carta_debito_det.n_fac, sl_fact_carta_debito_det.dt_emissao, sl_fact_carta_debito_det.VALOR_TOTAL, "
        Report.SQLQuery = Report.SQLQuery & " sl_fact_carta_debito_cred.seq_carta_debito_cred , sl_fact_carta_debito_cred.serie_nota_cred, sl_fact_carta_debito_cred.nota_cred, sl_fact_carta_debito_cred.dt_emiss_nota_cred, sl_fact_carta_debito_cred.VALOR_ACERTO"
        Report.SQLQuery = Report.SQLQuery & " FROM sl_fact_carta_debito , sl_fact_carta_debito_det LEFT OUTER JOIN sl_fact_carta_debito_cred   ON "
        Report.SQLQuery = Report.SQLQuery & " sl_fact_carta_debito_det.seq_carta_debito_det = sl_fact_carta_debito_cred.seq_carta_debito_det"
        Report.SQLQuery = Report.SQLQuery & " WHERE sl_fact_carta_debito.seq_carta_debito = sl_fact_carta_debito_det.seq_carta_debito "
        Report.SQLQuery = Report.SQLQuery & " AND  sl_fact_carta_debito.seq_carta_debito= " & entidades(iEnt).seq_carta_debito
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_fact_carta_debito.seq_carta_debito ASC, sl_fact_carta_debito_det.seq_carta_debito_det, sl_fact_carta_debito_cred.seq_carta_debito_cred"
            
        'Report.SubreportToChange = ""
        Call BL_ExecutaReport
    Next


End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Emiss�o de Cartas de D�vida"
    Me.left = 5
    Me.top = 5
    Me.Width = 8160
    Me.Height = 6795 ' Normal
    'Me.Height = 8330 ' Campos Extras


End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    EcListaEFR.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormFactCartaDebito = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    CkApenasNovas.value = vbUnchecked
    CkDataPagVencida.value = vbUnchecked
    EcListaEFR.Clear
    totalEntidades = 0
    ReDim entidades(0)
    Tv.Nodes.Clear
    indice = 0
End Sub

Sub DefTipoCampos()
    

End Sub

Sub PreencheCampos()
    On Error GoTo TrataErro
    Me.SetFocus
    Dim root As Node
    Dim texto As String
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
    Tv.Visible = False
    DoEvents
    Tv.LineStyle = tvwRootLines
        While Not rs.EOF
            If entidades(totalEntidades).cod_efr <> BL_HandleNull(rs!cod_efr, "") Then
                totalEntidades = totalEntidades + 1
                ReDim Preserve entidades(totalEntidades)
                entidades(totalEntidades).cod_efr = BL_HandleNull(rs!cod_efr, "")
                entidades(totalEntidades).cod_postal = BL_HandleNull(rs!cod_postal, "")
                entidades(totalEntidades).descr_efr = BL_HandleNull(rs!descr_efr, "")
                entidades(totalEntidades).descr_postal = BL_HandleNull(rs!descr_postal, "")
                entidades(totalEntidades).morada = BL_HandleNull(rs!mor_efr, "")
                entidades(totalEntidades).nr_dias_venc = BL_HandleNull(rs!nr_dias_venc, 0)
                ReDim entidades(totalEntidades).faturas(0)
                entidades(totalEntidades).totalFaturas = 0
                entidades(totalEntidades).seq_carta_debito = mediComboValorNull
                indice = indice + 1
                entidades(totalEntidades).indice = indice
                Set root = Tv.Nodes.Add(, tvwChild, "E" & entidades(totalEntidades).cod_efr, entidades(totalEntidades).cod_efr & " - " & entidades(totalEntidades).descr_efr)
                root.Expanded = False
            End If
            entidades(totalEntidades).totalFaturas = entidades(totalEntidades).totalFaturas + 1
            ReDim Preserve entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas)
            entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).dt_emissao = BL_HandleNull(rs!dt_emiss_fac, "")
            entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).n_fac = BL_HandleNull(rs!n_fac, "")
            entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).seq_carta_debito_det = mediComboValorNull
            entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).serie_fac = BL_HandleNull(rs!serie_fac, "")
            If BL_HandleNull(rs!val_pend_pag, 0) > 0 Then
                entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).valor = BL_HandleNull(rs!val_pend_pag, 0)
            Else
                entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).valor = BL_HandleNull(rs!val_total, "")
            End If
            entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).totalNotaCred = 0
            ReDim entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).notaCred(0)
            indice = indice + 1
            entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).indice = indice
            texto = entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).serie_fac & "/" & entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).n_fac
            texto = texto & Space(25 - Len(texto)) & entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).dt_emissao & "    " & entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).valor & "�"
            Set root = Tv.Nodes.Add(entidades(totalEntidades).indice, tvwChild, "F" & entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).serie_fac & entidades(totalEntidades).faturas(entidades(totalEntidades).totalFaturas).n_fac, texto)
            root.Expanded = False
            PreencheNotasCred totalEntidades, entidades(totalEntidades).totalFaturas
            rs.MoveNext
        Wend
    End If
    Tv.Visible = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCampos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCampos"
    Exit Sub
    Resume Next
End Sub


Sub FuncaoLimpar()
    
        LimpaCampos
        EcListaEFR.SetFocus
End Sub





Sub FuncaoProcurar()
    On Error GoTo TrataErro
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim CriterioTabela As String
    Dim i As Integer
    totalEntidades = 0
    ReDim entidades(0)
    indice = 0
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    'BRUNODSANTOS 07.02.2017 - Cedivet-201
    CriterioTabela = "SELECT 'F' tipo, x1.cod_efr, x2.descr_efr, x1.serie_fac, x1.n_fac, x1.dt_emiss_fac, x2.mor_efr, x2.cod_postal, x4.descr_postal, x1.val_total, x2.nr_dias_venc, X1.val_pend_pag "
    CriterioTabela = CriterioTabela & " FROM fa_fact x1, fa_efr x2, fa_cod_postal x4 WHERE   x1.cod_efr = x2.cod_efr AND x4.cod_postal = x2.cod_postal AND flg_estado = 'F'"
    '
        
    'entidades selecionadas
    If EcListaEFR.ListCount > 0 Then
        CriterioTabela = CriterioTabela & " AND x2.seq_efr  IN ("
        For i = 0 To EcListaEFR.ListCount - 1
            CriterioTabela = CriterioTabela & EcListaEFR.ItemData(i) & ", "
        Next i
        CriterioTabela = Mid(CriterioTabela, 1, Len(CriterioTabela) - 2) & ")"
    End If
    
    If CkApenasNovas.value = vbChecked Then
        CriterioTabela = CriterioTabela & " AND (serie_fac, n_fac) NOT IN (Select x3.serie_fac, x3.n_fac FROM sl_fact_carta_debito_det x3)"
    End If
    If CkDataPagVencida.value = vbChecked Then
        CriterioTabela = CriterioTabela & " AND x2.nr_dias_venc IS NOT NULL and x1.dt_emiss_fac + x2.nr_dias_venc < " & BL_TrataDataParaBD(Bg_DaData_ADO)
    End If
    
    'NOTAS DE DEBITO
    'BRUNODSANTOS 07.02.2017- Cedivet-201
    CriterioTabela = CriterioTabela & " UNION SELECT 'ND' tipo, x3.cod_efr, x2.descr_efr, x1.serie_nota_deb serie_fac, x1.n_nota_deb n_fac, x1.dt_emiss_nota_deb dt_emiss_fac, x2.mor_efr, x5.cod_postal, x5.descr_postal, x1.val_total, x2.nr_dias_venc, X1.val_pend_pag "
    CriterioTabela = CriterioTabela & " FROM fa_nota_deb x1, fa_efr x2, fa_fact x3, fa_cod_postal x5 WHERE x1.serie_fac = x3.serie_fac AND x1.n_fac = x3.n_fac AND x3.cod_efr = x2.cod_efr AND x5.cod_postal = x2.cod_postal AND x1.flg_doc_pag IS NULL"
    'entidades selecionadas
    If EcListaEFR.ListCount > 0 Then
        CriterioTabela = CriterioTabela & " AND x2.seq_efr  IN ("
        For i = 0 To EcListaEFR.ListCount - 1
            CriterioTabela = CriterioTabela & EcListaEFR.ItemData(i) & ", "
        Next i
        CriterioTabela = Mid(CriterioTabela, 1, Len(CriterioTabela) - 2) & ")"
    End If
    
    If CkApenasNovas.value = vbChecked Then
        CriterioTabela = CriterioTabela & " AND (serie_nota_deb, n_nota_deb) NOT IN (Select x3.serie_fac, x3.n_fac FROM sl_fact_carta_debito_det x3)"
    End If
    
    If CkDataPagVencida.value = vbChecked Then
        CriterioTabela = CriterioTabela & " AND x2.nr_dias_venc IS NOT NULL and x1.dt_emiss_nota_deb + x2.nr_dias_venc < " & BL_TrataDataParaBD(Bg_DaData_ADO)
    End If

    
    CriterioTabela = CriterioTabela & "  ORDER BY cod_efr ASC, dt_emiss_fac ASC"


    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        
    Else
        estado = 2
        LimpaCampos
        
        ' Inicio do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        
        
        BL_FimProcessamento Me
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Number & " - " & Err.Description & " - " & CriterioTabela, Me.Name, "FuncaoProcurar"
    Exit Sub
    Resume Next
End Sub

Private Sub EclistaEFR_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaEFR, KeyCode, Shift
End Sub
Private Sub BtPesquisaEFR_Click()
    PA_PesquisaEFRMultiSel EcListaEFR
End Sub

Private Sub PreencheNotasCred(iEfr As Integer, iFac As Integer)
    Dim sSql As String
    Dim rsNC As New ADODB.recordset
    Dim texto As String
    Dim root As Node
    On Error GoTo TrataErro
    
    
    sSql = "SELECT * FROM fa_nota_cred WHERE serie_fac = " & BL_TrataStringParaBD(entidades(iEfr).faturas(iFac).serie_fac)
    sSql = sSql & " AND n_fac = " & entidades(iEfr).faturas(iFac).n_fac
    sSql = sSql & " ORDER by dt_emiss_nota_cred"
    rsNC.CursorType = adOpenStatic
    rsNC.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsNC.Open sSql, gConexao
    If rsNC.RecordCount > 0 Then
        While Not rsNC.EOF
            entidades(iEfr).faturas(iFac).totalNotaCred = entidades(iEfr).faturas(iFac).totalNotaCred + 1
            ReDim Preserve entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred)
            entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).dt_emiss_nota_cred = BL_HandleNull(rsNC!dt_emiss_nota_cred, "")
            entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).nota_cred = BL_HandleNull(rsNC!n_nota_cred, "")
            entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).seq_carta_debito_cred = mediComboValorNull
            entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).Serie_nota_cred = BL_HandleNull(rsNC!Serie_nota_cred, "")
            entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).valor_cred = BL_HandleNull(rsNC!val_total, "")
            indice = indice + 1
            entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).indice = indice
            texto = "NCr�dito:" & entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).Serie_nota_cred & "/" & entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).nota_cred
            texto = texto & Space(25 - Len(texto)) & entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).dt_emiss_nota_cred & "    " & entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).valor_cred & "�"
            Set root = Tv.Nodes.Add(entidades(iEfr).faturas(iFac).indice, tvwChild, "NC" & entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).Serie_nota_cred & "/" & entidades(iEfr).faturas(iFac).notaCred(entidades(iEfr).faturas(iFac).totalNotaCred).nota_cred, texto)
            root.Expanded = False
            rsNC.MoveNext
        Wend
    End If
    rsNC.Close
    Set rsNC = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotasCred: " & Err.Number & " - " & Err.Description & " - " & sSql, Me.Name, "PreencheNotasCred"
    Exit Sub
    Resume Next
End Sub

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormConfigureMessageLists 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   7995
   ClientLeft      =   120
   ClientTop       =   420
   ClientWidth     =   13185
   Icon            =   "FormConfigureMessageLists.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7995
   ScaleWidth      =   13185
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   1320
      ScaleHeight     =   315
      ScaleWidth      =   555
      TabIndex        =   23
      Top             =   6480
      Width           =   615
   End
   Begin VB.TextBox TextBoxCode 
      Height          =   285
      Left            =   6000
      TabIndex        =   21
      Top             =   6000
      Width           =   1335
   End
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   120
      TabIndex        =   17
      Top             =   0
      Width           =   6615
      Begin VB.CheckBox CkPublica 
         Caption         =   "Lista P�blica"
         Height          =   195
         Left            =   5280
         TabIndex        =   22
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox TextBoxName 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   5055
      End
      Begin VB.OptionButton OptionButtonUsers 
         Caption         =   "Utilizadores"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   840
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton OptionButtonLists 
         Caption         =   "Listas de utilizadores"
         Height          =   255
         Left            =   2040
         TabIndex        =   18
         Top             =   840
         Width           =   1815
      End
   End
   Begin VB.Frame Frame3 
      Height          =   3735
      Left            =   3480
      TabIndex        =   15
      Top             =   720
      Width           =   3255
      Begin MSComctlLib.ListView ListViewMembers 
         Height          =   3300
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   5821
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ColHdrIcons     =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.TextBox EcUtilAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2880
      TabIndex        =   14
      Top             =   6000
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcDataAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   13
      Top             =   6000
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUtilCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   12
      Top             =   6000
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcDataCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1155
      TabIndex        =   11
      Top             =   6000
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.TextBox EcHoraAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5040
      TabIndex        =   10
      Top             =   6000
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcHoraCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      TabIndex        =   9
      Top             =   6000
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Frame Frame1 
      Height          =   3735
      Left            =   120
      TabIndex        =   7
      Top             =   720
      Width           =   3255
      Begin VB.ComboBox ComboBoxUsers 
         BackColor       =   &H00FFFFFF&
         Height          =   3300
         Left            =   120
         Style           =   1  'Simple Combo
         TabIndex        =   8
         Text            =   "ComboBoxUsers"
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.Frame Frame2 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   4440
      Width           =   6615
      Begin VB.Label LaDataCri 
         Caption         =   "LaDataCri"
         Height          =   195
         Left            =   1440
         TabIndex        =   6
         Top             =   240
         Width           =   810
      End
      Begin VB.Label LaDataAct 
         Caption         =   "LaDataAct"
         Height          =   195
         Left            =   1440
         TabIndex        =   5
         Top             =   480
         Width           =   810
      End
      Begin VB.Label LaUtilAct 
         Caption         =   "LaUtilAct"
         Height          =   195
         Left            =   3120
         TabIndex        =   4
         Top             =   480
         Width           =   3255
      End
      Begin VB.Label LaUtilCri 
         Caption         =   "LaUtilCri"
         Height          =   195
         Left            =   3120
         TabIndex        =   3
         Top             =   240
         Width           =   3255
      End
      Begin VB.Label LaAlteracao 
         AutoSize        =   -1  'True
         Caption         =   "Altera��o"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   675
      End
      Begin VB.Label LaCriacao 
         AutoSize        =   -1  'True
         Caption         =   "Cria��o"
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   540
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   360
      Top             =   6480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfigureMessageLists.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfigureMessageLists.frx":0441
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormConfigureMessageLists"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'     .................................................................
'    .            Form of the application messages system              .
'   .                                                                   .
'   .                    Paulo Ferreira 2009.08.07                      .
'    .                        � 2009 GLINTT-HS                         .
'     .................................................................

Option Explicit

' Keep number of tables fields.
Private NumCampos As Integer

' Keep array with table fields names.
Private CamposBD() As String

' Keep array with the corresponding table fiedls in the form.
Private CamposEc() As Object

' Keep table binding fields.
Private TextoCamposObrigatorios() As String

' Keep table name.
Private NomeTabela As String

' Keep sql criteria.
Private CriterioTabela As String

' Keep recordset of the table
Private rsTabela As ADODB.recordset

' Keep form state.
Private estado As Integer

' Keep form active object.
Private CampoActivo As Object

' Keep form focus object.
Private CampoDeFocus As Object

Private RsConsulta As ADODB.recordset

' Initialize list view of members.
Private Sub InitializeListViewMembers()
      
    On Error GoTo ErrorHander
    ListViewMembers.View = lvwReport
    ListViewMembers.ColumnHeaders.Add , , "Tipo", 500
    ListViewMembers.ColumnHeaders.Add , , "Descri��o", 2000
    ListViewMembers.MultiSelect = False
    SetListViewColor ListViewMembers, Picture1, vbWhite, &H8000000F
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' Fill in the list view.
Private Sub FillListView()

    Dim rMembers As ADODB.recordset
    Dim sCode As String
    Dim sDescription As String
    Dim iIcon As Integer
    Dim sSql As String
    
    On Error GoTo ErrorHander
    sSql = "select * from " & gBD_PREFIXO_TAB & "mensagens_membros where cod_lista = " & rsTabela!Codigo
    Set rMembers = New ADODB.recordset
    rMembers.CursorLocation = adUseServer
    rMembers.Open sSql, gConexao, adOpenStatic
    While (Not rMembers.EOF)
        If rMembers!flg_lista = 1 Then
            sCode = "L-"
            iIcon = 1
            sDescription = CR_BL_DevolveNomeUtilizadorActual(rMembers!cod_membro)
        Else
            sCode = "U-"
            iIcon = 2
            sDescription = CR_BL_DevolveNomeUtilizadorActual(rMembers!cod_membro)
        End If
        ListViewMembers.ListItems.Add , sCode & rMembers!cod_membro, , iIcon, iIcon
        ListViewMembers.ListItems(ListViewMembers.ListItems.Count).SubItems(1) = sDescription
        rMembers.MoveNext
    Wend
    If (rMembers.state = adStateOpen) Then: rMembers.Close
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' Add list member.
Private Sub AddMember()
    
    On Error GoTo ErrorHander
    If (ComboBoxUsers.ListIndex = cr_mediComboValorNull) Then: Exit Sub
    If (OptionButtonUsers.value = True) Then
        ListViewMembers.ListItems.Add , "U-" & ComboBoxUsers.ItemData(ComboBoxUsers.ListIndex), , 2, 2
    Else
        If (ComboBoxUsers <> Empty) Then
            If (TextBoxCode = ComboBoxUsers.ItemData(ComboBoxUsers.ListIndex)) Then: BG_Mensagem cr_mediMsgBox, "N�o poder� este item a lista.", vbCritical, "...": Exit Sub
        End If
        ListViewMembers.ListItems.Add , "L-" & ComboBoxUsers.ItemData(ComboBoxUsers.ListIndex), , 1, 1
    End If
    ListViewMembers.ListItems(ListViewMembers.ListItems.Count).SubItems(1) = ComboBoxUsers.List(ComboBoxUsers.ListIndex)
    Exit Sub
    
ErrorHander:
    If (Err.Number = 35602) Then: BG_Mensagem cr_mediMsgBox, "O item j� se encontra seleccionado", vbInformation, "..."
    Exit Sub
   
End Sub

' Remove list members.
Private Sub RemoveMembers()

    On Error GoTo ErrorHander
    If (ListViewMembers.ListItems.Count > Empty) Then: ListViewMembers.ListItems.Remove ListViewMembers.SelectedItem.Index
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Private Sub ComboBoxUsers_DblClick()
  
    On Error GoTo ErrorHander
    If (OptionButtonUsers.value = True) Then: AddMember
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Private Sub ComboBoxUsers_KeyDown(KeyCode As Integer, Shift As Integer)
  
    On Error GoTo ErrorHander
    If (KeyCode = vbKeyReturn) Then: AddMember
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Private Sub ListViewMembers_KeyDown(KeyCode As Integer, Shift As Integer)
  
    On Error GoTo ErrorHander
    If (KeyCode = vbKeyDelete Or KeyCode = vbKeyBack) Then: RemoveMembers
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' OptionButtonLists_Click.
Private Sub OptionButtonLists_Click()
    
    On Error GoTo ErrorHander
    If (OptionButtonLists.value = True) Then: CR_BG_PreencheComboBD_ADO "select * from " & gBD_PREFIXO_TAB & "mensagens_listas", "codigo", "descricao", ComboBoxUsers, cr_mediAscComboDesignacao
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' OptionButtonUsers_Click.
Private Sub OptionButtonUsers_Click()
    
    On Error GoTo ErrorHander
    If (OptionButtonUsers.value = True) Then: CR_BG_PreencheComboBD_ADO gBD_PREFIXO_TAB & "utilizadores", "codigo", "nome", ComboBoxUsers, cr_mediAscComboDesignacao
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Public Sub Form_Load()
    LoadEvent
End Sub

Public Sub Form_Activate()
    ActivateEvent
End Sub

Public Sub Form_Unload(Cancel As Integer)
    UnloadEvent
End Sub

' Initializations of the form.
Private Sub Inicializacoes()

    On Error GoTo ErrorHander
    Set CampoDeFocus = TextBoxName
    Me.caption = " Listas de Utilizadores"
    Me.left = 100
    Me.top = 100
    Me.Width = 6930
    Me.Height = 5805
    NomeTabela = gBD_PREFIXO_TAB & "mensagens_listas"
    NumCampos = 9
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    CamposBD(0) = "codigo": Set CamposEc(0) = TextBoxCode
    CamposBD(1) = "descricao":  Set CamposEc(1) = TextBoxName
    CamposBD(2) = "user_cri": Set CamposEc(2) = EcUtilCri
    CamposBD(3) = "dt_cri": Set CamposEc(3) = EcDataCri
    CamposBD(4) = "hr_cri": Set CamposEc(4) = EcHoraCri
    CamposBD(5) = "user_act": Set CamposEc(5) = EcUtilAct
    CamposBD(6) = "dt_act": Set CamposEc(6) = EcDataAct
    CamposBD(7) = "hr_act": Set CamposEc(7) = EcHoraAct
    CamposBD(8) = "flg_publica": Set CamposEc(8) = CkPublica
    TextoCamposObrigatorios(1) = "Descricao"
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Private Sub LoadEvent()

    On Error GoTo ErrorHander
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    estado = 1
    BG_StackJanelas_Push Me
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Private Sub ActivateEvent()
    
    On Error GoTo ErrorHander
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    CR_BL_ToolbarEstadoN estado
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Private Sub UnloadEvent()
    
    On Error GoTo ErrorHander
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    CR_BL_ToolbarEstadoN 0
    If (Not rsTabela.state = adStateClosed) Then: rsTabela.Close
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' Clean form objects.
Private Sub LimpaCampos()
    
    On Error GoTo ErrorHander
    Me.SetFocus
    BG_LimpaCampo_Todos CamposEc
    LaUtilCri.caption = Empty
    LaUtilAct.caption = Empty
    LaDataCri.caption = Empty
    LaDataAct.caption = Empty
    ListViewMembers.ListItems.Clear
    OptionButtonUsers_Click
    CkPublica.value = vbGrayed
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' Define the type of the form objects.
Sub DefTipoCampos()

    On Error GoTo ErrorHander
    CR_BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCri, cr_mediTipoData
    CR_BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcHoraCri, cr_mediTipoHora
    CR_BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAct, cr_mediTipoData
    CR_BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcHoraAct, cr_mediTipoHora
    CkPublica.value = vbGrayed
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' Fill default values.
Private Sub PreencheValoresDefeito()
    
    On Error GoTo ErrorHander
    TextBoxName = Empty
    ComboBoxUsers.ListIndex = cr_mediComboValorNull
    ListViewMembers.ListItems.Clear
    LaDataCri = Empty
    LaUtilCri = Empty
    LaDataAct = Empty
    LaUtilAct = Empty
    InitializeListViewMembers
    OptionButtonUsers_Click
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

' Fill form fields.
Private Sub PreencheCampos()
    
    On Error GoTo ErrorHander
    Me.SetFocus
    If (rsTabela.RecordCount = 0) Then
        FuncaoLimpar
    Else
        CR_BG_PreencheCampoEc_Todos_ADO rsTabela, CamposBD, CamposEc
        CR_BL_PreencheControloAcessos EcUtilCri, EcUtilAct, LaUtilCri, LaUtilAct
        LaDataCri = EcDataCri
        LaDataAct = EcDataAct
        FillListView
    End If
    Exit Sub
    
ErrorHander:
    Exit Sub
    
End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        'CampoDeFocus.SetFocus
    End If

End Sub

Public Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 1
        CR_BL_ToolbarEstadoN estado
        LimpaCampos
        CampoDeFocus.SetFocus
        rsTabela.Close
    Else
        Unload Me
    End If
    
End Sub

Private Function ValidaCamposEc() As Integer

    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> Empty Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Public Sub FuncaoProcurar()
    
    On Error GoTo ErrorHander
    BL_InicioProcessamento Me, "A pesquisar registos..."
    EcHoraCri = Empty
    EcHoraAct = Empty
    Set rsTabela = New ADODB.recordset
    CriterioTabela = CR_BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, True)
    rsTabela.CursorLocation = adUseServer
    rsTabela.Open CriterioTabela, gConexao, adOpenStatic
    If (rsTabela.RecordCount = 0) Then
        FuncaoLimpar
        BG_Mensagem cr_mediMsgBox, "N�o foi seleccionado nenhum registo! ", vbExclamation, "Procurar"
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        CR_BL_ToolbarEstadoN estado
    End If
    BL_FimProcessamento Me
    Exit Sub

ErrorHander:
    Exit Sub

End Sub

Public Sub FuncaoAnterior()

    rsTabela.MovePrevious
    
    If rsTabela.BOF Then
        rsTabela.MoveNext
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Public Sub FuncaoSeguinte()

    rsTabela.MoveNext
    
    If rsTabela.EOF Then
        rsTabela.MovePrevious
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Public Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(cr_mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        EcUtilCri = gCodUtilizador
        EcDataCri = CR_Bg_DaData_ADO
        EcHoraCri = CR_Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
    End If
    
End Sub

Sub BD_Insert()
    Dim RsTeste As ADODB.recordset
    Dim sCodigo() As String
    Dim sSql As String
    Dim i As Integer
    Dim iRes As Integer

    On Error GoTo Trata_Erro
    
    
    If ListViewMembers.ListItems.Count = 0 Then
        BG_Mensagem cr_mediMsgBox, "A lista tem que incluir pelo menos um item.", vbInformation, "..."
        Exit Sub
    End If
            
    sSql = "SELECT * FROM " & NomeTabela & " WHERE descricao=" & BG_VfValor(TextBoxName, "'")
    Set RsTeste = New ADODB.recordset
    RsTeste.CursorLocation = adUseServer
    RsTeste.Open sSql, gConexao, adOpenStatic
    If RsTeste.RecordCount > 0 Then
        BG_Mensagem cr_mediMsgBox, "J� existe um registo com essa descri��o.", vbInformation, "..."
        RsTeste.Close
        Set RsTeste = Nothing
        Exit Sub
    End If
    RsTeste.Close
    Set RsTeste = Nothing
    
    TextBoxCode = BG_DaMAX(NomeTabela, "codigo") + 1
    
    sSql = CR_BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    iRes = CR_BG_ExecutaQuery_ADO(sSql)
    
    If iRes = -1 Then
        CR_BG_Trata_BDErro
    Else
        InsertMembers
        i = TextBoxCode
        FuncaoLimpar
        TextBoxCode = i
        FuncaoProcurar
    End If
    
    Exit Sub

Trata_Erro:

End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    
    If gCodUtilizador <> rsTabela!user_cri And gCodGrupo <> gGrupoAdministradores Then
        BG_Mensagem cr_mediMsgBox, "N�o tem permiss�es para modificar este registo.", vbCritical, "..."
        Exit Sub
    End If
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(cr_mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        EcUtilAct = gCodUtilizador
        EcDataAct = CR_Bg_DaData_ADO
        EcHoraAct = CR_Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Update
        End If
    End If
    
End Sub

Sub BD_Update()
    Dim sCondicao As String
    Dim sSql As String
    Dim MarcaLocal As Variant
    
    
    gSQLError = 0
    gSQLISAM = 0
    
    If ListViewMembers.ListItems.Count = 0 Then
        BG_Mensagem cr_mediMsgBox, "A lista tem que incluir pelo menos um item.", vbInformation, "..."
        Exit Sub
    End If
    
    sCondicao = "codigo=" & TextBoxCode
    sSql = CR_BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, sCondicao)
    CR_BG_ExecutaQuery_ADO sSql
    
    CR_BG_Trata_BDErro
    
    Dim i As Integer
    Dim sCodigo() As String
    
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "mensagens_membros WHERE cod_lista=" & rsTabela!Codigo
    CR_BG_ExecutaQuery_ADO sSql
    
    CR_BG_Trata_BDErro
    
    For i = 1 To ListViewMembers.ListItems.Count
        sCodigo = Split(ListViewMembers.ListItems(i).Key, "-")
        sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "mensagens_membros (cod_lista,cod_membro,flg_lista) VALUES("
        sSql = sSql & TextBoxCode.Text & ","
        sSql = sSql & sCodigo(1) & ","
        If sCodigo(0) = "U" Then
            sSql = sSql & "0)"
        Else
            sSql = sSql & "1)"
        End If
        
        CR_BG_ExecutaQuery_ADO sSql
    Next

    MarcaLocal = rsTabela.Bookmark
    rsTabela.Requery
    
    If rsTabela.BOF And rsTabela.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    rsTabela.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos
    
End Sub

Sub FuncaoRemover()
    
    If gCodUtilizador <> rsTabela!user_cri And gCodGrupo <> gGrupoAdministradores Then
        BG_Mensagem cr_mediMsgBox, "N�o tem permiss�es para remover este registo.", vbCritical, "..."
        Exit Sub
    End If

    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer remover este registo ?"
    gMsgResp = BG_Mensagem(cr_mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub

Sub BD_Delete()
    Dim sSql As String
    Dim sCondicao As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    gSQLISAM = 0
    
    sCondicao = "codigo=" & rsTabela!Codigo
    sSql = "DELETE FROM " & NomeTabela & " WHERE " & sCondicao
    CR_BG_ExecutaQuery_ADO sSql
    
    CR_BG_Trata_BDErro
    
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "mensagens_membros WHERE cod_lista=" & rsTabela!Codigo
    CR_BG_ExecutaQuery_ADO sSql
    
    CR_BG_Trata_BDErro
    
    MarcaLocal = rsTabela.Bookmark
    rsTabela.Requery
        
    If rsTabela.BOF And rsTabela.EOF Then
        BG_Mensagem cr_mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rsTabela.EOF Then
        rsTabela.MovePrevious
    Else
        If MarcaLocal <> 1 Then
            rsTabela.Bookmark = MarcaLocal - 1
        End If
    End If
    
    LimpaCampos
    PreencheCampos
    
End Sub


' pferreira 2007.08.27
' Paint a list view with colored bars.
Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture(Empty)
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Insert list members.
Private Sub InsertMembers()
    
    Dim vItem As Variant
    Dim sSql As String
    Dim sCode() As String
    
    On Error GoTo ErrorHandler
    For Each vItem In ListViewMembers.ListItems
        sCode = Split(vItem.Key, "-")
        sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens_membros (cod_lista,cod_membro,flg_lista) values (" & TextBoxCode.Text & "," & sCode(1) & ","
        If (sCode(0) = "U") Then: sSql = sSql & "0)"
        If (sCode(0) <> "U") Then: sSql = sSql & "1)"
        CR_BG_ExecutaQuery_ADO sSql
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

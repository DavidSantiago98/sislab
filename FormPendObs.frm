VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormPendObs 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormPendObs"
   ClientHeight    =   6810
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13350
   Icon            =   "FormPendObs.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6810
   ScaleWidth      =   13350
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodSala 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8160
      TabIndex        =   8
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcDescrSala 
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8880
      Locked          =   -1  'True
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   120
      Width           =   3375
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   12240
      Picture         =   "FormPendObs.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   120
      Width           =   375
   End
   Begin VB.ListBox EcListaReq 
      Height          =   5325
      Left            =   240
      TabIndex        =   5
      Top             =   600
      Width           =   3855
   End
   Begin MSComCtl2.DTPicker EcDtIni 
      Height          =   300
      Left            =   1680
      TabIndex        =   1
      Top             =   40
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   529
      _Version        =   393216
      Format          =   147456001
      CurrentDate     =   39463
   End
   Begin MSFlexGridLib.MSFlexGrid FgObs 
      Height          =   5535
      Left            =   4440
      TabIndex        =   0
      Top             =   600
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   9763
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin MSComCtl2.DTPicker EcDtFim 
      Height          =   300
      Left            =   4920
      TabIndex        =   2
      Top             =   45
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   529
      _Version        =   393216
      Format          =   147456001
      CurrentDate     =   39463
   End
   Begin VB.Label LbObs 
      Height          =   615
      Left            =   240
      TabIndex        =   10
      Top             =   6120
      Width           =   10095
   End
   Begin VB.Label Label1 
      Caption         =   "Sala"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   7440
      TabIndex        =   9
      Top             =   120
      Width           =   675
   End
   Begin VB.Label Label1 
      Caption         =   "Data Final"
      Height          =   255
      Index           =   1
      Left            =   4080
      TabIndex        =   4
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicial"
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   3
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "FormPendObs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

' ESTRUTURA COM NOTAS DA REQUISI��O
Private Type obs
    codAgrup As String
    descrObs As String
    dt_cri As String
    user_cri As String
    hr_cri As String
End Type

' ESTRUTURA COM AS REQUISI��ES PERTENCENTES AO INTERVALO DE DATAS
Private Type Obs_Requis
    NReq As String
    DtChega As String
    Notas() As obs
    TotalNotas As Integer
End Type
Dim req() As Obs_Requis
Dim totalReq As Integer

Public rs As ADODB.recordset



Private Sub BtProcurar_Click()
    FuncaoProcurar
End Sub

Private Sub EcListaReq_Click()
    LimpaFgObs
    If EcListaReq.Text <> "" Then
        LimpaFgObs
        PreencheFgObs EcListaReq.ListIndex + 1
    End If
End Sub

Private Sub FgObs_Click()
    If FgObs.row < FgObs.rows - 1 Then
        LbObs = ""
        LbObs = req(EcListaReq.ListIndex + 1).Notas(FgObs.row).descrObs
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
        
End Sub

Sub Inicializacoes()

    Me.caption = "Listagem de Observa��es"
    Me.left = 50
    Me.top = 50
    Me.Width = 13440
    Me.Height = 7290 ' Normal
    
    totalReq = 0
    ReDim req(totalReq)
   
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Listagem de Observa��es")
    
    Set FormPendObs = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    EcDtFim.value = Bg_DaData_ADO
    EcDtIni.value = Bg_DaData_ADO
    EcCodSala = ""
    EcCodsala_Validate False
    totalReq = 0
    ReDim req(totalReq)
    EcListaReq.Clear
    LimpaCampos
End Sub

Sub DefTipoCampos()

    'Tipo Data
    EcDtIni.Tag = adDate
    EcDtFim.Tag = adDate
    Set CampoDeFocus = EcDtIni
    With FgObs
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 1500
        .Col = 0
        .Text = "An�lise"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(1) = 3000
        .Col = 1
        .Text = "Observa��o"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(2) = 1500
        .Col = 2
        .Text = "Data"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(3) = 2000
        .Col = 3
        .Text = "Utilizador"
        .CellAlignment = flexAlignCenterCenter
        .row = 1
        .Col = 0
        
    End With
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsOBS As New ADODB.recordset
    EcListaReq.Clear
    LimpaFgObs
    totalReq = 0
    ReDim req(0)
    sSql = "SELECT sl_requis.n_req, sl_requis.dt_chega, sl_obs_ana_req.cod_agrup, "
    sSql = sSql & " sl_obs_ana_req.descr_obs, sl_obs_ana_req.dt_cri, sl_obs_ana_req.user_cri, sl_obs_ana_req.hr_cri "
    sSql = sSql & " FROM sl_requis, sl_obs_ana_req WHERE sl_requis.n_req = sl_obs_ana_req.n_req"
    sSql = sSql & " AND sl_requis.dt_chega BETWEEN  " & BL_TrataDataParaBD(EcDtIni.value) & " AND "
    sSql = sSql & BL_TrataDataParaBD(EcDtFim.value) & " ORDER BY sl_requis.N_REQ "

    
    rsOBS.CursorType = adOpenStatic
    rsOBS.CursorLocation = adUseServer
    rsOBS.Open sSql, gConexao
    If rsOBS.RecordCount = 0 Then
        Call BG_Mensagem(mediMsgBox, "N�o foi encontrada qualquer observa��o.", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    Else
        While Not rsOBS.EOF
            If rsOBS!n_req <> req(totalReq).NReq Then
                totalReq = totalReq + 1
                ReDim Preserve req(totalReq)
                req(totalReq).NReq = rsOBS!n_req
                req(totalReq).DtChega = BL_HandleNull(rsOBS!dt_chega, "")
                req(totalReq).TotalNotas = 0
                ReDim req(totalReq).Notas(0)
                
                EcListaReq.AddItem req(totalReq).NReq & Space(20) & req(totalReq).DtChega
                
            End If
            
            req(totalReq).TotalNotas = req(totalReq).TotalNotas + 1
            ReDim Preserve req(totalReq).Notas(req(totalReq).TotalNotas)
            req(totalReq).Notas(req(totalReq).TotalNotas).codAgrup = BL_HandleNull(rsOBS!cod_agrup, "")
            req(totalReq).Notas(req(totalReq).TotalNotas).descrObs = BL_HandleNull(rsOBS!descr_obs, "")
            req(totalReq).Notas(req(totalReq).TotalNotas).dt_cri = BL_HandleNull(rsOBS!dt_cri, "")
            req(totalReq).Notas(req(totalReq).TotalNotas).user_cri = BL_HandleNull(rsOBS!user_cri, "")
            req(totalReq).Notas(req(totalReq).TotalNotas).hr_cri = BL_HandleNull(rsOBS!hr_cri, "")
            
            rsOBS.MoveNext
        Wend
    End If
    rsOBS.Close
End Sub







Private Sub BtPesquisaSala_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_cod_salas", _
                        "descr_sala", "cod_sala", _
                        EcCodSala, EcDescrSala
End Sub


Private Sub PreencheSalaDefeito()
    EcCodSala.Enabled = True
    BtPesquisaSala.Enabled = True
    If gCodSalaAssocUser > 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate True
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
    End If
End Sub
Public Sub EcCodsala_Validate(Cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodSala.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_sala FROM sl_cod_salas WHERE cod_sala='" & Trim(EcCodSala.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodSala.Text = ""
            EcDescrSala.Text = ""
        Else
            EcDescrSala.Text = Tabela!descr_sala
        End If
       
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrSala.Text = ""
    End If
    
End Sub


Private Sub LimpaFgObs()
    Dim i As Integer
    FgObs.rows = 2
        For i = 0 To FgObs.Cols - 1
            FgObs.TextMatrix(1, i) = ""
        Next
End Sub
Private Sub PreencheFgObs(indice As Integer)
    Dim i As Integer
    For i = 1 To req(indice).TotalNotas
        FgObs.TextMatrix(i, 0) = BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", req(indice).Notas(i).codAgrup)
        FgObs.TextMatrix(i, 1) = req(indice).Notas(i).descrObs
        FgObs.TextMatrix(i, 2) = req(indice).Notas(i).dt_cri & " " & req(indice).Notas(i).hr_cri
        FgObs.TextMatrix(i, 3) = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", req(indice).Notas(i).user_cri)
        FgObs.AddItem ""
    Next
End Sub
Public Sub FuncaoImprimir()
    Dim sSql As String
    Dim i As Integer
    Dim Report As CrystalReport
    Dim continua As Boolean
    
    sSql = "DELETE FROM sl_cr_notas WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    For i = 1 To totalReq
        BL_ImprimeNotasRequisicao req(i).NReq
    Next
    Set Report = MDIFormInicio.Report
    If gImprimirDestino = 1 Then

        continua = BL_IniciaReport("Notas", "Notas", crptToPrinter, , , , , , , "Notas")
    Else
    continua = BL_IniciaReport("Notas", "Notas", crptToWindow, , , , , , , "Notas")
End If
    Report.SQLQuery = " SELECT " & _
                     " SL_CR_NOTAS.n_req , SL_CR_NOTAS.descr_ana, SL_CR_NOTAS.descr_obs, SL_CR_NOTAS.dt_cri " & _
                     " From SL_CR_NOTAS Where nome_computador = " & BL_TrataStringParaBD(gComputador) & _
                     " AND num_sessao = " & gNumeroSessao & " order By SL_CR_NOTAS.n_req Asc "
    Call BL_ExecutaReport
    sSql = "DELETE FROM sl_cr_notas WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
End Sub





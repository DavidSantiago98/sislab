Attribute VB_Name = "FACTUS"
Option Explicit
Const lCodDomicilio = 3
Const lCodUrbano = 2
Const lCodNaoUrbano = 3
Const lCodUrbanoPortoLisboa = 1
Const lCodUrbanoBiDiario = 4
'edgar.parada LOTE 97 - Conven��o
Global Const lCod97 = 4
'
'edgar.parada LJMANSO-351 10.01.2019
Global Const lCodProfissional = 3
Global Const lCodInternacional = 2 '(MIGRANTES)
Global Const lCodNormal = 1
Global Const lCodNormalEspecial = 5
Global Const lCodInternacionalEspecial = 6 '(MIGRANTES ESPECIAL)
Global Const lCodProfissionalEspecial = 7
'
Private Type AnaFact
    cod_ana As String
    cod_rubr As Long
    iAna As Integer
    n_seq_prog As Long
    iResp As Integer
    iAnaFact As Integer
    seq_ana_fact As Long
    cod_medico As String
End Type
Public AnalisesFact() As AnaFact
Public TotalAnalisesFact As Integer

Public Type Recibo
    tipo As String
    cod_efr As Long
    descr_efr As String
    valor As Double
    Desconto As Double
    modo_pagamento As Integer
    descr_pagamento As String
    serie_fac_doe As String
    n_Fac_doe As Long
    dt_emissao As String
    dt_pagamento As String
    flg_estado As String
    descr_estado As String
    venda_dinheiro As String
    num_analises As Integer
    iEmiRec As Integer
    seq_utente As Long
    empresa_id As String
    flg_borla As String
    totalDet As Double
End Type

Public RecibosNew() As Recibo
Public totalRecibosNew As Integer

Private Type movi_fact
    n_seq_prog                     As Long
    cod_prog                          As Long
    t_doente                          As String
    doente                            As String
    t_episodio                       As String
    episodio                         As String
    n_ord                             As Long
    dt_ini_real                      As String
    dt_fim_real                       As String
    cod_tip_rubr                      As Long
    cod_grupo                        As Long
    cod_rubr                          As Long
    cod_rubr_efr                      As String
    descr_rubr                        As String
    cod_serv_req                      As Long
    cod_serv_exec                     As Long
    descr_lin_fac                     As String
    val_pr_unit                       As Double
    val_pr_unit_2m                    As Double
    val_k                             As Double
    val_k_2m                          As Double
    val_c                             As Double
    val_c_2m                          As Double
    nr_k                              As Double
    nr_c                              As Double
    qtd                              As Double
    perc_desc_lin                     As Double
    perc_pag_efr                      As Double
    val_total                         As Double
    val_total_2m                      As Double
    perc_pag_doe                      As Double
    val_pr_u_doe                      As Double
    val_pr_u_doe_2m                   As Double
    val_pag_doe                       As Double
    val_pag_doe_2m                    As Double
    val_prev                          As Double
    val_prev_2m                       As Double
    cod_isen_doe                      As Integer
    flg_pagou_taxa                    As String
    cod_prov                          As String
    cod_dest                          As String
    nr_req_ars                        As String
    t_int                             As Integer
    t_cond_esp                        As String
    n_incidencias                     As Integer
    flg_stdby                         As Integer
    flg_ventil                        As String
    flg_psiq                          As String
    flg_orig_rubr                     As String
    flg_estado                        As Integer
    serie_fac                         As String
    n_fac                             As Long
    flg_estado_doe                    As Integer
    serie_fac_doe                     As String
    n_Fac_doe                         As Long
    cod_mot_rej_anul                  As Long
    chave_prog                        As String
    cod_serv_req_gh                   As String
    cod_serv_exec_gh                  As String
    n_doc                             As String
    user_cri                          As String
    dt_cri                            As String
    user_act                          As String
    dt_act                            As String
    val_Taxa                          As Double
    val_taxa_2m                       As Double
    flg_pagou_val_doe                 As String
    flg_ventil_perm                   As Integer
    t_gdh                             As Integer
    tab_prog                          As String
    hr_ini_real                       As String
    hr_fim_real                       As String
    nome_interv                       As String
    nome_doe                          As String
    cod_rubr_cir                      As Long
    n_ord_ins                         As Long
    erro_reg                          As String
    tipo_interv                       As String
    cod_resp                          As Long
    n_interv                          As String
    flg_estado_tx                     As Integer
    serie_fac_tx                      As String
    n_fac_tx                          As Long
    flg_erro                          As String
    flg_fact_pat                      As String
    n_seq_prog_orig                   As Integer
    flg_alt_pr                        As String
    flg_reg_termo                     As String
    empresa_id                        As String
    id_mot_efr                        As Integer
    id_mot_doe                        As Integer
    flg_prod_marg                     As String
    flg_tip_req                       As String
    cod_dente                         As String
    unidade                           As String
    perc_desc_lin_doe                 As Double
    num_proc_efr                      As String
    flg_prog_urg                      As String
    id_preco_fech                     As Integer
    val_ref                           As Double
    val_pvp                           As Double
    t_doente_gh                       As String
    doente_gh                         As String
    t_episodio_gh                     As String
    episodio_gh                       As String
    n_marcadores                      As Integer
    id_ugc                            As Integer
    origem_valor                      As String

    convencao As Integer
    cod_dom As Integer
    km_dom As Integer
    cod_post_dom As String
    localidade_dom As String
    cod_medico As String
    cod_u_saude As String
    iAnalise                        As Integer
        id_unico_externo As String
End Type

Private Type movi_resp
    n_ord                          As Long
    t_episodio                     As String
    episodio                       As String
    n_benef_doe                    As String
    cod_tip_rubr                   As Long
    cod_rubr                       As Long
    cod_efr                        As Long
    descr_efr                      As String
    mor_efr                        As String
    cod_post_efr                   As String
    n_seq_post_efr                 As Integer
    telef_efr                      As String
    n_contrib_efr                  As String
    cod_modal                      As Long
    val_franq                      As Double
    val_franq_2m                   As Double
    per_paga                       As Double
    val_max                        As Double
    val_max_2m                     As Double
    cod_desc                       As Integer
    perc_desc                      As Double
    val_total                      As Double
    val_total_2m                   As Double
    user_cri                       As String
    dt_cri                         As String
    user_act                       As String
    dt_act                         As String
    descr_causa_adm                As String
    n_ord_franq                    As Integer
    cod_dom                        As Integer
    qtd_dom                        As Integer
    n_mecan_med                    As String
    nome_med                       As String
    n_desloc                       As Integer
    localidade                     As String
    flg_estado                     As Integer
    serie_fac                      As String
    n_fac                          As Integer
    sigla_t_orig                   As String
    t_doente                       As String
    doente                         As String
    cod_efr_base                   As Integer
    n_ord_fact                     As Integer
    flg_estado_efr                 As Integer
    serie_fac_efr                  As String
    n_fac_efr                      As Long
    cod_efr_igif                   As Long
    empresa_id                     As String
    perc_franq                     As Double
    val_min_franq                  As Double
    val_max_franq                  As Double
    id_ugc                         As Integer
    
    flg_compart_dom  As Integer
    flg_dom_defeito As Integer
    flg_novaArs As Integer
    
    analises() As movi_fact
    totalAna As Integer
    valor_marcado_utilizador As Double
    flg_preco_alterado As Boolean
End Type

Public fa_movi_resp() As movi_resp
Public fa_movi_resp_tot As Integer

'UALIA-871
Public Enum EstadoFaturacao
    NaoTransferido = 0
    ProntoAFaturar = 1
    Rejeitado = 2
    Faturado = 3
    Anulado = 4
    NaoFaturavelPorPrecario = 5
    ProntoAFaturar2 = 7
    NaoFaturavel = 8
    NaoFatExtraPrecoFechado = 9
End Enum

Public Function FACTUS_PreencheRequis(n_req As String, Criterio As String, Optional credencial As String) As Boolean
    Dim ssql As String
    Dim rsResp As New adodb.recordset
    Dim iBdAberta As Integer
    On Error GoTo TrataErro
    
    fa_movi_resp_tot = 0
    ReDim fa_movi_resp(0)
    If n_req = "" And Criterio = "" Then Exit Function
    
    ' ABRE CONEXAO COM FACTUS
    iBdAberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iBdAberta = 0 Then
        Exit Function
    End If
    
    'BRUNODSANTOS LJMANSO-307
    'Adicionada sl_efr para incluir flg_compart_dom, flg_dom_defeito, cod_urbano, flg_nova_ars  na estrutura fa_movi_resp
        'edgar.parada  LJMANSO-339 24.04.2019 tabela sl_requis
    ssql = "SELECT x1.*, x2.flg_compart_dom, x2.flg_dom_defeito,x2.flg_nova_ars FROM fa_movi_resp x1, sl_efr x2, sl_requis x3 WHERE t_episodio = 'SISLAB' "
    If n_req <> "" Then
        ssql = ssql & " AND episodio =" & BL_TrataStringParaBD(n_req)
    End If
    
    ssql = ssql & " AND (episodio, n_ord) in (select episodio, n_ord from fa_movi_fact x2 "
    ssql = ssql & " WHERE x2.t_episodio = x1.t_episodio and x2.episodio = x1.episodio and "
    ssql = ssql & " (x2.flg_estado_doe <>2 or x2.flg_estado_doe is null))"
    If Criterio <> "" Then
        ssql = ssql & Criterio
    End If
    ssql = ssql & " AND x1.cod_efr = x2.cod_efr AND x3.n_req = x1.episodio ORDER BY to_number(x1.episodio) ASC "
    
    If gModoDebug = 1 Then BG_LogFile_Erros (ssql)

    Set rsResp = New adodb.recordset
    rsResp.CursorLocation = adUseServer
    rsResp.Open ssql, gConexao, adOpenStatic

    If rsResp.RecordCount > 0 Then
        While Not rsResp.EOF
            fa_movi_resp_tot = fa_movi_resp_tot + 1
            ReDim Preserve fa_movi_resp(fa_movi_resp_tot)
            
            fa_movi_resp(fa_movi_resp_tot).n_ord = BL_HandleNull(rsResp!n_ord, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).t_episodio = BL_HandleNull(rsResp!t_episodio, "")
            fa_movi_resp(fa_movi_resp_tot).episodio = BL_HandleNull(rsResp!episodio, "")
            fa_movi_resp(fa_movi_resp_tot).n_benef_doe = BL_HandleNull(rsResp!n_benef_doe, "")
            fa_movi_resp(fa_movi_resp_tot).cod_tip_rubr = BL_HandleNull(rsResp!cod_tip_rubr, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).cod_rubr = BL_HandleNull(rsResp!cod_rubr, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).cod_efr = BL_HandleNull(rsResp!cod_efr, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).descr_efr = BL_HandleNull(rsResp!descr_efr, "")
            fa_movi_resp(fa_movi_resp_tot).mor_efr = BL_HandleNull(rsResp!mor_efr, "")
            fa_movi_resp(fa_movi_resp_tot).cod_post_efr = BL_HandleNull(rsResp!cod_post_efr, "")
            fa_movi_resp(fa_movi_resp_tot).n_seq_post_efr = BL_HandleNull(rsResp!n_seq_post_efr, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).telef_efr = BL_HandleNull(rsResp!telef_efr, "")
            fa_movi_resp(fa_movi_resp_tot).n_contrib_efr = BL_HandleNull(rsResp!n_contrib_efr, "")
            fa_movi_resp(fa_movi_resp_tot).cod_modal = BL_HandleNull(rsResp!cod_modal, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_franq = BL_HandleNull(rsResp!val_franq, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_franq_2m = BL_HandleNull(rsResp!val_franq_2m, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).per_paga = BL_HandleNull(rsResp!per_paga, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_max = BL_HandleNull(rsResp!val_max, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_max_2m = BL_HandleNull(rsResp!val_max_2m, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).cod_desc = BL_HandleNull(rsResp!cod_desc, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).perc_desc = BL_HandleNull(rsResp!perc_desc, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_total = BL_HandleNull(rsResp!val_total, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_total_2m = BL_HandleNull(rsResp!val_total_2m, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).user_cri = BL_HandleNull(rsResp!user_cri, "")
            fa_movi_resp(fa_movi_resp_tot).dt_cri = BL_HandleNull(rsResp!dt_cri, "")
            fa_movi_resp(fa_movi_resp_tot).user_act = BL_HandleNull(rsResp!user_act, "")
            fa_movi_resp(fa_movi_resp_tot).dt_act = BL_HandleNull(rsResp!dt_act, "")
            fa_movi_resp(fa_movi_resp_tot).descr_causa_adm = BL_HandleNull(rsResp!descr_causa_adm, "")
            fa_movi_resp(fa_movi_resp_tot).n_ord_franq = BL_HandleNull(rsResp!n_ord_franq, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).cod_dom = BL_HandleNull(rsResp!cod_dom, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).qtd_dom = BL_HandleNull(rsResp!qtd_dom, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).n_mecan_med = BL_HandleNull(rsResp!n_mecan_med, "")
            fa_movi_resp(fa_movi_resp_tot).nome_med = BL_HandleNull(rsResp!nome_med, "")
            fa_movi_resp(fa_movi_resp_tot).n_desloc = BL_HandleNull(rsResp!n_desloc, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).localidade = BL_HandleNull(rsResp!localidade, "")
            fa_movi_resp(fa_movi_resp_tot).flg_estado = BL_HandleNull(rsResp!flg_estado, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).serie_fac = BL_HandleNull(rsResp!serie_fac, "")
            fa_movi_resp(fa_movi_resp_tot).n_fac = BL_HandleNull(rsResp!n_fac, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).sigla_t_orig = BL_HandleNull(rsResp!sigla_t_orig, "")
            fa_movi_resp(fa_movi_resp_tot).t_doente = BL_HandleNull(rsResp!t_doente, "")
            fa_movi_resp(fa_movi_resp_tot).doente = BL_HandleNull(rsResp!doente, "")
            fa_movi_resp(fa_movi_resp_tot).cod_efr_base = BL_HandleNull(rsResp!cod_efr_base, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).n_ord_fact = BL_HandleNull(rsResp!n_ord_fact, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).flg_estado_efr = BL_HandleNull(rsResp!flg_estado_efr, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).serie_fac_efr = BL_HandleNull(rsResp!serie_fac_efr, "")
            fa_movi_resp(fa_movi_resp_tot).n_fac_efr = BL_HandleNull(rsResp!n_fac_efr, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).cod_efr_igif = BL_HandleNull(rsResp!cod_efr_igif, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).empresa_id = BL_HandleNull(rsResp!empresa_id, "")
            fa_movi_resp(fa_movi_resp_tot).perc_franq = BL_HandleNull(rsResp!perc_franq, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_min_franq = BL_HandleNull(rsResp!val_min_franq, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).val_max_franq = BL_HandleNull(rsResp!val_max_franq, mediComboValorNull)
            fa_movi_resp(fa_movi_resp_tot).flg_compart_dom = BL_HandleNull(rsResp!flg_compart_dom, 0)
            fa_movi_resp(fa_movi_resp_tot).flg_dom_defeito = BL_HandleNull(rsResp!flg_dom_defeito, 0)
            'fa_movi_resp(fa_movi_resp_tot).cod_dom = FACTUS_DevolveCodUrbanoFromRequis(n_req)
            fa_movi_resp(fa_movi_resp_tot).flg_novaArs = BL_HandleNull(rsResp!flg_nova_ars, 0)
                        'edgar.parada  LJMANSO-339 24.04.2019
            FACTUS_PreencheAna fa_movi_resp_tot, credencial
            '
            rsResp.MoveNext
        Wend
    End If
    rsResp.Close
    Set rsResp = Nothing
    FACTUS_PreencheRequis = True
Exit Function
TrataErro:
    FACTUS_PreencheRequis = False
    BG_LogFile_Erros "Erro  ao FACTUS_PreencheRequis: " & Err.Description, "FACTUS", "FACTUS_PreencheRequis", True
    Exit Function
    Resume Next
End Function

Public Function FACTUS_InsereResp(linha As Integer) As Boolean
    Dim ssql As String
    Dim rv As Integer
    On Error GoTo TrataErro
    
    fa_movi_resp(linha).user_cri = CStr(gCodUtilizador)
    ssql = "DELETE FROM fa_movi_resp x0 WHERE t_episodio = " & BL_TrataStringParaBD(fa_movi_resp(linha).t_episodio)
    ssql = ssql & " AND episodio = " & BL_TrataStringParaBD(fa_movi_resp(linha).episodio)
    ssql = ssql & " AND cod_efr = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_efr))
    ssql = ssql & " AND (t_episodio, episodio, n_ord ) NOT IN (select x1.t_episodio, x1.episodio, x1.n_ord FROM fa_movi_fact x1  WHERE x1.t_episodio = x0.t_episodio "
    ssql = ssql & " AND x1.episodio = x0.episodio and x1.n_ord = x0.n_ord)"
    BG_ExecutaQuery_ADO ssql
    
    ssql = "INSERT INTO fa_movi_resp (n_ord, t_episodio, episodio, n_benef_doe, cod_tip_rubr,cod_rubr, cod_efr, descr_efr, mor_efr, cod_post_efr, "
    ssql = ssql & " n_seq_post_efr, telef_efr, n_contrib_efr, cod_modal,val_franq, per_paga, val_max,"
    ssql = ssql & " cod_desc, perc_desc, val_total, user_cri,dt_cri, user_act, dt_act, descr_causa_adm, n_ord_franq,"
    ssql = ssql & " cod_dom, qtd_dom, n_mecan_med, nome_med, n_desloc,localidade, flg_estado, serie_fac, n_fac, sigla_t_orig,"
    ssql = ssql & " t_doente, doente, cod_efr_base, n_ord_fact, flg_estado_efr, serie_fac_efr, n_fac_efr, cod_efr_igif,"
    ssql = ssql & " empresa_id, perc_franq, val_min_franq, val_max_franq) VALUES ("
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_ord)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).t_episodio) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).episodio) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).n_benef_doe) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_tip_rubr)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_rubr)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_efr)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).descr_efr) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).mor_efr) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).cod_post_efr) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_seq_post_efr)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).telef_efr) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).n_contrib_efr) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_modal)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_franq)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).per_paga)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_max)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_desc)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(linha).perc_desc)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(linha).val_total)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).user_cri) & ","
    ssql = ssql & " SYSDATE ,null, null,"
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).descr_causa_adm) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_ord_franq)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_dom)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).qtd_dom)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).n_mecan_med) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).nome_med) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_desloc)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).localidade) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).flg_estado)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).serie_fac) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_fac)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).sigla_t_orig) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).t_doente) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).doente) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_efr_base)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_ord_fact)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).flg_estado_efr)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).serie_fac_efr) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_fac_efr)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_efr_igif)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(linha).empresa_id) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).perc_franq)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_min_franq)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_max_franq)) & ")"
    rv = BL_ExecutaQuery_Secundaria(ssql)
    If rv = -1 Then
        BG_LogFile_Erros "Erro inserir FA_MOVI_RESP: " & ssql & vbCrLf & Err.Description, "FACTUS", "FACTUS_InsereResp", True
    Else
        FACTUS_InsereResp = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao FACTUS_PreencheRequis: " & Err.Description, "FACTUS", "FACTUS_InsereResp", True
    Exit Function
    Resume Next
End Function

Public Function FACTUS_AtualizaResp(linha As Integer) As Boolean
    Dim ssql As String
    Dim rv As Integer
    On Error GoTo TrataErro
    fa_movi_resp(linha).user_act = CStr(gCodUtilizador)
    ssql = "UPDATE fa_movi_resp SET "
    ssql = ssql & " n_benef_doe= " & BL_TrataStringParaBD(fa_movi_resp(linha).n_benef_doe) & ","
    ssql = ssql & " cod_tip_rubr= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_tip_rubr)) & ","
    ssql = ssql & " cod_rubr= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_rubr)) & ","
    ssql = ssql & " cod_efr= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_efr)) & ","
    ssql = ssql & " descr_efr= " & BL_TrataStringParaBD(fa_movi_resp(linha).descr_efr) & ","
    ssql = ssql & " mor_efr= " & BL_TrataStringParaBD(fa_movi_resp(linha).mor_efr) & ","
    ssql = ssql & " cod_post_efr= " & BL_TrataStringParaBD(fa_movi_resp(linha).cod_post_efr) & ","
    ssql = ssql & " n_seq_post_efr= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_seq_post_efr)) & ","
    ssql = ssql & " telef_efr= " & BL_TrataStringParaBD(fa_movi_resp(linha).telef_efr) & ","
    ssql = ssql & " n_contrib_efr= " & BL_TrataStringParaBD(fa_movi_resp(linha).n_contrib_efr) & ","
    ssql = ssql & " cod_modal= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_modal)) & ","
    ssql = ssql & " val_franq= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_franq)) & ","
    ssql = ssql & " per_paga= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).per_paga)) & ","
    ssql = ssql & " val_max= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_max)) & ","
    ssql = ssql & " cod_desc= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_desc)) & ","
    ssql = ssql & " perc_desc= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).perc_desc)) & ","
    ssql = ssql & " val_total= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_total)) & ","
    ssql = ssql & " user_act= " & BL_TrataStringParaBD(fa_movi_resp(linha).user_act) & ", dt_act = sysdate,"
    ssql = ssql & " descr_causa_adm= " & BL_TrataStringParaBD(fa_movi_resp(linha).descr_causa_adm) & ","
    ssql = ssql & " n_ord_franq= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_ord_franq)) & ","
    ssql = ssql & " cod_dom= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_dom)) & ","
    ssql = ssql & " qtd_dom = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).qtd_dom)) & ","
    ssql = ssql & " n_mecan_med= " & BL_TrataStringParaBD(fa_movi_resp(linha).n_mecan_med) & ","
    ssql = ssql & " nome_med= " & BL_TrataStringParaBD(fa_movi_resp(linha).nome_med) & ","
    ssql = ssql & " n_desloc= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_desloc)) & ","
    ssql = ssql & " localidade= " & BL_TrataStringParaBD(fa_movi_resp(linha).localidade) & ","
    ssql = ssql & " flg_estado= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).flg_estado)) & ","
    ssql = ssql & " serie_fac= " & BL_TrataStringParaBD(fa_movi_resp(linha).serie_fac) & ","
    ssql = ssql & " n_fac= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_fac)) & ","
    ssql = ssql & " sigla_t_orig= " & BL_TrataStringParaBD(fa_movi_resp(linha).sigla_t_orig) & ","
    ssql = ssql & " t_doente= " & BL_TrataStringParaBD(fa_movi_resp(linha).t_doente) & ","
    ssql = ssql & " doente= " & BL_TrataStringParaBD(fa_movi_resp(linha).doente) & ","
    ssql = ssql & " cod_efr_base= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_efr_base)) & ","
    ssql = ssql & " n_ord_fact= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_ord_fact)) & ","
    ssql = ssql & " flg_estado_efr= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).flg_estado_efr)) & ","
    ssql = ssql & " serie_fac_efr= " & BL_TrataStringParaBD(fa_movi_resp(linha).serie_fac_efr) & ","
    ssql = ssql & " n_fac_efr= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_fac_efr)) & ","
    ssql = ssql & " cod_efr_igif= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).cod_efr_igif)) & ","
    ssql = ssql & " empresa_id= " & BL_TrataStringParaBD(fa_movi_resp(linha).empresa_id) & ","
    ssql = ssql & " perc_franq= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).perc_franq)) & ","
    ssql = ssql & " val_min_franq= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_min_franq)) & ","
    ssql = ssql & " val_max_franq= " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).val_max_franq))
    ssql = ssql & " WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(fa_movi_resp(linha).episodio)
    ssql = ssql & " AND n_ord = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(linha).n_ord))
    rv = BL_ExecutaQuery_Secundaria(ssql)
    If rv = -1 Then
        BG_LogFile_Erros "Erro atualizar FA_MOVI_RESP: " & ssql & vbCrLf & Err.Description, "FACTUS", "FACTUS_AtualizaResp", True
    Else
        FACTUS_AtualizaResp = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao FACTUS_AtualizaResp: " & Err.Description, "FACTUS", "FACTUS_AtualizaResp", True
    Exit Function
    Resume Next
End Function

Private Sub FACTUS_InicializaAna(iResp As Integer, iAna As Integer)
    fa_movi_resp(iResp).analises(iAna).n_seq_prog = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_prog = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).t_doente = ""
    fa_movi_resp(iResp).analises(iAna).doente = ""
    fa_movi_resp(iResp).analises(iAna).t_episodio = ""
    fa_movi_resp(iResp).analises(iAna).episodio = ""
    fa_movi_resp(iResp).analises(iAna).n_ord = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).dt_ini_real = ""
    fa_movi_resp(iResp).analises(iAna).dt_fim_real = ""
    fa_movi_resp(iResp).analises(iAna).cod_tip_rubr = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_grupo = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_rubr = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_rubr_efr = ""
    fa_movi_resp(iResp).analises(iAna).descr_rubr = ""
    fa_movi_resp(iResp).analises(iAna).cod_serv_req = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_serv_exec = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).descr_lin_fac = ""
    fa_movi_resp(iResp).analises(iAna).val_pr_unit = 0
    fa_movi_resp(iResp).analises(iAna).val_pr_unit_2m = 0
    fa_movi_resp(iResp).analises(iAna).val_k = 0
    fa_movi_resp(iResp).analises(iAna).val_k_2m = 0
    fa_movi_resp(iResp).analises(iAna).val_c = 0
    fa_movi_resp(iResp).analises(iAna).val_c_2m = 0
    fa_movi_resp(iResp).analises(iAna).nr_k = 0
    fa_movi_resp(iResp).analises(iAna).nr_c = 0
    fa_movi_resp(iResp).analises(iAna).qtd = 0
    fa_movi_resp(iResp).analises(iAna).perc_desc_lin = 0
    fa_movi_resp(iResp).analises(iAna).perc_pag_efr = 100
    fa_movi_resp(iResp).analises(iAna).val_total = 0
    fa_movi_resp(iResp).analises(iAna).val_total_2m = 0
    fa_movi_resp(iResp).analises(iAna).perc_pag_doe = 0
    fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = 0
    fa_movi_resp(iResp).analises(iAna).val_pr_u_doe_2m = 0
    fa_movi_resp(iResp).analises(iAna).val_pag_doe = 0
    fa_movi_resp(iResp).analises(iAna).val_pag_doe_2m = 0
    fa_movi_resp(iResp).analises(iAna).val_prev = 0
    fa_movi_resp(iResp).analises(iAna).val_prev_2m = 0
    fa_movi_resp(iResp).analises(iAna).cod_isen_doe = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).flg_pagou_taxa = ""
    fa_movi_resp(iResp).analises(iAna).cod_prov = ""
    fa_movi_resp(iResp).analises(iAna).cod_dest = ""
    fa_movi_resp(iResp).analises(iAna).nr_req_ars = ""
    fa_movi_resp(iResp).analises(iAna).t_int = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).t_cond_esp = ""
    fa_movi_resp(iResp).analises(iAna).n_incidencias = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).flg_stdby = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).flg_ventil = ""
    fa_movi_resp(iResp).analises(iAna).flg_psiq = ""
    fa_movi_resp(iResp).analises(iAna).flg_orig_rubr = ""
    fa_movi_resp(iResp).analises(iAna).flg_estado = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).serie_fac = ""
    fa_movi_resp(iResp).analises(iAna).n_fac = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).flg_estado_doe = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).serie_fac_doe = ""
    fa_movi_resp(iResp).analises(iAna).n_Fac_doe = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_mot_rej_anul = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).chave_prog = ""
    fa_movi_resp(iResp).analises(iAna).cod_serv_req_gh = ""
    fa_movi_resp(iResp).analises(iAna).cod_serv_exec_gh = ""
    fa_movi_resp(iResp).analises(iAna).n_doc = ""
    fa_movi_resp(iResp).analises(iAna).user_cri = ""
    fa_movi_resp(iResp).analises(iAna).dt_cri = ""
    fa_movi_resp(iResp).analises(iAna).user_act = ""
    fa_movi_resp(iResp).analises(iAna).dt_act = ""
    fa_movi_resp(iResp).analises(iAna).val_Taxa = 0
    fa_movi_resp(iResp).analises(iAna).val_taxa_2m = 0
    fa_movi_resp(iResp).analises(iAna).flg_pagou_val_doe = ""
    fa_movi_resp(iResp).analises(iAna).flg_ventil_perm = 0
    fa_movi_resp(iResp).analises(iAna).t_gdh = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).tab_prog = ""
    fa_movi_resp(iResp).analises(iAna).hr_ini_real = ""
    fa_movi_resp(iResp).analises(iAna).hr_fim_real = ""
    fa_movi_resp(iResp).analises(iAna).nome_interv = ""
    fa_movi_resp(iResp).analises(iAna).nome_doe = ""
    fa_movi_resp(iResp).analises(iAna).cod_rubr_cir = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).n_ord_ins = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).erro_reg = ""
    fa_movi_resp(iResp).analises(iAna).tipo_interv = ""
    fa_movi_resp(iResp).analises(iAna).cod_resp = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).n_interv = ""
    fa_movi_resp(iResp).analises(iAna).flg_estado_tx = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).serie_fac_tx = ""
    fa_movi_resp(iResp).analises(iAna).n_fac_tx = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).flg_erro = ""
    fa_movi_resp(iResp).analises(iAna).flg_fact_pat = ""
    fa_movi_resp(iResp).analises(iAna).n_seq_prog_orig = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).flg_alt_pr = ""
    fa_movi_resp(iResp).analises(iAna).flg_reg_termo = ""
    fa_movi_resp(iResp).analises(iAna).empresa_id = ""
    fa_movi_resp(iResp).analises(iAna).id_mot_efr = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).id_mot_doe = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).flg_prod_marg = ""
    fa_movi_resp(iResp).analises(iAna).flg_tip_req = ""
    fa_movi_resp(iResp).analises(iAna).cod_dente = ""
    fa_movi_resp(iResp).analises(iAna).unidade = ""
    fa_movi_resp(iResp).analises(iAna).perc_desc_lin_doe = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).num_proc_efr = ""
    fa_movi_resp(iResp).analises(iAna).flg_prog_urg = ""
    fa_movi_resp(iResp).analises(iAna).id_preco_fech = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).val_ref = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).val_pvp = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).t_doente_gh = ""
    fa_movi_resp(iResp).analises(iAna).doente_gh = ""
    fa_movi_resp(iResp).analises(iAna).t_episodio_gh = ""
    fa_movi_resp(iResp).analises(iAna).episodio_gh = ""
    fa_movi_resp(iResp).analises(iAna).n_marcadores = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).id_ugc = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).origem_valor = ""
    fa_movi_resp(iResp).analises(iAna).convencao = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_dom = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).km_dom = mediComboValorNull
    fa_movi_resp(iResp).analises(iAna).cod_post_dom = ""
    fa_movi_resp(iResp).analises(iAna).localidade_dom = ""
    fa_movi_resp(iResp).analises(iAna).cod_medico = ""
    fa_movi_resp(iResp).analises(iAna).cod_u_saude = ""
End Sub
'edgar.parada  LJMANSO-339 24.04.2019 - campo credencial
Public Function FACTUS_PreencheAna(ByVal iResp As Integer, Optional credencial As String) As Boolean
    Dim ssql As String
    Dim rsAna As New adodb.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    
    fa_movi_resp(iResp).totalAna = 0
    ReDim fa_movi_resp(iResp).analises(0)
    
    ssql = "SELECT a.n_seq_prog, a.cod_prog, a.t_doente, a.doente, a.t_episodio,"
    ssql = ssql & " a.episodio, a.n_ord, a.dt_ini_real, a.dt_fim_real,"
    ssql = ssql & "a.cod_tip_rubr, a.cod_grupo, a.cod_rubr, a.cod_rubr_efr,"
    ssql = ssql & " a.descr_rubr, a.cod_serv_req, a.cod_serv_exec, a.descr_lin_fac,"
    ssql = ssql & " a.val_pr_unit, a.val_pr_unit_2m, a.val_k, a.val_k_2m, a.val_c,"
    ssql = ssql & " a.val_c_2m, a.nr_k, a.nr_c, a.qtd, a.perc_desc_lin,"
    ssql = ssql & " a.perc_pag_efr, a.val_total, a.val_total_2m, a.perc_pag_doe,"
    ssql = ssql & " a.val_pr_u_doe, a.val_pr_u_doe_2m, a.val_pag_doe,"
    ssql = ssql & " a.val_pag_doe_2m, a.val_prev, a.val_prev_2m, a.cod_isen_doe,"
    ssql = ssql & " a.flg_pagou_taxa, a.cod_prov, a.cod_dest, a.nr_req_ars, a.t_int,"
    ssql = ssql & " a.t_cond_esp, a.n_incidencias, a.flg_stdby, a.flg_ventil,"
    ssql = ssql & " a.flg_psiq, a.flg_orig_rubr, a.flg_estado, a.serie_fac, a.n_fac,"
    ssql = ssql & " a.flg_estado_doe, a.serie_fac_doe, a.n_fac_doe,"
    ssql = ssql & " a.cod_mot_rej_anul, a.chave_prog, a.cod_serv_req_gh,"
    ssql = ssql & " a.cod_serv_exec_gh, a.n_doc, a.user_cri, a.dt_cri, a.user_act,"
    ssql = ssql & " a.dt_act, a.val_taxa, a.val_taxa_2m, a.flg_pagou_val_doe,"
    ssql = ssql & " a.flg_ventil_perm, a.t_gdh, a.tab_prog, a.hr_ini_real,"
    ssql = ssql & " a.hr_fim_real, a.nome_interv, a.nome_doe, a.cod_rubr_cir,"
    ssql = ssql & " a.n_ord_ins, a.erro_reg, a.tipo_interv, a.cod_resp, a.n_interv,"
    ssql = ssql & " a.flg_estado_tx, a.serie_fac_tx, a.n_fac_tx, a.flg_erro,"
    ssql = ssql & " a.flg_fact_pat, a.n_seq_prog_orig, a.flg_alt_pr, a.flg_reg_termo,"
    ssql = ssql & " a.empresa_id, a.id_mot_efr, a.id_mot_doe, a.flg_prod_marg,"
    ssql = ssql & " a.flg_tip_req, a.cod_dente, a.unidade, a.perc_desc_lin_doe,"
    ssql = ssql & " a.num_proc_efr, a.flg_prog_urg, a.id_preco_fech, a.val_ref,"
    ssql = ssql & " a.val_pvp, a.t_doente_gh, a.doente_gh, a.t_episodio_gh,"
    ssql = ssql & " a.episodio_gh, a.n_marcadores, a.id_ugc, a.origem_valor,"
    'edgar.parada LJMANSO-352 09.01.2019 parametro id_unico_externo
    'edgar.parada LJMANSO-351 21.01.2019 decode para obter a convencao com base no flg_tip_req
    ssql = ssql & " a.cod_complementar, a.id_unico_externo, b.cod_medico,x1.cod_proven, decode(a.flg_tip_req,'2'," & lCodInternacional & ","
    ssql = ssql & "'0'," & lCodNormal & ",'1'," & lCodProfissional & ",'97'," & lCod97 & ",'3'," & lCodNormalEspecial & ",'5'," & lCodInternacionalEspecial & ","
    ssql = ssql & "'4'," & lCodProfissionalEspecial & ",'0') convencao"
    '
    ssql = ssql & " FROM fa_movi_fact a LEFT OUTER JOIN sl_movi_fact_ADSE b on a.n_seq_prog = b.n_seq_prog "
    'BRUNODSANTOS LJMANSO-307 09.08.2018
    ssql = ssql & ", sl_requis x1 "
    '
    ssql = ssql & " WHERE a.t_episodio = 'SISLAB' AND a.episodio =" & BL_TrataStringParaBD(fa_movi_resp(iResp).episodio)
    ssql = ssql & " AND (a.flg_estado_doe <> 2 or a.flg_estado_doe is null) AND a.n_ord =" & fa_movi_resp(iResp).n_ord
    'BRUNODSANTOS LJMANSO-307 09.08.2018
    ssql = ssql & " AND x1.n_req = a.episodio "
        'edgar.parada  LJMANSO-339 24.04.2019
    If credencial <> "" Then
      ssql = ssql & "AND a.nr_req_ars = " & BL_TrataStringParaBD(credencial)
    End If
    '
    'edgar.parada Glintt-HS-19834 24.10.2018 ordem apenas n_ord_ins
    If gFormActivo.Name = "FormFACTUSConfirmaNovo" Then
        ssql = ssql & " ORDER BY a.nr_req_ars ASC, a.n_ord_ins"
    Else
        ssql = ssql & " ORDER BY a.n_seq_prog"
    End If
    '
    If gModoDebug = 1 Then BG_LogFile_Erros (ssql)

    Set rsAna = New adodb.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.Open ssql, gConexao, adOpenStatic

    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            fa_movi_resp(iResp).totalAna = fa_movi_resp(iResp).totalAna + 1
            ReDim Preserve fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna)
            
            FACTUS_InicializaAna iResp, fa_movi_resp(iResp).totalAna
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_seq_prog = BL_HandleNull(rsAna!n_seq_prog, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_prog = BL_HandleNull(rsAna!cod_prog, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_doente = BL_HandleNull(rsAna!t_doente, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).doente = BL_HandleNull(rsAna!doente, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_episodio = BL_HandleNull(rsAna!t_episodio, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).episodio = BL_HandleNull(rsAna!episodio, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_ord = BL_HandleNull(rsAna!n_ord, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).dt_ini_real = BL_HandleNull(rsAna!dt_ini_real, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).dt_fim_real = BL_HandleNull(rsAna!dt_fim_real, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_tip_rubr = BL_HandleNull(rsAna!cod_tip_rubr, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_grupo = BL_HandleNull(rsAna!cod_grupo, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_rubr = BL_HandleNull(rsAna!cod_rubr, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_rubr_efr = BL_HandleNull(rsAna!cod_rubr_efr, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).descr_rubr = BL_HandleNull(rsAna!descr_rubr, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_serv_req = BL_HandleNull(rsAna!cod_serv_req, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_serv_exec = BL_HandleNull(rsAna!cod_serv_exec, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).descr_lin_fac = BL_HandleNull(rsAna!descr_lin_fac, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_unit = BL_HandleNull(rsAna!val_pr_unit, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_unit_2m = BL_HandleNull(rsAna!val_pr_unit_2m, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_k = BL_HandleNull(rsAna!val_k, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_k_2m = BL_HandleNull(rsAna!val_k_2m, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_c = BL_HandleNull(rsAna!val_c, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_c_2m = BL_HandleNull(rsAna!val_c_2m, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).nr_k = BL_HandleNull(rsAna!nr_k, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).nr_c = BL_HandleNull(rsAna!nr_c, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).qtd = BL_HandleNull(rsAna!qtd, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).perc_desc_lin = BL_HandleNull(rsAna!perc_desc_lin, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).perc_pag_efr = BL_HandleNull(rsAna!perc_pag_efr, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_total = BL_HandleNull(rsAna!val_total, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_total_2m = BL_HandleNull(rsAna!val_total_2m, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).perc_pag_doe = BL_HandleNull(rsAna!perc_pag_doe, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_u_doe = BL_HandleNull(rsAna!val_pr_u_doe, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_u_doe_2m = BL_HandleNull(rsAna!val_pr_u_doe_2m, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pag_doe = BL_HandleNull(rsAna!val_pag_doe, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pag_doe_2m = BL_HandleNull(rsAna!val_pag_doe_2m, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_prev = BL_HandleNull(rsAna!val_prev, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_prev_2m = BL_HandleNull(rsAna!val_prev_2m, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_isen_doe = BL_HandleNull(rsAna!cod_isen_doe, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_pagou_taxa = BL_HandleNull(rsAna!flg_pagou_taxa, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_prov = BL_HandleNull(rsAna!cod_prov, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_dest = BL_HandleNull(rsAna!cod_dest, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).nr_req_ars = BL_HandleNull(rsAna!nr_req_ars, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_int = BL_HandleNull(rsAna!t_int, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_cond_esp = BL_HandleNull(rsAna!t_cond_esp, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_incidencias = BL_HandleNull(rsAna!n_incidencias, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_stdby = BL_HandleNull(rsAna!flg_stdby, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_ventil = BL_HandleNull(rsAna!flg_ventil, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_psiq = BL_HandleNull(rsAna!flg_psiq, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_orig_rubr = BL_HandleNull(rsAna!flg_orig_rubr, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_estado = BL_HandleNull(rsAna!flg_estado, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).serie_fac = BL_HandleNull(rsAna!serie_fac, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_fac = BL_HandleNull(rsAna!n_fac, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_estado_doe = BL_HandleNull(rsAna!flg_estado_doe, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).serie_fac_doe = BL_HandleNull(rsAna!serie_fac_doe, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_Fac_doe = BL_HandleNull(rsAna!n_Fac_doe, mediComboValorNull)
            'fa_movi_resp(iresp).analises(fa_movi_resp(iresp).totalAna).cod_mot_rej_anul = BL_HandleNull(rsAna!cod_mot_rej_anul, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).chave_prog = BL_HandleNull(rsAna!chave_prog, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_serv_req_gh = BL_HandleNull(rsAna!cod_serv_req_gh, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_serv_exec_gh = BL_HandleNull(rsAna!cod_serv_exec_gh, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_doc = BL_HandleNull(rsAna!n_doc, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).user_cri = BL_HandleNull(rsAna!user_cri, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).dt_cri = BL_HandleNull(rsAna!dt_cri, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).user_act = BL_HandleNull(rsAna!user_act, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).dt_act = BL_HandleNull(rsAna!dt_act, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_Taxa = BL_HandleNull(rsAna!val_Taxa, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_taxa_2m = BL_HandleNull(rsAna!val_taxa_2m, 0)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_pagou_val_doe = BL_HandleNull(rsAna!flg_pagou_val_doe, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_ventil_perm = BL_HandleNull(rsAna!flg_ventil_perm, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_gdh = BL_HandleNull(rsAna!t_gdh, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).tab_prog = BL_HandleNull(rsAna!tab_prog, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).hr_ini_real = BL_HandleNull(rsAna!hr_ini_real, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).hr_fim_real = BL_HandleNull(rsAna!hr_fim_real, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).nome_interv = BL_HandleNull(rsAna!nome_interv, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).nome_doe = BL_HandleNull(rsAna!nome_doe, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_rubr_cir = BL_HandleNull(rsAna!cod_rubr_cir, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_ord_ins = BL_HandleNull(rsAna!n_ord_ins, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).erro_reg = BL_HandleNull(rsAna!erro_reg, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).tipo_interv = BL_HandleNull(rsAna!tipo_interv, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_resp = BL_HandleNull(rsAna!cod_resp, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_interv = BL_HandleNull(rsAna!n_interv, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_estado_tx = BL_HandleNull(rsAna!flg_estado_tx, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).serie_fac_tx = BL_HandleNull(rsAna!serie_fac_tx, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_fac_tx = BL_HandleNull(rsAna!n_fac_tx, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_erro = BL_HandleNull(rsAna!flg_erro, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_fact_pat = BL_HandleNull(rsAna!flg_fact_pat, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_seq_prog_orig = BL_HandleNull(rsAna!n_seq_prog_orig, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_alt_pr = BL_HandleNull(rsAna!flg_alt_pr, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_reg_termo = BL_HandleNull(rsAna!flg_reg_termo, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).empresa_id = BL_HandleNull(rsAna!empresa_id, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).id_mot_efr = BL_HandleNull(rsAna!id_mot_efr, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).id_mot_doe = BL_HandleNull(rsAna!id_mot_doe, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_prod_marg = BL_HandleNull(rsAna!flg_prod_marg, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = BL_HandleNull(rsAna!flg_tip_req, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_dente = BL_HandleNull(rsAna!cod_dente, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).unidade = BL_HandleNull(rsAna!unidade, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).perc_desc_lin_doe = BL_HandleNull(rsAna!perc_desc_lin_doe, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).num_proc_efr = BL_HandleNull(rsAna!num_proc_efr, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_prog_urg = BL_HandleNull(rsAna!flg_prog_urg, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).id_preco_fech = BL_HandleNull(rsAna!id_preco_fech, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_ref = BL_HandleNull(rsAna!val_ref, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pvp = BL_HandleNull(rsAna!val_pvp, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_doente_gh = BL_HandleNull(rsAna!t_doente_gh, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).doente_gh = BL_HandleNull(rsAna!doente_gh, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_episodio_gh = BL_HandleNull(rsAna!t_episodio_gh, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).episodio_gh = BL_HandleNull(rsAna!episodio_gh, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_marcadores = BL_HandleNull(rsAna!n_marcadores, mediComboValorNull)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).origem_valor = BL_HandleNull(rsAna!origem_valor, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_medico = BL_HandleNull(rsAna!cod_medico, "")
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_u_saude = BL_HandleNull(rsAna!cod_proven, "")
            'BRUNODSANTOS - LJMANSO-307 09.08.2018
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_dom = FACTUS_DevolveCodUrbanoFromRequis(fa_movi_resp(iResp).episodio)
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = BL_HandleNull(rsAna!convencao, mediComboValorNull)
            '
            'edgar.parada LJMANSO-352 09.01.2019
            fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).id_unico_externo = BL_HandleNull(rsAna!id_unico_externo, "")
            '
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
'    sSql = "SELECT * FROM sd_doente WHERE num_doc IN (SELECT nr_req_ars FROM fa_movi_fact WHERE t_episodio = 'SISLAB' and episodio = " & BL_TrataStringParaBD(fa_movi_resp(iResp).episodio) & ")"
'    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
'
'    Set rsAna = New ADODB.recordset
'    rsAna.CursorLocation = adUseServer
'    rsAna.Open sSql, gConexaoSecundaria, adOpenStatic
'
'    If rsAna.RecordCount > 0 Then
'        While Not rsAna.EOF
'            For i = 1 To fa_movi_resp(iResp).totalAna
'                If fa_movi_resp(iResp).analises(i).nr_req_ars = BL_HandleNull(rsAna!num_doc, "") Then
'                    fa_movi_resp(iResp).analises(i).
'                End If
'            Next i
'            rsAna.Close
'        Wend
'    End If
'    rsAna.Close
    Set rsAna = Nothing
    FACTUS_PreencheAna = True
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao FACTUS_PreencheAna: " & Err.Description, "FACTUS", "FACTUS_PreencheAna", True
    Exit Function
    Resume Next
End Function



Public Function FACTUS_InsereMoviFact(iResp As Integer, iAna As Integer) As Boolean
    Dim ssql As String
    Dim rv As Integer
    On Error GoTo TrataErro
    fa_movi_resp(iResp).analises(iAna).empresa_id = fa_movi_resp(iResp).empresa_id
    IF_SetContext fa_movi_resp(iResp).analises(iAna).empresa_id
    fa_movi_resp(iResp).analises(iAna).n_seq_prog = IF_RetornaNSeqProg
    fa_movi_resp(iResp).analises(iAna).user_cri = CStr(gCodUtilizador)
    
    ssql = "INSERT INTO fa_movi_fact (n_seq_prog, cod_prog, t_doente, doente, t_episodio,episodio, n_ord, dt_ini_real, dt_fim_real,"
    ssql = ssql & " cod_tip_rubr, cod_grupo, cod_rubr, cod_rubr_efr,descr_rubr, cod_serv_req, cod_serv_exec, descr_lin_fac,"
    ssql = ssql & " val_pr_unit,  val_k,  val_c,  nr_k, nr_c, qtd, perc_desc_lin,"
    ssql = ssql & " perc_pag_efr, val_total,  perc_pag_doe, val_pr_u_doe,  val_pag_doe,"
    ssql = ssql & "  val_prev,  cod_isen_doe, flg_pagou_taxa, cod_prov, cod_dest, nr_req_ars, t_int,"
    ssql = ssql & " t_cond_esp, n_incidencias, flg_stdby, flg_ventil, flg_psiq, flg_orig_rubr, flg_estado, serie_fac, n_fac,"
    ssql = ssql & " flg_estado_doe, serie_fac_doe, n_fac_doe,  chave_prog, cod_serv_req_gh,"
    ssql = ssql & " cod_serv_exec_gh, n_doc, user_cri, dt_cri, user_act, dt_act, val_taxa,  flg_pagou_val_doe,"
    ssql = ssql & " flg_ventil_perm, t_gdh, tab_prog, hr_ini_real, hr_fim_real, nome_interv, nome_doe, cod_rubr_cir,"
    ssql = ssql & " n_ord_ins, erro_reg, tipo_interv, cod_resp, n_interv, flg_estado_tx, serie_fac_tx, n_fac_tx, flg_erro,"
    ssql = ssql & " flg_fact_pat, n_seq_prog_orig, flg_alt_pr, flg_reg_termo, empresa_id, id_mot_efr, id_mot_doe, flg_prod_marg,"
    ssql = ssql & " flg_tip_req, cod_dente, unidade, perc_desc_lin_doe, num_proc_efr, flg_prog_urg, id_preco_fech, val_ref, "
    ssql = ssql & " val_pvp, t_doente_gh, doente_gh, t_episodio_gh, episodio_gh, n_marcadores,  origem_valor,id_unico_externo) VALUES("
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_seq_prog)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_prog)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_doente) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).doente) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_episodio) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).episodio) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_ord)) & ","
    ssql = ssql & BL_TrataDataParaBD(fa_movi_resp(iResp).analises(iAna).dt_ini_real) & ","
    ssql = ssql & BL_TrataDataParaBD(fa_movi_resp(iResp).analises(iAna).dt_fim_real) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_tip_rubr)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_grupo)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_rubr)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_rubr_efr) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).descr_rubr) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_serv_req)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_serv_exec)) & ","
    ssql = ssql & BL_TrataStringParaBD(Mid(fa_movi_resp(iResp).analises(iAna).descr_lin_fac, 1, 20)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pr_unit)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_k)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_c)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).nr_k)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).nr_c)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).qtd)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_desc_lin)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_pag_efr)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_total)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_pag_doe)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pr_u_doe)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pag_doe)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_prev)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_isen_doe)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_pagou_taxa) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_prov) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_dest) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).nr_req_ars) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).t_int)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_cond_esp) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_incidencias)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_stdby)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_ventil) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_psiq) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_orig_rubr) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_estado)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).serie_fac) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_fac)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_estado_doe)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).serie_fac_doe) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_Fac_doe)) & ","
    ssql = ssql & BL_TrataStringParaBD(Mid(fa_movi_resp(iResp).analises(iAna).chave_prog, 1, 15)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_serv_req_gh) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_serv_exec_gh) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).n_doc) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).user_cri) & ",sysdate, null, null,"
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_Taxa)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_pagou_val_doe) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_ventil_perm)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).t_gdh)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).tab_prog) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).hr_ini_real) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).hr_fim_real) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).nome_interv) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).nome_doe) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_rubr_cir)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_ord_ins)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).erro_reg) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).tipo_interv) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_resp)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).n_interv) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_estado_tx)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).serie_fac_tx) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_fac_tx)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_erro) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_fact_pat) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_seq_prog_orig)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_alt_pr) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_reg_termo) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).empresa_id) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).id_mot_efr)) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).id_mot_doe)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_prod_marg) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_tip_req) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_dente) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).unidade) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_desc_lin_doe)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).num_proc_efr) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_prog_urg) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).id_preco_fech)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_ref)) & ","
    ssql = ssql & BL_TrataDoubleParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pvp)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_doente_gh) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).doente_gh) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_episodio_gh) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).episodio_gh) & ","
    ssql = ssql & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_marcadores)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).origem_valor) & ","
        ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).id_unico_externo) & ")"
    
    rv = BL_ExecutaQuery_Secundaria(ssql)
    If rv < 1 Then
        BG_LogFile_Erros "Erro inserir FA_MOVI_FACT: " & ssql & vbCrLf & Err.Description, "FACTUS", "FACTUS_InsereMoviFact", True
    Else
        If FACTUS_InsereMoviFactADSE(iResp, iAna) = True Then
            FACTUS_InsereMoviFact = True
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao FACTUS_InsereMoviFact: " & Err.Description, "FACTUS", "FACTUS_InsereMoviFact", True
    Exit Function
    Resume Next
End Function


Public Function FACTUS_ActualizaMoviFact(iResp As Integer, iAna As Integer) As Boolean
    Dim ssql As String
    Dim rv As Integer
    On Error GoTo TrataErro
    fa_movi_resp(iResp).analises(iAna).user_act = CStr(gCodUtilizador)
    ssql = "UPDATE fa_movi_fact SET "
    ssql = ssql & "n_Seq_prog = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_seq_prog)) & ","
    ssql = ssql & "cod_prog = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_prog)) & ","
    ssql = ssql & "t_doente = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_doente) & ","
    ssql = ssql & "doente = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).doente) & ","
    ssql = ssql & "t_episodio = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_episodio) & ","
    ssql = ssql & "episodio = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).episodio) & ","
    ssql = ssql & "n_ord = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_ord)) & ","
    ssql = ssql & "dt_ini_real = " & BL_TrataDataParaBD(fa_movi_resp(iResp).analises(iAna).dt_ini_real) & ","
    ssql = ssql & "dt_fim_real = " & BL_TrataDataParaBD(fa_movi_resp(iResp).analises(iAna).dt_fim_real) & ","
    ssql = ssql & "cod_tip_rubr = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_tip_rubr)) & ","
    ssql = ssql & "cod_grupo = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_grupo)) & ","
    ssql = ssql & "cod_rubr = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_rubr)) & ","
    ssql = ssql & "cod_rubr_efr = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_rubr_efr) & ","
    ssql = ssql & "descr_rubr = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).descr_rubr) & ","
    ssql = ssql & "cod_serv_req = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_serv_req)) & ","
    ssql = ssql & "cod_serv_exec = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_serv_exec)) & ","
    ssql = ssql & "descr_lin_fac = " & BL_TrataStringParaBD(Mid(fa_movi_resp(iResp).analises(iAna).descr_lin_fac, 1, 20)) & ","
    ssql = ssql & "val_pr_unit = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pr_unit)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pr_unit))) & ","
    ssql = ssql & "val_k = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_k)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_k))) & ","
    ssql = ssql & "val_c = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_c)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_c))) & ","
    ssql = ssql & "nr_k = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).nr_k)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).nr_k))) & ","
    ssql = ssql & "nr_c = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).nr_c)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).nr_c))) & ","
        ssql = ssql & "qtd = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).qtd)) & ","
    ssql = ssql & "perc_desc_lin = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_desc_lin)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_desc_lin))) & ","
    ssql = ssql & "perc_pag_efr = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_pag_efr)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_pag_efr))) & ","
    ssql = ssql & "val_total = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_total)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_total))) & ","
    ssql = ssql & "perc_pag_doe = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_pag_doe)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_pag_doe))) & ","
    ssql = ssql & "val_pr_u_doe = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pr_u_doe)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pr_u_doe))) & ","
    ssql = ssql & "val_pag_doe = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pag_doe)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pag_doe))) & ","
    ssql = ssql & "val_prev = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_prev)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_prev))) & ","
    ssql = ssql & "cod_isen_doe = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_isen_doe)) & ","
    ssql = ssql & "flg_pagou_taxa = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_pagou_taxa) & ","
    ssql = ssql & "cod_prov = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_prov) & ","
    ssql = ssql & "cod_dest = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_dest) & ","
    ssql = ssql & "nr_req_ars = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).nr_req_ars) & ","
    ssql = ssql & "t_int = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).t_int)) & ","
    ssql = ssql & "t_cond_esp = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_cond_esp) & ","
    ssql = ssql & "n_incidencias = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_incidencias)) & ","
    ssql = ssql & "flg_stdby = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_stdby)) & ","
    ssql = ssql & "flg_ventil = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_ventil) & ","
    ssql = ssql & "flg_psiq = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_psiq) & ","
    ssql = ssql & "flg_orig_rubr = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_orig_rubr) & ","
    ssql = ssql & "flg_estado = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_estado)) & ","
    ssql = ssql & "serie_fac = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).serie_fac) & ","
    ssql = ssql & "n_fac = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_fac)) & ","
    ssql = ssql & "flg_estado_doe = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_estado_doe)) & ","
    ssql = ssql & "serie_fac_doe = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).serie_fac_doe) & ","
    ssql = ssql & "n_Fac_doe = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_Fac_doe)) & ","
    ssql = ssql & "chave_prog = " & BL_TrataStringParaBD(Mid(fa_movi_resp(iResp).analises(iAna).chave_prog, 1, 15)) & ","
    ssql = ssql & "cod_serv_req_gh = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_serv_req_gh) & ","
    ssql = ssql & "cod_serv_exec_gh = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_serv_exec_gh) & ","
    ssql = ssql & "n_doc = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).n_doc) & ","
    ssql = ssql & "user_act = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).user_act) & ","
    ssql = ssql & "dt_act = SYSDATE, "
    ssql = ssql & "val_Taxa = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_Taxa)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_Taxa))) & ","
    ssql = ssql & "flg_pagou_val_doe = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_pagou_val_doe) & ","
    ssql = ssql & "flg_ventil_perm = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_ventil_perm)) & ","
    ssql = ssql & "t_gdh = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).t_gdh)) & ","
    ssql = ssql & "tab_prog = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).tab_prog) & ","
    ssql = ssql & "hr_ini_real = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).hr_ini_real) & ","
    ssql = ssql & "hr_fim_real = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).hr_fim_real) & ","
    ssql = ssql & "nome_interv = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).nome_interv) & ","
    ssql = ssql & "nome_doe = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).nome_doe) & ","
    ssql = ssql & "cod_rubr_cir = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_rubr_cir)) & ","
    ssql = ssql & "n_ord_ins = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_ord_ins)) & ","
    ssql = ssql & "erro_reg = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).erro_reg) & ","
    ssql = ssql & "tipo_interv = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).tipo_interv) & ","
    ssql = ssql & "cod_resp = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).cod_resp)) & ","
    ssql = ssql & "n_interv = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).n_interv) & ","
    ssql = ssql & "flg_estado_tx = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).flg_estado_tx)) & ","
    ssql = ssql & "serie_fac_tx = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).serie_fac_tx) & ","
    ssql = ssql & "n_fac_tx = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_fac_tx)) & ","
    ssql = ssql & "flg_erro = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_erro) & ","
    ssql = ssql & "flg_fact_pat = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_fact_pat) & ","
    ssql = ssql & "n_seq_prog_orig = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_seq_prog_orig)) & ","
    ssql = ssql & "flg_alt_pr = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_alt_pr) & ","
    ssql = ssql & "flg_reg_termo = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_reg_termo) & ","
    ssql = ssql & "empresa_id = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).empresa_id) & ","
    ssql = ssql & "id_mot_efr = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).id_mot_efr)) & ","
    ssql = ssql & "id_mot_doe = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).id_mot_doe)) & ","
    ssql = ssql & "flg_prod_marg = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_prod_marg) & ","
    ssql = ssql & "flg_tip_req = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_tip_req) & ","
    ssql = ssql & "cod_dente = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).cod_dente) & ","
    ssql = ssql & "unidade = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).unidade) & ","
    ssql = ssql & "perc_desc_lin_doe = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_desc_lin_doe)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).perc_desc_lin_doe))) & ","
    ssql = ssql & "num_proc_efr = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).num_proc_efr) & ","
    ssql = ssql & "flg_prog_urg = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).flg_prog_urg) & ","
    ssql = ssql & "id_preco_fech = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).id_preco_fech)) & ","
    ssql = ssql & "val_ref = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_ref)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_ref))) & ","
    ssql = ssql & "val_pvp = " & IIf(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pvp)) = "0", "NULL", BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).val_pvp))) & ","
    ssql = ssql & "t_doente_gh = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_doente_gh) & ","
    ssql = ssql & "doente_gh = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).doente_gh) & ","
    ssql = ssql & "t_episodio_gh = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).t_episodio_gh) & ","
    ssql = ssql & "episodio_gh = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).episodio_gh) & ","
    ssql = ssql & "n_marcadores = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAna).n_marcadores)) & ","
    ssql = ssql & "origem_valor = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).origem_valor) & ","
    ssql = ssql & "id_unico_externo = " & IIf((fa_movi_resp(iResp).analises(iAna).id_unico_externo) = "-1", "''", BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAna).id_unico_externo))
    ssql = ssql & " WHERE n_seq_prog = " & fa_movi_resp(iResp).analises(iAna).n_seq_prog
    rv = BL_ExecutaQuery_Secundaria(ssql)
    If rv < 1 Then
        BG_LogFile_Erros "Erro atualizar FA_MOVI_FACT: " & ssql & vbCrLf & Err.Description, "FACTUS", "FACTUS_ActualizaMoviFact", True
    Else
        If FACTUS_AtualizaMoviFactADSE(iResp, iAna) = True Then
            FACTUS_ActualizaMoviFact = True
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao FACTUS_ActualizaMoviFact: " & Err.Description, "FACTUS", "FACTUS_ActualizaMoviFact", True
    Exit Function
    Resume Next
End Function


Private Function FACTUS_AdicionaEFR(cod_efr As String, n_benef_ute As String, t_doente As String, doente As String, n_req As String) As Integer
    On Error GoTo TrataErro
    Dim ssql As String
    Dim empresa_id As String
    Dim rs_aux As New adodb.recordset
    Dim i As Integer
    For i = 1 To fa_movi_resp_tot
        If fa_movi_resp(i).cod_efr = cod_efr And empresa_id = fa_movi_resp(i).empresa_id Then
            FACTUS_AdicionaEFR = i
            Exit Function
        End If
    Next i
    
    empresa_id = FACTUS_DevolveEmpresa(CLng(cod_efr), Bg_DaData_ADO)
    If empresa_id = "" Then
        FACTUS_AdicionaEFR = mediComboValorNull
        Exit Function
    End If
    fa_movi_resp_tot = fa_movi_resp_tot + 1
    ReDim Preserve fa_movi_resp(fa_movi_resp_tot)
    ssql = "SELECT * FROM sl_efr WHERE cod_efr = " & cod_efr
    Set rs_aux = New adodb.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rs_aux.Open ssql, gConexao, adOpenStatic
    If rs_aux.RecordCount <> 0 Then
    
        fa_movi_resp(fa_movi_resp_tot).cod_efr = cod_efr
        fa_movi_resp(fa_movi_resp_tot).descr_efr = BL_HandleNull(rs_aux!descr_efr, "")
        fa_movi_resp(fa_movi_resp_tot).empresa_id = empresa_id
        fa_movi_resp(fa_movi_resp_tot).n_benef_doe = n_benef_ute
        fa_movi_resp(fa_movi_resp_tot).t_doente = t_doente
        fa_movi_resp(fa_movi_resp_tot).doente = doente
        fa_movi_resp(fa_movi_resp_tot).t_episodio = "SISLAB"
        fa_movi_resp(fa_movi_resp_tot).episodio = n_req
        fa_movi_resp(fa_movi_resp_tot).n_ord = FACTUS_RetornaMaxNOrd(fa_movi_resp_tot)
        fa_movi_resp(fa_movi_resp_tot).flg_compart_dom = BL_HandleNull(rs_aux!flg_compart_dom, 0)
        fa_movi_resp(fa_movi_resp_tot).flg_dom_defeito = BL_HandleNull(rs_aux!flg_dom_defeito, 0)
        fa_movi_resp(fa_movi_resp_tot).flg_novaArs = BL_HandleNull(rs_aux!flg_nova_ars, 0)
        fa_movi_resp(fa_movi_resp_tot).per_paga = mediComboValorNull
        fa_movi_resp(fa_movi_resp_tot).val_franq = mediComboValorNull
        fa_movi_resp(fa_movi_resp_tot).val_max = mediComboValorNull
        fa_movi_resp(fa_movi_resp_tot).perc_desc = mediComboValorNull
        fa_movi_resp(fa_movi_resp_tot).val_total = mediComboValorNull
        fa_movi_resp(fa_movi_resp_tot).perc_franq = mediComboValorNull
        fa_movi_resp(fa_movi_resp_tot).val_min_franq = mediComboValorNull
        fa_movi_resp(fa_movi_resp_tot).val_max_franq = mediComboValorNull
        
        FACTUS_AdicionaEFR = fa_movi_resp_tot
    End If
    rs_aux.Close
    Set rs_aux = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AdicionaEFR"
    FACTUS_AdicionaEFR = mediComboValorNull
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' FACTURACAO DE UMA ANALISE ( INSERT NA FA_MOVI_FACT ) PRIVADO!!

' -----------------------------------------------------------------------------------------------
Public Function FACTUS_AdicionaAnaliseDet(ByVal cod_efr As String, ByVal cod_ana As String, _
                                  ByVal p_data As String, ByVal descontoDoe As Double, ByVal cod_hemodialise As Integer, _
                                  ByVal cod_isencao As Integer, ByVal iAna As Integer, ByVal n_benef_ute As String, ByVal t_doente As String, ByVal doente As String, _
                                  ByVal n_req As String, ByVal credencial As String, ByVal convencao As Integer, ByVal cod_medico As String, _
                                  ByVal cod_dom As Integer, ByVal km_dom As Integer, ByVal cod_post_dom As String, ByVal localidade_dom As String, _
                                  ByVal cod_u_saude As String, ByVal cod_rubr As Long) As Integer
    Dim rv As Integer
    Dim anaFacturar As String
    Dim iResp As Integer
    Dim iva As Integer
    Dim resFact As Integer
    Dim maxOrd As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    
    '-----------------------------------------------------------------------
    ' VERIFICA SE JA EXISTE CABE�ALHO NA FA_MOVI_RESP. SE NAO EXISTIR, CRIA
    '-----------------------------------------------------------------------
    iResp = FACTUS_DevolveLinhaResp(n_req, cod_efr)

    If iResp = mediComboValorNull Then
        iResp = FACTUS_AdicionaEFR(cod_efr, n_benef_ute, t_doente, doente, n_req)
    End If
    If iResp <= 0 Then
        Exit Function
    End If
    fa_movi_resp(iResp).totalAna = fa_movi_resp(iResp).totalAna + 1
    ReDim Preserve fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna)
    
    FACTUS_InicializaAna iResp, fa_movi_resp(iResp).totalAna
    
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).iAnalise = iAna
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_doente = fa_movi_resp(iResp).t_doente
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).doente = fa_movi_resp(iResp).doente
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).t_episodio = fa_movi_resp(iResp).t_episodio
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).episodio = fa_movi_resp(iResp).episodio
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).dt_ini_real = p_data
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).dt_fim_real = p_data
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_prog = gCodProg
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_ord = fa_movi_resp(iResp).n_ord
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_serv_exec = gCodServExec
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_estado = 1
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_estado_doe = mediComboValorNull
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).nr_req_ars = credencial
    maxOrd = 0
    For i = 1 To fa_movi_resp(iResp).totalAna
        If fa_movi_resp(iResp).analises(i).nr_req_ars = fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).nr_req_ars And i <> fa_movi_resp(iResp).totalAna Then
            If fa_movi_resp(iResp).analises(i).n_ord_ins > maxOrd Then
                maxOrd = fa_movi_resp(iResp).analises(i).n_ord_ins
            End If
        End If
    Next i
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).n_ord_ins = maxOrd + 1
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_medico = cod_medico
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_u_saude = cod_u_saude
    
    If convencao = lCodInternacional Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "2"
        'edgar.parada LJMANSO-351 11.01.2019
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = lCodInternacional
        '
    ElseIf convencao = lCodNormal Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "0"
        'edgar.parada LJMANSO-351 11.01.2019
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = lCodNormal
        '
    ElseIf convencao = lCodProfissional Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "1"
        'edgar.parada LJMANSO-351 11.01.2019
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = lCodProfissional
        '
    'edgar.parada LOTE 97 - Conven��o
    ElseIf convencao = lCod97 Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "97"
    '
        'edgar.parada LJMANSO-351 11.01.2019
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = lCod97
        '
    'edgar.parada LJMANSO-351 11.01.2019
    ElseIf convencao = lCodNormalEspecial Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "3"
        'edgar.parada LJMANSO-351 11.01.2019
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = lCodNormalEspecial
        '
    ElseIf convencao = lCodInternacionalEspecial Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "5"
        'edgar.parada LJMANSO-351 11.01.2019
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = lCodInternacionalEspecial
        '
    ElseIf convencao = lCodProfissionalEspecial Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "4"
        'edgar.parada LJMANSO-351 11.01.2019
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).convencao = lCodProfissionalEspecial
        '
    End If
    
    
    If Len(credencial) = 19 Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).chave_prog = credencial
    Else
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).chave_prog = fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).episodio & "_" & credencial
    End If
    
    '-----------------------------------------------------------------------
    ' VERIFICA SE ANALISE MAPEADA PARA FACTUS
    '-----------------------------------------------------------------------
    If IF_AnaliseMapeada(cod_efr, cod_ana) = False Then
        BG_LogFile_Erros "1. An�lise n�o mapeada para FACTUS:" & cod_ana, "FACTUS", "FACTUS_AdicionaAnaliseDet"
        FACTUS_AdicionaAnaliseDet = mediComboValorNull
        Exit Function
    End If
    
    '-----------------------------------------------------------------------
    ' VERIFICA SE PRETENDE FATURAR OUTRAS ANALISES
    '-----------------------------------------------------------------------
    anaFacturar = IF_VerificaFacturarNovaAnalise(cod_ana, cod_efr)
    If anaFacturar <> "" Then
        gMsgTitulo = "Inserir"
        gMsgMsg = "Quer facturar a an�lise: " & BL_SelCodigo("SLV_ANALISES_FACTUS", "DESCR_ANA", "COD_ANA", anaFacturar) & "?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
        If gMsgResp = vbYes Then
        ' FAZ CENAS;
        End If
    End If
    
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_isen_doe = cod_isencao
    
    resFact = FACTUS_PreencheValoresAna(iResp, fa_movi_resp(iResp).totalAna, cod_ana, p_data, cod_hemodialise, descontoDoe, 1, cod_rubr)
    If resFact < 0 And gEfrParticular <> mediComboValorNull And fa_movi_resp(iResp).cod_efr <> gEfrParticular Then
    
        If resFact = -4 Then
            If FACTUS_EliminaAna(iResp, fa_movi_resp(iResp).totalAna, True) = False Then
                BG_Mensagem mediMsgBox, "Erro ao alterar an�lise.", vbOKOnly + vbInformation, "Particular"
            
            End If
        End If
        FACTUS_AdicionaAnaliseDet = resFact
        Exit Function
    End If
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_grupo = FACTUS_DevolveGrupoRubr(fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_rubr)
    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_tip_rubr = FACTUS_DevolveTipoRubr(fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_grupo)
    iva = IF_RetornaIVA(fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).empresa_id, CStr(fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).cod_grupo))
    If iva > 0 Then
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_u_doe = BG_newRound(((fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_u_doe * iva) / 100) + fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_u_doe, 2)
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pag_doe = BG_newRound(((fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pag_doe * iva) / 100) + fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pag_doe, 2)
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_unit = BG_newRound(((fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_unit * iva) / 100) + fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_pr_unit, 2)
        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_total = BG_newRound(((fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_total * iva) / 100) + fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).val_total, 2)
    End If
    
    FACTUS_AdicionaAnaliseDet = mediSim
Exit Function
TrataErro:
    FACTUS_AdicionaAnaliseDet = 0
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AdicionaAnaliseDet"
    Exit Function
    Resume Next
End Function

Public Function FACTUS_AdicionaAnalise(ByVal cod_efr As String, ByVal cod_ana As String, _
                                  ByVal p_data As String, ByVal descontoDoe As Double, ByVal cod_hemodialise As Integer, _
                                  ByVal cod_isencao As Integer, ByVal iAna As Integer, ByVal n_benef_ute As String, ByVal t_doente As String, ByVal doente As String, _
                                  ByVal n_req As String, ByVal credencial As String, ByVal convencao As Integer, ByVal cod_medico As String, _
                                  ByVal cod_dom As Integer, ByVal km_dom As Integer, ByVal cod_post_dom As String, ByVal localidade_dom As String, _
                                  ByVal cod_u_saude As String, ByVal cod_rubr As Long) As Boolean
 
    Dim iRes As Integer
    Dim iResp As Integer
    Dim encontrou As Boolean
    Dim i As Integer
    
    iRes = FACTUS_AdicionaAnaliseDet(cod_efr, cod_ana, p_data, descontoDoe, cod_hemodialise, cod_isencao, iAna, n_benef_ute, t_doente, _
                                 doente, n_req, credencial, convencao, cod_medico, cod_dom, km_dom, cod_post_dom, localidade_dom, cod_u_saude, cod_rubr)
    If iRes > 0 Then
        FACTUS_AdicionaAnalise = True
    Else
        If iRes = -4 And cod_efr <> gEfrParticular Then
            'NELSONPSILVA UALIA-889
            iResp = FACTUS_DevolveLinhaResp(n_req, gEfrParticular)
            If iResp <> mediComboValorNull Then
                For i = 1 To fa_movi_resp(iResp).totalAna
                    If fa_movi_resp(iResp).analises(i).cod_rubr = FACTUS_DevolveRubrFactus(cod_ana) Then
                        encontrou = True
                        Exit For
                    End If
                Next i
            End If
            
            If encontrou = False Then
                    'FGONCALVES_UALIA
                    iRes = FACTUS_AdicionaAnaliseDet(gEfrParticular, cod_ana, p_data, descontoDoe, cod_hemodialise, cod_isencao, iAna, n_benef_ute, t_doente, _
                                 doente, n_req, credencial, convencao, cod_medico, cod_dom, km_dom, cod_post_dom, localidade_dom, cod_u_saude, cod_rubr)
                                 
                    If iRes = mediSim Or iRes = mediComboValorNull Then
                        FACTUS_AdicionaAnalise = True
                    Else
                        FACTUS_AdicionaAnalise = False
                    End If
            Else
                FACTUS_AdicionaAnalise = False
            End If
        Else
            FACTUS_AdicionaAnalise = False
        End If
    End If
Exit Function
TrataErro:
    FACTUS_AdicionaAnalise = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AdicionaAnalise"
    Exit Function
    Resume Next
End Function

Public Function FACTUS_DevolveLinhaResp(episodio As String, cod_efr As String) As Integer
    Dim i As Integer
    For i = 1 To UBound(fa_movi_resp)
        If fa_movi_resp(i).cod_efr = cod_efr And fa_movi_resp(i).episodio = episodio Then
            FACTUS_DevolveLinhaResp = i
            Exit Function
        End If
    Next i
    FACTUS_DevolveLinhaResp = mediComboValorNull
End Function

'------------------------------------------------------------------------------------------------
' VAI AO FACTUS BUSCAR O COD_GRUPO
' -----------------------------------------------------------------------------------------------
Private Function FACTUS_DevolveGrupoRubr(cod_rubr As Long) As Integer
    Dim ssql As String
    Dim rs_aux As New adodb.recordset
    
    ssql = "SELECT cod_grupo FROM fa_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(CStr(cod_rubr)) & ""
    Set rs_aux = New adodb.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rs_aux.Open ssql, gConexaoSecundaria, adOpenStatic
    If rs_aux.RecordCount <> 0 Then
        FACTUS_DevolveGrupoRubr = BL_HandleNull(rs_aux!cod_grupo, "0")
    Else
        FACTUS_DevolveGrupoRubr = 0
    End If
    rs_aux.Close
    Set rs_aux = Nothing

Exit Function
TrataErro:
    FACTUS_DevolveGrupoRubr = 0
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_DevolveGrupoRubr"
    Exit Function
    Resume Next
End Function

'------------------------------------------------------------------------------------------------
' VAI AO FACTUS BUSCAR O TIPO de RUBRICA
' -----------------------------------------------------------------------------------------------
Private Function FACTUS_DevolveTipoRubr(cod_grupo As Long) As Integer
    Dim ssql As String
    Dim rs_aux As New adodb.recordset
    On Error GoTo TrataErro
    
    ssql = "SELECT cod_tip_rubr FROM fa_grup_rubr WHERE cod_grupo = " & BL_TrataStringParaBD(CStr(cod_grupo)) & ""
    Set rs_aux = New adodb.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rs_aux.Open ssql, gConexaoSecundaria, adOpenStatic
    
    If rs_aux.RecordCount <> 0 Then
        FACTUS_DevolveTipoRubr = BL_HandleNull(rs_aux!cod_tip_rubr, mediComboValorNull)
    Else
        FACTUS_DevolveTipoRubr = mediComboValorNull
    End If

Exit Function
TrataErro:
    FACTUS_DevolveTipoRubr = mediComboValorNull
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_DevolveTipoRubr"
    Exit Function
    Resume Next
End Function



Private Function FACTUS_PreencheValoresAna(iResp As Integer, iAna As Integer, _
                                           cod_ana As String, p_data As String, cod_hemodialise As Integer, _
                                           desconto_utente As Double, qtd As Integer, cod_rubr As Long) As Integer
    Dim cod_sub_prec As String
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    Dim rsAnaFact As New adodb.recordset
    Dim rsEntidades As New adodb.recordset
    Dim rsRubr As New adodb.recordset
    Dim rsTaxas As New adodb.recordset
    Dim iBdAberta As Integer
    Dim rsPortarias As New adodb.recordset
    Dim flgPercentagem As Boolean
    Dim i As Integer
    Dim Flg_UsaPrecoEntidade  As Boolean
    Dim cod_ana_gh As Long
    
    On Error GoTo TrataErro

    If fa_movi_resp(iResp).analises(iAna).cod_isen_doe = gTipoIsencaoNaoIsento Or fa_movi_resp(iResp).analises(iAna).cod_isen_doe = gTipoIsencaoIsento Or cod_ana = GCodAnaActMed Then
        ssql = "SELECT * FROM sl_efr WHERE cod_efr = " & fa_movi_resp(iResp).cod_efr
        RsEFR.CursorLocation = adUseServer
        RsEFR.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        RsEFR.Open ssql, gConexao
        If RsEFR.RecordCount > 0 Then
            
            flgPercentagem = BL_HandleNull(RsEFR!flg_perc_facturar, 0)
            
            'VERIFICA MAPEAMENTO PARA FACTUS
            ssql = "SELECT cod_ana_gh,flg_conta_membros FROM sl_ana_facturacao WHERE cod_efr = " & fa_movi_resp(iResp).cod_efr & " AND  cod_ana = " & BL_TrataStringParaBD(cod_ana)
            ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
            ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAnaFact.CursorLocation = adUseServer
            rsAnaFact.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAnaFact.Open ssql, gConexao
            If rsAnaFact.RecordCount <= 0 Then
                rsAnaFact.Close
                ssql = "SELECT cod_ana_gh, flg_conta_membros,qtd FROM sl_ana_facturacao WHERE cod_efr IS NULL AND cod_ana = " & BL_TrataStringParaBD(cod_ana)
                ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
                ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
                rsAnaFact.CursorLocation = adUseServer
                rsAnaFact.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros ssql
                rsAnaFact.Open ssql, gConexao
                If rsAnaFact.RecordCount <= 0 Then
                    If cod_rubr <> mediComboValorNull And cod_ana = "" Then
                        cod_ana_gh = cod_rubr
                        rsAnaFact.Close
                        Set rsAnaFact = Nothing
                    Else
                        ' ANALISE NAO ESTA MAPEADA PARA FACTUS
                        FACTUS_PreencheValoresAna = "-1"
                        rsAnaFact.Close
                        Set rsAnaFact = Nothing
                        Exit Function
                    End If
                Else
                    cod_ana_gh = BL_HandleNull(rsAnaFact!cod_ana_gh, mediComboValorNull)
                    fa_movi_resp(iResp).analises(iAna).qtd = BL_HandleNull(rsAnaFact!qtd, 1)
                    
                End If
            Else
                cod_ana_gh = BL_HandleNull(rsAnaFact!cod_ana_gh, mediComboValorNull)
                fa_movi_resp(iResp).analises(iAna).qtd = qtd
            End If
            ' -------------------------------------------------------
            ' PARA ALGUMAS ENTIDADES O PRECO PARA RECIBO = ENTIDADE
            ' -------------------------------------------------------
            If BL_HandleNull(RsEFR!flg_usa_preco_ent, 0) = mediSim Then
                Flg_UsaPrecoEntidade = True
            Else
                Flg_UsaPrecoEntidade = False
            End If
            
            
            FACTUS_PreencheValoresAna = FACTUS_PreencheValoresanaDet(iResp, iAna, cod_ana_gh, desconto_utente, _
                                                                     qtd, p_data, Flg_UsaPrecoEntidade, flgPercentagem)
            
        End If
                'LJMANSO-339
        If rsAnaFact.state = adStateOpen Then
          rsAnaFact.Close
          Set rsAnaFact = Nothing
                End If
    End If
        'LJMANSO-339
    If RsEFR.state = adStateOpen Then
      RsEFR.Close
      Set RsEFR = Nothing
        End If
        
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_PreencheValoresAna"
    Exit Function
    Resume Next
End Function

Public Function FACTUS_PreencheValoresanaDet(iResp As Integer, iAna As Integer, cod_rubr As Long, desconto_utente As Double, qtd As Integer, _
                                            data As String, Flg_UsaPrecoEntidade As Boolean, flgPercentagem As Boolean) As Integer
    Dim cod_sub_prec As String
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    Dim rsAnaFact As New adodb.recordset
    Dim rsEntidades As New adodb.recordset
    Dim rsRubr As New adodb.recordset
    Dim rsTaxas As New adodb.recordset
    Dim iBdAberta As Integer
    Dim rsPortarias As New adodb.recordset
    Dim tab_subPrecario As Long
    Dim perc_desc As Double
    Dim perc_pag_doe As Double
    Dim valor_k As Double
    Dim valor_c As Double
    Dim tab_utilizada As Long
    Dim i As Integer
    On Error GoTo TrataErro
    
    ' ABRE CONEXAO COM FACTUS
    iBdAberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iBdAberta = 0 Then
        ' NAO EXISTE CONEXAO FACTUS
        FACTUS_PreencheValoresanaDet = "-2"
    End If
            
    ' DADOS DE RUBRICAS
    ssql = "SELECT cod_rubr, descr_rubr FROM fa_rubr WHERE cod_rubr = " & cod_rubr
    rsRubr.CursorLocation = adUseServer
    rsRubr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRubr.Open ssql, gConexao
    If rsRubr.RecordCount = 1 Then
        fa_movi_resp(iResp).analises(iAna).cod_rubr = CStr(BL_HandleNull(rsRubr!cod_rubr, 0))
        fa_movi_resp(iResp).analises(iAna).descr_rubr = CStr(BL_HandleNull(rsRubr!descr_rubr, ""))
    End If
    rsRubr.Close
    Set rsRubr = Nothing
            
            
    cod_sub_prec = IF_VerificaUsaSubPrecario(CStr(fa_movi_resp(iResp).cod_efr), data)
    If cod_sub_prec <> "" Then
        tab_subPrecario = IF_RetornaEntidadeRef(cod_sub_prec, CStr(fa_movi_resp(iResp).cod_efr), perc_desc, perc_pag_doe)
    End If
            
    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    ssql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & CStr(fa_movi_resp(iResp).cod_efr)
    rsEntidades.CursorLocation = adUseServer
    rsEntidades.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsEntidades.Open ssql, gConexaoSecundaria
    If rsEntidades.RecordCount <= 0 Then
        ' NAO EXISTE ENTIDADE CODIFICADA
        FACTUS_PreencheValoresanaDet = "-5"
    Else
        'UALIA-911
        If cod_sub_prec = "" Then
            tab_utilizada = BL_HandleNull(rsEntidades!tab_utilizada, "0")
            cod_sub_prec = IF_VerificaUsaSubPrecario(CStr(tab_utilizada), data)
            If cod_sub_prec <> "" Then
                tab_subPrecario = IF_RetornaEntidadeRef(cod_sub_prec, CStr(tab_utilizada), perc_desc, perc_pag_doe)
            End If
        End If
        ' --------------------------------------------------------
        ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
        ' --------------------------------------------------------
        ssql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(data)
        ssql = ssql & " AND dt_fim >=" & BL_TrataStringParaBD(data) & "))"
        'NELSONPSILVA UALIA-868
        If cod_sub_prec <> "" Then
            ssql = ssql & " AND cod_efr = " & tab_subPrecario & " AND user_rem is null  "
        Else
            ssql = ssql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null  "
        End If
        '
        ssql = ssql & " AND (empresa_id IS NULL OR empresa_id = " & BL_TrataStringParaBD(CStr(fa_movi_resp(iResp).empresa_id)) & ")"
    
        rsPortarias.CursorLocation = adUseServer
        rsPortarias.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsPortarias.Open ssql, gConexaoSecundaria
        If rsPortarias.RecordCount <= 0 Then
            ' NAO EXISTE PORTARIA ACTIVA
            FACTUS_PreencheValoresanaDet = "-3"
        Else
            ' --------------------------------------
            ' RETORNA A TAXA PARA A ANALISE EM CAUSA
            ' --------------------------------------
            ssql = "SELECT x1.val_taxa,x1.val_pag_doe, x1.cod_rubr_efr, x1.val_pag_ent,x1.descr_rubr_efr, x2.cod_rubr, x2.descr_rubr, nvl(x1.nr_c,0) nr_c, nvl(x1.nr_k,0) nr_k FROM fa_pr_rubr x1 join fa_rubr x2 on x1.cod_rubr =x2.cod_rubr"
            ssql = ssql & " WHERE x1.cod_rubr = " & BL_TrataStringParaBD(CStr(cod_rubr))
            ssql = ssql & " AND x1.portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria) & " AND x1.user_rem IS NULL "
            rsTaxas.CursorLocation = adUseServer
            rsTaxas.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsTaxas.Open ssql, gConexaoSecundaria
            If rsTaxas.RecordCount <= 0 Then
                ' NAO EXISTE TAXA CODIFICADA
                FACTUS_PreencheValoresanaDet = "-4"
            Else
                'UALIA-867
                fa_movi_resp(iResp).analises(iAna).nr_c = rsTaxas!nr_c
                fa_movi_resp(iResp).analises(iAna).nr_k = rsTaxas!nr_k
                'UALIA-896 Adicionado nrC e nrK
                Call FACTUS_PreencheValoresKC(rsPortarias!Portaria, valor_c, valor_k, fa_movi_resp(iResp).analises(iAna).nr_c, fa_movi_resp(iResp).analises(iAna).nr_k)
                fa_movi_resp(iResp).analises(iAna).val_c = valor_c
                fa_movi_resp(iResp).analises(iAna).val_k = valor_k
                '
                fa_movi_resp(iResp).analises(iAna).cod_rubr_efr = CStr(BL_HandleNull(rsTaxas!cod_rubr_efr, 0))
                fa_movi_resp(iResp).analises(iAna).descr_lin_fac = CStr(BL_HandleNull(rsTaxas!descr_rubr_efr, ""))
                'NELSONPSILVA UALIA-868
                If cod_sub_prec <> "" Then
                    fa_movi_resp(iResp).analises(iAna).perc_desc_lin = BL_HandleNull(perc_desc, "")
                End If
                If fa_movi_resp(iResp).analises(iAna).cod_isen_doe = gTipoIsencaoIsento Then
                    fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = 0
                    fa_movi_resp(iResp).analises(iAna).val_pr_unit = BL_HandleNull(rsTaxas!val_pag_ent, 0)
                    fa_movi_resp(iResp).analises(iAna).val_Taxa = 0
                Else
                    If (BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Or BL_HandleNull(rsTaxas!val_Taxa, "0") <> "0") And Flg_UsaPrecoEntidade = False Then
                        If BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Then
                            'NELSONPSILVA UALIA-868
                            If cod_sub_prec <> "" Then
                                fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = Round(CDbl(BL_HandleNull(rsTaxas!val_pag_doe, "0")) * CDbl(perc_pag_doe / 100), 2)
                            Else
                                fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = BL_HandleNull(rsTaxas!val_pag_doe, mediComboValorNull)
                            End If
                            fa_movi_resp(iResp).analises(iAna).val_Taxa = 0
                            fa_movi_resp(iResp).analises(iAna).flg_estado_doe = mediComboValorNull
                        Else
                            fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = 0
                            fa_movi_resp(iResp).analises(iAna).val_Taxa = BL_HandleNull(rsTaxas!val_Taxa, mediComboValorNull)
                            fa_movi_resp(iResp).analises(iAna).flg_estado_tx = mediComboValorNull
                        End If
                    
                    ElseIf (BL_HandleNull(rsTaxas!val_pag_ent, "0") <> "0") And Flg_UsaPrecoEntidade = True Then
                        'NELSONPSILVA UALIA-868
                        If cod_sub_prec <> "" Then
                            fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = Round(CDbl(BL_HandleNull(rsTaxas!val_pag_ent, "0")) * CDbl(perc_pag_doe / 100), 2)
                        Else
                            fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = BL_HandleNull(rsTaxas!val_pag_ent, mediComboValorNull)
                        End If
                        fa_movi_resp(iResp).analises(iAna).flg_estado_doe = mediComboValorNull
                    End If
                    
                    If flgPercentagem = True And BL_HandleNull(rsTaxas!val_pag_ent, mediComboValorNull) > 0 Then
                        If desconto_utente = 0 Then
                            fa_movi_resp(iResp).analises(iAna).perc_pag_doe = 100
                            fa_movi_resp(iResp).analises(iAna).perc_pag_efr = 0
                        Else
                            fa_movi_resp(iResp).analises(iAna).perc_pag_efr = 100 - desconto_utente
                            fa_movi_resp(iResp).analises(iAna).perc_pag_doe = desconto_utente
                            If fa_movi_resp(iResp).analises(iAna).flg_estado = 1 Or fa_movi_resp(iResp).analises(iAna).flg_estado = 8 Or fa_movi_resp(iResp).analises(iAna).flg_estado = mediComboValorNull Then
                                fa_movi_resp(iResp).analises(iAna).flg_estado = 1
                            End If
                        End If
                         'NELSONPSILVA UALIA-868
                        If cod_sub_prec <> "" Then
                            fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = Round(CDbl(rsTaxas!val_pag_ent) * CDbl(perc_pag_doe / 100), 2)
                            fa_movi_resp(iResp).analises(iAna).val_pr_unit = Round(CDbl(rsTaxas!val_pag_ent))
                        Else
                            fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = Round(CDbl(rsTaxas!val_pag_ent) * (fa_movi_resp(iResp).analises(iAna).perc_pag_doe / 100), 2)
                            fa_movi_resp(iResp).analises(iAna).val_pr_unit = Round(CDbl(rsTaxas!val_pag_ent) * (fa_movi_resp(iResp).analises(iAna).perc_pag_efr / 100), 2)
                        End If
                        '
                    Else
                        If Flg_UsaPrecoEntidade = True Then
                            fa_movi_resp(iResp).analises(iAna).val_pr_unit = 0
                        Else
                            If BL_HandleNull(rsTaxas!val_Taxa, "0") <> "0" Then
                                fa_movi_resp(iResp).analises(iAna).val_pr_unit = CDbl(BL_HandleNull(rsTaxas!val_pag_ent, 0)) - CDbl(BL_HandleNull(rsTaxas!val_Taxa, 0))
                            Else
                                fa_movi_resp(iResp).analises(iAna).val_pr_unit = CDbl(BL_HandleNull(rsTaxas!val_pag_ent, 0))
                            End If
                        End If
                    End If
                End If
                
            End If
            rsTaxas.Close
            Set rsTaxas = Nothing
        End If
        rsPortarias.Close
        Set rsPortarias = Nothing
    End If
    rsEntidades.Close
    Set rsEntidades = Nothing
    
    FACTUS_CalculaQuantidade iResp, iAna, flgPercentagem, desconto_utente, Flg_UsaPrecoEntidade, cod_sub_prec, perc_desc
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_PreencheValoresAnaDet"
    Exit Function
    Resume Next
End Function

Public Function FACTUS_VerificaAnaJaMarcada(cod_ana As String) As Boolean
    FACTUS_VerificaAnaJaMarcada = False
End Function

Private Function FACTUS_VerificaCodPostal()

End Function

Public Function FACTUS_GravaDados(NReq As Long, iMoviResp As Integer) As Boolean
    On Error GoTo TrataErro
    Dim iResp As Integer
    Dim iFact As Integer
    Dim lastP1 As String
    gConexaoSecundaria.BeginTrans
    For iResp = 1 To fa_movi_resp_tot
        If iResp = iMoviResp Or iMoviResp = mediComboValorNull Then
            'UALIA-930 Adicionado -> fa_movi_resp(iResp).episodio <> CStr(NReq)
            If fa_movi_resp(iResp).episodio = "" Or fa_movi_resp(iResp).episodio <> CStr(NReq) Then
                fa_movi_resp(iResp).episodio = NReq
            End If
            FACTUS_AtualizaOrdens iResp
            If fa_movi_resp(iResp).user_cri = "" Then
                FACTUS_InsereResp iResp
            Else
                FACTUS_AtualizaResp iResp
            End If
            lastP1 = ""
            For iFact = 1 To fa_movi_resp(iResp).totalAna
                'UALIA-930 Adicionado -> fa_movi_resp(iResp).episodio <> CStr(NReq)
                If fa_movi_resp(iResp).analises(iFact).episodio = "" Or fa_movi_resp(iResp).analises(iFact).episodio <> CStr(NReq) Then
                    fa_movi_resp(iResp).analises(iFact).episodio = NReq
                    
                    If Len(fa_movi_resp(iResp).analises(iFact).nr_req_ars) = 19 Then
                        fa_movi_resp(iResp).analises(iFact).chave_prog = fa_movi_resp(iResp).analises(iFact).nr_req_ars
                    Else
                        fa_movi_resp(iResp).analises(iFact).chave_prog = fa_movi_resp(iResp).analises(iFact).episodio & "_" & fa_movi_resp(iResp).analises(iFact).n_ord
                    End If
                End If
            
                If fa_movi_resp(iResp).analises(iFact).n_seq_prog <= 0 Then
                    FACTUS_InsereMoviFact iResp, iFact
                Else
                    FACTUS_ActualizaMoviFact iResp, iFact
                End If
                
                ' ------------------------------------------------------------------------------------
                ' SE FOR UM NOVO P1, ENTAO ENVIA CABE�ALHO PARA TABELA SL_REQ_ARS
                ' -------------------------------------------------------------------------------------
                If fa_movi_resp(iResp).analises(iFact).nr_req_ars <> lastP1 And Len(fa_movi_resp(iResp).analises(iFact).nr_req_ars) = 19 Then
                    lastP1 = fa_movi_resp(iResp).analises(iFact).nr_req_ars
                    FACTUS_InsereReqARS fa_movi_resp(iResp).episodio, lastP1, "A", CStr(fa_movi_resp(iResp).cod_dom), _
                                    CStr(fa_movi_resp(iResp).qtd_dom), fa_movi_resp(iResp).analises(iFact).cod_post_dom, _
                                    fa_movi_resp(iResp).analises(iFact).localidade_dom, "", "", fa_movi_resp(iResp).t_doente, fa_movi_resp(iResp).doente, _
                                    fa_movi_resp(iResp).analises(iFact).convencao, fa_movi_resp(iResp).analises(iFact).flg_tip_req
                End If
            
            Next iFact
        End If
    Next iResp
    gConexaoSecundaria.CommitTrans
    FACTUS_GravaDados = True
Exit Function
TrataErro:
    FACTUS_GravaDados = False
    gConexaoSecundaria.RollbackTrans
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_GravaDados"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' INSERE LINHA NA TABELA SL_REQ_ARS

' -----------------------------------------------------------------------------------------------
Public Function FACTUS_InsereReqARS(n_req As String, nr_req_ars As String, natureza As String, cod_dom As String, _
                           qtd_dom As String, cod_post_dom As String, localidade_dom As String, _
                           dt_ini_sess_fisio As String, dt_fim_sess_fisio As String, t_utente As String, _
                           Utente As String, convencao As Integer, flg_tip_req As String) As Boolean
    Dim ssql As String
    Dim iReg As Integer
    Dim grupo_req As String
    Dim rsGH As New adodb.recordset
    On Error GoTo TrataErro
    
    FACTUS_InsereReqARS = False
    If cod_dom = "0" Then
        cod_dom = "null"
        qtd_dom = "null"
        cod_post_dom = ""
        localidade_dom = ""
    End If
    'NELSONPSILVA Glintt-HS-21031 *Domicilio
    If gAtivaESP = mediSim Then
        If ESP_CredencialDom(nr_req_ars) = False Then
            cod_dom = "null"
            qtd_dom = "null"
            cod_post_dom = ""
            localidade_dom = ""
        End If
    End If
            
    If gTipoInstituicao <> gTipoInstituicaoHospitalar Then
    
        ssql = "SELECT * FROM sd_req_ars WHERE num_doc = " & BL_TrataStringParaBD(nr_req_ars)
        rsGH.CursorLocation = adUseServer
        rsGH.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsGH.Open ssql, gConexao
        If rsGH.RecordCount >= 1 Then
            'NELSONPSILVA LJMANSO-301
            If gFormActivo.Name = "FormFACTUSConfirmaNovo" And cod_dom <> "0" Then
                ssql = "UPDATE sd_req_ars SET convencao = " & BL_TrataNumberParaBD(IF_RetornaConvLote97(CStr(convencao))) & ", cod_dom = " & BL_TrataNumberParaBD(CStr(cod_dom)) & ", km_dom = " & BL_TrataNumberParaBD(CStr(qtd_dom))
                ssql = ssql & ", cod_post_dom = (SELECT cod_postal FROM sl_Requis WHERE n_req = " & BL_TrataStringParaBD(n_req) & ")"
                ssql = ssql & ", localidade_dom = (SELECT descr_postal FROM sl_cod_postal WHERE cod_postal = (SELECT cod_postal FROM sl_Requis WHERE n_req = " & BL_TrataStringParaBD(n_req)
                ssql = ssql & ")) Where num_doc = " & BL_TrataStringParaBD(nr_req_ars)
            Else
                ssql = "UPDATE sd_req_ars SET convencao = " & BL_TrataNumberParaBD(IF_RetornaConvLote97(CStr(convencao))) & ", cod_dom = " & BL_TrataNumberParaBD(CStr(cod_dom)) & ", km_dom = " & BL_TrataNumberParaBD(CStr(qtd_dom))
                ssql = ssql & ", cod_post_dom = " & BL_TrataStringParaBD(cod_post_dom) & ", localidade_dom = " & BL_TrataStringParaBD(localidade_dom) & " WHERE num_doc = " & BL_TrataStringParaBD(nr_req_ars)
                'sSql = "UPDATE sd_req_ars SET convencao = " & BL_TrataNumberParaBD(CStr(convencao)) & ", cod_dom = " & BL_TrataNumberParaBD(CStr(cod_dom)) & ", km_dom = " & BL_TrataNumberParaBD(CStr(qtd_dom))
                'sSql = sSql & ", cod_post_dom = (select cod_postal_ute FROM sl_identif WHERE seq_utente = (SELECT seq_utente FROM sl_Requis WHERE  n_req = " & BL_TrataStringParaBD(n_req) & "))"
                'sSql = sSql & ", localidade_dom = (SELECT descr_postal FROM sl_cod_postal WHERE cod_postal = (SELECT cod_postal_ute FROM sl_identif WHERE seq_utente = (SELECT seq_utente FROM sl_Requis WHERE n_req = " & BL_TrataStringParaBD(n_req) & ")))"
                'sSql = sSql & " WHERE num_doc = " & BL_TrataStringParaBD(nr_req_ars)
                '
            End If
            BG_ExecutaQuery_ADO ssql
            FACTUS_InsereReqARS = True
            rsGH.Close
            Set rsGH = Nothing
            Exit Function
        End If
        rsGH.Close
        
        ssql = "SELECT grupo_req FROM gh_fa.fa_tbf_tip_req WHERE flg_tip_req = " & BL_TrataStringParaBD(flg_tip_req)
        rsGH.CursorLocation = adUseServer
        rsGH.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsGH.Open ssql, gConexao
        If rsGH.RecordCount >= 1 Then
            grupo_req = BL_HandleNull(rsGH!grupo_req, "")
        End If
        rsGH.Close

        'NELSONPSILVA LJMANSO-301
        If gFormActivo.Name = "FormFACTUSConfirmaNovo" And cod_dom <> "0" Then
            ssql = "INSERT INTO sd_req_ars( num_doc, natureza, convencao, cod_dom, km_dom, cod_post_dom, localidade_dom, "
            ssql = ssql & " dt_ini_sess_fisio,dt_fim_sess_fisio, grupo_req, t_doente, doente) VALUES("
            ssql = ssql & BL_TrataStringParaBD(nr_req_ars) & ","
            ssql = ssql & BL_TrataStringParaBD(natureza) & ","
            ssql = ssql & BL_TrataNumberParaBD(IF_RetornaConvLote97(CStr(convencao))) & ","
            ssql = ssql & BL_TrataNumberParaBD(cod_dom) & ","
            ssql = ssql & BL_TrataNumberParaBD(qtd_dom) & ","
            'NELSONPSILVA LJMANSO-301
            'sSql = sSql & BL_TrataStringParaBD(cod_post_dom) & ","
            ssql = ssql & "(SELECT cod_postal FROM sl_Requis WHERE  n_req = " & BL_TrataStringParaBD(n_req) & "),"
            'NELSONPSILVA LJMANSO-301
            'sSql = sSql & BL_TrataStringParaBD(localidade_dom) & ","
            ssql = ssql & "(SELECT descr_postal FROM sl_cod_postal WHERE cod_postal = (SELECT cod_postal FROM sl_Requis WHERE n_req = " & BL_TrataStringParaBD(n_req) & ")),"
            ssql = ssql & BL_TrataDataParaBD(dt_ini_sess_fisio) & ","
            ssql = ssql & BL_TrataDataParaBD(dt_fim_sess_fisio) & ","
            ssql = ssql & BL_TrataStringParaBD(grupo_req) & ","
            ssql = ssql & BL_TrataStringParaBD(CStr(t_utente)) & ","
            ssql = ssql & BL_TrataStringParaBD(CStr(Utente)) & ")"
        Else
            ssql = "INSERT INTO sd_req_ars( num_doc, natureza, convencao, cod_dom, km_dom, cod_post_dom, localidade_dom, "
            ssql = ssql & " dt_ini_sess_fisio,dt_fim_sess_fisio, grupo_req, t_doente, doente) VALUES("
            ssql = ssql & BL_TrataStringParaBD(nr_req_ars) & ","
            ssql = ssql & BL_TrataStringParaBD(natureza) & ","
            ssql = ssql & BL_TrataNumberParaBD(IF_RetornaConvLote97(CStr(convencao))) & ","
            ssql = ssql & BL_TrataNumberParaBD(cod_dom) & ","
            ssql = ssql & BL_TrataNumberParaBD(qtd_dom) & ","
            'BRUNODSANTOS LJMANSO-301
            ssql = ssql & BL_TrataStringParaBD(cod_post_dom) & ","
            'sSql = sSql & "(SELECT cod_postal_ute FROM sl_identif WHERE seq_utente = (SELECT seq_utente FROM sl_Requis WHERE  n_req = " & BL_TrataStringParaBD(n_req) & ")),"
            'BRUNODSANTOS LJMANSO-301
            ssql = ssql & BL_TrataStringParaBD(localidade_dom) & ","
            'sSql = sSql & "(SELECT descr_postal FROM sl_cod_postal WHERE cod_postal = (SELECT cod_postal_ute FROM sl_identif WHERE seq_utente = (SELECT seq_utente FROM sl_Requis WHERE n_req = " & BL_TrataStringParaBD(n_req) & "))),"
            ssql = ssql & BL_TrataDataParaBD(dt_ini_sess_fisio) & ","
            ssql = ssql & BL_TrataDataParaBD(dt_fim_sess_fisio) & ","
            ssql = ssql & BL_TrataStringParaBD(grupo_req) & ","
            ssql = ssql & BL_TrataStringParaBD(CStr(t_utente)) & ","
            ssql = ssql & BL_TrataStringParaBD(CStr(Utente)) & ")"
        End If
        BG_ExecutaQuery_ADO ssql
    End If
    
    FACTUS_InsereReqARS = True
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "FACTUS_InsereReqARS"
    Exit Function
    Resume Next
End Function


Private Function FACTUS_DevolveEmpresa(cod_efr As Long, p_data As String) As String
    Dim ssql As String
    Dim rsPortarias As New adodb.recordset
    Dim iBdAberta  As Integer
    On Error GoTo TrataErro
    
    
    ' --------------------------------------------------------
    ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
    ' --------------------------------------------------------
    ssql = "SELECT distinct empresa_id FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
    ssql = ssql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
    ssql = ssql & " AND cod_efr in(SELECT tab_utilizada FROM fa_EFR WHERE cod_efr = " & cod_efr & ") AND user_rem is null  "
    ssql = ssql & " AND (empresa_id IS NULL OR empresa_id = " & BL_TrataStringParaBD(gEmpresaIdFactDefault) & ")"

    iBdAberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iBdAberta = 1 Then
        rsPortarias.CursorLocation = adUseServer
        rsPortarias.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsPortarias.Open ssql, gConexaoSecundaria
        If rsPortarias.RecordCount >= 1 Then
            FACTUS_DevolveEmpresa = gEmpresaIdFactDefault
        Else
            rsPortarias.Close
            ssql = "SELECT distinct empresa_id FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
            ssql = ssql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
            ssql = ssql & " AND cod_efr in(SELECT tab_utilizada FROM fa_EFR WHERE cod_efr = " & cod_efr & ") AND user_rem is null  "
            rsPortarias.CursorLocation = adUseServer
            rsPortarias.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsPortarias.Open ssql, gConexaoSecundaria
            If rsPortarias.RecordCount >= 1 Then
                FACTUS_DevolveEmpresa = BL_HandleNull(rsPortarias!empresa_id)
            Else
                BG_Mensagem mediMsgBox, "Entidades n�o tem pre�arios ativos.", vbOKOnly + vbInformation, "Entidade Financeira"

            End If
        End If
        rsPortarias.Close
        Set rsPortarias = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_DevolveEmpresa"
    FACTUS_DevolveEmpresa = ""
    Exit Function
    Resume Next
End Function


Public Function FACTUS_AlteraEntidade(nova_cod_efr As String, iRespAtual As Integer, iAnaAtual As Integer, _
                                      cod_ana As String, cod_hemodialise As Integer, dt_ini_real As String, _
                                      cod_isen_doe As Integer, iAnalise As Integer, _
                                      convencao As Integer, cod_medico As String, cod_dom As Integer, km_dom As Integer, _
                                      cod_postal_dom As String, localidade_dom As String, n_benef As String, _
                                      t_doente As String, doente As String, req As String, cod_u_saude As String, cod_rubr As Long) As Boolean
    On Error GoTo TrataErro
    Dim novoiResp As Integer
    Dim iResp As Integer
    Dim iAna As Integer
    Dim nr_req_ars As String
    
    If nova_cod_efr = fa_movi_resp(iRespAtual).cod_efr Then
        BG_Mensagem mediMsgBox, "Entidade � a mesma que anterior.", vbOKOnly + vbInformation, "Entidade Financeira"
        Exit Function
    End If
    novoiResp = mediComboValorNull
    For iResp = 1 To fa_movi_resp_tot
        If fa_movi_resp(iResp).cod_efr = nova_cod_efr Then
            novoiResp = iResp
            Exit For
        End If
    Next iResp
    If novoiResp = mediComboValorNull Then
        If FACTUS_AdicionaEFR(nova_cod_efr, n_benef, t_doente, doente, req) <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade financeira n�o alterada", vbOKOnly + vbInformation, "Entidade Financeira"
            FACTUS_AlteraEntidade = False
            Exit Function
        End If
        novoiResp = fa_movi_resp_tot
    End If
    If iAnaAtual > 0 Then
        nr_req_ars = fa_movi_resp(iRespAtual).analises(iAnaAtual).nr_req_ars
    Else
        nr_req_ars = ""
    End If
    
    If FACTUS_AdicionaAnalise(nova_cod_efr, cod_ana, dt_ini_real, 0, cod_hemodialise, cod_isen_doe, iAnalise, _
                            n_benef, t_doente, doente, req, nr_req_ars, convencao, cod_medico, cod_dom, _
                            km_dom, cod_postal_dom, localidade_dom, cod_u_saude, cod_rubr) = True Then
        'REJEITA LINHA ATUAL
        If iAnaAtual > 0 Then
            If FACTUS_EliminaAna(iRespAtual, iAnaAtual, True) = False Then
                GoTo TrataErro
            End If
        End If
    Else
        FACTUS_AlteraEntidade = False
        Exit Function
    End If
    
    FACTUS_AlteraEntidade = True
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AlteraEntidade"
    FACTUS_AlteraEntidade = False
    Exit Function
    Resume Next
End Function


Private Function FACTUS_copiaAnalise(iResp1 As Integer, iAnaFact1 As Integer, iResp2 As Integer, iAnaFact2 As Integer, flgAtualizarOrdemiAna As Boolean) As Boolean
    On Error GoTo TrataErro
    
    FACTUS_InicializaAna iResp2, iAnaFact2
    
    fa_movi_resp(iResp2).analises(iAnaFact2).n_seq_prog = fa_movi_resp(iResp1).analises(iAnaFact1).n_seq_prog
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_prog = fa_movi_resp(iResp1).analises(iAnaFact1).cod_prog
    fa_movi_resp(iResp2).analises(iAnaFact2).t_doente = fa_movi_resp(iResp1).analises(iAnaFact1).t_doente
    fa_movi_resp(iResp2).analises(iAnaFact2).doente = fa_movi_resp(iResp1).analises(iAnaFact1).doente
    fa_movi_resp(iResp2).analises(iAnaFact2).t_episodio = fa_movi_resp(iResp1).analises(iAnaFact1).t_episodio
    fa_movi_resp(iResp2).analises(iAnaFact2).episodio = fa_movi_resp(iResp1).analises(iAnaFact1).episodio
    fa_movi_resp(iResp2).analises(iAnaFact2).n_ord = fa_movi_resp(iResp1).analises(iAnaFact1).n_ord
    fa_movi_resp(iResp2).analises(iAnaFact2).dt_ini_real = fa_movi_resp(iResp1).analises(iAnaFact1).dt_ini_real
    fa_movi_resp(iResp2).analises(iAnaFact2).dt_fim_real = fa_movi_resp(iResp1).analises(iAnaFact1).dt_fim_real
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_tip_rubr = fa_movi_resp(iResp1).analises(iAnaFact1).cod_tip_rubr
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_grupo = fa_movi_resp(iResp1).analises(iAnaFact1).cod_grupo
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_rubr = fa_movi_resp(iResp1).analises(iAnaFact1).cod_rubr
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_rubr_efr = fa_movi_resp(iResp1).analises(iAnaFact1).cod_rubr_efr
    fa_movi_resp(iResp2).analises(iAnaFact2).descr_rubr = fa_movi_resp(iResp1).analises(iAnaFact1).descr_rubr
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_serv_req = fa_movi_resp(iResp1).analises(iAnaFact1).cod_serv_req
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_serv_exec = fa_movi_resp(iResp1).analises(iAnaFact1).cod_serv_exec
    fa_movi_resp(iResp2).analises(iAnaFact2).descr_lin_fac = fa_movi_resp(iResp1).analises(iAnaFact1).descr_lin_fac
    fa_movi_resp(iResp2).analises(iAnaFact2).val_pr_unit = fa_movi_resp(iResp1).analises(iAnaFact1).val_pr_unit
    fa_movi_resp(iResp2).analises(iAnaFact2).val_pr_unit_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_pr_unit_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).val_k = fa_movi_resp(iResp1).analises(iAnaFact1).val_k
    fa_movi_resp(iResp2).analises(iAnaFact2).val_k_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_k_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).val_c = fa_movi_resp(iResp1).analises(iAnaFact1).val_c
    fa_movi_resp(iResp2).analises(iAnaFact2).val_c_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_c_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).nr_k = fa_movi_resp(iResp1).analises(iAnaFact1).nr_k
    fa_movi_resp(iResp2).analises(iAnaFact2).nr_c = fa_movi_resp(iResp1).analises(iAnaFact1).nr_c
    fa_movi_resp(iResp2).analises(iAnaFact2).qtd = fa_movi_resp(iResp1).analises(iAnaFact1).qtd
    fa_movi_resp(iResp2).analises(iAnaFact2).perc_desc_lin = fa_movi_resp(iResp1).analises(iAnaFact1).perc_desc_lin
    fa_movi_resp(iResp2).analises(iAnaFact2).perc_pag_efr = fa_movi_resp(iResp1).analises(iAnaFact1).perc_pag_efr
    fa_movi_resp(iResp2).analises(iAnaFact2).val_total = fa_movi_resp(iResp1).analises(iAnaFact1).val_total
    fa_movi_resp(iResp2).analises(iAnaFact2).val_total_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_total_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).perc_pag_doe = fa_movi_resp(iResp1).analises(iAnaFact1).perc_pag_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).val_pr_u_doe = fa_movi_resp(iResp1).analises(iAnaFact1).val_pr_u_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).val_pr_u_doe_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_pr_u_doe_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).val_pag_doe = fa_movi_resp(iResp1).analises(iAnaFact1).val_pag_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).val_pag_doe_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_pag_doe_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).val_prev = fa_movi_resp(iResp1).analises(iAnaFact1).val_prev
    fa_movi_resp(iResp2).analises(iAnaFact2).val_prev_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_prev_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_isen_doe = fa_movi_resp(iResp1).analises(iAnaFact1).cod_isen_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_pagou_taxa = fa_movi_resp(iResp1).analises(iAnaFact1).flg_pagou_taxa
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_prov = fa_movi_resp(iResp1).analises(iAnaFact1).cod_prov
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_dest = fa_movi_resp(iResp1).analises(iAnaFact1).cod_dest
    fa_movi_resp(iResp2).analises(iAnaFact2).nr_req_ars = fa_movi_resp(iResp1).analises(iAnaFact1).nr_req_ars
    fa_movi_resp(iResp2).analises(iAnaFact2).t_int = fa_movi_resp(iResp1).analises(iAnaFact1).t_int
    fa_movi_resp(iResp2).analises(iAnaFact2).t_cond_esp = fa_movi_resp(iResp1).analises(iAnaFact1).t_cond_esp
    fa_movi_resp(iResp2).analises(iAnaFact2).n_incidencias = fa_movi_resp(iResp1).analises(iAnaFact1).n_incidencias
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_stdby = fa_movi_resp(iResp1).analises(iAnaFact1).flg_stdby
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_ventil = fa_movi_resp(iResp1).analises(iAnaFact1).flg_ventil
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_psiq = fa_movi_resp(iResp1).analises(iAnaFact1).flg_psiq
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_orig_rubr = fa_movi_resp(iResp1).analises(iAnaFact1).flg_orig_rubr
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_estado = fa_movi_resp(iResp1).analises(iAnaFact1).flg_estado
    fa_movi_resp(iResp2).analises(iAnaFact2).serie_fac = fa_movi_resp(iResp1).analises(iAnaFact1).serie_fac
    fa_movi_resp(iResp2).analises(iAnaFact2).n_fac = fa_movi_resp(iResp1).analises(iAnaFact1).n_fac
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_estado_doe = fa_movi_resp(iResp1).analises(iAnaFact1).flg_estado_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).serie_fac_doe = fa_movi_resp(iResp1).analises(iAnaFact1).serie_fac_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).n_Fac_doe = fa_movi_resp(iResp1).analises(iAnaFact1).n_Fac_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_mot_rej_anul = fa_movi_resp(iResp1).analises(iAnaFact1).cod_mot_rej_anul
    fa_movi_resp(iResp2).analises(iAnaFact2).chave_prog = fa_movi_resp(iResp1).analises(iAnaFact1).chave_prog
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_serv_req_gh = fa_movi_resp(iResp1).analises(iAnaFact1).cod_serv_req_gh
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_serv_exec_gh = fa_movi_resp(iResp1).analises(iAnaFact1).cod_serv_exec_gh
    fa_movi_resp(iResp2).analises(iAnaFact2).n_doc = fa_movi_resp(iResp1).analises(iAnaFact1).n_doc
    fa_movi_resp(iResp2).analises(iAnaFact2).user_cri = fa_movi_resp(iResp1).analises(iAnaFact1).user_cri
    fa_movi_resp(iResp2).analises(iAnaFact2).dt_cri = fa_movi_resp(iResp1).analises(iAnaFact1).dt_cri
    fa_movi_resp(iResp2).analises(iAnaFact2).user_act = fa_movi_resp(iResp1).analises(iAnaFact1).user_act
    fa_movi_resp(iResp2).analises(iAnaFact2).dt_act = fa_movi_resp(iResp1).analises(iAnaFact1).dt_act
    fa_movi_resp(iResp2).analises(iAnaFact2).val_Taxa = fa_movi_resp(iResp1).analises(iAnaFact1).val_Taxa
    fa_movi_resp(iResp2).analises(iAnaFact2).val_taxa_2m = fa_movi_resp(iResp1).analises(iAnaFact1).val_taxa_2m
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_pagou_val_doe = fa_movi_resp(iResp1).analises(iAnaFact1).flg_pagou_val_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_ventil_perm = fa_movi_resp(iResp1).analises(iAnaFact1).flg_ventil_perm
    fa_movi_resp(iResp2).analises(iAnaFact2).t_gdh = fa_movi_resp(iResp1).analises(iAnaFact1).t_gdh
    fa_movi_resp(iResp2).analises(iAnaFact2).tab_prog = fa_movi_resp(iResp1).analises(iAnaFact1).tab_prog
    fa_movi_resp(iResp2).analises(iAnaFact2).hr_ini_real = fa_movi_resp(iResp1).analises(iAnaFact1).hr_ini_real
    fa_movi_resp(iResp2).analises(iAnaFact2).hr_fim_real = fa_movi_resp(iResp1).analises(iAnaFact1).hr_fim_real
    fa_movi_resp(iResp2).analises(iAnaFact2).nome_interv = fa_movi_resp(iResp1).analises(iAnaFact1).nome_interv
    fa_movi_resp(iResp2).analises(iAnaFact2).nome_doe = fa_movi_resp(iResp1).analises(iAnaFact1).nome_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_rubr_cir = fa_movi_resp(iResp1).analises(iAnaFact1).cod_rubr_cir
    fa_movi_resp(iResp2).analises(iAnaFact2).n_ord_ins = fa_movi_resp(iResp1).analises(iAnaFact1).n_ord_ins
    fa_movi_resp(iResp2).analises(iAnaFact2).erro_reg = fa_movi_resp(iResp1).analises(iAnaFact1).erro_reg
    fa_movi_resp(iResp2).analises(iAnaFact2).tipo_interv = fa_movi_resp(iResp1).analises(iAnaFact1).tipo_interv
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_resp = fa_movi_resp(iResp1).analises(iAnaFact1).cod_resp
    fa_movi_resp(iResp2).analises(iAnaFact2).n_interv = fa_movi_resp(iResp1).analises(iAnaFact1).n_interv
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_estado_tx = fa_movi_resp(iResp1).analises(iAnaFact1).flg_estado_tx
    fa_movi_resp(iResp2).analises(iAnaFact2).serie_fac_tx = fa_movi_resp(iResp1).analises(iAnaFact1).serie_fac_tx
    fa_movi_resp(iResp2).analises(iAnaFact2).n_fac_tx = fa_movi_resp(iResp1).analises(iAnaFact1).n_fac_tx
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_erro = fa_movi_resp(iResp1).analises(iAnaFact1).flg_erro
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_fact_pat = fa_movi_resp(iResp1).analises(iAnaFact1).flg_fact_pat
    fa_movi_resp(iResp2).analises(iAnaFact2).n_seq_prog_orig = fa_movi_resp(iResp1).analises(iAnaFact1).n_seq_prog_orig
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_alt_pr = fa_movi_resp(iResp1).analises(iAnaFact1).flg_alt_pr
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_reg_termo = fa_movi_resp(iResp1).analises(iAnaFact1).flg_reg_termo
    fa_movi_resp(iResp2).analises(iAnaFact2).empresa_id = fa_movi_resp(iResp1).analises(iAnaFact1).empresa_id
    fa_movi_resp(iResp2).analises(iAnaFact2).id_mot_efr = fa_movi_resp(iResp1).analises(iAnaFact1).id_mot_efr
    fa_movi_resp(iResp2).analises(iAnaFact2).id_mot_doe = fa_movi_resp(iResp1).analises(iAnaFact1).id_mot_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_prod_marg = fa_movi_resp(iResp1).analises(iAnaFact1).flg_prod_marg
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_tip_req = fa_movi_resp(iResp1).analises(iAnaFact1).flg_tip_req
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_dente = fa_movi_resp(iResp1).analises(iAnaFact1).cod_dente
    fa_movi_resp(iResp2).analises(iAnaFact2).unidade = fa_movi_resp(iResp1).analises(iAnaFact1).unidade
    fa_movi_resp(iResp2).analises(iAnaFact2).perc_desc_lin_doe = fa_movi_resp(iResp1).analises(iAnaFact1).perc_desc_lin_doe
    fa_movi_resp(iResp2).analises(iAnaFact2).num_proc_efr = fa_movi_resp(iResp1).analises(iAnaFact1).num_proc_efr
    fa_movi_resp(iResp2).analises(iAnaFact2).flg_prog_urg = fa_movi_resp(iResp1).analises(iAnaFact1).flg_prog_urg
    fa_movi_resp(iResp2).analises(iAnaFact2).id_preco_fech = fa_movi_resp(iResp1).analises(iAnaFact1).id_preco_fech
    fa_movi_resp(iResp2).analises(iAnaFact2).val_ref = fa_movi_resp(iResp1).analises(iAnaFact1).val_ref
    fa_movi_resp(iResp2).analises(iAnaFact2).val_pvp = fa_movi_resp(iResp1).analises(iAnaFact1).val_pvp
    fa_movi_resp(iResp2).analises(iAnaFact2).t_doente_gh = fa_movi_resp(iResp1).analises(iAnaFact1).t_doente_gh
    fa_movi_resp(iResp2).analises(iAnaFact2).doente_gh = fa_movi_resp(iResp1).analises(iAnaFact1).doente_gh
    fa_movi_resp(iResp2).analises(iAnaFact2).t_episodio_gh = fa_movi_resp(iResp1).analises(iAnaFact1).t_episodio_gh
    fa_movi_resp(iResp2).analises(iAnaFact2).episodio_gh = fa_movi_resp(iResp1).analises(iAnaFact1).episodio_gh
    fa_movi_resp(iResp2).analises(iAnaFact2).n_marcadores = fa_movi_resp(iResp1).analises(iAnaFact1).n_marcadores
    fa_movi_resp(iResp2).analises(iAnaFact2).id_ugc = fa_movi_resp(iResp1).analises(iAnaFact1).id_ugc
    fa_movi_resp(iResp2).analises(iAnaFact2).origem_valor = fa_movi_resp(iResp1).analises(iAnaFact1).origem_valor
    
    
    fa_movi_resp(iResp2).analises(iAnaFact2).convencao = fa_movi_resp(iResp1).analises(iAnaFact1).convencao
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_dom = fa_movi_resp(iResp1).analises(iAnaFact1).cod_dom
    fa_movi_resp(iResp2).analises(iAnaFact2).km_dom = fa_movi_resp(iResp1).analises(iAnaFact1).km_dom
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_post_dom = fa_movi_resp(iResp1).analises(iAnaFact1).cod_post_dom
    fa_movi_resp(iResp2).analises(iAnaFact2).localidade_dom = fa_movi_resp(iResp1).analises(iAnaFact1).localidade_dom
    fa_movi_resp(iResp2).analises(iAnaFact2).cod_medico = fa_movi_resp(iResp1).analises(iAnaFact1).cod_medico
    If flgAtualizarOrdemiAna = True Then
        fa_movi_resp(iResp2).analises(iAnaFact2).iAnalise = fa_movi_resp(iResp1).analises(iAnaFact1).iAnalise
    End If
    If fa_movi_resp(iResp2).analises(iAnaFact2).iAnalise > 0 Then
        AnalisesFact(fa_movi_resp(iResp2).analises(iAnaFact2).iAnalise).iResp = iResp2
        AnalisesFact(fa_movi_resp(iResp2).analises(iAnaFact2).iAnalise).iAnaFact = iAnaFact2
        AnalisesFact(fa_movi_resp(iResp2).analises(iAnaFact2).iAnalise).cod_rubr = fa_movi_resp(iResp1).analises(iAnaFact1).cod_rubr
    End If
    FACTUS_InicializaAna iResp1, iAnaFact1
    FACTUS_copiaAnalise = True
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_copiaAnalise"
    FACTUS_copiaAnalise = False
    Exit Function
    Resume Next
End Function

Private Function FACTUS_copiaEntidade(iResp1 As Integer, iResp2 As Integer) As Boolean
    On Error GoTo TrataErro
    Dim iAna As Integer
    fa_movi_resp(iResp1).n_ord = fa_movi_resp(iResp2).n_ord
    fa_movi_resp(iResp1).t_episodio = fa_movi_resp(iResp2).t_episodio
    fa_movi_resp(iResp1).episodio = fa_movi_resp(iResp2).episodio
    fa_movi_resp(iResp1).n_benef_doe = fa_movi_resp(iResp2).n_benef_doe
    fa_movi_resp(iResp1).cod_tip_rubr = fa_movi_resp(iResp2).cod_tip_rubr
    fa_movi_resp(iResp1).cod_rubr = fa_movi_resp(iResp2).cod_rubr
    fa_movi_resp(iResp1).cod_efr = fa_movi_resp(iResp2).cod_efr
    fa_movi_resp(iResp1).descr_efr = fa_movi_resp(iResp2).descr_efr
    fa_movi_resp(iResp1).mor_efr = fa_movi_resp(iResp2).mor_efr
    fa_movi_resp(iResp1).cod_post_efr = fa_movi_resp(iResp2).cod_post_efr
    fa_movi_resp(iResp1).n_seq_post_efr = fa_movi_resp(iResp2).n_seq_post_efr
    fa_movi_resp(iResp1).telef_efr = fa_movi_resp(iResp2).telef_efr
    fa_movi_resp(iResp1).n_contrib_efr = fa_movi_resp(iResp2).n_contrib_efr
    fa_movi_resp(iResp1).cod_modal = fa_movi_resp(iResp2).cod_modal
    fa_movi_resp(iResp1).val_franq = fa_movi_resp(iResp2).val_franq
    fa_movi_resp(iResp1).val_franq_2m = fa_movi_resp(iResp2).val_franq_2m
    fa_movi_resp(iResp1).per_paga = fa_movi_resp(iResp2).per_paga
    fa_movi_resp(iResp1).val_max = fa_movi_resp(iResp2).val_max
    fa_movi_resp(iResp1).val_max_2m = fa_movi_resp(iResp2).val_max_2m
    fa_movi_resp(iResp1).cod_desc = fa_movi_resp(iResp2).cod_desc
    fa_movi_resp(iResp1).perc_desc = fa_movi_resp(iResp2).perc_desc
    fa_movi_resp(iResp1).val_total = fa_movi_resp(iResp2).val_total
    fa_movi_resp(iResp1).val_total_2m = fa_movi_resp(iResp2).val_total_2m
    fa_movi_resp(iResp1).user_cri = fa_movi_resp(iResp2).user_cri
    fa_movi_resp(iResp1).dt_cri = fa_movi_resp(iResp2).dt_cri
    fa_movi_resp(iResp1).user_act = fa_movi_resp(iResp2).user_act
    fa_movi_resp(iResp1).dt_act = fa_movi_resp(iResp2).dt_act
    fa_movi_resp(iResp1).descr_causa_adm = fa_movi_resp(iResp2).descr_causa_adm
    fa_movi_resp(iResp1).n_ord_franq = fa_movi_resp(iResp2).n_ord_franq
    fa_movi_resp(iResp1).cod_dom = fa_movi_resp(iResp2).cod_dom
    fa_movi_resp(iResp1).qtd_dom = fa_movi_resp(iResp2).qtd_dom
    fa_movi_resp(iResp1).n_mecan_med = fa_movi_resp(iResp2).n_mecan_med
    fa_movi_resp(iResp1).nome_med = fa_movi_resp(iResp2).nome_med
    fa_movi_resp(iResp1).n_desloc = fa_movi_resp(iResp2).n_desloc
    fa_movi_resp(iResp1).localidade = fa_movi_resp(iResp2).localidade
    fa_movi_resp(iResp1).flg_estado = fa_movi_resp(iResp2).flg_estado
    fa_movi_resp(iResp1).serie_fac = fa_movi_resp(iResp2).serie_fac
    fa_movi_resp(iResp1).n_fac = fa_movi_resp(iResp2).n_fac
    fa_movi_resp(iResp1).sigla_t_orig = fa_movi_resp(iResp2).sigla_t_orig
    fa_movi_resp(iResp1).t_doente = fa_movi_resp(iResp2).t_doente
    fa_movi_resp(iResp1).doente = fa_movi_resp(iResp2).doente
    fa_movi_resp(iResp1).cod_efr_base = fa_movi_resp(iResp2).cod_efr_base
    fa_movi_resp(iResp1).n_ord_fact = fa_movi_resp(iResp2).n_ord_fact
    fa_movi_resp(iResp1).flg_estado_efr = fa_movi_resp(iResp2).flg_estado_efr
    fa_movi_resp(iResp1).serie_fac_efr = fa_movi_resp(iResp2).serie_fac_efr
    fa_movi_resp(iResp1).n_fac_efr = fa_movi_resp(iResp2).n_fac_efr
    fa_movi_resp(iResp1).cod_efr_igif = fa_movi_resp(iResp2).cod_efr_igif
    fa_movi_resp(iResp1).empresa_id = fa_movi_resp(iResp2).empresa_id
    fa_movi_resp(iResp1).perc_franq = fa_movi_resp(iResp2).perc_franq
    fa_movi_resp(iResp1).val_min_franq = fa_movi_resp(iResp2).val_min_franq
    fa_movi_resp(iResp1).val_max_franq = fa_movi_resp(iResp2).val_max_franq
    fa_movi_resp(iResp1).flg_compart_dom = fa_movi_resp(iResp2).flg_compart_dom
    fa_movi_resp(iResp1).flg_dom_defeito = fa_movi_resp(iResp2).flg_dom_defeito
    fa_movi_resp(iResp1).flg_novaArs = fa_movi_resp(iResp2).flg_novaArs
    
    fa_movi_resp(iResp1).analises = fa_movi_resp(iResp2).analises
    fa_movi_resp(iResp1).totalAna = fa_movi_resp(iResp2).totalAna
    For iAna = 1 To fa_movi_resp(iResp1).totalAna
        If fa_movi_resp(iResp1).analises(iAna).iAnalise > 0 Then
            AnalisesFact(fa_movi_resp(iResp1).analises(iAna).iAnalise).iResp = iResp1
            AnalisesFact(fa_movi_resp(iResp1).analises(iAna).iAnalise).iAnaFact = iAna
            AnalisesFact(fa_movi_resp(iResp1).analises(iAna).iAnalise).cod_rubr = fa_movi_resp(iResp1).analises(iAna).cod_rubr
        End If
    
    Next iAna
    
    FACTUS_copiaEntidade = True
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_copiaEntidade"
    FACTUS_copiaEntidade = False
    Exit Function
    Resume Next
End Function


Public Function FACTUS_EliminaAna(ByVal iRespAtual As Integer, ByVal iAnaAtual As Integer, ByVal flgAtualizarOrdemiAna As Boolean) As Boolean
    On Error GoTo TrataErro
    Dim iA As Integer
    Dim iR As Integer
    Dim Elimina As Boolean
    'NELSONPSILVA Adicionada condi��o para poder remover an�lises sem precarios -> (iRespAtual > 0 And iAnaAtual > 0)
    If iRespAtual <= fa_movi_resp_tot And iRespAtual > 0 And iAnaAtual > 0 Then
        If iAnaAtual <= fa_movi_resp(iRespAtual).totalAna Then
            If fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado = 3 Or _
                fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado_doe = 3 Or _
                fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado_tx = 3 Then
                    BG_Mensagem mediMsgBox, "An�lise j� faturada. N�o � possivel apagar atividade.", vbOKOnly + vbInformation, "Taxas"
                    Exit Function
            End If
            If fa_movi_resp(iRespAtual).analises(iAnaAtual).n_seq_prog <= mediComboValorNull Then
                For iA = iAnaAtual To fa_movi_resp(iRespAtual).totalAna - 1
                    FACTUS_copiaAnalise iRespAtual, iA + 1, iRespAtual, iA, flgAtualizarOrdemiAna
                Next iA
                fa_movi_resp(iRespAtual).totalAna = fa_movi_resp(iRespAtual).totalAna - 1
                ReDim Preserve fa_movi_resp(iRespAtual).analises(fa_movi_resp(iRespAtual).totalAna)
                If fa_movi_resp(iRespAtual).totalAna = 0 Then
                    For iR = iRespAtual To fa_movi_resp_tot - 1
                        FACTUS_copiaEntidade iR, iR + 1
                    Next iR
                    Elimina = True
                End If
                If Elimina = True Then
                    fa_movi_resp_tot = fa_movi_resp_tot - 1
                    ReDim Preserve fa_movi_resp(fa_movi_resp_tot)
                End If
            Else
                If fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado = 3 Or fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado_doe = 3 Or _
                    fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado_tx = 3 Then
                    BG_Mensagem mediMsgBox, "An�lise j� faturada. N�o � possivel apagar atividade.", vbOKOnly + vbInformation, "Taxas"
                Else
                    fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado = 2
                    fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado_doe = 2
                    fa_movi_resp(iRespAtual).analises(iAnaAtual).flg_estado_tx = 2
                    fa_movi_resp(iRespAtual).analises(iAnaAtual).iAnalise = mediComboValorNull
                End If
            End If
        End If
    End If
    FACTUS_EliminaAna = True
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_EliminaAna"
    FACTUS_EliminaAna = False
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------------------------------

' PERMITE MUDAR TIPO DE ISENCAO DE UMA ANALISE ATRAVES DAS TECLAS F8, F9, F10

' --------------------------------------------------------------------------------
Public Function FACTUS_AlteraIsencao(ByVal TipoIsencao As Integer, ByVal iResp As Integer, ByVal iAna As Integer, ByVal cod_ana As String, ByVal qtd As Integer, _
                                     ByVal cod_hemodialise As Integer, cod_rubr As Long) As Boolean
On Error GoTo TrataErro
    Dim taxa As String
    Dim codAnaEfr As String
    Dim mens As String
    Dim ssql As String
    Dim flg_FdsValFixo As Integer
    Dim isencaoAnterior As Integer
    Dim taxaEfr As String
    Dim valorUnitEFR As Double
    Dim descrAnaEfr As String
    Dim j As Integer
    
    
    If fa_movi_resp(iResp).analises(iAna).flg_estado_tx = 3 Or fa_movi_resp(iResp).analises(iAna).flg_estado_doe = 3 Then
        BG_Mensagem mediMsgBox, "An�lise j� faturada.", vbOKOnly + vbInformation, "Taxas"
        FACTUS_AlteraIsencao = False
        Exit Function
    End If
    
    
    isencaoAnterior = BL_HandleNull(fa_movi_resp(iResp).analises(iAna).cod_isen_doe, gTipoIsencaoIsento)
    
    If IF_ContaMembros(cod_ana, CStr(fa_movi_resp(iResp).cod_efr)) <= 0 Then
        fa_movi_resp(iResp).analises(iAna).qtd = qtd
    End If
    
    fa_movi_resp(iResp).analises(iAna).cod_isen_doe = TipoIsencao
    FACTUS_PreencheValoresAna iResp, iAna, cod_ana, Bg_DaData_ADO, cod_hemodialise, 0, qtd, cod_rubr
'
'    If FACTUS_AdicionaAnalise(CStr(fa_movi_resp(iResp).cod_efr), cod_ana, fa_movi_resp(iResp).analises(iAna).dt_ini_real, 0, cod_hemodialise, _
'                            TipoIsencao, fa_movi_resp(iResp).analises(iAna).iAnalise, _
'                            fa_movi_resp(iResp).n_benef_doe, fa_movi_resp(iResp).analises(iAna).t_doente, _
'                            fa_movi_resp(iResp).analises(iAna).doente, fa_movi_resp(iResp).analises(iAna).episodio, _
'                            fa_movi_resp(iResp).analises(iAna).nr_req_ars, fa_movi_resp(iResp).analises(iAna).convencao, _
'                            fa_movi_resp(iResp).analises(iAna).cod_medico, fa_movi_resp(iResp).analises(iAna).cod_dom, _
'                            fa_movi_resp(iResp).analises(iAna).km_dom, fa_movi_resp(iResp).analises(iAna).cod_post_dom, _
'                            fa_movi_resp(iResp).analises(iAna).localidade_dom, fa_movi_resp(iResp).analises(iAna).cod_u_saude) = True Then
'        'REJEITA LINHA ATUAL
'        If FACTUS_EliminaAna(iResp, iAna) = False Then
'            GoTo TrataErro
'        End If
'    End If
    FACTUS_AlteraIsencao = True
Exit Function
TrataErro:
    BG_LogFile_Erros "AlteraIsencao: " & Err.Number & " - " & Err.Description, "FACTUS", "AlteraIsencao"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------

' VERIFICA SE CREDENCIAL FOI ENVIADA NOUTRA REQUISICAO

' --------------------------------------------------------------------------------------------
Private Function FACTUS_VerificaCredJaExistente(ByVal iResp As Integer, iAnaFact As Integer) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsCred As New adodb.recordset
    'UALIA-905 Adicionado -> flg_estado <> 2 (Rejeitado)
    ssql = "SELECT * FROM fa_movi_fact WHERE nr_req_ars = " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars)
    ssql = ssql & " AND episodio <> " & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAnaFact).episodio) & " AND flg_estado <> 2"
    rsCred.CursorLocation = adUseClient
    rsCred.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsCred.Open ssql, gConexaoSecundaria
    If rsCred.RecordCount > 0 Then
        FACTUS_VerificaCredJaExistente = True
    Else
        FACTUS_VerificaCredJaExistente = False
    End If
    rsCred.Close
    Set rsCred = Nothing
Exit Function
TrataErro:
     FACTUS_VerificaCredJaExistente = False
    BG_LogFile_Erros "Erro ao FACTUS_VerificaCredJaExistente : " & " " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_VerificaCredJaExistente", True
    Exit Function
    Resume Next
End Function

'BRUNODSANTOS LJMANSO-307 09.08.2018
Public Sub FACTUS_AtualizaTipoReqFromConvencao(ByVal convencao As Integer, ByVal iResp As Integer, ByRef iAnaFact As Integer)
    Dim i As Integer
    'edgar.parada LJMANSO-351 21.01.2019 retirado o ciclo
    'For i = 1 To fa_movi_resp(iResp).totalAna
        
        If convencao = lCodInternacional Then
            fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "2"
            fa_movi_resp(iResp).analises(iAnaFact).convencao = lCodInternacional
        ElseIf convencao = lCodNormal Then
            fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "0"
            fa_movi_resp(iResp).analises(iAnaFact).convencao = lCodNormal
        ElseIf convencao = lCodProfissional Then
             fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "1"
             fa_movi_resp(iResp).analises(iAnaFact).convencao = lCodProfissional
        'edgar.parada LOTE 97 - Conven��o ESP
        ElseIf convencao = lCod97 Then
           fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "97"
           fa_movi_resp(iResp).analises(iAnaFact).convencao = lCod97
        '
        'edgar.parada LJMANSO-351 11.01.2019
        ElseIf convencao = lCodNormalEspecial Then
           fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "3"
           fa_movi_resp(iResp).analises(iAnaFact).convencao = lCodNormalEspecial
        ElseIf convencao = lCodInternacionalEspecial Then
           fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "5"
           fa_movi_resp(iResp).analises(iAnaFact).convencao = lCodInternacionalEspecial
        ElseIf convencao = lCodProfissionalEspecial Then
           fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "4"
           fa_movi_resp(iResp).analises(iAnaFact).convencao = lCodProfissionalEspecial
        End If
   ' Next i
    
    
End Sub
'edgar.parada Glintt-HS-19165 * novo parametro que define se a credencial � exportada
'edgar.parada LJMANSO-351 retirado o parametro convencao
'Vai usar a convecao pre-carrega na estrutura
Public Function FACTUS_ValidaRegras(existeCredImportada As Boolean, countEstrutCat As Integer) As Boolean
    On Error GoTo TrataErro
    Dim iResp As Integer
    Dim iAnaFact As Integer
    Dim lastP1 As String
    
    For iResp = 1 To fa_movi_resp_tot
        lastP1 = ""
    
        ' VERIFICA SE TODAS ANALISES DA ADSE TEM MEDICO PREENCHIDO
        If EFR_Verifica_ADSE(CStr(fa_movi_resp(iResp).cod_efr)) = True Then
            For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
                If fa_movi_resp(iResp).analises(iAnaFact).cod_medico = "" And fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 1 Then
                    BG_Mensagem mediMsgBox, "An�lises da ADSE sem m�dico indicado", vbOKOnly + vbInformation, "ADSE"
                    FACTUS_ValidaRegras = False
                    Exit Function
                End If
            Next iAnaFact
        End If
        
        For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
            'BRUNODSANTOS LJMANSO-307 09.08.2018
            'edgar.parada LJMANSO-351 Comentado
            'fa_movi_resp(iResp).analises(iAnaFact).convencao = convencao
            '
            If Len(fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars) = 19 And fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars <> lastP1 Then
                ' VALIDA ALGORITMO PARA CREDENCIAL ARS
                If IF_ValidaReqArs(fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars, True) = False Then
                    BG_Mensagem mediMsgBox, "Requisi��o ARS(P1) n�o � v�lido:" & fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars, vbOKOnly + vbInformation, "Factura��o"
                    FACTUS_ValidaRegras = False
                    Exit Function
                End If
                'VERIFICA SE CREDENCIAL J� EXISTE NO FACTUS
                If FACTUS_VerificaCredJaExistente(iResp, iAnaFact) = True And countEstrutCat = 1 Then
                    BG_Mensagem mediMsgBox, "Credencial j� existente anteriormente", vbOKOnly + vbInformation, "Factura��o"
                    FACTUS_ValidaRegras = False
                    Exit Function
                End If
                'VERIFICA SE CREDENCIAL TEM APENAS 6 AN�LISES
                'If EFR_Verifica_ADSE(CStr(fa_movi_resp(iResp).cod_efr)) = False Then
                '                        'edgar.parada Glintt-HS-19165 * valida se credencial importada
                '    If Not existeCredImportada And countEstrutCat <= 0 Then
                '       If FACTUS_VerificaTotalAnaCred(iResp, iAnaFact) = False Then
                '          BG_Mensagem mediMsgBox, "Existem credenciais da ARS com mais que 6 an�lises", vbOKOnly + vbInformation, "Factura��o"
                '          FACTUS_ValidaRegras = False
                '          Exit Function
                '       End If
                '    End If
                'End If
            End If
            ' ------------------------------------------------------------------------------------
            ' FLG_TIP_REQ - TIPO DE DOMICILIO(N,D,I,C) CONCATENADO COM TIPO DE P1(B,V)
            ' HA ENTIDADES EM QUE DOMICILIO NAO E COMPARTICIPADO PELO QUE NAO PODE IR PARA FACTUS
            ' -------------------------------------------------------------------------------------
            If fa_movi_resp(iResp).flg_compart_dom = "1" Then
                ' VERIFICA SE PARA EFR CAUSA SE ENVIA SEMPRE DOMICILIO
                If fa_movi_resp(iResp).flg_dom_defeito <> "1" Then
                    If fa_movi_resp(iResp).analises(iAnaFact).cod_dom > 0 Then
                        If Len(fa_movi_resp(iResp).analises(iAnaFact).cod_post_dom) <> 8 Then
                            BG_Mensagem mediMsgBox, "O c�digo postal deve ser no formato XXXX-XXX. Requisi��o n�o enviada.", vbOKOnly + vbInformation, "Factura��o"
                            FACTUS_ValidaRegras = False
                            Exit Function
                        End If
                        
                        ' VERIFICA SE ESTAO SOB REGRA DOS 65 ANOS
                        If fa_movi_resp(iResp).analises(iAnaFact).cod_isen_doe = gTipoIsencaoBorla Or fa_movi_resp(iResp).analises(iAnaFact).cod_isen_doe = gTipoIsencaoNaoIsento Then
'                            If convencao = lCodInternacional Then
'                                fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "2"
'                            ElseIf convencao = lCodNormal Then
'                                fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "0"
'                            Else
'                                fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "1"
'                            End If
                            'BRUNODSANTOS LJMANSO-307 09.08.2018
                            'edgar.parada LJMANSO-351 21.01.2019 novo parametro IAnafact
                            Call FACTUS_AtualizaTipoReqFromConvencao(fa_movi_resp(iResp).analises(iAnaFact).convencao, iResp, iAnaFact)
                            '
                        Else
'                            If convencao = lCodInternacional Then
'                                fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "2"
'                            ElseIf convencao = lCodNormal Then
'                                fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "0"
'                            Else
'                                fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "1"
'                            End If
                            'BRUNODSANTOS LJMANSO-307 09.08.2018
                            'edgar.parada LJMANSO-351 21.01.2019 novo parametro IAnafact
                            Call FACTUS_AtualizaTipoReqFromConvencao(fa_movi_resp(iResp).analises(iAnaFact).convencao, iResp, iAnaFact)
                            '
                        End If
                        
                        ' VERIFICA SE ENTIDADE PODE FACTURAR NAO URBANOS
                        If fa_movi_resp(iResp).flg_compart_dom = "0" Then
                            fa_movi_resp(iResp).analises(iAnaFact).cod_dom = lCodUrbano
                        End If
                        If fa_movi_resp(iResp).analises(iAnaFact).cod_dom = lCodUrbano Then
                            fa_movi_resp(iResp).analises(iAnaFact).km_dom = 1
                        ElseIf fa_movi_resp(iResp).analises(iAnaFact).cod_dom = lCodUrbanoBiDiario Then
                            fa_movi_resp(iResp).analises(iAnaFact).km_dom = 2
                        End If
                    End If
                Else
'                    If convencao = lCodInternacional Then
'                        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "2"
'                    ElseIf convencao = lCodNormal Then
'                        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "0"
'                    Else
'                        fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "1"
'                    End If
                    'BRUNODSANTOS LJMANSO-307 09.08.2018
                    'edgar.parada LJMANSO-351 21.01.2019 novo parametro IAnafact
                    Call FACTUS_AtualizaTipoReqFromConvencao(fa_movi_resp(iResp).analises(iAnaFact).convencao, iResp, iAnaFact)
                    '
                    fa_movi_resp(iResp).analises(iAnaFact).cod_dom = lCodUrbano
                    fa_movi_resp(iResp).analises(iAnaFact).km_dom = 1
               End If
            Else
'                If convencao = lCodInternacional Then
'                    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "2"
'                ElseIf convencao = lCodNormal Then
'                    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "0"
'                Else
'                    fa_movi_resp(iResp).analises(fa_movi_resp(iResp).totalAna).flg_tip_req = "1"
'                End If
                'BRUNODSANTOS LJMANSO-307 09.08.2018
                'edgar.parada LJMANSO-351 21.01.2019 novo parametro IAnafact
                Call FACTUS_AtualizaTipoReqFromConvencao(fa_movi_resp(iResp).analises(iAnaFact).convencao, iResp, iAnaFact)
                '
                fa_movi_resp(iResp).analises(iAnaFact).cod_dom = 0
                fa_movi_resp(iResp).analises(iAnaFact).km_dom = 0
                 
           End If
'            ' If fa_movi_resp(iResp).analises(iAnaFact).convencao = gConvencaoInternacional Then
'                 If fa_movi_resp(iResp).flg_novaArs = 1 Then
'                     fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "2"
'                 Else
'                     fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "I"
'                 End If
'            ElseIf fa_movi_resp(iResp).analises(iAnaFact).convencao = gConvencaoProfissional Then
'                 If fa_movi_resp(iResp).flg_novaArs = 1 Then
'                     fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "1"
'                 Else
'                     fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "P"
'                 End If
'            'NELSONPSILVA LJMANSO-307 17.08.2018
'            ElseIf fa_movi_resp(iResp).analises(iAnaFact).convencao = gconvencaoNormal Then
'                 If fa_movi_resp(iResp).flg_novaArs = 1 Then
'                     fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "0"
'                 Else
'                     fa_movi_resp(iResp).analises(iAnaFact).flg_tip_req = "N"
'                 End If
'            End If
        
        Next iAnaFact
    Next iResp
    
    FACTUS_ValidaRegras = True
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_ValidaRegras: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_ValidaRegras"
    FACTUS_ValidaRegras = False
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------

' VERIFICA SE P1 tem mais que 6 analises

' --------------------------------------------------------------------------------------------
Private Function FACTUS_VerificaTotalAnaCred(ByVal iResp As Integer, ByVal iAnaFact As Integer) As Boolean
    Dim i As Integer
    Dim totalAna As Integer
    On Error GoTo TrataErro
    
    FACTUS_VerificaTotalAnaCred = False
    totalAna = 0
    
    For i = 1 To fa_movi_resp(iResp).totalAna
        If fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars = fa_movi_resp(iResp).analises(i).nr_req_ars And fa_movi_resp(iResp).analises(i).flg_estado_doe <> 1 Then
            If fa_movi_resp(iResp).analises(i).flg_estado <> 2 And fa_movi_resp(iResp).analises(i).flg_estado <> 8 Then
                totalAna = totalAna + 1
                If totalAna > 6 Then
                    FACTUS_VerificaTotalAnaCred = False
                    Exit Function
                End If
            End If
        End If
    Next
    FACTUS_VerificaTotalAnaCred = True
Exit Function
TrataErro:
     FACTUS_VerificaTotalAnaCred = False
    BG_LogFile_Erros "Erro ao FACTUS_VerificaTotalAnaCred : " & " " & Err.Number & " - " & Err.Description, "Envio_Factus", "FACTUS_VerificaTotalAnaCred", True
    Exit Function
    Resume Next
End Function

Private Function FACTUS_InsereMoviFactADSE(iResp As Integer, iAnaFact As Integer) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim iRes As Integer
    
    ssql = "INSERT INTO sl_movi_fact_adse (n_req, n_seq_prog, p1, cod_proven, cod_medico, user_cri, dt_cri) VALUES(" & fa_movi_resp(iResp).episodio & ","
    ssql = ssql & BL_TrataNumberParaBD(str(fa_movi_resp(iResp).analises(iAnaFact).n_seq_prog)) & ","
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars) & ","
    ssql = ssql & BL_TrataStringParaBD(CStr(fa_movi_resp(iResp).analises(iAnaFact).cod_u_saude)) & ","
    ssql = ssql & BL_TrataStringParaBD(Trim(fa_movi_resp(iResp).analises(iAnaFact).cod_medico)) & "," & CStr(gCodUtilizador) & ",sysdate)"
    iRes = BG_ExecutaQuery_ADO(ssql)
    If iRes = 1 Then
        FACTUS_InsereMoviFactADSE = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_InsereMoviFactADSE: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_InsereMoviFactADSE"
    FACTUS_InsereMoviFactADSE = False
    Exit Function
    Resume Next
End Function

Private Function FACTUS_AtualizaMoviFactADSE(iResp As Integer, iAnaFact As Integer) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim iRes As Integer
    'LJMANSO-309 - Trim(fa_movi_resp(iResp).analises(iAnaFact).cod_medico))
    ssql = "UPDATE sl_movi_fact_adse SET "
    ssql = ssql & " n_req =" & BL_TrataStringParaBD(fa_movi_resp(iResp).episodio) & ","
    ssql = ssql & " p1 =" & BL_TrataStringParaBD(fa_movi_resp(iResp).analises(iAnaFact).nr_req_ars) & ","
    ssql = ssql & "  cod_proven = " & BL_TrataStringParaBD(BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAnaFact).cod_u_saude))) & ","
    ssql = ssql & " cod_medico = " & BL_TrataStringParaBD(Trim(fa_movi_resp(iResp).analises(iAnaFact).cod_medico)) & ", user_cri = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", dt_cri = sysdate "
    ssql = ssql & " WHERE n_seq_prog = " & BL_TrataNumberParaBD(CStr(fa_movi_resp(iResp).analises(iAnaFact).n_seq_prog))
    iRes = BG_ExecutaQuery_ADO(ssql)
    If iRes = 1 Then
        FACTUS_AtualizaMoviFactADSE = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_InsereMoviFactADSE: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_InsereMoviFactADSE"
    FACTUS_AtualizaMoviFactADSE = False
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' insere os dados do certificado do lado do sislab

' -----------------------------------------------------------------------------------------------
Public Function FACTUS_FaturaDoente(iResp As Integer, cod_frm_pag As Integer) As Boolean
    On Error GoTo TrataErro
    
    Dim CmdFatura As New adodb.Command
    Dim PmtNReq As adodb.Parameter
    Dim PmtFrmPag As adodb.Parameter
    Dim PmtComp As adodb.Parameter
    Dim PmtCodEFR As adodb.Parameter
    Dim PmtNOrd As adodb.Parameter
    Dim PmtUSER As adodb.Parameter
    Dim PmtRET As adodb.Parameter
    Dim PmtRes As adodb.Parameter
    Dim resultado As Long
    Dim resultado2 As String
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdFatura
        .ActiveConnection = gConexao
        .CommandText = "SLP_FATURA_DOENTE2"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtRET = CmdFatura.CreateParameter("R_RES", adNumeric, adParamOutput, 0)
    PmtRET.Precision = 12
    Set PmtRes = CmdFatura.CreateParameter("R_RESULTADO", adVarChar, adParamOutput, 200)
    Set PmtNReq = CmdFatura.CreateParameter("N_REQ", adVarChar, adParamInput, 10)
    Set PmtFrmPag = CmdFatura.CreateParameter("A_COD_FRM_PAG", adNumeric, adParamInput, 0)
    Set PmtComp = CmdFatura.CreateParameter("A_DESCR_COMPUTADOR", adVarChar, adParamInput, 50)
    Set PmtCodEFR = CmdFatura.CreateParameter("A_COD_EFR", adNumeric, adParamInput, 0)
    Set PmtNOrd = CmdFatura.CreateParameter("A_N_ORD", adNumeric, adParamInput, 0)
    Set PmtUSER = CmdFatura.CreateParameter("A_COD_UTIL", adVarChar, adParamInput, 50)
    
    CmdFatura.Parameters.Append PmtNReq
    CmdFatura.Parameters.Append PmtFrmPag
    CmdFatura.Parameters.Append PmtComp
    CmdFatura.Parameters.Append PmtCodEFR
    CmdFatura.Parameters.Append PmtNOrd
    CmdFatura.Parameters.Append PmtUSER
    CmdFatura.Parameters.Append PmtRET
    CmdFatura.Parameters.Append PmtRes
    
    CmdFatura.Parameters("N_REQ") = fa_movi_resp(iResp).episodio
    CmdFatura.Parameters("A_COD_FRM_PAG") = cod_frm_pag
    CmdFatura.Parameters("A_DESCR_COMPUTADOR") = CStr(gComputador)
    CmdFatura.Parameters("A_COD_EFR") = fa_movi_resp(iResp).cod_efr
    CmdFatura.Parameters("A_N_ORD") = fa_movi_resp(iResp).n_ord
    CmdFatura.Parameters("A_COD_UTIL") = CStr(gCodUtilizador)
    CmdFatura.Execute
    
    resultado = Trim(CmdFatura.Parameters.Item("R_RES").value)
    resultado2 = Trim(BL_HandleNull(CmdFatura.Parameters.Item("R_RESULTADO").value, ""))
    If resultado = 0 Then
        FACTUS_FaturaDoente = True
    Else
        FACTUS_FaturaDoente = False
        BG_LogFile_Erros "Erro ao faturar Doente:" & resultado & " - " & resultado2, "FACTUS", "FACTUS_FaturaDoente", True
        
    End If
    
    Set CmdFatura = Nothing
    Set PmtRET = Nothing
    Set PmtNReq = Nothing
    Set PmtFrmPag = Nothing
    Set PmtComp = Nothing
    Set PmtCodEFR = Nothing
    Set PmtNOrd = Nothing
    Set PmtUSER = Nothing


Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao faturar:" & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_FaturaDoente", True
    FACTUS_FaturaDoente = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' insere os dados do certificado do lado do sislab

' -----------------------------------------------------------------------------------------------
Public Function FACTUS_RetornaCertificado(serie_fac As String, n_fac As Long) As String
    On Error GoTo TrataErro
    
    Dim CmdFatura As New adodb.Command
    Dim PmtNReq As adodb.Parameter
    Dim PmtFrmPag As adodb.Parameter
    Dim PmtComp As adodb.Parameter
    Dim PmtCodEFR As adodb.Parameter
    Dim PmtNOrd As adodb.Parameter
    Dim PmtUSER As adodb.Parameter
    Dim PmtRET As adodb.Parameter
    Dim resultado As Long
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdFatura
        .ActiveConnection = gConexao
        .CommandText = "SLP_DEVOLVE_CERTIFICADO"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtRET = CmdFatura.CreateParameter("P_CERTIFICADO", adVarChar, adParamOutput, 400)
    Set PmtNReq = CmdFatura.CreateParameter("V_SERIE_REC", adVarChar, adParamInput, 10)
    Set PmtFrmPag = CmdFatura.CreateParameter("V_N_REC", adNumeric, adParamInput, 0)
    
    CmdFatura.Parameters.Append PmtNReq
    CmdFatura.Parameters.Append PmtFrmPag
    CmdFatura.Parameters.Append PmtRET
    
    CmdFatura.Parameters("V_SERIE_REC") = serie_fac
    CmdFatura.Parameters("V_N_REC") = n_fac
    CmdFatura.Execute
    
    FACTUS_RetornaCertificado = Trim(BL_HandleNull(CmdFatura.Parameters.Item("P_CERTIFICADO").value, ""))
    
    Set CmdFatura = Nothing
    Set PmtRET = Nothing
    Set PmtNReq = Nothing
    Set PmtFrmPag = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao faturar:" & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_RetornaCertificado", True
    FACTUS_RetornaCertificado = ""
    Exit Function
    Resume Next
End Function

Private Function FACTUS_InsereDadosDoc(iRec As Integer, Estado As String, flg_borla As String) As Boolean
    Dim data As String
    Dim ssql As String
    FACTUS_InsereDadosDoc = False
    If Estado = gEstadoReciboCobranca Then
        data = "null"
    Else
        data = " dt_emiss_doc "
    End If
    
    ssql = "INSERT INTO sl_dados_doc (tipo, serie_doc, n_doc, desconto, dt_pagamento, flg_borla) "
    ssql = ssql & " SELECT tipo, serie_doc,n_doc,0, " & data & "," & BL_TrataStringParaBD(flg_borla) & "  from slv_documentos_doente WHERE (tipo, serie_doc, n_doc) NOT IN ("
    ssql = ssql & " SELECT tipo, serie_doc, n_doc FROM sl_dados_doc) AND episodio = " & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.EcNumReq.text)
    BG_ExecutaQuery_ADO ssql
    FACTUS_InsereDadosDoc = True

End Function
Public Function FACTUS_ReciboCobranca(iRec As Integer, Estado As String) As Boolean
    Dim data As String
    Dim ssql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    
    If Estado = gEstadoReciboCobranca Then
        data = "null"
    Else
        data = "trunc(sysdate)"
    End If
    ssql = "UPDATE sl_dados_doc SET dt_pagamento = " & data & " WHERE tipo = " & BL_TrataStringParaBD(RecibosNew(iRec).tipo)
    ssql = ssql & " AND serie_doc = " & BL_TrataStringParaBD(RecibosNew(iRec).serie_fac_doe)
    ssql = ssql & " AND n_doc = " & BL_TrataStringParaBD(CStr(RecibosNew(iRec).n_Fac_doe))
    
    iReg = BG_ExecutaQuery_ADO(ssql)
    If iReg = 0 Then
        FACTUS_ReciboCobranca = FACTUS_InsereDadosDoc(iRec, Estado, "N")
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao alterar estado:" & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_ReciboCobranca", True
    FACTUS_ReciboCobranca = False
    Exit Function
    Resume Next
End Function

Public Function FACTUS_ReciboBorla(iRec As Integer, flg_borla As String) As Boolean
    Dim data As String
    Dim ssql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    
    ssql = "UPDATE sl_dados_doc SET flg_borla = " & BL_TrataStringParaBD(flg_borla) & " WHERE tipo = " & BL_TrataStringParaBD(RecibosNew(iRec).tipo)
    ssql = ssql & " AND serie_doc = " & BL_TrataStringParaBD(RecibosNew(iRec).serie_fac_doe)
    ssql = ssql & " AND n_doc = " & BL_TrataStringParaBD(CStr(RecibosNew(iRec).n_Fac_doe))
    
    iReg = BG_ExecutaQuery_ADO(ssql)
    If iReg = 0 Then
        FACTUS_ReciboBorla = FACTUS_InsereDadosDoc(iRec, gEstadoReciboPago, flg_borla)
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao alterar borla:" & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_ReciboBorla", True
    FACTUS_ReciboBorla = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' insere os dados do certificado do lado do sislab

' -----------------------------------------------------------------------------------------------
Public Function FACTUS_CreditaFatura(iRec As Integer, id_mot As Integer, id_frm_pag As Integer) As Boolean
    On Error GoTo TrataErro
    Dim Serie_nota_cred As String
    Dim n_nota_cred As Long
    
    Dim CmdFatura As New adodb.Command
    Dim PmtSerieFac As adodb.Parameter
    Dim PmtNFac As adodb.Parameter
    Dim PmtCodUtilizador As adodb.Parameter
    Dim PmtDtEmissao As adodb.Parameter
    Dim PmtCodCaixa As adodb.Parameter
    Dim PmtCkdevolucaoUtente As adodb.Parameter
    Dim PmtMotivo As adodb.Parameter
    Dim PmtSerieDef As adodb.Parameter
    Dim PmtFrmPag As adodb.Parameter
    Dim PmtSerieNC As adodb.Parameter
    Dim PmtNumNC As adodb.Parameter
    Dim PmtEmpresaID As adodb.Parameter
    Dim PmtLibertaAdiantamento As adodb.Parameter
    Dim resultado As Long
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdFatura
        .ActiveConnection = gConexao
        .CommandText = "SLP_GERA_NOTA_CREDITO_FACTUS"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtSerieNC = CmdFatura.CreateParameter("O_SERIE_NOTA_CRED", adVarChar, adParamOutput, 20)
    Set PmtNumNC = CmdFatura.CreateParameter("O_N_NOTA_CRED", adNumeric, adParamOutput, 10)
    
    Set PmtSerieFac = CmdFatura.CreateParameter("I_SERIE_FAC", adVarChar, adParamInput, 10)
    Set PmtNFac = CmdFatura.CreateParameter("I_N_FAC", adNumeric, adParamInput, 0)
    Set PmtCodUtilizador = CmdFatura.CreateParameter("I_COD_UTILIZADOR", adNumeric, adParamInput, 0)
    Set PmtDtEmissao = CmdFatura.CreateParameter("I_DT_EMISS_NOTA_CRED", adDate, adParamInput, 0)
    Set PmtCodCaixa = CmdFatura.CreateParameter("I_COD_CAIXA", adNumeric, adParamInput, 0)
    Set PmtCkdevolucaoUtente = CmdFatura.CreateParameter("I_CK_DEVOUCAO_DOENTE", adVarChar, adParamInput, 10)
    Set PmtMotivo = CmdFatura.CreateParameter("I_ID_MOT_CREDITO", adNumeric, adParamInput, 0)
    Set PmtSerieDef = CmdFatura.CreateParameter("I_SERIE_DEF", adVarChar, adParamInput, 20)
    Set PmtFrmPag = CmdFatura.CreateParameter("I_COD_FRM_PAG", adNumeric, adParamInput, 0)
    Set PmtEmpresaID = CmdFatura.CreateParameter("I_EMPRESA_ID", adVarChar, adParamInput, 20)
    Set PmtLibertaAdiantamento = CmdFatura.CreateParameter("I_LIBERTA_ADIA", adVarChar, adParamInput, 20)
    
    CmdFatura.Parameters.Append PmtSerieFac
    CmdFatura.Parameters.Append PmtNFac
    CmdFatura.Parameters.Append PmtCodUtilizador
    CmdFatura.Parameters.Append PmtDtEmissao
    CmdFatura.Parameters.Append PmtCodCaixa
    CmdFatura.Parameters.Append PmtCkdevolucaoUtente
    CmdFatura.Parameters.Append PmtMotivo
    CmdFatura.Parameters.Append PmtSerieDef
    CmdFatura.Parameters.Append PmtFrmPag
    CmdFatura.Parameters.Append PmtEmpresaID
    CmdFatura.Parameters.Append PmtLibertaAdiantamento
    CmdFatura.Parameters.Append PmtSerieNC
    CmdFatura.Parameters.Append PmtNumNC
    
    CmdFatura.Parameters("I_SERIE_FAC") = RecibosNew(iRec).serie_fac_doe
    CmdFatura.Parameters("I_N_FAC") = RecibosNew(iRec).n_Fac_doe
    CmdFatura.Parameters("I_COD_UTILIZADOR") = CStr(gCodUtilizador)
    CmdFatura.Parameters("I_DT_EMISS_NOTA_CRED") = Bg_DaData_ADO
    CmdFatura.Parameters("I_COD_CAIXA") = 1
    CmdFatura.Parameters("I_CK_DEVOUCAO_DOENTE") = "S"
    CmdFatura.Parameters("I_ID_MOT_CREDITO") = id_mot
    CmdFatura.Parameters("I_SERIE_DEF") = GSerieNotaCred
    CmdFatura.Parameters("I_COD_FRM_PAG") = id_frm_pag
    CmdFatura.Parameters("I_EMPRESA_ID") = RecibosNew(iRec).empresa_id
    CmdFatura.Parameters("I_LIBERTA_ADIA") = "N"
    CmdFatura.Execute
    
    Serie_nota_cred = Trim(CmdFatura.Parameters.Item("O_SERIE_NOTA_CRED").value)
    n_nota_cred = Trim(CmdFatura.Parameters.Item("O_N_NOTA_CRED").value)
    
    If Serie_nota_cred <> "" And Serie_nota_cred <> "-1" Then
        FACTUS_CreditaFatura = True
    Else
        FACTUS_CreditaFatura = False
    End If
    
    Set PmtSerieFac = Nothing
    Set PmtNFac = Nothing
    Set PmtCodUtilizador = Nothing
    Set PmtDtEmissao = Nothing
    Set PmtCodCaixa = Nothing
    Set PmtCkdevolucaoUtente = Nothing
    Set PmtMotivo = Nothing
    Set PmtSerieDef = Nothing
    Set PmtFrmPag = Nothing
    Set PmtSerieNC = Nothing
    Set PmtNumNC = Nothing
    Set PmtEmpresaID = Nothing
    Set PmtLibertaAdiantamento = Nothing
    
    Set CmdFatura = Nothing


Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao creditar:" & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_CreditaFatura", True
    FACTUS_CreditaFatura = False
    Exit Function
    Resume Next
End Function



Public Sub FACTUS_ImprimeRecibo(descricao As String, n_req As String, NumDoc As Long, serieRec As String, _
                                codEfr As Long, impressora As String, seq_utente As Long, tipo As String)
    On Error GoTo TrataErro
    Dim continua As Boolean
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim num_recibo As Long
    Dim data As String
    Dim efr As String
    Dim nome As String
    Dim num_benef As String
    Dim analises As String
    Dim ult_analise As String
    Dim total As Double
    Dim Tot As String
    Dim taxa_ana As String
    Dim user_emi As String
    Dim DataChega As String
    Dim cod_empresa As String
    Dim nome_empresa As String
    Dim num_contribuinte As String
    Dim n_contrib_ute As String
    Dim descr_analise As String
    Dim total_original As Double
    Dim Desconto As Double
    Dim num_copias_rec As Integer
    Dim Cabecalho As String
    Dim Formacao As String
    Dim Certificado As String
    Dim totalPagar As Double
    Dim flgDocCaixa As String
    Dim morada As String
    Dim cod_postal As String
    Dim descr_postal As String
    Dim i As Integer
    'NELSONPSILVA GLINTT-HS-21312
    Dim qtd As Integer
    'Dim descr_ana As String
    
    Dim vd As String
    
    If gReciboCrystal = 1 Then
        ssql = "DELETE FROM SL_CR_RECIBO WHERE nome_computador = '" & BG_SYS_GetComputerName & "'"
        ssql = ssql & " AND num_sessao = " & gNumeroSessao
        BG_ExecutaQuery_ADO ssql
    End If
    
    'soliveira lacto **
    If tipo = "R" Or tipo = "C" Then
        ssql = " SELECT sl_requis.dt_chega,sl_requis.dt_previ, fa_fact.descr_efr descr_efr, sl_identif.nome_ute, fa_lin_fact.cod_rubr cod_ana,fa_lin_fact.descr_rubr descr_ana,"
        ssql = ssql & " fa_lin_fact.val_total taxa, sl_requis.n_benef n_benef_req, fa_lin_fact.cod_rubr_efr cod_ana_efr, fa_fact.dt_emiss_fac dt_emi , fa_fact.user_cri user_emi ,"
        ssql = ssql & " sl_dados_doc.venda_dinheiro, fa_fact.n_benef_doe n_benef_rec, sl_dados_doc.desconto,  fa_fact.empresa_id cod_empresa, sl_empresas.nome_empresa,"
        ssql = ssql & " sl_empresas.num_contribuinte, null cod_facturavel, fa_fact.val_total valor_original, sl_efr.num_copias_rec, sl_empresas.cabecalho,"
        ssql = ssql & " fa_fact.val_total total_pagar, 0 flg_doc_caixa,  fa_fact.n_contrib_doe N_CONTRIB_UTE,"
        ssql = ssql & " sl_identif.descr_mor_ute , sl_identif.cod_postal_ute, sl_cod_postal.descr_postal, fa_lin_fact.n_lin_fac, fa_fact.obs_efr, fa_lin_fact.qtd"
        'GX-62545
        ssql = ssql & "   ,fa_fact.atcud,fa_fact.msg_qrcode "
        '
        ssql = ssql & " from fa_fact join fa_lin_fact on fa_fact.serie_fac = fa_lin_fact.serie_fac and fa_fact.n_fac = fa_lin_fact.n_fac"
        ssql = ssql & " join sl_requis on fa_fact.episodio = sl_requis.n_req"
        ssql = ssql & " join sl_empresas on fa_fact.empresa_id = sl_empresas.cod_empresa"
        ssql = ssql & " join sl_efr on fa_fact.cod_Efr = sl_efr.cod_efr"
        ssql = ssql & " join sl_dados_doc on fa_fact.serie_fac = sl_dados_doc.serie_doc and fa_fact.n_fac = sl_dados_doc.n_doc  and sl_dados_doc.tipo in ('R', 'C')"
        If seq_utente = -1 Then
            ssql = ssql & " join sl_identif on sl_identif.seq_utente = sl_requis.seq_utente"
        Else
            ssql = ssql & " join sl_identif ON sl_identif.seq_utente = sl_dados_doc.seq_utente "
        End If
        ssql = ssql & " left outer join sl_cod_postal on sl_identif.cod_postal_ute = sl_cod_postal.cod_postal"
        ssql = ssql & " WHERE fa_fact.serie_fac = " & BL_TrataStringParaBD(serieRec)
        ssql = ssql & " AND fa_fact.n_fac = " & NumDoc
        ssql = ssql & " AND sl_requis.n_req = " & n_req
        ssql = ssql & " ORDER by n_lin_fac "
    ElseIf tipo = "NC" Then
        ssql = "SELECT sl_requis.dt_chega,sl_requis.dt_previ, fa_nota_cred.descr_efr descr_efr, sl_identif.nome_ute, fa_lin_fact.cod_rubr cod_ana,fa_lin_fact.descr_rubr"
        ssql = ssql & " descr_ana, fa_lin_fact.val_total taxa, sl_requis.n_benef n_benef_req, fa_lin_fact.cod_rubr_efr cod_ana_efr, fa_nota_cred.dt_emiss_nota_cred dt_emi ,"
        ssql = ssql & " fa_nota_cred.user_cri user_emi , sl_dados_doc.venda_dinheiro, fa_nota_cred.n_benef_doe n_benef_rec, sl_dados_doc.desconto,  fa_nota_cred.empresa_id cod_empresa,"
        ssql = ssql & "   sl_empresas.nome_empresa, sl_empresas.num_contribuinte, null cod_facturavel, fa_nota_cred.val_total valor_original, sl_efr.num_copias_rec,"
        ssql = ssql & "   sl_empresas.cabecalho, fa_nota_cred.val_total total_pagar, 0 flg_doc_caixa,  fa_fact.n_contrib_doe N_CONTRIB_UTE, sl_identif.descr_mor_ute ,"
        ssql = ssql & "   sl_identif.cod_postal_ute , sl_cod_postal.descr_postal, fa_lin_fact.n_lin_fac, fa_fact.obs_efr"
        'GX-62545
        ssql = ssql & "   ,fa_fact.atcud,fa_fact.msg_qrcode "
        '
        ssql = ssql & "   from fa_fact join fa_lin_fact on fa_fact.serie_fac = fa_lin_fact.serie_fac and fa_fact.n_fac = fa_lin_fact.n_fac, fa_lin_fact.qtd"
        ssql = ssql & "   join fa_nota_cred on fa_nota_cred.n_fac= fa_fact.n_fac and fa_nota_cred.serie_fac = fa_fact.serie_fac"
        ssql = ssql & "   join sl_requis on fa_fact.episodio = sl_requis.n_req"
        ssql = ssql & "   join sl_empresas on fa_fact.empresa_id = sl_empresas.cod_empresa"
        ssql = ssql & "   join sl_efr on fa_fact.cod_Efr = sl_efr.cod_efr"
        ssql = ssql & "   join sl_dados_doc on fa_fact.serie_fac = sl_dados_doc.serie_doc and fa_fact.n_fac = sl_dados_doc.n_doc  and sl_dados_doc.tipo = 'R'"
        If seq_utente = -1 Then
            ssql = ssql & " join sl_identif on sl_identif.seq_utente = sl_requis.seq_utente"
        Else
            ssql = ssql & " join sl_identif ON sl_identif.seq_utente = sl_dados_doc.seq_utente "
        End If
        ssql = ssql & "   left outer join sl_cod_postal on sl_identif.cod_postal_ute = sl_cod_postal.cod_postal"
        ssql = ssql & " WHERE fa_nota_cred.serie_nota_cred = " & BL_TrataStringParaBD(serieRec)
        ssql = ssql & " AND fa_nota_cred.n_nota_cred = " & NumDoc
        ssql = ssql & " AND sl_requis.n_req = " & n_req
        ssql = ssql & " ORDER by n_lin_fac "
    
    End If
    
    Set rsRecibos = New adodb.recordset
    rsRecibos.CursorType = adOpenStatic
    rsRecibos.CursorLocation = adUseClient
    rsRecibos.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao

    If rsRecibos.RecordCount > 0 Then
        
        'GX-62545
        If Bg_DaData_ADO >= gFactQrCodeDate Then
            If BL_HandleNull(rsRecibos!msg_qrcode, "") = "" Then
                BG_Mensagem mediMsgBox, "QR Code n�o encontrado no FACTUS!", vbExclamation + vbOKOnly, "Impress�o documento"
                Exit Sub
            End If
            If tipo = "R" Or tipo = "C" Then
                If BillingQRcode.ManageBillingQRCode((rsRecibos!msg_qrcode), n_req) = False Then
                    Exit Sub
                End If
            End If
        End If
        '
        
        num_recibo = NumDoc
        'data = BL_HandleNull(rsRecibos!dt_chega, rsRecibos!dt_previ)
        data = BL_HandleNull(rsRecibos!dt_emi, Bg_DaData_ADO)
        DataChega = BL_HandleNull(rsRecibos!dt_chega, rsRecibos!dt_previ)
        vd = BL_HandleNull(rsRecibos!venda_dinheiro, "")
        efr = BL_HandleNull(rsRecibos!obs_efr, "")
        nome = BL_HandleNull(rsRecibos!nome_ute, "")
        num_benef = BL_HandleNull(rsRecibos!n_benef_rec, BL_HandleNull(rsRecibos!n_benef_req, ""))
        ult_analise = ""
        analises = ""
        total = 0
        user_emi = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", rsRecibos!user_emi)
        cod_empresa = BL_HandleNull(rsRecibos!cod_empresa, "")
        nome_empresa = BL_HandleNull(rsRecibos!nome_empresa, "")
        total_original = BL_HandleNull(rsRecibos!valor_original, 0)
        totalPagar = BL_HandleNull(rsRecibos!total_pagar, 0)
        flgDocCaixa = BL_HandleNull(rsRecibos!flg_doc_caixa, "0")
        If BL_HandleNull(rsRecibos!n_contrib_ute, "") <> "" Then
            If Not IsNumeric(BL_HandleNull(rsRecibos!n_contrib_ute, "")) Then
                n_contrib_ute = "null"
            Else
                n_contrib_ute = BL_HandleNull(rsRecibos!n_contrib_ute, "")
            End If
        Else
            n_contrib_ute = "null"
        End If
        Desconto = BL_HandleNull(rsRecibos!Desconto, 0)
        'UALIA-870
        'num_copias_rec = BL_HandleNull(rsRecibos!num_copias_rec, 0)
        num_copias_rec = BL_DevolveNumCopiasRecibo(codEfr)
        '
        morada = BL_HandleNull(rsRecibos!descr_mor_ute, "")
        cod_postal = BL_HandleNull(rsRecibos!cod_postal_ute, "")
        descr_postal = BL_HandleNull(rsRecibos!descr_postal, "")
        
        If gDEMO = mediSim Then
            Certificado = ""
            Formacao = "Documento emitido para fins de forma��o"
            Cabecalho = "GLINTT HS"
        Else
            Certificado = FACTUS_RetornaCertificado(serieRec, num_recibo)
            Formacao = ""
            Cabecalho = BL_HandleNull(rsRecibos!Cabecalho, "")
            num_contribuinte = BL_HandleNull(rsRecibos!num_contribuinte, "")
        End If
        If Certificado = "" And gDEMO <> mediSim And tipo <> "C" Then
            BG_Mensagem mediMsgBox, "Erro ao imprimir documento. N�o � poss�vel obter certificado!", vbExclamation + vbOKOnly, "Impress�o documento"
            Exit Sub
        End If
        While Not rsRecibos.EOF
 '           If ult_analise <> rsRecibos!cod_ana Then
                
                '----
                'soliveira lacto para situacoes multicare e medis aplica o desconto em cada analise
                If Desconto < 0 Then
'                    taxa_ana = Round(rsRecibos!Taxa - (rsRecibos!Taxa * (RegistosR(indice).Desconto / 100)), 2)
                    taxa_ana = "---"
                ElseIf Desconto > 0 And gControlaDesconto <> mediSim Then
                    taxa_ana = "---"
                Else
                    taxa_ana = BL_HandleNull(rsRecibos!taxa, 0)
                    If InStr(1, taxa_ana, ",") = 0 Then
                        taxa_ana = taxa_ana & ",0"
                    ElseIf Len(Mid(taxa_ana, InStr(1, taxa_ana, ",") + 1)) = 1 Then
                        taxa_ana = taxa_ana & "0"
                    End If
                End If
                'NELSONPSILVA GLINTT-HS-21312
                qtd = BL_HandleNull(rsRecibos!qtd, 1)
                '
                If rsRecibos!cod_facturavel <> rsRecibos!cod_ana And Mid(rsRecibos!cod_facturavel, 1, 1) = "P" Then
                    descr_analise = BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", rsRecibos!cod_facturavel, "V")
                Else
                    descr_analise = rsRecibos!descr_ana
                End If
                analises = analises & Replace(Mid(BL_HandleNull(rsRecibos!cod_ana_efr, "") & "-" & descr_analise, 1, 26), Chr(160), "") & Space(26 - Len(Mid(Trim(BL_HandleNull(rsRecibos!cod_ana_efr, "") & "-" & descr_analise), 1, 26))) & "  " & qtd & "    " & taxa_ana & vbCrLf
                total = total + CDbl(BL_HandleNull(rsRecibos!taxa, 0))
                ult_analise = rsRecibos!cod_ana
                If gReciboCrystal = 1 Then
                    ssql = "INSERT INTO sl_cr_recibo(n_req, num_recibo, efr, data, nome, num_benef, " & _
                            "cod_ana_efr, analise, taxa_ana, qtd, nome_computador, cod_empresa, nome_empresa, num_contribuinte," & _
                            "cabecalho,num_sessao, flg_doc_caixa, n_contrib_ute, morada, cod_postal, descr_postal,atcud) VALUES (" & _
                            n_req & "," & num_recibo & "," & BL_TrataStringParaBD(efr) & "," & _
                            BL_TrataStringParaBD(data) & "," & BL_TrataStringParaBD(nome) & "," & _
                            BL_TrataStringParaBD(num_benef) & "," & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_ana_efr, "")) & "," & _
                            BL_TrataStringParaBD(rsRecibos!descr_ana) & "," & BL_TrataStringParaBD(taxa_ana) & "," & _
                            BL_TrataStringParaBD(BL_HandleNull(rsRecibos!qtd, 1)) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & _
                            BL_TrataStringParaBD(cod_empresa) & "," & BL_TrataStringParaBD(nome_empresa) & "," & BL_TrataStringParaBD(BL_HandleNull(num_contribuinte, "")) & "," & _
                            BL_TrataStringParaBD(Cabecalho) & "," & gNumeroSessao & "," & flgDocCaixa & ", " & n_contrib_ute & "," & _
                            BL_TrataStringParaBD(morada) & ", " & BL_TrataStringParaBD(cod_postal) & ", " & BL_TrataStringParaBD(descr_postal) & "," & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!ATCUD, "")) & ")"
                            
                    BG_ExecutaQuery_ADO ssql
                    If gSQLError <> 0 Then
                        BG_Mensagem mediMsgBox, "Erro a inserir an�lise no recibo!", vbExclamation + vbOKOnly, "Recibos"
                        Exit Sub
                    End If
                End If
'            End If
            rsRecibos.MoveNext
        Wend
        'soliveira lacto em situacoes que ha desconto s� apresenta o valor do total a pagar
        If Desconto > 0 Or efr = "MULTICARE" Or efr = "MEDIS" Then
            total = totalPagar
        ElseIf total <> totalPagar Then
            BG_LogFile_Erros "Valor no recibo: " & total & " <> do valor calculado " & totalPagar & " na Requisicao: " & n_req
            total = totalPagar
        End If
        If InStr(1, total, ",") = 0 Then
            Tot = total & ",0"
        ElseIf Len(Mid(total, InStr(1, total, ",") + 1)) = 1 Then
            Tot = total & "0"
        Else
            Tot = total
        End If

               
        If gReciboCrystal = "1" Then
            For i = 0 To num_copias_rec
                If i > 0 And descricao = "" Then
                    descricao = "C�PIA"
                End If
                'Imprime recibo por crystal
                If BL_PreviewAberto("Recibo") = True Then Exit Sub
                continua = BL_IniciaReport("ReciboReq", "Recibo", crptToPrinter, False)
                If continua = False Then Exit Sub
    
                'Imprime o relat�rio no Crystal Reports
                Dim Report As CrystalReport
                Set Report = forms(0).Controls("Report")
                'F�rmulas do Report
                Report.formulas(0) = "TotalPagar=" & BL_TrataStringParaBD("" & CStr(Tot))
                Report.formulas(1) = "TotalPagarExtenso=" & BL_TrataStringParaBD("" & LCase(BL_Preco_EURO_Extenso(CDbl(Tot))))
                Report.formulas(2) = "TipoRec=" & BL_TrataStringParaBD("" & serieRec)
                Report.formulas(3) = "Descricao=" & BL_TrataStringParaBD("" & descricao)
                Report.formulas(4) = "TotalOriginal=" & BL_TrataStringParaBD("" & total_original)
                Report.formulas(5) = "Desconto=" & BL_TrataStringParaBD("" & Desconto)
                Report.formulas(6) = "Formacao=" & BL_TrataStringParaBD("" & Formacao)
                Report.formulas(7) = "Certificado=" & BL_TrataStringParaBD("" & Certificado)
                
                Report.SQLQuery = " SELECT sl_cr_recibo.n_req, sl_cr_recibo.num_recibo, sl_cr_recibo.efr, sl_cr_recibo.data, sl_cr_recibo.nome, sl_cr_recibo.num_benef,"
                Report.SQLQuery = Report.SQLQuery & " sl_cr_recibo.cod_ana_efr , sl_cr_recibo.analise, sl_cr_recibo.taxa_ana, sl_cr_recibo.qtd, "
                Report.SQLQuery = Report.SQLQuery & " sl_cr_recibo.nome_computador, sl_cr_recibo.cod_empresa, sl_cr_recibo.nome_empresa, sl_cr_recibo.num_contribuinte,"
                Report.SQLQuery = Report.SQLQuery & " sl_cr_recibo.cabecalho,sl_cr_recibo.num_sessao,sl_cr_recibo.flg_doc_caixa,sl_cr_recibo.n_contrib_ute "
                Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_recibo WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
                Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
                Report.Connect = "DSN=" & gDSN
                
                Call BL_ExecutaReport
                
                
            Next

        Else
            For i = 0 To num_copias_rec
                If i > 0 And descricao = "" Then
                    descricao = "C�PIA"
                End If
                'Imprime recibo por ficheiro
                If flgDocCaixa = 1 Then
                    If Not BL_LerEtiqIni("DOCCAIXA.INI") Then
                        MsgBox "Ficheiro de inicializa��o de documentos de caixa ausente!"
                        Exit Sub
                    End If
                Else
                    If Not BL_LerEtiqIni("RECIBOS" & "_" & codEfr & ".INI") Then
                        If Not BL_LerEtiqIni("RECIBOS.ini") Then
                            MsgBox "Ficheiro de inicializa��o de recibos ausente!"
                            Exit Sub
                        End If
                    End If
                End If
                If Not BL_EtiqOpenPrinter(impressora) Then
                    MsgBox "Imposs�vel abrir impressora recibos"
                    Exit Sub
                End If
                
                Call RECIBO_EtiqPrint(serieRec, CStr(num_recibo), data, n_req, efr, nome, num_benef, analises, CStr(Tot), "", _
                                      user_emi, descricao, DataChega, vd, cod_empresa, nome_empresa, num_contribuinte, _
                                      total_original, Desconto, Cabecalho, Formacao, Certificado, "", "", n_contrib_ute, morada, _
                                      cod_postal, descr_postal)
        
                BL_EtiqClosePrinter
            Next
        End If
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
        
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "FACTUS_ImprimeRecibo: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_ImprimeRecibo"
    Exit Sub
    Resume Next
End Sub





Private Function FACTUS_RetornaMaxNOrd(iMoviResp As Integer) As Integer
    Dim ssql As String
    Dim rsCert As New adodb.recordset
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    FACTUS_RetornaMaxNOrd = 0
    ssql = "SELECT n_ord FROM fa_movi_resp WHERE t_episodio = " & BL_TrataStringParaBD(fa_movi_resp(iMoviResp).t_episodio)
    ssql = ssql & " AND  episodio = " & BL_TrataStringParaBD(fa_movi_resp(iMoviResp).episodio) & " ORDER by n_ord desc "
    rsCert.CursorLocation = adUseServer
    rsCert.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsCert.Open ssql, gConexao
    If rsCert.RecordCount > 0 Then
        FACTUS_RetornaMaxNOrd = BL_HandleNull(rsCert!n_ord, "")
    End If
    rsCert.Close
    Set rsCert = Nothing
    For i = 1 To fa_movi_resp_tot
        If fa_movi_resp(i).n_ord > FACTUS_RetornaMaxNOrd Then
            FACTUS_RetornaMaxNOrd = fa_movi_resp(i).n_ord
        End If
    Next i
    FACTUS_RetornaMaxNOrd = FACTUS_RetornaMaxNOrd + 1
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_RetornaMaxNOrd: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_RetornaMaxNOrd"
    FACTUS_RetornaMaxNOrd = mediComboValorNull
    Exit Function
    Resume Next
End Function


Public Function FACTUS_AlteraOrdemAnalises(iMoviResp As Integer, iAna1 As Integer, iAna2 As Integer) As Integer
    On Error GoTo TrataErro
    Dim analise1 As movi_fact
    Dim analise2 As movi_fact
    FACTUS_AlteraOrdemAnalises = mediNao
    analise1 = fa_movi_resp(iMoviResp).analises(iAna1)
    analise2 = fa_movi_resp(iMoviResp).analises(iAna2)

    fa_movi_resp(iMoviResp).analises(iAna1) = analise2
    fa_movi_resp(iMoviResp).analises(iAna2) = analise1
    
    fa_movi_resp(iMoviResp).analises(iAna1).n_ord_ins = analise1.n_ord_ins
    fa_movi_resp(iMoviResp).analises(iAna2).n_ord_ins = analise2.n_ord_ins
    FACTUS_AlteraOrdemAnalises = mediSim
    
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_AlteraOrdemAnalises: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AlteraOrdemAnalises"
    FACTUS_AlteraOrdemAnalises = mediComboValorNull
    Exit Function
    Resume Next
End Function

Public Function FACTUS_AlteraPercentagemUtente(iResp As Integer, desconto_utente As Double) As Boolean
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    Dim flgPercentagem As Boolean
    Dim flg_usa_preco_ent As Boolean
    Dim iAna As Integer
    Dim flg_perc_facturar As Boolean
    FACTUS_AlteraPercentagemUtente = False
    
    ssql = "SELECT * FROM sl_efr WHERE cod_efr = " & fa_movi_resp(iResp).cod_efr
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsEFR.Open ssql, gConexao
    If RsEFR.RecordCount > 0 Then
        
        If BL_HandleNull(RsEFR!flg_usa_preco_ent, 0) = mediSim Then
            flg_usa_preco_ent = True
        Else
            flg_usa_preco_ent = False
        End If
        If BL_HandleNull(RsEFR!flg_perc_facturar, 0) = mediSim Then
            flg_perc_facturar = True
        Else
            flg_perc_facturar = False
        End If
    End If
    RsEFR.Close
    Set RsEFR = Nothing
    

    For iAna = 1 To fa_movi_resp(iResp).totalAna
        If fa_movi_resp(iResp).analises(iAna).flg_estado_doe <> 3 Then
            If FACTUS_PreencheValoresanaDet(iResp, iAna, fa_movi_resp(iResp).analises(iAna).cod_rubr, desconto_utente, _
                                            CInt(fa_movi_resp(iResp).analises(iAna).qtd), fa_movi_resp(iResp).analises(iAna).dt_fim_real, _
                                            flg_usa_preco_ent, flg_perc_facturar) > 0 Then
                FACTUS_AlteraPercentagemUtente = True
            End If
        End If
    Next iAna
    FACTUS_AlteraPercentagemUtente = True
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_AlteraPercentagemUtente: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AlteraPercentagemUtente"
    FACTUS_AlteraPercentagemUtente = False
    Exit Function
    Resume Next
End Function

Public Function FACTUS_CalculaQuantidade(iResp As Integer, iAna As Integer, flgPercentagem As Boolean, desconto_utente As Double, Flg_UsaPrecoEntidade As Boolean, sub_prec As String, perc_desc As Double) As Integer
    On Error GoTo TrataErro
    FACTUS_CalculaQuantidade = mediNao

    'If fa_movi_resp(iResp).analises(iAna).val_pr_u_doe > 0 And fa_movi_resp(iResp).analises(iAna).qtd > 0 Then
    If fa_movi_resp(iResp).analises(iAna).val_Taxa > 0 Then
        fa_movi_resp(iResp).analises(iAna).val_Taxa = fa_movi_resp(iResp).analises(iAna).val_Taxa
    End If
    fa_movi_resp(iResp).analises(iAna).val_pag_doe = fa_movi_resp(iResp).analises(iAna).qtd * fa_movi_resp(iResp).analises(iAna).val_pr_u_doe
    If sub_prec <> "" Then
        fa_movi_resp(iResp).analises(iAna).val_total = BG_newRound(fa_movi_resp(iResp).analises(iAna).val_pr_unit * ((100 - perc_desc) / 100) * fa_movi_resp(iResp).analises(iAna).qtd, 2)
    Else
        fa_movi_resp(iResp).analises(iAna).val_total = BG_newRound(fa_movi_resp(iResp).analises(iAna).val_pr_unit * fa_movi_resp(iResp).analises(iAna).qtd, 2)
    End If
    
    If flgPercentagem = True Then
        If fa_movi_resp(iResp).analises(iAna).cod_isen_doe = gTipoIsencaoIsento Then
            fa_movi_resp(iResp).analises(iAna).perc_pag_efr = 100
            fa_movi_resp(iResp).analises(iAna).perc_pag_doe = 0
            fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = 0
            fa_movi_resp(iResp).analises(iAna).val_pag_doe = 0
        Else
            fa_movi_resp(iResp).analises(iAna).perc_pag_efr = 100 - desconto_utente
            fa_movi_resp(iResp).analises(iAna).perc_pag_doe = desconto_utente

        End If
    Else
        If fa_movi_resp(iResp).analises(iAna).cod_isen_doe = gTipoIsencaoIsento Then
            fa_movi_resp(iResp).analises(iAna).perc_pag_efr = 100
            fa_movi_resp(iResp).analises(iAna).perc_pag_doe = 0
            fa_movi_resp(iResp).analises(iAna).val_pr_u_doe = 0
            fa_movi_resp(iResp).analises(iAna).val_pag_doe = 0
            fa_movi_resp(iResp).analises(iAna).val_Taxa = 0
            fa_movi_resp(iResp).analises(iAna).val_pr_unit = fa_movi_resp(iResp).analises(iAna).val_pr_unit
            If sub_prec <> "" Then
                fa_movi_resp(iResp).analises(iAna).val_total = BG_newRound(fa_movi_resp(iResp).analises(iAna).val_pr_unit * ((100 - perc_desc) / 100) * fa_movi_resp(iResp).analises(iAna).qtd, 2)
            Else
                fa_movi_resp(iResp).analises(iAna).val_total = BG_newRound(fa_movi_resp(iResp).analises(iAna).val_pr_unit * fa_movi_resp(iResp).analises(iAna).qtd, 2)
            End If
        End If
    End If
    
    FACTUS_CalculaQuantidade = mediSim
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_CalculaQuantidade: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_CalculaQuantidade"
    FACTUS_CalculaQuantidade = mediComboValorNull
    Exit Function
    Resume Next
End Function




' --------------------------------------------------------------------------------

' PERMITE MUDAR TIPO DE ISENCAO DE UMA ANALISE ATRAVES DAS TECLAS F8, F9, F10

' --------------------------------------------------------------------------------
Public Function FACTUS_IsencaoEntidade(ByVal iResp As Integer, ByVal iAna As Integer) As Boolean
On Error GoTo TrataErro
    Dim taxa As String
    Dim codAnaEfr As String
    Dim mens As String
    Dim ssql As String
    Dim flg_FdsValFixo As Integer
    Dim isencaoAnterior As Integer
    Dim taxaEfr As String
    Dim valorUnitEFR As Double
    Dim descrAnaEfr As String
    Dim j As Integer
    
    'LJMANSO-306
    'If fa_movi_resp(iResp).analises(iAna).flg_estado <> 1 Or (fa_movi_resp(iResp).analises(iAna).flg_estado_doe > 1 And fa_movi_resp(iResp).analises(iAna).flg_estado_doe <> 8) Or (fa_movi_resp(iResp).analises(iAna).flg_estado_tx > 1 And fa_movi_resp(iResp).analises(iAna).flg_estado_tx <> 8) Then
    If fa_movi_resp(iResp).analises(iAna).flg_estado = 3 And fa_movi_resp(iResp).analises(iAna).flg_estado_doe = 3 And fa_movi_resp(iResp).analises(iAna).flg_estado_tx = 3 Then
        BG_Mensagem mediMsgBox, "An�lise n�o pode ser isentada.", vbOKOnly + vbInformation, "Isen��o Entidade"
        FACTUS_IsencaoEntidade = False
        Exit Function
    End If
    
    fa_movi_resp(iResp).analises(iAna).flg_estado = 2
    'LJMANSO-306 Adicionado flg_estado_doe = 8 e flg_estado_tx = 8
    If fa_movi_resp(iResp).analises(iAna).flg_estado_doe = mediComboValorNull Or fa_movi_resp(iResp).analises(iAna).flg_estado_doe = 1 Or fa_movi_resp(iResp).analises(iAna).flg_estado_doe = 8 Then
        fa_movi_resp(iResp).analises(iAna).flg_estado_doe = 2
    End If
    If fa_movi_resp(iResp).analises(iAna).flg_estado_tx = mediComboValorNull Or fa_movi_resp(iResp).analises(iAna).flg_estado_tx = 1 Or fa_movi_resp(iResp).analises(iAna).flg_estado_tx = 8 Then
        fa_movi_resp(iResp).analises(iAna).flg_estado_tx = 2
    End If
    FACTUS_IsencaoEntidade = True
Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_IsencaoEntidade: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_IsencaoEntidade"
    Exit Function
    Resume Next
End Function






Public Sub FACTUS_VerificaUtentePagaActoMedico()
    On Error GoTo TrataErro
    Dim RsAM As adodb.recordset
    Dim sql As String
    Dim iMoviFact As Integer
    Dim iMoviResp As Integer
    Dim Flg_ExisteActoMed As Boolean
    
    Set RsAM = New adodb.recordset
    For iMoviResp = 1 To fa_movi_resp_tot
        RsAM.CursorLocation = adUseServer
        RsAM.CursorType = adOpenStatic
        sql = "select flg_acto_med_ute from sl_efr where cod_efr = " & Trim(fa_movi_resp(iMoviResp).cod_efr)
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsAM.Open sql, gConexao
        If RsAM.RecordCount > 0 Then
            If BL_HandleNull(RsAM!flg_acto_med_ute, 0) = 1 Then
                For iMoviFact = 1 To fa_movi_resp(iMoviResp).totalAna
                    If fa_movi_resp(iMoviResp).analises(iMoviFact).cod_rubr = FACTUS_RetornaRubrFactus(fa_movi_resp(iMoviResp).cod_efr, GCodAnaActMed) Then
                        Flg_ExisteActoMed = True
                        Exit For
                    End If
                Next iMoviFact
                
                If Flg_ExisteActoMed = False Then
                    If FACTUS_AdicionaAnalise(fa_movi_resp(iMoviResp).cod_efr, GCodAnaActMed, Bg_DaData_ADO, 0, mediComboValorNull, fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).cod_isen_doe, _
                                mediComboValorNull, fa_movi_resp(iMoviResp).n_benef_doe, fa_movi_resp(iMoviResp).t_doente, fa_movi_resp(iMoviResp).doente, _
                                fa_movi_resp(iMoviResp).episodio, fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).nr_req_ars, fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).convencao, _
                                fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).cod_medico, fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).cod_dom, _
                                fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).km_dom, fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).cod_post_dom, _
                                fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).localidade_dom, fa_movi_resp(iMoviResp).analises(fa_movi_resp(iMoviResp).totalAna).cod_u_saude, _
                                mediComboValorNull) = True Then
                                
                    End If
                End If
            End If
        End If
        RsAM.Close
    Next iMoviResp
 Exit Sub
TrataErro:
    BG_LogFile_Erros "FACTUS_VerificaUtentePagaActoMedico: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_VerificaUtentePagaActoMedico"
    Exit Sub
    Resume Next
End Sub

Public Sub FACTUS_AtualizaDataIniReal()
    On Error GoTo TrataErro
    Dim RsAM As adodb.recordset
    Dim sql As String
    Dim iMoviFact As Integer
    Dim iMoviResp As Integer
    Dim Flg_ExisteActoMed As Boolean
    Set RsAM = New adodb.recordset
    For iMoviResp = 1 To fa_movi_resp_tot
        sql = "SELECT flg_obriga_recibo FROM sl_efr WHERE cod_efr = " & fa_movi_resp(iMoviResp).cod_efr
        RsAM.CursorLocation = adUseServer
        RsAM.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsAM.Open sql, gConexao
        If RsAM.RecordCount = 1 Then
            If BL_HandleNull(RsAM!flg_obriga_recibo, 0) = 1 Then
                For iMoviFact = 1 To fa_movi_resp(iMoviResp).totalAna
                    If fa_movi_resp(iMoviResp).analises(iMoviFact).n_fac = mediComboValorNull And fa_movi_resp(iMoviResp).analises(iMoviFact).n_Fac_doe = mediComboValorNull And fa_movi_resp(iMoviResp).analises(iMoviFact).n_fac_tx = mediComboValorNull Then
                        fa_movi_resp(iMoviResp).analises(iMoviFact).dt_ini_real = Bg_DaData_ADO
                        fa_movi_resp(iMoviResp).analises(iMoviFact).dt_fim_real = fa_movi_resp(iMoviResp).analises(iMoviFact).dt_ini_real
                    End If
                Next iMoviFact
            End If
        End If
        RsAM.Close
    Next iMoviResp
    Set RsAM = Nothing
 Exit Sub
TrataErro:
    BG_LogFile_Erros "FACTUS_AtualizaDataIniReal: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AtualizaDataIniReal"
    Exit Sub
    Resume Next
End Sub


Private Function FACTUS_RetornaRubrFactus(cod_efr As Long, cod_ana As String) As String
    Dim ssql As String
    Dim rsAnaFact As New adodb.recordset
    On Error GoTo TrataErro
    'VERIFICA MAPEAMENTO PARA FACTUS
    ssql = "SELECT cod_ana_gh,flg_conta_membros FROM sl_ana_facturacao WHERE cod_efr = " & cod_efr & " AND  cod_ana = " & BL_TrataStringParaBD(cod_ana)
    ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(Bg_DaData_ADO, "")) & " BETWEEN "
    ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAnaFact.Open ssql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        ssql = "SELECT cod_ana_gh, flg_conta_membros,qtd FROM sl_ana_facturacao WHERE cod_efr IS NULL AND cod_ana = " & BL_TrataStringParaBD(cod_ana)
        ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(Bg_DaData_ADO, "")) & " BETWEEN "
        ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsAnaFact.Open ssql, gConexao
        If rsAnaFact.RecordCount <= 0 Then
            ' ANALISE NAO ESTA MAPEADA PARA FACTUS
            FACTUS_RetornaRubrFactus = "-1"
            rsAnaFact.Close
            Set rsAnaFact = Nothing
            Exit Function
        Else
            FACTUS_RetornaRubrFactus = BL_HandleNull(rsAnaFact!cod_ana_gh, mediComboValorNull)
            
        End If
    Else
        FACTUS_RetornaRubrFactus = BL_HandleNull(rsAnaFact!cod_ana_gh, mediComboValorNull)
        
    End If
 Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_RetornaRubrFactus: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_RetornaRubrFactus"
    Exit Function
    Resume Next
End Function


Public Function FACTUS_AtualizaDom(cod_dom As Integer, km_dom As Integer) As Boolean
    On Error GoTo TrataErro
    Dim iResp As Integer
    
    For iResp = 1 To fa_movi_resp_tot
        fa_movi_resp(iResp).cod_dom = cod_dom
        If cod_dom = 1 Or cod_dom = 2 Then
            fa_movi_resp(iResp).qtd_dom = 1
        Else
            fa_movi_resp(iResp).qtd_dom = km_dom
        End If
    Next iResp
 Exit Function
TrataErro:
    FACTUS_AtualizaDom = False
    BG_LogFile_Erros "FACTUS_AtualizaDom: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AtualizaDom"
    Exit Function
    Resume Next
End Function

Public Function FACTUS_AtualizaOrdens(iResp As Integer) As Boolean
    Dim maxOrd As Integer
    Dim iAna As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    For iAna = 1 To fa_movi_resp(iResp).totalAna
        If fa_movi_resp(iResp).analises(iAna).flg_estado <> 8 And fa_movi_resp(iResp).analises(iAna).flg_estado <> 2 Then
            FACTUS_AtualizaOrdensDet iResp, iAna
        End If
    Next iAna
 Exit Function
TrataErro:
    FACTUS_AtualizaOrdens = False
    BG_LogFile_Erros "FACTUS_AtualizaOrdens: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AtualizaOrdens"
    Exit Function
    Resume Next
End Function


Private Function FACTUS_AtualizaOrdensDet(iResp As Integer, iAna As Integer) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    
    For i = 1 To iAna - 1
        If fa_movi_resp(iResp).analises(i).nr_req_ars = fa_movi_resp(iResp).analises(iAna).nr_req_ars And i <> iAna Then
            If fa_movi_resp(iResp).analises(i).flg_estado <> 8 And fa_movi_resp(iResp).analises(i).flg_estado <> 2 Then
                
                If fa_movi_resp(iResp).analises(iAna).n_ord_ins = fa_movi_resp(iResp).analises(i).n_ord_ins Then
                    fa_movi_resp(iResp).analises(iAna).n_ord_ins = fa_movi_resp(iResp).analises(iAna).n_ord_ins + 1
                    FACTUS_AtualizaOrdensDet iResp, iAna
                    Exit For
                End If
            End If
        End If
    Next i
 Exit Function
TrataErro:
    FACTUS_AtualizaOrdensDet = False
    BG_LogFile_Erros "FACTUS_AtualizaOrdensDet: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_AtualizaOrdensDet"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' DEVOLVE UMA STRING CONCATENADA COM OS ESTADOS DOS DIVERSOS RECIBOS

' ------------------------------------------------------------------------------------------------
Public Function FACTUS_DevolveEstadoRecibos(n_req As String)
    Dim ssql As String
    Dim n_ord As Integer
    Dim rsDescr As New adodb.recordset
    Dim SerieDoc() As String
    Dim nDoc() As String
    Dim contaSemPagamento As Integer
    Dim pos As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    ReDim SerieDoc(0)
    ReDim nDoc(0)
    
    FACTUS_DevolveEstadoRecibos = ""
    ssql = "SELECT cod_isencao FROM sl_requis WHERE n_req = " & n_req
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsDescr.Open ssql, gConexao
    FACTUS_DevolveEstadoRecibos = ""
    If rsDescr.RecordCount > 0 Then
        If BL_HandleNull(rsDescr!cod_isencao, "0") <> gTipoIsencaoBorla Then
            rsDescr.Close
            ssql = "SELECT * FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(n_req) & " ORDER BY n_ord "
            rsDescr.CursorLocation = adUseServer
            rsDescr.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsDescr.Open ssql, gConexao
            FACTUS_DevolveEstadoRecibos = ""
            If rsDescr.RecordCount > 0 Then
                n_ord = 0
                While Not rsDescr.EOF
                    'LJMANSO-311
                    'Guarda todos os serie_fac e n_fac para validar na sl_dados_doc caso seja necess�rio
                    pos = pos + 1
                    ReDim Preserve SerieDoc(pos)
                    ReDim Preserve nDoc(pos)
                    SerieDoc(pos) = BL_HandleNull(rsDescr.Fields("serie_fac_doe").value, BL_HandleNull(rsDescr.Fields("serie_fac_tx").value, ""))
                    nDoc(pos) = BL_HandleNull(rsDescr.Fields("n_fac_doe").value, BL_HandleNull(rsDescr.Fields("n_fac_tx").value, ""))
                    '
                    If BL_HandleNull(rsDescr!flg_estado_doe, 1) = 1 And BL_HandleNull(rsDescr!val_pag_doe, 0) > 0 Then
                        FACTUS_DevolveEstadoRecibos = "N"
                    ElseIf BL_HandleNull(rsDescr!flg_estado_tx, 1) = 1 And BL_HandleNull(rsDescr!val_Taxa, 0) > 0 Then
                        FACTUS_DevolveEstadoRecibos = "N"
                    End If
                    rsDescr.MoveNext
                Wend
            End If
            rsDescr.Close
            
            'LJMANSO-311
            'Adicionada condi��o para validar na sl_dados_doc se n�o verificar na fa_movi_fact
            If FACTUS_DevolveEstadoRecibos = "" Then
                For i = 1 To UBound(SerieDoc)
                    If contaSemPagamento > 0 Then Exit For
                    If SerieDoc(i) <> "" And nDoc(i) <> "" Then
                        ssql = "SELECT * FROM sl_dados_doc WHERE serie_doc = " & BL_TrataStringParaBD(SerieDoc(i)) & " AND n_doc = " & BL_TrataStringParaBD(nDoc(i))
                        rsDescr.Open ssql, gConexao
                        If rsDescr.RecordCount > 0 Then
                            While Not rsDescr.EOF And contaSemPagamento = 0
                                If BL_HandleNull(rsDescr("DT_PAGAMENTO").value, "") = "" Then
                                    contaSemPagamento = contaSemPagamento + 1
                                    FACTUS_DevolveEstadoRecibos = "N"
                                End If
                                rsDescr.MoveNext
                            Wend
                        End If
                        rsDescr.Close
                    End If
                Next i
            End If
            
            If FACTUS_DevolveEstadoRecibos = "" Then
                FACTUS_DevolveEstadoRecibos = "P"
            End If
        Else
            FACTUS_DevolveEstadoRecibos = "B"
        End If
    End If
    Set rsDescr = Nothing
 Exit Function
TrataErro:
    FACTUS_DevolveEstadoRecibos = ""
    BG_LogFile_Erros "FACTUS_DevolveEstadoRecibos: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_DevolveEstadoRecibos"
    Exit Function
    Resume Next
End Function

'BRUNODSANTOS LJMANSO-301 06.08.2018
Public Sub FACTUS_OrdenaCredenciais(n_req As String)
    
    Dim cmd As New adodb.Command
    Dim Pmt As adodb.Parameter
    Dim PmtRes As adodb.Parameter
    Dim Result As Integer

    On Error GoTo TrataErro

    With cmd
        .ActiveConnection = gConexao
        .CommandText = "slp_ordena_credenciais"
        .CommandType = adodb.CommandTypeEnum.adCmdStoredProc
    End With
    
    Set Pmt = cmd.CreateParameter("p_n_req", adodb.DataTypeEnum.adVarChar, adodb.ParameterDirectionEnum.adParamInput, Len(n_req))
    cmd.Parameters.Append Pmt
    cmd.Parameters("p_n_req").value = n_req

    cmd.Execute
    
    Set cmd = Nothing
    Set Pmt = Nothing
    
    Exit Sub

TrataErro:
        BG_LogFile_Erros "FACTUS_OrdenaCredenciais ERRO : " & Err.Description
        Exit Sub
        Resume Next

End Sub

'BRUNODSANTOS LJMANSO-307  09.08.2018
Private Function FACTUS_DevolveCodUrbanoFromRequis(ByVal NReq As String) As Integer

    Dim rs As New adodb.recordset
    Dim ssql As String
    
    On Error GoTo TrataErro
    
    FACTUS_DevolveCodUrbanoFromRequis = 0
    
    ssql = "SELECT cod_urbano FROM sl_requis WHERE n_req = " & BL_TrataStringParaBD(NReq)
    rs.CursorLocation = adUseServer
    rs.Open ssql, gConexao, adOpenStatic
    
    If rs.RecordCount > 0 Then
        FACTUS_DevolveCodUrbanoFromRequis = BL_HandleNull(rs.Fields("cod_urbano").value, 0)
    End If
    
    Exit Function
    
    Set rs = Nothing
TrataErro:
        BG_LogFile_Erros "FACTUS_DevolveCodUrbanoFromRequis ERRO : " & Err.Description
        Exit Function
        Resume Next
        
End Function

'UALIA -867 Devolve valor de K e de C
'UALIA-896 Adicionado numero de Cs e Ks para n�o devolver valor caso sejam = 0
Private Function FACTUS_PreencheValoresKC(ByVal Portaria As String, ByRef valor_c As Double, ByRef valor_k As Double, ByVal nr_c As Double, ByVal nr_k As Double) As String
    Dim ssql As String
    
    Dim rsValorC As New adodb.recordset
    On Error GoTo TrataErro
    
    ssql = "SELECT valor_c, valor_k  FROM fa_pr_grup_rubr where portaria = " & Portaria & " AND cod_grupo in (" & gGrupoRubricaFactus & ")"
    rsValorC.CursorLocation = adUseServer
    rsValorC.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsValorC.Open ssql, gConexaoSecundaria
    If rsValorC.RecordCount > 0 Then
        valor_c = IIf(nr_c > 0, rsValorC!valor_c, Null)
        valor_k = IIf(nr_k > 0, rsValorC!valor_k, Null)
    End If
    rsValorC.Close
    Set rsValorC = Nothing
    
Exit Function
TrataErro:
        BG_LogFile_Erros "FACTUS_PreencheValoresKC ERRO : " & Err.Description
        Exit Function
        Resume Next
End Function

'UALIA-889 - Devolve rubrica com base no cod_ana
Private Function FACTUS_DevolveRubrFactus(cod_ana As String) As String
    Dim ssql As String
    Dim rsAnaFact As New adodb.recordset
    On Error GoTo TrataErro

    ssql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana) & " AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAnaFact.Open ssql, gConexao
    If rsAnaFact.RecordCount > 0 Then
            FACTUS_DevolveRubrFactus = BL_HandleNull(rsAnaFact!cod_ana_gh, mediComboValorNull)
    End If
    rsAnaFact.Close
    Set rsAnaFact = Nothing
            
 Exit Function
TrataErro:
    BG_LogFile_Erros "FACTUS_DevolveRubrFactus: " & Err.Number & " - " & Err.Description, "FACTUS", "FACTUS_DevolveRubrFactus"
    Exit Function
    Resume Next
End Function

Private Function IF_RetornaConvLote97(cod_convencao As String) As String
On Error GoTo TrataErro
 Dim ssql As String
 Dim rsConv As New adodb.recordset
If cod_convencao = lCod97 Then
   ssql = "select descr_convencao from sl_tbf_convencao where cod_convencao= " & BL_TrataStringParaBD(cod_convencao)
   rsConv.CursorLocation = adUseServer
   rsConv.CursorType = adOpenStatic
   
   If gModoDebug = mediSim Then BG_LogFile_Erros ssql
   
   rsConv.Open ssql, gConexao
   If rsConv.RecordCount >= 0 Then
      IF_RetornaConvLote97 = BL_HandleNull(rsConv!descr_convencao, cod_convencao)
      
      rsConv.Close
      Set rsConv = Nothing
      Exit Function
   End If
   
   rsConv.Close
   Set rsConv = Nothing
End If
IF_RetornaConvLote97 = cod_convencao
Exit Function
TrataErro:
    IF_RetornaConvLote97 = cod_convencao
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaConvLote97"
    Exit Function
    Resume Next
End Function

'GX-62545
Private Sub ManageBillingQRCode(ByVal strQrCode As String, ByVal n_req As String)

    Dim byteArray() As Byte
    Dim fnum As Integer
    Dim continua As Boolean
    Dim path As String
    path = gDirCliente & "\bin\" & "qr_code.png"
    
    If strQrCode <> "" Then
        Dim b64str As String
        Dim qrMsgOut As String
        b64str = BillQRCode_GetB64QRcodeString(strQrCode, qrMsgOut)
        
        If b64str <> "-1" Then
         byteArray = BillingQRcode.DecodeBase64(b64str)
         fnum = FreeFile
         Open path For Binary As #fnum
         Put #fnum, 1, byteArray
         Close fnum
         
        Call BillingQRcode.BillQRCode_insertQrBd(path, n_req)
  
            Exit Sub
        Else
            BG_Mensagem mediMsgBox, "Erro ao obter QR CODE: " & qrMsgOut, vbOKOnly + vbError, "FACTUS_ImprimeRecibo"
            BG_LogFile_Erros "FACTUS_ImprimeRecibo (ManageBillingQRCode) " & qrMsgOut, "FACTUS", "FACTUS_ImprimeRecibo"
        End If
    End If
End Sub

Public Sub IF_GravaSdReqArs(inum_requis As String)
On Error GoTo TrataErro
 Dim ssql As String
 Dim rs As New adodb.recordset
'If cod_convencao = lCod97 Then
   ssql = " SELECT distinct a.n_req episodio, a.cod_urbano, a.km, a.cod_postal, a.convencao, b.utente, b.t_utente, d.n_req credencial, c.descr_postal" & _
          " FROM sl_requis a, sl_identif b, sl_cod_postal c, sl_credenciais d WHERE A.seq_utente = b.seq_utente AND c.cod_postal = a.cod_postal" & _
          " AND d.n_req_orig = a.n_req and a.n_req= '" & inum_requis & "'"
   rs.CursorLocation = adUseServer
   rs.CursorType = adOpenStatic
   
   If gModoDebug = mediSim Then BG_LogFile_Erros ssql
   
   rs.Open ssql, gConexao
   If rs.RecordCount > 0 Then
       FACTUS_InsereReqARS rs!episodio, BL_HandleNull(rs!credencial, ""), "A", BL_HandleNull(rs!cod_urbano, ""), _
                           BL_HandleNull(rs!km, ""), BL_HandleNull(rs!cod_postal, ""), BL_HandleNull(rs!descr_postal, ""), "", "", _
                           BL_HandleNull(rs!t_utente, ""), BL_HandleNull(rs!Utente, ""), BL_HandleNull(rs!convencao, -1), "97"
   End If
   
   rs.Close
   Set rs = Nothing
'End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_GravaSdReqArs"
    Exit Sub
    Resume Next
End Sub

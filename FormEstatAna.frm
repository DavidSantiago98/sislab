VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormEstatAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstatAna"
   ClientHeight    =   8895
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8820
   Icon            =   "FormEstatAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8895
   ScaleWidth      =   8820
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkApenasSemResultado 
      Caption         =   "Apenas Sem Resultado"
      Height          =   255
      Left            =   2280
      TabIndex        =   50
      Top             =   8400
      Width           =   2055
   End
   Begin VB.ListBox EcListaLocalEntrega 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   46
      Top             =   7080
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaLocalEntrega 
      Height          =   315
      Left            =   7560
      Picture         =   "FormEstatAna.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   45
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   7080
      Width           =   375
   End
   Begin VB.CheckBox CkRepet 
      Caption         =   "Contar Repeti��es"
      Height          =   255
      Left            =   6720
      TabIndex        =   44
      Top             =   8400
      Width           =   2055
   End
   Begin VB.CheckBox CkAgruparGrupo 
      Caption         =   "Agrupar por Grupo An�."
      Height          =   255
      Left            =   120
      TabIndex        =   41
      Top             =   8040
      Width           =   2055
   End
   Begin VB.CheckBox CkAgruparMedico 
      Caption         =   "Agrupar por M�dico"
      Height          =   255
      Left            =   6720
      TabIndex        =   40
      Top             =   7680
      Width           =   2055
   End
   Begin VB.CheckBox CkRestringeRequis 
      Caption         =   "Requisi��es Com Apenas An�lises Seleccionadas"
      Height          =   375
      Left            =   120
      TabIndex        =   39
      Top             =   8400
      Width           =   2175
   End
   Begin VB.CommandButton BtPesquisaMedicos 
      Height          =   315
      Left            =   7560
      Picture         =   "FormEstatAna.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   37
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   5640
      Width           =   375
   End
   Begin VB.ListBox EcListaMedicos 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   36
      Top             =   5640
      Width           =   5775
   End
   Begin VB.CheckBox CkMembros 
      Caption         =   "Descriminar Membros"
      Height          =   255
      Left            =   2280
      TabIndex        =   35
      Top             =   8040
      Width           =   2055
   End
   Begin VB.CheckBox CkMarcacoes 
      Caption         =   "Incluir Sem Resultado"
      Height          =   255
      Left            =   4560
      TabIndex        =   34
      Top             =   8400
      Width           =   2055
   End
   Begin VB.CheckBox CkAgruparSalasSede 
      Caption         =   "Agrupar Salas do Laborat�rio"
      Height          =   195
      Left            =   1800
      TabIndex        =   33
      Top             =   10320
      Width           =   2415
   End
   Begin VB.CheckBox CkEntidade 
      Caption         =   "Agrupar por Entidade"
      Height          =   255
      Left            =   120
      TabIndex        =   30
      Top             =   7680
      Width           =   2055
   End
   Begin VB.CheckBox CkAgruparSala 
      Caption         =   "Agrupar por Sala/Posto"
      Height          =   255
      Left            =   4560
      TabIndex        =   20
      Top             =   7680
      Width           =   2055
   End
   Begin VB.CheckBox CkAgruparProven 
      Caption         =   "Agrupar por Proveni�ncia"
      Height          =   255
      Left            =   2280
      TabIndex        =   19
      Top             =   7680
      Width           =   2175
   End
   Begin VB.CheckBox CkDescrAna 
      Caption         =   "Descriminar An�lises"
      Height          =   255
      Left            =   6720
      TabIndex        =   18
      Top             =   8040
      Width           =   2055
   End
   Begin VB.CheckBox CkDescrReq 
      Caption         =   "Descriminar Requisi��es"
      Height          =   255
      Left            =   4560
      TabIndex        =   17
      Top             =   8040
      Width           =   2055
   End
   Begin VB.ListBox EcListaEFR 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   16
      Top             =   4920
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaEFR 
      Height          =   315
      Left            =   7560
      Picture         =   "FormEstatAna.frx":0B20
      Style           =   1  'Graphical
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   4920
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaAnalises 
      Height          =   315
      Left            =   7560
      Picture         =   "FormEstatAna.frx":10AA
      Style           =   1  'Graphical
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   6360
      Width           =   375
   End
   Begin VB.ListBox EcListaAnalises 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   13
      Top             =   6360
      Width           =   5775
   End
   Begin VB.ListBox EcListaLocais 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   12
      Top             =   4200
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaLocais 
      Height          =   315
      Left            =   7560
      Picture         =   "FormEstatAna.frx":1634
      Style           =   1  'Graphical
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   4200
      Width           =   375
   End
   Begin VB.ListBox EcListaSala 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   10
      Top             =   3480
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   7560
      Picture         =   "FormEstatAna.frx":1BBE
      Style           =   1  'Graphical
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   3480
      Width           =   375
   End
   Begin VB.ListBox EcListaProven 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   8
      Top             =   2760
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaProveniencia 
      Height          =   315
      Left            =   7560
      Picture         =   "FormEstatAna.frx":2148
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   2760
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaRapidaGrTrab 
      Height          =   375
      Left            =   7560
      Picture         =   "FormEstatAna.frx":26D2
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Pesquisa R�pida de Grupos de Trabalho"
      Top             =   2040
      Width           =   375
   End
   Begin VB.ListBox EcListaGrTrab 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   5
      Top             =   2040
      Width           =   5775
   End
   Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
      Height          =   375
      Left            =   7560
      Picture         =   "FormEstatAna.frx":2C5C
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1320
      Width           =   375
   End
   Begin VB.ListBox EcListaGrAna 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1800
      TabIndex        =   3
      Top             =   1320
      Width           =   5775
   End
   Begin VB.Frame Frame3 
      Height          =   1215
      Left            =   360
      TabIndex        =   0
      Top             =   0
      Width           =   7575
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   49
         Top             =   720
         Width           =   1335
      End
      Begin VB.ComboBox CbSexo 
         Height          =   315
         Left            =   6120
         Style           =   2  'Dropdown List
         TabIndex        =   42
         Top             =   720
         Width           =   1215
      End
      Begin VB.ComboBox cbUrgencia 
         Height          =   315
         Left            =   6120
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   315
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   1440
         TabIndex        =   21
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145686529
         CurrentDate     =   39588
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3120
         TabIndex        =   22
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145686529
         CurrentDate     =   39588
      End
      Begin VB.Label Label4 
         Caption         =   "Situa��o"
         Height          =   255
         Index           =   2
         Left            =   720
         TabIndex        =   48
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Sexo"
         Height          =   255
         Index           =   1
         Left            =   5400
         TabIndex        =   43
         Top             =   765
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Urg�ncia"
         Height          =   255
         Index           =   0
         Left            =   5400
         TabIndex        =   31
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   2
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   1
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Local Entrega"
      Height          =   255
      Index           =   8
      Left            =   360
      TabIndex        =   47
      Top             =   7080
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "M�dicos"
      Height          =   255
      Index           =   7
      Left            =   360
      TabIndex        =   38
      Top             =   5640
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "An�lises"
      Height          =   255
      Index           =   6
      Left            =   360
      TabIndex        =   29
      Top             =   6360
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Entidades Finac."
      Height          =   255
      Index           =   5
      Left            =   360
      TabIndex        =   28
      Top             =   4920
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Locais"
      Height          =   255
      Index           =   4
      Left            =   360
      TabIndex        =   27
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Salas / Postos"
      Height          =   255
      Index           =   3
      Left            =   360
      TabIndex        =   26
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Proveni�ncias"
      Height          =   255
      Index           =   2
      Left            =   360
      TabIndex        =   25
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Grupos Trabalho"
      Height          =   255
      Index           =   1
      Left            =   360
      TabIndex        =   24
      Top             =   2040
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Grupos An�lise"
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   23
      Top             =   1320
      Width           =   1095
   End
End
Attribute VB_Name = "FormEstatAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Private Sub BtPesquisaLocalEntrega_Click()
    PA_PesquisaLocalEntregaMultiSel EcListaLocalEntrega
End Sub

'rcoelho 29.07.2013 glintt-hs-4958
Private Sub CbSexo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then CbSexo.ListIndex = -1
End Sub

'rcoelho 29.07.2013 glintt-hs-4958
Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then CbSituacao.ListIndex = -1
End Sub

'rcoelho 29.07.2013 glintt-hs-4958
Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then cbUrgencia.ListIndex = -1
End Sub

'rcoelho 5.06.2013 chvng-4193
Private Sub CkApenasSemResultado_Click()
    If CkApenasSemResultado.value = vbChecked Then
        CkMarcacoes.value = vbUnchecked
    End If
End Sub

'rcoelho 5.06.2013 chvng-4193
Private Sub CkMarcacoes_Click()
    If CkMarcacoes.value = vbChecked Then
        CkApenasSemResultado.value = vbUnchecked
    End If
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Estat�stica de An�lises"
    
    Me.left = 540
    Me.top = 250
    Me.Width = 8910
    Me.Height = 9300 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de An�lises")
    
    Set FormEstatAna = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    EcListaAnalises.Clear
    EcListaGrAna.Clear
    EcListaGrTrab.Clear
    EcListaProven.Clear
    EcListaSala.Clear
    EcListaLocais.Clear
    EcListaEFR.Clear
    EcListaAnalises.Clear
    EcListaMedicos.Clear
    EcListaLocalEntrega.Clear
    CkAgruparProven.value = vbUnchecked
    CkAgruparMedico.value = vbUnchecked
    CkAgruparGrupo.value = vbUnchecked
    CkAgruparSala.value = vbUnchecked
    CkDescrAna.value = vbUnchecked
    CkDescrReq.value = vbUnchecked
    CkRestringeRequis.value = vbUnchecked
    CkRestringeRequis.Enabled = False
    EcDtFim.value = Bg_DaData_ADO
    EcDtInicio.value = Bg_DaData_ADO
End Sub

Sub DefTipoCampos()
        
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", cbUrgencia
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", CbSexo
    EcDtFim.value = Bg_DaData_ADO
    EcDtInicio.value = Bg_DaData_ADO
    CkRestringeRequis.value = vbUnchecked
    CkRestringeRequis.Enabled = False
    If BL_HandleNull(gCodSalaAssocUser, "-1") <> "-1" And gCodSalaAssocUser <> 0 Then
        EcListaSala.AddItem BL_SelCodigo("SL_COD_SALAS", "DESCR_SALA", "COD_SALA", gCodSalaAssocUser)
        EcListaSala.ItemData(EcListaSala.NewIndex) = gCodSalaAssocUser
        EcListaSala.Enabled = False
        BtPesquisaSala.Enabled = False
    Else
        EcListaSala.Enabled = True
        BtPesquisaSala.Enabled = True
    End If
    'rcoelho 3.06.2013 chvng-4177
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub

Private Sub EclistaGrAna_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaGrAna, KeyCode, Shift
End Sub
Private Sub EclistaGrtrab_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaGrTrab, KeyCode, Shift
End Sub

Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProven, KeyCode, Shift
End Sub

Private Sub EclistaSala_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaSala, KeyCode, Shift
End Sub
Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocais, KeyCode, Shift
End Sub

Private Sub EclistaEFR_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaEFR, KeyCode, Shift
End Sub
Private Sub EcListaLocalEntrega_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocalEntrega, KeyCode, Shift
End Sub
Private Sub EcListaMedicos_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaMedicos, KeyCode, Shift
End Sub

Private Sub Eclistaanalises_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaAnalises.ListCount > 0 Then     'Delete
        If EcListaAnalises.ListIndex > mediComboValorNull Then
            EcListaAnalises.RemoveItem (EcListaAnalises.ListIndex)
            If EcListaAnalises.ListCount > 0 Then
                CkRestringeRequis.Enabled = True
            Else
                CkRestringeRequis.value = vbUnchecked
                CkRestringeRequis.Enabled = False
            End If
        End If
    End If
End Sub

Private Sub BtPesquisaRapidaGrAnalises_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_ana"
    CWhere = ""
    CampoPesquisa = "descr_gr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_ana ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(resultados(i) <> "") Then
                If EcListaGrAna.ListCount = 0 Then
                    EcListaGrAna.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                    EcListaGrAna.ItemData(0) = resultados(i)
                Else
                    EcListaGrAna.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                    EcListaGrAna.ItemData(EcListaGrAna.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If
End Sub
Private Sub BtPesquisaMedicos_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_medicos"
    CWhere = ""
    CampoPesquisa = "nome_med"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_med ", _
                                                                           " M�dicos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(resultados(i) <> "") Then
                If EcListaMedicos.ListCount = 0 Then
                    EcListaMedicos.AddItem BL_SelCodigo("sl_medicos", "nome_med", "seq_med", resultados(i))
                    EcListaMedicos.ItemData(0) = resultados(i)
                Else
                    EcListaMedicos.AddItem BL_SelCodigo("sl_medicos", "nome_med", "seq_med", resultados(i))
                    EcListaMedicos.ItemData(EcListaMedicos.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If
End Sub



Private Sub BtPesquisaRapidaGrTrab_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_trab"
    CamposEcran(1) = "cod_gr_trab"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_trab"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_trab"
    CWhere = ""
    CampoPesquisa = "descr_gr_trab"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_trab ", _
                                                                           " Grupo Trabalho")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If EcListaGrTrab.ListCount = 0 Then
                    EcListaGrTrab.AddItem BL_SelCodigo("sl_gr_trab", "descr_gr_trab", "seq_gr_trab", resultados(i))
                    EcListaGrTrab.ItemData(0) = resultados(i)
                Else
                    EcListaGrTrab.AddItem BL_SelCodigo("sl_gr_trab", "descr_gr_trab", "seq_gr_trab", resultados(i))
                    EcListaGrTrab.ItemData(EcListaGrTrab.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If
End Sub

Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(0) = resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSalaMultiSel EcListaSala

End Sub

Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(0) = resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub BtPesquisaEFR_Click()
    PA_PesquisaEFRMultiSel EcListaEFR
End Sub

Private Sub BtPesquisaAnalises_Click()
    PesquisaAnalises
    If EcListaAnalises.ListCount > 0 Then
        CkRestringeRequis.Enabled = True
    Else
        CkRestringeRequis.value = vbUnchecked
        CkRestringeRequis.Enabled = False
    End If
    
End Sub

Private Sub PesquisaPerfis()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis"
    CWhere = " flg_activo = 1  "
    CampoPesquisa = "descr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaAnalises.ListCount = 0 Then
                EcListaAnalises.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                EcListaAnalises.ItemData(0) = resultados(i)
            Else
                EcListaAnalises.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                EcListaAnalises.ItemData(EcListaAnalises.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub PesquisaComplexas()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_c"
    'CWhere = "  (flg_invisivel is null or flg_invisivel = 0) "
    CampoPesquisa = "descr_ana_c"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana_c ", _
                                                                           " Complexas")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaAnalises.ListCount = 0 Then
                EcListaAnalises.AddItem BL_SelCodigo("sl_ana_c", "descr_ana_c", "seq_ana_c", resultados(i))
                EcListaAnalises.ItemData(0) = resultados(i)
            Else
                EcListaAnalises.AddItem BL_SelCodigo("sl_ana_c", "descr_ana_c", "seq_ana_c", resultados(i))
                EcListaAnalises.ItemData(EcListaAnalises.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub PesquisaAnalises()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "slv_analises"
    'CWhere = "  (flg_invisivel is null or flg_invisivel = 0) "
    CampoPesquisa = "descr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY Descr_ana ", _
                                                                           " Analises")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaAnalises.ListCount = 0 Then
                EcListaAnalises.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                EcListaAnalises.ItemData(0) = resultados(i)
            Else
                EcListaAnalises.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                EcListaAnalises.ItemData(EcListaAnalises.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub



Sub Preenche_Estatistica()
    Dim sSql As String
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim nomeReport As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    If CkAgruparSala = vbChecked Then
        nomeReport = "EstatisticaAnalises_OrderSala"
    ElseIf CkAgruparProven = vbChecked Then
        nomeReport = "EstatisticaAnalises"
    ElseIf CkAgruparMedico = vbChecked Then
        nomeReport = "EstatisticaAnalises_OrderMedico"
    ElseIf CkAgruparGrupo = vbChecked Then
        nomeReport = "EstatisticaAnalises_OrderGrupo"
    ElseIf CkEntidade = vbChecked Then
        nomeReport = "EstatisticaAnalises_OrderEFR"
    ElseIf CkDescrAna.value = vbChecked And CkDescrReq = vbUnchecked And CkAgruparProven.value = vbUnchecked Then
        nomeReport = "EstatisticaAnalises_OrderAna"
    ElseIf CkDescrReq = vbChecked Then
        nomeReport = "EstatisticaAnalises_OrderReq"
    Else
        nomeReport = "EstatisticaAnalises"
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Estat�stica de An�lises", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Estat�stica de An�lises", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    PreencheTabelaTemporaria
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT sl_cr_analises.nome_computador, sl_cr_analises.num_sessao, sl_cr_analises.utente, sl_cr_analises.nome_ute, n_req,"
    Report.SQLQuery = Report.SQLQuery & " sl_cr_analises.dt_chega, sl_cr_analises.cod_proven, sl_cr_analises.descr_proven, sl_cr_analises.cod_sala,"
    Report.SQLQuery = Report.SQLQuery & " sl_cr_analises.descr_sala, sl_cr_analises.cod_gr_ana, sl_cr_analises.descr_gr_ana, sl_cr_analises.cod_gr_trab,"
    Report.SQLQuery = Report.SQLQuery & " sl_cr_analises.Descr_Gr_Trab , sl_cr_analises.cod_ana, sl_cr_analises.descr_ana, sl_cr_analises.peso_estatistico, "
    'rcoelho 3.06.2013 chvng-4177
    Report.SQLQuery = Report.SQLQuery & " sl_cr_analises.N_EPIS , sl_cr_analises.t_sit, sl_cr_analises.descr_t_sit "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_analises WHERE sl_cr_analises.nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & " AND sl_cr_analises.num_sessao = " & gNumeroSessao
    
    If CkEntidade.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY sl_cr_analises.cod_efr"
        If CkDescrReq.value = vbChecked Then
            Report.SQLQuery = Report.SQLQuery & ",sl_cr_analises.n_req asc "
        End If
        Report.SQLQuery = Report.SQLQuery & ", sl_cr_analises.cod_ana, sl_cr_analises.ord_marca "
    ElseIf CkAgruparMedico.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_analises.descr_medico"
        If CkDescrReq.value = vbChecked Then
            Report.SQLQuery = Report.SQLQuery & ",sl_cr_analises.n_req asc "
        End If
        Report.SQLQuery = Report.SQLQuery & ", sl_cr_analises.cod_ana, sl_cr_analises.ord_marca "
    ElseIf CkAgruparProven.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_analises.cod_proven"
        If CkAgruparGrupo.value = vbChecked Then
            Report.SQLQuery = Report.SQLQuery & ", sl_cr_analises.descr_gr_ana"
        End If
        If CkDescrReq.value = vbChecked Then
            Report.SQLQuery = Report.SQLQuery & ",sl_cr_analises.n_req asc "
        End If
        Report.SQLQuery = Report.SQLQuery & ", sl_cr_analises.cod_ana, sl_cr_analises.ord_marca "
    ElseIf CkAgruparGrupo.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_analises.descr_gr_ana "
        If CkDescrReq.value = vbChecked Then
            Report.SQLQuery = Report.SQLQuery & ",sl_cr_analises.n_req asc "
        End If
        Report.SQLQuery = Report.SQLQuery & ", sl_cr_analises.cod_ana, sl_cr_analises.ord_marca "
    ElseIf CkAgruparSala.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_analises.cod_sala "
        If CkDescrReq.value = vbChecked Then
            Report.SQLQuery = Report.SQLQuery & ",sl_cr_analises.n_req asc "
        End If
        Report.SQLQuery = Report.SQLQuery & ", sl_cr_analises.cod_ana, sl_cr_analises.ord_marca "
    ElseIf CkDescrAna.value = vbChecked And CkDescrReq = vbUnchecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_analises.descr_ana ASC, sl_cr_analises.cod_ana ASC  "
    Else
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_analises.n_req, sl_cr_analises.ord_marca "
    End If
    
    Report.SelectionFormula = "{sl_cr_analises.nome_computador} = '" & gComputador & "' AND {sl_cr_analises.num_sessao}= " & gNumeroSessao
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.value)
    
    'GRUPO AN�LISES
    StrTemp = ""
    For i = 0 To EcListaGrAna.ListCount - 1
        StrTemp = StrTemp & EcListaGrAna.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(2) = "GrAna=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(2) = "GrAna=" & BL_TrataStringParaBD("Todos")
    End If
    
    'GRUPO TRABALHO
    StrTemp = ""
    For i = 0 To EcListaGrTrab.ListCount - 1
        StrTemp = StrTemp & EcListaGrTrab.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(3) = "GrTrab=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(3) = "GrTrab=" & BL_TrataStringParaBD("Todos")
    End If
       
    'PROVENIENCIAS
    StrTemp = ""
    For i = 0 To EcListaProven.ListCount - 1
        StrTemp = StrTemp & EcListaProven.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(4) = "Proven=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(4) = "Proven=" & BL_TrataStringParaBD("Todas")
    End If
    
    'SALAS
    StrTemp = ""
    For i = 0 To EcListaSala.ListCount - 1
        StrTemp = StrTemp & EcListaSala.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(5) = "Salas=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(5) = "Salas=" & BL_TrataStringParaBD("Todas")
    End If
    
    'LOCAIS
    StrTemp = ""
    For i = 0 To EcListaLocais.ListCount - 1
        StrTemp = StrTemp & EcListaLocais.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(6) = "Locais=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(6) = "Locais=" & BL_TrataStringParaBD("Todos")
    End If
    
    'ENTIDADES
    StrTemp = ""
    For i = 0 To EcListaEFR.ListCount - 1
        StrTemp = StrTemp & EcListaEFR.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(7) = "EFR=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(7) = "EFR=" & BL_TrataStringParaBD("Todas")
    End If
    
    'ANALISES
    StrTemp = ""
    For i = 0 To EcListaAnalises.ListCount - 1
        StrTemp = StrTemp & EcListaAnalises.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(8) = "Analises=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(8) = "Analises=" & BL_TrataStringParaBD("Todas")
    End If
    
    'rcoelho 20.08.2013 chvng-4401
    'Report.formulas(9) = "AgruparGrAna=" & BL_TrataStringParaBD("N")
    'AGRUPAR SALAS LABORATORIO
    If CkAgruparSalasSede.value = vbChecked Then
        Report.formulas(9) = "AgruparSalasLab=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(9) = "AgruparSalasLab=" & BL_TrataStringParaBD("N")
    End If
    
    Report.formulas(10) = "AgruparGrTrab=" & BL_TrataStringParaBD("N")
   
    'AGRUPAR PROVENI�NCIA
    If CkAgruparProven.value = vbChecked Then
        Report.formulas(11) = "AgruparProven=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(11) = "AgruparProven=" & BL_TrataStringParaBD("N")
    End If
    
    'rcoelho 20.08.2013 chvng-4401
    'AGRUPAR PROVENI�NCIA
'    If CkEntidade.Value = vbChecked Then
'        Report.formulas(12) = "AgruparEFR=" & BL_TrataStringParaBD("S")
'    Else
'        Report.formulas(12) = "AgruparEFR=" & BL_TrataStringParaBD("N")
'    End If
    'DESCRIMINAR MEMBROS
    If CkMembros.value = vbChecked Then
        Report.formulas(12) = "DescrMembros=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(12) = "DescrMembros=" & BL_TrataStringParaBD("N")
    End If
    
    'AGRUPAR SALA
    If CkAgruparSala.value = vbChecked Then
        Report.formulas(13) = "AgruparSala=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(13) = "AgruparSala=" & BL_TrataStringParaBD("N")
    End If
    
    'DESCRIMINAR ANALISES
    If CkDescrAna.value = vbChecked Then
        Report.formulas(14) = "DescrAna=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(14) = "DescrAna=" & BL_TrataStringParaBD("N")
    End If
    
    'DESCRIMINAR REQUISICOES
    If CkDescrReq.value = vbChecked Then
        Report.formulas(15) = "DescrReq=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(15) = "DescrReq=" & BL_TrataStringParaBD("N")
    End If
    
    'AGRUPAR EFR
    If CkEntidade.value = vbChecked Then
        Report.formulas(16) = "AgruparEFR=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(16) = "AgruparEFR=" & BL_TrataStringParaBD("N")
    End If
    
    'URGENCIA
    Report.formulas(17) = "Urgencia=" & BL_TrataStringParaBD(cbUrgencia)
    
    'MEDICOS
    StrTemp = ""
    For i = 0 To EcListaMedicos.ListCount - 1
        StrTemp = StrTemp & EcListaMedicos.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(18) = "MEDICOS=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(18) = "MEDICOS=" & BL_TrataStringParaBD("Todos")
    End If
    
    'REQUISICOES COM APENAS ANALISES SELECIONADAS
    If CkRestringeRequis.value = vbChecked Then
        Report.formulas(19) = "RestrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(19) = "RestrRequis=" & BL_TrataStringParaBD("N")
    End If
    
    'AGRUPAR GRUPO ANALISES
    If CkAgruparGrupo.value = vbChecked Then
        Report.formulas(20) = "AgruparGrAna=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(20) = "AgruparGrAna=" & BL_TrataStringParaBD("N")
    End If
    
    'AGRUPAR MEDICO
    If CkAgruparMedico.value = vbChecked Then
        Report.formulas(21) = "AgruparMedico=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(21) = "AgruparMedico=" & BL_TrataStringParaBD("N")
    End If
    
    'SEXO
    Report.formulas(22) = "Sexo=" & BL_TrataStringParaBD(CbSexo)
    
    'rcoelho 3.06.2013 chvng-4177
    'SITUACAO
    Report.formulas(23) = "Situacao=" & BL_TrataStringParaBD(CbSituacao)
    
    'rcoelho 20.08.2013 chvng-4401
    'TIPO ANALISE
    Report.formulas(24) = "TipoAnalise=''"
    'DESCONTINUADO
'    If Opt(0).Value = True Then
'        Report.formulas(24) = "TipoAnalise='Simples'"
'    ElseIf Opt(1).Value = True Then
'        Report.formulas(24) = "TipoAnalise='Complexas'"
'    ElseIf Opt(2).Value = True Then
'        Report.formulas(24) = "TipoAnalise='Exames'"
'    End If
    
    'rcoelho 20.08.2013 chvng-4401
    'APENAS SEM RESULTADO
    If CkApenasSemResultado.value = vbChecked Then
        Report.formulas(25) = "ApenasSemResultado=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(25) = "ApenasSemResultado=" & BL_TrataStringParaBD("N")
    End If
    
    'rcoelho 20.08.2013 chvng-4401
    'INCLUIR RESULTADO
    If CkMarcacoes.value = vbChecked Then
        Report.formulas(26) = "IncluirResultado=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(26) = "IncluirResultado=" & BL_TrataStringParaBD("N")
    End If
    
    'rcoelho 20.08.2013 chvng-4401
    'CONTAR REPETI��ES
    If CkRepet.value = vbChecked Then
        Report.formulas(27) = "ContarRepeticoes=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(27) = "ContarRepeticoes=" & BL_TrataStringParaBD("N")
    End If
    
    'Report.SubreportToChange = ""
    Call BL_ExecutaReport
    
    sSql = " DELETE FROM sl_cr_analises WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql

    
End Sub




Private Sub PreencheTabelaTemporaria()
    Dim sSql As String
    Dim sSqlCONDICOES As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = " DELETE FROM sl_cr_analises WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
        
    sSql = "INSERT INTO sl_Cr_analises (nome_computador, num_sessao, utente, nome_ute, n_req,dt_chega, cod_proven,"
    sSql = sSql & " descr_proven, cod_sala, descr_sala, cod_gr_ana, descr_gr_ana, "
    sSql = sSql & " cod_ana, descr_ana, peso_estatistico, cod_efr, descr_efr, n_epis, t_sit, ord_marca, quantidade, "
    'rcoelho 3.06.2013 chvng-4177
    sSql = sSql & " cod_medico, descr_medico, cod_local_entrega, descr_local_entrega,num_repeticoes, descr_t_sit)"
    
    'NELSONSPSILVA lgas-263 28.07.2017 Foi substituido o campo ana.peso_estatisco pela fun��o PesoEstatistico
    'rcoelho 5.06.2013 chvng-4193
    If CkApenasSemResultado.value = vbChecked Then
            sSql = sSql & " ( SELECT " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", ute.utente, ute.nome_ute, Req.n_req, Req.dt_chega,"
            sSql = sSql & " Proven.cod_proven , Proven.descr_proven, "
            sSql = sSql & " sala.cod_sala, sala.descr_sala,  grAna.cod_gr_ana, grAna.descr_gr_Ana, ana.cod_ana, ana.descr_ana, "
            sSql = sSql & PesoEstatistico & ", req.cod_efr, efr.descr_Efr ,req.n_epis, req.t_sit, res.ord_ana, 1, med.cod_med, med.nome_med "
            sSql = sSql & ", le.cod_local_entrega, le.descr_local_entrega "
            sSql = sSql & ConstroiRepeticoes
            'rcoelho 3.06.2013 chvng-4177
            sSql = sSql & ", sit.descr_t_sit"
            sSql = sSql & ConstroiFROM("sl_marcacoes")
            sSql = sSql & ConstroiWHERE
            sSql = sSql & ConstroiGROUPBY
    Else
        sSql = sSql & " ( SELECT " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", ute.utente, ute.nome_ute, Req.n_req, Req.dt_chega,"
        sSql = sSql & " Proven.cod_proven , Proven.descr_proven, "
        sSql = sSql & " sala.cod_sala, sala.descr_sala,  grAna.cod_gr_ana, grAna.descr_gr_Ana, ana.cod_ana, ana.descr_ana, "
        sSql = sSql & PesoEstatistico & ", req.cod_efr, efr.descr_Efr ,req.n_epis, req.t_sit, res.ord_ana, 1, med.cod_med, med.nome_med "
        sSql = sSql & ", le.cod_local_entrega, le.descr_local_entrega "
        sSql = sSql & ConstroiRepeticoes
        'rcoelho 3.06.2013 chvng-4177
        sSql = sSql & ", sit.descr_t_sit"
        sSql = sSql & ConstroiFROM("sl_realiza")
        sSql = sSql & ConstroiWHERE
        sSql = sSql & ConstroiGROUPBY
        
        If CkMarcacoes.value = vbChecked Then
            sSql = sSql & " UNION SELECT " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", ute.utente, ute.nome_ute, Req.n_req, Req.dt_chega,"
            sSql = sSql & " Proven.cod_proven , Proven.descr_proven, "
            sSql = sSql & " sala.cod_sala, sala.descr_sala,  grAna.cod_gr_ana, grAna.descr_gr_Ana, ana.cod_ana, ana.descr_ana, "
            sSql = sSql & PesoEstatistico & ", req.cod_efr, efr.descr_Efr ,req.n_epis, req.t_sit, res.ord_ana, 1, med.cod_med, med.nome_med "
            sSql = sSql & ", le.cod_local_entrega, le.descr_local_entrega "
            sSql = sSql & ConstroiRepeticoes
            'rcoelho 3.06.2013 chvng-4177
            sSql = sSql & ", sit.descr_t_sit"
            sSql = sSql & ConstroiFROM("sl_marcacoes")
            sSql = sSql & ConstroiWHERE
            sSql = sSql & ConstroiGROUPBY
        End If
        
        sSql = sSql & " UNION SELECT " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", ute.utente, ute.nome_ute, Req.n_req, Req.dt_chega,"
        sSql = sSql & " Proven.cod_proven , Proven.descr_proven, "
        sSql = sSql & " sala.cod_sala, sala.descr_sala,  grAna.cod_gr_ana, grAna.descr_gr_Ana, ana.cod_ana, ana.descr_ana, "
        sSql = sSql & PesoEstatistico & ", req.cod_efr, efr.descr_Efr ,req.n_epis, req.t_sit, res.ord_ana, 1, med.cod_med, med.nome_med "
        sSql = sSql & ", le.cod_local_entrega, le.descr_local_entrega "
        sSql = sSql & ConstroiRepeticoes
        'rcoelho 3.06.2013 chvng-4177
        sSql = sSql & ", sit.descr_t_sit"
        sSql = sSql & ConstroiFROM("sl_realiza_h")
        sSql = sSql & ConstroiWHERE
        sSql = sSql & ConstroiGROUPBY
    End If
    
    sSql = sSql & ")" & ConstroiORDER
    BG_ExecutaQuery_ADO sSql
Exit Sub

TrataErro:
    BG_Mensagem mediMsgBox, "N�o � poss�vel gerar a estat�stica de an�lises.", vbCritical, "Erro de Base Dados"
    BG_LogFile_Erros "FormEstatAna - PreencheTabelaTemporaria: " & Err.Number & " (" & Err.Description & ")"
    BG_LogFile_Erros sSql
    Exit Sub
    Resume Next
End Sub

Private Function ConstroiFROM(Tabela As String) As String
    Dim sSql As String
    Dim i As Integer
    sSql = ""
    sSql = sSql & " FROM sl_identif ute, sl_efr efr , sl_requis req, sl_cod_local_entrega le "
    
    If gMultiLocalAmbito = mediSim Then
        sSql = sSql & ", sl_ana_acrescentadas aa "
    End If

    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_proven proven"
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_proven proven ON req.cod_proven = proven.cod_proven"
    End If

    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_cod_salas sala"
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_cod_salas sala ON req.cod_sala = sala.cod_sala "
    End If
        
    ' ------------------------------------------------------------------------------
    ' M�DICO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_medicos med "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_medicos med ON req.cod_med = med.cod_med "
    End If
    
    sSql = sSql & ", " & Tabela & " res, slv_analises ana "
    
    ' ------------------------------------------------------------------------------
    ' GRUPO DE ANALISES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_gr_ana grAna "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_gr_ana grAna ON ana.cod_gr_ana = grAna.cod_gr_ana "
    End If
    
    ' ------------------------------------------------------------------------------
    ' CODIGO DE FACTURACAO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        'sSql = sSql & " LEFT OUTER JOIN sl_ana_facturacao anaFact ON ana.cod_ana = anaFact.cod_ana "
    End If
    
    ' ------------------------------------------------------------------------------
    ' GRUPO DE TRABALHO PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaGrTrab.ListCount > 0 Then
        sSql = sSql & ", sl_gr_trab grTrab, sl_ana_trab anaTrab "
    End If
    

    ' ------------------------------------------------------------------------------
    ' ANALISES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaAnalises.ListCount > 0 Then
        sSql = sSql & ", slv_analises anaV"
    End If
    
    'rcoelho 3.06.2013 chvng-4177
    If gSGBD = gOracle Then
        sSql = sSql & ", sl_tbf_t_sit sit "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " LEFT OUTER JOIN sl_tbf_t_sit sit ON req.t_sit = sit.cod_t_sit "
    End If
    
    ConstroiFROM = sSql
End Function


Private Function ConstroiWHERE() As String
    Dim sSql As String
    Dim i As Integer
    sSql = ""
    sSql = sSql & " WHERE ute.seq_utente = req.seq_utente AND req.n_req = res.n_req "
    If CkMembros.value = vbChecked Then
        sSql = sSql & " AND res.cod_ana_s = ana.cod_ana AND res.cod_Ana_s <> 'S99999' "
    Else
        sSql = sSql & " AND res.cod_agrup = ana.cod_ana "
    End If
    sSql = sSql & " AND req.estado_req <> " & BL_TrataStringParaBD(gEstadoReqCancelada)
    sSql = sSql & " AND (req.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & " )"
    sSql = sSql & " AND ana.peso_estatistico >0 AND (ana.seq_sinonimo is null or ana.seq_sinonimo = -1)"
    sSql = sSql & " AND req.cod_local_entrega = le.cod_local_entrega (+)"
    
    If gTipoInstituicao = "HOSPITALAR" Then
        sSql = sSql & " AND req.cod_efr = efr.cod_efr (+)"
    Else
        sSql = sSql & " AND req.cod_efr = efr.cod_efr "
    End If
    
    If gMultiLocalAmbito = mediSim Then
        sSql = sSql & " AND  req.n_Req  = aa.n_req AND res.cod_agrup = aa.cod_agrup AND (flg_eliminada = 0 or flg_eliminada IS NULL) "
    End If
    
    ' ------------------------------------------------------------------------------
    ' SE URGENCIA PREENCHIDA
    ' ------------------------------------------------------------------------------
    If cbUrgencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND req.t_urg = " & BG_DaComboSel(cbUrgencia)
    End If
    
    ' ------------------------------------------------------------------------------
    ' SE SEXO PREENCHIDO
    ' ------------------------------------------------------------------------------
    If CbSexo.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND ute.sexo_ute = " & BG_DaComboSel(CbSexo)
    End If
    
    ' ------------------------------------------------------------------------------
    ' GRUPO DE ANALISES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & " AND ana.cod_gr_ana = grAna.cod_gr_ana (+) "
    End If
    
    If EcListaGrAna.ListCount > 0 Then
        sSql = sSql & " AND grAna.seq_gr_ana  IN ("
        For i = 0 To EcListaGrAna.ListCount - 1
            sSql = sSql & EcListaGrAna.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' GRUPO DE TRABALHO PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaGrTrab.ListCount > 0 Then
        sSql = sSql & " AND ana.cod_ana = anaTrab.cod_analise AND anaTrab.cod_gr_trab = grTrab.cod_gr_trab "
        sSql = sSql & " AND grTrab.seq_gr_Trab  IN ("
        For i = 0 To EcListaGrTrab.ListCount - 1
            sSql = sSql & EcListaGrTrab.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------------------------------------------------
    ' MEDICOS PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & " AND req.cod_med = med.cod_med (+) "
    End If
    
    If EcListaMedicos.ListCount > 0 Then
        sSql = sSql & " AND med.seq_med  IN ("
        For i = 0 To EcListaMedicos.ListCount - 1
            sSql = sSql & EcListaMedicos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If gSGBD = gOracle Then
        sSql = sSql & "  AND req.cod_proven= proven.cod_proven  (+) "
    End If
    If EcListaProven.ListCount > 0 Then
        sSql = sSql & " AND proven.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & EcListaProven.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------------------------------------------------
    ' LOCAL ENTREGA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocalEntrega.ListCount > 0 Then
        sSql = sSql & "  AND le.cod_local_entrega in ("
        For i = 0 To EcListaLocalEntrega.ListCount - 1
            sSql = sSql & EcListaLocalEntrega.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If CkAgruparSalasSede.value = vbChecked Then
        sSql = sSql & "  AND req.cod_sala in (SELECT cod_sala FROM sl_cod_salas WHERE flg_colheita =1 )"
        If gSGBD = gOracle Then
            sSql = sSql & "  AND req.cod_sala= sala.cod_sala (+) "
        End If
    Else
        If gSGBD = gOracle Then
            sSql = sSql & "  AND req.cod_sala= sala.cod_sala (+) "
        End If
        If EcListaSala.ListCount > 0 Then
            sSql = sSql & " AND sala.seq_sala  IN ("
            For i = 0 To EcListaSala.ListCount - 1
                sSql = sSql & EcListaSala.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
    End If
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        If gMultiLocalAmbito = mediSim Then
            sSql = sSql & " AND aa.cod_local IN ("
        Else
            sSql = sSql & " AND req.cod_local IN ("
        End If
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' ENTIDADES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaEFR.ListCount > 0 Then
        sSql = sSql & " AND req.cod_efr= efr.cod_efr AND efr.seq_efr  IN ("
        For i = 0 To EcListaEFR.ListCount - 1
            sSql = sSql & EcListaEFR.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' ANALISES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaAnalises.ListCount > 0 Then
        Dim sSqlAux As String
        sSql = sSql & " AND res.cod_agrup = anaV.cod_ana AND anav.seq_ana  IN ("
        
        For i = 0 To EcListaAnalises.ListCount - 1
            sSql = sSql & EcListaAnalises.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        
        ' ------------------------------------------------------------------------------
        ' REQUISI��O N�O PODE TER MAIS NADA DO QUE AS AN�LISES SELECCIONADAS
        ' ------------------------------------------------------------------------------
        If CkRestringeRequis.value = vbChecked Then
            sSql = sSql & " AND req.n_req NOT IN (SELECT n_req FROM SL_MARCACOES m1 WHERE m1.n_req = req.n_req"
            
            sSql = sSql & " AND m1.cod_agrup<> anav.cod_ana AND anaV.seq_ana  IN ("
            
            For i = 0 To EcListaAnalises.ListCount - 1
                sSql = sSql & EcListaAnalises.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") )"
        
        
            sSql = sSql & " AND req.n_req NOT IN (SELECT n_req FROM sl_Realiza r1 WHERE r1.n_req = req.n_req"
            
            sSql = sSql & " AND r1.cod_agrup<> anav.cod_ana AND anaV.seq_ana  IN ("
            
            For i = 0 To EcListaAnalises.ListCount - 1
                sSql = sSql & EcListaAnalises.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") )"
        End If
    End If
    
    'rcoelho 3.06.2013 chvng-4177
    If CbSituacao.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND req.t_sit = " & BG_DaComboSel(CbSituacao)
    End If
    If gSGBD = gOracle Then
        sSql = sSql & "  AND req.t_sit = sit.cod_t_sit (+) "
    End If
    
    ConstroiWHERE = sSql
End Function

Private Function ConstroiRepeticoes() As String

    If CkRepet.value = vbChecked Then
        ConstroiRepeticoes = ConstroiRepeticoes & ", (SELECT MAX (COUNT ( * )) conta FROM sl_repeticoes rep WHERE n_req = req.n_req AND (   rep.cod_perfil = ana.cod_ana "
        ConstroiRepeticoes = ConstroiRepeticoes & " OR rep.cod_ana_c = ana.cod_ana OR rep.cod_ana_s = ana.cod_ana) GROUP BY n_Req, cod_perfil, cod_ana_c, cod_ana_s) repet "
    Else
        ConstroiRepeticoes = ConstroiRepeticoes & ", 0 repet"
    End If
End Function

Private Function ConstroiGROUPBY() As String
    Dim sSql As String
    If gSGBD = gOracle Then
        sSql = "GROUP BY utente, nome_ute, req.n_req, req.dt_chega, proven.cod_proven,proven.descr_proven, "
        sSql = sSql & "grAna.cod_gr_ana, grAna.descr_gr_ana, sala.cod_sala , sala.descr_sala, ana.cod_ana, ana.descr_ana,"
        sSql = sSql & " ana.peso_estatistico, req.cod_efr, req.n_epis, req.t_sit, res.ord_ana, efr.descr_efr, med.cod_med,med.nome_med "
        sSql = sSql & ", le.cod_local_entrega, le.descr_local_entrega"
        'rcoelho 3.06.2013 chvng-4177
        sSql = sSql & ", sit.descr_t_sit"
    End If
    ConstroiGROUPBY = sSql
    
End Function

Private Function ConstroiORDER() As String
    Dim sSql As String
    sSql = ""
    If gSGBD = gOracle Then
        If CkEntidade.value = vbChecked Then
           sSql = sSql & " ORDER BY  cod_efr, n_Req, cod_sala,ord_ana, cod_ana "
        Else
           sSql = sSql & " ORDER BY   n_Req, cod_sala,ord_ana, cod_ana "
        End If
    ElseIf gSGBD = gSqlServer Then
        If CkEntidade.value = vbChecked Then
               sSql = sSql & " ORDER BY req.cod_efr, req.n_Req, sala.cod_sala, ord_ana, ana.cod_ana "
        Else
               sSql = sSql & " ORDER BY req.n_Req, sala.cod_sala, ord_ana, ana.cod_ana "
        End If
    End If
    ConstroiORDER = sSql

End Function

'NELSONPSILVA lgas-263 28.07.2017
Private Function PesoEstatistico() As String

    If CkRepet.value = vbChecked Then
        PesoEstatistico = " ana.peso_estatistico * (decode((SELECT MAX (COUNT (*)) conta FROM sl_repeticoes rep Where n_req = Req.n_req " & _
        " AND (rep.cod_perfil = ana.cod_ana OR rep.cod_ana_c = ana.cod_ana OR rep.cod_ana_s = ana.cod_ana)GROUP BY n_req, " & _
        " cod_perfil, cod_ana_c, cod_ana_s),NULL,0,0,0,(SELECT MAX (COUNT (*)) conta FROM sl_repeticoes rep WHERE n_req = Req.n_req " & _
        " AND (rep.cod_perfil = ana.cod_ana OR rep.cod_ana_c = ana.cod_ana OR rep.cod_ana_s = ana.cod_ana) " & _
        " GROUP BY n_req, cod_perfil, cod_ana_c, cod_ana_s))+ ana.peso_estatistico) peso_estatistico "
    Else
        PesoEstatistico = " ana.peso_estatistico "
    End If
    
End Function


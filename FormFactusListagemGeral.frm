VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormFactusListagemGeral 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactusListagemGeral"
   ClientHeight    =   5985
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7935
   Icon            =   "FormFactusListagemGeral.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   7935
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Height          =   975
      Left            =   120
      TabIndex        =   16
      Top             =   4800
      Width           =   7575
      Begin VB.CheckBox CkMostrarAna 
         Caption         =   "Discriminar An�lises"
         Height          =   255
         Left            =   5760
         TabIndex        =   20
         Top             =   120
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Agrupar Por Sala"
         Height          =   255
         Index           =   2
         Left            =   1800
         TabIndex        =   19
         Top             =   120
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Agrupar por An�lise"
         Height          =   255
         Index           =   1
         Left            =   3600
         TabIndex        =   18
         Top             =   120
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Agrupar Por EFR"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Height          =   3375
      Left            =   120
      TabIndex        =   6
      Top             =   1440
      Width           =   7575
      Begin VB.CommandButton BtPesquisaRubr 
         Height          =   315
         Left            =   6120
         Picture         =   "FormFactusListagemGeral.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   2040
         Width           =   375
      End
      Begin VB.ListBox ListaRubr 
         Height          =   645
         Left            =   1320
         MultiSelect     =   2  'Extended
         TabIndex        =   13
         Top             =   2040
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaPostos 
         Height          =   315
         Left            =   6120
         Picture         =   "FormFactusListagemGeral.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   1200
         Width           =   375
      End
      Begin VB.ListBox ListaPostos 
         Height          =   645
         Left            =   1320
         MultiSelect     =   2  'Extended
         TabIndex        =   10
         Top             =   1200
         Width           =   4800
      End
      Begin VB.ListBox ListaEntidades 
         Height          =   645
         Left            =   1320
         MultiSelect     =   2  'Extended
         TabIndex        =   8
         Top             =   360
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaEntidades 
         Height          =   315
         Left            =   6120
         Picture         =   "FormFactusListagemGeral.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   360
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "An�lises"
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   15
         Top             =   2040
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Postos"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   12
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Entidades"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1455
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   7575
      Begin VB.OptionButton Opt1 
         Caption         =   "Separar por Ano"
         Height          =   195
         Index           =   2
         Left            =   3960
         TabIndex        =   23
         Top             =   1080
         Width           =   1815
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Separar por M�s"
         Height          =   195
         Index           =   1
         Left            =   2040
         TabIndex        =   22
         Top             =   1080
         Width           =   1815
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Sem Separa��o"
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   21
         Top             =   1080
         Width           =   1815
      End
      Begin VB.CommandButton BtReportLista 
         Height          =   725
         Left            =   6000
         Picture         =   "FormFactusListagemGeral.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Pr� visualizar Listagem"
         Top             =   240
         Width           =   735
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   300
         Left            =   1200
         TabIndex        =   2
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   145555457
         CurrentDate     =   39300
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   300
         Left            =   4200
         TabIndex        =   3
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   145555457
         CurrentDate     =   39300
      End
      Begin VB.Label Label1 
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Data Final"
         Height          =   255
         Index           =   1
         Left            =   3120
         TabIndex        =   4
         Top             =   480
         Width           =   975
      End
   End
End
Attribute VB_Name = "FormFactusListagemGeral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Public CamposBDparaListBoxF
Dim NumEspacos

Private Type dados
    t_utente As String
    Utente As String
    nome_ute As String
    n_req As String
    dt_chega As String
    cod_sala As String
    descr_sala As String
    cod_efr As String
    descr_efr As String
    valor_utente As Double  'total_pagar tabela recibos
    dt_emi As String
    serie_rec As String
    n_rec As String
    valor_efr_sislab As Double   'SUM (srd.quantidade * fpr.val_pag_ent)
    qtd_sislab As Integer
    dt_fact As String 'factus
    n_fac_efr As String ' factus
    valor_efr_factus As Double 'factus
    valor_ute_factus As Double 'factus
    qtd_factus As Integer 'factus
    tabela_efr As String 'factus
End Type

Dim estrutDados() As dados
Dim totalDados As Integer


Sub Preenche_Estatistica()
    Dim sql As String
    Dim nomeReport As String
    
    Dim i As Integer
    Dim continua As Boolean
    sql = "DELETE FROM sl_cr_listagem_factus WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sql
    
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Listagem de Factura��o - Geral") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    
    If Option1(0).value = True Then
        nomeReport = "ListagemFactus_EFR"
    ElseIf Option1(2).value = True Then
        nomeReport = "ListagemFactus_Sala"
    ElseIf Option1(1).value = True Then
        nomeReport = "ListagemFactus_Rubr"
    End If
    
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Listagem de Factura��o - Geral", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Listagem de Factura��o - Geral", crptToWindow)
    End If
    If continua = False Then Exit Sub
    PreencheTabelaTemporaria
        
    
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.WindowState = crptMaximized
    
    Report.SQLQuery = "SELECT * "
    Report.SQLQuery = Report.SQLQuery & " From sl_cr_listagem_factus sl_cr_listagem_factus  WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & "  ORDER BY "
    If Option1(0).value = True Then
        Report.SQLQuery = Report.SQLQuery & " descr_efr ASC "
    ElseIf Option1(2).value = True Then
        Report.SQLQuery = Report.SQLQuery & " descr_sala ASC "
    End If

    Report.SQLQuery = Report.SQLQuery & ", agrupamento ASC, cod_rubr ASC"
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.value)
    
    ' ---------------------------------------------------------------------------------
    ' ENTIDADES
    ' ---------------------------------------------------------------------------------
    Dim StrEntidades As String
    For i = 0 To ListaEntidades.ListCount - 1
        StrEntidades = StrEntidades & ListaEntidades.List(i) & "; "
    Next i
    If StrEntidades <> "" Then
        StrEntidades = Mid(StrEntidades, 1, Len(StrEntidades) - 2)
        If Len(StrEntidades) > 200 Then
            StrEntidades = left(StrEntidades, 200) & "..."
        End If
        Report.formulas(2) = "Entidades=" & BL_TrataStringParaBD("" & StrEntidades)
    Else
        Report.formulas(2) = "Entidades=" & BL_TrataStringParaBD("Todas")
    End If
    
    ' ---------------------------------------------------------------------------------
    ' POSTOS
    ' ---------------------------------------------------------------------------------
    Dim StrPostos As String
    For i = 0 To ListaPostos.ListCount - 1
        StrPostos = StrPostos & ListaPostos.List(i) & "; "
    Next i
    If StrPostos <> "" Then
        StrPostos = Mid(StrPostos, 1, Len(StrPostos) - 2)
        If Len(StrPostos) > 200 Then
            StrPostos = left(StrPostos, 200) & "..."
        End If
        Report.formulas(3) = "Postos=" & BL_TrataStringParaBD("" & StrPostos)
    Else
        Report.formulas(3) = "Postos=" & BL_TrataStringParaBD("Todos")
    End If
    
    ' ---------------------------------------------------------------------------------
    ' An�lises
    ' ---------------------------------------------------------------------------------
    Dim StrAna As String
    For i = 0 To ListaRubr.ListCount - 1
        StrPostos = StrAna & ListaRubr.List(i) & "; "
    Next i
    If StrAna <> "" Then
        StrAna = Mid(StrAna, 1, Len(StrAna) - 2)
        If Len(StrAna) > 200 Then
            StrAna = left(StrAna, 200) & "..."
        End If
        Report.formulas(3) = "Rubr=" & BL_TrataStringParaBD("" & StrAna)
    Else
        Report.formulas(3) = "Rubr=" & BL_TrataStringParaBD("Todas")
    End If
    
    If CkMostrarAna.value = vbChecked Then
        Report.formulas(4) = "DescrRubr='S'"
    Else
        Report.formulas(4) = "DescrRubr='N'"
    End If
    
    Call BL_ExecutaReport
 

End Sub

Private Sub BtPesquisaentidades_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_efr "
    CWhere = ""
    CampoPesquisa = "descr_efr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_efr ", _
                                                                           "Entidades")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaEntidades.ListCount = 0 Then
                ListaEntidades.AddItem BL_SelCodigo("sl_efr", "descr_efr", "seq_efr", resultados(i))
                ListaEntidades.ItemData(0) = resultados(i)
            Else
                ListaEntidades.AddItem BL_SelCodigo("sl_efr", "descr_efr", "seq_efr", resultados(i))
                ListaEntidades.ItemData(ListaEntidades.NewIndex) = resultados(i)
            End If
        Next i
        

    End If

End Sub


Private Sub BtPesquisaPostos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CWhere = ""
    CampoPesquisa = "descr_sala"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_sala ", _
                                                                           " Postos Colheita")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPostos.ListCount = 0 Then
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(0) = resultados(i)
            Else
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(ListaPostos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Listagem Factura��o - Geral"
    
    Me.left = 100
    Me.top = 100
    Me.Width = 8025
    Me.Height = 6405 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Listagem de Factura��o - Geral")
    
    Set FormFactusListagemGeral = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    Option1(0).value = True
    ListaEntidades.Clear
    ListaPostos.Clear
    ListaRubr.Clear
    Opt1(0).value = True
    
End Sub

Sub DefTipoCampos()
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    Option1(0).value = True
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    Opt1(0).value = True
    
End Sub

Sub FuncaoProcurar()
' nada
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub



Sub PreencheTabelaTemporaria()
    Dim rsSISLAB As New ADODB.recordset
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    ReDim estrutDados(0)
    totalDados = 0
    sSql = "DELETE FROM sl_cr_listagem_factus WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    sSql = "SELECT x1.serie_fac, x1.n_fac, x3.dt_emiss_fac, x1.doente, x1.nome_doente, x2.n_req,"
    sSql = sSql & " x1.dt_ini_real, x1.cod_rubr, x1.cod_rubr_efr, x1.descr_rubr,"
    sSql = sSql & " x1.val_pr_unit, x1.val_taxa, x1.val_total, x1.val_pag_doe, x4.cod_efr,"
    sSql = sSql & " x4.descr_efr, x5.cod_sala, x5.descr_sala FROM fa_lin_fact x1, sl_Requis x2, "
    sSql = sSql & " fa_fact x3,sl_efr x4, sl_cod_salas x5, fa_rubr x6 "
    sSql = sSql & " WHERE x1.episodio = x2.n_req AND x1.serie_fac = x3.serie_fac and x1.n_fac = x3.n_fac "
    sSql = sSql & " AND x1.cod_rubr = x6.cod_rubr    "
    sSql = sSql & " AND x3.cod_Efr = x4.cod_efr AND x2.cod_sala = x5.cod_sala "
    sSql = sSql & " AND x3.dt_emiss_fac BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    sSql = sSql & " AND x3.user_canc IS NULL "
    
    ' ------------------------------------------------------------------------------
    ' ENTIDADE PREENCHIDA
    ' ------------------------------------------------------------------------------
    If ListaEntidades.ListCount > 0 Then
        sSql = sSql & " AND x4.seq_efr IN ("
        For i = 0 To ListaEntidades.ListCount - 1
            sSql = sSql & ListaEntidades.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' POSTOS PREENCHIDA
    ' ------------------------------------------------------------------------------
    If ListaPostos.ListCount > 0 Then
        sSql = sSql & " AND x5.seq_sala IN ("
        For i = 0 To ListaPostos.ListCount - 1
            sSql = sSql & ListaPostos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' RUBRICAS PREENCHIDA
    ' ------------------------------------------------------------------------------
    If ListaPostos.ListCount > 0 Then
        sSql = sSql & " AND x6.cod_rubr IN ("
        For i = 0 To ListaPostos.ListCount - 1
            sSql = sSql & ListaPostos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    rsSISLAB.CursorType = adOpenStatic
    rsSISLAB.CursorLocation = adUseServer
    rsSISLAB.Open sSql, gConexao
    
    If rsSISLAB.RecordCount > 0 Then
        While Not rsSISLAB.EOF
            sSql = "INSERT INTO sl_cr_listagem_factus ( nome_computador, num_sessao,serie_fac, n_fac, "
            sSql = sSql & " dt_emiss_fac,doente, nome_doente, n_req,"
            sSql = sSql & " dt_ini_real, cod_rubr, cod_rubr_efr, descr_rubr,"
            sSql = sSql & " val_pr_unit, val_taxa, val_total, val_pag_doe, cod_efr,"
            sSql = sSql & " descr_efr, cod_sala, descr_sala,agrupamento) VALUES( "
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ","
            sSql = sSql & gNumeroSessao & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!serie_fac, "")) & ","
            sSql = sSql & BL_HandleNull(rsSISLAB!n_fac, 0) & ","
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSISLAB!dt_emiss_fac, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!doente, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!nome_doente, "")) & ","
            sSql = sSql & BL_HandleNull(rsSISLAB!n_req, 0) & ","
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSISLAB!dt_ini_real, "")) & ","
            sSql = sSql & BL_HandleNull(rsSISLAB!cod_rubr, 0) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!cod_rubr_efr, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!descr_rubr, "")) & ","
            sSql = sSql & Replace(BL_HandleNull(rsSISLAB!val_pr_unit, 0), ",", ".") & ","
            sSql = sSql & Replace(BL_HandleNull(rsSISLAB!val_Taxa, 0), ",", ".") & ","
            sSql = sSql & Replace(BL_HandleNull(rsSISLAB!val_total, 0), ",", ".") & ","
            sSql = sSql & Replace(BL_HandleNull(rsSISLAB!val_pag_doe, 0), ",", ".") & ","
            sSql = sSql & BL_HandleNull(rsSISLAB!cod_efr, 0) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!descr_efr, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!cod_sala, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISLAB!descr_sala, "")) & ","
            If Opt1(1).value = True Then
                sSql = sSql & Month(BL_HandleNull(rsSISLAB!dt_emiss_fac, "")) & " )"
            ElseIf Opt1(1).value = True Then
                sSql = sSql & Year(BL_HandleNull(rsSISLAB!dt_emiss_fac, "")) & " )"
            Else
                sSql = sSql & " 0)"
            End If
            BG_ExecutaQuery_ADO sSql
            rsSISLAB.MoveNext
        Wend
    End If
    rsSISLAB.Close
    Set rsSISLAB = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Tabela Temporaria: " & sSql & " - " & Err.Description, Me.Name, "PreencheTabelaTemporaria", True
    Exit Sub
    Resume Next
End Sub



Private Sub Listaentidades_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaEntidades.ListCount > 0 Then     'Delete
        ListaEntidades.RemoveItem (ListaEntidades.ListIndex)
    End If
End Sub

Private Sub ListaPostos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPostos.ListCount > 0 Then     'Delete
        ListaPostos.RemoveItem (ListaPostos.ListIndex)
    End If
End Sub

Private Sub BtReportLista_Click()
    gImprimirDestino = 0
    Call Preenche_Estatistica
End Sub






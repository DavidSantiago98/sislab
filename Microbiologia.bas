Attribute VB_Name = "Microbiologia"
Option Explicit
' ----------------------------------------------
' ESTRUTURA PARA CARREGA RESULTADOS ANTIBIOTICOS
' ----------------------------------------------
Private Type ResultadoMicrorg
    seq_realiza As Long
    seq_utente As Long
    n_req As String
    dt_chega As String
    cod_micro As String
    Cod_Gr_Antibio As String
    cod_produto As String
    prazo_ve As Integer
    Prova As Integer
    errosMinor As Integer
    errosMajor As Integer
    antibioticosComuns As Integer
    antibioticos() As ResAntib
    totalAntib As Integer
End Type
' ----------------------------------------------

Private Type CaracterizacaoMicro
    cod_carac_micro As Integer
    descr_carac_micro As String
    flg_ve As Integer
    flg_defeito As Integer
End Type
Global gCaracMicro() As CaracterizacaoMicro
Global gTotalCaracMicro As Integer
Dim A As String


Public Sub MICRO_PreencheCaracMicro()
    Dim sSql As String
    Dim rsMicro As New ADODB.recordset
    gTotalCaracMicro = 0
    ReDim gCaracMicro(0)
    
    sSql = "SELECT * FROM sl_tbf_carac_micro"
    rsMicro.CursorLocation = adUseClient
    rsMicro.CursorType = adOpenKeyset
    rsMicro.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMicro.Open sSql, gConexao
    If rsMicro.RecordCount >= 1 Then
        While Not rsMicro.EOF
            gTotalCaracMicro = gTotalCaracMicro + 1
            ReDim Preserve gCaracMicro(gTotalCaracMicro)
            gCaracMicro(gTotalCaracMicro).cod_carac_micro = BL_HandleNull(rsMicro!cod_carac_micro, "")
            gCaracMicro(gTotalCaracMicro).descr_carac_micro = BL_HandleNull(rsMicro!descr_carac_micro, "")
            gCaracMicro(gTotalCaracMicro).flg_ve = BL_HandleNull(rsMicro!flg_ve, 0)
            gCaracMicro(gTotalCaracMicro).flg_defeito = BL_HandleNull(rsMicro!flg_defeito, 0)
            rsMicro.MoveNext
        Wend
    End If
    rsMicro.Close
    Set rsMicro = Nothing
End Sub

Public Function MICRO_DevolveCaracMicro(cod_agrup As String) As String
    On Error GoTo TrataErro
    Dim i As Integer
    Dim sSql As String
    Dim rsMicro As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_agrup)
    rsMicro.CursorLocation = adUseClient
    rsMicro.CursorType = adOpenKeyset
    rsMicro.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMicro.Open sSql, gConexao
    If rsMicro.RecordCount >= 1 Then
        If BL_HandleNull(rsMicro!cod_carac_micro, "") <> "" Then
            MICRO_DevolveCaracMicro = BL_HandleNull(rsMicro!cod_carac_micro, "")
            rsMicro.Close
            Set rsMicro = Nothing
            Exit Function
        End If
    End If
    rsMicro.Close
    Set rsMicro = Nothing
    
    For i = 1 To gTotalCaracMicro
        If gCaracMicro(i).flg_defeito = mediSim Then
            MICRO_DevolveCaracMicro = gCaracMicro(i).cod_carac_micro
            Exit Function
        End If
    Next i
Exit Function
TrataErro:
        MICRO_DevolveCaracMicro = ""
        BG_LogFile_Erros "MICRO_DevolveCaracMicro: " & Err.Number & " - " & Err.Description, "MICRO", "MICRO_DevolveCaracMicro"
        Exit Function
        Resume Next
End Function

' -------------------------------------------------------------------------------------

' verifica se caracteriza��o do microrganismo permite vigilancia epid

' -------------------------------------------------------------------------------------
Public Function MICRO_VerificaCaracMicroVE(cod_carac_micro As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    If cod_carac_micro = "" Then
        MICRO_VerificaCaracMicroVE = True
        Exit Function
    End If
    For i = 1 To gTotalCaracMicro
        If gCaracMicro(i).cod_carac_micro = cod_carac_micro Then
            If gCaracMicro(i).flg_ve = mediSim Then
                MICRO_VerificaCaracMicroVE = True
            Else
                MICRO_VerificaCaracMicroVE = False
            End If
            Exit Function
        End If
    Next i
Exit Function
TrataErro:
        MICRO_VerificaCaracMicroVE = False
        BG_LogFile_Erros "MICRO_VerificaCaracMicroVE: " & Err.Number & " - " & Err.Description, "MICRO", "MICRO_VerificaCaracMicroVE"
        Exit Function
        Resume Next
End Function

' -------------------------------------------------------------------------------------

' PARA UM UTENTE, MICRORGANISMO E RESULTADOS DE ANTIBIOTICOS AVALIA SE � DUPLICADO

' -------------------------------------------------------------------------------------
Public Function MICRO_VerificaDuplicados(seq_utente As String, n_req As Long, seq_realiza As Long, cod_micro As String, _
                                         dt_chega As String, hr_chega As String, antib() As ResAntib, cod_produto As String) As Boolean
    On Error GoTo TrataErro
    Dim microrg() As ResultadoMicrorg
    Dim totalMicro As Integer
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    Dim data As String
    
    Dim rsMicro As New ADODB.recordset
    
    ' INICIALIZA A ESTRUTURA MICRORGANISMOS
    totalMicro = 1
    ReDim microrg(totalMicro)
    microrg(totalMicro).cod_micro = cod_micro
    microrg(totalMicro).n_req = n_req
    microrg(totalMicro).dt_chega = dt_chega
    microrg(totalMicro).seq_realiza = seq_realiza
    microrg(totalMicro).seq_utente = seq_utente
    microrg(totalMicro).antibioticos = antib
    microrg(totalMicro).totalAntib = UBound(antib)
    If microrg(totalMicro).totalAntib = 0 Then
        MICRO_PreencheAntibioticos microrg, totalMicro
    End If
    If cod_produto <> "" Then
        sSql = "SELECT * FROM sl_produto WHERE cod_produto = " & BL_TrataStringParaBD(cod_produto)
        rsMicro.CursorLocation = adUseClient
        rsMicro.CursorType = adOpenKeyset
        rsMicro.LockType = adLockOptimistic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsMicro.Open sSql, gConexao
        If rsMicro.RecordCount = 1 Then
            microrg(totalMicro).cod_produto = BL_HandleNull(rsMicro!cod_produto, "")
            microrg(totalMicro).prazo_ve = BL_HandleNull(rsMicro!prazo_ve, 0)
        End If
        rsMicro.Close
        Set rsMicro = Nothing
    End If
    
    
    
    If gLAB = "CHVNG" Then
        data = "01-01-" & Year(dt_chega)
        If CDate(data) <= CDate(dt_chega) - CDate(gIntervaloTempoRepetidosMicro) Then
            data = CDate(dt_chega) - CDate(gIntervaloTempoRepetidosMicro)
        End If
    Else
        data = CDate(dt_chega) - CDate(gIntervaloTempoRepetidosMicro)
    End If
    sSql = "SELECT x1.cod_micro, x1.seq_realiza, x1.quantif, x1.flg_imp, x1.flg_tsq, x1.cod_gr_antibio, x1.prova, x1.flg_testes, "
    sSql = sSql & " x2.cod_antib, x2.res_sensib, x3.dt_chega, x3.n_Req, x3.seq_utente, x3.hr_chega, "
    sSql = sSql & " x5.cod_produto, x5.prazo_ve "
    sSql = sSql & " FROM sl_requis x3, sl_realiza x0, sl_res_micro x1 LEFT OUTER JOIN sl_res_tsq x2 "
    sSql = sSql & " ON x1.seq_realiza = x2.seq_realiza AND x1.cod_micro = x2.cod_micro, "
    sSql = sSql & " slv_analises_apenas x4 LEFT OUTER JOIN sl_produto x5 on x4.cod_produto = x5.cod_produto "
    sSql = sSql & " WHERE x0.n_req = x3.n_Req AND x0.seq_realiza = x1.seq_realiza "
    sSql = sSql & " AND x0.seq_utente = " & seq_utente
    sSql = sSql & " AND x0.seq_realiza <> " & seq_realiza
    sSql = sSql & " AND x1.cod_micro = " & BL_TrataStringParaBD(cod_micro)
    sSql = sSql & " AND (x1.cod_carac_micro <> 2  or X1.COD_CARAC_MICRO is null) "
    sSql = sSql & " AND x3.dt_chega between " & BL_TrataDataParaBD(data)
    sSql = sSql & " AND " & BL_TrataDataParaBD(dt_chega)
    sSql = sSql & " AND x0.cod_Agrup = x4.cod_ana "
    sSql = sSql & " AND x0.flg_estado IN (" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica) & "," & BL_TrataStringParaBD(gEstadoAnaImpressa) & ") "
    sSql = sSql & " ORDER by x1.seq_realiza, x1.cod_micro, x2.cod_antib "
    
    rsAntib.CursorLocation = adUseClient
    rsAntib.CursorType = adOpenKeyset
    rsAntib.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAntib.Open sSql, gConexao
    If rsAntib.RecordCount >= 1 Then
        If microrg(totalMicro).totalAntib = 0 Then
            MICRO_VerificaDuplicados = True
        Else
            While Not rsAntib.EOF
                If (CDate(dt_chega) = CDate(BL_HandleNull(rsAntib!dt_chega))) Or CDate(dt_chega) > CDate(BL_HandleNull(rsAntib!dt_chega, "")) Then
                    If microrg(totalMicro).seq_realiza <> BL_HandleNull(rsAntib!seq_realiza, mediComboValorNull) Then
                        totalMicro = totalMicro + 1
                        ReDim Preserve microrg(totalMicro)
                        microrg(totalMicro).totalAntib = 0
                        ReDim microrg(totalMicro).antibioticos(0)
                        microrg(totalMicro).Cod_Gr_Antibio = BL_HandleNull(rsAntib!Cod_Gr_Antibio, "")
                        microrg(totalMicro).cod_micro = BL_HandleNull(rsAntib!cod_micro, "")
                        microrg(totalMicro).cod_produto = BL_HandleNull(rsAntib!cod_produto, "")
                        microrg(totalMicro).prazo_ve = BL_HandleNull(rsAntib!prazo_ve, 0)
                        microrg(totalMicro).dt_chega = BL_HandleNull(rsAntib!dt_chega, "")
                        microrg(totalMicro).n_req = BL_HandleNull(rsAntib!n_req, "")
                        microrg(totalMicro).Prova = BL_HandleNull(rsAntib!Prova, "")
                        microrg(totalMicro).seq_realiza = BL_HandleNull(rsAntib!seq_realiza, mediComboValorNull)
                        microrg(totalMicro).seq_utente = BL_HandleNull(rsAntib!seq_utente, mediComboValorNull)
                        microrg(totalMicro).errosMajor = 0
                        microrg(totalMicro).errosMinor = 0
                    End If
                    If BL_HandleNull(rsAntib!cod_antib, "") <> "" Then
                        microrg(totalMicro).totalAntib = microrg(totalMicro).totalAntib + 1
                        ReDim Preserve microrg(totalMicro).antibioticos(microrg(totalMicro).totalAntib)
                        microrg(totalMicro).antibioticos(microrg(totalMicro).totalAntib).codAntib = BL_HandleNull(rsAntib!cod_antib, "")
                        microrg(totalMicro).antibioticos(microrg(totalMicro).totalAntib).sensibilidade = BL_HandleNull(rsAntib!res_sensib, "")
                    End If
                End If
                rsAntib.MoveNext
            Wend
            MICRO_VerificaDuplicados = MICRO_ValidaAlgoritmo(microrg, totalMicro)
        End If
    End If
    rsAntib.Close
    Set rsAntib = Nothing
Exit Function
TrataErro:
        MICRO_VerificaDuplicados = False
        BG_LogFile_Erros "MICRO_VerificaDuplicados: " & Err.Number & " - " & Err.Description, "MICRO", "MICRO_VerificaDuplicados"
        Exit Function
        Resume Next
End Function

Private Function MICRO_ValidaAlgoritmo(microrg() As ResultadoMicrorg, totalMicro As Integer)
    On Error GoTo TrataErro
    Dim iAntib As Integer
    Dim iAntib2 As Integer
    Dim iMicro As Integer
    Dim flg_existe As Boolean
    
    MICRO_ValidaAlgoritmo = False
    

    
    For iMicro = 2 To totalMicro
        microrg(iMicro).errosMajor = 0
        microrg(iMicro).errosMinor = 0
        If microrg(1).totalAntib <= 0 And microrg(iMicro).totalAntib > 0 Then
            MICRO_ValidaAlgoritmo = True
            Exit Function
        End If
        ' VERIFICA REGRA PARA PRODUTO SANGUE. REQUIS COM PRODUTOS SNAGUE APENAS PODE SER COMPARADO COM MESMO PRODUTO NUM PERIODO DE ESPACO DE TEMPO.
        ' FORA DESSE PERIODO PODE SER COMPARADO COM QUALQUER PRODUTO
        If microrg(iMicro).cod_produto <> microrg(1).cod_produto And microrg(1).prazo_ve > 0 And CDate(microrg(1).dt_chega) <= CDate(DateAdd("d", microrg(1).prazo_ve, microrg(iMicro).dt_chega)) Then
            microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
        End If
        ' SE NAO TEM ANTIBIOTICOS INCREMENTA O NUMERO DE ERROS MAJOR
        If microrg(iMicro).totalAntib = 0 Then
            microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
        End If
        microrg(iMicro).antibioticosComuns = 0
        ' PARA CADA ANTIBIOTICO
        For iAntib = 1 To microrg(1).totalAntib
            flg_existe = False
            ' PROCURA NOUTRAS REQUISICOES O MESMO ANTIBIOTICO
            For iAntib2 = 1 To microrg(iMicro).totalAntib
                If microrg(1).antibioticos(iAntib).codAntib = microrg(iMicro).antibioticos(iAntib2).codAntib Then
                    microrg(iMicro).antibioticosComuns = microrg(iMicro).antibioticosComuns + 1
                    flg_existe = True
                    'AVALIA ERROS MINOR E MAJOR
                    Select Case microrg(1).antibioticos(iAntib).sensibilidade
                        Case "R"
                            Select Case microrg(iMicro).antibioticos(iAntib2).sensibilidade
                                Case "I"
                                    microrg(iMicro).errosMinor = microrg(iMicro).errosMinor + 1
                                Case "P", "N", "S"
                                    microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
                            End Select
                        Case "S"
                            Select Case microrg(iMicro).antibioticos(iAntib2).sensibilidade
                                Case "I"
                                    microrg(iMicro).errosMinor = microrg(iMicro).errosMinor + 1
                                Case "P", "N", "R"
                                    microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
                            End Select
                        Case "I"
                            Select Case microrg(iMicro).antibioticos(iAntib2).sensibilidade
                                Case "R", "S"
                                    microrg(iMicro).errosMinor = microrg(iMicro).errosMinor + 1
                                Case "P", "N"
                                    microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
                            End Select
                        Case "P"
                            Select Case microrg(iMicro).antibioticos(iAntib2).sensibilidade
                                Case "R", "S", "I", "N"
                                    microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
                            End Select
                        Case "N"
                            Select Case microrg(iMicro).antibioticos(iAntib2).sensibilidade
                                Case "R", "S", "I", "P"
                                    microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
                            End Select
                    End Select
                End If
            Next iAntib2
            
            'DR PAULO DIZ QUE SE NAO ENCONTROU CONSIDERA DUPLICADO NA MESMA REQ 3439936
            'If flg_existe = False Then
            '     microrg(iMicro).errosMajor = microrg(iMicro).errosMajor + 1
            'End If
            
        Next iAntib
        ' SE ENCONTROU UMA REQUISI��O/MICRORGANISMO QUE � CONSIDERADO DUPLICADO SAI FORA..
        If microrg(iMicro).errosMajor = 0 And microrg(iMicro).errosMinor <= gNumeroErrosMinorMicro And microrg(iMicro).antibioticosComuns >= 1 Then
            MICRO_ValidaAlgoritmo = True
            Exit Function
        End If
    Next iMicro
    
Exit Function
TrataErro:
        MICRO_ValidaAlgoritmo = False
        BG_LogFile_Erros "MICRO_ValidaAlgoritmo: " & Err.Number & " - " & Err.Description, "MICRO", "MICRO_ValidaAlgoritmo"
        Exit Function
        Resume Next
End Function

Public Sub MICRO_retroativos()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    Dim antib() As ResAntib
    Dim i As Integer
    Dim conta As Long
    ReDim antib(0)
    
    sSql = "SELECT x0.n_req, x0.dt_chega, x0.hr_chega, x1.seq_realiza, x0.seq_utente, x2.cod_micro, x3.cod_produto "
    sSql = sSql & " FROM sl_requis x0, sl_Realiza x1, sl_res_micro x2, slv_analises_apenas x3 "
    sSql = sSql & " WHERE x0.n_req = x1.n_req AND x1.seq_realiza = x2.seq_realiza AND x0.dt_chega is not null "
    sSql = sSql & " AND x0.dt_chega between '01-01-2014' and '30-07-2014' and (x2.cod_carac_micro IS NULL OR x2.cod_carac_micro <>2) "
    sSql = sSql & " AND x3.cod_ana = x1.cod_agrup  ORDER BY SEQ_REALIZA "
    rsAntib.CursorLocation = adUseClient
    rsAntib.CursorType = adOpenKeyset
    rsAntib.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAntib.Open sSql, gConexao
    If rsAntib.RecordCount >= 1 Then
        While Not rsAntib.EOF
            conta = conta + 1
            If MICRO_VerificaDuplicados(BL_HandleNull(rsAntib!seq_utente, mediComboValorNull), BL_HandleNull(rsAntib!n_req, mediComboValorNull), _
                     BL_HandleNull(rsAntib!seq_realiza, mediComboValorNull), BL_HandleNull(rsAntib!cod_micro, ""), BL_HandleNull(rsAntib!dt_chega, ""), _
                     BL_HandleNull(rsAntib!hr_chega, ""), antib, BL_HandleNull(rsAntib!cod_produto, "")) = True Then
                    
                    sSql = "UPDATE sl_res_micro SET cod_carac_micro = 2 WHERE seq_realiza = " & BL_HandleNull(rsAntib!seq_realiza, mediComboValorNull)
                    sSql = sSql & " AND cod_micro = " & BL_TrataStringParaBD(BL_HandleNull(rsAntib!cod_micro, mediComboValorNull))
                    BG_ExecutaQuery_ADO sSql
            End If
            rsAntib.MoveNext
            'i = i + 1
            
        Wend
    End If
    rsAntib.Close
    Set rsAntib = Nothing
Exit Sub
TrataErro:
        BG_LogFile_Erros "MICRO_retroativos: " & Err.Number & " - " & Err.Description, "MICRO", "MICRO_retroativos"
        Exit Sub
        Resume Next
End Sub

Private Sub MICRO_PreencheAntibioticos(ByRef microrg() As ResultadoMicrorg, ByRef totalMicro As Integer)
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    
    microrg(totalMicro).totalAntib = 0
    ReDim Preserve microrg(totalMicro).antibioticos(microrg(totalMicro).totalAntib)
    
    If microrg(totalMicro).seq_realiza <= 0 Then
        Exit Sub
    End If
    
    sSql = "SELECT COD_ANTIB, res_sensib FROM sl_Res_tsq WHERE Seq_realiza =  " & microrg(totalMicro).seq_realiza
    sSql = sSql & " AND cod_micro = " & BL_TrataStringParaBD(microrg(totalMicro).cod_micro)
    sSql = sSql & " UNION SELECT COD_ANTIB, res_sensib FROM sl_Res_tsq_h WHERE Seq_realiza =  " & microrg(totalMicro).seq_realiza
    sSql = sSql & " AND cod_micro = " & BL_TrataStringParaBD(microrg(totalMicro).cod_micro)
    
    rsAntib.CursorLocation = adUseClient
    rsAntib.CursorType = adOpenKeyset
    rsAntib.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAntib.Open sSql, gConexao
    If rsAntib.RecordCount >= 1 Then
        While Not rsAntib.EOF
            microrg(totalMicro).totalAntib = microrg(totalMicro).totalAntib + 1
            ReDim Preserve microrg(totalMicro).antibioticos(microrg(totalMicro).totalAntib)
            microrg(totalMicro).antibioticos(microrg(totalMicro).totalAntib).codAntib = BL_HandleNull(rsAntib!cod_antib, "")
            microrg(totalMicro).antibioticos(microrg(totalMicro).totalAntib).sensibilidade = BL_HandleNull(rsAntib!res_sensib, "")
            rsAntib.MoveNext
        Wend
    End If
    rsAntib.Close
    Set rsAntib = Nothing
End Sub




VERSION 5.00
Begin VB.Form FormIntroducaoSala 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Sala"
   ClientHeight    =   750
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   7950
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   750
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   240
      Picture         =   "FormIntroducaoSala.frx":0000
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   4
      Top             =   70
      Width           =   495
   End
   Begin VB.CommandButton ButtonCancelar 
      Height          =   315
      Left            =   6720
      Picture         =   "FormIntroducaoSala.frx":08CA
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   240
      Width           =   1095
   End
   Begin VB.CommandButton ButtonOK 
      Default         =   -1  'True
      Height          =   315
      Left            =   5520
      Picture         =   "FormIntroducaoSala.frx":0C54
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   240
      Width           =   1095
   End
   Begin VB.ComboBox CbSala 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   240
      Width           =   3975
   End
   Begin VB.Label Label1 
      Caption         =   "Sala"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   3
      Top             =   240
      Width           =   495
   End
End
Attribute VB_Name = "FormIntroducaoSala"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''
' Actualiza��o: 07/03/2007 '''''''''''''''''
' T�cnico: Paulo Ferreira ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''
' Variaveis globais para este form '''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim bContinue As Boolean
Dim Descricao As String

''''''''''''''''''''''''''''''''''''''''''''
' Evento load do form ''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes
    PreencheValoresDefeito
    ' Define acesso a controlos dependendo do utilizador
    If (gCodGrupo = cTecnico And CbSala.ListCount <> 0) Then: ButtonCancelar.Enabled = False
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento unload do form ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoUnload()

    If (bContinue = True) Then
    
        ' Extrai codigo da sala
        gCodSala = BG_DaComboSel(CbSala)
        
    End If
    
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Inicializacoes do form '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Inicializacoes()
    
    Me.Width = 8040
    Me.Height = 1110
    Me.caption = " Sala de " & BG_SYS_GetComputerName
    Me.left = 3000
    Me.top = 3000
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Preenche valores de defeito do form ''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub PreencheValoresDefeito()
    Dim sSql As String
    bContinue = False
    sSql = "SELECT * FROM sl_cod_salas WHERE flg_colheita = 1"

    BG_PreencheComboBD_ADO sSql, "seq_sala", "descr_sala", CbSala, mediAscComboDesignacao
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Botao cancelar '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub ButtonCancelar_Click()
    gCodSala = -1
    BG_Mensagem mediMsgBox, "Dever� definir uma sala!", vbExclamation, " Aten��o"
    Unload Me
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Botao ok '''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub ButtonOK_Click()

    If (CbSala.ListIndex <> -1) Then
        gCodSala = 1
        bContinue = True
        Unload Me
    Else
        MsgBox "Ter� de indicar uma sala.", vbInformation
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Load do form '''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Load()
    EventoLoad
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Unload do form '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

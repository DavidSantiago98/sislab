VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormGestaoRequisicaoAguas 
   BackColor       =   &H80000004&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormGestaoRequisicaoAguas"
   ClientHeight    =   8235
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13185
   Icon            =   "FormGestaoRequisicaoAguas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8235
   ScaleWidth      =   13185
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.TextBox EcNumColheita 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   315
      Left            =   7800
      Locked          =   -1  'True
      TabIndex        =   218
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox EcDataPrevista 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8640
      TabIndex        =   214
      Top             =   10140
      Width           =   1335
   End
   Begin VB.TextBox EcFlgDiagSec 
      Height          =   285
      Left            =   3480
      TabIndex        =   200
      Top             =   9840
      Width           =   375
   End
   Begin RichTextLib.RichTextBox EcInfo 
      Height          =   375
      Left            =   4080
      TabIndex        =   199
      Top             =   9000
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"FormGestaoRequisicaoAguas.frx":000C
   End
   Begin VB.TextBox EcDescrEstado 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   198
      Top             =   120
      Width           =   3255
   End
   Begin VB.PictureBox PicCredencial 
      Height          =   735
      Left            =   960
      ScaleHeight     =   675
      ScaleWidth      =   795
      TabIndex        =   197
      Top             =   9000
      Width           =   855
   End
   Begin VB.TextBox EcHemodialise 
      Height          =   285
      Left            =   3360
      TabIndex        =   190
      Top             =   10440
      Width           =   975
   End
   Begin VB.TextBox EcAbrevUte 
      Height          =   285
      Left            =   7680
      TabIndex        =   185
      Top             =   10560
      Width           =   1335
   End
   Begin VB.CommandButton BtConfirm 
      Height          =   375
      Left            =   5640
      Picture         =   "FormGestaoRequisicaoAguas.frx":0097
      Style           =   1  'Graphical
      TabIndex        =   179
      ToolTipText     =   "Confirmar Requisi��o"
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcSMSEnviada 
      Height          =   285
      Left            =   1920
      TabIndex        =   175
      Top             =   10560
      Width           =   375
   End
   Begin VB.TextBox EcAuxRec 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   11400
      TabIndex        =   166
      Top             =   13080
      Width           =   375
   End
   Begin VB.TextBox EcFlgFacturado 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   163
      Top             =   12480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton BtCancelarReq 
      Height          =   315
      Left            =   5520
      Picture         =   "FormGestaoRequisicaoAguas.frx":0421
      Style           =   1  'Graphical
      TabIndex        =   107
      TabStop         =   0   'False
      ToolTipText     =   "Raz�o cancelamento"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcEtqNAdm 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   147
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNTubos 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   146
      Top             =   11640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   145
      Top             =   10920
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqTipo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   144
      Top             =   12120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNEsp 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   143
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNCopias 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   142
      Top             =   11160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcFimSemana 
      Height          =   285
      Left            =   7680
      TabIndex        =   140
      Top             =   11040
      Width           =   375
   End
   Begin VB.ComboBox CbSala 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6960
      Style           =   2  'Dropdown List
      TabIndex        =   135
      Top             =   13440
      Width           =   1335
   End
   Begin VB.TextBox EcPrinterRecibo 
      Height          =   285
      Left            =   3960
      TabIndex        =   127
      Top             =   13320
      Width           =   1215
   End
   Begin VB.TextBox EcObsReq 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1560
      MultiLine       =   -1  'True
      TabIndex        =   125
      Top             =   13320
      Width           =   1215
   End
   Begin VB.TextBox EcPesqRapMedico 
      Height          =   285
      Left            =   1920
      TabIndex        =   120
      Top             =   12120
      Width           =   375
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   5760
      TabIndex        =   113
      Top             =   11760
      Width           =   375
   End
   Begin VB.TextBox EcDescrEfr2 
      Height          =   285
      Left            =   5760
      TabIndex        =   111
      Top             =   11400
      Width           =   375
   End
   Begin VB.TextBox EcCodEfr2 
      Height          =   285
      Left            =   5760
      TabIndex        =   108
      Top             =   11040
      Width           =   375
   End
   Begin VB.TextBox EcNumReq 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   1320
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
   Begin VB.ComboBox CbEstadoReq 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   315
      Left            =   2400
      Style           =   1  'Simple Combo
      TabIndex        =   105
      TabStop         =   0   'False
      Top             =   10200
      Width           =   3135
   End
   Begin VB.TextBox EcUrgencia 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   88
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPagUte 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   87
      Top             =   11640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEstadoReq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   86
      Top             =   11160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapProven 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   85
      Top             =   11040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CbEstadoReqAux 
      Height          =   315
      Left            =   7080
      Style           =   2  'Dropdown List
      TabIndex        =   84
      TabStop         =   0   'False
      Top             =   11760
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Timer TimerRS 
      Interval        =   60000
      Left            =   0
      Top             =   8880
   End
   Begin VB.TextBox EcSeqUtente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   83
      Top             =   11400
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.TextBox EcPesqRapProduto 
      Enabled         =   0   'False
      Height          =   285
      Left            =   8280
      TabIndex        =   82
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox CodAnaTemp 
      Height          =   285
      Left            =   3960
      TabIndex        =   81
      Top             =   11520
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodIsencao 
      Height          =   285
      Left            =   3960
      TabIndex        =   80
      Top             =   11280
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcAuxProd 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   79
      Top             =   12480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcAuxAna 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      MaxLength       =   20
      TabIndex        =   78
      Top             =   12480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcTEFR 
      BackColor       =   &H00EDFAED&
      Enabled         =   0   'False
      Height          =   285
      Left            =   6240
      TabIndex        =   77
      Top             =   12480
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcImprimirResumo 
      Height          =   285
      Left            =   8520
      TabIndex        =   76
      Top             =   12120
      Width           =   975
   End
   Begin VB.TextBox EcPrinterResumo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4560
      TabIndex        =   75
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   8520
      TabIndex        =   74
      Top             =   12480
      Width           =   1095
   End
   Begin VB.TextBox EcGrupoAna 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   73
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcHoraChegada 
      Height          =   285
      Left            =   3960
      Locked          =   -1  'True
      TabIndex        =   72
      Top             =   12840
      Width           =   975
   End
   Begin VB.TextBox EcAuxTubo 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   71
      Top             =   12840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcPesqRapTubo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9000
      TabIndex        =   70
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcinfComplementar 
      Height          =   285
      Left            =   6240
      TabIndex        =   69
      Top             =   12840
      Width           =   735
   End
   Begin VB.TextBox EcPrColheita 
      Height          =   285
      Left            =   7920
      TabIndex        =   68
      Top             =   12600
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   50
      Top             =   360
      Width           =   12975
      Begin VB.TextBox EcSexo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10800
         TabIndex        =   169
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   1935
      End
      Begin VB.TextBox EcIdade 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   11880
         TabIndex        =   5
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   600
         Width           =   4335
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   2
         Top             =   240
         Width           =   3255
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10800
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   240
         Width           =   1095
      End
      Begin VB.ComboBox CbTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   28
         Left            =   9480
         TabIndex        =   168
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   375
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   53
         Top             =   600
         Width           =   405
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   52
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   9480
         TabIndex        =   51
         ToolTipText     =   "Data de Nascimento"
         Top             =   240
         Width           =   795
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Registo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1695
      Left            =   120
      TabIndex        =   21
      Top             =   6360
      Width           =   12975
      Begin VB.TextBox EcUtilConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   182
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox EcHrConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   181
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox EcDtConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   180
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   37
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcDataCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcHoraCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   35
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox EcDataImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10080
         Locked          =   -1  'True
         TabIndex        =   34
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcHoraImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcDataImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10080
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcHoraImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   29
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcHoraAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcDataAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcDataFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   25
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcHoraFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   24
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox EcUtilizadorFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcLocal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   960
         Width           =   615
      End
      Begin VB.Label EcUtilNomeConf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   184
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Confirma��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   30
         Left            =   120
         TabIndex        =   183
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Cria��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   49
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label16 
         Caption         =   "Altera��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Impress�o"
         Height          =   255
         Index           =   42
         Left            =   6720
         TabIndex        =   47
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "�ltima 2� Via"
         Height          =   255
         Index           =   41
         Left            =   6720
         TabIndex        =   46
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label LbNomeUtilAlteracao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   45
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label LbNomeUtilCriacao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   44
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label LbNomeUtilImpressao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8520
         TabIndex        =   43
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label LbNomeUtilImpressao2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8520
         TabIndex        =   42
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label LbNomeUtilFecho 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   41
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "Fecho"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   960
         Width           =   855
      End
      Begin VB.Label LbNomeLocal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8520
         TabIndex        =   39
         Top             =   960
         Width           =   3495
      End
      Begin VB.Label Label1 
         Caption         =   "Local Requis."
         Height          =   255
         Index           =   39
         Left            =   6720
         TabIndex        =   38
         Top             =   960
         Width           =   1095
      End
   End
   Begin TabDlg.SSTab SSTGestReq 
      Height          =   4935
      Left            =   120
      TabIndex        =   54
      Top             =   1440
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   8705
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Dados da Requisi��o"
      TabPicture(0)   =   "FormGestaoRequisicaoAguas.frx":0AAB
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(5)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label10"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1(9)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label1(2)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label1(7)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label1(3)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label1(6)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label1(8)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label1(10)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label1(11)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label1(12)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label1(13)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label1(17)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "EcDescrEFR"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "CbUrgencia"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "BtPesquisaEntFin"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "EcCodEFR"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "CbDestino"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Frame3"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Frame5"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Frame7"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "EcCodSala"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "BtPesquisaSala"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Frame9"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Frame10"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "EcDtEntrega"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "BtPesquisaTipoAmostra"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "EcDescrTipoAmostra"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "EcCodtipoAmostra"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "BtPesquisaOrigemAmostra"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "EcDescrOrigemAmostra"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "EcCodOrigemAmostra"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "EcDescrSala"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "BtPesquisaPontoColheita"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "EcDescrPontoColheita"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "EcCodPontoColheita"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "EcDtColheita"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "EcLocalColheita"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "EcRespColheita"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "EcObs"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "EcDataChegada"
      Tab(0).Control(40).Enabled=   0   'False
      Tab(0).ControlCount=   41
      TabCaption(1)   =   "&An�lises"
      TabPicture(1)   =   "FormGestaoRequisicaoAguas.frx":0AC7
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FrAnalises"
      Tab(1).Control(1)=   "ListaDiagnSec"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Pro&dutos"
      TabPicture(2)   =   "FormGestaoRequisicaoAguas.frx":0AE3
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FrBtProd"
      Tab(2).Control(1)=   "FrProdutos"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Tu&bos"
      TabPicture(3)   =   "FormGestaoRequisicaoAguas.frx":0AFF
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FrBtTubos"
      Tab(3).Control(1)=   "FGTubos"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Fatura Recib&o"
      TabPicture(4)   =   "FormGestaoRequisicaoAguas.frx":0B1B
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FrameAdiantamentos"
      Tab(4).Control(1)=   "FrameAnaRecibos"
      Tab(4).Control(2)=   "BtAdiantamentos"
      Tab(4).Control(3)=   "BtPagamento"
      Tab(4).Control(4)=   "BtCobranca"
      Tab(4).Control(5)=   "BtGerarRecibo"
      Tab(4).Control(6)=   "BtAnularRecibo"
      Tab(4).Control(7)=   "BtImprimirRecibo"
      Tab(4).Control(8)=   "FGRecibos"
      Tab(4).ControlCount=   9
      Begin VB.TextBox EcDataChegada 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4440
         TabIndex        =   217
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcObs 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         TabIndex        =   20
         Top             =   4440
         Width           =   9975
      End
      Begin VB.TextBox EcRespColheita 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7800
         TabIndex        =   19
         Top             =   3840
         Width           =   3495
      End
      Begin VB.TextBox EcLocalColheita 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7800
         TabIndex        =   18
         Top             =   3360
         Width           =   3495
      End
      Begin VB.TextBox EcDtColheita 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   208
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcCodPontoColheita 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   16
         Top             =   3840
         Width           =   735
      End
      Begin VB.TextBox EcDescrPontoColheita 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   206
         TabStop         =   0   'False
         Top             =   3840
         Width           =   3375
      End
      Begin VB.CommandButton BtPesquisaPontoColheita 
         Height          =   315
         Left            =   5400
         Picture         =   "FormGestaoRequisicaoAguas.frx":0B37
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   3840
         Width           =   375
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   138
         TabStop         =   0   'False
         Top             =   1920
         Width           =   3375
      End
      Begin VB.TextBox EcCodOrigemAmostra 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   14
         Top             =   3360
         Width           =   735
      End
      Begin VB.TextBox EcDescrOrigemAmostra 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   204
         TabStop         =   0   'False
         Top             =   3360
         Width           =   3375
      End
      Begin VB.CommandButton BtPesquisaOrigemAmostra 
         Height          =   315
         Left            =   5400
         Picture         =   "FormGestaoRequisicaoAguas.frx":10C1
         Style           =   1  'Graphical
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   3360
         Width           =   375
      End
      Begin VB.TextBox EcCodtipoAmostra 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   12
         Top             =   2880
         Width           =   735
      End
      Begin VB.TextBox EcDescrTipoAmostra 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   202
         TabStop         =   0   'False
         Top             =   2880
         Width           =   3375
      End
      Begin VB.CommandButton BtPesquisaTipoAmostra 
         Height          =   315
         Left            =   5400
         Picture         =   "FormGestaoRequisicaoAguas.frx":164B
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   2880
         Width           =   375
      End
      Begin VB.Frame FrameAdiantamentos 
         Caption         =   "Adiantamentos"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   186
         Top             =   360
         Width           =   12735
         Begin VB.CommandButton BtSairAdiantamento 
            Height          =   495
            Left            =   10320
            Picture         =   "FormGestaoRequisicaoAguas.frx":1BD5
            Style           =   1  'Graphical
            TabIndex        =   192
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtImprimirAdiantamento 
            Height          =   495
            Left            =   12000
            Picture         =   "FormGestaoRequisicaoAguas.frx":289F
            Style           =   1  'Graphical
            TabIndex        =   189
            ToolTipText     =   "ReImprimir Factura Recibo"
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtAnularAdiamento 
            BackColor       =   &H80000004&
            Height          =   495
            Left            =   11160
            Picture         =   "FormGestaoRequisicaoAguas.frx":3569
            Style           =   1  'Graphical
            TabIndex        =   188
            ToolTipText     =   "Devolver Adiantamento"
            Top             =   3840
            Width           =   615
         End
         Begin MSFlexGridLib.MSFlexGrid FgAdiantamentos 
            Height          =   2655
            Left            =   120
            TabIndex        =   187
            Top             =   360
            Width           =   8775
            _ExtentX        =   15478
            _ExtentY        =   4683
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            Appearance      =   0
         End
      End
      Begin VB.Frame FrameAnaRecibos 
         Caption         =   "An�lises"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   157
         Top             =   360
         Width           =   12735
         Begin VB.CommandButton BtAnaAcresc 
            Height          =   375
            Left            =   5880
            Picture         =   "FormGestaoRequisicaoAguas.frx":4233
            Style           =   1  'Graphical
            TabIndex        =   196
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox EcQtdAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2160
            TabIndex        =   162
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcTaxaAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3000
            Locked          =   -1  'True
            TabIndex        =   195
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcDescrAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1200
            Locked          =   -1  'True
            TabIndex        =   194
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcCodAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   240
            TabIndex        =   161
            Top             =   720
            Width           =   735
         End
         Begin VB.CommandButton BtFgAnaRecRecalcula 
            Height          =   495
            Left            =   10560
            Picture         =   "FormGestaoRequisicaoAguas.frx":45BD
            Style           =   1  'Graphical
            TabIndex        =   160
            ToolTipText     =   "Recalcula Pre�o Factura Recibo"
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtFgAnaRecSair 
            Height          =   495
            Left            =   11280
            Picture         =   "FormGestaoRequisicaoAguas.frx":5287
            Style           =   1  'Graphical
            TabIndex        =   159
            Top             =   3840
            Width           =   615
         End
         Begin MSFlexGridLib.MSFlexGrid FgAnaRec 
            Height          =   2655
            Left            =   240
            TabIndex        =   158
            Top             =   1080
            Width           =   6615
            _ExtentX        =   11668
            _ExtentY        =   4683
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
         End
      End
      Begin VB.CommandButton BtAdiantamentos 
         BackColor       =   &H80000004&
         Height          =   495
         Left            =   -69000
         Picture         =   "FormGestaoRequisicaoAguas.frx":5F51
         Style           =   1  'Graphical
         TabIndex        =   193
         ToolTipText     =   "Adiantamentos Emitidos"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtPagamento 
         Height          =   495
         Left            =   -67200
         Picture         =   "FormGestaoRequisicaoAguas.frx":6C1B
         Style           =   1  'Graphical
         TabIndex        =   119
         ToolTipText     =   "Colocar em Pagamento"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtCobranca 
         Height          =   495
         Left            =   -67920
         Picture         =   "FormGestaoRequisicaoAguas.frx":78E5
         Style           =   1  'Graphical
         TabIndex        =   118
         ToolTipText     =   "Colocar em Cobran�a"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtGerarRecibo 
         Height          =   495
         Left            =   -65640
         Picture         =   "FormGestaoRequisicaoAguas.frx":85AF
         Style           =   1  'Graphical
         TabIndex        =   117
         ToolTipText     =   "Emitir Factura Recibo"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtAnularRecibo 
         Height          =   495
         Left            =   -64920
         Picture         =   "FormGestaoRequisicaoAguas.frx":9279
         Style           =   1  'Graphical
         TabIndex        =   116
         ToolTipText     =   "Cancelar Factura Recibo"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtImprimirRecibo 
         Height          =   495
         Left            =   -64200
         Picture         =   "FormGestaoRequisicaoAguas.frx":9F43
         Style           =   1  'Graphical
         TabIndex        =   115
         ToolTipText     =   "ReImprimir Factura Recibo"
         Top             =   4320
         Width           =   615
      End
      Begin VB.TextBox EcDtEntrega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   177
         Top             =   900
         Width           =   1335
      End
      Begin VB.Frame Frame10 
         Height          =   600
         Left            =   11520
         TabIndex        =   170
         ToolTipText     =   "Resumo"
         Top             =   960
         Width           =   1335
         Begin VB.CommandButton BtResumo 
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoAguas.frx":AC0D
            Style           =   1  'Graphical
            TabIndex        =   171
            ToolTipText     =   "Folha Resumo"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label6 
            Caption         =   "Resumo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   172
            ToolTipText     =   "Folha Resumo"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame9 
         Height          =   600
         Left            =   11520
         TabIndex        =   154
         ToolTipText     =   "Identifica��o"
         Top             =   360
         Width           =   1335
         Begin VB.CommandButton BtIdentif 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoAguas.frx":B377
            Style           =   1  'Graphical
            TabIndex        =   155
            ToolTipText     =   "Identifica��o"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Identif."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   156
            ToolTipText     =   "Identifica��o"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   5400
         Picture         =   "FormGestaoRequisicaoAguas.frx":C041
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   1920
         Width           =   375
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   8
         Top             =   1920
         Width           =   735
      End
      Begin VB.Frame Frame7 
         Height          =   600
         Left            =   11520
         TabIndex        =   132
         ToolTipText     =   "Consulta de Requisi��es"
         Top             =   1560
         Width           =   1335
         Begin VB.CommandButton BtConsReq 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoAguas.frx":C5CB
            Style           =   1  'Graphical
            TabIndex        =   133
            ToolTipText     =   "Consulta de Requisi��es"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Consulta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   120
            TabIndex        =   134
            ToolTipText     =   "Consulta de Requisi��es"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame5 
         Height          =   600
         Left            =   11520
         TabIndex        =   129
         ToolTipText     =   "Impress�o de Etiquetas"
         Top             =   2760
         Width           =   1335
         Begin VB.CommandButton BtFichaCruzada 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoAguas.frx":C955
            Style           =   1  'Graphical
            TabIndex        =   130
            ToolTipText     =   "Impress�o de Etiquetas"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaFichaCruz 
            Caption         =   "Etiquetas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   131
            ToolTipText     =   "Etiquetas"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame3 
         Height          =   600
         Left            =   11520
         TabIndex        =   122
         ToolTipText     =   "Notas Associadas � Requisi��o"
         Top             =   2160
         Width           =   1335
         Begin VB.CommandButton BtNotasVRM 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoAguas.frx":D0BF
            Style           =   1  'Graphical
            TabIndex        =   139
            ToolTipText     =   "Notas"
            Top             =   120
            Width           =   495
         End
         Begin VB.CommandButton BtNotas 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoAguas.frx":D829
            Style           =   1  'Graphical
            TabIndex        =   123
            ToolTipText     =   "Notas"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Notas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   124
            ToolTipText     =   "Notas"
            Top             =   240
            Width           =   975
         End
      End
      Begin MSFlexGridLib.MSFlexGrid FGRecibos 
         Height          =   3135
         Left            =   -74880
         TabIndex        =   114
         Top             =   600
         Width           =   12495
         _ExtentX        =   22040
         _ExtentY        =   5530
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         Appearance      =   0
      End
      Begin VB.ComboBox CbDestino 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Frame FrBtProd 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   -74760
         TabIndex        =   66
         Top             =   4200
         Width           =   12495
         Begin VB.Label LaProdutos 
            Height          =   345
            Left            =   1320
            TabIndex        =   67
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.Frame FrProdutos 
         BorderStyle     =   0  'None
         Height          =   3615
         Left            =   -74880
         TabIndex        =   64
         Top             =   480
         Width           =   12615
         Begin MSFlexGridLib.MSFlexGrid FgProd 
            Height          =   2235
            Left            =   120
            TabIndex        =   65
            Top             =   120
            Width           =   10500
            _ExtentX        =   18521
            _ExtentY        =   3942
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrAnalises 
         BorderStyle     =   0  'None
         Height          =   4455
         Left            =   -74880
         TabIndex        =   61
         Top             =   360
         Width           =   12615
         Begin VB.CommandButton BtPesquisaAna 
            Height          =   375
            Left            =   12240
            Picture         =   "FormGestaoRequisicaoAguas.frx":DF93
            Style           =   1  'Graphical
            TabIndex        =   62
            Top             =   90
            Width           =   375
         End
         Begin MSFlexGridLib.MSFlexGrid FGAna 
            Height          =   4290
            Left            =   120
            TabIndex        =   63
            TabStop         =   0   'False
            ToolTipText     =   "F7 - Para introduzir observa��o"
            Top             =   120
            Width           =   10065
            _ExtentX        =   17754
            _ExtentY        =   7567
            _Version        =   393216
            BackColorBkg    =   -2147483633
            SelectionMode   =   1
            AllowUserResizing=   3
            BorderStyle     =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label LbTotalAna 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   11400
            TabIndex        =   174
            Top             =   4080
            Width           =   375
         End
         Begin VB.Label Label7 
            Caption         =   "Total An�lises:"
            Height          =   255
            Left            =   10320
            TabIndex        =   173
            Top             =   4080
            Width           =   1095
         End
      End
      Begin VB.Frame FrBtTubos 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   -74880
         TabIndex        =   59
         Top             =   4140
         Width           =   12615
         Begin VB.Label LaTubos 
            Height          =   465
            Left            =   1320
            TabIndex        =   60
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.TextBox EcCodEFR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   10
         Top             =   2400
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   5400
         Picture         =   "FormGestaoRequisicaoAguas.frx":E51D
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   2400
         Width           =   375
      End
      Begin VB.ComboBox CbUrgencia 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   4440
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1320
         Width           =   1335
      End
      Begin VB.ListBox ListaDiagnSec 
         Height          =   2790
         Left            =   -69240
         TabIndex        =   55
         Top             =   480
         Visible         =   0   'False
         Width           =   2655
      End
      Begin MSFlexGridLib.MSFlexGrid FGTubos 
         Height          =   3315
         Left            =   -74760
         TabIndex        =   165
         Top             =   600
         Width           =   12420
         _ExtentX        =   21908
         _ExtentY        =   5847
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   56
         TabStop         =   0   'False
         Top             =   2400
         Width           =   3375
      End
      Begin VB.Label Label1 
         Caption         =   "Observa��o"
         Height          =   255
         Index           =   17
         Left            =   120
         TabIndex        =   213
         Top             =   4440
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Colheita"
         Height          =   255
         Index           =   13
         Left            =   6600
         TabIndex        =   212
         Top             =   3840
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Local Colheita"
         Height          =   255
         Index           =   12
         Left            =   6600
         TabIndex        =   211
         Top             =   3360
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Dt. Rece��o"
         Height          =   255
         Index           =   11
         Left            =   3240
         TabIndex        =   210
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Dt. Colheita"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   209
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Ponto Colheita"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   207
         Top             =   3840
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Origem Amostra"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   205
         Top             =   3360
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo Amostra"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   203
         Top             =   2880
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Dt Entrega"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   178
         Top             =   915
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Sala / Posto"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   137
         Top             =   1920
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Destino"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   104
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   2400
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Urg�ncia"
         Height          =   255
         Index           =   5
         Left            =   3240
         TabIndex        =   57
         Top             =   1320
         Width           =   735
      End
   End
   Begin MSComctlLib.ImageList ListaImagensAna 
      Left            =   8280
      Top             =   13080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":EAA7
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":EC01
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":ED5B
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":EEB5
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F00F
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F169
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F2C3
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F41D
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F577
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F6D1
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F82B
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":F985
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton BtGetSONHO 
      Enabled         =   0   'False
      Height          =   315
      Left            =   5040
      MaskColor       =   &H00C0C0FF&
      Picture         =   "FormGestaoRequisicaoAguas.frx":FADF
      Style           =   1  'Graphical
      TabIndex        =   106
      ToolTipText     =   "Importar do SONHO"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin MSComctlLib.ImageList ImageListSearch 
      Left            =   480
      Top             =   9600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   24
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":FDF1
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":1056B
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":10CE5
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoAguas.frx":1145F
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label LbNumColheita 
      AutoSize        =   -1  'True
      Caption         =   "N�Colheita"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   6840
      TabIndex        =   219
      Top             =   120
      Width           =   750
   End
   Begin VB.Label Label1 
      Caption         =   "Dt &chegada"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   10800
      TabIndex        =   216
      Top             =   10140
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Dt previs&ta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   7440
      TabIndex        =   215
      Top             =   10140
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgDiagSec"
      Enabled         =   0   'False
      Height          =   255
      Index           =   29
      Left            =   2160
      TabIndex        =   201
      Top             =   9840
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcHemodialise"
      Enabled         =   0   'False
      Height          =   255
      Index           =   28
      Left            =   2400
      TabIndex        =   191
      Top             =   10440
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcSMSEnviada"
      Enabled         =   0   'False
      Height          =   255
      Index           =   27
      Left            =   600
      TabIndex        =   176
      Top             =   10560
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxRec"
      Enabled         =   0   'False
      Height          =   255
      Index           =   26
      Left            =   10320
      TabIndex        =   167
      Top             =   13080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgFacturado"
      Enabled         =   0   'False
      Height          =   255
      Index           =   23
      Left            =   10200
      TabIndex        =   164
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNAdm"
      Enabled         =   0   'False
      Height          =   255
      Index           =   18
      Left            =   10200
      TabIndex        =   153
      Top             =   11880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNTubos"
      Enabled         =   0   'False
      Height          =   255
      Index           =   16
      Left            =   10200
      TabIndex        =   152
      Top             =   11640
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   12
      Left            =   10200
      TabIndex        =   151
      Top             =   10920
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqTipo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   15
      Left            =   10200
      TabIndex        =   150
      Top             =   12120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNEsp"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   10200
      TabIndex        =   149
      Top             =   11400
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNCopias"
      Enabled         =   0   'False
      Height          =   255
      Index           =   0
      Left            =   10200
      TabIndex        =   148
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcFimSemana"
      Enabled         =   0   'False
      Height          =   255
      Index           =   11
      Left            =   6240
      TabIndex        =   141
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Requisi��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Index           =   0
      Left            =   240
      TabIndex        =   136
      Top             =   120
      Width           =   900
   End
   Begin VB.Label Label15 
      Caption         =   "EcPrinterRecibo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   2880
      TabIndex        =   128
      Top             =   13320
      Width           =   975
   End
   Begin VB.Label Label15 
      Caption         =   "Obs. &Final"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   126
      Top             =   13320
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapMedico"
      Enabled         =   0   'False
      Height          =   255
      Index           =   7
      Left            =   840
      TabIndex        =   121
      Top             =   12120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodPostalAux"
      Enabled         =   0   'False
      Height          =   255
      Index           =   4
      Left            =   4440
      TabIndex        =   112
      Top             =   11640
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcDescrEfr2"
      Enabled         =   0   'False
      Height          =   255
      Index           =   3
      Left            =   4440
      TabIndex        =   110
      Top             =   11400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodEfr2"
      Enabled         =   0   'False
      Height          =   255
      Index           =   2
      Left            =   4440
      TabIndex        =   109
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxProd"
      Enabled         =   0   'False
      Height          =   255
      Index           =   5
      Left            =   480
      TabIndex        =   103
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcUrgencia"
      Enabled         =   0   'False
      Height          =   255
      Index           =   10
      Left            =   840
      TabIndex        =   102
      Top             =   11880
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcPagUte"
      Enabled         =   0   'False
      Height          =   255
      Index           =   9
      Left            =   840
      TabIndex        =   101
      Top             =   11640
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcEstadoReq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   840
      TabIndex        =   100
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProven"
      Enabled         =   0   'False
      Height          =   255
      Index           =   6
      Left            =   2520
      TabIndex        =   99
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcSeqUtente"
      Enabled         =   0   'False
      Height          =   255
      Index           =   8
      Left            =   840
      TabIndex        =   98
      Top             =   11400
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProduto"
      Enabled         =   0   'False
      Height          =   255
      Index           =   14
      Left            =   6840
      TabIndex        =   97
      Top             =   11400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "CodAnaTemp"
      Height          =   255
      Index           =   0
      Left            =   2520
      TabIndex        =   96
      Top             =   11520
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodIsencao"
      Height          =   255
      Index           =   19
      Left            =   2520
      TabIndex        =   95
      Top             =   11280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   17
      Left            =   2880
      TabIndex        =   94
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcTEFR"
      Enabled         =   0   'False
      Height          =   255
      Index           =   20
      Left            =   5400
      TabIndex        =   93
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcGrupoAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   13
      Left            =   2520
      TabIndex        =   92
      Top             =   11880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxTubo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   22
      Left            =   480
      TabIndex        =   91
      Top             =   12840
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcinfComplementar"
      Height          =   255
      Index           =   21
      Left            =   5400
      TabIndex        =   90
      Top             =   12840
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrColheita"
      Enabled         =   0   'False
      Height          =   255
      Index           =   24
      Left            =   7200
      TabIndex        =   89
      Top             =   12600
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "FormGestaoRequisicaoAguas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/03/2003
' T�cnico : Paulo Costa

' Vari�veis  para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Public CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim Formato1 As String
Dim Formato2 As String


Public rs As ADODB.recordset
Public MarcaLocal As Variant

'Controlo dos produtos
Public RegistosP As New Collection
Dim LastColP As Integer
Dim LastRowP As Integer
Dim ExecutaCodigoP As Boolean

'Controlo dos tubos
Dim LastColT As Integer
Dim LastRowT As Integer
Dim ExecutaCodigoT As Boolean

'Flag que indica se a entidade em causa � ARS
Dim FlgARS As Boolean

'Flag que indica se a entidade em causa � ADSE
Dim FlgADSE As Boolean

'Controlo de analises
Public RegistosA As New Collection
Dim LastColA As Integer
Dim LastRowA As Long
Dim ExecutaCodigoA As Boolean

'Controlo de Recibos
Public RegistosR As New Collection
Dim LastColR As Integer
Dim LastRowR As Integer
Dim ExecutaCodigoR As Boolean

'Controlo de Recibos Manuais
Public RegistosRM As New Collection

'Controlo de Adiantamentos
Public RegistosAD As New Collection

'acumula o numero do P1 para quando este for superior a 7 incrementar 1 ao P1
Dim ContP1 As Integer
Dim Flg_IncrementaP1 As Boolean

'Comando utilizados obter a ordem correcta das an�lises
Dim CmdOrdAna As New ADODB.Command

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdAnaReq As String
Dim CmdAnaReq_H As String


'Flag usada para controlar a coloca��o da data actual no lostfocus da EcDtColheita
'quando se faz o limpacampos
Dim Flg_LimpaCampos As Boolean

'flag que indica se se deve preencher a data de chegada de todos os produtos na altura do Insert ou UpDate
Dim Flg_PreencherDtChega As Boolean

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

'valor do indice nas combos de Pagamento (TM,Ent,Ute) quando se executa a fun��o procurar
Dim CbTMIndex As Integer
Dim CbEntIndex As Integer
Dim CbUteIndex As Integer

Dim gRec As Long

'Flag que evita os beep dos enters nas textbox
Dim Enter As Boolean

'Constante com cor azul
Const azul = &H8000000D

Dim Flg_PergProd As Boolean
Dim Flg_DtChProd As Boolean
Dim Resp_PergProd As Boolean
Dim Resp_PergDtChProd As Boolean

Dim Flg_PergTubo As Boolean
Dim Flg_DtChTubo As Boolean
Dim Resp_PergTubo As Boolean
Dim Resp_PergDtChTubo As Boolean


'contem a data de chegada da verificacao dos produtos para utilizacao na marcacao de analises
Dim gColocaDataChegada As String

'Flag que indica a entrada no form de pesquisa de utentes
Dim Flg_PesqUtente As Boolean


'Variaveis utilizadas na marca��o de an�lises GhostMember na pr�pria requisi�ao
Dim GhostPerfil As String
Dim GhostComplexa As String

' 02


Dim Flg_Gravou As Boolean


'Estrutura que guarda as an�lises eliminadas
Private Type Tipo_AnalisesEliminadas
    codAna As String
    NReq As String
End Type
Dim Eliminadas() As Tipo_AnalisesEliminadas
Dim TotalEliminadas As Integer

Private Type Tipo_tuboEliminado
    seq_req_tubo As Long
    n_req As String
End Type
Dim tubosEliminados() As Tipo_tuboEliminado
Dim TotalTubosEliminados As Integer

'Estrutura que guarda as an�lises acrescentadas
Private Type Tipo_AnalisesAcrescentadas
    cod_agrup As String
    NReq As String
End Type
Dim Acrescentadas() As Tipo_AnalisesAcrescentadas
Dim TotalAcrescentadas As Integer



Const vermelho = &HC0C0FF
Const Verde = &HC0FFC0
Const Amarelo = &HC0FFFF

Const lColFgAnaCodigo = 0
Const lColFgAnaDescricao = 1
Const lColFgAnaP1 = 2
Const lColFgAnaMedico = 3
Const lColFgAnaCodEFR = 4
Const lColFgAnaDescrEFR = 5
Const lColFgAnaNrBenef = 6
Const lColFgAnaPreco = 7

Const lColFgAnaEFRlinha = 0
Const lColFgAnaEFRCodigo = 1
Const lColFgAnaEFRDescricao = 2
Const lColFgAnaEFRP1 = 3
Const lColFgAnaEFROrdemP1 = 4
Const lColFgAnaEFRMedico = 5
Const lColFgAnaEFRCodEFR = 6
Const lColFgAnaEFRNrBenef = 7
Const lColFgAnaEFRIsen = 8
Const lColFgAnaEFRFact = 9
Const lColFgAnaEFRCB = 10
Const lColFgAnaEFRQtd = 11
Const lColFgAnaEFRRec = 12
Const lColFgAnaEFRPreco = 13
Const lColFgAnaEFRPrecoEFR = 14

Const lColFgAnaRecCodAna = 0
Const lColFgAnaRecDescrAna = 1
Const lColFgAnaRecQtd = 2
Const lColFgAnaRecTaxa = 3

Const lCodUrbano = 2
Const lCodNaoUrbano = 3
Const lCodUrbanoPortoLisboa = 1
Const lCodUrbanoBiDiario = 4

Dim linhaAnaliseOBS As Long
Dim CodEfrAUX As String


' ESTRUTURA COM ANALISES PERTENCENTES A UM RECIBO
Private Type AnaRecibo
    codAna As String
    descrAna As String
    taxa As String
    qtd As Integer
    tipo As String  ' MARCADA - MANUAL
    indice As Long  ' INDICE NA ESTRUTURA
    Flg_adicionada As Integer
End Type
Dim EstrutAnaRecibo() As AnaRecibo
Dim TotalAnaRecibo As Integer


Const rosa = &HFFC0FF
Const azul2 = &HFFFFC0
Const CorBorla = rosa
Const CorIsento = azul2

Dim TuboColunaActual As Long
Dim TuboLinhaActual As Long

Dim flgEmissaoRecibos As Boolean

' -------------------------------------------------------------------
' COLUNAS NA GRELHA RECIBOS
' -------------------------------------------------------------------
Const lColRecibosNumDoc = 0
Const lColRecibosEntidade = 1
Const lColRecibosValorPagar = 2
Const lColRecibosCaucao = 7
Const lColRecibosModoPag = 8
Const lColRecibosDtEmissao = 4
Const lColRecibosDtPagamento = 5
Const lColRecibosEstado = 6
Const lColRecibosPercentagem = 3
Const lColRecibosBorla = 9
Const lColRecibosNaoConfmidade = 10

Const lColAdiantamentosNumDoc = 0
Const lColAdiantamentosValor = 1
Const lColAdiantamentosEstado = 2
Const lColAdiantamentosDtEmissao = 3
Const lColAdiantamentosDtAnulacao = 4

Dim flg_Aberto As Boolean
Dim flg_preencheCampos As Boolean
Dim flg_ReqBloqueada As Boolean
Dim flg_codBarras As Boolean
Dim Acumula As String
Dim Cabecalho As String

Const laranja = &HC0E0FF

Dim flg_novaArs As Integer

Dim dataAct As String
Const vermelhoClaro = &HBDC8F4

'NELSONPSILVA Glintt-HS-18011 04.04.2018 - Acesso contextualizado quando chamado pelo form anterior (Log RGPD)
Public ExternalAccess As Boolean
'

' ---------------------------------------------------

Sub Elimina_Mareq(codAgrup As String)

    Dim i As Integer

    On Error GoTo Trata_Erro


    i = 1
    While i <= UBound(MaReq)
        If Trim(UCase(codAgrup)) = Trim(UCase(MaReq(i).cod_agrup)) Then
            Actualiza_Estrutura_MAReq i
        Else
            i = i + 1
        End If
    Wend

    If UBound(MaReq) = 0 Then ReDim MaReq(1)

Exit Sub
Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "Elimina_Mareq: " & Err.Description
    End If

    Resume Next
End Sub

Sub Elimina_RegistoA(codAgrup As String)
    On Error GoTo TrataErro
    Dim i As Long

    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            RegistosA.Remove i
            FGAna.RemoveItem i
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Elimina_RegistoA: " & Err.Number & " - " & Err.Description, Me.Name, "Elimina_RegistoA"
    Exit Sub
    Resume Next
End Sub

Function Insere_Nova_Analise(indice As Long, codAgrup As String, AnaMarcada As String, codBarras As Boolean, multiAna As String) As Boolean
    On Error GoTo TrataErro

    Dim descricao As String
    Dim Marcar() As String
    Dim reqComAnalise As String
    Dim DataReq As String
    Dim CodFacturavel As String



    Insere_Nova_Analise = False

    descricao = SELECT_Descr_Ana(codAgrup)
    If codAgrup <> AnaMarcada Then
        If Mid(codAgrup, 2) <> UCase(AnaMarcada) Then
            CodFacturavel = AnaMarcada
        Else
            CodFacturavel = codAgrup
        End If

    Else
        CodFacturavel = codAgrup
    End If
    If descricao = "" Then
        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
        EcAuxAna.text = ""
    Else
        If VerificaRegraSexoMarcar(codAgrup, EcSeqUtente) = False Then
            BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
            EcAuxAna.text = ""
            Insere_Nova_Analise = False
            Exit Function
        End If
        gColocaDataChegada = ""
        If Verifica_Ana_Prod(codAgrup, Marcar) Then
            If Trim(RegistosA(indice).codAna) <> "" Then
                Elimina_Mareq RegistosA(indice).codAna
                RegistosA(indice).codAna = ""
            End If

            ' --------------------------------------------------------------------------------

            If (gProcAnPendentesAoRegistar = mediSim) Then

                ' Indica a requisi��o mais recente com esta an�lise pendente, para este utente.

                reqComAnalise = ""
                reqComAnalise = REQUISICAO_Get_Com_Analise_Pendente(UCase(EcAuxAna.text), _
                                                                    EcSeqUtente.text, _
                                                                    DataReq)

                If (Len(reqComAnalise)) Then
                    MsgBox "A an�lise " & UCase(EcAuxAna.text) & " est� pendente na requisi��o " & reqComAnalise & "        " & vbCrLf & _
                           "datada de " & DataReq & ".", vbInformation, " Pesquisa de An�lises Pendentes "
                End If

            End If
            ' --------------------------------------------------------------------------------
            Select Case Mid(Trim(UCase(codAgrup)), 1, 1)
                Case "P"

                    SELECT_Dados_Perfil Marcar, indice, Trim(UCase(codAgrup)), CodFacturavel, multiAna
                Case "C"
                    SELECT_Dados_Complexa Marcar, indice, Trim(UCase(codAgrup)), multiAna, , , , CodFacturavel
                Case "S"
                    If SELECT_Dados_Simples(Marcar, indice, Trim(UCase(codAgrup)), multiAna, , , , , , CodFacturavel) = False Then
                        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
                        EcAuxAna.text = ""
                        Exit Function
                    End If
                Case Else
            End Select


            Insere_Nova_Analise = True
        End If

    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Insere_Nova_Analise: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_Nova_Analise"
    Insere_Nova_Analise = False
    Exit Function
    Resume Next
End Function

Sub InsereAutoProd(CodProd As String, DtChega As String)
    On Error GoTo TrataErro

    Dim LastLastRP As Integer
    Dim i As Integer

    EcAuxProd.text = CodProd

    LastLastRP = LastRowP
    LastRowP = FgProd.rows - 1

    'Verificar se o produto j� est� na lista
    For i = 1 To LastRowP
        If Trim(FgProd.TextMatrix(i, 0)) = Trim(CodProd) Then
            Exit Sub
        End If
    Next

    If SELECT_Descr_Prod = True Then
        ExecutaCodigoP = False

        If Trim(DtChega) <> "" Then
            FgProd.TextMatrix(LastRowP, 5) = Trim(DtChega)
            RegistosP(LastRowP).DtPrev = Trim(DtChega)
            FgProd.TextMatrix(LastRowP, 6) = Trim(DtChega)
            RegistosP(LastRowP).DtChega = Trim(DtChega)
            gColocaDataChegada = Trim(DtChega)
            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto Then
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
            End If
        Else
            FgProd.TextMatrix(LastRowP, 5) = EcDtColheita
            RegistosP(LastRowP).DtPrev = EcDtColheita
        End If

        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd

        FgProd.TextMatrix(LastRowP, 0) = UCase(CodProd)
        RegistosP(LastRowP).CodProd = UCase(CodProd)

        'Cria linha vazia
        FgProd.AddItem ""
        CriaNovaClasse RegistosP, FgProd.rows - 1, "PROD"

        ExecutaCodigoP = True

        FgProd.row = FgProd.rows - 1
        FgProd.Col = 0
        LastColP = FgProd.Col
        LastRowP = FgProd.row
    Else
        LastRowP = LastLastRP
    End If

    EcAuxProd.text = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "InsereAutoProd: " & Err.Number & " - " & Err.Description, Me.Name, "InsereAutoProd"
    Exit Sub
    Resume Next
End Sub



Sub InsereGhostMember(codAnaS As String)
    On Error GoTo TrataErro

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim Marcar(1) As String
    Dim indice  As Long
    Dim i As Long
    Dim OrdAna As Long
    Dim CodPerfil As String
    Dim DescrPerfil As String
    Dim OrdPerf As Integer
    Dim CodAnaC As String
    Dim DescrAnaC As String

codAnaS = Trim(codAnaS)

'For�ar a marca��o
Marcar(1) = codAnaS & ";"

indice = -1
For i = 1 To UBound(MaReq)
    If Trim(GhostPerfil) = Trim(MaReq(i).Cod_Perfil) And _
       Trim(GhostComplexa) = Trim(MaReq(i).cod_ana_c) Then
       indice = i
       Exit For
    End If
Next i

If indice = -1 Then
    BG_Mensagem mediMsgBox, "Erro a inserir an�lise!", vbCritical + vbOKOnly, App.ProductName
    Exit Sub
End If

gColocaDataChegada = MaReq(indice).dt_chega

'Dados da Complexa
sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    .Open , gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
End With

If RsCompl.RecordCount > 0 Then
    descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
Else
    BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
End If
RsCompl.Close
Set RsCompl = Nothing

OrdAna = MaReq(indice).Ord_Ana
CodPerfil = MaReq(indice).Cod_Perfil
DescrPerfil = MaReq(indice).Descr_Perfil
OrdPerf = MaReq(indice).Ord_Ana_Perf
CodAnaC = MaReq(indice).cod_ana_c
DescrAnaC = MaReq(indice).Descr_Ana_C

'Dados do Membro da complexa (CodAnaS)
sql = "SELECT ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa) & _
        " AND cod_membro = " & BL_TrataStringParaBD(codAnaS)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    .Open , gConexao
End With

If Not RsCompl.EOF Then
    If GhostPerfil <> "0" Then
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, "", CodPerfil, DescrPerfil, OrdPerf, CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    Else
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, "", , , , CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    End If
End If

RsCompl.Close
Set RsCompl = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "InsereGhostMember: " & Err.Number & " - " & Err.Description, Me.Name, "InsereGhostMember"
    Exit Sub
    Resume Next
End Sub

Function Pesquisa_RegistosA(codAgrup As String) As Long
    On Error GoTo TrataErro

    Dim i As Long
    Dim lRet As Long

    lRet = -1

    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            lRet = i
            Exit For
        End If
    Next i

    Pesquisa_RegistosA = lRet
Exit Function
TrataErro:
    BG_LogFile_Erros "Pesquisa_RegistosA: " & Err.Number & " - " & Err.Description, Me.Name, "Pesquisa_RegistosA"
    Pesquisa_RegistosA = -1
    Exit Function
    Resume Next
End Function

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes
    flgEmissaoRecibos = False

    Max = -1
    ReDim FOPropertiesTemp(0)

    DefTipoCampos

    EcUtente.text = ""
    DoEvents

    PreencheValoresDefeito

    gF_REQUIS_AGUAS = 1
    Flg_PesqUtente = False

    Estado = 1
    BG_StackJanelas_Push Me

    SSTGestReq.Tab = 0
    DoEvents


End Sub

Sub Inicializacoes()
    Dim i As Integer
    On Error GoTo TrataErro
    Cabecalho = " Gest�o de Requisi��es"
    Me.caption = Cabecalho
    Me.left = 5
    Me.top = 20
    Me.Width = 13275
    Me.Height = 8655

    NomeTabela = "sl_requis"
    Set CampoDeFocus = EcNumReq
    NumCampos = 35
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim CoresCamposEc(0 To NumCampos - 1)
    ReDim CamposEcBckp(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)

    ' Campos da Base de Dados

    CamposBD(0) = "n_req"
    CamposBD(1) = "seq_utente"
    CamposBD(2) = "dt_previ"
    CamposBD(3) = "dt_chega"
    CamposBD(4) = "dt_imp"
    CamposBD(5) = "t_urg"
    CamposBD(6) = "cod_efr"
    CamposBD(7) = "obs_req"
    CamposBD(8) = "hr_imp"
    CamposBD(9) = "user_imp"
    CamposBD(10) = "dt_imp2"
    CamposBD(11) = "hr_imp2"
    CamposBD(12) = "user_imp2"
    CamposBD(13) = "estado_req"
    CamposBD(14) = "user_cri"
    CamposBD(15) = "hr_cri"
    CamposBD(16) = "dt_cri"
    CamposBD(17) = "user_act"
    CamposBD(18) = "hr_act"
    CamposBD(19) = "dt_act"
    CamposBD(20) = "gr_ana"
    CamposBD(21) = "hr_chega"
    CamposBD(22) = "cod_local"
    CamposBD(23) = "user_fecho"
    CamposBD(24) = "dt_fecho"
    CamposBD(25) = "hr_fecho"
    CamposBD(26) = "cod_sala"
    CamposBD(27) = "cod_destino"
    CamposBD(28) = "flg_facturado"
    CamposBD(29) = "dt_conclusao"
    CamposBD(30) = "user_confirm"
    CamposBD(31) = "dt_confirm"
    CamposBD(32) = "hr_confirm"
    CamposBD(33) = "dt_colheita"
    CamposBD(34) = "user_colheita"

    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcSeqUtente
    Set CamposEc(2) = EcDtColheita
    Set CamposEc(3) = EcDataChegada
    Set CamposEc(4) = EcDataImpressao
    Set CamposEc(5) = CbUrgencia
    Set CamposEc(6) = EcCodEFR
    Set CamposEc(7) = EcObs
    Set CamposEc(8) = EcHoraImpressao
    Set CamposEc(9) = EcUtilizadorImpressao
    Set CamposEc(10) = EcDataImpressao2
    Set CamposEc(11) = EcHoraImpressao2
    Set CamposEc(12) = EcUtilizadorImpressao2
    Set CamposEc(13) = EcEstadoReq
    Set CamposEc(14) = EcUtilizadorCriacao
    Set CamposEc(15) = EcHoraCriacao
    Set CamposEc(16) = EcDataCriacao
    Set CamposEc(17) = EcUtilizadorAlteracao
    Set CamposEc(18) = EcHoraAlteracao
    Set CamposEc(19) = EcDataAlteracao
    Set CamposEc(20) = EcGrupoAna
    Set CamposEc(21) = EcHoraChegada
    Set CamposEc(22) = EcLocal
    Set CamposEc(23) = EcUtilizadorFecho
    Set CamposEc(24) = EcDataFecho
    Set CamposEc(25) = EcHoraFecho
    Set CamposEc(26) = EcCodSala
    Set CamposEc(27) = CbDestino
    Set CamposEc(28) = EcFlgFacturado
    Set CamposEc(29) = EcDtEntrega
    Set CamposEc(30) = EcUtilConf
    Set CamposEc(31) = EcDtConf
    Set CamposEc(32) = EcHrConf
    Set CamposEc(33) = EcDtColheita
    Set CamposEc(34) = EcRespColheita
    
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Utente"
    TextoCamposObrigatorios(2) = "Data activa��o da requisi��o"
    TextoCamposObrigatorios(5) = "Tipo de urg�ncia"

    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_req"
    Set ChaveEc = EcNumReq

    'inicializa��es das estruturas de mem�ria (an�lises e recibos)
    Inicializa_Estrutura_Analises

    'O TAB de an�lises est� desactivado at� se introduzir uma entidade financeira
    ' e o de produtos at� introduzir uma data prevista
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    
    FlgARS = False
    FlgADSE = False
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = False


    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False


    'Inicializa comando para ir buscar as an�lises da requisi��o e os seus dados

    CmdAnaReq = "( Select  -1 seq_realiza, m.n_folha_trab, m.n_req, m.dt_chega, m.cod_agrup, m.ord_ana_perf, m.ord_ana_compl, m.ord_ana, m.cod_perfil, descr_perfis, m.cod_ana_c, descr_ana_c, m.cod_ana_s, descr_ana_s, m.flg_apar_trans, '-1' as flg_estado, m.flg_facturado,m.ord_marca, m.transmit_psm, m.n_envio from sl_marcacoes m LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = m.cod_perfil LEFT OUTER JOIN sl_ana_c c on c.cod_ana_c = m.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = m.cod_ana_s   where m.n_req =  ?  )" & _
                            " Union " & _
                            "( select  r.seq_realiza, r.n_folha_trab, r.n_req, r.dt_chega, r.cod_agrup, r.ord_ana_perf, r.ord_ana_compl, r.ord_ana, r.cod_perfil, descr_perfis, r.cod_ana_c, descr_ana_c, r.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, r.flg_estado, r.flg_facturado,r.ord_marca,1 transmit_psm, 1 n_envio from  sl_realiza r LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = r.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = r.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = r.cod_ana_s  where r.n_req =  ? )" & _
                            "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"

    ' HISTORICO
    CmdAnaReq_H = " select  h.seq_realiza,h.n_folha_trab, h.n_req, h.dt_chega, h.cod_agrup, h.ord_ana_perf, h.ord_ana_compl, h.ord_ana, h.cod_perfil, descr_perfis, h.cod_ana_c, descr_ana_c, h.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, h.flg_estado, h.flg_facturado,h.ord_marca, 1 transmit_psm, 1 n_envio from  sl_realiza_h h LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = h.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = h.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = h.cod_ana_s  where h.n_req =  ? " & _
                  "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"


    'Inicializa comando para ir buscar a ordem e o grupo das an�lises
    Set CmdOrdAna.ActiveConnection = gConexao
    CmdOrdAna.CommandType = adCmdText
    CmdOrdAna.CommandText = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis = ? "
    CmdOrdAna.Prepared = True


    'Inicializa��o das variaveis que guardam os indices nas combos de pagamentos
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1

    EcPrinterRecibo = BL_SelImpressora("ReciboReq.rpt")
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")


    Flg_Gravou = False
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    flg_preencheCampos = False

Exit Sub
TrataErro:
    BG_LogFile_Erros "Inicializacoes: " & Err.Number & " - " & Err.Description, Me.Name, "Inicializacoes"
    Exit Sub
    Resume Next
End Sub

Function FuncaoProcurar_PreencheProd(NumReq As Long)
    On Error GoTo TrataErro

    ' Selecciona todos os produtos da requisi��o e preenche a lista

    Dim i As Long
    Dim rsDescr As ADODB.recordset
    Dim RsProd As ADODB.recordset
    Dim CmdDescrP As New ADODB.Command
    Dim PmtCodProd As ADODB.Parameter
    Dim CmdDescrE As New ADODB.Command
    Dim PmtCodEsp As ADODB.Parameter

    Set RsProd = New ADODB.recordset
    With RsProd
        .Source = "SELECT cod_prod, cod_especif,dt_previ, dt_chega, volume, obs_especif FROM sl_req_prod " & _
                   "WHERE n_req =" & NumReq

        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With

    'Inicializa o comando ADO para selec��o da descri��o do produto
    Set CmdDescrP.ActiveConnection = gConexao
    CmdDescrP.CommandType = adCmdText
    CmdDescrP.CommandText = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                            "cod_produto =?"
    CmdDescrP.Prepared = True
    Set PmtCodProd = CmdDescrE.CreateParameter("COD_PRODUTO", adVarChar, adParamInput, 5)
    CmdDescrP.Parameters.Append PmtCodProd

    'Inicializa o comando ADO para selec��o da descri��o da especifica��o
     Set CmdDescrE.ActiveConnection = gConexao
     CmdDescrE.CommandType = adCmdText
     CmdDescrE.CommandText = "SELECT descr_especif FROM sl_especif WHERE " & _
                              "cod_especif =?"
     CmdDescrE.Prepared = True
     Set PmtCodEsp = CmdDescrE.CreateParameter("COD_ESPECIF", adVarChar, adParamInput, 5)
     CmdDescrE.Parameters.Append PmtCodEsp

     i = 1
     LimpaFgProd
     While Not RsProd.EOF
        RegistosP(i).CodProd = RsProd!cod_prod
        RegistosP(i).Volume = BL_HandleNull(RsProd!Volume, "")
        CmdDescrP.Parameters(0).value = Trim(UCase(RsProd!cod_prod))
        Set rsDescr = CmdDescrP.Execute
        RegistosP(i).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(i).EspecifObrig = rsDescr!especif_obrig
        RegistosP(i).ObsEspecif = BL_HandleNull(RsProd!obs_especif, "")
        rsDescr.Close

        RegistosP(i).CodEspecif = BL_HandleNull(RsProd!cod_Especif, "")
        'descri��o da especifica��o
        If Trim(RegistosP(i).CodEspecif) <> "" Then
            CmdDescrE.Parameters(0) = UCase(RsProd!cod_Especif)
            Set rsDescr = CmdDescrE.Execute
            RegistosP(i).DescrEspecif = BL_HandleNull(rsDescr!descr_especif, "")
            rsDescr.Close
        End If
        RegistosP(i).DtPrev = BL_HandleNull(RsProd!dt_previ, "")
        RegistosP(i).DtChega = BL_HandleNull(RsProd!dt_chega, "")

        RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)

        FgProd.TextMatrix(i, 0) = RegistosP(i).CodProd
        FgProd.TextMatrix(i, 1) = RegistosP(i).DescrProd
        FgProd.TextMatrix(i, 2) = RegistosP(i).CodEspecif
        FgProd.TextMatrix(i, 3) = RegistosP(i).DescrEspecif
        FgProd.TextMatrix(i, 4) = RegistosP(i).Volume
        FgProd.TextMatrix(i, 5) = RegistosP(i).DtPrev
        FgProd.TextMatrix(i, 6) = RegistosP(i).DtChega
        FgProd.TextMatrix(i, 7) = RegistosP(i).EstadoProd

        'Se h� produtos que j� chegaram ent�o estado n�o deve estar "� espera de prod."
        If (Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto) And Trim(FgProd.TextMatrix(i, 5)) <> "" Then
            EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        End If

        RsProd.MoveNext
        i = i + 1
        CriaNovaClasse RegistosP, i, "PROD"
        FgProd.AddItem "", i
        FgProd.row = i
     Wend

     Set CmdDescrE = Nothing
     Set CmdDescrP = Nothing
     Set rsDescr = Nothing
     Set RsProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar_PreencheProd: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar_PreencheProd"
    Exit Function
    Resume Next
End Function

Sub FuncaoProcurar_PreencheAna(NumReq As Long)
    On Error GoTo TrataErro

    Dim i As Long
    Dim k As Long
    Dim MudaIcon As Integer
    Dim LastIntermedio As String
    Dim LastPai As String
    Dim Pai As String
    Dim Ja_Existe As Boolean
    Dim rsAna As ADODB.recordset
    Dim EstadoFinal As Integer
    Dim EArs As Boolean
    Dim ImgPai As Integer
    Dim ImgPaiSel As Integer
    Dim ObsAnaReq As String
    Dim seqObsAnaReq As Long
    Dim sql As String
    Dim flg_estado_aux As String

    ' PARA REQUISICOES QUE ESTAO NO HISTORICO APENAS VAI TABELA SL_REALIZA_H
    If EcEstadoReq = gEstadoReqHistorico Then
        sql = Replace(CmdAnaReq_H, "?", NumReq)
    Else
        sql = Replace(CmdAnaReq, "?", NumReq)
    End If

    Set rsAna = New ADODB.recordset
    With rsAna
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockPessimistic
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open
    End With
i = 1
LimpaFGAna

ExecutaCodigoA = False
ContP1 = 0
FGAna.Col = 0
LastIntermedio = ""
LastPai = ""
Pai = ""
MudaIcon = 0
ImgPai = 5
ImgPaiSel = 6

EArs = EFR_Verifica_ARS(EcCodEFR)

While Not rsAna.EOF
    flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
    If BL_HandleNull(rsAna!flg_estado, "-1") <> "-1" Then
        If VerificaAnaliseJaExisteRealiza(NumReq, BL_HandleNull(rsAna!Cod_Perfil, ""), BL_HandleNull(rsAna!cod_ana_c, ""), BL_HandleNull(rsAna!cod_ana_s, ""), False) = 0 Then
            flg_estado_aux = -1
        Else
            flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
        End If
    Else
        flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
    End If

        'Debug.Print RsAna!Cod_Ana_C & "-" & RsAna!cod_ana_s
        BL_Preenche_MAReq FGTubos, BL_HandleNull(rsAna!Cod_Perfil, ""), _
                        BL_HandleNull(rsAna!descr_perfis, ""), _
                        BL_HandleNull(rsAna!cod_ana_c, ""), _
                        BL_HandleNull(rsAna!Descr_Ana_C, ""), _
                        BL_HandleNull(rsAna!cod_ana_s, ""), _
                        BL_HandleNull(rsAna!descr_ana_s, ""), _
                        BL_HandleNull(rsAna!Ord_Ana, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Compl, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Perf, 0), _
                        BL_HandleNull(rsAna!cod_agrup, ""), _
                        BL_HandleNull(rsAna!N_Folha_Trab, 0), _
                        BL_HandleNull(rsAna!dt_chega, ""), _
                        BL_HandleNull(rsAna!flg_apar_trans, "0"), _
                        flg_estado_aux, _
                        BL_HandleNull(rsAna!Flg_Facturado, 0), _
                         BL_HandleNull(rsAna!Ord_Marca, 0), _
                         BL_HandleNull(rsAna!transmit_psm, 0), _
                         BL_HandleNull(rsAna!n_envio, 0), _
                         BL_HandleNull(EcDtColheita, "")

        Ja_Existe = False
        For k = 1 To RegistosA.Count
            If Trim(RegistosA(k).codAna) = Trim(MaReq(i).cod_agrup) Then
                Ja_Existe = True
                Exit For
            End If
        Next k



        If MaReq(i).flg_estado = "-1" Then
            MudaIcon = 0
        Else
            MudaIcon = 6
        End If

        If Not Ja_Existe Then
            LbTotalAna = LbTotalAna + 1
            RegistosA(FGAna.rows - 1).codAna = MaReq(i).cod_agrup
            If Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).Cod_Perfil) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Perfil
                ImgPai = 1 + MudaIcon
                ImgPaiSel = 2 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_c) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Ana_C
                ImgPai = 3 + MudaIcon
                ImgPaiSel = 4 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_s) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).descr_ana_s
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            Else
                RegistosA(FGAna.rows - 1).descrAna = ""
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            End If

            ObsAnaReq = ""
            seqObsAnaReq = 0
            BL_DevolveObsAnaReq NumReq, MaReq(i).cod_agrup, ObsAnaReq, seqObsAnaReq
            RegistosA(FGAna.rows - 1).ObsAnaReq = ObsAnaReq
            RegistosA(FGAna.rows - 1).seqObsAnaReq = seqObsAnaReq

            ' ------------------------------------------------------------------------------------------
            ' PREENCHE DADOS DOS RECIBOS ASSOCIADO A ANALISE EM CAUSA
            ' ------------------------------------------------------------------------------------------
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodigo) = RegistosA(FGAna.rows - 1).codAna
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescricao) = RegistosA(FGAna.rows - 1).descrAna
            RegistosA(FGAna.rows - 1).DtChega = BL_HandleNull(rsAna!dt_chega, "")

            If MaReq(i).flg_estado <> "-1" And Trim(FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodigo)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbBlack
            End If


            CriaNovaClasse RegistosA, 0, "ANA"
            FGAna.AddItem ""
            FGAna.row = FGAna.rows - 1

        End If







        i = i + 1
        rsAna.MoveNext

        'Se encontrou analises o estado n�o deve estar "Sem Analises"
        If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
            EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        End If
Wend

For i = 1 To FGAna.rows - 2
    EstadoFinal = -1
    For k = 1 To UBound(MaReq)
        If Trim(RegistosA(i).codAna) = Trim(MaReq(k).cod_agrup) Then
            If MaReq(k).flg_estado <> "-1" Then
                EstadoFinal = 0
                Exit For
            End If
        End If
    Next k
    RegistosA(i).Estado = EstadoFinal
Next i
RegistosA(FGAna.rows - 1).Estado = "-1"

rsAna.Close
Set rsAna = Nothing

ExecutaCodigoA = True

FGAna.Col = 0
FGAna.row = 1
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar_PreencheAna: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar_PreencheAna"
    Exit Sub
    Resume Next
End Sub

Sub DefTipoCampos()
    On Error GoTo TrataErro
    dataAct = Bg_DaData_ADO
    If gLAB = "CL" Then
        CbDestino.BackColor = &HC0FFFF
    Else
        CbDestino.BackColor = vbWhite
    End If
    If gGestaoColheita <> mediSim Then
        LbNumColheita.Visible = False
        EcNumColheita.Visible = False
    Else
        LbNumColheita.Visible = True
        EcNumColheita.Visible = True
    End If
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_previ", EcDtColheita, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_chega", EcDataChegada, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp", EcDataImpressao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp", EcHoraImpressao, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp2", EcDataImpressao2, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp2", EcHoraImpressao2, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fecho", EcDataFecho, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_conclusao", EcDtEntrega, mediTipoData

    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp", EcUtilizadorImpressao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp2", EcUtilizadorImpressao2, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_cri", EcUtilizadorCriacao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_act", EcUtilizadorAlteracao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_confirm", EcDtConf, mediTipoData


    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    ExecutaCodigoP = False

    With FGAna
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0

        .ColWidth(lColFgAnaCodigo) = 1000
        .Col = lColFgAnaCodigo
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaCodigo) = "C�digo"

        .ColWidth(lColFgAnaDescricao) = 2500
        .Col = lColFgAnaDescricao
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaDescricao) = "Descri��o"

        .ColWidth(lColFgAnaP1) = 1900
        .Col = lColFgAnaP1
        .TextMatrix(0, lColFgAnaP1) = "Credencial"

        .ColWidth(lColFgAnaMedico) = 800
        .Col = lColFgAnaMedico
        .TextMatrix(0, lColFgAnaMedico) = "M�dico"

        .ColWidth(lColFgAnaCodEFR) = 0
        .Col = lColFgAnaCodEFR
        .TextMatrix(0, lColFgAnaCodEFR) = ""

        .ColWidth(lColFgAnaDescrEFR) = 1500
        .Col = lColFgAnaDescrEFR
        .TextMatrix(0, lColFgAnaDescrEFR) = "Entidade"

        .ColWidth(lColFgAnaNrBenef) = 1400
        .Col = lColFgAnaNrBenef
        .TextMatrix(0, lColFgAnaNrBenef) = "Nr. Benef."

        .ColWidth(lColFgAnaPreco) = 700
        .Col = lColFgAnaPreco
        .TextMatrix(0, lColFgAnaPreco) = "Taxa �"

        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    LastColA = 0
    LastRowA = 1
    CriaNovaClasse RegistosA, 1, "ANA", True


    With FgProd
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Produto"
        .ColWidth(1) = 2700
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 850
        .Col = 2
        .TextMatrix(0, 2) = "Especifica."
        .ColWidth(3) = 2700
        .Col = 3
        .TextMatrix(0, 3) = "Descri��o"
        .Col = 4
        .ColWidth(4) = 750
        .TextMatrix(0, 4) = "Volume"
        .ColWidth(5) = 900
        .Col = 5
        .TextMatrix(0, 5) = "Previsto"
        .ColWidth(6) = 900
        .Col = 6
        .TextMatrix(0, 6) = "Chegada"
        .ColWidth(7) = 600
        .Col = 7
        .TextMatrix(0, 7) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    LastColP = 0
    LastRowP = 1
    CriaNovaClasse RegistosP, 1, "PROD", True

    TB_DefTipoFGTubos FGTubos
    LastColT = 0
    LastRowT = 1

    With FGRecibos
        .rows = 2
        .FixedRows = 1
        .Cols = 11
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285


        .ColWidth(0) = 1500

        .Col = lColRecibosNumDoc
        .TextMatrix(0, lColRecibosNumDoc) = "N�mero"
        .ColAlignment(lColRecibosNumDoc) = flexAlignLeftCenter

        .ColWidth(lColRecibosEntidade) = 2500
        .Col = lColRecibosEntidade
        .TextMatrix(0, lColRecibosEntidade) = "Entidade"

        .ColWidth(lColRecibosValorPagar) = 700
        .Col = lColRecibosValorPagar
        .TextMatrix(0, lColRecibosValorPagar) = "Valor �"

        .ColWidth(lColRecibosCaucao) = 0
        .Col = lColRecibosCaucao
        .TextMatrix(0, lColRecibosCaucao) = "Cau��o"

        .ColWidth(lColRecibosDtEmissao) = 1300
        .Col = lColRecibosDtEmissao
        .TextMatrix(0, lColRecibosDtEmissao) = "Data Emiss�o"

        .ColWidth(lColRecibosDtPagamento) = 1300
        .Col = lColRecibosDtPagamento
        .TextMatrix(0, lColRecibosDtPagamento) = "Data Pagamento"

        .ColWidth(lColRecibosEstado) = 1300
        .Col = lColRecibosEstado
        .TextMatrix(0, lColRecibosEstado) = "Estado"

        .ColWidth(lColRecibosPercentagem) = 900
        .Col = lColRecibosPercentagem
        .TextMatrix(0, lColRecibosPercentagem) = "Percent %"

        .ColWidth(lColRecibosModoPag) = 1200
        .Col = lColRecibosModoPag
        .TextMatrix(0, lColRecibosModoPag) = "Modo Pag."

        .ColWidth(lColRecibosBorla) = 800
        .Col = lColRecibosBorla
        .TextMatrix(0, lColRecibosBorla) = "Borla"

        .ColWidth(lColRecibosNaoConfmidade) = 800
        .Col = lColRecibosNaoConfmidade
        .TextMatrix(0, lColRecibosNaoConfmidade) = "Conform."

        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    With FgAdiantamentos
        .rows = 2
        .FixedRows = 1
        .Cols = 5
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285


        .ColWidth(0) = 1200

        .Col = lColAdiantamentosNumDoc
        .TextMatrix(0, lColAdiantamentosNumDoc) = "N�mero"
        .ColAlignment(lColAdiantamentosNumDoc) = flexAlignLeftCenter


        .ColWidth(lColAdiantamentosValor) = 1000
        .Col = lColAdiantamentosValor
        .TextMatrix(0, lColAdiantamentosValor) = "Valor �"


        .ColWidth(lColAdiantamentosDtEmissao) = 1500
        .Col = lColAdiantamentosDtEmissao
        .TextMatrix(0, lColAdiantamentosDtEmissao) = "Data Emiss�o"

        .ColWidth(lColAdiantamentosDtAnulacao) = 1500
        .Col = lColAdiantamentosDtAnulacao
        .TextMatrix(0, lColAdiantamentosDtAnulacao) = "Data Anula��o"


        .ColWidth(lColAdiantamentosEstado) = 2000
        .Col = lColAdiantamentosEstado
        .TextMatrix(0, lColAdiantamentosEstado) = "Estado"


        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    With FgAnaRec
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285


        .ColWidth(lColFgAnaRecCodAna) = 1000
        .Col = lColFgAnaRecCodAna
        .TextMatrix(0, lColFgAnaRecCodAna) = "C�d Ana"
        .ColAlignment(lColFgAnaRecCodAna) = flexAlignLeftCenter

        .ColWidth(lColFgAnaRecDescrAna) = 3000
        .Col = lColFgAnaRecDescrAna
        .TextMatrix(0, lColFgAnaRecDescrAna) = "An�lise"

        .ColWidth(lColFgAnaRecQtd) = 600
        .Col = lColFgAnaRecQtd
        .TextMatrix(0, lColFgAnaRecQtd) = "Qtd"

        .ColWidth(lColFgAnaRecTaxa) = 1000
        .Col = lColFgAnaRecTaxa
        .TextMatrix(0, lColFgAnaRecTaxa) = "Valor �"

        .row = 1
        .Col = 0
    End With



    EcAuxRec.MaxLength = 10



    Flg_PergProd = False
    Resp_PergProd = False
    Flg_DtChProd = False
    Resp_PergDtChProd = False

    Flg_PergTubo = False
    Resp_PergTubo = False
    Flg_DtChTubo = False
    Resp_PergDtChTubo = False


    ExecutaCodigoA = False
    ExecutaCodigoP = False


    SSTGestReq.Enabled = True
    BtAnularRecibo.Enabled = False
    BtCobranca.Enabled = False
    BtPagamento.Enabled = False
    BtGerarRecibo.Enabled = False
    BtResumo.Enabled = False

    BtImprimirRecibo.Enabled = False
    FrameAnaRecibos.Visible = False
    GereFrameAdiantamentos False

    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    ReDim reqColh(0)
    TotalReqColh = 0
    PM_LimpaEstrutura
    LimpaCamposAguas
Exit Sub
TrataErro:
    BG_LogFile_Erros "DefTipoCampos: " & Err.Number & " - " & Err.Description, Me.Name, "DefTipoCampos"
    Exit Sub
    Resume Next
End Sub


Sub PreencheValoresDefeito()
    On Error GoTo TrataErro

    EcNumColheita.text = ""
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, mediAscComboCodigo
    ' PFerreira 04.04.2007
    BG_PreencheComboBD_ADO "sl_tbf_t_destino", "cod_t_dest", "descr_t_dest", CbDestino, mediAscComboCodigo


    TotalEliminadas = 0
    ReDim Eliminadas(0)

    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)

    If gF_ENTREGA_RESULTADOS <> mediSim Then
        PreencheSalaDefeito
    End If
    Flg_IncrementaP1 = False

 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheValoresDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValoresDefeito"
    Exit Sub
    Resume Next
End Sub

Sub EventoActivate()
    On Error GoTo TrataErro

    Dim i As Integer

    If FormGestaoRequisicaoAguas.Enabled = False Then
        FormGestaoRequisicaoAguas.Enabled = True
    End If
    If flgEmissaoRecibos = True And FGRecibos.rows > 2 Then
        PreencheFgRecibos
        PreencheFgAdiantamentos
        flgEmissaoRecibos = False
    End If
    Set gFormActivo = Me

    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind

        Max = -1
        ReDim FOPropertiesTemp(0)
    End If

    BG_StackJanelas_Actualiza Me

    BL_ToolbarEstadoN Estado

    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BG_ParametrizaPermissoes_ADO Me.Name

    If Estado = 2 Then
       BL_Toolbar_BotaoEstado "Remover", "Activo"

        Err.Clear

        If EcEstadoReq = gEstadoReqCancelada Then

            Select Case Err.Number
                Case 0
                    ' OK
                Case 3021
                    ' BOF ou EOF � verdadeiro ou o registo actual foi eliminado; a opera��o pedida necessita de um registo actual.
                    rs.Requery
                Case Else

            End Select

            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            BtCancelarReq.Visible = False
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
        End If
    End If

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

    Dim RsPesqUte As ADODB.recordset
    Dim sql As String
    If gDUtente.seq_utente <> "" And (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        If gDUtente.seq_utente = "-1" And gF_IDENTIF = 0 Then
'            ' UTENTE HIS
            Set RsPesqUte = New ADODB.recordset
            With RsPesqUte
                .Source = "SELECT * from sl_identif where dt_nasc_ute = " & BL_TrataDataParaBD(gDUtente.dt_nasc_ute) & _
                        " and nome_ute = " & BL_TrataStringParaBD(gDUtente.nome_ute) & _
                        " and sexo_ute = " & BL_TrataStringParaBD(gDUtente.sexo_ute) & _
                        " and t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & _
                        " and utente = " & BL_TrataStringParaBD(gDUtente.Utente)
                .CursorType = adOpenStatic
                .CursorLocation = adUseServer
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open , gConexao
            End With
            If RsPesqUte.RecordCount > 0 Then
                EcSeqUtente = RsPesqUte!seq_utente
                PreencheDadosUtente
            Else
                BD_Insert_Utente
            End If
        ElseIf gDUtente.seq_utente = "-1" And gF_IDENTIF = 1 Then
            FormIdentificaUtente.Flg_PesqUtente = True
            Unload Me
            Exit Sub
        Else
            'j� tem utente
            EcSeqUtente = gDUtente.seq_utente
            PreencheDadosUtente
            Flg_PesqUtente = False
        End If
        Flg_PesqUtente = False

    End If
    '-------------------------------------------------------------------



    If EcNumReq <> "" Then
        PreencheNotas
    End If

    LaFichaCruz.caption = "Etiquetas"

'Coloca codigo da sala por defeito

    If gCodSalaDefeito <> "-1" Then

        If EcCodSala.text = "" Then

            EcCodSala.text = gCodSalaDefeito

        End If

    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheValoresDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValoresDefeito"
    Exit Sub
    Resume Next
End Sub

Sub PreencheDadosUtente()
    On Error GoTo TrataErro

    Dim Campos(6) As String
    Dim retorno() As String
    Dim iret As Integer

    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "dt_nasc_ute"
    Campos(4) = "sexo_ute"
    Campos(5) = "abrev_ute"

    iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)

    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.text = ""
        Exit Sub
    End If

    EcUtente.text = retorno(0)
    CbTipoUtente.text = retorno(1)
    EcNome.text = retorno(2)
    EcDataNasc.text = retorno(3)
    If retorno(4) = gT_Masculino Then
        EcSexo = "Masculino"
    Else
        EcSexo = "Feminino"
    End If
    EcAbrevUte.text = retorno(5)
    If EcDataNasc <> "" Then
        EcIdade = BG_CalculaIdade(CDate(EcDataNasc.text))
    End If
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim And Me.ExternalAccess = True Then
    Dim contextJson As String
    Dim responseJson As String
    Dim requestJson As String
   
        contextJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & Chr(34) & "," & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & "}"
        
        responseJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & ", " & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & _
                ", " & Chr(34) & "EcNome" & Chr(34) & ":" & Chr(34) & retorno(2) & Chr(34) & ", " & Chr(34) & "EcDataNasc" & Chr(34) & ":" & Chr(34) & retorno(3) & Chr(34) & _
                ", " & Chr(34) & "EcSexo" & Chr(34) & ":" & Chr(34) & EcSexo & Chr(34) & ", " & Chr(34) & "EcAbrevUte" & Chr(34) & ":" & Chr(34) & retorno(5) & Chr(34) & "}"
        
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, contextJson, contextJson, IIf(responseJson = vbNullString, "{}", responseJson)

    End If

    BtConsReq.Enabled = True
    BtIdentif.Enabled = True

 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosUtente: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosUtente"
    Exit Sub
    Resume Next
End Sub

Sub PreencheEFRUtente()

    On Error GoTo ErrorHandler

    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim SQLEFR As String
    Dim tabelaEFR As ADODB.recordset
    Dim SelEntUte As Boolean

'    If (Trim(EcCodEFR.Text) = "") Then

        SelEntUte = False
        If Trim(EcSeqUtente) <> "" Then
            If rs Is Nothing Then
                SelEntUte = True
            ElseIf rs.state = adStateClosed Then
                SelEntUte = True
            End If
        End If

        If SelEntUte = True Then
            Campos(0) = "cod_efr_ute"
            Campos(1) = "n_benef_ute"
            iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)

            If iret = -1 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar E.F.R. do utente!", vbCritical + vbOKOnly, App.ProductName
                Exit Sub
            End If

            EcCodEFR.text = retorno(0)



            Set tabelaEFR = New ADODB.recordset
            tabelaEFR.CursorType = adOpenStatic
            tabelaEFR.CursorLocation = adUseServer

            SQLEFR = "SELECT " & _
                     "      descr_efr " & _
                     "FROM " & _
                     "      sl_efr " & _
                     "WHERE " & _
                     "      cod_efr = " & Trim(EcCodEFR.text)

            If gModoDebug = mediSim Then BG_LogFile_Erros SQLEFR
            tabelaEFR.Open SQLEFR, gConexao
            If tabelaEFR.RecordCount > 0 Then
                EcDescrEFR.text = tabelaEFR!descr_efr
            End If

            tabelaEFR.Close
            Set tabelaEFR = Nothing
        End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : PreencheEFRUtente (FormGestaoRequisicaoAguas) -> " & Err.Description
            Exit Sub
    End Select
End Sub
Sub EventoUnload()
    On Error GoTo TrataErro

    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0


    ' Descarregar todos os objectos
    Set RegistosP = Nothing

    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then
            rs.Close
        End If
        Set rs = Nothing
    End If

    Call BL_FechaPreview("Isencao")
    Call BL_FechaPreview("Recibo Requisi��o")

    Set FormGestaoRequisicaoAguas = Nothing

    Call BG_RollbackTransaction


    gF_REQUIS_AGUAS = 0

    If (gFormGesReqCons_Aberto = True) Then
        Flg_PesqUtente = False
        ' Quando este form foi invocado atrav�s de FormGesReqCons.
        gFormGesReqCons_Aberto = False
        Set gFormActivo = FormGesReqCons
        FormGesReqCons.Enabled = True
    ElseIf gF_IDENTIF = 1 Then
        FormIdentificaUtente.Enabled = True
        Set gFormActivo = FormIdentificaUtente
    ElseIf gF_FILA_ESPERA = mediSim Then
        FormFilaEspera.Enabled = True
        Set gFormActivo = FormFilaEspera
    Else
        Set gFormActivo = MDIFormInicio
    End If

 Exit Sub
TrataErro:
    BG_LogFile_Erros "EventoUnload: " & Err.Number & " - " & Err.Description, Me.Name, "EventoUnload"
    Exit Sub
    Resume Next
End Sub

Sub SELECT_Dados_Perfil(Marcar() As String, indice As Long, CodPerfil As String, CodFacturavel As String, multiAna As String)
    On Error GoTo TrataErro

    Dim RsPerf As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim JaExiste As Boolean
    Dim flgIncrmenta As Boolean
    Dim qtd As Integer
    Dim qtdMapeada As String
    Dim flg_FdsValFixo   As Integer
    CodPerfil = UCase(Trim(CodPerfil))

    'Activo ?
    sql = "SELECT descr_perfis, flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
    Set RsPerf = New ADODB.recordset
    With RsPerf
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With

    Activo = "-1"
    If RsPerf.RecordCount > 0 Then
        Activo = BL_HandleNull(RsPerf!flg_activo, "0")
        descricao = BL_HandleNull(RsPerf!descr_perfis, " ")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Perfil: Erro a seleccionar dados do perfil!"
    End If
    RsPerf.Close
    Set RsPerf = Nothing

    'Membros do perfil
    sql = "SELECT cod_analise, ordem FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)

    Set RsPerf = New ADODB.recordset
    With RsPerf
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With

    If RsPerf.RecordCount <> 0 Then
        If Activo = "1" Then
            LbTotalAna = LbTotalAna + 1
            'soliveira 03.12.2007 Acrescentar analise "C99999" para descri��o do perfil no ecra de resultados
            BL_Preenche_MAReq FGTubos, CodPerfil, descricao, "C99999", " ", gGHOSTMEMBER_S, " ", indice, -1, -1, CodPerfil, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDtColheita
        End If
    Else
        BG_Mensagem mediMsgBox, "Perfil '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
    End If

    'SE Perfil MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
    qtdMapeada = RetornaQtd(CodPerfil, EcCodEFR)
    If qtdMapeada <> "" Then
        qtd = qtdMapeada
        flgIncrmenta = False
    Else
        If IF_ContaMembros(CodPerfil, EcCodEFR) > 0 Then
            qtd = 0
            flgIncrmenta = True
        Else
            qtd = 1
            flgIncrmenta = False
        End If
    End If

    FGAna.CellBackColor = vbWhite
    FGAna.CellForeColor = vbBlack
    While Not RsPerf.EOF
        JaExiste = Verifica_Ana_ja_Existe(indice, BL_HandleNull(RsPerf!cod_analise, " "))

        If Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "S" Then
            If Activo = "1" Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), , , , CodFacturavel
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    If SELECT_Dados_Simples(Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, , , , , , , CodFacturavel) = False Then
                          indice = indice - 1
                    End If
                End If
            End If
        ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "C" Then
            If Activo = "1" Then
                SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), CodFacturavel
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, , , , CodFacturavel
                End If
            End If
        ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "P" Then
            If Activo = "1" Then
                SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodFacturavel, multiAna
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodFacturavel, multiAna
                End If
            End If
        End If
        RsPerf.MoveNext

        If Not RsPerf.EOF And Activo = "0" And JaExiste = False Then
            ExecutaCodigoA = False
            indice = indice + 1
            FGAna.AddItem "", indice
            Insere_aMeio_RegistosA FGAna.row + 1
            FGAna.row = FGAna.row + 1
            FGAna.Col = 0
            ExecutaCodigoA = True
        End If
        If flgIncrmenta = True Then
            qtd = qtd + 1
        End If
    Wend

    If flgIncrmenta = True Then
        FlgIncrementa indice, qtd
    End If
    RsPerf.Close
    Set RsPerf = Nothing
 Exit Sub
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Perfil: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Perfil"
    Exit Sub
    Resume Next
End Sub

Sub SELECT_Dados_Complexa(Marcar() As String, indice As Long, CodComplexa As String, multiAna As String, Optional Perfil As Variant, _
                          Optional DescrPerfil As Variant, Optional OrdemP As Variant, Optional CodFacturavel As String)
    On Error GoTo TrataErro

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim qtd As Integer
    Dim flgIncrmenta As Boolean
    Dim qtdMapeada As String
    Dim flg_FdsValFixo   As Integer
    'Activo ?
    sql = "SELECT descr_ana_c, flg_marc_memb " & _
          "FROM sl_ana_c " & _
          "WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)

    Set RsCompl = New ADODB.recordset

    With RsCompl
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With

    Activo = "-1"
    If RsCompl.RecordCount > 0 Then
        Activo = BL_HandleNull(RsCompl!flg_marc_memb, "0")
        descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
    End If
    RsCompl.Close
    Set RsCompl = Nothing


    If Activo = "1" Then
        'soliveira teste
        If Not IsMissing(Perfil) Then
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, , , , CodComplexa, descricao, -1, CodFacturavel
        End If

        'Membros da complexa
        sql = "SELECT cod_membro, ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND t_membro = 'A' ORDER BY ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With

        If RsCompl.RecordCount = 0 Then
            BG_Mensagem mediMsgBox, "Complexa '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
        Else
        End If

        'SE COMPLEXA MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
        qtdMapeada = RetornaQtd(CodComplexa, EcCodEFR)
        If qtdMapeada <> "" Then
            qtd = qtdMapeada
            flgIncrmenta = False
        Else
            If IF_ContaMembros(CodComplexa, EcCodEFR) > 0 Then
                qtd = 0
                flgIncrmenta = True
            Else
                qtd = 1
                flgIncrmenta = False
            End If
        End If


        While Not RsCompl.EOF
            If Not IsMissing(Perfil) Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), CodFacturavel
            Else
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), CodFacturavel
            End If
            RsCompl.MoveNext

            If flgIncrmenta = True Then
                qtd = qtd + 1
            End If
        Wend
        If flgIncrmenta = True Then
            FlgIncrementa indice, qtd
        End If
    Else
        If Not IsMissing(Perfil) Then
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, , , , CodComplexa, descricao, -1, CodFacturavel
        End If

        'Membros da complexa
        sql = "SELECT cod_membro, sl_membro.ordem FROM sl_membro,sl_ana_s " & _
            " WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND cod_ana_s = cod_membro " & _
            " AND flg_marc_auto = '1' " & _
            " AND t_membro = 'A' ORDER BY sl_membro.ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With

        If Not RsCompl.EOF Then
            While Not RsCompl.EOF

                If Not IsMissing(Perfil) Then
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                Else
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                End If
                RsCompl.MoveNext

            Wend
            If flgIncrmenta = True Then
                FlgIncrementa indice, qtd
            End If
        Else
            If IsMissing(Perfil) Then
                GhostPerfil = "0"
                GhostComplexa = CodComplexa
                Lista_GhostMembers GhostComplexa, descricao
            End If
        End If

    End If
    RsCompl.Close
    Set RsCompl = Nothing
 Exit Sub
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Complexa: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Complexa"
    Exit Sub
    Resume Next
End Sub

Function SELECT_Dados_Simples(Marcar() As String, indice As Long, CodSimples As String, multiAna As String, Optional Perfil As Variant, Optional DescrPerfil As Variant, Optional OrdemP As Variant, Optional complexa As Variant, Optional DescrComplexa As Variant, Optional OrdemC As Variant, Optional CodFacturavel As String) As Boolean
    On Error GoTo TrataErro
    Dim PerfilFact As Integer
    Dim RsSimpl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim codAgrup As String
    Dim descrAgrup As String
    Dim i As Integer
    Dim NaoMarcar As Boolean
    Dim taxa As String
    Dim codAnaEfr As String
    Dim mens As String
    Dim p1 As Long
    Dim corP1 As String
    Dim AnaRegra As String
    Dim anaFacturar As String
    Dim flg_FdsValFixo As Integer
    Dim iRec As Long
    SELECT_Dados_Simples = True

    If CodSimples = gGHOSTMEMBER_S Then
        If Not IsMissing(complexa) Then
            If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDtColheita
            Else
                codAgrup = complexa
                descrAgrup = DescrComplexa

                BL_Preenche_MAReq FGTubos, "0", " ", complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDtColheita
            End If
        End If
    Else

        sql = "SELECT descr_ana_s, flg_invisivel FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(CodSimples)
        Set RsSimpl = New ADODB.recordset
        With RsSimpl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With

        If RsSimpl.RecordCount > 0 Then
            descricao = BL_HandleNull(RsSimpl!descr_ana_s, " ")
            'N�o marcar a an�lise se flg_invisivel = 1
            If RsSimpl!flg_invisivel = 1 Then
                RsSimpl.Close
                Set RsSimpl = Nothing
                SELECT_Dados_Simples = False
                Exit Function
            End If
        End If
        RsSimpl.Close
        Set RsSimpl = Nothing

        If Not IsMissing(complexa) Then
             If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDtColheita
             Else
                codAgrup = complexa
                descrAgrup = DescrComplexa

                BL_Preenche_MAReq FGTubos, "0", " ", complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDtColheita
             End If
        Else
             If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil

                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, "0", " ", CodSimples, descricao, indice, 0, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDtColheita
             Else
                LbTotalAna = LbTotalAna + 1
                codAgrup = CodSimples
                descrAgrup = descricao

                BL_Preenche_MAReq FGTubos, "0", " ", "0", " ", CodSimples, descricao, indice, 0, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDtColheita
             End If
        End If

    End If

    If Verifica_Ana_ja_Existe(0, codAgrup) = False Then
        If Trim(RegistosA(indice).codAna) = "" Then

            TotalAcrescentadas = TotalAcrescentadas + 1
            ReDim Preserve Acrescentadas(TotalAcrescentadas)
            Acrescentadas(TotalAcrescentadas).cod_agrup = codAgrup

            RegistosA(indice).codAna = codAgrup
            RegistosA(indice).descrAna = descrAgrup
            RegistosA(indice).Estado = "-1"
            RegistosA(indice).ObsAnaReq = ""
            RegistosA(indice).seqObsAnaReq = 0
            RegistosA(indice).multiAna = multiAna

            ' ------------------------------------------------------------------------------------------
            ' PARA ANALISES QUE NAOOOOO SAO MARCADAS DENTRO DE NENHUM PERFIL DE MARCACAO
            ' ------------------------------------------------------------------------------------------
            If RegistosA(indice).codAna <> "" And flg_codBarras = False Then
                If RegistosA(indice).codAna <> BL_HandleNull(UCase(CodFacturavel), "0") And Mid(UCase(CodFacturavel), 1, 1) = "P" Then
                    PerfilFact = BL_HandleNull(BL_SelCodigo("SL_PERFIS", "flg_nao_facturar", "cod_perfis", UCase(CodFacturavel), "V"), 0)
                    If PerfilFact = 0 Then
                        ' PERFIL DE MARCACAO. COMO TAL VERIFICAR SE JA FOI MARCADO
                        If VerificaPerfilMarcacaoJaMarcado(BL_HandleNull(UCase(CodFacturavel), "0")) = False Then
                            AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", BL_HandleNull(UCase(CodFacturavel), "0"), False
                        End If
                    Else
                        AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(RegistosA(indice).codAna), "0"), 1, "", RegistosA(indice).codAna, False
                    End If
                Else
                    AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", RegistosA(indice).codAna, False
                End If
            End If

            If indice <> 1 Then
                If Trim(RegistosA(indice - 1).NReqARS) <> "" Then
                    RegistosA(indice).NReqARS = RegistosA(indice - 1).NReqARS
                Else
                    If Trim(EcNumReq) <> "" Then
                        RegistosA(indice).NReqARS = EcNumReq
                    Else
                        RegistosA(indice).NReqARS = "A gerar"
                    End If
                End If
            Else
                If Trim(EcNumReq) <> "" Then
                    RegistosA(indice).NReqARS = EcNumReq
                Else
                    RegistosA(indice).NReqARS = "A gerar"
                End If
            End If
            iRec = DevIndice(codAgrup)
            FGAna.TextMatrix(indice, lColFgAnaCodigo) = codAgrup
            FGAna.TextMatrix(indice, lColFgAnaDescricao) = descrAgrup
            If iRec > -1 Then
                FGAna.TextMatrix(indice, lColFgAnaP1) = RegistosRM(iRec).ReciboP1
                FGAna.TextMatrix(indice, lColFgAnaMedico) = RegistosRM(iRec).ReciboCodMedico
                FGAna.TextMatrix(indice, lColFgAnaCodEFR) = RegistosRM(iRec).ReciboEntidade
                FGAna.TextMatrix(indice, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(iRec).ReciboEntidade)
                FGAna.TextMatrix(indice, lColFgAnaNrBenef) = RegistosRM(iRec).ReciboNrBenef
                FGAna.TextMatrix(indice, lColFgAnaPreco) = RegistosRM(iRec).ReciboTaxa
            End If
            FGAna.row = indice
            FGAna.Col = 1
            If multiAna <> "" Then
                FGAna.CellBackColor = laranja
            Else
                FGAna.CellBackColor = vbWhite
            End If

        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Simples: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Simples"
    SELECT_Dados_Simples = False
    Exit Function
    Resume Next
End Function

Function SELECT_Descr_Ana(codAna As String) As String
    On Error GoTo TrataErro

    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    Dim Flg_SemTipo As Boolean

    codAna = UCase(codAna)
    If InStr(1, "SCP", Mid(codAna, 1, 1)) = 0 Then
        Flg_SemTipo = True
    End If

    With rsDescr
        If Flg_SemTipo = True Then
            .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                            "cod_ana_s ='S" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                            "cod_ana_c='C" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_perfis as descricao, 'P' Tipo, null inibe_marcacao FROM sl_perfis WHERE " & _
                            "cod_perfis='P" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND (cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") OR flg_activo = '0'))"

        Else
            .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                            "cod_ana_s ='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                            "cod_ana_c='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (Select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")) " & _
                        " UNION " & _
                            "(SELECT descr_perfis as descricao, 'P' Tipo,null inibe_marcacao FROM sl_perfis WHERE " & _
                            "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND (cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") OR flg_activo = '0'))"


        End If
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With

    If rsDescr.RecordCount <= 0 Then
        SELECT_Descr_Ana = ""
    Else
        If BL_HandleNull(rsDescr!inibe_marcacao, "0") = "1" Then
            SELECT_Descr_Ana = ""
        Else
            SELECT_Descr_Ana = BL_HandleNull(rsDescr!descricao, "")
            If Flg_SemTipo = True Then
                codAna = rsDescr!tipo & codAna & ""
            End If
        End If
    End If

    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Descr_Ana: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Descr_Ana"
    SELECT_Descr_Ana = ""
    Exit Function
    Resume Next
End Function

Function Verifica_Ana_Prod(codAgrup As String, ByRef Marcar() As String) As Boolean
    On Error GoTo TrataErro

    Dim RsAnaProd As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim k As Integer
    Dim CodProduto() As String
    Dim Resp As Integer
    Dim ListaProd() As String
    Dim JaTemProduto() As String
    Dim TotJaTemProduto As Integer
    Dim SQLAux As String
    Dim RsPerf As New ADODB.recordset

    gColocaDataChegada = ""
    codAgrup = UCase(Trim(codAgrup))

    'PERFIL Activo ?
    If Mid(codAgrup, 1, 1) = "P" Then
        sql = "SELECT  flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
        Set RsPerf = New ADODB.recordset
        With RsPerf
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With

        If RsPerf.RecordCount > 0 Then
            If BL_HandleNull(RsPerf!flg_activo, 0) = 0 Then
                RsPerf.Close
                sql = "SELECT * FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
                With RsPerf
                    .Source = sql
                    .CursorLocation = adUseServer
                    .CursorType = adOpenStatic
                    If gModoDebug = mediSim Then BG_LogFile_Erros sql
                    .Open , gConexao
                End With
                If RsPerf.RecordCount > 0 Then
                    While Not RsPerf.EOF
                        Verifica_Ana_Prod BL_HandleNull(RsPerf!cod_analise, ""), Marcar
                        RsPerf.MoveNext
                    Wend
                End If
            End If
        End If
        RsPerf.Close
        Set RsPerf = Nothing

    End If

'----------- PROCURAR NOS PRODUTOS JA MARCADOS, OS PRODUTOS DA ANALISE

sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
    " FROM sl_ana_s a LEFT OUTER JOIN  sl_produto p  ON a.cod_produto = p.cod_produto  " & _
    " Where " & _
    " Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
    " Union " & _
    " SELECT ac.cod_ana_c cod_ana_s, ac.descr_ana_c descr_ana_s, ac.cod_produto, p.descr_produto " & _
    " FROM sl_ana_c ac LEFT OUTER JOIN  sl_produto p  ON ac.cod_produto = p.cod_produto  " & _
    " Where  " & _
    " ac.Cod_Ana_c in (select cod_analise from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup) & ")" & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
    " FROM sl_ana_s a LEFT OUTER JOIN  sl_produto p  ON a.cod_produto = p.cod_produto   " & _
    " WHERE  " & _
    " cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
    " AND t_membro = 'A' )" & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
    " FROM sl_ana_s a LEFT OUTER JOIN  sl_produto p  ON a.cod_produto = p.cod_produto  " & _
    " WHERE  " & _
    " cod_ana_s IN " & _
    "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
    " AND cod_analise LIKE 'S%' ) Union " & _
    " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
    " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' )) "

SQLAux = " select cod_perfis cod_ana_s, descr_perfis descr_ana_s, sl_perfis.cod_produto,descr_produto from sl_perfis, sl_produto where " & _
          " sl_perfis.cod_produto = sl_produto.cod_produto and cod_perfis = " & BL_TrataStringParaBD(codAgrup)

Set RsAnaProd = New ADODB.recordset
With RsAnaProd
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    .Source = SQLAux
    .ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
    .Open
End With


Verifica_Ana_Prod = False

ReDim JaTemProduto(0)
TotJaTemProduto = 0
ReDim ListaProd(0)
ReDim Marcar(0)
k = 0
If RsAnaProd.RecordCount <= 0 Then
    SQLAux = " select cod_ana_c cod_ana_s, descr_ana_c descr_ana_s, sl_ana_c.cod_produto,descr_produto from sl_ana_c, sl_produto where " & _
              " sl_ana_c.cod_produto = sl_produto.cod_produto and cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    Set RsAnaProd = New ADODB.recordset
    With RsAnaProd
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Source = SQLAux
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
        .Open
    End With
    If RsAnaProd.RecordCount <= 0 Then
        Set RsAnaProd = New ADODB.recordset
        With RsAnaProd
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .Source = sql
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open
        End With
    End If
End If

While Not RsAnaProd.EOF
    If Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) <> "" Then
        For i = 1 To RegistosP.Count - 1
            If Trim(RegistosP(i).CodProd) = Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) Then
                Verifica_Ana_Prod = True
                gColocaDataChegada = RegistosP(i).DtChega
                Exit For
            Else
                Verifica_Ana_Prod = False
            End If
        Next i

        If Verifica_Ana_Prod = False Then
            k = k + 1
            ReDim Preserve ListaProd(k)
            ListaProd(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";" & BL_HandleNull(RsAnaProd!cod_produto, "") & ";" & BL_HandleNull(RsAnaProd!descr_produto, "") & Space(35 - Len(Trim(BL_HandleNull(RsAnaProd!descr_produto, "")))) & " -> " & Trim(BL_HandleNull(RsAnaProd!descr_ana_s, ""))
        Else
            TotJaTemProduto = TotJaTemProduto + 1
            ReDim Preserve JaTemProduto(TotJaTemProduto)
            JaTemProduto(TotJaTemProduto) = BL_HandleNull(RsAnaProd!cod_ana_s, "")
        End If
    End If

    RsAnaProd.MoveNext
Wend

'----------- ALGUM NAO ESTA NA LISTA

If k <> 0 Then

    Verifica_Ana_Prod = False

    'Pergunta 1: Quer marcar o produto da analise ?

    If Flg_PergProd = True Then
        'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
        'que se deve adoptar a ultima resposta � pergunta

        If Resp_PergProd = True Then
            'respondeu na ultima vez que sim � pergunta 1
            Verifica_Ana_Prod = True

            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?

            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                k = 0
                ReDim Marcar(0)
                For i = 1 To UBound(ListaProd)
                    CodProduto = Split(ListaProd(i), ";")
                    k = k + 1
                    ReDim Preserve Marcar(k)
                    Marcar(k) = CodProduto(0) & ";" & CodProduto(1)
                Next i

                'FormGestaoRequisicaoAguas.Enabled = False

                If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If

            End If
        Else
            'respondeu na ultima vez que n�o � pergunta 1
            Verifica_Ana_Prod = True
        End If
    Else
        'O utilizador ainda n�o escolheu que a pergunta n�o
        'deve ser efectuada de novo, logo questiona-se a pergunta 1

        'FormGestaoRequisicaoAguas.Enabled = False

        If FormPergProd.PerguntaProduto(ConfirmarDefeito, Flg_PergProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
            'respondeu sim � pergunta 1
            Resp_PergProd = True
            Verifica_Ana_Prod = True

            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?

            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                If gRegChegaTubos <> 1 Then
                    If gIgnoraPerguntaProdutoTubo = 1 Then
                        For i = 1 To UBound(Marcar)
                            CodProduto = Split(Marcar(i), ";")
                            InsereAutoProd CodProduto(1), dataAct
                        Next i
                        Flg_PreencherDtChega = True
                    Else
                        'O utilizador ainda n�o escolheu que a pergunta n�o
                        'deve ser efectuada de novo, logo questiona-se a pergunta 2
                        If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                            'respondeu sim � pergunta 2
                            Flg_PreencherDtChega = True
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), dataAct
                            Next i
                        Else
                            'respondeu n�o � pergunta 2
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), ""
                            Next i
                            Flg_PreencherDtChega = False
                        End If
                    End If
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
            End If
        Else
            'respondeu n�o � pergunta 1
            Resp_PergProd = False
        End If

    End If

    ' retirar do array de an�lises a marcar o codigo do produto (fica "S1;S2;S3;S4;")
    For i = 1 To UBound(Marcar)
        CodProduto = Split(Marcar(i))
        Marcar(i) = CodProduto(0) & ";"
    Next i

    '----------- PERMITIR MARCAR AS ANALISES QUE NAO TEM PRODUTO ASSOCIADO

    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = UBound(Marcar)
    While Not RsAnaProd.EOF
        If BL_HandleNull(RsAnaProd!cod_produto, "") = "" Then
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
            If gColocaDataChegada = "" Then gColocaDataChegada = dataAct
            Verifica_Ana_Prod = True
        End If
        RsAnaProd.MoveNext
    Wend

    '----------- MARCAR AS ANALISES QUE J� TINHAM O PRODUTO REGISTADO

    k = UBound(Marcar)
    If TotJaTemProduto <> 0 Then
        For i = 1 To TotJaTemProduto
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = JaTemProduto(i) & ";"
        Next i
    End If

Else
    Verifica_Ana_Prod = True

    'Marca todas
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = 0
    ReDim Marcar(0)
    If Not RsAnaProd.EOF Then
        If gColocaDataChegada = "" And EcDataChegada.text <> "" Then    'sdo em vez de: If gColocaDataChegada = "" Then gColocaDataChegada = dataAct
            gColocaDataChegada = EcDataChegada.text                     'sdo
        ElseIf EcDataChegada.text = "" Then                             'sdo
            gColocaDataChegada = ""                                     'sdo
        End If                                                          'sdo
    End If
    While Not RsAnaProd.EOF
        k = k + 1
        ReDim Preserve Marcar(k)
        Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
        RsAnaProd.MoveNext
    Wend

End If

RsAnaProd.Close
Set RsAnaProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_Prod"
    Verifica_Ana_Prod = False
    Exit Function
    Resume Next
End Function

Private Sub BtAdiantamentos_Click()
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    GereFrameAdiantamentos True
End Sub



Private Sub BtAnularAdiamento_Click()
    If FgAdiantamentos.row < FgAdiantamentos.rows - 1 And FgAdiantamentos.row > 0 Then
        If RegistosAD(FgAdiantamentos.row).NumAdiantamento > 0 Then
            If RegistosAD(FgAdiantamentos.row).UserAnul = "" And RegistosAD(FgAdiantamentos.row).DtAnul = "" Then
                gMsgTitulo = "Adiantamento"
                gMsgMsg = "Tem a certeza que quer devolver o adiantamento: " & RegistosAD(FgAdiantamentos.row).NumAdiantamento & "?"
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp = vbYes Then
                    If RegistosAD(FgAdiantamentos.row).UserAnul = "" And RegistosAD(FgAdiantamentos.row).DtAnul = "" Then
                        RECIBO_AnulaAdiantamento CStr(EcNumReq), FgAdiantamentos.row, RegistosAD
                        PreencheFgAdiantamentos
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub BtAnularRecibo_Click()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
        FormGestaoRequisicaoAguas.Enabled = False
        FormCancelarRecibos.Show
    ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Then
        FormGestaoRequisicaoAguas.Enabled = False
        FormCancelarRecibos.Show
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtAnularRecibo_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtAnularRecibo_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtCancelarReq_Click()
    On Error GoTo TrataErro

    Max = UBound(gFieldObjectProperties)
    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind

    MarcaLocal = rs.Bookmark
    FormGestaoRequisicaoAguas.Enabled = False
    FormCancelarRequisicoes.Show
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtCancelarReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtCancelarReq_Click"
    Exit Sub
    Resume Next
End Sub




Private Sub BtCobranca_Click()
    On Error GoTo TrataErro

    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If

    ' ------------------------------------------------------------------------------------------
    ' COLOCA RECIBO PARA COBRAN�A
    ' ------------------------------------------------------------------------------------------
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
        RegistosR(FGRecibos.row).DtPagamento = ""
        RegistosR(FGRecibos.row).HrPagamento = ""
        RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca

        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosDtPagamento) = ""
        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Em Cobran�a"
        BL_MudaCorFg FGRecibos, FGRecibos.row, Amarelo
        ColocaCobrancaPago FGRecibos.row, RegistosR(FGRecibos.row).SerieDoc, CStr(RegistosR(FGRecibos.row).NumDoc), 1
        BtPagamento.Enabled = True
        BtCobranca.Enabled = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtCobranca_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtCobranca_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtConfirm_Click()
    If EcNumReq <> "" And EcUtilConf = "" And EcDtConf = "" And EcHrConf = "" Then
        EcUtilConf = gCodUtilizador
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcDtConf = dataAct
        EcHrConf = Bg_DaHora_ADO
        BD_Update
    End If
End Sub

Private Sub BtConsReq_Click()
    On Error GoTo TrataErro
    If EcSeqUtente.text <> "" Then
        FormGestaoRequisicaoAguas.Enabled = False
        'gRequisicaoActiva = EcNumReq
        BL_LimpaDadosUtente
        gDUtente.seq_utente = EcSeqUtente
        FormReqUte.Show
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtConsReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtConsReq_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtFgAnaRecRecalcula_Click()
    On Error GoTo TrataErro
    Dim i As Long
    Dim taxa As Double
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo Or _
        RegistosR(FGRecibos.row).Estado = gEstadoReciboPerdido Then
        taxa = 0
        For i = 1 To TotalAnaRecibo
            taxa = taxa + EstrutAnaRecibo(i).taxa
        Next
        RegistosR(FGRecibos.row).ValorPagar = taxa
        RegistosR(FGRecibos.row).ValorOriginal = taxa
        RegistosR(FGRecibos.row).NumAnalises = TotalAnaRecibo
        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = taxa

        If taxa = 0 And TotalAnaRecibo = 0 Then
            FGRecibos.RemoveItem FGRecibos.row
            LimpaColeccao RegistosR, FGRecibos.row

        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFgAnaRecRecalcula_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFgAnaRecRecalcula_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtFgAnaRecSair_Click()
    On Error GoTo TrataErro
    FrameAnaRecibos.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFgAnaRecSair_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFgAnaRecSair_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtFichaCruzada_Click()
    On Error GoTo TrataErro
    If gUsaNovaImpEtiquetas = mediSim Then
        ImprimeEtiqNovo
    Else
        ImprimeEtiq
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFichaCruzada_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFichaCruzada_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtIdentif_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 0 To 20
        If StackJanelas(i).Name = "FormIdentificaUtente" Then
            Exit Sub
        End If
    Next
    If EcNumReq <> "" Then
        FormIdentificaUtente.Show
        Set gFormActivo = FormIdentificaUtente
        FormIdentificaUtente.EcCodSequencial = EcSeqUtente
        FormIdentificaUtente.FuncaoProcurar
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtIdentif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtIdentif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtImprimirAdiantamento_Click()
    If FgAdiantamentos.row < FgAdiantamentos.rows - 1 And FgAdiantamentos.row > 0 Then
        If RegistosAD(FgAdiantamentos.row).NumAdiantamento > 0 Then
            RECIBO_ImprimeAdiantamento EcNumReq, RegistosAD(FgAdiantamentos.row).NumAdiantamento, EcPrinterRecibo
        End If
    End If
End Sub

Public Sub BtImprimirRecibo_Click()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    If gUsaCopiaRecibo <> mediSim Then
        ImprimeRecibo ""
    Else
        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_IMPR_REC
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtImprimirRecibo_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtImprimirRecibo_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotas_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicaoAguas.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcNumReq = EcNumReq
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotas_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotas_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotasVRM_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicaoAguas.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcNumReq = EcNumReq
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotasVRM_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotasVRM_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtPagamento_Click()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If

    ' ------------------------------------------------------------------------------------------
    ' COLOCA RECIBO COMO PAGO
    ' ------------------------------------------------------------------------------------------
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Then
        'RegistosR(FGRecibos.Row).us = dataAct
        RegistosR(FGRecibos.row).UserEmi = gCodUtilizador
        RegistosR(FGRecibos.row).DtPagamento = dataAct
        RegistosR(FGRecibos.row).HrPagamento = Bg_DaHora_ADO
        RegistosR(FGRecibos.row).Estado = gEstadoReciboPago

        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosDtPagamento) = RegistosR(FGRecibos.row).DtPagamento & " " & RegistosR(FGRecibos.row).HrPagamento
        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Pago"
        BL_MudaCorFg FGRecibos, FGRecibos.row, Verde

        ColocaCobrancaPago FGRecibos.row, RegistosR(FGRecibos.row).SerieDoc, CStr(RegistosR(FGRecibos.row).NumDoc), 0
        BtPagamento.Enabled = False
        BtCobranca.Enabled = True
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPagamento_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPagamento_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo TrataErro
    FGAna.Col = 0
    FGAna.row = FGAna.rows - 1
    ExecutaCodigoA = True
    If Button = 1 Then
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = 0 Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAMARC
        End If
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaAna_MouseDown: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaAna_MouseDown"
    Exit Sub
    Resume Next
End Sub



Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, BL_HandleNull(EcLocal, gCodLocal)
    EcCodEFR_LostFocus
End Sub

Sub Actualiza_Estrutura_MAReq(inicio As Integer)
    On Error GoTo TrataErro

    Dim j As Long

    If inicio <> UBound(MaReq) Then
        For j = inicio To UBound(MaReq) - 1
            MaReq(j).Cod_Perfil = MaReq(j + 1).Cod_Perfil
            MaReq(j).Descr_Perfil = MaReq(j + 1).Descr_Perfil
            MaReq(j).cod_ana_c = MaReq(j + 1).cod_ana_c
            MaReq(j).Descr_Ana_C = MaReq(j + 1).Descr_Ana_C
            MaReq(j).cod_ana_s = MaReq(j + 1).cod_ana_s
            MaReq(j).descr_ana_s = MaReq(j + 1).descr_ana_s
            MaReq(j).Ord_Ana = MaReq(j + 1).Ord_Ana
            MaReq(j).Ord_Ana_Compl = MaReq(j + 1).Ord_Ana_Compl
            MaReq(j).Ord_Ana_Perf = MaReq(j + 1).Ord_Ana_Perf
            MaReq(j).cod_agrup = MaReq(j + 1).cod_agrup
            MaReq(j).N_Folha_Trab = MaReq(j + 1).N_Folha_Trab
            MaReq(j).dt_chega = MaReq(j + 1).dt_chega
            MaReq(j).Flg_Apar_Transf = MaReq(j + 1).Flg_Apar_Transf
            MaReq(j).flg_estado = MaReq(j + 1).flg_estado
            MaReq(j).Flg_Facturado = MaReq(j + 1).Flg_Facturado
            MaReq(j).seq_req_tubo = MaReq(j + 1).seq_req_tubo
            MaReq(j).indice_tubo = MaReq(j + 1).indice_tubo
        Next
    End If

    ReDim Preserve MaReq(UBound(MaReq) - 1)

Exit Sub
TrataErro:
    BG_LogFile_Erros "Actualiza_Estrutura_MAReq: " & Err.Number & " - " & Err.Description, Me.Name, "Actualiza_Estrutura_MAReq"
    Exit Sub
    Resume Next
End Sub

Sub Insere_aMeio_RegistosA(inicio As Integer)
    On Error GoTo TrataErro

    Dim j As Long

    CriaNovaClasse RegistosA, -1, "ANA"

    If inicio <> RegistosA.Count Then
        For j = RegistosA.Count To inicio + 1 Step -1
            RegistosA(j).codAna = RegistosA(j - 1).codAna
            RegistosA(j).descrAna = RegistosA(j - 1).descrAna
            RegistosA(j).NReqARS = RegistosA(j - 1).NReqARS
            RegistosA(j).p1 = RegistosA(j - 1).p1
            RegistosA(j).Estado = RegistosA(j - 1).Estado
            RegistosA(j).DtChega = RegistosA(j - 1).DtChega
            RegistosA(j).ObsAnaReq = RegistosA(j - 1).ObsAnaReq
            RegistosA(j).seqObsAnaReq = RegistosA(j - 1).seqObsAnaReq

        Next
    End If

    RegistosA(inicio).codAna = ""
    RegistosA(inicio).descrAna = ""
    RegistosA(inicio).NReqARS = ""
    RegistosA(inicio).p1 = ""
    RegistosA(inicio).Estado = ""
    RegistosA(inicio).DtChega = ""
    RegistosA(inicio).ObsAnaReq = ""
    RegistosA(inicio).seqObsAnaReq = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "Insere_aMeio_RegistosA: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_aMeio_RegistosA"
    Exit Sub
    Resume Next
End Sub

Function Calcula_Valor(valor As Double, TaxaConv) As Double
    On Error GoTo TrataErro

    Calcula_Valor = Round(valor / TaxaConv)
Exit Function
TrataErro:
    BG_LogFile_Erros "Calcula_Valor: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_aMeCalcula_Valorio_RegistosA"
    Calcula_Valor = 0
    Exit Function
    Resume Next
End Function

Private Sub BtPesquisaOrigemAmostra_Click()
    PA_PesquisaOrigemAmostra EcCodOrigemAmostra, EcDescrOrigemAmostra

End Sub

Private Sub BtPesquisaPontoColheita_Click()
    PA_PesquisaPontosColheita EcCodPontoColheita, EcDescrPontoColheita
End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, EcLocal.text
End Sub

Private Sub BtPesquisaTipoAmostra_Click()
    PA_PesquisaTipoAmostra EcCodtipoAmostra, EcDescrTipoAmostra

End Sub

Private Sub BtSairAdiantamento_Click()
    GereFrameAdiantamentos False
End Sub


Private Sub CbTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro

    BG_LimpaOpcao CbTipoUtente, KeyCode

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoUtente_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoUtente_KeyDown"
    Exit Sub
    Resume Next
End Sub

Public Sub CbTipoUtente_Validate(cancel As Boolean)
    On Error GoTo TrataErro

    Call EcUtente_Validate(cancel)

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoUtente_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoUtente_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub CbUrgencia_Click()
'    EcUrgencia.Text = Mid(CbUrgencia.Text, 1, 1)
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro

    BG_LimpaOpcao CbUrgencia, KeyCode

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbUrgencia_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbUrgencia_KeyDown"
    Exit Sub
    Resume Next
End Sub



Private Sub cmdOK_Click()
    On Error GoTo TrataErro

    FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "cmdOK_Click: " & Err.Number & " - " & Err.Description, Me.Name, "cmdOK_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtResumo_Click()
    ImprimeResumo
End Sub

Private Sub btAnaAcresc_Click()
    ' ---------------------------------------------------------------------------
    ' VERIFICA SE ANALISE ESTA MAPEADA
    ' ---------------------------------------------------------------------------
    AdicionaReciboManual 1, RegistosR(FGRecibos.row).codEntidade, RegistosR(FGRecibos.row).NumDoc, RegistosR(FGRecibos.row).SerieDoc, EcCodAnaAcresc, BL_HandleNull(EcQtdAnaAcresc, 1), "", EcCodAnaAcresc, False
    FGRecibos_DblClick
    EcCodAnaAcresc = ""
    EcDescrAnaAcresc = ""
    EcQtdAnaAcresc = ""
    EcTaxaAnaAcresc = ""

End Sub



Private Sub EcAuxAna_GotFocus()
    On Error GoTo TrataErro

    Dim i As Integer

    On Local Error Resume Next
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i

    If EcAuxAna.SelLength = 0 Then
        Sendkeys ("{END}")
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxAna_LostFocus()
    On Error GoTo TrataErro

    Dim i As Integer

    On Local Error Resume Next

    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i

    EcAuxAna.Visible = False
    If FGAna.Enabled = True Then FGAna.SetFocus
    Me.Enabled = True

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxAna_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro

    If Enter = True Then KeyAscii = 0

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_KeyPress"
    Exit Sub
    Resume Next
End Sub


Public Sub EcAuxAna_KeyDown(KeyCode As Integer, Shift As Integer)
     On Error GoTo TrataErro
    Dim a1 As String
    Dim CodigoMarcado As String
    Dim i As Long
    Dim aux As String
    Dim an_aux As String
    Dim rv As Integer
    Dim iRec As Long

    ' UTILIZADOR MARCA UMA MAS PODE MAPEAR PARA OUTRA. ESTA VARIAVEL GUARDA O QUE ELE REALMENTE DIGITA
    Dim AnaFacturavel As String
    a1 = EcAuxAna
    ' For�a o formato "0".
    'DoEvents
    Enter = False
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxAna.text) = "") Then
        Enter = True
        EcAuxAna.Visible = False
        FGAna.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        DoEvents
        CodigoMarcado = EcAuxAna

        Select Case LastColA
            Case lColFgAnaCodigo ' Codigo da An�lise
                AdicionaAnalise CodigoMarcado, False, False, ""
            Case lColFgAnaP1 ' P1
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                RegistosRM(i).ReciboP1 = CodigoMarcado
                                FGAna.TextMatrix(LastRowA, lColFgAnaP1) = CodigoMarcado
                                RegistosRM(i).ReciboOrdemMarcacao = RetornaOrdemP1(i)
                            End If
                        Next
                    End If
                End If
            Case lColFgAnaNrBenef ' nr benef
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                CodigoMarcado = UCase(CodigoMarcado)
                'If FGAna.TextMatrix(LastRowA, lColFgAnaNrBenef) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                RegistosRM(i).ReciboNrBenef = CodigoMarcado
                                FGAna.TextMatrix(LastRowA, lColFgAnaNrBenef) = CodigoMarcado
                            End If
                        Next
                    End If
                'End If
            Case lColFgAnaMedico ' MEDICO
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                RegistosRM(i).ReciboCodMedico = CodigoMarcado
                                FGAna.TextMatrix(LastRowA, lColFgAnaMedico) = CodigoMarcado
                            End If
                        Next
                    End If
                End If
            Case lColFgAnaPreco ' TAXA
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                aux = Replace(Trim(CodigoMarcado), ",", ".")
                If (IsNumeric(aux)) Then
                    If (CLng(aux) = 0) Then
                        CodigoMarcado = "0"
                    End If
                End If
                CodigoMarcado = Replace(CodigoMarcado, ".", ",")
                If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                If EliminaReciboManual(i, RegistosRM(i).ReciboCodFacturavel, False) = True Then
                                    If (RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Or RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla) And CodigoMarcado > 0 Then
                                        RegistosRM(i).ReciboIsencao = gTipoIsencaoNaoIsento
                                        BG_Mensagem mediMsgBox, "An�lise passou a N�O ISENTA", vbInformation, ""
                                    End If
                                    RegistosRM(i).ReciboTaxa = CodigoMarcado
                                    AdicionaAnaliseAoReciboManual i, False
                                    FGAna.TextMatrix(LastRowA, lColFgAnaPreco) = EcAuxAna
                                End If
                            End If
                        Next
                    End If
                End If
        End Select
        CodigoMarcado = ""
        EcAuxAna = ""
        EcAuxAna.Visible = False
        If FGAna.row <= FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
            FGAna.row = FGAna.row + 1
            'FgAna.Col = 1
        Else
            Fgana_RowColChange
        End If
        If FGAna.Enabled = True Then
            FGAna.SetFocus
        End If

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxProd_GotFocus()
     On Error GoTo TrataErro

    Dim i As Integer

    On Local Error Resume Next

    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i

    If LastColP = 5 Then
        EcAuxProd.Tag = adDate
    Else
        EcAuxProd.Tag = ""
    End If

    If EcAuxProd.SelLength = 0 Then
        Sendkeys ("{END}")
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_GotFocus"
    Exit Sub
    Resume Next
End Sub


Private Sub EcAuxRec_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    If KeyCode = vbKeyReturn Then
        If FGRecibos.Col = lColRecibosPercentagem Then
            RegistosR(FGRecibos.row).Desconto = BL_HandleNull(CDbl(100 - CDbl(EcAuxRec)), 0)
            RegistosR(FGRecibos.row).ValorPagar = Round((RegistosR(FGRecibos.row).ValorOriginal * CDbl(EcAuxRec)) / 100, 2)
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = EcAuxRec
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = RegistosR(FGRecibos.row).ValorPagar
            EcAuxRec.Visible = False
            EcAuxRec = ""
            If RegistosR(FGRecibos.row).ValorPagar > 0 Then

                RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "N�o Emitido"
            ElseIf RegistosR(FGRecibos.row).ValorPagar = 0 Then
                RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Recibo Nulo"
            End If
            If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
            Else
                RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
            End If
        ElseIf FGRecibos.Col = lColRecibosValorPagar Then
            If IsNumeric(EcAuxRec) Then
                RegistosR(FGRecibos.row).Desconto = CDbl(100 - BL_HandleNull(Replace(EcAuxRec, ".", ",") * 100 / (RegistosR(FGRecibos.row).ValorOriginal), 3))
                RegistosR(FGRecibos.row).ValorPagar = Replace(EcAuxRec, ".", ",")
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = Round(CDbl(100 - BL_HandleNull(CDbl(RegistosR(FGRecibos.row).Desconto), 0)), 3)
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = RegistosR(FGRecibos.row).ValorPagar
                EcAuxRec.Visible = False
                EcAuxRec = ""
                If RegistosR(FGRecibos.row).ValorPagar > 0 Then

                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "N�o Emitido"
                ElseIf RegistosR(FGRecibos.row).ValorPagar = 0 Then
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Recibo Nulo"
                End If
                If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                End If
            End If
        Else
            EcAuxRec.Visible = False
            EcAuxRec = ""
        End If
    End If
    FGRecibos_Click
Exit Sub
TrataErro:
    RegistosR(FGRecibos.row).Desconto = 0
    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = "100"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxRec_LostFocus()
    On Error GoTo TrataErro
    EcAuxRec.Visible = False
    EcAuxRec = ""

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxRec_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxRec_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxTubo_GotFocus()
    On Error GoTo TrataErro

    Dim i As Integer

    On Local Error Resume Next

    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i

    If LastColT = 2 Then
        EcAuxTubo.Tag = adDate
    Else
        EcAuxTubo.Tag = ""
    End If

    If EcAuxTubo.SelLength = 0 Then
        Sendkeys ("{END}")
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxTubo_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxTubo_GotFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub EcAuxProd_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro

    Enter = False

    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxProd.text) = "") Then
        Enter = True
        EcAuxProd.Visible = False
        If FgProd.Enabled = True Then FgProd.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        Select Case LastColP
            Case 0 ' Codigo do produto
                If Verifica_Prod_ja_Existe(LastRowP, EcAuxProd.text) = False Then
                    If SELECT_Descr_Prod = True Then
                        If Trim(EcDtColheita.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 5) = Trim(EcDtColheita.text)
                            RegistosP(LastRowP).DtPrev = Trim(EcDtColheita.text)
                        End If
                        If Trim(EcDataChegada.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 6) = Trim(EcDataChegada.text)
                            RegistosP(LastRowP).DtChega = Trim(EcDataChegada.text)
                        End If

                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd

                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).CodProd = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        If FgProd.row = FgProd.rows - 1 Then
                            'Cria linha vazia
                            FgProd.AddItem ""
                            CriaNovaClasse RegistosP, FgProd.row + 1, "PROD"
                            FgProd.row = FgProd.row + 1
                            FgProd.Col = 0
                        Else
                            FgProd.Col = 2
                        End If
                    End If
                Else
                    Beep
                    BG_Mensagem mediMsgStatus, "Produto j� indicado!"
                End If
            Case 2 ' Codigo da especificacao
                If SELECT_Especif = True Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).CodEspecif = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 5
                End If
            Case 4 ' volume
                If Trim(EcAuxProd.text) <> "" Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).Volume = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 0
                End If
            Case 5 ' Data Prevista
                If Trim(EcAuxProd.text) <> "" Then
                    If BG_ValidaTipoCampo_ADO(Me, EcAuxProd) = True Then
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).DtPrev = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        FgProd.Col = 0
                    Else
                        Beep
                        BG_Mensagem mediMsgStatus, "Data inv�lida!"
                    End If

                End If
        End Select
        If FgProd.Enabled = True Then FgProd.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_KeyDown"
    Exit Sub
    Resume Next
End Sub


Private Sub EcAuxProd_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro

    If Enter = True Then KeyAscii = 0
    Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_KeyPress"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxTubo_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro

    If Enter = True Then KeyAscii = 0
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxTubo_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxTubo_KeyPress"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxProd_LostFocus()
    On Error GoTo TrataErro

    Dim i As Integer

    On Local Error Resume Next

    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i


    EcAuxProd.Visible = False
    FgProd.SetFocus
    Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_LostFocus"
    Exit Sub
    Resume Next
End Sub


Private Sub EcCodAnaAcresc_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim sSql As String
    Dim rsAnalises As New ADODB.recordset
    Dim taxa As String
    Dim mens As String
    Dim i As Long
    On Error GoTo TrataErro

    If KeyCode = 13 Then
        If UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "S" And UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "C" And UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "P" Then
            If gSGBD = gSqlServer Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE substring(cod_ana,2,10) = " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            ElseIf gSGBD = gOracle Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(substr(cod_ana,2,10))= " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            End If
            rsAnalises.CursorLocation = adUseServer
            rsAnalises.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnalises.Open sSql, gConexao

            If rsAnalises.RecordCount > 0 Then
                EcCodAnaAcresc = rsAnalises!cod_ana
                EcDescrAnaAcresc = rsAnalises!descr_ana
                EcQtdAnaAcresc = "1"
                rsAnalises.Close
            Else
                EcCodAnaAcresc = ""
                EcDescrAnaAcresc = ""
                EcQtdAnaAcresc = ""
                rsAnalises.Close
                Exit Sub
            End If
        Else
            If gSGBD = gSqlServer Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(cod_ana) = " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            ElseIf gSGBD = gOracle Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(cod_ana)= " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            End If
            rsAnalises.CursorLocation = adUseServer
            rsAnalises.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnalises.Open sSql, gConexao

            If rsAnalises.RecordCount > 0 Then
                EcCodAnaAcresc = rsAnalises!cod_ana
                EcDescrAnaAcresc = rsAnalises!descr_ana
                EcQtdAnaAcresc = "1"
                EcTaxaAnaAcresc = RetornaPrecoAnaRec(EcCodAnaAcresc, RegistosR(FGRecibos.row).codEntidade, CInt(EcQtdAnaAcresc))
                rsAnalises.Close
            Else
                EcCodAnaAcresc = ""
                EcDescrAnaAcresc = ""
                EcQtdAnaAcresc = ""
                rsAnalises.Close
            End If
        End If
        EcQtdAnaAcresc.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodAnaAcresc_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodAnaAcresc_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodAnaAcresc_LostFocus()
    EcCodAnaAcresc_KeyDown 13, 0
End Sub

Private Sub EcCodEFR_LostFocus()
    Dim iRec As Long
    On Error GoTo TrataErro
    Dim i As Long



    If EcCodEFR <> "" And EcCodEFR <> CodEfrAUX And RegistosA.Count > 1 Then
        CodEfrAUX = EcCodEFR
        gMsgTitulo = "Entidade"
        gMsgMsg = "Deseja alterar todas as an�lises para " & EcDescrEFR & "?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            For i = 1 To RegistosRM.Count - 1
                If RegistosRM(i).ReciboNumDoc = "0" Or RegistosRM(i).ReciboNumDoc = "" Then
                    AlteraEntidadeReciboManual i, EcCodEFR, EcDescrEFR, "", -1
                    VerificaEFRCodigoFilho RegistosRM(i).ReciboCodFacturavel, EcCodEFR, EcDescrEFR, i
                Else
                    'BG_Mensagem mediMsgBox, "N�o � possivel alterar a entidade das an�lises associadas ao recibo emitido.", vbInformation, "Altera��o entidade"
                End If
            Next
            For i = 1 To RegistosA.Count - 1
                iRec = DevIndice(RegistosA(i).codAna)
                If iRec = -1 Then
                    AdicionaReciboManual 0, EcCodEFR, "0", "0", RegistosA(i).codAna, 1, "", RegistosA(i).codAna, False
                End If
            Next
            If EcNumReq <> "" And EcUtilizadorCriacao <> "" Then
                FuncaoModificar (True)
            End If
        End If


    End If

    ActualizaLinhasRecibos -1
    CodEfrAUX = EcCodEFR
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodEFR_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodEFR_LostFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodEFR_Validate(cancel As Boolean)
    cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, BL_HandleNull(EcLocal, gCodLocal))
    EcCodEFR_LostFocus
End Sub


Private Sub EcCodOrigemAmostra_Validate(cancel As Boolean)
     PA_ValidateOrigemAmostra EcCodOrigemAmostra, EcDescrOrigemAmostra
End Sub

Private Sub EcCodPontoColheita_validate(cancel As Boolean)
     PA_ValidatePontoColheita EcCodPontoColheita, EcDescrPontoColheita

End Sub

Private Sub EcCodtipoAmostra_Validate(cancel As Boolean)
     PA_ValidateTipoAmostra EcCodtipoAmostra, EcDescrTipoAmostra
End Sub

Private Sub EcDataAlteracao_Validate(cancel As Boolean)
     On Error GoTo TrataErro

    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataAlteracao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataAlteracao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataAlteracao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataChegada_GotFocus()
    On Error GoTo TrataErro

    If Trim(EcDataChegada.text) = "" Then
        EcDataChegada.text = dataAct
    End If
    EcDataChegada.SelStart = 0
    EcDataChegada.SelLength = Len(EcDataChegada)
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataChegada_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataChegada_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataChegada_Validate(cancel As Boolean)
    On Error GoTo TrataErro

    If EcDataChegada.text <> "" Then
        cancel = Not ValidaDataChegada
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataChegada_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataChegada_Validate"
    Exit Sub
    Resume Next
End Sub




Private Sub EcDataCriacao_Validate(cancel As Boolean)
     On Error GoTo TrataErro

    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataCriacao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataCriacao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataCriacao_Validate"
    Exit Sub
    Resume Next
End Sub


Private Sub EcDataImpressao_Validate(cancel As Boolean)
     On Error GoTo TrataErro

    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataImpressao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataImpressao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataImpressao2_Validate(cancel As Boolean)
     On Error GoTo TrataErro

    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao2)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataImpressao2_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataImpressao2_Validate"
    Exit Sub
    Resume Next
End Sub

Public Sub EcDtColheita_GotFocus()
    On Error GoTo TrataErro
    Dim Campos(5) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim i As Integer
    'Para evitar que a nova entidade seja reposta pela codificada
    ' no utente
    If gPreencheEFRUtente = 1 Then
        If (Trim(EcCodEFR.text) = "") Then
            Call PreencheEFRUtente
        End If
    End If
    If EcUtente <> "" Then
        If CbUrgencia.ListCount > 0 Then
            CbUrgencia.ListIndex = 0
        End If
        If CbDestino.ListCount > 0 And gLAB <> "CL" Then
            CbDestino.ListIndex = 0
        End If
    End If

    Call ValidaDataPrevista


    EcDtColheita.SelStart = 0
    EcDtColheita.SelLength = Len(EcDtColheita)
    If EcDataChegada.text = "" Then
        EcDataChegada.text = EcDtColheita.text
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDtColheita_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcDtColheita_GotFocus"
    Exit Sub
    Resume Next
End Sub

Sub Preenche_Data_Prev_Prod()
    On Error GoTo TrataErro

    Dim i As Integer

    ExecutaCodigoP = False
    For i = 1 To RegistosP.Count - 1
        If RegistosP(i).CodProd <> "" Then
            If Trim(RegistosP(i).DtPrev) = "" Then
                RegistosP(i).DtPrev = EcDtColheita.text
                FgProd.TextMatrix(i, 4) = EcDtColheita.text
                RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
                FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
            End If
        If RegistosP(i).EstadoProd = "" Then
            RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
            FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
        End If
        End If
    Next i

    ExecutaCodigoP = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_Data_Prev_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_Data_Prev_Prod"
    Exit Sub
    Resume Next
End Sub

Function ValidaDataChegada() As Boolean
    On Error GoTo TrataErro

    ValidaDataChegada = False
    If EcDataChegada.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataChegada) Then
'            If DateValue(EcDataChegada.Text) > DateValue(dataAct) Then
'                BG_Mensagem mediMsgBox, "A data de Chegada tem que ser inferior ou igual � data actual ", vbInformation, "Aten��o"
'                SendKeys ("{HOME}+{END}")
'                If EcDataChegada.Enabled = True Then
'                    EcDataChegada.SetFocus
'                End If
'            Else
'                ValidaDataChegada = True
'            End If
            ValidaDataChegada = True
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaDataChegada: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaDataChegada"
    ValidaDataChegada = False
    Exit Function
    Resume Next
End Function

Function ValidaDataPrevista() As Boolean
    On Error GoTo TrataErro

    ValidaDataPrevista = False
    If EcDtColheita.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDtColheita) And EcDtColheita.Enabled = True Then
'            If DateValue(EcDtColheita.Text) < DateValue(dataAct) Then
'                BG_Mensagem mediMsgBox, "A data prevista tem que ser superior ou igual � data actual ", vbInformation, "Aten��o"
'                If EcDtColheita.Enabled = True Then
'                    EcDtColheita.Text = ""
'                    EcDtColheita.SetFocus
'                End If
'            Else
'                ValidaDataPrevista = True
'            End If
            ValidaDataPrevista = True
        End If
    ElseIf EcDtColheita.text = "" Then
        If Flg_LimpaCampos = False Then
            If EcFimSemana <> "1" Then
                EcDtColheita.text = Format(dataAct, gFormatoData)
                ValidaDataPrevista = True
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaDataPrevista: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaDataPrevista"
    ValidaDataPrevista = False
    Exit Function
    Resume Next
End Function

Public Sub EcDtColheita_Validate(cancel As Boolean)
    On Error GoTo TrataErro


    If Trim(EcDtColheita.text) <> "" Then
        cancel = Not ValidaDataPrevista
    End If
    If cancel = True Then Exit Sub

    If EcDtColheita.Enabled = True And Trim(EcSeqUtente.text) <> "" Then
        Preenche_Data_Prev_Prod
        SSTGestReq.TabEnabled(1) = True
        SSTGestReq.TabEnabled(2) = True
        SSTGestReq.TabEnabled(3) = True
        SSTGestReq.TabEnabled(4) = True
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FgProd.row = LastRowP
        FgProd.Col = LastColP
        ExecutaCodigoP = True
        FrBtTubos.Enabled = True
        FGTubos.row = LastRowT
        FGTubos.Col = LastColT
        ExecutaCodigoT = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDtColheita_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDtColheita_Validate"
    Exit Sub
    Resume Next
End Sub


Private Sub EcNome_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNome As New ADODB.recordset
    Dim SeqUtente As String

    'soliveira LJM 06.11.2008
    If EcNumReq.text = "" And EcNome.text <> "" Then
        sSql = BL_Upper_Campo("SELECT seq_utente FROM sl_identif WHERE nome_ute = " & BL_TrataStringParaBD(EcNome), "nome_ute", gPesquisaDentroCampo)
        Set RsNome = BG_ExecutaSELECT(sSql)
        SeqUtente = ""
        If RsNome.RecordCount >= 1 Then
            SeqUtente = "("
            While Not RsNome.EOF
                SeqUtente = SeqUtente & RsNome!seq_utente & ", "
                RsNome.MoveNext
            Wend
            SeqUtente = Mid(SeqUtente, 1, Len(SeqUtente) - 2) & ")"

            EcSeqUtente.Tag = adVarChar
            EcSeqUtente.MaxLength = 100
            EcSeqUtente = SeqUtente
        'soliveira ualia
        Else
            EcSeqUtente = -1
        End If
        BG_DestroiRecordSet RsNome
    'soliveira ualia
    ElseIf EcNumReq.text = "" And EcNome.text = "" Then
        EcSeqUtente = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNome_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNome_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_LostFocus()
    On Error GoTo TrataErro

    cmdOK.Default = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    If gNReqPreImpressa = mediSim And gPreencheSalaDefeitoPreImpressa = mediSim Then
        PreencheSalaAuto EcNumReq
    End If
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_Validate"
    Exit Sub
    Resume Next
End Sub



Private Sub EcPesqRapProduto_Change()
    On Error GoTo TrataErro

    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer

    If EcPesqRapProduto.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        rsCodigo.Open "SELECT cod_produto FROM sl_produto WHERE seq_produto = " & EcPesqRapProduto, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do produto!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxProd.text = BL_HandleNull(rsCodigo!cod_produto, "")
            EcAuxProd.left = FgProd.CellLeft + 270
            EcAuxProd.top = FgProd.CellTop + 2060
            EcAuxProd.Width = FgProd.CellWidth + 20
            EcAuxProd.Visible = True
            EcAuxProd.Enabled = True
            EcAuxProd.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapProduto_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapProduto_Change"
    Exit Sub
    Resume Next
End Sub



Private Sub EcPesqRapTubo_Change()
    On Error GoTo TrataErro

    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer

    If EcPesqRapTubo.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_tubo FROM sl_tubo WHERE seq_tubo = " & EcPesqRapTubo, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do tubo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxTubo.text = BL_HandleNull(rsCodigo!cod_tubo, "")
            EcAuxTubo.left = FGTubos.CellLeft + 270
            EcAuxTubo.top = FGTubos.CellTop + 2060
            EcAuxTubo.Width = FGTubos.CellWidth + 20
            EcAuxTubo.Visible = True
            EcAuxTubo.Enabled = True
            EcAuxTubo.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapTubo_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapTubo_Change"
    Exit Sub
    Resume Next
End Sub



Private Sub EcQtdAnaAcresc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        If IsNumeric(EcQtdAnaAcresc) = False Then
            EcQtdAnaAcresc = "1"
        End If
        EcTaxaAnaAcresc = RetornaPrecoAnaRec(EcCodAnaAcresc, RegistosR(FGRecibos.row).codEntidade, CInt(EcQtdAnaAcresc))
        If BtAnaAcresc.Enabled = True Then
            btAnaAcresc_Click
        End If
    End If
End Sub

Private Sub EcQtdAnaAcresc_LostFocus()
    EcQtdAnaAcresc_KeyDown 13, 0
End Sub

Public Sub EcUtente_Validate(cancel As Boolean)
    On Error GoTo TrataErro

    Dim sql As String
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer

    'Limpa a estrutura de dados do utente
    If CbTipoUtente.ListIndex <> -1 And EcUtente.text <> "" Then
        'Procurar o sequencial e chamar a fun��o que preenche a identifica��o do utente
        Campos(0) = "seq_utente"

        iret = BL_DaDadosUtente(Campos, retorno, , CbTipoUtente.text, EcUtente.text)

        If iret = -1 Then
            cancel = True
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbInformation + vbOKOnly, "Pesquisa ao utente!"
            EcNome.text = ""
            EcDataNasc.text = ""
            EcSexo = ""
            EcIdade.text = ""
        Else
            BtConsReq.Enabled = True

            EcSeqUtente.text = retorno(0)
            BL_PreencheDadosUtente EcSeqUtente

            PreencheDadosUtente

        End If
    Else
        EcNome.text = ""
        EcDataNasc.text = ""
        EcSexo = ""
        EcIdade.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtente_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtente_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorAlteracao_Validate(cancel As Boolean)
    On Error GoTo TrataErro

    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorAlteracao)
    If cancel = False Then
        LbNomeUtilAlteracao = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorAlteracao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorAlteracao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorCriacao_Validate(cancel As Boolean)
    On Error GoTo TrataErro

    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorCriacao)
    If cancel = False Then
        LbNomeUtilCriacao = BL_SelNomeUtil(EcUtilizadorCriacao.text)
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorCriacao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorCriacao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_GotFocus()
    On Error GoTo TrataErro

    Set CampoActivo = Me.ActiveControl
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_LostFocus()
    On Error GoTo TrataErro

    LbNomeUtilImpressao = BL_SelNomeUtil(EcUtilizadorImpressao.text)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_Validate(cancel As Boolean)
    On Error GoTo TrataErro

    cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao2_Validate(cancel As Boolean)
    On Error GoTo TrataErro

    LbNomeUtilImpressao2 = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao2_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao2_Validate"
    Exit Sub
    Resume Next
End Sub




Private Sub Fgana_DblClick()
    On Error GoTo TrataErro
    FgAna_KeyDown 13, 0
Exit Sub
TrataErro:
    BG_LogFile_Erros "Fgana_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "Fgana_DblClick"
    Exit Sub
    Resume Next
End Sub

'Private Sub FGAna_Click()
'
'    MsgBox Me.FGAna_Recibo.ColSel, vbInformation, "ZZ"
'
'End Sub

Private Sub FGAna_GotFocus()
    On Error GoTo TrataErro

    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo)) <> "" Then
        FGAna.CellBackColor = vbRed
        FGAna.CellForeColor = vbWhite
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_GotFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    Dim ana As String
    Dim taxa As String
    Dim mens As String
    Dim codAnaEfr As String
    Dim flgPermite As Boolean
    Dim i As Long
    Dim k As Long
    Dim iRec As Long

    flgPermite = False
    If LastRowA > FGAna.rows - 1 Then
        LastRowA = FGAna.rows - 1
    End If
    If FGAna.row < RegistosA.Count Then
        iRec = DevIndice(RegistosA(FGAna.row).codAna)
    Else
        iRec = -1
    End If
    Select Case KeyCode
        Case 96
            KeyCode = 48
        Case 97
            KeyCode = 49
        Case 98
            KeyCode = 50
        Case 99
            KeyCode = 51
        Case 100
            KeyCode = 52
        Case 101
            KeyCode = 53
        Case 102
            KeyCode = 54
        Case 103
            KeyCode = 55
        Case 104
            KeyCode = 56
        Case 105
            KeyCode = 57
    End Select

    ' --------------------------------------------------------------------------------------------
    ' TECLAS DE ATALHO PARA MUDAR DADOS DAS ANALISES
    ' --------------------------------------------------------------------------------------------
    If KeyCode = 113 Then                   'F2 - Muda credencial
        FGAna.Col = lColFgAnaP1
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 114 Then               'F3 - Muda M�dico
        FGAna.Col = lColFgAnaMedico
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 115 Then               'F4 - Muda entidade
        FGAna.Col = lColFgAnaDescrEFR
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 116 And gPermAltPreco = 1 Then               'F5 - Muda taxa
        FGAna.Col = lColFgAnaPreco
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 122 Then               'F11 - Incrementa n� P1
        Flg_IncrementaP1 = True
        Exit Sub
        flgPermite = True
    End If



    If (FGAna.Col = lColFgAnaCodigo Or (FGAna.Col = lColFgAnaP1 And KeyCode <> 13) Or FGAna.Col = lColFgAnaMedico Or (FGAna.Col = lColFgAnaPreco And gPermAltPreco = 1)) And (KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)) Then ' Enter = Editar
        If FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo) <> "" And FGAna.Col = lColFgAnaCodigo Then Exit Sub
        ' --------------------------------------------------------------------------------------------
        ' SE RECIBO EMITIDO NAO DEIXA ALTERAR VALOR
        ' --------------------------------------------------------------------------------------------
        If FGAna.row < RegistosA.Count And iRec > 0 Then
            If RegistosRM(iRec).ReciboNumDoc <> "0" And RegistosRM(iRec).ReciboNumDoc <> "" And FGAna.Col = 5 Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar TAXA", vbOKOnly + vbInformation, "Taxas"
                Exit Sub
            End If
        End If

        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = lColFgAnaCodigo Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            If LastColA <> lColFgAnaCodigo And Trim(FGAna.TextMatrix(LastRowA, lColFgAnaCodigo)) = "" Then
                Beep
                BG_Mensagem mediMsgStatus, "Tem que indicar primeiro uma an�lise!"
            Else
                If EcDataFecho.text <> "" Then
                    Beep
                    BG_Mensagem mediMsgStatus, "N�o pode alterar uma requisi��o fechada!"
                    Exit Sub
                End If
                If LastRowA > 0 And LastColA = lColFgAnaP1 And (KeyCode = 86 Or KeyCode = 66) Then
                    AlteraCorP1
                    Exit Sub
                End If
                ExecutaCodigoA = False
                EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
                ExecutaCodigoA = True
                If KeyCode = 8 Then
                    If Len(EcAuxAna.text) > 1 Then EcAuxAna.text = Mid(EcAuxAna.text, 1, Len(EcAuxAna.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxAna.SelStart = 0
                    EcAuxAna.SelLength = Len(EcAuxAna)
                Else
                    EcAuxAna.SelStart = Len(EcAuxAna)
                    EcAuxAna.SelLength = 0
                End If

                EcAuxAna.left = FGAna.CellLeft + 360
                EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
                EcAuxAna.Width = FGAna.CellWidth + 20
                EcAuxAna.Visible = True
                EcAuxAna.Enabled = True
                EcAuxAna.SetFocus

            End If
        End If

    ElseIf FGAna.Col = lColFgAnaP1 And KeyCode = 13 Then
        ' --------------------------------------------------------------------------------------------
        ' DUPLO CLICK ALTERA COR DO P1
        ' --------------------------------------------------------------------------------------------
        If iRec <> -1 Then
            If RegistosRM(iRec).ReciboNumDoc <> "0" Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar tipo P1", vbOKOnly + vbInformation, "Taxas"
                Exit Sub
            End If

            EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
            If KeyCode = 13 Then
                EcAuxAna.SelStart = 0
                EcAuxAna.SelLength = Len(EcAuxAna)
            Else
                EcAuxAna.SelLength = 0
            End If

            EcAuxAna.left = FGAna.CellLeft + 350
            EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
            EcAuxAna.Width = FGAna.CellWidth + 20
            EcAuxAna.Visible = True
            EcAuxAna.Enabled = True
            EcAuxAna.SetFocus
        End If

        'AlteraCorP1
    ElseIf FGAna.Col = lColFgAnaNrBenef And KeyCode = 13 Then
        ' --------------------------------------------------------------------------------------------
        ' DUPLO CLICK EDITA NR BENEF
        ' --------------------------------------------------------------------------------------------
        If RegistosRM(iRec).ReciboNumDoc <> "0" And FGAna.Col = 5 Then
            BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar N� Benefici�rio", vbOKOnly + vbInformation, "Taxas"
            Exit Sub
        End If
        EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
        If KeyCode = 13 Then
            EcAuxAna.SelStart = 0
            EcAuxAna.SelLength = Len(EcAuxAna)
        Else
            EcAuxAna.SelLength = 0
        End If

        EcAuxAna.left = FGAna.CellLeft + 350
        EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
        EcAuxAna.Width = FGAna.CellWidth + 20
        EcAuxAna.Visible = True
        EcAuxAna.Enabled = True
        EcAuxAna.SetFocus
    ElseIf (FGAna.Col = lColFgAnaDescrEFR) And (KeyCode = 13 And Shift = 0) And FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo) <> "" Then  ' Enter = Editar
        ' --------------------------------------------------------------------------------------------
        ' SE DA ENTER NA COLUNA DA ENTIDADE
        ' --------------------------------------------------------------------------------------------
            If iRec > 0 Then
                If BL_HandleNull(RegistosRM(iRec).ReciboNumDoc, "0") <> "0" Then
                    ' JA NAO E MARTELANCO--SOLIVEIRA LHL MARTELAN�O S� NAO PODE MUDAR A ENTIDADE SE O RECIBO ESTIVER NO ESTADO PAGO
                    Exit Sub
                End If
            End If

            PesquisaEntFin
            If EcCodEfr2 <> "" Then

                AlteraEntidadeReciboManual iRec, EcCodEfr2, EcDescrEfr2, "", FGAna.row
                iRec = DevIndice(RegistosA(FGAna.row).codAna)
                VerificaEFRCodigoFilho RegistosRM(iRec).ReciboCodFacturavel, EcCodEfr2, EcDescrEfr2, FGAna.row
            End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga inha
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser eliminada: j� cont�m resultados!"
        ElseIf RegistosA(FGAna.row).codAna <> "" Then
            Dim TempPerfilMarcacao As String
            iRec = DevIndice(RegistosA(FGAna.row).codAna)

            If iRec > 0 Then
                TempPerfilMarcacao = RegistosRM(iRec).ReciboCodFacturavel

                If TempPerfilMarcacao = "" Then
                    If EliminaReciboManual(iRec, ana, True) = True Then
                        LimpaColeccao RegistosRM, iRec
                    End If
                    iRec = -1
                    EliminaCodigoFilho RegistosA(FGAna.row).codAna
                    TotalEliminadas = TotalEliminadas + 1
                    ReDim Preserve Eliminadas(TotalEliminadas)
                    Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
                    Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                    Elimina_Mareq RegistosA(FGAna.row).codAna
                    LbTotalAna = LbTotalAna - 1
                    If LbTotalAna < 0 Then
                        LbTotalAna = 0
                    End If
                    ana = RegistosA(FGAna.row).codAna
                    RegistosA.Remove FGAna.row
                    FGAna.RemoveItem FGAna.row
                    EliminaTuboEstrut
                    EliminaProduto ana
                Else
                    For i = RegistosA.Count - 1 To 1 Step -1
                        iRec = DevIndice(RegistosA(i).codAna)
                        If iRec > -1 Then
                            If RegistosRM(iRec).ReciboCodFacturavel = TempPerfilMarcacao Then
                                If EliminaReciboManual(iRec, ana, True) = True Then
                                    EliminaCodigoFilho RegistosRM(iRec).ReciboCodFacturavel
                                    LimpaColeccao RegistosRM, iRec
                                Else
                                    RegistosRM(iRec).ReciboFlgMarcacaoRetirada = "1"
                                End If
                                TotalEliminadas = TotalEliminadas + 1
                                ReDim Preserve Eliminadas(TotalEliminadas)
                                Eliminadas(TotalEliminadas).codAna = RegistosA(i).codAna
                                Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                                Elimina_Mareq RegistosA(i).codAna
                                LbTotalAna = LbTotalAna - 1
                                If LbTotalAna < 0 Then
                                    LbTotalAna = 0
                                End If
                                ana = RegistosA(i).codAna
                                RegistosA.Remove i
                                FGAna.RemoveItem i
                                EliminaTuboEstrut
                                EliminaProduto ana
                                'Actualiza ord_marca da estrutura MaReq
                                k = 1
                                While k <= UBound(MaReq)
                                    If MaReq(k).Ord_Marca >= FGAna.row Then
                                        MaReq(k).Ord_Marca = MaReq(k).Ord_Marca - 1
                                    End If
                                    k = k + 1
                                Wend
                            End If
                        End If
                        iRec = -1
                    Next
                End If
            Else
                EliminaCodigoFilho RegistosA(FGAna.row).codAna
                TotalEliminadas = TotalEliminadas + 1
                ReDim Preserve Eliminadas(TotalEliminadas)
                Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
                Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                Elimina_Mareq RegistosA(FGAna.row).codAna
                LbTotalAna = LbTotalAna - 1
                If LbTotalAna < 0 Then
                    LbTotalAna = 0
                End If
                ana = RegistosA(FGAna.row).codAna
                RegistosA.Remove FGAna.row
                FGAna.RemoveItem FGAna.row
                EliminaTuboEstrut
                EliminaProduto ana
            End If
        Else
            If FGAna.TextMatrix(FGAna.row, 0) <> "" Then
            'Situa��es que se adicionou linha mas que n�o se acrescentou nada
            'Selecciou op��o delete ou elimina linha
            FGAna.RemoveItem FGAna.row
            'Actualiza ord_marca da estrutura MaReq
            k = 1
            While k <= UBound(MaReq)
                If MaReq(k).Ord_Marca >= FGAna.row Then
                    MaReq(k).Ord_Marca = MaReq(k).Ord_Marca - 1
                End If
                k = k + 1
            Wend
            End If
        End If

        FGAna.SetFocus

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgAna_KeyDown"
    Exit Sub
    Resume Next
End Sub


Private Sub FGAna_LostFocus()
    On Error GoTo TrataErro

    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo)) <> "" Then
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbRed
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_LostFocus"
    Exit Sub
    Resume Next

End Sub

Private Sub FGAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo TrataErro
    If Button = 2 And FGAna.Col = 0 And FGAna.row > 0 Then
        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANA
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_MouseDown: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_MouseDown"
    Exit Sub
    Resume Next
End Sub

Private Sub Fgana_RowColChange()
    On Error GoTo TrataErro

    Dim tmpCol As Integer
    Dim tmpRow As Integer

    If ExecutaCodigoA = True Then
        ExecutaCodigoA = False

        tmpCol = FGAna.Col
        tmpRow = FGAna.row
        If FGAna.Col <> LastColA Then FGAna.Col = LastColA
        If FGAna.row <> LastRowA Then FGAna.row = LastRowA

        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(LastRowA).Estado <> "-1" And LastColA = 0 And Trim(FGAna.TextMatrix(LastRowA, 0)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                'FGAna.CellBackColor = vbWhite
                'FGAna.CellForeColor = vbBlack
            End If
        End If

        If FGAna.Col <> tmpCol Then FGAna.Col = tmpCol
        If FGAna.row <> tmpRow Then FGAna.row = tmpRow

        ' Controlar as colunas
        If FGAna.Cols = 4 Then
            If FGAna.Col = 3 Then
'                FGAna.Col = 0          'sliveira 10-12-2003
'                If FGAna.Row < FGAna.Rows - 1 Then FGAna.Row = FGAna.Row + 1
            ElseIf FGAna.Col = 1 Then
                If LastColA = 2 Then
                    FGAna.Col = 0
                Else
                    FGAna.Col = 2
                End If
            End If
        Else
            If FGAna.Col = 1 Then
             '   FgAna.Col = 0
             '   If FgAna.row < FgAna.rows - 1 Then FgAna.row = FgAna.row + 1
            End If
        End If

        LastColA = FGAna.Col
        LastRowA = FGAna.row

        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" Then
                FGAna.CellBackColor = vbRed
                FGAna.CellForeColor = vbWhite
            Else
                'FGAna.CellBackColor = azul
                'FGAna.CellForeColor = vbWhite
            End If
        End If

        ExecutaCodigoA = True
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Fgana_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "Fgana_RowColChange"
    Exit Sub
    Resume Next
End Sub




Private Sub FgAnaRec_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    On Error GoTo TrataErro
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Or RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or _
       RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Or EstrutAnaRecibo(FgAnaRec.row).Flg_adicionada = 1 Then
        Exit Sub
    End If
    If KeyCode = 46 Then
        If (RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo Or _
           RegistosR(FGRecibos.row).Estado = gEstadoReciboPerdido) And UBound(EstrutAnaRecibo) > 0 Then
            If EstrutAnaRecibo(FgAnaRec.row).tipo = "MANUAL" Then
                EliminaReciboManual EstrutAnaRecibo(FgAnaRec.row).indice, EstrutAnaRecibo(FgAnaRec.row).codAna, True
                LimpaColeccao RegistosRM, EstrutAnaRecibo(FgAnaRec.row).indice
                FgAnaRec.RemoveItem FgAnaRec.row
                FGRecibos_DblClick
            Else
                For i = 1 To RegistosA.Count - 1
                    If RegistosA(i).codAna = EstrutAnaRecibo(FgAnaRec.row).codAna Then
                        Exit Sub
                    End If
                Next
                EliminaReciboManual EstrutAnaRecibo(FgAnaRec.row).indice, EstrutAnaRecibo(FgAnaRec.row).codAna, True
                FgAnaRec.RemoveItem FgAnaRec.row
                LimpaColeccao RegistosRM, EstrutAnaRecibo(FgAnaRec.row).indice
                FGRecibos_DblClick
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgAnaRec_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgAnaRec_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub FgProd_GotFocus()
    On Error GoTo TrataErro
    Dim i As Integer
    FgProd.CellBackColor = azul
    FgProd.CellForeColor = vbWhite



Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_GotFocus"
    Exit Sub
    Resume Next
End Sub


Private Sub FGRecibos_Click()
    On Error GoTo TrataErro
    If flg_ReqBloqueada = True Then
        BtAnularRecibo.Enabled = False
        BtImprimirRecibo.Enabled = False
        BtGerarRecibo.Enabled = False
        BtCobranca.Enabled = False
        BtPagamento.Enabled = False

        Exit Sub
    End If
    If FGRecibos.row > 0 And FGRecibos.TextMatrix(FGRecibos.row, 0) <> "" Then
        If RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Then
            BtAnularRecibo.Enabled = True
            BtImprimirRecibo.Enabled = False
            BtGerarRecibo.Enabled = False
            BtCobranca.Enabled = False
            BtPagamento.Enabled = False
        ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboPerdido Then
            BtAnularRecibo.Enabled = False
            BtImprimirRecibo.Enabled = False
            BtGerarRecibo.Enabled = True
            BtCobranca.Enabled = False
            BtPagamento.Enabled = False
        ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
            BtAnularRecibo.Enabled = True
            BtImprimirRecibo.Enabled = True
            BtGerarRecibo.Enabled = False
            BtCobranca.Enabled = True
            BtPagamento.Enabled = False
        ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Then
            BtAnularRecibo.Enabled = True
            BtImprimirRecibo.Enabled = True
            BtGerarRecibo.Enabled = False
            BtCobranca.Enabled = False
            BtPagamento.Enabled = True
        ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Then
            BtAnularRecibo.Enabled = False
            BtImprimirRecibo.Enabled = False
            BtGerarRecibo.Enabled = True
            BtCobranca.Enabled = False
            BtPagamento.Enabled = False
        ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo Then
            BtAnularRecibo.Enabled = False
            BtImprimirRecibo.Enabled = False
            BtGerarRecibo.Enabled = False
            BtCobranca.Enabled = False
            BtPagamento.Enabled = False
        End If
    Else
        BtCobranca.Enabled = False
        BtPagamento.Enabled = False
        BtAnularRecibo.Enabled = False
        BtImprimirRecibo.Enabled = False
        BtGerarRecibo.Enabled = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGRecibos_Click: " & Err.Number & " - " & Err.Description, Me.Name, "FGRecibos_Click"
    Exit Sub
    Resume Next

End Sub

Private Sub FGRecibos_DblClick()
    On Error GoTo TrataErro
    Dim i As Long

    If FGRecibos.row > 0 And FGRecibos.TextMatrix(FGRecibos.row, 0) <> "" Then
        If FGRecibos.Col = lColRecibosPercentagem And gPermAltPreco = mediSim Then
            If RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or (RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo And RegistosR(FGRecibos.row).Desconto <> 0) Then
                EcAuxRec.text = FGRecibos.TextMatrix(FGRecibos.row, FGRecibos.Col)
                EcAuxRec.left = FGRecibos.CellLeft + 270
                EcAuxRec.top = SSTGestReq.top + FGRecibos.CellTop + 600
                EcAuxRec.Width = FGRecibos.CellWidth + 20
                EcAuxRec.Visible = True
                EcAuxRec.Enabled = True
                EcAuxRec.SetFocus
            End If
        ElseIf FGRecibos.Col = lColRecibosValorPagar And gPermAltPreco = mediSim Then
            If RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or (RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo And RegistosR(FGRecibos.row).Desconto <> 0) Then
                EcAuxRec.text = FGRecibos.TextMatrix(FGRecibos.row, FGRecibos.Col)
                EcAuxRec.left = FGRecibos.CellLeft + 270
                EcAuxRec.top = SSTGestReq.top + FGRecibos.CellTop + 600
                EcAuxRec.Width = FGRecibos.CellWidth + 20
                EcAuxRec.Visible = True
                EcAuxRec.Enabled = True
                EcAuxRec.SetFocus
            End If
        ElseIf FGRecibos.Col = lColRecibosBorla And gPermAltPreco = mediSim Then
            If RegistosR(FGRecibos.row).FlgBorla = mediSim Then
                RegistosR(FGRecibos.row).FlgBorla = mediNao
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosBorla) = "N�o"
            Else
                RegistosR(FGRecibos.row).FlgBorla = mediSim
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosBorla) = "Sim"
            End If

        ElseIf FGRecibos.Col = lColRecibosNaoConfmidade And gPermAltPreco = mediSim Then
            If RegistosR(FGRecibos.row).FlgNaoConforme = mediSim Then
                RegistosR(FGRecibos.row).FlgNaoConforme = mediNao
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosNaoConfmidade) = "Sim"
            Else
                RegistosR(FGRecibos.row).FlgNaoConforme = mediSim
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosNaoConfmidade) = "N�o"
            End If
        Else
            TotalAnaRecibo = 0
            ReDim EstrutAnaRecibo(0)

            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboEntidade = RegistosR(FGRecibos.row).codEntidade Then
                    If CLng(RegistosRM(i).ReciboNumDoc) = CLng(BL_HandleNull(RegistosR(FGRecibos.row).NumDoc, "0")) Then
                        TotalAnaRecibo = TotalAnaRecibo + 1
                        ReDim Preserve EstrutAnaRecibo(TotalAnaRecibo)

                        EstrutAnaRecibo(TotalAnaRecibo).codAna = RegistosRM(i).ReciboCodFacturavel
                        EstrutAnaRecibo(TotalAnaRecibo).descrAna = BL_SelCodigo("SLV_ANALISES_FACTUS", "DESCR_ANA", "COD_ANA", RegistosRM(i).ReciboCodFacturavel)
                        EstrutAnaRecibo(TotalAnaRecibo).indice = i
                        EstrutAnaRecibo(TotalAnaRecibo).Flg_adicionada = RegistosRM(i).ReciboFlgAdicionado
                        If RegistosRM(i).ReciboFlgAdicionado = "1" Then
                            EstrutAnaRecibo(TotalAnaRecibo).taxa = 0
                        Else
                            EstrutAnaRecibo(TotalAnaRecibo).taxa = RegistosRM(i).ReciboTaxa
                        End If
                        EstrutAnaRecibo(TotalAnaRecibo).qtd = RegistosRM(i).ReciboQuantidade
                        If RegistosRM(i).ReciboFlgReciboManual = 1 Then
                            EstrutAnaRecibo(TotalAnaRecibo).tipo = "MANUAL"
                        Else
                            EstrutAnaRecibo(TotalAnaRecibo).tipo = "AUTOM"
                        End If

                    End If
                End If
            Next
            LimpaFgRecAna
            For i = 1 To TotalAnaRecibo
                FgAnaRec.TextMatrix(i, lColFgAnaRecCodAna) = EstrutAnaRecibo(i).codAna
                FgAnaRec.TextMatrix(i, lColFgAnaRecDescrAna) = EstrutAnaRecibo(i).descrAna
                FgAnaRec.TextMatrix(i, lColFgAnaRecQtd) = EstrutAnaRecibo(i).qtd
                FgAnaRec.TextMatrix(i, lColFgAnaRecTaxa) = EstrutAnaRecibo(i).taxa
                FgAnaRec.AddItem ""
            Next
            InicializaFrameAnaRecibos
            FrameAnaRecibos.Visible = True
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGRecibos_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "FGRecibos_DblClick"
    Exit Sub
    Resume Next
End Sub


Public Sub FgProd_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro

    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
        If LastColP <> 0 And Trim(FgProd.TextMatrix(LastRowP, 0)) = "" Then
            Beep
            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um produto!"
        Else
            If LastColP = 6 And EcDtColheita.Enabled = False Then
                Beep
                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este produto!"
            Else
                If LastColP = 0 And gLAB = "CHVNG" Then
                Else
                EcAuxProd.text = FgProd.TextMatrix(FgProd.row, FgProd.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                If KeyCode = 8 Then
                    EcAuxProd.text = Mid(EcAuxProd.text, 1, Len(EcAuxProd.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxProd.SelStart = 0
                    EcAuxProd.SelLength = Len(EcAuxProd)
                Else
                    EcAuxProd.SelLength = 0
                End If
                EcAuxProd.left = FgProd.CellLeft + 270
                EcAuxProd.top = FgProd.CellTop + 2060
                EcAuxProd.Width = FgProd.CellWidth + 20
                EcAuxProd.Visible = True
                EcAuxProd.Enabled = True
                EcAuxProd.SetFocus
                End If
            End If
        End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
        Select Case FgProd.Col
            Case 0
                If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
                    If FgProd.row < FgProd.rows - 1 Then
                        RegistosP.Remove FgProd.row
                        FgProd.RemoveItem FgProd.row
                    Else
                        FgProd.TextMatrix(FgProd.row, 0) = ""
                        FgProd.TextMatrix(FgProd.row, 1) = ""
                        FgProd.TextMatrix(FgProd.row, 2) = ""
                        FgProd.TextMatrix(FgProd.row, 3) = ""
                        FgProd.TextMatrix(FgProd.row, 4) = ""
                        FgProd.TextMatrix(FgProd.row, 5) = ""
                        FgProd.TextMatrix(FgProd.row, 6) = ""
                        FgProd.TextMatrix(FgProd.row, 7) = ""
                        RegistosP(FgProd.row).CodProd = ""
                        RegistosP(FgProd.row).DescrProd = ""
                        RegistosP(FgProd.row).EspecifObrig = ""
                        RegistosP(FgProd.row).CodEspecif = ""
                        RegistosP(FgProd.row).DescrEspecif = ""
                        RegistosP(FgProd.row).DtPrev = ""
                        RegistosP(FgProd.row).DtChega = ""
                        RegistosP(FgProd.row).EstadoProd = ""
                        RegistosP(FgProd.row).Volume = ""
                    End If
                End If
            Case 2
                FgProd.TextMatrix(FgProd.row, 2) = ""
                FgProd.TextMatrix(FgProd.row, 3) = ""
                RegistosP(FgProd.row).CodEspecif = ""
                RegistosP(FgProd.row).DescrEspecif = ""
            Case 4
                FgProd.TextMatrix(FgProd.row, 4) = ""
                RegistosP(FgProd.row).Volume = ""
            Case 5
                FgProd.TextMatrix(FgProd.row, 5) = ""
                RegistosP(FgProd.row).DtPrev = ""
            Case 6
                FgProd.TextMatrix(FgProd.row, 6) = ""
                RegistosP(FgProd.row).DtChega = ""
        End Select
        FgProd.SetFocus
    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FgProd.TextMatrix(FgProd.row, 1) & " " & FgProd.TextMatrix(FgProd.row, 3) & " ?", vbYesNo + vbQuestion, "Eliminar produto.") = vbYes Then
                If FgProd.row < FgProd.rows - 1 Then
                    RegistosP.Remove FgProd.row
                    FgProd.RemoveItem FgProd.row
                Else
                    FgProd.TextMatrix(FgProd.row, 0) = ""
                    FgProd.TextMatrix(FgProd.row, 1) = ""
                    FgProd.TextMatrix(FgProd.row, 2) = ""
                    FgProd.TextMatrix(FgProd.row, 3) = ""
                    FgProd.TextMatrix(FgProd.row, 4) = ""
                    FgProd.TextMatrix(FgProd.row, 5) = ""
                    FgProd.TextMatrix(FgProd.row, 6) = ""
                    FgProd.TextMatrix(FgProd.row, 7) = ""
                    RegistosP(FgProd.row).CodProd = ""
                    RegistosP(FgProd.row).DescrProd = ""
                    RegistosP(FgProd.row).EspecifObrig = ""
                    RegistosP(FgProd.row).CodEspecif = ""
                    RegistosP(FgProd.row).DescrEspecif = ""
                    RegistosP(FgProd.row).DtPrev = ""
                    RegistosP(FgProd.row).DtChega = ""
                    RegistosP(FgProd.row).EstadoProd = ""
                    RegistosP(FgProd.row).Volume = ""

                End If
            End If
        End If
        FgProd.SetFocus
    Else
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_KeyDown"
    Exit Sub
    Resume Next

End Sub

Public Sub FGTubos_KeyDown(KeyCode As Integer, Shift As Integer)
     On Error GoTo TrataErro
'
'    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
'        If LastColT <> 0 And Trim(FGTubos.TextMatrix(LastRowT, 0)) = "" Then
'            Beep
'            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um tubo!"
'        Else
'            If LastColT = 3 And EcDtColheita.Enabled = False Then
'                Beep
'                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este tubo!"
'            Else
'                EcAuxTubo.text = FGTubos.TextMatrix(FGTubos.row, FGTubos.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
'                If KeyCode = 8 Then
'                    EcAuxTubo.text = Mid(EcAuxTubo.text, 1, Len(EcAuxTubo.text) - 1)
'                End If
'                If KeyCode = 13 Then
'                    EcAuxTubo.SelStart = 0
'                    EcAuxTubo.SelLength = Len(EcAuxTubo)
'                Else
'                    EcAuxTubo.SelLength = 0
'                End If
'                EcAuxTubo.left = FGTubos.CellLeft + 270
'                EcAuxTubo.top = FGTubos.CellTop + 2060
'                EcAuxTubo.Width = FGTubos.CellWidth + 20
'                EcAuxTubo.Visible = True
'                EcAuxTubo.Enabled = True
'                EcAuxTubo.SetFocus
'            End If
'        End If
'    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
'        Select Case FGTubos.Col
'            Case 0
'                If Trim(FGTubos.TextMatrix(FGTubos.row, 0)) <> "" And Procura_Tubo_ana = False Then
'                    AcrescentaTubosEliminados EcNumReq, RegistosT(FGTubos.row).seqTubo
'                    If FGTubos.row < FGTubos.rows - 1 Then
'                        RegistosT.Remove FGTubos.row
'                        FGTubos.RemoveItem FGTubos.row
'                    Else
'                        FGTubos.TextMatrix(FGTubos.row, 0) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 1) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 2) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 3) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 4) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 6) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 7) = ""
'                        RegistosT(FGTubos.row).seqTubo = mediComboValorNull
'                        RegistosT(FGTubos.row).CodTubo = ""
'                        RegistosT(FGTubos.row).descrTubo = ""
'                        RegistosT(FGTubos.row).garrafa = ""
'                        RegistosT(FGTubos.row).DtPrev = ""
'                        RegistosT(FGTubos.row).DtChega = ""
'                        RegistosT(FGTubos.row).HrChega = ""
'                        RegistosT(FGTubos.row).UserChega = ""
'                        RegistosT(FGTubos.row).EstadoTubo = ""
'                    End If
'                End If
'            Case 2  'Garrafa
'                FGTubos.TextMatrix(FGTubos.row, 2) = ""
'                RegistosT(FGTubos.row).garrafa = ""
'            Case 3  'Data Prevista
'                FGTubos.TextMatrix(FGTubos.row, 3) = ""
'                RegistosT(FGTubos.row).DtPrev = ""
'            Case 4  'Data Chegada
'                FGTubos.TextMatrix(FGTubos.row, 4) = ""
'                RegistosT(FGTubos.row).DtChega = ""
'                FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                RegistosT(FGTubos.row).HrChega = ""
'            Case 5  'Hora Chegada
'                FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                RegistosT(FGTubos.row).HrChega = ""
'        End Select
'        FGTubos.SetFocus
'    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
'        If Trim(FGTubos.TextMatrix(FGTubos.row, 0)) <> "" And Procura_Tubo_ana = False Then
'            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FGTubos.TextMatrix(FgProd.row, 1) & " ?", vbYesNo + vbQuestion, "Eliminar tubo.") = vbYes Then
'                AcrescentaTubosEliminados EcNumReq, RegistosT(FGTubos.row).seqTubo
'                If FGTubos.row < FGTubos.rows - 1 Then
'                    RegistosT.Remove FGTubos.row
'                    FGTubos.RemoveItem FGTubos.row
'                Else
'                    FGTubos.TextMatrix(FGTubos.row, 0) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 1) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 2) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 3) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 4) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 6) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 7) = ""
'                    RegistosT(FGTubos.row).seqTubo = mediComboValorNull
'                    RegistosT(FGTubos.row).CodTubo = ""
'                    RegistosT(FGTubos.row).descrTubo = ""
'                    RegistosT(FGTubos.row).garrafa = ""
'                    RegistosT(FGTubos.row).DtPrev = ""
'                    RegistosT(FGTubos.row).DtChega = ""
'                    RegistosT(FGTubos.row).HrChega = ""
'                    RegistosT(FGTubos.row).UserChega = ""
'                    RegistosT(FGTubos.row).EstadoTubo = ""
'                End If
'            End If
'        End If
'        FGTubos.SetFocus
'    Else
'    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGTubos_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FGTubos_KeyDown"
    Exit Sub
    Resume Next
End Sub


Function SELECT_Descr_Prod() As Boolean
     On Error GoTo TrataErro

    'Fun��o que procura a descri��o do produto
    'A fun��o devolve TRUE se encontrou a descri��o caso contr�rio devolve FALSE

    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset

    SELECT_Descr_Prod = False
    With rsDescr
        .Source = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                  "cod_produto = " & BL_TrataStringParaBD(UCase(EcAuxProd.text))
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With

    If rsDescr.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Produto inexistente!", vbOKOnly + vbInformation, "Produtos"
        EcAuxProd.text = ""
    Else
        FgProd.TextMatrix(LastRowP, 1) = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).EspecifObrig = rsDescr!especif_obrig
        SELECT_Descr_Prod = True
    End If

    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Descr_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Descr_Prod"
    SELECT_Descr_Prod = False
    Exit Function
    Resume Next
End Function

Function SELECT_Especif() As Boolean
     On Error GoTo TrataErro

    Dim RsDescrEspecif As ADODB.recordset
    Dim encontrou As Boolean

    SELECT_Especif = False
    encontrou = False
    If Trim(EcAuxProd.text) <> "" Then
        Set RsDescrEspecif = New ADODB.recordset
        With RsDescrEspecif
            .Source = "SELECT descr_especif FROM sl_especif WHERE " & _
                    "cod_especif=" & UCase(BL_TrataStringParaBD(EcAuxProd.text))
            .ActiveConnection = gConexao
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
        If RsDescrEspecif.RecordCount > 0 Then
            FgProd.TextMatrix(LastRowP, 3) = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            RegistosP(LastRowP).DescrEspecif = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            SELECT_Especif = True
        Else
            BG_Mensagem mediMsgBox, "Especifica��o inexistente!", vbOKOnly + vbInformation, "Especifica��es"
        End If
        RsDescrEspecif.Close
        Set RsDescrEspecif = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Especif: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Especif"
    SELECT_Especif = False
    Exit Function
    Resume Next

End Function

Private Sub FgProd_LostFocus()
    On Error GoTo TrataErro

    FgProd.CellBackColor = vbWhite
    FgProd.CellForeColor = vbBlack
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_LostFocus"
    Exit Sub
    Resume Next

End Sub

Private Sub FGTubos_LostFocus()

    FGTubos.CellBackColor = vbWhite
    FGTubos.CellForeColor = vbBlack
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGTubos_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGTubos_LostFocus"
    Exit Sub
    Resume Next

End Sub

Private Sub FgProd_RowColChange()
     On Error GoTo TrataErro

    Dim tmpCol As Integer
    Dim tmpRow As Integer
    Dim RsEspecif As ADODB.recordset

    If ExecutaCodigoP = True Then
        ExecutaCodigoP = False


        If Trim(FgProd.TextMatrix(LastRowP, 0)) <> "" And Trim(FgProd.TextMatrix(LastRowP, 2)) = "" Then

            'Escolher a especifica��o de defeito do produto

            Set RsEspecif = New ADODB.recordset

            With RsEspecif
                .Source = "select sl_produto.cod_especif, sl_especif.descr_especif from sl_produto, sl_especif where sl_produto.cod_especif = sl_especif.cod_especif and cod_produto = " & BL_TrataStringParaBD(Trim(FgProd.TextMatrix(LastRowP, 0)))
                .CursorLocation = adUseServer
                .CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open , gConexao
            End With

            If Not RsEspecif.EOF Then
                FgProd.TextMatrix(LastRowP, 2) = Trim(BL_HandleNull(RsEspecif!cod_Especif, ""))
                FgProd.TextMatrix(LastRowP, 3) = Trim(BL_HandleNull(RsEspecif!descr_especif, ""))
            End If

            RsEspecif.Close
        End If

        Set RsEspecif = Nothing

        tmpCol = FgProd.Col
        tmpRow = FgProd.row
        If FgProd.Col <> LastColP Then FgProd.Col = LastColP
        If FgProd.row <> LastRowP Then FgProd.row = LastRowP
        FgProd.CellBackColor = vbWhite
        FgProd.CellForeColor = vbBlack
        If FgProd.Col <> tmpCol Then FgProd.Col = tmpCol
        If FgProd.row <> tmpRow Then FgProd.row = tmpRow

        If FgProd.row <> LastRowP Then
            Preenche_LaProdutos FgProd.row
        End If

        ' Controlar as colunas
        If FgProd.Col = 1 Then
            If LastColP >= 2 Then
                FgProd.Col = 0
            Else
                FgProd.Col = 2
            End If
        ElseIf FgProd.Col = 3 Then
            If LastColP >= 5 Then
                FgProd.Col = 2
            Else
                FgProd.Col = 5
            End If
        ElseIf LastColP = 5 And LastRowP = FgProd.row And FgProd.Col <> 6 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        ElseIf FgProd.Col > 4 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        End If

        LastColP = FgProd.Col
        LastRowP = FgProd.row

        FgProd.CellBackColor = azul
        FgProd.CellForeColor = vbWhite

        ExecutaCodigoP = True

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_RowColChange"
    Exit Sub
    Resume Next
End Sub

Private Sub FGTubos_RowColChange()
     On Error GoTo TrataErro

    Dim tmpCol As Integer
    Dim tmpRow As Integer

    If ExecutaCodigoT = True Then
        ExecutaCodigoT = False

        tmpCol = FGTubos.Col
        tmpRow = FGTubos.row
        If FGTubos.Col <> LastColT Then FGTubos.Col = LastColT
        If FGTubos.row <> LastRowT Then FGTubos.row = LastRowT
        FGTubos.CellBackColor = vbWhite
        FGTubos.CellForeColor = vbBlack
        If FGTubos.Col <> tmpCol Then FGTubos.Col = tmpCol
        If FGTubos.row <> tmpRow Then FGTubos.row = tmpRow

        If FGTubos.row <> LastRowT And FGTubos.row <> 0 Then
            Preenche_LaTubos FGTubos.row
        End If

        ' Controlar as colunas
        'verificar este controlo de colunas de na FGtubos '04-07-2006
        If FGTubos.Col = 1 Then
            If LastColT >= 2 Then
                FGTubos.Col = 0
            Else
                FGTubos.Col = 2
            End If
        ElseIf FGTubos.Col = 3 Then
            If LastColT >= 4 Then
                FGTubos.Col = 2
            Else
                FGTubos.Col = 4
            End If
        ElseIf LastColT = 4 And LastRowT = FGTubos.row And FGTubos.Col <> 5 Then
            FGTubos.Col = 0
            If FGTubos.row <> FGTubos.rows - 1 Then FGTubos.row = FGTubos.row + 1
        ElseIf FGTubos.Col > 4 Then
            FGTubos.Col = 0
            If FGTubos.row <> FGTubos.rows - 1 Then FGTubos.row = FGTubos.row + 1
        End If

        LastColT = FGTubos.Col
        LastRowT = FGTubos.row

        FGTubos.CellBackColor = azul
        FGTubos.CellForeColor = vbWhite

        ExecutaCodigoT = True

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGTubos_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "FGTubos_RowColChange"
    Exit Sub
    Resume Next
End Sub

Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Deactivate()

    Call BL_FimProcessamento(Me, "")

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

    Flg_LimpaCampos = False

End Sub

Private Sub Form_Load()

    EventoLoad

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Flg_LimpaCampos = False

End Sub

Private Sub Form_QueryUnload(cancel As Integer, UnloadMode As Integer)
    On Error GoTo TrataErro

    If UnloadMode <> 1 Then
        If Flg_Gravou = False Then
            If BG_Mensagem(mediMsgBox, "Deseja realmente sair sem gravar?", vbQuestion + vbYesNo, "Sislab") = vbNo Then
                'respondeu n�o
                cancel = True
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Form_QueryUnload: " & Err.Number & " - " & Err.Description, Me.Name, "Form_QueryUnload"
    Exit Sub
    Resume Next
End Sub

Private Sub Form_Unload(cancel As Integer)

    EventoUnload

End Sub

Sub FuncaoInserir(Optional not_msg As Boolean)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsNumBenef As New ADODB.recordset
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    Dim rv As Integer
    Dim mostraMgmPagar As Boolean
    mostraMgmPagar = False
    Dim RsVerifNReq As New ADODB.recordset

    If BL_VerificaCampoRequis(Me, EcNumReq, EcCodSala) = False Then
        Exit Sub
    End If



    If gPreencheDatasReq = mediSim Then
        If EcDtColheita.text = "" Then
            EcDtColheita.text = dataAct
        End If
        If EcDataChegada.text = "" Then
            EcDataChegada.text = dataAct
        End If
    End If


    If EcCodEFR = "" Then
            BG_Mensagem mediMsgBox, "Tem de indicar uma entidade fin.", vbInformation, "Aten��o"
            Sendkeys ("{HOME}+{END}")
            EcCodEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
    End If
    If EcCodSala <> "" Then
        If BL_VerificaSalaValida(EcCodSala, CStr(gCodLocal)) = False Then
            EcCodSala.text = ""
            EcDescrSala = ""
            EcCodSala.SetFocus
            Exit Sub
        End If
    End If

    If EcCodEFR <> "" Then
        If BL_VerificaEfrValida(EcCodEFR, mediComboValorNull) = False Then
            EcCodEFR.text = ""
            EcDescrEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
        End If
    Else
        Exit Sub
    End If






    If not_msg = True Then
        gMsgResp = vbYes
    Else
        gMsgTitulo = "Inserir"
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    End If

    Me.SetFocus

    If gMsgResp = vbYes Then

        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = dataAct
        EcHoraCriacao = Bg_DaHora_ADO

        'se a data prevista estiver vazia
        If EcDtColheita.text = "" Then
            EcDtColheita.text = Format(dataAct, gFormatoData)
        End If

        ValidaDataPrevista
'        If DateValue(EcDtColheita.Text) < DateValue(dataAct) Then Exit Sub

        ' Obriga a especifica��o de produtos.
        If (gObrigaProduto) Then
            If Not (ValidaProdutos) Then
                MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
                Exit Sub
            End If
        End If

        If Len(EcGrupoAna.text) = 1 Then
            If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count > 1 Then
                MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            End If
        End If


        If Not ValidaEspecifObrig Then Exit Sub

        iRes = ValidaCamposEc
        If iRes = True Then

            Call BD_Insert
            If Trim(EcNumReq.text) <> "" Then
                'Se n�o houve erro
                If (BL_HandleNull(BL_SelCodigo("SL_COD_SALAS", "FLG_COLHEITA", "COD_SALA", EcCodSala), "") = "0" And gNReqPreImpressa <> 1) Or gImprAutoEtiq = mediSim Then
                    If gUsaNovaImpEtiquetas = mediSim Then
                        If gMostraMgmPagaRecibo = mediNao Then
                            ImprimeEtiqNovo
                        End If
                    Else

                        ImprimeEtiq
                    End If
                    DoEvents
                End If
                BL_Toolbar_BotaoEstado "Inserir", "InActivo"
                BL_Toolbar_BotaoEstado "Procurar", "Activo"
                BL_Toolbar_BotaoEstado "Limpar", "Activo"
                'Mart. ergonomico
                Call FuncaoProcurar

                If gTipoInstituicao = "PRIVADA" And RegistosR.Count > 1 Then
                    If gMostraMgmPagaRecibo <> mediNao Then
                        For i = 1 To RegistosR.Count - 1
                            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Then
                                If RegistosR(i).ValorPagar > 0 Then
                                    mostraMgmPagar = True
                                    Exit For
                                End If
                            End If
                        Next
                        If mostraMgmPagar = True Then
                            If EcDataChegada.text = "" Then
                                BG_Mensagem mediMsgBox, "Requisi��o sem data de chegada. ", vbInformation, "Aten��o"
                                Sendkeys ("{HOME}+{END}")
                                EcDataChegada.text = ""
                                EcDataChegada.SetFocus
                            Else

                                gMsgTitulo = "Recibos"
                                gMsgMsg = "Utente vai pagar?"
                                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                                If gMsgResp = vbYes Then
                                    VerificaUtentePagaActoMedico RegistosR(i).codEntidade, RegistosR(i).SerieDoc, RegistosR(i).NumDoc
                                    UPDATE_Recibos_Req
                                    flgEmissaoRecibos = True
                                    FormEmissaoRecibos.Form = Me.Name
                                    FormEmissaoRecibos.Show 'vbModal
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If

    If (gFormGesReqCons_Aberto = True) Then
        ' Quando este form foi invocado atrav�s de FormGesReqCons,
        ' apaga a marca��o correspondente.

        rv = MARCACAO_Apaga(FormGesReqCons.EcNumReq.text)
        FormGesReqCons.LimpaCampos
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoInserir: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoInserir"
    Exit Sub
    Resume Next
End Sub

Sub BD_Insert()

    Dim SQLQuery As String
    Dim i As Integer
    Dim k As Integer
    Dim req As Long
    Dim ProdJaChegou As Boolean
    Dim rv As Integer

    On Error GoTo Trata_Erro

    gSQLError = 0
    gSQLISAM = 0

    'soliveira arunce
    'Utiliza etiquetas pr�-impressas com indica��o do n�mero de requisi��o final
    If gNReqPreImpressa = 1 Then
        req = EcNumReq.text
    Else
        'Determinar o novo n�mero da requisi��o (TENTA 10 VEZES - LOCK!)
        i = 0
        req = -1
        While req = -1 And i <= 10
            req = BL_GeraNumero("N_REQUIS")
            i = i + 1
        Wend

        If req = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero de requisi��o !", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If

        EcNumReq.text = ""
        EcNumReq.text = req
    End If



    gRequisicaoActiva = CLng(EcNumReq.text)
    EcLocal = gCodLocal

    BG_BeginTransaction

    If EcNumColheita.text = "" Then
        EcNumColheita.text = COLH_InsereNovaColheita(CLng(EcSeqUtente.text))
    End If
    If EcNumColheita.text = "" Then
        BG_Mensagem mediMsgBox, "Erro ao gravar colheita!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    Else
        If COLH_AssociaReqColheita(EcNumReq.text, EcNumColheita.text, CLng(EcSeqUtente.text)) = "" Then
            BG_Mensagem mediMsgBox, "Erro ao associar requisi��o a colheita!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
    End If
    ' Produtos da requisi��o
    Grava_Prod_Req
    If gSQLError <> 0 Then
        'Erro a gravar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    ' Tubos da requisi��o
    'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
    TB_GravaTubosReq EcNumReq.text, EcDataChegada.text
    If gSQLError <> 0 Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    ' RECIBOS DA REQUISICAO
    If RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False) = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os recibos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    ' ADIANTAMENTOS DA REQUISICAO
    If RECIBO_GravaAdiantamento(EcNumReq, RegistosAD) = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os adiantamentos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    ' An�lises da requisi��o
    Call Grava_Ana_Req
    If gSQLError <> 0 Then
        'Erro a gravar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'GRAVA PERFIS MARCADOS
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar perfis marcados", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'Actualizar estado da requisi��o em conformidade com an�lises marcadas, produtos marcados e sua data de chegada
    If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or EcEstadoReq = gEstadoReqEsperaProduto Or EcEstadoReq = gEstadoReqEsperaResultados Or EcEstadoReq = gEstadoReqSemAnalises Then
        ProdJaChegou = False
        If FGAna.rows >= 1 And Trim(FGAna.TextMatrix(1, lColFgAnaCodigo)) <> "" Then
            If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                For k = 1 To RegistosP.Count - 1
                    If Trim(RegistosP(1).DtChega) <> "" Then
                        ProdJaChegou = True
                    End If
                Next k
                If ProdJaChegou = True Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                Else
                    EcEstadoReq = gEstadoReqEsperaProduto
                End If
            Else
                EcEstadoReq = gEstadoReqEsperaResultados
                EcDataChegada.text = dataAct
            End If
        Else
           EcEstadoReq = " "
        End If
    End If
    EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)


    EcGrupoAna.text = ""




    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If

    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)

    ' Gravar os restantes dados da requisi��o
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    BDInsertAguas
    If gSQLError <> 0 Then
        'Erro a actualizar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a inserir dados da requisi��o - �guas !", vbError, "ERRO"
        GoTo Trata_Erro
    End If



    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        BG_CommitTransaction
        gRequisicaoActiva = CLng(EcNumReq.text)
        Flg_Gravou = True

    End If

    Exit Sub

Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a inserir requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicaoAguas: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar
    Exit Sub
    Resume Next
End Sub


Sub Grava_Ana_Req()
    On Error GoTo TrataErro

    Dim i As Integer
    Dim k As Integer
    Dim sSql As String

    Dim maximo As Long
    Dim sql As String
    Dim RsOrd As New ADODB.recordset
    Dim analises As String
    Dim flg_realiza As Boolean
    Dim seqReqTubo As String
    Set RsOrd = New ADODB.recordset


    If UBound(MaReq) = 0 Then
        BG_Mensagem mediMsgBox, "N�o foram registadas an�lises!", vbInformation, "Grava��o de an�lises"
        SSTGestReq.Tab = 2
    End If

    flg_realiza = False

    'Marca��o de an�lises (sl_marcacoes)
    For i = 1 To UBound(MaReq)

        MaReq(i).dt_chega = EcDataChegada.text  'Bruno & sdo

        'S� s�o gravadas as an�lise "novas" ou que estavam na tabela de marca��es, as an�lises da tabela de realiza��es n�o s�o marcadas
            '-1 -> an�lise nova ou an�lise da tabela sl_marca�oes
        sSql = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup) & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup) & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup)

        RsOrd.CursorType = adOpenStatic
        RsOrd.CursorLocation = adUseServer
        RsOrd.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsOrd.Open sSql, gConexao

        'Set RsOrd = CmdOrdAna.Execute

        If Not RsOrd.EOF Then
            If BL_HandleNull(RsOrd!ordem, 0) <> 0 Then
                MaReq(i).Ord_Ana = BL_HandleNull(RsOrd!ordem, 0)
            Else
                maximo = 0
                For k = 1 To UBound(MaReq)
                    If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
                Next k
                maximo = maximo + 1
                'Colocar a an�lise para o fim
                MaReq(i).Ord_Ana = maximo
            End If
            If RsOrd!gr_ana <> "" Then
                If EcGrupoAna.text = "" Then
                    EcGrupoAna.text = RsOrd!gr_ana
                Else
                    If InStr(1, EcGrupoAna.text, RsOrd!gr_ana) <> 0 Then
                    Else
                        EcGrupoAna.text = EcGrupoAna.text & ";" & RsOrd!gr_ana
                    End If
                End If
            End If
        Else
            'Colocar a an�lise para o fim
            maximo = 0
            For k = 1 To UBound(MaReq)
                If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
            Next k
            maximo = maximo + 1
            'Colocar a an�lise para o fim
            MaReq(i).Ord_Ana = maximo
        End If
        RsOrd.Close

        'MaReq(i).Ord_Ana = i

        If MaReq(i).flg_estado = "-1" Then
            If VerificaAnaliseJaExisteRealiza(CLng(Trim(EcNumReq.text)), UCase((MaReq(i).Cod_Perfil)), UCase((MaReq(i).cod_ana_c)), UCase((MaReq(i).cod_ana_s)), True) > 0 Then
                flg_realiza = True
            Else
                Dim RsTrans As ADODB.recordset
                'verifica se a flag "flg_transmanual" na an�lise est� a 1,
                'se sim coloca "flg_apar_trans" a 2 em sl_marcacoes
                Set RsTrans = New ADODB.recordset
                RsTrans.CursorLocation = adUseServer
                RsTrans.CursorType = adOpenStatic
                sql = "SELECT flg_transmanual FROM sl_ana_s WHERE cod_ana_s=" & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                RsTrans.Open sql, gConexao

                If (Not (RsTrans.EOF)) Then
                    If (RsTrans!flg_transManual = 1 And MaReq(i).Flg_Apar_Transf = 0) Then
                        MaReq(i).Flg_Apar_Transf = 2
                    End If
                End If
                If MaReq(i).seq_req_tubo > mediComboValorNull Then
                    seqReqTubo = MaReq(i).seq_req_tubo
                Else
                    seqReqTubo = " NULL "
                End If
                sql = "INSERT INTO sl_marcacoes(n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,flg_apar_trans,ord_marca, flg_facturado, transmit_psm, n_envio, seq_req_tubo"
                sql = sql & ") VALUES (" & CLng(Trim(EcNumReq.text)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s)) & "," & MaReq(i).Ord_Ana
                sql = sql & "," & MaReq(i).Ord_Ana_Compl & "," & MaReq(i).Ord_Ana_Perf & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_agrup)) & "," & MaReq(i).N_Folha_Trab & "," & IIf((MaReq(i).dt_chega = ""), "NULL", BL_TrataDataParaBD(MaReq(i).dt_chega)) & ","
                sql = sql & UCase(BL_TrataStringParaBD(MaReq(i).Flg_Apar_Transf)) & "," & MaReq(i).Ord_Marca & ", " & MaReq(i).Flg_Facturado & ", " & MaReq(i).transmit_psm & ", " & MaReq(i).n_envio & ", " & seqReqTubo & ")"
                BG_ExecutaQuery_ADO sql


                If gSQLError <> 0 Then
                    Exit For
                End If

                RsTrans.Close
                Set RsTrans = Nothing
            End If

        'soliveira 08-09-2003
        'para alterar a flag flg_facturado quando as analises j� se encontram na tabela sl_realiza
        'ElseIf MaReq(i).Flg_Facturado = 1 Then 'alterei por causa do tratamento de rejeitados
        Else
            If EcEstadoReq = gEstadoReqHistorico Then
                sql = "UPDATE sl_realiza_h set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & BL_TrataStringParaBD(CStr(Trim(EcNumReq.text))) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            Else
                sql = "UPDATE sl_realiza set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & BL_TrataStringParaBD(CStr(Trim(EcNumReq.text))) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            End If
            BG_ExecutaQuery_ADO sql
        End If

        '----------------
        If InStr(1, analises, MaReq(i).cod_agrup) = 0 Then
            analises = analises & IIf(BL_HandleNull(MaReq(i).cod_agrup, "") <> "", BL_TrataStringParaBD(MaReq(i).cod_agrup) & ",", "")
        End If

    Next i

    Set RsOrd = Nothing

    Call RegistaTubosEliminados
    Call RegistaEliminadas
    Call RegistaAcrescentadas
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Ana_Req"
    Exit Sub
    Resume Next
End Sub

Sub Grava_Prod_Req()
    On Error GoTo TrataErro

    Dim sql As String
    Dim i As Integer
    Dim MenorData As String
    Dim DataPrevistaAux As String
    Dim TotalRegistos As Long
    Dim Perguntou As Boolean

    Perguntou = False
    If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
        'determinar a menor data da lista de produtos
        MenorData = RegistosP(1).DtPrev
        For i = 1 To RegistosP.Count - 1
            If (RegistosP(i).DtChega) <> "" Then
                If DateValue(RegistosP(i).DtPrev) < DateValue(MenorData) And DateValue(RegistosP(i).DtChega) > DateValue(EcDtColheita) Then MenorData = RegistosP(i).DtPrev
            End If
        Next i
        If DateValue(MenorData) > DateValue(EcDtColheita.text) Then
            gMsgTitulo = "ATEN��O"
            gMsgMsg = "Existe um produto com data inferior � data prevista, deseja alterar a data prevista para " & MenorData & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                DataPrevistaAux = EcDtColheita.text
                EcDtColheita = DateValue(MenorData)
                sql = "UPDATE sl_requis SET dt_previ=" & BL_TrataDataParaBD(EcDtColheita.text) & " WHERE dt_previ=" & BL_TrataDataParaBD(DataPrevistaAux) & " AND n_req=" & EcNumReq.text
                BG_ExecutaQuery_ADO sql, gConexao
                If gSQLError <> 0 Then Exit Sub
            End If
        End If

        TotalRegistos = RegistosP.Count - 1
        For i = 1 To TotalRegistos
            If Trim(RegistosP(i).CodProd) <> "" Then
                If RegistosP(i).DtChega = "" And EcDataChegada.text <> "" Then
                    'se a data de chegada estiver vazia vai preenche-la
                    If Flg_PreencherDtChega = True Then
                        RegistosP(i).DtChega = DateValue(EcDataChegada.text)
                        FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
                    ElseIf Perguntou = False Then
'                        If gLAB <> "BIO" Then
'                        If BG_Mensagem(mediMsgBox, "Deseja preencher a data de chegada dos produtos com a data '" & EcDataChegada.Text & "' ?", vbQuestion + vbYesNo, "Data Chegada") = vbYes Then
'                            RegistosP(i).DtChega = DateValue(EcDataChegada.Text)
'                            FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
'                            Flg_PreencherDtChega = True
'                        End If
                        Perguntou = True
'                        End If
                    End If
                End If
                sql = "INSERT INTO sl_req_prod(n_req,cod_prod,cod_especif,dt_previ,dt_chega, volume, obs_especif) VALUES(" & EcNumReq.text & _
                    "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodProd)) & "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodEspecif)) & "," & BL_TrataDataParaBD(RegistosP(i).DtPrev) & "," & BL_TrataDataParaBD(RegistosP(i).DtChega) & ", " & BL_TrataStringParaBD(RegistosP(i).Volume) & ", " & BL_TrataStringParaBD(RegistosP(i).ObsEspecif) & ")"
                BG_ExecutaQuery_ADO sql

                If gSQLError <> 0 Then
                    i = TotalRegistos + 1
                End If
            End If
        Next i

        If gSQLError <> 0 Then Exit Sub

        'Actualizar o estado da requisi��o se foi introduzida data de chegada (dt_chega)
        If Flg_PreencherDtChega Then
            sql = "UPDATE sl_requis SET estado_req='A' WHERE n_req=" & EcNumReq.text
            BG_ExecutaQuery_ADO sql
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_Prod_Req: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Prod_Req"
    Exit Sub
    Resume Next
End Sub


Public Sub FuncaoModificar(Optional not_msg As Boolean)
     On Error GoTo TrataErro

    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    
    ' Obriga a especifica��o de produtos.
    If (gObrigaProduto) Then
        If Not (ValidaProdutos) Then
            MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                "A modifica��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
    End If
    If Len(EcGrupoAna.text) = 1 Then
        If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count - 1 > 1 Then
            MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
    End If

    If EcCodEFR = "" Then
        BG_Mensagem mediMsgBox, "Tem de indicar uma entidade fin.", vbInformation, "Aten��o"
        Sendkeys ("{HOME}+{END}")
        EcCodEFR = ""
        EcCodEFR.SetFocus
        Exit Sub
    End If



    If gPreencheDatasReq = mediSim Then
        If EcDtColheita.text = "" Then
            EcDtColheita.text = dataAct
        End If
        If EcDataChegada.text = "" Then
            EcDataChegada.text = dataAct
        End If
    End If


    If not_msg = True Then
        gMsgResp = vbYes
    Else
        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    End If

    If gMsgResp = vbYes Then

        'Impedir que se alterem valores de cria��o / impress�o de registos
        EcDataCriacao = BL_HandleNull(rs!dt_cri, "")
        EcUtilizadorCriacao = BL_HandleNull(rs!user_cri, "")
        EcHoraCriacao = BL_HandleNull(rs!hr_cri, "")

        EcDataImpressao = BL_HandleNull(rs!dt_imp, "")
        EcUtilizadorImpressao = BL_HandleNull(rs!user_imp, "")
        EcHoraImpressao = BL_HandleNull(rs!hr_imp, "")

        EcDataImpressao2 = BL_HandleNull(rs!dt_imp2, "")
        EcUtilizadorImpressao2 = BL_HandleNull(rs!user_imp2, "")
        EcHoraImpressao2 = BL_HandleNull(rs!hr_imp2, "")

        EcDataAlteracao = dataAct
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = Bg_DaHora_ADO

        'se a data prevista estiver vazia
        If EcDtColheita.text = "" Then
            EcDtColheita.text = Format(dataAct, gFormatoData)
        End If



        If UBound(MaReq) > 0 Then
            If Trim(MaReq(1).cod_ana_s) <> "" Then
                If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                    EcEstadoReq = EcEstadoReq
                ElseIf EcEstadoReq = gEstadoReqEsperaProduto Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                End If
            Else
                EcEstadoReq = gEstadoReqSemAnalises
            End If
        Else
            EcEstadoReq = gEstadoReqSemAnalises
        End If
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)



        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)


        iRes = ValidaCamposEc
        If iRes = True Then

            Call BD_Update
            If Trim(EcNumReq.text) <> "" Then
                'Se n�o houve erro
                If gRequisicaoActiva = 0 Then
                Else
                    gRequisicaoActiva = EcNumReq.text
                End If

                RegistaAcrescentadas
                RegistaAlteracoesRequis EcNumReq
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoModificar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoModificar"
    Exit Sub
    Resume Next
End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim k As Integer
    Dim ProdJaChegou As Boolean

    On Error GoTo Trata_Erro

    gSQLError = 0
    gSQLISAM = 0

    BG_BeginTransaction


    'actualizar os produtos da requisi��o
    UPDATE_Prod_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'actualizar os tubos da requisi��o
    UPDATE_Tubos_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If


    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'actualizar os recibos da requisi��o

    If UPDATE_Recibos_Req = False Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os recibos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    ' ADIANTAMENTOS DA REQUISICAO
    If RECIBO_GravaAdiantamento(EcNumReq, RegistosAD) = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os adiantamentos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'actualizar as an�lises da requisi��o
    UPDATE_Ana_Req
    If gSQLError <> 0 Then
        'Erro a actualizar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If


    'GRAVA PERFIS MARCADOS
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar perfis marcados", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If

    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)
    EcEstadoReq.text = BL_MudaEstadoReq(CLng(EcNumReq))

    'gravar restantes dados da requisicao
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery

    BDUpdateAguas
    If gSQLError <> 0 Then
        'Erro a actualizar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar dados da requisi��o - �guas !", vbError, "ERRO"
        GoTo Trata_Erro
    End If


    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        Flg_Gravou = True
        gRequisicaoActiva = CLng(EcNumReq)
        BG_CommitTransaction

        EcEstadoReq.text = BL_MudaEstadoReq(CLng(EcNumReq))

        If (EcEstadoReq.text = gEstadoReqSemAnalises) Then
            Me.EcDataChegada.Enabled = True
        Else
            Me.EcDataChegada.Enabled = False
        End If

        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)

    End If

    MarcaLocal = rs.Bookmark
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rs.Bookmark = MarcaLocal

    'CbSituacao.SetFocus
    EcNumReq.SetFocus
    Exit Sub

Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a modificar requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicaoAguas: BD_Update -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar
    Resume Next

End Sub



Sub UPDATE_Prod_Req()
    On Error GoTo TrataErro
   Dim sql As String

   gSQLError = 0
   sql = "DELETE FROM sl_req_prod WHERE n_req=" & Trim(EcNumReq.text)
   BG_ExecutaQuery_ADO sql

   If gSQLError <> 0 Then
        Exit Sub
   End If
   Grava_Prod_Req
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Prod_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Prod_Req"
    Exit Sub
    Resume Next
End Sub
Sub UPDATE_Tubos_Req()
    On Error GoTo TrataErro
    'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
   TB_GravaTubosReq EcNumReq.text, EcDataChegada.text
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Tubos_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Tubos_Req"
    Exit Sub
    Resume Next
End Sub

Function UPDATE_Recibos_Req() As Boolean
     On Error GoTo TrataErro
     UPDATE_Recibos_Req = RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False)
Exit Function
TrataErro:
    UPDATE_Recibos_Req = False
    BG_LogFile_Erros "UPDATE_Recibos_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Recibos_Req"
    Exit Function
    Resume Next
End Function

Sub UPDATE_Ana_Req()
    On Error GoTo TrataErro

    Dim sql As String
    ActualizaFlgAparTrans
    gSQLError = 0
    'Apagar todas as requisi��es que n�o t�m dt_chega
    sql = "DELETE FROM sl_marcacoes WHERE n_req=" & Trim(EcNumReq.text)
    BG_ExecutaQuery_ADO sql

    If gSQLError <> 0 Then
        Exit Sub
    End If

    EcGrupoAna.text = ""

    Call Grava_Ana_Req
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Ana_Req"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoRemover()
    On Error GoTo TrataErro
    Dim i As Integer
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    For i = 1 To RegistosR.Count
        If RegistosR(i).NumDoc <> "0" And RegistosR(i).NumDoc <> "" Then
            If RegistosR(i).Estado = gEstadoReciboPago Then
                Call BG_Mensagem(mediMsgBox, "N�o pode cancelar requisi��es com recibos emitidos!", vbExclamation + vbOKOnly, App.ProductName)
                Exit Sub
            End If
        End If
    Next
    If Trim(EcEstadoReq) <> Trim(gEstadoReqSemAnalises) And EcEstadoReq <> gEstadoReqEsperaProduto And EcEstadoReq <> gEstadoReqEsperaResultados Then
        Call BG_Mensagem(mediMsgBox, "N�o pode cancelar requisi��es com resultados!", vbExclamation + vbOKOnly, App.ProductName)
        Exit Sub
    End If

    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"

    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind

    MarcaLocal = rs.Bookmark

    FormGestaoRequisicaoAguas.Enabled = False

    FormCancelarRequisicoes.Show

    SSTGestReq.TabEnabled(1) = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False


Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoRemover: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoRemover"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoProcurar()

    Dim SelTotal As Boolean
    Dim SimNao As Integer

    On Error GoTo Trata_Erro



    Set rs = New ADODB.recordset
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    CriterioTabela = Replace(CriterioTabela, "seq_utente = '(", "seq_utente IN (")
    CriterioTabela = Replace(CriterioTabela, "'(", "(")
    CriterioTabela = Replace(CriterioTabela, ")'", ")")
    If SelTotal = True Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos as requisi��es ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set rs = Nothing
            Exit Sub
        End If
    Else
    End If




    CriterioTabela = CriterioTabela & "  ORDER BY dt_chega DESC, n_req DESC"

    BL_InicioProcessamento Me, "A pesquisar registos."

    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao

    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        BL_ToolbarEstadoN Estado

        'inicializa��es da lista de marca��es de an�lises
        Inicializa_Estrutura_Analises

        'limpar grids
        LimpaFGAna

        LimpaFgProd
        LimpaFgTubos
        LimpaFgRecibos
        LimpaFgAdiantamentos
        

        Call PreencheCampos(True)


        Set CampoDeFocus = EcDtColheita

        ' SE NAO TIVER IMPRESSO VERIFICA SE JA FOI DISPONIBILIZADO PARA O ER
        ' FGONCALVES HMACEDO CAVALEIROS

        If BL_HandleNull(rs!dt_imp, "") = "" Then
            Dim rsEresults As New ADODB.recordset
            Dim sSqlEresults As String

            If gSGBD = gOracle Then
                sSqlEresults = " SELECT trunc(dt_cri) dt_imp, to_char(dt_cri, 'HH24:MI:SS') hr_imp FROM sl_eresults WHERE "
                sSqlEresults = sSqlEresults & " flg_estado = 1 AND cod_original = " & BL_TrataStringParaBD(rs!n_req)
            ElseIf gSGBD = gSqlServer Then
                sSqlEresults = " SELECT convert(VARCHAR(10),dt_cri ,105) dt_imp, convert(VARCHAR(10),dt_cri ,108) hr_imp FROM sl_eresults WHERE "
                sSqlEresults = sSqlEresults & " flg_estado = 1 AND cod_original = " & BL_TrataStringParaBD(rs!n_req)
            ElseIf gSGBD = gPostGres Then
                sSqlEresults = " SELECT to_char(dt_cri, 'DD-MM-YYYY') dt_imp, to_char(dt_cri, 'HH12:MI') hr_imp FROM sl_eresults WHERE "
                sSqlEresults = sSqlEresults & " flg_estado = 1 AND cod_original = " & BL_TrataStringParaBD(rs!n_req)
            End If
            rsEresults.CursorType = adOpenStatic
            rsEresults.CursorLocation = adUseServer
            rsEresults.LockType = adLockReadOnly
            If gModoDebug = mediSim Then BG_LogFile_Erros sSqlEresults
            rsEresults.Open sSqlEresults, gConexao
            If rsEresults.RecordCount > 0 Then
                EcDataImpressao = BL_HandleNull(rsEresults!dt_imp, "")
                EcHoraImpressao = BL_HandleNull(rsEresults!hr_imp, "")
                If EcDataImpressao <> "" Then
                    LbNomeUtilImpressao = "eResults"
                Else
                    LbNomeUtilImpressao = ""
                End If
            End If
            rsEresults.Close
            Set rsEresults = Nothing
        End If

        BL_FimProcessamento Me
        gRequisicaoActiva = CLng(EcNumReq.text)

        If rs!estado_req = gEstadoReqSemAnalises Or EcDataChegada.text = "" Then
            Me.EcDataChegada.Enabled = True
        Else
            Me.EcDataChegada.Enabled = False
        End If

    End If

    Exit Sub

Trata_Erro:

    BG_LogFile_Erros Me.Name & ": FuncaoProcurar (" & Err.Number & "-" & Err.Description & ")"
    Err.Clear
    BL_FimProcessamento Me
    BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
    FuncaoLimpar
    Resume Next

End Sub

Sub Funcao_CopiaReq()
    On Error GoTo TrataErro

    If gRequisicaoActiva <> 0 Then
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaReq"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoLimpar()
     On Error GoTo TrataErro

    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        gMsgTitulo = "Limpar"
        gMsgMsg = "Tem a certeza que quer LIMPAR o ecr�?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            LimpaCampos
            EcUtente.text = ""
            DoEvents

            CampoDeFocus.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoLimpar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoLimpar"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoEstadoAnterior()
    On Error GoTo TrataErro

    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoEstadoAnterior: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoEstadoAnterior"
    Exit Sub
    Resume Next
End Sub

Function ValidaCamposEc() As Integer
    On Error GoTo TrataErro

    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i

    ValidaCamposEc = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaCamposEc: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaCamposEc"
    ValidaCamposEc = -1
    Exit Function
    Resume Next
End Function

Sub LimpaCampos()
    On Error GoTo TrataErro

    Me.SetFocus


    EcNumColheita.text = ""
    BG_LimpaCampo_Todos CamposEc
    EcDescrEstado.BackColor = 14737632

    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    flg_novaArs = -1
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    EcUtilNomeConf = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDataNasc.text = ""
    EcSexo = ""
    EcIdade.text = ""
    EcNome.text = ""
    EcUtente.text = ""
    EcDescrEFR.text = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcPesqRapTubo.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrBtTubos.Enabled = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    EcCodEFR.locked = False
    BtPesquisaEntFin.Enabled = True
    EcCodEFR.ToolTipText = ""
    BtPesquisaEntFin.ToolTipText = ""
    EcCodEFR.ForeColor = vbBlack
    CbSala.ListIndex = mediComboValorNull
    CbDestino.ListIndex = mediComboValorNull
    CbDestino.ListIndex = mediComboValorNull
    EcCodEfr2 = ""
    EcDescrEfr2 = ""
    EcDtColheita.text = ""
    EcDescrSala = ""
    LimpaCamposAguas
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    EcDescrEstado = ""
    'Fun��o que limpa os tubos da requisi��o
    LaTubos.caption = ""

    CodEfrAUX = ""
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises



    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False


    CbTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcDtColheita.Enabled = True
    EcDataChegada.Enabled = True

    BtConsReq.Enabled = False
    Set CampoDeFocus = EcNumReq

    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    BtFichaCruzada.Enabled = False

    BtResumo.Enabled = False
    LimpaFgProd
    LimpaFgTubos
    LimpaFGAna

    LimpaFgRecibos
    LimpaFgAdiantamentos
    LimpaFgRecAna

    FrameAnaRecibos.Visible = False
    SSTGestReq.Tab = 0
    DoEvents



    SSTGestReq.Enabled = True

    TotalEliminadas = 0
    ReDim Eliminadas(0)
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)

    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    PreencheSalaDefeito

    BtResumo.Enabled = False

    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)

    flgEmissaoRecibos = False
    GereFrameAdiantamentos False

    flg_ReqBloqueada = False


    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    PM_LimpaEstrutura
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos"
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaCamposAguas()
    EcCodtipoAmostra.text = ""
    EcCodtipoAmostra_Validate False
    EcCodOrigemAmostra.text = ""
    EcCodOrigemAmostra_Validate False
    EcCodPontoColheita.text = ""
    EcCodPontoColheita_validate False
    EcLocalColheita.text = ""
    EcRespColheita.text = ""
End Sub
Sub LimpaCampos2()
    On Error GoTo TrataErro

    Me.SetFocus


    EcNumColheita.text = ""
    BG_LimpaCampo_Todos CamposEc
    EcDescrEstado.BackColor = 14737632

    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    flg_novaArs = -1
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDataNasc.text = ""
    EcSexo = ""
    EcIdade.text = ""
    EcNome.text = ""
    EcUtente.text = ""
    EcDescrEFR.text = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrBtTubos.Enabled = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    EcCodEFR.locked = False

    BtPesquisaEntFin.Enabled = True
    EcCodEFR.ToolTipText = ""
    BtPesquisaEntFin.ToolTipText = ""
    EcCodEFR.ForeColor = vbBlack


    CbSala.ListIndex = mediComboValorNull
    EcCodEfr2 = ""
    EcDescrEfr2 = ""

    CbDestino.ListIndex = mediComboValorNull

    EcDescrSala = ""
    EcUtilNomeConf = ""
    EcDescrEstado = ""



    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""

    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises

    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False

    CbTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcDtColheita.Enabled = True
    EcDataChegada.Enabled = True

    BtConsReq.Enabled = False
    Set CampoDeFocus = EcNumReq

    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True


    LimpaFgProd
    LimpaFgTubos
    LimpaFGAna

    LimpaFgRecibos
    LimpaFgAdiantamentos

    SSTGestReq.Tab = 0
    DoEvents






    TotalEliminadas = 0
    ReDim Eliminadas(0)
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)

    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)

    BtNotas.Visible = True
    BtNotasVRM.Visible = False

    PreencheSalaDefeito
    flgEmissaoRecibos = False
    GereFrameAdiantamentos False

    flg_ReqBloqueada = False
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    PM_LimpaEstrutura
    LimpaCamposAguas
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos2: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos2"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoAnterior()
    On Error GoTo TrataErro
    rs.MovePrevious

    If rs.BOF Then
        rs.MoveNext
        BG_MensagemAnterior
    Else

        Call LimpaCampos
        gFlg_JaExisteReq = True
        Call PreencheCampos(True)

        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabEnabled(4) = True
            BtCancelarReq.Visible = False

        End If
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoAnterior: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoAnterior"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoSeguinte()

    On Error GoTo TrataErro
    rs.MoveNext

    If rs.EOF Then
        rs.MovePrevious
        BG_MensagemSeguinte
    Else

        Call LimpaCampos
        gFlg_JaExisteReq = True
        Call PreencheCampos(True)

        ' Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados.
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabEnabled(4) = True
            BtCancelarReq.Visible = False

        End If
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoSeguinte: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoSeguinte"
    Exit Sub
    Resume Next
End Sub

Sub PreencheCampos(flg_preencheCampos As Boolean)
    Dim aux As String
    On Error GoTo TrataErro
    Dim i As Integer
    Me.SetFocus

    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BtCancelarReq.Visible = True
        BtNotas.Enabled = True
        BtNotasVRM.Enabled = True
        BtFichaCruzada.Enabled = True
        BtResumo.Enabled = True

        If flg_preencheCampos = True Then
            BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        End If
        EcNumColheita.text = COLH_DevolveNumColheita(EcNumReq.text)
        FuncaoProgurarAguas EcNumReq.text
        LbNomeUtilCriacao.caption = BL_SelNomeUtil(EcUtilizadorCriacao.text)
        LbNomeUtilCriacao.ToolTipText = LbNomeUtilCriacao.caption
        LbNomeUtilAlteracao.caption = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
        LbNomeUtilAlteracao.ToolTipText = LbNomeUtilAlteracao.caption
        LbNomeUtilImpressao.caption = BL_SelNomeUtil(EcUtilizadorImpressao.text)
        LbNomeUtilImpressao.ToolTipText = LbNomeUtilImpressao.caption
        LbNomeUtilImpressao2.caption = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
        LbNomeUtilImpressao2.ToolTipText = LbNomeUtilImpressao2.caption
        LbNomeUtilFecho.caption = BL_SelNomeUtil(EcUtilizadorFecho.text)
        LbNomeUtilFecho.ToolTipText = LbNomeUtilFecho.caption
        LbNomeLocal.caption = BL_SelCodigo("gr_empr_inst", "nome_empr", "empresa_id", EcLocal, "V")
        LbNomeLocal.ToolTipText = LbNomeLocal.caption
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcUtilNomeConf.ToolTipText = EcUtilNomeConf.caption

        BL_PreencheDadosUtente EcSeqUtente.text
        PreencheDescEntFinanceira
        PreencheEstadoRequisicao
        
        PM_CarregaPerfisMarcados EcNumReq.text

        EcNumReq.locked = True

        PreencheDadosUtente
        If EcDataNasc <> "" Then
            EcIdade = BG_CalculaIdade(CDate(EcDataNasc.text))
        End If

        'colocar o bot�o de cancelar requisi��es activo
        gRequisicaoActiva = CLng(EcNumReq.text)
        gFlg_JaExisteReq = True


        CbTipoUtente.Enabled = False
        EcUtente.Enabled = False
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FrBtTubos.Enabled = True
        If Trim(EcDataChegada.text) <> "" Then
            EcDataChegada.Enabled = False
        Else
            EcDataChegada.Enabled = True
        End If
        FuncaoProcurar_PreencheProd CLng(Trim(EcNumReq.text))
        TB_ProcuraTubos FGTubos, CLng(Trim(EcNumReq.text)), True
        FuncaoProcurar_PreencheAna CLng(Trim(EcNumReq.text))
        RECIBO_PreencheCabecalho EcNumReq, RegistosR
        RECIBO_PreencheDetManuais Trim(EcNumReq.text), RegistosRM, CbTipoUtente, EcUtente



        RECIBO_PreencheAdiantamento Trim(EcNumReq.text), RegistosAD
        ActualizaLinhasRecibos -1

        PreencheFgRecibos
        PreencheFgAdiantamentos
        PreencheNotas
        'CbSituacao.SetFocus
        EcNumReq.SetFocus

        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            If flg_preencheCampos = True Then
                SSTGestReq.TabEnabled(1) = True
                SSTGestReq.TabEnabled(2) = True
                SSTGestReq.TabEnabled(3) = True
                SSTGestReq.TabEnabled(4) = True


                BtCancelarReq.Visible = False
                If flg_ReqBloqueada = False Then
                    BL_Toolbar_BotaoEstado "Remover", "Activo"
                    BL_Toolbar_BotaoEstado "Modificar", "Activo"
                Else
                    BL_Toolbar_BotaoEstado "Remover", "InActivo"
                    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
                End If
            End If


        End If

        EcCodsala_Validate True
        CodEfrAUX = EcCodEFR
    End If
    flg_preencheCampos = False

Exit Sub
TrataErro:
    flg_preencheCampos = False
    BG_LogFile_Erros "PreencheCampos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCampos"
    Exit Sub
    Resume Next
End Sub

Sub PreencheEstadoRequisicao()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
       EcDescrEstado.BackColor = vermelhoClaro
    Else
       EcDescrEstado.BackColor = 14737632
    End If
   EcDescrEstado = BL_DevolveEstadoReq(Trim(BL_HandleNull(rs!estado_req, "")))

Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstadoRequisicao: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEstadoRequisicao"
    Exit Sub
    Resume Next

End Sub


Sub PreencheDescEntFinanceira()
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodEFR.text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_efr, flg_nova_ars FROM sl_efr WHERE cod_efr=" & Trim(EcCodEFR.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade inexistente!", vbOKOnly + vbInformation, "Entidades"
            EcCodEFR.text = ""
            EcDescrEFR.text = ""
            flg_novaArs = -1
            If (gLAB = cHSMARTA) Then
                Me.EcCodEFR = "0"
                Call EcCodEFR_Validate(True)
            End If

            EcCodEFR.SetFocus
        Else
            flg_novaArs = BL_HandleNull(Tabela!flg_nova_ars, 0)
            EcDescrEFR.text = Tabela!descr_efr
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescEntFinanceira: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescEntFinanceira"
    Exit Sub
    Resume Next

End Sub


Private Sub SSTGestReq_Click(PreviousTab As Integer)
    On Error GoTo TrataErro
    Dim i
    Dim j As Integer
    DoEvents

    If SSTGestReq.Tab = 1 Then
        If EcDataFecho.text <> "" Then
            BtPesquisaAna.Enabled = False
        Else
            BtPesquisaAna.Enabled = True
            FGAna.SetFocus
            FGAna.row = FGAna.rows - 1
            FGAna.Col = 0

        End If

    ElseIf SSTGestReq.Tab = 0 Then
        EcAuxAna.Visible = False
        EcAuxProd.Visible = False
        If EcDtColheita.Enabled = True And EcDtColheita.Visible = True Then
            If gMultiReport <> 1 Then
                EcDtColheita.SetFocus
            End If
        ElseIf EcDataChegada.Enabled = True And EcDataChegada.Visible = True Then
            'CbSituacao.SetFocus             'sdo
        End If

    ElseIf SSTGestReq.Tab = 2 Then
        If FgProd.Enabled = True Then FgProd.SetFocus
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "SSTGestReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "SSTGestReq_Click"
    Exit Sub
    Resume Next
End Sub

Sub Funcao_DataActual()
    On Error GoTo TrataErro

    If Me.ActiveControl.Name = "EcDataCriacao" Then
        BL_PreencheData EcDataCriacao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataAlteracao" Then
        BL_PreencheData EcDataAlteracao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao" Then
        BL_PreencheData EcDataImpressao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao2" Then
        BL_PreencheData EcDataImpressao2, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtColheita" Then
        BL_PreencheData EcDtColheita, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataChegada" Then
        BL_PreencheData EcDataChegada, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtColheita" Then
        BL_PreencheData EcDtColheita, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcAuxProd" Then
        If EcAuxProd.Tag = adDate Then
            BL_PreencheData EcAuxProd, Me.ActiveControl
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_DataActual: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_DataActual"
    Exit Sub
    Resume Next

End Sub

Function Coloca_Estado(DtPrev, DtChega, Optional DtElim = "") As String
    On Error GoTo TrataErro
    If DateValue(DtPrev) < DateValue(dataAct) And DtChega = "" Then
        Coloca_Estado = "Falta"
    ElseIf DateValue(DtPrev) >= DateValue(dataAct) And DtChega = "" Then
        Coloca_Estado = "Espera"
    ElseIf DtChega <> "" Then
        Coloca_Estado = "Entrou"
    Else
        Coloca_Estado = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Coloca_Estado: " & Err.Number & " - " & Err.Description, Me.Name, "Coloca_Estado"
    Coloca_Estado = ""
    Exit Function
    Resume Next

End Function

Sub Preenche_LaProdutos(indice As Integer)
    On Error GoTo TrataErro

    Dim TamDescrProd As Integer
    Dim TamDescrEspecif As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String

    LaProdutos.caption = ""
    If RegistosP(indice).CodProd <> "" Then
        Str1 = "Produto vai chegar a: "
        Str2 = "Produto devia ter chegado a: "
        Str3 = "Produto chegou a: "
        If RegistosP(indice).EstadoProd = "Entrou" Then
            LaProdutos.caption = LaProdutos.caption & Str3 & RegistosP(indice).DtChega
        ElseIf RegistosP(indice).EstadoProd = "Falta" Then
            LaProdutos.caption = LaProdutos.caption & Str2 & RegistosP(indice).DtPrev
        ElseIf EcDtColheita.text <> "" Then
            LaProdutos.caption = LaProdutos.caption & Str1 & RegistosP(indice).DtPrev
        End If
        TamDescrProd = Len(Trim(UCase(RegistosP(indice).DescrProd)))
        TamDescrEspecif = Len(Trim(UCase(RegistosP(indice).DescrEspecif)))
        For i = 1 To 80 - (TamDescrProd + TamDescrEspecif)
            LaProdutos.caption = LaProdutos.caption & Chr(32)
        Next i
        LaProdutos.caption = LaProdutos.caption & Trim(UCase(RegistosP(indice).DescrProd)) & " " & Trim(UCase(RegistosP(indice).DescrEspecif))
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_LaProdutos: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_LaProdutos"
    Exit Sub
    Resume Next

End Sub

Sub Preenche_LaTubos(indice As Integer)
    On Error GoTo TrataErro

    Dim TamDescrTubo As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String

    LaTubos.caption = ""
    If gEstruturaTubos(indice).CodTubo <> "" Then
        Str1 = "Tubo vai chegar a: "
        Str2 = "Tubo devia ter chegado a: "
        Str3 = "Tubo chegou a: "
        If gEstruturaTubos(indice).estado_tubo = "Entrou" Then
            LaTubos.caption = LaTubos.caption & Str3 & gEstruturaTubos(indice).dt_chega
        ElseIf gEstruturaTubos(indice).estado_tubo = "Falta" Then
            LaTubos.caption = LaTubos.caption & Str2 & gEstruturaTubos(indice).dt_previ
        ElseIf EcDtColheita.text <> "" Then
            LaTubos.caption = LaProdutos.caption & Str1 & gEstruturaTubos(indice).dt_previ
        End If
        TamDescrTubo = Len(Trim(UCase(gEstruturaTubos(indice).Designacao_tubo)))

        For i = 1 To 80 - (TamDescrTubo)
            LaTubos.caption = LaTubos.caption & Chr(32)
        Next i
        LaTubos.caption = LaTubos.caption & Trim(UCase(gEstruturaTubos(indice).Designacao_tubo))
        LaTubos.caption = LaTubos.caption & vbCrLf & "Norma:  " & gEstruturaTubos(indice).normaTubo
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_LaTubos: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_LaTubos"
    Exit Sub
    Resume Next

End Sub

Sub Inicializa_Estrutura_Analises()
    On Error GoTo TrataErro

    'inicializa��es da lista de marca��es de an�lises
    ReDim MaReq(0 To 0)
    MaReq(0).Cod_Perfil = ""
    MaReq(0).Descr_Perfil = ""
    MaReq(0).cod_ana_c = ""
    MaReq(0).Descr_Ana_C = ""
    MaReq(0).cod_ana_s = ""
    MaReq(0).descr_ana_s = ""
    MaReq(0).Ord_Ana = 0
    MaReq(0).Ord_Ana_Compl = 0
    MaReq(0).Ord_Ana_Perf = 0
    MaReq(0).cod_agrup = ""
    MaReq(0).N_Folha_Trab = 0
    MaReq(0).dt_chega = ""
    MaReq(0).Flg_Apar_Transf = ""
    MaReq(0).flg_estado = ""
    MaReq(0).Flg_Facturado = 0
    LbTotalAna = "0"
Exit Sub
TrataErro:
    BG_LogFile_Erros "Inicializa_Estrutura_Analises: " & Err.Number & " - " & Err.Description, Me.Name, "Inicializa_Estrutura_Analises"
    Exit Sub
    Resume Next

End Sub

Sub ImprimeResumo()
    On Error GoTo TrataErro
    If gImprFolhaResumoParaRecibos <> mediSim Then
        ImprimeResumoCrystal
    Else
        FR_ImprimeResumoParaImpressoraRecibos EcNumReq
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumo"
    Exit Sub
    Resume Next
End Sub

Private Sub CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)
    On Error GoTo TrataErro

    ' Tipo = ANA ou PROD
    ' EmCascata = True : Cria todos elementos at� o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRegEnt As New ClRegRecibos
    Dim AuxRegProd As New ClRegProdutos
    Dim AuxRegAna As New ClRegAnalises
    Dim AuxRegTubo As New ClRegTubos
    Dim AuxRegRecManuais As New ClRegRecibosAna
    Dim AuxAdiantamento As New ClAdiantamentos

    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If

    If tipo = "PROD" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegProd
            Next i
        Else
            Coleccao.Add AuxRegProd
        End If
    ElseIf tipo = "TUBO" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegTubo
            Next i
        Else
            Coleccao.Add AuxRegTubo
        End If
    ElseIf tipo = "REC" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegEnt
            Next i
        Else
            Coleccao.Add AuxRegEnt
        End If
    ElseIf tipo = "ADI" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxAdiantamento
            Next i
        Else
            Coleccao.Add AuxAdiantamento
        End If
    ElseIf tipo = "REC_MAN" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegRecManuais
            Next i
        Else
            Coleccao.Add AuxRegRecManuais
        End If
    Else
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegAna
            Next i
        Else
            Coleccao.Add AuxRegAna
        End If
    End If

    Set AuxRegAna = Nothing
    Set AuxAdiantamento = Nothing
    Set AuxRegProd = Nothing
    Set AuxRegTubo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "CriaNovaClasse: " & Err.Number & " - " & Err.Description, Me.Name, "CriaNovaClasse"
    Exit Sub
    Resume Next

End Sub

Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)
    On Error GoTo TrataErro

    ' Index = -1 : Apaga todos elementos

    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        If Index <= Coleccao.Count Then
            Coleccao.Remove Index
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaColeccao: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaColeccao"
    Exit Sub
    Resume Next

End Sub

Function Verifica_Prod_ja_Existe(indice As Integer, Produto As String) As Boolean
    On Error GoTo TrataErro

    Dim i As Integer
    Dim k As Integer
    Dim TotalRegistos As Integer

    Verifica_Prod_ja_Existe = False
    TotalRegistos = RegistosP.Count - 1
    If indice >= 0 Then
        i = 1
        While i <= TotalRegistos And Verifica_Prod_ja_Existe = False
            If UCase(Trim(Produto)) = UCase(Trim(RegistosP(i).CodProd)) And indice <> i Then
                Verifica_Prod_ja_Existe = True
            End If
            i = i + 1
        Wend
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Prod_ja_Existe: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Prod_ja_Existe"
    Verifica_Prod_ja_Existe = True
    Exit Function
    Resume Next

End Function

Function Verifica_Ana_ja_Existe(indice As Long, codAgrup As String) As Boolean

    Dim i As Integer
    Dim TotalRegistos As Integer
    Dim CodAgrupAux As String
    Dim codAna As String

    codAna = codAgrup
    SELECT_Descr_Ana codAna



    Verifica_Ana_ja_Existe = False
    TotalRegistos = RegistosA.Count - 1

   If InStr(1, "SCP", UCase(Mid(codAgrup, 1, 1))) > 0 Then
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    Else
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    End If

    codAgrup = CodAgrupAux & codAgrup

Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_ja_Existe: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_ja_Existe"
    Verifica_Ana_ja_Existe = True
    Exit Function
    Resume Next

End Function

Sub LimpaFgProd()
     On Error GoTo TrataErro

    Dim j As Long

    ExecutaCodigoP = False

    j = FgProd.rows - 1
    While j > 0
        If j > 1 Then
            FgProd.RemoveItem j
        Else
            FgProd.TextMatrix(j, 0) = ""
            FgProd.TextMatrix(j, 1) = ""
            FgProd.TextMatrix(j, 2) = ""
            FgProd.TextMatrix(j, 3) = ""
            FgProd.TextMatrix(j, 4) = ""
            FgProd.TextMatrix(j, 5) = ""
            FgProd.TextMatrix(j, 6) = ""
            FgProd.TextMatrix(j, 7) = ""
        End If

        j = j - 1
    Wend

    ExecutaCodigoP = True
    LastColP = 0
    LastRowP = 1

    CriaNovaClasse RegistosP, 1, "PROD", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgProd: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgProd"
    Exit Sub
    Resume Next

End Sub

Sub LimpaFgTubos()
     On Error GoTo TrataErro

    Dim j As Long

    ExecutaCodigoT = False

    j = FGTubos.rows - 1
    While j > 0
        If j > 1 Then
            FGTubos.RemoveItem j
        Else
            FGTubos.TextMatrix(j, 0) = ""
            FGTubos.TextMatrix(j, 1) = ""
            FGTubos.TextMatrix(j, 2) = ""
            FGTubos.TextMatrix(j, 3) = ""
            FGTubos.TextMatrix(j, 4) = ""
            FGTubos.TextMatrix(j, 5) = ""
            FGTubos.TextMatrix(j, 6) = ""
            FGTubos.TextMatrix(j, 7) = ""
        End If

        j = j - 1
    Wend

    ExecutaCodigoT = True
    LastColT = 0
    LastRowT = 1

Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgTubos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgTubos"
    Exit Sub
    Resume Next

End Sub

Sub LimpaFGAna()
     On Error GoTo TrataErro
   Dim i As Integer
    Dim j As Long

    ExecutaCodigoA = False

    j = FGAna.rows - 1
    While j > 0
        If j > 1 Then
            FGAna.RemoveItem j
        Else
            For i = 0 To FGAna.Cols - 1
                FGAna.TextMatrix(j, i) = ""
            Next
            BL_MudaCorFg FGAna, j, vbWhite
        End If

        j = j - 1
    Wend

    ExecutaCodigoA = True
    LastColA = 0
    LastRowA = 1

    CriaNovaClasse RegistosA, 1, "ANA", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFGAna: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFGAna"
    Exit Sub
    Resume Next

End Sub
Sub LimpaFgRecAna()
    On Error GoTo TrataErro

    Dim j As Long


    j = FgAnaRec.rows - 1
    While j > 0
        If j > 1 Then
            FgAnaRec.RemoveItem j
        Else
            FgAnaRec.TextMatrix(j, 0) = ""
            FgAnaRec.TextMatrix(j, 1) = ""
            FgAnaRec.TextMatrix(j, 2) = ""
        End If

        j = j - 1
    Wend
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgRecAna: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgRecAna"
    Exit Sub
    Resume Next
End Sub
Function Procura_Prod_ana() As Boolean

    On Error GoTo TrataErro
    'Verifica se existem an�lises com o produto que se pretende eliminar
    'No caso de existirem n�o deixa apagar o produto sem que se tenha apagado a an�lise

    Dim RsProd As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim ListaP As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset

    Procura_Prod_ana = False

    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)

    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            ListaP = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    CmdPesqGhost.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsPesqGhost = CmdPesqGhost.Execute
                    While Not RsPesqGhost.EOF
                        lista = lista & BL_TrataStringParaBD(BL_HandleNull(RsPesqGhost!cod_membro, "-1")) & ","
                        RsPesqGhost.MoveNext
                    Wend
                    RsPesqGhost.Close
                    Set RsPesqGhost = Nothing
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i

            lista = Mid(lista, 1, Len(lista) - 1)

            sql = "SELECT descr_ana_s from sl_ana_s WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_ana_s IN ( " & lista & ")"

            Set RsProd = New ADODB.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly

            lista = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_ana_s, "") & ","
                RsProd.MoveNext
            Wend

            For i = 1 To UBound(MaReq)
                If MaReq(i).Cod_Perfil <> "" Then
                    If InStr(1, ListaP, MaReq(i).Cod_Perfil) = 0 Then
                        ListaP = ListaP & BL_TrataStringParaBD(MaReq(i).Cod_Perfil)
                    End If
                End If
            Next i

            sql = "SELECT descr_perfis from sl_perfis WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_perfis IN ( " & ListaP & ")"
            Set RsProd = New ADODB.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly

            ListaP = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_perfis, "") & ","
                RsProd.MoveNext
            Wend

            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Prod_ana = True

                If RsProd.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                End If
            End If

            RsProd.Close
            Set RsProd = Nothing
        End If
    End If

    Set CmdPesqGhost = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Procura_Prod_ana: " & Err.Number & " - " & Err.Description, Me.Name, "Procura_Prod_ana"
    Procura_Prod_ana = False
    Exit Function
    Resume Next

End Function

Function Procura_Tubo_ana() As Boolean

    'Verifica se existem an�lises com o tubo que se pretende eliminar
    'No caso de existirem n�o deixa apagar o tubo sem que se tenha apagado a an�lise

    Dim rsTubo As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset

    Procura_Tubo_ana = False

    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)

    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_c = "C99999" Then
                    lista = lista & BL_TrataStringParaBD(MaReq(i).Cod_Perfil) & ","
                ElseIf MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_c) & ","
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            lista = Mid(lista, 1, Len(lista) - 1)

            sql = "SELECT descr_ana descr_ana_s from slv_analises WHERE cod_tubo = " & BL_TrataStringParaBD(FGTubos.TextMatrix(FGTubos.row, 0)) & _
                    " AND cod_ana IN ( " & lista & ")"

            Set rsTubo = New ADODB.recordset
            rsTubo.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            rsTubo.Open sql, gConexao, adOpenStatic, adLockReadOnly

            lista = ""
            While Not rsTubo.EOF
                lista = lista & BL_HandleNull(rsTubo!descr_ana_s, "") & ","
                rsTubo.MoveNext
            Wend

            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Tubo_ana = True

                If rsTubo.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                End If
            End If

            rsTubo.Close
            Set rsTubo = Nothing
        End If
    End If

    Set CmdPesqGhost = Nothing

End Function

Function ValidaEspecifObrig() As Boolean
    On Error GoTo TrataErro

    Dim i As Integer
    Dim n As Integer

    n = RegistosP.Count
    If n > 0 Then
        n = n - 1
        For i = 1 To n
            If RegistosP(i).CodProd <> "" Then
                If RegistosP(i).EspecifObrig = "S" Then
                    If RegistosP(i).CodEspecif = "" Then
                        BG_Mensagem mediMsgBox, "O produto " & RegistosP(i).DescrProd & " exige especifica��o !", vbInformation, ""
                        SSTGestReq.Tab = 1
                        Exit Function
                    End If
                End If
            End If
        Next
    End If
    ValidaEspecifObrig = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaEspecifObrig: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaEspecifObrig"
    ValidaEspecifObrig = False
    Exit Function
    Resume Next

End Function

Function ValidaProdutos() As Boolean


    On Error GoTo ErrorHandler

    Dim ret As Boolean
    ret = True

    If (RegistosP.Count > 1) Then
        ret = True
    Else
        If (RegistosA.Count > 1) Then
            ret = False
        Else
            ret = True
        End If
    End If

    ValidaProdutos = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Descricao (ValidaProdutos) -> " & Err.Description)
            ValidaProdutos = True
            Exit Function
    End Select
End Function


Sub BD_Insert_Utente()


    Dim SQLQuery As String
    Dim RsNUtente As ADODB.recordset
    Dim Query As String
    Dim i As Integer
    Dim seq As Long

    'se a data de inscri��o estiver vazia
    If gDUtente.dt_inscr = "" Then
        gDUtente.dt_inscr = Format(dataAct, gFormatoData)
    End If

    If gDUtente.dt_nasc_ute = "" Then
        gDUtente.dt_nasc_ute = "01-01-1900"
    End If

   ' caso o tipo de utente nao esteja preenchido
    If gDUtente.t_utente = "" Then
        gDUtente.t_utente = "LAB"           'obrigar a que seja por defeito criado um utente LAB
    End If


    On Error GoTo Trata_Erro

    If Trim(gDUtente.Utente) = "" Then
        If BG_Mensagem(mediMsgBox, "Quer gerar um n�mero sequencial para o utente do tipo " & gDUtente.t_utente & "?", vbYesNo + vbDefaultButton2 + vbQuestion, "Gerar Sequencial") = vbNo Then
            Exit Sub
        End If

        'Determinar o novo numero para o Tipo Utente (TENTA 10 VEZES - LOCK!)
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraUtente(gDUtente.t_utente)
            i = i + 1
        Wend

        If seq = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero do utente!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If

        EcUtente = seq
    Else
        Set RsNUtente = New ADODB.recordset
        RsNUtente.CursorLocation = adUseServer
        RsNUtente.Open "SELECT nome_ute, dt_nasc_ute FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & " AND utente = " & BL_TrataStringParaBD(gDUtente.Utente), gConexao, adOpenStatic, adLockReadOnly

        If RsNUtente.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe um utente do tipo " & gDUtente.t_utente & " com o n�mero " & gDUtente.Utente & "!" & Chr(13) & "Nome : " & BL_HandleNull(RsNUtente!nome_ute) & Chr(13) & "Dt. Nasc : " & BL_HandleNull(RsNUtente!dt_nasc_ute), vbOKOnly + vbExclamation, "Inserir Utente"
            RsNUtente.Close
            Set RsNUtente = Nothing
            Exit Sub
        Else
            RsNUtente.Close
            Set RsNUtente = Nothing
        End If
    End If

    'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
    i = 0
    seq = -1
    While seq = -1 And i <= 10
        seq = BL_GeraNumero("SEQ_UTENTE")
        i = i + 1
    Wend

    If seq = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If

    EcSeqUtente = seq

    gSQLError = 0
    gSQLISAM = 0


    SQLQuery = "INSERT INTO sl_identif (seq_utente,t_utente,utente,n_proc_1,n_proc_2," & _
            " dt_inscr,n_cartao_ute,nome_ute,dt_nasc_ute,sexo_ute,est_civ_ute, " & _
            " descr_mor_ute,cod_postal_ute,telef_ute, cod_efr_ute,user_cri,dt_cri )" & _
            " values (" & EcSeqUtente & "," & BL_TrataStringParaBD(gDUtente.t_utente) & "," & BL_TrataStringParaBD(gDUtente.Utente) & "," & BL_TrataStringParaBD(gDUtente.n_proc_1) & "," & BL_TrataStringParaBD(gDUtente.n_proc_2) & _
            "," & BL_TrataDataParaBD(gDUtente.dt_inscr) & "," & BL_TrataStringParaBD(gDUtente.n_cartao_ute) & "," & BL_TrataStringParaBD(gDUtente.nome_ute) & "," & BL_TrataStringParaBD(gDUtente.dt_nasc_ute) & "," & BL_TrataStringParaBD(gDUtente.sexo_ute) & "," & BL_TrataStringParaBD(gDUtente.est_civil) & _
            "," & BL_TrataStringParaBD(gDUtente.morada) & "," & BL_TrataStringParaBD(gDUtente.cod_postal) & "," & BL_TrataStringParaBD(gDUtente.telefone) & "," & BL_TrataStringParaBD(gDUtente.cod_efr) & ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(dataAct) & ")"

    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro

    If gSQLError <> 0 Then
        'Erro a inserir utente

        GoTo Trata_Erro
    End If


    gDUtente.seq_utente = BL_String2Double(EcSeqUtente)
    PreencheDadosUtente

    BG_CommitTransaction

    Exit Sub

Trata_Erro:
    BG_LogFile_Erros "FormIdentificaUtente: BD_Insert_Utente -> " & Err.Description
    BG_RollbackTransaction

    LimpaCampos

End Sub



Function Verifica_Ana_Pertence_Local(codAgrup As String) As Boolean
    On Error GoTo TrataErro
    Dim RsLocal As ADODB.recordset

    Set RsLocal = New ADODB.recordset
    RsLocal.CursorLocation = adUseServer
    RsLocal.CursorType = adOpenStatic
    RsLocal.Source = "select * from slv_analises where cod_ana = " & BL_TrataStringParaBD(codAgrup) & " and cod_local = " & gCodLocal
    RsLocal.ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros RsLocal.Source
    RsLocal.Open
    If RsLocal.RecordCount <= 0 Then
        Verifica_Ana_Pertence_Local = False
    Else
        Verifica_Ana_Pertence_Local = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_Pertence_Local: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_Pertence_Local"
    Verifica_Ana_Pertence_Local = False
    Exit Function
    Resume Next

End Function


Public Sub EliminaTuboEstrut()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim flg_temAna As Boolean
    On Error GoTo TrataErro

    For i = 1 To gTotalTubos
        If i <= gTotalTubos Then
            flg_temAna = False
            For j = 1 To UBound(MaReq)
                If MaReq(j).indice_tubo = i Then
                    flg_temAna = True
                    Exit For
                End If
            Next j
            'BEDSIDE -> gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado
            If flg_temAna = False And gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado Then
                AcrescentaTubosEliminados EcNumReq.text, gEstruturaTubos(i).seq_req_tubo
                For k = i To gTotalTubos - 1
                    TB_MoveEstrutTubos k + 1, k
                Next
                gTotalTubos = gTotalTubos - 1
                ReDim Preserve gEstruturaTubos(gTotalTubos)
                FGTubos.RemoveItem i
                i = i - 1
            End If
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicaoAguas -> Elimina��o de Tubos: " & Err.Description
    Exit Sub
    Resume Next
End Sub


Private Sub EliminaProduto(cod_ana As String)
    Dim i  As Integer
    Dim sSql As String
    Dim sSqlP As String
    Dim sSqlC As String
    Dim rsAna As New ADODB.recordset
    Dim RsProd As New ADODB.recordset
    Dim CodProduto As String
    Dim flg_PodeEliminar As Boolean
    Dim flg_existeProd As Boolean

    On Error GoTo TrataErro


    ' SE FOR COMPLEXA CHAMA A MESMA FUNCAO PARA CADA SIMPLES
    If Mid(cod_ana, 1, 1) = "C" Then
        sSqlC = "SELECT cod_produto FROM sl_ana_C WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlC
        rsAna.Open sSqlC, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_membro
                    rsAna.MoveNext
                Wend
            End If
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If


    ' SE FOR PERFIL CHAMA A MESMA FUNCAO PARA CADA MEMBRO
    If Mid(cod_ana, 1, 1) = "P" Then
        sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        sSqlP = "SELECT cod_produto FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlP
        rsAna.Open sSqlP, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_analise
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If

    ' ENCONTRA O CODIGO DO PRODUTO DA ANALISE EM CAUSA
    If Mid(cod_ana, 1, 1) = "S" Then
        CodProduto = ""
        sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
continuap:
            ' VERIFICA SE EXISTE PRODUTO NA FLEX GRID
            flg_existeProd = False
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    flg_existeProd = True
                    Exit For
                End If
            Next
            If flg_existeProd = False Then
                rsAna.Close
                Set rsAna = Nothing
                Exit Sub
            End If
            ' ---------------------------------------
        End If
        rsAna.Close
        Set rsAna = Nothing

        ' SE NAO TEM PRODUTO CODIFICADO...SAI
        If CodProduto = "" Then
            Exit Sub
        End If

        flg_PodeEliminar = VerificaPodeEliminarProduto(CodProduto)


        ' NAO ENCONTROU MAIS NENHUMA ANALISE COM ESSE TUBO LOGO PODE ELIMINAR
        If flg_PodeEliminar = True Then

            ' VERIFICA SE JA FOI DADO ENTRADA DO PRODUTO
            If EcNumReq <> "" Then
                sSql = "SELECT * FROM sl_req_prod WHERE n_req =" & EcNumReq
                sSql = sSql & " AND cod_prod = " & BL_TrataStringParaBD(CodProduto)
                RsProd.CursorLocation = adUseServer
                RsProd.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsProd.Open sSql, gConexao
                If RsProd.RecordCount > 0 Then
                    If BL_HandleNull(RsProd!dt_chega, "") <> "" Then
                        ' SE JA DEU ENTRADA NAO ELIMINA
                        RsProd.Close
                        Set RsProd = Nothing
                        Exit Sub
                    End If
                End If
                RsProd.Close
                Set RsProd = Nothing
                ' -----------------------------------------
            End If

            ' ELIMINA DA FLEX GRID E DA ESTRUTURA
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    FgProd.row = 1
                    If i < FgProd.rows - 1 Then
                        RegistosP.Remove i
                        FgProd.RemoveItem i
                    Else
                        FgProd.TextMatrix(i, 0) = ""
                        FgProd.TextMatrix(i, 1) = ""
                        FgProd.TextMatrix(i, 2) = ""
                        FgProd.TextMatrix(i, 3) = ""
                        FgProd.TextMatrix(i, 4) = ""
                        FgProd.TextMatrix(i, 5) = ""
                        FgProd.TextMatrix(i, 6) = ""
                        FgProd.TextMatrix(i, 7) = ""
                        RegistosP(i).CodProd = ""
                        RegistosP(i).DescrProd = ""
                        RegistosP(i).EspecifObrig = ""
                        RegistosP(i).CodEspecif = ""
                        RegistosP(i).DescrEspecif = ""
                        RegistosP(i).DtPrev = ""
                        RegistosP(i).DtChega = ""
                        RegistosP(i).EstadoProd = ""
                        RegistosP(i).Volume = ""
                    End If
                    Exit For
                End If
            Next
        End If
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicaoAguas -> Elimina��o de Produtos: " & Err.Description
    Exit Sub
    Resume Next
End Sub


Private Function VerificaPodeEliminarProduto(CodProduto As String) As Boolean
    Dim i As Integer
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro

    VerificaPodeEliminarProduto = True

    For i = 1 To RegistosA.Count - 1

        If Mid(RegistosA(i).codAna, 1, 1) = "C" Then
            flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, RegistosA(i).codAna)
        ElseIf Mid(RegistosA(i).codAna, 1, 1) = "P" Then
            flg_Elimina = VerificaPodeEliminarProduto_P(CodProduto, RegistosA(i).codAna)
        Else
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, RegistosA(i).codAna)
        End If

        If flg_Elimina = False Then
            VerificaPodeEliminarProduto = flg_Elimina
            Exit Function
        End If
    Next
    VerificaPodeEliminarProduto = True
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto"
    VerificaPodeEliminarProduto = False
    Exit Function
    Resume Next
End Function

Private Function VerificaPodeEliminarProduto_C(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
     On Error GoTo TrataErro

    flg_Elimina = False

    For i = 1 To UBound(MaReq)
        If MaReq(i).cod_ana_c = analise And MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, MaReq(i).cod_ana_s)
            If flg_Elimina = False Then
                VerificaPodeEliminarProduto_C = flg_Elimina
                Exit Function
            End If
        End If
    Next
    VerificaPodeEliminarProduto_C = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_C: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_C"
    VerificaPodeEliminarProduto_C = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_P(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro

    flg_Elimina = False

    sSql = "SELECT cod_produto from sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If Not rsAna.EOF Then
        If BL_HandleNull(rsAna!cod_produto, "") <> "" Then
            If rsAna!cod_produto <> CodProduto Then
                flg_Elimina = True
            End If
        Else
            For i = 1 To UBound(MaReq)
                If MaReq(i).Cod_Perfil = analise Then
                    If MaReq(i).cod_ana_c <> "0" And MaReq(i).cod_ana_c <> gGHOSTMEMBER_C And MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                        flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, MaReq(i).cod_ana_c)
                    ElseIf MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
                        flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, MaReq(i).cod_ana_s)
                    End If

                    If flg_Elimina = False Then
                        VerificaPodeEliminarProduto_P = flg_Elimina
                    End If
                End If
            Next
        End If

    End If


    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarProduto_P = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_P: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_P"
    VerificaPodeEliminarProduto_P = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_S(CodProduto As String, analise As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro


    sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(analise)
    sSql = sSql & " AND cod_produto = " & BL_TrataStringParaBD(CodProduto)

    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaPodeEliminarProduto_S = False
    Else
        VerificaPodeEliminarProduto_S = True
    End If

    rsAna.Close
    Set rsAna = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_S: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_S"
    VerificaPodeEliminarProduto_S = False
    Exit Function
    Resume Next
End Function


Sub Lista_GhostMembers(CodComplexa As String, complexa As String)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim iRec As Long
    On Error GoTo TrataErro

    Dim i As Long
    Dim TotalElementosSel As Integer
    Dim indice As Long

    PesqRapida = False
    iRec = DevIndice(RegistosA(FGAna.row).codAna)
    ChavesPesq(1) = "cod_membro"
    CamposEcran(1) = "cod_membro"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"

    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"


    CamposRetorno.InicializaResultados 1

    CFrom = "sl_membro, sl_ana_s "
    CampoPesquisa = "descr_ana_s"
    CWhere = " sl_membro.cod_membro = sl_ana_s.cod_ana_s and cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)

    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                        ChavesPesq, _
                                                                        CamposEcran, _
                                                                        CamposRetorno, _
                                                                        Tamanhos, _
                                                                        Headers, _
                                                                        CWhere, _
                                                                        CFrom, _
                                                                        "", _
                                                                        CampoPesquisa, _
                                                                        " ORDER BY sl_membro.ordem ", _
                                                                        " Membros da an�lise " & complexa)

    If PesqRapida = True Then

        FormPesqRapidaAvancadaMultiSel.Show vbModal

        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            For i = 1 To TotalElementosSel
                InsereGhostMember CStr(Resultados(i))
'                FormGestaoRequisicao.EcAuxAna.Enabled = True
'                FormGestaoRequisicao.EcAuxAna = Resultados(i)
'                FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
            Next i
            'SE COMPLEXA MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
            If IF_ContaMembros(CodComplexa, RegistosRM(iRec).ReciboEntidade) > 0 Then
                i = FGAna.row
                EliminaReciboManual iRec, RegistosA(i).codAna, False
                RegistosRM(iRec).ReciboQuantidade = TotalElementosSel
                RegistosRM(iRec).ReciboQuantidadeEFR = RegistosRM(iRec).ReciboQuantidade
                RegistosRM(iRec).ReciboTaxa = Replace(RegistosRM(iRec).ReciboTaxaUnitario, ".", ",") * TotalElementosSel
                FGAna.TextMatrix(i, lColFgAnaPreco) = RegistosRM(iRec).ReciboTaxa
                AdicionaAnaliseAoReciboManual iRec, False
            End If
            FormGestaoRequisicaoAguas.FGAna.SetFocus
        Else
        i = FGAna.row
        Dim ana As String
        ana = RegistosA(i).codAna
        If EliminaReciboManual(iRec, ana, True) = True Then

            LimpaColeccao RegistosRM, iRec
            FGAna.row = i
            Elimina_Mareq RegistosA(i).codAna
            ana = RegistosA(i).codAna
            RegistosA.Remove i
            If RegistosA.Count = 0 Then
                CriaNovaClasse RegistosA, 1, "ANA"
            End If
            If FGAna.rows = 2 Then
                FGAna.AddItem ""
            End If
            FGAna.RemoveItem i
            EliminaTuboEstrut
            EliminaProduto ana
        End If
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem membros", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicaoAguas" Then
            FormGestaoRequisicaoAguas.FGAna.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Lista_GhostMembers: " & Err.Number & " - " & Err.Description, Me.Name, "Lista_GhostMembers"
    Exit Sub
    Resume Next
End Sub


Private Function DevolveDataValidade(n_req As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro


    '----------------------------------
    ' DATA VALIDADE ACTUAL
    sql = "SELECT dt_conclusao FROM sl_requis WHERE n_req = " & n_req
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        DevolveDataValidade = BL_HandleNull(rs!dt_conclusao, CDate(1))
    End If
    rs.Close
    Set rs = Nothing
    '----------------------------------
Exit Function
TrataErro:
    BG_LogFile_Erros "DevolveDataValidade: " & Err.Number & " - " & Err.Description, Me.Name, "DevolveDataValidade"
    DevolveDataValidade = ""
    Exit Function
    Resume Next
End Function


' pferreira 2010.03.08
' TICKET GLINTTHS-263.
Public Sub RegistaEliminadas(Optional elimina_todas As Boolean)

    Dim i As Integer
    Dim total As Integer

    Dim sSql As String
    If (elimina_todas) Then
        total = FGAna.rows - 2
    Else
        total = TotalEliminadas
    End If
    If EcNumReq <> "" Then
        For i = 1 To total
            If (Not elimina_todas) Then
                BL_RegistaAnaEliminadas EcNumReq, Eliminadas(i).codAna, "", "", ""
            Else
                BL_RegistaAnaEliminadas EcNumReq, FGAna.TextMatrix(i, 0), "", "", ""

            End If
        Next i
    End If
    TotalEliminadas = 0
    ReDim Eliminadas(0)

End Sub

Sub RegistaAcrescentadas()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim t_urg As Integer
    Dim t_sit As Integer
    If CbUrgencia.ListIndex = mediComboValorNull Then
        t_urg = mediComboValorNull
    Else
        t_urg = CbUrgencia.ItemData(CbUrgencia.ListIndex)
    End If
    t_sit = mediComboValorNull
    If EcNumReq <> "" Then
        For i = 1 To TotalAcrescentadas
            BL_RegistaAnaAcrescentadas EcSeqUtente.text, EcNumReq.text, "", "", "", Acrescentadas(i).cod_agrup, dataAct, Bg_DaHora_ADO, t_urg, t_sit
        Next i
    End If
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)

Exit Sub
TrataErro:
    BG_LogFile_Erros "RegistaAcrescentadas: " & Err.Number & " - " & Err.Description, Me.Name, "RegistaAcrescentadas"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' IMPRIME FOLHA DE RESUMO ATRAV�S DO CRYSTAL REPORTS

' -----------------------------------------------------------------
Private Sub ImprimeResumoCrystal()
    Dim continua As Boolean
    Dim ObsUtente As String
    Dim codProfis As String
    Dim descrProfis As String
    Dim sSql As String
    On Error GoTo TrataErro

    continua = BL_IniciaReport("FolhaResumo", "Folha de Resumo", crptToPrinter)

    If continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me

    sSql = "DELETE FROM sl_cr_folha_Resumo WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql

    ImprimeResumoProdutos
    ImprimeResumoAnalises
    ImprimeResumoTubos

    'soliveira terrugem
    Dim rsOBS As ADODB.recordset
    Set rsOBS = New ADODB.recordset
    rsOBS.CursorLocation = adUseServer
    rsOBS.CursorType = adOpenStatic
    rsOBS.Source = "select obs,cod_profis_ute from sl_identif where seq_utente = " & EcSeqUtente
    If gModoDebug = mediSim Then BG_LogFile_Erros rsOBS.Source
    rsOBS.Open , gConexao
    If rsOBS.RecordCount > 0 Then
        ObsUtente = BL_HandleNull(rsOBS!obs, "")
        codProfis = BL_HandleNull(rsOBS!cod_profis_ute, "")
        If codProfis <> "" Then
            descrProfis = BL_SelCodigo("sl_profis", "DESCR_PROFIS", "COD_PROFIS", codProfis, "V")
        End If
    End If

    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")

    Report.SQLQuery = ""

    'F�rmulas do Report
    Report.formulas(0) = "NumReq=" & BL_TrataStringParaBD("" & EcNumReq)
    Report.formulas(1) = "NomeUtente=" & BL_TrataStringParaBD("" & EcNome)
    Report.formulas(2) = "DtNascUtente=" & BL_TrataStringParaBD("" & EcDataNasc)
    Report.formulas(3) = "DtChegada=" & BL_TrataStringParaBD("" & EcDataChegada)
    Report.formulas(4) = "Processo=" & BL_TrataStringParaBD("" & EcUtente)
    Report.formulas(5) = "DtValidade=" & BL_TrataStringParaBD("" & DevolveDataValidade(CStr(gRequisicaoActiva)))
    Report.formulas(6) = "NSC=" & BL_TrataStringParaBD("" & EcUtente)
    Report.formulas(7) = "Servico=''"
    Report.formulas(8) = "SeqUtente=" & BL_TrataStringParaBD("" & EcSeqUtente)
    Report.formulas(9) = "Medico=" & BL_TrataStringParaBD("")
    Report.formulas(10) = "Idade=" & BL_TrataStringParaBD("" & EcIdade)
    Report.formulas(11) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEFR)
    Report.formulas(12) = "Sala=" & BL_TrataStringParaBD("" & EcDescrSala)
    Report.formulas(13) = "Prioridade=" & BL_TrataStringParaBD("" & CbUrgencia)
    'soliveira terrugem
    Report.formulas(14) = "ObsUtente=" & BL_TrataStringParaBD("" & ObsUtente)
    Report.formulas(16) = "InfComplementar=" & BL_TrataStringParaBD("" & Replace(EcinfComplementar, vbCrLf, ""))
    Report.formulas(17) = "descrProfis=" & BL_TrataStringParaBD("" & descrProfis)
    Report.SubreportToChange = "analises"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & BG_SYS_GetComputerName & "' AND trim({sl_cr_folha_resumo.cod_ana})<>'' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = "SubRepTubos"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & BG_SYS_GetComputerName & "' AND trim({sl_cr_folha_resumo.tubo}) <> '' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = ""



    Call BL_ExecutaReport
    BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoCrystal: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoCrystal"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARIA AS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoAnalises()
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
    If FGAna.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FGAna.rows - 2
            sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,cod_ana,descr_ana, ordem) VALUES("
            sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            sSql = sSql & gRequisicaoActiva & ", "
            sSql = sSql & BL_TrataStringParaBD(FGAna.TextMatrix(i, lColFgAnaCodigo)) & ", "
            sSql = sSql & BL_TrataStringParaBD(FGAna.TextMatrix(i, lColFgAnaDescricao)) & "," & i & ")"

            BG_ExecutaQuery_ADO sSql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoAnalises: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoAnalises"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS PRODUTOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoProdutos()
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro

    If FgProd.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FgProd.rows - 2
            sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,produto) VALUES("
            sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            sSql = sSql & gRequisicaoActiva & ", "
            sSql = sSql & BL_TrataStringParaBD(FgProd.TextMatrix(i, 1)) & ")"

            BG_ExecutaQuery_ADO sSql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoProdutos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoProdutos"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS TUBOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoTubos()
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro

    If FGTubos.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FGTubos.rows - 2
            sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,tubo, norma_colheita) VALUES("
            sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            sSql = sSql & gRequisicaoActiva & ", "
            sSql = sSql & BL_TrataStringParaBD(gEstruturaTubos(i).Designacao_tubo) & ","
            sSql = sSql & BL_TrataStringParaBD(gEstruturaTubos(i).normaTubo) & ")"

            BG_ExecutaQuery_ADO sSql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "ImprimeResumoTubos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoTubos"
    Exit Sub
    Resume Next
End Sub
Function ProdutoUnico(GrupoAna As String) As Boolean
    Dim RsProdU As ADODB.recordset
    Dim sql As String
    Dim ret As Boolean
    On Error GoTo TrataErro


    ret = True

    sql = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)

    Set RsProdU = New ADODB.recordset
    RsProdU.CursorLocation = adUseServer
    RsProdU.CursorType = adOpenStatic
    RsProdU.Source = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsProdU.Open sql, gConexao
    If Not RsProdU.EOF Then
        If BL_HandleNull(RsProdU!flg_produnico, "") = "1" Then
            ret = True
        Else
            ret = False
        End If
    End If

    ProdutoUnico = ret

    RsProdU.Close
    Set RsProdU = Nothing

Exit Function
TrataErro:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : FormGestaoRequisicaoAguas (ProdutoUnico) -> " & Err.Description)
            ProdutoUnico = False
            Exit Function
    End Select
End Function


''''''''''''''''''''''''''''''''''''''''''''
' Verifica se requisicao vai para a fila '''
''''''''''''''''''''''''''''''''''''''''''''
Private Function IsToInsert() As Boolean
    On Error GoTo TrataErro

    Dim rsSala As New ADODB.recordset
    Dim sSql As String
    If (EcCodSala = "") Then
        IsToInsert = True
    Else
        sSql = "select flg_colheita from sl_cod_salas where cod_sala = " & EcCodSala
        rsSala.CursorLocation = adUseServer
        rsSala.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSala.Open sSql, gConexao
        If BL_HandleNull(rsSala!flg_colheita, 0) = mediSim Then
            IsToInsert = True
        Else
            IsToInsert = False
        End If

        If (rsSala.state = adStateOpen) Then
            rsSala.Close
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "IsToInsert: " & Err.Number & " - " & Err.Description, Me.Name, "IsToInsert"
    IsToInsert = False
    Exit Function
    Resume Next
End Function

' PFerreira 16.03.2007
Private Sub CbSala_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    BG_LimpaOpcao CbSala, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbSala_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbSala_KeyDown"
    Exit Sub
    Resume Next
End Sub

' PFerreira 04.04.2007
Private Sub CbDestino_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro

    BG_LimpaOpcao CbDestino, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbDestino_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbDestino_KeyDown"
    Exit Sub
    Resume Next
End Sub
Private Sub PesquisaEntFin()
    PA_PesquisaEFR EcCodEfr2, EcDescrEfr2, ""
End Sub


Private Sub BtGerarRecibo_click()
    On Error GoTo TrataErro
   Dim i As Integer
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    For i = 1 To RegistosAD.Count
        If RegistosAD.Item(i).Estado = gEstadoReciboPago Then
            MsgBox "Existem adiantamento(s) emitido(s). Primeiro tem de o(s) anular. ", vbExclamation, " Adiantamento(s)"
            Exit Sub
        End If
    Next i
    If EcDataChegada.text = "" Then
        BG_Mensagem mediMsgBox, "Requisi��o sem data de chegada. ", vbInformation, "Aten��o"
        Sendkeys ("{HOME}+{END}")
        EcDataChegada.text = ""
        EcDataChegada.SetFocus
        Exit Sub
    End If
    For i = 1 To RegistosR.Count - 1
        If RegistosR(i).codEntidade = RegistosR(FGRecibos.row).codEntidade And RegistosR(i).SerieDoc = RegistosR(FGRecibos.row).SerieDoc And RegistosR(i).NumDoc = RegistosR(FGRecibos.row).NumDoc Then
            VerificaUtentePagaActoMedico RegistosR(i).codEntidade, RegistosR(i).SerieDoc, RegistosR(i).NumDoc
            If RegistosR(i).Estado = gEstadoReciboCobranca Or RegistosR(i).Estado = gEstadoReciboPago Then
                Exit Sub
            End If
        End If
    Next
    If (EcNumReq <> "" And gNReqPreImpressa <> mediSim) Or (gNReqPreImpressa = mediSim And EcUtilizadorCriacao <> "") Then
        FuncaoModificar True
    ElseIf EcNumReq = "" Or (gNReqPreImpressa = mediSim And EcUtilizadorCriacao = "") Then
        FuncaoInserir True
    End If
    flgEmissaoRecibos = True
    FormEmissaoRecibos.Form = Me.Name
    FormEmissaoRecibos.Show 'vbModal
Exit Sub
TrataErro:
    Call BG_LogFile_Erros("FormGestaoRequisicaoAguas - CriaRecibo: " & Err.Description)
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' LIMPA FGRECIBOS

' -----------------------------------------------------------------------------------------------
Sub LimpaFgRecibos()
     On Error GoTo TrataErro
   Dim j As Long

    ExecutaCodigoR = False
    j = FGRecibos.rows - 1
    While j > 0
        If j > 1 Then
            FGRecibos.RemoveItem j
        Else
            FGRecibos.TextMatrix(j, 0) = ""
            FGRecibos.TextMatrix(j, 1) = ""
            FGRecibos.TextMatrix(j, 2) = ""
            FGRecibos.TextMatrix(j, 3) = ""
            FGRecibos.TextMatrix(j, 4) = ""
            FGRecibos.TextMatrix(j, 5) = ""
            FGRecibos.TextMatrix(j, 6) = ""
            BL_MudaCorFg FGRecibos, j, vbWhite
        End If
        j = j - 1
    Wend

    ExecutaCodigoR = True
    LastColR = 0
    LastRowR = 1
    CriaNovaClasse RegistosR, 1, "REC", True
    CriaNovaClasse RegistosRM, 1, "REC_MAN", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgRecibos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgRecibos"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' LIMPA FGRECIBOS

' -----------------------------------------------------------------------------------------------
Sub LimpaFgAdiantamentos()
   On Error GoTo TrataErro
   Dim j As Long
    Dim i As Integer

    j = FgAdiantamentos.rows - 1
    While j > 0
        If j > 1 Then
            FgAdiantamentos.RemoveItem j
        Else
            For i = 0 To FgAdiantamentos.Cols - 1
                FGRecibos.TextMatrix(j, i) = ""
            Next
        End If
        j = j - 1
    Wend

    CriaNovaClasse RegistosAD, 1, "ADI", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgAdiantamentos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgAdiantamentos"
    Exit Sub
    Resume Next
End Sub



' -----------------------------------------------------------------------------------------------

' QUANDO ADICIONA UMA NOVA ANALISE, ADICIONA TAMBEM RESPECTIVA TAXA NA ESTRUTURA DE RECIBOS

' -----------------------------------------------------------------------------------------------

Private Sub AdicionaAnaliseAoReciboManual(indice As Long, MudaEstadoPerdidas As Boolean)
    On Error GoTo TrataErro
    Dim i As Long
    Dim Flg_inseriuEntidade As Boolean
    Dim Flg_EntidadeJaEmitida As Boolean
    Flg_inseriuEntidade = False

    ' ------------------------------------------------------------------------------------------
    ' SE TAXA FOR 0 NAO E NECESSARIO ACRESCENTAR
    ' ------------------------------------------------------------------------------------------

    If RegistosRM(indice).ReciboCodFacturavel <> RegistosRM(indice).ReciboCodAna Then
        For i = 1 To indice
            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(indice).ReciboCodFacturavel Then
                If RegistosRM(i).ReciboCodAna <> RegistosRM(indice).ReciboCodAna Then
                    Exit Sub
                End If
            End If
        Next
    End If

    If RegistosR.Count = 0 Then
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
    Flg_EntidadeJaEmitida = False
    For i = 1 To RegistosR.Count
        ' ------------------------------------------------------------------------------------------
        ' ENTIDADE JA EXISTE NA ESTRUTURA
        ' ------------------------------------------------------------------------------------------
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboNulo Or (RegistosR(i).Estado = gEstadoReciboPerdido And MudaEstadoPerdidas = True) Then
                RegistosR(i).ValorPagar = RegistosR(i).ValorPagar + Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2)
                RegistosR(i).ValorOriginal = RegistosR(i).ValorOriginal + RegistosRM(indice).ReciboTaxa
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                FGRecibos.TextMatrix(i, lColRecibosValorPagar) = BG_CvDecimalParaCalculo(RegistosR(i).ValorPagar)
                If RegistosR(i).ValorPagar > 0 Then

                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                    FGRecibos.TextMatrix(i, lColRecibosEstado) = "N�o Emitido"
                ElseIf RegistosR(i).ValorPagar = 0 Then
                    RegistosR(i).Estado = gEstadoReciboNulo
                    FGRecibos.TextMatrix(i, lColRecibosEstado) = "Recibo Nulo"
                End If
                Flg_inseriuEntidade = True
                If RegistosR(i).ValorPagar > 0 Then
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(i).Estado = gEstadoReciboNulo
                End If
                If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
                    RegistosR(i).FlgBorla = mediSim
                    FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
                RegistosR(i).ValorPagar = (RegistosR(i).ValorPagar) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).ValorOriginal = (RegistosR(i).ValorOriginal) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
                Flg_inseriuEntidade = True
                Flg_EntidadeJaEmitida = True
                If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
                    RegistosR(i).FlgBorla = mediSim
                    FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca Then
                Flg_EntidadeJaEmitida = True
            End If

        End If
    Next
    i = i - 1
    ' ------------------------------------------------------------------------------------------
    ' ENTIDADE NAO EXISTE NA ESTRUTURA
    ' ------------------------------------------------------------------------------------------
    If Flg_inseriuEntidade = False Then
        RegistosR(i).SeqRecibo = RECIBO_RetornaSeqRecibo
        RegistosR(i).NumDoc = 0
        RegistosR(i).SerieDoc = "0"
        RegistosR(i).codEntidade = RegistosRM(indice).ReciboEntidade
        RegistosR(i).DescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).codEmpresa = BL_SelCodigo("SL_EFR", "cod_empresa", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).Caucao = 0
        RegistosR(i).UserEmi = ""
        RegistosR(i).DtEmi = ""
        RegistosR(i).HrEmi = ""
        RegistosR(i).flg_impressao = False
        RegistosR(i).DtPagamento = ""
        RegistosR(i).HrPagamento = ""
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            RegistosR(i).Estado = gEstadoReciboNaoEmitido
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            RegistosR(i).Estado = gEstadoReciboNulo
        ElseIf Flg_EntidadeJaEmitida = True Then
            RegistosR(i).Estado = gEstadoReciboPerdido
        End If
        RegistosR(i).NumAnalises = 1
        RegistosR(i).flg_DocCaixa = 0
        If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
            RegistosR(i).FlgBorla = mediSim
        Else
            RegistosR(i).FlgBorla = mediNao
        End If
        RegistosR(i).FlgNaoConforme = mediNao

        FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).NumDoc
        FGRecibos.TextMatrix(i, lColRecibosEntidade) = RegistosR(i).DescrEntidade
        FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
        FGRecibos.TextMatrix(i, lColRecibosDtEmissao) = RegistosR(i).DtEmi & " " & RegistosR(i).HrEmi
        FGRecibos.TextMatrix(i, lColRecibosDtPagamento) = RegistosR(i).DtPagamento & " " & RegistosR(i).HrPagamento
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "N�o Emitido"
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "Recibo Nulo"
        ElseIf Flg_EntidadeJaEmitida = True Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "An�lises Perdidas"
        End If
        If RegistosR(i).FlgBorla = mediSim Then
            FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
        Else
            FGRecibos.TextMatrix(i, lColRecibosBorla) = "N�o"
        End If
        If RegistosR(i).FlgNaoConforme = mediSim Then
            FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "Sim"
        Else
            FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "N�o"
        End If

        FGRecibos.AddItem ""
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AdicionaAnaliseAoReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaAnaliseAoReciboManual"
    Exit Sub
    Resume Next
End Sub




' -----------------------------------------------------------------------------------------------

' ELIMINA A TAXA DO RECIBO - APENAS SE O RECIBO AINDA NAO TIVER SIDO EMITIDO

' -----------------------------------------------------------------------------------------------
Private Function EliminaReciboManual(indice As Long, codAna As String, EliminaAnalise As Boolean) As Boolean
    On Error GoTo TrataErro
    Dim i As Long
    Dim sSql As String

    EliminaReciboManual = True
    For i = 1 To RegistosR.Count
        If Trim(RegistosRM(indice).ReciboNumDoc) = Trim(RegistosR(i).NumDoc) And Trim(RegistosRM(indice).ReciboSerieDoc) = Trim(RegistosR(i).SerieDoc) And RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboPerdido Or RegistosR(i).Estado = gEstadoReciboNulo Then
                If RegistosR(i).NumAnalises > 0 Then RegistosR(i).NumAnalises = RegistosR(i).NumAnalises - 1

                RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosR(i).ValorPagar) - BG_CvDecimalParaCalculo(Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2))
                RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosR(i).ValorOriginal) - BG_CvDecimalParaCalculo(BL_HandleNull(RegistosRM(indice).ReciboTaxa, 0))
                If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises = 0 Then
                    FGRecibos.RemoveItem i
                    LimpaColeccao RegistosR, i
                    Exit For
                ElseIf RegistosR(i).NumAnalises = 0 Then
                    FGRecibos.RemoveItem i
                    LimpaColeccao RegistosR, i
                    Exit For
                Else
                    If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises > 0 Then
                        RegistosR(i).Estado = gEstadoReciboNulo
                    End If
                    FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
                End If

                EliminaReciboManual = True
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = True Then
                EliminaReciboManual = False
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = False Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo para esta an�lise.N�o Pode alterar valor deste campo", vbExclamation, "Altera��o Recibo."
                EliminaReciboManual = False
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "EliminaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "EliminaReciboManual"
    EliminaReciboManual = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA A ORDEM DE MARCACAO

' -----------------------------------------------------------------------------------------------
Private Function RetornaOrdemMarcacao(ByVal linha As Long) As Long
    On Error GoTo TrataErro
    Dim codEfr As String
    Dim p1 As String
    Dim i As Integer
    Dim Max As Long
    Max = 0
    For i = 1 To RegistosA.Count - 1
        If RegistosRM(linha).ReciboCodFacturavel = RegistosA(i).codAna Then
            RetornaOrdemMarcacao = i
            Exit Function
        End If
    Next

    codEfr = RegistosRM(linha).ReciboEntidade
    p1 = RegistosRM(linha).ReciboP1
    If codEfr <> "" And p1 <> "" Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboEntidade = codEfr And RegistosRM(i).ReciboP1 = p1 And linha <> i Then
                If RegistosRM(i).ReciboOrdemMarcacao > Max Then
                    Max = RegistosRM(i).ReciboOrdemMarcacao
                End If
            End If
        Next
    End If
    RetornaOrdemMarcacao = Max + 1
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaOrdemMarcacao: " & Err.Number & " - " & Err.Description, Me.Name, "RetornaOrdemMarcacao"
    RetornaOrdemMarcacao = 0
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA A ORDEM DE MARCACAO DENTRO DO P1

' -----------------------------------------------------------------------------------------------
Private Function RetornaOrdemP1(ByVal linha As Long) As Long
    On Error GoTo TrataErro
    Dim codEfr As String
    Dim p1 As String
    Dim i As Integer
    Dim Max As Long
    Max = 0
    codEfr = RegistosRM(linha).ReciboEntidade
    p1 = RegistosRM(linha).ReciboP1
    If codEfr <> "" And p1 <> "" Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboEntidade = codEfr And RegistosRM(i).ReciboP1 = p1 And linha <> i Then
                If RegistosRM(i).ReciboOrdemMarcacao > Max Then
                    Max = RegistosRM(i).ReciboOrdemMarcacao
                End If
            End If
        Next
    End If
    RetornaOrdemP1 = Max + 1
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaOrdemP1: " & Err.Number & " - " & Err.Description, Me.Name, "RetornaOrdemP1"
    RetornaOrdemP1 = 0
    Exit Function
    Resume Next
End Function
' --------------------------------------------------------------------------------------------

' SE DA ENTER NA COLUNA DO P1

' --------------------------------------------------------------------------------------------

Private Sub AlteraCorP1()
    On Error GoTo TrataErro
    Dim ent_temp As String
    Dim p1_temp As String
    Dim cor_temp As Long
    Dim cor2_temp As String
    Dim linha_temp As Integer
    Dim iRec As Long
    Dim i As Integer

    ent_temp = FGAna.TextMatrix(FGAna.row, lColFgAnaCodEFR)
    p1_temp = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
    linha_temp = FGAna.row
    If FGAna.CellBackColor = vbWhite Then
        FGAna.CellBackColor = Verde
        cor_temp = Verde
        cor2_temp = "V"
    Else
        FGAna.CellBackColor = vbWhite
        cor_temp = vbWhite
        cor2_temp = "B"
    End If
    For i = 1 To FGAna.rows - 1
        If FGAna.TextMatrix(i, lColFgAnaP1) = p1_temp And FGAna.TextMatrix(i, lColFgAnaCodEFR) = ent_temp Then
            iRec = DevIndice(RegistosA(i).codAna)
            FGAna.row = i
            FGAna.Col = 2
            FGAna.CellBackColor = cor_temp
            RegistosRM(iRec).ReciboCorP1 = cor2_temp
        End If
    Next
    FGAna.row = linha_temp
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraCorP1: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraCorP1"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' VERIFICA SE ANALISE FOI MARCACDA PELO CODIGO DE BARRAS DO P1

' ------------------------------------------------------------------------------------------------
Private Function MarcacaoCodigoBarrasP1(codAna As String) As String
    On Error GoTo TrataErro
    Dim codRubrEfr As String
    Dim rsAnaFact As New ADODB.recordset
    Dim sSql As String
    Dim temp  As String
    Dim i As Integer
    Dim MultAna As String
    If codAna <> "" Then
        codRubrEfr = IF_ObtemRubrParaEFR(EcCodEFR, codAna)
        If codRubrEfr <> "" Then
            sSql = "SELECT x1.cod_ana FROM sl_ana_facturacao x1, slv_analises_factus x2 WHERE x1.cod_ana_gh IN " & codRubrEfr
            sSql = sSql & " AND x1.cod_ana = x2.cod_ana "
            sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
            rsAnaFact.CursorLocation = adUseServer
            rsAnaFact.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnaFact.Open sSql, gConexao
            If rsAnaFact.RecordCount = 0 Then
                MarcacaoCodigoBarrasP1 = ""
                rsAnaFact.Close
                Set rsAnaFact = Nothing
                Exit Function
            ElseIf rsAnaFact.RecordCount >= 1 Then
                If rsAnaFact.RecordCount > 1 Then
                    'MsgBox codRubrEfr
                    MultAna = "("
                    While Not rsAnaFact.EOF
                        MultAna = MultAna & BL_TrataStringParaBD(rsAnaFact!cod_ana) & ","
                        rsAnaFact.MoveNext
                    Wend
                    MultAna = Mid(MultAna, 1, Len(MultAna) - 1) & ")"
                Else
                    MultAna = ""
                End If
                rsAnaFact.MoveFirst
                MarcacaoCodigoBarrasP1 = BL_HandleNull(rsAnaFact!cod_ana, "")
                AdicionaAnalise MarcacaoCodigoBarrasP1, True, True, MultAna
                AdicionaReciboManual 1, EcCodEFR, "0", "0", BL_HandleNull(UCase(MarcacaoCodigoBarrasP1), "0"), 1, "", MarcacaoCodigoBarrasP1, True
            End If
            rsAnaFact.Close
            Set rsAnaFact = Nothing
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "MarcacaoCodigoBarrasP1: " & Err.Number & " - " & Err.Description, Me.Name, "MarcacaoCodigoBarrasP1"
    MarcacaoCodigoBarrasP1 = False
    Exit Function
    Resume Next
End Function


Sub Grava_ObsAnaReq()
    On Error GoTo TrataErro

    Dim i As Integer
    Dim sSql As String

    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA ANALISE NA TABELA SL_OBS_ANA_REQ
    ' -----------------------------------------------------------------------------------------------
    If RegistosA.Count > 0 And Trim(RegistosA(1).codAna) <> "" Then
        For i = 1 To RegistosA.Count - 1
            If RegistosA(i).codAna <> "" And EcNumReq <> "" And RegistosA(i).ObsAnaReq <> "" Then
                BL_GravaObsAnaReq CStr(EcNumReq), RegistosA(i).codAna, RegistosA(i).ObsAnaReq, _
                                  CLng(RegistosA(i).seqObsAnaReq), 0, EcSeqUtente, mediComboValorNull
                PreencheNotas
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_ObsAnaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_ObsAnaReq"
    Exit Sub
    Resume Next
End Sub



Private Function EtiqPrint_Recibo(serie_rec As String, num_recibo As String, data As String, n_req As String, efr As String _
                           , nome As String, num_benef As String, analises As String, _
                           total As String, num_anulacao As String, user_emi As String, descricao As String, dt_chega As String, _
                           vd As String, cod_empresa As String, nome_empresa As String, num_contribuinte As String, _
                           total_original As Double, Desconto As Double, Cabecalho As String, Formacao As String, Certificado As String)
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    On Error GoTo TrataErro


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{SERIE_REC}", serie_rec)
    sWrittenData = Replace(sWrittenData, "{NUM_RECIBO}", num_recibo)
    sWrittenData = Replace(sWrittenData, "{DATA}", data)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{EFR}", efr)
    sWrittenData = Replace(sWrittenData, "{NOME}", BL_RemovePortuguese(nome))
    sWrittenData = Replace(sWrittenData, "{NUM_BENEF}", BL_RemovePortuguese(num_benef))
    sWrittenData = Replace(sWrittenData, "{ANALISES}", BL_RemovePortuguese(analises))
    sWrittenData = Replace(sWrittenData, "{TOTAL}", total)
    sWrittenData = Replace(sWrittenData, "{NUM_ANULACAO}", num_anulacao)
    sWrittenData = Replace(sWrittenData, "{USER_EMI}", user_emi)
    sWrittenData = Replace(sWrittenData, "{DESCRICAO}", BL_RemovePortuguese(descricao))
    sWrittenData = Replace(sWrittenData, "{VD}", vd)
    sWrittenData = Replace(sWrittenData, "{DT_CHEGA}", dt_chega)
    sWrittenData = Replace(sWrittenData, "{COD_EMPRESA}", cod_empresa)
    sWrittenData = Replace(sWrittenData, "{NOME_EMPRESA}", BL_RemovePortuguese(nome_empresa))
    sWrittenData = Replace(sWrittenData, "{NUM_CONTRIBUINTE}", num_contribuinte)
    sWrittenData = Replace(sWrittenData, "{TOTAL_ORIGINAL}", total_original)
    sWrittenData = Replace(sWrittenData, "{DESCONTO}", Desconto)
    sWrittenData = Replace(sWrittenData, "{CABECALHO}", Cabecalho)
    sWrittenData = Replace(sWrittenData, "{FORMACAO}", Formacao)
    sWrittenData = Replace(sWrittenData, "{CERTIFICADO}", Certificado)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint_Recibo = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqPrint_Recibo: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqPrint_Recibo"
    EtiqPrint_Recibo = ""
    Exit Function
    Resume Next
End Function

Private Function EtiqPrint_FichaCruzada(NumReq As String, nome As String, morada As String, idade As String, Sexo As String, _
                                         DescrEFR As String, DataChegada As String, destino As String, _
                                         DescrSala As String, DescrMedico As String)
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    On Error GoTo TrataErro


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{NUM_REQ}", NumReq)
    sWrittenData = Replace(sWrittenData, "{NOME}", nome)
    sWrittenData = Replace(sWrittenData, "{MORADA}", morada)
    sWrittenData = Replace(sWrittenData, "{IDADE}", idade)
    sWrittenData = Replace(sWrittenData, "{DESCR_EFR}", DescrEFR)
    sWrittenData = Replace(sWrittenData, "{DATA_CHEGADA}", DataChegada)
    sWrittenData = Replace(sWrittenData, "{DESTINO}", destino)
    sWrittenData = Replace(sWrittenData, "{DESCR_SALA}", DescrSala)
    sWrittenData = Replace(sWrittenData, "{DESCR_MEDICO}", DescrMedico)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint_FichaCruzada = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqPrint_FichaCruzada: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqPrint_FichaCruzada"
    EtiqPrint_FichaCruzada = ""
    Exit Function
    Resume Next
End Function

Public Sub ImprimeAnulRecibo(numAnulacao As Long)
    RECIBO_ImprimeAnulRecibo numAnulacao, RegistosR, FGRecibos.row, EcNumReq, EcPrinterRecibo
    BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeAnulRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeAnulRecibo"
    Exit Sub
    Resume Next
End Sub


Public Sub EcCodsala_Validate(cancel As Boolean)
    cancel = PA_ValidateSala(EcCodSala, EcDescrSala, EcLocal.text)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodsala_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodsala_Validate"
    Exit Sub
    Resume Next
End Sub





''''''''''''''''''''''''''''''''''''''''''''
' Preenche tabela temporaria de ficha ''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Preenche_SLCRFICHACRUZADA(NReq As String, nome As String, morada As String, idade As String, Sexo As String, _
                            Entidade As String, data As String, destino As String, sala As String, Medico As String)
    On Error GoTo TrataErro
    Dim sSql As String
    sSql = "INSERT INTO sl_cr_ficha_cruzada (n_req,nome,morada,idade,sexo,entidade,data," & _
                        "destino,sala,medico, nome_computador, num_sessao) VALUES (" & NReq & "," & BL_TrataStringParaBD(nome) & "," & BL_TrataStringParaBD(morada) & "," & BL_TrataStringParaBD(idade) & "," & _
                        BL_TrataStringParaBD(Sexo) & "," & BL_TrataStringParaBD(Entidade) & "," & BL_TrataDataParaBD(data) & "," & BL_TrataStringParaBD(destino) & "," & BL_TrataStringParaBD(sala) & "," & BL_TrataStringParaBD(Medico) & ", " & _
                        BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ")"
    ' Insere os dados da ficha cruzada na tabela temporaria
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_SLCRFICHACRUZADA: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_SLCRFICHACRUZADA"
    Exit Sub
    Resume Next
End Sub


' --------------------------------------------------------

' PREENCHE TABELA CRYSTAL PARA FICHA CRUZADA

' --------------------------------------------------------

Private Sub Preenche_SLCRETIQFICHACRUZADA(n_req, cod_bar1 As String, legenda1 As String, _
                                          cod_bar2 As String, legenda2 As String, _
                                          cod_bar3 As String, legenda3 As String, _
                                          cod_bar4 As String, legenda4 As String, _
                                          cod_bar5 As String, legenda5 As String, _
                                          cod_bar6 As String, legenda6 As String, _
                                          cod_bar7 As String, legenda7 As String, _
                                          cod_bar8 As String, legenda8 As String, _
                                          cod_bar9 As String, legenda9 As String, _
                                          cod_bar10 As String, legenda10 As String)
    On Error GoTo TrataErro
    Dim sSql As String
    On Error GoTo TrataErro


    ' Variaveis auxiliares
    sSql = "INSERT INTO sl_cr_etiq_ficha_cruzada (n_req, nome_computador, num_sessao , cod_bar1,cod_bar_legend1, cod_bar2,cod_bar_legend2, "
    sSql = sSql & "cod_bar3,cod_bar_legend3, cod_bar4,cod_bar_legend4, cod_bar5,cod_bar_legend5, "
    sSql = sSql & " cod_bar6,cod_bar_legend6, cod_bar7,cod_bar_legend7, cod_bar8,cod_bar_legend8, "
    sSql = sSql & " cod_bar9,cod_bar_legend9, cod_bar10,cod_bar_legend10)"
    sSql = sSql & " VALUES (" & n_req & ", " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar1, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda1, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar2, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda2, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar3, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda3, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar4, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda4, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar5, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda5, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar6, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda6, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar7, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda7, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar8, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda8, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar9, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda9, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(cod_bar10, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda10, "")) & ") "

    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "Preenche_SLCRETIQFICHACRUZADA", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------------------------------------

' ELIMINA A TAXA DO RECIBO - APENAS SE O RECIBO AINDA NAO TIVER SIDO EMITIDO

' -----------------------------------------------------------------------------------------------
Private Function VerificaPodeAlterarRecibo(indice As Long) As Boolean
    On Error GoTo TrataErro
    Dim i As Long
    Dim sSql As String
    VerificaPodeAlterarRecibo = True
    For i = 1 To RegistosR.Count
        If Trim(RegistosRM(indice).ReciboNumDoc) = Trim(RegistosR(i).NumDoc) And RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) Then
                If BG_Mensagem(mediMsgBox, "J� foi emitido recibo para esta an�lise.Confirma a elimina��o?", vbYesNo + vbQuestion, "Eliminar an�lise.") = vbYes Then
                    VerificaPodeAlterarRecibo = False
                Else
                    VerificaPodeAlterarRecibo = True
                End If
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeAlterarRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeAlterarRecibo"
    VerificaPodeAlterarRecibo = False
    Exit Function
    Resume Next
End Function




' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset

    If EcNumReq = "" Then Exit Sub
    sSql = "SELECT count(*) total FROM sl_obs_ana_req WHERE n_req = " & EcNumReq

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True

        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotas: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheNotas"
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------

' UTILIZADORES DOS POSTOS PREENCHE POR DEFEITO A SALA

' --------------------------------------------------------
Private Sub PreencheSalaDefeito()
     On Error GoTo TrataErro
   EcCodSala.Enabled = True
    BtPesquisaSala.Enabled = True
    If gCodSalaAssocUser > 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate True
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheSalaDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheSalaDefeito"
    Exit Sub
    Resume Next
End Sub
Private Sub ImprimeEtiqNovo()
    'Limpar parametros
        BG_LimpaPassaParams
    'Passar parametros necessarios o ao form de impress�o
        gPassaParams.Param(0) = EcNumReq.text

    'Abrir form de impress�o
    FormNEtiqNTubos.Show
    DoEvents
    'FormGestaoRequisicaoAguas.SetFocus

End Sub

' pferreira 2010.08.10
' Selecciona campos "abrev_ute" da tabela sl_identif.
Sub ImprimeEtiq()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim k As Integer
    Dim j As Integer
    Dim sql As String
    Dim CmdDadosTubo As ADODB.Command
    Dim RsDadosTubo As ADODB.recordset
    Dim CmdDadosGhostM As ADODB.Command
    Dim RsDadosGhostM As ADODB.recordset
    Dim CodTuboBarLoc As String
    Dim encontrou As Boolean
    Dim CalculaCapaci As Double
    Dim ordem As Integer
    Dim GhostsJaTratados() As String
    Dim TotalGhostsJaTratados As Integer
    Dim Especiais As Long
    Dim EtqAdministrativas As Long
    Dim NumReqBar As String
    Dim continua As Boolean
    Dim EtqCrystal As String
    Dim EtqPreview As String
    Dim rsReq As ADODB.recordset
    Dim TotalP1s As Integer
    Dim NrReqARS As String
    Dim numEtiq As Long
    Dim dataImpressao As String
    Dim episodio As String
    Dim situacao As String
    Dim rv As Integer

    Dim idade As String
    Dim CodProd_aux As String

    idade = ""


    Especiais = 0

    i = 1



    If gTotalTubos <> 0 Then
        EcEtqNTubos = gTotalTubos
        EcEtqNEsp = Especiais

        'Aten��o - O FormNEtiq retorna nos campos deste form :
        ' EcEtqNAdm   = n� etiquetas administrativas
        ' EcEtqNCopias  = n� copias de etiquetas tubos
        ' EcEtqTipo   = tipo de impress�o
        '   0 - imprimir etq. para tubos e administrativas
        '   1 - imprimir etq. administrativas
        '   2 - imprimir etq. para tubos
        '  -1 - quando se cancela a impress�o
        '
        ' EcEtqNEsp envia para o form o n� etiquetas especiais (com o c�digo do produto ou ordem de an�lises)
        ' EcEtqNTubos envia para o form o n� etiquetas para tubos

        If EFR_Verifica_ARS(EcCodEFR.text) = True Then
            ReDim p1(0)
            Dim ExisteP1 As Boolean
            ExisteP1 = False
            For i = 1 To RegistosA.Count - 1

                For j = 1 To UBound(p1)
                    If RegistosA(i).NReqARS = p1(j).NrP1 Then
                        ExisteP1 = True
                    End If
                Next j
                If ExisteP1 = False Then
                    ReDim Preserve p1(UBound(p1) + 1)
                    p1(UBound(p1)).NrP1 = RegistosA(i).NReqARS
                End If
                ExisteP1 = False
            Next i

            TotalP1s = UBound(p1)
            gNumEtiqAdminDefeito = TotalP1s + 1
        Else
            TotalP1s = 0
            gNumEtiqAdminDefeito = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NumEtiqAdminDefeito"))
        End If
        FormNEtiq.Show vbModal

        FormGestaoRequisicaoAguas.SetFocus

        If Trim(EcEtqNAdm) = "" Then EcEtqNAdm = "0"
        If Trim(EcEtqNCopias) = "" Then EcEtqNCopias = "0"
        If Trim(EcEtqNEsp) = "" Then EcEtqNEsp = "0"
        If Trim(EcEtqNTubos) = "" Then EcEtqNTubos = "0"

        If EcEtqTipo <> "-1" Then

            EtqAdministrativas = EcEtqNAdm

            If gTotalTubos <> 0 And EtqAdministrativas <> 0 Then

                EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")

                If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                End If

                If EtqCrystal = "1" Then
                    If BL_PreviewAberto("Etiquetas") = True Then Exit Sub

                    continua = BL_IniciaReport("Etiqueta", "Etiquetas", crptToPrinter, False)
                    If continua = False Then Exit Sub

                    Call Cria_TmpEtq
                End If

                NumReqBar = ""
                For j = 1 To (7 - Len(EcNumReq))
                    NumReqBar = NumReqBar & "0"
                Next j
                NumReqBar = NumReqBar & EcNumReq

                If EcEtqTipo <> "1" Then
                    'Tubos
                    'N� de c�pias das etiquetas
                    For j = 1 To EcEtqNCopias
                        For i = 1 To gTotalTubos
                            If EtqCrystal = "1" Then
                        'Bruno 28-01-2003
                                If (gEstruturaTubos(i).Especial = True And gEstruturaTubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else
                                    CodProd_aux = gEstruturaTubos(i).CodTubo
                                    gEstruturaTubos(i).CodTubo = ""

                                    dataImpressao = BL_HandleNull(gEstruturaTubos(i).dt_chega, BL_HandleNull(EcDataChegada, EcDtColheita))
                                    sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                        " ( gr_ana, abr_ana, t_utente, utente, n_req, t_urg, dt_req, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                        " VALUES (" & BL_TrataStringParaBD(gEstruturaTubos(i).GrAna) & "," & BL_TrataStringParaBD(gEstruturaTubos(i).abrAna) & "," & BL_TrataStringParaBD(CbTipoUtente.text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                        "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " ") & _
                                        "'," & BL_TrataDataParaBD(dataImpressao) & "," & _
                                        "'" & gEstruturaTubos(i).CodProd & IIf(gEstruturaTubos(i).CodTubo <> "", " (", " ") & gEstruturaTubos(i).CodTubo & IIf(gEstruturaTubos(i).CodTubo <> "", ")'", "'") & ", " & _
                                        BL_TrataStringParaBD(BL_AbreviaNome(EcNome, 40)) & ",'" & gEstruturaTubos(i).codTuboBar & NumReqBar & "', '" & BG_CvPlica(gEstruturaTubos(i).Designacao_tubo) & "', '" & situacao & "', '" & episodio & "', '" & idade & "')"

                                    BG_ExecutaQuery_ADO sql

                                    If gSQLError <> 0 Then
                                        BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                        Set CmdDadosTubo = Nothing
                                        Set CmdDadosGhostM = Nothing
                                        Exit Sub
                                    End If
                                End If
                            Else
                                If (gEstruturaTubos(i).Especial = True And gEstruturaTubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else

                                    If Not LerEtiqInI Then
                                        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                        Exit Sub
                                    End If
                                    If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                        MsgBox "Imposs�vel abrir impressora etiquetas"
                                        Exit Sub
                                    End If

                                    dataImpressao = BL_HandleNull(gEstruturaTubos(i).dt_chega, BL_HandleNull(EcDataChegada, EcDtColheita))
                                    numEtiq = BL_ArredondaCima(gEstruturaTubos(i).num_ana / gEstruturaTubos(i).num_max_ana) * gEstruturaTubos(i).num_copias
                                    Call EtiqPrint(gEstruturaTubos(i).GrAna, gEstruturaTubos(i).abrAna, _
                                                   CbTipoUtente.text, EcUtente, _
                                                   "", EcNumReq, IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                                   dataImpressao, _
                                                   gEstruturaTubos(i).CodProd & IIf(gEstruturaTubos(i).CodTubo <> "", " (", " ") & gEstruturaTubos(i).CodTubo & IIf(gEstruturaTubos(i).CodTubo <> "", ")", ""), _
                                                   EcNome, EcAbrevUte.text, gEstruturaTubos(i).codTuboBar & NumReqBar, _
                                                   BG_CvPlica(gEstruturaTubos(i).Designacao_tubo), gEstruturaTubos(i).inf_complementar, numEtiq, gEstruturaTubos(i).Descr_etiq_ana, , "", gEstruturaTubos(i).codTuboBar)

                                    EtiqClosePrinter

                                    BL_RegistaImprEtiq EcNumReq, gEstruturaTubos(i).CodTubo
                                End If
                            End If
                        Next i
                    Next j
                End If


                If EcEtqTipo <> "2" Then
                    'Administrativas
                    For k = 1 To EtqAdministrativas
                        If EtqCrystal = "1" Then

                            dataImpressao = BL_HandleNull(EcDataChegada, EcDtColheita)
                            sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                " ( t_utente, utente, n_req, t_urg, dt_req, ordem, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                " VALUES (" & BL_TrataStringParaBD(CbTipoUtente.text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " ") & _
                                "'," & BL_TrataDataParaBD(dataImpressao) & ",null,null, " & _
                                BL_TrataStringParaBD(BL_AbreviaNome(EcNome, 40)) & ",'" & NumReqBar & "', 'Administrativa', '" & situacao & "', '" & episodio & "', '" & idade & "')"
                            BG_ExecutaQuery_ADO sql

                            If gSQLError <> 0 Then
                                BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                Set CmdDadosTubo = Nothing
                                Set CmdDadosGhostM = Nothing
                                Exit Sub
                            End If
                        Else
                            ' --------
                            'Tubos(k).GrAna = "Administrat."

                            If Not LerEtiqInI Then
                                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                Exit Sub
                            End If
                            If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                MsgBox "Imposs�vel abrir impressora etiquetas"
                                Exit Sub
                            End If

                            If TotalP1s >= k Then
                                NrReqARS = p1(k).NrP1
                            Else
                                NrReqARS = ""
                            End If

                            dataImpressao = BL_HandleNull(EcDataChegada, EcDtColheita)
                            Call EtiqPrint("", _
                                           "", _
                                           CbTipoUtente.text, _
                                           EcUtente, "", _
                                           EcNumReq, _
                                           IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                           dataImpressao, _
                                           "", EcNome, EcAbrevUte.text, "", "Administrativa", "", "1", "", NrReqARS, "", "")

                            EtiqClosePrinter
                            ' --------

                            ' imprime etiq fim
                            'soliveira arunce
                            'chave que indica se queremos etiqueta de fim ou n�o
                            If gEtiqFim = 1 Then
                                If Not LerEtiqIni_fim Then
                                    MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                    Exit Sub
                                End If
                                If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
                                    MsgBox "Imposs�vel abrir impressora etiquetas"
                                    Exit Sub
                                End If

                                dataImpressao = BL_HandleNull(EcDataChegada, EcDtColheita)
                                Call EtiqPrint("", _
                                               "", _
                                               CbTipoUtente.text, _
                                               EcUtente, "", _
                                               EcNumReq, _
                                               IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                               dataImpressao, _
                                               "", EcNome, EcAbrevUte.text, "", "Administrativa", "", "1", "", NrReqARS, "", "")

                                BL_EtiqClosePrinter
                            End If
                        End If
                    Next k
                End If



                If EtqCrystal = "1" Then
                    'Imprime o relat�rio no Crystal Reports

                    Dim Report As CrystalReport
                    Set Report = forms(0).Controls("Report")

                    Report.SQLQuery = "SELECT " & _
                                      "     T_UTENTE, " & _
                                      "     UTENTE, " & _
                                      "     N_PROC, " & _
                                      "     N_REQ, " & _
                                      "     T_URG, " & _
                                      "     DT_REQ, " & _
                                      "     ORDEM, " & _
                                      "     PRODUTO, " & _
                                      "     NOME_UTE, " & _
                                      "     N_REQ_BAR " & _
                                      "FROM " & _
                                      "     SL_CR_ETIQ" & gNumeroSessao & " SL_CR_ETIQ"

                    For i = 0 To Printers.Count - 1
                        If Printers(i).DeviceName = EcPrinterEtiq Then
                            Report.PrinterName = Printers(i).DeviceName
                            Report.PrinterDriver = Printers(i).DriverName
                            Report.PrinterPort = Printers(i).Port
                        End If
                    Next i

                    If Trim(gEtiqPreview) Then
                        Report.Destination = crptToWindow
                        Report.WindowState = crptMaximized
                        Report.PageShow Report.ReportStartPage
                    Else
                        Report.Destination = crptToPrinter
                    End If

                    Call BL_ExecutaReport

                    If Trim(gEtiqPreview) Then
                        Report.PageZoom (200)
                    End If

                    'Elimina as Tabelas criadas para a impress�o dos relat�rios
                    Call BL_RemoveTabela("sl_cr_etiq" & gNumeroSessao)
                Else
                    'EtiqClosePrinter
                End If
            End If

            If Me.EcImprimirResumo = "1" Then ImprimeResumo
        End If

    Set CmdDadosTubo = Nothing
    Set CmdDadosGhostM = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeEtiq: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeEtiq"
    Exit Sub
    Resume Next
End Sub

Sub Cria_TmpEtq()
    On Error GoTo TrataErro

    Dim TmpEtq(1 To 16) As DefTable

    TmpEtq(1).NomeCampo = "t_utente"
    TmpEtq(1).tipo = "STRING"
    TmpEtq(1).tamanho = 5

    TmpEtq(2).NomeCampo = "utente"
    TmpEtq(2).tipo = "STRING"
    TmpEtq(2).tamanho = 20

    TmpEtq(3).NomeCampo = "n_proc"
    TmpEtq(3).tipo = "STRING"
    TmpEtq(3).tamanho = 100

    TmpEtq(4).NomeCampo = "n_req"
    TmpEtq(4).tipo = "STRING"
    TmpEtq(4).tamanho = 9

    TmpEtq(5).NomeCampo = "t_urg"
    TmpEtq(5).tipo = "STRING"
    TmpEtq(5).tamanho = 3

    TmpEtq(6).NomeCampo = "dt_req"
    TmpEtq(6).tipo = "DATE"

    TmpEtq(7).NomeCampo = "ordem"
    TmpEtq(7).tipo = "STRING"
    TmpEtq(7).tamanho = 3

    TmpEtq(8).NomeCampo = "produto"
    TmpEtq(8).tipo = "STRING"
    TmpEtq(8).tamanho = 25

    TmpEtq(9).NomeCampo = "nome_ute"
    TmpEtq(9).tipo = "STRING"
    TmpEtq(9).tamanho = 80

    TmpEtq(10).NomeCampo = "n_req_bar"
    TmpEtq(10).tipo = "STRING"
    TmpEtq(10).tamanho = 20

    TmpEtq(11).NomeCampo = "gr_ana"
    TmpEtq(11).tipo = "STRING"
    TmpEtq(11).tamanho = 40

    TmpEtq(12).NomeCampo = "abr_ana"
    TmpEtq(12).tipo = "STRING"
    TmpEtq(12).tamanho = 10

    TmpEtq(13).NomeCampo = "descr_tubo"
    TmpEtq(13).tipo = "STRING"
    TmpEtq(13).tamanho = 40

    TmpEtq(14).NomeCampo = "situacao"
    TmpEtq(14).tipo = "STRING"
    TmpEtq(14).tamanho = 7

    TmpEtq(15).NomeCampo = "episodio"
    TmpEtq(15).tipo = "STRING"
    TmpEtq(15).tamanho = 15

    TmpEtq(16).NomeCampo = "idade"
    TmpEtq(16).tipo = "STRING"
    TmpEtq(16).tamanho = 3

    Call BL_CriaTabela("sl_cr_etiq" & gNumeroSessao, TmpEtq)
Exit Sub
TrataErro:
    BG_LogFile_Erros "Cria_TmpEtq: " & Err.Number & " - " & Err.Description, Me.Name, "Cria_TmpEtq"
    Exit Sub
    Resume Next
End Sub

Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal abrev_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String, _
    ByVal num_copias As Integer, _
    ByVal Descr_etiq_ana As String, _
    Optional ByVal N_REQ_ARS As String, _
    Optional ByVal n_Req_assoc As String, _
    Optional ByVal cod_etiq As String _
    ) As Boolean
     On Error GoTo TrataErro

    'soliveira terrugem n_req_assoc
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_AbreviaNome(BL_RemovePortuguese(StrConv(nome_ute, vbProperCase)), 40))
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", abrev_ute)
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", N_REQ_ARS)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", Descr_etiq_ana)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ASSOC}", n_Req_assoc)
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", num_copias)
    sWrittenData = Replace(sWrittenData, "{COD_ETIQ}", cod_etiq)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", num_copias)
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqPrint: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqPrint"
    Exit Function
    Resume Next
End Function
Sub AcrescentaAnalise(linha As Integer)

    Dim i As Integer
    Dim k As Long
    Dim iRec As Integer
    Dim lastCodAgrup As String
    On Error GoTo Trata_Erro

    'Cria linha vazia
    FGAna.AddItem "", FGAna.row
    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
    ExecutaCodigoA = True
    Fgana_RowColChange

    ReDim Preserve MaReq(UBound(MaReq) + 1)
    If UBound(MaReq) = 0 Then ReDim MaReq(1)

'    i = UBound(MaReq)
    LastRowA = linha
    LastColA = 0
    i = RegistosA.Count

    While i > linha

            RegistosA(i).codAna = RegistosA(i - 1).codAna
            RegistosA(i).descrAna = RegistosA(i - 1).descrAna
            RegistosA(i).Estado = RegistosA(i - 1).Estado
            RegistosA(i).ObsAnaReq = RegistosA(i - 1).ObsAnaReq
            RegistosA(i).seqObsAnaReq = RegistosA(i - 1).seqObsAnaReq
            RegistosA(i).NReqARS = RegistosA(i - 1).NReqARS
        i = i - 1
    Wend

    'Actualiza ord_marca da estrutura MaReq
    k = 1
    lastCodAgrup = ""
    While k <= UBound(MaReq)
        If MaReq(k).Ord_Marca >= linha Then
            MaReq(k).Ord_Marca = MaReq(k).Ord_Marca + 1
            If lastCodAgrup <> MaReq(k).cod_agrup Then
                iRec = DevIndice(MaReq(k).cod_agrup)
                If iRec > -1 Then
                    RegistosRM(iRec).ReciboOrdemMarcacao = RegistosRM(iRec).ReciboOrdemMarcacao + 1
                End If
                lastCodAgrup = MaReq(k).cod_agrup
            End If
        End If
        k = k + 1
    Wend


    RegistosA(linha).codAna = ""
    RegistosA(linha).descrAna = ""
    RegistosA(linha).Estado = "-1"
    RegistosA(linha).ObsAnaReq = ""
    RegistosA(linha).seqObsAnaReq = 0
    RegistosA(linha).NReqARS = ""
    RegistosA(linha).p1 = ""
    RegistosA(linha).NReqARS = ""


    FGAna.TextMatrix(LastRowA, 2) = RegistosA(linha).NReqARS
    FGAna.TextMatrix(LastRowA, 3) = RegistosA(linha).p1



    ExecutaCodigoA = True
    FGAna.RowSel = linha
    FGAna.ColSel = 0
    Call Fgana_RowColChange
    FGAna.row = linha
    LastRowA = linha
    LastColA = 0


    Exit Sub

Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "AcrescentaAnalise: " & Err.Description
    End If

    Resume Next


End Sub


' --------------------------------------------------------

' ALTERA ENTIDADE DE UMA ANALISE NO RECIBO MANUAL

' --------------------------------------------------------
Private Sub AlteraEntidadeReciboManual(linha As Long, codEntidade As String, DescrEFR As String, CodPai As String, linhaAna As Long)
    On Error GoTo TrataErro
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim valorUnitEFR As Double
    Dim mens As String
    Dim codigo As String
    Dim flg_FdsValFixo As Integer
    'codigo = RegistosA(linhaAna).codAna


    If linha > 0 Then
        If RegistosRM(linha).flgCodBarras = 1 Then
            Exit Sub
        End If
        If EliminaReciboManual(linha, RegistosRM(linha).ReciboCodFacturavel, False) = False Then
            Exit Sub
        End If
    End If

    If codEntidade <> "" Then
        If RegistosRM.Count = 0 Or linha = 0 Then
            'linha = RegistosRM.Count
            CriaNovaClasse RegistosRM, linha, "REC_MAN"
        End If
        If RegistosRM(linha).ReciboEntidade = "" And linhaAna > mediComboValorNull Then
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            RegistosRM(linha).ReciboQuantidade = 1
            RegistosRM(linha).ReciboQuantidadeEFR = 1
            RegistosRM(linha).ReciboNumDoc = "0"
            RegistosRM(linha).ReciboSerieDoc = "0"
            RegistosRM(linha).ReciboCodFacturavel = RegistosA(linhaAna).codAna
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            RegistosRM(linha).ReciboOrdemMarcacao = 0
            If linha = 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            ElseIf linha > 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            End If
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento
        Else
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            If RegistosRM(linha).ReciboOrdemMarcacao = 0 Then
                RegistosRM(linha).ReciboOrdemMarcacao = 0
            End If

        End If
        ' RECALCULA A TAXA
        codAnaEfr = ""
        If RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento Then
            taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, BL_HandleNull(EcDataChegada, dataAct), codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)

            taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                         EcDtColheita, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidadeEFR, "", False, descrAnaEfr, mediComboValorNull)
            RegistosRM(linha).ReciboValorUnitEFR = valorUnitEFR
            RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
            RegistosRM(linha).ReciboDescrAnaEFR = descrAnaEfr

        Else
            taxa = 0
        End If
        Select Case taxa
            Case -1
                mens = " An�lise n�o est� mapeada para o FACTUS!"
                taxa = 0
            Case -2
                mens = "Impossivel abrir conex�o com o FACTUS!"
                taxa = 0
            Case -3
                mens = "Impossivel seleccionar portaria activa para a entidade em causa!"
                taxa = 0
            Case -4
                mens = "N�o existe taxa codificada para a an�lise " & RegistosRM(linha).ReciboCodFacturavel & "!"
                taxa = 0
            Case Else
                mens = ""
        End Select
        taxa = BG_CvDecimalParaCalculo(taxa)

        RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
        If mens <> "" Then
            If codEntidade <> gCodEFRSemCredencial Then
                BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
            End If
            If CDbl(taxa) <= 0 And codEntidade <> gCodEFRSemCredencial Then
                RegistosRM(linha).ReciboEntidade = gEfrParticular
                RegistosRM(linha).ReciboCodEfrEnvio = gEfrParticular
                taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, EcDataChegada, codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)

                taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                             EcDtColheita, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidadeEFR, "", False, descrAnaEfr, mediComboValorNull)
                RegistosRM(linha).ReciboValorUnitEFR = valorUnitEFR
                RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
                RegistosRM(linha).ReciboDescrAnaEFR = descrAnaEfr

            If taxa > 0 Then
                    RegistosRM(linha).ReciboTaxa = taxa
                Else
                    RegistosRM(linha).ReciboTaxa = "0,0"
                End If
            Else
                RegistosRM(linha).ReciboTaxa = "0,0"
            End If
        Else
            RegistosRM(linha).ReciboTaxa = taxa
        End If

        RegistosRM(linha).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade), 1)
        RegistosRM(linha).ReciboQuantidadeEFR = RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboFDS = BL_HandleNull(EcFimSemana, "0")
        RegistosRM(linha).ReciboCodigoPai = CodPai
        RegistosRM(linha).ReciboTaxaUnitario = RegistosRM(linha).ReciboTaxa
        RegistosRM(linha).ReciboTaxa = Replace(RegistosRM(linha).ReciboTaxaUnitario, ".", ",") * RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboTaxa = Round(RegistosRM(linha).ReciboTaxa, 2)

        RegistosRM(linha).ReciboValorEFR = Replace(RegistosRM(linha).ReciboValorUnitEFR, ".", ",") * RegistosRM(linha).ReciboQuantidadeEFR
        RegistosRM(linha).ReciboValorEFR = Round(RegistosRM(linha).ReciboValorEFR, 2)
        RegistosRM(linha).flgCodBarras = 0

        'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
        'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
        flg_FdsValFixo = VerificaFdsValFixo(linha)
        If RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
            RegistosRM(linha).ReciboTaxa = RegistosRM(linha).ReciboTaxa * 2
        ElseIf RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
            RegistosRM(linha).ReciboTaxa = "0"
        End If

        AdicionaAnaliseAoReciboManual linha, False
        If linhaAna > 0 Then
            FGAna.TextMatrix(linhaAna, lColFgAnaP1) = RegistosRM(linha).ReciboP1
            FGAna.TextMatrix(linhaAna, lColFgAnaCodEFR) = RegistosRM(linha).ReciboEntidade
            FGAna.TextMatrix(linhaAna, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(linha).ReciboEntidade)
            FGAna.TextMatrix(linhaAna, lColFgAnaPreco) = RegistosRM(linha).ReciboTaxa
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraEntidadeReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraEntidadeReciboManual"
    Exit Sub
    Resume Next
End Sub

Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo erro

    Dim s As String

    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""

    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf

            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1

    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
Exit Function
erro:
    If Err.Number = 55 Then
        Close 1
        Open s For Input As 1
        Resume Next
    End If
    BG_LogFile_Erros "Erro ao abrir Ficheiro:" & s & " " & Err.Description, Me.Name, "LerEtiqIni", True
    Exit Function
    Resume Next
End Function
Private Function LerEtiqIni_fim() As Boolean
    Dim aux As String
    On Error GoTo erro

    Dim s As String

    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""

    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq_fim.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq_fim.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqIni_fim = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf

            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1

    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqIni_fim = True
    Exit Function
erro:
End Function
Private Function TrocaBinarios(ByVal s As String) As String
    On Error GoTo TrataErro

    Dim p As Long
    Dim k As Byte

    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s
Exit Function
TrataErro:
    BG_LogFile_Erros "TrocaBinarios: " & Err.Number & " - " & Err.Description, Me.Name, "TrocaBinarios"
    Exit Function
    Resume Next
End Function

Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    On Error GoTo TrataErro

    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String

    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function

    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function

    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function

    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If

    EtiqOpenPrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqOpenPrinter: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqOpenPrinter"
    Exit Function
    Resume Next
End Function

Private Function EtiqClosePrinter() As Boolean
    On Error GoTo TrataErro

    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function

    EtiqClosePrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqClosePrinter: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqClosePrinter"
    Exit Function
    Resume Next
End Function

Private Sub EcNome_LostFocus()
     On Error GoTo TrataErro

    If gNomeCapitalizado = 1 Then
        EcNome.text = StrConv(EcNome.text, vbProperCase)
    Else
        EcNome.text = UCase(EcNome.text)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNome_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNome_LostFocus"
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------

' VERIFICA SE ANALISE JA FOI MARCADA NOUTRA REQUUISICAO NO PROPRIO DIA

' --------------------------------------------------------
Private Function VerificaAnaliseJaMarcada(n_req As String, codAgrup As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim data As Integer
    Dim Data2 As String
    data = -1
    sSql = "SELECT prazo_Val FROM slv_analises WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup) & " AND prazo_val IS NOT NULL "
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        data = BL_HandleNull(rsAna!prazo_val, 0)
    End If
    rsAna.Close
    If data = -1 Then
        data = 0
    End If
    Data2 = CStr(CDate(dataAct) - data)
    sSql = "SELECT distinct x2.n_Req FROM  sl_requis x2, sl_marcacoes x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
    sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(dataAct)
    sSql = sSql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req   AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
    If n_req <> "" Then
        sSql = sSql & " AND x2.n_req <> " & n_req
    End If
    sSql = sSql & " UNION SELECT distinct x2.n_Req FROM  sl_requis x2, sl_realiza x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
    sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(dataAct)
    sSql = sSql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
    If n_req <> "" Then
        sSql = sSql & " AND x2.n_req <> " & n_req
    End If
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaAnaliseJaMarcada = True
        MsgBox "A an�lise j� foi marcada  para o Utente na requisi��o:" & rsAna!n_req & " !"
    Else
        VerificaAnaliseJaMarcada = False
    End If
    rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaAnaliseJaMarcada: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaAnaliseJaMarcada"
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------

' ADICIONA ANALISE AO RECIBO MANUAL

' --------------------------------------------------------
Private Function AdicionaReciboManual(ByVal flg_manual As Integer, ByVal codEfr As String, ByVal nRec As String, ByVal sRec As String, ByVal CodFacturavel As String, _
                                 ByVal quantidade As Integer, ByVal CodigoPai As String, ByVal codAna As String, ByVal codBarras As Boolean) As Integer
    On Error GoTo TrataErro
    Dim anaFacturar As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim mens As String
    Dim indice As Long
    Dim flg_FdsValFixo As Integer
    Dim AnaRegra As String
    Dim p1 As String
    Dim corP1 As String
    Dim valorUnitEFR As Double
    Dim iAux As Integer
    If CodFacturavel = "" Then Exit Function
    indice = BL_HandleNull(RegistosRM.Count, 0)
    If indice = 0 Then
        CriaNovaClasse RegistosRM, 1, "REC_MAN"
        indice = 1
    End If
    ' -----------------------------------------
    ' ENTIDADE E NUMERO DE DOCUMENTO
    ' -----------------------------------------
    RegistosRM(indice).ReciboEntidade = codEfr
    RegistosRM(indice).ReciboCodEfrEnvio = codEfr
    RegistosRM(indice).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEfr)
    RegistosRM(indice).ReciboNumDoc = nRec
    RegistosRM(indice).ReciboSerieDoc = sRec
    RegistosRM(indice).ReciboCodAna = BL_HandleNull(codAna, CodFacturavel)
    RegistosRM(indice).ReciboCodFacturavel = BL_HandleNull(UCase(CodFacturavel), "0")
    If VerificaCodFacturavel(RegistosRM(indice).ReciboCodFacturavel) = False Then
        RegistosRM(indice).ReciboCodFacturavel = codAna
    End If
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    RegistosRM(indice).ReciboAnaNaoFact = IF_Verifica_Regra_PRIVADO(RegistosRM(indice).ReciboCodFacturavel, codEfr)
    anaFacturar = IF_VerificaFacturarNovaAnalise(RegistosRM(indice).ReciboCodFacturavel, codEfr)
    If anaFacturar <> "" Then
        gMsgTitulo = "Inserir"
        gMsgMsg = "Quer facturar a an�lise: " & BL_SelCodigo("SLV_ANALISES_FACTUS", "DESCR_ANA", "COD_ANA", anaFacturar) & "?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)

        If gMsgResp = vbYes Then
            CriaNovaClasse RegistosRM, indice, "REC_MAN"
            AdicionaReciboManual 1, codEfr, "0", "0", anaFacturar, 1, RegistosRM(indice).ReciboCodAna, anaFacturar, codBarras
        End If
    End If


    ' -----------------------------------------
    ' TAXA
    ' -----------------------------------------
    codAnaEfr = ""
    descrAnaEfr = ""
    If RegistosRM(indice).ReciboEntidade <> gCodEFRSemCredencial Then
        taxa = IF_RetornaTaxaAnalise(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                     CbTipoUtente, EcUtente, EcDtColheita, codAnaEfr, descrAnaEfr, False)

        taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                     EcDtColheita, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, quantidade, "", False, descrAnaEfr, mediComboValorNull)
        RegistosRM(indice).ReciboValorUnitEFR = valorUnitEFR
        taxa = Replace(taxa, ".", ",")
        taxaEfr = Replace(taxaEfr, ".", ",")
        If Mid(taxa, 1, 1) = "," Then
            taxa = "0" & taxa
        End If


        Select Case taxa
            Case "-1"
                mens = " An�lise n�o est� mapeada para o FACTUS!"
                taxa = 0
            Case "-2"
                mens = "Impossivel abrir conex�o com o FACTUS!"
                taxa = 0
            Case "-3"
                mens = "Impossivel seleccionar portaria activa!"
                taxa = 0
            Case "-4"
                mens = "An�lise n�o comparticipada. Pre�o PARTICULAR!"
                taxa = 0
            Case "-5"
                mens = "N�o existe tabela codificada para a entidade em causa!"
                taxa = 0
            Case Else
                mens = ""
        End Select
    Else
        taxa = "0,0"
    End If
    taxa = BG_CvDecimalParaCalculo(taxa)
    RegistosRM(indice).ReciboCodAnaEFR = codAnaEfr
    RegistosRM(indice).ReciboDescrAnaEFR = descrAnaEfr
    If mens <> "" And gEfrParticular <> mediComboValorNull Then
        BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
        If CDbl(taxa) <= 0 Then
            RegistosRM(indice).ReciboEntidade = gEfrParticular
            RegistosRM(indice).ReciboCodEfrEnvio = gEfrParticular
            RegistosRM(indice).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", gEfrParticular)
        End If
        taxa = IF_RetornaTaxaAnalise(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, CbTipoUtente, EcUtente, EcDataChegada, codAnaEfr, RegistosRM(indice).ReciboDescrAnaEFR, False)
        taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                     EcDtColheita, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, quantidade, "", False, descrAnaEfr, mediComboValorNull)
        RegistosRM(indice).ReciboValorUnitEFR = valorUnitEFR
        If taxa > 0 Then
            RegistosRM(indice).ReciboTaxa = Replace(taxa, ".", ",")
        Else
            RegistosRM(indice).ReciboTaxa = "0,0"
        End If
    Else
        RegistosRM(indice).ReciboTaxa = taxa
    End If
    taxa = BG_CvDecimalParaCalculo(taxa)
    taxaEfr = BG_CvDecimalParaCalculo(taxaEfr)

    RegistosRM(indice).ReciboFDS = BL_HandleNull(EcFimSemana, "0")
    RegistosRM(indice).ReciboCodigoPai = BL_HandleNull(CodigoPai, "")
    If quantidade <= 1 Then
        RegistosRM(indice).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade), 1)
    Else
        RegistosRM(indice).ReciboQuantidade = quantidade
    End If
    RegistosRM(indice).ReciboQuantidadeEFR = RegistosRM(indice).ReciboQuantidade
    RegistosRM(indice).ReciboTaxaUnitario = RegistosRM(indice).ReciboTaxa
    RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxaUnitario * RegistosRM(indice).ReciboQuantidade
    RegistosRM(indice).ReciboValorEFR = RegistosRM(indice).ReciboValorUnitEFR * RegistosRM(indice).ReciboQuantidadeEFR

    'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
    'SE TIVER VALOR FIXO - COLOCA PRE�O A ZERO
    flg_FdsValFixo = VerificaFdsValFixo(indice)
    If RegistosRM(indice).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
        RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxa * 2 * RegistosRM(indice).ReciboQuantidade
    ElseIf RegistosRM(indice).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
        RegistosRM(indice).ReciboTaxa = "0"
    End If

    RegistosRM(indice).ReciboTaxa = Round(RegistosRM(indice).ReciboTaxa, 2)
    RegistosRM(indice).ReciboFlgReciboManual = flg_manual
    RegistosRM(indice).ReciboFlgMarcacaoRetirada = "0"

    ' -----------------------------------------
    ' VERIFICA SE APLICA A REGRA
    ' -----------------------------------------
    AnaRegra = ""
    AnaRegra = VerificaRegra(indice)
    If AnaRegra <> "" Then
        RegistosRM(indice).ReciboTaxa = "0,0"
        RegistosRM(indice).ReciboFlgRetirado = "1"
        BG_Mensagem mediMsgBox, "An�lise gratuita. An�lise " & AnaRegra & " marcada.", vbInformation, "An�lise"
    End If

    ' -----------------------------------------
    ' P1
    ' -----------------------------------------
    If indice = 1 Then
        p1 = 1
        corP1 = "B"
        RegistosRM(indice).ReciboCodMedico = ""
        RegistosRM(indice).ReciboNrBenef = ""
    ElseIf indice > 1 Then
        If flg_manual = 0 Then
            If FGAna.row > 1 Then
                iAux = -1
                iAux = DevIndice(FGAna.TextMatrix(FGAna.row - 1, 0))
                If iAux > -1 Then
                    p1 = BL_HandleNull(Trim(RegistosRM(iAux).ReciboP1), "-1")
                    If Flg_IncrementaP1 = True Then
                        If IsNumeric(p1) Then
                            p1 = p1 + 1
                            corP1 = "B"
                        End If
                    End If
                End If
            End If
        Else
            p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "-1")
            corP1 = BL_HandleNull(RegistosRM(indice - 1).ReciboCorP1, "B")
        End If
        If p1 = "" Then
            If indice > 1 Then
                p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "1")
            End If
        End If


        RegistosRM(indice).ReciboCodMedico = RegistosRM(indice - 1).ReciboCodMedico
        RegistosRM(indice).ReciboNrBenef = RegistosRM(indice - 1).ReciboNrBenef
    End If
    RegistosRM(indice).ReciboP1 = p1
    RegistosRM(indice).ReciboCorP1 = corP1
    RegistosRM(indice).ReciboFlgAutomatico = 0
    RegistosRM(indice).flgCodBarras = 0
    'ORDEM DE MARCACAO
    RegistosRM(indice).ReciboOrdemMarcacao = RetornaOrdemP1(indice)
    ' -----------------------------------------
    ' PERFIL MARCACAO - GUARDADO NA TABELA SL_RECIBOS_DET
    ' -----------------------------------------
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    RegistosRM(indice).ReciboIsencao = gTipoIsencaoNaoIsento
    'RegistosRM(indice).indiceAna = indiceAna

    ' -----------------------------------------
    ' ADICIONA ANALISE � ESTRUTURA DE RECIBOS SE NAO FOR ISENTO
    ' -----------------------------------------
    If RegistosRM(indice).ReciboEntidade <> mediComboValorNull Then
        AdicionaAnaliseAoReciboManual indice, False
    End If
    AdicionaReciboManual = indice
    CriaNovaClasse RegistosRM, indice, "REC_MAN"
Exit Function
TrataErro:
    AdicionaReciboManual = -1
    BG_LogFile_Erros "AdicionaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaReciboManual"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------

' ADICIONA ANALISE AO RECIBO MANUAL

' --------------------------------------------------------
Private Function VerificaRegra(indice As Long) As String
    On Error GoTo TrataErro
    Dim i As Long
    Dim iRec As Long

    VerificaRegra = ""

    For i = 1 To RegistosA.Count
        iRec = DevIndice(RegistosA(i).codAna)
        If RegistosA(i).codAna <> "" And iRec > -1 Then

            If RegistosA(i).codAna = RegistosRM(indice).ReciboAnaNaoFact Then
                If RegistosRM(iRec).ReciboIsencao = gTipoIsencaoNaoIsento And RegistosRM(iRec).ReciboEntidade = RegistosRM(iRec).ReciboEntidade Then
                    VerificaRegra = RegistosA(i).descrAna
                End If
            End If

            If RegistosRM(indice).ReciboCodFacturavel = RegistosRM(iRec).ReciboAnaNaoFact Then
                If RegistosRM(iRec).ReciboIsencao = gTipoIsencaoNaoIsento And RegistosRM(iRec).ReciboEntidade = RegistosA(i).ReciboEntidade Then
                    If VerificaPodeAlterarRecibo(i) = True Then
                        If EliminaReciboManual(iRec, RegistosA(i).codAna, False) = True Then
                            RegistosRM(iRec).ReciboTaxa = "0,0"
                            RegistosRM(iRec).ReciboFlgRetirado = "1"
                            FGAna.TextMatrix(i, lColFgAnaPreco) = "0,0"
                            BG_Mensagem mediMsgBox, "An�lise gratuita. An�lise " & RegistosA(indice).descrAna & " marcada.", vbInformation, "An�lise"
                            AdicionaAnaliseAoReciboManual iRec, False
                        End If
                    End If
                End If
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaRegra: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRegra"
    VerificaRegra = ""
    Exit Function
    Resume Next
End Function


Private Function VerificaRegraSexoMarcar(codAgrup As String, seq_utente As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAux As New ADODB.recordset
    Dim Sexo As String

    If Mid(codAgrup, 1, 1) = "S" Then
        sSql = "SELECT flg_sexo FROM sl_ana_s where cod_ana_S = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "C" Then
        sSql = "SELECT flg_sexo FROM sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "P" Then
        sSql = "SELECT flg_sexo FROM sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    End If
    rsAux.CursorType = adOpenStatic
    rsAux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAux.Open sSql, gConexao
    If rsAux.RecordCount > 0 Then
        If BL_HandleNull(rsAux!flg_sexo, "") <> "" Then
            Sexo = rsAux!flg_sexo
            rsAux.Close

            sSql = "SELECT sexo_ute FROM sl_identif where seq_utente = " & seq_utente
            rsAux.CursorType = adOpenStatic
            rsAux.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAux.Open sSql, gConexao
            If rsAux.RecordCount > 0 Then
                If BL_HandleNull(rsAux!sexo_ute, "") = Sexo Then
                    rsAux.Close
                    VerificaRegraSexoMarcar = True
                    Exit Function
                Else
                    rsAux.Close
                    VerificaRegraSexoMarcar = False
                    Exit Function
                End If
            Else
                rsAux.Close
                VerificaRegraSexoMarcar = False
                Exit Function
            End If

        Else
            rsAux.Close
            VerificaRegraSexoMarcar = True
            Exit Function
        End If

    End If
    Set rsAux = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaRegraSexoMarcar: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRegraSexoMarcar"
    VerificaRegraSexoMarcar = False
    Exit Function
    Resume Next

End Function

Public Sub ImprimeRecibo(descricao As String)
    On Error GoTo TrataErro
        RECIBO_ImprimeRecibo descricao, EcNumReq, RegistosR(FGRecibos.row).NumDoc, RegistosR(FGRecibos.row).SerieDoc, _
                             RegistosR(FGRecibos.row).codEntidade, EcPrinterRecibo, RegistosR(FGRecibos.row).SeqUtente

BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeRecibo"
    Exit Sub
    Resume Next
End Sub

Sub VerificaUtentePagaActoMedico(codEfr As String, serieRec As String, NumDoc As String)
    On Error GoTo TrataErro
    Dim RsAM As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim Flg_ExisteActoMed As Boolean

    Set RsAM = New ADODB.recordset
    RsAM.CursorLocation = adUseServer
    RsAM.CursorType = adOpenStatic
    sql = "select flg_acto_med_ute from sl_efr where cod_efr = " & Trim(codEfr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsAM.Open sql, gConexao
    If RsAM.RecordCount > 0 Then
        If BL_HandleNull(RsAM!flg_acto_med_ute, 0) = 1 Then
            For i = 1 To RegistosRM.Count
                If Trim(RegistosRM(i).ReciboCodFacturavel) <> "" Then
                    If Trim(RegistosRM(i).ReciboEntidade) = codEfr And Trim(RegistosRM(i).ReciboSerieDoc) = serieRec And Trim(RegistosRM(i).ReciboNumDoc) = NumDoc And RegistosRM(i).ReciboCodFacturavel = GCodAnaActMed Then
                        Flg_ExisteActoMed = True
                    End If
                End If
            Next i

            If Flg_ExisteActoMed = False Then
                AdicionaReciboManual 1, codEfr, NumDoc, serieRec, GCodAnaActMed, 1, "", GCodAnaActMed, False
                FGRecibos_DblClick
            End If
        End If
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "VerificaUtentePagaActoMedico: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaUtentePagaActoMedico"
    Exit Sub
    Resume Next
End Sub


Private Function VerificaObrigaNrBenef(codEfr As String) As Boolean
    On Error GoTo TrataErro
    Dim RsAM As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim Flg_ExisteActoMed As Boolean

    Set RsAM = New ADODB.recordset
    RsAM.CursorLocation = adUseServer
    RsAM.CursorType = adOpenStatic
    sql = "select FLG_OBRIGA_NR_BENEF from sl_efr where cod_efr = " & Trim(codEfr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsAM.Open sql, gConexao
    If RsAM.RecordCount > 0 Then
        If BL_HandleNull(RsAM!FLG_OBRIGA_NR_BENEF, 0) = 1 Then
            VerificaObrigaNrBenef = True
        Else
            VerificaObrigaNrBenef = False
        End If
    Else
        VerificaObrigaNrBenef = False
    End If
    RsAM.Close
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaObrigaNrBenef: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaObrigaNrBenef"
    VerificaObrigaNrBenef = False
    Exit Function
    Resume Next
End Function

Function VerificaCodFacturavel(CodFacturavel As String) As Boolean
    On Error GoTo TrataErro
    Dim RsVerf As ADODB.recordset
    Dim sql As String
    Set RsVerf = New ADODB.recordset
    RsVerf.CursorLocation = adUseServer
    RsVerf.CursorType = adOpenStatic
    sql = "select * from sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(CodFacturavel)
    sql = sql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
    sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsVerf.Open sql, gConexao
    If RsVerf.RecordCount = 0 Then
        VerificaCodFacturavel = False
    Else
        VerificaCodFacturavel = True
    End If
    RsVerf.Close
    Set RsVerf = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaCodFacturavel: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaCodFacturavel"
    VerificaCodFacturavel = False
    Exit Function
    Resume Next
End Function


' -------------------------------------------------------------------

' VERIFICA SE CODIGO � CODIGO PAI DE ALGUMA ANALISE DO RECIBO MANUAL

' -------------------------------------------------------------------
Private Sub VerificaEFRCodigoFilho(CodPai As String, cod_efr As String, descr_efr As String, linhaAna As Long)
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodigoPai = CodPai Then
            AlteraEntidadeReciboManual i, cod_efr, descr_efr, CodPai, linhaAna
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "erro ao verificar codigos pai: " & Err.Number & " - " & Err.Description, Me.Name, "VerificacodigoPai"
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------

' VERIFICA SE CODIGO � CODIGO PAI DE ALGUMA ANALISE DO RECIBO MANUAL

' -------------------------------------------------------------------
Private Sub EliminaCodigoFilho(CodPai As String)
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodigoPai = CodPai Then
            EliminaReciboManual i, RegistosRM(i).ReciboCodFacturavel, True

            LimpaColeccao RegistosRM, i
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "erro ao Eliminar codigos filhos: " & Err.Number & " - " & Err.Description, Me.Name, "EliminaCodigoFilho"
    Exit Sub
    Resume Next
End Sub


Sub ActualizaFlgAparTrans()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim RsTrans As ADODB.recordset
    Dim sql As String

    For i = 1 To UBound(MaReq)
        Set RsTrans = New ADODB.recordset
        RsTrans.CursorLocation = adUseServer
        RsTrans.CursorType = adOpenStatic
        sql = "SELECT flg_apar_trans,n_folha_trab FROM sl_marcacoes WHERE n_req = " & EcNumReq.text & " and cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & " And cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & " and cod_ana_s= " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsTrans.Open sql, gConexao
        If Not RsTrans.EOF Then
            MaReq(i).Flg_Apar_Transf = BL_HandleNull(RsTrans!flg_apar_trans, 0)
            MaReq(i).N_Folha_Trab = BL_HandleNull(RsTrans!N_Folha_Trab, 0)
        Else
            MaReq(i).Flg_Apar_Transf = 0
            MaReq(i).N_Folha_Trab = 0
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "ActualizaFlgAparTrans: " & Err.Number & " - " & Err.Description, Me.Name, "ActualizaFlgAparTrans"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------------------------------------

' RETORNA QUANTIDADE DE MAPEADA PARA FACTURACAO

' -----------------------------------------------------------------------------------------------

Public Function RetornaQtd(codAna As String, cod_efr As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
        sSql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr = " & cod_efr
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            RetornaQtd = BL_HandleNull(rsAna!qtd, "")
        Else
            rsAna.Close
            sSql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr IS NULL "
            sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                RetornaQtd = BL_HandleNull(rsAna!qtd, "")
            End If
        End If
        rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "RetornaQtd"
    RetornaQtd = ""
    Exit Function
    Resume Next
End Function


Private Sub FgTubos_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    FgTubos_DevolveColunaActual X
    FgTubos_DevolveLinhaActual Y
    If TuboLinhaActual > 0 Then
        If TuboLinhaActual <= gTotalTubos Then
            FGTubos.ToolTipText = gEstruturaTubos(TuboLinhaActual).normaTubo
        End If
    Else
        FGTubos.ToolTipText = ""
    End If
End Sub



' ----------------------------------------------------------------------

' RETORNA A COLUNA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveColunaActual(X As Single)
    Dim coluna As Integer
    For coluna = 0 To FGTubos.Cols - 1
        If FGTubos.ColPos(coluna) <= X And FGTubos.ColPos(coluna) + FGTubos.ColWidth(coluna) >= X Then
            TuboColunaActual = coluna
            Exit Sub
        End If
    Next
    TuboColunaActual = -1
End Sub


' ----------------------------------------------------------------------

' RETORNA A LINHA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveLinhaActual(Y As Single)
    Dim linha As Integer
        For linha = 0 To FGTubos.rows - 1
            If FGTubos.RowPos(linha) <= Y And FGTubos.RowPos(linha) + FGTubos.RowHeight(linha) >= Y Then
                TuboLinhaActual = linha
                Exit Sub
            End If
        Next
        TuboLinhaActual = -1
End Sub

' ----------------------------------------------------------------------

' PARA CADA ANALISE MARCADA NO TOP, REGISTA-A!

' ----------------------------------------------------------------------
Public Sub PreencheTopAna()
    Dim CamposRetorno As New ClassPesqResultados
    Dim cancelou As Boolean
    Dim total As Integer
    Dim Resultados(100) As Variant
    Dim i As Integer
    On Error GoTo TrataErro

    CamposRetorno.InicializaResultados 1
    FormMarcAnaTop.Inicializa CamposRetorno
    FormMarcAnaTop.Show vbModal
    CamposRetorno.RetornaResultados Resultados(), cancelou, total
    For i = 1 To total
        EcAuxAna.Enabled = True
        EcAuxAna = Resultados(i)
        EcAuxAna_KeyDown 13, 0
    Next i

Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheTopAna: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheTopAna"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' VERIFICA SE ENTIDADE TEM VALOR FIXO PARA FINS DE SEMANA

' ----------------------------------------------------------------------
Private Function VerificaFdsValFixo(indice As Long) As Long
    Dim i As Long
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    On Error GoTo TrataErro

    For i = 1 To RegistosR.Count
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade And RegistosRM(indice).ReciboNumDoc = RegistosR(i).NumDoc And _
           RegistosRM(indice).ReciboSerieDoc = RegistosR(i).SerieDoc Then
           VerificaFdsValFixo = RegistosR(i).flg_FdsFixo
           Exit Function
        End If
    Next

    sSql = "SELECT * FROM sl_efr where cod_efr = " & RegistosRM(indice).ReciboEntidade
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        VerificaFdsValFixo = BL_HandleNull(RsEFR!flg_fds_fixo, 0)
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaFdsValFixo: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaFdsValFixo"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' PREENCHE ALGUNS DADOS DA ENTIDADE COM APENAS UM ACESSO � BD

' ----------------------------------------------------------------------
Private Sub PreencheDadosEntidade(ByRef RegistosR As Collection, indice As Long)
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    On Error GoTo TrataErro

    sSql = "SELECT * FROM sl_efr where cod_efr = " & RegistosR(indice).codEntidade
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        RegistosR(indice).DescrEntidade = BL_HandleNull(RsEFR!descr_efr, "")
        RegistosR(indice).codEmpresa = BL_HandleNull(RsEFR!cod_empresa, "")
        RegistosR(indice).flg_FdsFixo = BL_HandleNull(RsEFR!flg_fds_fixo, 0)
        RegistosR(indice).NumCopias = BL_HandleNull(RsEFR!num_copias_rec, 0)
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosEntidade: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosEntidade"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' RECALCULA VALOR DO RECIBO

' ----------------------------------------------------------------------
Public Sub RecalculaRecibo(indice As Long)
    On Error GoTo TrataErro
    Dim i As Long
    Dim taxa As Double
    If RegistosR(indice).Estado = gEstadoReciboNaoEmitido Or RegistosR(indice).Estado = gEstadoReciboNulo Or _
        RegistosR(indice).Estado = gEstadoReciboPerdido Then
        taxa = 0
        For i = 1 To RegistosRM.Count
            If RegistosRM(i).ReciboEntidade = RegistosR(indice).codEntidade And RegistosRM(i).ReciboNumDoc = RegistosR(indice).NumDoc And RegistosRM(i).ReciboSerieDoc = RegistosR(indice).SerieDoc Then
                taxa = taxa + RegistosRM(i).ReciboTaxa
            End If
        Next
        RegistosR(indice).ValorPagar = taxa
        RegistosR(indice).ValorOriginal = taxa
        FGRecibos.TextMatrix(indice, lColRecibosValorPagar) = taxa
        If RegistosR(indice).ValorPagar = 0 Then
            RegistosR(indice).Estado = gEstadoReciboNulo
            FGRecibos.TextMatrix(indice, lColRecibosEstado) = "Recibo Nulo"
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "RecalculaRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "RecalculaRecibo"
    Exit Sub
    Resume Next
End Sub


Private Sub RegistaAlteracoesRequis(num_requis As String)
    Dim i As Integer
    Dim ValorControloNovo As String
    Dim seq_alteracao As Integer
    seq_alteracao = BL_ContaAltRequis(num_requis) + 1

    For i = 0 To NumCampos - 1
        ValorControloNovo = ""
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf CamposEc(i) Is TextBox Or _
           TypeOf CamposEc(i) Is Label Or _
           TypeOf CamposEc(i) Is MaskEdBox Or _
           TypeOf CamposEc(i) Is RichTextBox Then

            ValorControloNovo = CamposEc(i)
        'DataCombo
        ElseIf TypeOf CamposEc(i) Is DataCombo Then
            ValorControloNovo = BG_DataComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'ComboBox
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            ValorControloNovo = BG_DaComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'CheckBox
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            If CamposEc(i).value = 0 Then
                ValorControloNovo = "0"
            ElseIf CamposEc(i).value = 1 Then
                ValorControloNovo = "1"
            Else
                ValorControloNovo = ""
            End If
        Else
        End If

    Next
End Sub
Private Function VerificaAnaliseJaExisteRealiza(n_req As Long, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String, flg_apaga As Boolean) As Long
    Dim sSql As String
    Dim rsReal As New ADODB.recordset
    Dim RsRes As New ADODB.recordset
    On Error GoTo TrataErro

    VerificaAnaliseJaExisteRealiza = 0

    sSql = "SELECT seq_realiza FROM sl_realiza WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " UNION SELECT seq_realiza FROM sl_realiza_h WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    rsReal.CursorLocation = adUseServer
    rsReal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then
        BG_LogFile_Erros sSql
    End If
    rsReal.Open sSql, gConexao
    If rsReal.RecordCount > 0 Then
        VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        sSql = "SELECT seq_Realiza FROM sl_res_alfan where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_frase where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_micro where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_alfan_h where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_frase_h where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_micro_h where seq_realiza = " & rsReal!seq_realiza
        RsRes.CursorLocation = adUseServer
        RsRes.CursorType = adOpenStatic
        If gModoDebug = mediSim Then
            BG_LogFile_Erros sSql
        End If
        RsRes.Open sSql, gConexao
        If RsRes.RecordCount > 0 Or cod_ana_s = "S99999" Then
            VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        Else
            If flg_apaga = True Then
                sSql = "DELETE FROM sl_realiza WHERE seq_realiza = " & rsReal!seq_realiza
                BG_ExecutaQuery_ADO sSql
            End If
            VerificaAnaliseJaExisteRealiza = 0
        End If
        RsRes.Close
        Set RsRes = Nothing
    Else
        VerificaAnaliseJaExisteRealiza = 0
    End If
    rsReal.Close
    Set rsReal = Nothing

Exit Function
TrataErro:
    VerificaAnaliseJaExisteRealiza = 0
    BG_LogFile_Erros "Erro  ao VerificaAnaliseJaExisteRealiza: " & Err.Description, Me.Name, "VerificaAnaliseJaExisteRealiza", True
    Exit Function
    Resume Next
End Function


Public Sub PreencheFgRecibos()
    Dim i As Long
    Dim j As Integer
    Dim cor  As Long
    Dim txtEstado As String

    ExecutaCodigoR = False
    j = FGRecibos.rows - 1
    While j > 0
        If j > 1 Then
            FGRecibos.RemoveItem j
        Else
            FGRecibos.TextMatrix(j, 0) = ""
            FGRecibos.TextMatrix(j, 1) = ""
            FGRecibos.TextMatrix(j, 2) = ""
            FGRecibos.TextMatrix(j, 3) = ""
            FGRecibos.TextMatrix(j, 4) = ""
            FGRecibos.TextMatrix(j, 5) = ""
            FGRecibos.TextMatrix(j, 6) = ""
            FGRecibos.TextMatrix(j, 7) = ""
            FGRecibos.TextMatrix(j, 8) = ""
            FGRecibos.TextMatrix(j, 9) = ""
            BL_MudaCorFg FGRecibos, CLng(j), vbWhite
        End If
        j = j - 1
    Wend

    ExecutaCodigoR = True
    For i = 1 To RegistosR.Count - 1
        If RegistosR(i).Estado = gEstadoReciboAnulado Then
            cor = vermelho
            txtEstado = "Anulado"
        ElseIf RegistosR(i).Estado = gEstadoReciboPago Then
            cor = Verde
            txtEstado = "Pago"
        ElseIf RegistosR(i).Estado = gEstadoReciboCobranca Then
            cor = Amarelo
            txtEstado = "Cobran�a"
        ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
            cor = vbWhite
            txtEstado = "An�lises Perdidas"
        ElseIf RegistosR(i).Estado = gEstadoReciboNulo Then
            cor = vbWhite
            txtEstado = "Recibo Nulo"
        Else
            cor = vbWhite
            txtEstado = "N�o Emitido"
        End If
        If RegistosR(i).NumDoc <> "" Then
        ' -----------------------------------------------------------------------------------------------
            ' PREENCHCE A FLEXGRID
            ' -----------------------------------------------------------------------------------------------
            If BL_HandleNull(RegistosR(i).NumDoc, "0") <> "0" Then
                FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).SerieDoc & "/" & RegistosR(i).NumDoc
            Else
                FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).NumDoc
                RegistosR(i).CodFormaPag = gModoPagNaoPago
            End If
            FGRecibos.TextMatrix(i, lColRecibosEntidade) = RegistosR(i).DescrEntidade
            If RegistosR(i).SeqUtente <> -1 Then
                FGRecibos.TextMatrix(i, lColRecibosEntidade) = FGRecibos.TextMatrix(i, lColRecibosEntidade) & " (" & RegistosR(i).SeqUtente & ")"
            End If
            FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
            FGRecibos.TextMatrix(i, lColRecibosPercentagem) = Round(CDbl(100 - RegistosR(i).Desconto), 3)
            FGRecibos.TextMatrix(i, lColRecibosDtEmissao) = RegistosR(i).DtEmi & " " & RegistosR(i).HrEmi
            FGRecibos.TextMatrix(i, lColRecibosDtPagamento) = RegistosR(i).DtPagamento & " " & RegistosR(i).HrPagamento
            FGRecibos.TextMatrix(i, lColRecibosEstado) = txtEstado
            If RegistosR(i).CodFormaPag = gModoPagNaoPago Then
                FGRecibos.TextMatrix(i, lColRecibosModoPag) = ""
            Else
                FGRecibos.TextMatrix(i, lColRecibosModoPag) = BL_SelCodigo("sl_forma_pag", "DESCR_FORMA_PAG", "COD_FORMA_PAG", RegistosR(i).CodFormaPag)
            End If
            If RegistosR(i).FlgBorla = mediSim Then
                FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
            Else
                FGRecibos.TextMatrix(i, lColRecibosBorla) = "N�o"
            End If
            If RegistosR(i).FlgNaoConforme = mediSim Then
                FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "N�o"
            Else
                FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "Sim"
            End If
            BL_MudaCorFg FGRecibos, i, cor
            FGRecibos.AddItem ""
        End If
    Next

End Sub
Public Sub PreencheFgAdiantamentos()
    Dim i As Long
    Dim j As Integer
    Dim cor  As Long
    Dim txtEstado As String

    j = FgAdiantamentos.rows - 1
    While j > 0
        If j > 1 Then
            FgAdiantamentos.RemoveItem j
        Else
            For i = 0 To FgAdiantamentos.Cols - 1
                FgAdiantamentos.TextMatrix(j, i) = ""
            Next
            BL_MudaCorFg FgAdiantamentos, CLng(j), vbWhite
        End If
        j = j - 1
    Wend

    For i = 1 To RegistosAD.Count - 1
        If RegistosAD(i).Estado = gEstadoReciboAnulado Then
            cor = vermelho
            txtEstado = "Anulado"
        ElseIf RegistosAD(i).Estado = gEstadoReciboPago Then
            cor = Verde
            txtEstado = "Pago"
        ElseIf RegistosAD(i).Estado = gEstadoReciboCobranca Then
            cor = Amarelo
            txtEstado = "Cobran�a"
        ElseIf RegistosAD(i).Estado = gEstadoReciboPerdido Then
            cor = vbWhite
            txtEstado = "An�lises Perdidas"
        ElseIf RegistosAD(i).Estado = gEstadoReciboNulo Then
            cor = vbWhite
            txtEstado = "Recibo Nulo"
        Else
            cor = vbWhite
            txtEstado = "N�o Emitido"
        End If
        ' -----------------------------------------------------------------------------------------------
        ' PREENCHCE A FLEXGRID
        ' -----------------------------------------------------------------------------------------------
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosNumDoc) = RegistosAD(i).NumAdiantamento
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosDtEmissao) = RegistosAD(i).DtEmi & " " & RegistosAD(i).HrEmi
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosDtAnulacao) = RegistosAD(i).DtAnul & " " & RegistosAD(i).HrAnul
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosEstado) = txtEstado
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosValor) = RegistosAD(i).valor
        BL_MudaCorFg FgAdiantamentos, i, cor
        FgAdiantamentos.AddItem ""
    Next

End Sub

Private Sub GereFrameAdiantamentos(flg_Aberto As Boolean)
    If flg_Aberto = True Then
        FrameAdiantamentos.top = 360
        FrameAdiantamentos.Visible = True
    ElseIf flg_Aberto = False Then
        FrameAdiantamentos.Visible = False
    End If
End Sub


Private Sub VerificaDoenteHemodialise()
    Dim codAna As String
    Dim sSql As String
    Dim rsHemo As New ADODB.recordset
    Dim i As Integer
    Dim serieRec As String
    Dim numRec As String
    serieRec = "0"
    numRec = "0"
    If EcHemodialise <> "" Then

        sSql = "SELECT cod_ana FROM sl_cod_hemodialise WHERE cod_hemodialise = " & BL_TrataStringParaBD(EcHemodialise)
        rsHemo.CursorLocation = adUseServer
        rsHemo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsHemo.Open sSql, gConexao
        If rsHemo.RecordCount > 0 Then
            If BL_HandleNull(rsHemo!cod_ana, "") <> "" Then
                If VerificaCodAnaJaMarcadoRM(rsHemo!cod_ana) = False Then
                    For i = 1 To RegistosR.Count - 1
                        If RegistosR(i).Estado = gEstadoReciboNulo And RegistosR(i).codEntidade = EcCodEFR Then
                            numRec = RegistosR(i).NumDoc
                            serieRec = RegistosR(i).SerieDoc
                            Exit For
                        End If
                    Next
                    AdicionaReciboManual 1, RegistosR(i).codEntidade, numRec, serieRec, rsHemo!cod_ana, 1, "", rsHemo!cod_ana, False
                    FGRecibos_DblClick
                End If
            End If

        End If
        rsHemo.Close
        Set rsHemo = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao VerificaDoenteHemodialise: " & Err.Description, Me.Name, "VerificaDoenteHemodialise", False
    Exit Sub
    Resume Next
End Sub

Private Function VerificaCodAnaJaMarcadoRM(cod_ana As String) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = cod_ana Then
            VerificaCodAnaJaMarcadoRM = True
            Exit Function
        End If
    Next
    VerificaCodAnaJaMarcadoRM = False
Exit Function
TrataErro:
    VerificaCodAnaJaMarcadoRM = False
    BG_LogFile_Erros "Erro  ao VerificaCodAnaJaMarcadoRM: " & Err.Description, Me.Name, "VerificaCodAnaJaMarcadoRM", False
    Exit Function
    Resume Next
End Function

Private Function VerificaEfrHemodialise(cod_efr As String) As Boolean
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    On Error GoTo TrataErro
    VerificaEfrHemodialise = False
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    sSql = "select flg_hemodialise from sl_efr where cod_efr = " & Trim(cod_efr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        If BL_HandleNull(RsEFR!flg_hemodialise, 0) = 1 Then
            VerificaEfrHemodialise = True
        End If
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    VerificaEfrHemodialise = False
    BG_LogFile_Erros "Erro  ao VerificaEfrHemodialise: " & Err.Description, Me.Name, "VerificaEfrHemodialise", False
    Exit Function
    Resume Next
End Function

Private Sub InicializaFrameAnaRecibos()
    EcCodAnaAcresc.left = FgAnaRec.left
    EcCodAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecCodAna)
    EcDescrAnaAcresc.left = EcCodAnaAcresc.left + EcCodAnaAcresc.Width
    EcDescrAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecDescrAna)
    EcQtdAnaAcresc.left = EcDescrAnaAcresc.left + EcDescrAnaAcresc.Width
    EcQtdAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecQtd)
    EcTaxaAnaAcresc.left = EcQtdAnaAcresc.left + EcQtdAnaAcresc.Width
    EcTaxaAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecTaxa)
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Or RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or _
       RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
            BtAnaAcresc.Enabled = False
    Else
            BtAnaAcresc.Enabled = True
    End If
End Sub

Private Function RetornaPrecoAnaRec(cod_ana As String, cod_efr As String, qtd As Integer) As String
    Dim taxa As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim mens As String
    On Error GoTo TrataErro

    taxa = IF_RetornaTaxaAnalise(cod_ana, cod_efr, CbTipoUtente, EcUtente, EcDtColheita, codAnaEfr, descrAnaEfr, False)
    taxa = Replace(taxa, ".", ",")
    If Mid(taxa, 1, 1) = "," Then
        taxa = "0" & taxa
    End If

    Select Case taxa
        Case "-1"
            mens = " An�lise n�o est� mapeada para o FACTUS!"
            taxa = 0
        Case "-2"
            mens = "Impossivel abrir conex�o com o FACTUS!"
            taxa = 0
        Case "-3"
            mens = "Impossivel seleccionar portaria activa!"
            taxa = 0
        Case "-4"
            mens = "An�lise n�o comparticipada. Pre�o PARTICULAR!"

            taxa = 0
        Case "-5"
            mens = "N�o existe tabela codificada para a entidade em causa!"
            taxa = 0
        Case Else
            mens = ""
    End Select

    If mens <> "" Then
        taxa = "0,0"
    End If
    taxa = BG_CvDecimalParaCalculo(taxa) * qtd
    RetornaPrecoAnaRec = taxa
Exit Function
TrataErro:
    RetornaPrecoAnaRec = "0"
    BG_LogFile_Erros "Erro  ao RetornaPrecoAnaRec: " & Err.Description, Me.Name, "RetornaPrecoAnaRec", False
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------

' PREENCHE AUTOMATICAMENT A SALA AO INTRODUZIR UM NUMERO REQ PRE IMPRESSO

' -------------------------------------------------------------------------
Private Sub PreencheSalaAuto(n_req As String)
    On Error GoTo TrataErro
    If n_req = "" Then Exit Sub
    Dim sSql As String
    Dim rsSalas As New ADODB.recordset
    sSql = "SELECT cod_sala FROM sl_etiq_salas WHERE n_req = " & n_req
    rsSalas.CursorLocation = adUseServer
    rsSalas.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSalas.Open sSql, gConexao
    If rsSalas.RecordCount = 1 And EcCodSala = "" And BL_HandleNull(rsSalas!cod_sala, "") <> "" Then
        EcCodSala = BL_HandleNull(rsSalas!cod_sala, "")
        EcCodsala_Validate False
    End If
    rsSalas.Close
    Set rsSalas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PreencheSalaAuto: " & Err.Description, Me.Name, "PreencheSalaAuto", False
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------------

' COLOCA UM RECIBO EM COBRANCA/PAGO

' -------------------------------------------------------------------------

Private Sub ColocaCobrancaPago(linha As Integer, serieRec As String, nRec As String, tipo As Integer)
    Dim sSql As String
    On Error GoTo TrataErro
    If tipo = 0 Then ' COLOCAR EM PAGAMENTO
        BG_BeginTransaction
        sSql = "UPDATE sl_recibos set user_emi = " & BL_TrataStringParaBD(RegistosR(linha).UserEmi) & ","
        sSql = sSql & " dt_pagamento = " & BL_TrataDataParaBD(RegistosR(linha).DtPagamento) & ", "
        sSql = sSql & " hr_pagamento = " & BL_TrataStringParaBD(RegistosR(linha).HrPagamento) & ", "
        sSql = sSql & " estado ='P' WHERE serie_rec = " & BL_TrataStringParaBD(serieRec) & " AND n_rec = " & nRec

    ElseIf tipo = 1 Then
        sSql = "UPDATE sl_recibos set dt_pagamento = null, hr_pagamento = null, estado = 'C' WHERE serie_rec = " & BL_TrataStringParaBD(serieRec) & " AND n_rec = " & nRec
    End If
    BG_ExecutaQuery_ADO sSql
    BG_CommitTransaction
Exit Sub
TrataErro:
BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao ColocaCobrancapago: " & Err.Description, Me.Name, "ColocaCobrancapago", False
    Exit Sub
    Resume Next
End Sub


Private Sub FlgIncrementa(Ind As Long, qtd As Integer)
    Dim i As Integer
    Dim flg_FdsValFixo As Long
    On Error GoTo TrataErro

    For i = 1 To RegistosRM.Count
        If RegistosA(Ind).codAna = RegistosRM(i).ReciboCodAna Then
            Exit For
        End If
    Next
    EliminaReciboManual CLng(i), RegistosA(Ind).codAna, False
    RegistosRM(i).ReciboQuantidade = qtd
    RegistosRM(i).ReciboQuantidadeEFR = qtd
    RegistosRM(i).ReciboTaxa = Replace(RegistosRM(i).ReciboTaxaUnitario, ".", ",") * qtd

    'PARA FIM DE SEMANA DOBRA O PRE�O
    'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
    flg_FdsValFixo = VerificaFdsValFixo(Ind)
    If RegistosRM(i).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
        RegistosRM(i).ReciboTaxa = RegistosRM(i).ReciboTaxa * 2
    ElseIf RegistosRM(Ind).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
        RegistosRM(i).ReciboTaxa = "0"
    End If

    FGAna.TextMatrix(Ind, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
    AdicionaAnaliseAoReciboManual CLng(i), False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  aFlgIncrementa: " & Err.Description, Me.Name, "FlgIncrementa", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE GRELHA DE ANALISES COM INFORMACAO DO PRE�O ASSOCIADO

' -------------------------------------------------------------------------
Private Sub ActualizaLinhasRecibos(linha As Integer)
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro

    If linha > 0 Then

        FGAna.TextMatrix(linha, lColFgAnaP1) = ""
        FGAna.TextMatrix(linha, lColFgAnaMedico) = ""
        FGAna.TextMatrix(linha, lColFgAnaCodEFR) = ""
        FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = ""
        FGAna.TextMatrix(linha, lColFgAnaNrBenef) = ""
        FGAna.TextMatrix(linha, lColFgAnaPreco) = ""

        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna = RegistosA(linha).codAna Then
                If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                    If RegistosRM(i).ReciboCorP1 = "V" Then
                        FGAna.row = linha
                        FGAna.Col = 2
                        FGAna.CellBackColor = Verde
                        FGAna.Col = 0
                    Else
                        FGAna.row = linha
                        FGAna.Col = 2
                        FGAna.CellBackColor = vbWhite
                        FGAna.Col = 0
                    End If

                    FGAna.TextMatrix(linha, lColFgAnaP1) = RegistosRM(i).ReciboP1
                    FGAna.TextMatrix(linha, lColFgAnaMedico) = RegistosRM(i).ReciboCodMedico
                    FGAna.TextMatrix(linha, lColFgAnaCodEFR) = RegistosRM(i).ReciboEntidade
                    FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(i).ReciboEntidade)
                    FGAna.TextMatrix(linha, lColFgAnaNrBenef) = RegistosRM(i).ReciboNrBenef
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                    If RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla Then
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = CorBorla
                    ElseIf RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Then
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = CorIsento
                    Else
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = vbWhite
                    End If
                    FGAna.Col = lColFgAnaCodigo
                    Exit For
                End If
            End If
        Next
    Else
        For i = 1 To RegistosA.Count - 1
            FGAna.TextMatrix(i, lColFgAnaP1) = ""
            FGAna.TextMatrix(i, lColFgAnaMedico) = ""
            FGAna.TextMatrix(i, lColFgAnaCodEFR) = ""
            FGAna.TextMatrix(i, lColFgAnaDescrEFR) = ""
            FGAna.TextMatrix(i, lColFgAnaNrBenef) = ""
            FGAna.TextMatrix(i, lColFgAnaPreco) = ""
        Next
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                For j = 1 To RegistosA.Count - 1
                    If RegistosRM(i).ReciboCodAna = RegistosA(j).codAna Then

                        If RegistosRM(i).ReciboCorP1 = "V" Then
                            FGAna.row = j
                            FGAna.Col = 2
                            FGAna.CellBackColor = Verde
                            FGAna.Col = 0
                        Else
                            FGAna.row = j
                            FGAna.Col = 2
                            FGAna.CellBackColor = vbWhite
                            FGAna.Col = 0
                        End If

                        FGAna.TextMatrix(j, lColFgAnaP1) = RegistosRM(i).ReciboP1
                        FGAna.TextMatrix(j, lColFgAnaMedico) = RegistosRM(i).ReciboCodMedico
                        FGAna.TextMatrix(j, lColFgAnaCodEFR) = RegistosRM(i).ReciboEntidade
                        FGAna.TextMatrix(j, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(i).ReciboEntidade)
                        FGAna.TextMatrix(j, lColFgAnaNrBenef) = RegistosRM(i).ReciboNrBenef
                        FGAna.TextMatrix(j, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                        If RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla Then
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = CorBorla
                        ElseIf RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Then
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = CorIsento
                        Else
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = vbWhite
                        End If
                        FGAna.Col = lColFgAnaCodigo
                        Exit For
                    End If
                Next
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ActualizarLinhasRecibos: " & Err.Description, Me.Name, "ActualizarLinhasRecibos", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' RETORNA O INDICE DA ESTRUTURA DE RECIBOS PARA ANALISE EM CAUSA

' -------------------------------------------------------------------------

Public Function DevIndice(codAna As String) As Long
    Dim i As Integer
    On Error GoTo TrataErro
    DevIndice = -1
    If codAna = "" Then Exit Function
    For i = 1 To RegistosRM.Count - 1
        If RegistosRM(i).ReciboCodFacturavel = codAna And BL_HandleNull(RegistosRM(i).flgCodBarras, 0) = 0 Then
            DevIndice = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevIndice = -1
    BG_LogFile_Erros "Erro  DevIndice: " & Err.Description, Me.Name, "DevIndice", False
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------

' ADICIONA ANALISE � ESTRUTURA DE ANALISES

' -------------------------------------------------------------------------

Public Sub AdicionaAnalise(ByVal analise As String, codBarras As Boolean, ultimaLinha As Boolean, multiAna As String)
    On Error GoTo TrataErro
    Dim AnaFacturavel As String
    Dim cod_Agrup_aux As String
    Dim d As String
    DoEvents
    If ultimaLinha = True Then
        FGAna.row = FGAna.rows - 1
        LastRowA = FGAna.row
    End If
    AnaFacturavel = ""
    analise = BL_RetornaCodigoMarcar(analise, AnaFacturavel)
    DoEvents

    If Verifica_Ana_ja_Existe(LastRowA, analise) = False Then
        If Trim(UCase(analise)) <> Trim(UCase(RegistosA(LastRowA).codAna)) Then
            DoEvents
            If Insere_Nova_Analise(LastRowA, analise, AnaFacturavel, codBarras, multiAna) = True Then
                cod_Agrup_aux = EcAuxAna
                d = SELECT_Descr_Ana(cod_Agrup_aux)
                If LastRowA < FGAna.rows And codBarras = False Then
                    VerificaAnaliseJaMarcada EcNumReq, cod_Agrup_aux
                End If
                'o estado deixa de estar "Sem Analises"
                If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
                    EcEstadoReq = gEstadoReqEsperaProduto
                End If
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)

                If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                    'Cria linha vazia
                    FGAna.AddItem ""
                    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                End If


            End If
        End If
    Else
        Beep

        BG_Mensagem mediMsgStatus, "An�lise j� indicada!"
    End If
    FGAna.Col = 0
    Flg_IncrementaP1 = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaAnalise: " & Err.Description, Me.Name, "AdicionaAnalise", False
    Exit Sub
    Resume Next

End Sub



' -------------------------------------------------------------------------

' AP�S ANALISES TEREM SIDO APAGAAS, SE RECIBO CANCELADO, RETIRA ESSAS ANALISES

' -------------------------------------------------------------------------
Public Sub RetiraAnalisesApagadas()
    Dim i As Integer
    Dim j As Long
    Dim flg_apaga As Boolean
    On Error GoTo TrataErro
    For j = 1 To RegistosRM.Count - 1
        flg_apaga = True
        If RegistosRM(j).ReciboFlgReciboManual <> "1" Then
            For i = 1 To RegistosA.Count - 1
                If RegistosA(i).codAna = RegistosRM(j).ReciboCodAna Then
                    flg_apaga = False
                    Exit For
                End If
            Next
        Else
            flg_apaga = False
        End If
        If flg_apaga = True Then
            If EliminaReciboManual(j, RegistosRM(j).ReciboCodAna, True) = True Then
                LimpaColeccao RegistosRM, j
                j = j - 1
            End If

        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RetiraAnalisesApagadas: " & Err.Description, Me.Name, "RetiraAnalisesApagadas", False
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------------------------------

' VERIFICA SE JA EXISTE REGISTO PARA DETERMINADO PERFIL DE MARCACAO NA ESTRUTURA DE RECIBOS

' -------------------------------------------------------------------------------------------
Private Function VerificaPerfilMarcacaoJaMarcado(CodPerfil As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = CodPerfil And RegistosRM(i).ReciboCodAna = CodPerfil Then
            VerificaPerfilMarcacaoJaMarcado = True
            Exit Function
        End If
    Next
    VerificaPerfilMarcacaoJaMarcado = False

Exit Function
TrataErro:
    VerificaPerfilMarcacaoJaMarcado = False
    BG_LogFile_Erros "Erro  VerificaPerfilMarcacaoJaMarcado: " & Err.Description, Me.Name, "VerificaPerfilMarcacaoJaMarcado", False
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------

' REGISTA NA BASE DADOSTUBOS ELIMINADOS

' ---------------------------------------------------------------------------------------------------
Private Sub RegistaTubosEliminados()
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To TotalTubosEliminados
        TB_ApagaTuboBD BL_HandleNull(tubosEliminados(i).n_req, EcNumReq), tubosEliminados(i).seq_req_tubo
    Next
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RegistaTubosEliminados: " & Err.Description, Me.Name, "RegistaTubosEliminados", False
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------

' ACRESCENTA A ESTRUTURA OS TUBOS ELIMINADOS.

' ---------------------------------------------------------------------------------------------------
Private Sub AcrescentaTubosEliminados(n_req As String, seq_req_tubo As Long)
    On Error GoTo TrataErro
    TotalTubosEliminados = TotalTubosEliminados + 1
    ReDim Preserve tubosEliminados(TotalTubosEliminados)
    tubosEliminados(TotalTubosEliminados).n_req = n_req
    tubosEliminados(TotalTubosEliminados).seq_req_tubo = seq_req_tubo
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AcrescentaTubosEliminados: " & Err.Description, Me.Name, "AcrescentaTubosEliminados", False
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------

' PREENCHE CAMPOS ESPECIFICOS DE AMBIENTE AGUAS

' ---------------------------------------------------------------------------------------------------
Private Sub FuncaoProgurarAguas(n_req)
    Dim sSql As String
    Dim rsAguas As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM Sl_requis_aguas WHERE n_req = " & n_req
    rsAguas.CursorLocation = adUseServer
    rsAguas.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAguas.Open sSql, gConexao
    If rsAguas.RecordCount = 1 Then
        EcCodtipoAmostra.text = BL_HandleNull(rsAguas!cod_tipo_amostra, "")
        EcCodOrigemAmostra.text = BL_HandleNull(rsAguas!cod_origem_amostra, "")
        EcCodPontoColheita.text = BL_HandleNull(rsAguas!cod_ponto_colheita, "")
        EcLocalColheita.text = BL_HandleNull(rsAguas!local_colheita, "")
        EcCodtipoAmostra_Validate False
        EcCodOrigemAmostra_Validate False
        EcCodPontoColheita_validate False
    End If
    rsAguas.Close
    Set rsAguas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  FuncaoProgurarAguas: " & Err.Description, Me.Name, "FuncaoProgurarAguas", False
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------

' INSERE REGISTO NA TABELA DE REQUISICOES AGUAS

' ---------------------------------------------------------------------------------------------------
Private Function BDInsertAguas() As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim i As Long
    sSql = "INSERT INTO sl_requis_aguas(n_req,  cod_tipo_amostra, cod_origem_amostra,local_colheita,cod_ponto_colheita) VALUES("
    sSql = sSql & EcNumReq.text & ","
    sSql = sSql & BL_HandleNull(EcCodtipoAmostra.text, "Null") & ","
    sSql = sSql & BL_HandleNull(EcCodOrigemAmostra.text, "Null") & ","
    sSql = sSql & BL_TrataStringParaBD(EcLocalColheita.text) & ","
    sSql = sSql & BL_HandleNull(EcCodPontoColheita.text, "Null") & ")"
    i = BG_ExecutaQuery_ADO(sSql)
    If i = 1 Then
        BDInsertAguas = True
    Else
        BDInsertAguas = False
    End If
Exit Function
TrataErro:
    BDInsertAguas = False
    BG_LogFile_Erros "Erro  BDInsertAguas: " & Err.Description, Me.Name, "BDInsertAguas", False
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------

' UPDATE REGISTO NA TABELA DE REQUISICOES AGUAS

' ---------------------------------------------------------------------------------------------------
Private Function BDUpdateAguas() As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim i As Long
    sSql = "UPDATE sl_requis_aguas SET "
    sSql = sSql & " cod_tipo_amostra = " & BL_HandleNull(EcCodtipoAmostra.text, "NULL") & ","
    sSql = sSql & " cod_origem_amostra = " & BL_HandleNull(EcCodOrigemAmostra.text, "NULL") & ","
    sSql = sSql & " local_colheita = " & BL_TrataStringParaBD(EcLocalColheita.text) & ","
    sSql = sSql & " cod_ponto_colheita = " & BL_HandleNull(EcCodPontoColheita.text, "NULL")
    sSql = sSql & " WHERE n_req = " & EcNumReq.text
    i = BG_ExecutaQuery_ADO(sSql)
    
    If i = 1 Then
        BDUpdateAguas = True
    
    ElseIf i = 0 Then
        BDUpdateAguas = BDInsertAguas
    Else
        BDUpdateAguas = False
    End If
Exit Function
TrataErro:
    BDUpdateAguas = False
    BG_LogFile_Erros "Erro  BDUpdateAguas: " & Err.Description, Me.Name, "BDUpdateAguas", False
    Exit Function
    Resume Next
End Function



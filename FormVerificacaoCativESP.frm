VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormVerificacaoCativESP 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Verifica��o de erros de cativa��o de credenciais"
   ClientHeight    =   9120
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   15345
   Icon            =   "FormVerificacaoCativESP.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9120
   ScaleWidth      =   15345
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PicSucesso 
      Height          =   285
      Left            =   6720
      Picture         =   "FormVerificacaoCativESP.frx":000C
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   15
      Top             =   8160
      Width           =   290
   End
   Begin VB.PictureBox PicInsucesso 
      Height          =   290
      Left            =   5880
      Picture         =   "FormVerificacaoCativESP.frx":03D6
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   14
      Top             =   8160
      Width           =   290
   End
   Begin VB.TextBox txtSNS 
      Height          =   315
      Left            =   10560
      TabIndex        =   12
      Top             =   240
      Width           =   1695
   End
   Begin VB.TextBox txtErros 
      Height          =   2655
      Left            =   240
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   10
      Top             =   4680
      Width           =   14895
   End
   Begin VB.Frame frCativ 
      Height          =   3615
      Left            =   240
      TabIndex        =   8
      Top             =   840
      Width           =   14895
      Begin MSFlexGridLib.MSFlexGrid fgCativ 
         Height          =   3015
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   14535
         _ExtentX        =   25638
         _ExtentY        =   5318
         _Version        =   393216
      End
   End
   Begin VB.ComboBox cbEstado 
      Height          =   315
      Left            =   13200
      TabIndex        =   7
      Top             =   240
      Width           =   1935
   End
   Begin VB.TextBox txtUtente 
      Height          =   315
      Left            =   8350
      TabIndex        =   5
      Top             =   240
      Width           =   1455
   End
   Begin VB.ComboBox cbUtente 
      Height          =   315
      Left            =   7070
      TabIndex        =   4
      Top             =   240
      Width           =   1095
   End
   Begin VB.TextBox txtCredencial 
      Height          =   315
      Left            =   3720
      TabIndex        =   2
      Top             =   240
      Width           =   2415
   End
   Begin VB.TextBox txtData 
      Height          =   315
      Left            =   840
      TabIndex        =   0
      Top             =   240
      Width           =   1695
   End
   Begin VB.Label lbData 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Data"
      Height          =   195
      Left            =   350
      TabIndex        =   13
      Top             =   240
      Width           =   345
   End
   Begin VB.Label lbSNS 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SNS"
      Height          =   195
      Left            =   10080
      TabIndex        =   11
      Top             =   240
      Width           =   330
   End
   Begin VB.Label lbEstado 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Estado"
      Height          =   195
      Left            =   12600
      TabIndex        =   6
      Top             =   240
      Width           =   495
   End
   Begin VB.Label lbUtente 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Utente"
      Height          =   195
      Left            =   6450
      TabIndex        =   3
      Top             =   240
      Width           =   480
   End
   Begin VB.Label lbCredencial 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Credencial"
      Height          =   195
      Left            =   2840
      TabIndex        =   1
      Top             =   240
      Width           =   750
   End
End
Attribute VB_Name = "FormVerificacaoCativESP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim LinhaFgRes As Integer
'Inicializar vari�veis
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim pin As String
Dim nome As String
'''''

Sub Form_Activate()

    EventoActivate

End Sub
Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = FormVerificacaoCativESP
    
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Load()
 EventoLoad
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cr�..."
    DefTipoCampos
    Inicializacoes
    Set CampoActivo = Me.ActiveControl

    'BG_ParametrizaPermissoes_ADO Me.Name
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    BtCancelarClick = False
End Sub

Sub Inicializacoes()

    Me.caption = "Verifica��o de credenciais"
    
    Me.left = 400
    Me.top = 400
    Me.Width = 15435
    Me.Height = 8295 ' Normal
    
    LinhaFgRes = 1
    'lbErros = ""
    
    If gPassaParams.id = "FormCativarESP" Then
       txtData.Text = Bg_DaData_ADO
       txtCredencial.Text = gPassaParams.Param(0)
       cbUtente.Text = gPassaParams.Param(2)
       txtUtente.Text = gPassaParams.Param(5)
       cbEstado.Text = getStrEstado(gPassaParams.Param(3))
       txtErros.Text = gPassaParams.Param(4)
       pin = gPassaParams.Param(1)
       nome = gPassaParams.Param(6)
       txtSNS.Text = gPassaParams.Param(7)
       txtCredencial.Enabled = False
       cbUtente.Enabled = False
       txtUtente.Enabled = False
       cbEstado.Enabled = False
       txtData.Enabled = False
       txtSNS.Enabled = False
       PreencheFlexGrid txtData.Text, pin, txtCredencial.Text, cbUtente.Text & "-" & txtUtente.Text, cbEstado.Text, nome, txtSNS.Text
    End If
End Sub
Sub Form_Unload(Cancel As Integer)

    EventoUnload
    
End Sub


Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = FormCativarESP
    FormGestaoRequisicaoPrivado.Enabled = True
    
    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    Set FormVerificacaoCativESP = Nothing
    
  
End Sub

Sub DefTipoCampos()
  On Error GoTo TrataErro
  
  With fgCativ
        .rows = 2
        .FixedRows = 1

        .Cols = 7
        .FixedCols = 0
        
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        .row = 0
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Data"
        .ColWidth(0) = 1500
        .Col = 1
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "PIN Agendamento"
        .ColWidth(1) = 2000
        .Col = 2
        .ColAlignment(2) = flexAlignCenterBottom
        .TextMatrix(0, 2) = "Credencial"
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(2) = 2600
        .Col = 3
        .ColAlignment(3) = flexAlignCenterBottom
        .TextMatrix(0, 3) = "Utente"
        .ColAlignment(3) = flexAlignLeftCenter
        .ColWidth(3) = 1200
        .Col = 4
        .ColAlignment(4) = flexAlignCenterBottom
        .TextMatrix(0, 4) = "Nome"
        .ColAlignment(4) = flexAlignLeftCenter
        .ColWidth(4) = 4100
        .Col = 5
        .ColAlignment(5) = flexAlignCenterBottom
        .TextMatrix(0, 5) = "SNS"
        .ColAlignment(5) = flexAlignLeftCenter
        .ColWidth(5) = 2000
        .Col = 6
        .ColAlignment(6) = flexAlignCenterBottom
        .TextMatrix(0, 6) = "Estado"
        .ColAlignment(6) = flexAlignLeftCenter
        .ColWidth(6) = 1000
    '.WordWrap = False
        .row = 1
        
    End With
    
TrataErro:
    BG_LogFile_Erros "DefTipoCampos: " & Err.Number & " - " & Err.Description, Me.Name, "DefTipoCampos"
    Exit Sub
    Resume Next
End Sub

Sub PreencheFlexGrid(ByRef data As String, ByRef pin As String, ByRef credencial As String, ByRef Utente As String, ByRef estado As String, ByRef nome As String, _
                    ByRef SNS As String)
        
    fgCativ.Col = 0

    LinhaFgRes = LinhaFgRes + 1
    fgCativ.AddItem ""
    fgCativ.TextMatrix(LinhaFgRes - 1, 0) = data
    fgCativ.TextMatrix(LinhaFgRes - 1, 1) = pin
    fgCativ.TextMatrix(LinhaFgRes - 1, 2) = credencial
    fgCativ.TextMatrix(LinhaFgRes - 1, 3) = Utente
    fgCativ.TextMatrix(LinhaFgRes - 1, 4) = nome
    fgCativ.TextMatrix(LinhaFgRes - 1, 5) = SNS
        
    fgCativ.Col = 6
    fgCativ.row = LinhaFgRes - 1
    
    If estado = "ERRO" Then
       Set fgCativ.CellPicture = PicInsucesso.Image
    ElseIf estado = "CATIVADO" Then
       Set fgCativ.CellPicture = PicSucesso.Image
    End If
        
End Sub

Function getStrEstado(ByVal estado As String) As String

  On Error GoTo TrataErro
     
  If estado = cSUCESSO Then
     getStrEstado = "CATIVADO"
  ElseIf estado = cINSUCESSO Then
     getStrEstado = "ERRO"
  End If
  
  Exit Function
 
TrataErro:
    getStrEstado = ""
    BG_LogFile_Erros "getStrEstado: " & Err.Number & " - " & Err.Description, Me.Name, "getStrEstado"
    Exit Function
    Resume Next
End Function

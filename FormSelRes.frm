VERSION 5.00
Begin VB.Form FormSelRes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormSelRes"
   ClientHeight    =   10215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7335
   Icon            =   "FormSelRes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10215
   ScaleWidth      =   7335
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcEstadoReq 
      Height          =   285
      Left            =   3480
      TabIndex        =   81
      Top             =   9600
      Width           =   1095
   End
   Begin VB.TextBox EcDataChega 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   73
      TabStop         =   0   'False
      Top             =   9600
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Modo de trabalho "
      Height          =   615
      Left            =   120
      TabIndex        =   71
      Top             =   0
      Width           =   7095
      Begin VB.ComboBox CbModoTrab 
         BackColor       =   &H80000018&
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   72
         ToolTipText     =   "Aqui pode alternar entre introdu��o, altera��o e valida��o."
         Top             =   240
         Width           =   6855
      End
   End
   Begin VB.ListBox LstReq 
      Height          =   1425
      Left            =   6120
      TabIndex        =   64
      Top             =   3840
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5280
      TabIndex        =   62
      Top             =   8760
      Width           =   1095
   End
   Begin VB.TextBox EcSexo 
      Height          =   285
      Left            =   3480
      Locked          =   -1  'True
      TabIndex        =   58
      TabStop         =   0   'False
      Top             =   9240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcSgrAna 
      Height          =   285
      Left            =   3480
      TabIndex        =   55
      TabStop         =   0   'False
      Top             =   8520
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcGrAna 
      Height          =   285
      Left            =   1080
      TabIndex        =   54
      TabStop         =   0   'False
      Top             =   8520
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame FrChega 
      Height          =   615
      Left            =   120
      TabIndex        =   53
      Top             =   6480
      Width           =   7095
      Begin VB.TextBox EcDtChegaProd1 
         Height          =   285
         Left            =   2400
         TabIndex        =   36
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox EcDtChegaProd2 
         Height          =   285
         Left            =   3960
         TabIndex        =   37
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label LaDtCheP 
         AutoSize        =   -1  'True
         Caption         =   "Data de &chegada de produtos"
         Height          =   195
         Left            =   120
         TabIndex        =   35
         Top             =   240
         Width           =   2130
      End
      Begin VB.Line Line9 
         X1              =   3720
         X2              =   3840
         Y1              =   360
         Y2              =   360
      End
   End
   Begin VB.Frame FrAna 
      Height          =   1575
      Left            =   120
      TabIndex        =   52
      Top             =   4920
      Width           =   7095
      Begin VB.ComboBox CbUtilizadores 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   61
         Top             =   1080
         Width           =   5895
      End
      Begin VB.CommandButton BtPesqRapAna 
         Height          =   315
         Left            =   5640
         Picture         =   "FormSelRes.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   30
         ToolTipText     =   "Pesquisa R�pida de An�lises"
         Top             =   300
         Width           =   375
      End
      Begin VB.TextBox EcDescrAna 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   300
         Width           =   3975
      End
      Begin VB.TextBox EcCodAna 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   960
         TabIndex        =   28
         Top             =   300
         Width           =   735
      End
      Begin VB.TextBox EcResult 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   960
         TabIndex        =   32
         Top             =   720
         Width           =   2175
      End
      Begin VB.TextBox EcFolhaTrab 
         Height          =   285
         Left            =   5160
         TabIndex        =   34
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label LaUser 
         Caption         =   "Aparelho"
         Height          =   255
         Left            =   120
         TabIndex        =   60
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label LaAna 
         AutoSize        =   -1  'True
         Caption         =   "&An�lise"
         Height          =   195
         Left            =   120
         TabIndex        =   27
         Top             =   300
         Width           =   510
      End
      Begin VB.Label LaRes 
         AutoSize        =   -1  'True
         Caption         =   "R&esult."
         Height          =   195
         Left            =   120
         TabIndex        =   31
         Top             =   720
         Width           =   495
      End
      Begin VB.Label LaFt 
         AutoSize        =   -1  'True
         Caption         =   "&Folha Trabalho n�"
         Height          =   195
         Left            =   3720
         TabIndex        =   33
         Top             =   720
         Width           =   1260
      End
   End
   Begin VB.Frame FrGrp 
      Height          =   855
      Left            =   120
      TabIndex        =   24
      Top             =   4080
      Width           =   7095
      Begin VB.CommandButton BtPesqRapGgrp 
         Height          =   315
         Left            =   5640
         Picture         =   "FormSelRes.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Pesquisa R�pida de Grupos de an�lises"
         Top             =   165
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapSGrp 
         Height          =   315
         Left            =   5640
         Picture         =   "FormSelRes.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Pesquisa R�pida de SubGrupos de an�lises"
         Top             =   525
         Width           =   375
      End
      Begin VB.TextBox EcDescrSgrp 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   525
         Width           =   3975
      End
      Begin VB.TextBox EcCodSgrp 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   960
         TabIndex        =   23
         Top             =   525
         Width           =   735
      End
      Begin VB.TextBox EcDescrGrp 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   165
         Width           =   3975
      End
      Begin VB.TextBox EcCodGrp 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   960
         TabIndex        =   19
         Top             =   165
         Width           =   735
      End
      Begin VB.Label LaSgrp 
         AutoSize        =   -1  'True
         Caption         =   "Su&bGrup."
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   525
         Width           =   675
      End
      Begin VB.Label LaGrp 
         AutoSize        =   -1  'True
         Caption         =   "&Grupo"
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   165
         Width           =   435
      End
   End
   Begin VB.Frame FrReq 
      Height          =   1800
      Left            =   120
      TabIndex        =   50
      Top             =   2295
      Width           =   7095
      Begin VB.CommandButton BtPesquisaSalas 
         Height          =   315
         Left            =   5640
         Picture         =   "FormSelRes.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   80
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1400
         Width           =   375
      End
      Begin VB.TextBox EcDescrSala 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   79
         TabStop         =   0   'False
         Top             =   1400
         Width           =   3975
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         TabIndex        =   78
         Top             =   1400
         Width           =   735
      End
      Begin VB.TextBox EcNumReqAssoc 
         Height          =   285
         Left            =   6000
         TabIndex        =   75
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         TabIndex        =   67
         Top             =   1000
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   66
         TabStop         =   0   'False
         Top             =   1000
         Width           =   3975
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   5640
         Picture         =   "FormSelRes.frx":1634
         Style           =   1  'Graphical
         TabIndex        =   65
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1000
         Width           =   375
      End
      Begin VB.CommandButton BtPesqReq 
         Height          =   315
         Left            =   4320
         Picture         =   "FormSelRes.frx":1BBE
         Style           =   1  'Graphical
         TabIndex        =   63
         ToolTipText     =   "Pesquisa R�pida de Requisi��es"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcEpisodio 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   6000
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcNumReq 
         Height          =   285
         Left            =   960
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
      Begin VB.ComboBox CbEstadoReq 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   1800
         Locked          =   -1  'True
         Style           =   1  'Simple Combo
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   240
         Width           =   2535
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   600
         Width           =   1335
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   3000
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label9 
         Caption         =   "Sala"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   77
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Req. Assoc."
         Height          =   195
         Left            =   4800
         TabIndex        =   76
         Top             =   240
         Width           =   1125
      End
      Begin VB.Label Label9 
         Caption         =   "&Proven."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   68
         Top             =   1000
         Width           =   975
      End
      Begin VB.Label LaEpis 
         AutoSize        =   -1  'True
         Caption         =   "Epis�dio"
         Height          =   195
         Left            =   4800
         TabIndex        =   51
         Top             =   600
         Width           =   600
      End
      Begin VB.Label LaNumero 
         AutoSize        =   -1  'True
         Caption         =   "Nr. &Req."
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   600
      End
      Begin VB.Label LaSit 
         Caption         =   "Situa��o"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   630
         Width           =   735
      End
      Begin VB.Label LaUrg 
         Caption         =   "Prior."
         Height          =   255
         Left            =   2640
         TabIndex        =   15
         Top             =   630
         Width           =   375
      End
   End
   Begin VB.Frame FrUte 
      Height          =   1695
      Left            =   120
      TabIndex        =   44
      Top             =   615
      Width           =   7095
      Begin VB.TextBox EcReqAux 
         Height          =   285
         Left            =   6000
         TabIndex        =   69
         Top             =   240
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox EcNome 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   960
         Width           =   6015
      End
      Begin VB.ComboBox CbTipoUtente 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   165
         Width           =   855
      End
      Begin VB.CommandButton BtPesquisaUtente 
         Height          =   315
         Left            =   4560
         Picture         =   "FormSelRes.frx":2148
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Pesquisa R�pida de Utentes"
         Top             =   165
         Width           =   375
      End
      Begin VB.TextBox EcUtente 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   1800
         TabIndex        =   2
         Top             =   165
         Width           =   2775
      End
      Begin VB.TextBox EcProcHosp1 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox EcProcHosp2 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox EcDataNasc 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3480
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox EcCartaoUte 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   5640
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   645
         Width           =   1335
      End
      Begin VB.ComboBox EcDescrSexo 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   960
         Locked          =   -1  'True
         Style           =   1  'Simple Combo
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label LaReqAux 
         AutoSize        =   -1  'True
         Caption         =   "N. An�lise"
         Height          =   195
         Left            =   5160
         TabIndex        =   70
         Top             =   240
         Visible         =   0   'False
         Width           =   720
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   49
         Top             =   960
         Width           =   420
      End
      Begin VB.Label LbUtente 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         Height          =   195
         Left            =   120
         TabIndex        =   0
         Top             =   165
         Width           =   480
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "P. Hosp."
         Height          =   195
         Left            =   120
         TabIndex        =   48
         Top             =   600
         Width           =   615
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         Height          =   195
         Left            =   2520
         TabIndex        =   47
         Top             =   1320
         Width           =   810
      End
      Begin VB.Label LbCartaoUte 
         AutoSize        =   -1  'True
         Caption         =   "C. Utente"
         Height          =   195
         Left            =   4800
         TabIndex        =   46
         Top             =   645
         Width           =   675
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         Height          =   195
         Left            =   120
         TabIndex        =   45
         Top             =   1320
         Width           =   360
      End
   End
   Begin VB.TextBox EcSeqUteReq 
      Height          =   285
      Left            =   3480
      Locked          =   -1  'True
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   8880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcUrgencia 
      Height          =   285
      Left            =   1080
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   9240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcCodUteSeq 
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   8880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "EcEstadoReq"
      Height          =   255
      Index           =   1
      Left            =   2400
      TabIndex        =   82
      Top             =   9600
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label LaChega 
      Caption         =   "Chegada"
      Height          =   255
      Left            =   360
      TabIndex        =   74
      Top             =   9600
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "EcSexo"
      Height          =   255
      Index           =   0
      Left            =   2400
      TabIndex        =   59
      Top             =   9240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "EcSgrAna"
      Height          =   255
      Left            =   2400
      TabIndex        =   57
      Top             =   8520
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "EcGrAna"
      Height          =   255
      Left            =   120
      TabIndex        =   56
      Top             =   8520
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "EcSeqUteReq"
      Height          =   255
      Left            =   2400
      TabIndex        =   43
      Top             =   8880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label11 
      Caption         =   "EcUrgencia"
      Height          =   255
      Left            =   120
      TabIndex        =   41
      Top             =   9240
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label10 
      Caption         =   "EcCodUteSeq"
      Height          =   255
      Left            =   120
      TabIndex        =   40
      Top             =   8880
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "FormSelRes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 19/06/2002
' T�cnico Filipe Nogueira

' Vari�veis Globais para este Form.

Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim Flg_PesqUtente As Boolean

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    EcCodGrp.Tag = adVarChar
    EcCodGrp.MaxLength = 5
    Inicializacoes
    
    Flg_PesqUtente = False
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
        
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    gF_SELRES = 1

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    estado = 1
    
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    LimpaCampos
    
    If Trim(gDUtente.seq_utente) <> "" And Flg_PesqUtente = True Then
        Flg_PesqUtente = False
        If Trim(gDUtente.seq_utente) <> Trim(EcSeqUteReq.Text) And Trim(EcSeqUteReq.Text) <> "" Then
            BG_Mensagem mediMsgBox, "O utente escolhido e o da requisi��o n�o � o mesmo!", vbOKOnly + vbExclamation, "Requisi��o"
        Else
            EcCodUteSeq.Text = gDUtente.seq_utente
            PreencheIdentificacaoUtente
        End If
    End If
    
    Select Case gLAB
        Case "CITO", "BIO"
            EcReqAux.Visible = True
            LaReqAux.Visible = True
        Case "GM"
            EcReqAux.Visible = True
            LaReqAux.Visible = True
            LaReqAux.caption = "Nr. Caso"
    End Select
    
    If gModoRes = cModoResMicro And BL_SeleccionaGrupoAnalises = True Then
        'soliveira hcvp
        EcCodGrp.Text = BL_SelCodigo("sl_idutilizador", "gr_ana", "cod_utilizador", gCodUtilizador, "V")
        If EcCodGrp.Text = "" Then
            EcCodGrp.Text = gCodGrupoMicrobiologia
        End If
        EcCodGrp_Validate False
        EcNumReq.SetFocus
    End If
    
End Sub

Sub FuncaoProcurar()
    
    Dim continua As Boolean
    Dim sSql As String
    Dim modo As String
    Dim Tabela As String
    
    If EcEstadoReq = "" Then
        EcNumReq_Validate False
    End If
    
    If EcEstadoReq = gEstadoReqHistorico Then
        Tabela = " SL_REALIZA_H "
        modo = "_H"
    Else
        Tabela = " SL_REALIZA "
        modo = ""
    End If
    
    If EcDtChegaProd1.Text <> "" And EcDtChegaProd2.Text = "" Then
        EcDtChegaProd2.Text = Bg_DaData_ADO
    End If
    
    continua = True
    If gObrigaGrupo = "1" Then
        ' Verificar campos obrigat�rios
        If (Trim(EcUtente.Text) = "" Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "" Or Trim(EcCodGrp.Text) = "") And _
           (Trim(EcNumReq.Text) = "" Or Trim(EcCodGrp.Text) = "") And _
           (CbSituacao.ListIndex = -1 Or CbUrgencia.ListIndex = -1 Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "" Or Trim(EcCodGrp.Text) = "") And _
           (Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "" Or Trim(EcCodGrp.Text) = "") And _
           (Trim(EcFolhaTrab.Text) = "") And _
           (Trim(EcCodAna.Text) = "" Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "") And _
           (Trim(EcResult.Text) = "" Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "") Then
           
           BG_Mensagem mediMsgBox, "Deve prencher no minimo, uma das seguintes combina��es: " & Chr(13) & Chr(13) & _
            "1. Utente + Datas de chegada de produtos + Grupo de an�lises " & Chr(13) & _
            "2. Requisi��o + Grupo de an�lises " & Chr(13) & _
            "3. Situa��o ou Urg�ncia + Datas de chegada de produtos + Grupo de an�lise " & Chr(13) & _
            "4. Folha de trabalho " & Chr(13) & _
            "5. An�lise + Datas de chegada de produtos " & Chr(13) & _
            "6. Datas de chegada de produtos + Grupo de an�lise " & Chr(13) & _
            IIf(gModoRes = cModoResIns Or gPermResUtil = cIntRes, "", "7. Resultado + Datas de chegada de produtos " & Chr(13)) & _
            IIf(gModoRes = cModoResIns Or gPermResUtil = cIntRes, "", "8. Utilizador de inser��o + qualquer uma das combina��es acima referidas "), vbOKOnly + vbExclamation, "Seleccionar resultados"
            
            continua = False
        End If
    Else
        ' Verificar campos obrigat�rios excepto o grupo
        If (Trim(EcUtente.Text) = "" Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "") And _
           Trim(EcNumReq.Text) = "" And _
           (CbSituacao.ListIndex = -1 Or CbUrgencia.ListIndex = -1 Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "") And _
           (Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "") And _
           (Trim(EcFolhaTrab.Text) = "") And _
           (Trim(EcCodAna.Text) = "" Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "") And _
           (Trim(EcResult.Text) = "" Or Trim(EcDtChegaProd1.Text) = "" Or Trim(EcDtChegaProd2.Text) = "") Then
           
           BG_Mensagem mediMsgBox, "Deve prencher no minimo, uma das seguintes combina��es: " & Chr(13) & Chr(13) & _
            "1. Utente + Datas de chegada de produtos " & Chr(13) & _
            "2. Requisi��o " & Chr(13) & _
            "3. Situa��o ou Urg�ncia + Datas de chegada de produtos " & Chr(13) & _
            "4. Folha de trabalho " & Chr(13) & _
            "5. An�lise + Datas de chegada de produtos " & Chr(13) & _
            "6. Datas de chegada de produtos  " & Chr(13) & _
            IIf(gModoRes = cModoResIns Or gPermResUtil = cIntRes, "", "7. Resultado + Datas de chegada de produtos " & Chr(13)) & _
            IIf(gModoRes = cModoResIns Or gPermResUtil = cIntRes, "", "8. Aparelho de inser��o + qualquer uma das combina��es acima referidas "), vbOKOnly + vbExclamation, "Seleccionar resultados"
            
            continua = False
        End If
    End If
    
    'VERIFICAR SE A DATA DE CHEGADA DA SL_REQUIS ESTA PREENCHIDA
    
    If continua = True Then
    
        gSqlSelRes = ""
        gSqlSelResSELECT1 = ""
        gSqlSelResFROM1 = ""
        gSqlSelResWHERE1 = ""
        gSqlSelResSELECT2 = ""
        gSqlSelResFROM2 = ""
        gSqlSelResWHERE2 = ""
        gSqlSelResORDER = ""
        
        Select Case gModoRes
            Case cModoResIns
                ' -----------------------------------------------------------------
                'Introdu��o de Resultados
                ' -----------------------------------------------------------------
                gSqlSelResSELECT1 = "SELECT -1 as seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, " & _
                                " marca.dt_chega, marca.ord_ana, marca.ord_marca, " & _
                                " marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, marca.n_folha_trab, req.n_epis,req.t_sit, req.n_req_assoc, " & _
                                " req.dt_chega as dt_chega_req,  req.hr_chega as hr_chega_req, req.cod_proven,  " & _
                                " req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, " & _
                                " id.dt_nasc_ute, id.sexo_ute, " & _
                                " 'Marcacoes' as FLG_UNID_ACT1, 'Marcacoes' as FLG_UNID_ACT2, 'Marcacoes' as UNID_1_RES1, 'Marcacoes' as UNID_2_RES1, 'Marcacoes' as FAC_CONV_UNID1, 'Marcacoes' as UNID_1_RES2, 'Marcacoes' as UNID_2_RES2, 'Marcacoes' as FAC_CONV_UNID2, req.inf_complementar " & _
                                " flg_apar_trans, marca.flg_facturado,req.inf_complementar , pro.ordem ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                                " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                                " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                                " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, pro.Cod_Produto, " & _
                                " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                                " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                                    
               If (gSGBD = gOracle) Then
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id, slv_analises, " & _
                                        " sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto pro "
                                        
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req " & _
                                " AND id.seq_utente = req.seq_utente " & _
                                " AND marca.cod_agrup = slv_analises.cod_ana " & _
                                " AND marca.dt_chega IS NOT NULL " & _
                                " AND marca.cod_perfil = p.cod_perfis(+) " & _
                                " AND marca.cod_ana_c = c.cod_ana_c(+) " & _
                                " AND marca.cod_ana_s = s.cod_ana_s(+) " & _
                                " AND marca.cod_ana_s = s2.cod_ana_s(+) " & _
                                " AND slv_analises.cod_produto = pro.cod_produto "
                                
                ElseIf (gSGBD = gSqlServer) Then
                    
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca LEFT OUTER JOIN sl_perfis p on (marca.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (marca.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (marca.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (marca.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, slv_analises LEFT OUTER JOIN sl_produto pro ON slv_analises.cod_produto = pro.cod_produto "
                                          
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req " & _
                                        " AND  id.seq_utente = req.seq_utente " & _
                                        " AND marca.cod_agrup = slv_analises.cod_ana " & _
                                        " AND  marca.dt_chega IS NOT NULL "
                End If
            'soliveira terrugem
            Case cModoResVal, cModoResValAValTec, cModoResValApar
                ' -----------------------------------------------------------------
                'Valida��o de Resultados,consoante a permiss�o do utlizador, por aparelho
                ' -----------------------------------------------------------------
                gSqlSelResSELECT2 = "SELECT realiza.seq_realiza,realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                        " realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, " & _
                        " realiza.ord_ana_compl, realiza.ord_ana_perf, realiza.cod_agrup, realiza.n_folha_trab," & _
                        " realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, realiza.dt_act, realiza.hr_act," & _
                        " realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val,realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val," & _
                        " req.n_epis,req.t_sit, req.dt_chega as dt_chega_req,  req.hr_chega as hr_chega_req,  req.n_req_assoc, req.cod_proven,  " & _
                        " req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute," & _
                        " id.dt_nasc_ute, id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2," & _
                        " FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, " & _
                        " 1 as flg_apar_trans, realiza.flg_facturado,req.inf_complementar, pro.ordem ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                        " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                        " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                        " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, pro.Cod_Produto, " & _
                        " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                        " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                                
                If gSGBD = gOracle Then
                    gSqlSelResFROM2 = " FROM sl_realiza realiza, sl_requis req, sl_identif id, slv_analises, " & _
                                        " sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto pro "
                
                     gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req " & _
                                        " AND id.seq_utente = req.seq_utente " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana " & _
                                        " AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_perfil = p.cod_perfis(+) " & _
                                        " AND realiza.cod_ana_c = c.cod_ana_c(+) " & _
                                        " AND realiza.cod_ana_s = s.cod_ana_s(+) " & _
                                        " AND realiza.cod_ana_s = s2.cod_ana_s(+) " & _
                                        " AND slv_analises.cod_produto = pro.cod_produto "

                ElseIf gSGBD = gSqlServer Then
                    gSqlSelResFROM2 = " FROM sl_realiza realiza LEFT OUTER JOIN sl_perfis p on (realiza.cod_perfil = p.cod_perfis) " & _
                              " LEFT OUTER JOIN sl_ana_c c on (realiza.cod_ana_c = c.cod_ana_c) " & _
                              " LEFT OUTER JOIN sl_ana_s s on (realiza.cod_ana_s = s.cod_ana_s) " & _
                              " LEFT OUTER JOIN sl_segundo_res s2 on (realiza.cod_ana_s = s2.cod_ana_s), " & _
                              " sl_requis req, sl_identif id, slv_analises "
            
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req " & _
                                " AND realiza.cod_agrup = slv_analises.cod_ana " & _
                                " AND id.seq_utente = req.seq_utente " & _
                                " AND realiza.dt_chega IS NOT NULL "

                End If
                    
                If gPermResUtil = 1 Or gPermResUtil = 0 Then
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado=1) "
                ElseIf gPermResUtil = 2 Then
                    If gModoRes = cModoResVal Then
                        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado=1 OR realiza.flg_estado=5)"
                    'soliveira terrugem
                    ElseIf gModoRes = cModoResValApar Then
                        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND realiza.flg_estado=1 "
                    ElseIf gModoRes = cModoResValAValTec Then
                        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND realiza.flg_estado=5"
                    End If
                End If
                gModoRes = cModoResVal

            Case cModoResGeral
                If (gSGBD = gOracle) Then
                    'Visualiza��o de todos os resultados
                    gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, " & _
                           "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, " & _
                           "marca.n_folha_trab, NULL AS user_cri, TO_DATE(NULL) AS dt_cri, NULL AS hr_cri, NULL AS user_act, " & _
                           "TO_DATE(NULL) AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, TO_DATE(NULL) AS dt_val, " & _
                           "NULL AS hr_val, NULL AS user_tec_val, TO_DATE(NULL) AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, " & _
                           "req.dt_chega AS dt_chega_req,  req.hr_chega as hr_chega_req,  req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, " & _
                           "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, " & _
                           "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, " & _
                           "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, " & _
                           "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado, req.inf_complementar , 0 ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, slv_analises.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                            
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id ,slv_analises, sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2 "
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req AND id.seq_utente = req.seq_utente AND marca.dt_chega IS NOT NULL " & _
                                        " AND marca.cod_agrup = slv_analises.cod_ana " & _
                                        " AND marca.cod_perfil = p.cod_perfis(+) " & _
                                        " AND marca.cod_ana_c = c.cod_ana_c(+) " & _
                                        " AND marca.cod_ana_s = s.cod_ana_s(+) " & _
                                        " AND marca.cod_ana_s = s2.cod_ana_s(+) "

                    gSqlSelResSELECT2 = " UNION SELECT realiza.seq_realiza, realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                           "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, " & _
                           "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, " & _
                           "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, " & _
                           "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req, " & _
                           "req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, " & _
                           "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, " & _
                           "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado, req.inf_complementar , 0 ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia, " & _
                            " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, slv_analises.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                    
                    gSqlSelResFROM2 = " FROM " & Tabela & " realiza, sl_requis req, sl_identif id, slv_analises, sl_ana_s s, sl_ana_c c, sl_perfis p, sl_segundo_res s2 "
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req AND  " & _
                                        " id.seq_utente = req.seq_utente AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana " & _
                                        " AND realiza.cod_perfil = p.cod_perfis(+) " & _
                                        " AND realiza.cod_ana_c = c.cod_ana_c(+) " & _
                                        " AND realiza.cod_ana_s = s.cod_ana_s(+) " & _
                                        " AND realiza.cod_ana_s = s2.cod_ana_s(+) "
        
                ElseIf (gSGBD = gSqlServer) Then
                
                    'Visualiza��o de todos os resultados
                    gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, " & _
                           "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, " & _
                           "marca.n_folha_trab, NULL AS user_cri, convert(varchar(8),null,105) AS dt_cri, NULL AS hr_cri, NULL AS user_act, " & _
                           "convert(varchar(8),null,105) AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, convert(varchar(8),null,105) AS dt_val, " & _
                           "NULL AS hr_val, NULL AS user_tec_val, convert(varchar(8),null,105) AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, " & _
                           "req.dt_chega AS dt_chega_req,  req.hr_chega as hr_chega_req,  req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, " & _
                           "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, " & _
                           "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, " & _
                           "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, " & _
                           "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado, req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                            
                
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca LEFT OUTER JOIN sl_perfis p on (marca.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (marca.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (marca.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (marca.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, slv_analises "
                                      
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req AND id.seq_utente = req.seq_utente " & _
                                        " AND marca.dt_chega IS NOT NULL " & _
                                        " AND marca.cod_agrup = slv_analises.cod_ana "
                    
                    gSqlSelResSELECT2 = " UNION SELECT realiza.seq_realiza, realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                           "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, " & _
                           "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, " & _
                           "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, " & _
                           "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req, " & _
                           "req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, " & _
                           "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, " & _
                           "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado, req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                    
                    gSqlSelResFROM2 = " FROM " & Tabela & " realiza LEFT OUTER JOIN sl_perfis p on (realiza.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (realiza.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (realiza.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (realiza.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, slv_analises "
                                      
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req AND id.seq_utente = req.seq_utente " & _
                                        " AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana "

                End If
            Case cModoResMicro
            
                If (gSGBD = gOracle) Then
                    'Visualiza��o de todos os resultados
                    gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, " & _
                           "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, " & _
                           "marca.n_folha_trab, NULL AS user_cri, TO_DATE(NULL) AS dt_cri, NULL AS hr_cri, NULL AS user_act, " & _
                           "TO_DATE(NULL) AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, TO_DATE(NULL) AS dt_val, " & _
                           "NULL AS hr_val, NULL AS user_tec_val, TO_DATE(NULL) AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, " & _
                           "req.dt_chega AS dt_chega_req,  req.hr_chega as hr_chega_req,  req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, " & _
                           "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, " & _
                           "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, " & _
                           "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, " & _
                           "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado, " & _
                           "to_number(null) as n_res_frase, null as cod_frase, -1 as ord_frase, " & _
                           "to_number(null) n_res_micro, null as cod_micro, null as quantif,null as flg_imp,null as flg_tsq, null as flg_testes, null as prova,null as cod_gr_antibio,req.inf_complementar , prod.ordem ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, prod.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr, req.user_fecho, marca.seq_req_tubo "
                            
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id ,slv_analises, sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto prod "
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req AND id.seq_utente = req.seq_utente AND marca.dt_chega IS NOT NULL " & _
                                        " AND marca.cod_agrup = slv_analises.cod_ana " & _
                                        " AND marca.cod_perfil = p.cod_perfis(+) " & _
                                        " AND marca.cod_ana_c = c.cod_ana_c(+) " & _
                                        " AND marca.cod_ana_s = s.cod_ana_s(+) " & _
                                        " AND marca.cod_ana_s = s2.cod_ana_s(+) " & _
                                        " AND slv_analises.cod_produto = prod.cod_produto (+) "

                    
                    gSqlSelResSELECT2 = " UNION " & _
                           "SELECT realiza.seq_realiza, realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                           "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, " & _
                           "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, " & _
                           "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, " & _
                           "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req, " & _
                           "req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, " & _
                           "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, " & _
                           "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado, " & _
                           "frase.n_res n_res_frase, frase.cod_frase, frase.ord_frase, " & _
                           "micro.N_Res n_res_micro, micro.Cod_Micro, micro.Quantif, micro.flg_imp, micro.flg_tsq, micro.flg_testes, micro.Prova, micro.cod_gr_antibio,req.inf_complementar, prod.ordem ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, prod.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr, req.user_fecho, realiza.seq_req_tubo "
                            
                    gSqlSelResFROM2 = " FROM sl_realiza realiza, sl_requis req, sl_identif id, sl_res_micro micro, sl_res_frase frase, slv_analises, sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2, sl_produto prod "
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req AND  " & _
                                        " realiza.seq_realiza = frase.seq_realiza(+) and realiza.seq_realiza = micro.seq_realiza(+) and " & _
                                        " id.seq_utente = req.seq_utente AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana " & _
                                        " AND realiza.cod_perfil = p.cod_perfis(+) " & _
                                        " AND realiza.cod_ana_c = c.cod_ana_c(+) " & _
                                        " AND realiza.cod_ana_s = s.cod_ana_s(+) " & _
                                        " AND realiza.cod_ana_s = s2.cod_ana_s(+) " & _
                                        " AND slv_analises.cod_produto = prod.cod_produto (+) "

                End If
                
                If (gSGBD = gSqlServer) Then
                
                    'Visualiza��o de todos os resultados
                   gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, " & _
                           "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, " & _
                           "marca.n_folha_trab, NULL AS user_cri, NULL AS dt_cri, NULL AS hr_cri, NULL AS user_act, " & _
                           "NULL AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, NULL AS dt_val, " & _
                           "NULL AS hr_val, NULL AS user_tec_val, NULL AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, " & _
                           "req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req, req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, " & _
                           "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, " & _
                           "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, " & _
                           "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, " & _
                           "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado, " & _
                           "null as n_res_frase, null as cod_frase, -1 as ord_frase, " & _
                           "null n_res_micro, null as cod_micro, null as quantif,null as flg_imp,null as flg_tsq, null as flg_testes, null as prova,null as cod_gr_antibio,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr, req.user_fecho, marca.seq_req_tubo "
                            
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca LEFT OUTER JOIN sl_perfis p on (marca.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (marca.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (marca.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (marca.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, slv_analises "
                                      
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req AND id.seq_utente = req.seq_utente " & _
                                        " AND marca.dt_chega IS NOT NULL " & _
                                        " AND marca.cod_agrup = slv_analises.cod_ana "
                    
                    gSqlSelResSELECT2 = " UNION " & _
                           "SELECT realiza.seq_realiza, realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                           "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, " & _
                           "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, " & _
                           "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, " & _
                           "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req, " & _
                           "req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, " & _
                           "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, " & _
                           "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado, " & _
                           "frase.n_res n_res_frase, frase.cod_frase, frase.ord_frase, " & _
                           "micro.N_Res n_res_micro, micro.Cod_Micro, micro.Quantif, micro.flg_imp, micro.flg_tsq, micro.flg_testes, micro.Prova, micro.cod_gr_antibio,req.inf_complementar, slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr, req.user_fecho, realiza.seq_req_tubo "
                           
                    gSqlSelResFROM2 = " FROM sl_realiza realiza LEFT OUTER JOIN sl_res_frase frase ON (realiza.seq_realiza = frase.seq_realiza) " & _
                                      " LEFT OUTER JOIN  sl_res_micro micro ON (realiza.seq_realiza = micro.seq_realiza) " & _
                                      " LEFT OUTER JOIN sl_perfis p on (realiza.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (realiza.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (realiza.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (realiza.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, slv_analises "
                                      
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req AND id.seq_utente = req.seq_utente " & _
                                        " AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana "
                End If


            Case cModoResInsVal
                
                If (gSGBD = gOracle) Then
                    'Introdu��o e Valida��o de Resultados, consoante permiss�o do utilizador
                    gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, " & _
                           "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, " & _
                           "marca.n_folha_trab, NULL AS user_cri, TO_DATE(NULL) AS dt_cri, NULL AS hr_cri, NULL AS user_act, " & _
                           "TO_DATE(NULL) AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, TO_DATE(NULL) AS dt_val, " & _
                           "NULL AS hr_val, NULL AS user_tec_val, TO_DATE(NULL) AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, " & _
                           "req.dt_chega AS dt_chega_req, req.hr_chega as hr_chega_req,  req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, " & _
                           "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, " & _
                           "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, " & _
                           "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, " & _
                           "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                            
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id , slv_analises, sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2 "
                    
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req AND id.seq_utente = req.seq_utente AND marca.dt_chega IS NOT NULL " & _
                                        " AND marca.cod_agrup = slv_analises.cod_ana " & _
                                        " AND marca.cod_perfil = p.cod_perfis(+) " & _
                                        " AND marca.cod_ana_c = c.cod_ana_c(+) " & _
                                        " AND marca.cod_ana_s = s.cod_ana_s(+) " & _
                                        " AND marca.cod_ana_s = s2.cod_ana_s(+) "
                    
                    gSqlSelResSELECT2 = "UNION " & _
                           "SELECT realiza.seq_realiza, realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                           "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, " & _
                           "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, " & _
                           "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, " & _
                           "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req, " & _
                           "req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, " & _
                           "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, " & _
                           "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                           
                    gSqlSelResFROM2 = " FROM sl_realiza realiza, sl_requis req, sl_identif id, slv_analises, sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2 "
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req AND  " & _
                                        " id.seq_utente = req.seq_utente AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana " & _
                                        " AND realiza.cod_perfil = p.cod_perfis(+) " & _
                                        " AND realiza.cod_ana_c = c.cod_ana_c(+) " & _
                                        " AND realiza.cod_ana_s = s.cod_ana_s(+) " & _
                                        " AND realiza.cod_ana_s = s2.cod_ana_s(+) "

                    If gPermResUtil = 0 Or gPermResUtil = 1 Then
                        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado = 1) "
                    ElseIf gPermResUtil = 2 Then
                        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado = 1 OR realiza.flg_estado = 5) "
                    End If
                    
                End If
                
                If (gSGBD = gSqlServer) Then
                
                    'Introdu��o e Valida��o de Resultados, consoante permiss�o do utilizador
                    gSqlSelResSELECT1 = "(SELECT -1 AS seq_realiza, marca.n_req AS numero_req, marca.cod_perfil, marca.cod_ana_c ana_c, marca.cod_ana_s ana_s, " & _
                           "marca.dt_chega, marca.ord_ana, marca.ord_marca, marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, " & _
                           "marca.n_folha_trab, NULL AS user_cri, NULL AS dt_cri, NULL AS hr_cri, NULL AS user_act, " & _
                           "NULL AS dt_act, NULL AS hr_act, NULL AS flg_estado, NULL AS user_val, NULL AS dt_val, " & _
                           "NULL AS hr_val, NULL AS user_tec_val, NULL AS dt_tec_val, NULL AS hr_tec_val, req.n_epis,req.t_sit, " & _
                           "req.dt_chega AS dt_chega_req,  req.hr_chega as hr_chega_req,  req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, " & _
                           "id.nome_ute, id.dt_nasc_ute, id.sexo_ute, NULL AS flg_anormal, NULL AS flg_anormal_2, " & _
                           "'Marcacoes' AS FLG_UNID_ACT1, 'Marcacoes' AS FLG_UNID_ACT2, 'Marcacoes' AS UNID_1_RES1, " & _
                           "'Marcacoes' AS UNID_2_RES1, 'Marcacoes' AS FAC_CONV_UNID1, 'Marcacoes' AS UNID_1_RES2, " & _
                           "'Marcacoes' AS UNID_2_RES2, 'Marcacoes' AS FAC_CONV_UNID2, Marca.flg_apar_trans, marca.flg_facturado,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                    
                    gSqlSelResFROM1 = " FROM sl_marcacoes marca LEFT OUTER JOIN sl_perfis p on (marca.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (marca.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (marca.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (marca.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, slv_analises "
                                      
                    gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req AND id.seq_utente = req.seq_utente " & _
                                        " AND marca.dt_chega IS NOT NULL " & _
                                        " AND marca.cod_agrup = slv_analises.cod_ana "
                    
                    gSqlSelResSELECT2 = "UNION " & _
                           "SELECT realiza.seq_realiza, realiza.n_req AS numero_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                           "realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, realiza.ord_ana_compl, realiza.ord_ana_perf, " & _
                           "realiza.cod_agrup, realiza.n_folha_trab, realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, " & _
                           "realiza.dt_act, realiza.hr_act, realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val, " & _
                           "realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val, req.n_epis,req.t_sit, req.dt_chega AS dt_chega_req,   req.hr_chega as hr_chega_req, " & _
                           "req.n_req_assoc, req.cod_proven, req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, id.dt_nasc_ute, " & _
                           "id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2, FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, " & _
                           "UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, '1' AS flg_apar_trans, realiza.flg_facturado,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                           " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "

                    gSqlSelResFROM2 = " FROM sl_realiza realiza LEFT OUTER JOIN sl_perfis p on (realiza.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (realiza.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (realiza.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (realiza.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, slv_analises "
                                      
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req AND id.seq_utente = req.seq_utente " & _
                                        " AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana "

                    If gPermResUtil = 0 Or gPermResUtil = 1 Then
                        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado = 1) "
                    ElseIf gPermResUtil = 2 Then
                        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (realiza.flg_estado = 1 OR realiza.flg_estado = 5) "
                    End If
                End If

            Case cModoResAssinatura
                ' -----------------------------------------------------------------
                ' ASSINATURA DE RESULTADOS
                ' -----------------------------------------------------------------
                If gSGBD = gOracle Then
                    gSqlSelResSELECT2 = "SELECT realiza.seq_realiza,realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                            " realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, " & _
                            " realiza.ord_ana_compl, realiza.ord_ana_perf, realiza.cod_agrup, realiza.n_folha_trab," & _
                            " realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, realiza.dt_act, realiza.hr_act," & _
                            " realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val,realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val," & _
                            " req.n_epis,req.t_sit, req.dt_chega as dt_chega_req,   req.hr_chega as hr_chega_req, req.n_req_assoc, req.cod_proven, " & _
                            " req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute," & _
                            " id.dt_nasc_ute, id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2," & _
                            " FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, " & _
                            "1 as flg_apar_trans, realiza.flg_facturado,res.result result1,res2.result result2,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                            " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                                
                    gSqlSelResFROM2 = " FROM sl_realiza realiza, sl_requis req, sl_identif id, sl_res_alfan res, sl_res_alfan res2, slv_analises, sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2 "
                        
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req " & _
                            " AND realiza.seq_realiza = res.seq_realiza AND res.n_res = 1  " & _
                            " AND realiza.seq_realiza = res2.seq_realiza(+) AND res2.n_res(+) = 2 " & _
                            " AND id.seq_utente = req.seq_utente " & _
                            " AND realiza.dt_chega IS NOT NULL " & _
                            " AND realiza.cod_agrup = slv_analises.cod_ana " & _
                            " AND realiza.cod_perfil = p.cod_perfis(+) " & _
                            " AND realiza.cod_ana_c = c.cod_ana_c(+) " & _
                            " AND realiza.cod_ana_s = s.cod_ana_s(+) " & _
                            " AND realiza.cod_ana_s = s2.cod_ana_s(+) "
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (req.n_req not in (select distinct r.n_req  from sl_realiza r where r.n_req = realiza.n_req and flg_estado not in(1,2,3,5))) "
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (req.n_req not in (select distinct m.n_req  from sl_marcacoes m WHERE m.n_req = req.n_req)) "
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (req.n_req not in (select distinct a.n_req  from sl_req_assinatura a WHERE a.n_req = req.n_req)) "
                    
                ElseIf gSGBD = gSqlServer Then
                    gSqlSelResSELECT2 = "SELECT realiza.seq_realiza,realiza.n_req n_req, realiza.cod_perfil, realiza.cod_ana_c ana_c, realiza.cod_ana_s ana_s, " & _
                            " realiza.dt_chega, realiza.ord_ana, realiza.ord_marca, " & _
                            " realiza.ord_ana_compl, realiza.ord_ana_perf, realiza.cod_agrup, realiza.n_folha_trab," & _
                            " realiza.user_cri, realiza.dt_cri, realiza.hr_cri, realiza.user_act, realiza.dt_act, realiza.hr_act," & _
                            " realiza.flg_estado, realiza.user_val, realiza.dt_val, realiza.hr_val,realiza.user_tec_val, realiza.dt_tec_val, realiza.hr_tec_val," & _
                            " req.n_epis,req.t_sit, req.dt_chega as dt_chega_req,   req.hr_chega as hr_chega_req, req.n_req_assoc, req.cod_proven, " & _
                            " req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute," & _
                            " id.dt_nasc_ute, id.sexo_ute, realiza.flg_anormal, realiza.flg_anormal_2," & _
                            " FLG_UNID_ACT1, FLG_UNID_ACT2, UNID_1_RES1, UNID_2_RES1, FAC_CONV_UNID1, UNID_1_RES2, UNID_2_RES2, FAC_CONV_UNID2, " & _
                            "1 as flg_apar_trans, realiza.flg_facturado,res.result result1,res2.result result2,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia, " & _
                            " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "
                                
                    gSqlSelResFROM2 = " FROM sl_realiza realiza LEFT OUTER JOIN sl_res_alfan res2 on (realiza.seq_realiza = res2.seq_realiza and res2.n_res = 2), " & _
                                      " LEFT OUTER JOIN sl_perfis p on (realiza.cod_perfil = p.cod_perfis) " & _
                                      " LEFT OUTER JOIN sl_ana_c c on (realiza.cod_ana_c = c.cod_ana_c) " & _
                                      " LEFT OUTER JOIN sl_ana_s s on (realiza.cod_ana_s = s.cod_ana_s) " & _
                                      " LEFT OUTER JOIN sl_segundo_res s2 on (realiza.cod_ana_s = s2.cod_ana_s), " & _
                                      " sl_requis req, sl_identif id, sl_res_alfan res, slv_analises "
                                      
                    gSqlSelResWHERE2 = " WHERE req.n_req = realiza.n_req AND id.seq_utente = req.seq_utente " & _
                                        " AND realiza.seq_realiza = res.seq_realiza AND res.n_res = 1  " & _
                                        " AND realiza.dt_chega IS NOT NULL " & _
                                        " AND realiza.cod_agrup = slv_analises.cod_ana "
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (req.n_req not in (select distinct r.n_req  from sl_realiza r where r.n_req = req.n_req and r.flg_estado not in(1,2,3,5))) "
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (req.n_req not in (select distinct m.n_req  from sl_marcacoes m where m.n_req = req.n_req)) "
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND (req.n_req not in (select distinct a.n_req  from sl_req_assinatura a WHERE a.n_req = req.n_req)) "
                End If
                gModoRes = cModoResAssinatura
                
            Case cModoResValApar
                ' -----------------------------------------------------------------
                'Valida��o de Resultados,consoante a permiss�o do utlizador, por aparelho
                ' -----------------------------------------------------------------
                
                If CbUtilizadores.ListIndex <> mediComboValorNull Then
                    gSqlSelResSELECT2 = "SELECT -2 as seq_realiza,recebidos.n_req n_req, recebidos.cod_perfil, recebidos.cod_ana_c ana_c, recebidos.cod_ana_s ana_s, " & _
                            " recebidos.dt_chega, recebidos.ord_ana, recebidos.ord_marca, " & _
                            " recebidos.ord_ana_compl, recebidos.ord_ana_perf, recebidos.cod_agrup, recebidos.n_folha_trab," & _
                            " recebidos.user_cri, recebidos.dt_cri, recebidos.hr_cri, recebidos.user_act, recebidos.dt_act, recebidos.hr_act," & _
                            " recebidos.flg_estado, " & _
                            " req.n_epis,req.t_sit, req.dt_chega as dt_chega_req,   req.hr_chega as hr_chega_req, req.n_req_assoc, req.cod_proven, " & _
                            " req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute," & _
                            " id.dt_nasc_ute, id.sexo_ute, recebidos.flg_anormal, recebidos.flg_anormal_2," & _
                            " 'Marcacoes' as FLG_UNID_ACT1, 'Marcacoes' as FLG_UNID_ACT2, 'Marcacoes' as UNID_1_RES1, 'Marcacoes' as UNID_2_RES1, 'Marcacoes' as FAC_CONV_UNID1,'Marcacoes' as UNID_1_RES2, 'Marcacoes' as UNID_2_RES2, 'Marcacoes' as FAC_CONV_UNID2, " & _
                            " 1 as flg_apar_trans, recebidos.flg_facturado,recebidos.result1 result1,recebidos.result2 result2,req.inf_complementar , slv_analises.ord_produto, slv_analises.cod_gr_ana, req.tipo_urgencia,  " & _
                            " p.descr_perfis, c.descr_ana_c,s.descr_ana_s , " & _
                            " s.descr_formula, s.casas_dec, s.lim_inf, s.lim_sup, s.val_defeito, s.unid_1, s.unid_2, " & _
                            " s.unid_uso , s.fac_conv_unid, s.t_result, s.coment, s.flg_cut_off, s.flg_1_para_x, s.Cod_Produto, " & _
                            " s2.t_result t_result2, s2.descr_formula descr_formula2, s2.casas_dec casas_dec2, s2.lim_inf lim_inf2, s2.lim_sup lim_sup2, s2.val_defeito val_defeito2, s2.unid_1 unid_12, " & _
                            " s2.unid_2 unid_22, s2.unid_uso unid_uso2, s2.fac_conv_unid fac_conv_unid2, req.cod_efr "

                    If gSGBD = gOracle Then
                        gSqlSelResFROM2 = " FROM sl_recebidos recebidos , " & _
                                          " sl_requis req, sl_identif id, slv_analises, " & _
                                          " sl_perfis p, sl_ana_c c, sl_ana_s s, sl_segundo_res s2 "
                                          
                        gSqlSelResWHERE2 = " WHERE req.n_req = recebidos.n_req " & _
                                " AND id.seq_utente = req.seq_utente " & _
                                " AND recebidos.dt_chega IS NOT NULL " & _
                                " AND recebidos.cod_agrup = slv_analises.cod_ana " & _
                                " AND recebidos.cod_perfil = p.cod_perfis (+) " & _
                                " AND recebidos.cod_ana_c = c.cod_ana_c(+) " & _
                                " AND recebidos.cod_ana_s  = s.cod_ana_s (+) " & _
                                " AND recebidos.cod_ana_s = s2.cod_ana_s (+) "
                    
                    ElseIf gSGBD = gSqlServer Then
                        
                        gSqlSelResFROM2 = " FROM sl_recebidos recebidos LEFT OUTER JOIN sl_perfis p on (recebidos.cod_perfil = p.cod_perfis) " & _
                                          " LEFT OUTER JOIN sl_ana_c c on (recebidos.cod_ana_c = c.cod_ana_c) " & _
                                          " LEFT OUTER JOIN sl_ana_s s on (recebidos.cod_ana_s = s.cod_ana_s) " & _
                                          " LEFT OUTER JOIN sl_segundo_res s2 on (recebidos.cod_ana_s = s2.cod_ana_s), " & _
                                          " sl_requis req, sl_identif id, slv_analises "
                        
                        gSqlSelResWHERE2 = " WHERE req.n_req = recebidos.n_req " & _
                                " AND id.seq_utente = req.seq_utente " & _
                                " AND recebidos.dt_chega IS NOT NULL " & _
                                " AND recebidos.cod_agrup = slv_analises.cod_ana "

                    End If
                    gModoRes = cModoResValApar
                Else
                    BG_Mensagem mediMsgBox, "Identifica��o de aparelho obrigat�ria!", vbInformation, "Procurar"
                    Exit Sub
                End If
        End Select
        
        
        'Pesquisa por utilizador
        If gModoRes <> cModoResIns Then
            If CbUtilizadores.ListIndex <> -1 Then
                'soliveira terrugem
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND  realiza.user_cri = " & BL_TrataStringParaBD(CbUtilizadores.ItemData(CbUtilizadores.ListIndex))
                'gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND  recebidos.user_cri = " & BL_TrataStringParaBD(CbUtilizadores.ItemData(CbUtilizadores.ListIndex))
            End If
        End If
        
        
        If Trim(EcNumReq.Text) <> "" Then
            If gModoRes = cModoResIns Then
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.n_req = " & EcNumReq.Text
            ElseIf gModoRes = cModoResValApar Then
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND recebidos.n_req = " & EcNumReq.Text
            Else
                If gSqlSelResWHERE2 <> "" Then
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND realiza.n_req = " & BL_TrataStringParaBD(EcNumReq.Text)
                End If
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    If gSqlSelResWHERE1 <> "" Then
                        gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.n_req = " & BL_TrataStringParaBD(EcNumReq.Text)
                    End If
                End If
            End If
        Else
            If Trim(EcSeqUteReq.Text) <> "" Then
            ElseIf Trim(EcCodUteSeq.Text) <> "" Then
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND req.seq_utente = " & BL_TrataStringParaBD(EcCodUteSeq.Text)
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND req.seq_utente = " & BL_TrataStringParaBD(EcCodUteSeq.Text)
                End If
            End If
            If CbSituacao.ListIndex <> mediComboValorNull Then
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND req.t_sit= " & BG_DaComboSel(CbSituacao)
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND req.t_sit= " & BG_DaComboSel(CbSituacao)
                End If
            End If
            If CbUrgencia.ListIndex <> mediComboValorNull Then
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND req.t_urg = " & BL_TrataStringParaBD(Mid(CbUrgencia.Text, 1, 1))
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND req.t_urg = " & BL_TrataStringParaBD(Mid(CbUrgencia.Text, 1, 1))
                End If
            End If
        End If
                    
        If Trim(EcCodProveniencia.Text) <> "" Then
            gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND req.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
            If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND req.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
            End If
        End If
                    
        If Trim(EcCodSala.Text) <> "" Then
            gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND req.cod_sala=" & BL_TrataStringParaBD(EcCodSala.Text)
            If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND req.cod_sala=" & BL_TrataStringParaBD(EcCodSala.Text)
            End If
        End If
        
        If Trim(EcCodAna.Text) <> "" Then
            If gModoRes = cModoResIns Then
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND ( marca.cod_perfil = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR marca.cod_ana_c = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR marca.cod_ana_s = " & BL_TrataStringParaBD(EcCodAna.Text) & ")"
            ElseIf gModoRes = cModoResValApar Then
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND ( recebidos.cod_perfil = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR recebidos.cod_ana_c = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR recebidos.cod_ana_s = " & BL_TrataStringParaBD(EcCodAna.Text) & ")"
            Else
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND ( realiza.cod_perfil = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR realiza.cod_ana_c = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR realiza.cod_ana_s = " & BL_TrataStringParaBD(EcCodAna.Text) & ")"
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND ( marca.cod_perfil = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR marca.cod_ana_c = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR marca.cod_ana_s = " & BL_TrataStringParaBD(EcCodAna.Text) & ")"
                End If
            End If
        End If
'        gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND slv_analises.cod_ana = marca.cod_agrup "
'        If gModoRes = cModoResValApar Then
'            gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND slv_analises.cod_ana = recebidos.cod_agrup "
'            'gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND slv_analises.cod_ana = realiza.cod_agrup "
'        ElseIf gModoRes <> cModoResIns Then
'            gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND slv_analises.cod_ana = realiza.cod_agrup "
'        End If
        If Trim(EcCodSgrp.Text) <> "" Or Trim(EcCodGrp.Text) <> "" Then
            If gModoRes = cModoResIns Then
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND ( marca.cod_perfil in ( select cod_perfis from sl_perfis where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " OR marca.cod_ana_c in ( select cod_ana_c from sl_ana_c where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " OR marca.cod_ana_s in ( select cod_ana_s from sl_ana_s where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ") )"
            ElseIf gModoRes = cModoResValApar Then
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND ( recebidos.cod_perfil in ( select cod_perfis from sl_perfis where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " OR recebidos.cod_ana_c in ( select cod_ana_c from sl_ana_c where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " OR recebidos.cod_ana_s in ( select cod_ana_s from sl_ana_s where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ") )"
            Else
                'soliveira hcvp
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND slv_analises.cod_gr_ana = " & BL_TrataStringParaBD(EcCodGrp)
                'gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND ( realiza.cod_perfil in ( select cod_perfis from sl_perfis where " & IIf(Trim(EcCodGrp.text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.text), "") & IIf(Trim(EcCodSgrp.text) <> "" And Trim(EcCodGrp.text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.text), "") & ")"
                'gSqlSelResWHERE2 = gSqlSelResWHERE2 & " OR realiza.cod_ana_c in ( select cod_ana_c from sl_ana_c where " & IIf(Trim(EcCodGrp.text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.text), "") & IIf(Trim(EcCodSgrp.text) <> "" And Trim(EcCodGrp.text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.text), "") & ")"
                'gSqlSelResWHERE2 = gSqlSelResWHERE2 & " OR realiza.cod_ana_s in ( select cod_ana_s from sl_ana_s where " & IIf(Trim(EcCodGrp.text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.text), "") & IIf(Trim(EcCodSgrp.text) <> "" And Trim(EcCodGrp.text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.text), "") & ") )"
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    'soliveira hcvp
                    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND slv_analises.cod_gr_ana = " & BL_TrataStringParaBD(EcCodGrp)
                    
                    'gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND ( marca.cod_perfil in ( select cod_perfis from sl_perfis where " & IIf(Trim(EcCodGrp.text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.text), "") & IIf(Trim(EcCodSgrp.text) <> "" And Trim(EcCodGrp.text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.text), "") & ")"
                    'gSqlSelResWHERE1 = gSqlSelResWHERE1 & " OR marca.cod_ana_c in ( select cod_ana_c from sl_ana_c where " & IIf(Trim(EcCodGrp.text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.text), "") & IIf(Trim(EcCodSgrp.text) <> "" And Trim(EcCodGrp.text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.text), "") & ")"
                    'gSqlSelResWHERE1 = gSqlSelResWHERE1 & " OR marca.cod_ana_s in ( select cod_ana_s from sl_ana_s where " & IIf(Trim(EcCodGrp.text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.text), "") & IIf(Trim(EcCodSgrp.text) <> "" And Trim(EcCodGrp.text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.text), "") & ") )"
                End If
            End If
        End If
        
        If Trim(EcFolhaTrab.Text) <> "" Then
            If gModoRes = cModoResIns Then
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.n_folha_trab = " & EcFolhaTrab.Text
            ElseIf gModoRes = cModoResValApar Then
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND recebidos.n_folha_trab = " & EcFolhaTrab.Text
            Else
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND realiza.n_folha_trab = " & EcFolhaTrab.Text
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.n_folha_trab = " & EcFolhaTrab.Text
                End If
            End If
        End If
                
        If Trim(EcDtChegaProd1.Text) <> "" And Trim(EcDtChegaProd2.Text) <> "" Then
            If gModoRes = cModoResIns Then
                gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.Text) & _
                                    " AND " & BL_TrataDataParaBD(EcDtChegaProd2.Text)
            ElseIf gModoRes = cModoResValApar Then
                If gSGBD = gOracle Then
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND recebidos.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.Text) & " AND " & BL_TrataDataParaBD(EcDtChegaProd2.Text)
                ElseIf gSGBD = gSqlServer Then
                    'gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND convert(varchar,recebidos.dt_chega,105) BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.text) & " AND " & BL_TrataDataParaBD(EcDtChegaProd2.text)
                    gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND recebidos.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.Text) & " AND " & BL_TrataDataParaBD(EcDtChegaProd2.Text)
                    'gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND convert(varchar,realiza.dt_chega,105) BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.text) & " AND " & BL_TrataDataParaBD(EcDtChegaProd2.text)
                End If
                
            Else
                gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND realiza.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.Text) & _
                                    " AND " & BL_TrataDataParaBD(EcDtChegaProd2.Text)
                If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.Text) & _
                        " AND " & BL_TrataDataParaBD(EcDtChegaProd2.Text)
                End If
            End If
        End If
        
        'LOCAL CHVNG-Espinho filtra apenas as an�lises cujo local executante seja o activo
        gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND slv_analises.cod_ana = (SELECT cod_ana FROM sl_ana_locais_exec WHERE sl_ana_locais_exec.cod_ana = slv_analises.cod_ana AND sl_ana_locais_exec.cod_local = " & gCodLocal & " )"
        gSqlSelResWHERE2 = gSqlSelResWHERE2 & " AND slv_analises.cod_ana = (SELECT cod_ana FROM sl_ana_locais_exec WHERE sl_ana_locais_exec.cod_ana = slv_analises.cod_ana AND  sl_ana_locais_exec.cod_local = " & gCodLocal & " )"
        
        If (gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro) And gSqlSelResWHERE2 <> "" Then
            gSqlSelResWHERE2 = gSqlSelResWHERE2 & ")"
        End If
        
     
        
        'Order
        If Trim(EcFolhaTrab.Text) <> "" Then
            If gModoRes = cModoResIns Then
                gSqlSelResORDER = " ORDER BY marca.n_req,ord_produto, cod_gr_ana , marca.ord_ana, marca.cod_agrup, marca.cod_perfil, marca.ord_ana_perf, ana_c, marca.ord_ana_compl, ana_s"
            ElseIf gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                If gSGBD = gOracle Then
                    gSqlSelResORDER = " ORDER BY n_req,ord_produto, cod_gr_ana , ord_ana, cod_agrup, cod_perfil, ord_ana_perf, " & _
                                 "ana_c, ord_ana_compl, ana_s "
                End If
                ' n_req Alias numero_req
                If gSGBD = gSqlServer Then
                    gSqlSelResORDER = " ORDER BY numero_req,ord_produto, cod_gr_ana , ord_ana, cod_agrup, cod_perfil, ord_ana_perf, " & _
                                 "ana_c, ord_ana_compl, ana_s "
                End If
            Else
                gSqlSelResORDER = " ORDER BY realiza.n_req,ord_produto, cod_gr_ana , realiza.ord_ana, realiza.cod_agrup, realiza.cod_perfil, realiza.ord_ana_perf, ana_c, realiza.ord_ana_compl, ana_s "
            End If
        Else
            If gModoRes = cModoResIns Then
                gSqlSelResORDER = " ORDER BY marca.n_req,ord_produto, cod_gr_ana , marca.ord_ana, marca.cod_agrup, marca.cod_perfil, marca.ord_ana_perf, ana_c, marca.ord_ana_compl, ana_s"
            ElseIf gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResValApar Then
                If gSGBD = gOracle Then
                    gSqlSelResORDER = " ORDER BY n_req,ord_produto, cod_gr_ana , ord_ana, cod_agrup, cod_perfil, ord_ana_perf, " & _
                                "ana_c, ord_ana_compl, ana_s "
                ElseIf gSGBD = gSqlServer Then
                    If gModoRes = cModoResValApar Then
                        gSqlSelResORDER = " ORDER BY n_req, ord_produto, cod_gr_ana , ord_ana, cod_agrup, recebidos.cod_perfil, ord_ana_perf, " & _
                                "ana_c, ord_ana_compl, ana_s "
                    Else
                        gSqlSelResORDER = " ORDER BY n_req, ord_produto, cod_gr_ana , ord_ana, cod_agrup, cod_perfil, ord_ana_perf, " & _
                                "ana_c, ord_ana_compl, ana_s "
                    End If
                End If
            ElseIf gModoRes = cModoResMicro Then
                If gSGBD = gOracle Then
                    gSqlSelResORDER = " ORDER BY n_req,ord_produto, cod_gr_ana , ord_ana, cod_agrup, cod_perfil, ord_ana_perf, " & _
                                "ana_c, ord_ana_compl, ana_s, n_res_frase, ord_frase "
                End If
                ' n_req Alias numero_req
                If gSGBD = gSqlServer Then
                    gSqlSelResORDER = " ORDER BY n_req,ord_produto, cod_gr_ana , ord_ana, cod_agrup, cod_perfil, ord_ana_perf, " & _
                                "ana_c, ord_ana_compl, ana_s, n_res_frase, ord_frase "
                End If
            Else
                gSqlSelResORDER = " ORDER BY realiza.n_req,ord_produto, cod_gr_ana , realiza.ord_ana, realiza.cod_agrup, realiza.cod_perfil, realiza.ord_ana_perf, ana_c, realiza.ord_ana_compl, ana_s "
            End If
        End If
        
        If gSqlSelResSELECT1 <> "" Then
            gSqlSelRes = gSqlSelResSELECT1 & gSqlSelResFROM1 & gSqlSelResWHERE1 & gSqlSelResSELECT2 & gSqlSelResFROM2 & gSqlSelResWHERE2 & gSqlSelResORDER
        Else
            gSqlSelRes = gSqlSelResSELECT2 & gSqlSelResFROM2 & gSqlSelResWHERE2 & gSqlSelResORDER
        End If
        If gModoRes = cModoResMicro Then
            FormResMicro.Show
        Else
            FormResultados.Show
            FormResultados.ModoPesquisa = modo
        End If
    End If
    
End Sub


Sub FuncaoSeguinte()

End Sub

Sub FuncaoAnterior()

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormSelRes = Nothing
    gF_SELRES = 0

End Sub

Sub PreencheValoresDefeito()

    'Preenche combo sexo do utente
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    'Preenche combo situa��o da requisi��o
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    'Preenche combo tipo de utente
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    
    'Preenche combo urgencia da requisi��o
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
    'Preenche combo estado da requisi��o
    CbEstadoReq.AddItem "Sem an�lises"
    CbEstadoReq.AddItem "Espera de produtos"
    CbEstadoReq.AddItem "Espera de resultados"
    CbEstadoReq.AddItem "Espera de valida��o m�dica"
    CbEstadoReq.AddItem "Cancelada"
    CbEstadoReq.AddItem "Valida��o m�dica completa"
    CbEstadoReq.AddItem "N�o passar a hist�rico"
    CbEstadoReq.AddItem "Todas impressas"
    CbEstadoReq.AddItem "Hist�rico"
    CbEstadoReq.AddItem "Resultados parciais"
    CbEstadoReq.AddItem "Valida��o m�dica parcial"
    CbEstadoReq.AddItem "Impress�o parcial"
    CbEstadoReq.AddItem "Assinatura da Requisi��o"
    CbEstadoReq.AddItem "Bloqueada"
    
    'Preenche combo utilizadores (User_Cri) dos Resultados
    BG_PreencheComboBD_ADO "SELECT nome,Cod_Utilizador FROM sl_idutilizador,sl_gr_util WHERE sl_idutilizador.cod_gr_util = sl_gr_util.cod_gr_util and flg_removido=0 and descr_gr_util = 'Aparelhos' ", "Cod_Utilizador", "Nome", CbUtilizadores
    PreencheSalaDefeito
End Sub

Sub DefTipoCampos()

    EcNumReq.Tag = adInteger
    EcNumReq.MaxLength = 10
    EcDataChega.Tag = adDate
    EcDataChega.MaxLength = 10
    EcEpisodio.Tag = adVarChar
    EcEpisodio.MaxLength = 20
    EcUtente.Tag = adVarChar
    EcUtente.MaxLength = 20
    EcCodGrp.Tag = adVarChar
    EcCodGrp.MaxLength = 5
    EcCodSgrp.Tag = adVarChar
    EcCodSgrp.MaxLength = 5
    EcCodAna.Tag = adVarChar
    EcCodAna.MaxLength = 5
    EcResult.Tag = adVarChar
    EcFolhaTrab.Tag = adInteger
    EcFolhaTrab.MaxLength = 10
    EcDtChegaProd1.Tag = adDate
    EcDtChegaProd1.MaxLength = 10
    EcDtChegaProd2.Tag = adDate
    EcDtChegaProd2.MaxLength = 10
    
End Sub

Sub Inicializacoes()
    Dim op As Long
    
    LaRes.Enabled = True
    EcResult.Enabled = True
    CbUtilizadores.Enabled = True
    LaUser.Enabled = True
    
    op = -1

        CbModoTrab.List(0) = "Resultados Microbiologia"
    
    If gModoRes = cModoResIns Then
        CbModoTrab.ListIndex = 0
        gModoRes = cModoResIns
        LaRes.Enabled = False
        EcResult.Enabled = False
        CbUtilizadores.Enabled = False
        LaUser.Enabled = False
    ElseIf gModoRes = cModoResVal Then
        CbModoTrab.ListIndex = 0
        gModoRes = cModoResVal
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
    ElseIf gModoRes = cModoResValAValTec Then
        CbModoTrab.ListIndex = 0
        gModoRes = cModoResValAValTec
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
    ElseIf gModoRes = cModoResInsVal Then
        CbModoTrab.ListIndex = 0
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
    ElseIf gModoRes = cModoResGeral Then
        CbModoTrab.ListIndex = 0
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
    ElseIf gModoRes = cModoResMicro Then
        CbModoTrab.ListIndex = 0
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
    ElseIf gModoRes = cModoResValApar Then
        CbModoTrab.ListIndex = 0
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
    End If
    
    
    Me.caption = "Pesquisar Resultados"
    Me.left = 440
    Me.top = 10
    Me.Width = 7425
    Me.Height = 7650 ' Normal
    'Me.Height = 9000 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    
    LstReq.Visible = False

End Sub

Sub FuncaoLimpar()
        
        LimpaCampos
        CampoDeFocus.SetFocus

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        
        LimpaCampos
        CampoDeFocus.SetFocus
                
    Else
        Unload Me
    End If

End Sub

Sub PreencheCampos()

End Sub

Sub LimpaCampos()
    
    Dim i As Integer
    
    Me.SetFocus
    
    If gModoRes = cModoResVal Or gModoRes = cModoResValApar Then
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
        LaRes.Enabled = True
    Else
        EcResult.Enabled = False
        CbUtilizadores.Enabled = False
        LaUser.Enabled = False
        LaRes.Enabled = False
    End If
    
    EcNumReq.Enabled = True
    EcNumReqAssoc.Enabled = True
    
    LaNumero.Enabled = True
    EcDataChega.Enabled = True
    LaChega.Enabled = True
    EcEpisodio.Enabled = True
    LaEpis.Enabled = True
    CbSituacao.Enabled = True
    LaSit.Enabled = True
    CbUrgencia.Enabled = True
    LaUrg.Enabled = True
    CbEstadoReq.Enabled = True
    CbTipoUtente.Enabled = True
    LbUtente.Enabled = True
    EcNome.Enabled = True
    LbNome.Enabled = True
    EcProcHosp1.Enabled = True
    EcProcHosp2.Enabled = True
    LbProcesso.Enabled = True
    EcUtente.Enabled = True
    EcDtChegaProd1.Enabled = True
    EcDtChegaProd2.Enabled = True
    LaDtCheP.Enabled = True
    BtPesquisaUtente.Enabled = True
    EcCartaoUte.Enabled = True
    LbCartaoUte.Enabled = True
    EcDescrSexo.Enabled = True
    LbSexo.Enabled = True
    EcDataNasc.Enabled = True
    LbDataNasc.Enabled = True
    
    EcNumReq.Text = ""
    EcNumReqAssoc.Text = ""
    EcDataChega.Text = ""
    EcEpisodio.Text = ""
    EcUrgencia.Text = ""
    EcDescrSexo.Text = ""
    EcDataNasc.Text = ""
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbEstadoReq.ListIndex = mediComboValorNull
    CbTipoUtente.ListIndex = mediComboValorNull
    CbUtilizadores.ListIndex = mediComboValorNull
    EcNome.Text = ""
    EcCodUteSeq.Text = ""
    EcSeqUteReq.Text = ""
    If BL_SeleccionaGrupoAnalises = True Then
        EcCodGrp.Text = gCodGrAnaUtilizador
    Else
        EcCodGrp.Text = ""
    End If
    EcCodGrp_Validate False
    
    EcCodSgrp.Text = ""
    EcCodAna.Text = ""
    EcDescrAna.Text = ""
    EcDescrSgrp.Text = ""
    EcResult.Text = ""
    EcProcHosp1.Text = ""
    EcProcHosp2.Text = ""
    EcUtente.Text = ""
    EcFolhaTrab.Text = ""
    EcGrAna.Text = ""
    EcSgrAna.Text = ""
    EcDtChegaProd1.Text = ""
    EcDtChegaProd2.Text = ""
    EcUrgencia.Text = ""
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    EcCodSala = ""
    EcDescrSala = ""
    EcReqAux.Text = ""
    'LstReq.Clear
    EcEstadoReq = ""
    CampoDeFocus.SetFocus
    PreencheSalaDefeito
End Sub

Function SelDadosUtente() As Boolean
    
    Dim sql As String
    Dim Tabela As ADODB.recordset
    Dim Preencheu As Boolean
   
    Preencheu = False
    
    'Limpa a estrutura de dados do utente
    If CbTipoUtente.ListIndex <> -1 And EcUtente.Text <> "" Then
        'Procurar o sequencial e chamar a fun��o que preenche a identifica��o do utente
        Set Tabela = New ADODB.recordset
        sql = "SELECT seq_utente FROM sl_identif WHERE utente='" & EcUtente.Text & "' AND t_utente='" & CbTipoUtente.Text & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbExclamation, "ATEN��O"
            CbTipoUtente.ListIndex = mediComboValorNull
            EcUtente.Text = ""
            EcCodUteSeq.Text = ""
        Else
            If Trim(EcSeqUteReq.Text) <> "" And Trim(BL_HandleNull(Tabela!seq_utente, "")) <> Trim(EcSeqUteReq.Text) Then
                BG_Mensagem mediMsgBox, "O utente escolhido e o da requisi��o n�o � o mesmo!", vbOKOnly + vbExclamation, "Requisi��o"
                CbTipoUtente.ListIndex = mediComboValorNull
                EcUtente.Text = ""
                EcCodUteSeq.Text = ""
                Preencheu = True
            Else
                EcCodUteSeq.Text = BL_HandleNull(Tabela!seq_utente, "")
                PreencheIdentificacaoUtente
            End If
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

    SelDadosUtente = Preencheu

End Function

Private Sub BtPesqRapAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Button = 1 Then
'        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAREQ
        BL_PesquisaAnalises
    End If

End Sub

Private Sub BtPesqRapGgrp_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_gr_ana"
    CampoPesquisa = "descr_gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Grupos de An�lises")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FormSelRes.EcCodGrp.Text = resultados(1)
            FormSelRes.EcDescrGrp.Text = ""
            FormSelRes.EcCodGrp.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem grupos codificados!", vbExclamation, "ATEN��O"
        FormSelRes.EcCodGrp.SetFocus
    End If
    
End Sub

Private Sub BtPesqReq_Click()

    Dim sql As String
    Dim Sql2 As String
    Dim Sql3 As String
    Dim Sql4 As String
    Dim rsReq As ADODB.recordset

    LstReq.Visible = True
    LstReq.Clear
    
    sql = ""
    Sql2 = ""
    Sql3 = ""
    Sql4 = ""
    
    If gModoRes = cModoResIns Then
        'Introdu��o de Resultados
        sql = "SELECT distinct(x1.n_req) FROM sl_requis x1, sl_marcacoes x2" & _
            " WHERE X1.n_req = X2.n_req And X2.dt_chega IS NOT NULL AND x1.estado_req NOT IN ('I','C','H')"

        ConstruirSqlPesquisaReq sql, Sql2

        sql = sql & " ORDER BY x1.n_req ASC"

        Set rsReq = New ADODB.recordset
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        rsReq.Open sql, gConexao
        If rsReq.RecordCount > 0 Then
            rsReq.MoveFirst
            While Not rsReq.EOF
                LstReq.AddItem rsReq!n_req
                LstReq.ItemData(LstReq.NewIndex) = rsReq!n_req

                rsReq.MoveNext
            Wend
        End If
        rsReq.Close
        Set rsReq = Nothing
'    ElseIf gModoRes = cModoResTecVal Or gModoRes = cModoResAlt Then
'        'Valida��o T�cnica
'        Sql = "SELECT distinct(x1.n_req) FROM sl_requis x1, sl_realiza x2 " & _
'            " WHERE x1.n_req=x2.n_req AND x2.dt_chega IS NOT NULL AND " & _
'            " x2.flg_estado=1 AND x1.estado_req NOT IN ('I','C','H')"
'
'        ConstruirSqlPesquisaReq Sql, Sql2
'
'        Sql = Sql & " ORDER BY x1.n_req ASC"
'
'        Set RsReq = New ADODB.Recordset
'        RsReq.CursorLocation = adUseServer
'        RsReq.CursorType = adOpenStatic
'        RsReq.Open Sql, gConexao
'        If RsReq.RecordCount > 0 Then
'            RsReq.MoveFirst
'            While Not RsReq.EOF
'                LstReq.AddItem RsReq!n_req
'                LstReq.ItemData(LstReq.NewIndex) = RsReq!n_req
'
'                RsReq.MoveNext
'            Wend
'        End If
'        RsReq.Close
'        Set RsReq = Nothing
'    ElseIf gModoRes = cModoResTecAltVal Then
'        'Altera��o de Validados T�cnicos
'        Sql = "SELECT distinct(x1.n_req) FROM sl_requis x1, sl_realiza x2 " & _
'            " WHERE x1.n_req=x2.n_req AND x2.dt_chega IS NOT NULL AND " & _
'            " x2.flg_estado=5 AND x1.estado_req NOT IN ('I','C','H')"
'
'        ConstruirSqlPesquisaReq Sql, Sql2
'
'        Sql = Sql & " ORDER BY x1.n_req ASC"
'
'        Set RsReq = New ADODB.Recordset
'        RsReq.CursorLocation = adUseServer
'        RsReq.CursorType = adOpenStatic
'        RsReq.Open Sql, gConexao
'        If RsReq.RecordCount > 0 Then
'            RsReq.MoveFirst
'            While Not RsReq.EOF
'                LstReq.AddItem RsReq!n_req
'                LstReq.ItemData(LstReq.NewIndex) = RsReq!n_req
'
'                RsReq.MoveNext
'            Wend
'        End If
'        RsReq.Close
'        Set RsReq = Nothing
    ElseIf gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Then
        'Introdu��o e Valida��o M�dica de Resultados
        Sql2 = "SELECT distinct(x1.n_req) FROM sl_requis x1, sl_marcacoes x2" & _
            " WHERE X1.n_req = X2.n_req And X2.dt_chega IS NOT NULL AND x1.estado_req NOT IN ('I','C','H') "
        
            ConstruirSqlPesquisaReq sql, Sql2
        
        Sql3 = " UNION "
        
        sql = " SELECT distinct(x1.n_req) FROM sl_requis x1, sl_realiza x2 " & _
            " WHERE x1.n_req=x2.n_req AND x2.dt_chega IS NOT NULL AND " & _
            " x1.estado_req NOT IN ('I','C','H') "
'            " (x2.flg_estado=1 OR x2.flg_estado=5) AND x1.estado_req NOT IN ('I','C','H') "
            
            ConstruirSqlPesquisaReq sql, Sql2
            
        If (gSGBD = gSqlServer) Then
            Sql4 = Sql2 & Sql3 & sql & " ORDER BY x1.n_req ASC"
        Else
            Sql4 = Sql2 & Sql3 & sql & " ORDER BY n_req ASC"
        End If
        
        Set rsReq = New ADODB.recordset
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        rsReq.Open Sql4, gConexao
        If rsReq.RecordCount > 0 Then
            rsReq.MoveFirst
            While Not rsReq.EOF
                LstReq.AddItem rsReq!n_req
                LstReq.ItemData(LstReq.NewIndex) = rsReq!n_req
                
                rsReq.MoveNext
            Wend
        End If
        rsReq.Close
        Set rsReq = Nothing
        
    ElseIf gModoRes = cModoResVal Or gModoRes = cModoResValApar Then
        'Valida��o M�dica
        sql = "SELECT distinct(x1.n_req) FROM sl_requis x1, sl_realiza x2 " & _
            " WHERE x1.n_req=x2.n_req AND x2.dt_chega IS NOT NULL AND " & _
            " (x2.flg_estado=1 OR x2.flg_estado=5) AND x1.estado_req NOT IN ('I','C','H')"

        ConstruirSqlPesquisaReq sql, Sql2

        sql = sql & " ORDER BY x1.n_req ASC"

        Set rsReq = New ADODB.recordset
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        rsReq.Open sql, gConexao
        If rsReq.RecordCount > 0 Then
            rsReq.MoveFirst
            While Not rsReq.EOF
                LstReq.AddItem rsReq!n_req
                LstReq.ItemData(LstReq.NewIndex) = rsReq!n_req

                rsReq.MoveNext
            Wend
        End If
        rsReq.Close
        Set rsReq = Nothing
'    ElseIf gModoRes = cModoResAltVal Then
'         'Altera��o de Validados M�dicos
'        Sql = "SELECT distinct(x1.n_req) FROM sl_requis x1, sl_realiza x2 " & _
'            " WHERE x1.n_req=x2.n_req AND x2.dt_chega IS NOT NULL AND " & _
'            " (x2.Flg_Estado = 3 Or x2.Flg_Estado = 4) AND x1.estado_req NOT IN ('I','C','H')"
'
'        ConstruirSqlPesquisaReq Sql, Sql2
'
'        Sql = Sql & " ORDER BY x1.n_req ASC"
'
'        Set RsReq = New ADODB.Recordset
'        RsReq.CursorLocation = adUseServer
'        RsReq.CursorType = adOpenStatic
'        RsReq.Open Sql, gConexao
'        If RsReq.RecordCount > 0 Then
'            RsReq.MoveFirst
'            While Not RsReq.EOF
'                LstReq.AddItem RsReq!n_req
'                LstReq.ItemData(LstReq.NewIndex) = RsReq!n_req
'
'                RsReq.MoveNext
'            Wend
'        End If
'        RsReq.Close
'        Set RsReq = Nothing
    End If

End Sub

Private Sub BtPesquisaProveniencia_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CampoPesquisa = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Proveni�ncia")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem proveni�ncias codificados!", vbExclamation, "ATEN��O"
        EcCodProveniencia.SetFocus
    End If


End Sub

Private Sub BtPesquisaSalas_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_sala"
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CampoPesquisa = "descr_sala"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Salas")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodSala.Text = resultados(1)
            EcDescrSala.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Salas codificadas!", vbExclamation, "ATEN��O"
        EcCodSala.SetFocus
    End If


End Sub
Private Sub BtPesquisaUtente_Click()
    
    FormPesquisaUtentes.Show
    Flg_PesqUtente = True

End Sub

Private Sub BtPesqRapSgrp_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_sgr_ana"
    CamposEcran(1) = "cod_sgr_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sgr_ana"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_sgr_ana"
    CampoPesquisa = "descr_sgr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " SubGrupos de An�lises")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FormSelRes.EcCodSgrp.Text = resultados(1)
            FormSelRes.EcDescrSgrp.Text = ""
            FormSelRes.EcCodSgrp.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem subgrupos codificados!", vbExclamation, "ATEN��O"
        FormSelRes.EcCodSgrp.SetFocus
    End If

End Sub

Private Sub CbModoTrab_Click()

    LstReq.Clear
    EcCodGrp.Text = ""
    EcDescrGrp.Text = ""
    EcCodGrp.Text = gCodGrAnaUtilizador
    EcCodGrp_Validate False
    
    EcResult.Enabled = True
    CbUtilizadores.Enabled = True
    CbSituacao.Enabled = True
    CbUrgencia.Enabled = True
    EcCodProveniencia.Enabled = True
    BtPesquisaProveniencia.Enabled = True
    EcCodGrp.Enabled = True
    BtPesqRapGgrp.Enabled = True
    EcCodSgrp.Enabled = True
    BtPesqRapSGrp.Enabled = True
    EcCodAna.Enabled = True
    BtPesqRapAna.Enabled = True
    EcResult.Enabled = True
    EcFolhaTrab.Enabled = True
    EcDtChegaProd1.Enabled = True
    EcDtChegaProd2.Enabled = True
    CbUtilizadores.Enabled = False

    If CbModoTrab.List(CbModoTrab.ListIndex) = "Introdu��o de Resultados" Then
        gModoRes = cModoResIns
    ElseIf CbModoTrab.List(CbModoTrab.ListIndex) = "Valida��o de Resultados" Then
        gModoRes = cModoResVal
    ElseIf CbModoTrab.List(CbModoTrab.ListIndex) = "Valida��o de Resultados Ap�s Val. T�cnica" Then
        gModoRes = cModoResValAValTec
    ElseIf CbModoTrab.List(CbModoTrab.ListIndex) = "Introdu��o e Valida��o de Resultados" Then
        gModoRes = cModoResInsVal
    ElseIf CbModoTrab.List(CbModoTrab.ListIndex) = "Resultados Geral" Then
        gModoRes = cModoResGeral
    ElseIf CbModoTrab.List(CbModoTrab.ListIndex) = "Resultados Microbiologia" Then
        gModoRes = cModoResMicro
    ElseIf CbModoTrab.List(CbModoTrab.ListIndex) = "Assinatura de Requisi��o" Then
        gModoRes = cModoResAssinatura
    ElseIf CbModoTrab.List(CbModoTrab.ListIndex) = "Resultados por Aparelho" Then
        gModoRes = cModoResValApar
        CbUtilizadores.Enabled = True
    End If

    If gModoRes = cModoResIns Then
        LaRes.Enabled = False
        EcResult.Enabled = False
        CbUtilizadores.Enabled = False
        LaUser.Enabled = False
    ElseIf gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Then
        LaRes.Enabled = False
        EcResult.Enabled = False
        CbUtilizadores.Enabled = False
        LaUser.Enabled = False
    ElseIf gModoRes = cModoResMicro Then
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
        EcCodGrp.Tag = adVarChar
        EcCodGrp.MaxLength = 5
        If gCodGrAnaUtilizador = "" Then
            EcCodGrp.Text = gCodGrupoMicrobiologia
        Else
            EcCodGrp.Text = gCodGrAnaUtilizador
        End If
        EcCodGrp_Validate False
        
    ElseIf gModoRes = cModoResAssinatura Then
        EcResult = ""
        EcResult.Enabled = False
        CbUtilizadores.ListIndex = mediComboValorNull
        CbUtilizadores.Enabled = False
        CbSituacao.ListIndex = mediComboValorNull
        CbSituacao.Enabled = False
        CbSituacao.ListIndex = mediComboValorNull
        CbUrgencia.Enabled = False
        EcCodProveniencia = ""
        EcCodProveniencia_LostFocus
        EcCodProveniencia.Enabled = False
        BtPesquisaProveniencia.Enabled = False
        EcCodGrp = ""
        EcCodGrp_Validate True
        EcCodGrp.Enabled = False
        BtPesqRapGgrp.Enabled = False
        EcCodSgrp = ""
        EcCodSgrp_Validate True
        EcCodSgrp.Enabled = False
        BtPesqRapSGrp.Enabled = False
        EcCodAna = ""
        EcCodana_Validate True
        EcCodAna.Enabled = False
        BtPesqRapAna.Enabled = False
        EcResult = ""
        EcResult_Validate False
        EcResult.Enabled = False
        EcFolhaTrab = ""
        EcFolhaTrab_Validate False
        EcFolhaTrab.Enabled = False
        EcDtChegaProd1.Enabled = False
        EcDtChegaProd1 = ""
        EcDtChegaProd1_Validate False
        EcDtChegaProd2.Enabled = False
        EcDtChegaProd2 = ""
        EcDtChegaProd2_Validate False
    Else
        LaRes.Enabled = True
        EcResult.Enabled = True
        CbUtilizadores.Enabled = True
        LaUser.Enabled = True
    End If

    If EcNumReq.Visible = True Then
        EcNumReq.SetFocus
    End If

End Sub

Private Sub CbModoTrab_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then ' Delete
        CbModoTrab.ListIndex = mediComboValorNull
    End If
        
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbSituacao, KeyCode

End Sub

Private Sub CbTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoUtente, KeyCode

End Sub

Private Sub CbUrgencia_Click()
    
    Select Case CbUrgencia.ListIndex
        Case 0
            EcUrgencia.Text = "N"
        Case 1
            EcUrgencia.Text = "U"
        Case Else
            EcUrgencia.Text = ""
    End Select

End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUrgencia, KeyCode

End Sub

Private Sub CbUtilizadores_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUtilizadores_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUtilizadores, KeyCode

End Sub

Private Sub cmdOK_Click()
    
    FuncaoProcurar

End Sub

Private Sub EcCodAna_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodana_Validate(Cancel As Boolean)

    Dim rsDescr As ADODB.recordset
    Dim iRes As Integer
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    If Trim(EcCodAna.Text) = "" Then
        EcDescrAna.Text = ""
        Exit Sub
    End If
    
    EcCodAna.Text = UCase(EcCodAna.Text)
    Set rsDescr = New ADODB.recordset
    With rsDescr
        .Source = "(SELECT descr_ana_s as descricao, gr_ana, sgr_ana FROM sl_ana_s WHERE " & _
                    "cod_ana_s ='" & UCase(EcCodAna.Text) & "')" & _
                " UNION " & _
                    "(SELECT descr_ana_c as descricao, gr_ana, sgr_ana FROM sl_ana_c WHERE " & _
                    "cod_ana_c='" & UCase(EcCodAna.Text) & "')" & _
                " UNION " & _
                    "(SELECT descr_perfis as descricao, gr_ana, sgr_ana FROM sl_perfis WHERE " & _
                    "cod_perfis='" & UCase(EcCodAna.Text) & "')"
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Open
    End With
    If rsDescr.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, App.ProductName
        EcCodAna.Text = ""
        Cancel = True
    Else
        EcDescrAna.Text = BL_HandleNull(rsDescr!descricao, "")
        
        If BL_HandleNull(rsDescr!gr_ana, "") <> "" Then
            If Trim(rsDescr!gr_ana) <> Trim(EcCodGrp.Text) And Trim(EcCodGrp.Text) <> "" Then
                BG_Mensagem mediMsgBox, "An�lise n�o pertence ao grupo " & Trim(EcCodGrp.Text) & " (" & Trim(EcDescrGrp.Text) & ")!", vbOKOnly + vbExclamation, "Grupo"
                EcCodAna.Text = ""
                EcDescrAna.Text = ""
                Cancel = True
            Else
                EcCodGrp.Text = BL_HandleNull(rsDescr!gr_ana, "")
                EcGrAna.Text = UCase(BL_HandleNull(rsDescr!gr_ana, ""))
                EcCodGrp_Validate False
            End If
        Else
            EcGrAna.Text = ""
            If Trim(EcCodGrp.Text) <> "" Then
                iRes = BG_Mensagem(mediMsgBox, "A an�lise n�o est� inserida em nenhum grupo." & Chr(13) & "Deseja manter o grupo escolhido como filtro para selec��o de resultados ?", vbYesNo + vbQuestion, "Grupo")
                
                If iRes = vbNo Then
                    EcCodGrp.Text = ""
                    EcDescrGrp.Text = ""
                End If
            End If
        End If
        If BL_HandleNull(rsDescr!sgr_ana, "") <> "" Then
            If Cancel = False Then
                If Trim(rsDescr!sgr_ana) <> Trim(EcCodSgrp.Text) And Trim(EcCodSgrp.Text) <> "" Then
                    BG_Mensagem mediMsgBox, "An�lise n�o pertence ao subgrupo " & Trim(EcCodSgrp.Text) & " (" & Trim(EcDescrSgrp.Text) & ")!", vbOKOnly + vbExclamation, "Subgrupo"
                    EcCodAna.Text = ""
                    EcDescrAna.Text = ""
                    Cancel = True
                Else
                    EcCodSgrp.Text = BL_HandleNull(rsDescr!sgr_ana, "")
                    EcSgrAna.Text = UCase(BL_HandleNull(rsDescr!sgr_ana, ""))
                    EcCodSgrp_Validate False
                End If
            End If
        Else
            EcSgrAna.Text = ""
            If Trim(EcCodSgrp.Text) <> "" Then
                iRes = BG_Mensagem(mediMsgBox, "A an�lise n�o est� inserida em nenhum subgrupo." & Chr(13) & "Deseja manter o subgrupo escolhido como filtro para selec��o de resultados ?", vbYesNo + vbQuestion, "Subgrupo")
                
                If iRes = vbNo Then
                    EcCodSgrp.Text = ""
                    EcDescrSgrp.Text = ""
                End If
            End If
        End If
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing
    
End Sub

Private Sub EcCodGrp_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodGrp_Validate(Cancel As Boolean)

    Dim RsGrp As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrp)
    
    If RTrim(EcCodGrp) = "" Then
        EcDescrGrp.Text = ""
        Exit Sub
    End If
    
    EcCodGrp.Text = UCase(EcCodGrp.Text)
    Set RsGrp = New ADODB.recordset
    
    With RsGrp
        .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text)
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Open , gConexao
    End With
    
    If RsGrp.RecordCount > 0 Then
        EcDescrGrp.Text = BL_HandleNull(RsGrp!descr_gr_ana, "")
        If BL_HandleNull(RsGrp!descr_gr_ana, "") <> "" Then
            If Trim(EcCodGrp.Text) <> Trim(EcGrAna) And Trim(EcGrAna) <> "" Then
                BG_Mensagem mediMsgBox, "A an�lise escolhida n�o pertence ao grupo " & Trim(EcCodGrp.Text) & " (" & Trim(EcDescrGrp.Text) & ")!", vbOKOnly + vbExclamation, "Grupo de An�lises"
                EcDescrGrp.Text = ""
                EcCodGrp.Text = ""
                Cancel = True
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem grupos codificados com esse c�digo!", vbOKOnly + vbExclamation, "Grupo de An�lises"
        Cancel = True
    End If
    
    RsGrp.Close
    Set RsGrp = Nothing
    
End Sub

Private Sub EcCodProveniencia_LostFocus()

    Dim sql As String
    Dim RsProven As ADODB.recordset
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
        Set RsProven = New ADODB.recordset
        RsProven.CursorLocation = adUseServer
        RsProven.CursorType = adOpenStatic
        RsProven.Open sql, gConexao
        If RsProven.RecordCount > 0 Then
            EcDescrProveniencia.Text = BL_HandleNull(RsProven!descr_proven, "")
        End If
        RsProven.Close
        Set RsProven = Nothing
    End If
    
    
End Sub

Private Sub EcCodsala_Validate(Cancel As Boolean)

    Dim sql As String
    Dim rsSala As ADODB.recordset
    
    If Trim(EcCodSala.Text) <> "" Then
        sql = "SELECT descr_sala FROM sl_cod_salas WHERE cod_sala=" & BL_TrataStringParaBD(EcCodSala.Text)
        Set rsSala = New ADODB.recordset
        rsSala.CursorLocation = adUseServer
        rsSala.CursorType = adOpenStatic
        rsSala.Open sql, gConexao
        If rsSala.RecordCount > 0 Then
            EcDescrSala.Text = BL_HandleNull(rsSala!descr_sala, "")
        End If
        rsSala.Close
        Set rsSala = Nothing
    Else
        EcDescrSala = ""
    End If
    
    
End Sub

Private Sub EcCodSgrp_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodSgrp_Validate(Cancel As Boolean)

    Dim RsSgrp As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
        
    If RTrim(EcCodSgrp) = "" Then
        EcDescrSgrp.Text = ""
        Exit Sub
    End If
    
    EcCodSgrp.Text = UCase(EcCodSgrp.Text)
    Set RsSgrp = New ADODB.recordset
    
    With RsSgrp
        .Source = "SELECT descr_sgr_ana FROM sl_sgr_ana WHERE cod_sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text)
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Open , gConexao
    End With
    
    If RsSgrp.RecordCount > 0 Then
        EcDescrSgrp.Text = BL_HandleNull(RsSgrp!descr_sgr_ana, "")
        
        If BL_HandleNull(RsSgrp!descr_sgr_ana, "") <> "" Then
            If Trim(EcCodSgrp.Text) <> Trim(EcSgrAna) And Trim(EcSgrAna) <> "" Then
                BG_Mensagem mediMsgBox, "A an�lise escolhida n�o pertence ao subgrupo " & Trim(EcCodSgrp.Text) & " (" & Trim(EcDescrSgrp.Text) & ")!", vbOKOnly + vbExclamation, "Subgrupo de An�lises"
                EcDescrSgrp.Text = ""
                EcCodSgrp.Text = ""
                Cancel = True
            End If
        End If
        
    Else
        BG_Mensagem mediMsgBox, "N�o existem subgrupos codificados com esse c�digo!", vbOKOnly + vbExclamation, "Subgrupo de An�lises"
        Cancel = True
    End If
    
    RsSgrp.Close
    Set RsSgrp = Nothing

End Sub

Sub Funcao_DataActual()
    
    If Me.ActiveControl.Name = "EcDataChega" Then
        BL_PreencheData EcDataChega, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtChegaProd1" Then
        BL_PreencheData EcDtChegaProd1, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtChegaProd2" Then
        BL_PreencheData EcDtChegaProd2, Me.ActiveControl
    End If

End Sub

Private Sub EcDataChega_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDataChega_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)

End Sub

Private Sub EcDtChegaProd1_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDtChegaProd1_LostFocus()
    
    If EcDtChegaProd1.Text = "" Then
        EcDtChegaProd1.Text = Bg_DaData_ADO
    End If
    
End Sub

Private Sub EcDtChegaProd1_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)

End Sub

Private Sub EcDtChegaProd2_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDtChegaProd2_LostFocus()
    
    If EcDtChegaProd2.Text = "" Then
        EcDtChegaProd2.Text = Bg_DaData_ADO
    End If
    If EcDtChegaProd1.Text = "" Then
        EcDtChegaProd2.Text = ""
    End If

End Sub

Private Sub EcDtChegaProd2_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)

End Sub

Private Sub EcEpisodio_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub Ecepisodio_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)

End Sub

Private Sub EcFolhaTrab_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcFolhaTrab_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    If Trim(EcFolhaTrab.Text) <> "" Then
        EcNumReq.Text = ""
        EcDataChega.Text = ""
        EcEpisodio.Text = ""
        CbSituacao.ListIndex = mediComboValorNull
        CbUrgencia.ListIndex = mediComboValorNull
        CbUtilizadores.ListIndex = mediComboValorNull
        CbEstadoReq.ListIndex = mediComboValorNull
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUrgencia.Text = ""
        EcNome.Text = ""
        EcCodUteSeq.Text = ""
        EcSeqUteReq.Text = ""
        EcCodGrp.Text = ""
        EcCodSgrp.Text = ""
        EcCodAna.Text = ""
        EcDescrAna.Text = ""
        EcDescrGrp.Text = ""
        EcDescrSgrp.Text = ""
        EcResult.Text = ""
        EcProcHosp1.Text = ""
        EcProcHosp2.Text = ""
        EcUtente.Text = ""
        EcGrAna.Text = ""
        EcSgrAna.Text = ""
        EcDtChegaProd1.Text = ""
        EcDtChegaProd2.Text = ""
    
        EcNumReq.Enabled = False
        LaNumero.Enabled = False
        EcDataChega.Enabled = False
        LaChega.Enabled = False
        EcEpisodio.Enabled = False
        LaEpis.Enabled = False
        CbSituacao.Enabled = False
        LaSit.Enabled = False
        CbUrgencia.Enabled = False
        CbUtilizadores.Enabled = False
        LaUser.Enabled = False
        
        LaUrg.Enabled = False
        CbEstadoReq.Enabled = False
        CbTipoUtente.Enabled = False
        LbUtente.Enabled = False
        EcNome.Enabled = False
        LbNome.Enabled = False
        EcResult.Enabled = False
        LaRes.Enabled = False
        EcProcHosp1.Enabled = False
        EcProcHosp2.Enabled = False
        LbProcesso.Enabled = False
        EcUtente.Enabled = False
        EcDtChegaProd1.Enabled = False
        EcDtChegaProd2.Enabled = False
        LaDtCheP.Enabled = False
        BtPesquisaUtente.Enabled = False
        EcCartaoUte.Enabled = False
        LbCartaoUte.Enabled = False
        EcDescrSexo.Enabled = False
        LbSexo.Enabled = False
        EcDataNasc.Enabled = False
        LbDataNasc.Enabled = False
    Else
        EcNumReq.Enabled = True
        LaNumero.Enabled = True
        EcDataChega.Enabled = True
        LaChega.Enabled = True
        EcEpisodio.Enabled = True
        LaEpis.Enabled = True
        CbSituacao.Enabled = True
        
        If gModoRes <> cModoResIns Then
            CbUtilizadores.Enabled = True
            LaUser.Enabled = True
            EcResult.Enabled = True
            LaRes.Enabled = True
        End If
        
        LaSit.Enabled = True
        CbUrgencia.Enabled = True
        LaUrg.Enabled = True
        CbEstadoReq.Enabled = True
        CbTipoUtente.Enabled = True
        LbUtente.Enabled = True
        EcNome.Enabled = True
        LbNome.Enabled = True
        EcProcHosp1.Enabled = True
        EcProcHosp2.Enabled = True
        LbProcesso.Enabled = True
        EcUtente.Enabled = True
        EcDtChegaProd1.Enabled = True
        EcDtChegaProd2.Enabled = True
        LaDtCheP.Enabled = True
        BtPesquisaUtente.Enabled = True
        EcCartaoUte.Enabled = True
        LbCartaoUte.Enabled = True
        EcDescrSexo.Enabled = True
        LbSexo.Enabled = True
        EcDataNasc.Enabled = True
        LbDataNasc.Enabled = True
    End If
    
End Sub

Private Sub EcNumReq_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True

End Sub

Private Sub EcNumReq_LostFocus()
    
    cmdOK.Default = False

End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    Dim i As Integer
    Dim rsReq As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    If Cancel = False And Trim(EcNumReq.Text) <> "" Then
        Set rsReq = New ADODB.recordset
        
        With rsReq
            .Source = "SELECT sl_requis.seq_utente, t_utente, utente, estado_req, dt_chega, sl_requis.n_epis, sl_requis.t_sit, t_urg FROM " & _
                " sl_requis, sl_identif WHERE sl_identif.seq_utente = sl_requis.seq_utente AND " & _
                "n_req = " & EcNumReq
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
        
        If rsReq.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbOKOnly + vbExclamation, "Requisi��o"
            EcNumReq.Text = ""
            Cancel = True
        Else
            Select Case BL_HandleNull(rsReq!estado_req, "")
                'rcoelho 2.08.2013 ulsne-321
'                Case gEstadoReqEsperaProduto
'                    If gMostraAnalisesSemTubo <> mediSim Then
'                        BG_Mensagem mediMsgBox, "Requisi��o com todos os produtos por chegar!", vbOKOnly + vbExclamation, "Requisi��o"
'                        EcNumReq.text = ""
'                        Cancel = True
'                    End If
                Case gEstadoReqCancelada
                    BG_Mensagem mediMsgBox, "Requisi��o cancelada!", vbOKOnly + vbExclamation, "Requisi��o"
                    EcNumReq.Text = ""
                    Cancel = True
                Case gEstadoReqBloqueada
                    BG_Mensagem mediMsgBox, "Requisi��o Bloqueada!", vbOKOnly + vbExclamation, "Requisi��o"
                    EcNumReq.Text = ""
                    Cancel = True
'                Case gEstadoReqHistorico
'                    BG_Mensagem mediMsgBox, "Requisi��o em hist�rico!", vbOKOnly + vbExclamation, "Requisi��o"
'                    EcNumReq.text = ""
'                    Cancel = True
            End Select
            
            If BL_HandleNull(rsReq!dt_chega, "") = "" And Cancel = False Then
                BG_Mensagem mediMsgBox, "Requisi��o sem data de chegada!", vbOKOnly + vbExclamation, "Requisi��o"
                EcNumReq.Text = ""
                Cancel = True
            ElseIf Trim(EcCodUteSeq.Text) <> "" And Trim(EcCodUteSeq.Text) <> Trim(BL_HandleNull(rsReq!seq_utente, "")) And Cancel = False Then
                BG_Mensagem mediMsgBox, "O utente da requisi��o e o escolhido n�o s�o os mesmos!", vbOKOnly + vbExclamation, "Requisi��o"
                EcNumReq.Text = ""
                Cancel = True
            ElseIf Cancel = False Then
                PreencheEstado BL_HandleNull(rsReq!estado_req, " ")
                EcDataChega.Text = BL_HandleNull(rsReq!dt_chega, "")
                EcEpisodio.Text = BL_HandleNull(rsReq!n_epis, "")
                If BL_HandleNull(rsReq!t_urg, "") = "N" Then
                    CbUrgencia.ListIndex = 0
                ElseIf BL_HandleNull(rsReq!t_urg, "") = "U" Then
                    CbUrgencia.ListIndex = 1
                End If
                For i = 0 To CbSituacao.ListCount - 1
                
                    If CbSituacao.ItemData(i) = BL_HandleNull(rsReq!t_sit, -1) Then
                        CbSituacao.ListIndex = i
                        Exit For
                    End If
                Next

                EcSeqUteReq.Text = BL_HandleNull(rsReq!seq_utente, "")
            End If
        End If
            
        If Cancel = False Then
            If CbTipoUtente.ListIndex = mediComboValorNull And Trim(EcUtente.Text) = "" Then
                CbTipoUtente.Text = BL_HandleNull(rsReq!t_utente, "")
                EcUtente.Text = BL_HandleNull(rsReq!Utente, "")
                Cancel = SelDadosUtente
            End If
        End If
        rsReq.Close
        Set rsReq = Nothing
    End If
    
End Sub

Function PreencheEstado(estado As String)
    EcEstadoReq = estado
    
    If estado = gEstadoReqSemAnalises Then
        CbEstadoReq.ListIndex = 0
    ElseIf estado = gEstadoReqEsperaProduto Then
        CbEstadoReq.ListIndex = 1
    ElseIf estado = gEstadoReqEsperaResultados Then
        CbEstadoReq.ListIndex = 2
    ElseIf estado = gEstadoReqEsperaValidacao Then
        CbEstadoReq.ListIndex = 3
    ElseIf estado = gEstadoReqCancelada Then
        CbEstadoReq.ListIndex = 4
    ElseIf estado = gEstadoReqValicacaoMedicaCompleta Then
        CbEstadoReq.ListIndex = 5
    ElseIf estado = gEstadoReqNaoPassarHistorico Then
        CbEstadoReq.ListIndex = 6
    ElseIf estado = gEstadoReqTodasImpressas Then
        CbEstadoReq.ListIndex = 7
    ElseIf estado = gEstadoReqHistorico Then
        CbEstadoReq.ListIndex = 8
    ElseIf estado = gEstadoReqResultadosParciais Then
        CbEstadoReq.ListIndex = 9
    ElseIf estado = gEstadoReqValidacaoMedicaParcial Then
        CbEstadoReq.ListIndex = 10
    ElseIf estado = gEstadoReqImpressaoParcial Then
        CbEstadoReq.ListIndex = 11
    ElseIf estado = gEstadoReqBloqueada Then
        CbEstadoReq.ListIndex = 12
    Else
        CbEstadoReq.ListIndex = mediComboValorNull
    End If

End Function





Private Sub EcNumReqAssoc_Validate(Cancel As Boolean)
    Dim RsAnalise As ADODB.recordset
    Dim sql As String
    
    EcNumReqAssoc.Text = UCase(EcNumReqAssoc.Text)
    If EcNumReqAssoc.Text <> "" Then
        sql = "Select n_req from sl_requis where n_req_assoc = '" & EcNumReqAssoc.Text & "'"
        Set RsAnalise = New ADODB.recordset
        RsAnalise.CursorType = adOpenStatic
        RsAnalise.CursorLocation = adUseServer
        RsAnalise.Open sql, gConexao
        If RsAnalise.RecordCount > 0 Then
            EcNumReq.Text = RsAnalise!n_req
            EcNumReq_Validate (Cancel)
        Else
            BG_Mensagem mediMsgBox, "Requisi��o Associada inexistente!", vbOKOnly + vbExclamation, "SISLAB"
            LimpaCampos
        End If
        RsAnalise.Close
        Set RsAnalise = Nothing
    End If

End Sub

Private Sub EcReqAux_Validate(Cancel As Boolean)

    Dim RsAnalise As ADODB.recordset
    Dim sql As String
    
    EcReqAux.Text = UCase(EcReqAux.Text)
    If EcReqAux.Text <> "" Then
        sql = "Select n_req from sl_requis where req_aux = '" & EcReqAux.Text & "'"
        Set RsAnalise = New ADODB.recordset
        RsAnalise.CursorType = adOpenStatic
        RsAnalise.CursorLocation = adUseServer
        RsAnalise.Open sql, gConexao
        If RsAnalise.RecordCount > 0 Then
            EcNumReq.Text = RsAnalise!n_req
            EcNumReq_Validate (Cancel)
        Else
            BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbExclamation, "SISLAB"
            LimpaCampos
        End If
        RsAnalise.Close
        Set RsAnalise = Nothing
    End If


End Sub

Private Sub EcResult_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcResult_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcUrgencia_Change()
    
    Select Case Trim(EcUrgencia.Text)
        Case "N"
            CbUrgencia.ListIndex = 0
        Case "U"
            CbUrgencia.ListIndex = 1
        Case Else
            CbUrgencia.ListIndex = mediComboValorNull
    End Select

End Sub

Private Sub EcUtente_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcUtente_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    If Cancel = False Then
        Cancel = SelDadosUtente
    End If
    
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub PreencheIdentificacaoUtente()
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    sql = "SELECT utente,t_utente,n_proc_1,n_proc_2, nome_ute, sexo_ute, dt_nasc_ute FROM sl_identif WHERE seq_utente=" & EcCodUteSeq.Text
    Set Tabela = New ADODB.recordset
    Tabela.CursorType = adOpenStatic
    Tabela.CursorLocation = adUseServer
    Tabela.Open sql, gConexao
    If Tabela.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Erro ao seleccionar utente !", vbError, "ERRO"
    Else
        CbTipoUtente.Text = Trim(Tabela!t_utente)
        EcUtente.Text = Trim(Tabela!Utente)
        EcProcHosp1.Text = BL_HandleNull(Tabela!n_proc_1, "")
        EcProcHosp2.Text = BL_HandleNull(Tabela!n_proc_2, "")
        EcNome.Text = BL_HandleNull(Tabela!nome_ute, "")
        EcSexo.Text = BL_HandleNull(Tabela!sexo_ute, "")
        BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
        EcDataNasc.Text = BL_HandleNull(Tabela!dt_nasc_ute, "")
    End If
    Tabela.Close
    Set Tabela = Nothing

End Sub

Private Sub LstReq_DblClick()

    If LstReq.ListIndex <> mediComboValorNull Then
        EcNumReq.Text = LstReq.ItemData(LstReq.ListIndex)
        LstReq.RemoveItem LstReq.ListIndex
        FuncaoProcurar
    End If

End Sub

Function ConstruirSqlPesquisaReq(ByRef sql As String, ByRef Sql2 As String) As String

    ConstruirSqlPesquisaReq = ""

    If CbSituacao.ListIndex <> mediComboValorNull Then
        sql = sql & " AND x1.t_sit= " & BG_DaComboSel(CbSituacao)
        If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
            Sql2 = Sql2 & " AND x1.t_urg = " & BL_TrataStringParaBD(Mid(CbUrgencia.Text, 1, 1))
        End If
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
            sql = sql & " AND x1.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
            If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
                Sql2 = Sql2 & " AND x1.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
            End If
        End If
        
    If Trim(EcDtChegaProd1.Text) <> "" And Trim(EcDtChegaProd2.Text) <> "" Then
        sql = sql & " AND x2.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.Text) & _
            " AND " & BL_TrataDataParaBD(EcDtChegaProd2.Text)
        If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
            Sql2 = Sql2 & " AND x2.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtChegaProd1.Text) & _
                " AND " & BL_TrataDataParaBD(EcDtChegaProd2.Text)
        End If
    End If

    If Trim(EcCodAna.Text) <> "" Then
        sql = sql & " AND ( x2.cod_perfil = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR x2.cod_ana_c = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR x2.cod_ana_s = " & BL_TrataStringParaBD(EcCodAna.Text) & ")"
        If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
            Sql2 = Sql2 & " AND ( x2.cod_perfil = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR x2.cod_ana_c = " & BL_TrataStringParaBD(EcCodAna.Text) & " OR x2.cod_ana_s = " & BL_TrataStringParaBD(EcCodAna.Text) & ")"
        End If
    End If
        
    If Trim(EcCodSgrp.Text) <> "" Or Trim(EcCodGrp.Text) <> "" Then
        sql = sql & " AND ( x2.cod_perfil in ( select cod_perfis from sl_perfis where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
        sql = sql & " OR x2.cod_ana_c in ( select cod_ana_c from sl_ana_c where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
        sql = sql & " OR x2.cod_ana_s in ( select cod_ana_s from sl_ana_s where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ") )"
        If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
            Sql2 = Sql2 & " AND ( x2.cod_perfil in ( select cod_perfis from sl_perfis where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
            Sql2 = Sql2 & " OR x2.cod_ana_c in ( select cod_ana_c from sl_ana_c where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ")"
            Sql2 = Sql2 & " OR x2.cod_ana_s in ( select cod_ana_s from sl_ana_s where " & IIf(Trim(EcCodGrp.Text) <> "", "gr_ana = " & BL_TrataStringParaBD(EcCodGrp.Text), "") & IIf(Trim(EcCodSgrp.Text) <> "" And Trim(EcCodGrp.Text) <> "", " AND ", "") & IIf(Trim(EcCodSgrp.Text) <> "", " sgr_ana = " & BL_TrataStringParaBD(EcCodSgrp.Text), "") & ") )"
        End If
    End If
        
    If Trim(EcFolhaTrab.Text) <> "" Then
        sql = sql & " AND x2.n_folha_trab = " & EcFolhaTrab.Text
        If gModoRes = cModoResInsVal Or gModoRes = cModoResGeral Or gModoRes = cModoResMicro Then
            Sql2 = Sql2 & " AND x2.n_folha_trab = " & EcFolhaTrab.Text
        End If
    End If

End Function

' --------------------------------------------------------

' UTILIZADORES DOS POSTOS PREENCHE POR DEFEITO A SALA

' --------------------------------------------------------
Private Sub PreencheSalaDefeito()
    EcCodSala.Enabled = True
    BtPesquisaSalas.Enabled = True
    If gCodSalaAssocUser > 0 Then
        EcCodSala = gCodSalaAssocComp
        EcCodsala_Validate True
        EcCodSala.Enabled = False
        BtPesquisaSalas.Enabled = False
    End If
End Sub


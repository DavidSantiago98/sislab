VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormEnviarAnaApar 
   BackColor       =   &H00FED6CD&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEnviarAnaApar"
   ClientHeight    =   8820
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13260
   Icon            =   "FormEnviarAnaApar.frx":0000
   LinkTopic       =   "FormEnviarAnaApar"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8820
   ScaleWidth      =   13260
   ShowInTaskbar   =   0   'False
   Begin VB.Timer TimerRefresh 
      Left            =   12720
      Top             =   7200
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FEC6BA&
      Height          =   1095
      Left            =   240
      TabIndex        =   8
      Top             =   7680
      Width           =   12735
      Begin VB.CommandButton BtEnviar 
         Height          =   615
         Left            =   11520
         Picture         =   "FormEnviarAnaApar.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FEC6BA&
         Caption         =   "An�lise de envio manual"
         Height          =   255
         Index           =   7
         Left            =   5760
         TabIndex        =   17
         Top             =   720
         Width           =   3015
      End
      Begin VB.Label Label1 
         BackColor       =   &H0080C0FF&
         Height          =   255
         Index           =   6
         Left            =   5040
         TabIndex        =   16
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Analise selecionada para enviar para aparelho"
         Height          =   255
         Index           =   5
         Left            =   5760
         TabIndex        =   15
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FEC6BA&
         Caption         =   "An�lise pronta para ser enviada para aparelho"
         Height          =   255
         Index           =   4
         Left            =   720
         TabIndex        =   14
         Top             =   720
         Width           =   3495
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FEC6BA&
         Caption         =   "An�lise j� enviada para aparelho"
         Height          =   255
         Index           =   3
         Left            =   720
         TabIndex        =   13
         Top             =   240
         Width           =   3015
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FEA381&
         Height          =   255
         Index           =   2
         Left            =   5040
         TabIndex        =   12
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label1 
         BackColor       =   &H0080FFFF&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   360
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0FFC0&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   375
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FEC6BA&
      Height          =   1095
      Left            =   240
      TabIndex        =   1
      Top             =   0
      Width           =   12735
      Begin VB.CheckBox CkActualizar 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Actualizar (10s)"
         Height          =   195
         Left            =   11160
         TabIndex        =   18
         Top             =   840
         Width           =   1455
      End
      Begin VB.ComboBox CbAparelho 
         Height          =   315
         Left            =   2400
         TabIndex        =   3
         Text            =   "Combo1"
         Top             =   480
         Width           =   3375
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   7800
         TabIndex        =   2
         Top             =   480
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   144965633
         CurrentDate     =   39652
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   9720
         TabIndex        =   4
         Top             =   480
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   144965633
         CurrentDate     =   39652
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Aparelho"
         Height          =   255
         Index           =   0
         Left            =   1320
         TabIndex        =   7
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FEC6BA&
         Caption         =   "De"
         Height          =   255
         Index           =   1
         Left            =   7200
         TabIndex        =   6
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FEC6BA&
         Caption         =   "a"
         Height          =   255
         Index           =   2
         Left            =   9360
         TabIndex        =   5
         Top             =   480
         Width           =   255
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FgAna 
      Height          =   6375
      Left            =   240
      TabIndex        =   0
      Top             =   1200
      Width           =   12735
      _ExtentX        =   22463
      _ExtentY        =   11245
      _Version        =   393216
      BackColorBkg    =   16697018
      GridColor       =   16697018
      BorderStyle     =   0
   End
End
Attribute VB_Name = "FormEnviarAnaApar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer
Dim CampoDeFocus As Object

' ESTRUTURA COM DADOS DA LIST BOX
Private Type analises
    cod_analise As String
    abrev_Analise As String
End Type
Dim estrutAnalises() As analises
Dim totalAna As Long

' ESTRUTURA COM DADOS DAS ANALISES
Private Type requisicoes
    n_req As String
    dt_chega As String
    Existe() As Boolean
    Seleccionada() As Integer
    flg_apar_trans() As String
End Type
Dim estrutRequis() As requisicoes
Dim totalRequis As Long

' CORES UTILIZADAS
Const vermelho = &H8080FF
Const Amarelo = &H80FFFF
Const Branco = &HFFFFFF
Const laranja = &H80C0FF

Const cinzento = &HC0C0C0              '&HE0E0E0
Const Verde = &HC0FFC0
Const azul = &HFEA381                          '&HFF8080

Const AzulClaro = &HFF8080



' ----------------------------------------------------------------------

' ACTUALIZA ESTADO AO CLICAR NUMA COLUNA, LINHA OU CELULA

' ----------------------------------------------------------------------
Private Sub FgAna_Click()
    Dim i As Long
    Dim j As Long
    
    ' ----------------------------------------------------------------------------
    ' CLICOU NUMA CELULA  - SE EXISTIR  - SE MARCADA DESMARCA, SE DESMARCADA MARCA
    ' ----------------------------------------------------------------------------
    If FGAna.Col >= 2 And FGAna.row >= 2 Then
        If estrutRequis(FGAna.row - 1).Existe(FGAna.Col - 1) = True Then
            If estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 1 Then
                estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = -1
                If estrutRequis(FGAna.row - 1).flg_apar_trans(FGAna.Col - 1) = "1" Then
                    FGAna.CellBackColor = Verde
                ElseIf estrutRequis(FGAna.row - 1).flg_apar_trans(FGAna.Col - 1) = "0" Then
                    FGAna.CellBackColor = Amarelo
                ElseIf estrutRequis(FGAna.row - 1).flg_apar_trans(FGAna.Col - 1) = "2" Then
                    FGAna.CellBackColor = laranja
                End If
            ElseIf estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 0 Then
                estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 1
                FGAna.CellBackColor = Verde
            
            Else
                estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 0
                FGAna.CellBackColor = AzulClaro
            End If
        End If
    
    ' ----------------------------------------------------------------------------
    ' CLICOU NUMA COLUNA - MARCA TODAS REQUISICOES COM AKELA ANALISE
    ' -----------------------------------------------------------------------------
    ElseIf FGAna.Col >= 2 And FGAna.row = 1 Then
        For i = 1 To totalRequis
            If estrutRequis(i).Existe(FGAna.Col - 1) = True Then
                FGAna.row = i + 1
                If estrutRequis(i).Seleccionada(FGAna.Col - 1) = 1 Then
                    estrutRequis(i).Seleccionada(FGAna.Col - 1) = -1
                    If estrutRequis(i).flg_apar_trans(FGAna.Col - 1) = "1" Then
                        FGAna.CellBackColor = Verde
                    ElseIf estrutRequis(i).flg_apar_trans(FGAna.Col - 1) = "0" Then
                        FGAna.CellBackColor = Amarelo
                    ElseIf estrutRequis(i).flg_apar_trans(FGAna.Col - 1) = "2" Then
                        FGAna.CellBackColor = laranja
                    End If
                ElseIf estrutRequis(i).Seleccionada(FGAna.Col - 1) = 0 Then
                    estrutRequis(i).Seleccionada(FGAna.Col - 1) = 1
                    FGAna.CellBackColor = Verde
                
                Else
                    estrutRequis(i).Seleccionada(FGAna.Col - 1) = 0
                    FGAna.CellBackColor = AzulClaro
                End If
            End If
        Next
    
    ' ----------------------------------------------------------------------
    ' CLICOU NUMA LINHA - MARCA TODAS ANALISES DAKELA REQUISICAO
    ' ----------------------------------------------------------------------
    ElseIf FGAna.Col = 1 And FGAna.row >= 2 Then
        For i = 1 To totalAna
            If estrutRequis(FGAna.row - 1).Existe(i) = True Then
                    FGAna.Col = i + 1
                    If estrutRequis(FGAna.row - 1).Seleccionada(i) = 1 Then
                        estrutRequis(FGAna.row - 1).Seleccionada(i) = -1
                        If estrutRequis(FGAna.row - 1).flg_apar_trans(i) = "1" Then
                            FGAna.CellBackColor = Verde
                        ElseIf estrutRequis(FGAna.row - 1).flg_apar_trans(i) = "0" Then
                            FGAna.CellBackColor = Amarelo
                        ElseIf estrutRequis(FGAna.row - 1).flg_apar_trans(i) = "2" Then
                            FGAna.CellBackColor = laranja
                        End If
                    ElseIf estrutRequis(FGAna.row - 1).Seleccionada(i) = 0 Then
                        estrutRequis(FGAna.row - 1).Seleccionada(i) = 1
                        FGAna.CellBackColor = Verde
                    
                    Else
                        estrutRequis(FGAna.row - 1).Seleccionada(i) = 0
                        FGAna.CellBackColor = AzulClaro
                    End If
            End If
        Next
    ElseIf FGAna.Col = 1 And FGAna.row = 1 Then
                
        For i = 1 To totalAna
            For j = 1 To totalRequis
                If estrutRequis(j).Existe(i) = True Then
                        FGAna.row = j + 1
                        FGAna.Col = i + 1
                        If estrutRequis(j).Seleccionada(i) = 1 Then
                            estrutRequis(j).Seleccionada(i) = -1
                            If estrutRequis(j).flg_apar_trans(i) = "1" Then
                                FGAna.CellBackColor = Verde
                            ElseIf estrutRequis(j).flg_apar_trans(i) = "0" Then
                                FGAna.CellBackColor = Amarelo
                            ElseIf estrutRequis(j).flg_apar_trans(i) = "2" Then
                                FGAna.CellBackColor = laranja
                            End If
                        ElseIf estrutRequis(j).Seleccionada(i) = 0 Then
                            estrutRequis(j).Seleccionada(i) = 1
                            FGAna.CellBackColor = Verde
                        
                        Else
                            estrutRequis(j).Seleccionada(i) = 0
                            FGAna.CellBackColor = AzulClaro
                        End If
                End If
            Next
        Next
    End If
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormEnviarAnaApar = Nothing

End Sub

Sub DefTipoCampos()
    TimerRefresh.Interval = 10000
    With FGAna
        .rows = 3
        .FixedRows = 1
        .Cols = 3
        .FixedCols = 1
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 1000
        .ColWidth(1) = 0
        .RowHeight(1) = 0
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .row = 1
        .Col = 1
    End With
    
End Sub

Sub PreencheValoresDefeito()
    LimpaCampos
    LimpaEstruturas
    
    If gSGBD = gOracle Then
        BG_PreencheComboBD_ADO "SELECT seq_apar, descr_apar FROM gc_apar WHERE (cod_local = " & gCodLocal & " OR cod_local is null) and ( flg_desactivo IS NULL or flg_desactivo = 0)", "seq_apar", "descr_apar", CbAparelho, mediAscComboDesignacao
    ElseIf gSGBD = gSqlServer Then
        BG_PreencheComboBD_ADO "gescom.dbo.GC_APAR  ", "seq_apar", "descr_apar", CbAparelho
    End If
    
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    
End Sub

Sub Inicializacoes()
         
    Me.caption = "An�lises para Aparelho"
    Me.Width = 13350
    Me.Height = 9300 ' Normal
    Me.left = 50
    Me.top = 50
    Set CampoDeFocus = CbAparelho
    
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
End Sub

Sub LimpaCampos()
    gRequisicaoActiva = -1
    CbAparelho = ""
    LimpaFGAna
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    FGAna.Visible = False
End Sub

' ----------------------------------------------------------------------

' LIMPA FLEX GRID

' ----------------------------------------------------------------------
Sub LimpaFGAna()
    FGAna.Cols = 3
    FGAna.rows = 3
    FGAna.TextMatrix(0, 1) = ""
    FGAna.TextMatrix(0, 2) = ""
    FGAna.TextMatrix(1, 0) = ""
    FGAna.TextMatrix(1, 1) = ""
    FGAna.TextMatrix(1, 2) = ""
    FGAna.TextMatrix(2, 0) = ""
    FGAna.TextMatrix(2, 1) = ""
    FGAna.TextMatrix(2, 2) = ""
    
End Sub

' ----------------------------------------------------------------------

' PREENCHE FLEX GRID

' ----------------------------------------------------------------------
Private Sub PreencheFGAna()
    Dim i As Long
    Dim j As Long
    FGAna.Cols = totalAna + 2
    FGAna.rows = totalRequis + 2
    FGAna.ColWidth(0) = 700
    
    For i = 1 To totalAna
        FGAna.ColWidth(i + 1) = 800
        FGAna.ColAlignment(i + 1) = flexAlignCenterCenter
        FGAna.TextMatrix(1, i + 1) = estrutAnalises(i).abrev_Analise
        FGAna.TextMatrix(0, i + 1) = estrutAnalises(i).abrev_Analise
    Next
    
    
    For i = 1 To totalRequis
        FGAna.TextMatrix(i + 1, 0) = estrutRequis(i).n_req
        FGAna.TextMatrix(i + 1, 1) = estrutRequis(i).n_req
        FGAna.row = i + 1
        For j = 1 To totalAna
            FGAna.Col = j + 1
            ' ----------------------------------------------------------------------
            ' SE ANALISE MARCADA
            ' ----------------------------------------------------------------------
            If estrutRequis(i).Existe(j) = True Then
                
                ' ----------------------------------------------------------------------
                ' SE JA FOI ENVIADA - COLOCA A VERDE SENAO A AMARELO
                ' ----------------------------------------------------------------------
                If estrutRequis(i).flg_apar_trans(j) = "1" Then
                    FGAna.CellBackColor = Verde
                ElseIf estrutRequis(i).flg_apar_trans(j) = "0" Then
                    FGAna.CellBackColor = Amarelo
                ElseIf estrutRequis(i).flg_apar_trans(j) = "2" Then
                    FGAna.CellBackColor = laranja
                End If
                FGAna.TextMatrix(i + 1, j + 1) = " X "
            Else
                FGAna.CellBackColor = cinzento
                FGAna.TextMatrix(i + 1, j + 1) = ""
            End If
        Next
    Next
    
End Sub
Private Sub LimpaEstruturas()
    totalAna = 0
    ReDim estrutAnalises(totalAna)

    totalRequis = 0
    ReDim estrutRequis(totalRequis)
End Sub

Sub FuncaoLimpar()
    LimpaCampos
    LimpaEstruturas
    CbAparelho.SetFocus
    
End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    Dim codLocalApar As String
    If CbAparelho.ListIndex <> -1 Then
        codLocalApar = BL_SelCodigo("gc_apar", "cod_local", "seq_apar", CbAparelho.ItemData(CbAparelho.ListIndex))
        sSql = " SELECT x1.cod_ana_s, x2.abr_ana_s, x1.n_req, x1.dt_chega,x1.flg_apar_trans  "
        sSql = sSql & " FROM sl_marcacoes x1, sl_ana_s x2, gc_ana_apar x3, sl_requis x4, sl_ana_acrescentadas x5 WHERE x1.dt_chega IS NOT NULL AND x1.cod_ana_s = x2.cod_ana_s "
        sSql = sSql & " AND x1.n_Req = x4.n_req "
        sSql = sSql & " AND x5.n_req = x4.n_req and x5.cod_agrup = x1.cod_agrup and (x5.flg_eliminada is null or x5.flg_eliminada = 0) AND x5.cod_local = " & gCodLocal
        sSql = sSql & " AND x1.cod_ana_s = x3.cod_simpl and x3.seq_apar=" & CbAparelho.ItemData(CbAparelho.ListIndex) & " and t_ana in (1,2) "
        sSql = sSql & " AND x4.dt_chega between " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        If codLocalApar <> "" Then
            'sSql = sSql & " AND x4.cod_local = " & codLocalApar
        End If
        sSql = sSql & " ORDER BY x1.n_req"
        LimpaEstruturas
        
        rsReq.CursorType = adOpenStatic
        rsReq.CursorLocation = adUseServer
        rsReq.Open sSql, gConexao
    
        If Not rsReq.EOF Then
            While Not rsReq.EOF
                PreencheEstrutAna rsReq!cod_ana_s, BL_HandleNull(rsReq!abr_ana_s, "---")
                PreencheEstrutRequis rsReq!n_req, BL_HandleNull(rsReq!dt_chega, ""), rsReq!cod_ana_s, BL_HandleNull(rsReq!flg_apar_trans, "0")
                rsReq.MoveNext
            Wend
            LimpaFGAna
            PreencheFGAna
            FGAna.Visible = True
        Else
            BG_Mensagem mediMsgBox, "N�o foram selecionados registos segundo as condi��es de pesquisa!", vbExclamation, "Procurar"
            LimpaCampos
        End If
        rsReq.Close
    End If

End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

' ----------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ANALISES

' ----------------------------------------------------------------------
Private Sub PreencheEstrutAna(cod_ana As String, abrev_ana As String)
    Dim i As Long
    
    For i = 1 To totalAna
        If estrutAnalises(i).cod_analise = cod_ana Then
            Exit Sub
        End If
    Next
    totalAna = totalAna + 1
    ReDim Preserve estrutAnalises(totalAna)
    estrutAnalises(totalAna).cod_analise = cod_ana
    estrutAnalises(totalAna).abrev_Analise = abrev_ana
    
    For i = 1 To totalRequis
        ReDim Preserve estrutRequis(i).Existe(totalAna)
        ReDim Preserve estrutRequis(i).Seleccionada(totalAna)
        ReDim Preserve estrutRequis(i).flg_apar_trans(totalAna)
    Next
End Sub

' ----------------------------------------------------------------------

' PREENCHE ESTRUTURA DE REQUISICOES

' ----------------------------------------------------------------------

Private Sub PreencheEstrutRequis(n_req As String, dt_chega As String, cod_ana_s As String, flg_apar_trans As String)
    Dim i As Long
    Dim j As Long
    
    For i = 1 To totalRequis
        If estrutRequis(i).n_req = n_req Then
            For j = 1 To totalAna
                If estrutAnalises(j).cod_analise = cod_ana_s Then
                    estrutRequis(i).Existe(j) = True
                    estrutRequis(i).Seleccionada(j) = -1
                    estrutRequis(i).flg_apar_trans(j) = flg_apar_trans
                    Exit Sub
                End If
            Next
        End If
    Next
    
    totalRequis = totalRequis + 1
    ReDim Preserve estrutRequis(totalRequis)
    estrutRequis(totalRequis).n_req = n_req
    estrutRequis(totalRequis).dt_chega = dt_chega
    ReDim estrutRequis(totalRequis).Existe(totalAna)
    ReDim estrutRequis(totalRequis).Seleccionada(totalAna)
    ReDim estrutRequis(totalRequis).flg_apar_trans(totalAna)
    For i = 1 To totalAna
        If estrutAnalises(i).cod_analise = cod_ana_s Then
            estrutRequis(totalRequis).Existe(i) = True
            estrutRequis(totalRequis).Seleccionada(i) = -1
            estrutRequis(totalRequis).flg_apar_trans(i) = flg_apar_trans
        Else
            estrutRequis(totalRequis).Existe(i) = False
            estrutRequis(totalRequis).Seleccionada(i) = -1
            estrutRequis(totalRequis).flg_apar_trans(i) = "0"
        End If
    Next
    
End Sub


' ----------------------------------------------------------------------

' FAZ UPDATE SL_MARCACOES PARA ANALISES SELECIONADAS

' ----------------------------------------------------------------------

Private Sub BtEnviar_Click()
    Dim i As Long
    Dim j As Long
    Dim sSql As String
    On Error GoTo TrataErro
    
    BG_BeginTransaction
    
    For i = 1 To totalRequis
        For j = 1 To totalAna
            If (estrutRequis(i).Seleccionada(j) = 0 Or estrutRequis(i).Seleccionada(j) = 1) And estrutRequis(i).Existe(j) = True Then
                If gSGBD = gOracle Then
                    sSql = "DELETE from gc_listas_enviadas where seq_apar = " & CbAparelho.ItemData(CbAparelho.ListIndex) & _
                            " and n_req like '%" & Trim(estrutRequis(i).n_req) & "%' And cod_simpl = " & BL_TrataStringParaBD(estrutAnalises(j).cod_analise)
                Else
                    sSql = "DELETE from gescom.dbo.gc_listas_enviadas where seq_apar = " & CbAparelho.ItemData(CbAparelho.ListIndex) & _
                            " and n_req like '%" & Trim(estrutRequis(i).n_req) & "%' And cod_simpl = " & BL_TrataStringParaBD(estrutAnalises(j).cod_analise)
                End If
                BG_ExecutaQuery_ADO sSql
                If estrutRequis(i).Seleccionada(j) = 0 Then
                    sSql = "UPDATE sl_marcacoes SET flg_apar_trans = 0 WHERE n_req = " & BL_TrataStringParaBD(estrutRequis(i).n_req)
                    sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(estrutAnalises(j).cod_analise)
                Else
                    sSql = "UPDATE sl_marcacoes SET flg_apar_trans = 1 WHERE n_req = " & BL_TrataStringParaBD(estrutRequis(i).n_req)
                    sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(estrutAnalises(j).cod_analise)
                End If
                BG_ExecutaQuery_ADO sSql
            End If
        Next
    Next
    BG_CommitTransaction
    FuncaoProcurar
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao Enviar Ana para aparelhos: " & Err.Description, Me.Name, "BtEnviar_click", False
    Exit Sub
    Resume Next
End Sub

Private Sub TimerRefresh_Timer()
    If CkActualizar.value = vbChecked Then
        FuncaoProcurar
    End If
End Sub

VERSION 5.00
Begin VB.Form FormStkLotesAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormStkLotesAna"
   ClientHeight    =   5610
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11805
   Icon            =   "FormStkLotesAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   11805
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodLote 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1110
      Locked          =   -1  'True
      MaxLength       =   4
      TabIndex        =   10
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcDescrLote 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   120
      Width           =   4815
   End
   Begin VB.CommandButton BtPesqLote 
      Height          =   315
      Left            =   6600
      Picture         =   "FormStkLotesAna.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton BtInsere 
      Height          =   495
      Left            =   7200
      Picture         =   "FormStkLotesAna.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   " Inserir no Perfil "
      Top             =   2280
      Width           =   495
   End
   Begin VB.CommandButton BtRetira 
      Height          =   495
      Left            =   7200
      Picture         =   "FormStkLotesAna.frx":0920
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   " Retirar do Perfil "
      Top             =   2835
      Width           =   495
   End
   Begin VB.ListBox EcAnalisesSel 
      Appearance      =   0  'Flat
      Height          =   4125
      Left            =   7920
      TabIndex        =   2
      Top             =   840
      Width           =   3375
   End
   Begin VB.ListBox EcAnalises 
      Appearance      =   0  'Flat
      Height          =   4125
      Left            =   3600
      MultiSelect     =   2  'Extended
      TabIndex        =   1
      Top             =   840
      Width           =   3375
   End
   Begin VB.ListBox EcAparelhos 
      Appearance      =   0  'Flat
      Height          =   4125
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   3375
   End
   Begin VB.Label Label4 
      BackColor       =   &H80000004&
      Caption         =   "Lote"
      Height          =   255
      Index           =   5
      Left            =   240
      TabIndex        =   11
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "An�lises Seleccionadas"
      Height          =   255
      Index           =   2
      Left            =   8040
      TabIndex        =   7
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "An�lises do Aparelho"
      Height          =   255
      Index           =   1
      Left            =   3720
      TabIndex        =   6
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Aparelhos"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   1575
   End
End
Attribute VB_Name = "FormStkLotesAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 30/03/2003
' T�cnico : Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'
Private Type AnaLote
    cod_lote As String
    seq_apar As Integer
    descr_apar As String
    cod_ana_s As String
    descr_ana_s As String
    user_apar As String
    seq_ana_s As String
End Type
Dim estrutAnaLote() As AnaLote
Dim totalAnaLote As Integer

Private Sub BD_Update()
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    If EcCodLote = "" Then
        Exit Sub
    End If
    
    BG_BeginTransaction
    
    ' APAGA O QUE EXISTE
    sSql = "DELETE FROM sl_stk_ana_lotes WHERE cod_lote = " & EcCodLote
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalAnaLote
        sSql = "INSERT INTO sl_stk_ana_lotes (cod_lote, seq_apar, cod_ana_S, user_apar) VALUES ("
        sSql = sSql & estrutAnaLote(i).cod_lote & ", "
        sSql = sSql & estrutAnaLote(i).seq_apar & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutAnaLote(i).cod_ana_s) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutAnaLote(i).user_apar) & ") "
        BG_ExecutaQuery_ADO sSql
    Next
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao Gravar An�lises: " & Err.Description, Me.Name, "BD_Update", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtInsere_Click()
    Dim aux As String
    Dim sSql As String
    Dim rsDados As New ADODB.recordset
    Dim j As Integer
    Dim i As Integer
    Dim Existe As Boolean
    For i = 0 To EcAnalises.ListCount - 1
        Existe = False
        If EcAnalises.Selected(i) = True Then
            aux = EcAnalises.ItemData(i)
            While Len(aux) < 5
                aux = "0" & aux
            Wend
            aux = EcAparelhos.ItemData(EcAparelhos.ListIndex) & aux
            For j = 1 To totalAnaLote
                If estrutAnaLote(j).seq_apar = EcAparelhos.ItemData(EcAparelhos.ListIndex) Then
                    If estrutAnaLote(j).seq_ana_s = EcAnalises.ItemData(i) Then
                       Existe = True
                       Exit For
                    End If
                End If
            Next
            If Existe = False Then
                sSql = "SELECT cod_ana_s, descr_ana_s,  util_apar, descr_apar FROM sl_ana_s, gc_apar "
                sSql = sSql & " WHERE seq_ana_s = " & EcAnalises.ItemData(i)
                sSql = sSql & " AND seq_apar = " & EcAparelhos.ItemData(EcAparelhos.ListIndex)
                rsDados.CursorLocation = adUseServer
                rsDados.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsDados.Open sSql, gConexao
                If rsDados.RecordCount = 1 Then
                
                    totalAnaLote = totalAnaLote + 1
                    ReDim Preserve estrutAnaLote(totalAnaLote)
                    estrutAnaLote(totalAnaLote).seq_ana_s = EcAnalises.ItemData(i)
                    estrutAnaLote(totalAnaLote).seq_apar = EcAparelhos.ItemData(EcAparelhos.ListIndex)
                    estrutAnaLote(totalAnaLote).cod_ana_s = BL_HandleNull(rsDados!cod_ana_s, "")
                    estrutAnaLote(totalAnaLote).descr_ana_s = BL_HandleNull(rsDados!descr_ana_s, "")
                    estrutAnaLote(totalAnaLote).descr_apar = BL_HandleNull(rsDados!descr_apar, "")
                    estrutAnaLote(totalAnaLote).user_apar = BL_HandleNull(rsDados!util_apar, "")
                    estrutAnaLote(totalAnaLote).cod_lote = EcCodLote
                    
                    EcAnalisesSel.AddItem estrutAnaLote(totalAnaLote).descr_ana_s & " (" & estrutAnaLote(totalAnaLote).descr_apar & ")"
                    EcAnalisesSel.ItemData(EcAnalisesSel.NewIndex) = CLng(aux)
                End If
                rsDados.Close
            End If
        End If
    Next
End Sub

Private Sub BtRetira_Click()
    Dim i As Integer
    Dim j As Integer
    If EcAnalisesSel.ListIndex > -1 Then
        j = EcAnalisesSel.ListIndex
        For i = EcAnalisesSel.ListIndex + 1 To totalAnaLote - 1
            estrutAnaLote(i).cod_ana_s = estrutAnaLote(i + 1).cod_ana_s
            estrutAnaLote(i).cod_lote = estrutAnaLote(i + 1).cod_lote
            estrutAnaLote(i).descr_ana_s = estrutAnaLote(i + 1).descr_ana_s
            estrutAnaLote(i).descr_apar = estrutAnaLote(i + 1).descr_apar
            estrutAnaLote(i).seq_ana_s = estrutAnaLote(i + 1).seq_ana_s
            estrutAnaLote(i).seq_apar = estrutAnaLote(i + 1).seq_apar
            estrutAnaLote(i).user_apar = estrutAnaLote(i + 1).user_apar
        Next
        totalAnaLote = totalAnaLote - 1
        ReDim Preserve estrutAnaLote(totalAnaLote)
        EcAnalisesSel.RemoveItem EcAnalisesSel.ListIndex
        If j < EcAnalisesSel.ListCount Then
            EcAnalisesSel.ListIndex = j
            EcAnalisesSel.SetFocus
        End If
    End If
End Sub

Private Sub EcAparelhos_Click()
    Dim sSql As String
    EcAnalises.Clear
    sSql = "select distinct seq_ana, cod_simpl, slv_analises.descr_ana from gc_ana_apar, slv_analises where cod_simpl = cod_ana "
    If EcAparelhos.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND seq_apar = " & EcAparelhos.ItemData(EcAparelhos.ListIndex)
    End If
    BG_PreencheComboBD_ADO sSql, "seq_ana", "descr_ana", EcAnalises, mediAscComboDesignacao

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()

    Me.caption = " Codifica��o de An�lises Associadas a Lotes"
    Me.left = 540
    Me.top = 450
    Me.Width = 11895
    Me.Height = 6030 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    Set CampoDeFocus = EcCodLote
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

   
    Call FuncaoProcurar
    
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "Activo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"

'    Me.opC.Value = True
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormStkLotesAna = Nothing

End Sub

Sub LimpaCampos()
    
    ' Me.SetFocus
    EcAparelhos.Clear
    EcAnalises.Clear
    EcAnalisesSel.Clear
    EcCodLote = ""
    EcDescrLote = ""
    

End Sub

Sub DefTipoCampos()
    BG_PreencheComboBD_ADO "gc_apar", "seq_apar", "descr_apar", EcAparelhos, mediAscComboDesignacao
    
'nada
End Sub

Sub PreencheValoresDefeito()
    

End Sub

Sub PreencheCampos()
    
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

    Me.EcCodLote.Locked = False

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        
        
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    Dim aux As String
    totalAnaLote = 0
    ReDim estrutAnaLote(totalAnaLote)
    EcAnalisesSel.Clear
    
    If EcCodLote <> "" Then
        sSql = "SELECT x1.cod_lote, x1.seq_apar, x1.cod_ana_s, x1.user_apar, x2.descr_ana_s, x3.descr_apar, x2.seq_ana_s "
        sSql = sSql & " FROM sl_stk_ana_lotes x1, sl_ana_s x2, gc_apar x3"
        sSql = sSql & " WHERE cod_lote = " & EcCodLote & " AND x1.cod_ana_s = x2.cod_ana_s AND x1.seq_apar = x3.seq_apar"
        Set rsLote = New ADODB.recordset
        rsLote.CursorLocation = adUseServer
        rsLote.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsLote.Open sSql, gConexao
        If rsLote.RecordCount > 0 Then
            While Not rsLote.EOF
                totalAnaLote = totalAnaLote + 1
                ReDim Preserve estrutAnaLote(totalAnaLote)
                estrutAnaLote(totalAnaLote).seq_apar = BL_HandleNull(rsLote!seq_ana_s, "")
                estrutAnaLote(totalAnaLote).cod_ana_s = BL_HandleNull(rsLote!cod_ana_s, "")
                estrutAnaLote(totalAnaLote).descr_ana_s = BL_HandleNull(rsLote!descr_ana_s, "")
                estrutAnaLote(totalAnaLote).descr_apar = BL_HandleNull(rsLote!descr_apar, "")
                estrutAnaLote(totalAnaLote).cod_lote = BL_HandleNull(rsLote!cod_lote, "")
                estrutAnaLote(totalAnaLote).seq_apar = BL_HandleNull(rsLote!seq_apar, "")
                estrutAnaLote(totalAnaLote).seq_ana_s = BL_HandleNull(rsLote!seq_ana_s, "")
                
                aux = estrutAnaLote(totalAnaLote).seq_ana_s
                While Len(aux) < 5
                    aux = "0" & aux
                Wend
                aux = estrutAnaLote(totalAnaLote).seq_apar & aux
                EcAnalisesSel.AddItem estrutAnaLote(totalAnaLote).descr_ana_s & "(" & estrutAnaLote(totalAnaLote).descr_apar & ")"
                EcAnalisesSel.ItemData(EcAnalisesSel.NewIndex) = CLng(aux)
                rsLote.MoveNext
            Wend
        End If
        rsLote.Close
        Set rsLote = Nothing
    End If
End Sub

Sub FuncaoAnterior()
    
'nada

End Sub

Sub FuncaoSeguinte()
    
'nada
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        
        BD_Update
        BL_FimProcessamento Me
    End If

End Sub



Sub BD_Insert()
    'nada
End Sub
Sub FuncaoInserir()

    Dim iRes As Integer
    Dim SQLQuery As String
    Dim rsInsAux As ADODB.recordset

    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me
        BD_Insert
        BL_FimProcessamento Me
    End If
    
End Sub


Sub FuncaoRemover()

    Dim sql As String

    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja apagar o registo ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub

Sub BD_Delete()
    
'nada

End Sub


Public Sub eccodlote_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodLote.Text = UCase(EcCodLote.Text)
    If Trim(EcCodLote) <> "" Then

        sql = "SELECT " & _
              "     descricao " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "stk_lotes " & _
              "WHERE " & _
              "     codigo=" & (EcCodLote)

        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Lote inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrLote = ""
            EcCodLote = ""
        Else
            EcDescrLote.Text = rsCodigo!descricao
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrLote.Text = ""
        EcCodLote = ""
    End If

End Sub





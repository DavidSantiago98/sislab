VERSION 5.00
Begin VB.Form FormModifReq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormModifReq"
   ClientHeight    =   4785
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   8670
   Icon            =   "FormModifReq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   8670
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtConfirmar 
      Caption         =   "Confirmar"
      Height          =   735
      Left            =   3720
      Picture         =   "FormModifReq.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   3960
      Width           =   855
   End
   Begin VB.Frame Frame2 
      Caption         =   "Novos Dados"
      Height          =   1335
      Left            =   0
      TabIndex        =   16
      Top             =   2520
      Width           =   8535
      Begin VB.ComboBox CbTUtenteNew 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcUtenteNew 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         TabIndex        =   19
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox EcNomeUteNew 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   960
         TabIndex        =   18
         Top             =   840
         Width           =   7095
      End
      Begin VB.TextBox EcDtNascNew 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         TabIndex        =   17
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Utente"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   23
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Nome"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   22
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Data Nascimento"
         Height          =   255
         Index           =   6
         Left            =   3240
         TabIndex        =   21
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados Actuais"
      Height          =   1335
      Left            =   0
      TabIndex        =   8
      Top             =   840
      Width           =   8415
      Begin VB.TextBox EcDtNasc 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcNomeUte 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   840
         Width           =   7095
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   360
         Width           =   855
      End
      Begin VB.ComboBox CbTUtente 
         Height          =   315
         Left            =   960
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Data Nascimento"
         Height          =   255
         Index           =   5
         Left            =   3240
         TabIndex        =   14
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Nome"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   13
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Utente"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.TextBox EcSeqUtenteNew 
      Height          =   375
      Left            =   1680
      TabIndex        =   7
      Top             =   5400
      Width           =   1095
   End
   Begin VB.TextBox EcSeqUtenteOld 
      Height          =   375
      Left            =   480
      TabIndex        =   6
      Top             =   5400
      Width           =   1095
   End
   Begin VB.TextBox EcDtCri 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   7440
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   240
      Width           =   975
   End
   Begin VB.TextBox EcDtChega 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4200
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   240
      Width           =   975
   End
   Begin VB.TextBox EcNumReq 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   840
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Data Requisi��o"
      Height          =   255
      Index           =   2
      Left            =   6120
      TabIndex        =   4
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Data Chegada"
      Height          =   255
      Index           =   1
      Left            =   2880
      TabIndex        =   2
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   240
      Width           =   855
   End
End
Attribute VB_Name = "FormModifReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim ListarRemovidos As Boolean

'NELSONPSILVA Glintt-HS-18011 09.02.2018
Dim tabela_aux As String

Public rs As ADODB.recordset



Private Sub BtConfirmar_Click()
    Dim sSql As String
    Dim registos As Long
    On Error GoTo TrataErro
    If EcNumReq <> "" And EcSeqUtenteOld <> "" And EcSeqUtenteNew <> "" Then
        If BG_Mensagem(mediMsgBox, "Deseja realmente transferir a requisi��o?", vbQuestion + vbYesNo, "Transfer�ncia") = vbYes Then
            BG_BeginTransaction
            
            ' ---------------------------------------------------------------------------------------------------------------------------
            ' ALTERA SL_rEQUIS
            ' ---------------------------------------------------------------------------------------------------------------------------
            sSql = "UPDATE sl_requis SET flg_transf = 1, seq_utente = " & EcSeqUtenteNew & " WHERE n_req = " & EcNumReq & " AND seq_utente = " & EcSeqUtenteOld
            registos = BG_ExecutaQuery_ADO(sSql)
            If registos <= 0 Then
                BG_RollbackTransaction
                Exit Sub
            End If
            
            
            If gPassaRecFactus = mediSim Then
                ' ---------------------------------------------------------------------------------------------------------------------------
                ' ALTERA FA_MOVI_RESP
                ' ---------------------------------------------------------------------------------------------------------------------------
                sSql = "update fa_movi_resp set T_doente = " & BL_TrataStringParaBD(CbTUtenteNew.Text) & ", doente = " & BL_TrataStringParaBD(EcUtenteNew.Text)
                sSql = sSql & " WHERE T_doente = " & BL_TrataStringParaBD(CbTUtente.Text) & " AND doente = " & BL_TrataStringParaBD(EcUtente.Text)
                BG_ExecutaQuery_ADO sSql
                registos = BG_ExecutaQuery_ADO(sSql)
                ' ---------------------------------------------------------------------------------------------------------------------------
                ' ALTERA FA_MOVI_FACT
                ' ---------------------------------------------------------------------------------------------------------------------------
                sSql = "update fa_movi_fact set T_doente = " & BL_TrataStringParaBD(CbTUtenteNew.Text) & ", doente = " & BL_TrataStringParaBD(EcUtenteNew.Text)
                sSql = sSql & " WHERE T_doente = " & BL_TrataStringParaBD(CbTUtente.Text) & " AND doente = " & BL_TrataStringParaBD(EcUtente.Text)
                BG_ExecutaQuery_ADO sSql
                registos = BG_ExecutaQuery_ADO(sSql)
            End If
            
            ' ---------------------------------------------------------------------------------------------------------------------------
            ' APAGA RESULTADOS ANTERIORES PARA REQUISI��O EM CAUSA
            ' ---------------------------------------------------------------------------------------------------------------------------
            sSql = "UPDATE sl_res_alfan SET res_ant1 = null, res_ant2 = null, res_ant3 = null, dt_res_ant1 = null, dt_res_ant2  = null, "
            sSql = sSql & " dt_res_ant3 = null, local_ant1 = null, local_ant2 = null, local_ant3 = null WHERE seq_realiza in ("
            sSql = sSql & " SELECT seq_realiza FROM sl_Realiza WHERE n_Req = " & EcNumReq & ") "
            BG_ExecutaQuery_ADO sSql
            registos = BG_ExecutaQuery_ADO(sSql)
            sSql = "UPDATE sl_res_alfan_h SET res_ant1 = null, res_ant2 = null, res_ant3 = null, dt_res_ant1 = null, dt_res_ant2  = null, "
            sSql = sSql & " dt_res_ant3 = null, local_ant1 = null, local_ant2 = null, local_ant3 = null WHERE seq_realiza in ("
            sSql = sSql & " SELECT seq_realiza FROM sl_Realiza_h WHERE n_Req = " & EcNumReq & ") "
            BG_ExecutaQuery_ADO sSql
            registos = BG_ExecutaQuery_ADO(sSql)
            
            
            
            ' ---------------------------------------------------------------------------------------------------------------------------
            ' UPDATE SEQ_UTENTE NA SL_REALIZA
            ' ---------------------------------------------------------------------------------------------------------------------------
            sSql = "UPDATE sl_realiza SET seq_utente = " & EcSeqUtenteNew & " WHERE n_req = " & EcNumReq
            registos = BG_ExecutaQuery_ADO(sSql)
            sSql = "UPDATE sl_realiza_h SET seq_utente = " & EcSeqUtenteNew & " WHERE n_req = " & EcNumReq
            registos = BG_ExecutaQuery_ADO(sSql)
            
            
            ' ---------------------------------------------------------------------------------------------------------------------------
            ' INSERE REGISTO NA TABELA SL_REQUIS_MODIF
            ' ---------------------------------------------------------------------------------------------------------------------------
            sSql = "INSERT INTO sl_requis_modif (n_req, seq_utente_old, seq_utente_new, user_cri, dt_cri, hr_cri) VALUES("
            sSql = sSql & EcNumReq & ", "
            sSql = sSql & EcSeqUtenteOld & ", "
            sSql = sSql & EcSeqUtenteNew & ", "
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
            registos = BG_ExecutaQuery_ADO(sSql)
            BG_CommitTransaction
            BG_Mensagem mediMsgBox, "Requisi��o alterada com sucesso!", vbExclamation, "Transfer�ncia"
            
            
            'NELSONPSILVA Glintt-HS-18011 04.04.2018
            If gATIVA_LOGS_RGPD = mediSim Then
            Dim requestJson As String
            Dim responseJson As String
            Dim CamposEcra() As Object
            Dim NumCampos As Integer
            NumCampos = 12
            ReDim CamposEcra(0 To NumCampos - 1)
             Set CamposEcra(0) = EcNumReq
             Set CamposEcra(1) = EcDtChega
             Set CamposEcra(2) = EcDtCri
             Set CamposEcra(3) = EcSeqUtenteOld
             Set CamposEcra(4) = CbTUtente
             Set CamposEcra(5) = EcUtente
             Set CamposEcra(6) = EcDtNasc
             Set CamposEcra(7) = EcNomeUte
             Set CamposEcra(8) = CbTUtenteNew
             Set CamposEcra(9) = EcUtenteNew
             Set CamposEcra(10) = EcDtNascNew
             Set CamposEcra(11) = EcNomeUteNew
    
        requestJson = BL_TrataCamposRGPD(CamposEcra)
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Insercao) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        
        FuncaoLimpar
    End If
        End If
    End If
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  gravar alteracao " & sSql & " - " & Err.Description, Me.Name, "BtConfirmar_click", True
    Exit Sub
    Resume Next
End Sub

Private Sub CbTUtenteNew_Change()
    PreencheCamposNovoUtente
End Sub


Private Sub CbTUtenteNew_LostFocus()
PreencheCamposNovoUtente
End Sub

Private Sub EcUtenteNew_LostFocus()
    PreencheCamposNovoUtente
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Transfer�ncia de Requisi��es"
    Me.left = 5
    Me.top = 5
    Me.Width = 8760
    Me.Height = 5175 ' Normal
    
    NomeTabela = "sl_requis"
    Set CampoDeFocus = EcNumReq
    
    NumCampos = 4
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "dt_chega"
    CamposBD(2) = "dt_previ"
    CamposBD(3) = "seq_utente"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcDtChega
    Set CamposEc(2) = EcDtCri
    Set CamposEc(3) = EcSeqUtenteOld
    
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_Req"
    Set ChaveEc = EcNumReq
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    PreencheValoresDefeito
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormModifReq = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcUtente = ""
    CbTUtente.ListIndex = mediComboValorNull
    EcDtNasc = ""
    EcNomeUte = ""
    EcSeqUtenteOld = ""

    EcUtenteNew = ""
    CbTUtenteNew.ListIndex = mediComboValorNull
    EcDtNascNew = ""
    EcNomeUteNew = ""
    EcSeqUtenteNew = ""
End Sub

Sub DefTipoCampos()
    
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    End If
End Sub
Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTUtente
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTUtenteNew
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SimNao As Integer
    On Error GoTo TrataErro
    If EcNumReq = "" Then Exit Sub
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, False)
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        PreencheCamposUtente
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        
        BL_FimProcessamento Me
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Procurar Requisi��o " & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoInserir()
End Sub

Sub FuncaoModificar()
End Sub

Sub FuncaoRemover()
End Sub

' ---------------------------------------------------------------------------------------------------------------------------

' PREENCHE CAMPOS RELATIVAMENTE AO UTENTE

' ---------------------------------------------------------------------------------------------------------------------------
Private Sub PreencheCamposUtente()
    Dim sSql As String
    Dim rsUte As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM " & tabela_aux & " WHERE seq_utente = " & EcSeqUtenteOld
    rsUte.CursorType = adOpenStatic
    rsUte.CursorLocation = adUseServer
    rsUte.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsUte.Open sSql, gConexao
    If rsUte.RecordCount = 1 Then
        EcNomeUte = BL_HandleNull(rsUte!nome_ute, "")
        EcUtente = BL_HandleNull(rsUte!Utente, "")
        EcDtNasc = BL_HandleNull(rsUte!dt_nasc_ute, "")
        CbTUtente = BL_HandleNull(rsUte!t_utente, "")
    End If
    rsUte.Close
    Set rsUte = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  Preencher Dados do Utente Actual " & Err.Description, Me.Name, "PreencheCamposUtente", True
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------------------------------

' PREENCHE CAMPOS RELATIVAMENTE AO NOVO UTENTE

' ---------------------------------------------------------------------------------------------------------------------------
Private Sub PreencheCamposNovoUtente()
    Dim sSql As String
    Dim rsUte As New ADODB.recordset
    On Error GoTo TrataErro
    If EcUtenteNew <> "" And CbTUtenteNew.ListIndex <> mediComboValorNull And EcSeqUtenteOld <> "" Then
    
        sSql = "SELECT * FROM " & tabela_aux & " WHERE t_utente = " & BL_TrataStringParaBD(CbTUtenteNew)
        sSql = sSql & " AND utente  = " & BL_TrataStringParaBD(EcUtenteNew)
        rsUte.CursorType = adOpenStatic
        rsUte.CursorLocation = adUseServer
        rsUte.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsUte.Open sSql, gConexao
        If rsUte.RecordCount = 1 Then
            EcNomeUteNew = BL_HandleNull(rsUte!nome_ute, "")
            EcSeqUtenteNew = BL_HandleNull(rsUte!seq_utente, "")
            EcDtNascNew = BL_HandleNull(rsUte!dt_nasc_ute, "")
        Else
            BG_Mensagem mediMsgBox, "Utente n�o encontrado.", vbExclamation, "Procurar"
            EcNomeUteNew = ""
            EcSeqUtenteNew = ""
            EcDtNascNew = ""
            CbTUtenteNew.ListIndex = mediComboValorNull
            EcUtenteNew = ""
        End If
        rsUte.Close
        Set rsUte = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  Preencher Dados do Utente Novo " & Err.Description, Me.Name, "PreencheCamposNovoUtente", True
    Exit Sub
    Resume Next
End Sub


Attribute VB_Name = "OBJ_PROVEN"
Option Explicit

Public Function PROVEN_Get_Cod_SONHO(cod_proven As String) As String
    
    ' Mapeia para o c�digo do SONHO.
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim ret As String
    
    cod_proven = Trim(cod_proven)
    
    If (Len(cod_proven) = 0) Then
        PROVEN_Get_Cod_SONHO = ""
        Exit Function
    End If
    
    sql = "SELECT " & vbCrLf & _
          "     cod_sonho " & vbCrLf & _
          "FROM " & vbCrLf & _
          "     sl_proven " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "     cod_proven = " & cod_proven
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    If (rs.RecordCount > 0) Then
        ret = BL_HandleNull(rs(0), "")
    Else
        ret = ""
    End If
    rs.Close
    Set rs = Nothing

    PROVEN_Get_Cod_SONHO = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : PROVEN_Get_Cod_SONHO (OBJ_PROVEN) -> " & Err.Description)
            PROVEN_Get_Cod_SONHO = ""
            Exit Function
    End Select
End Function



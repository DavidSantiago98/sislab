Attribute VB_Name = "OBJ_Resultados_Anteriores"
Option Explicit

Public Function Get_Res_Anteriores(seq_utente As String, _
                                   Cod_Perfil As String, _
                                   cod_ana_c As String, _
                                   cod_ana_s As String, _
                                   seq_realiza As String, _
                                   ByRef res_ant1 As String, _
                                   ByRef dt_res_ant1 As String, _
                                   ByRef local_ant1 As String, _
                                   ByRef res_ant2 As String, _
                                   ByRef dt_res_ant2 As String, _
                                   ByRef local_ant2 As String, _
                                   ByRef res_ant3 As String, _
                                   ByRef dt_res_ant3 As String, _
                                   ByRef local_ant3 As String, _
                                   ByRef flg_res_ant1 As String, _
                                   ByRef flg_res_ant2 As String, _
                                   ByRef flg_res_ant3 As String) As Integer

    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim sql1 As String
    Dim RsRealiza As ADODB.recordset
    Dim RsRealiza1 As ADODB.recordset
    Dim RsRes As ADODB.recordset
    Dim data_aux As Date
    Dim i As Integer
    Dim j As Integer
    
    seq_utente = Trim(seq_utente)
    cod_ana_s = UCase(Trim(cod_ana_s))
    seq_realiza = Trim(seq_realiza)
    ReDim saida(3, 0)
    
    If ((Len(seq_utente) = 0) Or _
        (Len(cod_ana_s) = 0)) Then
        Get_Res_Anteriores = -2
        Exit Function
    End If
    
    Select Case gSGBD
    
        Case gOracle

        ' Bruno 08-01-2003 2
           sql1 = "SELECT " & vbCrLf & _
                  "     n_req " & vbCrLf & _
                  "FROM " & vbCrLf & _
                  "     sl_realiza " & vbCrLf & _
                  "WHERE " & vbCrLf & _
                  "     seq_realiza = " & seq_realiza & " "
                  
           Set RsRealiza1 = New ADODB.recordset
           RsRealiza1.CursorLocation = adUseServer
           RsRealiza1.CursorType = adOpenForwardOnly
           RsRealiza1.LockType = adLockReadOnly
                        
           RsRealiza1.Open sql1, gConexao
           
           If (Len(seq_realiza) = 0) Then
                sql = "SELECT " & vbCrLf & _
                      "     R.seq_realiza, " & vbCrLf & _
                      "     R.dt_val, " & vbCrLf & _
                      "     S.cod_local " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_realiza R, " & vbCrLf & _
                      "     sl_ana_s S " & vbCrLf
                If cod_ana_c <> "0" Then
                    sql = sql & ", sl_ana_c C " & vbCrLf
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & ", sl_perfis P " & vbCrLf
                End If
                sql = sql & "WHERE " & vbCrLf & _
                      "     R.cod_ana_s  = S.cod_ana_s AND S.cod_estatistica = '" & cod_ana_s & "' "
                If cod_ana_c <> "0" Then
                    sql = sql & " AND R.cod_ana_c = C.cod_ana_c AND C.cod_estatistica = '" & cod_ana_c & "' "
                Else
                    sql = sql & " AND R.cod_ana_c = '0' "
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & " AND R.cod_perfil = P.cod_perfis AND P.cod_estatistica = '" & Cod_Perfil & "' "
                Else
                    sql = sql & " AND R.cod_perfil = '0' "
                End If
                sql = sql & " AND (R.flg_estado = '3' or R.flg_estado='4') AND " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & vbCrLf & _
                      " ORDER BY " & vbCrLf & _
                      "     R.dt_val DESC, R.seq_realiza DESC"
            Else
                sql = "SELECT " & vbCrLf & _
                      "     R.seq_realiza, " & vbCrLf & _
                      "     R.dt_val, " & vbCrLf & _
                      "     S.cod_local " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_realiza R," & vbCrLf & _
                      "     sl_ana_s S " & vbCrLf
                If cod_ana_c <> "0" Then
                    sql = sql & ", sl_ana_c C " & vbCrLf
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & ", sl_perfis P " & vbCrLf
                End If
                sql = sql & "WHERE " & vbCrLf & _
                      "     R.cod_ana_s  = S.cod_ana_s AND S.cod_estatistica = '" & cod_ana_s & "' "
                If cod_ana_c <> "0" Then
                    sql = sql & " AND R.cod_ana_c = C.cod_ana_c AND C.cod_estatistica = '" & cod_ana_c & "' "
                Else
                    sql = sql & " AND R.cod_ana_c = '0' "
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & " AND R.cod_perfil = P.cod_perfis AND P.cod_estatistica = '" & Cod_Perfil & "' "
                Else
                    sql = sql & " AND R.cod_perfil = '0' "
                End If
                sql = sql & " AND " & _
                      "    (R.flg_estado = '3' or R.flg_estado='4') AND " & vbCrLf & _
                      "     R.seq_realiza < " & seq_realiza & " AND " & vbCrLf & _
                      "     R.n_req <> " & RsRealiza1!n_req & " AND " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & vbCrLf & _
                      " ORDER BY " & vbCrLf & _
                      "     dt_val DESC, seq_realiza DESC"
            End If
    
        Case gSqlServer
           
           If (Len(seq_realiza) = 0) Then
                sql = "SELECT TOP 3 " & vbCrLf & _
                      "     R.seq_realiza, " & vbCrLf & _
                      "     R.dt_val, " & vbCrLf & _
                      "     S.cod_local " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_realiza R, " & vbCrLf & _
                      "     sl_ana_s S " & vbCrLf
                If cod_ana_c <> "0" Then
                    sql = sql & ", sl_ana_c C " & vbCrLf
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & ", sl_perfis P " & vbCrLf
                End If
                sql = sql & "WHERE " & vbCrLf & _
                      "     R.cod_ana_s  = S.cod_ana_s AND S.cod_estatistica = '" & cod_ana_s & "' "
                If cod_ana_c <> "0" Then
                    sql = sql & " AND R.cod_ana_c = C.cod_ana_c AND C.cod_estatistica = '" & cod_ana_c & "' "
                Else
                    sql = sql & " AND R.cod_ana_c = '0' "
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & " AND R.cod_perfil = P.cod_perfis AND P.cod_estatistica = '" & Cod_Perfil & "' "
                Else
                    sql = sql & " AND R.cod_perfil = '0' "
                End If
                sql = sql & " AND " & _
                      "    (R.flg_estado = '3' or R.flg_estado='4') AND " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & vbCrLf & _
                      " ORDER BY " & vbCrLf & _
                      "     seq_realiza DESC"
            Else
                sql = "SELECT TOP 3 " & vbCrLf & _
                      "     R.seq_realiza, " & vbCrLf & _
                      "     R.dt_val, " & vbCrLf & _
                      "     S.cod_local " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_realiza R, " & vbCrLf & _
                      "     sl_ana_s S " & vbCrLf
                If cod_ana_c <> "0" Then
                    sql = sql & ", sl_ana_c C " & vbCrLf
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & ", sl_perfis P " & vbCrLf
                End If
                sql = sql & "WHERE " & vbCrLf & _
                      "     R.cod_ana_s  = S.cod_ana_s AND S.cod_estatistica = '" & cod_ana_s & "' "
                If cod_ana_c <> "0" Then
                    sql = sql & " AND R.cod_ana_c = C.cod_ana_c AND C.cod_estatistica = '" & cod_ana_c & "' "
                Else
                    sql = sql & " AND R.cod_ana_c = '0' "
                End If
                If Cod_Perfil <> "0" Then
                    sql = sql & " AND R.cod_perfil = P.cod_perfis AND P.cod_estatistica = '" & Cod_Perfil & "' "
                Else
                    sql = sql & " AND R.cod_perfil = '0' "
                End If
                sql = sql & " AND " & _
                      "    (R.flg_estado = '3' or R.flg_estado='4') AND " & vbCrLf & _
                      "     R.seq_realiza < " & seq_realiza & " AND " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & vbCrLf & _
                      " ORDER BY " & vbCrLf & _
                      "     seq_realiza DESC"
            End If
    
    End Select
    
    
    Set RsRealiza = New ADODB.recordset
    RsRealiza.CursorLocation = adUseServer
    RsRealiza.CursorType = adOpenForwardOnly
    RsRealiza.LockType = adLockReadOnly
    
    RsRealiza.Open sql, gConexao

'    If RsRealiza.RecordCount > 0 Then
    If Not (RsRealiza.EOF) Then
        
        i = 1
        j = 1
        
        While Not RsRealiza.EOF And i <= 4
            
            sql = "SELECT " & vbCrLf & _
                  "     seq_realiza, " & vbCrLf & _
                  "     result " & vbCrLf & _
                  "FROM " & vbCrLf & _
                  "     sl_res_alfan " & vbCrLf & _
                  "WHERE " & vbCrLf & _
                  "     n_res = 1 AND " & vbCrLf & _
                  "     seq_realiza = " & RsRealiza!seq_realiza & _
                  " "
            
            Set RsRes = New ADODB.recordset
            RsRes.CursorLocation = adUseServer
            RsRes.CursorType = adOpenForwardOnly
            RsRes.LockType = adLockReadOnly
            RsRes.Open sql, gConexao
            
'            If RsRes.RecordCount > 0 Then
            If Not (RsRes.EOF) Then
                
                While ((Not RsRes.EOF) And (j < 4))
                    
                    Select Case j
                        Case 1
                            ' RsRes!seq_realiza
                            res_ant1 = BL_HandleNull(RsRes!result, "")
                            dt_res_ant1 = BG_CvDataParaDisplay_ADO(RsRealiza!dt_val)
                            local_ant1 = BL_HandleNull(RsRealiza!cod_local, "")
                            'flg_res_ant1 = BL_HandleNull(RsRealiza!flg_anormal, "")
                        Case 2
                            ' RsRes!seq_realiza
                            res_ant2 = BL_HandleNull(RsRes!result, "")
                            dt_res_ant2 = BG_CvDataParaDisplay_ADO(RsRealiza!dt_val)
                            local_ant2 = BL_HandleNull(RsRealiza!cod_local, "")
                            'flg_res_ant2 = BL_HandleNull(RsRealiza!flg_anormal, "")
                        Case 3
                            ' RsRes!seq_realiza
                            res_ant3 = BL_HandleNull(RsRes!result, "")
                            dt_res_ant3 = BG_CvDataParaDisplay_ADO(RsRealiza!dt_val)
                            local_ant3 = BL_HandleNull(RsRealiza!cod_local, "")
                            'flg_res_ant3 = BL_HandleNull(RsRealiza!flg_anormal, "")
                    End Select
                    RsRes.MoveNext
                    j = j + 1
                Wend
            End If
            RsRes.Close
            Set RsRes = Nothing
            
            i = i + 1
            RsRealiza.MoveNext
        Wend
    End If
    RsRealiza.Close
    Set RsRealiza = Nothing
    
    Get_Res_Anteriores = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Get_Res_Anteriores (OBJ_Resultados_Anteriores) -> " & Err.Number & " : " & Err.Description)
            Set RsRes = Nothing
            Set RsRealiza = Nothing
            res_ant1 = ""
            dt_res_ant1 = ""
            local_ant1 = ""
            res_ant2 = ""
            dt_res_ant2 = ""
            local_ant2 = ""
            res_ant3 = ""
            dt_res_ant3 = ""
            local_ant3 = ""
            Get_Res_Anteriores = -1
            Exit Function
    End Select
End Function

' Para retirar.

Public Function Update_Res_Anteriores(req As String) As Integer

    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim Sql2 As String
    Dim rs As ADODB.recordset
    Dim cmd As ADODB.Command
    Dim rv As Integer
    
    Dim n_req As String
    
    Dim seq_utente As String
    Dim cod_ana_s As String
    Dim seq_realiza As String
    
    Dim res_ant1 As String
    Dim dt_res_ant1 As String
    Dim local_ant1 As String
    Dim flg_res_ant1 As String
    Dim res_ant2 As String
    Dim dt_res_ant2 As String
    Dim local_ant2 As String
    Dim flg_res_ant2 As String
    Dim res_ant3 As String
    Dim dt_res_ant3 As String
    Dim local_ant3 As String
    Dim flg_res_ant3 As String
    
    Dim n_proc As Long
    
    req = Trim(req)
    If (Len(req) = 0) Then
        Update_Res_Anteriores = -2
        Exit Function
    End If
    
    sql = "SELECT " & vbCrLf & _
          "     REA.seq_utente, " & vbCrLf & _
          "     REA.cod_ana_s, " & vbCrLf & _
          "     REA.seq_realiza, " & vbCrLf & _
          "     RES.Result, " & vbCrLf & _
          "     REA.n_req " & vbCrLf & _
          "FROM " & vbCrLf & _
          "     sl_realiza   REA, " & vbCrLf & _
          "     sl_res_alfan RES " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "     REA.seq_realiza = RES.seq_realiza AND " & vbCrLf & _
          "     REA.n_req > " & req & " " & _
          "ORDER BY " & vbCrLf & _
          "     REA.n_req"
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    Set cmd = New ADODB.Command
    cmd.ActiveConnection = gConexao
    
    n_proc = 0
    
    While Not rs.EOF
        
        n_req = Trim(rs(4))
'        MDIFormInicio.Text1.Text = n_req
        DoEvents
        DoEvents
        
        res_ant1 = ""
        dt_res_ant1 = ""
        local_ant1 = ""
        res_ant2 = ""
        dt_res_ant2 = ""
        local_ant2 = ""
        res_ant3 = ""
        dt_res_ant3 = ""
        local_ant3 = ""
        
        If (Not IsNull(rs(0))) Then
            
            seq_utente = Trim(rs(0))
            
            If (Not IsNull(rs(1))) Then
                
                cod_ana_s = Trim(rs(1))
                    
                If (Not IsNull(rs(2))) Then
                    
                    seq_realiza = Trim(rs(2))
                    
                    rv = Get_Res_Anteriores(seq_utente, _
                                            "cod_ana_s", _
                                            "cod_ana_s", _
                                            cod_ana_s, _
                                            seq_realiza, _
                                            res_ant1, _
                                            dt_res_ant1, _
                                            local_ant1, _
                                            res_ant2, _
                                            dt_res_ant2, _
                                            local_ant2, _
                                            res_ant3, _
                                            dt_res_ant3, _
                                            local_ant3, _
                                            flg_res_ant1, _
                                            flg_res_ant2, _
                                            flg_res_ant3)
                
                    If IsNull(res_ant1) Then
                        res_ant1 = "NULL"
                    Else
                        res_ant1 = "'" & left(Trim(res_ant1), 20) & "'"
                    End If
                    If IsNull(dt_res_ant1) Then
                        dt_res_ant1 = "NULL"
                    Else
                        dt_res_ant1 = "'" & left(Trim(dt_res_ant1), 10) & "'"
                    End If
                    
                    If IsNull(local_ant1) Then
                        local_ant1 = "NULL"
                    Else
                        local_ant1 = "'" & BL_DevolveDescrLocal(local_ant1) & "'"
                    End If
                                    
                    ' =====================================================
                        
                    If IsNull(res_ant2) Then
                        res_ant2 = "NULL"
                    Else
                        res_ant2 = "'" & left(Trim(res_ant2), 20) & "'"
                    End If
                    If IsNull(dt_res_ant2) Then
                        dt_res_ant2 = "NULL"
                    Else
                        dt_res_ant2 = "'" & left(Trim(dt_res_ant2), 10) & "'"
                    End If
                    
                    If IsNull(local_ant2) Then
                        local_ant2 = "NULL"
                    Else
                        local_ant2 = "'" & BL_DevolveDescrLocal(local_ant2) & "'"
                    End If
                                        
                    ' =====================================================
                    If IsNull(res_ant3) Then
                        res_ant3 = "NULL"
                    Else
                        res_ant3 = "'" & left(Trim(res_ant3), 20) & "'"
                    End If
                    If IsNull(dt_res_ant3) Then
                        dt_res_ant3 = "NULL"
                    Else
                        dt_res_ant3 = "'" & left(Trim(dt_res_ant3), 10) & "'"
                    End If
                                    
                    If IsNull(local_ant3) Then
                        local_ant3 = "NULL"
                    Else
                        local_ant3 = "'" & BL_DevolveDescrLocal(local_ant3) & "'"
                    End If
                    
                    
                    
                    Sql2 = "UPDATE " & vbCrLf & _
                           "        sl_res_alfan " & vbCrLf & _
                           "SET " & vbCrLf & _
                           "        res_ant1    = " & res_ant1 & ", " & vbCrLf & _
                           "        dt_res_ant1 = " & dt_res_ant1 & ", " & vbCrLf & _
                           "        local_ant1 = " & local_ant1 & ", " & vbCrLf & _
                           "        res_ant2    = " & res_ant2 & ", " & vbCrLf & _
                           "        dt_res_ant2 = " & dt_res_ant2 & ", " & vbCrLf & _
                           "        local_ant2 = " & local_ant2 & ", " & vbCrLf & _
                           "        res_ant3    = " & res_ant3 & ", " & vbCrLf & _
                           "        dt_res_ant3 = " & dt_res_ant3 & " " & vbCrLf & _
                           "        local_ant3 = " & local_ant3 & ", " & vbCrLf & _
                           "WHERE " & vbCrLf & _
                           "        seq_realiza = " & seq_realiza & " "
                    
                    cmd.CommandText = Sql2
                    cmd.Execute
                
                Else
                    ' Erro.
                    Call BG_LogFile_Erros("Erro Update_Res_Anteriores (OBJ_Resultados_Anteriores) -> seq_realiza = NULL : " & n_req)
                End If
            Else
                ' Erro.
                Call BG_LogFile_Erros("Erro Update_Res_Anteriores (OBJ_Resultados_Anteriores) -> cod_ana_s = NULL : " & n_req)
            End If
        Else
            ' Erro.
            Call BG_LogFile_Erros("Erro Update_Res_Anteriores (OBJ_Resultados_Anteriores) -> seq_utente = NULL : " & n_req)
        End If
        
        rs.MoveNext
        
        DoEvents
        DoEvents
        DoEvents
    
        n_proc = n_proc + 1
        
    Wend
    
    rs.Close
    Set cmd = Nothing
    Set rs = Nothing
    
    MsgBox n_proc & " processados"
    
    Update_Res_Anteriores = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Update_Res_Anteriores (OBJ_Resultados_Anteriores) -> " & Err.Number & " : " & Err.Description)
            Set rs = Nothing
            MsgBox "KO!"
            Update_Res_Anteriores = -1
            Exit Function
    End Select
End Function

Public Function Get_Res_Micro_Anteriores(n_req As String, seq_utente As String, _
                                   Cod_Perfil As String, _
                                   cod_ana_c As String, _
                                   cod_ana_s As String, _
                                   seq_realiza As String, _
                                   ByRef res_ant1 As String, _
                                   ByRef dt_res_ant1 As String, _
                                   ByRef res_ant2 As String, _
                                   ByRef dt_res_ant2 As String, _
                                   ByRef res_ant3 As String, _
                                   ByRef dt_res_ant3 As String) As Integer

    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim sql1 As String
    Dim RsRealiza As ADODB.recordset
    Dim RsRes As ADODB.recordset
    Dim data_aux As Date
    Dim i As Integer
    Dim j As Integer
    
    seq_utente = Trim(seq_utente)
    cod_ana_s = UCase(Trim(cod_ana_s))
    seq_realiza = Trim(seq_realiza)
    ReDim saida(3, 0)
    
    If ((Len(seq_utente) = 0) Or _
        (Len(cod_ana_s) = 0)) Then
        Get_Res_Micro_Anteriores = -2
        Exit Function
    End If
    


    sql = "SELECT Real.n_req, R.seq_realiza,Real.dt_val, R.cod_micro, " & _
            " R.n_res,R.quantif,R.flg_tsq,M.descr_microrg " & _
            " FROM sl_res_micro R, sl_realiza REAL, sl_microrg M " & _
            " WHERE Real.seq_realiza = r.seq_realiza " & _
            " AND REAL.seq_utente =" & seq_utente & " AND REAL.seq_realiza < " & seq_realiza & _
            " AND REAL.n_req <> " & n_req & _
            " AND REAL.cod_perfil ='" & Cod_Perfil & "' AND REAL.cod_ana_c ='" & cod_ana_c & "'" & _
            " AND REAL.cod_ana_s ='" & cod_ana_s & "' AND (flg_estado = '3' or flg_estado='4') " & _
            " AND R.flg_imp='S' AND R.n_res = 1 AND R.cod_micro=M.cod_microrg " & _
            " ORDER BY REAL.dt_val DESC, R.seq_realiza DESC, M.descr_microrg "
            '" ORDER BY REAL.n_req desc, R.seq_realiza, M.descr_microrg "
            

    Set RsRealiza = New ADODB.recordset
    RsRealiza.CursorLocation = adUseServer
    RsRealiza.CursorType = adOpenForwardOnly
    RsRealiza.LockType = adLockReadOnly
    
    RsRealiza.Open sql, gConexao
    If Not (RsRealiza.EOF) Then
        
        i = 1
        While Not RsRealiza.EOF And i <= 4
                    
            Select Case i
                Case 1
                    ' RsRes!seq_realiza
                    res_ant1 = BL_HandleNull(RsRealiza!cod_micro, "")
                    dt_res_ant1 = BG_CvDataParaDisplay_ADO(RsRealiza!dt_val)
                Case 2
                    ' RsRes!seq_realiza
                    res_ant2 = BL_HandleNull(RsRealiza!cod_micro, "")
                    dt_res_ant2 = BG_CvDataParaDisplay_ADO(RsRealiza!dt_val)
                Case 3
                    ' RsRes!seq_realiza
                    res_ant3 = BL_HandleNull(RsRealiza!cod_micro, "")
                    dt_res_ant3 = BG_CvDataParaDisplay_ADO(RsRealiza!dt_val)
            End Select
            RsRealiza.MoveNext
            i = i + 1
        Wend

    End If
    RsRealiza.Close
    Set RsRealiza = Nothing
    
    Get_Res_Micro_Anteriores = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Get_Res_Micro_Anteriores (OBJ_Resultados_Anteriores) -> " & Err.Number & " : " & Err.Description)
            Set RsRes = Nothing
            Set RsRealiza = Nothing
            res_ant1 = ""
            dt_res_ant1 = ""
            res_ant2 = ""
            dt_res_ant2 = ""
            res_ant3 = ""
            dt_res_ant3 = ""
            Get_Res_Micro_Anteriores = -1
            Exit Function
    End Select
End Function


                
                
               


Attribute VB_Name = "OBJ_Requisicao"
Option Explicit

' Actualiza��o : 27/02/2002
' T�cnico Paulo Costa

'************************************************************************
'*
'*  Objecto REQUISI��O
'*
'************************************************************************

Public Function REQUISICAO_UtentePagouTaxa(n_req As String) As String
    
    ' Indica ...
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim RsTaxa As ADODB.recordset
    Dim ret As String
    
    n_req = Trim(n_req)
    
    If (Len(n_req) = 0) Then
        REQUISICAO_UtentePagouTaxa = "N"
        Exit Function
    End If
    
    ret = "N"
    
    sql = "SELECT " & _
          "     cod_isencao " & _
          "FROM " & _
          "     sl_requis " & _
          "WHERE " & _
          "n_req = " & n_req
    
    Set RsTaxa = New ADODB.recordset
    RsTaxa.CursorLocation = adUseServer
    RsTaxa.CursorType = adOpenForwardOnly
    RsTaxa.LockType = adLockReadOnly
    
    RsTaxa.Open sql, gConexao
    
    If (RsTaxa.RecordCount > 0) Then
        If BL_HandleNull(RsTaxa!cod_isencao, "") <> "" Then
            ret = "N"
        Else
            ret = "S"
        End If
    End If
    RsTaxa.Close
    Set RsTaxa = Nothing

    REQUISICAO_UtentePagouTaxa = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_UtentePagouTaxa (OBJ_Requisicao) -> " & Err.Description)
            REQUISICAO_UtentePagouTaxa = "N"
            Exit Function
    End Select
End Function

Public Function REQUISICAO_Data_Criacao(num_req As String) As String

    ' Devolve a data de cria��o de uma requisi��o.
    ' null em caso de erro.

    On Error GoTo ErrorHandler
    
    Dim str_sql As String
    Dim rs As ADODB.recordset
    Dim ret As String
    
    ret = ""
    
    str_sql = "SELECT " & vbCrLf & _
              "     dt_cri " & vbCrLf & _
              "FROM " & _
              "     sl_requis " & vbCrLf & _
              "WHERE " & _
              "     n_req = " & num_req & " "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open str_sql, gConexao, adOpenForwardOnly, adLockReadOnly
    
    If (rs.EOF) Then
        ret = ""
    Else
        If IsNull(rs(0)) Then
            ret = ""
        Else
            ret = "" & left(Trim(rs(0)), 10) & ""
            ret = Format(ret, gFormatoData)
        End If
    End If

    rs.Close
    Set rs = Nothing
            
    REQUISICAO_Data_Criacao = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Data_Criacao (OBJ_Requisicao) -> " & Err.Description)

            Set rs = Nothing
            REQUISICAO_Data_Criacao = ""
            Exit Function
    End Select
End Function

' Marca uma requisi��o como enviada para a factura��o.

Public Sub REQUISICAO_Set_Enviada_Facturacao(num_req As String, flg As Boolean)

    On Error GoTo ErrorHandler

    Dim str_sql As String
    Dim cmd As ADODB.Command
    Dim flg_env As String

    num_req = Trim(num_req)
    If (Len(num_req) = 0) Then
        Exit Sub
    End If
    
    Set cmd = New ADODB.Command
    
    If (flg = True) Then
        flg_env = factEnviadaFact
    Else
        flg_env = factPorEnviarFact
    End If
    
    str_sql = "UPDATE sl_requis " & _
              "SET    flg_facturado = " & factEnviadaFact & " " & _
              "WHERE  n_req = " & num_req
    
    cmd.ActiveConnection = gConexao
    cmd.CommandText = str_sql
    cmd.Execute
        
    Set cmd = Nothing

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Set_Enviada_Facturacao (OBJ_Requisicao) -> " & Err.Description)
            Exit Sub
    End Select
End Sub

' Verifica se o utilizador � isento para uma dada requisi��o.

Public Sub REQUISICAO_Isencao(num_req As String, _
                              ByRef isento As Boolean, _
                              ByRef tipo_isencao As String)

    On Error GoTo ErrorHandler

    Dim str_sql As String
    Dim rs As ADODB.recordset
    Dim ret As Boolean

    num_req = Trim(num_req)
    If (Len(num_req) = 0) Then
        isento = False
        tipo_isencao = ""
        Exit Sub
    End If
    
    If (gSGBD = gSqlServer) Or (gSGBD = gOracle) Then
        
        str_sql = "SELECT " & _
                  "     cod_isencao " & _
                  "FROM " & _
                  "     sl_requis " & _
                  "WHERE " & _
                  "     n_req = " & num_req
    End If
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    
    rs.Open str_sql, gConexao, adOpenForwardOnly, adLockReadOnly
    
    If (rs.EOF) Then
        ' A requisi��o n�o existe.
        isento = False
        tipo_isencao = ""
        BG_LogFile_Erros ("Aviso : REQUISICAO_Isencao (FormGestaoCredenciais) -> A requisi��o n�o existe.")
    Else
        If IsNull(rs(0)) Then
            isento = False
            tipo_isencao = ""
        Else
            If (Len(Trim(rs(0))) = 0) Then
                isento = False
                tipo_isencao = ""
            Else
                isento = True
                tipo_isencao = Trim(rs(0))
            End If
        End If
    End If

    rs.Close
    Set rs = Nothing

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : REQUISICAO_Isencao (OBJ_Requisicao) -> " & Err.Description)
            Set rs = Nothing
            Exit Sub
    End Select
End Sub

' Altera o domic�lio para a requisi��o (1 ou 2 ou 3 + Km : Factura��o).

Public Function REQUISICAO_Set_Cod_Urbano(num_req As String, _
                                          cod_urbano As String, _
                                          km As String) As Integer
    On Error GoTo ErrorHandler

    Dim sql As String
    Dim cmd As ADODB.Command

    num_req = Trim(num_req)
    cod_urbano = Trim(cod_urbano)
    km = Trim(km)
    If (Len(km) = 0) Then
        km = "0"
    End If
    
    If ((Len(num_req) = 0) Or _
         Not (IsNumeric(cod_urbano)) Or _
         Not (IsNumeric(km))) Then
        
        REQUISICAO_Set_Cod_Urbano = -2
        Exit Function
    End If
    
    ' ?
    If (CDbl(km) < 0) Then
        km = 0
    End If
    
    Set cmd = New ADODB.Command
    
    Select Case cod_urbano
        Case "1"
            sql = "UPDATE sl_requis " & _
                  "SET    cod_urbano = 1, " & _
                  "       km         = NULL " & _
                  "WHERE  n_req = " & num_req
        Case "2"
            sql = "UPDATE sl_requis " & _
                  "SET    cod_urbano = 2, " & _
                  "       km         = NULL " & _
                  "WHERE  n_req = " & num_req
        Case "3"
            sql = "UPDATE sl_requis " & _
                  "SET    cod_urbano = 3, " & _
                  "       km         = " & km & " " & _
                  "WHERE  n_req = " & num_req
        Case Else
            REQUISICAO_Set_Cod_Urbano = -4
            Exit Function
    End Select
    
    cmd.ActiveConnection = gConexao
    cmd.CommandText = sql
    cmd.Execute
        
    Set cmd = Nothing

    REQUISICAO_Set_Cod_Urbano = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Set_Cod_Urbano (OBJ_Requisicao) -> " & Err.Description)
            
            REQUISICAO_Set_Cod_Urbano = -1
            Exit Function
    End Select
End Function

Public Function REQUISICAO_Situacao_Episodio(num_req As String, _
                                             ByRef situacao As String, _
                                             ByRef episodio) As Integer

    ' Devolve a situacao e data de cria��o de uma requisi��o.
    ' "", "" e -1 em caso de erro.

    On Error GoTo ErrorHandler
    
    Dim str_sql As String
    Dim rs As ADODB.recordset
    Dim ret As String
    
    situacao = ""
    episodio = ""
    ret = ""
    
    str_sql = "SELECT " & vbCrLf & _
              "     t_sit, " & vbCrLf & _
              "     n_epis " & vbCrLf & _
              "FROM " & _
              "     sl_requis " & vbCrLf & _
              "WHERE " & _
              "     n_req = " & num_req & " "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open str_sql, gConexao, adOpenForwardOnly, adLockReadOnly
    
    If (rs.EOF) Then
        ret = -2
    Else
            
        If (Not IsNull(rs(0))) Then
            situacao = Trim(rs(0))
        Else
            situacao = ""
        End If
            
        If (Not IsNull(rs(1))) Then
            episodio = Trim(rs(1))
        Else
            episodio = ""
        End If
        
        ret = 1
    End If

    rs.Close
    Set rs = Nothing
            
    REQUISICAO_Situacao_Episodio = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Situacao_Episodio (OBJ_Requisicao) -> " & Err.Description)
            Set rs = Nothing
            situacao = ""
            episodio = ""
            REQUISICAO_Situacao_Episodio = -1
            Exit Function
    End Select
End Function

Public Function REQUISICAO_Conta_Grupos_Req(NReq As String) As Integer

    On Error GoTo ErrorHandler

    Dim rs As ADODB.recordset
    Dim sql As String
    Dim ret As Integer
    Dim aux As String
    Dim aux2 As Integer

    NReq = Trim(NReq)

    If (Len(NReq) = 0) Then
        REQUISICAO_Conta_Grupos_Req = 0
        Exit Function
    End If

    sql = "SELECT " & _
          "     gr_ana " & _
          "FROM " & _
          "     sl_requis " & _
          "WHERE " & _
          "     n_req = " & NReq

    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
        
    If Not (rs.EOF) And Not (IsNull(rs(0))) Then
        aux = rs(0)
        ret = 1

        aux2 = InStr(1, aux, ";", vbTextCompare)
        While (aux2 <> 0)
            aux = Mid(aux, aux2 + 1, Len(aux))
            ret = ret + 1
            aux2 = InStr(aux2, aux, ";", vbTextCompare)
        Wend
    Else
        REQUISICAO_Conta_Grupos_Req = 0
        Exit Function
    End If
    
    rs.Close
    Set rs = Nothing

    REQUISICAO_Conta_Grupos_Req = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro REQUISICAO_Conta_Grupos_Req (OBJ_Requisicao) -> " & Err.Number & " : " & Err.Description)
            Set rs = Nothing
            REQUISICAO_Conta_Grupos_Req = -1
            Exit Function
    End Select
End Function

Public Function REQUISICAO_Get_Com_Analise_Pendente(cod_ana As String, _
                                                    seq_utente As String, _
                                                    ByRef data_req As String) As String
    
    ' Procura a requisi��o mais recente com a analise cod_ana pendente, para o utente seq_utente.
    ' N�o procura perfis.
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim ret As String
    Dim data_ini As Date
    
    Dim dia As String
    Dim mes As String
    Dim ano As String
    
    cod_ana = Trim(cod_ana)
    seq_utente = Trim(seq_utente)
    data_req = ""
    
    If (Len(cod_ana) = 0) Or _
       (Len(seq_utente) = 0) Then
        REQUISICAO_Get_Com_Analise_Pendente = ""
        Exit Function
    End If
    
    ' Por defeito : vai ver at� um m�s atras.
    data_ini = DateAdd("m", -1, Date)
    
    dia = Right("00" & Trim(CStr(Day(data_ini))), 2)
    mes = Right("00" & Trim(CStr(Month(data_ini))), 2)
    ano = Right("0000" & Trim(CStr(Year(data_ini))), 4)
    
    ret = ""
    
    Select Case left(cod_ana, 1)
        Case "P"
            ' Deixa passar. N�o procura
            REQUISICAO_Get_Com_Analise_Pendente = ""
            Exit Function
        Case "C"
        
            If (gSGBD = gOracle) Then
                sql = "SELECT DISTINCT " & vbCrLf & _
                      "     R.seq_utente, " & vbCrLf & _
                      "     M.n_req as numreq, " & vbCrLf & _
                      "     M.cod_ana_c, " & vbCrLf & _
                      "     M.dt_chega " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_requis    R, " & vbCrLf & _
                      "     sl_marcacoes M " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & " AND " & vbCrLf & _
                      "     R.n_req = M.n_req AND " & vbCrLf & _
                      "     M.cod_ana_c = '" & cod_ana & "' AND " & vbCrLf & _
                      "     M.dt_chega >= TO_DATE('" & dia & "-" & mes & "-" & ano & "', 'DD-MM-YYYY')" & vbCrLf & _
                      "ORDER BY " & vbCrLf & _
                      "     M.n_req DESC "
            End If
            
            If (gSGBD = gSqlServer) Then
                sql = "SELECT DISTINCT " & vbCrLf & _
                      "     R.seq_utente, " & vbCrLf & _
                      "     M.n_req as numreq, " & vbCrLf & _
                      "     M.cod_ana_c, " & vbCrLf & _
                      "     M.dt_chega " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_requis    R, " & vbCrLf & _
                      "     sl_marcacoes M " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & " AND " & vbCrLf & _
                      "     R.n_req = M.n_req AND " & vbCrLf & _
                      "     M.cod_ana_c = '" & cod_ana & "' AND " & vbCrLf & _
                      "     M.dt_chega >= '" & dia & "-" & mes & "-" & ano & "'" & vbCrLf & _
                      "ORDER BY " & vbCrLf & _
                      "     M.n_req DESC "
            End If
            
        Case "S"
            
            If (gSGBD = gOracle) Then
                sql = "SELECT DISTINCT " & vbCrLf & _
                      "     R.seq_utente, " & vbCrLf & _
                      "     M.n_req as numreq, " & vbCrLf & _
                      "     M.cod_ana_s, " & vbCrLf & _
                      "     M.dt_chega " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_requis    R, " & vbCrLf & _
                      "     sl_marcacoes M " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & " AND " & vbCrLf & _
                      "     R.n_req = M.n_req AND " & vbCrLf & _
                      "     M.cod_ana_s = '" & cod_ana & "' AND " & vbCrLf & _
                      "     M.dt_chega >= TO_DATE('" & dia & "-" & mes & "-" & ano & "', 'DD-MM-YYYY')" & vbCrLf & _
                      "ORDER BY " & vbCrLf & _
                      "     M.n_req DESC "
            End If
        
            If (gSGBD = gSqlServer) Then
                sql = "SELECT DISTINCT " & vbCrLf & _
                      "     R.seq_utente, " & vbCrLf & _
                      "     M.n_req as numreq, " & vbCrLf & _
                      "     M.cod_ana_s, " & vbCrLf & _
                      "     M.dt_chega " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_requis    R, " & vbCrLf & _
                      "     sl_marcacoes M " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     R.seq_utente = " & seq_utente & " AND " & vbCrLf & _
                      "     R.n_req = M.n_req AND " & vbCrLf & _
                      "     M.cod_ana_s = '" & cod_ana & "' AND " & vbCrLf & _
                      "     M.dt_chega >= '" & dia & "-" & mes & "-" & ano & "'" & vbCrLf & _
                      "ORDER BY " & vbCrLf & _
                      "     M.n_req DESC "
            End If
        
        Case Else
            REQUISICAO_Get_Com_Analise_Pendente = ""
            Exit Function
    End Select
        
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
       
    rs.Open sql, gConexao
    
    data_req = ""
    If (rs.EOF) Then
        ret = ""
    Else
        ret = BL_HandleNull(rs!NumReq, "")
    
        data_req = BL_HandleNull(rs!dt_chega, "")
        
        If (Len(rs!dt_chega) > 0) Then
            data_req = Format(data_req, gFormatoData)
        Else
            data_req = "?"
        End If
    
    End If
    
    rs.Close
    Set rs = Nothing
    
    REQUISICAO_Get_Com_Analise_Pendente = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Get_Com_Analise_Pendente (OBJ_Requisicao) -> " & Err.Description)
            REQUISICAO_Get_Com_Analise_Pendente = ""
            Exit Function
    End Select
End Function

Public Function REQUISICAO_Get_Com_Analise_Pendente_Consultas(cod_ana As String, _
                                                              seq_utente As String, _
                                                              ByRef data_req As String) As String
    
    ' Procura a requisi��o mais recente com a analise cod_ana pendente, para o utente seq_utente.
    ' N�o procura perfis.
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim ret As String
    Dim data_ini As Date
    Dim dia As Integer
    Dim mes As Integer
    Dim ano As Integer
    
    cod_ana = Trim(cod_ana)
    seq_utente = Trim(seq_utente)
    data_req = ""
    
    If (Len(cod_ana) = 0) Or _
       (Len(seq_utente) = 0) Then
        REQUISICAO_Get_Com_Analise_Pendente_Consultas = ""
        Exit Function
    End If
    
    ' Por defeito : vai ver at� um m�s atras.
    data_ini = DateAdd("m", -1, Date)
    dia = Day(data_ini)
    mes = Month(data_ini)
    ano = Year(data_ini)
    
    ret = ""
    
    Select Case left(cod_ana, 1)
        Case "P"
            ' Deixa passar. N�o procura
            REQUISICAO_Get_Com_Analise_Pendente_Consultas = ""
            Exit Function
        Case "C"
            sql = "SELECT DISTINCT " & vbCrLf & _
                  "     R.seq_utente, " & vbCrLf & _
                  "     M.n_req as numreq, " & vbCrLf & _
                  "     M.cod_ana_c, " & vbCrLf & _
                  "     M.dt_chega " & vbCrLf & _
                  "FROM " & vbCrLf & _
                  "     sl_requis_consultas    R, " & vbCrLf & _
                  "     sl_marcacoes_consultas M " & vbCrLf & _
                  "WHERE " & vbCrLf & _
                  "     R.seq_utente = " & seq_utente & " AND " & vbCrLf & _
                  "     R.n_req = M.n_req AND " & vbCrLf & _
                  "     M.cod_ana_c = '" & cod_ana & "' AND " & vbCrLf & _
                  "     M.dt_chega >= TO_DATE('" & dia & "-" & mes & "-" & ano & "', 'DD-MM-YYYY')" & vbCrLf & _
                  "ORDER BY " & vbCrLf & _
                  "     M.n_req DESC "
        Case "S"
            sql = "SELECT DISTINCT " & vbCrLf & _
                  "     R.seq_utente, " & vbCrLf & _
                  "     M.n_req as numreq, " & vbCrLf & _
                  "     M.cod_ana_s, " & vbCrLf & _
                  "     M.dt_chega " & vbCrLf & _
                  "FROM " & vbCrLf & _
                  "     sl_requis_consultas    R, " & vbCrLf & _
                  "     sl_marcacoes_consultas M " & vbCrLf & _
                  "WHERE " & vbCrLf & _
                  "     R.seq_utente = " & seq_utente & " AND " & vbCrLf & _
                  "     R.n_req = M.n_req AND " & vbCrLf & _
                  "     M.cod_ana_s = '" & cod_ana & "' AND " & vbCrLf & _
                  "     M.dt_chega >= TO_DATE('" & dia & "-" & mes & "-" & ano & "', 'DD-MM-YYYY')" & vbCrLf & _
                  "ORDER BY " & vbCrLf & _
                  "     M.n_req DESC "
        Case Else
            REQUISICAO_Get_Com_Analise_Pendente_Consultas = ""
            Exit Function
    End Select
        
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    data_req = ""
    If (rs.EOF) Then
        ret = ""
    Else
        ret = BL_HandleNull(rs!NumReq, "")
    
        data_req = BL_HandleNull(rs!dt_chega, "")
        
        If (Len(rs!dt_chega) > 0) Then
            data_req = Format(data_req, gFormatoData)
        Else
            data_req = "?"
        End If
    
    End If

    
    rs.Close
    Set rs = Nothing
    
    REQUISICAO_Get_Com_Analise_Pendente_Consultas = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Get_Com_Analise_Pendente_Consultas (OBJ_Requisicao) -> " & Err.Description)
            REQUISICAO_Get_Com_Analise_Pendente_Consultas = ""
            Exit Function
    End Select
End Function

Public Function MARCACAO_Apaga(n_marcacao As String) As String
    
    ' Elimina uma marcacao.
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim ret As String
    
    n_marcacao = Trim(n_marcacao)
    
    If (Len(n_marcacao) = 0) Then
        MARCACAO_Apaga = -1
        Exit Function
    End If
    
    ret = 1
    
    
    
    ' Tabela de requisi��es.
    sql = "update sl_requis_consultas SET flg_invisivel = 1 " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "     n_req = " & n_marcacao
    
    BG_ExecutaQuery_ADO sql
    

    MARCACAO_Apaga = 1
    

Exit Function
ErrorHandler:
    BG_LogFile_Erros "Erro Inesperado : MARCACAO_Apaga (OBJ_Requisicao) -> " & Err.Description, "OBJ_REQUISICAO", "Marcacao_apaga", True
    MARCACAO_Apaga = -1
    Exit Function
End Function

Public Function REQUISICAO_Tem_Microbiologia(num_req As String) As Boolean

    ' Indica se a requisicao contem an�lises de Microbiologia.

    On Error GoTo ErrorHandler
    
    Dim cod_microbiologia As String
    Dim cod_micobacterias As String
    Dim str_grupos As String
    Dim str_sql As String
    Dim rs As ADODB.recordset
    Dim ret As String
    
    ' Determina o c�digo da microbiologia para cada cliente.
    Select Case gLAB
        Case cHSMARTA, "CHVNG"
            cod_microbiologia = ";6;"
            cod_micobacterias = ";9;"
        Case Else
            cod_microbiologia = ";XYZ;"
    End Select
    
    ret = False
    
    str_sql = "SELECT " & vbCrLf & _
              "     gr_ana " & vbCrLf & _
              "FROM " & _
              "     sl_requis " & vbCrLf & _
              "WHERE " & _
              "     n_req = " & num_req & " "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open str_sql, gConexao, adOpenForwardOnly, adLockReadOnly
    
    If (rs.EOF) Then
        ret = False
    Else
        If IsNull(rs(0)) Then
            ret = False
        Else
            str_grupos = ";" & Trim(rs(0)) & ";"
            If (InStr(str_grupos, cod_microbiologia) <> 0) Or (InStr(str_grupos, cod_micobacterias) <> 0) Then
                ret = True
            Else
                ret = False
            End If
        End If
    End If

    rs.Close
    Set rs = Nothing
            
    REQUISICAO_Tem_Microbiologia = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Tem_Microbiologia (OBJ_Requisicao) -> " & Err.Description)
            Set rs = Nothing
            REQUISICAO_Tem_Microbiologia = False
            Exit Function
    End Select
End Function




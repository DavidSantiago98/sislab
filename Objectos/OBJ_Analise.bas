Attribute VB_Name = "OBJ_Analise"
Option Explicit

' Actualiza��o : 19/02/2002
' T�cnico Paulo Costa

'************************************************************************
'*
'*  Objecto AN�LISE
'*               Simples
'*               Complexa
'*               Perfil
'*
'************************************************************************

Public Function ANALISE_Para_Facturar(cod_ana As String, _
                                      cod_efr As String) As Boolean
    
    ' Determina se uma an�lise deve ser enviada para a factura��o.
    
'    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As Boolean
    
    ret = False
    
    cod_ana = UCase(Trim(cod_ana))
    cod_efr = UCase(Trim(cod_efr))
    
    If ((Len(cod_ana) = 0) Or (Len(cod_efr) = 0)) Then
        ANALISE_Para_Facturar = False
        Exit Function
    End If
        
    sql = "SELECT " & _
          "     cod_ana " & _
          "FROM " & _
          "     sl_nao_facturar " & _
          "WHERE " & _
          "     cod_ana = '" & cod_ana & "' AND " & _
          "     cod_efr = " & cod_efr
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.LockType = adLockReadOnly
    
    rs_aux.Open sql, gConexao
        
    If (rs_aux.EOF) Then
        ret = True
    Else
        ' Se conta da tabela, n�o � facturada.
        ret = False
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    ANALISE_Para_Facturar = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Para_Facturar (OBJ_Analise) -> " & Err.Description)
            ANALISE_Para_Facturar = False
            Exit Function
    End Select
End Function


Public Function ANALISE_Ordem_Requisicao(codAna As String, _
                                         NReq As Long, _
                                         ars As Boolean) As String

    ' Devolve a ordem de introdu��o de uma an�lise para uma dada requisi��o.
    ' No caso de uma requisi��o ARS, devolve a ordem dentro da credencial.
    
'    On Error GoTo ErrorHandler
    
    Dim RsOrdem As ADODB.recordset
    Dim sql As String
    Dim RsOrdem2 As ADODB.recordset
    Dim ret As Integer
    
    ' Se a requisi��o for da ARS, procura a ordem dentro da credencial.
    
    ' ANALISE_Ordem = 0
    
    sql = "SELECT " & _
          "     MIN(ord_marca) as ordem " & _
          "FROM " & _
          "     sl_realiza " & _
          "WHERE " & _
          "     n_req = " & NReq & " AND " & _
          "     cod_agrup = " & BL_TrataStringParaBD(codAna)
    
    Set RsOrdem = New ADODB.recordset
    RsOrdem.CursorLocation = adUseServer
    RsOrdem.CursorType = adOpenForwardOnly
    RsOrdem.LockType = adLockReadOnly
    
    RsOrdem.Open sql, gConexao
    
    If (RsOrdem.RecordCount > 0) Then
    
        ret = BL_HandleNull(RsOrdem!ordem, 0)
        If (ret <= 0) Then
        
            sql = "SELECT " & _
                  "     MIN(ord_marca) AS ordem " & _
                  "FROM " & _
                  "     sl_marcacoes " & _
                  "WHERE " & _
                  "     n_req = " & NReq & " AND " & _
                  "     cod_agrup = " & BL_TrataStringParaBD(codAna)
            
            Set RsOrdem2 = New ADODB.recordset
            RsOrdem2.CursorLocation = adUseServer
            RsOrdem2.CursorType = adOpenStatic
            RsOrdem2.Open sql, gConexao
            If (RsOrdem2.RecordCount > 0) Then
                ret = BL_HandleNull(RsOrdem2!ordem, 0)
            End If
            RsOrdem2.Close
            Set RsOrdem2 = Nothing
        End If
    Else
        sql = "SELECT " & _
              "     MIN(ord_marca) AS ordem " & _
              "FROM " & _
              "     sl_marcacoes " & _
              "WHERE " & _
              "     n_req = " & NReq & " AND " & _
              "     cod_agrup = " & BL_TrataStringParaBD(codAna)
        
        Set RsOrdem2 = New ADODB.recordset
        RsOrdem2.CursorLocation = adUseServer
        RsOrdem2.CursorType = adOpenStatic
        RsOrdem2.Open sql, gConexao
        If (RsOrdem2.RecordCount > 0) Then
            ret = BL_HandleNull(RsOrdem!ordem, 0)
        End If
        RsOrdem2.Close
        Set RsOrdem2 = Nothing
    End If
    RsOrdem.Close
    Set RsOrdem = Nothing
    
    ANALISE_Ordem_Requisicao = Right("00" & Trim(CStr(ret)), 2)

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Ordem_Requisicao (OBJ_Analise) -> " & Err.Description)
            ANALISE_Ordem_Requisicao = "-1"
            Exit Function
    End Select
End Function

Public Function ANALISE_Descricao(cod_ana As String) As String
    
    ' Devolve a descri��o de uma an�lise.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As String
    
    ret = ""
    
    cod_ana = UCase(Trim(cod_ana))
    
    If (Len(cod_ana) = 0) Then
        ANALISE_Descricao = ""
        Exit Function
    End If
        
    Select Case left(cod_ana, 1)
        Case "S"
            sql = "SELECT descr_ana_s " & _
                  "FROM   sl_ana_s " & _
                  "WHERE  cod_ana_s = '" & cod_ana & "'"
        Case "C"
            sql = "SELECT descr_ana_c " & _
                  "FROM   sl_ana_c " & _
                  "WHERE  cod_ana_c = '" & cod_ana & "'"
        Case "P"
            sql = "SELECT descr_perfis " & _
                  "FROM   sl_perfis " & _
                  "WHERE  cod_perfis = '" & cod_ana & "'"
        Case Else
    End Select
    
    Set rs_aux = New ADODB.recordset
    
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.LockType = adLockReadOnly
    
    rs_aux.Open sql, gConexao
        
    If (Not rs_aux.EOF) Then
        If IsNull(rs_aux(0)) Then
            ret = ""
        Else
            ret = Trim(rs_aux(0))
        End If
    Else
        ret = ""
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    ANALISE_Descricao = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Descricao (OBJ_Analise) -> " & Err.Description)
            ANALISE_Descricao = ""
            Exit Function
    End Select
End Function

Public Function ANALISE_Preco_FACTUS_Entidade(cod_rubr As String, _
                                              cod_efr As String) As String
    
    ' Devolve o pre�o de uma an�lise no FACTUS.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As String
    
    ' Agrupa as ARS.
    If (EFR_Verifica_ARS(cod_efr) = True) Then
        cod_efr = gGrupoARS
    Else
        If (EFR_Verifica_ADSE(cod_efr) = True) Then
            ' Agrupa as ADSE.
            cod_efr = gGrupoADSE
        Else
            ' Mant�m.
        End If
    End If
    
    ret = "?"
    
    cod_rubr = UCase(Trim(cod_rubr))
    cod_efr = UCase(Trim(cod_efr))
    
    If ((Len(cod_rubr) = 0) Or (Len(cod_efr) = 0)) Then
        ANALISE_Preco_FACTUS_Entidade = "?"
        Exit Function
    End If
    
    ' Erro da view do FACTUS no LABJM.
    ' val_pag_ent_esc ->
    
    sql = "SELECT " & _
          "     val_pag_ent_esc " & _
          "FROM " & _
          "     sl_pr_rubr " & _
          "WHERE " & _
          "     cod_rubr = " & cod_rubr & " AND " & _
          "     cod_efr = " & cod_efr & " "
          
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.LockType = adLockReadOnly
    
    rs_aux.Open sql, gConexao
        
    If (Not rs_aux.EOF) Then
        If IsNull(rs_aux(0)) Then
            ret = "?"
        Else
            ret = Trim(rs_aux(0))
        End If
    Else
        ret = "?"
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    If Not (IsNumeric(ret)) Then
        Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\Factura��o\Sem_Pre�o.Log", "EFR : " & Right("     " & cod_efr, 5) & "  R�brica : " & Right("     " & cod_rubr, 5))
    End If
    
    ANALISE_Preco_FACTUS_Entidade = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Preco_FACTUS_Entidade (OBJ_Analise) -> " & Err.Description)
            ANALISE_Preco_FACTUS_Entidade = "Erro"
            Exit Function
    End Select
End Function

Public Function ANALISE_Preco_FACTUS_Utente(cod_rubr As String, _
                                            cod_efr As String) As String
    
    ' Devolve o pre�o de uma an�lise no FACTUS.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As String
    
    ' Agrupa as ARS.
    If (EFR_Verifica_ARS(cod_efr) = True) Then
        cod_efr = gGrupoARS
    Else
        If (EFR_Verifica_ADSE(cod_efr) = True) Then
            ' Agrupa as ADSE.
            cod_efr = gGrupoADSE
        Else
            ' Mant�m.
        End If
    End If
    
    ret = "?"
    
    cod_rubr = UCase(Trim(cod_rubr))
    cod_efr = UCase(Trim(cod_efr))
    
    If ((Len(cod_rubr) = 0) Or (Len(cod_efr) = 0)) Then
        ANALISE_Preco_FACTUS_Utente = "?"
        Exit Function
    End If
        
    If (cod_efr = gGrupoARS Or cod_efr = gGrupoADSE) Then
        sql = "SELECT " & _
            "     val_taxa_esc " & _
            "FROM " & _
            "     sl_pr_rubr " & _
            "WHERE " & _
            "     cod_rubr = " & cod_rubr & " AND " & _
            "     cod_efr = " & cod_efr & " "
    Else
        sql = "SELECT " & _
              "     val_pag_doe_esc " & _
            "FROM " & _
            "     sl_pr_rubr " & _
            "WHERE " & _
            "     cod_rubr = " & cod_rubr & " AND " & _
            "     cod_efr = " & cod_efr & " "
    End If
          
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.LockType = adLockReadOnly
    
    rs_aux.Open sql, gConexao
        
    If (Not rs_aux.EOF) Then
        If IsNull(rs_aux(0)) Then
            ret = "?"
        Else
            ret = Trim(rs_aux(0))
        End If
    Else
        ret = "?"
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    If Not (IsNumeric(ret)) Then
        Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\Factura��o\Sem_Pre�o.Log", "EFR : " & Right("     " & cod_efr, 5) & "  R�brica : " & Right("     " & cod_rubr, 5))
    End If
    
    ANALISE_Preco_FACTUS_Utente = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Preco_FACTUS_Utente (OBJ_Analise) -> " & Err.Description)
            ANALISE_Preco_FACTUS_Utente = "Erro"
            Exit Function
    End Select
End Function


Public Function ANALISE_Descricao_FACTUS(seq_rubr As String) As String
    
    ' Devolve a descri��o de uma an�lise no FACTUS.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As String
    
    ret = "?"
    
    seq_rubr = UCase(Trim(seq_rubr))
    
    If (Len(seq_rubr) = 0) Then
        ANALISE_Descricao_FACTUS = "?"
        Exit Function
    End If
        
    sql = "SELECT " & _
          "     descr_rubr " & _
          "FROM " & _
          "     sl_rubr " & _
          "WHERE " & _
          "     seq_rubr = " & seq_rubr
          
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.LockType = adLockReadOnly
    
    rs_aux.Open sql, gConexao
        
    If (Not rs_aux.EOF) Then
        If IsNull(rs_aux(0)) Then
            ret = "?"
        Else
            ret = Trim(rs_aux(0))
        End If
    Else
        ret = "?"
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    ANALISE_Descricao_FACTUS = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Descricao_FACTUS (OBJ_Analise) -> " & Err.Description)
            ANALISE_Descricao_FACTUS = "Erro"
            Exit Function
    End Select
End Function

Public Function ANALISE_Descr_Classe(cod_ana As String) As String
    
    ' Devolve a descri��o de uma an�lise.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As String
    
    ret = ""
    
    cod_ana = UCase(Trim(cod_ana))
    
    If (Len(cod_ana) = 0) Then
        ANALISE_Descr_Classe = ""
        Exit Function
    End If
    
    If (gSGBD = gOracle) Then
        
        Select Case left(cod_ana, 1)
            Case "S"
                sql = "SELECT " & vbCrLf & _
                      "     CA.descr_classe_ana " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_ana_s      AN, " & vbCrLf & _
                      "     sl_classe_ana CA " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     AN.classe_ana = cod_classe_ana (+) AND " & vbCrLf & _
                      "     AN.cod_ana_s = '" & cod_ana & "' "
            Case "C"
                sql = "SELECT " & vbCrLf & _
                      "     CA.descr_classe_ana " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_ana_c      AN, " & vbCrLf & _
                      "     sl_classe_ana CA " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     AN.classe_ana = cod_classe_ana (+) AND " & vbCrLf & _
                      "     AN.cod_ana_c = '" & cod_ana & "' "
            Case "P"
                sql = "SELECT " & vbCrLf & _
                      "     CA.descr_classe_ana " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_perfis      AN, " & vbCrLf & _
                      "     sl_classe_ana CA " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     AN.classe_ana = cod_classe_ana (+) AND " & vbCrLf & _
                      "     AN.cod_perfis = '" & cod_ana & "' "
            Case Else
            
        End Select
    
    End If
    
    If (gSGBD = gSqlServer) Then
        
        Select Case left(cod_ana, 1)
            Case "S"
                sql = "SELECT " & vbCrLf & _
                      "     CA.descr_classe_ana " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_ana_s      AN, " & vbCrLf & _
                      "     sl_classe_ana CA " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     AN.classe_ana *= cod_classe_ana AND " & vbCrLf & _
                      "     AN.cod_ana_s = '" & cod_ana & "' "
            Case "C"
                sql = "SELECT " & vbCrLf & _
                      "     CA.descr_classe_ana " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_ana_c      AN, " & vbCrLf & _
                      "     sl_classe_ana CA " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     AN.classe_ana *= cod_classe_ana AND " & vbCrLf & _
                      "     AN.cod_ana_c = '" & cod_ana & "' "
            Case "P"
                sql = "SELECT " & vbCrLf & _
                      "     CA.descr_classe_ana " & vbCrLf & _
                      "FROM " & vbCrLf & _
                      "     sl_perfis      AN, " & vbCrLf & _
                      "     sl_classe_ana CA " & vbCrLf & _
                      "WHERE " & vbCrLf & _
                      "     AN.classe_ana *= cod_classe_ana AND " & vbCrLf & _
                      "     AN.cod_perfis = '" & cod_ana & "' "
            Case Else
            
        End Select
    
    End If
    
    Set rs_aux = New ADODB.recordset
    
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.LockType = adLockReadOnly
    
    rs_aux.Open sql, gConexao
        
    If (Not rs_aux.EOF) Then
        If IsNull(rs_aux(0)) Then
            ret = ""
        Else
            ret = Trim(rs_aux(0))
        End If
    Else
        ret = ""
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    ANALISE_Descr_Classe = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Descr_Classe (OBJ_Analise) -> " & Err.Description)
            ANALISE_Descr_Classe = ""
            Exit Function
    End Select
End Function



Public Sub ANALISE_CarregaVector_gMicro(ByRef Vector As Variant, chave As String)

    ' Carrega as entidades do tipo gMicro num Array.
    
    On Error GoTo ErrorHandler
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    
    sql = "SELECT " & _
          "     conteudo " & _
          "FROM " & _
                cParamAmbiente & " " & _
          "WHERE " & _
          "     chave = '" & chave & "'"
    
    Set Tabela = New ADODB.recordset
    Tabela.CursorLocation = adUseServer
    Tabela.CursorType = adOpenForwardOnly
    Tabela.LockType = adLockReadOnly
    
    Tabela.Open sql, gConexao
    
    If Tabela.RecordCount > 0 Then
        ReDim Vector(Tabela.RecordCount)
        i = 1
        While Not Tabela.EOF
            Vector(i) = Trim(Tabela!conteudo)
            i = i + 1
            Tabela.MoveNext
        Wend
    End If

    Set Tabela = Nothing

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_CarregaVector_gMicro (OBJ_Analise) -> " & Err.Description)
            Exit Sub
    End Select
End Sub

Public Function ANALISE_Verifica_MICRO(cod_ana As String) As Boolean
    
    On Error GoTo ErrorHandler
    
    Dim i
    Dim ret As Boolean
    
    ret = False
    
    ' Para testar se est� inicializado.
    If (UBound(gMicro) >= 0) Then
    End If
    
    If (Trim(cod_ana) <> "") Then
        For Each i In gMicro
            If i = cod_ana Then ret = True
        Next
    End If

    ANALISE_Verifica_MICRO = ret
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case 9
            ' Subscript out of range.
            ANALISE_Verifica_MICRO = False
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Verifica_MICRO (OBJ_ANALISE) -> " & Err.Description)
            ANALISE_Verifica_MICRO = False
            Exit Function
    End Select
End Function



Public Function ANALISE_Verifica_AnaNaoFacturaGESREQ(cod_ana As String) As Boolean
    
    On Error GoTo ErrorHandler
    
    Dim i
    Dim ret As Boolean
    
    ret = False
    
    ' Para testar se est� inicializado.
    If (UBound(gAnaNaoFacturaGESREQ) >= 0) Then
    End If
    
    If (Trim(cod_ana) <> "") Then
        For Each i In gAnaNaoFacturaGESREQ
            If i = cod_ana Then ret = True
        Next
    End If

    ANALISE_Verifica_AnaNaoFacturaGESREQ = ret
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case 9
            ' Subscript out of range.
            ANALISE_Verifica_AnaNaoFacturaGESREQ = False
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ANALISE_Verifica_AnaNaoFacturaGESREQ (OBJ_ANALISE) -> " & Err.Description)
            ANALISE_Verifica_AnaNaoFacturaGESREQ = False
            Exit Function
    End Select
End Function


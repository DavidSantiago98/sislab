Attribute VB_Name = "Gesdoc"
' CPCta@2002

Private Declare Function InternetOpen Lib "wininet.dll" Alias "InternetOpenA" (ByVal lpszCallerName As String, ByVal dwAccessType As Long, ByVal lpszProxyName As String, ByVal lpszProxyBypass As String, ByVal dwFlags As Long) As Long
Private Declare Function InternetConnect Lib "wininet.dll" Alias "InternetConnectA" (ByVal hInternetSession As Long, ByVal lpszServerName As String, ByVal nProxyPort As Integer, ByVal lpszUsername As String, ByVal lpszPassword As String, ByVal dwService As Long, ByVal dwFlags As Long, ByVal dwContext As Long) As Long
Private Declare Function InternetReadFile Lib "wininet.dll" (ByVal hFile As Long, ByVal sBuffer As String, ByVal lNumBytesToRead As Long, lNumberOfBytesRead As Long) As Integer
Private Declare Function HttpOpenRequest Lib "wininet.dll" Alias "HttpOpenRequestA" (ByVal hInternetSession As Long, ByVal lpszVerb As String, ByVal lpszObjectName As String, ByVal lpszVersion As String, ByVal lpszReferer As String, ByVal lpszAcceptTypes As Long, ByVal dwFlags As Long, ByVal dwContext As Long) As Long
Private Declare Function HttpSendRequest Lib "wininet.dll" Alias "HttpSendRequestA" (ByVal hHttpRequest As Long, ByVal sHeaders As String, ByVal lHeadersLength As Long, ByVal sOptional As String, ByVal lOptionalLength As Long) As Boolean
Private Declare Function InternetCloseHandle Lib "wininet.dll" (ByVal hInternetHandle As Long) As Boolean
Private Declare Function HttpAddRequestHeaders Lib "wininet.dll" Alias "HttpAddRequestHeadersA" (ByVal hHttpRequest As Long, ByVal sHeaders As String, ByVal lHeadersLength As Long, ByVal lModifiers As Long) As Integer

'Const PutServlet = "http://joaopires/gesdoc-igm/servlet/PutServletVB"
Const Port = "80"

'Input  DocPath - Path do ficheiro a inserir
'       DocType - Id do tipo do documento
'       DocCode - C�digo do doccumento
'       DocName - Nome do documento
'       NReq    - Requisi��o
'Output Id      - Id do documento criado   - O em caso de Erro

Public Function ID(DocPath As String, DocCode As String, DocName As String, _
                    NReq As String, NAnalise As String, NUtente As String, NomeUtente As String, _
                    DtEntrada As String, DtSaida As String, RespRel As String, SeqRel As Integer, NReqInt As Long) As Integer

    On Error Resume Next:
    
    Dim Port As Integer
    Dim srv$, script$
    Dim Data$, ret$
    Dim sID As Long
    
    SplitAddr gPutServlet, srv$, script$
    
    'verifica se o relat�rio j� existe na GesDoc
    sID = BL_DaIdDoc(DocCode)
    
    'doc j� existe na GesDoc
    If sID > 0 Then
        script$ = script$ & "?p_id_doc=" & sID & "&p_checkout=true"
    End If
    
    '"--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""p_checkin""" & vbCrLf & "" & vbCrLf & "true" & vbCrLf &
    Data$ = _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""p_nom_doc""" & vbCrLf & "" & vbCrLf & DocName & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""p_cod_doc""" & vbCrLf & "" & vbCrLf & DocCode & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""p_tip_doc""" & vbCrLf & "" & vbCrLf & gDocType & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""p_folder""" & vbCrLf & "" & vbCrLf & gDefFolder & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""N_REQ""" & vbCrLf & "" & vbCrLf & NReq & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""N_ANALISE""" & vbCrLf & "" & vbCrLf & NAnalise & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""N_UTENTE""" & vbCrLf & "" & vbCrLf & NUtente & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""NOME_UTENTE""" & vbCrLf & "" & vbCrLf & NomeUtente & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""DT_ENTRADA""" & vbCrLf & "" & vbCrLf & Format(DtEntrada, "yyyy") & "/" & Format(DtEntrada, "mm") & "/" & Format(DtEntrada, "dd") & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""DT_SAIDA""" & vbCrLf & "" & vbCrLf & Format(DtSaida, "yyyy") & "/" & Format(DtSaida, "mm") & "/" & Format(DtSaida, "dd") & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""RESP_REL""" & vbCrLf & "" & vbCrLf & RespRel & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""SEQ_REL""" & vbCrLf & "" & vbCrLf & SeqRel & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""N_REQ_INT""" & vbCrLf & "" & vbCrLf & NReqInt & vbCrLf & _
        "--CPC_BOUNDARY" & vbCrLf & "Content-Disposition: form-data; name=""file""; filename=""" & Dir(DocPath) & """" & vbCrLf & "Content-Type: application/octet-stream" & vbCrLf & "" & vbCrLf
 
    Open DocPath For Binary Access Read As #1
    If LOF(1) > 0 Then
        fdata$ = String(LOF(1), " ")
        Get #1, , fdata$
        Close #1
    End If
     
    Data$ = Data$ & fdata$ & vbCr

    ret$ = PostInfo(srv$, script, CInt(PortPutS), Data$)
    
    If InStr(1, ret$, "id=", vbTextCompare) > 0 Then
        ID = Replace(Replace(Mid(ret$, InStr(1, ret$, "id=", vbTextCompare) + 3), Chr(13), ""), Chr(10), "")
    Else
        MsgBox (ret$)
        ID = 0
    End If

End Function

Public Function PostInfo$(srv$, script$, Port As Integer, postdat$)

    Dim hInternetOpen As Long
    Dim hInternetConnect As Long
    Dim hHttpOpenRequest As Long
    Dim bRet As Boolean
    
    hInternetOpen = 0
    hInternetConnect = 0
    hHttpOpenRequest = 0
    
    'Use registry access settings.
    Const INTERNET_OPEN_TYPE_PRECONFIG = 0
    hInternetOpen = InternetOpen("http generic", INTERNET_OPEN_TYPE_PRECONFIG, vbNullString, vbNullString, 0)
    
    If hInternetOpen <> 0 Then
       'Type of service to access.
       Const INTERNET_SERVICE_HTTP = 3
       Const INTERNET_DEFAULT_HTTP_PORT = 9000
       
       'Change the server to your server name
       'hInternetConnect = InternetConnect(hInternetOpen, srv$, INTERNET_DEFAULT_HTTP_PORT, vbNullString, "HTTP/1.0", INTERNET_SERVICE_HTTP, 0, 0)
       hInternetConnect = InternetConnect(hInternetOpen, srv$, Port, vbNullString, "HTTP/1.0", INTERNET_SERVICE_HTTP, 0, 0)
    
       If hInternetConnect <> 0 Then
        'Brings the data across the wire even if it locally cached.
         Const INTERNET_FLAG_RELOAD = &H80000000
         hHttpOpenRequest = HttpOpenRequest(hInternetConnect, "POST", script$, "HTTP/1.0", vbNullString, 0, INTERNET_FLAG_RELOAD, 0)
    
          If hHttpOpenRequest <> 0 Then
              Dim sHeader As String
              Dim sHeader1 As String
              Dim sHeader2 As String
              
              Const HTTP_ADDREQ_FLAG_ADD = &H20000000
              Const HTTP_ADDREQ_FLAG_REPLACE = &H80000000
              
             sHeader = "Content-Type: multipart/form-data" & "    boundary=CPC_BOUNDARY" & vbCrLf
             bRet = HttpAddRequestHeaders(hHttpOpenRequest, sHeader, Len(sHeader), HTTP_ADDREQ_FLAG_REPLACE Or HTTP_ADDREQ_FLAG_ADD)

             Dim lpszPostData As String
             Dim lPostDataLen As Long
             
             lpszPostData = postdat$
             lPostDataLen = Len(lpszPostData)
                    
             bRet = HttpSendRequest(hHttpOpenRequest, _
                    vbNullString, _
                    0, _
                    lpszPostData, _
                    lPostDataLen)
                    
                    
             Dim bDoLoop             As Boolean
             Dim sReadBuffer         As String * 2048
             Dim lNumberOfBytesRead  As Long
             Dim sBuffer             As String
             bDoLoop = True
             While bDoLoop
              sReadBuffer = vbNullString
              bDoLoop = InternetReadFile(hHttpOpenRequest, _
                 sReadBuffer, Len(sReadBuffer), lNumberOfBytesRead)
              sBuffer = sBuffer & _
                   Left(sReadBuffer, lNumberOfBytesRead)
              If Not CBool(lNumberOfBytesRead) Then bDoLoop = False
             Wend
             PostInfo = sBuffer
             bRet = InternetCloseHandle(hHttpOpenRequest)
          End If
          bRet = InternetCloseHandle(hInternetConnect)
       End If
       bRet = InternetCloseHandle(hInternetOpen)
    End If
End Function

Public Sub SplitAddr(ByVal addr$, srv$, script$)
'Inputs: The full url including http://       Two variables that will be changed
'Returns: Splits the addr$ var into the server name and the script path

    Dim i%

    i = InStr(addr$, "/")
    srv$ = Mid(addr$, i + 2, Len(addr$) - (i + 1))
    i = InStr(srv$, "/")
    script$ = Mid(srv$, i, Len(srv$) + 1 - i)
    srv$ = Left$(srv$, i - 1)

End Sub




Attribute VB_Name = "OBJ_Exame"
Option Explicit

Public Function EXAME_Repetido(n_req As String, _
                               Perfil As String, _
                               ana_c As String, _
                               ana_s As String) As Boolean
    
    ' Determina se um exame de uma determinada requisição foi repetido.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As Boolean
    
    ret = False
    
    n_req = UCase(Trim(n_req))
    Perfil = UCase(Trim(Perfil))
    ana_c = UCase(Trim(ana_c))
    ana_s = UCase(Trim(ana_s))
    
    If (Len(n_req) = 0) Then
        EXAME_Repetido = False
        Exit Function
    End If
        
    sql = "SELECT " & _
          "     COUNT(*) AS conta " & _
          "FROM " & _
          "     sl_repeticoes " & _
          "WHERE " & _
          "     n_req      = " & BL_TrataStringParaBD(n_req) & " AND " & _
          "     cod_perfil = " & BL_TrataStringParaBD(Perfil) & " AND " & _
          "     cod_ana_c  = " & BL_TrataStringParaBD(ana_c) & " AND " & _
          "     cod_ana_s = " & BL_TrataStringParaBD(ana_s)
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.LockType = adLockReadOnly
    
    rs_aux.Open sql, gConexao
        
    If (rs_aux(0) > 0) Then
        ret = True
    Else
        ret = False
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    EXAME_Repetido = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EXAME_Repetido (OBJ_Exame) -> " & Err.Description)
            EXAME_Repetido = False
            Exit Function
    End Select
End Function





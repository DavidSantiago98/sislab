Attribute VB_Name = "OBJ_Utente"
Option Explicit

' Actualiza��o : 15/02/2002
' T�cnico Paulo Costa

'************************************************************************
'*
'*  Objecto UTENTE
'*
'************************************************************************

Public Function UTENTE_Tipo(SeqUtente As Double) As String
    
    ' Devolve o tipo de utente.
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim RsUtente As ADODB.recordset
    Dim ret As String
    
    ret = ""
    
    sql = "SELECT " & _
          "     t_utente " & _
          "FROM " & _
          "     sl_identif " & _
          "WHERE " & _
          "     seq_utente = " & SeqUtente
    
    Set RsUtente = New ADODB.recordset
    RsUtente.CursorLocation = adUseServer
    RsUtente.CursorType = adOpenForwardOnly
    RsUtente.LockType = adLockReadOnly
    
    RsUtente.Open sql, gConexao
    
    If RsUtente.RecordCount > 0 Then
        ret = RsUtente!t_utente
    Else
        ret = ""
    End If
    RsUtente.Close
    Set RsUtente = Nothing
    
    UTENTE_Tipo = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("Erro Inesperado : UTENTE_Tipo (OBJ_Utente) -> " & Err.Description)

            Set RsUtente = Nothing
            UTENTE_Tipo = ""
            Exit Function
    End Select
End Function

Public Function Utente_Existe(processo1 As String, _
                              situacao As String, _
                              episodio As String, _
                              ByRef seq_utente As String) As Boolean

    ' Verifica pelo processo1 ou situa��o/epis�dio se um utente existe no SISLAB.
    
    On Error GoTo ErrorHandler
    
    Dim Existe As Boolean
    Dim sql As String
    Dim rs As ADODB.recordset
    
    Existe = False
    seq_utente = ""
    
    processo1 = Trim(processo1)
    situacao = Trim(situacao)
    episodio = Trim(episodio)
    seq_utente = Trim(seq_utente)
        
    Set rs = New ADODB.recordset
    
    If (Len(processo1) > 0) Then
        ' Verifica pelo Processo1.
        
        sql = "SELECT " & vbCrLf & _
              "     seq_utente, " & vbCrLf & _
              "     n_proc_1 " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_identif " & vbCrLf & _
              "WHERE " & vbCrLf & _
              "     n_proc_1 = " & processo1
        
        With rs
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenForwardOnly
            .LockType = adLockReadOnly
            .ActiveConnection = gConexao
            .Open
        End With
        
        sql = ""
        
        If (rs.RecordCount = 0) Then
            ' N�o consta.
            ' Verifica pela situa��o e epis�dio.
        Else
            If (rs.RecordCount = 1) Then
                ' Uma refer�ncia.
                seq_utente = BL_HandleNull(rs!seq_utente, "")
                Existe = True
            Else
                ' V�rias refer�ncias.
                ' Verifica pela situa��o e epis�dio.
            End If
        End If
        
    End If
    
    If ((Len(situacao) > 0) And _
        (Len(episodio) > 0) And _
         Existe = False) Then
         
        ' Verifica pela situa��o e epis�dio.
    
        sql = "SELECT DISTINCT " & vbCrLf & _
              "     seq_utente    " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_requis " & vbCrLf & _
              "WHERE " & vbCrLf & _
              "     t_sit = " & situacao & " AND " & vbCrLf & _
              "     n_epis = " & episodio & " "
    
        Set rs = Nothing
        Set rs = New ADODB.recordset
        
        With rs
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenForwardOnly
            .LockType = adLockReadOnly
            .ActiveConnection = gConexao
            .Open
        End With
        
        sql = ""
        
        If (rs.EOF) Then
            ' N�o consta.
             Utente_Existe = False
        Else
            ' Existe
            seq_utente = BL_HandleNull(rs!seq_utente, "")
            Existe = True
        End If
    
    End If
        
    rs.Close
    Set rs = Nothing
    
    Utente_Existe = Existe

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Utente_Existe (OBJ_Utente) -> " & Err.Number & " : " & Err.Description & vbCrLf & _
                                   sql)
            seq_utente = ""
            Utente_Existe = True
            Exit Function
    End Select

End Function

Public Function UTENTE_Inserir(ByVal UT_utente As String, _
                               ByVal UT_t_utente As String, _
                               ByVal UT_n_proc_1 As String, _
                               ByVal UT_n_proc_2 As String, _
                               ByVal UT_n_cartao As String, _
                               ByVal UT_dt_inscr As String, _
                               ByVal UT_nome As String, _
                               ByVal UT_dt_nasc As String, _
                               ByVal UT_sexo As String, _
                               ByVal UT_est_civ As String, _
                               ByVal UT_descr_morada As String, _
                               ByVal UT_cod_postal As String, _
                               ByVal UT_telef As String, _
                               ByVal UT_cod_profissao As String, _
                               ByVal UT_cod_efr As String, _
                               ByVal UT_n_benef As String, _
                               ByVal UT_cod_isencao As String, _
                               ByVal UT_obs As String, _
                               ByVal UT_user_cri As String, _
                               ByRef seq_utente As String) As Integer
    
    On Error GoTo ErrorHandler
    
    Dim Flg_abortar As Boolean
    Dim UT_dt_cri As String
    Dim i As Long
    Dim seq As Long
    Dim RsNUtente As New ADODB.recordset
    Dim sql As String
    
    ' -------
    Dim iRes As Integer
    Dim msgRespAux As String
    Dim processo As Boolean
    ' -------

    seq_utente = ""
    
    Call BG_BeginTransaction
    
    Flg_abortar = False
    
    UT_utente = Trim(UT_utente)
    UT_dt_inscr = Trim(UT_dt_inscr)
    UT_dt_nasc = Trim(UT_dt_nasc)
    UT_t_utente = UCase(Trim(UT_t_utente))
    UT_n_proc_1 = Trim(UT_n_proc_1)
    UT_n_proc_2 = Trim(UT_n_proc_2)
    
    UT_dt_cri = Bg_DaData_ADO
    
    ' Se a data de inscri��o estiver vazia.
    If (Len(UT_dt_inscr) = 0) Then
        UT_dt_inscr = Format(Bg_DaData_ADO, gFormatoData)
    Else
        UT_dt_inscr = Format(UT_dt_inscr, gFormatoData)
    End If
   
    ' Se a data de nbascimento estiver vazia, assume 01/01/1900.
    If (Len(UT_dt_nasc) = 0) Then
        UT_dt_nasc = Format("01-01-1850", gFormatoData)
    Else
        UT_dt_nasc = Format(UT_dt_nasc, gFormatoData)
    End If
   
   ' Caso o tipo de utente nao esteja preenchido.
    If (Len(UT_t_utente) = 0) Then
        UT_t_utente = ""
    End If
    
    ' Validar processo 1.
    processo = UTENTE_VerificaProcesso("1", _
                                       "", _
                                       UT_n_proc_1, _
                                       UT_n_proc_2)
    
    If (processo = True) Then
        
        gMsgTitulo = "Validar Processo"
        gMsgMsg = "J� existe um processo N� " & UT_n_proc_1
        
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton2 + vbExclamation, gMsgTitulo)
        If (gMsgResp = vbCancel) Then
            Flg_abortar = True
        End If
    
    End If
        
    If (Flg_abortar = False) Then
        
        ' Valida processo 2.
        processo = UTENTE_VerificaProcesso("2", _
                                           "", _
                                           UT_n_proc_1, _
                                           UT_n_proc_2)
        
        If (processo = True) Then
            gMsgTitulo = "Validar Processo"
            gMsgMsg = "J� existe um processo N� " & UT_n_proc_2
            
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton2 + vbExclamation, gMsgTitulo)
            If gMsgResp = vbCancel Then
                Flg_abortar = True
            End If
        End If
    End If
        
    ' O n�mero do utente e o tipo do utente foram preenchidos e os processos validados.
    
    If (Flg_abortar = False) Then
                    
        i = 1
        
        ' --------------------------------------------------------
        ' FormIdentificaUtente.BD_Insert
        ' --------------------------------------------------------
        
        If Trim(UT_utente) = "" Then
            
            ' Por defeito se n�o tem c�digo de Utente fica tipo de Utente "LAB".
            
            UT_t_utente = "LAB"
            
            'Determinar o novo numero para o Tipo Utente (TENTA 10 VEZES - LOCK!)
            i = 0
            seq = -1
            While (seq = -1 And i <= 10)
                seq = BL_GeraUtente(UT_t_utente)
                i = i + 1
            Wend
        
            If (seq = -1) Then
                BG_Mensagem mediMsgBox, "Erro ao importar Utente !", vbCritical + vbOKOnly, "ERRO"
                UTENTE_Inserir = -2
                Exit Function
            End If
            
            UT_utente = UCase(Trim(CStr(seq)))
            
        Else
            
            Set RsNUtente = New ADODB.recordset
            RsNUtente.CursorLocation = adUseServer
            
            RsNUtente.Open "SELECT " & vbCrLf & _
                           "    nome_ute, " & vbCrLf & _
                           "    dt_nasc_ute " & vbCrLf & _
                           "FROM " & vbCrLf & _
                           "    sl_identif " & vbCrLf & _
                           "WHERE " & vbCrLf & _
                           "    t_utente = " & BL_TrataStringParaBD(UT_t_utente) & " AND " & vbCrLf & _
                           "    utente = " & BL_TrataStringParaBD(UT_utente), gConexao, adOpenStatic, adLockReadOnly
            
            If (RsNUtente.RecordCount > 0) Then
                BG_Mensagem mediMsgBox, "J� existe um utente do tipo " & UT_t_utente & " com o n�mero " & UT_utente & "!" & Chr(13) & "Nome : " & BL_HandleNull(RsNUtente!nome_ute) & Chr(13) & "Dt. Nasc : " & BL_HandleNull(RsNUtente!dt_nasc_ute), vbOKOnly + vbExclamation, "Inserir Utente"
                RsNUtente.Close
                Set RsNUtente = Nothing
                Exit Function
            Else
                RsNUtente.Close
                Set RsNUtente = Nothing
            End If
        End If
            
        'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraNumero("SEQ_UTENTE")
            i = i + 1
        Wend
        
        ' Out
        seq_utente = seq
        
        If seq = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Function
        End If
        
        gSQLError = 0
        gSQLISAM = 0
        
        UT_utente = BL_TrataStringParaBD(Trim(UT_utente))
        UT_t_utente = BL_TrataStringParaBD(Trim(UT_t_utente))
        UT_n_proc_1 = BL_TrataStringParaBD(Trim(UT_n_proc_1))
        UT_n_proc_2 = BL_TrataStringParaBD(Trim(UT_n_proc_2))
        UT_n_cartao = BL_TrataStringParaBD(Trim(UT_n_cartao))
        UT_nome = BL_TrataStringParaBD(Trim(UT_nome))
        UT_sexo = BL_TrataStringParaBD(Trim(UT_sexo))
        UT_est_civ = BL_TrataStringParaBD(Trim(UT_est_civ))
        UT_descr_morada = BL_TrataStringParaBD(Trim(UT_descr_morada))
        UT_cod_postal = BL_TrataStringParaBD(Trim(UT_cod_postal))
        UT_telef = BL_TrataStringParaBD(Trim(UT_telef))
        UT_cod_profissao = BL_TrataStringParaBD(Trim(UT_cod_profissao))
        UT_cod_efr = BL_TrataStringParaBD(Trim(UT_cod_efr))
        UT_n_benef = BL_TrataStringParaBD(Trim(UT_n_benef))
        UT_cod_isencao = BL_TrataStringParaBD(Trim(UT_cod_isencao))
        UT_obs = BL_TrataStringParaBD(Trim(UT_obs))
        UT_user_cri = BL_TrataStringParaBD(Trim(UT_user_cri))
        
        UT_dt_inscr = Trim(UT_dt_inscr)
        If (Len(UT_dt_inscr) = 0) Then
            UT_dt_inscr = "null"
        Else
            UT_dt_inscr = "'" & UT_dt_inscr & "'"
        End If
        
        UT_dt_nasc = Trim(UT_dt_nasc)
        If (Len(UT_dt_nasc) = 0) Then
            UT_dt_nasc = "null"
        Else
            UT_dt_nasc = "'" & UT_dt_nasc & "'"
        End If
        
        If (gSGBD = gOracle) Then
        
        sql = "INSERT INTO sl_identif " & vbCrLf & _
              "( " & vbCrLf & _
              "    seq_utente, " & vbCrLf & _
              "    utente, " & vbCrLf & _
              "    t_utente, " & vbCrLf & _
              "    n_proc_1, n_proc_2, " & vbCrLf & _
              "    n_cartao_ute, dt_inscr, nome_ute, dt_nasc_ute, sexo_ute, " & vbCrLf & _
              "    est_civ_ute, descr_mor_ute, cod_postal_ute, telef_ute, cod_profis_ute, cod_efr_ute, n_benef_ute, cod_isencao_ute, obs,  " & vbCrLf & _
              "    user_cri, dt_cri, user_act, dt_act, cod_genero " & vbCrLf & _
              ") " & vbCrLf & _
              "VALUES " & vbCrLf & _
              "( " & vbCrLf & _
              "    " & seq & ", " & vbCrLf & _
              "    " & UT_utente & ", " & vbCrLf & _
              "    " & UT_t_utente & ", " & vbCrLf & _
              "    " & UT_n_proc_1 & ", " & UT_n_proc_2 & ", " & vbCrLf & _
              "    " & UT_n_cartao & ", " & UT_dt_inscr & ", " & UT_nome & ", " & UT_dt_nasc & ", " & UT_sexo & ", " & vbCrLf & _
              "    " & UT_est_civ & ", " & UT_descr_morada & ", " & UT_cod_postal & ", " & UT_telef & ", " & UT_cod_profissao & ", " & UT_cod_efr & ", " & UT_n_benef & ", " & UT_cod_isencao & ", " & UT_obs & ", " & vbCrLf & _
              "    " & UT_user_cri & ", SYSDATE, null, null, " & gCodGeneroDefeito & vbCrLf & _
              ") "

        End If
        BG_ExecutaQuery_ADO sql
        
        If (gSQLError <> 0) Then
            'Erro a inserir utente
        End If
        
        Call BG_CommitTransaction
        
        ' --------------------------------------------------------
    
    End If
        
    UTENTE_Inserir = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_RollbackTransaction
            Call BG_LogFile_Erros("Erro UTENTE_Inserir (OBJ_Utente) -> " & Err.Number & " : " & Err.Description)
            UTENTE_Inserir = -1
            Exit Function
    End Select
End Function
Public Function UTENTE_Actualizar(ByVal UT_utente As String, _
                               ByVal UT_t_utente As String, _
                               ByVal UT_n_proc_1 As String, _
                               ByVal UT_n_proc_2 As String, _
                               ByVal UT_n_cartao As String, _
                               ByVal UT_dt_inscr As String, _
                               ByVal UT_nome As String, _
                               ByVal UT_dt_nasc As String, _
                               ByVal UT_sexo As String, _
                               ByVal UT_est_civ As String, _
                               ByVal UT_descr_morada As String, _
                               ByVal UT_cod_postal As String, _
                               ByVal UT_telef As String, _
                               ByVal UT_cod_profissao As String, _
                               ByVal UT_cod_efr As String, _
                               ByVal UT_n_benef As String, _
                               ByVal UT_cod_isencao As String, _
                               ByVal UT_obs As String, _
                               ByVal UT_user_cri As String, _
                               ByVal seq_utente As Long) As Integer
    
    On Error GoTo ErrorHandler
    
    Dim Flg_abortar As Boolean
    Dim UT_dt_act As String
    Dim UT_hr_act As String
    Dim i As Long
    Dim seq As Long
    Dim RsNUtente As New ADODB.recordset
    Dim sql As String
    
    ' -------
    Dim iRes As Integer
    Dim msgRespAux As String
    Dim processo As Boolean
    ' -------
    
    Flg_abortar = False
    
    UT_utente = Trim(UT_utente)
    UT_dt_inscr = Trim(UT_dt_inscr)
    UT_dt_nasc = Trim(UT_dt_nasc)
    UT_t_utente = UCase(Trim(UT_t_utente))
    UT_n_proc_1 = Trim(UT_n_proc_1)
    UT_n_proc_2 = Trim(UT_n_proc_2)
    
    UT_dt_act = Bg_DaData_ADO
    UT_hr_act = Bg_DaDataHora_ADO
    
    gSQLError = 0
    gSQLISAM = 0
    
    UT_utente = BL_TrataStringParaBD(Trim(UT_utente))
    UT_t_utente = BL_TrataStringParaBD(Trim(UT_t_utente))
    UT_n_proc_1 = BL_TrataStringParaBD(Trim(UT_n_proc_1))
    UT_n_proc_2 = BL_TrataStringParaBD(Trim(UT_n_proc_2))
    UT_n_cartao = BL_TrataStringParaBD(Trim(UT_n_cartao))
    UT_nome = BL_TrataStringParaBD(Trim(UT_nome))
    UT_sexo = BL_TrataStringParaBD(Trim(UT_sexo))
    UT_est_civ = BL_TrataStringParaBD(Trim(UT_est_civ))
    UT_descr_morada = BL_TrataStringParaBD(Trim(UT_descr_morada))
    UT_cod_postal = BL_TrataStringParaBD(Trim(UT_cod_postal))
    UT_telef = BL_TrataStringParaBD(Trim(UT_telef))
    UT_cod_profissao = BL_TrataStringParaBD(Trim(UT_cod_profissao))
    UT_cod_efr = BL_TrataStringParaBD(Trim(UT_cod_efr))
    UT_n_benef = BL_TrataStringParaBD(Trim(UT_n_benef))
    UT_cod_isencao = BL_TrataStringParaBD(Trim(UT_cod_isencao))
    UT_obs = BL_TrataStringParaBD(Trim(UT_obs))
    UT_user_cri = BL_TrataStringParaBD(Trim(UT_user_cri))
        
        UT_dt_inscr = Trim(UT_dt_inscr)
        If (Len(UT_dt_inscr) = 0) Then
            UT_dt_inscr = "null"
        Else
            UT_dt_inscr = "'" & UT_dt_inscr & "'"
        End If
        
        UT_dt_nasc = Trim(UT_dt_nasc)
        If (Len(UT_dt_nasc) = 0) Then
            UT_dt_nasc = "null"
        Else
            UT_dt_nasc = "'" & UT_dt_nasc & "'"
        End If
        
        If (gSGBD = gOracle) Then
        
        sql = "UPDATE sl_identif " & vbCrLf & _
              " SET n_cartao_ute = " & UT_n_cartao & ", nome_ute = " & UT_nome & ", dt_nasc_ute = " & UT_dt_nasc & ", sexo_ute = " & UT_sexo & ", " & vbCrLf & _
              "    est_civ_ute = " & UT_est_civ & ", descr_mor_ute = " & UT_descr_morada & ", cod_postal_ute = " & UT_cod_postal & ", telef_ute = " & UT_telef & ", " & _
              " dt_act = " & BL_TrataDataParaBD(UT_dt_act) & _
              " WHERE seq_utente = " & seq_utente

        End If
        BG_ExecutaQuery_ADO sql
        
        If (gSQLError <> 0) Then
            'Erro a inserir utente
        End If
        
        
    UTENTE_Actualizar = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            Call BG_LogFile_Erros("Erro UTENTE_Inserir (OBJ_Utente) -> " & Err.Number & " : " & Err.Description)
            UTENTE_Actualizar = -1
            Exit Function
    End Select
End Function



Public Function UTENTE_VerificaProcesso(ordem As String, _
                                        CodSequencial As String, _
                                        ProcHosp1 As String, _
                                        ProcHosp2 As String) As Boolean

    On Error Resume Next
    
    Dim ret As Boolean
    Dim sql As String
    Dim Tabela As ADODB.recordset

    ret = False
    If ProcHosp1 <> "" Then
        If ordem = "1" Then
            If Trim(CodSequencial) <> "" Then
                sql = "SELECT COUNT(*) as conta " & _
                      "FROM   sl_identif " & _
                      "WHERE  n_proc_1 = '" & ProcHosp1 & "' AND " & _
                      "       seq_utente != " & CodSequencial
            Else
                sql = "SELECT COUNT(*) as conta " & _
                      "FROM   sl_identif " & _
                      "WHERE  n_proc_1 = '" & ProcHosp1 & "'"
            End If
        Else
            If Trim(CodSequencial) <> "" Then
                sql = "SELECT COUNT(*) as conta " & _
                      "FROM   sl_identif " & _
                      "WHERE  n_proc_2='" & ProcHosp2 & "' AND " & _
                      "       seq_utente != " & CodSequencial
            Else
                sql = "SELECT COUNT(*) as conta " & _
                      "FROM   sl_identif " & _
                      "WHERE  n_proc_2 = '" & ProcHosp2 & "'"
            End If
        End If
        Set Tabela = New ADODB.recordset
        
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
        
        If (Tabela.RecordCount > 0) Then
            
            If (Tabela!conta > 0) Then
                ret = True
            Else
                ret = False
            End If
            
        Else
            ret = False
        End If
    End If
    
    UTENTE_VerificaProcesso = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("UTENTE_VerificaProcesso (OBJ_Utente) -> " & Err.Number & " : " & Err.Description)
            UTENTE_VerificaProcesso = True
            Exit Function
    End Select
End Function


Public Function UTENTE_VerificaNrBenef(NumBenef As String, codEfr As String) As Boolean

    On Error Resume Next
    
    Dim ret As Boolean
    Dim sql As String
    Dim Tabela As New ADODB.recordset

    ret = False
    If NumBenef <> "" And codEfr <> "" Then
        sql = "SELECT COUNT(*) as conta " & _
              "FROM   sl_identif " & _
              "WHERE  cod_efr_ute = " & codEfr & " AND " & _
              "       n_benef_ute = " & BL_TrataStringParaBD(NumBenef)
                
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
        
        If (Tabela.RecordCount > 0) Then
            
            If (Tabela!conta > 0) Then
                ret = True
            Else
                ret = False
            End If
            
        Else
            ret = False
        End If
    End If
    
    UTENTE_VerificaNrBenef = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("UTENTE_VerificaProcesso (OBJ_Utente) -> " & Err.Number & " : " & Err.Description)
            UTENTE_VerificaNrBenef = True
            Exit Function
    End Select
End Function





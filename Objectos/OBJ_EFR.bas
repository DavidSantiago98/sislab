Attribute VB_Name = "OBJ_EFR"
Option Explicit

' Actualiza��o : 15/02/2002
' T�cnico Paulo Costa

'************************************************************************
'*
'*  Objecto EFR
'*
'************************************************************************

Public Function EFR_Descricao(cod_efr As String) As String

    ' Devolve a descri��o da EFR.
    
    On Error GoTo ErrorHandler

    Dim str_sql As String
    Dim rs As ADODB.recordset
    Dim ret As String

    cod_efr = Trim(cod_efr)
    If (Len(cod_efr) = 0) Then
        EFR_Descricao = ""
        Exit Function
    End If
    
    ret = ""
        
    str_sql = "SELECT " & vbCrLf & _
              "     descr_efr " & vbCrLf & _
              "FROM " & _
              "     sl_efr " & vbCrLf & _
              "WHERE " & _
              "     cod_efr = " & cod_efr & " "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open str_sql, gConexao, adOpenForwardOnly, adLockReadOnly
    
    If (rs.EOF) Then
        ret = ""
    Else
        If IsNull(rs(0)) Then
            ret = ""
        Else
            ret = Trim(rs(0))
        End If
    End If

    rs.Close
    Set rs = Nothing
            
    EFR_Descricao = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Descricao (OBJ_EFR) -> " & Err.Description)

            Set rs = Nothing
            EFR_Descricao = ""
            Exit Function
    End Select
End Function

Public Function EFR_Verifica_ARS(Entidade As String) As Boolean
    
    On Error GoTo ErrorHandler
    
    Dim i
    Dim ret As Boolean
    
    ret = False
    
    ' Para testar se est� inicializado.
    If (UBound(gARS) >= 0) Then
    End If
    
    If (Trim(Entidade) <> "") Then
        For Each i In gARS
            If i = Entidade Then ret = True
        Next
    End If

    EFR_Verifica_ARS = ret
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case 9
            ' Subscript out of range.
            EFR_Verifica_ARS = False
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Verifica_ARS (OBJ_EFR) -> " & Err.Description)
            EFR_Verifica_ARS = False
            Exit Function
    End Select
End Function

Public Function EFR_Verifica_ADSE(Entidade As String) As Boolean
    
    On Error GoTo ErrorHandler
    
    Dim i
    Dim ret As Boolean
    
    ret = False
        
    ' Para testar se est� inicializado.
    If (UBound(gADSE) >= 0) Then
    End If
    
    If (Trim(Entidade) <> "") Then
        For Each i In gADSE
            If i = Entidade Then ret = True
        Next
    End If

    EFR_Verifica_ADSE = ret
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case 9
            ' Subscript out of range.
            EFR_Verifica_ADSE = False
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Verifica_ADSE (OBJ_EFR) -> " & Err.Description)
            EFR_Verifica_ADSE = False
            Exit Function
    End Select
End Function

Public Sub EFR_CarregaVector_gARS(ByRef Vector As Variant, chave As String)

    ' Carrega as entidades do tipo gARS num Array.
    
    On Error GoTo ErrorHandler
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    
    'BRUNODSANTOS  LRV-457 - 16.05.2017
    If gVERSAO_PARAM_AMB = "V1" Then
    sql = "SELECT " & _
          "     conteudo " & _
          "FROM " & _
                cParamAmbiente & " " & _
          "WHERE " & _
          "     chave = '" & chave & "'"
    ElseIf gVERSAO_PARAM_AMB = "V2" Then
        sql = "SELECT conteudo FROM sl_paramet_locais WHERE cod_paramet = (select codigo FROM sl_paramet WHERE chave = '" & chave & "' )"
    End If
    '
    
    Set Tabela = New ADODB.recordset
    Tabela.CursorLocation = adUseClient
    Tabela.CursorType = adOpenForwardOnly
    Tabela.LockType = adLockReadOnly
    
    Tabela.Open sql, gConexao
    
    If Tabela.RecordCount > 0 Then
        ReDim Vector(Tabela.RecordCount)
        i = 1
        While Not Tabela.EOF
            Vector(i) = Trim(Tabela!conteudo)
            i = i + 1
            Tabela.MoveNext
        Wend
    End If

    Set Tabela = Nothing
    
    ' Determina a entidade onde se codifica os pre�os para todas as ARS.
    
    sql = "SELECT " & _
          "     conteudo " & _
          "FROM " & _
                cParamAmbiente & " " & _
          "WHERE " & _
          "     chave = 'ARS_PRECOS'"
    
    Set Tabela = New ADODB.recordset
    Tabela.CursorLocation = adUseClient
    Tabela.CursorType = adOpenForwardOnly
    Tabela.LockType = adLockReadOnly
    
    Tabela.Open sql, gConexao
    
    If Not Tabela.EOF Then
        If IsNull(Tabela(0)) Then
            gGrupoARS = ""
        Else
            gGrupoARS = Trim(Tabela(0))
        End If
    Else
        gGrupoARS = ""
    End If
    
    Set Tabela = Nothing

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_CarregaVector_gARS (OBJ_EFR) -> " & Err.Description)
            Exit Sub
    End Select
End Sub


Public Sub EFR_CarregaVector_gADSE(ByRef Vector As Variant, chave As String)

    ' Carrega as entidades do tipo gARS num Array.
    
    On Error GoTo ErrorHandler
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    
    sql = "SELECT " & _
          "     conteudo " & _
          "FROM " & _
                cParamAmbiente & " " & _
          "WHERE " & _
          "     chave = '" & chave & "'"
    
    Set Tabela = New ADODB.recordset
    Tabela.CursorLocation = adUseClient
    Tabela.CursorType = adOpenForwardOnly
    Tabela.LockType = adLockReadOnly
    
    Tabela.Open sql, gConexao
    
    If Tabela.RecordCount > 0 Then
        ReDim Vector(Tabela.RecordCount)
        i = 1
        While Not Tabela.EOF
            Vector(i) = Trim(Tabela!conteudo)
            i = i + 1
            Tabela.MoveNext
        Wend
    End If

    Set Tabela = Nothing
    
    ' Determina a entidade onde se codifica os pre�os para todas as ADSE.
    
    sql = "SELECT " & _
          "     conteudo " & _
          "FROM " & _
                cParamAmbiente & " " & _
          "WHERE " & _
          "     chave = 'ADSE_PRECOS'"
    
    Set Tabela = New ADODB.recordset
    Tabela.CursorLocation = adUseClient
    Tabela.CursorType = adOpenForwardOnly
    Tabela.LockType = adLockReadOnly
    
    Tabela.Open sql, gConexao
    
    If Not Tabela.EOF Then
        If IsNull(Tabela(0)) Then
            gGrupoADSE = ""
        Else
            gGrupoADSE = Trim(Tabela(0))
        End If
    Else
        gGrupoADSE = ""
    End If
    
    Set Tabela = Nothing

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_CarregaVector_gADSE (OBJ_EFR) -> " & Err.Description)
            Exit Sub
    End Select
End Sub

Public Function EFR_Pesquisa(ByRef Codigo, _
                             ByRef descricao, _
                             Optional ByRef morada, _
                             Optional ByRef Contribuinte, _
                             Optional ByRef telefone, _
                             Optional ByRef CodigoPostal, _
                             Optional ByRef SeqPostal)
    
    ' Retorna os dados duma EFR.
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim cmd As New ADODB.Command
    
    sql = "SELECT " & _
          "     cod_efr, " & _
          "     descr_efr, " & _
          "     mor_efr, " & _
          "     n_contrib_efr, " & _
          "     telef_efr, " & _
          "     cod_postal, " & _
          "     n_seq_postal " & _
          "FROM " & _
          "     fa_efr " & _
          "WHERE " & _
          "     cod_efr = " & Codigo
    
    cmd.Prepared = True
    cmd.CommandType = adCmdText
    cmd.CommandText = sql
    Set cmd.ActiveConnection = gConexao
    
    Set rs = cmd.Execute
    If Not rs.EOF Then
        descricao = BL_HandleNull(rs!descr_efr, "")
        morada = BL_HandleNull(rs!mor_efr)
        Contribuinte = BL_HandleNull(rs!n_contrib_efr, "")
        telefone = BL_HandleNull(rs!telef_efr, "")
        CodigoPostal = BL_HandleNull(rs!cod_postal, "")
        SeqPostal = BL_HandleNull(rs!n_seq_postal)
    End If
    Set cmd = Nothing
    rs.Close
    Set rs = Nothing

    EFR_Pesquisa = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Pesquisa (OBJ_EFR) -> " & Err.Description)
            EFR_Pesquisa = -1
            Exit Function
    End Select
End Function


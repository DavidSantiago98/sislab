VERSION 5.00
Begin VB.Form FormFrase 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFrase"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7080
   Icon            =   "FormFrase.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   7080
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Estat. Microbiologia"
      Height          =   1215
      Left            =   240
      TabIndex        =   25
      Top             =   1920
      Width           =   6615
      Begin VB.CheckBox CkQuestionario 
         Caption         =   "Frase para Question�rio"
         Height          =   255
         Left            =   3480
         TabIndex        =   29
         Top             =   720
         Width           =   2895
      End
      Begin VB.CheckBox CkFacultativa 
         Caption         =   "Frase com Significado ""Facultativa"""
         Height          =   255
         Left            =   3480
         TabIndex        =   28
         Top             =   360
         Width           =   2895
      End
      Begin VB.CheckBox CkContaminante 
         Caption         =   "Frase com Significado ""Contaminante"""
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   360
         Width           =   3135
      End
      Begin VB.CheckBox CkImpropria 
         Caption         =   "Frase com Significado ""Impr�pria"""
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   720
         Width           =   2775
      End
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Cancelada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   305
      Left            =   3120
      TabIndex        =   24
      Top             =   120
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   90
      TabIndex        =   3
      Top             =   3510
      Width           =   6855
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6720
      TabIndex        =   14
      Top             =   7200
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcDescricao 
      Height          =   1335
      Left            =   930
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   480
      Width           =   5895
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   930
      TabIndex        =   1
      Top             =   120
      Width           =   1425
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   13
      Top             =   7560
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   12
      Top             =   7560
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   11
      Top             =   7200
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   10
      Top             =   7200
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   90
      TabIndex        =   0
      Top             =   5820
      Width           =   6885
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   7
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   6
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   5
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   4
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   3000
      TabIndex        =   23
      Top             =   7560
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3120
      TabIndex        =   22
      Top             =   7200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   7560
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   360
      TabIndex        =   20
      Top             =   7200
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   5160
      TabIndex        =   19
      Top             =   7200
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   540
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   225
      Left            =   120
      TabIndex        =   17
      Top             =   150
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   0
      TabIndex        =   16
      Top             =   3270
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   2760
      TabIndex        =   15
      Top             =   3270
      Width           =   855
   End
End
Attribute VB_Name = "FormFrase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 09/01/2003
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodigo_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)

End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Dicion�rio de Frases"
    Me.left = 540
    Me.top = 450
    Me.Width = 7170
    Me.Height = 7110 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_dicionario"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 12
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_frase"
    CamposBD(1) = "cod_frase"
    CamposBD(2) = "descr_frase"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"
    CamposBD(7) = "flg_invisivel"
    CamposBD(8) = "flg_contaminante"
    CamposBD(9) = "flg_impropria"
    CamposBD(10) = "flg_facultativa"
    CamposBD(11) = "flg_questionario"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcUtilizadorAlteracao
    Set CamposEc(6) = EcDataAlteracao
    Set CamposEc(7) = CkInvisivel
    Set CamposEc(8) = CkContaminante
    Set CamposEc(9) = CkImpropria
    Set CamposEc(10) = CkFacultativa
    Set CamposEc(11) = CkQuestionario
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo da Frase"
    TextoCamposObrigatorios(2) = "Descri��o da Frase"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_frase"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_frase", "descr_frase")
    NumEspacos = Array(22, 32)
    CkInvisivel.value = vbGrayed
    CkContaminante.value = vbGrayed
    CkImpropria.value = vbGrayed
    CkFacultativa.value = vbGrayed
    CkInvisivel.Visible = False
    CkQuestionario.value = vbGrayed
    

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormFrase = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CkInvisivel.value = vbGrayed
    CkContaminante.value = vbGrayed
    CkImpropria.value = vbGrayed
    CkInvisivel.Visible = False
    CkFacultativa.value = vbGrayed
    CkQuestionario.value = vbGrayed
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        If CkInvisivel.value = 1 Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
    End If

End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_frase ASC"
    Else
    End If
              
    ' Permite que a pesquisa n�o seja case sensitive.
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                    "descr_frase", _
                                    True)
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    Dim iRes As Integer
    
    sSql = "SELECT * FROM sl_dicionario WHERE cod_frase = " & BL_TrataStringParaBD(EcCodigo)
    rsDic.CursorType = adOpenStatic
    rsDic.CursorLocation = adUseServer
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount >= 1 Then
        BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
        rsDic.Close
        Set rsDic = Nothing
        Exit Sub
    End If
    rsDic.Close
    Set rsDic = Nothing
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_frase") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

'Sub BD_Delete()
'
'    Dim condicao As String
'    Dim SQLQuery As String
'    Dim MarcaLocal As Variant
'
'    condicao = ChaveBD & " = '" & BG_CvPlica(Rs(ChaveBD)) & "'"
'    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
'    BG_ExecutaQuery_ADO SQLQuery
'
'    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
'    MarcaLocal = Rs.Bookmark
'    Rs.Requery
'
'    If Rs.BOF And Rs.EOF Then
'        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
'        FuncaoEstadoAnterior
'        Exit Sub
'    End If
'
'    ' Inicio do preenchimento de 'EcLista'
'    BG_PreencheListBoxMultipla_ADO EcLista, Rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
'    ' Fim do preenchimento de 'EcLista'
'
'    If MarcaLocal <= EcLista.ListCount Then
'        If MarcaLocal <= Rs.RecordCount Then Rs.Move EcLista.ListIndex, MarcaLocal
'        If MarcaLocal <= Rs.RecordCount Then Rs.Bookmark = MarcaLocal
'    Else
'        Rs.MoveLast
'    End If
'
'    If Rs.EOF Then Rs.MovePrevious
'
'    LimpaCampos
'    PreencheCampos
'
'End Sub
'
'
'
Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    'Para os valores de refer�ncia
    BG_RollbackTransaction
    BG_BeginTransaction
    
    'Apaga os registos das Tabelas relacionadas
    'Call ApagaRelacoes(rs!cod_ana_s)
    
    'Apaga o registo da an�lise
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = "UPDATE sl_dicionario SET flg_invisivel = 1, " & _
                " user_act = '" & gCodUtilizador & "'," & _
                " dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
                "  WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    'Confirma a elimina��o
    BG_BeginTransaction
    
    'Temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
     'A Tabela Ficou Vazia?
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    
    'Era o �ltimo Registo?
    If rs.EOF Then
        rs.MovePrevious
    Else
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    End If
    
    LimpaCampos
    PreencheCampos

End Sub



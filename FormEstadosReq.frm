VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormEstadosReq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstadosReq"
   ClientHeight    =   6345
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6090
   Icon            =   "FormEstadosReq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6345
   ScaleWidth      =   6090
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ListView LvRequisicoes 
      Height          =   4335
      Left            =   240
      TabIndex        =   6
      Top             =   1440
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   7646
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   1320
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcDtFim 
      Height          =   285
      Left            =   3720
      TabIndex        =   1
      Top             =   600
      Width           =   975
   End
   Begin VB.TextBox EcDtIni 
      Height          =   285
      Left            =   1320
      TabIndex        =   0
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Data Final"
      Height          =   255
      Index           =   1
      Left            =   2880
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicial"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   1095
   End
End
Attribute VB_Name = "FormEstadosReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'


Public rs As ADODB.recordset

Private Type requisicoes
    n_req As String
    dt_chega As String
    estado_req As String
End Type
Dim EstrutReq() As requisicoes
Dim totalReq As Integer



Private Sub EcDtIni_Click()
    If EcDtIni = "" Then
        EcDtIni = Bg_DaData_ADO
    End If
End Sub
Private Sub EcDtfim_Click()
    If EcDtFim = "" Then
        If EcDtIni = "" Then
            EcDtFim = Bg_DaData_ADO
        Else
            EcDtFim = EcDtIni
        End If
    End If
End Sub



Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
    If KeyCode = vbKeyReturn Then
        For i = 1 To totalReq
            If EstrutReq(i).n_req = EcNumReq Then
                BG_Mensagem mediMsgBox, "Requisi��o em causa j� est� na lista!", vbExclamation, "Procurar"
                Exit Sub
            End If
        Next
        
        Set rs = New ADODB.recordset
    
        CriterioTabela = ConstroiSQL
        If ConstroiSQL = "" Then
            BL_FimProcessamento Me
            BG_Mensagem mediMsgBox, "N�o foi indicado nenhum crit�rio!", vbExclamation, "Procurar"
            Exit Sub
        End If
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
        rs.Open CriterioTabela, gConexao
        
        If rs.RecordCount <= 0 Then
            BL_FimProcessamento Me
            BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
            Exit Sub
        Else
            PreencheCampos
            
            BL_FimProcessamento Me
        End If
        rs.Close
        EcNumReq = ""
        EcNumReq.SetFocus
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Set CampoDeFocus = EcNumReq
    Me.caption = " Consulta de Estados de Requisi��o"
    Me.left = 5
    Me.top = 5
    Me.Width = 6180
    Me.Height = 6765 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        'rs.Close
        Set rs = Nothing
    End If
    Set FormEstadosReq = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    EcDtIni = ""
    EcDtFim = ""
    EcNumReq = ""
    EcNumReq.SetFocus
    LvRequisicoes.ListItems.Clear
    LimpaEstrut
End Sub
Private Sub LimpaEstrut()
    totalReq = 0
    ReDim EstrutReq(0)
    
End Sub

Sub DefTipoCampos()
    With LvRequisicoes
        .ColumnHeaders.Add(, , "Requisi��o", 2000, lvwColumnLeft).Key = "REQ"
        .ColumnHeaders.Add(, , "Estado", 3500, lvwColumnCenter).Key = "ESTADO"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .FullRowSelect = True
        .ForeColor = vbGreen
        .MultiSelect = False
    End With
    
    EcNumReq.Tag = adNumeric
    EcDtIni.Tag = adDate
    EcDtFim.Tag = adDate
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        While Not rs.EOF
            totalReq = totalReq + 1
            ReDim Preserve EstrutReq(totalReq)
            EstrutReq(totalReq).n_req = BL_HandleNull(rs!n_req, "")
            EstrutReq(totalReq).dt_chega = BL_HandleNull(rs!dt_chega, "")
            EstrutReq(totalReq).estado_req = BL_HandleNull(rs!estado_req, "")
            With LvRequisicoes.ListItems.Add(totalReq, "KEY_" & rs!n_req, rs!n_req)
                
                Select Case rs!estado_req
                    Case gEstadoReqCancelada
                        .ListSubItems.Add , , "Cancelada"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbRed
                    Case gEstadoReqEsperaProduto
                        .ListSubItems.Add , , "Espera Produto"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbRed
                    Case gEstadoReqEsperaResultados
                        .ListSubItems.Add , , "Espera Resultados"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbRed
                    Case gEstadoReqEsperaValidacao
                        .ListSubItems.Add , , "Espera Valida��o"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbRed
                    Case gEstadoReqHistorico
                        .ListSubItems.Add , , "Hist�rico"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbGreen
                    Case gEstadoReqImpressaoParcial
                        .ListSubItems.Add , , "Impress�o Parcial"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbRed
                    Case gEstadoReqResultadosParciais
                        .ListSubItems.Add , , "Resultados Parciais"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbRed
                    Case gEstadoReqTodasImpressas
                        .ListSubItems.Add , , "Todas Impressas"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbGreen
                    Case gEstadoReqValicacaoMedicaCompleta
                        .ListSubItems.Add , , "Valida��o M�dica Completa"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbGreen
                    Case gEstadoReqValidacaoMedicaParcial
                        .ListSubItems.Add , , "Valida��o M�dica Parcial"
                        LvRequisicoes.ListItems.item(totalReq).ForeColor = vbRed
                    End Select
                    LvRequisicoes.ListItems.item(totalReq).ListSubItems(1).ForeColor = LvRequisicoes.ListItems.item(totalReq).ForeColor
            End With
            
            rs.MoveNext
        Wend
    End If
End Sub


Sub FuncaoLimpar()
    
        LimpaCampos
        CampoDeFocus.SetFocus

End Sub

Sub FuncaoEstadoAnterior()

End Sub

Function ValidaCamposEc() As Integer

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = ConstroiSQL
    If ConstroiSQL = "" Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi indicado nenhum crit�rio!", vbExclamation, "Procurar"
        FuncaoLimpar
        Exit Sub
    End If
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        
        PreencheCampos
        BL_ToolbarEstadoN estado
        'BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    rs.Close
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()

End Sub

Sub BD_Insert()
        
    
End Sub

Sub FuncaoModificar()

End Sub

Sub BD_Update()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

' -----------------------------------------------------------------

' CONSTROI CRITERIO PARA SQL

' -----------------------------------------------------------------

Private Function ConstroiSQL() As String
    Dim sSql As String
    Dim flg As Boolean
    sSql = "SELECT n_Req, estado_Req, dt_chega FROM sl_requis WHERE n_Req = n_req "
    flg = False
    If EcNumReq <> "" Then
        sSql = sSql & " AND n_Req = " & EcNumReq
        flg = True
    End If
    
    If EcDtIni <> "" And EcDtFim <> "" Then
        sSql = sSql & " AND dt_chega between " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
        flg = True
    End If
    If flg = True Then
        ConstroiSQL = sSql
    Else
        ConstroiSQL = ""
    End If
Exit Function
TrataErro:
    ConstroiSQL = ""
    BG_LogFile_Erros "Erro  ao Construir SQL: " & Err.Description, Me.Name, "ConstroiSQL", True
    Exit Function
    Resume Next
End Function
Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function


Attribute VB_Name = "Recibos"
Option Explicit



Const lDocCaixa = 1
Const lRecibo = 2

' --------------------------------------------------------------------------------------------------------------------------------------------

' GERA O RECIBO

' --------------------------------------------------------------------------------------------------------------------------------------------
Public Function RECIBO_GeraReciboDetalhe(n_req As String, indice As Long, tipoDocumento As Integer, ByRef RegistosA As Collection, _
                                  ByRef RegistosR As Collection, ByRef RegistosRM As Collection, flg_adiantamento As Boolean) As Boolean
    Dim numRecibo As Long
    Dim i As Integer
    Dim serie_rec As String
    Dim Certificado As Boolean
    Dim resRecibo As Boolean
    Dim idCert As Long
    Dim iBD_Aberta As Integer
    On Error GoTo TrataErro
    RECIBO_GeraReciboDetalhe = False
        
    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA RECIBO
    ' -----------------------------------------------------------------------------------------------
    Certificado = False
    serie_rec = ""
    
    If gUsaSerieAnualRec = mediSim Then
        serie_rec = RECIBO_DevolveSerie(RegistosR(indice).codEmpresa, tipoDocumento)
    Else
        If tipoDocumento = lRecibo Then
            serie_rec = BL_SelCodigo("SL_EMPRESAS", "SERIE_REC", "COD_EMPRESA", RegistosR(indice).codEmpresa, "V")
        ElseIf tipoDocumento = lDocCaixa Then
            serie_rec = BL_SelCodigo("SL_EMPRESAS", "SERIE_DOC", "COD_EMPRESA", RegistosR(indice).codEmpresa, "V")
        End If
    End If
    If serie_rec = "" Then
        GoTo TrataErro
    End If
    RegistosR(indice).codEmpresa = BL_HandleNull(RegistosR(indice).codEmpresa, BL_SelCodigo("SL_EFR", "COD_EMPRESA", "COD_EFR", RegistosR(indice).codEntidade, "V"))
    
    If gSGBD = gSqlServer Or gSGBD = gPostGres Then
        ' ABRE CONEXAO COM FACTUS
        iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
        If iBD_Aberta = 0 Then
            RECIBO_GeraReciboDetalhe = False
            Exit Function
        End If
    
        gConexaoSecundaria.BeginTrans
    End If
    
    
    If (RegistosR(indice).Estado = gEstadoReciboNaoEmitido Or RegistosR(indice).Estado = gEstadoReciboPerdido) And RegistosR(indice).NumDoc = "0" Then
        RECIBO_RecalculaValor CLng(indice), RegistosA, RegistosR, RegistosRM
        'abre transacao
        BG_BeginTransaction
        numRecibo = -1
        idCert = -1
        numRecibo = RECIBO_GeraNumero(indice, Certificado, tipoDocumento, serie_rec, RegistosR, idCert)
        If numRecibo <> -1 And Certificado = True Then
        
            
            ' -----------------------------------------------------------------------------------------------
            ' ACTUALIZA OS VALORES PARA CADA ANALISE MANUAL DESSA ENTIDADE
            ' -----------------------------------------------------------------------------------------------
            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboCodFacturavel <> "" And RegistosRM(i).ReciboEntidade = RegistosR(indice).codEntidade And RegistosRM(i).ReciboFlgAdicionado = 0 Then
                    If CLng(BL_HandleNull(RegistosRM(i).ReciboNumDoc, "0")) = CLng(BL_HandleNull(RegistosR(indice).NumDoc, 0)) Then
                        RegistosRM(i).ReciboNumDoc = numRecibo
                        RegistosRM(i).ReciboSerieDoc = serie_rec
                        RegistosRM(i).ReciboUserEmi = gCodUtilizador
                        RegistosRM(i).ReciboDtEmi = Bg_DaData_ADO
                        RegistosRM(i).ReciboHrEmi = Bg_DaHora_ADO
                        RegistosRM(i).ReciboFlgImpresso = 1
                        RegistosRM(i).ReciboFlgFacturado = BL_HandleNull(RegistosRM(i).ReciboFlgFacturado, 0)
                    End If
                End If
            Next
            
            ' -----------------------------------------------------------------------------------------------
            ' ACTUALIZA OS VALORES DA ENTIDADE
            ' -----------------------------------------------------------------------------------------------
            RegistosR(indice).NumDoc = numRecibo
            RegistosR(indice).Caucao = 0
            RegistosR(indice).UserEmi = gCodUtilizador
            RegistosR(indice).DtEmi = Bg_DaData_ADO
            RegistosR(indice).HrEmi = Bg_DaHora_ADO
            RegistosR(indice).flg_impressao = 1
            RegistosR(indice).NumVendaDinheiro = RegistosR(indice).NumVendaDinheiro
            If flg_adiantamento = True Then
                RegistosR(indice).Estado = gEstadoReciboCobranca
                RegistosR(indice).DtPagamento = ""
                RegistosR(indice).HrPagamento = ""
            Else
                RegistosR(indice).Estado = gEstadoReciboPago
                RegistosR(indice).DtPagamento = Bg_DaData_ADO
                RegistosR(indice).HrPagamento = Bg_DaHora_ADO
            End If
            RegistosR(indice).Certificado = ""
            
            'soliveira lacto **
            RegistosR(indice).Desconto = RegistosR(indice).Desconto
            RegistosR(indice).ValorPagar = RegistosR(indice).ValorPagar
            RegistosR(indice).SerieDoc = serie_rec
            If tipoDocumento = lRecibo Then
                RegistosR(indice).flg_DocCaixa = 0
            ElseIf tipoDocumento = lDocCaixa Then
                RegistosR(indice).flg_DocCaixa = 1
            End If
            
            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboCodFacturavel <> "" And RegistosRM(i).ReciboEntidade = RegistosR(indice).codEntidade And RegistosRM(i).ReciboFlgAdicionado = mediSim Then
                    RECIBO_AdicionaAnaliseAoReciboManual CLng(i), False, RegistosA, RegistosR, RegistosRM
                End If
            Next
            
            resRecibo = RECIBO_GravaRecibo(n_req, RegistosA, RegistosR, RegistosRM, True)
            If resRecibo = False Then
                GoTo TrataErro
            End If
            

            'GX-62545
            Dim ATValidNumber As String
            Dim ATCUD As String
            Dim tipoDesc As String
            Dim seq As String
            If tipoDocumento = lRecibo Then
                'tipoDesc = "FAC_REC"
                tipoDesc = "FS"
            End If
                        
            If gUsaSerieAnualRec = mediSim Then
              ATValidNumber = RECIBO_Devolve_AtCode(RegistosR(indice).codEmpresa, tipoDocumento)
            End If
            If ATValidNumber <> "-1" Then
                seq = Format(numRecibo, String(10, "0"))
                ATCUD = ATValidNumber & "-" & seq
                
                Dim sql As String
                Dim rs As New adodb.recordset
                rs.CursorLocation = adUseClient
                rs.CursorType = adOpenStatic
                
                sql = "UPDATE sl_recibos SET atcud = " & BL_TrataStringParaBD(ATCUD) & " WHERE seq_recibo = " & RegistosR(indice).SeqRecibo
                If BG_ExecutaQuery_ADO(sql) <= 0 Then
                    GoTo TrataErro
                End If
                If rs.state = adodb.ObjectStateEnum.adStateOpen Then rs.Close
            End If
            '
            
            If gUsaSerieAnualRec = mediSim Then
                If RECIBO_ActualizaNumeroAnual(RegistosR(indice).SerieDoc, numRecibo) = False Then
                    GoTo TrataErro
                End If
            Else
                If RECIBO_ActualizaNumero(RegistosR(indice).codEmpresa, tipoDocumento, numRecibo) = False Then
                    GoTo TrataErro
                End If
            End If
            
            If tipoDocumento = lRecibo And gUsaSerieAnualRec = mediSim Then
                If IF_DevolveCertificado(idCert) = False Then
                    GoTo TrataErro
                End If
            End If
            
            'conclui transacao
            BG_CommitTransaction
            If gSGBD = gSqlServer Then
                gConexaoSecundaria.CommitTrans
            End If
            If tipoDocumento = lRecibo Or gImprimeDocCaixa = mediSim Then
                RECIBO_ImprimeRecibo "", n_req, numRecibo, serie_rec, RegistosR(indice).codEntidade, FormGestaoRequisicaoPrivado.EcPrinterRecibo, RegistosR(indice).SeqUtente
            End If
        Else
            BG_Mensagem mediMsgBox, "Erro ao gerar recibo.", vbOKOnly + vbCritical, "InsereRecibo"
            GoTo TrataErro
        End If
    End If
    
    RECIBO_GeraReciboDetalhe = True
    
Exit Function
TrataErro:
    On Error Resume Next
    gConexaoSecundaria.RollbackTrans
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel <> "" And RegistosRM(i).ReciboEntidade = RegistosR(indice).codEntidade Then
            If CLng(BL_HandleNull(RegistosRM(i).ReciboNumDoc, "0")) = CLng(BL_HandleNull(RegistosR(indice).NumDoc, 0)) Then
                RegistosRM(i).ReciboNumDoc = "0"
                RegistosRM(i).ReciboSerieDoc = "0"
                RegistosRM(i).ReciboUserEmi = ""
                RegistosRM(i).ReciboDtEmi = ""
                RegistosRM(i).ReciboHrEmi = ""
                RegistosRM(i).ReciboFlgImpresso = 0
                RegistosRM(i).ReciboFlgFacturado = 0
            End If
        End If
    Next
    RegistosR(indice).NumDoc = "0"
    RegistosR(indice).Caucao = 0
    RegistosR(indice).UserEmi = ""
    RegistosR(indice).DtEmi = ""
    RegistosR(indice).HrEmi = ""
    RegistosR(indice).flg_impressao = 0
    RegistosR(indice).DtPagamento = ""
    RegistosR(indice).HrPagamento = ""
    RegistosR(indice).NumVendaDinheiro = ""
    RegistosR(indice).Estado = gEstadoReciboNaoEmitido
    RegistosR(indice).Certificado = ""
    RegistosR(indice).SerieDoc = "0"
    If gSGBD = gSqlServer Then
        gConexaoSecundaria.RollbackTrans
    End If
    BG_RollbackTransaction
    RECIBO_GeraReciboDetalhe = False
    BG_LogFile_Erros "RECIBO_GeraReciboDetalhe: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_GeraReciboDetalhe"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' APAGA DAS TABELAS E CHAMA FUNCAO PARA INSERIR NA BD

' ----------------------------------------------------------------------
Public Function RECIBO_GravaRecibo(n_req As String, ByRef RegistosA As Collection, _
                                  ByRef RegistosR As Collection, ByRef RegistosRM As Collection, obrigaInsert As Boolean) As Boolean
     On Error GoTo TrataErro
  
    Dim sql As String
    Dim detalhe As Boolean
    RECIBO_GravaRecibo = False
    gSQLError = 0
    
    sql = "DELETE FROM sl_recibos_det WHERE  n_req=" & Trim(n_req)
    sql = sql & " AND (flg_marcacao_retirada is null or flg_marcacao_retirada = 0) AND n_rec = 0 "
    BG_ExecutaQuery_ADO sql
    If gSQLError <> 0 Then
        GoTo TrataErro
    End If
    
    sql = "DELETE FROM sl_recibos WHERE n_req=" & Trim(n_req) & " AND n_Rec = 0"
    BG_ExecutaQuery_ADO sql
           
    If gSQLError <> 0 Then
        GoTo TrataErro
    End If
    
    detalhe = RECIBO_GravaBD(n_req, RegistosA, RegistosR, RegistosRM, obrigaInsert)
    If detalhe = False Then
        GoTo TrataErro
    End If
    RECIBO_GravaRecibo = True
Exit Function
TrataErro:
    RECIBO_GravaRecibo = False
    BG_LogFile_Erros "RECIBO_GravaRecibo: " & Err.Number & " - " & Err.Description, "RECIBOS", "RECIBO_GravaRecibo"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------

' INSERE NA BD

' ----------------------------------------------------------------------
Private Function RECIBO_GravaBD(n_req As String, ByRef RegistosA As Collection, _
                                  ByRef RegistosR As Collection, ByRef RegistosRM As Collection, obrigaInsert As Boolean) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim ssql As String
    Dim j As Integer
    Dim ret As Long
    Dim seq_Recibo As Long
    Dim inseriu As Boolean
    
    RECIBO_GravaBD = False
    
    If RegistosR.Count <= 1 Then
        RECIBO_GravaBD = True
        Exit Function
    End If
    
    gSQLError = 0
    inseriu = False
    
    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA RECIBO
    ' -----------------------------------------------------------------------------------------------
    For i = 1 To RegistosR.Count
        RegistosR(i).insere = False
        
        RECIBO_RecalculaValor CLng(i), RegistosA, RegistosR, RegistosRM
        
        RegistosR(i).insere = RECIBO_VerificaInsereRecibo(n_req, RegistosR, i) = True
        
        'BG_LogFile_Erros "RECIBO_GravaBD VP:" & RegistosR(i).ValorPagar & " - INSERE: " & CStr(RegistosR(i).insere), "RECIBO", "RECIBO_GravaBD"
        If RegistosR(i).ValorPagar >= 0 And RegistosR(i).insere = True Then
            ' -----------------------------------------------------------------------------------------------
            ' TRATA-SE DE UM RECIBO
            ' -----------------------------------------------------------------------------------------------
            If RegistosR(i).codEntidade <> "" Then
                If RegistosR(i).DtCri = "" And (RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboNulo Or RegistosR(i).Estado = gEstadoReciboPerdido) Then
                    RegistosR(i).NumDoc = 0
                    RegistosR(i).SerieDoc = 0
                    RegistosR(i).DtCri = Bg_DaData_ADO
                    RegistosR(i).DtCri = Bg_DaData_ADO
                    RegistosR(i).HrCri = Bg_DaHora_ADO
                    RegistosR(i).UserCri = gCodUtilizador
                    RegistosR(i).DtEmi = ""
                    RegistosR(i).HrEmi = ""
                    RegistosR(i).UserEmi = ""
                    RegistosR(i).flg_impressao = 0
                    RegistosR(i).DtPagamento = ""
                    RegistosR(i).HrPagamento = ""
                    RegistosR(i).NumVendaDinheiro = ""
                    RegistosR(i).flg_DocCaixa = 0
                    RegistosR(i).Certificado = ""
                    RegistosR(i).CodFormaPag = gModoPagNaoPago
                    RegistosR(i).SeqUtente = -1
                ElseIf (RegistosR(i).NumDoc = 0 Or RegistosR(i).NumDoc = "") And (RegistosR(i).Estado = gEstadoReciboPago) Then
                    GoTo TrataErro
                End If
                
                'VERIFICA SE TEM CODIGO DA EMPRESA
                If RegistosR(i).codEmpresa = "" Then
                    RegistosR(i).codEmpresa = BL_SelCodigo("SL_EFR", "COD_EMPRESA", "COD_EFR", RegistosR(i).codEntidade, "V")
                End If
                
                ssql = "INSERT INTO sl_recibos(seq_recibo, n_req, cod_efr, serie_rec, n_rec, total_pagar,user_cri,"
                ssql = ssql & " dt_cri,hr_cri,user_emi,dt_emi,hr_emi, flg_impresso, "
                ssql = ssql & " estado, dt_pagamento, hr_pagamento, venda_dinheiro,desconto, num_analises,"
                ssql = ssql & " flg_doc_caixa, valor_original, COD_EMPRESA, cod_forma_pag, seq_utente, flg_borla,"
                ssql = ssql & " nao_conformidade) VALUES( "
                
                If RegistosR(i).SeqRecibo > 0 Then
                    ssql = ssql & RegistosR(i).SeqRecibo & ", "
                Else
                    RegistosR(i).SeqRecibo = RECIBO_RetornaSeqRecibo
                    ssql = ssql & RegistosR(i).SeqRecibo & ", "
                End If
                seq_Recibo = RegistosR(i).SeqRecibo
                
                ssql = ssql & n_req & ", "
                ssql = ssql & RegistosR(i).codEntidade & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).SerieDoc) & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).NumDoc) & ", "
                ssql = ssql & Replace(RegistosR(i).ValorPagar, ",", ".") & ", "
                ssql = ssql & BL_TrataStringParaBD(CStr(RegistosR(i).UserCri)) & ", "
                ssql = ssql & BL_TrataDataParaBD(RegistosR(i).DtCri) & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).HrCri) & ", "
                ssql = ssql & BL_TrataStringParaBD(CStr(RegistosR(i).UserEmi)) & ", "
                ssql = ssql & BL_TrataDataParaBD(RegistosR(i).DtEmi) & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).HrEmi) & ", "
                ssql = ssql & CInt(RegistosR(i).flg_impressao) & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).Estado) & ", "
                ssql = ssql & BL_TrataDataParaBD(RegistosR(i).DtPagamento) & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).HrPagamento) & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).NumVendaDinheiro) & ", "
                ssql = ssql & Replace(RegistosR(i).Desconto, ",", ".") & ", "
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).NumAnalises) & ", "
                ssql = ssql & RegistosR(i).flg_DocCaixa & ", "
                If gSGBD = gPostGres Then
                    ssql = ssql & "to_number(" & BL_TrataStringParaBD(Replace(RegistosR(i).ValorOriginal, ",", ".")) & ",'99999,99'), "
                Else
                    ssql = ssql & Replace(RegistosR(i).ValorOriginal, ",", ".") & ", "
                End If
                ssql = ssql & BL_TrataStringParaBD(RegistosR(i).codEmpresa) & ", "
                ssql = ssql & RegistosR(i).CodFormaPag & ","
                ssql = ssql & RegistosR(i).SeqUtente & ","
                ssql = ssql & RegistosR(i).FlgBorla & ","
                ssql = ssql & BL_HandleNull(RegistosR(i).FlgNaoConforme, 0) & ")"
                BG_LogFile_Erros ssql, "RECIBO", "GRAVABD", False
                If BG_ExecutaQuery_ADO(ssql) <= 0 Then
                    GoTo TrataErro
                Else
                    inseriu = True
                End If
                If gSQLError <> 0 Then
                    GoTo TrataErro
                End If
            End If
        ElseIf RegistosR(i).insere = False And RegistosR(i).SeqRecibo > 0 Then
            ssql = "UPDATE SL_RECIBOS SET flg_borla = " & RegistosR(i).FlgBorla
            ssql = ssql & ", nao_conformidade = " & BL_HandleNull(RegistosR(i).FlgNaoConforme, 0)
            ssql = ssql & " WHERE seq_recibo = " & RegistosR(i).SeqRecibo
            If BG_ExecutaQuery_ADO(ssql) <= 0 Then
                GoTo TrataErro
            Else
                inseriu = True
            End If
        End If
    Next
    
    
    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA ANALISE NA TABELA SL_RECIBOS_DET - recibos manuais
    ' -----------------------------------------------------------------------------------------------
    If RegistosRM.Count > 0 Then
        If Trim(RegistosRM(1).ReciboCodFacturavel) <> "" Then
            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboCodFacturavel <> "" And (Trim(RegistosRM(i).ReciboEntidade) <> "" And RegistosRM(i).ReciboIsencao <> "") Then
                    For j = 1 To RegistosR.Count
                        If RegistosRM(i).ReciboNumDoc = RegistosR(j).NumDoc And RegistosRM(i).ReciboSerieDoc = RegistosR(j).SerieDoc _
                            And RegistosRM(i).ReciboEntidade = RegistosR(j).codEntidade Then
                            
                            If RegistosR(j).insere = True Then
                                If RegistosRM(i).ReciboDtCri = "" Then
                                    RegistosRM(i).ReciboDtCri = Bg_DaData_ADO
                                    RegistosRM(i).ReciboHrCri = Bg_DaHora_ADO
                                    RegistosRM(i).ReciboUserCri = gCodUtilizador
                                    RegistosRM(i).ReciboDtEmi = ""
                                    RegistosRM(i).ReciboHrEmi = ""
                                    RegistosRM(i).ReciboUserEmi = ""
                                    RegistosRM(i).ReciboFlgImpresso = 0
                                    RegistosRM(i).ReciboFlgFacturado = 0
                                    'RegistosRM(i).ReciboFlgRetirado = 0
                                End If
                                'RGONCALVES 04.08.2016 LRV-381 - comentado if
                                'If RegistosRM(i).ReciboSeqRecibo <= 0 Then
                                '
                                    'RGONCALVES 14.07.2016 LRV-368
                                    'RegistosRM(i).ReciboSeqRecibo = seq_Recibo
                                    
                                    'BRUNOSANTOS - 06.09.2016 - LRV-395 -> ADICIONADO  RegistosRM(i).estado
                                    'RegistosRM(i).ReciboSeqRecibo = RECIBO_RetornaSeqReciboPorEntidade(RegistosRM(i).ReciboEntidade, RegistosRM(i).ReciboEstado, RegistosR)
                                     RegistosRM(i).ReciboSeqRecibo = RECIBO_RetornaSeqReciboPorEntidade(RegistosRM(i).ReciboEntidade, RegistosR)
                                    '
                               ' End If
                                ssql = "INSERT INTO sl_recibos_det (n_req,n_rec,serie_rec, cod_efr,cod_ana,cod_facturavel,p1,taxa,user_cri,"
                                ssql = ssql & " dt_cri,hr_cri,user_emi,dt_emi,hr_emi,flg_impresso, flg_facturado,flg_retirado, "
                                ssql = ssql & " flg_adicionada, ISENCAO, ordem_marcacao,cor_p1,cod_medico, cod_ana_efr,quantidade, taxa_unitario, "
                                ssql = ssql & " flg_marcacao_retirada, flg_recibo_manual, ana_nao_fact, n_benef,fds, codigo_pai, descr_ana_efr, "
                                ssql = ssql & " flg_cod_barras, flg_automatico, cod_efr_envio, quantidade_envio, SEQ_RECIBO) VALUES( "
                                ssql = ssql & n_req & ", "
                                ssql = ssql & RegistosRM(i).ReciboNumDoc & ", "
                                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(RegistosRM(i).ReciboSerieDoc, "")) & ", "
                                ssql = ssql & RegistosRM(i).ReciboEntidade & ", "
                                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(RegistosRM(i).ReciboCodAna, RegistosRM(i).ReciboCodFacturavel)) & ", "
                                ssql = ssql & BL_TrataStringParaBD(RegistosRM(i).ReciboCodFacturavel) & ", "
                                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(RegistosRM(i).ReciboP1, "NULL")) & ", "
                                ssql = ssql & BL_HandleNull(Replace(RegistosRM(i).ReciboTaxa, ",", "."), 0) & ", "
                                ssql = ssql & BL_TrataStringParaBD(CStr(RegistosRM(i).ReciboUserCri)) & ", "
                                ssql = ssql & BL_TrataDataParaBD(RegistosRM(i).ReciboDtCri) & ", "
                                ssql = ssql & BL_TrataStringParaBD(RegistosRM(i).ReciboHrCri) & ", "
                                ssql = ssql & BL_TrataStringParaBD(CStr(RegistosRM(i).ReciboUserEmi)) & ", "
                                ssql = ssql & BL_TrataDataParaBD(RegistosRM(i).ReciboDtEmi) & ", "
                                ssql = ssql & BL_TrataStringParaBD(RegistosRM(i).ReciboHrEmi) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFlgImpresso, 0) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFlgFacturado, 0) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFlgRetirado, 0) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFlgAdicionado, 0) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboIsencao, 0) & ", "
                                ssql = ssql & RegistosRM(i).ReciboOrdemMarcacao & " , "
                                ssql = ssql & BL_TrataStringParaBD(RegistosRM(i).ReciboCorP1) & ", "
                                ssql = ssql & BL_TrataStringParaBD(RegistosRM(i).ReciboCodMedico) & ", "
                                ssql = ssql & BL_TrataStringParaBD(RegistosRM(i).ReciboCodAnaEFR) & " , "
                                ssql = ssql & RegistosRM(i).ReciboQuantidade & " , "
                                ssql = ssql & BL_HandleNull(Replace(RegistosRM(i).ReciboTaxaUnitario, ",", "."), 0) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFlgMarcacaoRetirada, 0) & " , "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFlgReciboManual, 0) & ", "
                                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(RegistosRM(i).ReciboAnaNaoFact, "")) & ", "
                                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(RegistosRM(i).ReciboNrBenef, "")) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFDS, 0) & ", "
                                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(RegistosRM(i).ReciboCodigoPai, "")) & ", "
                                ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(RegistosRM(i).ReciboDescrAnaEFR, "")) & ", "
                                ssql = ssql & BL_HandleNull(RegistosRM(i).flgCodBarras, 0) & ","
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboFlgAutomatico, 0) & ","
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboCodEfrEnvio, RegistosRM(i).ReciboEntidade) & ","
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboQuantidadeEFR, RegistosRM(i).ReciboQuantidade) & ","
                                ssql = ssql & BL_HandleNull(RegistosRM(i).ReciboSeqRecibo, mediComboValorNull) & ")"
                                If BG_ExecutaQuery_ADO(ssql) <= 0 Then
                                    GoTo TrataErro
                                End If
                                If gSQLError <> 0 Then
                                    GoTo TrataErro
                                End If
                            ElseIf BL_HandleNull(RegistosRM(i).ReciboFlgMarcacaoRetirada, 0) = 1 Then
                                ssql = "UPDATE sl_Recibos_Det SET flg_marcacao_retirada = 1 WHERE "
                                ssql = ssql & " n_req = " & n_req
                                ssql = ssql & " AND n_rec = " & RegistosRM(i).ReciboNumDoc
                                ssql = ssql & " AND serie_rec = " & BL_TrataStringParaBD(RegistosRM(i).ReciboSerieDoc)
                                ssql = ssql & " AND cod_ana = " & BL_TrataStringParaBD(RegistosRM(i).ReciboCodFacturavel)
                                If BG_ExecutaQuery_ADO(ssql) <= 0 Then
                                    GoTo TrataErro
                                End If
                                If gSQLError <> 0 Then
                                    GoTo TrataErro
                                End If
                            End If
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    If obrigaInsert = True And inseriu = False Then
        BG_LogFile_Erros "RECIBO_GravaBD NAO INSERIU: " & n_req, "RECIBO", "RECIBO_GravaBD"
        GoTo TrataErro
    Else
        RECIBO_GravaBD = True
    End If
Exit Function
TrataErro:
    RECIBO_GravaBD = False
    BG_LogFile_Erros "RECIBO_GravaBD: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_GravaBD"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------

' RECALCULA VALOR DO RECIBO

' ----------------------------------------------------------------------
Private Sub RECIBO_RecalculaValor(indice As Long, ByRef RegistosA As Collection, _
                                  ByRef RegistosR As Collection, ByRef RegistosRM As Collection)
    On Error GoTo TrataErro
    Dim i As Long
    Dim taxa As Double
    If RegistosR(indice).Estado = gEstadoReciboNaoEmitido Or RegistosR(indice).Estado = gEstadoReciboNulo Or _
        RegistosR(indice).Estado = gEstadoReciboPerdido Then
        taxa = 0

        For i = 1 To RegistosRM.Count
            If RegistosRM(i).ReciboEntidade = RegistosR(indice).codEntidade And RegistosRM(i).ReciboNumDoc = RegistosR(indice).NumDoc And RegistosRM(i).ReciboSerieDoc = RegistosR(indice).SerieDoc And RegistosRM(i).ReciboFlgAdicionado = 0 Then
                taxa = taxa + RegistosRM(i).ReciboTaxa
            End If
        Next
        If BL_HandleNull(RegistosR(indice).Desconto) = 0 Then
            RegistosR(indice).ValorPagar = taxa
        Else
            RegistosR(indice).ValorPagar = Round(CDbl(taxa) - RegistosR(indice).Desconto * CDbl(taxa) / 100, 2)
        End If
        
        RegistosR(indice).ValorOriginal = taxa
        If RegistosR(indice).ValorPagar = 0 Then
            RegistosR(indice).Estado = gEstadoReciboNulo
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_RecalculaValor: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_RecalculaValor"
    Exit Sub
    Resume Next
End Sub


' --------------------------------------------------------------------------------------------------------------

' GERA O NUMERO DE RECIBO / DOC DE CAIXA E AVALIA O RESPECTIVO CERTIFICADO

' --------------------------------------------------------------------------------------------------------------
Private Function RECIBO_GeraNumero(ByVal j As Long, ByRef Certificado As Boolean, tipoDocumento As Integer, _
                                   serie_rec As String, ByRef RegistosR As Collection, ByRef idCert As Long) As Long
    Dim i As Integer
    Dim retorno As Long
    On Error GoTo TrataErro
    
    retorno = -1
    i = 0
    While (retorno = -1 And i <= 10)
        'BG_LogFile_Erros i & "Geracao de Recibo: " & J & "rec gerado:" & Retorno & " Rec Estrutura:" & RegistosR(J).NumDoc & " efr:" & RegistosR(J).codEntidade, "TESTE1", "TESTE1", False
        If gUsaSerieAnualRec = mediSim Then
            retorno = RECIBO_DevolveNumeroSerie(serie_rec)
        Else
            retorno = RECIBO_DevolveNumero(RegistosR(j).codEmpresa, tipoDocumento)
        End If
        If retorno = -1 Then
            i = i + 1
        End If
        BG_LogFile_Erros i & "Geracao de Recibo: " & j & "rec gerado:" & retorno & " Rec Estrutura:" & RegistosR(j).NumDoc & " efr:" & RegistosR(j).codEntidade, "TESTE", "TESTE", False
    Wend
    
    If tipoDocumento = lRecibo Then
        If retorno > -1 Then
            ' VERIFICA SE CERTIFICADO FOI GERADO CORRECTAMENTE
            Certificado = IF_GeraCertificado(Bg_DaData_ADO, RegistosR(j).codEmpresa, _
                                             serie_rec, CStr(retorno), _
                                             RegistosR(j).ValorPagar, idCert)
        End If
        
    ElseIf tipoDocumento = lDocCaixa Then
        Certificado = True
    End If
    If Certificado = False Then
        RECIBO_GeraNumero = -1
    Else
        RECIBO_GeraNumero = retorno
    End If
Exit Function
TrataErro:
    Certificado = False
    RECIBO_GeraNumero = -1
    BG_LogFile_Erros "RECIBO_GeraNumero: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_GeraNumero"
    Exit Function
    Resume Next
End Function


Public Sub RECIBO_ImprimeRecibo(descricao As String, n_req As String, NumDoc As Long, serieRec As String, _
                                codEfr As String, impressora As String, seq_utente As Long)
    On Error GoTo TrataErro
    Dim continua As Boolean
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim num_recibo As String
    Dim data As String
    Dim efr As String
    Dim nome As String
    Dim num_benef As String
    Dim analises As String
    Dim ult_analise As String
    Dim total As Double
    Dim Tot As String
    Dim taxa_ana As String
    Dim user_emi As String
    Dim DataChega As String
    Dim cod_empresa As String
    Dim nome_empresa As String
    Dim num_contribuinte As String
    Dim n_contrib_ute As String
    Dim descr_analise As String
    Dim total_original As Double
    Dim Desconto As Double
    Dim num_copias_rec As Integer
    Dim cabecalho As String
    Dim Formacao As String
    Dim Certificado As String
    Dim totalPagar As Double
    Dim flgDocCaixa As String
    Dim morada As String
    Dim cod_postal As String
    Dim descr_postal As String
    Dim i As Integer
    Dim qtd As Integer
    'Dim descr_ana As String
    
    Dim vd As String
    
    If gReciboCrystal = 1 Then
        ssql = "DELETE FROM SL_CR_RECIBO WHERE nome_computador = '" & BG_SYS_GetComputerName & "'"
        ssql = ssql & " AND num_sessao = " & gNumeroSessao
        BG_ExecutaQuery_ADO ssql
    End If
    
    'soliveira lacto **
    'NELSONPSILVA GLINTT-HS-21312 Adicionado -> quantidade
    ssql = " SELECT sl_requis.dt_chega,sl_requis.dt_previ, sl_efr.descr_efr descr_efr, sl_identif.nome_ute, "
    ssql = ssql & " slv_analises_factus.cod_ana, descr_ana, taxa, sl_requis.n_benef n_benef_req, sl_recibos_det.cod_ana_efr, sl_recibos.dt_emi, "
    ssql = ssql & " sl_recibos.user_emi ,  sl_recibos.venda_dinheiro, sl_recibos_det.n_benef  n_benef_rec, sl_recibos.desconto, "
    ssql = ssql & " sl_recibos.cod_empresa, sl_empresas.nome_empresa, sl_empresas.num_contribuinte, sl_recibos_det.cod_facturavel, "
    ssql = ssql & " sl_recibos.valor_original, sl_efr.num_copias_rec, sl_empresas.cabecalho, sl_recibos.total_pagar, sl_recibos.flg_doc_caixa, "
    ssql = ssql & " sl_identif.N_CONTRIB_UTE, sl_identif.descr_mor_ute, sl_identif.cod_postal_ute, sl_cod_postal.descr_postal, sl_recibos_det.quantidade"
    'GX-62545
    ssql = ssql & ", sl_recibos.ATCUD "
    '
    ssql = ssql & " FROM SL_RECIBOS, sl_requis, sl_efr, sl_identif LEFT OUTER JOIN sl_cod_postal ON sl_identif.cod_postal_ute = sl_Cod_postal.cod_postal,"
    ssql = ssql & " slv_analises_factus, sl_recibos_det, sl_empresas "
    ssql = ssql & " WHERE  sl_recibos.n_rec = " & NumDoc
    ssql = ssql & " AND  sl_recibos.serie_rec = " & BL_TrataStringParaBD(serieRec)
    ssql = ssql & " AND sl_recibos.serie_rec = sl_recibos_det.serie_rec "
    ssql = ssql & " AND sl_recibos.n_req = sl_requis.n_req and sl_recibos.cod_efr = sl_efr.cod_efr "
    If seq_utente = -1 Then
        ssql = ssql & " AND sl_identif.seq_utente = sl_requis.seq_utente "
    Else
        ssql = ssql & " AND sl_identif.seq_utente = sl_recibos.seq_utente "
    End If
    ssql = ssql & " AND sl_recibos_det.n_rec = sl_recibos.n_rec "
    ssql = ssql & " AND slv_analises_factus.cod_ana = sl_Recibos_det.cod_facturavel and seq_sinonimo is null "
    If codEfr <> gEfrParticular Then
        ssql = ssql & " AND sl_recibos_det.isencao = " & gTipoIsencaoNaoIsento
    End If
    ssql = ssql & " AND sl_recibos.cod_empresa = sl_empresas.cod_empresa "
    ssql = ssql & " AND sl_recibos_det.flg_adicionada = 0 "
    ssql = ssql & " ORDER by ordem_marcacao "
    
    Set rsRecibos = New adodb.recordset
    rsRecibos.CursorType = adOpenStatic
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao

    If rsRecibos.RecordCount > 0 Then
        
        'GX-62545
        If Bg_DaData_ADO >= gFactQrCodeDate Then
            Dim qrCodeStr As String
            qrCodeStr = BillQRCode_GenerateQRcodeString(serieRec, NumDoc)
            
            If qrCodeStr = "-1" Then
                BG_Mensagem mediMsgBox, "Erro ao Gerar String QR Code para Report!", vbExclamation + vbOKOnly, "Recibos"
                Exit Sub
            Else
                If BL_HandleNull(qrCodeStr, "") <> "" Then
                   If BillingQRcode.ManageBillingQRCode(Trim(qrCodeStr), n_req) = False Then
                        Exit Sub
                   End If
                End If
            End If
        End If
        '
        
        num_recibo = NumDoc
        'data = BL_HandleNull(rsRecibos!dt_chega, rsRecibos!dt_previ)
        data = BL_HandleNull(rsRecibos!dt_emi, Bg_DaData_ADO)
        DataChega = BL_HandleNull(rsRecibos!dt_chega, rsRecibos!dt_previ)
        vd = BL_HandleNull(rsRecibos!venda_dinheiro, "")
        efr = BL_HandleNull(rsRecibos!descr_efr, "")
        nome = BL_HandleNull(rsRecibos!nome_ute, "")
        num_benef = BL_HandleNull(rsRecibos!n_benef_rec, BL_HandleNull(rsRecibos!n_benef_req, ""))
        ult_analise = ""
        analises = ""
        total = 0
        user_emi = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", rsRecibos!user_emi)
        cod_empresa = BL_HandleNull(rsRecibos!cod_empresa, "")
        nome_empresa = BL_HandleNull(rsRecibos!nome_empresa, "")
        total_original = BL_HandleNull(rsRecibos!valor_original, 0)
        totalPagar = BL_HandleNull(rsRecibos!total_pagar, 0)
        flgDocCaixa = BL_HandleNull(rsRecibos!flg_doc_caixa, "0")
        If BL_HandleNull(rsRecibos!n_contrib_ute, "") <> "" Then
            If Not IsNumeric(BL_HandleNull(rsRecibos!n_contrib_ute, "")) Then
                n_contrib_ute = "null"
            Else
                n_contrib_ute = BL_HandleNull(rsRecibos!n_contrib_ute, "")
            End If
        Else
            n_contrib_ute = "null"
        End If
        Desconto = BL_HandleNull(rsRecibos!Desconto, 0)
        num_copias_rec = BL_HandleNull(rsRecibos!num_copias_rec, 0)
        morada = BL_HandleNull(rsRecibos!descr_mor_ute, "")
        cod_postal = BL_HandleNull(rsRecibos!cod_postal_ute, "")
        descr_postal = BL_HandleNull(rsRecibos!descr_postal, "")
        
        If gDEMO = mediSim Then
            Certificado = ""
            Formacao = "Documento emitido para fins de formação"
            cabecalho = "GLINTT HS"
        Else
            Certificado = RECIBO_RetornaCertificado(num_recibo, serieRec)
            Formacao = ""
            cabecalho = BL_HandleNull(rsRecibos!cabecalho, "")
            num_contribuinte = BL_HandleNull(rsRecibos!num_contribuinte, "")
        End If
        
        While Not rsRecibos.EOF
            If ult_analise <> rsRecibos!cod_facturavel Then
                
                '----
                'soliveira lacto para situacoes multicare e medis aplica o desconto em cada analise
                If Desconto < 0 Then
'                    taxa_ana = Round(rsRecibos!Taxa - (rsRecibos!Taxa * (RegistosR(indice).Desconto / 100)), 2)
                    taxa_ana = "---"
                ElseIf Desconto > 0 And gControlaDesconto <> mediSim Then
                    taxa_ana = "---"
                Else
                    taxa_ana = BL_HandleNull(rsRecibos!taxa, 0)
                    If InStr(1, taxa_ana, ",") = 0 Then
                        taxa_ana = taxa_ana & ",0"
                    ElseIf Len(Mid(taxa_ana, InStr(1, taxa_ana, ",") + 1)) = 1 Then
                        taxa_ana = taxa_ana & "0"
                    End If
                End If
                'NELSONPSILVA GLINTT-HS-21312
                qtd = BL_HandleNull(rsRecibos!quantidade, 1)
                '
                If rsRecibos!cod_facturavel <> rsRecibos!cod_ana And Mid(rsRecibos!cod_facturavel, 1, 1) = "P" Then
                    descr_analise = BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", rsRecibos!cod_facturavel, "V")
                Else
                    descr_analise = rsRecibos!descr_ana
                End If
                analises = analises & Replace(Mid(BL_HandleNull(rsRecibos!cod_ana_efr, "") & "-" & descr_analise, 1, 26), Chr(160), "") & Space(26 - Len(Mid(Trim(BL_HandleNull(rsRecibos!cod_ana_efr, "") & "-" & descr_analise), 1, 26))) & "  1    " & taxa_ana & vbCrLf
                total = total + CDbl(BL_HandleNull(rsRecibos!taxa, 0))
                ult_analise = rsRecibos!cod_facturavel
                If gReciboCrystal = 1 Then
                    ssql = "INSERT INTO sl_cr_recibo(n_req, num_recibo, efr, data, nome, num_benef, " & _
                            "cod_ana_efr, analise, taxa_ana, qtd, nome_computador, cod_empresa, nome_empresa, num_contribuinte," & _
                            "cabecalho,num_sessao, flg_doc_caixa, n_contrib_ute, morada, cod_postal, descr_postal,atcud) VALUES (" & _
                            n_req & "," & num_recibo & "," & BL_TrataStringParaBD(efr) & "," & _
                            BL_TrataStringParaBD(data) & "," & BL_TrataStringParaBD(nome) & "," & _
                            BL_TrataStringParaBD(num_benef) & "," & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!cod_ana_efr, "")) & "," & _
                            BL_TrataStringParaBD(rsRecibos!descr_ana) & "," & BL_TrataStringParaBD(taxa_ana) & "," & _
                            BL_TrataStringParaBD(BL_HandleNull(rsRecibos!quantidade, 1)) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & _
                            BL_TrataStringParaBD(cod_empresa) & "," & BL_TrataStringParaBD(nome_empresa) & "," & BL_TrataStringParaBD(BL_HandleNull(num_contribuinte, "")) & "," & _
                            BL_TrataStringParaBD(cabecalho) & "," & gNumeroSessao & "," & flgDocCaixa & ", " & n_contrib_ute & "," & _
                            BL_TrataStringParaBD(morada) & ", " & BL_TrataStringParaBD(cod_postal) & ", " & BL_TrataStringParaBD(descr_postal) & "," & BL_TrataStringParaBD(BL_HandleNull(rsRecibos!ATCUD, "")) & ")"
                            
                    BG_ExecutaQuery_ADO ssql
                    If gSQLError <> 0 Then
                        BG_Mensagem mediMsgBox, "Erro a inserir análise no recibo!", vbExclamation + vbOKOnly, "Recibos"
                        Exit Sub
                    End If
                End If
            End If
            rsRecibos.MoveNext
        Wend
        'soliveira lacto em situacoes que ha desconto só apresenta o valor do total a pagar
        If Desconto > 0 Or efr = "MULTICARE" Or efr = "MEDIS" Then
            total = totalPagar
        ElseIf total <> totalPagar Then
            BG_LogFile_Erros "Valor no recibo: " & total & " <> do valor calculado " & totalPagar & " na Requisicao: " & n_req
            total = totalPagar
        End If
        If InStr(1, total, ",") = 0 Then
            Tot = total & ",0"
        ElseIf Len(Mid(total, InStr(1, total, ",") + 1)) = 1 Then
            Tot = total & "0"
        Else
            Tot = total
        End If

               
        If gReciboCrystal = "1" Then
            For i = 0 To num_copias_rec
                If i > 0 And descricao = "" Then
                    descricao = "C�PIA"
                End If
                'Imprime recibo por crystal
                If BL_PreviewAberto("Recibo") = True Then Exit Sub
                continua = BL_IniciaReport("ReciboReq", "Recibo", crptToPrinter, False)
                If continua = False Then Exit Sub
    
                'Imprime o relatório no Crystal Reports
                Dim Report As CrystalReport
                Set Report = forms(0).Controls("Report")
                'Fórmulas do Report
                Report.formulas(0) = "TotalPagar=" & BL_TrataStringParaBD("" & CStr(Tot))
                Report.formulas(1) = "TotalPagarExtenso=" & BL_TrataStringParaBD("" & LCase(BL_Preco_EURO_Extenso(CDbl(Tot))))
                Report.formulas(2) = "TipoRec=" & BL_TrataStringParaBD("" & serieRec)
                Report.formulas(3) = "Descricao=" & BL_TrataStringParaBD("" & descricao)
                Report.formulas(4) = "TotalOriginal=" & BL_TrataStringParaBD("" & total_original)
                Report.formulas(5) = "Desconto=" & BL_TrataStringParaBD("" & Desconto)
                Report.formulas(6) = "Formacao=" & BL_TrataStringParaBD("" & Formacao)
                Report.formulas(7) = "Certificado=" & BL_TrataStringParaBD("" & Certificado)
                
                Report.SQLQuery = " SELECT sl_cr_recibo.n_req, sl_cr_recibo.num_recibo, sl_cr_recibo.efr, sl_cr_recibo.data, sl_cr_recibo.nome, sl_cr_recibo.num_benef,"
                Report.SQLQuery = Report.SQLQuery & " sl_cr_recibo.cod_ana_efr , sl_cr_recibo.analise, sl_cr_recibo.taxa_ana, sl_cr_recibo.qtd, "
                Report.SQLQuery = Report.SQLQuery & " sl_cr_recibo.nome_computador, sl_cr_recibo.cod_empresa, sl_cr_recibo.nome_empresa, sl_cr_recibo.num_contribuinte,"
                Report.SQLQuery = Report.SQLQuery & " sl_cr_recibo.cabecalho,sl_cr_recibo.num_sessao,sl_cr_recibo.flg_doc_caixa,sl_cr_recibo.n_contrib_ute "
                Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_recibo WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
                Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
                Report.Connect = "DSN=" & gDSN
                
                Call BL_ExecutaReport
                
                
            Next

        Else
            For i = 0 To num_copias_rec
                If i > 0 And descricao = "" Then
                    descricao = "CÓPIA"
                End If
                'Imprime recibo por ficheiro
                If flgDocCaixa = 1 Then
                    If Not BL_LerEtiqIni("DOCCAIXA.INI") Then
                        MsgBox "Ficheiro de inicialização de documentos de caixa ausente!"
                        Exit Sub
                    End If
                Else
                    If Not BL_LerEtiqIni("RECIBOS" & "_" & codEfr & ".INI") Then
                        If Not BL_LerEtiqIni("RECIBOS.ini") Then
                            MsgBox "Ficheiro de inicializaçãoo de recibos ausente!"
                            Exit Sub
                        End If
                    End If
                End If
                If Not BL_EtiqOpenPrinter(impressora) Then
                    MsgBox "Impossível abrir impressora recibos"
                    Exit Sub
                End If
                
                Call RECIBO_EtiqPrint(serieRec, num_recibo, data, n_req, efr, nome, num_benef, analises, CStr(Tot), "", _
                                      user_emi, descricao, DataChega, vd, cod_empresa, nome_empresa, num_contribuinte, _
                                      total_original, Desconto, cabecalho, Formacao, Certificado, "", "", n_contrib_ute, morada, _
                                      cod_postal, descr_postal)
        
                BL_EtiqClosePrinter
            Next
        End If
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
        
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_ImprimeRecibo: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_ImprimeRecibo"
    Exit Sub
    Resume Next
End Sub



Public Function RECIBO_EtiqPrint(serie_rec As String, num_recibo As String, data As String, n_req As String, efr As String _
                           , nome As String, num_benef As String, analises As String, _
                           total As String, num_anulacao As String, user_emi As String, descricao As String, dt_chega As String, _
                           vd As String, cod_empresa As String, nome_empresa As String, num_contribuinte As String, _
                           total_original As Double, Desconto As Double, cabecalho As String, Formacao As String, Certificado As String, _
                           dataAnul As String, HrAnul As String, n_contrib_ute As String, morada As String, cod_postal As String, descr_postal As String)
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    On Error GoTo TrataErro


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{SERIE_REC}", serie_rec)
    sWrittenData = Replace(sWrittenData, "{NUM_RECIBO}", num_recibo)
    sWrittenData = Replace(sWrittenData, "{DATA}", data)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{EFR}", efr)
    sWrittenData = Replace(sWrittenData, "{NOME}", BL_RemovePortuguese(nome))
    sWrittenData = Replace(sWrittenData, "{NUM_BENEF}", BL_RemovePortuguese(num_benef))
    sWrittenData = Replace(sWrittenData, "{ANALISES}", BL_RemovePortuguese(analises))
    sWrittenData = Replace(sWrittenData, "{TOTAL}", total)
    sWrittenData = Replace(sWrittenData, "{NUM_ANULACAO}", num_anulacao)
    sWrittenData = Replace(sWrittenData, "{USER_EMI}", user_emi)
    sWrittenData = Replace(sWrittenData, "{DESCRICAO}", BL_RemovePortuguese(descricao))
    sWrittenData = Replace(sWrittenData, "{VD}", vd)
    sWrittenData = Replace(sWrittenData, "{DT_CHEGA}", dt_chega)
    sWrittenData = Replace(sWrittenData, "{COD_EMPRESA}", cod_empresa)
    sWrittenData = Replace(sWrittenData, "{NOME_EMPRESA}", BL_RemovePortuguese(nome_empresa))
    sWrittenData = Replace(sWrittenData, "{NUM_CONTRIBUINTE}", num_contribuinte)
    sWrittenData = Replace(sWrittenData, "{TOTAL_ORIGINAL}", total_original)
    sWrittenData = Replace(sWrittenData, "{DESCONTO}", Desconto)
    sWrittenData = Replace(sWrittenData, "{CABECALHO}", cabecalho)
    sWrittenData = Replace(sWrittenData, "{FORMACAO}", Formacao)
    sWrittenData = Replace(sWrittenData, "{CERTIFICADO}", Replace(Certificado, "º", "§"))
    sWrittenData = Replace(sWrittenData, "{DATA_ANUL}", dataAnul)
    sWrittenData = Replace(sWrittenData, "{HR_ANUL}", HrAnul)
    sWrittenData = Replace(sWrittenData, "{N_CONTRIB_UTE}", n_contrib_ute)
    sWrittenData = Replace(sWrittenData, "{MORADA}", morada)
    sWrittenData = Replace(sWrittenData, "{COD_POSTAL}", cod_postal)
    sWrittenData = Replace(sWrittenData, "{DESCR_POSTAL}", descr_postal)
    If gModoDebug = mediSim Then BG_LogFile_Erros sWrittenData
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    RECIBO_EtiqPrint = True
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_EtiqPrint: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_EtiqPrint"
    RECIBO_EtiqPrint = False
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------------------------------------------------------------

' ANULACAO DO RECIBO

' --------------------------------------------------------------------------------------------------------------
Public Function RECIBO_Anulacao(numAnulacao As Long, linha As Long, n_req As String, tCanc As String, obsAnul As String, _
                                ByRef RegistosA As Collection, ByRef RegistosR As Collection, ByRef RegistosRM As Collection) As Boolean
    On Error GoTo TrataErro
    Dim i As Long
    Dim ssql As String
    Dim resCabecalho As Boolean
    Dim resDetalhe As Boolean
    Dim resRecibos As Boolean
    Dim rsSQL As New adodb.recordset
    RECIBO_Anulacao = False
    
    BG_BeginTransaction
    If RegistosR(linha).Estado = gEstadoReciboCobranca Then
        RegistosR(linha).DtPagamento = Bg_DaData_ADO
        RegistosR(linha).HrPagamento = Bg_DaHora_ADO
    End If
    
    resCabecalho = RECIBO_InsereCabecalhoAnul(linha, CLng(RegistosR(linha).NumDoc), RegistosR(linha).SerieDoc, _
                                               tCanc, obsAnul, CLng(n_req), numAnulacao, RegistosR(linha).DtPagamento, RegistosR(linha).HrPagamento)
    If resCabecalho = False Then
        GoTo TrataErro
    End If
    ' ------------------------------------------------------------------------------------------
    ' VOLTA A COLOCAR AS ANALISES DO DOCUMENTO EM CAUSA POR EMITIR RECIBO
    ' ------------------------------------------------------------------------------------------
    If RegistosR(linha).ValorPagar <> "0" And RegistosR(linha).Caucao = "0" Then
        
        RegistosR(linha).Estado = gEstadoReciboAnulado
        BG_LogFile_Erros "Vai anular recibo:" & RegistosR(linha).NumDoc & " REQ:" & n_req, "RECIBO", "ANULACAO", False
        resDetalhe = RECIBO_InsereDetalheAnul(CLng(n_req), numAnulacao, CLng(RegistosR(linha).NumDoc), RegistosR(linha).SerieDoc)
        If resDetalhe = False Then
            GoTo TrataErro
        End If
        
        For i = 1 To RegistosRM.Count
            If i <= RegistosRM.Count Then
                If CLng(BL_HandleNull(RegistosRM(i).ReciboNumDoc, 0)) = CLng(RegistosR(linha).NumDoc) Then
                    If BL_HandleNull(RegistosRM(i).ReciboFlgMarcacaoRetirada, "0") = "0" Then
                        RegistosRM(i).ReciboNumDoc = "0"
                        RegistosRM(i).ReciboSerieDoc = "0"
                        RegistosRM(i).ReciboUserCri = ""
                        RegistosRM(i).ReciboDtCri = ""
                        RegistosRM(i).ReciboHrCri = ""
                        RegistosRM(i).ReciboUserEmi = ""
                        RegistosRM(i).ReciboDtEmi = ""
                        RegistosRM(i).ReciboHrEmi = ""
                        RegistosRM(i).ReciboFlgImpresso = 0
                        RegistosRM(i).ReciboCodMedico = ""
                        RegistosRM(i).ReciboFDS = "0"
                        RegistosRM(i).ReciboCodigoPai = ""
                        RECIBO_AdicionaAnaliseAoReciboManual i, True, RegistosA, RegistosR, RegistosRM
                    Else
                        RECIBO_LimpaColeccao RegistosRM, i
                        i = i - 1
                    End If
                End If
            End If
        Next
    End If
    resRecibos = RECIBO_GravaRecibo(n_req, RegistosA, RegistosR, RegistosRM, False)
    If resRecibos = False Then
        GoTo TrataErro
    End If
    
    RECIBO_Anulacao = True
    BG_CommitTransaction
Exit Function
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "RECIBO_Anulacao: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_Anulacao"
    RECIBO_Anulacao = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' QUANDO ADICIONA UMA NOVA ANALISE, ADICIONA TAMBEM RESPECTIVA TAXA NA ESTRUTURA DE RECIBOS

' -----------------------------------------------------------------------------------------------

Private Sub RECIBO_AdicionaAnaliseAoReciboManual(indice As Long, MudaEstadoPerdidas As Boolean, ByRef RegistosA As Collection, _
                                  ByRef RegistosR As Collection, ByRef RegistosRM As Collection)
    On Error GoTo TrataErro
    Dim i As Long
    Dim Flg_inseriuEntidade As Boolean
    Dim Flg_EntidadeJaEmitida As Boolean
    Flg_inseriuEntidade = False
    
    ' ------------------------------------------------------------------------------------------
    ' SE TAXA FOR 0 NAO E NECESSARIO ACRESCENTAR
    ' ------------------------------------------------------------------------------------------

    If RegistosRM(indice).ReciboCodFacturavel <> RegistosRM(1).ReciboCodFacturavel Then
        For i = 1 To indice
            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(indice).ReciboCodFacturavel Then
                If RegistosRM(i).ReciboCodFacturavel <> RegistosRM(indice).ReciboCodFacturavel Then
                    Exit Sub
                End If
            End If
        Next
    End If
    
    If RegistosR.Count = 0 Then
        RECIBO_CriaNovaClasse RegistosR, i + 1, "REC"
    End If
    Flg_EntidadeJaEmitida = False
    For i = 1 To RegistosR.Count
        ' ------------------------------------------------------------------------------------------
        ' ENTIDADE JA EXISTE NA ESTRUTURA
        ' ------------------------------------------------------------------------------------------
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboNulo Or (RegistosR(i).Estado = gEstadoReciboPerdido And MudaEstadoPerdidas = True) Then
                RegistosR(i).ValorPagar = RegistosR(i).ValorPagar + Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2)
                RegistosR(i).ValorOriginal = RegistosR(i).ValorOriginal + RegistosRM(indice).ReciboTaxa
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                If RegistosR(i).ValorPagar > 0 Then
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                ElseIf RegistosR(i).ValorPagar = 0 Then
                    RegistosR(i).Estado = gEstadoReciboNulo
                End If
                Flg_inseriuEntidade = True
                If RegistosR(i).ValorPagar > 0 Then
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(i).Estado = gEstadoReciboNulo
                End If
                If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
                    RegistosR(i).FlgBorla = mediSim
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
                RegistosR(i).ValorPagar = (RegistosR(i).ValorPagar) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).ValorOriginal = (RegistosR(i).ValorOriginal) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                Flg_inseriuEntidade = True
                Flg_EntidadeJaEmitida = True
                If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
                    RegistosR(i).FlgBorla = mediSim
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca Then
                Flg_EntidadeJaEmitida = True
            End If
        End If
    Next
    i = i - 1
    ' ------------------------------------------------------------------------------------------
    ' ENTIDADE NAO EXISTE NA ESTRUTURA
    ' ------------------------------------------------------------------------------------------
    If Flg_inseriuEntidade = False Then
        RegistosR(i).SeqRecibo = RECIBO_RetornaSeqRecibo
        RegistosR(i).NumDoc = 0
        RegistosR(i).SerieDoc = "0"
        RegistosR(i).codEntidade = RegistosRM(indice).ReciboEntidade
        RegistosR(i).DescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).codEmpresa = BL_SelCodigo("SL_EFR", "cod_empresa", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).Caucao = 0
        RegistosR(i).UserEmi = ""
        RegistosR(i).DtEmi = ""
        RegistosR(i).HrEmi = ""
        RegistosR(i).flg_impressao = False
        RegistosR(i).DtPagamento = ""
        RegistosR(i).HrPagamento = ""
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            RegistosR(i).Estado = gEstadoReciboNaoEmitido
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            RegistosR(i).Estado = gEstadoReciboNulo
        ElseIf Flg_EntidadeJaEmitida = True Then
            RegistosR(i).Estado = gEstadoReciboPerdido
        End If
        RegistosR(i).NumAnalises = 1
        RegistosR(i).flg_DocCaixa = 0
        If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
            RegistosR(i).FlgBorla = mediSim
        Else
            RegistosR(i).FlgBorla = mediNao
        End If
        RegistosR(i).FlgNaoConforme = 0
        
        RECIBO_CriaNovaClasse RegistosR, i + 1, "REC"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_AdicionaAnaliseAoReciboManual: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_AdicionaAnaliseAoReciboManual"
    Exit Sub
    Resume Next
End Sub

Private Sub RECIBO_CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)
    On Error GoTo TrataErro

    ' Tipo = ANA ou PROD
    ' EmCascata = True : Cria todos elementos até o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxAdiantamento As New ClAdiantamentos
    Dim AuxRegEnt As New ClRegRecibos
    Dim AuxRegProd As New ClRegProdutos
    Dim AuxRegAna As New ClRegAnalises
    Dim AuxRegTubo As New ClRegTubos
    Dim AuxRegRecManuais As New ClRegRecibosAna
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "PROD" Then
        If Cascata = True Then
            RECIBO_LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegProd
            Next i
        Else
            Coleccao.Add AuxRegProd
        End If
    ElseIf tipo = "TUBO" Then
        If Cascata = True Then
            RECIBO_LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegTubo
            Next i
        Else
            Coleccao.Add AuxRegTubo
        End If
    ElseIf tipo = "ADI" Then
        If Cascata = True Then
            RECIBO_LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxAdiantamento
            Next i
        Else
            Coleccao.Add AuxAdiantamento
        End If
    ElseIf tipo = "REC" Then
        If Cascata = True Then
            RECIBO_LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegEnt
            Next i
        Else
            Coleccao.Add AuxRegEnt
        End If
    ElseIf tipo = "REC_MAN" Then
        If Cascata = True Then
            RECIBO_LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegRecManuais
            Next i
        Else
            Coleccao.Add AuxRegRecManuais
        End If
    Else
        If Cascata = True Then
            RECIBO_LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegAna
            Next i
        Else
            Coleccao.Add AuxRegAna
        End If
    End If
    
    Set AuxRegAna = Nothing
    Set AuxRegProd = Nothing
    Set AuxRegTubo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_CriaNovaClasse: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_CriaNovaClasse"
    Exit Sub
    Resume Next
    
End Sub


' ----------------------------------------------------------------------

' PREENCHE ALGUNS DADOS DA ENTIDADE COM APENAS UM ACESSO À BD

' ----------------------------------------------------------------------
Private Sub RECIBO_PreencheDadosEntidade(ByRef RegistosR As Collection, indice As Long)
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    On Error GoTo TrataErro
    
    ssql = "SELECT * FROM sl_efr where cod_efr = " & RegistosR(indice).codEntidade
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsEFR.Open ssql, gConexao
    If RsEFR.RecordCount > 0 Then
        RegistosR(indice).DescrEntidade = BL_HandleNull(RsEFR!descr_efr, "")
        RegistosR(indice).codEmpresa = BL_HandleNull(RsEFR!cod_empresa, "")
        RegistosR(indice).flg_FdsFixo = BL_HandleNull(RsEFR!flg_fds_fixo, 0)
        RegistosR(indice).NumCopias = BL_HandleNull(RsEFR!num_copias_rec, 0)
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_PreencheDadosEntidade: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_PreencheDadosEntidade"
    Exit Sub
    Resume Next
End Sub

Private Sub RECIBO_LimpaColeccao(Coleccao As Collection, Index As Long)
    On Error GoTo TrataErro

    ' Index = -1 : Apaga todos elementos
    
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        If Index <= Coleccao.Count Then
            Coleccao.Remove Index
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_LimpaColeccao: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_LimpaColeccao"
    Exit Sub
    Resume Next

End Sub


' ------------------------------------------------------------------------------------------

' INSERE LINHA NA TABELA SL_RECIBOS_CANC

' ------------------------------------------------------------------------------------------
Private Function RECIBO_InsereCabecalhoAnul(linha As Long, NumDoc As Long, SerieDoc As String, tCanc As String, _
                                            obsCanc As String, n_req As Long, numAnulacao As Long, dt_pagamento As String, hr_pagamento As String) As Boolean
    On Error GoTo TrataErro
    Dim utilizadorCanc As String
    Dim dataCanc As String
    Dim horaCanc As String
    Dim ssql As String
    
    
    RECIBO_InsereCabecalhoAnul = False
    
    ' ------------------------------------------------------------------------------------------
    ' ANULA O RECIBO EM CAUSA
    ' ------------------------------------------------------------------------------------------
    utilizadorCanc = gCodUtilizador
    dataCanc = Bg_DaData_ADO
    horaCanc = Bg_DaHora_ADO
    
    gSQLError = 0
    ssql = "INSERT INTO SL_RECIBOS_CANC ( n_rec, serie_rec, dt_canc, hr_canc, t_canc, obs_canc, user_canc, n_req, n_anulacao) VALUES ("
    ssql = ssql & CLng(NumDoc) & ", "
    ssql = ssql & BL_TrataStringParaBD(SerieDoc) & ", "
    ssql = ssql & BL_TrataDataParaBD(dataCanc) & ", "
    ssql = ssql & BL_TrataStringParaBD(horaCanc) & ", "
    ssql = ssql & tCanc & ", "
    ssql = ssql & BL_TrataStringParaBD(obsCanc) & ", "
    ssql = ssql & BL_TrataStringParaBD(utilizadorCanc) & ", "
    ssql = ssql & n_req & ", "
    ssql = ssql & numAnulacao & ") "
    BG_ExecutaQuery_ADO ssql
    BG_LogFile_Erros ssql, "RECIBO", "RECIBO_InsereCabecalhoAnul", False
    If gSQLError <> 0 Then
        GoTo TrataErro
    End If
    
    If NumDoc <> "0" Then
        ssql = "UPDATE sl_recibos SET estado = 'A', n_anulacao =  " & numAnulacao & ", dt_pagamento = " & BL_TrataDataParaBD(dt_pagamento)
        ssql = ssql & ", hr_pagamento = " & BL_TrataStringParaBD(hr_pagamento)
        ssql = ssql & " WHERE n_rec = " & NumDoc & " AND serie_rec = " & BL_TrataStringParaBD(SerieDoc)
        BG_LogFile_Erros ssql, "RECIBO", "RECIBO_InsereCabecalhoAnul2", False
    End If
    BG_ExecutaQuery_ADO ssql
    If gSQLError <> 0 Then
        GoTo TrataErro
    End If
    
    RECIBO_InsereCabecalhoAnul = True
    Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_InsereCabecalhoAnul: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_InsereCabecalhoAnul"
    RECIBO_InsereCabecalhoAnul = False
    Exit Function
    Resume Next
End Function

Private Function RECIBO_InsereDetalheAnul(n_req As Long, numAnulacao As Long, NumDoc As Long, SerieDoc As String) As Boolean
    Dim rsSQL As New adodb.recordset
    Dim ssql As String
    
    On Error GoTo TrataErro
    RECIBO_InsereDetalheAnul = False
    
    ssql = "SELECT * from sl_recibos_det WHERE n_rec = " & CLng(NumDoc) & " and serie_rec = " & BL_TrataStringParaBD(SerieDoc)
    rsSQL.CursorType = adOpenStatic
    rsSQL.CursorLocation = adUseServer
    rsSQL.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsSQL.Open ssql, gConexao
    'NELSONPSILVA GLINTT-HS-21312 Adicionado -> quantidade
    If rsSQL.RecordCount > 0 Then
        While Not rsSQL.EOF
            ssql = "INSERT INTO sl_recibos_det_canc (n_req,n_rec,serie_rec, cod_efr,cod_ana,cod_facturavel,p1,taxa,user_cri,"
            ssql = ssql & " dt_cri,hr_cri,user_emi,dt_emi,hr_emi,flg_impresso, flg_facturado,flg_retirado, "
            ssql = ssql & " flg_adicionada, isencao,  ordem_marcacao, cor_p1, n_anulacao, cod_ana_efr, fds, codigo_pai, "
            ssql = ssql & " flg_fds_fixo, descr_ana_efr, flg_cod_barras, Seq_recibo, quantidade) VALUES( "
            ssql = ssql & n_req & ", "
            ssql = ssql & rsSQL!n_rec & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!serie_rec, "")) & ", "
            ssql = ssql & BL_HandleNull(rsSQL!cod_efr, "-1") & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!cod_ana, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!cod_facturavel, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!p1, "1")) & ", "
            ssql = ssql & Replace(BL_HandleNull(rsSQL!taxa, ""), ",", ".") & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(BL_HandleNull(rsSQL!user_cri, ""))) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsSQL!dt_cri, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!hr_cri, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(BL_HandleNull(rsSQL!user_emi, ""))) & ", "
            ssql = ssql & BL_TrataDataParaBD(BL_HandleNull(rsSQL!dt_emi, "")) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!hr_emi, "")) & ", "
            ssql = ssql & BL_HandleNull(rsSQL!flg_impresso, "0") & ", "
            ssql = ssql & BL_HandleNull(rsSQL!Flg_Facturado, "0") & ", "
            ssql = ssql & BL_HandleNull(rsSQL!flg_retirado, "0") & ", "
            ssql = ssql & BL_HandleNull(rsSQL!Flg_adicionada, "0") & ", "
            ssql = ssql & BL_HandleNull(rsSQL!isencao, "0") & ", "
            ssql = ssql & BL_HandleNull(rsSQL!ordem_marcacao, "0") & " , "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!cor_p1, "B")) & ", "
            ssql = ssql & numAnulacao & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!cod_ana_efr, "")) & ", "
            ssql = ssql & BL_HandleNull(rsSQL!fds, "0") & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!codigo_pai, "")) & ", "
            ssql = ssql & BL_HandleNull(rsSQL!flg_fds_fixo, 0) & ", "
            ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(rsSQL!descr_ana_efr, "")) & ", "
            ssql = ssql & BL_HandleNull(rsSQL!flg_cod_barras, 0) & ", "
            ssql = ssql & BL_HandleNull(rsSQL!seq_Recibo, mediComboValorNull) & ", "
            ssql = ssql & BL_HandleNull(rsSQL!quantidade, 1) & ") "
            BG_LogFile_Erros ssql, "RECIBO", "RECIBO_InsereDetalheAnul", False
            BG_ExecutaQuery_ADO ssql
            
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            rsSQL.MoveNext
        Wend
        rsSQL.Close
        Set rsSQL = Nothing
    
        ssql = "DELETE FROM SL_RECIBOS_DET WHERE n_req =" & n_req & " AND n_rec = " & CLng(NumDoc)
        ssql = ssql & " AND serie_rec = " & BL_TrataStringParaBD(SerieDoc)
        BG_ExecutaQuery_ADO ssql
        
        If gSQLError <> 0 Then
            GoTo TrataErro
        End If
    Else
        GoTo TrataErro
    End If
    RECIBO_InsereDetalheAnul = True
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_InsereDetalheAnul: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_InsereDetalheAnul"
    RECIBO_InsereDetalheAnul = False
    Exit Function
    Resume Next
End Function

Public Sub RECIBO_ImprimeAnulRecibo(numAnulacao As Long, ByRef RegistosR As Collection, indice As Long, _
                                    n_req As String, impressora As String)
    On Error GoTo TrataErro
    Dim continua As Boolean
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim num_recibo As String
    Dim serie_rec As String
    Dim data As String
    Dim dataAnul As String
    Dim HrAnul As String
    Dim n_contrib_ute As String
    Dim efr As String
    Dim nome As String
    Dim num_benef As String
    Dim analises As String
    Dim ult_analise As String
    Dim total As Double
    Dim Tot As String
    Dim taxa_ana As String
    Dim user_emi As String
    Dim DataChega As String
    Dim vd As String
    Dim Formacao As String
    Dim cabecalho As String
    Dim num_contribuinte As String
    Dim Certificado As String
    Dim qtd As Integer
    
    ssql = " SELECT sl_requis.dt_chega,sl_requis.dt_previ, sl_efr.descr_efr, sl_identif.nome_ute, "
    ssql = ssql & " slv_analises.cod_ana, descr_ana, taxa,sl_requis.n_benef n_benef_req, sl_recibos.dt_emi, "
    ssql = ssql & " sl_recibos.user_emi ,  sl_recibos.venda_dinheiro,sl_recibos_det_canc.n_benef n_benef_rec,"
    ssql = ssql & " sl_empresas.cabecalho, sl_empresas.num_contribuinte, sl_recibos_canc.dt_canc, sl_recibos_canc.hr_canc, sl_identif.n_contrib_ute, sl_recibos_det_canc.quantidade "
    ssql = ssql & " FROM SL_RECIBOS, sl_requis, sl_efr, sl_identif, slv_analises_factus slv_analises,"
    ssql = ssql & " sl_recibos_det_canc,sl_empresas, sl_recibos_canc "
    ssql = ssql & " WHERE  sl_recibos.n_rec = " & RegistosR(indice).NumDoc
    ssql = ssql & " AND sl_recibos.n_req = sl_requis.n_req and sl_requis.cod_efr = sl_efr.cod_efr "
    ssql = ssql & " AND sl_identif.seq_utente = sl_requis.seq_utente AND sl_recibos_det_canc.n_rec = sl_recibos.n_rec "
    ssql = ssql & " AND sl_recibos.serie_rec = sl_recibos_det_canc.serie_rec "
    ssql = ssql & " AND sl_recibos.serie_rec =  " & BL_TrataStringParaBD(RegistosR(indice).SerieDoc)
    ssql = ssql & " AND sl_recibos.cod_empresa = sl_empresas.cod_empresa "
    ssql = ssql & " AND sl_recibos.serie_rec = sl_recibos_canc.serie_rec "
    ssql = ssql & " AND sl_recibos.n_rec = sl_recibos_canc.n_rec "
    
    'sSql = sSql & " AND slv_analises.cod_ana = sl_Recibos_det.cod_facturavel ORDER by descr_ana "
    'soliveira lacto
    ssql = ssql & " AND slv_analises.cod_ana = sl_Recibos_det_canc.cod_ana and seq_sinonimo is null ORDER by ordem_marcacao "
    
    rsRecibos.CursorType = adOpenStatic
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao

    If rsRecibos.RecordCount > 0 Then
        num_recibo = RegistosR(indice).NumDoc
        serie_rec = RegistosR(indice).SerieDoc
        'data = BL_HandleNull(rsRecibos!dt_chega, rsRecibos!dt_previ)
        data = BL_HandleNull(rsRecibos!dt_emi, Bg_DaData_ADO)
        DataChega = BL_HandleNull(rsRecibos!dt_chega, rsRecibos!dt_previ)
        vd = BL_HandleNull(rsRecibos!venda_dinheiro, "")
        dataAnul = BL_HandleNull(rsRecibos!dt_canc, "")
        HrAnul = BL_HandleNull(rsRecibos!hr_canc, "")
        
        efr = BL_HandleNull(rsRecibos!descr_efr, "")
        nome = BL_HandleNull(rsRecibos!nome_ute, "")
        num_benef = BL_HandleNull(rsRecibos!n_benef_rec, BL_HandleNull(rsRecibos!n_benef_req, ""))
        ult_analise = ""
        analises = ""
        total = 0
        user_emi = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", rsRecibos!user_emi)
        If BL_HandleNull(rsRecibos!n_contrib_ute, "") <> "" Then
            If Not IsNumeric(BL_HandleNull(rsRecibos!n_contrib_ute, "")) Then
                n_contrib_ute = ""
            Else
                n_contrib_ute = BL_HandleNull(rsRecibos!n_contrib_ute, "")
            End If
        Else
            n_contrib_ute = "null"
        End If
        If gDEMO = mediSim Then
            Formacao = "Documento emitido para fins de formação"
            cabecalho = "GLINTT HS"
        Else
            Formacao = ""
            cabecalho = BL_HandleNull(rsRecibos!cabecalho, "")
            num_contribuinte = BL_HandleNull(rsRecibos!num_contribuinte, "")
        End If
        Certificado = ""

        'NELSONPSILVA GLINTT-HS-21312
            qtd = BL_HandleNull(rsRecibos!quantidade, 1)
        '
        While Not rsRecibos.EOF
            taxa_ana = BL_HandleNull(rsRecibos!taxa, 0)
            If Len(Mid(taxa_ana, InStr(1, taxa_ana, ",") + 1)) = 1 Then
                taxa_ana = taxa_ana & "0"
            End If
            analises = analises & rsRecibos!descr_ana & Space(30 - Len(Mid(rsRecibos!descr_ana, 1, 30))) & "1    " & taxa_ana & vbCrLf
            total = total + CDbl(BL_HandleNull(rsRecibos!taxa, 0))
            ult_analise = rsRecibos!cod_ana
            rsRecibos.MoveNext
        Wend
        If Len(Mid(total, InStr(1, total, ",") + 1)) = 1 Then
            Tot = total & "0"
        Else
            Tot = total
        End If
        
        If gReciboCrystal = 1 Then
            ssql = "DELETE FROM SL_CR_RECIBO WHERE nome_computador = '" & BG_SYS_GetComputerName & "'"
            BG_ExecutaQuery_ADO ssql
            
            'Imprime recibo por crystal
            ssql = "INSERT INTO sl_cr_recibo(anulacao,n_req, num_recibo, efr, data, nome, num_benef,nome_computador,cabecalho,data_canc, hr_canc,n_contrib_ute) VALUES (" & _
                    numAnulacao & "," & n_req & "," & num_recibo & "," & BL_TrataStringParaBD(efr) & "," & _
                    BL_TrataStringParaBD(data) & "," & BL_TrataStringParaBD(nome) & "," & _
                    BL_TrataStringParaBD(num_benef) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & _
                    BL_TrataStringParaBD(cabecalho) & "," & BL_TrataDataParaBD(dataAnul) & "," & BL_TrataStringParaBD(HrAnul) & "," & n_contrib_ute & ")"
                    
            BG_ExecutaQuery_ADO ssql
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro a inserir dados no talão de anulação!", vbExclamation + vbOKOnly, "Recibos"
                Exit Sub
            End If

            continua = BL_IniciaReport("ReciboAnul", "Talão Anulação", crptToPrinter, False)
            If continua = False Then Exit Sub

            'Imprime o relatório no Crystal Reports
            Dim Report As CrystalReport
            Set Report = forms(0).Controls("Report")
            'Fórmulas do Report
            Report.formulas(0) = "TotalPagar=" & BL_TrataStringParaBD("" & CStr(Tot))
            Report.formulas(1) = "TotalPagarExtenso=" & BL_TrataStringParaBD("" & LCase(BL_Preco_EURO_Extenso(CDbl(Tot))))
            'SOLIVEIRA CARTAXO
            Report.formulas(2) = "TipoRec=" & BL_TrataStringParaBD("" & RegistosR(indice).SerieDoc)
            Report.formulas(3) = "Formacao=" & BL_TrataStringParaBD("" & Formacao)
            Report.formulas(4) = "Certificado=" & BL_TrataStringParaBD("" & Certificado)
            Report.SQLQuery = ""
            Report.Connect = "DSN=" & gDSN
            Report.SelectionFormula = "{sl_cr_recibo.nome_computador} = '" & BG_SYS_GetComputerName & "' "
            
            Call BL_ExecutaReport
        Else
            If Not BL_LerEtiqIni("RECIBOS_ANUL.ini") Then
                MsgBox "Ficheiro de inicialização de recibos ausente!"
                Exit Sub
            End If
            If Not BL_EtiqOpenPrinter(impressora) Then
                MsgBox "Impossível abrir impressora recibos"
                Exit Sub
            End If
            Call RECIBO_EtiqPrint(serie_rec, num_recibo, data, n_req, efr, nome, num_benef, analises, CStr(Tot), _
                                  CStr(numAnulacao), user_emi, "", DataChega, vd, "", "", "", 0, 0, cabecalho, _
                                  Formacao, Certificado, dataAnul, HrAnul, n_contrib_ute, "", "", "")
        End If
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
    
    BL_EtiqClosePrinter
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_ImprimeAnulRecibo: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_ImprimeAnulRecibo"
    Exit Sub
    Resume Next
End Sub


Public Function RECIBO_PreencheCabecalho(n_req As String, ByRef RegistosR As Collection) As Boolean
    Dim i As Long
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    RECIBO_PreencheCabecalho = False
    RECIBO_CriaNovaClasse RegistosR, 1, "REC", True
    ssql = "SELECT 'RECIBO' tipo,seq_recibo, cod_efr,serie_rec,n_rec num_doc, total_pagar total,user_cri,dt_cri,"
    ssql = ssql & " hr_cri,user_emi,dt_emi,hr_emi, estado, flg_impresso, "
    ssql = ssql & " dt_pagamento, hr_pagamento,venda_dinheiro,desconto, num_analises, flg_doc_caixa,valor_original,COD_EMPRESA,"
    ssql = ssql & " cod_forma_pag, seq_utente, flg_borla, nao_conformidade, atcud FROM sl_recibos "
    ssql = ssql & " WHERE n_req = " & n_req
    ssql = ssql & " ORDER BY COD_EFR ASC, dt_cri ASC, hr_cri ASC, dt_emi ASC, hr_emi ASC, dt_cri ASC, hr_cri ASC"
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount > 0 Then
        i = 1
        While Not rsRecibos.EOF
            
            ' -----------------------------------------------------------------------------------------------
            ' PROCURA NA TABELA SL_RECIBOS
            ' -----------------------------------------------------------------------------------------------
            RegistosR(i).SeqRecibo = BL_HandleNull(rsRecibos!seq_Recibo, -1)
            RegistosR(i).SerieDoc = BL_HandleNull(rsRecibos!serie_rec, "0")
            RegistosR(i).NumDoc = BL_HandleNull(rsRecibos!num_doc, "0")
            RegistosR(i).codEntidade = BL_HandleNull(rsRecibos!cod_efr, "0")
            RECIBO_PreencheDadosEntidade RegistosR, i
            
            If BL_HandleNull(rsRecibos!tipo, "") = "RECIBO" Then
                RegistosR(i).ValorPagar = BL_HandleNull(rsRecibos!total, 0)
                RegistosR(i).ValorOriginal = BL_HandleNull(rsRecibos!valor_original, 0)
            End If
            RegistosR(i).Desconto = BL_HandleNull(rsRecibos!Desconto, 0)
            RegistosR(i).UserCri = BL_HandleNull(rsRecibos!user_cri, "")
            RegistosR(i).DtCri = BL_HandleNull(rsRecibos!dt_cri, "")
            RegistosR(i).HrCri = BL_HandleNull(rsRecibos!hr_cri, "")
            RegistosR(i).UserEmi = BL_HandleNull(rsRecibos!user_emi, "")
            RegistosR(i).DtEmi = BL_HandleNull(rsRecibos!dt_emi, "")
            RegistosR(i).HrEmi = BL_HandleNull(rsRecibos!hr_emi, "")
            RegistosR(i).flg_impressao = BL_HandleNull(rsRecibos!flg_impresso, 0)
            RegistosR(i).DtPagamento = BL_HandleNull(rsRecibos!dt_pagamento, "")
            RegistosR(i).HrPagamento = BL_HandleNull(rsRecibos!hr_pagamento, "")
            RegistosR(i).NumVendaDinheiro = BL_HandleNull(rsRecibos!venda_dinheiro, "")
            RegistosR(i).Estado = BL_HandleNull(rsRecibos!Estado, "N")
            RegistosR(i).NumAnalises = BL_HandleNull(rsRecibos!num_analises, 0)
            RegistosR(i).flg_DocCaixa = BL_HandleNull(rsRecibos!flg_doc_caixa, 0)
            RegistosR(i).codEmpresa = BL_HandleNull(rsRecibos!cod_empresa, "")
            RegistosR(i).CodFormaPag = BL_HandleNull(rsRecibos!cod_forma_pag, 0)
            RegistosR(i).Certificado = ""
            RegistosR(i).SeqUtente = BL_HandleNull(rsRecibos!seq_utente, -1)
            RegistosR(i).FlgBorla = BL_HandleNull(rsRecibos!flg_borla, mediNao)
            RegistosR(i).FlgNaoConforme = BL_HandleNull(rsRecibos!nao_conformidade, 0)
            RegistosR(i).AT_ValidNumber = BL_HandleNull(rsRecibos!ATCUD, "")
            rsRecibos.MoveNext
            i = i + 1
            RECIBO_CriaNovaClasse RegistosR, i, "REC"
        Wend
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
    RECIBO_PreencheCabecalho = True
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_PreencheCabecalho: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_PreencheCabecalho"
    RECIBO_PreencheCabecalho = False
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' VAI A BD PROCURAR REGISTOS DOS RECIBOS PARA UMA DETERMINADA ANALISE

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_PreencheDetManuais(NumReq As String, ByRef RegistosRM As Collection, _
                                          TUtente As String, Utente As String) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim i As Long
    Dim valorUnitEFR As Double
    Dim valorEFR As Double
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    
    RECIBO_PreencheDetManuais = False
    
    ssql = "SELECT * FROM sl_recibos_det WHERE n_req = " & BL_TrataStringParaBD(CStr(NumReq))
    ssql = ssql & "  ORDER by cod_efr ASC, p1 ASC, ordem_marcacao ASC"
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    
    If rsRecibos.RecordCount > 0 Then
        i = 1
        While Not rsRecibos.EOF
            valorEFR = 0
            valorUnitEFR = 0
            descrAnaEfr = ""
            codAnaEfr = ""
            RegistosRM(i).ReciboSeqRecibo = BL_HandleNull(rsRecibos!seq_Recibo, "-1")
            RegistosRM(i).ReciboP1 = BL_HandleNull(rsRecibos!p1, "-1")
            RegistosRM(i).ReciboNumDoc = BL_HandleNull(rsRecibos!n_rec, "0")
            RegistosRM(i).ReciboSerieDoc = BL_HandleNull(rsRecibos!serie_rec, "0")
            RegistosRM(i).ReciboEntidade = BL_HandleNull(rsRecibos!cod_efr, "0")
            RegistosRM(i).ReciboCodEfrEnvio = BL_HandleNull(rsRecibos!cod_efr_envio, RegistosRM(i).ReciboEntidade)
            RegistosRM(i).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", BL_HandleNull(rsRecibos!cod_efr, "0"))
            RegistosRM(i).ReciboTaxa = BL_HandleNull(rsRecibos!taxa, "0")
            RegistosRM(i).ReciboCodAna = BL_HandleNull(rsRecibos!cod_ana, "0")
            RegistosRM(i).ReciboCodFacturavel = BL_HandleNull(rsRecibos!cod_facturavel, "0")
            RegistosRM(i).ReciboUserCri = BL_HandleNull(rsRecibos!user_cri, "")
            RegistosRM(i).ReciboDtCri = BL_HandleNull(rsRecibos!dt_cri, "")
            RegistosRM(i).ReciboHrCri = BL_HandleNull(rsRecibos!hr_cri, "")
            RegistosRM(i).ReciboUserEmi = BL_HandleNull(rsRecibos!user_emi, "")
            RegistosRM(i).ReciboDtEmi = BL_HandleNull(rsRecibos!dt_emi, "")
            RegistosRM(i).ReciboHrEmi = BL_HandleNull(rsRecibos!hr_emi, "")
            RegistosRM(i).ReciboFlgImpresso = BL_HandleNull(rsRecibos!flg_impresso, "0")
            RegistosRM(i).ReciboFlgFacturado = BL_HandleNull(rsRecibos!Flg_Facturado, "0")
            RegistosRM(i).ReciboFlgRetirado = BL_HandleNull(rsRecibos!flg_retirado, "0")
            RegistosRM(i).ReciboOrdemMarcacao = BL_HandleNull(rsRecibos!ordem_marcacao, "0")
            RegistosRM(i).ReciboFlgAdicionado = BL_HandleNull(rsRecibos!Flg_adicionada, "0")
            RegistosRM(i).ReciboIsencao = BL_HandleNull(rsRecibos!isencao, "0")
            RegistosRM(i).ReciboCorP1 = BL_HandleNull(rsRecibos!cor_p1, "B")
            RegistosRM(i).ReciboCodAnaEFR = BL_HandleNull(rsRecibos!cod_ana_efr, "")
            RegistosRM(i).ReciboDescrAnaEFR = BL_HandleNull(rsRecibos!descr_ana_efr, "")
            RegistosRM(i).ReciboCodMedico = BL_HandleNull(rsRecibos!cod_medico, "")
            RegistosRM(i).ReciboQuantidade = BL_HandleNull(rsRecibos!quantidade, "1")
            RegistosRM(i).ReciboQuantidadeEFR = BL_HandleNull(rsRecibos!quantidade_envio, RegistosRM(i).ReciboQuantidade)
            RegistosRM(i).ReciboTaxaUnitario = BL_HandleNull(rsRecibos!taxa_unitario, "")
            RegistosRM(i).ReciboFlgReciboManual = BL_HandleNull(rsRecibos!flg_recibo_manual, "")
            RegistosRM(i).ReciboFlgMarcacaoRetirada = BL_HandleNull(rsRecibos!flg_marcacao_Retirada, "")
            RegistosRM(i).ReciboAnaNaoFact = BL_HandleNull(rsRecibos!ana_nao_fact, "")
            RegistosRM(i).ReciboFDS = BL_HandleNull(rsRecibos!fds, "")
            RegistosRM(i).ReciboCodigoPai = BL_HandleNull(rsRecibos!codigo_pai, "")
            RegistosRM(i).flgCodBarras = BL_HandleNull(rsRecibos!flg_cod_barras, 0)
            RegistosRM(i).ReciboFlgAutomatico = BL_HandleNull(rsRecibos!flg_automatico, 0)
            RegistosRM(i).ReciboNrBenef = BL_HandleNull(rsRecibos!n_benef, "")
            valorEFR = IF_RetornaTaxaAnaliseEntidade(RegistosRM(i).ReciboCodFacturavel, RegistosRM(i).ReciboEntidade, _
                                                      RegistosRM(i).ReciboDtCri, codAnaEfr, TUtente, Utente, _
                                                      valorUnitEFR, RegistosRM(i).ReciboQuantidade, "", False, descrAnaEfr, mediComboValorNull)
            RegistosRM(i).ReciboValorEFR = valorUnitEFR * RegistosRM(i).ReciboQuantidade
            RegistosRM(i).ReciboValorUnitEFR = valorUnitEFR
            If RegistosRM(i).ReciboCodAnaEFR = "" Then
                RegistosRM(i).ReciboCodAnaEFR = codAnaEfr
            End If
            If RegistosRM(i).ReciboDescrAnaEFR = "" Then
                RegistosRM(i).ReciboDescrAnaEFR = descrAnaEfr
            End If

            rsRecibos.MoveNext
            i = i + 1
            RECIBO_CriaNovaClasse RegistosRM, i, "REC_MAN"
        Wend
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
Exit Function
TrataErro:
    RECIBO_PreencheDetManuais = False
    BG_LogFile_Erros "RECIBO_PreencheDetManuais: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_PreencheDetManuais"
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' VAI A BD PROCURAR REGISTOS DOS RECIBOS PARA UMA DETERMINADA ANALISE

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_PreencheDetalhe(NumReq As Long, codAna As String, indice As Long, ByRef RegistosA As Collection) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    RECIBO_PreencheDetalhe = False
    
    ssql = "SELECT * FROM sl_recibos_det WHERE n_req = " & BL_TrataStringParaBD(CStr(NumReq))
    ssql = ssql & " AND (flg_adicionada is null or flg_adicionada =0 )AND cod_ana = " & BL_TrataStringParaBD(codAna)
    ssql = ssql & " AND (flg_recibo_manual is null or flg_recibo_manual = 0)"
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    
    If rsRecibos.RecordCount > 0 Then
        RegistosA(indice).ReciboSeqRecibo = BL_HandleNull(rsRecibos!seq_Recibo, "-1")
        RegistosA(indice).ReciboP1 = BL_HandleNull(rsRecibos!p1, "-1")
        RegistosA(indice).ReciboNumDoc = BL_HandleNull(rsRecibos!n_rec, "0")
        RegistosA(indice).ReciboSerieDoc = BL_HandleNull(rsRecibos!serie_rec, "0")
        RegistosA(indice).ReciboEntidade = BL_HandleNull(rsRecibos!cod_efr, "0")
        RegistosA(indice).ReciboCodEfrEnvio = BL_HandleNull(rsRecibos!cod_efr_envio, "0")
        RegistosA(indice).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", BL_HandleNull(rsRecibos!cod_efr, "0"))
        RegistosA(indice).ReciboTaxa = BL_HandleNull(rsRecibos!taxa, "0")
        RegistosA(indice).ReciboCodFacturavel = BL_HandleNull(rsRecibos!cod_facturavel, "0")
        RegistosA(indice).ReciboUserCri = BL_HandleNull(rsRecibos!user_cri, "")
        RegistosA(indice).ReciboDtCri = BL_HandleNull(rsRecibos!dt_cri, "")
        RegistosA(indice).ReciboHrCri = BL_HandleNull(rsRecibos!hr_cri, "")
        RegistosA(indice).ReciboUserEmi = BL_HandleNull(rsRecibos!user_emi, "")
        RegistosA(indice).ReciboDtEmi = BL_HandleNull(rsRecibos!dt_emi, "")
        RegistosA(indice).ReciboHrEmi = BL_HandleNull(rsRecibos!hr_emi, "")
        RegistosA(indice).ReciboFlgImpresso = BL_HandleNull(rsRecibos!flg_impresso, "0")
        RegistosA(indice).ReciboFlgFacturado = BL_HandleNull(rsRecibos!Flg_Facturado, "0")
        RegistosA(indice).ReciboFlgRetirado = BL_HandleNull(rsRecibos!flg_retirado, "0")
        RegistosA(indice).ReciboOrdemMarcacao = BL_HandleNull(rsRecibos!ordem_marcacao, "0")
        RegistosA(indice).ReciboFlgAdicionado = BL_HandleNull(rsRecibos!Flg_adicionada, "0")
        RegistosA(indice).ReciboIsencao = BL_HandleNull(rsRecibos!isencao, "0")
        RegistosA(indice).ReciboCorP1 = BL_HandleNull(rsRecibos!cor_p1, "B")
        RegistosA(indice).ReciboCodAnaEFR = BL_HandleNull(rsRecibos!cod_ana_efr, "")
        RegistosA(indice).ReciboDescrAnaEFR = BL_HandleNull(rsRecibos!descr_ana_efr, "")
        RegistosA(indice).ReciboCodMedico = BL_HandleNull(rsRecibos!cod_medico, "")
        RegistosA(indice).ReciboQuantidade = BL_HandleNull(rsRecibos!quantidade, 1)
        RegistosA(indice).ReciboQuantidadeEFR = BL_HandleNull(rsRecibos!quantidade_envio, RegistosA(indice).ReciboQuantidade)
        RegistosA(indice).ReciboTaxaUnitario = BL_HandleNull(rsRecibos!taxa_unitario, "")
        RegistosA(indice).ReciboFlgReciboManual = BL_HandleNull(rsRecibos!flg_recibo_manual, "")
        RegistosA(indice).ReciboFlgMarcacaoRetirada = BL_HandleNull(rsRecibos!flg_marcacao_Retirada, "")
        RegistosA(indice).ReciboAnaNaoFact = BL_HandleNull(rsRecibos!ana_nao_fact, "")
        RegistosA(indice).ReciboNrBenef = BL_HandleNull(rsRecibos!n_benef, "")
        RegistosA(indice).ReciboFDS = BL_HandleNull(rsRecibos!fds, "0")
        RegistosA(indice).ReciboCodigoPai = BL_HandleNull(rsRecibos!codigo_pai, "")
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
    RECIBO_PreencheDetalhe = True
Exit Function
TrataErro:
    RECIBO_PreencheDetalhe = False
    BG_LogFile_Erros "RECIBO_PreencheDetalhe: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_PreencheDetalhe"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE PODE GERAR RECIBO

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_VerificaBloqueio(n_rec As Long) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim registos As Long
    RECIBO_VerificaBloqueio = False
    
    If gSGBD = gOracle Then
        ssql = "SELECT * FROM sl_bloqueio WHERE n_rec = 0 for update of sl_bloqueio.n_rec"
    ElseIf gSGBD = gSqlServer Then
        ssql = "SELECT * FROM sl_bloqueio (UPDLOCK) WHERE n_rec = 0 "
    End If
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    rsRecibos.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount = 1 Then
        ssql = "UPDATE sl_bloqueio set n_Rec = " & n_rec & " WHERE n_rec = 0 "
        registos = BG_ExecutaQuery_ADO(ssql)
        If registos = 1 Then
            RECIBO_VerificaBloqueio = True
        Else
            RECIBO_VerificaBloqueio = False
        End If
    End If
    rsRecibos.Close
Exit Function
TrataErro:
    RECIBO_VerificaBloqueio = False
    BG_LogFile_Erros "RECIBO_VerificaBloqueio: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_VerificaBloqueio"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE PODE GERAR RECIBO

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_DevolveNumero(cod_empresa As String, tipoDocumento As Integer) As Long
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim registos As Long
    Dim ret As Long
    ret = -1
    
    If tipoDocumento = lRecibo Then
        If gSGBD = gOracle Then
            ssql = "SELECT n_rec  FROM sl_empresas WHERE cod_empresa =" & BL_TrataStringParaBD(cod_empresa) & " FOR UPDATE OF sl_empresas.cod_empresa "
        ElseIf gSGBD = gSqlServer Then
            ssql = "SELECT n_rec  FROM sl_empresas (UPDLOCK) WHERE cod_empresa = " & BL_TrataStringParaBD(cod_empresa)
        ElseIf gSGBD = gPostGres Then
            ssql = "SELECT n_rec  FROM sl_empresas WHERE cod_empresa =" & BL_TrataStringParaBD(cod_empresa) & " FOR UPDATE  "
        End If
    ElseIf tipoDocumento = lDocCaixa Then
        If gSGBD = gOracle Then
            ssql = "SELECT n_doc  FROM sl_empresas WHERE cod_empresa =" & BL_TrataStringParaBD(cod_empresa) & " FOR UPDATE OF sl_empresas.cod_empresa "
        ElseIf gSGBD = gSqlServer Then
            ssql = "SELECT n_doc FROM sl_empresas (UPDLOCK)  WHERE cod_empresa = " & BL_TrataStringParaBD(cod_empresa)
        ElseIf gSGBD = gPostGres Then
            ssql = "SELECT n_doc  FROM sl_empresas WHERE cod_empresa =" & BL_TrataStringParaBD(cod_empresa) & " FOR UPDATE "
        End If
    End If
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount = 1 Then
        If tipoDocumento = lRecibo Then
            ret = BL_HandleNull(rsRecibos!n_rec, 0) + 1
        ElseIf tipoDocumento = lDocCaixa Then
            ret = BL_HandleNull(rsRecibos!n_doc, 0) + 1
        End If
    End If
    rsRecibos.Close
    RECIBO_DevolveNumero = ret
    
Exit Function
TrataErro:
    RECIBO_DevolveNumero = -1
    BG_LogFile_Erros "RECIBO_DevolveNumero: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_DevolveNumero"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE PODE GERAR RECIBO

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_DevolveSerie(cod_empresa As String, tipoDocumento As Integer) As String
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim registos As Long
    Dim ret As String
    Dim tipo As String
    Dim ano As Integer
    ret = ""
    
    If tipoDocumento = lRecibo Then
        tipo = "FAC_REC"
    ElseIf tipoDocumento = lDocCaixa Then
        tipo = "DOC_CAIXA"
    End If
    ano = Year(Bg_DaData_ADO)
    
    ' VERIFICA QUAL A SERIE
    ssql = "SELECT serie_doc  FROM sl_serie_doc WHERE tipo_doc = " & BL_TrataStringParaBD(tipo)
    ssql = ssql & " AND ano_doc = " & BL_TrataStringParaBD(CStr(ano)) & " AND empresa_id =" & BL_TrataStringParaBD(cod_empresa)
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount = 0 Then
        rsRecibos.Close
        ' SE NAO EXISTE SERIE PARA O ANO CORRENTE, INSERE NOVA LINHA NA SL_NUM_GEN E SL_SERIE_DOC
        If RECIBO_InsereNovaSerie(cod_empresa, tipoDocumento, ano) = False Then
            GoTo TrataErro
        End If
        ' VERIFICA QUAL A SERIE
        ssql = "SELECT serie_doc  FROM sl_serie_doc WHERE tipo_doc = " & BL_TrataStringParaBD(tipo)
        ssql = ssql & " AND ano_doc = " & BL_TrataStringParaBD(CStr(ano)) & " AND empresa_id =" & BL_TrataStringParaBD(cod_empresa)
        rsRecibos.CursorLocation = adUseServer
        rsRecibos.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsRecibos.Open ssql, gConexao
        If rsRecibos.RecordCount <> 1 Then
            rsRecibos.Close
            Set rsRecibos = Nothing
            GoTo TrataErro
        End If
    End If
    If rsRecibos.RecordCount = 1 Then
        ret = BL_HandleNull(rsRecibos!serie_doc, "")
    End If
    rsRecibos.Close
    RECIBO_DevolveSerie = ret
    
Exit Function
TrataErro:
    RECIBO_DevolveSerie = ""
    BG_LogFile_Erros "RECIBO_DevolveSerie: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_DevolveSerie"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' INSERE NOVA LINHA NA TABELA SL_SERIE_DOC

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_InsereNovaSerie(cod_empresa As String, tipoDocumento As Integer, ano As Integer) As Boolean
    Dim ssql As String
    Dim iret As Long
    Dim serie As String
    Dim tipo As String
    On Error GoTo TrataErro
    
    RECIBO_InsereNovaSerie = False
    If tipoDocumento = lRecibo Then
        tipo = "FAC_REC"
        serie = cod_empresa & "FR" & ano
    ElseIf tipoDocumento = lDocCaixa Then
        tipo = "DOC_CAIXA"
        serie = cod_empresa & "DC" & ano
    End If
    
    ' INSERE NA TABELA SL_SERIE_DOC
    ssql = "INSERT INTO SL_SERIE_DOC(tipo_doc, ano_doc, serie_doc,user_cri, dt_cri,empresa_id, activo) VALUES ("
    ssql = ssql & BL_TrataStringParaBD(tipo) & ", "
    ssql = ssql & BL_TrataStringParaBD(CStr(ano)) & ","
    ssql = ssql & BL_TrataStringParaBD(serie) & ", "
    ssql = ssql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    ssql = ssql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
    ssql = ssql & BL_TrataStringParaBD(CStr(cod_empresa)) & ",'S') "
    iret = BG_ExecutaQuery_ADO(ssql)
    If iret <> 1 Then
        GoTo TrataErro
    End If
    
    ' INSERE NA TABELA SL_NUM_GEN
    ssql = "INSERT INTO sl_num_gen(serie, num_gen) VALUES( "
    ssql = ssql & BL_TrataStringParaBD(serie) & ",0)"
    iret = BG_ExecutaQuery_ADO(ssql)
    If iret <> 1 Then
        GoTo TrataErro
    End If
    
    BG_LogFile_Erros "RECIBO_InsereNovaSerie: " & cod_empresa & " " & tipoDocumento & " " & ano, "RECIBO", "RECIBO_ActualizaNumero"
    RECIBO_InsereNovaSerie = True
Exit Function
TrataErro:
    RECIBO_InsereNovaSerie = False
    BG_LogFile_Erros "RECIBO_InsereNovaSerie: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_InsereNovaSerie"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' PARA UMA DETERMINADA SERIE DEVOLVE O NUMERO

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_DevolveNumeroSerie(serie As String) As Long
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim registos As Long
    Dim ret As Long
    ret = -1
    If gSGBD = gOracle Then
        ssql = "SELECT num_gen  FROM sl_num_gen WHERE serie =" & BL_TrataStringParaBD(serie) & " FOR UPDATE OF sl_num_gen.num_gen "
    ElseIf gSGBD = gSqlServer Then
        ssql = "SELECT num_gen  FROM sl_num_gen (UPDLOCK) WHERE serie = " & BL_TrataStringParaBD(serie)
    ElseIf gSGBD = gPostGres Then
        ssql = "SELECT num_gen  FROM sl_num_gen WHERE serie =" & BL_TrataStringParaBD(serie) & " FOR UPDATE "
    End If
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount = 1 Then
        ret = BL_HandleNull(rsRecibos!num_gen, 0) + 1
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
    RECIBO_DevolveNumeroSerie = ret
Exit Function
TrataErro:
    RECIBO_DevolveNumeroSerie = -1
    BG_LogFile_Erros "RECIBO_DevolveNumeroSerie: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_DevolveNumeroSerie"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE PODE GERAR RECIBO

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_ActualizaNumero(cod_empresa As String, tipoDocumento As Integer, numero As Long) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim registos As Long
    RECIBO_ActualizaNumero = False
    
    If tipoDocumento = lRecibo Then
        ssql = "UPDATE sl_empresas SET n_rec = " & numero & " WHERE cod_empresa = " & BL_TrataStringParaBD(cod_empresa)
    ElseIf tipoDocumento = lDocCaixa Then
        ssql = "UPDATE sl_empresas SET n_doc = " & numero & " WHERE cod_empresa = " & BL_TrataStringParaBD(cod_empresa)
    End If
    registos = BG_ExecutaQuery_ADO(ssql)
    If registos <> 1 Then
        GoTo TrataErro
    End If
    RECIBO_ActualizaNumero = True
    
    'BG_LogFile_Erros "Gerou documento: " & numero, "RECIBO", "RECIBO_ActualizaNumero"
Exit Function
TrataErro:
    RECIBO_ActualizaNumero = False
    BG_LogFile_Erros "RECIBO_ActualizaNumero: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_ActualizaNumero"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE PODE GERAR RECIBO

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_ActualizaNumeroAnual(serie As String, numero As Long) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim registos As Long
    RECIBO_ActualizaNumeroAnual = False
    
    ssql = "UPDATE sl_num_gen SET num_gen = " & numero & " WHERE serie = " & BL_TrataStringParaBD(BL_HandleNull(serie, ""))
    registos = BG_ExecutaQuery_ADO(ssql)
    If registos <> 1 Then
        GoTo TrataErro
    End If
    RECIBO_ActualizaNumeroAnual = True
    
    BG_LogFile_Erros "Gerou documento: " & numero, "RECIBO", "RECIBO_ActualizaNumeroAnual"
Exit Function
TrataErro:
    RECIBO_ActualizaNumeroAnual = False
    BG_LogFile_Erros "RECIBO_ActualizaNumeroAnual: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_ActualizaNumeroAnual"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' CARREGA ADIANTAMENTOS

' -----------------------------------------------------------------------------------------------
Public Function RECIBO_PreencheAdiantamento(n_req As String, ByRef RegistosAD As Collection) As Boolean
    Dim i As Long
    Dim ssql As String
    Dim rsAdiantamentos As New adodb.recordset
    On Error GoTo TrataErro
    RECIBO_PreencheAdiantamento = False
    
    ssql = "SELECT n_adiantamento, valor, estado, dt_emi, hr_emi, user_emi, dt_anul, hr_anul, user_anul "
    ssql = ssql & " FROM sl_adiantamentos WHERE n_req = " & n_req
    rsAdiantamentos.CursorLocation = adUseServer
    rsAdiantamentos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAdiantamentos.Open ssql, gConexao
    If rsAdiantamentos.RecordCount > 0 Then
        i = 1
        While Not rsAdiantamentos.EOF
            
            ' -----------------------------------------------------------------------------------------------
            ' PROCURA NA TABELA SL_RECIBOS
            ' -----------------------------------------------------------------------------------------------
            RegistosAD(i).NumAdiantamento = BL_HandleNull(rsAdiantamentos!n_adiantamento, 0)
            RegistosAD(i).valor = BL_HandleNull(rsAdiantamentos!valor, 0)
            RegistosAD(i).Estado = BL_HandleNull(rsAdiantamentos!Estado, "")
            RegistosAD(i).DtEmi = BL_HandleNull(rsAdiantamentos!dt_emi, "")
            RegistosAD(i).UserEmi = BL_HandleNull(rsAdiantamentos!user_emi, "")
            RegistosAD(i).HrEmi = BL_HandleNull(rsAdiantamentos!hr_emi, "")
            RegistosAD(i).DtAnul = BL_HandleNull(rsAdiantamentos!dt_anul, "")
            RegistosAD(i).HrAnul = BL_HandleNull(rsAdiantamentos!hr_anul, "")
            RegistosAD(i).UserAnul = BL_HandleNull(rsAdiantamentos!user_anul, "")
                        
            rsAdiantamentos.MoveNext
            i = i + 1
            RECIBO_CriaNovaClasse RegistosAD, i, "ADI"
        Wend
    End If
    rsAdiantamentos.Close
    Set rsAdiantamentos = Nothing
    RECIBO_PreencheAdiantamento = True
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_PreencheAdiantamento: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_PreencheAdiantamento"
    RECIBO_PreencheAdiantamento = False
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------

' INSERE NA BD

' ----------------------------------------------------------------------
Private Function RECIBO_GravaAdiantamentoBD(n_req As String, ByRef RegistosAD As Collection) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim ssql As String
    
    RECIBO_GravaAdiantamentoBD = False
    
    If RegistosAD.Count <= 1 Then
        RECIBO_GravaAdiantamentoBD = True
        Exit Function
    End If
    
    gSQLError = 0
    
    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA RECIBO
    ' -----------------------------------------------------------------------------------------------
    For i = 1 To RegistosAD.Count
        If RegistosAD(i).valor > 0 Then
            ssql = "INSERT INTO sl_adiantamentos(n_req,n_adiantamento,valor, estado, user_emi, dt_emi, hr_emi,"
            ssql = ssql & " user_anul, dt_anul, hr_anul ) VALUES( "
            ssql = ssql & n_req & ", "
            ssql = ssql & RegistosAD(i).NumAdiantamento & ", "
            ssql = ssql & Replace(RegistosAD(i).valor, ",", ".") & ", "
            ssql = ssql & BL_TrataStringParaBD(RegistosAD(i).Estado) & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(RegistosAD(i).UserEmi)) & ", "
            ssql = ssql & BL_TrataDataParaBD(RegistosAD(i).DtEmi) & ", "
            ssql = ssql & BL_TrataStringParaBD(RegistosAD(i).HrEmi) & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(RegistosAD(i).UserAnul)) & ", "
            ssql = ssql & BL_TrataDataParaBD(RegistosAD(i).DtAnul) & ", "
            ssql = ssql & BL_TrataStringParaBD(RegistosAD(i).HrAnul) & ") "
            BG_ExecutaQuery_ADO ssql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
        End If
    Next
    RECIBO_GravaAdiantamentoBD = True
    
Exit Function
TrataErro:
    RECIBO_GravaAdiantamentoBD = False
    BG_LogFile_Erros "RECIBO_GravaAdiantamentoBD: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_GravaAdiantamentoBD"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------

' APAGA DAS TABELAS E CHAMA FUNCAO PARA INSERIR NA BD

' ----------------------------------------------------------------------
Public Function RECIBO_GravaAdiantamento(n_req As String, ByRef RegistosAD As Collection) As Boolean
     On Error GoTo TrataErro
  
    Dim sql As String
    Dim detalhe As Boolean
    RECIBO_GravaAdiantamento = False
    gSQLError = 0
    
    
    sql = "DELETE FROM sl_adiantamentos WHERE n_req=" & Trim(n_req)
    BG_ExecutaQuery_ADO sql
           
    If gSQLError <> 0 Then
        GoTo TrataErro
    End If
    
    detalhe = RECIBO_GravaAdiantamentoBD(n_req, RegistosAD)
    If detalhe = False Then
        GoTo TrataErro
    End If
    RECIBO_GravaAdiantamento = True
Exit Function
TrataErro:
    RECIBO_GravaAdiantamento = False
    BG_LogFile_Erros "RECIBO_GravaAdiantamento: " & Err.Number & " - " & Err.Description, "RECIBOS", "RECIBO_GravaAdiantamento"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------

' ANULA ADIANTAMENTO

' ----------------------------------------------------------------------
Public Function RECIBO_AnulaAdiantamento(n_req As String, indice As Long, ByRef RegistosAD As Collection) As Boolean
     On Error GoTo TrataErro
    RECIBO_AnulaAdiantamento = False
    
    RegistosAD(indice).Estado = "A"
    RegistosAD(indice).UserAnul = CStr(gCodUtilizador)
    RegistosAD(indice).DtAnul = Bg_DaData_ADO
    RegistosAD(indice).HrAnul = Bg_DaHora_ADO
    
    RECIBO_AnulaAdiantamento = RECIBO_GravaAdiantamento(n_req, RegistosAD)
Exit Function
TrataErro:
    RECIBO_AnulaAdiantamento = False
    BG_LogFile_Erros "RECIBO_AnulaAdiantamento: " & Err.Number & " - " & Err.Description, "RECIBOS", "RECIBO_AnulaAdiantamento"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' GERA O ADIANTAMENTO

' ----------------------------------------------------------------------
Public Function RECIBO_GeraAdiantamento(n_req As String, valor As Double, ByRef RegistosAD As Collection) As Boolean
    Dim NumAdiantamento As Long
    Dim i As Integer
    Dim j As Long
    Dim serie_rec As String
    Dim Certificado As Boolean
    Dim resRecibo As Boolean
    
    On Error GoTo TrataErro
    RECIBO_GeraAdiantamento = False
        
    'abre transacao
    BG_BeginTransaction
    NumAdiantamento = -1
    NumAdiantamento = BL_GeraNumero("N_CAUCAO")
    
        If NumAdiantamento <> -1 Then
            RegistosAD(RegistosAD.Count).NumAdiantamento = NumAdiantamento
            RegistosAD(RegistosAD.Count).Estado = gEstadoReciboPago
            RegistosAD(RegistosAD.Count).valor = valor
            RegistosAD(RegistosAD.Count).UserEmi = CStr(gCodUtilizador)
            RegistosAD(RegistosAD.Count).DtEmi = Bg_DaData_ADO
            RegistosAD(RegistosAD.Count).HrEmi = Bg_DaHora_ADO
            RECIBO_CriaNovaClasse RegistosAD, RegistosAD.Count, "ADI"
                
            resRecibo = RECIBO_GravaAdiantamento(n_req, RegistosAD)
            If resRecibo = False Then
                GoTo TrataErro
            End If
            
            'conclui transacao
            BG_CommitTransaction
                
            RECIBO_ImprimeAdiantamento n_req, NumAdiantamento, FormGestaoRequisicaoPrivado.EcPrinterRecibo
            RECIBO_GeraAdiantamento = True
        Else
            BG_Mensagem mediMsgBox, "Erro ao gerar adiantamento.", vbOKOnly + vbCritical, "InsereRecibo"
            GoTo TrataErro
        End If
    
Exit Function
TrataErro:
    BG_RollbackTransaction
    RECIBO_GeraAdiantamento = False
    BG_LogFile_Erros "RECIBO_GeraAdiantamento: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_GeraAdiantamento"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' IMPRIME DOCUMENTO DE ADIANTAMENTO.

' ----------------------------------------------------------------------
Public Sub RECIBO_ImprimeAdiantamento(n_req As String, NumAdiantamento As Long, impressora As String)
    On Error GoTo TrataErro
    Dim continua As Boolean
    Dim ssql As String
    Dim rsAdiantamentos As New adodb.recordset
    Dim data As String
    Dim nome As String
    Dim num_benef As String
    Dim user_emi As String
    Dim DataChega As String
    Dim totalPagar As Double
    
    If gReciboCrystal = 1 Then
        ssql = "DELETE FROM sl_cr_adiantamentos WHERE nome_computador = '" & gComputador & "'"
        BG_ExecutaQuery_ADO ssql
    End If
    
    ssql = " SELECT sl_requis.dt_chega,sl_requis.dt_previ, sl_identif.nome_ute, "
    ssql = ssql & " sl_requis.n_benef n_benef_req, sl_adiantamentos.dt_emi, "
    ssql = ssql & " sl_adiantamentos.user_emi, "
    ssql = ssql & " sl_adiantamentos.valor "
    ssql = ssql & " FROM sl_adiantamentos, sl_requis, sl_identif"
    ssql = ssql & " WHERE  sl_adiantamentos.n_adiantamento = " & NumAdiantamento
    ssql = ssql & " AND sl_requis.n_req = " & n_req
    ssql = ssql & " AND sl_requis.n_req = sl_adiantamentos.n_req "
    ssql = ssql & " AND sl_identif.seq_utente= sl_Requis.seq_utente"

    
    Set rsAdiantamentos = New adodb.recordset
    rsAdiantamentos.CursorType = adOpenStatic
    rsAdiantamentos.CursorLocation = adUseServer
    rsAdiantamentos.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAdiantamentos.Open ssql, gConexao

    If rsAdiantamentos.RecordCount = 1 Then
        
        data = BL_HandleNull(rsAdiantamentos!dt_emi, Bg_DaData_ADO)
        DataChega = BL_HandleNull(rsAdiantamentos!dt_chega, rsAdiantamentos!dt_previ)
        nome = BL_HandleNull(rsAdiantamentos!nome_ute, "")
        num_benef = BL_HandleNull(rsAdiantamentos!n_benef_req, "")
        user_emi = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", rsAdiantamentos!user_emi)
        totalPagar = BL_HandleNull(rsAdiantamentos!valor, 0)
            
        If gReciboCrystal = 1 Then
            ssql = "INSERT INTO sl_cr_adiantamentos(n_req,n_adiantamento,data, nome, num_benef, nome_computador, valor) VALUES (" & _
                    n_req & "," & NumAdiantamento & "," & BL_TrataStringParaBD(data) & "," & BL_TrataStringParaBD(nome) & "," & _
                    BL_TrataStringParaBD(num_benef) & "," & BL_TrataStringParaBD(gComputador) & "," & Replace(totalPagar, ",", ".") & ")"
            BG_ExecutaQuery_ADO ssql
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro!", vbExclamation + vbOKOnly, "Recibos"
                Exit Sub
            End If
            
            'Imprime recibo por crystal
            If BL_PreviewAberto("Adiantamento") = True Then Exit Sub
            continua = BL_IniciaReport("Adiantamento", "Adiantamento", crptToPrinter, False)
            If continua = False Then Exit Sub

            'Imprime o relatório no Crystal Reports
            Dim Report As CrystalReport
            Set Report = forms(0).Controls("Report")
            Report.SQLQuery = "SELECT n_req,n_adiantamento,data, nome, num_benef, nome_computador FROM sl_cr_adiantamentos"
            Report.SQLQuery = Report.SQLQuery & " WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            Report.Connect = "DSN=" & gDSN
            
            Call BL_ExecutaReport

        Else
            If Not BL_LerEtiqIni("ADIANTAMENTO.INI") Then
                MsgBox "Ficheiro de inicialização de adiantamentos ausente!"
                Exit Sub
            End If
            If Not BL_EtiqOpenPrinter(impressora) Then
                MsgBox "Impossível abrir impressora recibos"
                Exit Sub
            End If
                
            Call RECIBO_EtiqPrintAD(n_req, CStr(NumAdiantamento), data, DataChega, nome, num_benef, user_emi, CStr(totalPagar))
    
            BL_EtiqClosePrinter
        End If
    End If
    rsAdiantamentos.Close
    Set rsAdiantamentos = Nothing
        
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "RECIBO_ImprimeAdiantamento: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_ImprimeAdiantamento"
    Exit Sub
    Resume Next
End Sub

Private Function RECIBO_EtiqPrintAD(n_req As String, num_adiantamento As String, data As String, DataChega As String, _
                                    nome As String, num_benef As String, user_emi As String, totalPagar As String)
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    On Error GoTo TrataErro


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{NUM_ADIANTAMENTO}", num_adiantamento)
    sWrittenData = Replace(sWrittenData, "{DATA}", data)
    sWrittenData = Replace(sWrittenData, "{DATA_REQ}", DataChega)
    sWrittenData = Replace(sWrittenData, "{NOME}", nome)
    sWrittenData = Replace(sWrittenData, "{NUM_BENEF}", num_benef)
    sWrittenData = Replace(sWrittenData, "{USER_EMI}", user_emi)
    sWrittenData = Replace(sWrittenData, "{TOTAL}", totalPagar)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    RECIBO_EtiqPrintAD = True
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_EtiqPrintAD: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_EtiqPrintAD"
    RECIBO_EtiqPrintAD = False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' RETORNA O VALOR TOTAL ADIANTADO.

' ----------------------------------------------------------------------

Public Function RECIBO_RetornaTotalAdiantamentos(ByRef RegistosAD As Collection) As Double
    Dim i As Integer
    On Error GoTo TrataErro
    RECIBO_RetornaTotalAdiantamentos = 0
    For i = 1 To RegistosAD.Count
        If RegistosAD(i).NumAdiantamento > 0 Then
            If RegistosAD(i).Estado = gEstadoReciboPago Then
                RECIBO_RetornaTotalAdiantamentos = RECIBO_RetornaTotalAdiantamentos + RegistosAD(i).valor
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_RetornaTotalAdiantamentos: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_RetornaTotalAdiantamentos"
    RECIBO_RetornaTotalAdiantamentos = 0
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' RETORNA CERTIFICADO ASSOCIADO AO RECIBO

' ----------------------------------------------------------------------
Private Function RECIBO_RetornaCertificado(n_rec As String, serie_rec As String) As String
    Dim ssql As String
    Dim rsCert As New adodb.recordset
    On Error GoTo TrataErro
    
    RECIBO_RetornaCertificado = ""
    ssql = "SELECT * FROM sl_rec_certificado WHERE serie_rec = " & BL_TrataStringParaBD(serie_rec)
    ssql = ssql & " AND n_rec = " & n_rec
    rsCert.CursorLocation = adUseServer
    rsCert.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsCert.Open ssql, gConexao
    If rsCert.RecordCount > 0 Then
        RECIBO_RetornaCertificado = BL_HandleNull(rsCert!Certificado, "")
    End If
    rsCert.Close
    Set rsCert = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_RetornaCertificado: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_RetornaCertificado"
    RECIBO_RetornaCertificado = ""
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' VERIFICA SE JA EXISTE UM RECIBO EMITIDO PARA AS MESMAS ANALISES

' ----------------------------------------------------------------------
Private Function RECIBO_VerificaInsereRecibo(n_req As String, ByRef RegistosR As Collection, indice As Integer) As Boolean
    Dim ssql As String
    Dim rsRec As New adodb.recordset
    On Error GoTo TrataErro
    If RegistosR(indice).NumDoc = "" Then
        RECIBO_VerificaInsereRecibo = False
        Exit Function
    End If
    
    'SE JA TEM NUMERO DO RECIBO VERIFICA SE ESSE NUMERO JA EXISTE. SE EXISTIR NAO FAZ NADA
    If RegistosR(indice).NumDoc <> "0" Then
        ssql = "SeLECT * from sl_recibos WHERE n_Req = " & n_req & " AND serie_rec = " & BL_TrataStringParaBD(RegistosR(indice).SerieDoc)
        ssql = ssql & " AND n_Rec = " & BL_TrataStringParaBD(RegistosR(indice).NumDoc)
    Else
        ssql = "SeLECT * from sl_recibos WHERE n_Req = " & n_req & " AND n_Rec = " & BL_TrataStringParaBD(RegistosR(indice).NumDoc)
        ssql = ssql & " AND cod_efr = " & BL_TrataStringParaBD(RegistosR(indice).codEntidade)
    End If
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRec.Open ssql, gConexao
    If rsRec.RecordCount > 0 Then
        'BG_LogFile_Erros "RECIBO_VerificaInsereRecibo: " & sSql, "RECIBO", "RECIBO_VerificaInsereRecibo"
        RECIBO_VerificaInsereRecibo = False
        rsRec.Close
        Set rsRec = Nothing
        Exit Function
    End If
    rsRec.Close
    
    ssql = " SELECT *  FROM sl_recibos WHERE n_req = " & n_req & " AND seq_recibo = " & RegistosR(indice).SeqRecibo
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRec.Open ssql, gConexao
    If rsRec.RecordCount > 0 Then
        'BG_LogFile_Erros "RECIBO_VerificaInsereRecibo: " & sSql, "RECIBO", "RECIBO_VerificaInsereRecibo"
        RECIBO_VerificaInsereRecibo = False
        rsRec.Close
        Set rsRec = Nothing
        Exit Function
    End If
    rsRec.Close
    Set rsRec = Nothing
    RECIBO_VerificaInsereRecibo = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_VerificaInsereRecibo: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_VerificaInsereRecibo"
    RECIBO_VerificaInsereRecibo = False
    Exit Function
    Resume Next
End Function

Public Function RECIBO_RetornaSeqRecibo() As Long
    Dim ssql As String
    Dim rsDual As New adodb.recordset
    Dim j As Integer
    Dim ret As Long
    On Error GoTo TrataErro
    RECIBO_RetornaSeqRecibo = -1
    
    If gSGBD = gOracle Then
        ssql = "SELECT seq_recibo.nextval seq FROM dual "
        rsDual.CursorLocation = adUseServer
        rsDual.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsDual.Open ssql, gConexao
        If rsDual.RecordCount = 1 Then
            RECIBO_RetornaSeqRecibo = BL_HandleNull(rsDual!seq, -1)
        End If
        rsDual.Close
        Set rsDual = Nothing
    ElseIf gSGBD = gSqlServer Then
        j = 0
        ret = -1
        While j < 10 Or ret = -1
            ret = BL_GeraNumero("SEQ_RECIBO")
            j = j + 1
        Wend
        RECIBO_RetornaSeqRecibo = ret
    ElseIf gSGBD = gPostGres Then
        ssql = "SELECT nextval('seq_req_tubo') SEQ "
        rsDual.CursorLocation = adUseServer
        rsDual.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsDual.Open ssql, gConexao
        If rsDual.RecordCount = 1 Then
            RECIBO_RetornaSeqRecibo = BL_HandleNull(rsDual!seq, -1)
        End If
        rsDual.Close
        Set rsDual = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_RetornaSeqRecibo: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_RetornaSeqRecibo"
    RECIBO_RetornaSeqRecibo = -1
    Exit Function
    Resume Next
End Function

'RGONCALVES 14.07.2016 LRV-368
Private Function RECIBO_RetornaSeqReciboPorEntidade(ByVal cod_entidade As String, ByRef RegistosR As Collection) As Long 'ByVal estado As String,
    Dim i As Integer

    For i = 1 To RegistosR.Count
    'BRUNOSANTOS - 06.09.2016 - LRV-395 -> ADICIONADO  RegistosR(i).Estado
        If RegistosR(i).codEntidade = cod_entidade And RegistosR(i).Estado <> "A" Then
            RECIBO_RetornaSeqReciboPorEntidade = RegistosR(i).SeqRecibo
            Exit Function
        End If
    Next
    RECIBO_RetornaSeqReciboPorEntidade = -1
Exit Function
TrataErro:
    BG_LogFile_Erros "RECIBO_RetornaSeqReciboPorEntidade: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_RetornaSeqRecibo"
    RECIBO_RetornaSeqReciboPorEntidade = -1
    Exit Function
    Resume Next
End Function

Public Function RECIBO_Devolve_AtCode(ByVal cod_empresa As String, ByVal tipoDocumento As String) As String
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim ret As String
    Dim tipo As String
    Dim ano As Integer
    ret = "-1"
    
    If tipoDocumento = lRecibo Then
        tipo = "FAC_REC"
    ElseIf tipoDocumento = lDocCaixa Then
        tipo = "DOC_CAIXA"
    End If
    ano = Year(Bg_DaData_ADO)
    
    ssql = "SELECT cod_val_at  FROM sl_serie_doc WHERE tipo_doc = " & BL_TrataStringParaBD(tipo)
    ssql = ssql & " AND ano_doc = " & BL_TrataStringParaBD(CStr(ano)) & " AND empresa_id =" & BL_TrataStringParaBD(cod_empresa)
    rsRecibos.CursorLocation = adUseClient
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount = 1 Then
        ret = BL_HandleNull(rsRecibos!cod_val_at, "-1")
    End If
    rsRecibos.Close
    RECIBO_Devolve_AtCode = ret
    
Exit Function
TrataErro:
    RECIBO_Devolve_AtCode = ""
    BG_LogFile_Erros "RECIBO_Devolve_AtCode: " & Err.Number & " - " & Err.Description, "RECIBO", "RECIBO_Devolve_AtCode"
    Exit Function
    Resume Next
End Function

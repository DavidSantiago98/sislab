VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormHistoricoMicro 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9195
   Icon            =   "FormHistoricoMicro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   9195
   ShowInTaskbar   =   0   'False
   Begin MSFlexGridLib.MSFlexGrid FGHist 
      Height          =   4575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   8070
      _Version        =   393216
   End
End
Attribute VB_Name = "FormHistoricoMicro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.Caption = " Hist�rico de Microrganismos "
    Me.Left = 540
    Me.Top = 450
    Me.Width = 9225
    Me.Height = 5055 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = ""
'    Set CampoDeFocus = EcCodAnaS
    

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    

    Set FormHistoricoMicro = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus


End Sub

Sub DefTipoCampos()
    Inicializa_FGHist

    
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
'        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
'        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
'        LaDataCriacao = EcDataCriacao
'        LaDataAlteracao = EcDataAlteracao
    End If
End Sub


Sub FuncaoLimpar()

    
    
End Sub

Sub FuncaoEstadoAnterior()
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos

        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.Caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      

End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me

    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me

    End If
End Sub

Sub FuncaoInserir(Optional inibe_msg As Boolean)
    Dim iRes As Integer
    
'    If Avalia_Limites(BL_String2Double(EcVal_C0), BL_String2Double(EcVal_C1), EcVal_C2) = False Then
'        Exit Sub
'    End If
    
    If inibe_msg = False Then
        gMsgTitulo = "Inserir"
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    Else
        gMsgResp = vbYes
    End If
    
    Me.SetFocus
    
'    If EcUtilizadorCriacao.Text = "" Or EcDataCriacao.Text = "" Or EcHoraCriacao.Text = "" Then
'        BG_Mensagem mediMsgBox, "Utilizador / Data / Hora obrigat�rios!", vbInformation, gMsgTitulo
'        EcDataCriacao.SetFocus
'        Exit Sub
'    End If
    
    If gMsgResp = vbYes Then
'        BL_InicioProcessamento Me, "A inserir registo."
'        If EcUtilizadorCriacao.Text = "" Then
'            EcUtilizadorCriacao = gCodUtilizador
'        End If
'        If EcDataCriacao.Text = "" Then
'            EcDataCriacao = Bg_DaData_ADO
'        End If
'        If EcHoraCriacao.Text = "" Then
'            EcHoraCriacao = Bg_DaHora_ADO
'        End If
'        iRes = ValidaCamposEc
'
'        If iRes = True Then
'            BD_Delete
'            BD_Insert
'        End If
'        BL_FimProcessamento Me
    End If
    
    
    FuncaoProcurar

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    Dim i As Integer
    
    gSQLError = 0
    gSQLISAM = 0
    

'
    
    
End Sub

Sub FuncaoModificar(Optional inibe_msg As Boolean)
    Dim iRes As Integer
    Dim i As Integer
    Dim SQLQuery As String
    
'    If EcUtilizadorCriacao.Text = "" Or EcDataCriacao.Text = "" Or EcHoraCriacao.Text = "" Then
'        BG_Mensagem mediMsgBox, "Utilizador / Data / Hora obrigat�rios!", vbInformation, "Modificar"
'        EcDataCriacao.SetFocus
'        Exit Sub
'    End If
'

'
'    If inibe_msg = False Then
'        gMsgTitulo = "Modificar"
'        gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
'        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
'    Else
'        gMsgResp = vbYes
'    End If
'
'    If gMsgResp = vbYes Then
'        BL_InicioProcessamento Me, "A modificar registo."
'        EcDataAlteracao = Bg_DaData_ADO
'        EcUtilizadorAlteracao = gCodUtilizador
'        EcHoraAlteracao = Bg_DaHora_ADO
'        iRes = ValidaCamposEc
'        If iRes = True Then
'            BD_Delete
'            BD_Insert
'        End If
'        BL_FimProcessamento Me
'    End If
    
    
    Unload Me

End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    
End Sub

Sub Inicializa_FGHist()
    
End Sub




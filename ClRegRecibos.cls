VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClRegRecibos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public SeqRecibo As Long
Public NumDoc As String
Public SerieDoc As String
Public codEntidade As String
Public DescrEntidade As String
Public ValorPagar As Double
Public Caucao As Double
Public UserCri As String
Public DtCri As String
Public HrCri As String
Public UserEmi As String
Public DtEmi As String
Public HrEmi As String
Public DtPagamento As String
Public HrPagamento As String
Public flg_impressao As Integer
Public Estado As String
Public NumVendaDinheiro As String
Public Desconto As Double
Public NumAnalises As Integer
Public flg_DocCaixa As Integer
Public ValorOriginal As Double
Public n_ord As Integer
Public codEmpresa As String
Public flg_FdsFixo As Integer
Public NumCopias As Integer
Public Certificado As String
Public insere As Boolean
Public CodFormaPag As Integer
Public SeqUtente As Long
Public FlgBorla As Integer
Public FlgNaoConforme As Integer
Public AT_ValidNumber As String

VERSION 5.00
Begin VB.Form FormPesquisaUtentes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormPesquisaUtentes"
   ClientHeight    =   8730
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13410
   Icon            =   "FormPesquisaUtentes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8730
   ScaleWidth      =   13410
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcNumContrib 
      BackColor       =   &H00E9FEFD&
      Enabled         =   0   'False
      Height          =   285
      Left            =   11880
      Locked          =   -1  'True
      TabIndex        =   50
      TabStop         =   0   'False
      Top             =   7080
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcTelemovel 
      BackColor       =   &H00E9FEFD&
      Enabled         =   0   'False
      Height          =   285
      Left            =   11880
      Locked          =   -1  'True
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   6720
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcTelefone 
      BackColor       =   &H00E9FEFD&
      Enabled         =   0   'False
      Height          =   285
      Left            =   11880
      Locked          =   -1  'True
      TabIndex        =   46
      TabStop         =   0   'False
      Top             =   6360
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcCodGenero 
      BackColor       =   &H00E9FEFD&
      Height          =   285
      Left            =   3960
      TabIndex        =   44
      Top             =   7560
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcEmail 
      BackColor       =   &H00E9FEFD&
      Height          =   285
      Left            =   1320
      TabIndex        =   42
      Top             =   7200
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcIsencao 
      BackColor       =   &H00E9FEFD&
      Height          =   285
      Left            =   4560
      TabIndex        =   40
      Top             =   6360
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   6840
      TabIndex        =   34
      Top             =   6000
      Width           =   1215
   End
   Begin VB.TextBox EcSexoUte 
      BackColor       =   &H00E9FEFD&
      Height          =   285
      Left            =   4680
      TabIndex        =   28
      Top             =   5880
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcTipoUtente 
      Height          =   285
      Left            =   1920
      TabIndex        =   27
      Top             =   6240
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   2160
      TabIndex        =   25
      Top             =   5880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3180
      Left            =   240
      TabIndex        =   12
      Top             =   2280
      Width           =   13095
   End
   Begin VB.Frame Frame1 
      Height          =   1815
      Left            =   240
      TabIndex        =   13
      Top             =   120
      Width           =   13095
      Begin VB.TextBox EcPrescricao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5640
         TabIndex        =   38
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox EcCartaoUte 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9360
         TabIndex        =   37
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox EcReqAssoc 
         Height          =   285
         Left            =   9360
         TabIndex        =   36
         Top             =   960
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.ComboBox EcSituacao 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   9360
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox EcEpisodio 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   11520
         TabIndex        =   1
         Top             =   600
         Width           =   1455
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   840
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   960
         Width           =   1935
      End
      Begin VB.ComboBox EcDescrTipoUtente 
         Height          =   315
         Left            =   840
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDataInscr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12000
         TabIndex        =   7
         Tag             =   "7"
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1970
         TabIndex        =   3
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox EcProcHosp1 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9360
         TabIndex        =   10
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   840
         TabIndex        =   4
         Top             =   600
         Width           =   6135
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   6000
         TabIndex        =   6
         Tag             =   "7"
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcProcHosp2 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11160
         TabIndex        =   11
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox EcTexto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2810
         TabIndex        =   9
         Top             =   1320
         Width           =   10170
      End
      Begin VB.ComboBox EcCampoBD 
         Height          =   315
         Left            =   840
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1320
         Width           =   1935
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "N� Pedido"
         Height          =   195
         Left            =   4800
         TabIndex        =   39
         Top             =   240
         Width           =   720
      End
      Begin VB.Label LbReqAssoc 
         AutoSize        =   -1  'True
         Caption         =   "Req Associada"
         Height          =   195
         Left            =   9360
         TabIndex        =   35
         Top             =   960
         Visible         =   0   'False
         Width           =   1080
      End
      Begin VB.Label Label10 
         Caption         =   "Situa��o"
         Height          =   255
         Left            =   8040
         TabIndex        =   32
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label11 
         Caption         =   "Epis�dio"
         Height          =   255
         Left            =   10800
         TabIndex        =   31
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         Height          =   195
         Left            =   120
         TabIndex        =   30
         Top             =   960
         Width           =   360
      End
      Begin VB.Label Label5 
         Caption         =   "Data Inscri��o"
         Height          =   255
         Left            =   10800
         TabIndex        =   24
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Outros"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label LbUtente 
         AutoSize        =   -1  'True
         Caption         =   "Utente"
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   240
         Width           =   480
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Processos Hosp."
         Height          =   195
         Left            =   8040
         TabIndex        =   21
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   600
         Width           =   420
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data Nascimento"
         Height          =   195
         Left            =   4680
         TabIndex        =   19
         Top             =   960
         Width           =   1230
      End
      Begin VB.Label LbCartaoUte 
         AutoSize        =   -1  'True
         Caption         =   "Cart�o Utente"
         Height          =   195
         Left            =   7920
         TabIndex        =   18
         Top             =   960
         Width           =   990
      End
   End
   Begin VB.Label Label8 
      Caption         =   "EcNumContrib"
      Height          =   255
      Index           =   6
      Left            =   10680
      TabIndex        =   51
      Top             =   7080
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "EcTelemovel"
      Height          =   255
      Index           =   5
      Left            =   10680
      TabIndex        =   49
      Top             =   6720
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "EcTelefone"
      Height          =   255
      Index           =   4
      Left            =   10680
      TabIndex        =   47
      Top             =   6360
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "EcCodGenero"
      Height          =   255
      Index           =   3
      Left            =   2760
      TabIndex        =   45
      Top             =   7560
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "EcEmail"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   43
      Top             =   7200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "EcIsencao"
      Height          =   255
      Index           =   1
      Left            =   3360
      TabIndex        =   41
      Top             =   6360
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label LaImportado 
      Caption         =   "LaImportado"
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   6600
      TabIndex        =   33
      Top             =   2040
      Visible         =   0   'False
      Width           =   5535
   End
   Begin VB.Label Label8 
      Caption         =   "EcSexoUte"
      Height          =   255
      Index           =   0
      Left            =   3480
      TabIndex        =   29
      Top             =   5880
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Left            =   720
      TabIndex        =   26
      Top             =   5880
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Nome"
      Height          =   255
      Left            =   3120
      TabIndex        =   17
      Top             =   2040
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "Tipo"
      Height          =   255
      Left            =   2480
      TabIndex        =   16
      Top             =   2040
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "N�mero"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   2040
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "EctipoUtente"
      Height          =   255
      Left            =   720
      TabIndex        =   14
      Top             =   6240
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "FormPesquisaUtentes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String
Dim VectorCamposBD

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

'Comando e parametro utilizados para seleccionar tipos de estado civil
Dim CmdEstCiv As New ADODB.Command
Dim PmtEstCiv As ADODB.Parameter

'Comando e parametro utilizados para seleccionar codigo de profissao
Dim CmdProfis As New ADODB.Command
Dim PmtProfis As ADODB.Parameter

'Comando e parametro utilizados para seleccionar codigo de EFR
Dim CmdEFR As New ADODB.Command
Dim PmtEFR As ADODB.Parameter

'Comando e parametro utilizados para seleccionar codigo de isencao
Dim CmdIsenc As New ADODB.Command
Dim PmtIsenc As ADODB.Parameter

'Comando e parametro utilizados para seleccionar Seq_Utente a partir do n� da Requisi��o
Dim CmdRequis As New ADODB.Command

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    BL_LimpaDadosUtente
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    gEpisodioActivo = ""
    gSituacaoActiva = mediComboValorNull
    gReqAssocActiva = ""

    gEpisodioActivoAux = ""
    gSituacaoActivaAux = mediComboValorNull
    
    If gLAB = "BIO" Then
        EcReqAssoc.Visible = True
        LbReqAssoc.Visible = True
        EcCartaoUte.Visible = False
        LbCartaoUte.Visible = False
        EcReqAssoc.SetFocus
    Else
        EcReqAssoc.Visible = False
        LbReqAssoc.Visible = False
        EcCartaoUte.Visible = True
        LbCartaoUte.Visible = True
    End If
    
    If gF_IDENTIF = 1 Then
        If HIS.nome = cFACTURACAO_NOVAHIS Then
            EcPrescricao.BackColor = &HC0FFFF
            EcPrescricao.SetFocus
        Else
            EcSituacao.BackColor = &HC0FFFF
            EcEpisodio.BackColor = &HC0FFFF
            EcSituacao.SetFocus
        End If
    Else
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub EventoUnload()
    BL_Fecha_conexao_HIS
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then rs.Close
        Set rs = Nothing
    End If
    
    Set FormPesquisaUtentes = Nothing
    
    Set CmdEstCiv = Nothing
    Set PmtEstCiv = Nothing
    Set CmdProfis = Nothing
    Set PmtProfis = Nothing
    Set CmdEFR = Nothing
    Set PmtEFR = Nothing
    Set CmdIsenc = Nothing
    Set PmtIsenc = Nothing
    Set CmdRequis = Nothing
    
        
    
    
    If gF_REQUIS = 1 Then
        FormGestaoRequisicao.Enabled = True
    End If
    
    If gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.Enabled = True
    End If
    
    If gF_IDENTIF = 1 Then
        FormIdentificaUtente.Enabled = True
    End If
    
    If gF_LRES = 1 Then
        FormListarResultados.Enabled = True
    End If
    
    
    If gF_REQCONS = 1 Then
        FormGesReqCons.Enabled = True
    End If

End Sub

Sub PreencheComboCamposBD()
    
    EcCampoBD.AddItem "Estado C�vil"
    EcCampoBD.AddItem "Morada"
    EcCampoBD.AddItem "C�digo Postal"
    EcCampoBD.AddItem "Telefone"
    EcCampoBD.AddItem "Profiss�o"
    EcCampoBD.AddItem "Entidade Financeira"
    EcCampoBD.AddItem "Num. Benefici�rio"
    EcCampoBD.AddItem "Isen��o"
    EcCampoBD.AddItem "Observa��es"
    
    EcCampoBD.AddItem "N� Requisi��o"
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", EcDescrTipoUtente
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", EcSituacao, "1"
    PreencheComboCamposBD
    'EcSituacao.ListIndex = 0
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_nasc_ute", EcDataNasc, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_inscr", EcDataInscr, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub Inicializacoes()
    
    Me.caption = " Pesquisa Utentes"
    Me.left = 440
    Me.top = 350
    Me.Width = 13500
    Me.Height = 6340 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_identif"

    ' -----------------------------------------------
    ' PAULO COSTA 07/04/2003 CONSULTA ?
    ' -----------------------------------------------
    
    Set CampoDeFocus = EcNome
'    Set CampoDeFocus = Me.EcEpisodio
    
    ' -----------------------------------------------
    ' FIM PAULO COSTA 07/04/2003 CONSULTA ?
    ' -----------------------------------------------
    
    NumCampos = 19
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_utente"
    CamposBD(1) = "utente"
    CamposBD(2) = "t_utente"
    CamposBD(3) = "n_proc_1"
    CamposBD(4) = "n_proc_2"
    CamposBD(5) = "n_cartao_ute"
    CamposBD(6) = "dt_inscr"
    CamposBD(7) = "nome_ute"
    CamposBD(8) = "dt_nasc_ute"
    CamposBD(9) = "sexo_ute"
    CamposBD(10) = "n_epis"
    CamposBD(11) = "t_sit"
    CamposBD(12) = "n_prescricao"
    CamposBD(13) = "COD_ISENCAO_UTE"
    CamposBD(14) = "email"
    CamposBD(15) = "cod_genero"
    'ICIL-930
    CamposBD(16) = "telef_ute"
    CamposBD(17) = "telemovel"
    CamposBD(18) = "n_contrib_ute"
    
    ' Campos do Ecr�
    
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcUtente
    Set CamposEc(2) = EcTipoUtente
    Set CamposEc(3) = EcProcHosp1
    Set CamposEc(4) = EcProcHosp2
    Set CamposEc(5) = EcCartaoUte
    Set CamposEc(6) = EcDataInscr
    Set CamposEc(7) = EcNome
    Set CamposEc(8) = EcDataNasc
    Set CamposEc(9) = EcSexoUte
    Set CamposEc(10) = EcEpisodio
    Set CamposEc(11) = EcSituacao
    Set CamposEc(12) = EcPrescricao
    Set CamposEc(13) = EcIsencao
    Set CamposEc(14) = EcEmail
    Set CamposEc(15) = EcCodGenero
    'ICIL-930
    Set CamposEc(16) = EcTelefone
    Set CamposEc(17) = EcTelemovel
    Set CamposEc(18) = EcNumContrib
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_utente"
    Set ChaveEc = EcCodSequencial
    
    'inicializacao do comando ADO para seleccao dos tipos estado civil
    Set CmdEstCiv.ActiveConnection = gConexao
    CmdEstCiv.CommandType = adCmdText
    CmdEstCiv.CommandText = "SELECT cod_est_civ FROM sl_tbf_estciv WHERE UPPER(descr_est_civ) LIKE ?"
    CmdEstCiv.Prepared = True
    Set PmtEstCiv = CmdEstCiv.CreateParameter("DESCR_ESTCIV", adVarChar, adParamInput, 20)
    CmdEstCiv.Parameters.Append PmtEstCiv
    
    'inicializacao do comando ADO para seleccao dos codigos de profissao
    Set CmdProfis.ActiveConnection = gConexao
    CmdProfis.CommandType = adCmdText
    CmdProfis.CommandText = "SELECT cod_profis FROM sl_profis WHERE UPPER(descr_profis) LIKE ?"
    CmdProfis.Prepared = True
    Set PmtProfis = CmdProfis.CreateParameter("DESCR_PROFIS", adVarChar, adParamInput, 40)
    CmdProfis.Parameters.Append PmtProfis
    
    'inicializacao do comando ADO para seleccao dos codigos de EFR
    Set CmdEFR.ActiveConnection = gConexao
    CmdEFR.CommandType = adCmdText
    CmdEFR.CommandText = "SELECT cod_efr FROM sl_efr WHERE UPPER(descr_efr) LIKE ?"
    CmdEFR.Prepared = True
    Set PmtEFR = CmdEFR.CreateParameter("DESCR_EFR", adVarChar, adParamInput, 70)
    CmdEFR.Parameters.Append PmtEFR
    
    'inicializacao do comando ADO para seleccao dos codigos de isencao
    Set CmdIsenc.ActiveConnection = gConexao
    CmdIsenc.CommandType = adCmdText
    CmdIsenc.CommandText = "SELECT cod_isencao FROM sl_isencao WHERE UPPER(descr_isencao) LIKE ?"
    CmdIsenc.Prepared = True
    Set PmtIsenc = CmdIsenc.CreateParameter("DESCR_ISENC", adVarChar, adParamInput, 30)
    CmdIsenc.Parameters.Append PmtIsenc
    
    'inicializacao do comando ADO para seleccao do codigo sequencial do utente
    Set CmdRequis.ActiveConnection = gConexao
    CmdRequis.CommandType = adCmdText
    CmdRequis.CommandText = "SELECT seq_utente FROM sl_requis WHERE n_req=?"
    CmdRequis.Prepared = True
    CmdRequis.Parameters.Append CmdRequis.CreateParameter("N_REQ", adInteger, adParamInput, 7)
    
    If UCase(HIS.nome) = UCase("SONHO") Then
        CamposBDparaListBox = Array("utente", "t_utente", "nome_ute")
    Else
        CamposBDparaListBox = Array("utente", "t_utente", "nome_ute", "t_sit", "n_epis", "dt_episodio", "descr_sit")
    End If
    NumEspacos = Array(10, 5, 40, 10, 10, 15, 30)
    VectorCamposBD = Array("est_civ_ute", "descr_mor_ute", "cod_postal_ute", "telef_ute", "cod_profis_ute", "cod_efr_ute", "n_benef_ute", "cod_isencao_ute", "obs")
    

    
End Sub

Private Sub cmdOK_Click()

    If (EcLista.ListIndex <> -1) Then
        Call EcLista_DblClick
    Else
        Call FuncaoProcurar
    End If

End Sub

Private Sub EcCampoBD_Click()
    
    EcTexto.SetFocus

End Sub

Private Sub EcCampoBD_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcCampoBD.ListIndex = -1

End Sub

Private Sub EcCodSequencial_Change()
    
    If Trim(EcCodSequencial) = "-1" And gHIS_Import = 1 Then
        LaImportado.caption = "Utente importado do " & HIS.nome
        LaImportado.Visible = True
    Else
        LaImportado.caption = ""
        LaImportado.Visible = False
    End If

End Sub

Private Sub EcDataInscr_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInscr)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataNasc_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataNasc)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDescrSexo_Click()
    
    BL_ColocaComboTexto "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexoUte, EcDescrSexo

End Sub

Private Sub EcDescrTipoUtente_Click()
    
    BL_ColocaComboTexto "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente

End Sub

Sub FuncaoLimpar()
        
        EcLista.Clear
        LimpaCampos
        CampoDeFocus.SetFocus

End Sub

Sub FuncaoProcurar()
    
    Dim SelTotal As Boolean
    Dim SelCsi As Boolean
    Dim SimNao As Integer
    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim WhereAnd As String
    Dim HisAberto As Integer
    Dim RetPesquisaSONHO As Integer
    Dim situacao As String
    Dim NumEpisodio As String
    On Error GoTo TrataErro
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    
    If UCase(HIS.nome) = cFACTURACAO_NOVAHIS And Trim(EcPrescricao.Text) = "" Then
        Call BG_Mensagem(mediMsgBox, "Tem que indicar n�mero do pedido!", vbOKOnly + vbInformation, App.ProductName)
        Exit Sub
    ElseIf (UCase(HIS.nome) = UCase("SONHO") Or UCase(HIS.nome) = UCase("SISBIT") Or UCase(HIS.nome) = UCase("GH")) And _
            SelTotal = True And Trim(EcEpisodio.Text) = "" And EcSituacao.ListIndex = mediComboValorNull And EcReqAssoc.Text = "" Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos os utentes ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Exit Sub
        End If
        
        WhereAnd = " WHERE "
    Else
        WhereAnd = " AND "
    End If
        If EcCampoBD.ListIndex = 0 Then
            CmdEstCiv.Parameters(0).value = UCase(BG_CvWilcard(EcTexto.Text))
            Set Tabela = CmdEstCiv.Execute
            If Not Tabela.EOF Then
                CriterioTabela = CriterioTabela & WhereAnd & VectorCamposBD(EcCampoBD.ListIndex) & "='" & BL_HandleNull(Tabela!cod_est_civ, "'*'") & "'"
                Tabela.Close
                Set Tabela = Nothing
            Else
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo com estado civil '" & EcTexto.Text & "'!", vbInformation, "Procurar"
                FuncaoLimpar
                Tabela.Close
                Set Tabela = Nothing
                Exit Sub
            End If
        ElseIf EcCampoBD.ListIndex = 4 Then
            CmdProfis.Parameters(0).value = UCase(BG_CvWilcard(EcTexto.Text))
            Set Tabela = CmdProfis.Execute
            If Not Tabela.EOF Then
                CriterioTabela = CriterioTabela & WhereAnd & VectorCamposBD(EcCampoBD.ListIndex) & "='" & BL_HandleNull(Tabela!cod_profis, "'*'") & "'"
                Tabela.Close
                Set Tabela = Nothing
            Else
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo com profiss�o '" & EcTexto.Text & "'!", vbInformation, "Procurar"
                FuncaoLimpar
                Tabela.Close
                Set Tabela = Nothing
                Exit Sub
            End If
        ElseIf EcCampoBD.ListIndex = 5 Then
            CmdEFR.Parameters(0).value = UCase(BG_CvWilcard(EcTexto.Text))
            Set Tabela = CmdEFR.Execute
            If Not Tabela.EOF Then
                CriterioTabela = CriterioTabela & WhereAnd & VectorCamposBD(EcCampoBD.ListIndex) & "='" & BL_HandleNull(Tabela!cod_efr, "'*'") & "'"
                Tabela.Close
                Set Tabela = Nothing
            Else
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo com E.F.R. '" & EcTexto.Text & "'!", vbInformation, "Procurar"
                FuncaoLimpar
                Tabela.Close
                Set Tabela = Nothing
                Exit Sub
            End If
        ElseIf EcCampoBD.ListIndex = 7 Then
            CmdIsenc.Parameters(0).value = UCase(BG_CvWilcard(EcTexto.Text))
            Set Tabela = CmdIsenc.Execute
            If Not Tabela.EOF Then
                CriterioTabela = CriterioTabela & WhereAnd & VectorCamposBD(EcCampoBD.ListIndex) & "='" & BL_HandleNull(Tabela!cod_isencao, "'*'") & "'"
                Tabela.Close
                Set Tabela = Nothing
            Else
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo com isen��o '" & EcTexto.Text & "'!", vbInformation, "Procurar"
                FuncaoLimpar
                Tabela.Close
                Set Tabela = Nothing
                Exit Sub
            End If
        ElseIf EcCampoBD.ListIndex = 9 Then
            If Trim(EcTexto.Text) = "" Then
                Call BG_Mensagem(mediMsgBox, "Tem que indicar o n� da requisi��o!", vbOKOnly + vbInformation, App.ProductName)
                Exit Sub
            Else
                If Not IsNumeric(Trim(EcTexto.Text)) Then
                    Call BG_Mensagem(mediMsgBox, "Tem que indicar um valor num�rico!", vbOKOnly + vbInformation, App.ProductName)
                    Exit Sub
                Else
                    CmdRequis.Parameters("N_REQ").value = Trim(EcTexto.Text)
                    Set Tabela = CmdRequis.Execute
                    If Not Tabela.EOF Then
                        CriterioTabela = CriterioTabela & WhereAnd & "Seq_utente=" & Tabela!seq_utente & " "
                        Tabela.Close
                        Set Tabela = Nothing
                    Else
                        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo com requisi��o '" & EcTexto.Text & "'!", vbInformation, "Procurar"
                        FuncaoLimpar
                        Tabela.Close
                        Set Tabela = Nothing
                        Exit Sub
                    End If
                End If
            End If
        ElseIf EcCampoBD.ListIndex <> -1 Then
            CriterioTabela = CriterioTabela & WhereAnd & VectorCamposBD(EcCampoBD.ListIndex) & " LIKE '" & BG_CvWilcard(EcTexto.Text) & "'"
        ElseIf (EcSituacao.ListIndex <> mediComboValorNull And Trim(EcEpisodio.Text) <> "") Or EcReqAssoc.Text <> "" Then
            'CriterioTabela = CriterioTabela & " WHERE seq_utente=-9999
            'CriterioTabela = CriterioTabela & " WHERE seq_utente=-1"
            'CriterioTabela = CriterioTabela & WhereAnd & " t_sit='" & BG_DaComboSel(EcSituacao) & "' AND " & " n_epis='" & EcEpisodio.Text & "'"
            'soliveira 30.10.2007 HCVP para obrigar a ir ao GH
            CriterioTabela = CriterioTabela & WhereAnd & " seq_utente=-1"
        End If
        
        CriterioTabela = CriterioTabela & " order by nome_ute"
    
        CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                        "nome_ute", _
                                        True)
    
    'Estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
        If Not rs Is Nothing Then
            If rs.state <> adStateClosed Then
                rs.Close
            End If
            Set rs = Nothing
        End If
        
        Set rs = New ADODB.recordset
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
    
        BL_InicioProcessamento Me, "A pesquisar SISLAB."
        
'        HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
'
'        If HisAberto = 1 Then
'
        rs.Open CriterioTabela, gConexao
'        End If
        If rs.RecordCount <= 0 Or (gImportaSempreUtente = mediSim And EcNome = "") Then
            
            BL_FimProcessamento Me
            'BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo no SISLAB!", vbInformation, "Procurar"
            If gF_IDENTIF = 0 And gF_REQUIS = 0 Then      'soliveira 05-12-2003
                FuncaoLimpar
            ElseIf gHIS_Import = 1 Then
                SelCsi = True
                If SelTotal = True And (Trim(EcEpisodio.Text) = "" Or EcSituacao.ListIndex = mediComboValorNull) And (EcReqAssoc.Text = "" And Trim(EcEpisodio.Text) = "" And EcSituacao.ListIndex = mediComboValorNull) Then
                    If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa! Quer seleccionar todos os utentes do " & HIS.nome & " ?" & Chr(13) & "Esta opera��o pode demorar algum tempo!", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
                        SelCsi = False
                    End If
                End If
                
                If SelCsi = True Then
                    'IMPORTAR DO HIS
                    
                    BL_InicioProcessamento Me, "A pesquisar " & HIS.nome
                    
                    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
                    
                    If HisAberto = 1 Then
                        'NELSONPSILVA Glintt-HS-18011 09.02.2018 (sl_identif -> view do GH para importar utentes)
                        CriterioTabela = Replace(CriterioTabela, "slv_identif", "sl_identif")
                        '
                        If UCase(HIS.nome) = UCase("SONHO") Then
                            
                            If EcUtente.Text = "" And EcProcHosp1.Text = "" And _
                               EcSituacao.ListIndex = mediComboValorNull And _
                               Trim(EcEpisodio.Text) = "" Then
                                
                                BG_Mensagem mediMsgBox, "Tente indicar o n.� de utente ou n� processo ou a situa��o e o epis�dio", vbInformation, App.ProductName
                                EcEpisodio.SetFocus
                                BL_FimProcessamento Me
                                Exit Sub
                            
                            End If
                            
                            If EcSituacao.ListIndex <> mediComboValorNull And _
                               Trim(EcEpisodio.Text) = "" Then
                                
                                BG_Mensagem mediMsgBox, "Se indicar a situa��o tem de indicar o epis�dio", vbExclamation, App.ProductName
                                EcEpisodio.SetFocus
                                BL_FimProcessamento Me
                                Exit Sub
                            
                            End If
                            
                            If EcSituacao.ListIndex = mediComboValorNull And _
                               Trim(EcEpisodio.Text) <> "" Then
                                
                                BG_Mensagem mediMsgBox, "Se indicar o epis�dio tem de indicar a situa��o", vbExclamation, App.ProductName
                                EcSituacao.SetFocus
                                BL_FimProcessamento Me
                                Exit Sub
                            
                            End If

                            If EcSituacao.ListIndex <> mediComboValorNull Then
                                
                                situacao = ""
                                If EcSituacao.ItemData(EcSituacao.ListIndex) = gT_Urgencia Then
                                    situacao = "URG"
                                ElseIf EcSituacao.ItemData(EcSituacao.ListIndex) = gT_Consulta Then
                                    situacao = "CON"
                                ElseIf EcSituacao.ItemData(EcSituacao.ListIndex) = gT_Internamento Then
                                    situacao = "INT"
                                ElseIf EcSituacao.ItemData(EcSituacao.ListIndex) = gT_Externo Then
                                    situacao = "HDI"
                                ElseIf EcSituacao.ItemData(EcSituacao.ListIndex) = gT_LAB Then   'soliveira - Admiss�o directa ao laborat�rio
                                    situacao = "LAB"
                                ElseIf EcSituacao.ItemData(EcSituacao.ListIndex) = gT_Bloco Then
                                    situacao = "BLO"
                                ElseIf EcSituacao.ItemData(EcSituacao.ListIndex) = gT_RAD Then
                                    situacao = "RAD"
                                End If
                            End If
                            
                            RetPesquisaSONHO = SONHO_PesquisaUtente(EcUtente.Text, EcProcHosp1, EcEpisodio, situacao)
                            
                            If RetPesquisaSONHO = -1 Then
                                BG_Mensagem mediMsgBox, vbCrLf & _
                                                        "N�o foi seleccionado nenhum registo do " & HIS.nome & ".         " & _
                                                        vbCrLf & vbCrLf, vbExclamation, " " & App.ProductName
                                BL_FimProcessamento Me
                                Exit Sub
                            End If
                            CriterioTabela = "select * from sl_identif where cod_apl = " & gNumeroSessao
                        
                        ElseIf UCase(HIS.nome) = cFACTURACAO_NOVAHIS Then
                            
                            If Trim(EcPrescricao.Text) = "" Then
                                
                                BG_Mensagem mediMsgBox, "Se indicar o n�mero do pedido", vbExclamation, App.ProductName
                                BL_FimProcessamento Me
                                Exit Sub
                            
                            End If

                            CriterioTabela = "select * from sl_identif where n_prescricao = " & EcPrescricao
                        
                        ElseIf UCase(HIS.nome) = UCase("GH") And ((EcSituacao.ListIndex <> mediComboValorNull And EcEpisodio.Text <> "") Or EcReqAssoc.Text <> "") And gLAB <> "HMP" Then
                            
                            If gLAB = "BIO" Then
                                If EcReqAssoc.Text = "" Then
                                    
                                    BG_Mensagem mediMsgBox, "Tem de indicar o n�mero da requisi��o associada da GH", vbExclamation, App.ProductName
                                    EcReqAssoc.SetFocus
                                    BL_FimProcessamento Me
                                    Exit Sub
                                
                                End If
                            End If
       
                            If EcSituacao.ListIndex <> mediComboValorNull And Trim(EcEpisodio.Text) = "" Then
                                BG_Mensagem mediMsgBox, "Se indicar a situa��o tem de indicar o epis�dio", vbExclamation, App.ProductName
                                EcEpisodio.SetFocus
                                BL_FimProcessamento Me
                                Exit Sub
                            End If
    
                            If EcSituacao.ListIndex = mediComboValorNull And Trim(EcEpisodio.Text) <> "" Then
                                BG_Mensagem mediMsgBox, "Se indicar o epis�dio tem de indicar a situa��o", vbExclamation, App.ProductName
                                EcSituacao.SetFocus
                                BL_FimProcessamento Me
                                Exit Sub
                            End If
                            
                            'RetPesquisaGH = GH_PesquisaUtente(EcUtente.Text, EcEpisodio, situacao)
                            Dim RsPesquisaGH As ADODB.recordset
                            Dim SqlGH As String
                            Set RsPesquisaGH = New ADODB.recordset
                            With RsPesquisaGH
                                .CursorType = adOpenStatic
                                .CursorLocation = adUseServer
                                If gLAB = "BIO" Then
                                    .Source = "select distinct e.n_doc, e.t_doente, e.doente, e.episodio, e.dt_act_med, e.dt_cri, c.cod_u_saude " & _
                                                "from  sd_episod_act_med e, sd_cons_marc c " & _
                                                "where e.cod_serv_exec = '3' and e.n_doc = '" & EcReqAssoc.Text & _
                                                "' and  e.tipo_origem = 'CON' and (e.flg_estado is null or e.flg_estado <> 'A') " & _
                                                " and e.t_doente = c.t_doente and e.doente = c.doente and e.cod_serv_exec = c.cod_serv " & _
                                                " and e.dt_act_med = c.dt_cons "
                                Else
                                    Dim t_episodio As String
                                    ' Mapeia a situa��o do SISLAB para a GH.
                                    Select Case EcSituacao.ItemData(EcSituacao.ListIndex)
                                        Case gT_Urgencia
                                            t_episodio = "Urgencias"
                                        Case gT_Consulta
                                            t_episodio = "Consultas"
                                        Case gT_Internamento
                                            t_episodio = "Internamentos"
                                        Case gT_Externo
                                            t_episodio = "Hosp-Dia"
                                        Case gT_Ficha_ID
                                            t_episodio = "Ficha-ID"
                                        Case gT_Ambulatorio
                                            t_episodio = "Ambulatorio"
                                        Case gT_Acidente
                                            t_episodio = "Acidente"
                                        Case gT_Ambulatorio
                                            t_episodio = "Ambulatorio"
                                        Case gT_Cons_Inter
                                            t_episodio = "Cons-Inter"
                                        Case gT_Cons_Telef
                                            t_episodio = "Cons-Telef"
                                        Case gT_Consumos
                                            t_episodio = "Consumos"
                                        Case gT_Credenciais
                                            t_episodio = "Credenciais"
                                        Case gT_Diagnosticos
                                            t_episodio = "Diagnosticos"
                                        Case gT_Exame
                                            t_episodio = "Exame"
                                        Case gT_Fisio
                                            t_episodio = "Fisio"
                                        Case gT_HDI
                                            t_episodio = "Hosp-Dia"
                                        Case gT_Intervencao
                                            t_episodio = "Intervencao"
                                        Case gT_MCDT
                                            t_episodio = "Mcdt"
                                        Case gT_Plano_Oper
                                            t_episodio = "Plano-Oper"
                                        Case gT_Pre_Intern
                                            t_episodio = "Pre-Intern"
                                        Case gT_Prog_Cirugico
                                            t_episodio = "Prog-Cirurgico"
                                        Case gT_Protoc
                                            t_episodio = "Protoc"
                                        Case gT_Referenciacao
                                            t_episodio = "Referenciacao"
                                        Case gT_Reg_Oper
                                            t_episodio = "Reg-Oper"
                                        Case gT_Tratamentos
                                            t_episodio = "Tratamentos"
                                        Case Else
                                            t_episodio = ""
                                    End Select
                                    If t_episodio <> "" Then
                                        .Source = "select e.t_doente, e.doente " & _
                                                    "from  sd_episodio e " & _
                                                    "where e.episodio = " & BL_TrataStringParaBD(EcEpisodio.Text) & _
                                                    " and  e.t_episodio = " & BL_TrataStringParaBD(t_episodio) & " AND flg_estado IS NULL "
                                    Else
                                        BG_Mensagem mediMsgBox, vbCrLf & _
                                                                "Erro na selec��o do doente do " & HIS.nome & ".         " & _
                                                                vbCrLf & vbCrLf, vbExclamation, " " & App.ProductName
                                        BL_FimProcessamento Me
                                        Exit Sub
                                    End If
                                End If
                                .ActiveConnection = gConnHIS
                                .Open
                            End With
                            
                            If RsPesquisaGH.RecordCount <= 0 Then
                            'If RetPesquisaGH = -1 Then
                                BG_Mensagem mediMsgBox, vbCrLf & _
                                                        "N�o foi seleccionado nenhum registo do " & HIS.nome & ".         " & _
                                                        vbCrLf & vbCrLf, vbExclamation, " " & App.ProductName
                                BL_FimProcessamento Me
                                Exit Sub
                            Else
                                If gLAB = "BIO" Then
                                    gReqAssocActiva = EcReqAssoc.Text
                                    EcSituacao.ListIndex = gT_Consulta
                                    EcEpisodio.Text = RsPesquisaGH!episodio
                                End If

                                CriterioTabela = "select * from sl_identif where t_utente = " & BL_TrataStringParaBD(RsPesquisaGH!t_doente) & _
                                                " and utente = " & BL_TrataStringParaBD(RsPesquisaGH!doente) & ""
                                    
                                If EcSituacao.ListIndex <> mediComboValorNull And Trim(EcEpisodio.Text) <> "" Then
                                    CriterioTabela = CriterioTabela & " AND t_sit = " & BG_DaComboSel(EcSituacao) & " AND n_epis = " & BL_TrataStringParaBD(EcEpisodio)
                                End If
                            End If
                            
                        End If
                                        
                        rs.Close
                    
                        If EcSituacao.ListIndex <> mediComboValorNull And _
                           Trim(EcEpisodio.Text) = "" Then
                            
                            BG_Mensagem mediMsgBox, "Se indicar a situa��o tem de indicar o epis�dio", vbExclamation, App.ProductName
                            EcEpisodio.SetFocus
                            BL_FimProcessamento Me
                            Exit Sub
                        
                        End If
                        
                        If Trim(EcPrescricao.Text) <> "" And HIS.nome <> cFACTURACAO_NOVAHIS Then
                            BG_Mensagem mediMsgBox, "Se indicar o epis�dio tem de indicar a situa��o", vbExclamation, App.ProductName
                            EcSituacao.SetFocus
                            BL_FimProcessamento Me
                            Exit Sub
                        
                        End If
                        
                        With rs
                            .Source = CriterioTabela
                            .CursorType = adOpenStatic
                            .CursorLocation = adUseServer
                            .Open , gConnHIS
                        End With
                        
                        If rs.RecordCount <= 0 Then
                            BL_FimProcessamento Me
                            BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo do " & HIS.nome & "!", vbInformation, "Procurar"
                        Else
                            
                            gEpisodioActivo = EcEpisodio.Text
                            If EcSituacao.ListIndex > -1 Then
                                gSituacaoActiva = EcSituacao.ItemData(EcSituacao.ListIndex)
                            Else
                                gSituacaoActiva = -1
                            End If
                            gReqAssocActiva = EcReqAssoc.Text
                            
                            gEpisodioActivoAux = EcEpisodio.Text
                            If EcSituacao.ListIndex > -1 Then
                                gSituacaoActiva = EcSituacao.ItemData(EcSituacao.ListIndex)
                            Else
                                gSituacaoActiva = -1
                            End If
                           
                            Dim Ep2 As String
                            Dim Sit2 As String
                            Ep2 = gEpisodioActivo
                            Sit2 = gSituacaoActiva
                            
                            LimpaCampos
                            
                            ' Inicio do preenchimento de 'EcLista'
                            MarcaInicial = 1
                            BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
                            ' Fim do preenchimento de 'EcLista'
                
                            PreencheCampos
                            If UCase(HIS.nome) <> cFACTURACAO_NOVAHIS Then
                                gEpisodioActivo = Ep2
                                gSituacaoActiva = Sit2
                                Ep2 = ""
                                Sit2 = ""
                                
                                EcEpisodio.Text = gEpisodioActivo
                                ' CUIDADO
                                BL_SeleccionaValorCombo CStr(gSituacaoActiva), EcSituacao
                                'EcSituacao.ListIndex = gSituacaoActiva
                            Else
                                gEpisodioActivo = EcEpisodio.Text
                                If EcSituacao.ListIndex > -1 Then
                                    gSituacaoActiva = EcSituacao.ItemData(EcSituacao.ListIndex)
                                Else
                                    gSituacaoActiva = -1
                                End If
                            End If
                            
                            BL_Toolbar_BotaoEstado "Anterior", "Activo"
                            BL_Toolbar_BotaoEstado "Seguinte", "Activo"
                            BL_FimProcessamento Me
                        End If
                        
                        
                        ' Apaga o registo da tabela tempor�ria do SONHO.
                        If UCase(HIS.nome) = UCase("SONHO") Then
                             Dim rsAux As ADODB.recordset
                             Set rsAux = New ADODB.recordset
                             
                             sql = "DELETE FROM sl_identif " & _
                                   "WHERE cod_apl = " & gNumeroSessao
                            
                             With rsAux
                                 .Source = sql
                                 .CursorType = adOpenForwardOnly
                                 .CursorLocation = adUseServer
                                 .Open , gConnHIS
                             End With
                             
                             Set rsAux = Nothing
                        End If
                        
                    Else
                        BL_FimProcessamento Me
                    End If
                    
                End If
            End If
        Else
            gEpisodioActivo = EcEpisodio.Text
            If EcSituacao.ListIndex > -1 Then
                gSituacaoActiva = EcSituacao.ItemData(EcSituacao.ListIndex)
            Else
                gSituacaoActiva = -1
            End If
            gReqAssocActiva = EcReqAssoc.Text
        
            gEpisodioActivoAux = EcEpisodio.Text
            If EcSituacao.ListIndex > -1 Then
                gSituacaoActivaAux = EcSituacao.ItemData(EcSituacao.ListIndex)
            Else
                gSituacaoActivaAux = -1
            End If
            
            LimpaCampos
            

            ' Inicio do preenchimento de 'EcLista'
            MarcaInicial = 1
            BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
            ' Fim do preenchimento de 'EcLista'

            'PreencheCampos
            BL_Toolbar_BotaoEstado "Anterior", "Activo"
            BL_Toolbar_BotaoEstado "Seguinte", "Activo"
            BL_FimProcessamento Me
        End If
Exit Sub
TrataErro:
    BL_FimProcessamento Me
    BG_LogFile_Erros "Erro ao Procurar Registos: " & CriterioTabela & " " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRecADSE", True
    Exit Sub
    Resume Next
End Sub


Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Sub PreencheCampos()
        
    'Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexoUte, EcDescrSexo
        BL_ColocaTextoCombo "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente
        If EcSituacao.ListIndex > -1 Then
            gSituacaoActiva = EcSituacao.ItemData(EcSituacao.ListIndex)
        Else
            gSituacaoActiva = -1
        End If
        gEpisodioActivo = EcEpisodio.Text
        'EcSituacao.ListIndex = gSituacaoActiva
        'EcEpisodio.Text = gEpisodioActivo
        If gLAB = "BIO" Then
            EcReqAssoc.Text = gReqAssocActiva
        End If
    End If
    
End Sub

Sub LimpaCampos()
    
    'Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcDescrTipoUtente.ListIndex = mediComboValorNull
'    EcCampoBD.ListIndex = mediComboValorNull
'    EcTexto.Text = ""
    EcDescrSexo.ListIndex = -1
    
    EcSituacao.ListIndex = mediComboValorNull
    EcEpisodio.Text = ""
    EcReqAssoc.Text = ""

End Sub

Private Sub EcEpisodio_GotFocus()
    
    cmdOK.Default = True

End Sub

Private Sub EcEpisodio_LostFocus()
    
    cmdOK.Default = False

End Sub
Private Sub EcPrescricao_gotfocus()
    
    cmdOK.Default = True

End Sub

Private Sub EcPrescricao_LostFocus()
    
    cmdOK.Default = False

End Sub
Private Sub EcReqAssoc_GotFocus()
    
    cmdOK.Default = True

End Sub

Private Sub EcReqAssoc_LostFocus()
    
    cmdOK.Default = False

End Sub
Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        
'        On Error Resume Next
        Dim Ep2 As String
        Dim Sit2 As String
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        If HIS.nome = "SONHO" Then
            Ep2 = gEpisodioActivo
            Sit2 = gSituacaoActiva
            PreencheCampos
        Else
            PreencheCampos
            Ep2 = gEpisodioActivo
            Sit2 = gSituacaoActiva
        End If
        gEpisodioActivo = Ep2
        gSituacaoActiva = Sit2
        gEpisodioActivoAux = Ep2
        gSituacaoActivaAux = Sit2
        Ep2 = ""
        Sit2 = ""
        EcEpisodio.Text = gEpisodioActivo
        'cuidado
        BL_SeleccionaValorCombo CStr(gSituacaoActiva), EcSituacao
        'EcSituacao.ListIndex = gSituacaoActiva
    
    End If

End Sub

Private Sub EcLista_DblClick()
    
    Dim est_civil As String
    Dim descr_est_civil As String
    Dim cod_efr As String
    Dim nr_benef As String
    Dim morada As String
    Dim cod_postal As String
    Dim telefone As String
    Dim cod_profis As String
    Dim isencao As String
    Dim RsCodPostal As ADODB.recordset
    Dim CodigoCSI As String
    Dim DescrCSI As String
    Dim sql As String
    Dim n_epis As String
    Dim t_sit As String
    Dim email As String
    Dim cod_genero As String
    On Error GoTo Trata_Erro
    
    est_civil = ""
    descr_est_civil = ""
    cod_efr = ""
    nr_benef = ""
    morada = ""
    cod_postal = ""
    telefone = ""
    cod_profis = ""
    isencao = ""
    email = ""
    BL_LimpaDadosUtente
    
    If Not rs Is Nothing Then
        If rs.state = adStateOpen Then
            If rs.RecordCount >= 0 Then
                est_civil = BL_HandleNull(rs!est_civ_ute, "")
                'If est_civil <> "" Then descr_est_civil = BL_SelCodigo("sl_tbf_estciv", "cod_est_civ", "descr_est_civ", est_civil)
                If HIS.nome = cFACTURACAO_NOVAHIS Then
                    cod_efr = IN_DevolveCodigo("sl_efr", "sl_ass_efr_locais", "cod_efr", BL_HandleNull(rs!cod_efr_ute, "0"))
                Else
                    cod_efr = BL_HandleNull(rs!cod_efr_ute, "0")
                End If
                nr_benef = BL_HandleNull(rs!n_benef_ute, "")
                morada = BL_HandleNull(rs!descr_mor_ute, "")
                email = BL_HandleNull(rs!email, "")
                cod_genero = BL_HandleNull(rs!cod_genero, CStr(gCodGeneroDefeito))
                cod_postal = BL_HandleNull(rs!cod_postal_ute, "")
                isencao = BL_HandleNull(rs!cod_isencao_ute, "")
                If (Trim(HIS.nome) = "CSI") And _
                   (gHIS_Import = 1) And _
                   (Trim(EcCodSequencial.Text) = "-1") Then
                    
                    'Utentes do CSI v�m com o sequencial no c�digo postal!
                    'Para as liga��es CSI tem que haver uma view para a tabela tcodp do CSI
                    'ou uma tabela csi_cod_postal com os dados importados
                    '    csi_cod_postal:
                    '    SEQ_POSTAL Number(9)
                    '    COD_POSTAL VARCHAR2(15) (igual a: codp_num & '-' & codps_cod)
                    '    DESCR_POSTAL VARCHAR2(50)
                    
                    Set RsCodPostal = New ADODB.recordset
                    RsCodPostal.CursorLocation = adUseServer
                    RsCodPostal.CursorType = adOpenStatic
                    
                    sql = "SELECT " & _
                          "     codp_num, " & _
                          "     codp_scod, " & _
                          "     codp_desc as descr_postal " & _
                          "FROM " & _
                          "     csi.tcodp " & _
                          "WHERE " & _
                          "     codp_cod = " & BL_HandleNull(rs!cod_postal_ute)
                    
                    RsCodPostal.Open sql, gConnHIS
                    
                    If RsCodPostal.state = adStateOpen Then
                        If RsCodPostal.RecordCount <= 0 Then
                            
                            'N�o encontrou na view tcodp tenta localmente:
                            RsCodPostal.Close
                            
                            sql = "SELECT " & _
                                  "     cod_postal, " & _
                                  "     descr_postal " & _
                                  "FROM " & _
                                  "     csi_cod_postal " & _
                                  "WHERE " & _
                                  "     seq_postal = " & BL_HandleNull(rs!cod_postal_ute)
                            
                            RsCodPostal.Open sql, gConexao
                            If RsCodPostal.RecordCount <= 0 Then
                                'N�o encontrou na csi_cod_postal -> n�o existe
                                cod_postal = ""
                                CodigoCSI = ""
                                DescrCSI = ""
                            Else
                                CodigoCSI = BL_HandleNull(RsCodPostal!cod_postal, "")
                                DescrCSI = BL_HandleNull(RsCodPostal!descr_postal, "")
                            End If
                        Else
                            CodigoCSI = BL_HandleNull(RsCodPostal!codp_num, "") & "-" & BL_HandleNull(RsCodPostal!codp_scod, "")
                            DescrCSI = BL_HandleNull(RsCodPostal!descr_postal, "")
                        End If
                        RsCodPostal.Close
                    Else
                        'View tcodp inexistente -> tenta localmente:
                        
                        sql = "SELECT " & _
                              "     cod_postal, descr_postal " & _
                              "FROM " & _
                              "     csi_cod_postal " & _
                              "WHERE " & _
                              "     seq_postal = " & BL_HandleNull(rs!cod_postal_ute)
                        
                        RsCodPostal.Open sql, gConexao
                        If RsCodPostal.RecordCount <= 0 Then
                            'N�o encontrou na csi_cod_postal -> n�o existe
                            cod_postal = ""
                            CodigoCSI = ""
                            DescrCSI = ""
                        Else
                            CodigoCSI = BL_HandleNull(RsCodPostal!cod_postal, "")
                            DescrCSI = BL_HandleNull(RsCodPostal!descr_postal, "")
                        End If
                        RsCodPostal.Close
                    End If
                    
                    If Right(Trim(CodigoCSI), 1) = "-" Then
                        'N�o traz o codigo da rua -> procura pela descri��o (se existir)
                        'um codigo com rua
                        If Trim(DescrCSI) <> "" Then
                            
                            sql = "SELECT " & _
                                  "     cod_postal " & _
                                  "FROM " & _
                                  "     sl_cod_postal " & _
                                  "WHERE " & _
                                  "     descr_postal LIKE '" & DescrCSI & "%'"
                            
                            RsCodPostal.Open sql, gConexao
                            If RsCodPostal.RecordCount > 0 Then
                                'Encontrou -> atribui o primeiro da busca
                                cod_postal = BL_HandleNull(RsCodPostal!cod_postal, "")
                            Else
                                'n�o encontrou nenhuma designacao igual -> limpa o codigo postal
                                cod_postal = ""
                            End If
                        Else
                            'n�o tem designacao -> limpa o codigo postal
                            cod_postal = ""
                        End If
                    Else
                        'Tem o c�digo postal completo
                        cod_postal = Trim(CodigoCSI)
                    End If
                        
                    If RsCodPostal.state = adStateOpen Then RsCodPostal.Close
                    Set RsCodPostal = Nothing
                    
                End If
                
                cod_profis = BL_HandleNull(rs!cod_profis_ute, "")
            End If
        End If
    End If
    
    BL_PreencheDadosUtente EcCodSequencial.Text, EcUtente.Text, EcDescrTipoUtente.Text, EcProcHosp1.Text, _
                           EcProcHosp2.Text, EcCartaoUte.Text, EcDataInscr.Text, EcNome.Text, EcDataNasc.Text, _
                           EcSexoUte.Text, EcDescrSexo.Text, EcTelefone, est_civil, descr_est_civil, cod_efr, _
                           nr_benef, morada, cod_postal, cod_profis, gSituacaoActiva, gEpisodioActivo, EcTelemovel, _
                           EcPrescricao, EcIsencao, email, cod_genero, EcNumContrib.Text
    If HIS.nome = cFACTURACAO_NOVAHIS Then
        IN_TransferePedidoParaSISLAB EcUtente, CLng(EcPrescricao)
        If EcPrescricao.Text <> "" Then
            sql = "UPDATE SL_IDENTIF set n_prescricao = " & BL_TrataStringParaBD(EcPrescricao.Text) & "WHERE t_utente = " & BL_TrataStringParaBD(EcDescrTipoUtente.Text)
            sql = sql & " AND utente = " & BL_TrataStringParaBD(EcUtente.Text)
            BG_ExecutaQuery_ADO sql
        End If
    End If
    Unload Me
    
    Exit Sub
    
Trata_Erro:
    If Err.Number = -2147217865 Then
        'Tabela inexistente
        Resume Next
    End If
    Exit Sub
    Resume Next
    Unload Me

End Sub

Private Sub EcLista_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        EcLista_Click
        Call EcLista_DblClick
    End If
    
End Sub

Private Sub EcNome_LostFocus()
    
    If gNomeCapitalizado = 1 Then
        EcNome.Text = StrConv(EcNome.Text, vbProperCase)
    Else
        EcNome.Text = UCase(EcNome.Text)
    End If

End Sub



Private Sub EcSituacao_KeyDown(KeyCode As Integer, Shift As Integer)

    BG_LimpaOpcao EcSituacao, KeyCode
    
End Sub


Private Sub EcTexto_GotFocus()
    
    EcTexto.SelStart = 0
    EcTexto.SelLength = Len(EcTexto)

End Sub

Private Sub Form_Activate()
    
    EventoActivate

    ' -----------------------------------------------
    ' PAULO COSTA 07/04/2003 CONSULTA ?
    ' -----------------------------------------------
    
'    Me.EcSituacao.ListIndex = 0
    
    ' -----------------------------------------------
    ' FIM PAULO COSTA 07/04/2003 CONSULTA ?
    ' -----------------------------------------------

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    If UnloadMode <> 0 Then
        If Trim(gDUtente.seq_utente) = "" Then
            BL_PreencheDadosUtente EcCodSequencial.Text, EcUtente.Text, EcDescrTipoUtente.Text, _
                                    EcProcHosp1.Text, EcProcHosp2.Text, EcCartaoUte.Text, EcDataInscr.Text, _
                                    EcNome.Text, EcDataNasc.Text, EcSexoUte.Text, EcDescrSexo.Text, , , , , , , , , gSituacaoActiva, gEpisodioActivo, , , , EcEmail, EcCodGenero
        End If
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub




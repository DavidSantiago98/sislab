VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormExames 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormExames"
   ClientHeight    =   8190
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13095
   Icon            =   "FormExames.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8190
   ScaleWidth      =   13095
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcColunaPreco 
      Height          =   285
      Left            =   11400
      TabIndex        =   90
      Top             =   11400
      Width           =   615
   End
   Begin VB.TextBox EcLinhaPreco 
      Height          =   285
      Left            =   11400
      TabIndex        =   89
      Top             =   11040
      Width           =   615
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   360
      TabIndex        =   16
      Top             =   6090
      Width           =   12615
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Cancelada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   375
      Left            =   8640
      TabIndex        =   52
      Top             =   120
      Width           =   2775
   End
   Begin VB.TextBox EcAbreviatura 
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   9615
      TabIndex        =   19
      Top             =   180
      Width           =   1785
   End
   Begin VB.CommandButton BtAnaPerfis 
      Caption         =   "An�lises "
      Height          =   615
      Left            =   11760
      Picture         =   "FormExames.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   22
      ToolTipText     =   " Atribui��o de An�lises ao Perfil "
      Top             =   0
      Width           =   975
   End
   Begin VB.ComboBox EcLocal 
      Height          =   315
      Left            =   3000
      Style           =   2  'Dropdown List
      TabIndex        =   57
      Top             =   12240
      Width           =   4095
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5055
      Left            =   120
      TabIndex        =   55
      Top             =   600
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   8916
      _Version        =   393216
      Tab             =   2
      TabHeight       =   520
      TabCaption(0)   =   "Dados Exame"
      TabPicture(0)   =   "FormExames.frx":08D6
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Label55(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label55(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "EcAnaLocaisExec"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "EcAnaLocais"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "BtSinonimos"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Frame3"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "FrameSinon"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Factura��o"
      TabPicture(1)   =   "FormExames.frx":08F2
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label12"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label1(46)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Label100"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "FgPrecos"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "EcAuxPreco"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "EcCodRubrica"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "EcDescrRubrica"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "BtPesquisaRubrica"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "BtAdicionarRubrica"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "BtGravarAnaFact"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "BtCopiaPrecario"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "CkFlgfacturarAna"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "CkFlgRegra"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "CkContaMembros"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "EcQtd"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "EcRubrSeqAna"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "EcCodAnaFacturar"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "EcCodAnaRegra"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "FrameRubrica"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).ControlCount=   19
      TabCaption(2)   =   "Informa��o"
      TabPicture(2)   =   "FormExames.frx":090E
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Label21(2)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Label21(1)"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "Label21(0)"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "Label26"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "Label57(1)"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "Label1(54)"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "Label1(58)"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "Label1(40)"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "Label1(59)"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "Label1(3)"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).Control(10)=   "Label1(10)"
      Tab(2).Control(10).Enabled=   0   'False
      Tab(2).Control(11)=   "EcPrazoVal"
      Tab(2).Control(11).Enabled=   0   'False
      Tab(2).Control(12)=   "EcOrdemARS"
      Tab(2).Control(12).Enabled=   0   'False
      Tab(2).Control(13)=   "EcPeso"
      Tab(2).Control(13).Enabled=   0   'False
      Tab(2).Control(14)=   "EcCodEstatistica"
      Tab(2).Control(14).Enabled=   0   'False
      Tab(2).Control(15)=   "CbSexo"
      Tab(2).Control(15).Enabled=   0   'False
      Tab(2).Control(16)=   "CkFacturar"
      Tab(2).Control(16).Enabled=   0   'False
      Tab(2).Control(17)=   "EcGarrafa"
      Tab(2).Control(17).Enabled=   0   'False
      Tab(2).Control(18)=   "EcActivo"
      Tab(2).Control(18).Enabled=   0   'False
      Tab(2).Control(19)=   "EcListaFolhasTrab"
      Tab(2).Control(19).Enabled=   0   'False
      Tab(2).Control(20)=   "EcInformacao"
      Tab(2).Control(20).Enabled=   0   'False
      Tab(2).Control(21)=   "EcPrazoConc"
      Tab(2).Control(21).Enabled=   0   'False
      Tab(2).Control(22)=   "EcQuestoes"
      Tab(2).Control(22).Enabled=   0   'False
      Tab(2).Control(23)=   "CkGrafico"
      Tab(2).Control(23).Enabled=   0   'False
      Tab(2).Control(24)=   "EcMeios"
      Tab(2).Control(24).Enabled=   0   'False
      Tab(2).Control(25)=   "CkInibeDescricao"
      Tab(2).Control(25).Enabled=   0   'False
      Tab(2).Control(26)=   "EcDescrEtiq"
      Tab(2).Control(26).Enabled=   0   'False
      Tab(2).Control(27)=   "CkObrigaInfClin"
      Tab(2).Control(27).Enabled=   0   'False
      Tab(2).Control(28)=   "CkObrigaTerap"
      Tab(2).Control(28).Enabled=   0   'False
      Tab(2).Control(29)=   "CkAcreditacao"
      Tab(2).Control(29).Enabled=   0   'False
      Tab(2).ControlCount=   30
      Begin VB.CheckBox CkAcreditacao 
         Caption         =   "An�lise Acreditada"
         Height          =   255
         Left            =   4680
         TabIndex        =   157
         Top             =   2800
         Width           =   2415
      End
      Begin VB.CheckBox CkObrigaTerap 
         Caption         =   "ObrigaTerap�utica "
         Height          =   255
         Left            =   4680
         TabIndex        =   156
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   2500
         Width           =   3615
      End
      Begin VB.CheckBox CkObrigaInfClin 
         Caption         =   "Obriga Inf. Clinica"
         Height          =   255
         Left            =   4680
         TabIndex        =   155
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   2200
         Width           =   3615
      End
      Begin VB.TextBox EcDescrEtiq 
         Height          =   285
         Left            =   2040
         TabIndex        =   153
         Top             =   3240
         Width           =   735
      End
      Begin VB.CheckBox CkInibeDescricao 
         Caption         =   "Inibe designa��o no relat�rio"
         Height          =   255
         Left            =   4680
         TabIndex        =   152
         Top             =   1900
         Width           =   2415
      End
      Begin VB.ListBox EcMeios 
         Appearance      =   0  'Flat
         Height          =   1380
         Left            =   4200
         Style           =   1  'Checkbox
         TabIndex        =   150
         Top             =   3120
         Width           =   3615
      End
      Begin VB.CheckBox CkGrafico 
         Caption         =   "Gr�fico"
         Height          =   195
         Left            =   4680
         TabIndex        =   149
         Top             =   1600
         Width           =   1335
      End
      Begin VB.ListBox EcQuestoes 
         Appearance      =   0  'Flat
         Height          =   1380
         Left            =   9000
         Style           =   1  'Checkbox
         TabIndex        =   147
         Top             =   3120
         Width           =   3615
      End
      Begin VB.TextBox EcPrazoConc 
         Height          =   285
         Left            =   2040
         TabIndex        =   141
         Top             =   2880
         Width           =   735
      End
      Begin VB.ListBox EcInformacao 
         Appearance      =   0  'Flat
         Height          =   1155
         Left            =   9000
         Style           =   1  'Checkbox
         TabIndex        =   139
         Top             =   1800
         Width           =   3615
      End
      Begin VB.ListBox EcListaFolhasTrab 
         Appearance      =   0  'Flat
         Height          =   1155
         Left            =   9000
         Style           =   1  'Checkbox
         TabIndex        =   137
         Top             =   480
         Width           =   3615
      End
      Begin VB.CheckBox EcActivo 
         Caption         =   "Perfil activo"
         Height          =   195
         Left            =   4680
         TabIndex        =   136
         Top             =   1300
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CheckBox EcGarrafa 
         Caption         =   "Identifica Garrafa"
         Height          =   255
         Left            =   4680
         TabIndex        =   135
         Top             =   700
         Width           =   1575
      End
      Begin VB.CheckBox CkFacturar 
         Caption         =   "An�lise a facturar"
         Height          =   255
         Left            =   4680
         TabIndex        =   134
         Top             =   1000
         Width           =   1815
      End
      Begin VB.ComboBox CbSexo 
         Height          =   315
         Left            =   2040
         Style           =   2  'Dropdown List
         TabIndex        =   128
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox EcCodEstatistica 
         Height          =   285
         Left            =   2040
         TabIndex        =   127
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox EcPeso 
         Height          =   285
         Left            =   2040
         TabIndex        =   126
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox EcOrdemARS 
         Height          =   285
         Left            =   2040
         TabIndex        =   125
         Top             =   2100
         Width           =   735
      End
      Begin VB.TextBox EcPrazoVal 
         Height          =   285
         Left            =   2040
         TabIndex        =   124
         Top             =   2460
         Width           =   735
      End
      Begin VB.Frame FrameSinon 
         Caption         =   "Sin�nimos"
         Height          =   4260
         Left            =   -67920
         TabIndex        =   93
         Top             =   4800
         Visible         =   0   'False
         Width           =   12735
         Begin VB.CommandButton BtAdicSinonimo 
            Height          =   375
            Left            =   10800
            Picture         =   "FormExames.frx":092A
            Style           =   1  'Graphical
            TabIndex        =   97
            ToolTipText     =   "Adicionar Sinonimo"
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox EcSinonimo 
            Height          =   285
            Left            =   960
            TabIndex        =   96
            Top             =   240
            Width           =   9735
         End
         Begin VB.CommandButton BtFecharSinonimos 
            Height          =   555
            Left            =   11400
            Picture         =   "FormExames.frx":0CB4
            Style           =   1  'Graphical
            TabIndex        =   95
            ToolTipText     =   "Fechar Sin�nimos"
            Top             =   1920
            Width           =   915
         End
         Begin VB.ListBox EcListaSinonimos 
            Height          =   3180
            Left            =   960
            TabIndex        =   94
            Top             =   600
            Width           =   10215
         End
      End
      Begin VB.Frame Frame3 
         Height          =   4575
         Left            =   -74880
         TabIndex        =   99
         Top             =   360
         Width           =   6135
         Begin VB.TextBox EcCodTuboS 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   145
            Top             =   3120
            Width           =   735
         End
         Begin VB.TextBox EcDescrTuboS 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   144
            TabStop         =   0   'False
            Top             =   3120
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaTuboS 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":197E
            Style           =   1  'Graphical
            TabIndex        =   143
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Tubos Secund�rios"
            Top             =   3120
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaMetodo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":1D08
            Style           =   1  'Graphical
            TabIndex        =   122
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida Tubos Prim�rios"
            Top             =   4080
            Width           =   375
         End
         Begin VB.TextBox EcDescrMetodo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   121
            TabStop         =   0   'False
            Top             =   4080
            Width           =   3375
         End
         Begin VB.TextBox EcCodMetodo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   10
            Top             =   4080
            Width           =   735
         End
         Begin VB.TextBox EcCodigoGrPerfis 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   9
            Top             =   3600
            Width           =   735
         End
         Begin VB.TextBox EcDescrGrPerfis 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   119
            TabStop         =   0   'False
            Top             =   3600
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaGrPerfis 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":2092
            Style           =   1  'Graphical
            TabIndex        =   118
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida Tubos Prim�rios"
            Top             =   3600
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaGrImpr 
            Height          =   315
            Left            =   5525
            Picture         =   "FormExames.frx":241C
            Style           =   1  'Graphical
            TabIndex        =   111
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
            Top             =   1635
            Width           =   375
         End
         Begin VB.TextBox EcDescrGrImpr 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   110
            TabStop         =   0   'False
            Top             =   1635
            Width           =   3375
         End
         Begin VB.TextBox EcCodGrImpr 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   6
            Top             =   1635
            Width           =   735
         End
         Begin VB.TextBox EcCodProd 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   7
            Top             =   2160
            Width           =   735
         End
         Begin VB.TextBox EcDescrProduto 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   109
            TabStop         =   0   'False
            Top             =   2160
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaProduto 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":27A6
            Style           =   1  'Graphical
            TabIndex        =   108
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   2160
            Width           =   375
         End
         Begin VB.TextBox EcCodTuboP 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   8
            Top             =   2640
            Width           =   735
         End
         Begin VB.TextBox EcDescrTuboP 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   107
            TabStop         =   0   'False
            Top             =   2640
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaTuboP 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":2B30
            Style           =   1  'Graphical
            TabIndex        =   106
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida Tubos Prim�rios"
            Top             =   2640
            Width           =   375
         End
         Begin VB.TextBox EcCodGrupo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   3
            Top             =   240
            Width           =   735
         End
         Begin VB.TextBox EcDescrGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   105
            TabStop         =   0   'False
            Top             =   240
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaGrupo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":2EBA
            Style           =   1  'Graphical
            TabIndex        =   104
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox EcCodSubGrupo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   4
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcDescrSubGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   103
            TabStop         =   0   'False
            Top             =   720
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaSubGrupo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":3244
            Style           =   1  'Graphical
            TabIndex        =   102
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Sub Grupos"
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox EcCodClasse 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   5
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox EcDescrClasse 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   101
            TabStop         =   0   'False
            Top             =   1200
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaClasse 
            Height          =   315
            Left            =   5520
            Picture         =   "FormExames.frx":35CE
            Style           =   1  'Graphical
            TabIndex        =   100
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Classes"
            Top             =   1200
            Width           =   375
         End
         Begin VB.Label Label1 
            Caption         =   "Tubo Aliquota"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   146
            Top             =   3120
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "M�todo"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   123
            Top             =   4080
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo Exames"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   120
            Top             =   3600
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo Impress."
            Height          =   255
            Index           =   62
            Left            =   240
            TabIndex        =   117
            Top             =   1680
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Produto"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   116
            Top             =   2160
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Tubo Prim�rio"
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   115
            Top             =   2640
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   114
            Top             =   240
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo"
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   113
            Top             =   720
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Classe"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   112
            Top             =   1200
            Width           =   855
         End
      End
      Begin VB.CommandButton BtSinonimos 
         Height          =   550
         Left            =   -63240
         Picture         =   "FormExames.frx":3958
         Style           =   1  'Graphical
         TabIndex        =   98
         ToolTipText     =   "Sin�nimos"
         Top             =   3720
         Width           =   765
      End
      Begin VB.Frame FrameRubrica 
         Caption         =   "Cria��o de R�brica"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   60
         Top             =   360
         Width           =   12615
         Begin VB.TextBox EcRubrFlgExecHosp 
            Height          =   285
            Left            =   2760
            TabIndex        =   68
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox EcRubrUserCri 
            Height          =   285
            Left            =   1800
            TabIndex        =   67
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox EcRubrDtCri 
            Height          =   285
            Left            =   960
            TabIndex        =   66
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.CommandButton BtSairRubr 
            Height          =   495
            Left            =   11760
            Picture         =   "FormExames.frx":4622
            Style           =   1  'Graphical
            TabIndex        =   65
            ToolTipText     =   "Sair Sem Gravar"
            Top             =   3600
            Width           =   735
         End
         Begin VB.CommandButton BtGravaRubr 
            Height          =   495
            Left            =   10800
            Picture         =   "FormExames.frx":52EC
            Style           =   1  'Graphical
            TabIndex        =   64
            ToolTipText     =   "Insere R�brica"
            Top             =   3600
            Width           =   735
         End
         Begin VB.ComboBox CbRubrGrupo 
            Height          =   315
            Left            =   960
            Style           =   2  'Dropdown List
            TabIndex        =   63
            Top             =   1200
            Width           =   5895
         End
         Begin VB.TextBox EcRubrCodigo 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   960
            TabIndex        =   62
            Top             =   480
            Width           =   855
         End
         Begin VB.TextBox EcRubrDescr 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3120
            TabIndex        =   61
            Top             =   480
            Width           =   3735
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   49
            Left            =   240
            TabIndex        =   71
            Top             =   1200
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Descri��o"
            Height          =   255
            Index           =   48
            Left            =   2280
            TabIndex        =   70
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   47
            Left            =   240
            TabIndex        =   69
            Top             =   480
            Width           =   855
         End
      End
      Begin VB.TextBox EcCodAnaRegra 
         Height          =   285
         Left            =   -65640
         TabIndex        =   87
         Top             =   855
         Width           =   735
      End
      Begin VB.TextBox EcCodAnaFacturar 
         Height          =   285
         Left            =   -65640
         TabIndex        =   86
         Top             =   470
         Width           =   735
      End
      Begin VB.TextBox EcRubrSeqAna 
         Height          =   285
         Left            =   -65760
         TabIndex        =   85
         Top             =   1560
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox EcQtd 
         Height          =   285
         Left            =   -63600
         TabIndex        =   84
         Top             =   480
         Width           =   735
      End
      Begin VB.CheckBox CkContaMembros 
         Caption         =   "Envia Membros para FACTUS"
         Height          =   255
         Left            =   -68760
         TabIndex        =   83
         Top             =   1185
         Width           =   2535
      End
      Begin VB.CheckBox CkFlgRegra 
         Caption         =   "S� factura se n�o marcada a an�lise:"
         Height          =   195
         Left            =   -68760
         TabIndex        =   82
         Top             =   840
         Width           =   3015
      End
      Begin VB.CheckBox CkFlgfacturarAna 
         Caption         =   "Pergunta se pretende facturar an�lise:"
         Height          =   195
         Left            =   -68760
         TabIndex        =   81
         Top             =   480
         Width           =   3015
      End
      Begin VB.CommandButton BtCopiaPrecario 
         Height          =   315
         Left            =   -74280
         Picture         =   "FormExames.frx":5FB6
         Style           =   1  'Graphical
         TabIndex        =   79
         ToolTipText     =   "Copiar Tabela de Outra An�lise"
         Top             =   960
         Width           =   375
      End
      Begin VB.CommandButton BtGravarAnaFact 
         Height          =   315
         Left            =   -73560
         Picture         =   "FormExames.frx":6340
         Style           =   1  'Graphical
         TabIndex        =   78
         ToolTipText     =   "Gravar Mapeamento Factura��o"
         Top             =   960
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CommandButton BtAdicionarRubrica 
         Height          =   315
         Left            =   -74760
         Picture         =   "FormExames.frx":66CA
         Style           =   1  'Graphical
         TabIndex        =   77
         TabStop         =   0   'False
         ToolTipText     =   "Adicionar R�brica ao FACTUS"
         Top             =   960
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaRubrica 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormExames.frx":6A54
         Style           =   1  'Graphical
         TabIndex        =   76
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida R�bricas"
         Top             =   480
         Width           =   375
      End
      Begin VB.TextBox EcDescrRubrica 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -72960
         TabIndex        =   75
         Top             =   480
         Width           =   3735
      End
      Begin VB.TextBox EcCodRubrica 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73800
         TabIndex        =   74
         Top             =   480
         Width           =   855
      End
      Begin VB.TextBox EcAuxPreco 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -72720
         TabIndex        =   72
         Top             =   1920
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.ListBox EcAnaLocais 
         Height          =   960
         Left            =   -67440
         Style           =   1  'Checkbox
         TabIndex        =   11
         Top             =   780
         Width           =   5175
      End
      Begin VB.ListBox EcAnaLocaisExec 
         BackColor       =   &H00FFFFFF&
         Height          =   960
         Left            =   -67440
         Style           =   1  'Checkbox
         TabIndex        =   12
         Top             =   2280
         Width           =   5175
      End
      Begin MSFlexGridLib.MSFlexGrid FgPrecos 
         Height          =   3135
         Left            =   -74880
         TabIndex        =   73
         Top             =   1440
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   5530
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.Label Label1 
         Caption         =   "Design. Etiqueta"
         Height          =   255
         Index           =   10
         Left            =   240
         TabIndex        =   154
         Top             =   3240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Meios"
         Height          =   255
         Index           =   3
         Left            =   3600
         TabIndex        =   151
         Top             =   3120
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Quest�es"
         Height          =   255
         Index           =   59
         Left            =   7920
         TabIndex        =   148
         Top             =   3120
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Tempo Conclus�o"
         Height          =   255
         Index           =   40
         Left            =   240
         TabIndex        =   142
         Top             =   2880
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Informa��es"
         Height          =   255
         Index           =   58
         Left            =   7920
         TabIndex        =   140
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Folhas Trab."
         Height          =   255
         Index           =   54
         Left            =   7920
         TabIndex        =   138
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label57 
         Caption         =   "An�lise S� marcada em:"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   133
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label26 
         Caption         =   "C�d. Estat�stico"
         Height          =   255
         Left            =   240
         TabIndex        =   132
         Top             =   1695
         Width           =   1335
      End
      Begin VB.Label Label21 
         Caption         =   "Peso Estat�stico"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   131
         Top             =   1215
         Width           =   1335
      End
      Begin VB.Label Label21 
         Caption         =   "Ordem ARS"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   130
         Top             =   2115
         Width           =   1335
      End
      Begin VB.Label Label21 
         Caption         =   "Validade"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   129
         Top             =   2475
         Width           =   1335
      End
      Begin VB.Label Label100 
         Caption         =   "Quantidade:"
         Height          =   255
         Left            =   -64560
         TabIndex        =   88
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Rubrica"
         Height          =   255
         Index           =   46
         Left            =   -74760
         TabIndex        =   80
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label55 
         Caption         =   "Locais de marca��o"
         Height          =   255
         Index           =   1
         Left            =   -67920
         TabIndex        =   59
         Top             =   540
         Width           =   2415
      End
      Begin VB.Label Label55 
         Caption         =   "Local de execu��o"
         Height          =   255
         Index           =   0
         Left            =   -67800
         TabIndex        =   58
         Top             =   2040
         Width           =   2535
      End
      Begin VB.Label Label12 
         Height          =   255
         Left            =   -74280
         TabIndex        =   56
         Top             =   540
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.TextBox EcGrPerfis1 
      Height          =   285
      Left            =   1560
      TabIndex        =   53
      Top             =   12240
      Width           =   1095
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6150
      TabIndex        =   34
      Top             =   10440
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcDescricao 
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   3360
      TabIndex        =   2
      Top             =   180
      Width           =   5055
   End
   Begin VB.TextBox EcCodigo 
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      Top             =   180
      Width           =   1185
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1950
      TabIndex        =   33
      Top             =   10800
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3870
      TabIndex        =   32
      Top             =   10800
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1830
      TabIndex        =   31
      Top             =   10440
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3750
      TabIndex        =   30
      Top             =   10440
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   360
      TabIndex        =   23
      Top             =   7230
      Width           =   12615
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   27
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   26
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   25
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   24
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.TextBox EcCodSubGrupo1 
      Height          =   285
      Left            =   1590
      TabIndex        =   21
      Top             =   11520
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcCodGrupo1 
      Height          =   285
      Left            =   1590
      TabIndex        =   20
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcCodClasse1 
      Height          =   285
      Left            =   1590
      TabIndex        =   18
      Top             =   11880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcOrdem 
      Height          =   285
      Left            =   6150
      TabIndex        =   17
      Top             =   10560
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5550
      TabIndex        =   15
      Top             =   11760
      Width           =   1335
   End
   Begin VB.TextBox EcCodMetodo1 
      Height          =   285
      Left            =   4350
      TabIndex        =   14
      Top             =   11880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcCodProduto1 
      Height          =   285
      Left            =   3990
      TabIndex        =   13
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcCodTubo1 
      Height          =   285
      Left            =   3990
      TabIndex        =   0
      Top             =   11520
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "EcColunaPreco"
      Height          =   375
      Index           =   51
      Left            =   10440
      TabIndex        =   92
      Top             =   11400
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcLinhaPreco"
      Height          =   375
      Index           =   50
      Left            =   10440
      TabIndex        =   91
      Top             =   11040
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Abreviatura"
      Height          =   255
      Index           =   0
      Left            =   8670
      TabIndex        =   40
      Top             =   180
      Width           =   975
   End
   Begin VB.Label Label28 
      Caption         =   "EcGrPerfis"
      Height          =   255
      Left            =   240
      TabIndex        =   54
      Top             =   12240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Index           =   0
      Left            =   4710
      TabIndex        =   51
      Top             =   10440
      Visible         =   0   'False
      Width           =   1365
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   2400
      TabIndex        =   50
      Top             =   180
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   1
      Left            =   180
      TabIndex        =   49
      Top             =   180
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   390
      TabIndex        =   48
      Top             =   5880
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   1830
      TabIndex        =   47
      Top             =   5880
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   270
      TabIndex        =   46
      Top             =   10440
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   270
      TabIndex        =   45
      Top             =   10800
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2430
      TabIndex        =   44
      Top             =   10440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2550
      TabIndex        =   43
      Top             =   10800
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label15 
      Caption         =   "EcCodGrupo"
      Height          =   255
      Left            =   270
      TabIndex        =   42
      Top             =   11160
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label16 
      Caption         =   "EcCodSubGrupo"
      Height          =   255
      Left            =   270
      TabIndex        =   41
      Top             =   11520
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label18 
      Caption         =   "EcCodClasse"
      Height          =   255
      Left            =   270
      TabIndex        =   39
      Top             =   11880
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label52 
      Caption         =   "EcOrdem"
      Height          =   255
      Left            =   5430
      TabIndex        =   38
      Top             =   11040
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label20 
      Caption         =   "EcCodMetodo"
      Height          =   255
      Left            =   3030
      TabIndex        =   37
      Top             =   11880
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label23 
      Caption         =   "EcCodProduto"
      Height          =   255
      Left            =   2910
      TabIndex        =   36
      Top             =   11160
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label25 
      Caption         =   "EcCodTubo"
      Height          =   255
      Left            =   2910
      TabIndex        =   35
      Top             =   11520
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "FormExames"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 30/03/2003
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer
Private Type Precarios
    cod_rubr As String
    Portaria As String
    descr_Portaria As String
    cod_efr As String
    cod_rubr_efr As String
    descr_rubr_efr As String
    val_Taxa As String
    t_fac As String
    nr_k As String
    nr_c As String
    val_pag_ent As String
    perc_pag_doe As String
    val_pag_doe As String
    user_cri As String
    dt_cri As String
    user_act As String
    dt_act As String
    empresa_id As String
    valor_c As String
    
    flg_modificado As Boolean
End Type
Dim EstrutPrec() As Precarios
Dim TotalPrec As Long
Const lColPrecosCodPrecario = 0
Const lColPrecosDescrPrecario = 1
Const lColPrecosCodRubrEFR = 2
Const lColPrecosDescrRubrEFR = 3
Const lColPrecosTxModeradora = 4
Const lColPrecosTipoFact = 5
Const lColPrecosNumK = 6
Const lColPrecosNumC = 7
Const lColPrecosValPagEFR = 8
Const lColPrecosValPagDoe = 9
Const lColPrecosPercDoente = 10

Dim NomeTabelaFactus As String
Dim NumCamposFACTUS As Integer
Dim CamposFactusBD() As String
Dim CamposFactusEc() As Object
Dim ChaveBDFactus As String
Dim ChaveEcFactus As Object
' --------------------------------------
' Estrutura de Sinonimos
' --------------------------------------
Private Type Sinonimos
    seq_sinonimo As Long
    descr_sinonimo As String
    flg_original As Integer
End Type
Dim EstrutSinonimos() As Sinonimos
Dim TotalEstrutSinonimos As Integer
Sub PreencheValoresDefeito()

    BL_InicioProcessamento Me, "Carregar valores de defeito..."
    
    
    ' Locais
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcAnaLocais, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcAnaLocaisExec, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocal, mediAscComboCodigo
    
    BL_IniFolhasTrab Me
    BL_Informacao Me
    BL_Questoes Me
    DIA_PreencheMeiosDefeitoAna Me
    
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", CbSexo, mediAscComboCodigo

    EcActivo.value = 1
    CkFacturar.value = vbGrayed
    CkGrafico.value = vbGrayed
    
    BL_FimProcessamento Me
    SSTab1.Tab = 0
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BG_PreencheComboBDSecundaria_ADO "FA_GRUP_RUBR", "cod_grupo", "descr_grupo", CbRubrGrupo
    End If
    FrameRubrica.Visible = False

End Sub

Private Sub BtAnaPerfis_Click()
    
    If EcCodigo <> "" And EcDescricao <> "" Then
        Max = UBound(gFieldObjectProperties)
    
        ' Guardar as propriedades dos campos do form
        For Ind = 0 To Max
            ReDim Preserve FOPropertiesTemp(Ind)
            FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
            FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
            FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
            FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
            FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
            FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
        Next Ind
           
        FormAnaExames.Show
    Else
        MsgBox "Para utilizar esta op��o, deve escolher um exame.    ", vbInformation, " Membros do Perfil "
    End If

End Sub


Private Sub BtFecharSinonimos_Click()
    EcListaSinonimos.Clear
    FrameSinon.Visible = False
End Sub

Private Sub BtSinonimos_Click()
    FrameSinon.Visible = True
    FuncaoProcurarSinonimos
End Sub

Private Sub FuncaoProcurarSinonimos()
    Dim sSql As String
    Dim rsSin As New ADODB.recordset
    sSql = "SELECT * FROM sl_ana_sinonimos WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo)
    sSql = sSql & " AND (flg_original IS NULL or flg_original = 0) "
    rsSin.CursorType = adOpenStatic
    rsSin.CursorLocation = adUseServer
    rsSin.Open sSql, gConexao
    TotalEstrutSinonimos = 0
    ReDim EstrutSinonimos(0)
    EcListaSinonimos.Clear
    If rsSin.RecordCount > 0 Then
        While Not rsSin.EOF
            TotalEstrutSinonimos = TotalEstrutSinonimos + 1
            ReDim Preserve EstrutSinonimos(TotalEstrutSinonimos)
            
            EstrutSinonimos(TotalEstrutSinonimos).seq_sinonimo = rsSin!seq_sinonimo
            EstrutSinonimos(TotalEstrutSinonimos).descr_sinonimo = BL_HandleNull(rsSin!descr_sinonimo, "")
            EstrutSinonimos(TotalEstrutSinonimos).flg_original = 0
            EcListaSinonimos.AddItem EstrutSinonimos(TotalEstrutSinonimos).descr_sinonimo
            rsSin.MoveNext
        Wend
    End If
    rsSin.Close
    Set rsSin = Nothing
End Sub







Private Sub CbSexo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbSexo.ListIndex = mediComboValorNull
    End If
End Sub



Private Sub EcListaSinonimos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        ApagaSinonimo
    End If
End Sub

Private Sub EcSinonimo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        BtAdicSinonimo_Click
    End If
End Sub

Private Sub ApagaSinonimo()
    Dim sSql As String
    sSql = "DELETE FROM sl_ana_sinonimos WHERE seq_sinonimo =  " & EstrutSinonimos(EcListaSinonimos.ListIndex + 1).seq_sinonimo
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    FuncaoProcurarSinonimos
    EcSinonimo = ""
    EcSinonimo.SetFocus

End Sub

Private Sub BtAdicSinonimo_Click()
    Dim sSql As String
    On Error GoTo TrataErro
    
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_ana_sinonimos (seq_sinonimo, cod_ana, descr_sinonimo,flg_original) VALUES (seq_sinonimo.nextval, "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcSinonimo) & ",0)"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_ana_sinonimos ( cod_ana, descr_sinonimo,flg_original) VALUES ( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcSinonimo) & ",0)"
    End If
    
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    FuncaoProcurarSinonimos
    EcSinonimo = ""
    EcSinonimo.SetFocus
TrataErro:
    BL_LogFile_BD Me.Name, "BtAdicSinonimo_click", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub


Private Sub cmdOK_Click()

    EcCodigo.Text = UCase(Trim(EcCodigo.Text))
    If (Mid(EcCodigo.Text, 1, 1) <> "P") Then
        EcCodigo.Text = "P" & EcCodigo.Text
    End If
    
    Call FuncaoProcurar

End Sub

Private Sub EcAbreviatura_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcAbreviatura_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub


Private Sub EcCodigo_LostFocus()

    cmdOK.Default = False

End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    EcCodigo.Text = UCase(EcCodigo.Text)
    If Trim(EcCodigo.Text) = "P" Then
        BG_Mensagem mediMsgBox, "O [P] por si s�, n�o pode ser c�digo do perfil, pois � identificativo do mesmo!", vbExclamation, "C�digo do Perfil"
        Cancel = True
    ElseIf left(Trim(EcCodigo.Text), 1) <> "P" And Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = "P" & EcCodigo.Text
    ElseIf Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = left(Trim(EcCodigo.Text), 1) & Mid(EcCodigo.Text, 2, Len(EcCodigo.Text) - 1)
    End If
    
End Sub


Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
End Sub

Private Sub EcCodigo_GotFocus()

    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True
    
End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub


Private Sub EcLocal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcLocal.ListIndex = -1
End Sub


Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BL_Abre_Conexao_Secundaria gSGBD_SECUNDARIA
    End If

    Me.caption = " Exames"
    Me.left = 5
    Me.top = 5
    Me.Width = 13185
    Me.Height = 8610 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_perfis"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 35
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_perfis"
    CamposBD(1) = "cod_perfis"
    CamposBD(2) = "descr_perfis"
    CamposBD(3) = "flg_activo"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "gr_ana"
    CamposBD(9) = "sgr_ana"
    CamposBD(10) = "abr_ana_p"
    CamposBD(11) = "classe_ana"
    CamposBD(12) = "ordem"
    CamposBD(13) = "peso"
    CamposBD(14) = "metodo"
    CamposBD(15) = "cod_produto"
    CamposBD(16) = "cod_tubo"
    CamposBD(17) = "cod_estatistica"
    CamposBD(18) = "flg_garrafa"
    CamposBD(19) = "flg_invisivel"
    CamposBD(20) = "cod_gr_perfis"
    CamposBD(21) = "flg_facturar"
    CamposBD(22) = "cod_local"
    CamposBD(23) = "flg_sexo"
    CamposBD(24) = "ordem_ars"
    CamposBD(25) = "prazo_val"
    CamposBD(26) = "gr_impr"
    CamposBD(27) = "prazo_conc"
    CamposBD(28) = "cod_tubo_sec"
    CamposBD(29) = "flg_grafico"
    CamposBD(30) = "flg_inibe_descr"
    CamposBD(31) = "descr_etiq"
    CamposBD(32) = "flg_obriga_infocli"
    CamposBD(33) = "flg_obriga_terap"
    'RGONCALVES 10.12.2015 CHSJ-2511
    CamposBD(34) = "ana_acreditada"
    '
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcActivo
    Set CamposEc(4) = EcUtilizadorCriacao
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = EcCodGrupo
    Set CamposEc(9) = EcCodSubGrupo
    Set CamposEc(10) = EcAbreviatura
    Set CamposEc(11) = EcCodClasse
    Set CamposEc(12) = EcOrdem
    Set CamposEc(13) = EcPeso
    Set CamposEc(14) = EcCodMetodo
    Set CamposEc(15) = EcCodProd
    Set CamposEc(16) = EcCodTuboP
    Set CamposEc(17) = EcCodEstatistica
    Set CamposEc(18) = EcGarrafa
    Set CamposEc(19) = CkInvisivel
    Set CamposEc(20) = EcCodigoGrPerfis
    Set CamposEc(21) = CkFacturar
    Set CamposEc(22) = EcLocal
    Set CamposEc(23) = CbSexo
    Set CamposEc(24) = EcOrdemARS
    Set CamposEc(25) = EcPrazoVal
    Set CamposEc(26) = EcCodGrImpr
    Set CamposEc(27) = EcPrazoConc
    Set CamposEc(28) = EcCodTuboS
    Set CamposEc(29) = CkGrafico
    Set CamposEc(30) = CkInibeDescricao
    Set CamposEc(31) = EcDescrEtiq
    Set CamposEc(32) = CkObrigaInfClin
    Set CamposEc(33) = CkObrigaTerap
    'RGONCALVES 10.12.2015 CHSJ-2511
    Set CamposEc(34) = CkAcreditacao
    '
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Perfi de An�lise"
    TextoCamposObrigatorios(2) = "Descri��o do Perfil de An�lise"
    TextoCamposObrigatorios(3) = "Perfil Activo"
    TextoCamposObrigatorios(17) = "C�digo Estat�stico"
    TextoCamposObrigatorios(21) = "An�lise a Facturar"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_perfis"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_perfis", "descr_perfis")
    NumEspacos = Array(12, 42)
    
'    EcActivo.Value = vbGrayed
    EcGarrafa.value = vbGrayed
    CkInvisivel.value = vbGrayed
    
    EcDescricao.ToolTipText = cMsgWilcards
    Call BL_FormataCodigo(EcCodigo)
        
    CkInvisivel.Visible = False
    
    NumCamposFACTUS = 6
    ReDim CamposFactusBD(0 To NumCamposFACTUS - 1)
    ReDim CamposFactusEc(0 To NumCamposFACTUS - 1)
    ReDim TextoCamposObrigatoriosFactus(0 To NumCamposFACTUS - 1)
    NomeTabelaFactus = "FA_RUBR"
    ' Campos da Base de Dados
    CamposFactusBD(0) = "cod_grupo"
    CamposFactusBD(1) = "cod_rubr"
    CamposFactusBD(2) = "descr_rubr"
    CamposFactusBD(3) = "user_cri"
    CamposFactusBD(4) = "dt_cri"
    CamposFactusBD(5) = "flg_exec_hosp"

    ' Campos do Ecr�
    Set CamposFactusEc(0) = CbRubrGrupo
    Set CamposFactusEc(1) = EcRubrCodigo
    Set CamposFactusEc(2) = EcRubrDescr
    Set CamposFactusEc(3) = EcRubrUserCri
    Set CamposFactusEc(4) = EcRubrDtCri
    Set CamposFactusEc(5) = EcRubrFlgExecHosp
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    Max = -1
    ReDim FOPropertiesTemp(0)

    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    gF_EXAMES = 1
    BL_FimProcessamento Me
    
    PreencheValoresDefeito

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    'If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    gF_EXAMES = 0
    Set FormExames = Nothing

End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus
    SSTab1.Tab = 0
    DoEvents
    EcDescrClasse = ""
    EcDescrGrImpr = ""
    EcDescrGrPerfis = ""
    EcDescrGrupo = ""
    EcDescrProduto = ""
    EcDescrTuboP = ""
    EcDescrTuboS = ""
    EcDescrSubGrupo = ""
    EcDescrMetodo = ""
    
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcActivo.value = 1
    CkInvisivel.Visible = False
    CkInvisivel.value = vbGrayed
    CkFacturar.value = vbGrayed
    LimpaPrecos
    For i = 0 To EcAnaLocais.ListCount - 1
        EcAnaLocais.Selected(i) = False
    Next
    For i = 0 To EcAnaLocaisExec.ListCount - 1
        EcAnaLocaisExec.Selected(i) = False
    Next
    For i = 0 To EcListaFolhasTrab.ListCount - 1
        EcListaFolhasTrab.Selected(i) = False
    Next
    CkFacturar.value = vbGrayed
    CkContaMembros.value = vbGrayed
    CkFlgRegra.value = vbGrayed
    CkGrafico.value = vbGrayed
    EcCodAnaRegra = ""
    CkFlgfacturarAna.value = vbGrayed
    EcCodAnaFacturar = ""
    EcCodRubrica = ""
    EcCodRubrica_Validate False
    EcRubrSeqAna = ""
    EcAuxPreco = ""
    EcAuxPreco.Visible = False
    EcColunaPreco = ""
    EcLinhaPreco = ""
    EcQtd = ""
    EcListaSinonimos.Clear
    FrameSinon.Visible = False
    BL_LimpaDadosInfAna Me
    BL_LimpaDadosQuestoes Me
    DIA_LimpaMeiosAna Me
    CkInibeDescricao.value = vbGrayed
    CkObrigaInfClin.value = vbGrayed
    CkObrigaTerap.value = vbGrayed
End Sub

Sub DefTipoCampos()
    EcCodigo.Tag = adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    
    With FgPrecos
       .rows = 2
       .Cols = 11
       .FixedRows = 1
       .FixedCols = 0
       .AllowBigSelection = False
       .HighLight = flexHighlightAlways
       .FocusRect = flexFocusHeavy
       .MousePointer = flexDefault
       .FillStyle = flexFillSingle
       .SelectionMode = flexSelectionFree
       .AllowUserResizing = flexResizeColumns
       .GridLinesFixed = flexGridInset
       .GridColorFixed = flexTextInsetLight
       .MergeCells = flexMergeFree
       .PictureType = flexPictureColor
       .RowHeightMin = 280
       .row = 0
       
       .ColWidth(lColPrecosCodPrecario) = 500
       .Col = lColPrecosCodPrecario
       .Text = "C�d"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrPrecario) = 2800
       .Col = lColPrecosDescrPrecario
       .Text = "Pre��rio"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosCodRubrEFR) = 1000
       .Col = lColPrecosCodRubrEFR
       .Text = "C�d.Rubr."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrRubrEFR) = 2800
       .Col = lColPrecosDescrRubrEFR
       .Text = "Descr. Rubrica"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTxModeradora) = 800
       .Col = lColPrecosTxModeradora
       .Text = "Tx.Modr."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTipoFact) = 800
       .Col = lColPrecosTipoFact
       .Text = "Tipo Fact"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumC) = 700
       .Col = lColPrecosNumC
       .Text = "N�Cs"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumK) = 700
       .Col = lColPrecosNumK
       .Text = "N�Ks"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagEFR) = 700
       .Col = lColPrecosValPagEFR
       .Text = "Val.EFR"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagDoe) = 700
       .Col = lColPrecosValPagDoe
       .Text = "Val.Doe."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosPercDoente) = 700
       .Col = lColPrecosPercDoente
       .Text = "%Doe"
       .CellAlignment = flexAlignLeftCenter
       
       .Col = 0
    End With
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        SSTab1.TabVisible(1) = True
    Else
        SSTab1.TabVisible(1) = False
    End If
    CbRubrGrupo.Tag = adNumeric
    EcRubrDtCri.Tag = adDate
    EcRubrUserCri.Tag = adVarChar
    EcRubrCodigo.Tag = adNumeric
    EcRubrDescr.Tag = adVarChar
    CkInibeDescricao.value = vbGrayed
    CkObrigaInfClin.value = vbGrayed
    CkObrigaTerap.value = vbGrayed
    'RGONCALVES 10.12.2015 CHSJ-2511
    CkAcreditacao.value = vbGrayed
    '
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        BL_PreencheDadosInfAna Me, EcCodigo
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        BL_CarregaLocaisAna EcCodigo, Me
        BL_CarregaLocaisAnaExec EcCodigo, Me
        BL_CarregaFolhasTrab EcCodigo, Me
        BL_PreencheDadosQuestoes Me, EcCodigo
        DIA_PreencheMeiosAna Me, EcCodigo

        If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
            PreencheFacturacao
            PreenchePrecarios
        End If
        EcCodclasse_Validate False
        EcCodgrImpr_Validate False
        EcCodGrupo_Validate False
        EcCodsubGrupo_Validate False
        EcCodProd_Validate False
        EcCodtubop_Validate False
        EcCodigogrperfis_Validate False
        EcCodtuboS_Validate False
        EcCodMetodo_Validate False
        
        If CkInvisivel.value = 1 Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
        
    End If

End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

    Me.EcCodigo.Locked = False

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function


Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
              
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                    "descr_perfis", _
                                    gPesquisaDentroCampo)
    
'    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY descr_perfis ASC, cod_perfis ASC "
'    End If
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        Call BG_PreencheListBoxMultipla_ADO(EcLista, _
                                            rs, _
                                            CamposBDparaListBox, _
                                            NumEspacos, _
                                            CamposBD, _
                                            CamposEc, _
                                            "SELECT")
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        'BL_Toolbar_BotaoEstado "Remover", "InActivo"
        Me.EcCodigo.Locked = True
        
        BL_FimProcessamento Me
    
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    Dim Codigo As String
    Dim iRes As Integer
    
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        If EcCodigo = "" Then
            gMsgTitulo = " C�digo"
            gMsgMsg = "Quer gerar um novo c�digo?   "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            
            If gMsgResp = vbYes Then
                Codigo = BL_GeraCodigoAnalise
                If Codigo <> "" Then
                    gMsgTitulo = " C�digo"
                    gMsgMsg = "Foi gerado o c�digo: P" & Codigo & ". Aceitar?"
                    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                    If gMsgResp = vbYes Then
                        EcCodigo = "P" & Codigo
                        EcCodEstatistica = EcCodigo
                    Else
                        Exit Sub
                    End If
                End If
            End If
        Else
            If BL_VerificaCodigoExiste(EcCodigo) = True Then
                gMsgTitulo = " C�digo"
                gMsgMsg = "J� existe uma an�lise com esse c�digo. Quer continuar? "
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp <> vbYes Then
                    Exit Sub
                End If
            End If
        End If
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcOrdem.Text = BG_DaMAX(NomeTabela, "ordem") + 1
    
    EcCodSequencial = BG_DaMAX("SLV_ANALISES_APENAS", "seq_ana") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        GravaFacturacao
        GravaPrecarios
    End If
    GravaSinonimo
    BL_GravaLocaisAna EcCodigo, Me
    BL_GravaLocaisAnaExec EcCodigo, Me
    BL_GravaFolhasTrab EcCodigo, Me
    BL_GravaDadosInfAna Me, EcCodigo
    BL_GravaDadosQuestoes Me, EcCodigo
    DIA_GravaMeiosAna Me, EcCodigo
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        GravaFacturacao
        GravaPrecarios
    End If
    GravaSinonimo
    BL_GravaLocaisAna EcCodigo, Me
    BL_GravaLocaisAnaExec EcCodigo, Me
    BL_GravaFolhasTrab EcCodigo, Me
    BL_GravaDadosInfAna Me, EcCodigo
    BL_GravaDadosQuestoes Me, EcCodigo
    DIA_GravaMeiosAna Me, EcCodigo
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    
    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja desactivar o registo ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "UPDATE " & NomeTabela & " SET flg_invisivel = 1 WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    

    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub


' ------------------------------------------------------

' PREENCHE DADOS DE FACTURACAO

' ------------------------------------------------------
Private Sub PreencheFacturacao()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo) & " AND cod_efr IS NULL "
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    RsFact.Open sSql, gConexao
    If RsFact.RecordCount >= 1 Then
        EcRubrSeqAna = BL_HandleNull(RsFact!seq_ana, "")
        EcCodRubrica = BL_HandleNull(RsFact!cod_ana_gh, "")
        EcCodRubrica_Validate False
        
        If BL_HandleNull(RsFact!flg_conta_membros, "0") = mediSim Then
            CkContaMembros.value = vbChecked
        Else
            CkContaMembros.value = vbUnchecked
        End If
        
        If BL_HandleNull(RsFact!flg_ana_facturar, "0") = mediSim Then
            CkFlgfacturarAna.value = vbChecked
        Else
            CkFlgfacturarAna.value = vbUnchecked
        End If
        EcCodAnaFacturar = BL_HandleNull(RsFact!cod_ana_facturar, "")
        
        If BL_HandleNull(RsFact!Flg_Regra, "0") = mediSim Then
            CkFlgRegra.value = vbChecked
        Else
            CkFlgRegra.value = vbUnchecked
        End If
        EcCodAnaRegra = BL_HandleNull(RsFact!Cod_Ana_Regra, "")
        EcQtd = BL_HandleNull(RsFact!qtd, "")
    End If
    RsFact.Close
    Set RsFact = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a  Preencher Factura��o ", Me.Name, "PreencheFacturacao"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' CARREGA PRECARIOS

' ------------------------------------------------------
Private Sub PreenchePrecarios()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    Dim rsPrRubr As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM fa_portarias where DT_FIM IS NULL ORDER BY cod_efr, dt_ini"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    RsFact.Open sSql, gConexaoSecundaria
    If RsFact.RecordCount >= 1 Then
        While Not RsFact.EOF
            TotalPrec = TotalPrec + 1
            ReDim Preserve EstrutPrec(TotalPrec)
            
            EstrutPrec(TotalPrec).cod_rubr = EcCodRubrica
            EstrutPrec(TotalPrec).cod_efr = BL_HandleNull(RsFact!cod_efr, "")
            EstrutPrec(TotalPrec).Portaria = BL_HandleNull(RsFact!Portaria, "")
            EstrutPrec(TotalPrec).descr_Portaria = BL_HandleNull(RsFact!descr_Port, "")
            EstrutPrec(TotalPrec).empresa_id = BL_HandleNull(RsFact!empresa_id, "")
            EstrutPrec(TotalPrec).flg_modificado = False
            
            FgPrecos.TextMatrix(TotalPrec, lColPrecosCodPrecario) = EstrutPrec(TotalPrec).Portaria
            FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrPrecario) = EstrutPrec(TotalPrec).descr_Portaria
            
            If EcCodRubrica <> "" Then
                sSql = "SELECT * FROM fa_pr_rubr WHERE cod_rubr = " & EcCodRubrica & " AND portaria = " & EstrutPrec(TotalPrec).Portaria
                rsPrRubr.CursorType = adOpenStatic
                rsPrRubr.CursorLocation = adUseServer
                rsPrRubr.Open sSql, gConexaoSecundaria
                If rsPrRubr.RecordCount = 1 Then
                    EstrutPrec(TotalPrec).cod_rubr_efr = BL_HandleNull(rsPrRubr!cod_rubr_efr, "")
                    EstrutPrec(TotalPrec).descr_rubr_efr = BL_HandleNull(rsPrRubr!descr_rubr_efr, "")
                    EstrutPrec(TotalPrec).nr_c = BL_HandleNull(rsPrRubr!nr_c, "")
                    EstrutPrec(TotalPrec).nr_k = BL_HandleNull(rsPrRubr!nr_k, "")
                    EstrutPrec(TotalPrec).perc_pag_doe = BL_HandleNull(rsPrRubr!perc_pag_doe, "")
                    EstrutPrec(TotalPrec).t_fac = BL_HandleNull(rsPrRubr!t_fac, "")
                    EstrutPrec(TotalPrec).val_pag_doe = BL_HandleNull(rsPrRubr!val_pag_doe, "")
                    EstrutPrec(TotalPrec).val_pag_ent = BL_HandleNull(rsPrRubr!val_pag_ent, "")
                    EstrutPrec(TotalPrec).val_Taxa = BL_HandleNull(rsPrRubr!val_Taxa, "")
                    EstrutPrec(TotalPrec).user_cri = BL_HandleNull(rsPrRubr!user_cri, "")
                    EstrutPrec(TotalPrec).user_act = BL_HandleNull(rsPrRubr!user_act, "")
                    EstrutPrec(TotalPrec).dt_cri = BL_HandleNull(rsPrRubr!dt_cri, "")
                    EstrutPrec(TotalPrec).dt_act = BL_HandleNull(rsPrRubr!dt_act, "")
                    EstrutPrec(TotalPrec).valor_c = IF_RetornaValorC(EstrutPrec(TotalPrec).Portaria)
                     
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosCodRubrEFR) = EstrutPrec(TotalPrec).cod_rubr_efr
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrRubrEFR) = EstrutPrec(TotalPrec).descr_rubr_efr
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosNumC) = EstrutPrec(TotalPrec).nr_c
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosNumK) = EstrutPrec(TotalPrec).nr_k
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosPercDoente) = EstrutPrec(TotalPrec).perc_pag_doe
                    
                    If EstrutPrec(TotalPrec).t_fac = "1" Then
                        FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "KS E CS"
                    ElseIf EstrutPrec(TotalPrec).t_fac = "2" Then
                        FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "EUROS"
                    End If
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagDoe) = EstrutPrec(TotalPrec).val_pag_doe
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagEFR) = EstrutPrec(TotalPrec).val_pag_ent
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosTxModeradora) = EstrutPrec(TotalPrec).val_Taxa
                End If
                rsPrRubr.Close
            End If
            FgPrecos.AddItem ""
            RsFact.MoveNext
        Wend
        RsFact.Close
        Set RsFact = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Carregar Precarios", Me.Name, "PreenchePrecarios"
    Exit Sub
    Resume Next
End Sub


Private Sub EcCodRubrica_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodRubrica.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_rubr, descr_rubr FROM fa_rubr WHERE upper(cod_rubr)= " & BL_TrataStringParaBD(UCase(EcCodRubrica.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexaoSecundaria
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodRubrica.Text = "" & RsDescrGrupo!cod_rubr
            EcDescrRubrica.Text = "" & RsDescrGrupo!descr_rubr
        
        Else
            Cancel = True
            EcDescrRubrica.Text = ""
            BG_Mensagem mediMsgBox, "A R�brica indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrRubrica.Text = ""
    End If

End Sub


' ------------------------------------------------------

' LIMPA TODOS CAMPOS RELATIVOS AOS VALORES DE REFERENCIA

' ------------------------------------------------------
Private Sub LimpaPrecos()
    Dim i  As Long
    On Error GoTo TrataErro
    
    FgPrecos.rows = 2
    FgPrecos.row = 1
    For i = 0 To FgPrecos.Cols - 1
        FgPrecos.TextMatrix(1, i) = ""
    Next
    TotalPrec = 0
    ReDim EstrutPrec(0)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a limpar FgPrecos ", Me.Name, "LimpaPrecos"
    Exit Sub
    Resume Next
End Sub

Private Sub BtAdicionarRubrica_Click()
    FrameRubrica.Visible = True
    FrameRubrica.top = 360
    FrameRubrica.left = 50
End Sub

Private Sub BtCopiaPrecario_Click()
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_rubr"
    CamposEcran(1) = "cod_rubr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_rubr"
    CamposEcran(2) = "descr_rubr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_rubr"
    CampoPesquisa1 = "descr_rubr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_rubr", " Pesquisar R�bricas")
    
    mensagem = "N�o foi encontrada nenhuma R�brica"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            gMsgTitulo = " Remover"
            gMsgMsg = "Tem a certeza que deseja copiar os pre�os da r�brica " & resultados(1) & " - " & resultados(2) & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
            If gMsgResp = vbYes Then
                CopiaRubrica CStr(resultados(1))
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub BtPesquisaRubrica_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_rubr"
    CamposEcran(1) = "cod_rubr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_rubr"
    CamposEcran(2) = "descr_rubr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_rubr"
    CampoPesquisa1 = "descr_rubr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_rubr", " Pesquisar R�bricas")
    
    mensagem = "N�o foi encontrada nenhuma R�brica"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodRubrica.Text = resultados(1)
            EcDescrRubrica.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


' ----------------------------------------------------------------------

' COPIA CODIFICA��ES DE OUTRA AN�LISE

' ----------------------------------------------------------------------
Private Sub CopiaRubrica(rubrica As String)
    Dim sSql As String
    Dim rsPrRubr As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    
    If EcCodRubrica <> "" Then
    
        ' APAGA AS PARAMETRIZACOES EXISTENTES
        For i = 1 To TotalPrec
           EstrutPrec(i).cod_rubr_efr = -1
           EstrutPrec(i).descr_rubr_efr = ""
           EstrutPrec(i).nr_c = ""
           EstrutPrec(i).nr_k = ""
           EstrutPrec(i).perc_pag_doe = ""
           EstrutPrec(i).t_fac = ""
           EstrutPrec(i).val_pag_doe = ""
           EstrutPrec(i).val_pag_ent = ""
           EstrutPrec(i).val_Taxa = ""
           EstrutPrec(i).user_cri = ""
           EstrutPrec(i).user_act = ""
           EstrutPrec(i).dt_cri = ""
           EstrutPrec(i).dt_act = ""
           EstrutPrec(i).flg_modificado = True
    
           FgPrecos.TextMatrix(i, lColPrecosCodRubrEFR) = EstrutPrec(i).cod_rubr_efr
           FgPrecos.TextMatrix(i, lColPrecosDescrRubrEFR) = EstrutPrec(i).descr_rubr_efr
           FgPrecos.TextMatrix(i, lColPrecosNumC) = EstrutPrec(i).nr_c
           FgPrecos.TextMatrix(i, lColPrecosNumK) = EstrutPrec(i).nr_k
           FgPrecos.TextMatrix(i, lColPrecosPercDoente) = EstrutPrec(i).perc_pag_doe
           FgPrecos.TextMatrix(i, lColPrecosTipoFact) = ""
           FgPrecos.TextMatrix(i, lColPrecosValPagDoe) = EstrutPrec(i).val_pag_doe
           FgPrecos.TextMatrix(i, lColPrecosValPagEFR) = EstrutPrec(i).val_pag_ent
           FgPrecos.TextMatrix(i, lColPrecosTxModeradora) = EstrutPrec(i).val_Taxa
           Exit For
        Next
        sSql = "SELECT * FROM fa_pr_rubr WHERE cod_rubr = " & rubrica
        rsPrRubr.CursorType = adOpenStatic
        rsPrRubr.CursorLocation = adUseServer
        rsPrRubr.Open sSql, gConexaoSecundaria
        If rsPrRubr.RecordCount >= 1 Then
            While Not rsPrRubr.EOF
                For i = 1 To TotalPrec
                    If BL_HandleNull(rsPrRubr!Portaria, "") = EstrutPrec(i).Portaria Then
                        EstrutPrec(i).cod_rubr_efr = BL_HandleNull(rsPrRubr!cod_rubr_efr, "")
                        EstrutPrec(i).descr_rubr_efr = EcDescrRubrica
                        EstrutPrec(i).nr_c = BL_HandleNull(rsPrRubr!nr_c, "")
                        EstrutPrec(i).nr_k = BL_HandleNull(rsPrRubr!nr_k, "")
                        EstrutPrec(i).perc_pag_doe = BL_HandleNull(rsPrRubr!perc_pag_doe, "")
                        EstrutPrec(i).t_fac = BL_HandleNull(rsPrRubr!t_fac, "")
                        EstrutPrec(i).val_pag_doe = BL_HandleNull(rsPrRubr!val_pag_doe, "")
                        EstrutPrec(i).val_pag_ent = BL_HandleNull(rsPrRubr!val_pag_ent, "")
                        EstrutPrec(i).val_Taxa = BL_HandleNull(rsPrRubr!val_Taxa, "")
                        EstrutPrec(i).user_cri = BL_HandleNull(rsPrRubr!user_cri, "")
                        EstrutPrec(i).user_act = BL_HandleNull(rsPrRubr!user_act, "")
                        EstrutPrec(i).dt_cri = BL_HandleNull(rsPrRubr!dt_cri, "")
                        EstrutPrec(i).dt_act = BL_HandleNull(rsPrRubr!dt_act, "")
                        EstrutPrec(i).flg_modificado = True
                 
                        FgPrecos.TextMatrix(i, lColPrecosCodRubrEFR) = EstrutPrec(i).cod_rubr_efr
                        FgPrecos.TextMatrix(i, lColPrecosDescrRubrEFR) = EstrutPrec(i).descr_rubr_efr
                        FgPrecos.TextMatrix(i, lColPrecosNumC) = EstrutPrec(i).nr_c
                        FgPrecos.TextMatrix(i, lColPrecosNumK) = EstrutPrec(i).nr_k
                        FgPrecos.TextMatrix(i, lColPrecosPercDoente) = EstrutPrec(i).perc_pag_doe
                        If BL_HandleNull(rsPrRubr!t_fac, "") = "1" Then
                            FgPrecos.TextMatrix(i, lColPrecosTipoFact) = "KS E CS"
                        ElseIf BL_HandleNull(rsPrRubr!t_fac, "") = "2" Then
                            FgPrecos.TextMatrix(i, lColPrecosTipoFact) = "EUROS"
                        End If
                        FgPrecos.TextMatrix(i, lColPrecosValPagDoe) = EstrutPrec(i).val_pag_doe
                        FgPrecos.TextMatrix(i, lColPrecosValPagEFR) = EstrutPrec(i).val_pag_ent
                        FgPrecos.TextMatrix(i, lColPrecosTxModeradora) = EstrutPrec(i).val_Taxa
                        Exit For
                    End If
                Next
                rsPrRubr.MoveNext
            Wend
        End If
        rsPrRubr.Close
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Copiar R�brica", Me.Name, "CopiaRubrica"
    Exit Sub
    Resume Next
End Sub


Private Sub BtGravaRubr_Click()
    Dim SQLQuery As String
    Dim RsFact As New ADODB.recordset
    On Error GoTo TrataErro
    
    SQLQuery = "SELECT * from fa_rubr WHERE cod_rubr = " & EcRubrCodigo
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    RsFact.Open SQLQuery, gConexaoSecundaria
    If RsFact.RecordCount >= 1 Then
        BG_Mensagem mediMsgBox, "R�brica indicada j� existe.", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    On Error GoTo TrataErro
    EcRubrDtCri = Bg_DaData_ADO
    EcRubrUserCri = gCodUtilizador
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabelaFactus, CamposFactusBD, CamposFactusEc)
    gConexaoSecundaria.Execute SQLQuery
    BG_Mensagem mediMsgBox, "R�brica Inserida.", vbExclamation, "ATEN��O"

    FrameRubrica.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar Nova Rubrica ", Me.Name, "BtGravaRubr_Click"
    BG_RollbackTransaction
    Exit Sub
    Resume Next
End Sub

Private Sub BtSairRubr_Click()
    FrameRubrica.Visible = False
End Sub
' ------------------------------------------------------

' GRAVA DADOS DE FACTURA��O

' ------------------------------------------------------

Private Function GravaFacturacao() As Boolean
    Dim sSql As String
    Dim FlgfacturarAna As Integer
    Dim flgRegra As Integer
    Dim ContaMembros As Integer
    Dim rsPortaria As New ADODB.recordset
    Dim portariaAtiba As Integer
    On Error GoTo TrataErro
    
    GravaFacturacao = False
    If CkFlgfacturarAna.value = vbChecked Then
        FlgfacturarAna = 1
    Else
        FlgfacturarAna = 0
    End If
    If CkFlgRegra.value = vbChecked Then
        flgRegra = 1
    Else
        flgRegra = 0
    End If
    If CkContaMembros.value = vbChecked Then
        ContaMembros = 1
    Else
        ContaMembros = 0
    End If
    
    If EcRubrSeqAna <> "" And EcCodRubrica.Text = "" Then
        sSql = "DELETE FROM sl_ana_facturacao WHERE seq_ana = " & EcRubrSeqAna
        BG_ExecutaQuery_ADO sSql
    ElseIf EcCodRubrica <> "" Then
        sSql = " SELECT x2.cod_portaria FROM sl_portarias x2 Where " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN dt_ini"
        sSql = sSql & " AND NVL (dt_fim,'31-12-2099')"
        rsPortaria.CursorType = adOpenStatic
        rsPortaria.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPortaria.Open sSql, gConexao
        If rsPortaria.RecordCount <> 1 Then
            GravaFacturacao = False
            Exit Function
        Else
            portariaAtiba = BL_HandleNull(rsPortaria!cod_portaria, mediComboValorNull)
        End If
        rsPortaria.Close
        Set rsPortaria = Nothing
        If EcRubrSeqAna.Text = "" Then
            EcRubrSeqAna = BG_DaMAX("sl_ana_facturacao", "seq_ana") + 1
            sSql = "INSERT INTO Sl_ana_facturacao (seq_ana, cod_ana, descr_ana,cod_ana_gh, descr_ana_Facturacao,flg_regra, "
            sSql = sSql & " cod_ana_regra, flg_conta_membros, flg_ana_facturar, cod_ana_facturar,qtd, cod_portaria, user_cri, dt_Cri, hr_cri  ) VALUES( "
            sSql = sSql & EcRubrSeqAna & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcRubrDescr) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodRubrica) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcDescrRubrica) & ", "
            sSql = sSql & flgRegra & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodAnaRegra) & ", "
            sSql = sSql & ContaMembros & ", "
            sSql = sSql & FlgfacturarAna & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodAnaFacturar) & ","
            sSql = sSql & BL_HandleNull(EcQtd, "NULL") & ", "
            sSql = sSql & portariaAtiba & ","
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
            BG_ExecutaQuery_ADO sSql
        Else
            sSql = "UPDATE Sl_ana_facturacao SET cod_ana_gh  = " & BL_TrataStringParaBD(EcCodRubrica) & ","
            sSql = sSql & " flg_regra  = " & flgRegra & ", "
            sSql = sSql & " cod_ana_regra  = " & BL_TrataStringParaBD(EcCodAnaRegra) & ", "
            sSql = sSql & " flg_conta_membros  = " & ContaMembros & ", "
            sSql = sSql & " flg_ana_facturar  = " & FlgfacturarAna & ", "
            sSql = sSql & " cod_ana_facturar  = " & BL_TrataStringParaBD(EcCodAnaFacturar.Text) & ", "
            sSql = sSql & " qtd  = " & BL_HandleNull(EcQtd, "NULL") & ", "
            sSql = sSql & " cod_portaria  = " & portariaAtiba & ", "
            sSql = sSql & " user_act  = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & " dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & " hr_act = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & " WHERE  seq_ana = " & EcRubrSeqAna.Text
            BG_ExecutaQuery_ADO sSql
        End If
    End If
    GravaFacturacao = True
Exit Function
TrataErro:
    GravaFacturacao = False
    BG_LogFile_Erros "Erro a Gravar Factura��o ", Me.Name, "GravaFacturacao"
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------

' GRAVA DADOS DA TABELA DE PRECOS

' ------------------------------------------------------

Private Sub GravaPrecarios()
    Dim sSql As String
    Dim i As Long
    On Error GoTo TrataErro
    
    gConexaoSecundaria.BeginTrans
    
    For i = 1 To TotalPrec
    
        If EstrutPrec(i).flg_modificado = True And EstrutPrec(i).cod_rubr <> "" Then
            sSql = "DELETE FROM fa_pr_rubr WHERE portaria = " & EstrutPrec(i).Portaria & " AND cod_rubr = " & EstrutPrec(i).cod_rubr
            gConexaoSecundaria.Execute sSql
        
            If EstrutPrec(i).cod_rubr_efr <> "" Or EstrutPrec(i).nr_c <> "" Or EstrutPrec(i).nr_k <> "" Or EstrutPrec(i).perc_pag_doe <> "" _
                    Or EstrutPrec(i).val_pag_doe <> "" Or EstrutPrec(i).val_pag_ent <> "" Or EstrutPrec(i).val_pag_doe <> "" _
                    Or EstrutPrec(i).val_Taxa <> "" Or EstrutPrec(i).perc_pag_doe <> "" Then
                    
                sSql = "INSERT INTO fa_pr_rubr (cod_rubr, portaria, cod_Efr, cod_rubr_efr, descr_rubr_efr, val_taxa, t_fac, nr_c, nr_k, "
                sSql = sSql & " val_pag_ent, perc_pag_doe, val_pag_doe,user_cri, dt_cri, user_act, dt_act) VALUES ("
                sSql = sSql & EstrutPrec(i).cod_rubr & ", "
                sSql = sSql & EstrutPrec(i).Portaria & ", "
                sSql = sSql & EstrutPrec(i).cod_efr & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).cod_rubr_efr) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).descr_rubr_efr) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_Taxa, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).t_fac, ",", "."), 2) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_c, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_k, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_ent, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).perc_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_cri) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_cri) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_act) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_act) & ") "
                gConexaoSecundaria.Execute sSql
            End If
            
        End If
    Next
    gConexaoSecundaria.CommitTrans
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar Pre�arios ", Me.Name, "GravaPrecarios"
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub


Private Sub FgPrecos_Click()
    EcLinhaPreco = FgPrecos.row
    EcColunaPreco = FgPrecos.Col
End Sub

Private Sub FgPrecos_DblClick()
    EcLinhaPreco = ""
    EcColunaPreco = ""
    If FgPrecos.row > 0 And FgPrecos.row <= TotalPrec Then
        EcLinhaPreco = FgPrecos.row
        EcColunaPreco = FgPrecos.Col
        If FgPrecos.Col <> lColPrecosCodPrecario And FgPrecos.Col <> lColPrecosDescrPrecario And FgPrecos.Col <> lColPrecosTipoFact Then
            EcAuxPreco.Visible = True
            EcAuxPreco.left = FgPrecos.CellLeft + FgPrecos.left
            EcAuxPreco.top = FgPrecos.CellTop + FgPrecos.top
            EcAuxPreco.Width = FgPrecos.CellWidth + 10
            EcAuxPreco.SetFocus
        ElseIf FgPrecos.Col = lColPrecosTipoFact Then
            If EstrutPrec(EcLinhaPreco).t_fac = "1" Then
                EstrutPrec(EcLinhaPreco).t_fac = "2"
            Else
                EstrutPrec(EcLinhaPreco).t_fac = "1"
            End If
            If EstrutPrec(EcLinhaPreco).t_fac = "1" Then
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosTipoFact) = "KS E CS"
            ElseIf EstrutPrec(EcLinhaPreco).t_fac = "2" Then
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosTipoFact) = "EUROS"
            End If
    
        Else
            Exit Sub
        End If
        
        EstrutPrec(EcLinhaPreco).flg_modificado = True
        If EstrutPrec(EcLinhaPreco).user_cri = "" Then
            EstrutPrec(EcLinhaPreco).user_cri = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_cri = Bg_DaData_ADO
        Else
            EstrutPrec(EcLinhaPreco).user_act = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_act = Bg_DaData_ADO
        End If
        
        If FgPrecos.Col = lColPrecosCodRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).cod_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosDescrRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).descr_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosTxModeradora Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_Taxa
        ElseIf FgPrecos.Col = lColPrecosNumC Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_c
        ElseIf FgPrecos.Col = lColPrecosNumK Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_k
        ElseIf FgPrecos.Col = lColPrecosValPagEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_ent
        ElseIf FgPrecos.Col = lColPrecosValPagDoe Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_doe
        ElseIf FgPrecos.Col = lColPrecosPercDoente Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).perc_pag_doe
        End If
    End If
End Sub

Private Sub FgPrecos_GotFocus()
    EcAuxPreco.Visible = False
End Sub

Private Sub FgPrecos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FgPrecos_DblClick
    End If
End Sub

Private Sub EcAuxPreco_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If EcLinhaPreco <> "" And EcColunaPreco <> "" Then
            If EcColunaPreco = lColPrecosCodRubrEFR Then
                EstrutPrec(EcLinhaPreco).cod_rubr_efr = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosDescrRubrEFR Then
                EstrutPrec(EcLinhaPreco).descr_rubr_efr = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosTxModeradora Then
                EstrutPrec(EcLinhaPreco).val_Taxa = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosNumC Then
                EstrutPrec(EcLinhaPreco).nr_c = EcAuxPreco
                EstrutPrec(EcLinhaPreco).val_pag_ent = Round(CDbl(EstrutPrec(EcLinhaPreco).nr_c) * CDbl(EstrutPrec(EcLinhaPreco).valor_c), 2)
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosValPagEFR) = EstrutPrec(EcLinhaPreco).val_pag_ent
            ElseIf EcColunaPreco = lColPrecosNumK Then
                EstrutPrec(EcLinhaPreco).nr_k = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosPercDoente Then
                EstrutPrec(EcLinhaPreco).perc_pag_doe = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosValPagDoe Then
                EstrutPrec(EcLinhaPreco).val_pag_doe = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosValPagEFR Then
                EstrutPrec(EcLinhaPreco).val_pag_ent = EcAuxPreco
            End If
            FgPrecos.TextMatrix(EcLinhaPreco, EcColunaPreco) = EcAuxPreco
            EcAuxPreco = ""
            EcAuxPreco.Visible = False
            FgPrecos.SetFocus
        End If
    End If
End Sub

Private Sub EcAuxPreco_LostFocus()
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
End Sub

Private Sub EcAuxPreco_Validate(Cancel As Boolean)
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
End Sub

Private Sub EcOrdemARS_Validate(Cancel As Boolean)
    If EcOrdemARS = "" Then Exit Sub
    If Not IsNumeric(EcOrdemARS) Then
        BG_Mensagem mediMsgBox, "Valor tem que ser num�rico.", vbExclamation + vbOKOnly, "Ordem ARS."
        EcOrdemARS.SetFocus
    End If
End Sub

' ----------------------------------------------------------------------

' GRAVA SINONIMO IGUAL A DESCRICAO

' ----------------------------------------------------------------------
Private Sub GravaSinonimo()
    Dim sSql As String
    
    sSql = "DELETE FROM sl_ana_sinonimos WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo) & " AND flg_original = 1 "
    BG_ExecutaQuery_ADO sSql
    
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_ana_sinonimos (seq_sinonimo, cod_ana, descr_sinonimo, flg_original) VALUES (seq_sinonimo.nextval, "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcDescricao) & ",1)"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_ana_sinonimos ( cod_ana, descr_sinonimo, flg_original) VALUES ( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcDescricao) & ",1)"
    End If
    
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Gravar Sinonimos", Me.Name, "GravaSinonimo"
    Exit Sub
    Resume Next
End Sub


Private Sub EcCodgrImpr_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrImpr)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrImpr.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_impr, descr_gr_impr FROM sl_gr_impr WHERE upper(cod_gr_impr)= " & BL_TrataStringParaBD(UCase(EcCodGrImpr.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrImpr.Text = "" & RsDescrGrupo!cod_gr_impr
            EcDescrGrImpr.Text = "" & RsDescrGrupo!descr_gr_impr
        Else
            Cancel = True
            EcDescrGrImpr.Text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrImpr.Text = ""
    End If

End Sub

Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrupo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrupo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE upper(cod_gr_ana)= " & BL_TrataStringParaBD(UCase(EcCodGrupo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrupo.Text = "" & RsDescrGrupo!cod_gr_ana
            EcDescrGrupo.Text = "" & RsDescrGrupo!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrupo.Text = ""
    End If

End Sub

Private Sub EcCodsubGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrupo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodSubGrupo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_sgr_ana, descr_sgr_ana FROM sl_sgr_ana WHERE upper(cod_sgr_ana)= " & BL_TrataStringParaBD(UCase(EcCodSubGrupo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodSubGrupo.Text = "" & RsDescrGrupo!cod_sgr_ana
            EcDescrSubGrupo.Text = "" & RsDescrGrupo!descr_sgr_ana
        Else
            Cancel = True
            EcDescrSubGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O Subgrupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrSubGrupo.Text = ""
    End If

End Sub

Private Sub EcCodclasse_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodClasse.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_classe_ana, descr_classe_ana FROM sl_classe_ana WHERE upper(cod_classe_ana)= " & BL_TrataStringParaBD(UCase(EcCodClasse.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodClasse.Text = "" & RsDescrGrupo!cod_classe_ana
            EcDescrClasse.Text = "" & RsDescrGrupo!descr_classe_ana
        Else
            Cancel = True
            EcDescrClasse.Text = ""
            BG_Mensagem mediMsgBox, "A Classe indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrClasse.Text = ""
    End If

End Sub

Private Sub EcCodProd_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProd.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_produto, descr_produto FROM sl_produto WHERE upper(cod_produto)= " & BL_TrataStringParaBD(UCase(EcCodProd.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodProd.Text = "" & RsDescrGrupo!cod_produto
            EcDescrProduto.Text = "" & RsDescrGrupo!descr_produto
        Else
            Cancel = True
            EcDescrProduto.Text = ""
            BG_Mensagem mediMsgBox, "O Produto indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrProduto.Text = ""
    End If

End Sub

Private Sub EcCodtubop_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodTuboP.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_tubo, descr_tubo FROM sl_tubo WHERE upper(cod_tubo)= " & BL_TrataStringParaBD(UCase(EcCodTuboP.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodTuboP.Text = "" & RsDescrGrupo!cod_tubo
            EcDescrTuboP.Text = "" & RsDescrGrupo!descR_tubo
        Else
            Cancel = True
            EcDescrTuboP.Text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrTuboP.Text = ""
    End If
End Sub

Private Sub BtPesquisaGrupo_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.Text = resultados(1)
            EcDescrGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub
Private Sub BtPesquisaGrImpr_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_impr"
    CamposEcran(1) = "seq_gr_impr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_impr"
    CamposEcran(2) = "descr_gr_impr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_impr"
    CampoPesquisa1 = "descr_gr_impr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de Impress�o")
    
    mensagem = "N�o foi encontrada nenhum Grupo de Impress�o."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrImpr.Text = resultados(1)
            EcDescrGrImpr.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisasubGrupo_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_sgr_ana"
    CamposEcran(1) = "cod_sgr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_sgr_ana"
    CamposEcran(2) = "descr_sgr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_sgr_ana"
    CampoPesquisa1 = "descr_sgr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar SubGrupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum SubGrupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodSubGrupo.Text = resultados(1)
            EcDescrSubGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub



Private Sub BtPesquisaclasse_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_classe_ana"
    CamposEcran(1) = "cod_classe_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_classe_ana"
    CamposEcran(2) = "descr_classe_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_classe_ana"
    CampoPesquisa1 = "descr_classe_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Classe de An�lises")
    
    mensagem = "N�o foi encontrada nenhuma classe de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodClasse.Text = resultados(1)
            EcDescrClasse.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaProduto_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_produto"
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_produto"
    CampoPesquisa1 = "descr_produto"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Produto")
    
    mensagem = "N�o foi encontrado nenhum produto."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProd.Text = resultados(1)
            EcDescrProduto.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub BtPesquisaTubop_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTuboP.Text = resultados(1)
            EcDescrTuboP.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub





Private Sub BtPesquisaGrPerfis_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_perfis"
    CamposEcran(1) = "seq_gr_perfis"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_perfis"
    CamposEcran(2) = "descr_gr_perfis"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_perfis"
    CampoPesquisa1 = "descr_gr_perfis"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de Exames")
    
    mensagem = "N�o foi encontrado nenhum Grupo de Exames."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodigoGrPerfis.Text = resultados(1)
            EcDescrGrPerfis.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodigogrperfis_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodigoGrPerfis.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_perfis, descr_gr_perfis FROM sl_gr_perfis WHERE upper(cod_gr_perfis)= " & BL_TrataStringParaBD(UCase(EcCodigoGrPerfis.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodigoGrPerfis.Text = "" & RsDescrGrupo!cod_gr_perfis
            EcDescrGrPerfis.Text = "" & RsDescrGrupo!descr_gr_perfis
        Else
            Cancel = True
            EcDescrGrPerfis.Text = ""
            BG_Mensagem mediMsgBox, "O grupo de exames indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrPerfis.Text = ""
    End If
End Sub



Private Sub BtPesquisametodo_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_metodo"
    CamposEcran(1) = "seq_metodo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_metodo"
    CamposEcran(2) = "descr_metodo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_metodo"
    CampoPesquisa1 = "descr_metodo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Metodos")
    
    mensagem = "N�o foi encontrado nenhum Metodo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMetodo.Text = resultados(1)
            EcDescrMetodo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodMetodo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodMetodo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_metodo, descr_metodo FROM sl_metodo WHERE upper(cod_metodo)= " & BL_TrataStringParaBD(UCase(EcCodMetodo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodMetodo.Text = "" & RsDescrGrupo!cod_metodo
            EcDescrMetodo.Text = "" & RsDescrGrupo!descr_metodo
        Else
            Cancel = True
            EcDescrMetodo.Text = ""
            BG_Mensagem mediMsgBox, "O grupo de exames indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrMetodo.Text = ""
    End If
End Sub

Private Sub EcCodTuboS_LostFocus()
    
    EcCodTuboS.Text = UCase(EcCodTuboS.Text)
    

End Sub
Private Sub EcCodtuboS_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodTuboS.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_tubo, descr_tubo FROM sl_Tubo_sec WHERE upper(cod_tubo)= " & BL_TrataStringParaBD(UCase(EcCodTuboS.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodTuboS.Text = "" & RsDescrGrupo!cod_tubo
            EcDescrTuboS.Text = "" & RsDescrGrupo!descR_tubo
        Else
            Cancel = True
            EcDescrTuboS.Text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrTuboS.Text = ""
    End If

End Sub

Private Sub BtPesquisaTubos_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo_sec"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTuboS.Text = resultados(1)
            EcDescrTuboS.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub



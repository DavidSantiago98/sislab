VERSION 5.00
Begin VB.Form FormFACTUSNovasAnalises 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFacturacaoNovasAnalises"
   ClientHeight    =   6060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6765
   Icon            =   "FormFACTUSNovasAnalises.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   6765
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Caption         =   "R�brica "
      Height          =   1335
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   6495
      Begin VB.TextBox EcDescrEfr 
         Height          =   285
         Left            =   2280
         TabIndex        =   25
         Top             =   960
         Width           =   3615
      End
      Begin VB.TextBox EcCodEfr 
         Height          =   285
         Left            =   1440
         TabIndex        =   24
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox EcSeqAna 
         Height          =   285
         Left            =   1440
         TabIndex        =   20
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox EcCodAna 
         Height          =   285
         Left            =   1440
         TabIndex        =   19
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox EcDescrAna 
         Height          =   285
         Left            =   2280
         TabIndex        =   18
         Top             =   600
         Width           =   3615
      End
      Begin VB.Label Label3 
         Caption         =   "Entidade"
         Height          =   255
         Left            =   480
         TabIndex        =   23
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Factura��o"
         Height          =   255
         Left            =   480
         TabIndex        =   22
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Sislab"
         Height          =   255
         Left            =   480
         TabIndex        =   21
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "N�o facturar a r�brica para "
      Height          =   2480
      Left            =   120
      TabIndex        =   13
      Top             =   1560
      Width           =   6495
      Begin VB.CommandButton ButSelNada 
         Caption         =   "Nenhuma"
         Height          =   315
         Left            =   5280
         TabIndex        =   16
         Top             =   2040
         Width           =   975
      End
      Begin VB.CommandButton ButSelTudo 
         Caption         =   "Todas"
         Height          =   315
         Left            =   4200
         TabIndex        =   15
         Top             =   2040
         Width           =   975
      End
      Begin VB.ListBox ListaEFR 
         Height          =   1635
         Left            =   240
         Style           =   1  'Checkbox
         TabIndex        =   14
         Top             =   315
         Width           =   6015
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pesquisa R�pida "
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   4080
      Width           =   6495
      Begin VB.CommandButton BtPesqRapP 
         Height          =   320
         Left            =   5880
         Picture         =   "FormFACTUSNovasAnalises.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapC 
         Height          =   320
         Left            =   5880
         Picture         =   "FormFACTUSNovasAnalises.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   840
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapS 
         Height          =   320
         Left            =   5880
         Picture         =   "FormFACTUSNovasAnalises.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcDescrAnaP 
         Height          =   285
         Left            =   2280
         TabIndex        =   9
         Top             =   1320
         Width           =   3615
      End
      Begin VB.TextBox EcDescrAnaC 
         Height          =   285
         Left            =   2280
         TabIndex        =   8
         Top             =   840
         Width           =   3615
      End
      Begin VB.TextBox EcCodAnaP 
         Height          =   285
         Left            =   1440
         TabIndex        =   7
         Top             =   1320
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaC 
         Height          =   285
         Left            =   1440
         TabIndex        =   6
         Top             =   840
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaS 
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox EcDescrAnaS 
         Height          =   285
         Left            =   2280
         TabIndex        =   4
         Top             =   360
         Width           =   3615
      End
      Begin VB.OptionButton OptPerfis 
         Caption         =   "Perfis"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   1320
         Width           =   1095
      End
      Begin VB.OptionButton OptComplexas 
         Caption         =   "Complexas"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   1095
      End
      Begin VB.OptionButton OptSimples 
         Caption         =   "Simples"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   855
      End
   End
End
Attribute VB_Name = "FormFACTUSNovasAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub BtPesqRapC_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_c"
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_c"
    CampoPesquisa = "descr_ana_c"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaC.text = Resultados(2)
            EcCodAnaC.text = Resultados(1)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaC.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises complexas", vbExclamation, "ATEN��O"
        EcCodAnaC.SetFocus
    End If

End Sub

Private Sub BtPesqRapP_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_perfis"
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis"
    CampoPesquisa = "descr_perfis"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaP.text = Resultados(1)
            EcDescrAnaP.text = Resultados(2)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaP.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem perfis", vbExclamation, "ATEN��O"
        EcCodAnaP.SetFocus
    End If

End Sub

Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.text = Resultados(2)
            EcCodAnaS.text = Resultados(1)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If

End Sub

Private Sub ButSelNada_Click()
   
    On Error Resume Next
    
    Dim i As Integer
    
    Me.ListaEFR.Visible = False
    For i = Me.ListaEFR.ListCount To 0 Step -1
        Me.ListaEFR.Selected(i) = False
    Next
    Me.ListaEFR.ListIndex = 0
    Me.ListaEFR.Visible = True

End Sub

Private Sub ButSelTudo_Click()
    
    On Error Resume Next
    
    Dim i As Integer
    
    Me.ListaEFR.Visible = False
    For i = Me.ListaEFR.ListCount To 0 Step -1
        Me.ListaEFR.Selected(i) = True
    Next
    Me.ListaEFR.ListIndex = 0
    Me.ListaEFR.Visible = True

End Sub

Private Sub EcCodAnaC_LostFocus()

    Dim Sql As String
    Dim RsAna As ADODB.recordset
    
    If EcCodAnaC.text <> "" Then
        Sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c=" & BL_TrataStringParaBD(EcCodAnaC.text)
        Set RsAna = New ADODB.recordset
        RsAna.CursorLocation = adUseClient
        RsAna.CursorType = adOpenStatic
        RsAna.Open Sql, gConexao
        If RsAna.RecordCount > 0 Then
            EcDescrAnaC.text = BL_HandleNull(RsAna!Descr_Ana_C, "")
            EcCodAna.text = EcCodAnaC.text
            EcDescrAna.text = EcDescrAnaC.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaC.text = ""
            EcCodAnaC.SetFocus
        End If
        RsAna.Close
        Set RsAna = Nothing
    End If
    
End Sub

Private Sub EcCodAnaP_LostFocus()

    Dim Sql As String
    Dim RsAna As ADODB.recordset
    
    If EcCodAnaP.text <> "" Then
        Sql = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis=" & BL_TrataStringParaBD(EcCodAnaP.text)
        Set RsAna = New ADODB.recordset
        RsAna.CursorLocation = adUseClient
        RsAna.CursorType = adOpenStatic
        RsAna.Open Sql, gConexao
        If RsAna.RecordCount > 0 Then
            EcDescrAnaP.text = BL_HandleNull(RsAna!descr_perfis, "")
            EcCodAna.text = EcCodAnaP.text
            EcDescrAna.text = EcDescrAnaP.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaP.text = ""
            EcCodAnaP.SetFocus
        End If
        RsAna.Close
        Set RsAna = Nothing
    End If
    
End Sub

Private Sub EcCodAnaS_LostFocus()

    Dim Sql As String
    Dim RsAna As ADODB.recordset
    
    If EcCodAnaS.text <> "" Then
        Sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(EcCodAnaS.text)
        Set RsAna = New ADODB.recordset
        RsAna.CursorLocation = adUseClient
        RsAna.CursorType = adOpenStatic
        RsAna.Open Sql, gConexao
        If RsAna.RecordCount > 0 Then
            EcDescrAnaS.text = BL_HandleNull(RsAna!Descr_Ana_S, "")
            EcCodAna.text = EcCodAnaS.text
            EcDescrAna.text = EcDescrAnaS.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaS.text = ""
            EcCodAnaS.SetFocus
        End If
        RsAna.Close
        Set RsAna = Nothing
    End If
    
End Sub

Private Sub EcSeqAna_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcSeqAna)

End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate
    
End Sub

Sub Form_Unload(Cancel As Integer)

    EventoUnload
    
End Sub

Sub Inicializacoes()
    
    Me.Caption = " Cria��o de C�digo de R�brica para a Factura��o"
    Me.Left = 400
    Me.Top = 20
    Me.Width = 6855
    Me.Height = 6435 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_ana_facturacao"
    Set CampoDeFocus = EcSeqAna
    
    NumCampos = 3
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_ana"
    CamposBD(1) = "cod_ana"
    CamposBD(2) = "descr_ana"
    'CamposBD(3) = "cod_efr"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeqAna
    Set CamposEc(1) = EcCodAna
    Set CamposEc(2) = EcDescrAna
    'Set CamposEc(3) = EcCodEfr
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    '...
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_ana"
    Set ChaveEc = EcSeqAna
    
    EcDescrAnaS.Enabled = False
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    EcDescrAnaC.Enabled = False
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    EcDescrAnaP.Enabled = False
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormFACTUSNovasAnalises = Nothing
    
End Sub

Sub LimpaCampos()

    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    
    EcCodAnaS.text = ""
    EcCodAnaC.text = ""
    EcCodAnaP.text = ""
    
    EcDescrAnaS.text = ""
    EcDescrAnaC.text = ""
    EcDescrAnaP.text = ""
    
    EcDescrAnaS.Enabled = False
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    EcDescrAnaC.Enabled = False
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    EcDescrAnaP.Enabled = False
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    OptSimples.Value = vbUnchecked
    OptComplexas.Value = vbUnchecked
    OptPerfis.Value = vbUnchecked
    
    Me.ListaEFR.Clear
    DoEvents
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
        ' Preenche a listbox com entidades a n�o facturar.
        Me.ListaEFR.Visible = False
        Call Carrega_EFRs(Me.ListaEFR, Trim(EcCodAna))
        Me.ListaEFR.Visible = True

    End If
    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.Caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    If ((Len(Trim(Me.EcSeqAna.text)) = 0) And _
        (Len(Trim(Me.EcDescrAna.text)) = 0) And _
        (Len(Trim(Me.EcCodAna.text)) = 0)) Then
    
        MsgBox "Deve Indicar um crit�rio de pesquisa.    ", vbInformation, Me.Caption
        Exit Sub
    End If
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY seq_ana ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open CriterioTabela, gConexao
    
    If (rs.RecordCount <= 0) Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        Call LimpaCampos
        Call PreencheCampos
    
        BL_ToolbarEstadoN Estado
'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        
        BL_FimProcessamento Me
    End If
        
End Sub

Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim rv As Integer
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcSeqAna.text = BG_DaMAX(NomeTabela, "seq_ana") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    rv = Grava_EFRs_Nao_Facturar(EcCodAna.text, Me.ListaEFR)
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim rv As Integer
        
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
   
    rv = Grava_EFRs_Nao_Facturar(EcCodAna.text, Me.ListaEFR)

End Sub

Sub FuncaoRemover()
    
    'nada

End Sub

Sub BD_Delete()
   
   'nada

End Sub

Private Sub OptComplexas_Click()
    
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Enabled = True
    BtPesqRapC.Enabled = True
    
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaC.SetFocus
    
End Sub

Private Sub OptPerfis_Click()

    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Enabled = True
    BtPesqRapP.Enabled = True
    
    EcCodAnaP.SetFocus
    
End Sub

Private Sub OptSimples_Click()
    
    EcCodAnaS.Enabled = True
    BtPesqRapS.Enabled = True
    
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaS.SetFocus
    
End Sub

Private Function Carrega_EFRs(lista As ListBox, _
                              cod_ana As String) As Integer

    ' Carrega a lista de EFRs.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim Sql As String
    Dim ret As String
    Dim cod_efr As String
    
    ret = ""
    
    cod_ana = UCase(Trim(cod_ana))
    
    If (Len(cod_ana) = 0) Then
        Carrega_EFRs = -2
        Exit Function
    End If
        
    Sql = "SELECT " & _
          "     cod_efr, " & _
          "     descr_efr " & _
          "FROM " & _
          "     sl_efr " & _
          "ORDER BY " & _
          "     descr_efr"
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.Open Sql, gConexao
        
    lista.Clear
    DoEvents
    DoEvents
    
    While Not (rs_aux.EOF)
        If (Not IsNull(rs_aux(1))) Then
            
            If (Not IsNull(rs_aux(0))) Then
               cod_efr = Trim(rs_aux(0))
               
               lista.AddItem Trim(rs_aux(1))
               lista.ItemData(lista.ListCount - 1) = CLng(cod_efr)
               
               If (ANALISE_Para_Facturar(Trim(EcCodAna.text), cod_efr) = True) Then
                    lista.Selected(lista.ListCount - 1) = False
               Else
                    lista.Selected(lista.ListCount - 1) = True
               End If
               
            End If
        
        End If
        rs_aux.MoveNext
    Wend
    lista.ListIndex = 0
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    Carrega_EFRs = 1

Exit Function
ErrorHandler:
    Select Case Err.number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Carrega_EFRs (FormFACTUSNovasAnalises) -> " & Err.Description)
            Carrega_EFRs = -1
            Exit Function
    End Select
End Function

Private Function Grava_EFRs_Nao_Facturar(cod_ana As String, _
                                         lista As ListBox) As Integer
    
    On Error GoTo ErrorHandler
    
    Dim i As Integer
    Dim Sql As String
    Dim cmd As ADODB.Command

    cod_ana = Trim(cod_ana)
    If (Len(cod_ana) = 0) Then
        Grava_EFRs_Nao_Facturar = -2
        Exit Function
    End If
    
    Call BG_BeginTransaction
    
    ' Apaga as refer�ncias existentes na tabela para a an�lise.
    
    Set cmd = New ADODB.Command
    
    Sql = "DELETE " & _
          "FROM " & _
          "     sl_nao_facturar " & _
          "WHERE " & _
          "     cod_ana = '" & cod_ana & "'"
    
    cmd.ActiveConnection = gConexao
    cmd.CommandText = Sql
    cmd.Execute
        
    
    ' Insere cada uma das linhas da lista.
    
    For i = 0 To lista.ListCount - 1
        If (lista.Selected(i)) Then
            Sql = "INSERT INTO sl_nao_facturar " & vbCrLf & _
                  "(cod_ana, cod_efr) " & vbCrLf & _
                  "VALUES " & vbCrLf & _
                  "('" & cod_ana & "', " & lista.ItemData(i) & ")"
        
            cmd.CommandText = Sql
            cmd.Execute
        End If
    Next
    
    Set cmd = Nothing
    
    Call BG_CommitTransaction
    
    Grava_EFRs_Nao_Facturar = 1

Exit Function
ErrorHandler:
    Select Case Err.number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Grava_EFRs_Nao_Facturar = 1 (FormFACTUSNovasAnalises) -> " & Err.Description)
            Call BG_RollbackTransaction
            Grava_EFRs_Nao_Facturar = -1
            Exit Function
    End Select
End Function

Private Sub Text2_Change()

End Sub

VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form FormEstMicro 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstMicro"
   ClientHeight    =   6870
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9495
   Icon            =   "FormEstMicro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   9495
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Pesquisa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   120
      TabIndex        =   7
      Top             =   0
      Width           =   5295
      Begin VB.ComboBox CbMicro 
         Height          =   315
         Left            =   2280
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   480
         Width           =   2775
      End
      Begin MSComCtl2.DTPicker CbDataFinal 
         Height          =   255
         Left            =   480
         TabIndex        =   10
         Top             =   1080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   144965633
         CurrentDate     =   38774
      End
      Begin MSComCtl2.DTPicker CbDataInicial 
         Height          =   255
         Left            =   480
         TabIndex        =   8
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   144965633
         CurrentDate     =   38774
      End
      Begin VB.Label Label4 
         Caption         =   "Microrganismo"
         Height          =   255
         Left            =   1920
         TabIndex        =   12
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Data Final"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Data Inicial"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Gr�fico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   5640
      TabIndex        =   2
      Top             =   0
      Width           =   3735
      Begin VB.ComboBox CbTipoGrafico 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1080
         Width           =   1935
      End
      Begin VB.OptionButton Opt2 
         Caption         =   "Di�rio"
         Height          =   255
         Left            =   2160
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Mensal"
         Height          =   255
         Left            =   600
         TabIndex        =   3
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Modelo"
         Height          =   255
         Left            =   600
         TabIndex        =   6
         Top             =   840
         Width           =   615
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4815
      Left            =   120
      TabIndex        =   0
      Top             =   1680
      Width           =   9255
      Begin MSChart20Lib.MSChart Grafico 
         Height          =   4455
         Left            =   120
         OleObjectBlob   =   "FormEstMicro.frx":000C
         TabIndex        =   1
         Top             =   240
         Width           =   9015
      End
   End
End
Attribute VB_Name = "FormEstMicro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Type Grafico
    dataInicial As String
    dataFinal As String
    desc_micro As String
    cod_micro As String
    tipo_grafico As Integer
    dispocisao As Integer     ' 0 - mensal 1- diario
    total_colunas As Long
End Type
Dim estrutGrafico() As Grafico
Dim tamEstrutGrafico As Long

Dim legendas() As String
Dim rowLabels() As String

Private Type dados
    Label As String
    descr_antibio As String
    cod_antibio As String
    total As Long
End Type
Dim estrutDados() As dados
Dim tamEstrutDados As Long


Dim Flg_DataInicialValida As Boolean
Dim Flg_DataFinalValida As Boolean



Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Estatistica Microbiologia"
    Me.left = 50
    Me.top = 50
    Me.Width = 9585
    Me.Height = 7245
    
    tamEstrutGrafico = 0
    ReDim estrutGrafico(tamEstrutGrafico)
    tamEstrutGrafico = 1
    ReDim estrutGrafico(tamEstrutGrafico)
    
    tamEstrutDados = 0
    ReDim estrutDados(tamEstrutDados)
    
    ReDim legendas(0)
    ReDim rowLabels(0)
    
    Opt1.value = True
    Opt1_Click
    
    Set CampoDeFocus = CbDataInicial
    Flg_DataInicialValida = False
    Flg_DataFinalValida = False
    
    Grafico.ColumnCount = 0
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes
    
    '
    'BT_DefineIdioma

    DefTipoCampos
    PreencheValoresDefeito
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    
    Set FormEstMicro = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    CbMicro.ListIndex = mediComboValorNull

    Grafico.ColumnCount = 0
    Flg_DataInicialValida = False
    Flg_DataFinalValida = False
    
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    CampoDeFocus.SetFocus
    tamEstrutGrafico = 0
    ReDim estrutGrafico(tamEstrutGrafico)
    tamEstrutGrafico = 1
    ReDim estrutGrafico(tamEstrutGrafico)
    
    tamEstrutDados = 0
    ReDim estrutDados(tamEstrutDados)
    

    
    Opt1.value = True
    Opt1_Click
      
    ReDim legendas(0)
    ReDim rowLabels(0)
      
End Sub

Sub DefTipoCampos()
    CbDataInicial.Tag = adDate
    CbDataFinal.Tag = adDate
End Sub

Sub PreencheCampos()
    'nada
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    'nada
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

'    For i = 0 To NumCampos - 1
'        If TextoCamposObrigatorios(i) <> "" Then
'            iRes = BG_ValidaCampo(gFormActivo.Caption, CamposEc(i), TextoCamposObrigatorios(i))
'            If iRes = vbOK Or iRes = vbCancel Then
'                ValidaCamposEc = False
'                Exit Function
'            End If
'        End If
'    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    Dim retorno As Boolean
    tamEstrutDados = 0
    ReDim estrutDados(tamEstrutDados)
    Grafico.ColumnCount = 0
    Grafico.RowCount = 0
    Grafico.Visible = False
    Grafico.ColumnCount = 0
    Grafico.RowCount = 0
    Grafico.Visible = False
    ReDim legendas(0)
    ReDim rowLabels(0)
    If estrutGrafico(1).cod_micro <> "" Then
        If Opt1.value = True Then
            retorno = ProcuraMensal
        ElseIf Opt2.value = True Then
            retorno = ProcuraDiaria
        End If
        If retorno = True Then
            DesenhaGrafico
        Else
            BG_Mensagem mediMsgBox, "N�o foram encontrados registos."
        End If
    End If
End Sub

Sub FuncaoAnterior()
    'nada
End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub BD_Insert()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub BD_Update()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub

Sub BD_Delete()
    'nada
End Sub

Sub Funcao_DataActual()
    'nada
End Sub

Sub FuncaoImprimir()
    ' nada
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_microrg", "seq_microrg", "descr_microrg", CbMicro, mediAscComboDesignacao
    
End Sub

Private Sub cbDataFinal_Validate(Cancel As Boolean)
    Flg_DataFinalValida = False
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CbDataFinal)
    Flg_DataFinalValida = Not Cancel
    If CbDataFinal = "" Then
        Flg_DataFinalValida = False
    ElseIf Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub cbDataInicial_Validate(Cancel As Boolean)
    
    Flg_DataInicialValida = False
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CbDataInicial)
    Flg_DataInicialValida = Not Cancel
    If CbDataInicial = "" Then
        Flg_DataInicialValida = False
    ElseIf Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
End Sub


Private Function ProcuraMensal() As Boolean
    Dim Data1, Data2 As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    Dim flg_sair As Boolean
    Dim retorno As Boolean
    Dim cod_micro As String
    If CbDataFinal < CbDataInicial Then
        Exit Function
    End If
    
    Data1 = CbDataInicial
    flg_sair = False
    retorno = False
    
    cod_micro = DevolveCodMicro(CbMicro.ItemData(CbMicro.ListIndex))
    While Not flg_sair
        If DatasMesmoMes(CStr(Data1), CbDataFinal) Then
            Data2 = CDate(CbDataFinal)
            flg_sair = True
        Else
            Data2 = CDate(CalculaUltimoDiaMes(CStr(Data1)))
        End If
    
        
        sql = " SELECT x2.cod_antib, count(*) conta " & _
            " FROM sl_realiza x1, sl_res_tsq x2, sl_res_micro x3 " & _
            " WHERE x1.seq_realiza = x2.seq_realiza " & _
            " AND x2.seq_realiza = x3.seq_realiza" & _
            " AND x2.res_sensib = 'R'" & _
            " AND (x1.dt_val BETWEEN " & BL_TrataDataParaBD(CStr(Data1)) & " AND " & BL_TrataDataParaBD(CStr(Data2)) & ")" & _
            " AND x3.cod_micro =" & BL_TrataStringParaBD(cod_micro) & _
            " GROUP BY cod_antib "
            
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        rs.Open sql, gConexao
        

        If rs.RecordCount > 0 Then
            retorno = True
            While Not rs.EOF
                PreencheEstrutDados CStr(Data1), rs!conta, rs!cod_antib
                rs.MoveNext
            Wend
        End If
        rs.Close
        Set rs = Nothing
        
        Data1 = CDate(DateAdd("d", 1, Data2))
    Wend
    ProcuraMensal = retorno
    
End Function



' PROCURA DIARIA - AGRUPA O RESULTADO POR DIAS
Private Function ProcuraDiaria() As Boolean
    Dim sql As String
    Dim rs As New ADODB.recordset
    Dim retorno As Boolean
    Dim cod_micro As String
    
    cod_micro = DevolveCodMicro(CbMicro.ItemData(CbMicro.ListIndex))
    sql = " SELECT x1.dt_val, x2.cod_antib, count(*) conta " & _
            " FROM sl_realiza x1, sl_res_tsq x2, sl_res_micro x3 " & _
            " WHERE x1.seq_realiza = x2.seq_realiza " & _
            " AND x2.seq_realiza = x3.seq_realiza" & _
            " AND x2.res_sensib = 'R'" & _
            " AND (x1.dt_val BETWEEN " & BL_TrataDataParaBD(CStr(CbDataInicial)) & " AND " & BL_TrataDataParaBD(CStr(CbDataFinal)) & ")" & _
            " AND x3.cod_micro =" & BL_TrataStringParaBD(cod_micro) & _
        " GROUP BY  dt_val, cod_antib "
        
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        While Not rs.EOF
            PreencheEstrutDados rs!dt_val, rs!conta, rs!cod_antib
            rs.MoveNext
        Wend
        retorno = True
    End If
    rs.Close
    Set rs = Nothing
    ProcuraDiaria = retorno
    
End Function



' PREENCHE A ESTRUTURA DE DADOS. CADA ITEM DA ESTUTURA E UMA COLUNA DIFERENTE
' O NOME_COLUNA NORMALMENTE SER� A DATA
Private Sub PreencheEstrutDados(Label As String, total As Long, cod_antibio As String)
    tamEstrutDados = tamEstrutDados + 1
    ReDim Preserve estrutDados(tamEstrutDados)
    estrutDados(tamEstrutDados).Label = Label
    estrutDados(tamEstrutDados).total = total
    estrutDados(tamEstrutDados).cod_antibio = cod_antibio
    estrutDados(tamEstrutDados).descr_antibio = DevolveDescAntibio(BL_HandleNull(cod_antibio))
    
    PreencheLegendas estrutDados(tamEstrutDados).descr_antibio
    PreencheRowLabels estrutDados(tamEstrutDados).Label
End Sub


' DEVOLVE A DESCRICAO DO CODIGO DO ANTIBIOTICO
Private Function DevolveDescAntibio(cod_antibio As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    sql = " SELECT descr_antibio FROM sl_antibio WHERE cod_antibio = " & BL_TrataStringParaBD(cod_antibio)
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount = 1 Then
        DevolveDescAntibio = BL_HandleNull(rs!descr_antibio)
    End If
    rs.Close
    Set rs = Nothing
End Function


Private Sub CbMicro_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then  'DELETE
        CbMicro.ListIndex = mediComboValorNull
        estrutGrafico(1).cod_micro = ""
        estrutGrafico(1).desc_micro = ""
    End If
End Sub


Private Sub CbMicro_Click()
    If CbMicro.ListIndex <> -1 Then
        estrutGrafico(1).cod_micro = CbMicro.ItemData(CbMicro.ListIndex)
        estrutGrafico(1).desc_micro = CbMicro
    Else
        estrutGrafico(1).cod_micro = ""
        estrutGrafico(1).desc_micro = ""
    End If

End Sub

Private Sub Opt1_Click()
    If Opt1.value = True Then
        estrutGrafico(1).dispocisao = 0
    End If
End Sub
Private Sub Opt2_Click()
    If Opt2.value = True Then
        estrutGrafico(1).dispocisao = 1
    End If
End Sub


'  VERIFICA SE ANTIBIOTICO JA ESTA NO VECTOR, SENAO ACRESCENTA-O
Private Sub PreencheLegendas(Antibiotico As String)
    Dim i As Integer
    Dim tamVector As Integer
    
    tamVector = UBound(legendas)
    For i = 1 To tamVector
        If legendas(i) = Antibiotico Then
            Exit Sub
        End If
    Next
    tamVector = tamVector + 1
    ReDim Preserve legendas(tamVector)
    legendas(tamVector) = Antibiotico
    
End Sub

Private Sub PreencheRowLabels(Label As String)
    Dim i As Integer
    Dim tamVector As Integer
    
    tamVector = UBound(rowLabels)
    For i = 1 To tamVector
        If rowLabels(i) = Label Then
            Exit Sub
        End If
    Next
    tamVector = tamVector + 1
    ReDim Preserve rowLabels(tamVector)
    rowLabels(tamVector) = Label
End Sub


Private Function DatasMesmoMes(Data1 As String, Data2 As String) As Boolean
    If Mid(Data1, 3) = Mid(Data2, 3) Then
        DatasMesmoMes = True
    Else
        DatasMesmoMes = False
    End If
End Function

Private Function CalculaUltimoDiaMes(data As String) As String
    Dim mes As Integer
    Dim ano As Integer
    mes = Mid(data, 4, 2)
    ano = Mid(data, 7)
    Select Case mes
        Case 1
            CalculaUltimoDiaMes = 31
        Case 2
            If (ano Mod 4 = 0 And ano Mod 100 <> 0) Or ano Mod 400 = 0 Then
                CalculaUltimoDiaMes = 29
            Else
                CalculaUltimoDiaMes = 28
            End If
            
        Case 3
            CalculaUltimoDiaMes = 31
        Case 4
            CalculaUltimoDiaMes = 30
        Case 5
            CalculaUltimoDiaMes = 31
        Case 6
            CalculaUltimoDiaMes = 30
        Case 7
            CalculaUltimoDiaMes = 31
        Case 8
            CalculaUltimoDiaMes = 31
        Case 9
            CalculaUltimoDiaMes = 30
        Case 10
            CalculaUltimoDiaMes = 31
        Case 11
            CalculaUltimoDiaMes = 30
        Case 12
            CalculaUltimoDiaMes = 31
    End Select
    CalculaUltimoDiaMes = CalculaUltimoDiaMes & "-" & mes & "-" & ano
End Function

Private Sub DesenhaGrafico()
    Dim tamRowLabels As Integer
    Dim tamLegendas As Integer
    Dim i As Integer
    Dim j As Integer
    
    ' ------------------------------
    ' INICIALIZA GRAFICO
    Grafico.Visible = True
    
    tamRowLabels = UBound(rowLabels)
    Grafico.ColumnCount = tamRowLabels
    For i = 1 To tamRowLabels
        Grafico.Column = i
        Grafico.ColumnLabel = rowLabels(i)
    Next
    
    tamLegendas = UBound(legendas)
    Grafico.RowCount = tamLegendas
    For i = 1 To tamLegendas
        Grafico.row = i
        Grafico.RowLabel = legendas(i)
    Next
    ' ------------------------------
    ' ------------------------------
    
    
    ' ------------------------------
    ' PREENCHE O GRAFICO
    For i = 1 To tamLegendas
        For j = 1 To tamRowLabels
            Grafico.row = i
            Grafico.Column = j
            Grafico.data = DevolveDados(legendas(i), rowLabels(j))
        Next
    Next
    ' ------------------------------
    ' ------------------------------
    
End Sub

Private Function DevolveDados(Antibiotico As String, Label As String) As Integer
    Dim i As Integer
    For i = 1 To tamEstrutDados
        If estrutDados(1).descr_antibio = Antibiotico And estrutDados(i).Label = Label Then
            DevolveDados = BL_HandleNull(estrutDados(i).total, 0)
            Exit Function
        End If
    Next
    DevolveDados = 0
End Function

Private Function DevolveCodMicro(seq As Long) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    sql = " SELECT cod_microrg FROM sl_microrg WHERE seq_microrg = " & seq
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount = 1 Then
        DevolveCodMicro = BL_HandleNull(rs!cod_microrg)
    End If
    rs.Close
    Set rs = Nothing

End Function

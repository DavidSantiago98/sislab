VERSION 5.00
Begin VB.Form FormImprimeCodificacoes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listagem de Codifica��es"
   ClientHeight    =   810
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4935
   Icon            =   "FormImprimeCodificacoes.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   810
   ScaleWidth      =   4935
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbReport 
      Height          =   315
      Left            =   960
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   240
      Width           =   3855
   End
   Begin VB.Label Label1 
      Caption         =   "Listagem"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   855
   End
End
Attribute VB_Name = "FormImprimeCodificacoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2002
' T�cnico Paulo Costa

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Private Type Tipo_Codificacoes
    Titulo As String
    NomeTabela As String
    CampoCodigo As String
    CampoDescricao As String
End Type

Dim ListaCodificacoes() As Tipo_Codificacoes

Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub

Private Sub CarregaEstrutura()
    
    ReDim ListaCodificacoes(1 To 43) As Tipo_Codificacoes
    
    ListaCodificacoes(1).NomeTabela = "sl_gr_ana"
    ListaCodificacoes(1).Titulo = "GRUPOS DE AN�LISE"
    ListaCodificacoes(1).CampoCodigo = "cod_gr_ana"
    ListaCodificacoes(1).CampoDescricao = "descr_gr_ana"
    
    ListaCodificacoes(2).NomeTabela = "sl_sgr_ana"
    ListaCodificacoes(2).Titulo = "SUBGRUPOS DE AN�LISE"
    ListaCodificacoes(2).CampoCodigo = "cod_sgr_ana"
    ListaCodificacoes(2).CampoDescricao = "descr_sgr_ana"
    
    ListaCodificacoes(3).NomeTabela = "sl_perfis"
    ListaCodificacoes(3).Titulo = "AN�LISES DO TIPO PERFIL"
    ListaCodificacoes(3).CampoCodigo = "cod_perfis"
    ListaCodificacoes(3).CampoDescricao = "descr_perfis"
    
    ListaCodificacoes(4).NomeTabela = "sl_t_colh"
    ListaCodificacoes(4).Titulo = "TIPOS DE COLHEITA"
    ListaCodificacoes(4).CampoCodigo = "cod_t_colh"
    ListaCodificacoes(4).CampoDescricao = "descr_t_colh"
    
    
    ListaCodificacoes(6).NomeTabela = "sl_auto_res"
    ListaCodificacoes(6).Titulo = "AUTOMATIZA��O DE RESULTADOS"
    ListaCodificacoes(6).CampoCodigo = "cod_auto_res"
    ListaCodificacoes(6).CampoDescricao = "descr_auto_res"
    
    ListaCodificacoes(7).NomeTabela = "sl_val_auto"
    ListaCodificacoes(7).Titulo = "AUXILIAR DE VALIDA��O"
    ListaCodificacoes(7).CampoCodigo = "cod_val_auto"
    ListaCodificacoes(7).CampoDescricao = "descr_val_auto"
    
    ListaCodificacoes(8).NomeTabela = "sl_varia_dc"
    ListaCodificacoes(8).Titulo = "DELTA-CHECK"
    ListaCodificacoes(8).CampoCodigo = "cod_varia_dc"
    ListaCodificacoes(8).CampoDescricao = "descr_varia_dc"
    
    ListaCodificacoes(9).NomeTabela = "sl_tubo"
    ListaCodificacoes(9).Titulo = "TIPOS DE TUBO"
    ListaCodificacoes(9).CampoCodigo = "cod_tubo"
    ListaCodificacoes(9).CampoDescricao = "descr_tubo"
    
    ListaCodificacoes(10).NomeTabela = "sl_produto"
    ListaCodificacoes(10).Titulo = "PRODUTOS"
    ListaCodificacoes(10).CampoCodigo = "cod_produto"
    ListaCodificacoes(10).CampoDescricao = "descr_produto"
    
    ListaCodificacoes(11).NomeTabela = "sl_reagent"
    ListaCodificacoes(11).Titulo = "REAGENTES"
    ListaCodificacoes(11).CampoCodigo = "cod_reagent"
    ListaCodificacoes(11).CampoDescricao = "descr_reagent"
    
    ListaCodificacoes(12).NomeTabela = "sl_metodo"
    ListaCodificacoes(12).Titulo = "M�TODOS"
    ListaCodificacoes(12).CampoCodigo = "cod_metodo"
    ListaCodificacoes(12).CampoDescricao = "descr_metodo"
    
    ListaCodificacoes(13).NomeTabela = "sl_diag"
    ListaCodificacoes(13).Titulo = "DIAGN�STICOS"
    ListaCodificacoes(13).CampoCodigo = "cod_diag"
    ListaCodificacoes(13).CampoDescricao = "descr_diag"
    
    ListaCodificacoes(14).NomeTabela = "sl_gr_diag"
    ListaCodificacoes(14).Titulo = "GRUPOS DE DIAGN�STICOS"
    ListaCodificacoes(14).CampoCodigo = "cod_gr_diag"
    ListaCodificacoes(14).CampoDescricao = "descr_gr_diag"
    
    ListaCodificacoes(15).NomeTabela = "sl_tera_med"
    ListaCodificacoes(15).Titulo = "TERAP�UTICAS E MEDICA��O"
    ListaCodificacoes(15).CampoCodigo = "cod_tera_med"
    ListaCodificacoes(15).CampoDescricao = "descr_tera_med"
    
    ListaCodificacoes(16).NomeTabela = "sl_or_zonas"
    ListaCodificacoes(16).Titulo = "OUTRAS RAZ�ES (ZONAS DE REFER�NCIA)"
    ListaCodificacoes(16).CampoCodigo = "cod_or_zonas"
    ListaCodificacoes(16).CampoDescricao = "descr_or_zonas"
    
    ListaCodificacoes(17).NomeTabela = "sl_dicionario"
    ListaCodificacoes(17).Titulo = "DICION�RIO DE FRASES"
    ListaCodificacoes(17).CampoCodigo = "cod_frase"
    ListaCodificacoes(17).CampoDescricao = "descr_frase"
    
    ListaCodificacoes(18).NomeTabela = "sl_microrg"
    ListaCodificacoes(18).Titulo = "MICRORGANISMOS"
    ListaCodificacoes(18).CampoCodigo = "cod_microrg"
    ListaCodificacoes(18).CampoDescricao = "descr_microrg"
    
    ListaCodificacoes(19).NomeTabela = "sl_gr_microrg"
    ListaCodificacoes(19).Titulo = "GRUPOS DE MICRORGANISMOS"
    ListaCodificacoes(19).CampoCodigo = "cod_gr_microrg"
    ListaCodificacoes(19).CampoDescricao = "descr_gr_microrg"
    
    ListaCodificacoes(20).NomeTabela = "sl_antibio"
    ListaCodificacoes(20).Titulo = "ANTIBI�TICOS"
    ListaCodificacoes(20).CampoCodigo = "cod_antibio"
    ListaCodificacoes(20).CampoDescricao = "descr_antibio"
    
    ListaCodificacoes(21).NomeTabela = "sl_conc_ant"
    ListaCodificacoes(21).Titulo = "CONCENTRA��O DE ANTIBI�TICOS"
    ListaCodificacoes(21).CampoCodigo = "cod_antibio"
    ListaCodificacoes(21).CampoDescricao = "concentracao"
    
    ListaCodificacoes(22).NomeTabela = "sl_gr_antibio"
    ListaCodificacoes(22).Titulo = "GRUPOS DE ANTIBI�TICOS"
    ListaCodificacoes(22).CampoCodigo = "cod_gr_antibio"
    ListaCodificacoes(22).CampoDescricao = "descr_gr_antibio"
    
    ListaCodificacoes(23).NomeTabela = "sl_proven"
    ListaCodificacoes(23).Titulo = "PROVENI�NCIAS"
    ListaCodificacoes(23).CampoCodigo = "cod_proven"
    ListaCodificacoes(23).CampoDescricao = "descr_proven"
    
    ListaCodificacoes(24).NomeTabela = "sl_gr_trab"
    ListaCodificacoes(24).Titulo = "GRUPOS DE TRABALHO"
    ListaCodificacoes(24).CampoCodigo = "cod_gr_trab"
    ListaCodificacoes(24).CampoDescricao = "descr_gr_trab"
    
    ListaCodificacoes(25).NomeTabela = "sl_modelo"
    ListaCodificacoes(25).Titulo = "MODELOS LIVRES"
    ListaCodificacoes(25).CampoCodigo = "cod_modelo"
    ListaCodificacoes(25).CampoDescricao = "descr_modelo"
    
    ListaCodificacoes(26).NomeTabela = "sl_t_canc"
    ListaCodificacoes(26).Titulo = "TIPOS DE CANCELAMENTO"
    ListaCodificacoes(26).CampoCodigo = "cod_t_canc"
    ListaCodificacoes(26).CampoDescricao = "descr_canc"
    
    ListaCodificacoes(27).NomeTabela = "sl_pais"
    ListaCodificacoes(27).Titulo = "PA�SES"
    ListaCodificacoes(27).CampoCodigo = "cod_pais"
    ListaCodificacoes(27).CampoDescricao = "descr_pais"
    
    ListaCodificacoes(28).NomeTabela = "sl_cod_postal"
    ListaCodificacoes(28).Titulo = "C�DIGOS POSTAIS"
    ListaCodificacoes(28).CampoCodigo = "cod_postal"
    ListaCodificacoes(28).CampoDescricao = "descr_postal"
    
    ListaCodificacoes(29).NomeTabela = "sl_nacional"
    ListaCodificacoes(29).Titulo = "NACIONALIDADES"
    ListaCodificacoes(29).CampoCodigo = "cod_nacional"
    ListaCodificacoes(29).CampoDescricao = "descr_nacional"
    
    ListaCodificacoes(30).NomeTabela = "sl_profis"
    ListaCodificacoes(30).Titulo = "PROFISS�ES"
    ListaCodificacoes(30).CampoCodigo = "cod_profis"
    ListaCodificacoes(30).CampoDescricao = "descr_profis"
    
    ListaCodificacoes(31).NomeTabela = "sl_isencao"
    ListaCodificacoes(31).Titulo = "ISEN��O DOS UTENTES"
    ListaCodificacoes(31).CampoCodigo = "cod_isencao"
    ListaCodificacoes(31).CampoDescricao = "descr_isencao"
    
    ListaCodificacoes(32).NomeTabela = "sl_medicos"
    ListaCodificacoes(32).Titulo = "M�DICOS"
    ListaCodificacoes(32).CampoCodigo = "cod_med"
    ListaCodificacoes(32).CampoDescricao = "nome_med"
    
    ListaCodificacoes(33).NomeTabela = "sl_t_anul"
    ListaCodificacoes(33).Titulo = "TIPOS DE ANULA��O DE RECIBOS"
    ListaCodificacoes(33).CampoCodigo = "cod_t_anul"
    ListaCodificacoes(33).CampoDescricao = "descr_t_anul"
    
    ListaCodificacoes(34).NomeTabela = "sl_efr"
    ListaCodificacoes(34).Titulo = "ENTIDADES FINANCEIRAS"
    ListaCodificacoes(34).CampoCodigo = "cod_efr"
    ListaCodificacoes(34).CampoDescricao = "descr_efr"
    
    ListaCodificacoes(35).NomeTabela = "sl_portarias"
    ListaCodificacoes(35).Titulo = "PRE��RIOS"
    ListaCodificacoes(35).CampoCodigo = "portaria"
    ListaCodificacoes(35).CampoDescricao = "descr_port"
    
    ListaCodificacoes(36).NomeTabela = "sl_tip_rubricas"
    ListaCodificacoes(36).Titulo = "TIPOS DE RUBRICAS"
    ListaCodificacoes(36).CampoCodigo = "cod_tip_rubricas"
    ListaCodificacoes(36).CampoDescricao = "descr_tip_rubricas"
    
    ListaCodificacoes(37).NomeTabela = "sl_grup_rubr"
    ListaCodificacoes(37).Titulo = "GRUPOS DE RUBRICAS"
    ListaCodificacoes(37).CampoCodigo = "cod_grupo"
    ListaCodificacoes(37).CampoDescricao = "descr_grupo"
    
    ListaCodificacoes(38).NomeTabela = "sl_rubr"
    ListaCodificacoes(38).Titulo = "RUBRICAS"
    ListaCodificacoes(38).CampoCodigo = "cod_rubr"
    ListaCodificacoes(38).CampoDescricao = "descr_rubr"
    
    ListaCodificacoes(39).NomeTabela = "sl_ana_s"
    ListaCodificacoes(39).Titulo = "AN�LISES SIMPLES"
    ListaCodificacoes(39).CampoCodigo = "cod_ana_s"
    ListaCodificacoes(39).CampoDescricao = "descr_ana_s"
    
    ListaCodificacoes(40).NomeTabela = "sl_ana_c"
    ListaCodificacoes(40).Titulo = "AN�LISES COMPLEXAS"
    ListaCodificacoes(40).CampoCodigo = "cod_ana_c"
    ListaCodificacoes(40).CampoDescricao = "descr_ana_c"
    
    
    ListaCodificacoes(41).NomeTabela = "sl_gr_trab x1, sl_ana_trab x2, slv_analises x3"
    ListaCodificacoes(41).Titulo = "GRUPOS DE TRABALHO - AN�LISES"
    ListaCodificacoes(41).CampoCodigo = "x1.descr_gr_trab"
    ListaCodificacoes(41).CampoDescricao = "x3.descr_ana"
    
    ListaCodificacoes(42).NomeTabela = "sl_informacao x1, sl_ana_informacao x2, slv_analises x3"
    ListaCodificacoes(42).Titulo = "INFORMA��O ASSOCIADA A AN�LISES"
    ListaCodificacoes(42).CampoCodigo = "x1.descr_informacao"
    ListaCodificacoes(42).CampoDescricao = "x3.descr_ana"
    
    ListaCodificacoes(43).NomeTabela = "SLV_ANALISES_APENAS x1"
    ListaCodificacoes(43).Titulo = "AN�LISES PARA FACTURAR SEM MAPEAMENTO"
    ListaCodificacoes(43).CampoCodigo = "x1.cod_ana"
    ListaCodificacoes(43).CampoDescricao = "x1.descr_ana"
    
    'DESACTIVADAS
    'ListaCodificacoes(41).NomeTabela = "sl_freguesi"
    'ListaCodificacoes(41).Titulo = "FREGUESIAS"
    'ListaCodificacoes(41).CampoCodigo = "cod_freguesi"
    'ListaCodificacoes(41).CampoDescricao = "descr_freguesi"
    
    'ListaCodificacoes(42).NomeTabela = "sl_centro_saude"
    'ListaCodificacoes(42).Titulo = "CENTROS DE SA�DE"
    'ListaCodificacoes(42).CampoCodigo = "cod_centro_saude"
    'ListaCodificacoes(42).CampoDescricao = "descr_centro_saude"
    
End Sub

Private Sub CarregaCombo()
    
    Dim i As Integer
    
    For i = 1 To UBound(ListaCodificacoes)
        CbReport.AddItem ListaCodificacoes(i).Titulo
        CbReport.ItemData(CbReport.NewIndex) = i
    Next i
    
End Sub

Public Sub FuncaoImprimir()
        
    Dim Titulo As String
    
    Dim indice As Integer
    Dim NomeTabela As String
    Dim CampoCodigo As String
    Dim CampoDescricao As String
    Dim cWhere As String
    Dim sql As String
    Dim total As Integer
    
    Dim continua As Boolean
    
    Dim Erro As String
    Dim i As Integer
    
    sql = "DELETE FROM SL_CR_LCOD WHERE num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    'Verifica qual a Tabela a imprimir
    indice = CbReport.ItemData(CbReport.ListIndex)
    
    NomeTabela = ListaCodificacoes(indice).NomeTabela
    CampoCodigo = ListaCodificacoes(indice).CampoCodigo
    CampoDescricao = ListaCodificacoes(indice).CampoDescricao
    If indice = 41 Then
        cWhere = " WHERE x1.cod_gr_trab = x2.cod_gr_trab AND x2.cod_analise = x3.cod_ana "
    ElseIf indice = 42 Then
        cWhere = " WHERE x1.cod_informacao = x2.cod_informacao AND x2.cod_ana = x3.cod_ana "
    ElseIf indice = 43 Then
        cWhere = " WHERE x1.flg_facturar = 1 and cod_ana not in (select cod_ana from sl_ana_facturacao)"
    Else
        cWhere = ""
    End If
    Titulo = ListaCodificacoes(indice).Titulo
    
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Mapa de Codifica��es") = True Then Exit Sub
    
    '2.Verifica os campos nulos
    If CbReport.ListIndex = -1 Then
        BG_Mensagem mediMsgBox, "Indique o nome da codifica��o a imprimir!", vbOKOnly + vbExclamation, App.ProductName
        CbReport.SetFocus
        Exit Sub
    End If
     
    '3.Visualiza a Common Dialog para a Impress�o (no caso da listagem simples)
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("MapaCodificacoes", "Mapa de Codifica��es", crptToPrinter)
    Else
        continua = BL_IniciaReport("MapaCodificacoes", "Mapa de Codifica��es", crptToWindow, True)
    End If
    If continua = False Then Exit Sub
     
    '4.Verifica se foram inseridos registos para o Report
    sql = "INSERT INTO SL_CR_LCOD ( nome_computador, num_sessao, codigo, descricao) "
    sql = sql & " SELECT " & BL_TrataStringParaBD(gComputador) & "," & gNumeroSessao & "," & CampoCodigo & "," & CampoDescricao
    sql = sql & " FROM " & NomeTabela
    sql = sql & cWhere
    total = BG_ExecutaQuery_ADO(sql)
    
    If total <= 0 Then
        Call BG_Mensagem(mediMsgBox, "N�o foram encontrados nenhuns registos para imprimir!", vbOKOnly + vbExclamation)
        Exit Sub
    End If
   
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.Formulas(0) = "Titulo=" & BL_TrataStringParaBD(Titulo)
    Report.SQLQuery = "SELECT SL_CR_LCOD.CODIGO,SL_CR_LCOD.DESCRICAO " & _
                      "FROM SL_CR_LCOD  WHERE num_sessao = " & gNumeroSessao & _
                      "ORDER BY SL_CR_LCOD.DESCRICAO"
    Call BL_ExecutaReport
    Report.PageShow Report.ReportStartPage
    
    sql = "DELETE FROM SL_CR_LCOD WHERE num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
        
    Call BL_FimProcessamento(gFormActivo, "")
       
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub EventoUnload()
    
    Call BL_FechaPreview("Mapa de Codifica��es")
    
    BL_ToolbarEstadoN 0
    
    Set gFormActivo = MDIFormInicio
    Set FormImprimeCodificacoes = Nothing
    
End Sub

Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Me.Width = 5025
    Me.Height = 1185
    Me.caption = "Listagens Gen�ricas"
    Estado = 0
    Call CarregaEstrutura
    Call CarregaCombo
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
End Sub

Private Sub EventoActivate()
        
    Set gFormActivo = Me
    
    If CbReport.Enabled = True Then
        CbReport.SetFocus
    End If

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    
    MDIFormInicio.MousePointer = vbArrow
        
End Sub

Public Sub FuncaoLimpar()
    
    CbReport.ListIndex = -1
    
End Sub


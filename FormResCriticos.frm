VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormResCriticos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormResCriticos"
   ClientHeight    =   10200
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10890
   Icon            =   "FormResCriticos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10200
   ScaleWidth      =   10890
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   3120
      ScaleHeight     =   315
      ScaleWidth      =   555
      TabIndex        =   6
      Top             =   7800
      Width           =   615
   End
   Begin VB.Timer Timer1 
      Interval        =   30000
      Left            =   1080
      Top             =   6120
   End
   Begin VB.Frame Frame 
      BorderStyle     =   0  'None
      Height          =   5535
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   10695
      Begin VB.TextBox EcObservacao 
         Appearance      =   0  'Flat
         Height          =   855
         Left            =   0
         TabIndex        =   3
         Top             =   3720
         Width           =   10575
      End
      Begin VB.CheckBox CkTratado 
         Caption         =   "Resolvido"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   4680
         Width           =   1215
      End
      Begin VB.CommandButton BtValidar 
         Height          =   615
         Left            =   9720
         Picture         =   "FormResCriticos.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Validar"
         Top             =   4680
         Width           =   735
      End
      Begin MSComctlLib.ListView LvResCriticos 
         Height          =   3015
         Left            =   0
         TabIndex        =   4
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   5318
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.Label Label1 
         Caption         =   "Observa��o"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   3480
         Width           =   975
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   3360
      Top             =   4920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormResCriticos.frx":0CD6
            Key             =   "KO"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormResCriticos.frx":1070
            Key             =   "OK"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormResCriticos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer
Dim CampoDeFocus As Object

' CORES UTILIZADAS
Const vermelho = &H8080FF
Const Amarelo = &H80FFFF
Const Branco = &HFFFFFF
Const laranja = &H80C0FF

Const cinzento = &HC0C0C0              '&HE0E0E0
Const Verde = &HC0FFC0
Const azul = &HFEA381                          '&HFF8080

Const AzulClaro = &HFF8080
Private Type resCrit
    n_req As String
    cod_ana As String
    descr_ana As String
    dt_resultado As String
    hr_resultado As String
    resultado As String
    flg_tratado As Integer
    user_tratado As String
    dt_tratado As String
    hr_tratado As String
    user_cri As String
    dt_cri As String
    hr_cri As String
    seq_res_critico As Long
    observacao As String
    
End Type
Dim estrutResCrit() As resCrit
Dim totalResCrit As Integer
Dim indice As Long

Private Sub BtValidar_Click()
    Dim iReg As Integer
    Dim sSql As String
    On Error GoTo TrataErro
    
    If indice > mediComboValorNull And indice <= totalResCrit Then
        sSql = "UPDATE sl_res_critico SET flg_tratado = 1, user_tratado = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", dt_tratado = " & BL_TrataDataParaBD(Bg_DaData_ADO)
        sSql = sSql & ", hr_tratado =" & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", observacao = " & BL_TrataStringParaBD(EcObservacao) & " WHERE seq_res_critico = " & estrutResCrit(indice).seq_res_critico
         iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
    End If
    FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtValidar_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtValidar_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    FuncaoProcurar
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormResCriticos = Nothing

End Sub

Sub DefTipoCampos()
    With LvResCriticos
        .SmallIcons = ImageList2
        .ColumnHeaderIcons = ImageList2
        .ColumnHeaders.Add , "ICON", "", 300, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "REQUISICAO", "Requisi��o", 1200, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "DATA", "Data", 1700, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "ANALISE", "An�lise", 4500, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "RESULTADO", "Resultado", .Width - (300 + 1200 + 1700 + 4500), ListColumnAlignmentConstants.lvwColumnLeft
        .View = lvwReport
        .BorderStyle = ccNone
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = False
        .ForeColor = &H808080
        .GridLines = False
    End With
   ' SetListViewColor LvResCriticos, Picture1, vbWhite, &H8000000F
End Sub

Sub PreencheValoresDefeito()
    LimpaCampos
    LimpaEstruturas
    
    
End Sub

Sub Inicializacoes()
         
    Me.caption = "Resultados Criticos"
    Me.Width = 10980
    Me.Height = 5865 ' Normal
    Me.left = 50
    Me.top = 50
    Set CampoDeFocus = LvResCriticos
    
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
End Sub

Sub LimpaCampos()
    BtValidar.Enabled = False
    CkTratado.Enabled = False
    EcObservacao.Enabled = False
    
    indice = mediComboValorNull
    LvResCriticos.ListItems.Clear
    EcObservacao = ""
    CkTratado.value = vbUnchecked
End Sub



Private Sub LimpaEstruturas()
    totalResCrit = 0
    ReDim estrutResCrit(0)

End Sub

Sub FuncaoLimpar()
    LimpaCampos
    LimpaEstruturas
    
End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsCri As New ADODB.recordset
    Dim Icon As Integer
    On Error GoTo TrataErro
    Frame.Enabled = False
    FuncaoLimpar
    sSql = "SELECT x1.n_req, x2.descr_ana,x2.cod_ana, x3.dt_cri dt_result, x3.hr_Cri hr_result, x4.result,x6.flg_tratado, x6.user_tratado, x6.user_tratado, x6.dt_tratado, x6.hr_tratado,"
    sSql = sSql & " x6.user_cri, x6.dt_cri, x6.hr_cri,x6.seq_res_critico,x6.observacao FROM sl_requis x1, slv_analises_apenas x2, sl_realiza x3, sl_res_alfan x4, sl_res_critico x6"
    sSql = sSql & " WHERE x1.n_req = x3.n_Req AND x3.seq_realiza = x4.seq_realiza AND x3.seq_realiza = x6.seq_realiza AND x3.cod_ana_S = x2.cod_ana "
    sSql = sSql & " AND x6.dt_cri >= trunc(sysdate)-1 AND flg_tratado = 0  "
    sSql = sSql & " ORDER by flg_tratado ASC , dt_cri ASC, hr_cri ASC "
    rsCri.CursorLocation = adUseServer
    rsCri.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsCri.Open sSql, gConexao
    If rsCri.RecordCount > 0 Then
        While Not rsCri.EOF
            totalResCrit = totalResCrit + 1
            ReDim Preserve estrutResCrit(totalResCrit)
            estrutResCrit(totalResCrit).cod_ana = BL_HandleNull(rsCri!cod_ana, "")
            estrutResCrit(totalResCrit).descr_ana = BL_HandleNull(rsCri!descr_ana, "")
            estrutResCrit(totalResCrit).dt_cri = BL_HandleNull(rsCri!dt_cri, "")
            estrutResCrit(totalResCrit).dt_resultado = BL_HandleNull(rsCri!dt_result, "")
            estrutResCrit(totalResCrit).dt_tratado = BL_HandleNull(rsCri!dt_tratado, "")
            estrutResCrit(totalResCrit).flg_tratado = BL_HandleNull(rsCri!flg_tratado, "")
            estrutResCrit(totalResCrit).hr_cri = BL_HandleNull(rsCri!hr_cri, "")
            estrutResCrit(totalResCrit).hr_resultado = BL_HandleNull(rsCri!hr_result, "")
            estrutResCrit(totalResCrit).hr_tratado = BL_HandleNull(rsCri!hr_tratado, "")
            estrutResCrit(totalResCrit).n_req = BL_HandleNull(rsCri!n_req, "")
            estrutResCrit(totalResCrit).resultado = BL_HandleNull(rsCri!result, "")
            estrutResCrit(totalResCrit).seq_res_critico = BL_HandleNull(rsCri!seq_res_critico, 0)
            estrutResCrit(totalResCrit).user_cri = BL_HandleNull(rsCri!user_cri, "")
            estrutResCrit(totalResCrit).user_tratado = BL_HandleNull(rsCri!user_tratado, "")
            estrutResCrit(totalResCrit).observacao = BL_HandleNull(rsCri!observacao, "")
            If estrutResCrit(totalResCrit).flg_tratado = mediSim Then
                Icon = 2
            Else
                Icon = 1
            End If
            With LvResCriticos.ListItems.Add(, "K" & estrutResCrit(totalResCrit).seq_res_critico, "", , Icon)
                .ListSubItems.Add , , estrutResCrit(totalResCrit).n_req
                .ListSubItems.Add , , estrutResCrit(totalResCrit).dt_cri & " " & estrutResCrit(totalResCrit).hr_cri
                .ListSubItems.Add , , estrutResCrit(totalResCrit).descr_ana
                .ListSubItems.Add , , estrutResCrit(totalResCrit).resultado
            End With
            
            rsCri.MoveNext
        Wend
    End If
    rsCri.Close
    Set rsCri = Nothing
    Frame.Enabled = True
    LvResCriticos_Click
Exit Sub
TrataErro:
    Frame.Enabled = True
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar"
    Exit Sub
    Resume Next
End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Private Sub LvResCriticos_Click()
    indice = mediComboValorNull
    CkTratado.value = vbUnchecked
    EcObservacao = ""
    BtValidar.Enabled = False
    CkTratado.Enabled = False
    EcObservacao.Enabled = False
    If LvResCriticos.ListItems.Count > 0 Then
        If LvResCriticos.SelectedItem.Index <= totalResCrit Then
            indice = LvResCriticos.SelectedItem.Index
            EcObservacao = estrutResCrit(indice).observacao
            If estrutResCrit(indice).flg_tratado = mediSim Then
                CkTratado.value = vbChecked
            Else
                CkTratado.value = vbUnchecked
                BtValidar.Enabled = True
                EcObservacao.Enabled = True
                CkTratado.Enabled = True
            End If
        End If
    End If
    
End Sub

Private Sub timer1_Timer()
    FuncaoProcurar
End Sub
''''''''''''''''''''''''''''''''''''''''''''
' Define propriedades das linhas da lista ''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub SetListViewColor(pCtrlListView As ListView, pCtrlPictureBox As PictureBox, Color1 As Long, Color2 As Long)

    Dim iLineHeight As Long
    Dim iBarHeight  As Long
    Dim lBarWidth   As Long
    Dim lColor1     As Long
    Dim lColor2     As Long
 
    lColor1 = Color1
    lColor2 = Color2
    pCtrlListView.Picture = LoadPicture("")
    pCtrlListView.Refresh
    pCtrlPictureBox.Cls
    pCtrlPictureBox.AutoRedraw = True
    pCtrlPictureBox.BorderStyle = vbBSNone
    pCtrlPictureBox.ScaleMode = vbTwips
    pCtrlPictureBox.Visible = False
    pCtrlListView.PictureAlignment = lvwTile
    pCtrlPictureBox.Font = pCtrlListView.Font
    pCtrlPictureBox.top = pCtrlListView.top
    pCtrlPictureBox.Font = pCtrlListView.Font
    With pCtrlPictureBox.Font
        .Size = pCtrlListView.Font.Size + 2
        .Bold = pCtrlListView.Font.Bold
        .Charset = pCtrlListView.Font.Charset
        .Italic = pCtrlListView.Font.Italic
        .Name = pCtrlListView.Font.Name
        .Strikethrough = pCtrlListView.Font.Strikethrough
        .Underline = pCtrlListView.Font.Underline
        .Weight = pCtrlListView.Font.Weight
    End With
    pCtrlPictureBox.Refresh
    iLineHeight = pCtrlPictureBox.TextHeight("W") + Screen.TwipsPerPixelY
    iBarHeight = (iLineHeight * 1)
    lBarWidth = pCtrlListView.Width
    pCtrlPictureBox.Height = iBarHeight * 2
    pCtrlPictureBox.Width = lBarWidth
    pCtrlPictureBox.Line (0, 0)-(lBarWidth, iBarHeight), lColor1, BF
    pCtrlPictureBox.Line (0, iBarHeight)-(lBarWidth, iBarHeight * 2), lColor2, BF
    pCtrlPictureBox.AutoSize = True
    pCtrlListView.Picture = pCtrlPictureBox.Image
    pCtrlListView.Refresh
End Sub



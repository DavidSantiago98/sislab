VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormPrinter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Imprimir"
   ClientHeight    =   3615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6840
   Icon            =   "FormPrinter.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3615
   ScaleWidth      =   6840
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdOk 
      Caption         =   "OK"
      Height          =   400
      Left            =   4440
      TabIndex        =   0
      Top             =   3120
      Width           =   1095
   End
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "Cancelar"
      Height          =   400
      Left            =   5640
      TabIndex        =   9
      Top             =   3120
      Width           =   1095
   End
   Begin VB.Frame FrameImpressora 
      Caption         =   " Impressora "
      Height          =   1095
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   6615
      Begin VB.ComboBox CmbPrinters 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   4935
      End
      Begin VB.Label lbltitnome 
         Caption         =   "&Nome:"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lbltitOnde 
         Caption         =   "Onde:"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   720
         Width           =   495
      End
      Begin VB.Label lblOnde 
         Caption         =   "ONDE"
         Height          =   255
         Left            =   1080
         TabIndex        =   15
         Top             =   720
         Width           =   5175
      End
   End
   Begin VB.Frame FrameIntervalo 
      Caption         =   " Intervalo de Impress�o "
      Height          =   1575
      Left            =   120
      TabIndex        =   11
      Top             =   1320
      Width           =   3735
      Begin VB.OptionButton optTudo 
         Caption         =   "&Tudo"
         CausesValidation=   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   735
      End
      Begin VB.OptionButton optPaginas 
         Caption         =   "&P�ginas"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtate 
         Height          =   315
         Left            =   3000
         MaxLength       =   3
         TabIndex        =   5
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox txtde 
         Height          =   315
         Left            =   1680
         MaxLength       =   3
         TabIndex        =   4
         Top             =   960
         Width           =   615
      End
      Begin VB.Label lblde 
         Caption         =   "&de:"
         Height          =   255
         Left            =   1320
         TabIndex        =   13
         Top             =   960
         Width           =   255
      End
      Begin VB.Label lblate 
         Caption         =   "&at�:"
         Height          =   255
         Left            =   2640
         TabIndex        =   12
         Top             =   960
         Width           =   255
      End
   End
   Begin VB.Frame FrameCopias 
      Caption         =   " C�pias "
      Height          =   1575
      Left            =   3960
      TabIndex        =   8
      Top             =   1320
      Width           =   2775
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   240
         Left            =   2270
         TabIndex        =   18
         Top             =   385
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   423
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtCopias"
         BuddyDispid     =   196624
         OrigLeft        =   2270
         OrigTop         =   385
         OrigRight       =   2510
         OrigBottom      =   625
         Max             =   9999
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.TextBox txtCopias 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   6
         Top             =   360
         Width           =   735
      End
      Begin VB.CheckBox ChkAgrupar 
         Caption         =   "A&grupar"
         Height          =   255
         Left            =   1800
         TabIndex        =   7
         Top             =   840
         Width           =   855
      End
      Begin VB.Image ImgCollate2 
         Height          =   615
         Left            =   120
         Picture         =   "FormPrinter.frx":000C
         Stretch         =   -1  'True
         Top             =   720
         Width           =   1410
      End
      Begin VB.Label lbltitnumcopias 
         Caption         =   "N�mero de &c�pias:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   1335
      End
      Begin VB.Image ImgCollate1 
         Appearance      =   0  'Flat
         Height          =   600
         Left            =   120
         Picture         =   "FormPrinter.frx":2DCA
         Stretch         =   -1  'True
         Top             =   720
         Width           =   1605
      End
   End
End
Attribute VB_Name = "FormPrinter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Private Sub ChkAgrupar_Click()
    
    If ChkAgrupar.Value = Checked Then
        ImgCollate1.Visible = False
        ImgCollate2.Visible = True
    Else
        ImgCollate1.Visible = True
        ImgCollate2.Visible = False
    End If
    
End Sub

Private Sub CmbPrinters_Click()
    
    Call ActualizaPropriedades
    
End Sub


Private Sub CmdCancelar_Click()

    FormPrinter.Tag = "SAI"
    'Para poder continuar o c�digo parado pela FormModal
    FormPrinter.Hide
    
End Sub

Private Sub cmdOK_Click()
    
    'Verificar campos vazios
    If optPaginas.Value = True Then
        'De
        If Trim(txtde.Text) = "" Then
            txtde.SetFocus
            Beep
            Exit Sub
        End If
        
        'At�
        If Trim(txtate.Text) = "" Then
            txtate.SetFocus
            Beep
            Exit Sub
        End If
    End If
    
    If CmbPrinters.ListIndex <> -1 Then
        'Actualiza a impressora por defeito=>PRINTER (VB)
        Set Printer = Printers(CmbPrinters.ListIndex)
        
        'Da pr�xima vez o objecto Printer l� os valores no registry
        'e n�o o valor j� instanciado no objecto printer durante a aplica��o
        Printer.TrackDefault = True
    End If
    
    'Inicializa a Estrutura Global com os dados da impressora
    With gImpressoraActiva
        'Collate
        If ChkAgrupar.Value = Checked Then
            .Collate = True
        Else
            .Collate = False
        End If
        'N� de C�pias
        .Copias = Trim(txtCopias.Text)
        'Nome da Impressora
        .Nome = CmbPrinters.Text
        'P�ginas
        If optTudo.Value = True Then
            .PgInicial = 0
            .PgFinal = 0
        Else
            .PgInicial = Trim(txtde.Text)
            .PgFinal = Trim(txtate.Text)
        End If
    End With
    FormPrinter.Tag = "CONTINUA"
    'Para poder continuar o c�digo parado pela FormModal
    FormPrinter.Hide
    
End Sub




Private Sub Form_Load()
    
    Dim i As Integer
    
    'Por defeito a TAG � "SAI"
    FormPrinter.Tag = "SAI"
    
    'Inicializa as imagens do Collate
    ImgCollate1.Visible = True
    ImgCollate2.Visible = False
    
    'P�ginas
    optTudo.Value = True
    
    'C�pias
    txtCopias.Text = 1
            
    'Adiciona os itens da Colec��o Printers dispon�veis
    'Nome da impressora
    CmbPrinters.Clear
    For i = 0 To Printers.Count - 1
        CmbPrinters.AddItem Printers(i).DeviceName
    Next i
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set FormPrinter = Nothing
    
End Sub





Private Sub ActualizaPropriedades()
    
    'Onde
    lblOnde.Caption = Printers(CmbPrinters.ListIndex).Port
'    'Tipo
'    lblTipo.Caption = Printers(CmbPrinters.ListIndex)
    
End Sub




Private Sub optPaginas_Click()
    
    txtde.Enabled = True
    txtate.Enabled = True
    lblde.Enabled = True
    lblate.Enabled = True
    txtde.SetFocus
    
End Sub

Private Sub optTudo_Click()
    
    txtde.Enabled = False
    txtate.Enabled = False
    lblde.Enabled = False
    lblate.Enabled = False
    txtde.Text = ""
    txtate.Text = ""
    
End Sub


Private Sub txtate_KeyPress(KeyAscii As Integer)
    
    If InStr(1, "123456789", Chr(KeyAscii)) = 0 And KeyAscii <> 8 Then
        KeyAscii = 0
    End If
    
End Sub


Private Sub txtate_Validate(Cancel As Boolean)
            
    If Trim(txtde.Text) <> "" And Trim(txtate.Text) <> "" Then
        If CLng(txtate.Text) < CLng(txtde.Text) Then
            Cancel = True
            Beep
        End If
    End If
    
End Sub


Private Sub txtCopias_Validate(Cancel As Boolean)
    
    If Trim(txtCopias.Text) = "" Then
        txtCopias.Text = 1
    End If
    
End Sub


Private Sub txtde_KeyPress(KeyAscii As Integer)
    
    If InStr(1, "123456789", Chr(KeyAscii)) = 0 And KeyAscii <> 8 Then
        KeyAscii = 0
    End If
    
End Sub


Private Sub txtde_Validate(Cancel As Boolean)
        
    If Trim(txtde.Text) <> "" And Trim(txtate.Text) <> "" Then
        If CLng(txtde.Text) > CLng(txtate.Text) Then
            Cancel = True
            Beep
        End If
    End If
    
End Sub



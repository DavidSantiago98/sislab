VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormListarDevolvidosSONHO 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormListarDevolvidosSONHO"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9240
   Icon            =   "FormListarDevolvidosSONHO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6030
   ScaleWidth      =   9240
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   4815
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   8895
      Begin VB.Frame Frame2 
         Caption         =   "Frame2"
         Height          =   615
         Left            =   120
         TabIndex        =   11
         Top             =   0
         Visible         =   0   'False
         Width           =   8655
         Begin VB.TextBox EcErro 
            Height          =   285
            Left            =   5830
            Locked          =   -1  'True
            TabIndex        =   19
            Top             =   165
            Width           =   2400
         End
         Begin VB.TextBox EcDtChega 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2830
            Locked          =   -1  'True
            TabIndex        =   18
            Top             =   165
            Width           =   1000
         End
         Begin VB.TextBox EcProcesso 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1830
            Locked          =   -1  'True
            TabIndex        =   17
            Top             =   165
            Width           =   1000
         End
         Begin VB.TextBox EcEpisodio 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   830
            TabIndex        =   16
            Top             =   165
            Width           =   1000
         End
         Begin VB.TextBox EcServico 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3830
            Locked          =   -1  'True
            TabIndex        =   15
            Top             =   165
            Width           =   1000
         End
         Begin VB.TextBox EcSonho 
            Height          =   285
            Left            =   4830
            TabIndex        =   14
            Top             =   165
            Width           =   1000
         End
         Begin VB.CommandButton BtInsereLinha 
            Height          =   375
            Left            =   8130
            Picture         =   "FormListarDevolvidosSONHO.frx":000C
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   120
            Width           =   615
         End
         Begin VB.ComboBox CbTEpisodio 
            Height          =   315
            ItemData        =   "FormListarDevolvidosSONHO.frx":0316
            Left            =   0
            List            =   "FormListarDevolvidosSONHO.frx":0318
            TabIndex        =   12
            Top             =   165
            Width           =   840
         End
      End
      Begin VB.CommandButton BtImprimir 
         Caption         =   "&Imprimir"
         Height          =   615
         Left            =   3840
         Picture         =   "FormListarDevolvidosSONHO.frx":031A
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   4080
         Width           =   1215
      End
      Begin MSFlexGridLib.MSFlexGrid FgRes 
         Height          =   3255
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   8535
         _ExtentX        =   15055
         _ExtentY        =   5741
         _Version        =   393216
      End
      Begin VB.Label LaRubrica 
         Caption         =   "Label4"
         Height          =   255
         Left            =   4920
         TabIndex        =   10
         Top             =   120
         Visible         =   0   'False
         Width           =   3855
      End
      Begin VB.Label LbTotalRegistos 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   9
         Top             =   3720
         Width           =   2655
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8895
      Begin VB.TextBox EcDtFim 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "MM-dd-yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   3000
         TabIndex        =   2
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcDtIni 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "MM/dd/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "(Data de Chegada da Requisi��o)"
         Height          =   255
         Left            =   4200
         TabIndex        =   5
         Top             =   360
         Width           =   2895
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2730
         TabIndex        =   3
         Top             =   360
         Width           =   255
      End
   End
End
Attribute VB_Name = "FormListarDevolvidosSONHO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim NRegistos As Long
Dim NAnaSMarc As Long
Dim NAnaCMarc As Long
Dim NAnaSReal As Long
Dim NAnaCReal As Long
Dim NAnaSReq As Long
Dim NAnaCReq As Long
Dim NAnaTabela As Long

Public rs As ADODB.recordset
Dim Flg_ExecutaCodigoFgRes As Boolean
Dim LastRow As Integer
Dim LastCol As Integer

Private Type DadosFacturacao
    RowId As String
    t_doente As String
    doente As String
    t_episodio As String
    episodio As String
    dt_ini_acto As String
    cod_serv_req As String
    cod_acto As String
    qtd As String
    flg_estado As String
    dt_cri As String
    Erro As String
End Type
Dim EstruturaFact() As DadosFacturacao
Dim totalFact As Integer

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtImprimir_Click()

    gImprimirDestino = 1
    FuncaoImprimir

End Sub

'Private Sub BtInsereLinha_Click()
'    Dim i As Integer
'    Dim e As Integer
'    Dim retorno As String
'    Dim sql As String
'    Dim cod_modulo As String
'    Dim num_episodio As String
'    Dim num_processo As String
'    Dim cod_especialidade As String
'    Dim cod_analise As String
'    Dim dta_episodio As String
'    Dim dta_analise As String
'
''    If EcTEpisodio.Text <> "CON" And EcTEpisodio.Text <> "URG" And EcTEpisodio.Text <> "INT" And EcTEpisodio.Text <> "HDI" And EcTEpisodio.Text <> "LAB" Then
''        MsgBox vbCrLf & _
''        "Tipo de epis�dio imposs�vel!.", vbExclamation, " Actualizar Tipo Epis�dio"
''        EcTEpisodio.Text = ""
''        Exit Sub
''    End If
'    If CbTEpisodio.text = "" Or EcEpisodio.text = "" Or EcSonho.text = "" Or EcDtChega.text = "" Then
'        MsgBox vbCrLf & _
'        "Tipo de epis�dio, epis�dio, rubrica e data de entrada, obrigat�rios!.", vbExclamation, ""
'        Exit Sub
'    End If
'
'
'    FgRes.TextMatrix(FgRes.row, 0) = CbTEpisodio.text
'    FgRes.TextMatrix(FgRes.row, 1) = EcEpisodio.text
'    FgRes.TextMatrix(FgRes.row, 2) = EcProcesso.text
'    FgRes.TextMatrix(FgRes.row, 3) = EcDtChega.text
'    FgRes.TextMatrix(FgRes.row, 4) = EcServico.text
'    FgRes.TextMatrix(FgRes.row, 5) = EcSonho.text
'    FgRes.TextMatrix(FgRes.row, 6) = EcErro.text
''    If TotalEstruturaFact > 0 Then
''        ActualizaDadosEstrutura FgRes.Row
''    End If
'
'    gMsgTitulo = "Tratamento de Rejeitados"
'    gMsgMsg = "Deseja facturar ap�s correc��o ?"
'    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
'
'    Me.SetFocus
'
'    If gMsgResp = vbYes Then
'        BL_InicioProcessamento Me, "A facturar registo."
'
'        ' retorno indica o que o SONHO deve fazer com o registo.
'        ' I : Inserir no SONHO.
'        ' R : O SONHO j� conhece. Indica que foi realizada.
'        ' A : Indica que n�o foi realizada e que o SONHO a deve apagar.
'        retorno = "I"
'
'
'
'        If (Len(FgRes.TextMatrix(FgRes.row, 0)) > 0) And _
'           (Len(FgRes.TextMatrix(FgRes.row, 1)) > 0) Then
'
'
'            If Not (IsNumeric(FgRes.TextMatrix(FgRes.row, 1))) Then
'                ' N� de epis�dio n�o � um n�mero.
'                BG_Mensagem mediMsgBox, "Nr de epis�dio inv�lido!"
'                Exit Sub
'            End If
'
'            ' Caso 1 : Situa��o (m�dulo) + epis�dio
'
'            cod_modulo = "'" & FgRes.TextMatrix(FgRes.row, 0) & "'"
'            num_episodio = FgRes.TextMatrix(FgRes.row, 1)
'            dta_analise = "'" & Format(Trim(FgRes.TextMatrix(FgRes.row, 3)), gFormatoData) & "'"
'            num_processo = "NULL"
'            cod_especialidade = "NULL"
'            dta_episodio = "NULL"
'            cod_analise = FgRes.TextMatrix(FgRes.row, 5)
'
'        Else
'
'            ' Caso 2 : Situa��o (m�dulo) + processo + data de epis�dio + cod_especialidade.
'
'            cod_modulo = "'" & FgRes.TextMatrix(FgRes.row, 0) & "'"
'            num_processo = FgRes.TextMatrix(FgRes.row, 2)
'            dta_analise = "'" & Format(Trim(FgRes.TextMatrix(FgRes.row, 3)), gFormatoData) & "'"
'            dta_episodio = "'" & Format(Trim(FgRes.TextMatrix(FgRes.row, 3)), gFormatoData) & "'"
'            cod_especialidade = FgRes.TextMatrix(FgRes.row, 4)
'            num_episodio = "NULL"
'            cod_analise = FgRes.TextMatrix(FgRes.row, 5)
'
'        End If
'
'        sql = "INSERT INTO analises_in " & vbCrLf & _
'              "( " & vbCrLf & _
'              "     cod_modulo, " & vbCrLf & _
'              "     num_episodio, " & vbCrLf & _
'              "     num_processo, " & vbCrLf & _
'              "     dta_episodio, " & vbCrLf & _
'              "     dta_analise, " & vbCrLf & _
'              "     cod_especialidade, " & vbCrLf & _
'              "     cod_analise, " & vbCrLf & _
'              "     quantidade, " & vbCrLf & _
'              "     retorno " & vbCrLf & _
'              ") " & vbCrLf & _
'              "VALUES " & vbCrLf & _
'              "( " & vbCrLf & _
'              "     " & cod_modulo & ", " & vbCrLf & _
'              "     " & num_episodio & ", " & vbCrLf & _
'              "     " & num_processo & ", " & vbCrLf & _
'              "     " & dta_episodio & ", " & vbCrLf & _
'              "     " & dta_analise & ", " & vbCrLf & _
'              "     " & cod_especialidade & ", " & vbCrLf & _
'              "     '" & cod_analise & "', " & vbCrLf & _
'              "     1, " & vbCrLf & _
'              "     '" & retorno & "') "
'
'        'gConnHIS.Execute Sql
'        BG_ExecutaQuery_ADO sql
'        BG_Trata_BDErro
'
'        sql = "DELETE FROM analises_in WHERE rowid = " & BL_TrataStringParaBD(EstruturaFact(FgRes.row).RowId)
'        'gConnHIS.Execute Sql
'        BG_ExecutaQuery_ADO sql
'        BG_Trata_BDErro
'
'        BL_FimProcessamento Me
'
'        LimpaFlexGrid
'        CbTEpisodio.ListIndex = mediComboValorNull
'        CbTEpisodio.text = ""
'        EcEpisodio.text = ""
'        EcProcesso.text = ""
'        EcDtChega.text = ""
'        EcServico.text = ""
'        EcSonho.text = ""
'        EcErro.text = ""
'        FuncaoProcurar
'    End If
'
'End Sub
'
Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    If EcDtFim.Text <> "" Then
        Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    End If
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    If EcDtIni.Text <> "" Then
        Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    End If
    
End Sub

Private Sub EcEpisodio_KeyPress(KeyAscii As Integer)
    Dim e As Integer
    If KeyAscii = 13 Then
        'BtInsereLinha_Click
    End If
End Sub

Private Sub EcSonho_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        PreencheDescricaoRubrica EcSonho
        'BtInsereLinha_Click
    End If
End Sub

Private Sub CbTEpisodio_click()
'    Dim e As Integer
'    If KeyAscii = 13 Then
'        BtInsereLinha_Click
'    End If
    If Flg_ExecutaCodigoFgRes = True Then
    If CbTEpisodio.ListIndex <> mediComboValorNull Then
        'BtInsereLinha_Click
    End If
    End If
End Sub

'Private Sub FGRes_Click()
'    Flg_ExecutaCodigoFgRes = False
'    CbTEpisodio.text = FgRes.TextMatrix(FgRes.row, 0)
'    EcEpisodio.text = FgRes.TextMatrix(FgRes.row, 1)
'    EcProcesso.text = FgRes.TextMatrix(FgRes.row, 2)
'    EcDtChega.text = FgRes.TextMatrix(FgRes.row, 3)
'    EcServico.text = FgRes.TextMatrix(FgRes.row, 4)
'    EcSonho.text = FgRes.TextMatrix(FgRes.row, 5)
'    EcErro.text = FgRes.TextMatrix(FgRes.row, 6)
'
'    If FgRes.Col = 0 Then
'        CbTEpisodio.SetFocus
'    ElseIf FgRes.Col = 1 Then
'        EcEpisodio.SetFocus
'    ElseIf FgRes.Col = 2 Then
'        EcProcesso.SetFocus
'    ElseIf FgRes.Col = 3 Then
'        EcDtChega.SetFocus
'    ElseIf FgRes.Col = 4 Then
'        EcServico.SetFocus
'    ElseIf FgRes.Col = 5 Then
'        EcSonho.SetFocus
'    ElseIf FgRes.Col = 6 Then
'        EcErro.SetFocus
'    End If
'
'    If EcSonho.text <> "" Then
'        PreencheDescricaoRubrica EcSonho
'    End If
'
'    Flg_ExecutaCodigoFgRes = True
'
'End Sub
'
'Private Sub FGRes_RowColChange()
'    If Flg_ExecutaCodigoFgRes = True Then
'        Flg_ExecutaCodigoFgRes = False
'
'        MudaCorFundo
'
'        LastRow = FgRes.row
'        LastCol = FgRes.Col
'
'        'FgRes_Click
'
'        Flg_ExecutaCodigoFgRes = True
'    End If
'End Sub
'
Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
      
End Sub

Sub Inicializacoes()

    Me.caption = "Registos devolvidos da factura��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 9330
    Me.Height = 6410 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtIni
    
    LastCol = 0
    LastRow = 1
    
    BtImprimir.Enabled = False
    Flg_ExecutaCodigoFgRes = True
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
        
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Registos devolvidos da factura��o")
    
    Set FormListarDevolvidosSONHO = Nothing
    
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    LimpaFlexGrid
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BtImprimir.Enabled = False
    LbTotalRegistos.caption = ""
    LaRubrica.caption = ""
    
    FgRes.CellBackColor = vbWhite
    Flg_ExecutaCodigoFgRes = False
    FgRes.row = 1
    FgRes.Col = 0
            
    LastRow = 1
    LastCol = 0
'
'    TotalEstruturaFact = 0
'
'    LimpaFlexGridRes
    
    'EcTEpisodio.Text = ""
    EcEpisodio.Text = ""
    EcProcesso.Text = ""
    EcDtChega.Text = ""
    EcServico.Text = ""
    EcSonho.Text = ""
    CbTEpisodio.ListIndex = mediComboValorNull
    EcErro.Text = ""
    
'    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    Flg_ExecutaCodigoFgRes = True
    'CbTipoEpisodio.Clear
    
    CampoDeFocus.SetFocus
    
End Sub

Sub DefTipoCampos()

    'FlexGrid de resultados
    With FgRes
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        
        .row = 0
        .ColWidth(0) = 800
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "T. Epis."

        .ColWidth(1) = 1000
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 1) = "Episodio"

        .ColWidth(2) = 1000
        .Col = 2
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 2) = "Processo"

        .ColWidth(3) = 1000
        .Col = 3
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 3) = "Dt. Chega"

        .ColWidth(4) = 1000
        .Col = 4
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 4) = "Servi�o"

        .ColWidth(5) = 950
        .Col = 5
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 5) = "Acto"
        
        .ColWidth(6) = 2400
        .Col = 6
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 6) = "Erro"
        
        .row = 1
        .Col = 0
        
    End With

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"

    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    
    CbTEpisodio.AddItem "CON"
    CbTEpisodio.AddItem "URG"
    CbTEpisodio.AddItem "INT"
    CbTEpisodio.AddItem "HDI"
    CbTEpisodio.AddItem "LAB"
    CbTEpisodio.AddItem "RAD"
        
End Sub

Sub FuncaoProcurar()
       
    Dim sql As String
    Dim RsDevolvidos As ADODB.recordset
    Dim HisAberto As Integer
    Dim i As Long
    
    On Error GoTo TrataErro
    totalFact = 0
    ReDim EstruturaFact(0)
    
    LimpaFlexGrid
    LbTotalRegistos.caption = ""
    
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
        
    BL_MudaCursorRato mediMP_Espera, Me
    
    sql = "DELETE FROM sl_cr_devolvidos_fact"
    BG_ExecutaQuery_ADO sql
    
    i = 0
    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    'SONHO
    If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
        sql = "SELECT rowid, cod_modulo,num_episodio,num_processo,dta_episodio,dta_analise, " & _
            " cod_especialidade,cod_analise,erro FROM analises_in WHERE erro IS NOT NULL AND dta_analise BETWEEN " & _
            BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text) & _
            " ORDER BY cod_modulo, num_episodio, dta_analise ASC"
        Set RsDevolvidos = New ADODB.recordset
        RsDevolvidos.CursorLocation = adUseServer
        RsDevolvidos.CursorType = adOpenStatic
        'RsDevolvidos.Open Sql, gConexao
        RsDevolvidos.Open sql, gConnHIS
        If RsDevolvidos.RecordCount > 0 Then
            ReDim EstruturaFact(RsDevolvidos.RecordCount + 2)
            While Not RsDevolvidos.EOF
                FgRes.AddItem ""
                i = i + 1
                FgRes.TextMatrix(i, 0) = BL_HandleNull(RsDevolvidos!cod_modulo, "")
                FgRes.TextMatrix(i, 1) = BL_HandleNull(RsDevolvidos!num_episodio, "")
                FgRes.TextMatrix(i, 2) = BL_HandleNull(RsDevolvidos!num_processo, "")
                FgRes.TextMatrix(i, 3) = BL_HandleNull(RsDevolvidos!dta_analise, "")
                FgRes.TextMatrix(i, 4) = BL_HandleNull(RsDevolvidos!cod_especialidade, "")
                FgRes.TextMatrix(i, 5) = BL_HandleNull(RsDevolvidos!cod_analise, "")
                FgRes.TextMatrix(i, 6) = BL_HandleNull(RsDevolvidos!Erro, "")
                EstruturaFact(i).RowId = RsDevolvidos!RowId
                
                sql = "INSERT INTO sl_cr_devolvidos_fact(nome_computador, t_episodio, num_episodio, num_processo, " & _
                    " dt_chega, servico, cod_ana_gh, erro) VALUES ('" & BG_SYS_GetComputerName & "', '" & BL_HandleNull(RsDevolvidos!cod_modulo, "") & "', '" & _
                BL_HandleNull(RsDevolvidos!num_episodio, "") & "', '" & BL_HandleNull(RsDevolvidos!num_processo, "") & "', '" & _
                BL_HandleNull(RsDevolvidos!dta_analise, "") & "', '" & BL_HandleNull(RsDevolvidos!cod_especialidade, "") & "', '" & _
                BL_HandleNull(RsDevolvidos!cod_analise, "") & "', '" & BL_HandleNull(RsDevolvidos!Erro, "") & "')"
                BG_ExecutaQuery_ADO sql
                
                RsDevolvidos.MoveNext
            Wend
            
            LbTotalRegistos.caption = RsDevolvidos.RecordCount & " Registos"
            BL_Toolbar_BotaoEstado "Imprimir", "Activo"
            BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
            BtImprimir.Enabled = True
            
        Else
            BG_Mensagem mediMsgBox, "N�o foram seleccionados registos", vbExclamation, App.ProductName
        End If
        BL_Fecha_conexao_HIS
    'GH
    ElseIf ((HisAberto = 1) And (UCase(HIS.nome) = UCase("GH"))) Then
        sql = " SELECT rowid, t_doente, doente, t_episodio, episodio, dt_ini_acto,cod_serv_req, cod_acto,qtd,  flg_estado, dt_cri"
        sql = sql & " FROM dados_fact  WHERE flg_estado <> 'S' AND user_cri ='SISLAB' AND dt_ini_acto BETWEEN "
        sql = sql & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
        sql = sql & " ORDER BY t_episodio, episodio, dt_ini_acto ASC "
        
        Set RsDevolvidos = New ADODB.recordset
        RsDevolvidos.CursorLocation = adUseServer
        RsDevolvidos.CursorType = adOpenStatic
        RsDevolvidos.Open sql, gConnHIS
        If RsDevolvidos.RecordCount > 0 Then
            While Not RsDevolvidos.EOF
                totalFact = totalFact + 1
                ReDim Preserve EstruturaFact(totalFact)
                FgRes.AddItem ""
                FgRes.TextMatrix(totalFact, 0) = BL_HandleNull(RsDevolvidos!t_episodio, "")
                FgRes.TextMatrix(totalFact, 1) = BL_HandleNull(RsDevolvidos!episodio, "")
                FgRes.TextMatrix(totalFact, 2) = BL_HandleNull(RsDevolvidos!t_doente, "") & "/" & BL_HandleNull(RsDevolvidos!doente, "")
                FgRes.TextMatrix(totalFact, 3) = BL_HandleNull(RsDevolvidos!dt_ini_acto, "")
                FgRes.TextMatrix(totalFact, 4) = BL_HandleNull(RsDevolvidos!cod_serv_req, "")
                FgRes.TextMatrix(totalFact, 5) = BL_HandleNull(RsDevolvidos!cod_acto, "")
                FgRes.TextMatrix(totalFact, 6) = BL_HandleNull(RsDevolvidos!flg_estado, "")
                
                EstruturaFact(totalFact).RowId = RsDevolvidos!RowId
                EstruturaFact(totalFact).cod_acto = BL_HandleNull(RsDevolvidos!cod_acto, "")
                EstruturaFact(totalFact).cod_serv_req = BL_HandleNull(RsDevolvidos!cod_serv_req, "")
                EstruturaFact(totalFact).doente = BL_HandleNull(RsDevolvidos!doente, "")
                EstruturaFact(totalFact).dt_cri = BL_HandleNull(RsDevolvidos!dt_cri, "")
                EstruturaFact(totalFact).dt_ini_acto = BL_HandleNull(RsDevolvidos!dt_ini_acto, "")
                EstruturaFact(totalFact).episodio = BL_HandleNull(RsDevolvidos!episodio, "")
                EstruturaFact(totalFact).flg_estado = BL_HandleNull(RsDevolvidos!flg_estado, "")
                EstruturaFact(totalFact).qtd = BL_HandleNull(RsDevolvidos!qtd, "")
                EstruturaFact(totalFact).t_doente = BL_HandleNull(RsDevolvidos!t_doente, "")
                EstruturaFact(totalFact).t_episodio = BL_HandleNull(RsDevolvidos!t_episodio, "")
                
                sql = "INSERT INTO sl_cr_devolvidos_fact(nome_computador, t_episodio, num_episodio, num_processo, " & _
                    " dt_chega, servico, cod_ana_gh, erro) VALUES ('" & gComputador & "', '" & EstruturaFact(totalFact).t_episodio & "', '" & _
                EstruturaFact(totalFact).episodio & "', '" & EstruturaFact(totalFact).doente & "', '" & _
                EstruturaFact(totalFact).dt_ini_acto & "', '" & EstruturaFact(totalFact).cod_serv_req & "', '" & _
                EstruturaFact(totalFact).cod_acto & "', '" & EstruturaFact(totalFact).Erro & "')"
                BG_ExecutaQuery_ADO sql
                
                RsDevolvidos.MoveNext
            Wend
            
            LbTotalRegistos.caption = RsDevolvidos.RecordCount & " Registos"
            BL_Toolbar_BotaoEstado "Imprimir", "Activo"
            BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
            BtImprimir.Enabled = True
            
        Else
            BG_Mensagem mediMsgBox, "N�o foram seleccionados registos", vbExclamation, App.ProductName
        End If
        gConnHIS.Close
        Set gConnHIS = Nothing
    
    Else
        BG_Mensagem mediMsgBox, "Erro ao abrir conex�o ao " & HIS.nome & vbCrLf & Err.Description, vbCritical, App.ProductName
    End If
    
    BL_MudaCursorRato mediMP_Activo, Me
    Exit Sub
    
TrataErro:
    BL_MudaCursorRato mediMP_Activo, Me
    BG_Mensagem mediMsgBox, "Erro ao procurar registos no " & HIS.nome & vbCrLf & Err.Description, vbExclamation, App.ProductName
    
End Sub

Sub FuncaoImprimir()

    Dim continua As Boolean

    On Error GoTo TrataErro

    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("DevolvidosFacturacao", "Registos devolvidos da factura��o", crptToPrinter)
    Else
        continua = BL_IniciaReport("DevolvidosFacturacao", "Registos devolvidos da factura��o", crptToWindow)
    End If
    If continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SelectionFormula = "{sl_cr_devolvidos_fact.nome_computador} = '" & BG_SYS_GetComputerName & "'" ' AND {sb_id_instituicao.estado}='S'"
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    
    Me.SetFocus
    
    Report.Connect = "DSN=" & gDSN
    
    Call BL_ExecutaReport

    BL_MudaCursorRato mediMP_Activo, Me
    
    Exit Sub

TrataErro:

End Sub

Sub LimpaFlexGrid()

    Dim i As Long
    
    i = FgRes.rows
    While i > 2
        FgRes.RemoveItem i
        i = i - 1
    Wend
    FgRes.TextMatrix(1, 0) = ""
    FgRes.TextMatrix(1, 1) = ""
    FgRes.TextMatrix(1, 2) = ""
    FgRes.TextMatrix(1, 3) = ""
    FgRes.TextMatrix(1, 4) = ""
    FgRes.TextMatrix(1, 5) = ""
    FgRes.TextMatrix(1, 6) = ""
    
End Sub

Sub PreencheDescricaoRubrica(codRubrica As String)
    
    Dim sql As String
    Dim RsDescrRubr As ADODB.recordset
    
    On Error GoTo TrataErro
        
    sql = "SELECT descr_sonho descricao from sl_analises_sonho WHERE cod_sonho =" & BL_TrataStringParaBD(codRubrica)
    Set RsDescrRubr = New ADODB.recordset
    RsDescrRubr.CursorLocation = adUseServer
    RsDescrRubr.CursorType = adOpenStatic
    RsDescrRubr.Open sql, gConexao
    If RsDescrRubr.RecordCount > 0 Then
        LaRubrica = BL_HandleNull(RsDescrRubr!descricao, "N�o encontrou a rubrica na factura��o")
    Else
        LaRubrica.caption = "N�o encontrou a rubrica na factura��o"
    End If
    RsDescrRubr.Close
    Set RsDescrRubr = Nothing
    Exit Sub
TrataErro:
    Exit Sub
End Sub

Function ValidaEpisodio(processo As String, tEpisodio As String, episodio As String) As Integer
    
    On Error GoTo ErrorHandler:
    
    Dim rv As Integer
    Dim situacao As Integer
    
        Select Case tEpisodio
            Case "URG"
                situacao = gT_Urgencia
            Case "CON"
                situacao = gT_Consulta
            Case "INT"
                situacao = gT_Internamento
            Case "HDI"
                situacao = gT_Externo
            Case "LAB"
                situacao = gT_LAB
            Case "RAD"
                situacao = gT_RAD
            Case "BLO"
                situacao = gT_Bloco
            Case Else
                situacao = ""
        End Select

            ' Manda-se o processo para tornar o processo mais fiavel.
            ' (Na pesquisa de Utente, usamos o codigo de sessao).
            rv = SONHO_Get_Processo(situacao, _
                                    episodio, _
                                    processo, _
                                    "", _
                                    "")
                              
            If ((rv = 1) And (processo = Trim(EcProcesso.Text))) Then
                ' O processo coincide.

                
'                MsgBox vbCrLf & _
'                       "O epis�dio indicado corresponde ao Utente actual.            " & vbCrLf & vbCrLf, vbInformation, " Actualizar Epis�dio"
                ValidaEpisodio = 1
            Else
                If (rv = 1) Then
                    ' O processo nao coincide.
                    MsgBox vbCrLf & _
                        "O epis�dio indicado n�o corresponde ao Utente actual.", vbExclamation, " Actualizar Epis�dio"

                    ValidaEpisodio = -1
                Else
                    MsgBox vbCrLf & _
                        "O epis�dio n�o foi actualizado.             " & vbCrLf & vbCrLf, vbCritical, " Actualizar Epis�dio"
                    ValidaEpisodio = -1
                End If
            
            End If

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : ActualizaEpisodio (FormListarDevolvidosSONHO) -> " & Err.Description)
            Exit Function
    End Select
End Function

Sub MudaCorFundo()
    Dim tmpCol As Integer
    Dim tmpRow As Integer
    '
    tmpCol = FgRes.Col
    tmpRow = FgRes.row
    
    FgRes.Col = LastCol
    FgRes.row = LastRow
    If FgRes.row <> 0 Then
        FgRes.CellBackColor = vbWhite
    End If
    
    FgRes.Col = tmpCol
    FgRes.row = tmpRow
    
    If FgRes.row <> 0 Then
        FgRes.CellBackColor = vbCyan
    End If
    
End Sub

'Sub ActualizaDadosEstrutura(Indice As Integer)
'
'    If EcRubrica.Text = "" Then Exit Sub
'
'    If EcProcesso.Text <> "" Then
'        EstruturaFact(Indice).NumProcesso = EcProcesso.Text
'    End If
'    EstruturaFact(Indice).CodRubrica = EcRubrica.Text
'    EstruturaFact(Indice).TEpisodio = EcTipoEpisodio.Text
'    EstruturaFact(Indice).Episodio = EcEpisodio.Text
'    EstruturaFact(Indice).NumCartao = EcNumCartao.Text
'    EstruturaFact(Indice).CodEFR = EcCodEFR.Text
'
'End Sub

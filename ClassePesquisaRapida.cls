VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClassPesqResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Private ResPesquisa() As Variant
Private NumeroElementos As Integer
Private PesquisaFoiCancelada As Boolean

Public Sub InicializaResultados(Num As Integer)
    
    If Num = 0 Then Num = 1
    NumeroElementos = Num
    ReDim Preserve ResPesquisa(1 To NumeroElementos)
    
End Sub

Public Sub RetornaResultados(v() As Variant, _
                             Optional ByRef PesquisaRapidaFoiCancelada, _
                             Optional ByRef NumElementosSeleccionados As Integer)
    
    Dim i As Integer
    Dim k As Integer
    
    For i = 1 To NumeroElementos
        
        ' pferreira 2010.08.09
        If (ResPesquisa(i) <> Empty) Then
            k = k + 1
            v(k) = ResPesquisa(i)
        End If
       
    Next i
    If Not IsMissing(PesquisaRapidaFoiCancelada) Then
        PesquisaRapidaFoiCancelada = PesquisaFoiCancelada
    End If
    
    'NELSONPSILVA UALIA-856 31.01.2019
    If gPassaParams.id = "CarregaCativacao" Then
        NumElementosSeleccionados = k
    Else
        NumElementosSeleccionados = NumeroElementos
    End If

End Sub

Public Sub PesquisaCancelada(valor As Boolean)
    
    PesquisaFoiCancelada = valor

End Sub

Public Sub ResPesquisaLet(indice As Variant, valor As Variant)
    
    On Error Resume Next
    ResPesquisa(indice) = valor
   
End Sub

Public Function RetornaPosicaoResPesquisa(valor As Variant) As Long
    Dim i As Long
        valor = Replace(valor, "<KEY>", "")
        For i = 1 To NumeroElementos
            If ResPesquisa(i) = valor Then
                RetornaPosicaoResPesquisa = i
                Exit Function
            End If
        Next
    RetornaPosicaoResPesquisa = 0
End Function

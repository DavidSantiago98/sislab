VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form FormGrafico 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "GR�FICO"
   ClientHeight    =   6750
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9780
   Icon            =   "FormGrafico.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6750
   ScaleWidth      =   9780
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   8760
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tipo de gr�fico "
      Height          =   615
      Left            =   840
      TabIndex        =   1
      Top             =   120
      Width           =   3135
      Begin VB.ComboBox CmbEstilo 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   2655
      End
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   5535
      Left            =   240
      OleObjectBlob   =   "FormGrafico.frx":000C
      TabIndex        =   0
      Top             =   720
      Width           =   9255
   End
End
Attribute VB_Name = "FormGrafico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public FormName As String

' Actualiza��o : --/--/----
' T�cnico

Private Sub CmbEstilo_Click()
    
    If CmbEstilo.ListIndex = -1 Then
        Exit Sub
    End If
    
    With MSChart1
        Select Case CmbEstilo.ListIndex
            Case 0
                .chartType = VtChChartType2dArea
            Case 1
                .chartType = VtChChartType2dBar
            Case 2
                .chartType = VtChChartType2dLine
            Case 3
                .chartType = VtChChartType2dCombination
            Case 4
                .chartType = VtChChartType2dStep
            Case 5
                .chartType = VtChChartType2dPie
            Case 6
                .chartType = VtChChartType3dArea
            Case 7
                .chartType = VtChChartType3dBar
            Case 8
                .chartType = VtChChartType3dLine
            Case 9
                .chartType = VtChChartType3dCombination
            Case 10
                .chartType = VtChChartType3dStep
        End Select
        
        'Refresh
        .Layout
        'Settings originais excepto valores!
        '.ToDefaults
    End With
    
    If CmbEstilo.ListIndex = 5 Then
        MSChart1.Plot.DataSeriesInRow = True
        MSChart1.ShowLegend = True
    Else
        MSChart1.Plot.DataSeriesInRow = False
        MSChart1.ShowLegend = False
    End If
    
End Sub









Private Sub Form_Activate()
    
    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    Set gFormActivo = Me
    
End Sub

Private Sub Form_Load()
    
    'Preenche Combo com os diversos estilos de gr�ficos
    Call PreencheComboEstilos
    
    With MSChart1
        
        With .title
            With .Backdrop
                With .Frame
                    .Style = VtFrameStyleSingleLine
                    .Width = 20
                    .SpaceColor.Set 0, 0, 255
                End With
                With .Shadow
                    .Offset.Set 20, 5
                    .Style = VtShadowStyleNull
                    .Brush.Style = VtBrushStylePattern
                End With
                With .Fill
                    .Style = VtFillStyleBrush
                    .Brush.Style = VtBrushStyleSolid
                    .Brush.FillColor.Set 255, 255, 255
                End With
            End With
            
'            .Text = "EVOLU��O DE RESULTADOS"
            
            With .TextLayout
                .Orientation = VtOrientationHorizontal
                .HorzAlignment = VtHorizontalAlignmentCenter
                .VertAlignment = VtVerticalAlignmentCenter
                .WordWrap = True
            End With
            
            With .Location
                .LocationType = VtChLocationTypeTop
                .Visible = True
            End With
            
        End With
        
'        With .Legend
'            With .Backdrop
'                With .Frame
'                    .Style = VtFrameStyleSingleLine
'                    .Width = 20
'                End With
'                With .Shadow
'                    .Offset.Set 20, 5
'                    .Style = VtShadowStyleDrop
'                    .Brush.Style = VtBrushStylePattern
'                End With
'                With .Fill
'                    .Style = VtFillStyleBrush
'                    .Brush.FillColor.Set 0, 0, 0
'                    .Brush.Style = VtBrushStylePattern
'                    .Brush.PatternColor.Set 245, 255, 216
'                End With
'            End With
'
'            With .Location
'                .LocationType = VtChLocationTypeRight
'                .Visible = True
'            End With
'
'            With .TextLayout
'                .Orientation = VtOrientationVertical
'                .HorzAlignment = VtHorizontalAlignmentCenter
'                .VertAlignment = VtVerticalAlignmentCenter
'                .WordWrap = True
'            End With
'
'        End With
        
'        With .Footnote
'            With .Backdrop
'                With .Frame
'                    .Style = VtFrameStyleNull
'                End With
'                With .Shadow
'                    .Style = VtShadowStyleNull
'                End With
'                With .Fill
'                    .Style = VtFillStyleNull
'                End With
'            End With
'
'            With .Location
'                .LocationType = VtChLocationTypeBottom
'                .Visible = True
'            End With
'
'            .Text = "Nota:A evolu��o dos resultados verificada est� dentro dos valores normais."
'
'            With .TextLayout
'                .Orientation = VtOrientationHorizontal
'                .HorzAlignment = VtHorizontalAlignmentCenter
'                .VertAlignment = VtVerticalAlignmentCenter
'                .WordWrap = True
'            End With
'
'        End With
        
        With .Plot
            '.AutoLayout = True
            
            With .Backdrop
                With .Frame
                    .Style = VtFrameStyleSingleLine
                    '.FrameColor.Set 0, 0, 0
                    .Width = 20
                End With
                With .Fill
                    .Style = VtFillStyleBrush
                    .Brush.FillColor.Set 245, 255, 216
                    .Brush.Style = VtBrushStyleSolid
                End With
                With .Shadow
                    .Style = VtShadowStyleNull
                    .Brush.Style = VtBrushStylePattern
                    .Offset.Set 50, 50
                    '.Brush.FillColor.Set 120, 120, 120
                End With
            End With
            
            'Para os gr�fico tipo PIE:
            'Sentido em que o gr�fico � desenhado relativamente aos ponteiros do rel�gio
            .Clockwise = True
            .AngleUnit = VtAngleUnitsDegrees
            .Sort = VtSortTypeAscending
            'Posi��o das Labels
            .SubPlotLabelPosition = VtChSubPlotLabelLocationTypeAbove
            
            'PARA OS GR�FICOS 3D:
            'Posi��o inicial dos gr�ficos (CTRL)
            'Lado
            .Projection = VtProjectionTypePerspective
            'Rota��o
            .View3d.Elevation = 10
            
            'Ilumina��o
            With .Light
                '.AmbientIntensity = 0.4
                '.EdgeIntensity = 0.5
                '.EdgeVisible = True
            End With
            
            With .Wall
                '.Brush.FillColor.Set 0, 0, 0
                .Brush.Style = VtBrushStyleNull
                '.Brush.PatternColor.Set 255, 255, 255
                'Tipo de linha e cor para desenho da Wall
                With .Pen
                    'Como termina nos cantos
                    .Cap = VtPenCapRound
                    .Join = VtPenJoinRound
                    .Limit = 20
                    .Style = VtPenStyleSolid
                    .Width = 24
                    .VtColor.Set 0, 0, 255
                End With
            End With
            
            'Parte do Baixo do Quadro
            With .PlotBase
                .Brush.Style = VtBrushStyleSolid
                '.Brush.FillColor.Set 0, 0, 255
                .BaseHeight = 100
            End With
            
        End With
                 
        
        With .Backdrop
            With .Frame
                .Style = VtFrameStyleDoubleLine
                .Width = 20
                .SpaceColor.Set 0, 0, 255
            End With
            With .Shadow
                .Style = VtShadowStyleNull
            End With
            With .Fill
                .Style = VtFillStyleNull
            End With
        End With
        
        .chartType = VtChChartType2dLine
        
        .ShowLegend = False
              
        'Em gr�ficos de 3D carregando na tecla CTRL roda o gr�fico
        .AllowDynamicRotation = True
    
        'Permite o utilizador seleccionar os objectos do gr�fico
        .AllowSelections = True
    
        'Palete de cores do gr�fico (ajuste dos valores)
        .AllowDithering = False
        
        'Click na Data do Chart n�o selecciona apenas o ponto mas sim toda a S�rie associada
        .AllowSeriesSelection = True
    
        'Sobreposi��o de todas as s�ries
        .Stacking = False
        
    End With
    
    
End Sub



Private Sub PreencheComboEstilos()
    
    CmbEstilo.AddItem "�rea 2D"
    CmbEstilo.AddItem "Barras 2D"
    CmbEstilo.AddItem "Linha 2D"
    CmbEstilo.AddItem "Combi 2D"
    CmbEstilo.AddItem "Escada 2D"
    CmbEstilo.AddItem "C�rculo 2D"
    CmbEstilo.AddItem "�rea 3D"
    CmbEstilo.AddItem "Barras 3D"
    CmbEstilo.AddItem "Linha 3D"
    CmbEstilo.AddItem "Combi 3D"
    CmbEstilo.AddItem "Escada 3D"
    
    CmbEstilo.ListIndex = 2
    
End Sub



Private Sub Form_Unload(Cancel As Integer)
    
    Set FormGrafico = Nothing
    If FormName = "FormHistUtente" Then
        FormHistUtente.Enabled = True
        FormHistUtente.Visible = True
    End If
    
End Sub






Public Sub FuncaoImprimir()
        
    On Error GoTo TrataErro
    
        'Mostra a caixa de di�logo da impressora=>Carrega Estrutura
    If BL_CaixaImpressora(, , , , "Imprimir Gr�fico") = False Then
        Exit Sub
    End If
    
    MSChart1.SelectPart VtChPartTypeChart, 0, 0, 0, 0
    MSChart1.EditCopy
    MSChart1.EditPaste
    
    Set Picture1.Picture = Clipboard.GetData
    Clipboard.Clear
    
    Printer.ScaleMode = vbTwips
    Printer.PaintPicture Picture1.Picture, 1, 1, 9000, 7000
    Printer.EndDoc
            
    
    Exit Sub
    
TrataErro:
        MsgBox Err.Description, vbCritical, App.ProductName
        Err.Clear
    
End Sub


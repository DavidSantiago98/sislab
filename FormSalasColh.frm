VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormSalasColh 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormSalasColh"
   ClientHeight    =   6930
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10680
   Icon            =   "FormSalasColh.frx":0000
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6930
   ScaleWidth      =   10680
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcLocalADSE 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   9720
      TabIndex        =   77
      Top             =   960
      Width           =   855
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Sala Desactiva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   8760
      TabIndex        =   76
      Top             =   120
      Width           =   1695
   End
   Begin VB.Frame FrameEtiq 
      Caption         =   "Impress�o de Etiquetas Pr� impressas"
      Height          =   5415
      Left            =   120
      TabIndex        =   66
      Top             =   7680
      Width           =   10335
      Begin VB.TextBox EcAux 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   0
         TabIndex        =   74
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
      Begin MSFlexGridLib.MSFlexGrid FgTubos 
         Height          =   4335
         Left            =   240
         TabIndex        =   73
         Top             =   960
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   7646
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
         Appearance      =   0
      End
      Begin VB.TextBox EcNumImpr 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4920
         TabIndex        =   71
         Top             =   480
         Width           =   945
      End
      Begin VB.TextBox EcNumCopias 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2040
         TabIndex        =   69
         Top             =   480
         Width           =   945
      End
      Begin VB.CommandButton BtSairImpr 
         Height          =   615
         Left            =   9480
         Picture         =   "FormSalasColh.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   67
         ToolTipText     =   "Sair Sem Gravar"
         Top             =   4680
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "N�meros Gerados"
         Height          =   225
         Index           =   2
         Left            =   3600
         TabIndex        =   72
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Etiquetas Administrativas"
         Height          =   225
         Index           =   1
         Left            =   240
         TabIndex        =   70
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.CommandButton BtImprEtiq 
      Height          =   495
      Left            =   9960
      Picture         =   "FormSalasColh.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   68
      Top             =   3240
      Width           =   615
   End
   Begin VB.CheckBox CkGeraNumReq 
      Caption         =   "Gera N�m. Requis."
      Height          =   255
      Left            =   8520
      MousePointer    =   1  'Arrow
      TabIndex        =   64
      ToolTipText     =   "Inibe Impress�o de An�lises para entidades  e salas codificadas para tal."
      Top             =   2760
      Width           =   1815
   End
   Begin VB.CommandButton BtLimpaAnalises 
      Height          =   375
      Left            =   9720
      Picture         =   "FormSalasColh.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   22
      ToolTipText     =   "Limpa an�lises"
      Top             =   3360
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton BtAnalises 
      Height          =   375
      Left            =   10080
      Picture         =   "FormSalasColh.frx":1D2A
      Style           =   1  'Graphical
      TabIndex        =   23
      ToolTipText     =   "Procura an�lises"
      Top             =   3360
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodExt 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   7800
      TabIndex        =   9
      Top             =   960
      Width           =   855
   End
   Begin VB.ListBox EcLocais 
      Appearance      =   0  'Flat
      Height          =   705
      Left            =   1080
      Style           =   1  'Checkbox
      TabIndex        =   16
      Top             =   2040
      Width           =   3855
   End
   Begin VB.CheckBox CkInibeImpAna 
      Caption         =   "Inibe Impress�o Ana."
      Height          =   255
      Left            =   6000
      MousePointer    =   1  'Arrow
      TabIndex        =   21
      ToolTipText     =   "Inibe Impress�o de An�lises para entidades  e salas codificadas para tal."
      Top             =   2760
      Width           =   1815
   End
   Begin VB.TextBox EcIntervalo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   9720
      TabIndex        =   6
      Top             =   525
      Width           =   855
   End
   Begin VB.TextBox EcMinImpressao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6000
      TabIndex        =   4
      Top             =   525
      Width           =   855
   End
   Begin VB.TextBox EcMaxImpressao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   7800
      TabIndex        =   5
      Top             =   525
      Width           =   855
   End
   Begin VB.TextBox EcLocalidade 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6000
      TabIndex        =   15
      Top             =   1725
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   6240
      TabIndex        =   55
      Top             =   8160
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodPostalCompleto 
      Height          =   285
      Left            =   3240
      TabIndex        =   53
      Top             =   8400
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton BtPesqCodPostal 
      Height          =   315
      Left            =   4560
      Picture         =   "FormSalasColh.frx":22B4
      Style           =   1  'Graphical
      TabIndex        =   52
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
      Top             =   1680
      Width           =   375
   End
   Begin VB.TextBox EcDescrPostal 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   51
      Top             =   1680
      Width           =   2415
   End
   Begin VB.TextBox EcRuaPostal 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1800
      TabIndex        =   14
      Top             =   1680
      Width           =   495
   End
   Begin VB.TextBox EcCodPostal 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   13
      Top             =   1680
      Width           =   735
   End
   Begin VB.TextBox EcMorada 
      Appearance      =   0  'Flat
      Height          =   735
      Left            =   1080
      MultiLine       =   -1  'True
      TabIndex        =   7
      Top             =   840
      Width           =   3855
   End
   Begin VB.TextBox EcEmail 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      MaxLength       =   100
      TabIndex        =   3
      Top             =   2880
      Width           =   3855
   End
   Begin VB.TextBox EcMargem 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   9720
      TabIndex        =   12
      Top             =   1380
      Width           =   855
   End
   Begin VB.CheckBox CkSeqRecibos 
      Caption         =   "Sequ�ncia Recibos"
      Height          =   255
      Left            =   8520
      MousePointer    =   1  'Arrow
      TabIndex        =   20
      Top             =   2400
      Width           =   1815
   End
   Begin VB.TextBox EcSeqRecibos 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   6000
      TabIndex        =   10
      Top             =   1380
      Width           =   855
   End
   Begin VB.TextBox EcPrefixo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   7800
      TabIndex        =   11
      Top             =   1380
      Width           =   855
   End
   Begin VB.CheckBox EcFlgImpressao 
      Caption         =   " Impress�o Etiquetas"
      Height          =   255
      Left            =   8520
      MousePointer    =   1  'Arrow
      TabIndex        =   18
      Top             =   2040
      Width           =   1815
   End
   Begin VB.TextBox EcSeqImpressao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6000
      TabIndex        =   8
      Top             =   960
      Width           =   855
   End
   Begin VB.CheckBox EcFlgColheita 
      Caption         =   " Sala de Colheita"
      Height          =   255
      Left            =   6000
      MousePointer    =   1  'Arrow
      TabIndex        =   19
      Top             =   2400
      Width           =   1815
   End
   Begin VB.TextBox EcAbrev 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3960
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcAna 
      Enabled         =   0   'False
      Height          =   285
      Left            =   360
      TabIndex        =   42
      Top             =   8040
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcCodLocal 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2280
      TabIndex        =   41
      Top             =   8400
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CheckBox EcFlgCriancas 
      Caption         =   " Sala de Crian�as"
      Height          =   255
      Left            =   6000
      TabIndex        =   17
      Top             =   2040
      Width           =   1815
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   120
      TabIndex        =   30
      Top             =   5880
      Width           =   10485
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   36
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   35
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   34
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   33
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   29
      Top             =   8040
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   360
      TabIndex        =   28
      Top             =   8400
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   27
      Top             =   8400
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3240
      TabIndex        =   26
      Top             =   8040
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   0
      Top             =   120
      Width           =   945
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      MaxLength       =   100
      TabIndex        =   2
      Top             =   480
      Width           =   3855
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   2280
      TabIndex        =   25
      Top             =   8040
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      ItemData        =   "FormSalasColh.frx":283E
      Left            =   120
      List            =   "FormSalasColh.frx":2840
      TabIndex        =   24
      Top             =   3720
      Width           =   10455
   End
   Begin VB.CheckBox CkInterno 
      Caption         =   "Posto/Sala Interno"
      Height          =   255
      Left            =   6000
      MousePointer    =   1  'Arrow
      TabIndex        =   75
      ToolTipText     =   "Inibe Impress�o de An�lises para entidades  e salas codificadas para tal."
      Top             =   3120
      Width           =   1815
   End
   Begin VB.Label Label15 
      Caption         =   "Local ADSE"
      Height          =   225
      Index           =   7
      Left            =   8760
      TabIndex        =   78
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label10 
      Caption         =   "M�ximo"
      Height          =   255
      Left            =   4440
      TabIndex        =   65
      Top             =   3480
      Width           =   615
   End
   Begin VB.Label Label15 
      Caption         =   "C�d. Ext."
      Height          =   225
      Index           =   6
      Left            =   6960
      TabIndex        =   63
      Top             =   960
      Width           =   1095
   End
   Begin VB.Label LbLocal 
      AutoSize        =   -1  'True
      Caption         =   "Locais"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   62
      Top             =   2085
      Width           =   465
   End
   Begin VB.Label Label15 
      Caption         =   "Intervalo"
      Height          =   225
      Index           =   5
      Left            =   8760
      TabIndex        =   61
      Top             =   525
      Width           =   615
   End
   Begin VB.Label Label15 
      Caption         =   "Min. Imp."
      Height          =   225
      Index           =   4
      Left            =   5040
      TabIndex        =   60
      Top             =   525
      Width           =   735
   End
   Begin VB.Label Label15 
      Caption         =   "M�x. Imp."
      Height          =   225
      Index           =   3
      Left            =   6960
      TabIndex        =   59
      Top             =   525
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Localidade"
      Height          =   225
      Index           =   3
      Left            =   5040
      TabIndex        =   58
      Top             =   1725
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�d. Postal"
      Height          =   225
      Index           =   2
      Left            =   120
      TabIndex        =   57
      Top             =   1680
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodPostalAux"
      Height          =   255
      Left            =   4800
      TabIndex        =   56
      Top             =   8160
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "EcCodPostalCompleto"
      Height          =   255
      Left            =   1440
      TabIndex        =   54
      Top             =   8400
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Morada"
      Height          =   225
      Index           =   1
      Left            =   120
      TabIndex        =   50
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Email"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   49
      Top             =   2880
      Width           =   855
   End
   Begin VB.Label Label15 
      Caption         =   "Margem"
      Height          =   225
      Index           =   2
      Left            =   8760
      TabIndex        =   48
      Top             =   1380
      Width           =   615
   End
   Begin VB.Label Label15 
      Caption         =   "Seq. Recibos"
      Height          =   225
      Index           =   1
      Left            =   5040
      TabIndex        =   47
      Top             =   1380
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Prefixo"
      Height          =   225
      Index           =   0
      Left            =   6960
      TabIndex        =   46
      Top             =   1380
      Width           =   615
   End
   Begin VB.Label Label15 
      Caption         =   "Seq. Impr."
      Height          =   225
      Index           =   0
      Left            =   5040
      TabIndex        =   45
      Top             =   960
      Width           =   1095
   End
   Begin VB.Label Label14 
      Caption         =   "Abreviatura"
      Height          =   255
      Left            =   3000
      TabIndex        =   44
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label13 
      Caption         =   "M�nimo"
      Height          =   255
      Left            =   3240
      TabIndex        =   43
      Top             =   3480
      Width           =   615
   End
   Begin VB.Label Label9 
      Caption         =   "Abreviatura"
      Height          =   255
      Left            =   1080
      TabIndex        =   40
      Top             =   3480
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   39
      Top             =   3480
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   225
      Index           =   0
      Left            =   120
      TabIndex        =   38
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   37
      Top             =   480
      Width           =   855
   End
End
Attribute VB_Name = "FormSalasColh"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''
' Actualiza��o: 12/04/2007 '''''''''''''''''
' T�cnico: Paulo Ferreira ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de estado do ecra '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Const CS_INACTIVE = 0
Const CS_ACTIVE = 1
Const CS_WAIT = 2

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de indices ''''''''''''''''''''
Const CI_SEQSALA = 0
Const CI_CODSALA = 1
Const CI_DESCRSALA = 2
Const CI_USERCRI = 3
Const CI_DTCRI = 4
Const CI_USERACT = 5
Const CI_DTACT = 6
Const CI_FLGCRIANCAS = 7
Const CI_ANALISES = 9
Const CI_FLGCOLHEITA = 10
Const CI_ABREV = 11
Const CI_SEQIMPRESSAO = 12
Const CI_FLGIMPRESSAO = 13
Const CI_PREFIXO = 14
Const CI_SEQRECIBOS = 15
Const CI_FLGRECIBOS = 16
Const CI_Email = 17
Const CI_MORADA = 18
Const CI_CodPostal = 19
Const CI_Localidade = 20
Const CI_margem = 21
Const CI_MAXIMPRESSAO = 22
Const CI_MINIMPRESSAO = 23
Const CI_INTERVALO = 24
Const CI_INIBE_IMP_ANA = 25
Const CI_COD_EXT = 8
Const CI_GERA_REQ = 26
Const CI_Num_copias = 27
Const CI_num_impr = 28
Const CI_flg_interno = 29
Const CI_flg_invisivel = 30
Const CI_local_adse = 31

''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
' Variaveis de BD ''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public rs As ADODB.recordset
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String
Dim ChaveBD As String
Dim ChaveEc As Object
Dim NomeTabela As String
Dim CriterioTabela As String
Dim CamposBDparaListBox
Dim NumEspacos

''''''''''''''''''''''''''''''''''''''''''''
' Estado do ecra '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim estado As Integer

''''''''''''''''''''''''''''''''''''''''''''
' Variaveis de focus '''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim CampoActivo As Object
Dim CampoDeFocus As Object

''''''''''''''''''''''''''''''''''''''''''''
' Referencia do primeiro registo lista '''''
''''''''''''''''''''''''''''''''''''''''''''
Dim MarcaInicial As Variant

Dim MaxImp As Long
Dim SeqImp As Long

Private Type Tubos
    cod_tubo As String
    descR_tubo As String
    num_copias As Integer
End Type
Dim estrutTubos() As Tubos
Dim totalTubos As Integer

''''''''''''''''''''''''''''''''''''''''''''
' 'Click' do botao 'BtAnalises' ''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtAnalises_Click()
    MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAMARC
End Sub

Private Sub BtImprEtiq_Click()
    FrameEtiq.top = 480
    FrameEtiq.left = 120
    FrameEtiq.Visible = True
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' 'Click' do botao 'BtLimpaAnalises' '''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtLimpaAnalises_Click()
    EcAna.Text = ""
    EcCodigo_Validate False
End Sub




Private Sub CkAvisosPag_Click()

End Sub



Private Sub BtSairImpr_Click()
    FrameEtiq.Visible = False
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de validacao do campo 'EcCodigo' ''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcCodigo_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub



''''''''''''''''''''''''''''''''''''''''''''
' Evento de validacao do campo 'EcDescricao'
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcDescricao_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub

Private Sub EcFlgColheita_Click()
    If EcFlgColheita.value = vbChecked Then
        EcMargem.Enabled = False
        EcMargem = ""
        
'        EcCodPostal = ""
'        EcRuaPostal = ""
'        EcDescrPostal = ""
'        EcCodPostalAux = ""
'        EcCodPostalCompleto = ""
'        EcCodPostal.Enabled = True
'        EcRuaPostal.Enabled = True
'        EcDescrPostal.Enabled = True
'        BtPesqCodPostal.Enabled = True
'        EcMorada = ""
'        EcMorada.Enabled = True
'        EcLocalidade.Enabled = True
'        EcLocalidade = ""
    Else
        EcMargem.Enabled = True
'        EcCodPostal.Enabled = True
'        EcRuaPostal.Enabled = True
'        EcDescrPostal.Enabled = True
'        BtPesqCodPostal.Enabled = True
'        EcMorada.Enabled = True
'        EcLocalidade.Enabled = True
    End If
End Sub

Private Sub EcFlgImpressao_Click()
    If EcFlgImpressao.value = 1 Then
        EcSeqImpressao.Enabled = True
    Else
        EcSeqImpressao.Enabled = False
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de 'click' da lista '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcLista_Click()
    
    ' Se a lista contiver dados preenche campos correspondentes
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' 'Focus' do campo 'EcCodigo' ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' 'Lostfocus' do campo 'EcCodigo' ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcCodigo_LostFocus()
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' 'Focus' do campo 'EcDescricao' '''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcMargem_Change()
    If EcMargem <> "" Then
        If IsNumeric(EcMargem) = False Then
            BG_Mensagem mediMsgBox, "Valor inserido n�o � num�rico!", vbExclamation, "Procurar"
            Sendkeys ("{HOME}+{END}")
        Else
        End If
    End If
End Sub
Private Sub EcIntervalo_Change()
    If EcIntervalo <> "" Then
        If IsNumeric(EcIntervalo) = False Then
            BG_Mensagem mediMsgBox, "Valor inserido n�o � num�rico!", vbExclamation, "Procurar"
            Sendkeys ("{HOME}+{END}")
        End If
    End If
End Sub
Private Sub EcMargem_LostFocus()
    If EcMargem <> "" Then
        If IsNumeric(EcMargem) = False Then
            Sendkeys ("{HOME}+{END}")
            EcMargem.SetFocus
        Else
            If CInt(EcMargem) > 100 Or CInt(EcMargem) < 0 Then
                Sendkeys ("{HOME}+{END}")
                EcMargem.SetFocus
            End If
        End If
    End If
End Sub
Private Sub EcIntervalo_LostFocus()
    If EcIntervalo <> "" Then
        If IsNumeric(EcIntervalo) = False Then
            Sendkeys ("{HOME}+{END}")
            EcIntervalo.SetFocus
        Else
            If EcMinImpressao.Text <> "" Then
                EcMaxImpressao.Text = CLng(EcMinImpressao.Text) + CLng(EcIntervalo.Text) - 1
            End If
        End If
    End If
End Sub

Private Sub EcPrefixo_Change()
    If Len(EcPrefixo) > 2 Then
        EcPrefixo = Mid(EcPrefixo, 1, 2)
    End If
End Sub



''''''''''''''''''''''''''''''''''''''''''''
' 'Load' do ecra '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub Form_Load()
    EventoLoad
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' 'Activate' do ecra '''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub Form_Activate()
    EventoActivate
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' 'Unload' do ecra '''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Procede as inicializacoes do ecra ''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Inicializacoes()

    ' Propriedades do ecra
    Me.caption = " Salas de Colheita"
    Me.left = 540
    Me.top = 450
    Me.Width = 11065
    Me.Height = 7290
            
    ' Configuracao das variaveis de bd
    NomeTabela = "sl_cod_salas"
    Set CampoDeFocus = EcCodigo
    NumCampos = 32
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(CI_SEQSALA) = "seq_sala"
    CamposBD(CI_CODSALA) = "cod_sala"
    CamposBD(CI_DESCRSALA) = "descr_sala"
    CamposBD(CI_USERCRI) = "user_cri"
    CamposBD(CI_DTCRI) = "dt_cri"
    CamposBD(CI_USERACT) = "user_act"
    CamposBD(CI_DTACT) = "dt_act"
    CamposBD(CI_FLGCRIANCAS) = "flg_criancas"
    CamposBD(CI_ANALISES) = "analises"
    CamposBD(CI_FLGCOLHEITA) = "flg_colheita"
    CamposBD(CI_ABREV) = "abrev"
    CamposBD(CI_SEQIMPRESSAO) = "seq_impressao"
    CamposBD(CI_FLGIMPRESSAO) = "flg_impressao"
    CamposBD(CI_PREFIXO) = "prefixo_impressao"
    CamposBD(CI_SEQRECIBOS) = "seq_recibos"
    CamposBD(CI_FLGRECIBOS) = "flg_seq_recibos"
    CamposBD(CI_Email) = "email"
    CamposBD(CI_MORADA) = "morada"
    CamposBD(CI_CodPostal) = "cod_postal"
    CamposBD(CI_Localidade) = "localidade"
    CamposBD(CI_margem) = "margem"
    CamposBD(CI_MAXIMPRESSAO) = "max_impressao"
    CamposBD(CI_MINIMPRESSAO) = "min_impressao"
    CamposBD(CI_INTERVALO) = "intervalo_impressao"
    CamposBD(CI_INIBE_IMP_ANA) = "flg_inibe_imp_analises"
    CamposBD(CI_COD_EXT) = "cod_ext"
    CamposBD(CI_GERA_REQ) = "flg_gera_auto"
    CamposBD(CI_Num_copias) = "num_copias"
    CamposBD(CI_num_impr) = "num_impr"
    CamposBD(CI_flg_interno) = "flg_interna"
    CamposBD(CI_flg_invisivel) = "flg_invisivel"
    CamposBD(CI_local_adse) = "local_adse"
    
    ' Campos do Ecr�
    Set CamposEc(CI_SEQSALA) = EcCodSequencial
    Set CamposEc(CI_CODSALA) = EcCodigo
    Set CamposEc(CI_DESCRSALA) = EcDescricao
    Set CamposEc(CI_USERCRI) = EcUtilizadorCriacao
    Set CamposEc(CI_DTCRI) = EcDataCriacao
    Set CamposEc(CI_USERACT) = EcUtilizadorAlteracao
    Set CamposEc(CI_DTACT) = EcDataAlteracao
    Set CamposEc(CI_FLGCRIANCAS) = EcFlgCriancas
    Set CamposEc(CI_ANALISES) = EcAna
    Set CamposEc(CI_FLGCOLHEITA) = EcFlgColheita
    Set CamposEc(CI_ABREV) = EcAbrev
    Set CamposEc(CI_SEQIMPRESSAO) = EcSeqImpressao
    Set CamposEc(CI_FLGIMPRESSAO) = EcFlgImpressao
    Set CamposEc(CI_PREFIXO) = EcPrefixo
    Set CamposEc(CI_SEQRECIBOS) = EcSeqRecibos
    Set CamposEc(CI_FLGRECIBOS) = CkSeqRecibos
    Set CamposEc(CI_Email) = EcEmail
    Set CamposEc(CI_MORADA) = EcMorada
    'soliveira lacto correccao
    Set CamposEc(CI_Localidade) = EcDescrPostal
    Set CamposEc(CI_CodPostal) = EcCodPostalAux
    Set CamposEc(CI_margem) = EcMargem
    Set CamposEc(CI_MAXIMPRESSAO) = EcMaxImpressao
    Set CamposEc(CI_MINIMPRESSAO) = EcMinImpressao
    Set CamposEc(CI_INTERVALO) = EcIntervalo
    Set CamposEc(CI_INIBE_IMP_ANA) = CkInibeImpAna
    Set CamposEc(CI_COD_EXT) = EcCodExt
    Set CamposEc(CI_GERA_REQ) = CkGeraNumReq
    Set CamposEc(CI_Num_copias) = EcNumCopias
    Set CamposEc(CI_num_impr) = EcNumImpr
    Set CamposEc(CI_flg_interno) = CkInterno
    Set CamposEc(CI_flg_invisivel) = CkInvisivel
    Set CamposEc(CI_local_adse) = EcLocalADSE
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(CI_CODSALA) = "C�digo"
    TextoCamposObrigatorios(CI_ABREV) = "Abreviatura"
    TextoCamposObrigatorios(CI_DESCRSALA) = "Descri��o da Sala"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_sala"
    Set ChaveEc = EcCodigo
    
    ' Definicoes da lista
    CamposBDparaListBox = Array("cod_sala", "descr_sala", "min_impressao", "max_impressao", "intervalo_impressao")
    NumEspacos = Array(7, 20, 10, 10, 10)
    FrameEtiq.Visible = False
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Efectua 'load' do ecra '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    estado = CS_ACTIVE
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    EcFlgCriancas.Visible = True
    EcFlgColheita.Visible = True
    EcFlgImpressao.Visible = True
    EcFlgImpressao.value = vbGrayed
    EcFlgCriancas.value = vbGrayed
    EcFlgColheita.value = vbGrayed
    CkSeqRecibos.value = vbGrayed
    CkInibeImpAna.value = vbGrayed
    CkGeraNumReq.value = vbGrayed
    PreencheValoresDefeito
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Efectua 'activate' do ecra '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_ToolbarEstadoN estado
    If (estado = CS_WAIT) Then: BL_Toolbar_BotaoEstado "Remover", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Preenche valores de defeito ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub PreencheValoresDefeito()
    Dim sSql As String
    Dim Tabela As New ADODB.recordset
    Dim i As Integer
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    totalTubos = 0
    ReDim estrutTubos(0)
    sSql = "SELECT * FROM sl_tubo ORDER BY descr_tubo "
    Tabela.CursorType = adOpenStatic
    Tabela.CursorLocation = adUseServer
    Tabela.Open sSql, gConexao
    If Tabela.RecordCount > 0 Then
        While Not Tabela.EOF
            totalTubos = totalTubos + 1
            ReDim Preserve estrutTubos(totalTubos)
            estrutTubos(totalTubos).cod_tubo = BL_HandleNull(Tabela!cod_tubo, "")
            estrutTubos(totalTubos).descR_tubo = BL_HandleNull(Tabela!descR_tubo, "")
            estrutTubos(totalTubos).num_copias = 0
            
            FGTubos.TextMatrix(totalTubos, 0) = estrutTubos(totalTubos).descR_tubo
            FGTubos.TextMatrix(totalTubos, 1) = estrutTubos(totalTubos).num_copias
            FGTubos.AddItem ""
            Tabela.MoveNext
        Wend
    End If
    Tabela.Close
    Set Tabela = Nothing
    CkInterno.value = vbGrayed
    CkInvisivel.value = vbGrayed
    
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Efectua 'unload' do ecra '''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN CS_INACTIVE
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormSalasColh = Nothing
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Limpa todos os campos do ecra ''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcFlgCriancas.value = vbGrayed
    EcFlgColheita.value = vbGrayed
    EcFlgImpressao.value = vbGrayed
    CkSeqRecibos.value = vbGrayed
    CkGeraNumReq.value = vbGrayed
    CkInterno.value = vbGrayed
    CkInvisivel.value = vbGrayed
    
    EcCodPostal = ""
    EcRuaPostal = ""
    EcDescrPostal = ""
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next
    FrameEtiq.Visible = False
    For i = 1 To totalTubos
        estrutTubos(i).num_copias = 0
        FGTubos.TextMatrix(i, 0) = estrutTubos(i).descR_tubo
        FGTubos.TextMatrix(i, 1) = estrutTubos(i).num_copias
    Next
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Define o tipo de campos do ecra ''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub DefTipoCampos()
    EcCodigo.Tag = adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    With FGTubos
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0

        .ColWidth(0) = 3850
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "Descri��o do Tubo"

        .ColWidth(1) = 1480
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 1) = "N�. Etiqueta(s)"

        .row = 1
        .Col = 1
        
        .CellBackColor = vbWhite
    End With
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Preenche os campos do ecra '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub PreencheCampos()
    
    Me.SetFocus
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        PreencheCodigoPostal
        MaxImp = BL_HandleNull(EcMaxImpressao, 0)
        CarregaLocaisSalas EcCodigo
        CarregaEtiquetas EcCodigo
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao limpar do ecra ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoLimpar()
    
    If estado = CS_WAIT Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao retroceder o estado do ecra '''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoEstadoAnterior()
    
    If estado = CS_WAIT Then
        estado = CS_ACTIVE
        BL_ToolbarEstadoN estado
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que valida todos os campos Ec '''''
''''''''''''''''''''''''''''''''''''''''''''
Private Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer
    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    ValidaCamposEc = True
End Function
    
''''''''''''''''''''''''''''''''''''''''''''
' Funcao que procura registos ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoProcurar()
      
    Dim SimNao As Integer
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, True)
    CriterioTabela = CriterioTabela & "  ORDER BY min_impressao ASC, cod_sala ASC"
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = CS_WAIT
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que passa para o registo anterior '
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que passa para o proximo registo ''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que insere um registo '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoInserir()
    
    Dim iRes As Integer
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
            GravaLocaisSalas EcCodigo
        End If
        BL_FimProcessamento Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Insere um registo na bd ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BD_Insert()
    Dim i As Integer
    Dim SQLQuery As String
    
    ManageFlags
    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_sala") + 1
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery

    SQLQuery = "DELETE FROM sl_Cod_salas_etiq WHERE cod_sala = " & EcCodigo
    BG_ExecutaQuery_ADO SQLQuery
    For i = 1 To totalTubos
        SQLQuery = "INSERT INTO sl_cod_salas_etiq (cod_sala, cod_tubo, num_copias) VALUES("
        SQLQuery = SQLQuery & EcCodigo & ", " & BL_TrataStringParaBD(estrutTubos(i).cod_tubo) & "," & estrutTubos(i).num_copias & ")"
        BG_ExecutaQuery_ADO SQLQuery
    Next
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que modifica um registo '''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoModificar()
    
    Dim iRes As Integer
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        'ValidaMaxImpressao
        If iRes = True Then
            GravaLocaisSalas EcCodigo
            BD_Update
        End If
        BL_FimProcessamento Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que actualiza um registo na bd ''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    
    ManageFlags
    gSQLError = 0
    condicao = ChaveBD & " = '" & rs(ChaveBD) & "'"
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    SQLQuery = "DELETE FROM sl_Cod_salas_etiq WHERE cod_sala = " & EcCodigo
    BG_ExecutaQuery_ADO SQLQuery
    For i = 1 To totalTubos
        SQLQuery = "INSERT INTO sl_cod_salas_etiq (cod_sala, cod_tubo, num_copias) VALUES("
        SQLQuery = SQLQuery & EcCodigo & ", " & BL_TrataStringParaBD(estrutTubos(i).cod_tubo) & "," & estrutTubos(i).num_copias & ")"
        BG_ExecutaQuery_ADO SQLQuery
    Next
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que remove um registo '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoRemover()
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer desactivar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A desactivar registo."
        CkInvisivel.value = vbChecked
        BD_Update
        BL_FimProcessamento Me
    End If
End Sub


''''''''''''''''''''''''''''''''''''''''''''
' Gere as flags existentes no ecra '''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub ManageFlags()
    
    ' EcFlgColheita
    If (EcFlgColheita.value = vbGrayed) Then: EcFlgColheita.value = vbUnchecked
    
    ' EcFlgCriancas
    If (EcFlgCriancas.value = vbGrayed) Then: EcFlgCriancas.value = vbUnchecked
    
        ' EcFlgImpressao
    If (EcFlgImpressao.value = vbGrayed) Then: EcFlgImpressao.value = vbUnchecked: EcSeqImpressao.Text = 0: Exit Sub
    If (EcFlgImpressao.value = vbChecked) Then: EcSeqImpressao.Text = IIf(EcSeqImpressao.Text = "", 1, EcSeqImpressao.Text): Exit Sub
    If (EcFlgImpressao.value = vbUnchecked) Then: EcSeqImpressao.Text = 0: Exit Sub

End Sub
Private Sub EcCodPostal_Change()
    
    EcCodPostal.Text = UCase(EcCodPostal.Text)

End Sub

Private Sub EcCodPostalCompleto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalCompleto.Text <> "" Then
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     seq_postal = " & EcCodPostalCompleto
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcCodPostal.SetFocus
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
            EcCodPostalAux.Text = Trim(rsCodigo!cod_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub



Private Sub EcRuaPostal_Validate(Cancel As Boolean)
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset
    
    EcRuaPostal.Text = UCase(EcRuaPostal.Text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.Text) & "-" & Trim(EcRuaPostal.Text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcRuaPostal = ""
            EcCodPostal = ""
            EcCodPostalAux = ""
        Else
            EcCodPostalAux.Text = Postal
            EcDescrPostal.Text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.Text = ""
        EcCodPostalAux = ""
    End If
    
End Sub





Private Sub BtPesqCodPostal_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" And EcCodPostal = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    If EcDescrPostal <> "" Then
        ChavesPesq(1) = "descr_postal"
        CamposEcran(1) = "descr_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_postal"
        CamposEcran(2) = "cod_postal"
        Tamanhos(2) = 2000
        Headers(2) = "C�digo"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        ChavesPesq(1) = "cod_postal"
        CamposEcran(1) = "cod_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Codigo"
        
        ChavesPesq(2) = "descr_postal"
        CamposEcran(2) = "descr_postal"
        Tamanhos(2) = 2000
        Headers(2) = "Descr��o"
    End If
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_postal"
    If EcDescrPostal <> "" And EcCodPostal = "" Then
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        CWhere = "UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    Else
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
        CWhere = CWhere & " AND UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    End If
    CampoPesquisa = "descr_postal"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, resultados(2), "-") - 1
            If EcDescrPostal <> "" Then
                If i = -1 Then
                    s1 = resultados(2)
                    s2 = ""
                Else
                    s1 = Mid(resultados(2), 1, i)
                    s2 = Mid(resultados(2), InStr(1, resultados(2), "-") + 1)
                End If
                EcCodPostal.Text = s1
                EcRuaPostal.Text = s2
                EcCodPostalAux.Text = Trim(resultados(2))
                EcDescrPostal.Text = resultados(1)
                EcDescrPostal.SetFocus
            ElseIf EcDescrPostal = "" Then
                If i = -1 Then
                    s1 = resultados(1)
                    s2 = ""
                Else
                    s1 = Mid(resultados(1), 1, i)
                    s2 = Mid(resultados(1), InStr(1, resultados(1), "-") + 1)
                End If
                EcCodPostal.Text = s1
                EcRuaPostal.Text = s2
                EcCodPostalAux.Text = Trim(resultados(1))
                EcDescrPostal.Text = resultados(2)
                EcDescrPostal.SetFocus
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If

    
End Sub



Sub PreencheCodigoPostal()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostalAux)
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
            EcCodPostalAux.Text = ""
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub


Sub ValidaMaxImpressao()
    Dim RsVf As ADODB.recordset
    Dim sql As String
    
    If EcMaxImpressao.Text <> "" And gNReqPreImpressa = 1 Then
        Set RsVf = New ADODB.recordset
        RsVf.CursorLocation = adUseServer
        RsVf.CursorType = adOpenStatic
        RsVf.Source = "select n_requis from sl_config "
        RsVf.Open , gConexao
        If CLng(EcMaxImpressao) > RsVf!n_requis Then
            sql = "update sl_config set n_requis = " & EcMaxImpressao
            BG_ExecutaQuery_ADO sql
            BG_Trata_BDErro
        End If
        RsVf.Close
        Set RsVf = Nothing
    End If
End Sub

' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------
Public Sub CarregaLocaisSalas(cod_sala As String)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ass_salas_locais WHERE cod_sala = " & cod_sala
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    EcLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar locais: " & sSql & " " & Err.Description, Me.Name, "CarregaLocaisSalas", False
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' CARREGA ETIQUETAS

' ------------------------------------------------------------------------------------------------
Public Sub CarregaEtiquetas(cod_sala As String)
    Dim sSql As String
    Dim rsSala As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_cod_salas_etiq WHERE cod_sala = " & cod_sala
    Set rsSala = BG_ExecutaSELECT(sSql)
    If rsSala.RecordCount > 0 Then
        While Not rsSala.EOF
            For i = 1 To totalTubos
                If estrutTubos(i).cod_tubo = BL_HandleNull(rsSala!cod_tubo, "") Then
                    estrutTubos(i).num_copias = BL_HandleNull(rsSala!num_copias, 0)
                    FGTubos.TextMatrix(i, 1) = estrutTubos(i).num_copias
                End If
            Next
            rsSala.MoveNext
        Wend
    End If
    rsSala.Close
    Set rsSala = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar etiquetas: " & sSql & " " & Err.Description, Me.Name, "CarregaEtiquetas", False
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------

Public Sub GravaLocaisSalas(cod_sala As String)
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_ass_salas_locais WHERE cod_sala = " & cod_sala
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            sSql = "INSERT into sl_ass_salas_locais (cod_sala, cod_local) VALUES("
            sSql = sSql & cod_sala & ","
            sSql = sSql & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Gravar locais: " & sSql & " " & Err.Description, Me.Name, "GravaLocaisSalas", False
    Exit Sub
    Resume Next
End Sub


Private Sub FGTubos_DblClick()
    If FGTubos.Col = 1 And FGTubos.row <= totalTubos Then
        EcAux.left = FGTubos.CellLeft + FGTubos.left
        EcAux.top = FGTubos.CellTop + FGTubos.top
        EcAux.Width = FGTubos.CellWidth + 10
        EcAux = estrutTubos(FGTubos.row).num_copias
        EcAux.Visible = True
        EcAux.SetFocus
        Sendkeys ("{END}")
    End If
End Sub

Private Sub FGTubos_KeyDown(KeyCode As Integer, Shift As Integer)
    If FGTubos.row <= totalTubos And FGTubos.Col = 1 And KeyCode = vbKeyReturn Then
        EcAux.left = FGTubos.CellLeft + FGTubos.left
        EcAux.top = FGTubos.CellTop + FGTubos.top
        EcAux.Width = FGTubos.CellWidth + 10
        EcAux = estrutTubos(FGTubos.row).num_copias
        EcAux.Visible = True
        EcAux.SetFocus
        Sendkeys ("{END}")
    End If
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcAux = BL_HandleNull(EcAux, 0)
        estrutTubos(FGTubos.row).num_copias = EcAux
        FGTubos.TextMatrix(FGTubos.row, 1) = EcAux
        FGTubos.SetFocus
    End If
    
    If KeyCode = vbKeyEscape Then
        FGTubos.SetFocus
    End If
        
End Sub

Private Sub EcAux_LostFocus()
    EcAux.Visible = False
    FGTubos.SetFocus
End Sub


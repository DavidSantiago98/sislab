VERSION 5.00
Begin VB.Form FormListaTrabalhoBeppSISLAB 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormListaTrabalhoBeppSISLAB"
   ClientHeight    =   5355
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   Icon            =   "FormListaTrabalhoSISLAB.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5355
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcDataFinal 
      Height          =   285
      Left            =   3960
      TabIndex        =   4
      Top             =   240
      Width           =   975
   End
   Begin VB.TextBox EcDataInicial 
      Height          =   285
      Left            =   1320
      TabIndex        =   3
      Top             =   240
      Width           =   975
   End
   Begin VB.ListBox LstRequisoes 
      Height          =   2760
      Left            =   360
      Style           =   1  'Checkbox
      TabIndex        =   2
      Top             =   840
      Width           =   1935
   End
   Begin VB.ListBox LstAnalises 
      Height          =   2760
      Left            =   3000
      Style           =   1  'Checkbox
      TabIndex        =   1
      Top             =   840
      Width           =   1935
   End
   Begin VB.CommandButton BtConstroiFicheiro 
      Caption         =   "Constroi Lista"
      Height          =   495
      Left            =   1800
      TabIndex        =   0
      Top             =   3960
      Width           =   1575
   End
   Begin VB.Label LaDataFinal 
      Caption         =   "Data Final"
      Height          =   255
      Left            =   3000
      TabIndex        =   6
      Top             =   240
      Width           =   975
   End
   Begin VB.Label LaDataInicial 
      Caption         =   "Data Inicial"
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   240
      Width           =   975
   End
   Begin VB.Image Image1 
      Height          =   240
      Index           =   0
      Left            =   360
      Picture         =   "FormListaTrabalhoSISLAB.frx":000C
      ToolTipText     =   "Excluir Requis�o da Lista"
      Top             =   600
      Width           =   240
   End
End
Attribute VB_Name = "FormListaTrabalhoBeppSISLAB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim CriterioBase As String 'usado para os botoes de ordenacao
Dim CriterioAnterior As String 'usado para os botoes de ordenacao

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Private Type codAnalise
    cod_analise As String
    descr_analise As String
End Type

Dim EstrutAnalise() As codAnalise
Dim TamEstrutAnalise As Long

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public obTabela As ADODB.recordset

Dim nRequis As Long

Dim Flg_DataInicialValida As Boolean
Dim Flg_DataFinalValida As Boolean

Private Sub BtConstroiFicheiro_Click()
    ConstroiFicheiroTrabalho
End Sub
Sub ConstroiFicheiroTrabalho()
    Dim i As Integer, j As Integer
    Dim Requis As String
    Dim cod_analise As String
    Dim linha As String
    Dim f As Integer
    Dim flagRequis As Integer
    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    On Error GoTo TrataErro
    
    BG_Mensagem mediMsgStatus, "Em Processamento...", mediMsgBeep + mediMsgPermanece
    BL_MudaCursorRato vbHourglass, Me
    
    f = FreeFile
    flagRequis = 0
    
    If Dir(gDestinoFicheiroBeep) <> "" Then
        Kill gDestinoFicheiroBeep
    End If
    
    Open gDestinoFicheiroBeep For Append As #f
    linha = "H;" & Bg_DaData_ADO & ";" & Bg_DaHora_ADO & ":00"
    Print #f, linha
    
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    
    For i = 0 To nRequis - 1
        If LstRequisoes.Selected(i) = False Then
            flagRequis = 0
            For j = 0 To LstAnalises.ListCount - 1
                If LstAnalises.Selected(j) = True Then
                    If VerificaAnalise(LstAnalises.ItemData(j), LstRequisoes.ItemData(i)) Then
                        If flagRequis = 0 Then
                            Requis = LstRequisoes.ItemData(i)
                            linha = "P;" & Requis
                            Print #f, linha
                            flagRequis = 1
                        End If
                          
                        'cod_analise = LstAnalises.ItemData(j)
                        sql = "SELECT seq_ana_apar, ana_apar FROM gc_ana_apar WHERE seq_apar=5 AND cod_simpl in (" & _
                            "SELECT cod_ana_s cod_simpl FROM sl_ana_s WHERE seq_ana_s=" & LstAnalises.ItemData(j) & ")"
                        rsAna.Open sql, gConexao
                        If rsAna.RecordCount > 0 Then
                            cod_analise = BL_HandleNull(rsAna!ana_apar, 0)
                        Else
                            cod_analise = 0
                        End If
                        rsAna.Close
                        
                        linha = "O;" & cod_analise & ";1;1;1;1.0"
                    
                        Print #f, linha
                    End If
                End If
            Next j
        End If
    Next i
    
    BtConstroiFicheiro.Enabled = False
    
    Print #f, "L;"
    Close #f

    If Not rsAna Is Nothing Then
        Set rsAna = Nothing
    End If
    
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    BL_MudaCursorRato vbNormal, Me
    
    Exit Sub
    
TrataErro:
    
    BG_Mensagem mediMsgBox, "Erro ao construir ficheiro" & Err.Description, vbExclamation, App.ProductName
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    BL_MudaCursorRato vbNormal, Me
    
End Sub
Function VerificaAnalise(codAnalise As Integer, NumRequis As Long) As Boolean
    Dim sql As String
    Dim rs As ADODB.recordset
    
    sql = "SELECT n_req FROM sl_marcacoes WHERE n_req=" & NumRequis & " AND cod_ana_s='" & DevolveCodAnaS(CStr(codAnalise)) & "' ORDER BY n_req "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    
    
    If rs.RecordCount > 0 Then
        VerificaAnalise = True
    Else
        VerificaAnalise = False
    End If
    
    rs.Close
    Set rs = Nothing
    
End Function
Function DevolveCodAnaS(seq_ana_s As String) As String
    Dim sql As String
    Dim rs As ADODB.recordset
    
    sql = "SELECT cod_ana_s FROM sl_ana_s WHERE seq_ana_s=" & seq_ana_s
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    
    If rs.RecordCount > 0 Then
        DevolveCodAnaS = rs!cod_ana_s
    End If
    
    rs.Close
    Set rs = Nothing
    
End Function
Private Sub EcDataFinal_Validate(Cancel As Boolean)
    
    If EcDataFinal.Text = "" Then
        EcDataFinal.Text = Bg_DaData_ADO
    End If
    
    Flg_DataFinalValida = False
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataFinal)
    Flg_DataFinalValida = Not Cancel
    If EcDataFinal = "" Then
        Flg_DataFinalValida = False
        
    ElseIf Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDataInicial_Validate(Cancel As Boolean)
    
    Flg_DataInicialValida = False
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInicial)
    Flg_DataInicialValida = Not Cancel
    If EcDataInicial = "" Then
        Flg_DataInicialValida = False
    ElseIf Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

    
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    'Guarda a pasta onde copiar o ficheiro de trabalho do beep
    gDestinoFicheiroBeep = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DESTINO_FICHEIRO_BEEP")
    Me.caption = "Lista Trabalho Aparelho"
    Me.left = 1500
    Me.top = 1250
    Me.Width = 5520
    Me.Height = 5325 ' Normal
    'Me.Height = 7500 ' Campos Extras
    
    Set CampoDeFocus = EcDataInicial
    
    Flg_DataInicialValida = False
    Flg_DataFinalValida = False
        
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes
    
    
    PreencheValoresDefeito
    
    BtConstroiFicheiro.Enabled = False
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not obTabela Is Nothing Then
        obTabela.Close
        Set obTabela = Nothing
    End If
    
    Set FormListaTrabalhoBeppSISLAB = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcDataInicial.Text = ""
    EcDataFinal.Text = ""
    
    Flg_DataInicialValida = False
    Flg_DataFinalValida = False
    
    LstAnalises.Clear
    LstRequisoes.Clear
    
    BtConstroiFicheiro.Enabled = False
    
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    CampoDeFocus.SetFocus

End Sub

Sub DefTipoCampos()

    EcDataInicial.MaxLength = 10
    EcDataFinal.MaxLength = 10

    EcDataInicial.Tag = adDate
    EcDataFinal.Tag = adDate
    
End Sub

Sub PreencheCampos()
    'nada
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    'nada
End Sub
Sub PreencheValoresDefeito()
    
    'BG_PreencheComboBD_ADO "select codigo,descricao from sb_tbf_aparelhos order by codigo ASC", "codigo", "descricao", EcAparelho
    
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    Dim i As Integer
    Dim sql As String
    Dim rs As ADODB.recordset
    
    
    EcDataInicial_Validate False
    
    If Not Flg_DataInicialValida Then
        EcDataInicial.SetFocus
        Exit Sub
    End If
    
    EcDataFinal_Validate False
    If Not Flg_DataInicialValida Then
        EcDataFinal.SetFocus
        Exit Sub
    End If
    
        
    If EcDataFinal.Text < EcDataInicial.Text Then
        EcDataFinal.Text = ""
        EcDataInicial.SetFocus
        Exit Sub
    End If
        
    BG_Mensagem mediMsgStatus, "Em Processamento ...", mediMsgBeep + mediMsgPermanece
    
    'Colheitas Nao Validadas
    'BG_PreencheComboBD_ADO "select cod_colheita from sb_colheitas where estado_lab2 is null and data_colheita >='" & EcDataInicial.text & "' and data_colheita <= '" & EcDataFinal.text & "'", "cod_colheita", "cod_colheita", LstRequisoes
    'PREENCHE A LISTA
    'sql = "select distinct(n_req) from sl_marcacoes where dt_chega between '" & EcDataInicial.Text & "' AND '" & EcDataFinal.Text & "'"
    sql = "select distinct(n_req) from sl_requis where dt_chega between '" & EcDataInicial.Text & "' AND '" & EcDataFinal.Text & "' ORDER BY n_Req "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    
    nRequis = 0
    LstRequisoes.Clear
    While Not rs.EOF
        LstRequisoes.AddItem rs!n_req
        LstRequisoes.ItemData(nRequis) = CLng(rs!n_req)
        nRequis = nRequis + 1
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    
    If LstRequisoes.ListCount > 0 Then
        BG_PreencheComboBD_ADO "select seq_ana_s,descr_ana_s from sl_ana_s where cod_ana_s in ( select cod_simpl from gc_ana_apar where seq_apar=5)", "seq_ana_s", "descr_ana_s", LstAnalises, mediAscComboCodigo
    End If
    
    If LstRequisoes.ListCount > 0 Then
        BtConstroiFicheiro.Enabled = True
    End If
    
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    
End Sub
Sub FuncaoAnterior()
    'nada
End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub BD_Insert()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub BD_Update()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub

Sub BD_Delete()
    'nada
End Sub

Sub Funcao_DataActual()
    If Me.ActiveControl.Name = "EcDataInicial" Then
        BL_PreencheData EcDataInicial, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataFinal" Then
        BL_PreencheData EcDataFinal, Me.ActiveControl
    End If
End Sub

Sub FuncaoImprimir()

End Sub

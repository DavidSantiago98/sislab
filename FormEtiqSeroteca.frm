VERSION 5.00
Begin VB.Form FormEtiqSeroteca 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEtqSeroteca"
   ClientHeight    =   5070
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3975
   Icon            =   "FormEtiqSeroteca.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5070
   ScaleWidth      =   3975
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   8
      Top             =   4560
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton BtEtiq 
      Height          =   480
      Left            =   3330
      Picture         =   "FormEtiqSeroteca.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Imprimir Etiquetas Seroteca"
      Top             =   280
      Width           =   495
   End
   Begin VB.Frame Frame2 
      Height          =   2775
      Left            =   120
      TabIndex        =   3
      Top             =   960
      Width           =   3735
      Begin VB.ListBox EcLista 
         Height          =   2400
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   3255
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3735
      Begin VB.TextBox EcQtd 
         Height          =   285
         Left            =   2520
         MaxLength       =   2
         TabIndex        =   2
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcNumReq 
         Height          =   285
         Left            =   960
         MaxLength       =   9
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "Qtd"
         Height          =   255
         Left            =   2160
         TabIndex        =   5
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label5 
         Caption         =   "Requisi��o"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Label Label32 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   4560
      Visible         =   0   'False
      Width           =   1095
   End
End
Attribute VB_Name = "FormEtiqSeroteca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 02/07/2006
' T�cnico

' Vari�veis Globais para este Form.

Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Public CampoActivo As Object
Dim NomeTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

Private Sub BtEtiq_Click()

    Call ImprimeEtiq
    
End Sub

Private Sub EcLista_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    If KeyCode = 46 Then
        If EcLista.SelCount > 0 Then
            EcLista.RemoveItem (EcLista.ListIndex)
        End If
    End If
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcNumReq_Validate (False)
    End If
End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)
    If Cancel = True Then
        EcNumReq.Text = ""
        EcNumReq.SetFocus
        Exit Sub
    End If
    If Len(EcNumReq.Text) >= 7 Then
        EcNumReq.Text = Right(EcNumReq.Text, 7)
    End If
    If VerificaReq = True Then
        If EcQtd.Text <> "" Then
            PreencheLista
        Else
            EcQtd.SetFocus
        End If
    End If
    

End Sub

Private Sub EcQtd_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Then
        EcQtd_Validate (False)
    End If
End Sub

Private Sub EcQtd_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcQtd)
    If Cancel = True Then
        Exit Sub
    End If
    If EcQtd.Text <> "" Then
        If EcNumReq.Text <> "" Then
            PreencheLista
        End If
    Else
        EcQtd.SetFocus
    End If
End Sub

Sub Form_Load()
    
    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Etiquetas Seroteca"
    Me.left = 440
    Me.top = 20
    Me.Width = 4065
    Me.Height = 4305 ' Normal
    
    EcNumReq.Tag = 5
    EcQtd.Tag = adInteger
    
    EcPrinterEtiq.Text = BL_SelImpressora("Seroteca.ini")
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

    EcNumReq.SetFocus
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormEtiqSeroteca = Nothing
End Sub

Sub FuncaoLimpar()

End Sub

Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()
    
End Sub

Sub FuncaoInserir()
 
End Sub

Sub FuncaoModificar()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub ImprimeEtiq()
    Dim i As Integer
    Dim j As Integer
    Dim NReq As Long
    Dim qtd As Integer
    Dim EtqCrystal As String
    On Error GoTo ErrorHandler
    
    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
                
    If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                
    If EtqCrystal = "1" Then
    Else
        For i = 0 To EcLista.ListCount - 1
            NReq = Trim(left(EcLista.List(i), InStr(1, EcLista.List(i), " ") - 1))
            qtd = Trim(Right(EcLista.List(i), 2))
            
            For j = 1 To qtd
            
                If Not LerEtiqInI Then
                    MsgBox "Ficheiro de inicializa��o de etiquetas seroteca ausente!"
                    Exit Sub
                End If
                If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                    MsgBox "Imposs�vel abrir impressora etiquetas seroteca"
                    Exit Sub
                End If
                
                Call EtiqPrint(NReq)
                
                EtiqClosePrinter
            Next j
        Next i
        EcLista.Clear
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : ImprimeEtiq (FormEtiqSeroteca) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub


Private Function LerEtiqInI() As Boolean
    Dim aux As String
    
    On Error GoTo Erro
    
    Dim s As String
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Seroteca.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Seroteca.ini"
    End If
    

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While aux <> "P1"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function

Erro:
    
End Function
Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True
    

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function

Private Function EtiqPrint(ByVal n_req As String) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", n_req)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function


Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function

Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function

Private Sub PreencheLista()
    EcLista.AddItem EcNumReq.Text & Space(15) & EcQtd.Text
    EcNumReq.Text = ""
    EcQtd.Text = ""
    EcNumReq.SetFocus
End Sub

Private Function VerificaReq() As Boolean
    Dim sql As String
    Dim rsReq As ADODB.recordset
    
    If EcNumReq.Text <> "" Then
        Set rsReq = New ADODB.recordset
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        
        sql = "select n_req from sl_requis where n_req = " & EcNumReq.Text
        'NELSONPSILVA Glintt-HS-18011 09.02.2018
        If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
            sql = "select a.n_req from sl_requis a, slv_identif b where a.n_req = " & EcNumReq.Text & " and a.seq_utente = b.seq_utente"
        End If
        '
        rsReq.Open sql, gConexao
        If rsReq.RecordCount > 0 Then
            VerificaReq = True
        Else
            VerificaReq = False
            BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbInformation
            EcNumReq.Text = ""
            EcNumReq.SetFocus
        End If
    End If
End Function

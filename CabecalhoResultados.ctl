VERSION 5.00
Begin VB.UserControl CabecalhoResultados 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFE0C7&
   ClientHeight    =   2790
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   14760
   Picture         =   "CabecalhoResultados.ctx":0000
   ScaleHeight     =   2790
   ScaleWidth      =   14760
   Begin VB.PictureBox PicVe 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFE0C7&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   13920
      Picture         =   "CabecalhoResultados.ctx":0CCA
      ScaleHeight     =   375
      ScaleWidth      =   375
      TabIndex        =   28
      ToolTipText     =   "Utente Com Vig. Epid. Positiva"
      Top             =   360
      Width           =   375
   End
   Begin VB.PictureBox PicSeroteca 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFE0C7&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   13920
      Picture         =   "CabecalhoResultados.ctx":1434
      ScaleHeight     =   375
      ScaleWidth      =   375
      TabIndex        =   27
      ToolTipText     =   "Requisi��o disponivel na Seroteca"
      Top             =   45
      Width           =   375
   End
   Begin VB.ListBox EcReqAnteriores 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFE0C7&
      Height          =   1005
      Left            =   9960
      TabIndex        =   15
      Top             =   1320
      Width           =   4575
   End
   Begin VB.PictureBox PictureFem 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   3480
      Picture         =   "CabecalhoResultados.ctx":1B9E
      ScaleHeight     =   375
      ScaleWidth      =   375
      TabIndex        =   1
      Top             =   330
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.PictureBox PictureMas 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   3480
      Picture         =   "CabecalhoResultados.ctx":2308
      ScaleHeight     =   375
      ScaleWidth      =   375
      TabIndex        =   0
      Top             =   330
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Image ImgHipo 
      Height          =   360
      Left            =   14400
      Picture         =   "CabecalhoResultados.ctx":2A72
      ToolTipText     =   "Requisi��o de Hipocoagulado"
      Top             =   45
      Width           =   360
   End
   Begin VB.Label LbSexo 
      BackColor       =   &H00FFE0C7&
      Height          =   255
      Left            =   0
      TabIndex        =   26
      Top             =   4320
      Width           =   1335
   End
   Begin VB.Line Line1 
      Index           =   0
      X1              =   1080
      X2              =   19320
      Y1              =   740
      Y2              =   740
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Produtos"
      Height          =   255
      Index           =   6
      Left            =   5040
      TabIndex        =   25
      Top             =   1800
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "eResults"
      Height          =   255
      Index           =   5
      Left            =   5040
      TabIndex        =   24
      Top             =   1560
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Assinatura"
      Height          =   255
      Index           =   4
      Left            =   5040
      TabIndex        =   23
      Top             =   1320
      Width           =   855
   End
   Begin VB.Label LbRaca 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Ra�a"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   1920
      TabIndex        =   22
      ToolTipText     =   "Epis�dio"
      Top             =   2040
      Width           =   435
   End
   Begin VB.Label LbGenero 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Genero"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   1920
      TabIndex        =   21
      ToolTipText     =   "Epis�dio"
      Top             =   1800
      Width           =   615
   End
   Begin VB.Label LbRaca2 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Ra�a"
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   2040
      Width           =   1335
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Esp�cie"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   19
      Top             =   1800
      Width           =   1335
   End
   Begin VB.Label LbEpisodio 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "EpisodioReq"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   1920
      TabIndex        =   18
      ToolTipText     =   "Epis�dio"
      Top             =   1560
      Width           =   1080
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Epis�dio"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   17
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Label EcSeqUtente 
      BackColor       =   &H00FFE0C7&
      Caption         =   "EcSeqUtente"
      Height          =   255
      Left            =   960
      TabIndex        =   16
      Top             =   4920
      Width           =   375
   End
   Begin VB.Image ImgColaps 
      Height          =   240
      Left            =   14400
      Picture         =   "CabecalhoResultados.ctx":31DC
      Top             =   480
      Width           =   240
   End
   Begin VB.Image ImgExapande 
      Height          =   240
      Left            =   14400
      Picture         =   "CabecalhoResultados.ctx":3566
      Top             =   480
      Width           =   240
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Data Nascimento"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      Height          =   1335
      Index           =   2
      Left            =   0
      Shape           =   4  'Rounded Rectangle
      Top             =   1200
      Width           =   4815
   End
   Begin VB.Shape Shape1 
      Height          =   1335
      Index           =   1
      Left            =   4920
      Shape           =   4  'Rounded Rectangle
      Top             =   1200
      Width           =   4815
   End
   Begin VB.Shape Shape1 
      Height          =   1335
      Index           =   0
      Left            =   9840
      Shape           =   4  'Rounded Rectangle
      Top             =   1200
      Width           =   4815
   End
   Begin VB.Image ImgComTec 
      Height          =   240
      Left            =   1080
      Picture         =   "CabecalhoResultados.ctx":38F0
      ToolTipText     =   "Utente com Coment�rio T�cnico"
      Top             =   435
      Width           =   240
   End
   Begin VB.Image ImgComFinal 
      Height          =   240
      Left            =   1080
      Picture         =   "CabecalhoResultados.ctx":3C7A
      ToolTipText     =   "Requisi��o com Coment�rio Final"
      Top             =   0
      Width           =   240
   End
   Begin VB.Label LbeResults 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "eResults"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   6120
      TabIndex        =   12
      ToolTipText     =   "Estado eResults"
      Top             =   1560
      Width           =   750
   End
   Begin VB.Label LbTipoUrg 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Triagem"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   9600
      TabIndex        =   11
      ToolTipText     =   "Triagem"
      Top             =   720
      Width           =   690
   End
   Begin VB.Label LbAssinatura 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Assinatura"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   6120
      TabIndex        =   10
      ToolTipText     =   "Estado Assinatura"
      Top             =   1320
      Width           =   885
   End
   Begin VB.Label LbIdade 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Idade"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   1080
      TabIndex        =   9
      ToolTipText     =   "Idade"
      Top             =   720
      Width           =   465
   End
   Begin VB.Label LbChega 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Chegada"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   2280
      TabIndex        =   8
      ToolTipText     =   "Data de Chegada"
      Top             =   720
      Width           =   765
   End
   Begin VB.Label LbProveniencia 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Proveni�ncia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   240
      Left            =   3960
      TabIndex        =   7
      ToolTipText     =   "Proveni�ncia"
      Top             =   720
      Width           =   5610
   End
   Begin VB.Label LbEstadoReq 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "EstadoReq"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   345
      Left            =   3960
      TabIndex        =   6
      ToolTipText     =   "Estado da Requisi��o"
      Top             =   0
      Width           =   1005
   End
   Begin VB.Label LbRequisicao 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "1234567"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1320
      TabIndex        =   5
      ToolTipText     =   "N�mero Requisi��o"
      Top             =   -45
      Width           =   1155
   End
   Begin VB.Image Image1 
      Height          =   1065
      Left            =   0
      Picture         =   "CabecalhoResultados.ctx":4004
      Top             =   0
      Width           =   975
   End
   Begin VB.Label LbDataNAsc 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Data Nasc"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Left            =   1920
      TabIndex        =   3
      ToolTipText     =   "Data Nascimento"
      Top             =   1320
      Width           =   870
   End
   Begin VB.Label LbUtente 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   270
      Left            =   1350
      TabIndex        =   2
      ToolTipText     =   "Utente"
      Top             =   420
      Width           =   675
   End
   Begin VB.Label LbNome 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Nome"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   4
      Top             =   360
      Width           =   6840
      WordWrap        =   -1  'True
   End
   Begin VB.Label LbProdutos 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFE0C7&
      Caption         =   "Produtos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   585
      Left            =   6120
      TabIndex        =   13
      ToolTipText     =   "Produtos Associados"
      Top             =   1800
      Width           =   3495
   End
End
Attribute VB_Name = "CabecalhoResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public Sub ProcuraDados(n_req As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsUtente As New ADODB.recordset
    RsUtente.CursorLocation = adUseServer
    RsUtente.CursorType = adOpenStatic
    sSql = ConstroiQuery(n_req)
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsUtente.Open sSql, gConexao
    If RsUtente.RecordCount = 1 Then
        ActualizaDados n_req, BL_HandleNull(RsUtente!Utente, ""), BL_HandleNull(RsUtente!t_utente, ""), BL_HandleNull(RsUtente!nome_ute, ""), _
                       BL_HandleNull(RsUtente!estado_req, ""), BL_HandleNull(RsUtente!n_epis, ""), BL_HandleNull(RsUtente!t_sit, ""), _
                       BL_HandleNull(RsUtente!descr_proven, ""), BL_HandleNull(RsUtente!tipo_urgencia, ""), BL_HandleNull(RsUtente!dt_chega, ""), _
                       BL_HandleNull(RsUtente!hr_chega, ""), BL_HandleNull(RsUtente!dt_nasc_ute, ""), BL_HandleNull(RsUtente!sexo_ute, ""), _
                       BL_HandleNull(RsUtente!user_fecho, 0), BL_HandleNull(RsUtente!user_assinatura, 0), BL_HandleNull(RsUtente!seq_utente, 0), _
                       BL_HandleNull(RsUtente!descr_genero, ""), BL_HandleNull(RsUtente!descr_raca, ""), BL_HandleNull(RsUtente!flg_hipo, 0), _
                       BL_HandleNull(RsUtente!flg_ve, mediNao), BL_HandleNull(RsUtente!flg_seroteca, mediNao)
                       
    End If
    RsUtente.Close
    Set RsUtente = Nothing
    
    PreencheProdutos n_req
    PreencheRequisAnteriores
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Cabe�alho do Utente: " & Err.Description, "CabecalhoResultados", "ProcuraDados", True
    Exit Sub
    Resume Next
End Sub
Private Function ConstroiQuery(n_req As String) As String
    Dim sSql As String
    sSql = "SELECT x1.nome_ute, x1.dt_nasc_ute, x1.utente, x1.t_utente, x1.sexo_ute, x2.n_req,x2.estado_Req,x2.n_epis, x2.t_sit,"
    sSql = sSql & " x2.cod_proven, x3.descr_proven,x2.tipo_urgencia,x2.dt_chega, x2.hr_chega, x2.user_fecho, x4.user_cri user_assinatura, "
    sSql = sSql & " x1.seq_utente, x3.descr_genero, x5.descr_raca, x2.flg_hipo, x1.flg_ve, x2.flg_seroteca "
    sSql = sSql & " FROM sl_identif x1 LEFT OUTER JOIN  sl_racas x5 ON x1.cod_raca = x5.cod_raca, sl_genero x3, "
    sSql = sSql & " sl_requis x2 LEFT OUTER JOIN sl_proven x3 ON x2.cod_proven = x3.cod_proven"
    sSql = sSql & " LEFT OUTER JOIN sl_req_assinatura x4 ON x2.n_Req = x4.n_req "
    sSql = sSql & " WHERE x1.seq_utente = x2.seq_utente AND x2.n_req = " & n_req
    sSql = sSql & " AND x1.cod_genero = x3.cod_genero "
    ConstroiQuery = sSql
    
End Function
Public Sub ActualizaDados(n_req As String, Utente As String, TUtente As String, nome As String, _
                          EstadoReq As String, episodio As String, tEpisodio As String, descr_proven As String, _
                          triagem As String, dt_chega As String, hr_chega As String, dt_nasc As String, _
                          Sexo As String, requisFechada As Integer, requisAssinada As Integer, seq_utente As Long, _
                          descr_genero As String, descr_raca As String, flg_hipo As Integer, flg_ve As Integer, _
                          flg_seroteca As Integer)
    On Error GoTo TrataErro
    LbSexo.caption = Sexo
    If Sexo = gT_Masculino Then
        PictureFem.Visible = False
        PictureMas.Visible = True
    ElseIf Sexo = gT_Feminino Then
        PictureFem.Visible = True
        PictureMas.Visible = False
    Else
        PictureFem.Visible = False
        PictureMas.Visible = False
    End If
    LbChega = dt_chega & " " & hr_chega
    LbRequisicao = n_req
    LbUtente = TUtente & "/" & Utente
    LbAssinatura = ""
    LbDataNasc = dt_nasc
    LbIdade = BG_CalculaIdade(CDate(dt_nasc))
    LbEpisodio = tEpisodio & "/" & episodio
    If LbEpisodio = "/" Then
        LbEpisodio = ""
    End If
    LbEstadoReq = BL_DevolveEstadoReq(EstadoReq)
    If Len(nome) > 35 Then
        LbNome.FontSize = 9
    Else
        LbNome.FontSize = 14
    End If
    LbNome = nome
    LbTipoUrg = triagem
    LbTipoUrg.BackColor = BL_DevolveCorTipoUrgencia(triagem)
    If LbTipoUrg = "0" Then
        LbTipoUrg.Visible = False
    Else
        LbTipoUrg.Visible = True
    End If
    LbProveniencia = descr_proven
    'LbeResults = ""
    If EstadoReq = gEstadoReqBloqueada Then
        ColocaCorEstado &HBDC8F4
    Else
        ColocaCorEstado BackColor
    End If
    
    LbAssinatura = ""
    If requisFechada = mediSim Then
        LbAssinatura = "Requisi��o Fechada."
    End If
    
    If requisAssinada = mediSim Then
        LbAssinatura = "Requisi��o J� Assinada"
    End If
    
    EcSeqUtente.caption = seq_utente
    LbRaca.caption = descr_raca
    LbGenero.caption = descr_genero
    PreencheProdutos n_req
    PreencheRequisAnteriores
    ImgExapande.Visible = True
    ImgColaps.Visible = False
    If flg_hipo = mediSim Then
        ImgHipo.Visible = True
    Else
        ImgHipo.Visible = False
    End If
    
    If flg_ve = mediSim Then
        PicVe.Visible = True
    Else
        PicVe.Visible = False
    End If
    
    If flg_seroteca = mediSim Then
        PicSeroteca.Visible = True
    Else
        PicSeroteca.Visible = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ActualizaDados: " & Err.Description, "CabecalhoResultsdos", "ActualizaDados", True
    Exit Sub
    Resume Next
End Sub

Public Sub LimpaCampos(cor As Long)
    ImgColaps_Click
    LbSexo.caption = ""
    LbNome.BackColor = cor
    LbRequisicao.BackColor = cor
    LbEstadoReq.BackColor = cor
    BackColor = cor
    PictureFem.Visible = False
    LbAssinatura.BackColor = cor
    LbChega.BackColor = cor
    LbDataNasc.BackColor = cor
    LbEpisodio.BackColor = cor
    LbeResults.BackColor = cor
    LbEstadoReq.BackColor = cor
    LbIdade.BackColor = cor
    LbNome.BackColor = cor
    LbProveniencia.BackColor = cor
    LbRequisicao.BackColor = cor
    LbTipoUrg.BackColor = cor
    LbUtente.BackColor = cor
    PictureMas.Visible = False
    LbRequisicao = ""
    LbUtente = ""
    LbAssinatura = ""
    LbDataNasc = ""
    LbIdade = ""
    LbEpisodio = ""
    LbEstadoReq = ""
    LbNome = ""
    LbTipoUrg = ""
    LbProveniencia = ""
    If geResults = True Then
        LbeResults.Visible = True
    Else
        LbeResults.Visible = False
    End If
    LbeResults = ""
    LbChega = ""
    ImgComFinal.Visible = False
    ImgComTec.Visible = False
    LbProdutos.caption = ""
    ImgExapande.Visible = False
    ImgColaps.Visible = False
    EcReqAnteriores.Clear
    If gTipoInstituicao = gTipoInstituicaoVet Then
        LbRaca.Visible = True
        LbRaca2.Visible = True
    Else
        LbRaca.Visible = False
        LbRaca2.Visible = False
    End If
    LbRaca.caption = ""
    LbGenero.caption = ""
    ImgHipo.Visible = False
    PicVe.Visible = False
    PicSeroteca.Visible = False
End Sub

Public Function RetornaNumReq() As String
    RetornaNumReq = BL_HandleNull(LbRequisicao, "")
End Function
Public Function RetornaUtente() As String
    RetornaUtente = BL_HandleNull(LbUtente, "")
End Function
Public Function RetornaNome() As String
    RetornaNome = BL_HandleNull(LbNome, "")
End Function
Public Function RetornaSeqUtente() As Long
    RetornaSeqUtente = BL_HandleNull(EcSeqUtente.caption, mediComboValorNull)
End Function

Public Function RetornaSexo() As Long
    RetornaSexo = BL_HandleNull(LbSexo.caption, "")
End Function
Public Function RetornaDataChegada() As String
    RetornaDataChegada = BL_HandleNull(LbChega, "")
End Function
Public Sub MostraComTec(mostra As Boolean)
    ImgComTec.Visible = mostra
End Sub
Public Sub MostraComFin(mostra As Boolean)
    ImgComFinal.Visible = mostra
End Sub
Public Sub ColocaCorEstado(cor As Long)
    LbEstadoReq.BackColor = cor
End Sub
Public Sub ColocaEstadoEresults(estado As String)
    LbeResults = estado
End Sub

Private Sub Command1_Click()
UserControl.Height = 2670
End Sub

Private Sub ImgColaps_Click()
    UserControl.Height = 1000
    ImgExapande.Visible = True
    ImgColaps.Visible = False
End Sub

Private Sub ImgExapande_Click()
    UserControl.Height = 2600
    ImgExapande.Visible = False
    ImgColaps.Visible = True
End Sub

Private Sub LbRequisicao_DblClick()
    Clipboard.Clear
    Clipboard.SetText LbRequisicao
End Sub
Private Sub Lbnome_DblClick()
    Clipboard.Clear
    Clipboard.SetText LbNome
End Sub


Public Sub MostraGrafico(valor As Boolean)
    ' nada
End Sub

Public Sub MostraProdutos(valor As Boolean)
    LbProdutos.Visible = valor
End Sub


Private Sub PreencheProdutos(n_req As String)
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    On Error GoTo TrataErro
    
    LbProdutos.caption = ""
    sSql = "select x1.cod_prod, x2.descr_produto, x1.cod_especif, x3.descr_especif "
    sSql = sSql & " FROM sl_req_prod x1 LEFT OUTER JOIN sl_especif x3 ON x1.cod_especif = x3.cod_especif,"
    sSql = sSql & " sl_produto x2 WHERE x1.cod_prod = x2.cod_produto AND  n_req = " & n_req
    
    RsProd.CursorLocation = adUseServer
    RsProd.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    If RsProd.RecordCount >= 1 Then
        While Not RsProd.EOF
            LbProdutos.caption = LbProdutos.caption & BL_HandleNull(RsProd!descr_produto, "")
            If BL_HandleNull(RsProd!descr_especif, "") <> "" Then
                LbProdutos.caption = LbProdutos.caption & " (" & BL_HandleNull(RsProd!descr_especif, "") & ")"
            End If
            LbProdutos.caption = LbProdutos.caption & ";"
            LbProdutos.Visible = True
            RsProd.MoveNext
        Wend
    End If
    RsProd.Close
    Set RsProd = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheProdutos: " & Err.Description, "CabecalhoResultsdos", "PreencheProdutos", True
    Exit Sub
    Resume Next
End Sub

Private Sub UserControl_Initialize()
    ImgExapande.Visible = False
    ImgColaps.Visible = False

End Sub

Private Sub PreencheRequisAnteriores()
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    On Error Resume Next
    EcReqAnteriores.Clear
    sSql = "SELECT * FROM sl_Requis WHERE seq_utente = " & EcSeqUtente.caption & " ORDER by dt_chega DESC "
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao
    If rsReq.RecordCount >= 1 Then
        While Not rsReq.EOF
            EcReqAnteriores.AddItem BL_HandleNull(rsReq!n_req, "") & Space(30 - Len(BL_HandleNull(rsReq!n_req, ""))) & BL_HandleNull(rsReq!dt_chega, "")
            rsReq.MoveNext
        Wend
    End If
    rsReq.Close
    Set rsReq = Nothing
End Sub

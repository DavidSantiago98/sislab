VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCQAnalises 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCQlotes"
   ClientHeight    =   8070
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   11910
   Icon            =   "FormCQAnalises.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8070
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcNumDias 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7560
      TabIndex        =   29
      Top             =   600
      Width           =   735
   End
   Begin VB.TextBox EcCodLotePesq 
      Height          =   285
      Left            =   6960
      TabIndex        =   28
      Top             =   7440
      Width           =   735
   End
   Begin VB.CommandButton BtImportar 
      Caption         =   "Importar An�lises"
      Height          =   495
      Left            =   10440
      TabIndex        =   27
      ToolTipText     =   "Importar An�lises Baseado Lote"
      Top             =   600
      Width           =   735
   End
   Begin VB.TextBox EcLote 
      Height          =   285
      Left            =   5760
      TabIndex        =   26
      Top             =   7440
      Width           =   735
   End
   Begin VB.TextBox EcDesvioPadrao 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10440
      TabIndex        =   3
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcMedia 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7560
      TabIndex        =   2
      Top             =   120
      Width           =   735
   End
   Begin VB.CommandButton BtPesquisaAna 
      Height          =   315
      Left            =   6120
      Picture         =   "FormCQAnalises.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   22
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcDescrAna 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   120
      Width           =   4455
   End
   Begin VB.TextBox EcCodAna 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   960
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
   Begin VB.CommandButton BtPesquisaLote 
      Height          =   315
      Left            =   6120
      Picture         =   "FormCQAnalises.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   19
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   600
      Width           =   375
   End
   Begin VB.TextBox EcDescrLote 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   600
      Width           =   4455
   End
   Begin VB.TextBox EcCodLote 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   960
      TabIndex        =   4
      Top             =   600
      Width           =   735
   End
   Begin VB.Frame Frame2 
      Height          =   945
      Left            =   120
      TabIndex        =   8
      Top             =   5160
      Width           =   11685
      Begin VB.TextBox EcHrAct 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcDtAct 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox EcUserActNome 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox EcHrCri 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   195
         Width           =   735
      End
      Begin VB.TextBox EcDtCri 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   195
         Width           =   1215
      End
      Begin VB.TextBox EcUserCriNome 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   195
         Width           =   2055
      End
      Begin VB.Label Label6 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   600
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcSeqAna 
      Height          =   285
      Left            =   1080
      TabIndex        =   7
      Top             =   7320
      Width           =   975
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   2160
      TabIndex        =   6
      Top             =   7440
      Width           =   1335
   End
   Begin VB.TextBox EcUserAct 
      Height          =   285
      Left            =   3960
      TabIndex        =   5
      Top             =   7440
      Width           =   1335
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   0
      Top             =   7080
      Width           =   615
   End
   Begin MSComctlLib.ListView LvAnalise 
      Height          =   3495
      Left            =   120
      TabIndex        =   17
      Top             =   1560
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   6165
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label Label1 
      Caption         =   "N�Dias"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   6960
      TabIndex        =   30
      Top             =   630
      Width           =   555
   End
   Begin VB.Label Label1 
      Caption         =   "Desvio Padr�o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   9240
      TabIndex        =   25
      Top             =   150
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "M�dia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   6960
      TabIndex        =   24
      Top             =   150
      Width           =   555
   End
   Begin VB.Label Label1 
      Caption         =   "Analise"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   23
      Top             =   120
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "Lote"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   20
      Top             =   600
      Width           =   795
   End
End
Attribute VB_Name = "FormCQAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rsAna As ADODB.recordset
' Constant to define listview text color.
Const cLightGray = &H808080

Const cLightGreen = &HC0FFC0
Const cLightYellow = &H80000018
Const cWhite = &HFFFFFF
Const cLightBlue = &HFCE7D8

Private Type analise
    seq_ana As Long
    cod_ana_s As String
    descr_ana_s As String
    cod_lote As String
    descr_lote As String
    desvioPadrao As Double
    media As Double
    user_cri As String
    dt_cri As String
    hr_cri As String
    user_act As String
    dt_act As String
    hr_act As String
End Type
Dim estrutAna() As analise
Dim totalAna As Integer


Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub
Sub EventoUnload()
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    If Not rsAna Is Nothing Then
        rsAna.Close
        Set rsAna = Nothing
    End If
    If EcLote <> "" Then
        Set gFormActivo = FormCQlotes
    Else
        Set gFormActivo = MDIFormInicio
    End If
    Set FormCQAnalises = Nothing
End Sub

Sub Inicializacoes()

    Me.caption = "An�lises"
    Me.left = 5
    Me.top = 5
    Me.Width = 12000
    Me.Height = 7485 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "SL_CQ_ANA"
    Set CampoDeFocus = EcCodAna
    
    NumCampos = 12
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_ana"
    CamposBD(1) = "cod_ana_s"
    CamposBD(2) = "cod_lote"
    CamposBD(3) = "media"
    CamposBD(4) = "desvio_padrao"
    CamposBD(5) = "user_cri"
    CamposBD(6) = "dt_cri"
    CamposBD(7) = "hr_cri"
    CamposBD(8) = "user_act"
    CamposBD(9) = "dt_act"
    CamposBD(10) = "hr_act"
    CamposBD(11) = "num_dias"
    
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeqAna
    Set CamposEc(1) = EcCodAna
    Set CamposEc(2) = EcCodLote
    Set CamposEc(3) = EcMedia
    Set CamposEc(4) = EcDesvioPadrao
    Set CamposEc(5) = EcUserCri
    Set CamposEc(6) = EcDtCri
    Set CamposEc(7) = EcHrCri
    Set CamposEc(8) = EcUserAct
    Set CamposEc(9) = EcDtAct
    Set CamposEc(10) = EcHrAct
    Set CamposEc(11) = EcNumDias
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(2) = "C�digo do Lote"
    TextoCamposObrigatorios(1) = "C�digo da An�lise"
    TextoCamposObrigatorios(3) = "M�dia "
    TextoCamposObrigatorios(4) = "Desvio Padr�o"
        ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_ana"
    Set ChaveEc = EcSeqAna
        
End Sub
Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDtCri, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDtAct, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcHrCri, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcHrAct, mediTipoHora
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    InicializaTreeView
    
End Sub
Sub PreencheValoresDefeito()
End Sub

Private Sub BtImportar_Click()
    
    If EcCodLote = "" Then
        Exit Sub
    End If

    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    gMsgTitulo = "Copiar"
    gMsgMsg = "Tem a certeza que quer copiar an�lises de outro lote? Todas as an�lises existentes dser�o apagadas"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        'Defini��o dos campos a retornar
        ChavesPesq(1) = "cod_lote"
        CamposEcran(1) = "cod_lote"
        Tamanhos(1) = 2000
        
        ChavesPesq(2) = "descr_lote"
        CamposEcran(2) = "descr_lote"
        Tamanhos(2) = 3000
        
        'Cabe�alhos
        Headers(1) = "C�digo"
        Headers(2) = "Descri��o"
        
        'N� de Campos a pesquisar na Classe=>Resultaods
        CamposRetorno.InicializaResultados 2
    
        'Query
        ClausulaFrom = "sl_cq_lote"
        ClausulaWhere = " sl_cq_lote.cod_lote <> " & BL_TrataStringParaBD(EcCodLote)
        CampoPesquisa1 = "descr_lote"
        
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Lotes")
        
        mensagem = "N�o foi encontrada nenhum Lote."
        
        If PesqRapida = True Then
            FormPesqRapidaAvancada.Show vbModal
            CamposRetorno.RetornaResultados resultados, CancelouPesquisa
            If Not CancelouPesquisa Then
                EcCodLotePesq.Text = resultados(1)
            End If
        Else
            BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
        End If
    
        If EcCodLotePesq <> "" Then
            PreencheAnalisesLote EcCodLote, EcCodLotePesq
            Dim codAux As String
            codAux = EcCodLote
            FuncaoLimpar
            EcCodLote = codAux
            FuncaoProcurar
        End If
    End If
    
    EcCodLotePesq = ""
End Sub

Private Sub BtPesquisaAna_Click()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAna.Text = resultados(2)
            EcCodAna.Text = resultados(1)
            EcCodAna.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAna.SetFocus
    End If

End Sub



Private Sub BtPesquisaLote_Click()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_lote"
    CamposEcran(1) = "cod_lote"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_lote"
    CamposEcran(2) = "descr_lote"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cq_lote"
    CampoPesquisa = "descr_lote"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Lotes")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrLote.Text = resultados(2)
            EcCodLote.Text = resultados(1)
            EcCodLote.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Lotes", vbExclamation, "ATEN��O"
        EcCodLote.SetFocus
    End If

End Sub

Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rsAna Is Nothing Then
            rsAna.Close
            Set rsAna = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Sub LimpaCampos()
    Me.SetFocus
    EcCodAna.Locked = False
    EcDescrAna = ""
    EcCodLote.Locked = False
    EcDescrLote = ""
    BtPesquisaAna.Enabled = True
    BtPesquisaLote.Enabled = True
    BG_LimpaCampo_Todos CamposEc
    EcUserActNome = ""
    EcUserCriNome = ""
    LvAnalise.ListItems.Clear
    totalAna = 0
    ReDim estrutAna(0)
End Sub
Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUserCri = gCodUtilizador
        EcDtCri = Bg_DaData_ADO
        EcHrCri = Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcUserAct = gCodUtilizador
        EcDtAct = Bg_DaData_ADO
        EcHrAct = Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(ChaveEc)
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    rsAna.Requery
    PreencheCampos
End Sub


Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcSeqAna = BG_DaMAX(NomeTabela, "seq_ana") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoRemover()
    
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rsAna = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY COD_ANA_S ASC "
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    
    rsAna.Open CriterioTabela, gConexao
    
    If rsAna.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rsAna.RecordCount = 0 Then
        FuncaoLimpar
    Else
        PreencheLV
        eccodlote_Validate False
        EcCodana_Validate False
        LvAnalise_Click
        EcCodLote.Locked = True
        EcCodAna.Locked = True
        BtPesquisaAna.Enabled = False
        BtPesquisaLote.Enabled = False
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rsAna.MovePrevious
    
    If rsAna.BOF Then
        rsAna.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rsAna.MoveNext
    
    If rsAna.EOF Then
        rsAna.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        BL_FimProcessamento Me
        PreencheCampos
    End If
End Sub


Private Sub InicializaTreeView()
    With LvAnalise
        .ColumnHeaders.Add(, , "C�d. Ana ", 1000, lvwColumnLeft).Key = "COD_ANA"
        .ColumnHeaders.Add(, , "Descr. Ana", 3000, lvwColumnCenter).Key = "DESCR_ANA"
        .ColumnHeaders.Add(, , "C�d Lote", 2900, lvwColumnCenter).Key = "COD_LOTE"
        .ColumnHeaders.Add(, , "Descr Lote", 1500, lvwColumnCenter).Key = "DESCR_LOTE"
        .ColumnHeaders.Add(, , "M�dia", 1500, lvwColumnCenter).Key = "MEDIA"
        .ColumnHeaders.Add(, , "Desvio Padr�o", 1500, lvwColumnCenter).Key = "DESVIO_PADRAO"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .Checkboxes = False
        .FullRowSelect = True
        .ForeColor = cLightGray
        .MultiSelect = False
    End With
    ColocaCores LvAnalise, PictureListColor, cLightGreen, cWhite
    Exit Sub

End Sub

Public Sub ColocaCores(lv As ListView, pb As PictureBox, cor1 As Long, Optional cor2 As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo TrataErro
    
    lColor1 = cor1
    lColor2 = cor2
    lv.Picture = LoadPicture("")
    lv.Refresh
    pb.Cls
    pb.AutoRedraw = True
    pb.BorderStyle = vbBSNone
    pb.ScaleMode = vbTwips
    pb.Visible = False
    lv.PictureAlignment = lvwTile
    pb.Font = lv.Font
    pb.top = lv.top
    pb.Font = lv.Font
    With pb.Font
        .Size = lv.Font.Size + 2
        .Bold = lv.Font.Bold
        .Charset = lv.Font.Charset
        .Italic = lv.Font.Italic
        .Name = lv.Font.Name
        .Strikethrough = lv.Font.Strikethrough
        .Underline = lv.Font.Underline
        .Weight = lv.Font.Weight
    End With
    pb.Refresh
    lLineHeight = pb.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = lv.Width
    pb.Height = lBarHeight * 2
    pb.Width = lBarWidth
    pb.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    pb.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    pb.AutoSize = True
    lv.Picture = pb.Image
    lv.Refresh
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "ColocaCores", False
    Exit Sub
End Sub



Private Sub PreencheLV()
    Dim i As Integer
    LvAnalise.ListItems.Clear
    totalAna = 0
    ReDim estrutAna(0)
    While Not rsAna.EOF
        totalAna = totalAna + 1
        ReDim Preserve estrutAna(totalAna)
        
        estrutAna(totalAna).seq_ana = BL_HandleNull(rsAna!seq_ana, -1)
        estrutAna(totalAna).cod_ana_s = BL_HandleNull(rsAna!cod_ana_s, "")
        estrutAna(totalAna).descr_ana_s = BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", BL_HandleNull(rsAna!cod_ana_s, ""), "V")
        
        estrutAna(totalAna).cod_lote = BL_HandleNull(rsAna!cod_lote, "")
        estrutAna(totalAna).descr_lote = BL_SelCodigo("SL_CQ_LOTE", "DESCR_LOTE", "COD_LOTE", BL_HandleNull(rsAna!cod_lote, ""), "V")
        estrutAna(totalAna).media = BL_HandleNull(rsAna!media, 0)
        estrutAna(totalAna).desvioPadrao = BL_HandleNull(rsAna!desvio_padrao, 0)
        
        estrutAna(totalAna).user_cri = BL_HandleNull(rsAna!user_cri, "")
        estrutAna(totalAna).user_act = BL_HandleNull(rsAna!user_act, "")
        estrutAna(totalAna).dt_cri = BL_HandleNull(rsAna!dt_cri, "")
        estrutAna(totalAna).dt_act = BL_HandleNull(rsAna!dt_act, "")
        estrutAna(totalAna).hr_cri = BL_HandleNull(rsAna!hr_cri, "")
        estrutAna(totalAna).hr_act = BL_HandleNull(rsAna!hr_act, "")
        
        With LvAnalise.ListItems.Add(totalAna, "KEY_" & rsAna!seq_ana, rsAna!cod_ana_s)
            .ListSubItems.Add , , estrutAna(totalAna).descr_ana_s
            .ListSubItems.Add , , estrutAna(totalAna).cod_lote
            .ListSubItems.Add , , estrutAna(totalAna).descr_lote
            .ListSubItems.Add , , estrutAna(totalAna).media
            .ListSubItems.Add , , estrutAna(totalAna).desvioPadrao
        End With
        rsAna.MoveNext
    Wend
    rsAna.MoveFirst
End Sub





Private Sub LvAnalise_Click()
    If LvAnalise.ListItems.Count = 0 Then Exit Sub
    EcSeqAna = estrutAna(LvAnalise.SelectedItem.Index).seq_ana
    EcCodAna = estrutAna(LvAnalise.SelectedItem.Index).cod_ana_s
    EcDescrAna = estrutAna(LvAnalise.SelectedItem.Index).descr_ana_s
    EcCodLote = estrutAna(LvAnalise.SelectedItem.Index).cod_lote
    EcDescrLote = estrutAna(LvAnalise.SelectedItem.Index).descr_lote
    EcMedia = estrutAna(LvAnalise.SelectedItem.Index).media
    EcDesvioPadrao = estrutAna(LvAnalise.SelectedItem.Index).desvioPadrao
    EcUserCri = estrutAna(LvAnalise.SelectedItem.Index).user_cri
    EcUserAct = estrutAna(LvAnalise.SelectedItem.Index).user_act
    EcDtCri = estrutAna(LvAnalise.SelectedItem.Index).dt_cri
    EcDtAct = estrutAna(LvAnalise.SelectedItem.Index).dt_act
    EcHrCri = estrutAna(LvAnalise.SelectedItem.Index).hr_cri
    EcHrAct = estrutAna(LvAnalise.SelectedItem.Index).hr_act
    EcUserCriNome = BL_SelNomeUtil(EcUserCri.Text)
    EcUserActNome = BL_SelNomeUtil(EcUserAct.Text)
End Sub


Public Sub EcCodana_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodAna.Text <> "" Then
        EcCodAna = UCase(EcCodAna)
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s ='" & Trim(EcCodAna.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodAna.Text = ""
            EcDescrAna.Text = ""
        Else
            EcDescrAna.Text = BL_HandleNull(Tabela!descr_ana_s, "")
        End If
       
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrAna.Text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodAna_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodAna_Validate"
    Exit Sub
    Resume Next
End Sub


Public Sub eccodlote_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodLote.Text <> "" Then
        EcCodLote = UCase(EcCodLote)
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_lote FROM sl_cq_lote WHERE cod_lote='" & Trim(EcCodLote.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodLote.Text = ""
            EcDescrLote.Text = ""
        Else
            EcDescrLote.Text = BL_HandleNull(Tabela!descr_lote, "")
        End If
       
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrLote.Text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "eccodlote_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "eccodlote_Validate"
    Exit Sub
    Resume Next
End Sub

Public Sub ecmedia_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcMedia.Text <> "" Then
        If Not IsNumeric(EcMedia) Then
            BG_Mensagem mediMsgBox, "Valor incorrecto!", vbInformation, ""
            EcMedia.Text = ""
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ecmedia_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "ecmedia_Validate"
    Exit Sub
    Resume Next
End Sub
Public Sub ecdesviopadrao_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcDesvioPadrao.Text <> "" Then
        If Not IsNumeric(EcDesvioPadrao) Then
            BG_Mensagem mediMsgBox, "Valor incorrecto!", vbInformation, ""
            EcDesvioPadrao.Text = ""
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ecdesviopadrao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "ecdesviopadrao_Validate"
    Exit Sub
    Resume Next
End Sub






Private Sub PreencheAnalisesLote(CodLoteDestino As String, CodLoteOrigem As String)
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM SL_CQ_ANA WHERE cod_lote = " & BL_TrataStringParaBD(EcCodLote)
    BG_ExecutaQuery_ADO sSql
    sSql = "SELECT seq_ana, cod_ana_s, cod_lote, media, desvio_padrao, user_cri, dt_cri, hr_cri FROM sl_cq_ana WHERE cod_lote = " & BL_TrataStringParaBD(CodLoteOrigem)
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            sSql = "INSERT INTO sl_cq_ana( seq_ana, cod_ana_s, cod_lote, media, desvio_padrao, user_cri, dt_cri, hr_cri) VALUES ("
            sSql = sSql & BG_DaMAX("sl_cq_ana", "seq_ana") + 1 & ", " & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_ana_s, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(CodLoteDestino) & ",0,0, " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
            BG_ExecutaQuery_ADO sSql
            rsAna.MoveNext
        Wend
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao clonar Lote" & Err.Number & " - " & Err.Description, Me.Name, "PreencheAnalisesLote", True
    Exit Sub
    Resume Next
End Sub

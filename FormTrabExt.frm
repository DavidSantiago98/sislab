VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormTrabExt 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormTrabExt"
   ClientHeight    =   8430
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13485
   Icon            =   "FormTrabExt.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8430
   ScaleWidth      =   13485
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command2 
      Height          =   495
      Left            =   12600
      Picture         =   "FormTrabExt.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Integrar Resultados"
      Top             =   7800
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Height          =   495
      Left            =   12000
      Picture         =   "FormTrabExt.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Exportar para ficheiro"
      Top             =   7800
      Width           =   615
   End
   Begin VB.CommandButton BtGerar 
      Height          =   495
      Left            =   11400
      Picture         =   "FormTrabExt.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   13
      ToolTipText     =   "Gerar Folha de Trabalho para Exterior"
      Top             =   7800
      Width           =   615
   End
   Begin MSFlexGridLib.MSFlexGrid FgAna 
      Height          =   6375
      Left            =   120
      TabIndex        =   12
      Top             =   1320
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   11245
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13335
      Begin VB.CommandButton BtPesquisaLocais 
         Height          =   315
         Left            =   12890
         Picture         =   "FormTrabExt.frx":266A
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   600
         Width           =   375
      End
      Begin VB.ListBox EcListaLocais 
         Appearance      =   0  'Flat
         Height          =   420
         Left            =   8040
         TabIndex        =   18
         Top             =   600
         Width           =   4815
      End
      Begin VB.TextBox EcResultado 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3840
         TabIndex        =   16
         Top             =   240
         Width           =   2295
      End
      Begin VB.TextBox EcNumEnvio 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   840
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDescrEntExt 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   645
         Width           =   4215
      End
      Begin VB.CommandButton BtPesquisaRapidaEntExt 
         Height          =   375
         Left            =   6150
         Picture         =   "FormTrabExt.frx":2BF4
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Pesquisa R�pida de Entidades Externas"
         Top             =   600
         Width           =   435
      End
      Begin VB.TextBox EcCodEntExt 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   840
         TabIndex        =   2
         Top             =   645
         Width           =   1095
      End
      Begin VB.CheckBox CkDatas 
         Caption         =   "Intervalo de datas"
         Height          =   255
         Left            =   7440
         TabIndex        =   3
         Top             =   240
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   9840
         TabIndex        =   4
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   144965633
         CurrentDate     =   39638
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   11640
         TabIndex        =   5
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   144965633
         CurrentDate     =   39638
      End
      Begin VB.Label Label1 
         Caption         =   "Locais"
         Height          =   255
         Index           =   5
         Left            =   7440
         TabIndex        =   20
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Resutado Provis�rio"
         Height          =   255
         Index           =   4
         Left            =   2280
         TabIndex        =   15
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Envio N�"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Entidade"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   10
         Top             =   645
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "De"
         Height          =   255
         Index           =   0
         Left            =   9360
         TabIndex        =   7
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "a"
         Height          =   255
         Index           =   1
         Left            =   11280
         TabIndex        =   6
         Top             =   240
         Width           =   375
      End
   End
End
Attribute VB_Name = "FormTrabExt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer
Dim CampoDeFocus As Object

' ESTRUTURA COM DADOS DA LIST BOX
Private Type analises
    cod_analise As String
    abrev_Analise As String
    descr_analise As String
    cod_produto As String
    cod_tubo As String
    descr_produto As String
    descR_tubo As String
End Type
Dim estrutAnalises() As analises
Dim totalAna As Long

' ESTRUTURA COM DADOS DAS ANALISES
Private Type requisicoes
    seq_utente As String
    nome As String
    Utente As String
    dt_nasc As String
    Sexo As String
    n_req As String
    dt_chega As String
    cod_local As String
    Existe() As Boolean
    Seleccionada() As Integer
End Type
Dim estrutRequis() As requisicoes
Dim totalRequis As Long

' CORES UTILIZADAS
Const vermelho = &H8080FF
Const Amarelo = &H80FFFF
Const Branco = &HFFFFFF
Const laranja = &H80C0FF

Const cinzento = &HC0C0C0              '&HE0E0E0
Const Verde = &HC0FFC0
Const azul = &HFEA381                          '&HFF8080

Const AzulClaro = &HFF8080



Private Sub BtGerar_Click()
    If totalAna = 0 And totalRequis = 0 Then
        FuncaoProcurar True
    End If
    If EcNumEnvio = "" Then
        InsereBD
    End If
    imprime
End Sub

Private Sub CkDatas_Click()
    If CkDatas.value = vbChecked Then
        EcDtIni.Enabled = True
        EcDtFim.Enabled = True
    Else
        EcDtIni.Enabled = False
        EcDtFim.Enabled = False
    End If
End Sub

' ----------------------------------------------------------------------

' ACTUALIZA ESTADO AO CLICAR NUMA COLUNA, LINHA OU CELULA

' ----------------------------------------------------------------------
Private Sub FgAna_Click()
    Dim i As Long
    Dim j As Long
    If EcNumEnvio <> "" Then Exit Sub
    ' ----------------------------------------------------------------------------
    ' CLICOU NUMA CELULA  - SE EXISTIR  - SE MARCADA DESMARCA, SE DESMARCADA MARCA
    ' ----------------------------------------------------------------------------
    If FGAna.Col >= 2 And FGAna.row >= 2 Then
        If estrutRequis(FGAna.row - 1).Existe(FGAna.Col - 1) = True Then
            If estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 1 Then
                estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 0
                FGAna.CellBackColor = vermelho
            ElseIf estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 0 Then
                estrutRequis(FGAna.row - 1).Seleccionada(FGAna.Col - 1) = 1
                FGAna.CellBackColor = Verde
            End If
        End If

    ' ----------------------------------------------------------------------------
    ' CLICOU NUMA COLUNA - MARCA TODAS REQUISICOES COM AKELA ANALISE
    ' -----------------------------------------------------------------------------
    ElseIf FGAna.Col >= 2 And FGAna.row = 1 Then
        For i = 1 To totalRequis
            If estrutRequis(i).Existe(FGAna.Col - 1) = True Then
                    FGAna.row = i + 1
                    estrutRequis(i).Seleccionada(FGAna.Col - 1) = 1
                    FGAna.CellBackColor = Verde
            End If
        Next

    ' ----------------------------------------------------------------------
    ' CLICOU NUMA LINHA - MARCA TODAS ANALISES DAKELA REQUISICAO
    ' ----------------------------------------------------------------------
    ElseIf FGAna.Col = 1 And FGAna.row >= 2 Then
        For i = 1 To totalAna
            If estrutRequis(FGAna.row - 1).Existe(i) = True Then
                    FGAna.Col = i + 1
                    estrutRequis(FGAna.row - 1).Seleccionada(i) = 1
                    FGAna.CellBackColor = Verde
            End If
        Next
    ElseIf FGAna.Col = 1 And FGAna.row = 1 Then

        For i = 1 To totalAna
            For j = 1 To totalRequis
                If estrutRequis(j).Existe(i) = True Then
                        FGAna.row = j + 1
                        FGAna.Col = i + 1
                        estrutRequis(j).Seleccionada(i) = 1
                        FGAna.CellBackColor = Verde
                End If
            Next
        Next
    End If
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormTrabExt = Nothing

End Sub

Sub DefTipoCampos()
    With FGAna
        .rows = 3
        .FixedRows = 1
        .Cols = 3
        .FixedCols = 1
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 1000
        .ColWidth(1) = 0
        .RowHeight(1) = 0
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .row = 1
        .Col = 1
    End With
    
End Sub

Sub PreencheValoresDefeito()
    LimpaCampos
    LimpaEstruturas
    
    CkDatas.value = vbUnchecked
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    
End Sub

Sub Inicializacoes()
         
    Me.caption = "An�lises para Exterior"
    Me.Width = 13350
    Me.Height = 9300 ' Normal
    Me.left = 50
    Me.top = 50
    Set CampoDeFocus = EcCodEntExt
    
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
End Sub

Sub LimpaCampos()
    LimpaFGAna
    CkDatas.value = vbUnchecked
    EcNumEnvio = ""
    EcNumEnvio.Locked = False
    EcCodEntExt = ""
    EcDescrEntExt = ""
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    EcResultado = ""
    FGAna.Visible = False
    EcListaLocais.Clear
End Sub

' ----------------------------------------------------------------------

' LIMPA FLEX GRID

' ----------------------------------------------------------------------
Sub LimpaFGAna()
    FGAna.Cols = 3
    FGAna.rows = 3
    FGAna.TextMatrix(0, 1) = ""
    FGAna.TextMatrix(0, 2) = ""
    FGAna.TextMatrix(1, 0) = ""
    FGAna.TextMatrix(1, 1) = ""
    FGAna.TextMatrix(1, 2) = ""
    FGAna.TextMatrix(2, 0) = ""
    FGAna.TextMatrix(2, 1) = ""
    FGAna.TextMatrix(2, 2) = ""
    
End Sub

' ----------------------------------------------------------------------

' PREENCHE FLEX GRID

' ----------------------------------------------------------------------
Private Sub PreencheFGAna()
    Dim i As Long
    Dim j As Long
    FGAna.Cols = totalAna + 2
    FGAna.rows = totalRequis + 2
    FGAna.ColWidth(0) = 700
    
    For i = 1 To totalAna
        FGAna.ColWidth(i + 1) = 800
        FGAna.ColAlignment(i + 1) = flexAlignCenterCenter
        FGAna.TextMatrix(1, i + 1) = estrutAnalises(i).abrev_Analise
        FGAna.TextMatrix(0, i + 1) = estrutAnalises(i).abrev_Analise
    Next
    
    
    For i = 1 To totalRequis
        FGAna.TextMatrix(i + 1, 0) = estrutRequis(i).n_req
        FGAna.TextMatrix(i + 1, 1) = estrutRequis(i).n_req
        FGAna.row = i + 1
        For j = 1 To totalAna
            FGAna.Col = j + 1
            ' ----------------------------------------------------------------------
            ' SE ANALISE MARCADA
            ' ----------------------------------------------------------------------
            If estrutRequis(i).Existe(j) = True Then
                
                ' ----------------------------------------------------------------------
                ' SE JA FOI ENVIADA - COLOCA A VERDE SENAO A AMARELO
                ' ----------------------------------------------------------------------
                FGAna.CellBackColor = Verde
                FGAna.TextMatrix(i + 1, j + 1) = " X "
            Else
                FGAna.CellBackColor = vbWhite
                FGAna.TextMatrix(i + 1, j + 1) = ""
            End If
        Next
    Next
    
End Sub
Private Sub LimpaEstruturas()
    totalAna = 0
    ReDim estrutAnalises(totalAna)

    totalRequis = 0
    ReDim estrutRequis(totalRequis)
End Sub

Sub FuncaoLimpar()
    LimpaCampos
    LimpaEstruturas
    
End Sub

Sub FuncaoProcurar(Optional InibeFg As Boolean)
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    Dim codLocalApar As String
    Dim i As Integer
    On Error GoTo TrataErro
    LimpaEstruturas
    EcNumEnvio.Locked = True
    
    If EcNumEnvio = "" And EcCodEntExt <> "" Then
        sSql = "SELECT DISTINCT req.n_req, req.dt_chega, marc.cod_agrup, ana.descr_ana, ute.nome_ute, ute.dt_nasc_ute, ana.abr_ana, ute.utente, "
        sSql = sSql & " ute.sexo_ute, ute.seq_utente,req.cod_local, sl_produto.descR_produto, sl_tubo.descr_tubo, sl_produto.cod_produto, sl_tubo.cod_tubo  "
        sSql = sSql & " FROM sl_requis req, sl_identif ute, sl_marcacoes marc, slv_analises ana"
        sSql = sSql & " LEFT OUTER JOIN sl_produto ON ana.cod_produto = sl_produto.cod_produto "
        sSql = sSql & " LEFT OUTER JOIN sl_tubo ON ana.cod_tubo = sl_tubo.cod_tubo, "
        sSql = sSql & " sl_ent_ext ent, sl_ent_ext_ana b "
        sSql = sSql & " WHERE ute.seq_utente = req.seq_utente AND req.n_req  = marc.n_Req AND ana.cod_ana = marc.cod_agrup "
        sSql = sSql & " AND ent.cod_ent_ext =" & BL_TrataStringParaBD(EcCodEntExt) & " AND ent.cod_ent_ext = b.cod_ent_ext AND marc.cod_agrup = b.cod_ana "
        sSql = sSql & " AND (marc.n_envio IS NULL OR marc.n_envio = 0 )"
        sSql = sSql & " AND req.estado_Req not in (" & BL_TrataStringParaBD(gEstadoReqCancelada) & "," & BL_TrataStringParaBD(gEstadoReqBloqueada) & " ) "
        sSql = sSql & " AND ( ana.cod_tubo IS NULL OR ana.cod_tubo IN (SELECT cod_tubo FROM sl_req_tubo rt WHERE rt.n_Req = req.n_Req AND dt_chega IS NOT NULL AND dt_eliminacao IS NULL))"
        If CkDatas.value = vbChecked Then
            sSql = sSql & " AND req.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
        End If
        ' ------------------------------------------------------------------------------
        ' LOCAL PREENCHIDO
        ' ------------------------------------------------------------------------------
        If EcListaLocais.ListCount > 0 Then
            sSql = sSql & " AND req.cod_local IN ("
            For i = 0 To EcListaLocais.ListCount - 1
                sSql = sSql & EcListaLocais.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
        End If
        
    sSql = sSql & " ORDER by n_Req, cod_agrup "
        rsReq.CursorType = adOpenStatic
        rsReq.CursorLocation = adUseServer
        rsReq.Open sSql, gConexao
    
        If Not rsReq.EOF Then
            While Not rsReq.EOF
                PreencheEstrutAna rsReq!cod_agrup, BL_HandleNull(rsReq!abr_ana, "---"), BL_HandleNull(rsReq!descr_ana), BL_HandleNull(rsReq!cod_tubo, ""), BL_HandleNull(rsReq!descR_tubo, ""), _
                                 BL_HandleNull(rsReq!cod_produto, ""), BL_HandleNull(rsReq!descr_produto, "")
                PreencheEstrutRequis BL_HandleNull(rsReq!nome_ute, ""), BL_HandleNull(rsReq!Utente, ""), _
                    BL_HandleNull(rsReq!dt_nasc_ute, ""), BL_HandleNull(rsReq!sexo_ute, ""), rsReq!n_req, _
                    BL_HandleNull(rsReq!dt_chega, ""), rsReq!cod_agrup, rsReq!seq_utente, BL_HandleNull(rsReq!cod_local, "")
                rsReq.MoveNext
            Wend
            LimpaFGAna
            If InibeFg = False Then
                PreencheFGAna
            End If
            FGAna.Visible = True
        Else
            BG_Mensagem mediMsgBox, "N�o foram selecionados registos segundo as condi��es de pesquisa!", vbExclamation, "Procurar"
            LimpaCampos
        End If
        rsReq.Close
    ElseIf EcNumEnvio <> "" Then
        sSql = "SELECT DISTINCT req.n_req, req.dt_chega, marc.cod_agrup, ana.descr_ana, ute.nome_ute, ute.dt_nasc_ute, ana.abr_ana, ute.utente, "
        sSql = sSql & " ute.sexo_ute, ute.seq_utente,req.cod_local, sl_produto.descR_produto, sl_tubo.descr_tubo, sl_produto.cod_produto, sl_tubo.cod_tubo, ent.cod_ent_ext  "
        sSql = sSql & " FROM sl_requis req, sl_identif ute, sl_marcacoes marc, slv_analises ana"
        sSql = sSql & " LEFT OUTER JOIN sl_produto ON ana.cod_produto = sl_produto.cod_produto "
        sSql = sSql & " LEFT OUTER JOIN sl_tubo ON ana.cod_tubo = sl_tubo.cod_tubo, "
        sSql = sSql & " sl_ent_ext ent, sl_ent_ext_ana b "
        sSql = sSql & " WHERE ute.seq_utente = req.seq_utente AND req.n_req  = marc.n_Req AND ana.cod_ana = marc.cod_agrup "
        sSql = sSql & " AND ent.cod_ent_ext = b.cod_ent_ext AND marc.cod_agrup = b.cod_ana AND marc.n_envio = " & EcNumEnvio
        sSql = sSql & " ORDER by n_Req, cod_agrup "
        rsReq.CursorType = adOpenStatic
        rsReq.CursorLocation = adUseServer
        rsReq.Open sSql, gConexao
    
        If Not rsReq.EOF Then
            EcCodEntExt = BL_HandleNull(rsReq!cod_ent_ext, "")
            eccodentext_Validate False
            'EcResultado = BL_HandleNull(rsReq!resultado, "")
            While Not rsReq.EOF
                PreencheEstrutAna rsReq!cod_agrup, BL_HandleNull(rsReq!abr_ana, "---"), BL_HandleNull(rsReq!descr_ana), BL_HandleNull(rsReq!cod_tubo, ""), BL_HandleNull(rsReq!descR_tubo, ""), _
                                 BL_HandleNull(rsReq!cod_produto, ""), BL_HandleNull(rsReq!descr_produto, "")
                PreencheEstrutRequis BL_HandleNull(rsReq!nome_ute, ""), BL_HandleNull(rsReq!Utente, ""), _
                    BL_HandleNull(rsReq!dt_nasc_ute, ""), BL_HandleNull(rsReq!sexo_ute, ""), rsReq!n_req, _
                    BL_HandleNull(rsReq!dt_chega, ""), rsReq!cod_agrup, rsReq!seq_utente, BL_HandleNull(rsReq!cod_local, "")
                rsReq.MoveNext
            Wend
            LimpaFGAna
            If InibeFg = False Then
                PreencheFGAna
            End If
            FGAna.Visible = True
        Else
            BG_Mensagem mediMsgBox, "N�o foram selecionados registos segundo as condi��es de pesquisa!", vbExclamation, "Procurar"
            LimpaCampos
        End If
        rsReq.Close
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar"
    Exit Sub
    Resume Next
End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

' ----------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ANALISES

' ----------------------------------------------------------------------
Private Sub PreencheEstrutAna(cod_ana As String, abrev_ana As String, descr_ana, cod_tubo As String, descR_tubo As String, _
                              cod_produto As String, descr_produto As String)
    Dim i As Long
    On Error GoTo TrataErro
    
    For i = 1 To totalAna
        If estrutAnalises(i).cod_analise = cod_ana Then
            Exit Sub
        End If
    Next
    totalAna = totalAna + 1
    ReDim Preserve estrutAnalises(totalAna)
    estrutAnalises(totalAna).cod_analise = cod_ana
    estrutAnalises(totalAna).abrev_Analise = abrev_ana
    estrutAnalises(totalAna).descr_analise = descr_ana
    estrutAnalises(totalAna).cod_produto = cod_produto
    estrutAnalises(totalAna).cod_tubo = cod_tubo
    estrutAnalises(totalAna).descr_produto = descr_produto
    estrutAnalises(totalAna).descR_tubo = descR_tubo
    
    For i = 1 To totalRequis
        ReDim Preserve estrutRequis(i).Existe(totalAna)
        ReDim Preserve estrutRequis(i).Seleccionada(totalAna)
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstrutAna: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEstrutAna"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' PREENCHE ESTRUTURA DE REQUISICOES

' ----------------------------------------------------------------------

Private Sub PreencheEstrutRequis(nome As String, Utente As String, dt_nasc As String, Sexo As String, n_req As String, _
                                 dt_chega As String, cod_ana As String, seq_utente As String, cod_local As String)
    Dim i As Long
    Dim j As Long
    On Error GoTo TrataErro
    
    For i = 1 To totalRequis
        If estrutRequis(i).n_req = n_req Then
            For j = 1 To totalAna
                If estrutAnalises(j).cod_analise = cod_ana Then
                    estrutRequis(i).Existe(j) = True
                    estrutRequis(i).Seleccionada(j) = 1
                    Exit Sub
                End If
            Next
        End If
    Next
    
    totalRequis = totalRequis + 1
    ReDim Preserve estrutRequis(totalRequis)
    
    estrutRequis(totalRequis).n_req = n_req
    estrutRequis(totalRequis).dt_chega = dt_chega
    estrutRequis(totalRequis).nome = nome
    estrutRequis(totalRequis).Utente = Utente
    estrutRequis(totalRequis).Sexo = Sexo
    estrutRequis(totalRequis).dt_nasc = dt_nasc
    estrutRequis(totalRequis).seq_utente = seq_utente
    estrutRequis(totalRequis).cod_local = cod_local
    
    
    ReDim estrutRequis(totalRequis).Existe(totalAna)
    ReDim estrutRequis(totalRequis).Seleccionada(totalAna)
    For i = 1 To totalAna
        If estrutAnalises(i).cod_analise = cod_ana Then
            estrutRequis(totalRequis).Existe(i) = True
            estrutRequis(totalRequis).Seleccionada(i) = 1
        Else
            estrutRequis(totalRequis).Existe(i) = False
            estrutRequis(totalRequis).Seleccionada(i) = -1
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstrutRequis: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEstrutRequis"
    Exit Sub
    Resume Next
End Sub

Public Sub eccodentext_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodEntExt.Text = UCase(EcCodEntExt.Text)
    If Trim(EcCodEntExt) <> "" Then

        sql = "SELECT " & _
              "     descr_ent_ext " & _
              "FROM " & _
              "sl_ent_ext " & _
              "WHERE " & _
              "     cod_ent_ext=" & BL_TrataStringParaBD(EcCodEntExt)

        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Entidade inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrEntExt = ""
            EcCodEntExt = ""
        Else
            EcDescrEntExt.Text = rsCodigo!descr_ent_ext
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrEntExt.Text = ""
        EcCodEntExt = ""
    End If

End Sub



Private Sub BtPesquisaRapidaEntExt_click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_ent_ext"
    CamposEcran(1) = "cod_ent_ext"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_ent_ext"
    CamposEcran(2) = "descr_ent_ext"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_ent_ext"
    CampoPesquisa1 = "descr_ent_ext"
    ClausulaWhere = ""

        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Entidades")
    
    mensagem = "N�o foi encontrada nenhuma Entidade Externa."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodEntExt.Text = resultados(1)
            EcDescrEntExt.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

' -----------------------------------------------------------------

' GERA NOVO ENVIO E INSERE NA TABELA

' -----------------------------------------------------------------

Private Sub InsereBD()
    Dim util As String
    Dim dt As String
    Dim hr As String
    Dim i As Integer
    Dim j As Integer
    Dim sSql As String
    Dim registos As Integer
    On Error GoTo TrataErro
    
    EcNumEnvio = BG_DaMAX("sl_envio_trab_ext", "n_envio") + 1
    EcNumEnvio.Locked = True
    util = CStr(gCodUtilizador)
    dt = Bg_DaData_ADO
    hr = Bg_DaHora_ADO
    BG_BeginTransaction
    sSql = "INSERT INTO sl_envio_trab_ext_export (n_envio, flg_enviado, user_cri, dt_cri, hr_cri) VALUES("
    sSql = sSql & EcNumEnvio & ", 0," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalRequis
        For j = 1 To totalAna
            If estrutRequis(i).Seleccionada(j) = 1 Then
                sSql = "INSERT INTO sl_envio_trab_ext(n_envio, n_req, cod_ana, user_cri, dt_cri, hr_cri, cod_ent_ext, resultado) VALUES("
                sSql = sSql & EcNumEnvio & ", " & estrutRequis(i).n_req & "," & BL_TrataStringParaBD(estrutAnalises(j).cod_analise) & ", "
                sSql = sSql & BL_TrataStringParaBD(util) & ", " & BL_TrataDataParaBD(dt) & "," & BL_TrataStringParaBD(hr) & ","
                sSql = sSql & BL_TrataStringParaBD(EcCodEntExt) & "," & BL_TrataStringParaBD(Mid(EcResultado, 1, 20)) & ")"
                registos = BG_ExecutaQuery_ADO(sSql)
                If registos <= 0 Then
                    GoTo TrataErro
                End If
                
                sSql = "UPDATE sl_marcacoes SET n_envio = " & EcNumEnvio & "WHERE n_Req = " & estrutRequis(i).n_req & " AND cod_agrup  = " & BL_TrataStringParaBD(estrutAnalises(j).cod_analise)
                registos = BG_ExecutaQuery_ADO(sSql)
                If registos <= 0 Then
                    GoTo TrataErro
                End If
               
                If Trim(EcResultado) <> "" Then
                    If GravaResultados(i, j) = False Then
                        GoTo TrataErro
                    End If
                End If
            End If
        Next
    Next
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "InsereBD: " & Err.Number & " - " & Err.Description, Me.Name, "InsereBD"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' INSERE NA TABELA DO REPORT

' -----------------------------------------------------------------
Private Sub imprime()
    Dim util As String
    Dim dt As String
    Dim hr As String
    Dim i As Integer
    Dim j As Integer
    Dim sSql As String
    Dim idade As String
    
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_cr_envio_trab_ext WHERE num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalRequis
        idade = BG_CalculaIdade(CDate(estrutRequis(i).dt_nasc), CDate(estrutRequis(i).dt_chega))
        For j = 1 To totalAna
            If estrutRequis(i).Seleccionada(j) = 1 Then
                sSql = "INSERT INTO sl_cr_envio_trab_ext(nome_computador, num_sessao, utente, nome, sexo,"
                sSql = sSql & " dt_nasc, idade, n_req, dt_chega, cod_ana, descr_ana,n_envio,cod_ent_ext,cod_local, descr_produto, descr_tubo ) VALUES( "
                sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
                sSql = sSql & gNumeroSessao & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).Utente) & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).nome) & ", "
                If estrutRequis(i).Sexo = gT_Masculino Then
                    sSql = sSql & BL_TrataStringParaBD("Masculino") & ", "
                Else
                    sSql = sSql & BL_TrataStringParaBD("Feminino") & ", "
                End If
                sSql = sSql & BL_TrataDataParaBD(estrutRequis(i).dt_nasc) & ", "
                sSql = sSql & BL_TrataStringParaBD(idade) & ", "
                sSql = sSql & estrutRequis(i).n_req & ", "
                sSql = sSql & BL_TrataDataParaBD(estrutRequis(i).dt_chega) & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutAnalises(j).cod_analise) & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutAnalises(j).descr_analise) & ", "
                sSql = sSql & EcNumEnvio & ","
                sSql = sSql & BL_TrataStringParaBD(EcCodEntExt) & ","
                sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).cod_local) & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutAnalises(j).descr_produto) & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutAnalises(j).descR_tubo) & ") "
                BG_ExecutaQuery_ADO sSql
            End If
        Next
    Next
    ImprimeReport
    sSql = "DELETE FROM sl_cr_envio_trab_ext WHERE num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Imprime: " & Err.Number & " - " & Err.Description, Me.Name, "Imprime"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' IMPRIME REPORT

' -----------------------------------------------------------------
Private Sub ImprimeReport()
    Dim continua As Boolean
    On Error GoTo TrataErro
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("FolhaTrabExt", "Folha Trabalho para Exterior", crptToPrinter)
    Else
        continua = BL_IniciaReport("FolhaTrabExt", "Folha Trabalho para Exterior", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT sl_cr_envio_trab_ext.nome_computador, sl_cr_envio_trab_ext.num_sessao,sl_cr_envio_trab_ext.utente, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_envio_trab_ext.nome , sl_cr_envio_trab_ext.sexo, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_envio_trab_ext.dt_nasc, sl_cr_envio_trab_ext.idade, sl_cr_envio_trab_ext.n_req, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_envio_trab_ext.dt_chega, sl_cr_envio_trab_ext.cod_ana, sl_cr_envio_trab_ext.descr_ana, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_envio_trab_ext.n_envio, sl_ent_ext.cod_ent_ext, sl_ent_ext.descr_ent_ext,sl_cr_envio_trab_ext.cod_local "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_envio_trab_ext, sl_ent_Ext "
    Report.SQLQuery = Report.SQLQuery & " WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    Report.SQLQuery = Report.SQLQuery & " AND sl_ent_ext.cod_ent_ext = sl_cr_envio_trab_ext.cod_ent_ext"
    Report.SQLQuery = Report.SQLQuery & " order by N_REQ ASC "
    
    'Report.SubreportToChange = ""
    Call BL_ExecutaReport
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeReport: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeReport"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' GRAVA RESULTADO POR DEFEITO NA TABELA SL_REALIZA

' -----------------------------------------------------------------
Private Function GravaResultados(iReq As Integer, iAna As Integer) As Boolean
    Dim rsMarc As New ADODB.recordset
    Dim sSql As String
    Dim seq_real As Long
    Dim registos As Integer
    
    On Error GoTo TrataErro
    GravaResultados = False
    sSql = "SELECT * FROM sl_marcacoes WHERE n_Req = " & estrutRequis(iReq).n_req & " AND cod_agrup =" & BL_TrataStringParaBD(estrutAnalises(iAna).cod_analise)
    rsMarc.CursorType = adOpenStatic
    rsMarc.CursorLocation = adUseServer
    rsMarc.Open sSql, gConexao
    If Not rsMarc.EOF Then
        While Not rsMarc.EOF
            seq_real = -1
            seq_real = BL_GeraNumero("SEQ_REALIZA")
            If seq_real = -1 Then GoTo TrataErro
            
            ' INSERE NA TABELA SL_REALIZA
            sSql = "INSERT INTO sl_realiza (Seq_realiza, n_req, cod_perfil, cod_ana_c, cod_ana_s, ord_ana, "
            sSql = sSql & " ord_ana_compl, ord_ana_perf, cod_agrup, n_folha_trab, flg_estado, user_cri, "
            sSql = sSql & " dt_cri, hr_cri, dt_chega, seq_utente, ord_marca, flg_facturado, flg_assinado) VALUES ( "
            sSql = sSql & seq_real & ", " & BL_HandleNull(rsMarc!n_req, "") & ", " & BL_TrataStringParaBD(BL_HandleNull(rsMarc!Cod_Perfil, "0"))
            sSql = sSql & ", " & BL_TrataStringParaBD(BL_HandleNull(rsMarc!cod_ana_c, "0")) & ", " & BL_TrataStringParaBD(BL_HandleNull(rsMarc!cod_ana_s, "0"))
            sSql = sSql & ", " & BL_HandleNull(rsMarc!Ord_Ana, "0") & ", " & BL_HandleNull(rsMarc!Ord_Ana_Compl, "0") & "," & BL_HandleNull(rsMarc!Ord_Ana_Perf, "0")
            sSql = sSql & ", " & BL_TrataStringParaBD(BL_HandleNull(rsMarc!cod_agrup, "0")) & ", " & BL_HandleNull(rsMarc!N_Folha_Trab, "0") & ", '1'"
            sSql = sSql & ", " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & BL_TrataDataParaBD(Bg_DaData_ADO)
            sSql = sSql & ", " & BL_TrataStringParaBD(CStr(Bg_DaHora_ADO)) & "," & BL_TrataDataParaBD(estrutRequis(iReq).dt_chega)
            sSql = sSql & ", " & estrutRequis(iReq).seq_utente & ", " & BL_HandleNull(rsMarc!Ord_Marca, "0") & "," & BL_HandleNull(rsMarc!Flg_Facturado, "0") & ", 0)"
            registos = BG_ExecutaQuery_ADO(sSql)
            If registos <= 0 Then
                GoTo TrataErro
            End If
            
            ' INSERE NA TABELA SL_RES_ALFAN
            If BL_HandleNull(rsMarc!cod_ana_s, "") <> "S99999" Then
                sSql = "INSERT INTO sl_Res_alfan (seq_realiza, n_res, result) VALUES("
                sSql = sSql & seq_real & ", 1," & BL_TrataStringParaBD(Mid(EcResultado, 1, 20)) & ")"
                registos = BG_ExecutaQuery_ADO(sSql)
                If registos <= 0 Then
                    GoTo TrataErro
                End If
            End If
            
            ' APAGA DE MARCACOES
            sSql = "DELETE FROM sl_marcacoes WHERE n_req = " & BL_HandleNull(rsMarc!n_req, "")
            sSql = sSql & " AND Cod_perfil = " & BL_TrataStringParaBD(BL_HandleNull(rsMarc!Cod_Perfil, "0"))
            sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(rsMarc!cod_ana_c, "0"))
            sSql = sSql & " AND cod_ana_s  = " & BL_TrataStringParaBD(BL_HandleNull(rsMarc!cod_ana_s, "0"))
            registos = BG_ExecutaQuery_ADO(sSql)
            If registos <= 0 Then
                GoTo TrataErro
            End If
            
            
            rsMarc.MoveNext
        Wend
    End If
    rsMarc.Close
    Set rsMarc = Nothing
    GravaResultados = True
Exit Function
TrataErro:
    GravaResultados = False
    BG_LogFile_Erros "GravaResultados: " & Err.Number & " - " & Err.Description, Me.Name, "GravaResultados"
    Exit Function
    Resume Next
End Function
Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(0) = resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub




Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaLocais.ListCount > 0 Then     'Delete
        If EcListaLocais.ListIndex > mediComboValorNull Then
            EcListaLocais.RemoveItem (EcListaLocais.ListIndex)
        End If
    End If
End Sub


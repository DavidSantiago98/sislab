Attribute VB_Name = "VigilanciaEpidemiologica"

Option Explicit

' --------------------------------------------------------------------------------------
' ESTRUTURAS PARA AVALIA��O DE REGRAS EPID. (ECRA RESULTADOS, ETC)
' --------------------------------------------------------------------------------------
Private Type Produtos
    cod_produto As String
    descr_produto As String
End Type
Private Type frases
    cod_frase As String
    descr_frase As String
End Type
Private Type analises
    cod_analise As String
    descr_analise As String
    t_res As Integer
    cod_sinal As Integer
    resAlfan As String
    flg_validade As Boolean
    frases() As frases
    totalFrases As Integer
End Type
Private Type sensibilidades
    cod_sensib As String
    descr_sensib As String
End Type
Private Type antibioticos
    cod_antibio As String
    descr_antibio As String
    Sensib() As sensibilidades
    totalSensib As Integer
End Type
Private Type resultadosAntib
    cod_antibio As String
    res_sensib As String
End Type
Private Type SubClassesAntib
    cod_subclasse As String
    descr_subclasse As String
    ResAntib() As resultadosAntib
    totalResAntib As Integer
End Type
Private Type microrganismos
    cod_microrg As String
    descr_microrg As String
    num_classes_sensiv As Integer
    num_min_classes As Integer
    flg_nao_avalia_sensib As Integer
    antib() As antibioticos
    totalAntib As Integer
    subClasses() As SubClassesAntib
    totalSubClasses As Integer
End Type
Private Type regrasVE
    seq_regra As String
    cod_regra As Integer
    descr_regra As String
    cod_gr_epid As Integer
    seq_realiza As Long
    dt_cri As String
    hr_cri As String
    user_cri As String
    cod_estado_ve As String
    
    micro() As microrganismos
    totalMicro As Integer
    prod() As Produtos
    totalProd As Integer
    ana() As analises
    totalAna As Integer
    
End Type
Dim regras() As regrasVE
Dim totalRegras As Integer
' --------------------------------------------------------------------------------------


' --------------------------------------------------------------------------------------
' ESTRUTURAS PARA PREENCHER ECRA DE VIGILANCIA EPID.
' --------------------------------------------------------------------------------------
Public Type ResultadosAntibioticos
    cod_antib As String
    descr_antib As String
    cod_subclasse As String
    descr_subclasse As String
    res_antib As String
End Type

Public Type resultadosAna
    cod_ana As String
    descr_ana As String
    resultado As String
End Type
Public Type resultadosProva
    Cod_Prova As String
    descr_prova As String
    resultado As String
End Type
Private Type detalhe
    seq_vig_epid As String
    cod_ana As String
    descr_ana As String
    cod_micro As String
    descr_micro As String
    cod_estado As String
    descr_estado As String
    seq_ve_email As String
    seq_ve_bin As String
    n_req As String
    dt_chega As String
    seq_realiza As String
    descr_carac_micro As String
    descr_t_sit As String
    inf_complementar As String
    linha As Integer
    ResAntib() As ResultadosAntibioticos
    totalResAntib As Integer
    ResAna() As resultadosAna
    totalResAna As Integer
    ResProvas() As resultadosProva
    totalResProvas As Integer
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    id_sinave As String
    '
    'edgar.parada Glintt-HS-19848
    enviarResultado1 As Integer
    enviarResultado2 As Integer
    '
End Type

Private Type Regra
    cod_regra As String
    descr_regra As String
    descr_excel As String
    flg_mostra_micro As Integer
    flg_suspeita As Integer
    linha As Integer
    detalhes() As detalhe
    totDet As Integer
End Type

Public Type GrEpid
    cod_gr_epid As String
    descr_gr_epid As String
    descr_instituicao As String
    email_to As String
    email_cc As String
    email_body As String
    flg_html As Integer
    corpo_html As String
    descr_imagem_anexo As String
    id_lab As String
    flg_nome As Integer
    flg_req As Integer
    flg_dt_chega As Integer
    flg_proven As Integer
    flg_t_sit As Integer
    flg_dt_nasc As Integer
    flg_dt_int As Integer
    flg_sexo As Integer
    flg_sns As Integer
    flg_PT As Integer
    flg_produto As Integer
    flg_idade As Integer
    flg_inf_complementar As Integer
    flg_processo As Integer
    linha As Integer
    regras() As Regra
    totalRegra As Integer
    'BRUNODSANTOS CHVNG-7389 - 02.06.2016
    flg_data_geracao As Integer
    flg_n_admissoes_inst As Integer
    '
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    tipo_notificacao As Integer
    flg_num_sns_SNV As Integer
    flg_cod_conv_SNV As Integer
    '
End Type
' --------------------------------------------------------------------------------------

Const Cinza = &HE0E0E0
Const Verde = &H8000&
Const vermelho = &HC0&

Public Enum XlLineStyle
    xlLineStyleNone
    xlContinuous
    xlDashDot
    xlDashDotDot
    xlSlantDashDot
    xlDouble
    xlDot
    xlDash
    xlUndefined
End Enum

Public Enum XlBordersIndex
    xlDiagonalDown = 5
    xlDiagonalUp = 6
    xlEdgeBottom = 9
    xlEdgeLeft = 7
    xlEdgeRight = 10
    xlEdgeTop = 8
    xlInsideHorizontal = 12
    xlInsideVertical = 11
End Enum




' --------------------------------------------------------------------------------------------------------------------

' PARA PREENCHE ESTRUTURA DE REGRAS PARA ESTRUTURA DE RESULTADOS, SEM ESTAR NA BD

' --------------------------------------------------------------------------------------------------------------------
Private Function VE_PreencheRegrasMicro(cod_gr_epid As Integer, seq_realiza As Long, estrutRes() As Resultados, indice As Integer, cod_regra_ve As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsMicro As New ADODB.recordset
    Dim iRegra As Integer
    Dim iMicro As Integer
    Dim iAntib As Integer
    Dim iSensib As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim l As Integer
    VE_PreencheRegrasMicro = False
    
    
    sSql = "SELECT distinct x3.cod_regra_ve, x3.descr_regra_ve, x3.cod_gr_epid, x4.cod_microrg, x4.descr_microrg, "
    sSql = sSql & " x5.cod_antibio, x5.descr_antibio, x2.sensibilidade, x3.num_classes, x3.flg_nao_avalia_sensib, x3.tipo_regra "
    sSql = sSql & " FROM  sl_cod_regras_ve_micro x2 LEFT OUTER JOIN sl_antibio x5 ON x2.cod_antib = x5.cod_antibio, sl_cod_regras_ve x3, sl_microrg x4 "
    If indice <= mediComboValorNull Then
        sSql = sSql & ", sl_res_micro x1 "
    End If
    sSql = sSql & " WHERE x2.cod_regra_ve = x3.cod_regra_ve AND x3.cod_gr_epid = " & cod_gr_epid
    sSql = sSql & " AND (x3.flg_invisivel IS NULL or x3.flg_invisivel = 0) "
    sSql = sSql & " AND x2.cod_micro = x4.cod_microrg "
    If indice <= mediComboValorNull Then
        sSql = sSql & " AND x1.seq_realiza = " & seq_realiza & " AND x1.cod_micro = x2.cod_micro "
    Else
        sSql = sSql & " AND x4.cod_microrg = " & BL_TrataStringParaBD(estrutRes(indice).micro_codigo)
    End If
    If cod_regra_ve <> "" Then
        sSql = sSql & " AND x3.cod_regra_ve =" & BL_TrataStringParaBD(cod_regra_ve)
    End If
    sSql = sSql & " ORDER BY cod_regra_ve, cod_microrg, cod_antibio, sensibilidade"
    
    rsMicro.CursorType = adOpenStatic
    rsMicro.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMicro.Open sSql, gConexao
    While Not rsMicro.EOF
        iRegra = mediComboValorNull
        iMicro = mediComboValorNull
        iAntib = mediComboValorNull
        iSensib = mediComboValorNull
        
        For i = 1 To totalRegras
            If regras(i).cod_regra = BL_HandleNull(rsMicro!cod_regra_ve, "") Then
                iRegra = i
                For j = 1 To regras(i).totalMicro
                    If regras(i).micro(j).cod_microrg = BL_HandleNull(rsMicro!cod_microrg, "") Then
                        iMicro = j
                        For k = 1 To regras(i).micro(j).totalAntib
                            If regras(i).micro(j).antib(k).cod_antibio = BL_HandleNull(rsMicro!cod_antibio, "") Then
                                iAntib = k
                                For l = 1 To regras(i).micro(j).antib(k).totalSensib
                                    If regras(i).micro(j).antib(k).Sensib(l).cod_sensib = BL_HandleNull(rsMicro!sensibilidade, "") Then
                                        iSensib = l
                                        Exit For
                                    End If
                                Next l
                                Exit For
                            End If
                        Next k
                        Exit For
                    End If
                Next j
                Exit For
            End If
        Next i
        
        If iRegra = mediComboValorNull Then
            totalRegras = totalRegras + 1
            ReDim Preserve regras(totalRegras)
            iRegra = totalRegras
            VE_PreencheEstrutRegras rsMicro, mediComboValorNull, iRegra, estrutRes, indice
        ElseIf iMicro = mediComboValorNull Then
            regras(iRegra).totalMicro = regras(iRegra).totalMicro + 1
            ReDim Preserve regras(iRegra).micro(regras(iRegra).totalMicro)
            iMicro = regras(iRegra).totalMicro
            VE_PreencheEstrutMicr rsMicro, mediComboValorNull, iRegra, iMicro, estrutRes, indice
        ElseIf iAntib = mediComboValorNull Then
            regras(iRegra).micro(iMicro).totalAntib = regras(iRegra).micro(iMicro).totalAntib + 1
            ReDim Preserve regras(iRegra).micro(iMicro).antib(regras(iRegra).micro(iMicro).totalAntib)
            iAntib = regras(iRegra).micro(iMicro).totalAntib
            VE_PreencheEstrutAntib rsMicro, mediComboValorNull, iRegra, iMicro, iAntib
        ElseIf iSensib = mediComboValorNull Then
            regras(iRegra).micro(iMicro).antib(iAntib).totalSensib = regras(iRegra).micro(iMicro).antib(iAntib).totalSensib + 1
            ReDim Preserve regras(iRegra).micro(iMicro).antib(iAntib).Sensib(regras(iRegra).micro(iMicro).antib(iAntib).totalSensib)
            iSensib = regras(iRegra).micro(iMicro).antib(iAntib).totalSensib
            VE_PreencheEstrutSensib rsMicro, mediComboValorNull, iRegra, iMicro, iAntib, iSensib
        End If
        
        rsMicro.MoveNext
    Wend
    rsMicro.Close
    Set rsMicro = Nothing
    
    
    VE_PreencheRegrasMicro = True
Exit Function
TrataErro:
    VE_PreencheRegrasMicro = False
    BG_LogFile_Erros "Erro ao Verificar Regras  VE2: " & Err.Description, "VE", "VE_PreencheRegrasMicro", False
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM CABE�ALHO DAS REGRAS

' --------------------------------------------------------------------------------------------------------------------
Private Sub VE_PreencheEstrutRegras(ByVal rsT As ADODB.recordset, seq_realiza As Long, iRegra As Integer, estrutRes() As Resultados, indice As Integer)
    Dim iAna As Integer
    On Error GoTo TrataErro
    
    regras(iRegra).cod_gr_epid = BL_HandleNull(rsT!cod_gr_epid, mediComboValorNull)
    regras(iRegra).seq_regra = ""
    regras(iRegra).dt_cri = ""
    regras(iRegra).hr_cri = ""
    regras(iRegra).user_cri = ""
    regras(iRegra).cod_estado_ve = ""
    
    
    regras(iRegra).cod_regra = BL_HandleNull(rsT!cod_regra_ve, mediComboValorNull)
    regras(iRegra).descr_regra = BL_HandleNull(rsT!descr_regra_ve, "")
    regras(iRegra).seq_realiza = seq_realiza
    
    regras(iRegra).totalMicro = 1
    ReDim regras(iRegra).micro(regras(iRegra).totalMicro)
    
    regras(iRegra).totalAna = 0
    ReDim regras(iRegra).ana(regras(iRegra).totalAna)
    
    regras(iRegra).totalProd = 0
    ReDim regras(iRegra).prod(regras(iRegra).totalProd)
    
    VE_PreencheEstrutProdutos seq_realiza, iRegra
    If BL_HandleNull(rsT!tipo_Regra, 0) = 0 Then
        VE_PreencheEstrutMicr rsT, seq_realiza, iRegra, regras(iRegra).totalMicro, estrutRes, indice
    Else
        regras(iRegra).totalAna = regras(iRegra).totalAna + 1
        iAna = regras(iRegra).totalAna
        ReDim Preserve regras(iRegra).ana(iAna)
        regras(iRegra).ana(iAna).cod_analise = BL_HandleNull(rsT!cod_ana_s, "")
        regras(iRegra).ana(iAna).descr_analise = BL_HandleNull(rsT!descr_ana_s, "")
        regras(iRegra).ana(iAna).cod_sinal = BL_HandleNull(rsT!cod_sinal, mediComboValorNull)
        regras(iRegra).ana(iAna).t_res = BL_HandleNull(rsT!t_res, mediComboValorNull)
        regras(iRegra).ana(iAna).totalFrases = 0
        ReDim regras(iRegra).ana(iAna).frases(regras(iRegra).ana(iAna).totalFrases)
        If regras(iRegra).ana(iAna).t_res = gT_Alfanumerico Then
            regras(iRegra).ana(iAna).resAlfan = BL_HandleNull(rsT!resultado, "")
        ElseIf regras(iRegra).ana(iAna).t_res = gT_Frase Then
            regras(iRegra).ana(iAna).resAlfan = ""
            VE_PreencheRegrasAnaFrase iRegra, iAna
        End If
        regras(iRegra).ana(iAna).flg_validade = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheEstrutRegras: " & Err.Description, "VE", "VE_PreencheEstrutRegras", False
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM DADOS DO MICRORGANISMO

' --------------------------------------------------------------------------------------------------------------------
Private Sub VE_PreencheEstrutMicr(ByVal rsMicro As ADODB.recordset, seq_realiza As Long, iRegra As Integer, iMicro As Integer, estrutRes() As Resultados, indice As Integer)
    On Error GoTo TrataErro
    
    regras(iRegra).micro(iMicro).cod_microrg = BL_HandleNull(rsMicro!cod_microrg, "")
    regras(iRegra).micro(iMicro).descr_microrg = BL_HandleNull(rsMicro!descr_microrg, "")
    regras(iRegra).micro(iMicro).num_min_classes = BL_HandleNull(rsMicro!num_classes, mediComboValorNull)
    regras(iRegra).micro(iMicro).flg_nao_avalia_sensib = BL_HandleNull(rsMicro!flg_nao_avalia_sensib, 0)
    regras(iRegra).micro(iMicro).num_classes_sensiv = 0
    regras(iRegra).micro(iMicro).totalAntib = 1
    ReDim regras(iRegra).micro(iMicro).antib(regras(iRegra).micro(iMicro).totalAntib)
    
    regras(iRegra).micro(iMicro).totalSubClasses = 0
    ReDim regras(iRegra).micro(iMicro).subClasses(regras(iRegra).micro(iMicro).totalSubClasses)
    
    VE_PreencheEstrutAntib rsMicro, seq_realiza, iRegra, iMicro, regras(iRegra).micro(iMicro).totalAntib
    If seq_realiza > mediComboValorNull Then
        VE_PreencheEstrutRes seq_realiza, iRegra, iMicro
    Else
        VE_PreencheEstrutRes2 iRegra, iMicro, estrutRes, indice
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheEstrutMicr: " & Err.Description, "VE", "VE_PreencheEstrutMicr", False
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM DADOS DOS ANTIBIOTICOS

' --------------------------------------------------------------------------------------------------------------------
Private Sub VE_PreencheEstrutAntib(ByVal rsMicro As ADODB.recordset, seq_realiza As Long, iRegra As Integer, iMicro As Integer, iAntib As Integer)
    On Error GoTo TrataErro
    
    regras(iRegra).micro(iMicro).antib(iAntib).cod_antibio = BL_HandleNull(rsMicro!cod_antibio, "")
    regras(iRegra).micro(iMicro).antib(iAntib).descr_antibio = BL_HandleNull(rsMicro!descr_antibio, "")
    regras(iRegra).micro(iMicro).antib(iAntib).totalSensib = 1
    ReDim regras(iRegra).micro(iMicro).antib(iAntib).Sensib(regras(iRegra).micro(iMicro).antib(iAntib).totalSensib)
    
    VE_PreencheEstrutSensib rsMicro, seq_realiza, iRegra, iMicro, iAntib, regras(iRegra).micro(iMicro).antib(iAntib).totalSensib
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheEstrutAntib: " & Err.Description, "VE", "VE_PreencheEstrutAntib", False
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM DADOS DAS SENSIBILIDADES

' --------------------------------------------------------------------------------------------------------------------
Private Sub VE_PreencheEstrutSensib(ByVal rsMicro As ADODB.recordset, seq_realiza As Long, iRegra As Integer, iMicro As Integer, iAntib As Integer, iSensib As Integer)
    On Error GoTo TrataErro
    
    regras(iRegra).micro(iMicro).antib(iAntib).Sensib(iSensib).cod_sensib = BL_HandleNull(rsMicro!sensibilidade, "")
    regras(iRegra).micro(iMicro).antib(iAntib).Sensib(iSensib).descr_sensib = regras(iRegra).micro(iMicro).antib(iAntib).Sensib(iSensib).cod_sensib
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheEstrutSensib: " & Err.Description, "VE", "VE_PreencheEstrutSensib", False
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM RESULTADOS DO SEQ_REALIZA INDICADO

' --------------------------------------------------------------------------------------------------------------------
Private Sub VE_PreencheEstrutRes(seq_realiza, iRegra As Integer, iMicro As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim i As Integer
    Dim iClasse As Integer
    Dim flgClasse As Boolean
    Dim auxProd As String
    Dim rsAntib As New ADODB.recordset
    
    sSql = "SELECT x2.cod_antibio, x2.descR_antibio, x3.cod_subclasse_antibio, x3.descR_subclasse_antibio, x1.res_sensib "
    sSql = sSql & " FROM sl_res_tsq x1, sl_antibio x2 LEFT OUTER JOIN sl_subclasse_antibio x3  ON x2.cod_subclasse_antibio = x3.cod_subclasse_antibio "
    sSql = sSql & " WHERE x1.seq_realiza = " & seq_realiza & " AND x1.cod_micro = " & BL_TrataStringParaBD(regras(iRegra).micro(iMicro).cod_microrg)
    sSql = sSql & " AND x1.cod_antib = x2.cod_antibio"
    If regras(iRegra).totalProd > 0 Then
        auxProd = ""
        For i = 1 To regras(iRegra).totalProd
            If i > 1 Then auxProd = auxProd & ","
            auxProd = auxProd & BL_TrataStringParaBD(regras(iRegra).prod(i).cod_produto)
        Next
        sSql = sSql & " AND seq_realiza in (Select seq_realiza from sl_res_micro WHERE seq_realiza in "
        sSql = sSql & " (SELECT seq_realiza from sl_realiza WHERE cod_agrup IN("
        sSql = sSql & " SELECT cod_ana from slv_analises where cod_produto IN (" & auxProd & "))))"
    End If
    rsAntib.CursorType = adOpenStatic
    rsAntib.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAntib.Open sSql, gConexao
    While Not rsAntib.EOF
    
        'CRIA CLASSES DE ANTIBIOTICOS
        flgClasse = False
        For iClasse = 1 To regras(iRegra).micro(iMicro).totalSubClasses
            If regras(iRegra).micro(iMicro).subClasses(iClasse).cod_subclasse = BL_HandleNull(rsAntib!cod_subclasse_antibio, "") Then
                flgClasse = True
                Exit For
            End If
        Next iClasse
        If flgClasse = False Then
            regras(iRegra).micro(iMicro).totalSubClasses = regras(iRegra).micro(iMicro).totalSubClasses + 1
            ReDim Preserve regras(iRegra).micro(iMicro).subClasses(regras(iRegra).micro(iMicro).totalSubClasses)
            iClasse = regras(iRegra).micro(iMicro).totalSubClasses
            regras(iRegra).micro(iMicro).subClasses(iClasse).cod_subclasse = BL_HandleNull(rsAntib!cod_subclasse_antibio, "")
            regras(iRegra).micro(iMicro).subClasses(iClasse).descr_subclasse = BL_HandleNull(rsAntib!descr_subclasse_antibio, "")
            regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib = 0
            ReDim regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib)
        End If
        
        regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib = regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib + 1
        ReDim Preserve regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib)
        regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib).cod_antibio = BL_HandleNull(rsAntib!cod_antibio, "")
        regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib).res_sensib = BL_HandleNull(rsAntib!res_sensib, "")
        rsAntib.MoveNext
    Wend
    rsAntib.Close
    Set rsAntib = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheEstrutRes: " & Err.Description, "VE", "VE_PreencheEstrutRes", False
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM RESULTADOS DA ESTRUTURA DE ANTIBIOTICOS

' --------------------------------------------------------------------------------------------------------------------
Private Sub VE_PreencheEstrutRes2(iRegra As Integer, iMicro As Integer, estrutRes() As Resultados, indice As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim i As Integer
    Dim iClasse As Integer
    Dim flgClasse As Boolean
    Dim auxProd As String
    Dim rsAntib As New ADODB.recordset
    Dim j As Integer
    Dim CodProduto As String
    Dim flg_prod As Boolean
    
'    codProduto = BL_HandleNull(estrutAnalises(estrutRes(indice).idxP).cod_prod, BL_HandleNull(estrutAnalises(estrutRes(indice).idxP).EstrutComplexas(estrutRes(indice).idxC).cod_prod, _
'                  estrutAnalises(estrutRes(indice).idxP).EstrutComplexas(estrutRes(indice).idxC).EstrutSimples(estrutRes(indice).idxS).CodProd))
'    If regras(iRegra).totalProd > 0 Then
'        For i = 1 To regras(iRegra).totalProd
'            If codProduto = regras(iRegra).prod(i).cod_produto Then
'                flg_prod = True
'                Exit For
'            End If
'        Next i
'    Else
'        flg_prod = True
'    End If
'    If flg_prod = False Then
'        Exit Sub
'    End If
    
    For j = 1 To estrutRes(indice).totalAntib
    
        sSql = "SELECT x2.cod_antibio, x2.descR_antibio, x3.cod_subclasse_antibio, x3.descR_subclasse_antibio "
        sSql = sSql & " FROM  sl_antibio x2 LEFT OUTER JOIN sl_subclasse_antibio x3  ON x2.cod_subclasse_antibio = x3.cod_subclasse_antibio "
        sSql = sSql & " WHERE x2.cod_antibio = " & BL_TrataStringParaBD(estrutRes(indice).antibioticos(j).codAntib)
        rsAntib.CursorType = adOpenStatic
        rsAntib.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAntib.Open sSql, gConexao
        While Not rsAntib.EOF
        
            'CRIA CLASSES DE ANTIBIOTICOS
            flgClasse = False
            For iClasse = 1 To regras(iRegra).micro(iMicro).totalSubClasses
                If regras(iRegra).micro(iMicro).subClasses(iClasse).cod_subclasse = BL_HandleNull(rsAntib!cod_subclasse_antibio, "") Then
                    flgClasse = True
                    Exit For
                End If
            Next iClasse
            If flgClasse = False Then
                regras(iRegra).micro(iMicro).totalSubClasses = regras(iRegra).micro(iMicro).totalSubClasses + 1
                ReDim Preserve regras(iRegra).micro(iMicro).subClasses(regras(iRegra).micro(iMicro).totalSubClasses)
                iClasse = regras(iRegra).micro(iMicro).totalSubClasses
                regras(iRegra).micro(iMicro).subClasses(iClasse).cod_subclasse = BL_HandleNull(rsAntib!cod_subclasse_antibio, "")
                regras(iRegra).micro(iMicro).subClasses(iClasse).descr_subclasse = BL_HandleNull(rsAntib!descr_subclasse_antibio, "")
                regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib = 0
                ReDim regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib)
            End If
            
            regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib = regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib + 1
            ReDim Preserve regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib)
            regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib).cod_antibio = BL_HandleNull(rsAntib!cod_antibio, "")
            regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib).res_sensib = estrutRes(indice).antibioticos(j).sensibilidade
            rsAntib.MoveNext
        Wend
        rsAntib.Close
    Next j
    Set rsAntib = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheEstrutRes2: " & Err.Description, "VE", "VE_PreencheEstrutRes2", False
    Exit Sub
    Resume Next
End Sub


' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA DOS PRODUTOS ASSOCIADOS A CADA REGRA

' --------------------------------------------------------------------------------------------------------------------
Private Sub VE_PreencheEstrutProdutos(seq_realiza, iRegra As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    
    sSql = "SELECT sl_produto.cod_produto, sl_produto.descr_produto FROM sl_cod_regras_ve_prod, sl_produto "
    sSql = sSql & " WHERE cod_regra_ve = " & regras(iRegra).cod_regra & " AND sl_cod_regras_ve_prod.cod_produto = sl_produto.cod_produto "
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    While Not RsProd.EOF
        regras(iRegra).totalProd = regras(iRegra).totalProd + 1
        ReDim Preserve regras(iRegra).prod(regras(iRegra).totalProd)
        regras(iRegra).prod(regras(iRegra).totalProd).cod_produto = BL_HandleNull(RsProd!cod_produto, "")
        regras(iRegra).prod(regras(iRegra).totalProd).descr_produto = BL_HandleNull(RsProd!descr_produto, "")
        RsProd.MoveNext
    Wend
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheEstrutProdutos: " & Err.Description, "VE", "VE_PreencheEstrutProdutos", False
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ANALISES ASSOCIADA A CADA REGRA

' --------------------------------------------------------------------------------------------------------------------
Private Function VE_PreencheRegrasAna(cod_gr_epid As Integer, seq_realiza As Long, estrutRes() As Resultados, indice As Integer, cod_regra_ve As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim RsRegra As New ADODB.recordset
    Dim iRegra As Integer
    Dim iAna As Integer
    Dim i As Integer
    Dim j As Integer
    VE_PreencheRegrasAna = False
    
    sSql = "SELECT distinct x1.cod_regra_ve FROM sl_cod_regras_ve_ana x1, sl_cod_regras_ve x2 WHERE "
    If indice > mediComboValorNull Then
        sSql = sSql & " x1.cod_ana_s = " & BL_TrataStringParaBD(estrutRes(indice).codAnaS)
    Else
        sSql = sSql & " x1.cod_ana_s IN (SELECT cod_ana_s FROM sl_realiza WHERE seq_realiza = " & seq_realiza & ")"
    End If
    sSql = sSql & " AND x2.cod_gr_epid = " & cod_gr_epid
    sSql = sSql & " AND x1.cod_regrA_ve = x2.cod_regra_ve "
    sSql = sSql & " AND (x2.flg_invisivel IS NULL or x2.flg_invisivel = 0) "
    If cod_regra_ve <> "" Then
        sSql = sSql & " AND x1.cod_regra_ve = " & BL_TrataStringParaBD(cod_regra_ve)
    End If
    
    RsRegra.CursorType = adOpenStatic
    RsRegra.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRegra.Open sSql, gConexao
    While Not RsRegra.EOF
        
        sSql = "SELECT distinct x3.cod_regra_ve, x3.descr_regra_ve, x3.cod_gr_epid, "
        sSql = sSql & " x1.cod_ana_s, x2.descr_ana_s, x1.t_res, x1.cod_sinal, x1.resultado, x3.tipo_regra "
        sSql = sSql & " FROM sl_cod_regras_ve_ana x1, sl_ana_s x2, sl_cod_regras_ve x3"
        If indice <= mediComboValorNull Then
            sSql = sSql & ", sl_realiza x4 "
        End If
        sSql = sSql & " WHERE x1.cod_regra_ve = x3.cod_regra_ve AND x1.cod_ana_s = x2.cod_ana_s "
        sSql = sSql & " AND x1.cod_regra_ve = " & BL_TrataStringParaBD(BL_HandleNull(RsRegra!cod_regra_ve, ""))
        
        If cod_regra_ve <> "" Then
            sSql = sSql & " AND x3.cod_regra_ve = " & BL_TrataStringParaBD(cod_regra_ve)
        End If
        
        rsAna.CursorType = adOpenStatic
        rsAna.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        While Not rsAna.EOF
            iRegra = mediComboValorNull
            iAna = mediComboValorNull
            
            For i = 1 To totalRegras
                If regras(i).cod_regra = BL_HandleNull(rsAna!cod_regra_ve, "") Then
                    iRegra = i
                    For j = 1 To regras(i).totalAna
                        If regras(i).ana(j).cod_analise = BL_HandleNull(rsAna!cod_ana_s, "") Then
                            iAna = j
                            Exit For
                        End If
                    Next j
                End If
                Exit For
            Next i
            
            If iRegra = mediComboValorNull Then
                totalRegras = totalRegras + 1
                ReDim Preserve regras(totalRegras)
                iRegra = totalRegras
                VE_PreencheEstrutRegras rsAna, mediComboValorNull, iRegra, estrutRes, indice
            ElseIf iAna = mediComboValorNull Then
                regras(iRegra).totalAna = regras(iRegra).totalAna + 1
                iAna = regras(iRegra).totalAna
                ReDim Preserve regras(iRegra).ana(iAna)
                regras(iRegra).ana(iAna).cod_analise = BL_HandleNull(rsAna!cod_ana_s, "")
                regras(iRegra).ana(iAna).descr_analise = BL_HandleNull(rsAna!descr_ana_s, "")
                regras(iRegra).ana(iAna).cod_sinal = BL_HandleNull(rsAna!cod_sinal, mediComboValorNull)
                regras(iRegra).ana(iAna).t_res = BL_HandleNull(rsAna!t_res, mediComboValorNull)
                regras(iRegra).ana(iAna).totalFrases = 0
                ReDim regras(iRegra).ana(iAna).frases(regras(iRegra).ana(iAna).totalFrases)
                If regras(iRegra).ana(iAna).t_res = gT_Alfanumerico Then
                    regras(iRegra).ana(iAna).resAlfan = BL_HandleNull(rsAna!resultado, "")
                ElseIf regras(iRegra).ana(iAna).t_res = gT_Frase Then
                    regras(iRegra).ana(iAna).resAlfan = ""
                    VE_PreencheRegrasAnaFrase iRegra, iAna
                End If
                regras(iRegra).ana(iAna).flg_validade = False
            End If
            rsAna.MoveNext
        Wend
        rsAna.Close
        
        RsRegra.MoveNext
    Wend
    RsRegra.Close
    Set RsRegra = Nothing
    Set rsAna = Nothing
    VE_PreencheRegrasAna = True
Exit Function
TrataErro:
    VE_PreencheRegrasAna = False
    BG_LogFile_Erros "Erro ao VE_PreencheRegrasAna: " & Err.Description, "VE", "VE_PreencheRegrasAna", False
    Exit Function
    Resume Next
End Function

Private Sub VE_PreencheRegrasAnaFrase(iRegra As Integer, iAna As Integer)
    Dim sSql As String
    Dim rsFrase As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT x2.cod_frase, x2.descr_frase, x2.seq_frase from sl_cod_regras_ve_ana_frase x1, sl_dicionario x2 "
    sSql = sSql & " WHERE cod_regra_Ve = " & regras(iRegra).cod_regra & " AND cod_ana_S = " & BL_TrataStringParaBD(regras(iRegra).ana(iAna).cod_analise)
    sSql = sSql & " AND x1.cod_frase =x2.cod_frase "
    
    rsFrase.CursorType = adOpenStatic
    rsFrase.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFrase.Open sSql, gConexao
    While Not rsFrase.EOF
        regras(iRegra).ana(iAna).totalFrases = regras(iRegra).ana(iAna).totalFrases + 1
        ReDim Preserve regras(iRegra).ana(iAna).frases(regras(iRegra).ana(iAna).totalFrases)
        regras(iRegra).ana(iAna).frases(regras(iRegra).ana(iAna).totalFrases).cod_frase = BL_HandleNull(rsFrase!cod_frase, "")
        regras(iRegra).ana(iAna).frases(regras(iRegra).ana(iAna).totalFrases).descr_frase = BL_HandleNull(rsFrase!descr_frase, "")
        rsFrase.MoveNext
    Wend
    
    rsFrase.Close
    Set rsFrase = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao VE_PreencheRegrasAnaFrase: " & Err.Description, "VE", "VE_PreencheRegrasAnaFrase", False
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------------------------------------------------------------------

'EDGAR PARADA

' --------------------------------------------------------------------------------------------------------------------
Private Function VE_AvaliaRegras(cod_gr_epid As Integer, seq_realiza As Long, ByRef estrutRes() As Resultados, _
                                 indice As Integer, cod_regra_ve As String, totalRes As Integer, indiceVe As Integer) As Boolean
    Dim iRegra As Integer
    Dim iMicro As Integer
    Dim iAntib As Integer
    Dim iResAntib As Integer
    Dim iSensib As Integer
    Dim iAna As Integer
    Dim iClasse As Integer
    Dim flg_classes As Boolean
    'Dim iRes As Integer
    Dim iFrase As Integer
    Dim flg_EncontrouAna As Boolean
    Dim iVe As Integer
    On Error GoTo TrataErro
    
    VE_AvaliaRegras = False
    
    totalRegras = 0
    ReDim regras(totalRegras)
    
    
    If VE_PreencheRegrasMicro(cod_gr_epid, seq_realiza, estrutRes, indice, cod_regra_ve) = False Then
        Exit Function
    End If
    
    If VE_PreencheRegrasAna(cod_gr_epid, seq_realiza, estrutRes, indice, cod_regra_ve) = False Then
        Exit Function
    End If
    For iRegra = 1 To totalRegras
    VE_AvaliaRegras = False
        ' AVALIA REGRAS DE ANTIBIOTICOS
        For iMicro = 1 To regras(iRegra).totalMicro
            If regras(iRegra).totalProd = 0 Or VE_VerificaProd(estrutRes, indice, iRegra) = True Then
                If regras(iRegra).micro(iMicro).flg_nao_avalia_sensib = mediSim Then
                    VE_AvaliaRegras = True
                End If
            
                For iClasse = 1 To regras(iRegra).micro(iMicro).totalSubClasses
                    flg_classes = False
                    For iResAntib = 1 To regras(iRegra).micro(iMicro).subClasses(iClasse).totalResAntib
                    
                        ' VERIFICA SE SUBCLASSE E SENSIVEL (BASTA TER UM ANTIBIOTICO SENSIVEL DENTRO DA SUBCLASSE)
                        If regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(iResAntib).res_sensib = "S" Then
                            If flg_classes = False Then
                                flg_classes = True
                                regras(iRegra).micro(iMicro).num_classes_sensiv = regras(iRegra).micro(iMicro).num_classes_sensiv + 1
                            End If
                        End If
                        
                        ' COMPRARA ANTIBIOTICOS CODIFICADOS NA REGRA COM OS DA ESTRUTURA OU BD.
                        For iAntib = 1 To regras(iRegra).micro(iMicro).totalAntib
                            If regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(iResAntib).cod_antibio = regras(iRegra).micro(iMicro).antib(iAntib).cod_antibio Then
                                For iSensib = 1 To regras(iRegra).micro(iMicro).antib(iAntib).totalSensib
                                    If regras(iRegra).micro(iMicro).subClasses(iClasse).ResAntib(iResAntib).res_sensib = regras(iRegra).micro(iMicro).antib(iAntib).Sensib(iSensib).cod_sensib Then
                                        VE_AvaliaRegras = True
                                    End If
                                Next iSensib
                            End If
                        Next iAntib
                    Next iResAntib
                Next iClasse
                
                'NUMERO DE SUBCLASSES SENSIVEIS � MENOR DO QUE PERMITIDO NA REGRA
                If regras(iRegra).micro(iMicro).cod_microrg <> "" And regras(iRegra).micro(iMicro).num_classes_sensiv <= regras(iRegra).micro(iMicro).num_min_classes And _
                    regras(iRegra).micro(iMicro).num_min_classes >= 0 And regras(iRegra).micro(iMicro).totalSubClasses > 0 Then
                        VE_AvaliaRegras = True
                End If
            
                If VE_AvaliaRegras = True Then
                    If indice > 0 Then
                        If indiceVe = mediComboValorNull Then
                            estrutRes(indice).totalVE = estrutRes(indice).totalVE + 1
                            ReDim Preserve estrutRes(indice).vigEpid(estrutRes(indice).totalVE)
                            iVe = estrutRes(indice).totalVE
                        Else
                            iVe = indiceVe
                        End If
                        If estrutRes(indice).vigEpid(iVe).seq_vig_epid = "" Then
                            estrutRes(indice).vigEpid(iVe).flg_manual = mediNao
                            estrutRes(indice).vigEpid(iVe).cod_estado_ve = gEstadoVePendente
                            estrutRes(indice).vigEpid(iVe).cod_micro = regras(iRegra).micro(iMicro).cod_microrg
                            estrutRes(indice).vigEpid(iVe).cod_regra_ve = regras(iRegra).cod_regra
                            estrutRes(indice).vigEpid(iVe).descr_regra_ve = regras(iRegra).descr_regra
                            estrutRes(indice).vigEpid(iVe).flg_mostra_aviso = BL_HandleNull(BL_SelCodigo("SL_COD_GR_EPID", "FLG_AVALIACAO_CONTINUA", "COD_GR_EPID", regras(iRegra).cod_gr_epid), 0)
                        End If
                    End If
                End If
            End If
        Next iMicro
        
        ' AVALIA RESULTADOS DAS ANALISES
        'edgar.parada Glintt-HS-19848 03.12.2018
        For iAna = 1 To regras(iRegra).totalAna
            'For iRes = 1 To totalRes
              ' flg_EncontrouAna = False
                If estrutRes(indice).codAnaS = regras(iRegra).ana(iAna).cod_analise Then
                    'flg_EncontrouAna = True
                    For iFrase = 1 To regras(iRegra).ana(iAna).totalFrases
                        If regras(iRegra).ana(iAna).frases(iFrase).cod_frase <> "" Then
                            If estrutRes(indice).cod_frase = regras(iRegra).ana(iAna).frases(iFrase).cod_frase Then
                                regras(iRegra).ana(iAna).flg_validade = True
                                Exit For
                           End If
                        End If
                    Next iFrase
                    'If regras(iRegra).ana(iAna).flg_validade = True Then
                   '     Exit For
                  '  End If
                   
                   
                    If regras(iRegra).ana(iAna).resAlfan <> "" And estrutRes(indice).res <> "" Then
                      'edgar.parada CHSJ-4371 04.09.2019 -Valida se � double/number
                      If BL_ValidaNumberDbl(estrutRes(indice).res) And BL_ValidaNumberDbl(regras(iRegra).ana(iAna).resAlfan) Then
                            Select Case regras(iRegra).ana(iAna).cod_sinal
                               Case 1 'sinal <
                                  If CDbl(Replace(estrutRes(indice).res, ".", ",")) < CDbl(Replace(regras(iRegra).ana(iAna).resAlfan, ".", ",")) Then
                                    regras(iRegra).ana(iAna).flg_validade = True
                        
                                  End If
                                Case 2 'Sinal >
                                  If CDbl(Replace(estrutRes(indice).res, ".", ",")) > CDbl(Replace(regras(iRegra).ana(iAna).resAlfan, ".", ",")) Then
                                    regras(iRegra).ana(iAna).flg_validade = True
                        
                                  End If
                                Case 3 'Sinal <=
                                  If CDbl(Replace(estrutRes(indice).res, ".", ",")) <= CDbl(Replace(regras(iRegra).ana(iAna).resAlfan, ".", ",")) Then
                                    regras(iRegra).ana(iAna).flg_validade = True
                        
                                  End If
                                Case 4 'Sinal >=
                                  If CDbl(Replace(estrutRes(indice).res, ".", ",")) >= CDbl(Replace(regras(iRegra).ana(iAna).resAlfan, ".", ",")) Then
                                    regras(iRegra).ana(iAna).flg_validade = True
                        
                                  End If
                                Case 5 'Sinal =
                                  If CDbl(Replace(estrutRes(indice).res, ".", ",")) = CDbl(Replace(regras(iRegra).ana(iAna).resAlfan, ".", ",")) Then
                                    regras(iRegra).ana(iAna).flg_validade = True
                        
                                  End If
                                Case Else
                                
                            End Select
                      Else
                      'edgar.parada CHSJ-4371 04.09.2019 - caso de String, apenas sinnal =
                           Select Case regras(iRegra).ana(iAna).cod_sinal
                                Case 5 'Sinal =
                                  If UCase(estrutRes(indice).res) = UCase(regras(iRegra).ana(iAna).resAlfan) Then
                                    regras(iRegra).ana(iAna).flg_validade = True
                                  End If
                                Case Else
                            End Select
                      End If
                      '
                    End If
                    '
                End If
            'Next iRes
            'If flg_EncontrouAna = False Then
               ' If VE_VerificaAnaliseBD(estrutRes(indice).n_req, regras(iRegra).ana(iAna).cod_analise, regras(iRegra).ana(iAna).frases, _
                                        regras(iRegra).ana(iAna).totalFrases, regras(iRegra).ana(iAna).resAlfan) = True Then
                  '  regras(iRegra).ana(iAna).flg_validade = True
               ' End If
            'End If
            'If regras(iRegra).ana(iAna).flg_validade = False Then
                'Exit For
           ' End If
          '  If iAna = regras(iRegra).totalAna Then
          'Else
        If regras(iRegra).ana(iAna).flg_validade = True Then
                VE_AvaliaRegras = True
                
                If indiceVe = mediComboValorNull Then
                    estrutRes(indice).totalVE = estrutRes(indice).totalVE + 1
                    ReDim Preserve estrutRes(indice).vigEpid(estrutRes(indice).totalVE)
                    iVe = estrutRes(indice).totalVE
                Else
                    iVe = indiceVe
                End If
                If estrutRes(indice).vigEpid(iVe).seq_vig_epid = "" Then
                    estrutRes(indice).vigEpid(iVe).flg_manual = mediNao
                    estrutRes(indice).vigEpid(iVe).cod_estado_ve = gEstadoVePendente
                    estrutRes(indice).vigEpid(iVe).cod_micro = ""
                    estrutRes(indice).vigEpid(iVe).cod_regra_ve = regras(iRegra).cod_regra
                    estrutRes(indice).vigEpid(iVe).descr_regra_ve = regras(iRegra).descr_regra
                    estrutRes(indice).vigEpid(iVe).flg_mostra_aviso = BL_HandleNull(BL_SelCodigo("SL_COD_GR_EPID", "FLG_AVALIACAO_CONTINUA", "COD_GR_EPID", regras(iRegra).cod_gr_epid), 0)
                    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
                    estrutRes(indice).vigEpid(iVe).GrEpid = cod_gr_epid
                    '
                End If
            End If
        Next iAna
        
        
    Next iRegra
    
    
Exit Function
TrataErro:
    VE_AvaliaRegras = False
    BG_LogFile_Erros "Erro ao avaliar Regras Micro VE: " & Err.Description, "VE", "VE_AvaliaRegras", False
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------

'

' --------------------------------------------------------------------------------------------------------------------
Public Function VE_VerificaGruposEpidAutom(seq_realiza As Long, ByRef estrutRes() As Resultados, ByVal indice As Integer, _
                                           ByVal cod_regra_ve As String, ByVal totalRes As Integer, iVe As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsGr As New ADODB.recordset
    VE_VerificaGruposEpidAutom = False
    
    ' VERIFICA SE CARACTERIZA��O DO MICRORGANISMO � PARA VERIFICAR VIGILANCIA
    If estrutRes(indice).micro_codigo <> "" Then
        If MICRO_VerificaCaracMicroVE(estrutRes(indice).micro_cod_carac) = False Then
           Exit Function
        End If
        If estrutRes(indice).micro_flg_imp = "N" Then
            Exit Function
        End If
    End If
    sSql = "SELECT cod_gr_epid FROM sl_cod_gr_epid WHERE (flg_invisivel IS NULL OR flg_invisivel = 0)"
    RsGr.CursorType = adOpenStatic
    RsGr.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsGr.Open sSql, gConexao
    While Not RsGr.EOF
        If VE_AvaliaRegras(BL_HandleNull(RsGr!cod_gr_epid, 0), seq_realiza, estrutRes, indice, cod_regra_ve, totalRes, iVe) = True Then
            VE_VerificaGruposEpidAutom = True
        End If
        RsGr.MoveNext
    Wend
    RsGr.Close
    Set RsGr = Nothing
Exit Function
TrataErro:
    VE_VerificaGruposEpidAutom = False
    BG_LogFile_Erros "Erro VE_VerificaGruposEpidAutom: " & Err.Description, "VE", "VE_VerificaGruposEpidAutom", False
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------

'

' --------------------------------------------------------------------------------------------------------------------
Public Function VE_InereRegistoBD(ByRef estrutRes() As Resultados, iRes As Long, seq_realiza As Long) As Boolean
    Dim sSql As String
    Dim iReg As Integer
    Dim iVe As Integer
    Dim seq_existente As String
    On Error GoTo TrataErro
    VE_InereRegistoBD = False
    If estrutRes(iRes).totalVE <= 0 Then
        VE_InereRegistoBD = True
        Exit Function
    End If
    For iVe = 1 To estrutRes(iRes).totalVE
        If estrutRes(iRes).vigEpid(iVe).flg_invisivel = 1 Then
            sSql = "UPDATE sl_vig_epid set flg_invisivel = 1 WHERE seq_vig_epid = " & BL_TrataStringParaBD(estrutRes(iRes).vigEpid(iVe).seq_vig_epid_apagar)
            BG_ExecutaQuery_ADO sSql
            If estrutRes(iRes).vigEpid(iVe).flg_manual = mediNao Then
                estrutRes(iRes).vigEpid(iVe).seq_vig_epid_apagar = ""
                estrutRes(iRes).vigEpid(iVe).flg_invisivel = 0
            End If
        End If
        
        If estrutRes(iRes).vigEpid(iVe).seq_vig_epid = "" And estrutRes(iRes).vigEpid(iVe).cod_regra_ve <> "" Then
            seq_existente = VE_VerificaRegraJaDetetada(estrutRes, iRes, iVe, estrutRes(iRes).vigEpid(iVe).cod_regra_ve, estrutRes(iRes).seqRealiza, _
                                                        estrutRes(iRes).vigEpid(iVe).cod_micro)
            If seq_existente = "" Then
                estrutRes(iRes).vigEpid(iVe).seq_vig_epid = GUID_GET()
                estrutRes(iRes).vigEpid(iVe).user_cri = CStr(gCodUtilizador)
                estrutRes(iRes).vigEpid(iVe).dt_cri = Bg_DaData_ADO
                estrutRes(iRes).vigEpid(iVe).hr_cri = Bg_DaHora_ADO
                estrutRes(iRes).vigEpid(iVe).cod_estado_ve = gEstadoVePendente
                
                sSql = "INSERT INTO sl_vig_epid(seq_vig_epid, cod_regra_ve, seq_realiza, dt_cri, hr_cri, user_cri,cod_estado_ve, cod_micro, flg_manual) VALUES ("
                sSql = sSql & BL_TrataStringParaBD(estrutRes(iRes).vigEpid(iVe).seq_vig_epid) & ","
                sSql = sSql & BL_TrataStringParaBD(CStr(estrutRes(iRes).vigEpid(iVe).cod_regra_ve)) & ","
                sSql = sSql & estrutRes(iRes).seqRealiza & ","
                sSql = sSql & BL_TrataDataParaBD(estrutRes(iRes).vigEpid(iVe).dt_cri) & ","
                sSql = sSql & BL_TrataStringParaBD(estrutRes(iRes).vigEpid(iVe).hr_cri) & ","
                sSql = sSql & BL_TrataStringParaBD(estrutRes(iRes).vigEpid(iVe).user_cri) & ","
                sSql = sSql & BL_TrataStringParaBD(CStr(estrutRes(iRes).vigEpid(iVe).cod_estado_ve)) & ","
                If estrutRes(iRes).vigEpid(iVe).cod_micro <> "" Then
                    sSql = sSql & BL_TrataStringParaBD(estrutRes(iRes).vigEpid(iVe).cod_micro) & ","
                Else
                    sSql = sSql & "NULL,"
                End If
                sSql = sSql & BL_HandleNull(estrutRes(iRes).vigEpid(iVe).flg_manual, 0) & ")"
                iReg = BG_ExecutaQuery_ADO(sSql)
                If iReg <> 1 Then
                    GoTo TrataErro
                End If
                If VE_InereDetalheRegraBD(estrutRes(iRes).vigEpid(iVe).seq_vig_epid, CStr(estrutRes(iRes).vigEpid(iVe).cod_estado_ve), "", "") = True Then
                    VE_InereRegistoBD = True
                End If
            Else
                VE_InereRegistoBD = True
            End If
        Else
            VE_InereRegistoBD = True
        End If
    Next
Exit Function
TrataErro:
    VE_InereRegistoBD = False
    BG_LogFile_Erros "Erro VE_InereRegistoBD: " & Err.Description, "VE", "VE_InereRegistoBD", False
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------

'

' --------------------------------------------------------------------------------------------------------------------

Private Function VE_InereDetalheRegraBD(seq_regra As String, Estado As String, seq_ve_email As String, seq_ve_bin As String)
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    VE_InereDetalheRegraBD = False

    sSql = "INSERT INTO sl_vig_epid_det(seq_vig_epid, dt_cri, user_cri,cod_estado_ve, seq_ve_email, seq_ve_bin) VALUES ("
    sSql = sSql & BL_TrataStringParaBD(seq_regra) & ","
    sSql = sSql & "sysdate, "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataStringParaBD(Estado) & ","
    sSql = sSql & BL_TrataStringParaBD(seq_ve_email) & ","
    sSql = sSql & BL_TrataStringParaBD(seq_ve_bin) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    VE_InereDetalheRegraBD = True
Exit Function
TrataErro:
    VE_InereDetalheRegraBD = False
    BG_LogFile_Erros "Erro VE_InereDetalheRegraBD: " & Err.Description, "VE", "VE_InereDetalheRegraBD", False
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------

'

' --------------------------------------------------------------------------------------------------------------------

Private Function VE_ActualizaRealiza(iRegra As Integer)
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    VE_ActualizaRealiza = False

    sSql = "UPDATE sl_realiza SET flg_ve = 1 WHERE seq_realiza  = "
    sSql = sSql & regras(iRegra).seq_realiza
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    VE_ActualizaRealiza = True
Exit Function
TrataErro:
    VE_ActualizaRealiza = False
    BG_LogFile_Erros "Erro VE_ActualizaRealiza: " & Err.Description, "VE", "VE_ActualizaRealiza", False
    Exit Function
    Resume Next
End Function


Public Sub VE_PreencheResultadosAvaliacao(Criterio As String, ByRef EstrutGrEpid() As GrEpid, ByRef totalGrEpid As Integer)
    Dim rsVe As New ADODB.recordset
    Dim iGrEpid As Integer
    Dim flgGrEpid As Boolean
    Dim iRegra As Integer
    Dim flgRegra As Boolean
    
    Set rsVe = New ADODB.recordset
    rsVe.CursorType = adOpenStatic
    rsVe.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros Criterio
    rsVe.Open Criterio, gConexao
    If rsVe.RecordCount > 0 Then
        While Not rsVe.EOF
            
            flgGrEpid = False
            For iGrEpid = 1 To totalGrEpid
                If EstrutGrEpid(iGrEpid).cod_gr_epid = BL_HandleNull(rsVe!cod_gr_epid, "") Then
                    flgGrEpid = True
                    Exit For
                End If
            Next iGrEpid
            
            If flgGrEpid = False Then
                totalGrEpid = totalGrEpid + 1
                ReDim Preserve EstrutGrEpid(totalGrEpid)
                
                EstrutGrEpid(totalGrEpid).cod_gr_epid = BL_HandleNull(rsVe!cod_gr_epid, "")
                EstrutGrEpid(totalGrEpid).descr_gr_epid = BL_HandleNull(rsVe!descr_gr_epid, "")
                EstrutGrEpid(totalGrEpid).email_to = BL_HandleNull(rsVe!email_to, "")
                EstrutGrEpid(totalGrEpid).email_cc = BL_HandleNull(rsVe!email_cc, "")
                EstrutGrEpid(totalGrEpid).email_body = BL_HandleNull(rsVe!email_body, "")
                EstrutGrEpid(totalGrEpid).flg_html = BL_HandleNull(rsVe!flg_html, 0)
                EstrutGrEpid(totalGrEpid).corpo_html = BL_HandleNull(rsVe!corpo_html, "")
                EstrutGrEpid(totalGrEpid).descr_imagem_anexo = BL_HandleNull(rsVe!descr_imagem_anexo, "")
                EstrutGrEpid(totalGrEpid).id_lab = BL_HandleNull(rsVe!id_lab, "")
                EstrutGrEpid(totalGrEpid).flg_nome = BL_HandleNull(rsVe!flg_nome, 0)
                EstrutGrEpid(totalGrEpid).flg_req = BL_HandleNull(rsVe!flg_req, 0)
                EstrutGrEpid(totalGrEpid).flg_dt_chega = BL_HandleNull(rsVe!flg_dt_chega, 0)
                EstrutGrEpid(totalGrEpid).flg_proven = BL_HandleNull(rsVe!flg_proven, 0)
                EstrutGrEpid(totalGrEpid).flg_t_sit = BL_HandleNull(rsVe!flg_t_sit, 0)
                EstrutGrEpid(totalGrEpid).flg_dt_nasc = BL_HandleNull(rsVe!flg_dt_nasc, 0)
                EstrutGrEpid(totalGrEpid).flg_sexo = BL_HandleNull(rsVe!flg_sexo, 0)
                EstrutGrEpid(totalGrEpid).flg_sns = BL_HandleNull(rsVe!flg_sns, 0)
                EstrutGrEpid(totalGrEpid).descr_instituicao = BL_HandleNull(rsVe!descr_instituicao, "")
                EstrutGrEpid(totalGrEpid).flg_PT = BL_HandleNull(rsVe!flg_PT, 0)
                EstrutGrEpid(totalGrEpid).flg_produto = BL_HandleNull(rsVe!flg_prod, 0)
                EstrutGrEpid(totalGrEpid).flg_dt_int = BL_HandleNull(rsVe!flg_dt_int, 0)
                EstrutGrEpid(totalGrEpid).flg_idade = BL_HandleNull(rsVe!flg_idade, 0)
                EstrutGrEpid(totalGrEpid).flg_inf_complementar = BL_HandleNull(rsVe!flg_inf_complementar, 0)
                EstrutGrEpid(totalGrEpid).flg_processo = BL_HandleNull(rsVe!flg_processo, 0)
                'NELSONPSILVA 27.09.2018 - CHVNG-9363
                EstrutGrEpid(totalGrEpid).flg_data_geracao = BL_HandleNull(rsVe!flg_data_geracao, 0)
                EstrutGrEpid(totalGrEpid).flg_n_admissoes_inst = BL_HandleNull(rsVe!flg_n_admissoes_inst, 0)
                '
                'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
                EstrutGrEpid(totalGrEpid).tipo_notificacao = BL_HandleNull(rsVe!tipo_notificacao, 0)
                EstrutGrEpid(totalGrEpid).flg_num_sns_SNV = BL_HandleNull(rsVe!flg_envio_num_sns, 0)
                EstrutGrEpid(totalGrEpid).flg_cod_conv_SNV = BL_HandleNull(rsVe!flg_envio_cod_conv, 0)
                '
                EstrutGrEpid(totalGrEpid).totalRegra = 0
                ReDim EstrutGrEpid(totalGrEpid).regras(EstrutGrEpid(totalGrEpid).totalRegra)
                iGrEpid = totalGrEpid
            End If
            flgRegra = False
            For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                If BL_HandleNull(rsVe!cod_regra_ve, "") <> "" Then
                    If BL_HandleNull(rsVe!cod_regra_ve, "") = EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra Then
                        flgRegra = True
                        Exit For
                    End If
                End If
            Next iRegra
            If flgRegra = False Then
                EstrutGrEpid(iGrEpid).totalRegra = EstrutGrEpid(iGrEpid).totalRegra + 1
                ReDim Preserve EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra)
                EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).cod_regra = BL_HandleNull(rsVe!cod_regra_ve, "")
                EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).descr_regra = BL_HandleNull(rsVe!descr_regra_ve, "")
                EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).descr_excel = BL_HandleNull(rsVe!descr_excel, "")
                EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).flg_mostra_micro = BL_HandleNull(rsVe!flg_mostra_micro, 0)
                EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).flg_suspeita = BL_HandleNull(rsVe!flg_suspeita, 0)
                EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).totDet = 0
                ReDim EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).detalhes(EstrutGrEpid(iGrEpid).regras(EstrutGrEpid(iGrEpid).totalRegra).totDet)
                iRegra = EstrutGrEpid(iGrEpid).totalRegra
            End If
            EstrutGrEpid(iGrEpid).regras(iRegra).totDet = EstrutGrEpid(iGrEpid).regras(iRegra).totDet + 1
            ReDim Preserve EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet)
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).cod_estado = BL_HandleNull(rsVe!cod_estado_ve, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).cod_ana = BL_HandleNull(rsVe!cod_ana, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).cod_micro = BL_HandleNull(rsVe!cod_microrg, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).descr_ana = BL_HandleNull(rsVe!descr_ana, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).descr_micro = BL_HandleNull(rsVe!descr_microrg, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).descr_estado = BL_HandleNull(rsVe!descr_estado_ve, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).dt_chega = BL_HandleNull(rsVe!dt_chega, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).seq_ve_email = BL_HandleNull(rsVe!seq_ve_email, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).seq_ve_bin = BL_HandleNull(rsVe!seq_ve_bin, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).n_req = BL_HandleNull(rsVe!n_req, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).seq_vig_epid = BL_HandleNull(rsVe!seq_vig_epid, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).seq_realiza = BL_HandleNull(rsVe!seq_realiza, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).descr_carac_micro = BL_HandleNull(rsVe!descr_carac_micro, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).descr_t_sit = BL_HandleNull(rsVe!descr_vig_epid, "")
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).inf_complementar = BL_HandleNull(rsVe!inf_complementar, "")
            'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).id_sinave = BL_HandleNull(rsVe!id_notif_sinave, "")
            '
            'edgar.parada Glintt-HS-19848 03.12.2018
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).enviarResultado1 = BL_HandleNull(rsVe!enviar1res, 0)
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(EstrutGrEpid(iGrEpid).regras(iRegra).totDet).enviarResultado2 = BL_HandleNull(rsVe!enviar2res, 0)
            '
            VE_PreencheEtrutResAntib EstrutGrEpid, iGrEpid, iRegra, EstrutGrEpid(iGrEpid).regras(iRegra).totDet
            VE_PreencheEtrutResAna EstrutGrEpid, iGrEpid, iRegra, EstrutGrEpid(iGrEpid).regras(iRegra).totDet
            VE_PreencheEtrutResProvas EstrutGrEpid, iGrEpid, iRegra, EstrutGrEpid(iGrEpid).regras(iRegra).totDet
            rsVe.MoveNext
        Wend
    End If
    rsVe.Close
    Set rsVe = Nothing
End Sub
Private Sub VE_PreencheEtrutResAntib(EstrutGrEpid() As GrEpid, iGrEpid As Integer, iRegra As Integer, iDet As Integer)
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    
    On Error GoTo TrataErro
    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib = 0
    ReDim EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib)
    
    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_micro <> "" And EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_realiza <> "" Then
        sSql = "SELECT x2.cod_antibio, x2.descr_antibio, x1.res_sensib, x10.cod_subclasse_antibio, x10.descr_subclasse_antibio FROM sl_res_tsq x1 RIGHT OUTER JOIN sl_cod_regras_ve_antib_envio x3 ON "
        sSql = sSql & "  X3.cod_antib = X1.cod_antib And X1.seq_realiza = " & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_realiza
        sSql = sSql & " AND x1.cod_micro = " & BL_TrataStringParaBD(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_micro)
        sSql = sSql & ", sl_antibio x2 LEFT OUTER JOIN sl_subclasse_antibio x10  ON x2.cod_subclasse_antibio = x10.cod_subclasse_antibio"
        sSql = sSql & " WHERE x3.cod_antib = x2.cod_antibio  "
        sSql = sSql & " AND x3.cod_regrA_ve = " & BL_TrataStringParaBD(EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra)
        sSql = sSql & " ORDER BY descr_subclasse_antibio, descr_antibio "
        rsAntib.CursorType = adOpenStatic
        rsAntib.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAntib.Open sSql, gConexao
        If rsAntib.RecordCount > 0 Then
            While Not rsAntib.EOF
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib + 1
                ReDim Preserve EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib)
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).cod_antib = BL_HandleNull(rsAntib!cod_antibio, "")
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).descr_antib = BL_HandleNull(rsAntib!descr_antibio, "")
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).cod_subclasse = BL_HandleNull(rsAntib!cod_subclasse_antibio, "")
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).descr_subclasse = BL_HandleNull(rsAntib!descr_subclasse_antibio, "")
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).res_antib = BL_HandleNull(rsAntib!res_sensib, "")
                 If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).res_antib = "" Then
                    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).res_antib = VE_ProcuraResAntibReferencia( _
                                    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_realiza, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_micro, _
                                    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAntib(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAntib).cod_antib)
                End If
               rsAntib.MoveNext
            Wend
        End If
        rsAntib.Close
        Set rsAntib = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro VE_PreencheEtrutResAntib: " & Err.Description, "VE", "VE_PreencheEtrutResAntib", False
    Exit Sub
    Resume Next
End Sub
Private Sub VE_PreencheEtrutResAna(EstrutGrEpid() As GrEpid, iGrEpid As Integer, iRegra As Integer, iDet As Integer)
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    On Error GoTo TrataErro
    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna = 0
    ReDim EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna)
    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_realiza <> "" Then
        sSql = "SELECT x2.cod_ana_s, x2.descr_ana_s, x1.result, x5.cod_frase "
        sSql = sSql & " FROM  sl_cod_regras_ve_Ana_envio x3 LEFT OUTER JOIN sl_realiza x0 ON x3.cod_ana = x0.cod_ana_s"
        sSql = sSql & " AND x0.n_req in (SELECT n_req FROM sl_realiza x8 WHERE x8.seq_realiza = " & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_realiza & ")"
        sSql = sSql & " LEFT OUTER JOIN sl_res_alfan x1 ON x0.seq_realiza = x1.seq_realiza"
        sSql = sSql & " LEFT OUTER JOIN sl_res_frase x5 ON x0.seq_realiza = x5.seq_realiza, sl_ana_s x2 "
        sSql = sSql & " WHERE  x3.cod_ana = x2.cod_ana_s "
        sSql = sSql & " AND x3.cod_regrA_ve = " & BL_TrataStringParaBD(EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra)
        sSql = sSql & " ORDER BY x3.ordem, descr_ana_s, result "
        rsAna.CursorType = adOpenStatic
        rsAna.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            While Not rsAna.EOF
                If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna).cod_ana <> BL_HandleNull(rsAna!cod_ana_s, "") Then
                    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna + 1
                    ReDim Preserve EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna)
                End If
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna).cod_ana = BL_HandleNull(rsAna!cod_ana_s, "")
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna).descr_ana = BL_HandleNull(rsAna!descr_ana_s, "")
                If VE_VerificaAnaFraseRestricao(EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra, BL_HandleNull(rsAna!cod_ana_s, ""), BL_HandleNull(rsAna!cod_frase, "")) = True Then
                    If BL_HandleNull(rsAna!Result, "") <> "" Then
                        EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna).resultado = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna).resultado & BL_HandleNull(rsAna!Result, "") & " "
                    Else
                        EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna).resultado = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResAna(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResAna).resultado & BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", BL_HandleNull(rsAna!cod_frase, ""), "V") & " "
                    End If
                End If
                rsAna.MoveNext
            Wend
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro VE_PreencheEtrutResAna: " & Err.Description, "VE", "VE_PreencheEtrutResAna", False
    Exit Sub
    Resume Next
End Sub

Private Sub VE_PreencheEtrutResProvas(EstrutGrEpid() As GrEpid, iGrEpid As Integer, iRegra As Integer, iDet As Integer)
    Dim sSql As String
    Dim rsProvas As New ADODB.recordset
    
    On Error GoTo TrataErro
    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas = 0
    ReDim EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResProvas(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas)
    
    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_micro <> "" And EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_realiza <> "" Then
        sSql = "SELECT x1.cod_Prova, x1.descr_Prova, x0.res_prova "
        sSql = sSql & " FROM sl_prova x1 LEFT OUTER JOIN sl_res_prova x0 ON x1.cod_prova = x0.cod_prova"
        sSql = sSql & " AND x0.seq_realiza = " & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_realiza
        sSql = sSql & " AND x0.cod_micro = " & BL_TrataStringParaBD(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_micro)
        sSql = sSql & " , sl_cod_regras_ve_Provas_envio x3 "
        sSql = sSql & " WHERE x1.cod_prova = x3.cod_prova "
        sSql = sSql & " AND x3.cod_regrA_ve = " & BL_TrataStringParaBD(EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra)
        sSql = sSql & " ORDER BY descr_Prova, res_prova "
        rsProvas.CursorType = adOpenStatic
        rsProvas.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsProvas.Open sSql, gConexao
        If rsProvas.RecordCount > 0 Then
            While Not rsProvas.EOF
                If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResProvas(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas).Cod_Prova <> BL_HandleNull(rsProvas!Cod_Prova, "") Then
                    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas + 1
                    ReDim Preserve EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResProvas(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas)
                End If
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResProvas(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas).Cod_Prova = BL_HandleNull(rsProvas!Cod_Prova, "")
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResProvas(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas).descr_prova = BL_HandleNull(rsProvas!descr_prova, "")
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResProvas(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas).resultado = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).ResProvas(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).totalResProvas).resultado & BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", BL_HandleNull(rsProvas!res_prova, ""), "V") & " "
                rsProvas.MoveNext
            Wend
        End If
        rsProvas.Close
        Set rsProvas = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro VE_PreencheEtrutResProvas: " & Err.Description, "VE", "VE_PreencheEtrutResProvas", False
    Exit Sub
    Resume Next
End Sub


Public Function VE_AtualizaEstado(seq_vig_epid As String, Estado As String, seq_ve_email As String, seq_ve_bin As String) As Boolean
    On Error GoTo TrataErro
    Dim iReg As Integer
    Dim sSql As String
    
    sSql = "UPDATE sl_vig_epid set cod_estado_ve = " & Estado & " WHERE seq_vig_epid = " & BL_TrataStringParaBD(seq_vig_epid)
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    
    If VE_InereDetalheRegraBD(seq_vig_epid, Estado, seq_ve_email, seq_ve_bin) = False Then
        GoTo TrataErro
    End If
    VE_AtualizaEstado = True
Exit Function
TrataErro:
    VE_AtualizaEstado = False
    BG_LogFile_Erros "Erro VE_AtualizaEstado: " & Err.Description, "VE", "VE_AtualizaEstado", False
    Exit Function
    Resume Next
End Function


Public Function VE_CriaFicheiroExcel(caminho As String, NomeFicheiro As String, _
                                     EstrutGrEpid() As GrEpid, iGrEpid As Integer, iRegra As Integer, ByVal ExcelVersion As String) As String
    Dim celulaExcel As Object
    Dim FolhaExcel As Object
    Dim coluna As Integer
    'Constantes de formata��o de Excel
    Const xlEdgeLeft = 7
    Const xlEdgeBottom = 9
    Const xlHAlignCenter = &HFFFFEFF4
    Const xlHAlignLeft = &HFFFFEFDD
    Const xlHAlignRight = &HFFFFEFC8
    Const xlContinuous = 1
    Const xlColorIndexAutomatic = &HFFFFEFF7
    Const xlDash = &HFFFFEFED
    Const xlHairline = 1
    Const xlMedium = &HFFFFEFD6
    On Error GoTo TrataErro
    'Criar ficheiro
    
    'BRUNODSANTOS CHVNG-7389 - 02.06.2016
    Dim ExcelApp As String
    ExcelApp = "Excel.application." & ExcelVersion
    '
    With CreateObject(ExcelApp)
        
        With .Workbooks.Add
            Set FolhaExcel = .Worksheets(1)
            FolhaExcel.Activate
            'BRUNODSANTOS CHVNG-7389 - 02.06.2016
            'FolhaExcel.PageSetup.Orientation = 2
            ' ESCREVE O TITULO EXCEL
            FolhaExcel.range(FolhaExcel.cells(1, 1), FolhaExcel.cells(1, 11)).Merge
            FolhaExcel.range(FolhaExcel.cells(1, 1), FolhaExcel.cells(1, 11)).HorizontalAlignment = 3 ' centro
            Set celulaExcel = FolhaExcel.cells(1, 1)
            celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).descr_excel
            celulaExcel.Font.Bold = True
            celulaExcel.Font.Size = 16
            
            'ESCREVE GRUPO DE VIGILANCIA EPID.
            FolhaExcel.range(FolhaExcel.cells(2, 1), FolhaExcel.cells(2, 11)).Merge
            FolhaExcel.range(FolhaExcel.cells(2, 1), FolhaExcel.cells(2, 11)).HorizontalAlignment = 3 ' centro
            Set celulaExcel = FolhaExcel.cells(2, 1)
            celulaExcel.value = EstrutGrEpid(iGrEpid).descr_gr_epid
            celulaExcel.Font.Bold = True
            celulaExcel.Font.Size = 16
            
            
            ' ESCREVE NOME LABORATORIO
            FolhaExcel.range(FolhaExcel.cells(5, 2), FolhaExcel.cells(5, 7)).Merge
            Set celulaExcel = FolhaExcel.cells(5, 2)
            celulaExcel.value = "Hospital: " & EstrutGrEpid(iGrEpid).descr_instituicao
            celulaExcel.Font.Bold = True
            celulaExcel.Font.Size = 16
            
            'BRUNODSANTOS CHVNG-7389 - 02.06.2016
            If EstrutGrEpid(iGrEpid).flg_data_geracao = mediSim Then
                'DATA GERA��O DO EXCEL
                FolhaExcel.range(FolhaExcel.cells(6, 2), FolhaExcel.cells(6, 3)).Merge
                Set celulaExcel = FolhaExcel.cells(6, 2)
                celulaExcel.value = "Data Notifica��o: "
                celulaExcel.Font.Bold = True
                celulaExcel.Font.Size = 16
                
                FolhaExcel.range(FolhaExcel.cells(6, 4), FolhaExcel.cells(6, 5)).Merge
                Set celulaExcel = FolhaExcel.cells(6, 4)
                celulaExcel.value = CStr(Bg_DaData_ADO)
                celulaExcel.HorizontalAlignment = 2
                celulaExcel.Font.Bold = True
                celulaExcel.Font.Size = 16
            End If
            '
            ' ESCREVE IDENTIFICA��O LABORATORIO
            Set celulaExcel = FolhaExcel.cells(7, 2)
            celulaExcel.value = EstrutGrEpid(iGrEpid).id_lab
            celulaExcel.Font.Bold = True
            celulaExcel.Font.Size = 16
            'BRUNODSANTOS CHVNG-7389 - 02.06.2016
            'celulaExcel.Columns.AutoFit
            'celulaExcel.columnwidth = 40
            '
            FolhaExcel.range(FolhaExcel.cells(9, 1), FolhaExcel.cells(11, 11)).HorizontalAlignment = 3 ' centro
            FolhaExcel.range(FolhaExcel.cells(10, 1), FolhaExcel.cells(11, 10)).RowHeight = 27
            FolhaExcel.range(FolhaExcel.cells(10, 1), FolhaExcel.cells(11, 11)).columnwidth = 9
            
            coluna = 1
                        
            
            ' Nome utente
            If EstrutGrEpid(iGrEpid).flg_nome = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Nome do Doente ou inicias"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' SNS
            If EstrutGrEpid(iGrEpid).flg_sns = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "N�  Sistema Nacional de Sa�de"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' PRORCESSO
            If EstrutGrEpid(iGrEpid).flg_processo = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Processo Hosp."
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' REQ
            If EstrutGrEpid(iGrEpid).flg_req = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "N�  da estirpe no Lab"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' PRODUTO
            If EstrutGrEpid(iGrEpid).flg_produto = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Produto biol�gico"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' DATA INTERNAMENTO
            If EstrutGrEpid(iGrEpid).flg_dt_int = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Data internamento"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' DATA COLHEITA
            If EstrutGrEpid(iGrEpid).flg_dt_chega = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Data colheita"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).columnwidth = 11
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' DATA NASCIMENTO
            If EstrutGrEpid(iGrEpid).flg_dt_nasc = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Data nascimento"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).columnwidth = 11
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            ' IDADE
            If EstrutGrEpid(iGrEpid).flg_idade = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Idade"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                coluna = coluna + 1
            End If
            
            ' INF_COMPLEMENTAR
            If EstrutGrEpid(iGrEpid).flg_inf_complementar = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Diagn�stico"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).columnwidth = 13
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                coluna = coluna + 1
            End If
            
            ' Situacao
            If EstrutGrEpid(iGrEpid).flg_t_sit = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Situa��o"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                coluna = coluna + 1
            End If
            ' SEXO
            If EstrutGrEpid(iGrEpid).flg_sexo = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Sexo"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            
            'BRUNODSANTOS CHVNG-7389 - 02.06.2016
            
            ' Admiss�es Hospitalares
            If EstrutGrEpid(iGrEpid).flg_n_admissoes_inst = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Viagens ou Admiss�es Hospitalares Pr�vias"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            
            ' SERVI�O
            
            If EstrutGrEpid(iGrEpid).flg_proven = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Servi�o ou Enfermaria"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            
            
            ' CODIGO PT
            If EstrutGrEpid(iGrEpid).flg_PT = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "C�digo PT"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                FolhaExcel.range(FolhaExcel.cells(10, coluna), FolhaExcel.cells(11, coluna)).Columns.autofit
                coluna = coluna + 1
            End If
            
            ' ESCREVE DADOS DO UTENTE
            Set celulaExcel = FolhaExcel.cells(9, 1)
            celulaExcel.value = "Dados dos Doentes/Estirpes isoladas"
            celulaExcel.Font.Bold = True
            celulaExcel.Font.Size = 12
            celulaExcel.Interior.color = Cinza
            If coluna > 1 Then
                FolhaExcel.range(FolhaExcel.cells(9, 1), FolhaExcel.cells(9, coluna - 1)).Merge
                FolhaExcel.range(FolhaExcel.cells(9, 1), FolhaExcel.cells(9, coluna - 1)).borderaround xlContinuous
            End If
            

            ' Suspeita
            If EstrutGrEpid(iGrEpid).regras(iRegra).flg_suspeita = mediSim Then
                Set celulaExcel = FolhaExcel.cells(10, coluna)
                celulaExcel.value = "Suspeita: Coloniza��o (C) ou Infe��o (I)"
                FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11, coluna)).Merge
                FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11, coluna)).borderaround xlContinuous
                FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11, coluna)).Font.Size = 9
                FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11, coluna)).Font.Name = "Arial"
                FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11, coluna)).Font.Bold = True
                FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11, coluna)).WrapText = True
                FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11, coluna)).Interior.color = Cinza
                coluna = coluna + 1
            End If
            
                        
            VE_EXCEL_InsereAntib FolhaExcel, EstrutGrEpid, iGrEpid, iRegra, coluna
            Set celulaExcel = Nothing
            Set FolhaExcel = Nothing
            
            .SaveAs caminho & "\" & NomeFicheiro, , , , , , , , False
        End With
        .Quit
    End With
    VE_CriaFicheiroExcel = VE_AdicionaExcelBD(caminho, NomeFicheiro)
    Sleep (200)
    BG_Mensagem mediMsgStatus, "Ficheiro criado com sucesso.", vbInformation, "Excel"
    
Exit Function
TrataErro:
    BG_Mensagem mediMsgBox, "Ficheiro N�o foi criado.", vbExclamation, "Excel"
    BG_LogFile_Erros "Erro VE_CriaFicheiroExcel: " & Err.Description, "VE", "VE_CriaFicheiroExcel", False
    Exit Function
    Resume Next
End Function

Private Sub VE_EXCEL_InsereAntib(FolhaExcel As Object, EstrutGrEpid() As GrEpid, iGrEpid As Integer, iRegra As Integer, colunaAntib As Integer)
   Dim iVe As Integer
    Dim iAntib As Integer
    Dim iProvas As Integer
    Dim iResultados As Integer
    Dim celulaExcel As Object
    Dim sSql  As String
    Dim rsDados As New ADODB.recordset
    Dim totLinhas As Integer
    Dim coluna As Integer
    Dim iSubClasse As Integer
    Dim colunaProva As Integer
    Dim colunaRes As Integer
    Dim colunaMicro As Integer
    On Error GoTo TrataErro
    
    If EstrutGrEpid(iGrEpid).regras(iRegra).flg_mostra_micro = mediSim Then
        colunaMicro = colunaAntib
        Set celulaExcel = FolhaExcel.cells(9, colunaMicro)
        celulaExcel.value = "Esp�cie bacteriana"
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).borderaround xlContinuous
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).Font.Size = 9
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).Font.Name = "Arial"
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).Font.Bold = True
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).WrapText = True
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).Interior.color = Cinza
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).Merge
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).HorizontalAlignment = 3   ' centro
        FolhaExcel.range(FolhaExcel.cells(9, colunaMicro), FolhaExcel.cells(11, colunaMicro)).columnwidth = 24
        colunaAntib = colunaAntib + 1
    End If
    For iAntib = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).totalResAntib
        If iAntib > 1 And EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResAntib(iAntib).cod_subclasse = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResAntib(iAntib - 1).cod_subclasse Then
            FolhaExcel.range(FolhaExcel.cells(9, colunaAntib + iSubClasse - 1), FolhaExcel.cells(9, colunaAntib + iAntib - 1)).Merge
            FolhaExcel.range(FolhaExcel.cells(9, colunaAntib + iSubClasse - 1), FolhaExcel.cells(9, colunaAntib + iAntib - 1)).borderaround xlContinuous
            FolhaExcel.range(FolhaExcel.cells(9, colunaAntib + iSubClasse - 1), FolhaExcel.cells(9, colunaAntib + iAntib - 1)).HorizontalAlignment = 3 ' centro
            FolhaExcel.range(FolhaExcel.cells(9, colunaAntib + iSubClasse - 1), FolhaExcel.cells(9, colunaAntib + iAntib - 1)).Columns.autofit
        Else
            iSubClasse = iAntib
            Set celulaExcel = FolhaExcel.cells(9, colunaAntib + iAntib - 1)
            celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResAntib(iAntib).descr_subclasse
            FolhaExcel.range(FolhaExcel.cells(9, colunaAntib + iSubClasse - 1), FolhaExcel.cells(9, colunaAntib + iAntib - 1)).borderaround xlContinuous
            FolhaExcel.range(FolhaExcel.cells(9, colunaAntib + iSubClasse - 1), FolhaExcel.cells(9, colunaAntib + iAntib - 1)).HorizontalAlignment = 3 ' centro
            FolhaExcel.range(FolhaExcel.cells(9, colunaAntib + iSubClasse - 1), FolhaExcel.cells(9, colunaAntib + iAntib - 1)).Columns.autofit
        End If
        Set celulaExcel = FolhaExcel.cells(11, colunaAntib + iAntib - 1)
        celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResAntib(iAntib).descr_antib
        FolhaExcel.range(FolhaExcel.cells(10, colunaAntib + iAntib - 1), FolhaExcel.cells(11, colunaAntib + iAntib - 1)).Merge
        FolhaExcel.range(FolhaExcel.cells(10, colunaAntib + iAntib - 1), FolhaExcel.cells(11, colunaAntib + iAntib - 1)).borderaround xlContinuous
        FolhaExcel.range(FolhaExcel.cells(11, colunaAntib + iAntib - 1), FolhaExcel.cells(11, colunaAntib + iAntib - 1)).columnwidth = 15
    Next iAntib
    colunaProva = colunaAntib + EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).totalResAntib
    For iProvas = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).totalResProvas
        Set celulaExcel = FolhaExcel.cells(11, colunaProva + iProvas - 1)
        celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResProvas(iProvas).descr_prova
        FolhaExcel.range(FolhaExcel.cells(10, colunaProva + iProvas - 1), FolhaExcel.cells(11, colunaProva + iProvas - 1)).Merge
        FolhaExcel.range(FolhaExcel.cells(10, colunaProva + iProvas - 1), FolhaExcel.cells(11, colunaProva + iProvas - 1)).borderaround xlContinuous
        FolhaExcel.range(FolhaExcel.cells(11, colunaProva + iProvas - 1), FolhaExcel.cells(11, colunaProva + iProvas - 1)).columnwidth = 18
        
    Next iProvas
    colunaRes = colunaProva + EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).totalResProvas
    For iResultados = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).totalResAna
        Set celulaExcel = FolhaExcel.cells(11, colunaRes + iResultados - 1)
        celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResAna(iResultados).descr_ana
        FolhaExcel.range(FolhaExcel.cells(10, colunaRes + iResultados - 1), FolhaExcel.cells(11, colunaRes + iResultados - 1)).Merge
        FolhaExcel.range(FolhaExcel.cells(10, colunaRes + iResultados - 1), FolhaExcel.cells(11, colunaRes + iResultados - 1)).borderaround xlContinuous
        FolhaExcel.range(FolhaExcel.cells(11, colunaRes + iResultados - 1), FolhaExcel.cells(11, colunaRes + iResultados - 1)).columnwidth = 18
        
    Next iResultados
    
    totLinhas = 1
    For iVe = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
    'BRUNODSANTOS CHVNG-7389 -> x1.seq_utente, x1.utente, x1.n_proc1
        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).cod_estado <> gEstadoVeRejeitado Then
            sSql = "SELECT x1.nome_ute, x2.n_req, x2.dt_chega, x4.cod_produto, x1.dt_nasc_ute, x5.descr_proven, "
            sSql = sSql & " x1.sexo_ute, x1.n_cartao_ute, x2.t_sit, x2.n_epis, x1.n_proc_1, x1.seq_utente, x1.utente, x1.n_proc_1 "
            sSql = sSql & " FROM sl_identif x1, sl_Requis x2 LEFT OUTER JOIN sl_proven x5 ON x2.cod_proven = x5.cod_proven, "
            sSql = sSql & " sl_realiza x3 LEFT OUTER JOIN sl_perfis x4 ON x3.cod_perfil = x4.cod_perfis"
            sSql = sSql & " WHERE x1.seq_utente = X2.seq_utente and x3.n_req = x2.n_req AND x3.seq_realiza = " & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).seq_realiza
            rsDados.CursorType = adOpenStatic
            rsDados.CursorLocation = adUseClient
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsDados.Open sSql, gConexao
            coluna = 1
            If rsDados.RecordCount > 0 Then
            
                If EstrutGrEpid(iGrEpid).flg_nome = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = BL_DevolveIniciais(BL_HandleNull(rsDados!nome_ute, ""))
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).flg_sns = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = BL_HandleNull(rsDados!n_cartao_ute, "")
                    celulaExcel.NumberFormat = "0"
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    coluna = coluna + 1
                End If
                
                If EstrutGrEpid(iGrEpid).flg_processo = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = BL_HandleNull(rsDados!n_proc_1, "")
                    celulaExcel.NumberFormat = "0"
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    coluna = coluna + 1
                End If
                
                If EstrutGrEpid(iGrEpid).flg_req = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = BL_HandleNull(rsDados!n_req, "")
                    celulaExcel.NumberFormat = "0"
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).flg_produto = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)

                    'celulaExcel.Value = BL_SeleccionaDescrProduto(BL_HandleNull(rsDados!n_req, ""), BL_HandleNull(rsDados!cod_produto, ""))
                    celulaExcel.value = BL_SelCodigo("SL_PRODUTO", "DESCR_PRODUTO", "COD_PRODUTO", BL_HandleNull(rsDados!cod_produto, ""), "V")
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).flg_dt_int = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.NumberFormat = "@"
                    If BL_HandleNull(rsDados!t_sit, "") = gT_Internamento Then
                        celulaExcel.value = CStr(IR_RetornaDataInternamento(BL_HandleNull(rsDados!n_epis, "")))
                    Else
                        celulaExcel.value = ""
                    End If
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).HorizontalAlignment = 2
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).flg_dt_chega = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.NumberFormat = "@"

                    celulaExcel.value = CStr(BL_HandleNull(rsDados!dt_chega, ""))
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).HorizontalAlignment = 2
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).flg_dt_nasc = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.NumberFormat = "@"
                    celulaExcel.value = CStr(BL_HandleNull(rsDados!dt_nasc_ute, ""))
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).HorizontalAlignment = 2
                    coluna = coluna + 1
                End If
                
                If EstrutGrEpid(iGrEpid).flg_idade = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = BG_CalculaIdade(BL_HandleNull(rsDados!dt_nasc_ute, "01-01-1900"), BL_HandleNull(rsDados!dt_chega, Date))
                    celulaExcel.borderaround xlContinuous
                    coluna = coluna + 1
                End If
                
                If EstrutGrEpid(iGrEpid).flg_inf_complementar = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).inf_complementar
                    celulaExcel.borderaround xlContinuous
                    coluna = coluna + 1
                End If
                
                If EstrutGrEpid(iGrEpid).flg_t_sit = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).descr_t_sit
                    celulaExcel.borderaround xlContinuous
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).flg_sexo = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    If BL_HandleNull(rsDados!sexo_ute, "") = gT_Masculino Then
                        celulaExcel.value = "M"
                    ElseIf BL_HandleNull(rsDados!sexo_ute, "") = gT_Feminino Then
                        celulaExcel.value = "F"
                    End If
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    celulaExcel.borderaround xlContinuous
                    coluna = coluna + 1
                End If
                'BRUNODSANTOS CHVNG-7389 - 03.06.2016
                If EstrutGrEpid(iGrEpid).flg_n_admissoes_inst = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                     celulaExcel.value = BL_HandleNull(VE_RetornaNumAdmissoesHosp(BL_HandleNull(rsDados.Fields("n_proc_1").value, ""), BL_HandleNull(rsDados.Fields("utente").value, ""), BL_HandleNull(rsDados.Fields("seq_utente").value, "")), "")
                     celulaExcel.borderaround xlContinuous
                     FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                     coluna = coluna + 1
                End If
                '
                If EstrutGrEpid(iGrEpid).flg_proven = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = BL_HandleNull(rsDados!descr_proven, "")
                    celulaExcel.borderaround xlContinuous
                    FolhaExcel.range(FolhaExcel.cells(9, coluna), FolhaExcel.cells(11 + totLinhas, coluna)).Columns.autofit
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).flg_PT = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = "A"
                    celulaExcel.borderaround xlContinuous
                    coluna = coluna + 1
                End If
                If EstrutGrEpid(iGrEpid).regras(iRegra).flg_suspeita = mediSim Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                    celulaExcel.value = Mid(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).descr_carac_micro, 1, 1)
                    celulaExcel.borderaround xlContinuous
                    coluna = coluna + 1
                End If
            
            End If
            rsDados.Close
            If EstrutGrEpid(iGrEpid).regras(iRegra).flg_mostra_micro = mediSim Then
                Set celulaExcel = FolhaExcel.cells(11 + totLinhas, colunaMicro)
                celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).descr_micro
            End If
            For iAntib = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).totalResAntib
                If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).ResAntib(iAntib).cod_antib = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResAntib(iAntib).cod_antib Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, colunaAntib + iAntib - 1)
                    celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).ResAntib(iAntib).res_antib
                End If
            Next iAntib
            
            For iProvas = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).totalResProvas
                If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).ResProvas(iProvas).Cod_Prova = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResProvas(iProvas).Cod_Prova Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, colunaProva + iProvas - 1)
                    celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).ResProvas(iProvas).resultado
                    FolhaExcel.range(FolhaExcel.cells(10, colunaProva + iProvas - 1), FolhaExcel.cells(11 + totLinhas, colunaProva + iProvas - 1)).Columns.autofit
                End If
            Next iProvas
            
            For iResultados = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).totalResAna
                If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).ResAna(iResultados).cod_ana = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(1).ResAna(iResultados).cod_ana Then
                    Set celulaExcel = FolhaExcel.cells(11 + totLinhas, colunaRes + iResultados - 1)
                    celulaExcel.value = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).ResAna(iResultados).resultado
                    FolhaExcel.range(FolhaExcel.cells(10, colunaRes + iResultados - 1), FolhaExcel.cells(11 + totLinhas, colunaRes + iResultados - 1)).Columns.autofit
                End If
            Next iResultados
            
            For coluna = 1 To colunaRes + EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iVe).totalResAna - 1
                Set celulaExcel = FolhaExcel.cells(11 + totLinhas, coluna)
                celulaExcel.borderaround xlContinuous
            Next coluna
            
            totLinhas = totLinhas + 1
        End If
        
    Next iVe
    Set rsDados = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro VE_EXCEL_InsereAntib: " & Err.Description, "VE", "VE_EXCEL_InsereAntib", False
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------

'

' ----------------------------------------------------------------------------

Public Function VE_AdicionaExcelBD(caminho As String, NomeFicheiro As String) As String
    On Error GoTo TrataErro
    Dim rsBin As New ADODB.recordset
    Dim ByteData() As Byte 'Byte array for Blob data.
    Dim sourceFile As Integer
    Dim FileLength As Long
    Dim Numblocks As Integer
    Dim LeftOver As Long
    Dim i As Integer
    Dim strFileName As String
    Dim sSql As String
    Dim seq_ve_bin As String
    
    Const BlockSize = 10000 'This size can be experimented with for
    VE_AdicionaExcelBD = ""
    seq_ve_bin = GUID_GET()
    sSql = "INSERT INTO sl_vig_epid_bin (Seq_ve_bin, nome_ficheiro, user_cri, dt_cri) VALUES( "
    sSql = sSql & BL_TrataStringParaBD(seq_ve_bin) & ","
    sSql = sSql & BL_TrataStringParaBD(NomeFicheiro) & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", sysdate)"
    BG_ExecutaQuery_ADO sSql
    
    sSql = "SELECT * FROM sl_vig_epid_bin WHERE seq_ve_bin = " & BL_TrataStringParaBD(seq_ve_bin)
    rsBin.CursorLocation = adUseClient
    rsBin.CursorType = adOpenKeyset
    rsBin.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsBin.Open sSql, gConexao
    If rsBin.RecordCount < 1 Then
        Exit Function
    End If
    
    strFileName = caminho & "\" & NomeFicheiro
    sourceFile = FreeFile
    Open strFileName For Binary Access Read As sourceFile
    FileLength = LOF(sourceFile) ' Get the length of the file.
    
    
    If FileLength = 0 Then
        Close sourceFile
        VE_AdicionaExcelBD = ""
        Exit Function
    Else
        Numblocks = FileLength \ BlockSize
        LeftOver = FileLength Mod BlockSize
        ReDim ByteData(LeftOver - 1)
        Get sourceFile, , ByteData()
        rsBin.Fields("dados").AppendChunk ByteData()
        ReDim ByteData(BlockSize - 1)
        For i = 1 To Numblocks
            Get sourceFile, , ByteData()
            rsBin.Fields("dados").AppendChunk ByteData()
            'rsBin.Fields("seq_res_bin") = 2
        Next i
        Close sourceFile
        VE_AdicionaExcelBD = seq_ve_bin
    End If
    rsBin.Update
    rsBin.Close
Exit Function
TrataErro:
        VE_AdicionaExcelBD = ""
        BG_LogFile_Erros "VE_AdicionaExcelBD: " & Err.Number & " - " & Err.Description, "VE", "VE_AdicionaExcelBD"
        Exit Function
        Resume Next
End Function


' ----------------------------------------------------------------------------

' VERIFICA RESULTADO DA ANALISE NA BASE DADOS. NAO ENCONTROU NA ESTRUTURA

' ----------------------------------------------------------------------------

Public Sub VE_atualizaVigEpidBin(seq_vig_epid As String, seq_ve_email As String, seq_ve_bin As String, cod_estado_ve As String)
    On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = "UPDATE sl_vig_epid SET seq_ve_bin = " & BL_TrataStringParaBD(seq_ve_bin)
    sSql = sSql & " WHERE seq_vig_epid = " & BL_TrataStringParaBD(seq_vig_epid)
    BG_ExecutaQuery_ADO sSql
    
    VE_InereDetalheRegraBD seq_vig_epid, cod_estado_ve, seq_ve_email, seq_ve_bin
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro VE_atualizaVigEpidBin: " & Err.Description, "VE", "VE_atualizaVigEpidBin", False
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------

' VERIFICA RESULTADO DA ANALISE NA BASE DADOS. NAO ENCONTROU NA ESTRUTURA

' ----------------------------------------------------------------------------

Public Sub VE_atualizaVigEpidEmail(seq_vig_epid As String, seq_ve_email As String, seq_ve_bin As String, cod_estado_ve As String)
    On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = "UPDATE sl_vig_epid SET seq_ve_email = " & BL_TrataStringParaBD(seq_ve_email)
    sSql = sSql & ", cod_estado_ve = " & cod_estado_ve
    sSql = sSql & " WHERE seq_vig_epid = " & BL_TrataStringParaBD(seq_vig_epid)
    BG_ExecutaQuery_ADO sSql
    
    VE_InereDetalheRegraBD seq_vig_epid, cod_estado_ve, seq_ve_email, seq_ve_bin
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro VE_atualizaVigEpidEmail: " & Err.Description, "VE", "VE_atualizaVigEpidEmail", False
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------

' VERIFICA SE ANALISE + RESULTADO CODIFICADOS EST�O NA BD E NAO NA ESTRUTURA

' ----------------------------------------------------------------------------
Private Function VE_VerificaAnaliseBD(n_req As Long, cod_ana_s As String, frases() As frases, totalFrases As Integer, resAlfan As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsRes As New ADODB.recordset
    Dim iFrase As Integer
    VE_VerificaAnaliseBD = False
    If totalFrases > 0 Then
        sSql = "SELECT n_req FROM sl_realiza, sl_res_frase  WHERE n_req = " & n_req & " AND sl_res_frase.seq_realiza = sl_realiza.seq_realiza"
        sSql = sSql & " AND sl_realiza.cod_ana_S = " & BL_TrataStringParaBD(cod_ana_s) & "  AND cod_frase IN ( "
        For iFrase = 1 To totalFrases
            sSql = sSql & BL_TrataStringParaBD(frases(iFrase).cod_frase) & ","
        Next iFrase
        sSql = Mid(sSql, 1, Len(sSql) - 1) & ") "
    Else
        sSql = "SELECT n_req FROM sl_realiza, sl_res_alfan  WHERE n_req = " & n_req & " AND sl_res_frase.seq_realiza = sl_realiza.seq_realiza"
        sSql = sSql & " AND sl_realiza.cod_ana_S = " & BL_TrataStringParaBD(cod_ana_s) & "  AND result = " & BL_TrataStringParaBD(resAlfan)
    End If
    RsRes.CursorLocation = adUseClient
    RsRes.CursorType = adOpenKeyset
    RsRes.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        VE_VerificaAnaliseBD = True
    Else
        VE_VerificaAnaliseBD = False
    End If
    RsRes.Close
    Set RsRes = Nothing
    
Exit Function
TrataErro:
        VE_VerificaAnaliseBD = False
        BG_LogFile_Erros "VE_VerificaAnaliseBD: " & Err.Number & " - " & Err.Description, "VE", "VE_VerificaAnaliseBD"
        Exit Function
        Resume Next
End Function



Private Function VE_VerificaProd(estrutRes() As Resultados, indice As Integer, iRegra As Integer) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim iProd As Integer
    On Error GoTo TrataErro
    VE_VerificaProd = False
    If regras(iRegra).totalProd <= 0 Then
        Exit Function
    End If
    sSql = "SELECT * FROM Slv_analises WHERE cod_ana = " & BL_TrataStringParaBD(estrutRes(indice).codAgrup)
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenKeyset
    rsAna.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        If BL_HandleNull(rsAna!cod_produto, "") <> "" Then
            For iProd = 1 To regras(iRegra).totalProd
                If regras(iRegra).prod(iProd).cod_produto = BL_HandleNull(rsAna!cod_produto, "") Then
                    VE_VerificaProd = True
                    Exit For
                End If
            Next iProd
        End If
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
        VE_VerificaProd = False
        BG_LogFile_Erros "VE_VerificaProd: " & Err.Number & " - " & Err.Description, "VE", "VE_VerificaProd"
        Exit Function
        Resume Next
End Function


Public Function VE_TrataDadosAntigos() As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsRes As New ADODB.recordset
    Dim estrutRes() As Resultados
    Dim totalRes As Long
    totalRes = 0
    ReDim estrutRes(0)
    
    sSql = "SELECT req.seq_utente,req.n_req, r.seq_realiza, r.cod_ana_s, r.cod_ana_c, r.cod_perfil, r.cod_agrup, flg_estado, r.user_cri, "
    sSql = sSql & " r.dt_cri, r.hr_cri, r.dt_act, r.hr_act, r.user_act, r.user_val, r.dt_val, r.hr_val, r.user_tec_val, r.dt_tec_val, "
    sSql = sSql & " r.hr_tec_val, '1' flg_apar_trans, r.flg_facturado, ord_marca, n_folha_trab,ord_ana, "
    sSql = sSql & " ord_ana_compl, ord_ana_perf,  RA.n_res, RA.result, r.dt_chega, 'REALIZA' tabela, cod_frase, ord_frase, r.flg_apar,   "
    sSql = sSql & " rm.cod_micro, rm.quantif, rm.flg_imp, rm.flg_tsq, rm.cod_gr_antibio, rm.prova, rm.flg_testes "
    sSql = sSql & " , ra.flg_imprimir flg_imprimir_ra, rf.flg_imprimir flg_imprimir_rf, rm.flg_imprimir flg_imprimir_rm, RF.N_RES N_RES_FRASE "
    sSql = sSql & ", ra.res_ant1 ra_res_ant1, ra.res_ant2 ra_res_ant2, ra.res_ant3 ra_res_ant3, ra.dt_res_ant1 ra_dt_res_ant1 , "
    sSql = sSql & " ra.dt_res_ant2 ra_dt_res_ant2,ra. dt_res_ant3 ra_dt_res_ant3, ra.seq_obs_ant1 ra_seq_obsant1, ra.seq_obs_ant2 ra_seq_obsant2 "
    sSql = sSql & " , ra.seq_obs_ant3 ra_seq_obsant3, rf.seq_obs_ant1 rf_seq_obsant1, rf.seq_obs_ant2 rf_seq_obsant2, rf.seq_obs_ant3 rf_seq_obsant3"
    sSql = sSql & ", r.flg_assinado, r.dt_assinado, r.hr_assinado, r.user_assinado, 1 transmit_psm, 1 n_envio, r.flg_anexo,r.flg_obs_ana, r.seq_req_tubo, "
    sSql = sSql & " r.flg_grafico, rm.cod_carac_micro, rm.flg_ve flg_ve_micro, ra.flg_ve flg_ve, rf.flg_ve flg_ve_frase, anas.t_result "
    sSql = sSql & " FROM sl_realiza r LEFT OUTER JOIN sl_res_alfan ra ON r.seq_realiza = ra.seq_realiza LEFT OUTER JOIN sl_res_frase rf ON r.seq_realiza = rf.seq_realiza "
    sSql = sSql & " LEFT OUTER JOIN sl_res_micro rm ON r.seq_realiza = rm.seq_realiza"
    sSql = sSql & " , sl_requis req, slv_analises ana, sl_ana_s anas "
    sSql = sSql & " WHERE r.n_req = req.n_req AND (rm.cod_carac_micro IS NULL OR rm.cod_carac_micro <> 2 ) "
    sSql = sSql & " AND r.cod_agrup = ana.cod_ana AND ( ana.seq_sinonimo is null or ana.seq_sinonimo = -1) "
    sSql = sSql & " AND req.dt_chega BETWEEN '01-01-2014' AND '30-07-2014' "
    sSql = sSql & " AND r.cod_ana_S IN (SELECT cod_ana_s FROM sl_ana_s WHERE flg_vigilancia_epid = 1)"
    sSql = sSql & " AND r.seq_realiza NOT IN (SELECT seq_realiza FROM sl_vig_epid)"
    sSql = sSql & " AND r.cod_Ana_S = anas.cod_ana_s "
    
    RsRes.CursorLocation = adUseClient
    RsRes.CursorType = adOpenKeyset
    RsRes.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        While Not RsRes.EOF
        
        
            totalRes = totalRes + 1
            ReDim Preserve estrutRes(totalRes)
            
            
            estrutRes(totalRes).n_req = BL_HandleNull(RsRes!n_req, "")
            estrutRes(totalRes).seq_utente = BL_HandleNull(RsRes!seq_utente, "")
            estrutRes(totalRes).idxU = mediComboValorNull
            estrutRes(totalRes).idxR = mediComboValorNull
            estrutRes(totalRes).idxP = mediComboValorNull
            estrutRes(totalRes).idxC = mediComboValorNull
            estrutRes(totalRes).idxS = mediComboValorNull
            estrutRes(totalRes).seqRealiza = BL_HandleNull(RsRes!seq_realiza, "")
            estrutRes(totalRes).codAnaS = BL_HandleNull(RsRes!cod_ana_s, "")
            estrutRes(totalRes).CodAnaC = BL_HandleNull(RsRes!cod_ana_c, "")
            estrutRes(totalRes).CodPerfil = BL_HandleNull(RsRes!Cod_Perfil, "")
            estrutRes(totalRes).codAgrup = BL_HandleNull(RsRes!cod_agrup, "")
            estrutRes(totalRes).Estado = BL_HandleNull(RsRes!flg_estado, "")
            estrutRes(totalRes).accao = ""
            estrutRes(totalRes).UserCri = BL_HandleNull(RsRes!user_cri, "")
            estrutRes(totalRes).DtCri = BL_HandleNull(RsRes!dt_cri, "")
            estrutRes(totalRes).HrCri = BL_HandleNull(RsRes!hr_cri, "")
            estrutRes(totalRes).UserAct = BL_HandleNull(RsRes!user_act, "")
            estrutRes(totalRes).DtAct = BL_HandleNull(RsRes!dt_act, "")
            estrutRes(totalRes).HrAct = BL_HandleNull(RsRes!hr_act, "")
            estrutRes(totalRes).dtVal = BL_HandleNull(RsRes!dt_val, "")
            estrutRes(totalRes).userVal = BL_HandleNull(RsRes!user_val, "")
            estrutRes(totalRes).hrVal = BL_HandleNull(RsRes!hr_val, "")
            estrutRes(totalRes).DtValTec = BL_HandleNull(RsRes!dt_tec_val, "")
            estrutRes(totalRes).UserValTec = BL_HandleNull(RsRes!user_tec_val, "")
            estrutRes(totalRes).HrValTec = BL_HandleNull(RsRes!hr_tec_val, "")
            estrutRes(totalRes).DtChega = BL_HandleNull(RsRes!dt_chega, "")
            estrutRes(totalRes).flg_assinado = BL_HandleNull(RsRes!flg_assinado, 0)
            estrutRes(totalRes).UserAssin = BL_HandleNull(RsRes!user_assinado, "")
            estrutRes(totalRes).DtAssin = BL_HandleNull(RsRes!dt_assinado, "")
            estrutRes(totalRes).HrAssin = BL_HandleNull(RsRes!hr_assinado, "")
            
            ' RESULTADOS DO TIPO ALFANUMERICO, NUMERICO, AUXILIAR
            ' ---------------------------------------------------
            If (BL_HandleNull(RsRes!t_result, "") = gT_Alfanumerico Or _
               BL_HandleNull(RsRes!t_result, "") = gT_Numerico Or _
               BL_HandleNull(RsRes!t_result, "") = gT_Auxiliar) And BL_HandleNull(RsRes!N_Res, -1) <= 1 Then
                    estrutRes(totalRes).res = BL_HandleNull(RsRes!Result, "")
                    estrutRes(totalRes).Seg_Res = ""
                    estrutRes(totalRes).flg_ve = BL_HandleNull(RsRes!flg_ve, 0)
            
            ' RESULTADOS DO TIPO FRASE
            ' ---------------------------------------------------
            ElseIf BL_HandleNull(RsRes!t_result, "") = gT_Frase And (BL_HandleNull(BL_HandleNull(RsRes!N_RES_FRASE, ""), -1) = 1 Or BL_HandleNull(RsRes!Result, "") = cSemResultado Or BL_HandleNull(RsRes!Result, "") = cVerObservacoes) Then
                If BL_HandleNull(RsRes!Result, "") = cSemResultado Or BL_HandleNull(RsRes!Result, "") = cVerObservacoes Then
                    estrutRes(totalRes).res = BL_HandleNull(RsRes!Result, "")
                    estrutRes(totalRes).descr_frase = BL_HandleNull(RsRes!Result, "")
                Else
                    PreencheEstrutResultados_FRASE estrutRes, totalRes, False, totalRes, BL_HandleNull(RsRes!cod_frase, ""), BL_HandleNull(RsRes!ord_frase, 0), BL_HandleNull(RsRes!flg_imprimir_rf, ""), CInt(BL_HandleNull(RsRes!N_RES_FRASE, ""))
                End If
                estrutRes(totalRes).flg_assinado = BL_HandleNull(RsRes!flg_assinado, 0)
                estrutRes(totalRes).UserAssin = BL_HandleNull(RsRes!user_assinado, "")
                estrutRes(totalRes).DtAssin = BL_HandleNull(RsRes!dt_assinado, "")
                estrutRes(totalRes).HrAssin = BL_HandleNull(RsRes!hr_assinado, "")
                estrutRes(totalRes).flg_ve = BL_HandleNull(RsRes!flg_ve_frase, 0)
                
            ' RESULTADOS DO TIPO MICRO
            ' ---------------------------------------------------
            ElseIf BL_HandleNull(RsRes!t_result, "") = gT_Microrganismo Then
                estrutRes(totalRes).Imprimir = BL_HandleNull(RsRes!flg_imprimir_rm, "")
                If BL_HandleNull(RsRes!Result, "") = cSemResultado Or BL_HandleNull(RsRes!Result, "") = cVerObservacoes Then
                    estrutRes(totalRes).res = BL_HandleNull(RsRes!Result, "")
                    estrutRes(totalRes).micro_descr = BL_HandleNull(RsRes!Result, "")
                Else
                    PreencheEstrutResultados_MICRO estrutRes, totalRes, False, totalRes, BL_HandleNull(RsRes!cod_micro, ""), BL_HandleNull(RsRes!Quantif, ""), BL_HandleNull(RsRes!flg_imp, ""), BL_HandleNull(RsRes!flg_tsq, ""), BL_HandleNull(RsRes!Cod_Gr_Antibio, ""), BL_HandleNull(RsRes!Prova, ""), _
                                                   BL_HandleNull(RsRes!flg_testes, ""), BL_HandleNull(RsRes!flg_imprimir_rm, ""), BL_HandleNull(RsRes!cod_carac_micro, 1), _
                                                   BL_HandleNull(RsRes!flg_ve_micro, 0)
                End If
                PreencheResAntib estrutRes, totalRes
                estrutRes(totalRes).flg_assinado = BL_HandleNull(RsRes!flg_assinado, 0)
                estrutRes(totalRes).UserAssin = BL_HandleNull(RsRes!user_assinado, "")
                estrutRes(totalRes).DtAssin = BL_HandleNull(RsRes!dt_assinado, "")
                estrutRes(totalRes).HrAssin = BL_HandleNull(RsRes!hr_assinado, "")
                estrutRes(totalRes).flg_ve = BL_HandleNull(RsRes!flg_ve_micro, 0)
                
            End If
            
            If VE_VerificaGruposEpidAutom(estrutRes(totalRes).seqRealiza, estrutRes, totalRes, "", totalRes, mediComboValorNull) = True Then
                estrutRes(totalRes).flg_ve = mediSim
                VE_InereRegistoBD estrutRes, totalRes, estrutRes(totalRes).seqRealiza
                If BL_HandleNull(RsRes!t_result, "") = gT_Microrganismo Then
                    sSql = "UPDATE SL_res_micro SET flg_ve = 1 where seq_realiza = " & estrutRes(totalRes).seqRealiza & " AND cod_micro = " & BL_TrataStringParaBD(estrutRes(totalRes).micro_codigo)
                ElseIf BL_HandleNull(RsRes!t_result, "") = gT_Frase Then
                    sSql = "UPDATE sl_res_frase SET flg_ve = 1 where seq_realiza = " & estrutRes(totalRes).seqRealiza & " AND cod_frase = " & BL_TrataStringParaBD(estrutRes(totalRes).cod_frase)
                Else
                    sSql = "UPDATE sl_res_alfan SET flg_ve = 1 where seq_realiza = " & estrutRes(totalRes).seqRealiza
                End If
                BG_ExecutaQuery_ADO sSql
            Else
                estrutRes(totalRes).flg_ve = mediNao
            End If
                    
            RsRes.MoveNext
        Wend
    End If
    RsRes.Close

    
Exit Function
TrataErro:
        VE_TrataDadosAntigos = False
        BG_LogFile_Erros "VE_TrataDadosAntigos: " & Err.Number & " - " & Err.Description, "VE", "VE_TrataDadosAntigos"
        Exit Function
        Resume Next
End Function


' ----------------------------------------------------------------------------------

' PREENCHE ESTRURURA DOS RESULTADOS do tipo frase

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutResultados_FRASE(estrutRes() As Resultados, totalRes As Long, flg_existe As Boolean, indice As Long, cod_frase As String, ordem_frase As Integer, flg_imprimir As String, N_Res As Integer)
    Dim i As Integer
    
    If flg_existe = False Then
        If N_Res = 1 Then
            estrutRes(indice).cod_frase = Replace(cod_frase, "\\", "")
            estrutRes(indice).ordem_frase = ordem_frase
            estrutRes(indice).Imprimir = BL_HandleNull(flg_imprimir, "1")
            'soliveira hcvp
            estrutRes(indice).descr_frase = BL_SelCodigo("SL_DICIONARIO", "descr_frase", "cod_frase", Replace(cod_frase, "\\", ""), "V")
        Else
            estrutRes(indice).SEG_cod_frase = Replace(cod_frase, "\\", "")
            estrutRes(indice).SEG_ordem_frase = ordem_frase
            estrutRes(indice).SEG_Imprimir = BL_HandleNull(flg_imprimir, "1")
            'soliveira hcvp
            estrutRes(indice).SEG_descr_frase = BL_SelCodigo("SL_DICIONARIO", "descr_frase", "cod_frase", Replace(cod_frase, "\\", ""), "V")
        End If
        Exit Sub
    ElseIf flg_existe = True Then
        For i = indice To totalRes
            If cod_frase = estrutRes(i).cod_frase And _
                estrutRes(i).n_req = estrutRes(indice).n_req And _
                estrutRes(i).CodPerfil = estrutRes(indice).CodPerfil And _
                estrutRes(i).CodAnaC = estrutRes(indice).CodAnaC And _
                estrutRes(i).codAnaS = estrutRes(indice).codAnaS Then
                    Exit Sub
            End If
            
            If N_Res = 1 And estrutRes(i).n_req = estrutRes(indice).n_req And _
                estrutRes(i).CodPerfil = estrutRes(indice).CodPerfil And _
                estrutRes(i).CodAnaC = estrutRes(indice).CodAnaC And _
                estrutRes(i).codAnaS = estrutRes(indice).codAnaS And estrutRes(i).cod_frase = "" Then
                    estrutRes(indice).SEG_cod_frase = Replace(cod_frase, "\\", "")
                    estrutRes(indice).SEG_ordem_frase = ordem_frase
                    estrutRes(indice).SEG_Imprimir = BL_HandleNull(flg_imprimir, "1")
                    'soliveira hcvp
                    estrutRes(indice).SEG_descr_frase = BL_SelCodigo("SL_DICIONARIO", "descr_frase", "cod_frase", Replace(cod_frase, "\\", ""), "V")
                    Exit Sub
            ElseIf N_Res = 2 And estrutRes(i).n_req = estrutRes(indice).n_req And _
                estrutRes(i).CodPerfil = estrutRes(indice).CodPerfil And _
                estrutRes(i).CodAnaC = estrutRes(indice).CodAnaC And _
                estrutRes(i).codAnaS = estrutRes(indice).codAnaS And estrutRes(i).SEG_cod_frase = "" Then
                    estrutRes(i).SEG_cod_frase = Replace(cod_frase, "\\", "")
                    estrutRes(i).SEG_ordem_frase = ordem_frase
                    estrutRes(i).SEG_Imprimir = BL_HandleNull(flg_imprimir, "1")
                    'soliveira hcvp
                    estrutRes(i).SEG_descr_frase = BL_SelCodigo("SL_DICIONARIO", "descr_frase", "cod_frase", Replace(cod_frase, "\\", ""), "V")
                    Exit Sub
            End If
        Next
    End If
    
    totalRes = totalRes + 1
    ReDim Preserve estrutRes(totalRes)
    
    estrutRes(totalRes).n_req = estrutRes(indice).n_req
    estrutRes(totalRes).seq_utente = estrutRes(indice).seq_utente
    estrutRes(totalRes).idxU = estrutRes(indice).idxU
    estrutRes(totalRes).idxR = estrutRes(indice).idxR
    estrutRes(totalRes).idxP = estrutRes(indice).idxP
    estrutRes(totalRes).idxC = estrutRes(indice).idxC
    estrutRes(totalRes).idxS = estrutRes(indice).idxS
    estrutRes(totalRes).seqRealiza = estrutRes(indice).seqRealiza
    estrutRes(totalRes).codAnaS = estrutRes(indice).codAnaS
    estrutRes(totalRes).CodAnaC = estrutRes(indice).CodAnaC
    estrutRes(totalRes).CodPerfil = estrutRes(indice).CodPerfil
    estrutRes(totalRes).codAgrup = estrutRes(indice).codAgrup
    estrutRes(totalRes).Estado = estrutRes(indice).Estado
    estrutRes(totalRes).UserCri = estrutRes(indice).UserCri
    estrutRes(totalRes).DtCri = estrutRes(indice).DtCri
    estrutRes(totalRes).HrCri = estrutRes(indice).HrCri
    estrutRes(totalRes).UserAct = estrutRes(indice).UserAct
    estrutRes(totalRes).DtAct = estrutRes(indice).DtAct
    estrutRes(totalRes).HrAct = estrutRes(indice).HrAct
    estrutRes(totalRes).dtVal = estrutRes(indice).dtVal
    estrutRes(totalRes).userVal = estrutRes(indice).userVal
    estrutRes(totalRes).hrVal = estrutRes(indice).hrVal
    estrutRes(totalRes).DtValTec = estrutRes(indice).DtValTec
    estrutRes(totalRes).UserValTec = estrutRes(indice).UserValTec
    estrutRes(totalRes).HrValTec = estrutRes(indice).HrValTec
    estrutRes(totalRes).Flg_Facturado = estrutRes(indice).Flg_Facturado
    estrutRes(totalRes).flg_aparTrans = estrutRes(indice).flg_aparTrans
    estrutRes(totalRes).transmit_psm = estrutRes(indice).transmit_psm
    estrutRes(totalRes).n_envio = estrutRes(indice).n_envio
    estrutRes(totalRes).N_Folha_Trab = estrutRes(indice).N_Folha_Trab
    estrutRes(totalRes).OrdMarca = estrutRes(indice).OrdMarca
    estrutRes(totalRes).ordem = estrutRes(indice).ordem
    estrutRes(totalRes).OrdC = estrutRes(indice).OrdC
    estrutRes(totalRes).OrdP = estrutRes(indice).OrdP
    estrutRes(totalRes).DtChega = estrutRes(indice).DtChega
    estrutRes(totalRes).Tabela = estrutRes(indice).Tabela
    estrutRes(totalRes).seq_req_tubo = estrutRes(indice).seq_req_tubo
    
    estrutRes(totalRes).DC_DeltaCheck = estrutRes(indice).DC_DeltaCheck
    estrutRes(totalRes).DC_ToolTip = estrutRes(indice).DC_ToolTip
    estrutRes(totalRes).DC_descrFormula = estrutRes(indice).DC_descrFormula
    estrutRes(totalRes).DC_resFormula = estrutRes(indice).DC_resFormula
    estrutRes(totalRes).DC_Accao = estrutRes(indice).DC_Accao
    estrutRes(totalRes).DC_Activo = estrutRes(indice).DC_Activo
    
    estrutRes(totalRes).IF_Interferencia = estrutRes(indice).IF_Interferencia
    estrutRes(totalRes).IF_ToolTip = estrutRes(indice).IF_ToolTip
    estrutRes(totalRes).IF_descrFormula = estrutRes(indice).IF_descrFormula
    estrutRes(totalRes).IF_descrFormula = estrutRes(indice).IF_descrFormula
    estrutRes(totalRes).IF_Accao = estrutRes(indice).IF_Accao
    estrutRes(totalRes).IF_Activo = estrutRes(indice).IF_Activo
    estrutRes(totalRes).IF_Indice = estrutRes(indice).IF_Indice
    
    estrutRes(totalRes).SeqObsAuto = estrutRes(indice).SeqObsAuto
    estrutRes(totalRes).ObsAuto = estrutRes(indice).SeqObsAuto
    estrutRes(totalRes).ObsAutoAccao = estrutRes(indice).SeqObsAuto
    estrutRes(totalRes).ObsAutoActivo = estrutRes(indice).SeqObsAuto
    
    estrutRes(totalRes).FlgApar = estrutRes(indice).FlgApar
    estrutRes(totalRes).descrFlgApar = estrutRes(indice).descrFlgApar
    estrutRes(totalRes).Flg_GravaHistAna = estrutRes(indice).Flg_GravaHistAna
    estrutRes(totalRes).ResAnterActivo = estrutRes(indice).ResAnterActivo
    estrutRes(totalRes).FlgAdicionada = estrutRes(indice).FlgAdicionada
    estrutRes(totalRes).motivoValAuto = estrutRes(indice).motivoValAuto
    estrutRes(totalRes).flg_assinado = estrutRes(indice).flg_assinado
    estrutRes(totalRes).UserAssin = estrutRes(indice).UserAssin
    estrutRes(totalRes).DtAssin = estrutRes(indice).DtAssin
    estrutRes(totalRes).HrAssin = estrutRes(indice).HrAssin
    estrutRes(totalRes).accao = ""
    estrutRes(totalRes).Editado = False
    estrutRes(totalRes).flg_obs_ana = estrutRes(indice).flg_obs_ana
    If N_Res = 1 Then
        estrutRes(totalRes).cod_frase = cod_frase
        estrutRes(totalRes).ordem_frase = ordem_frase
        estrutRes(totalRes).descr_frase = BL_SelCodigo("SL_DICIONARIO", "descr_frase", "cod_frase", cod_frase, "V")
        estrutRes(totalRes).Imprimir = BL_HandleNull(flg_imprimir, "1")
    Else
        estrutRes(totalRes).SEG_cod_frase = cod_frase
        estrutRes(totalRes).SEG_ordem_frase = ordem_frase
        estrutRes(totalRes).SEG_descr_frase = BL_SelCodigo("SL_DICIONARIO", "descr_frase", "cod_frase", cod_frase, "V")
        estrutRes(totalRes).SEG_Imprimir = BL_HandleNull(flg_imprimir, "1")
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Resultados FRASE" & Err.Description, "", "PreencheEstrutResultados_FRASE", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' PREENCHE ESTRURURA DOS RESULTADOS do tipo frase

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutResultados_MICRO(estrutRes() As Resultados, totalRes As Long, flg_existe As Boolean, indice As Long, cod_micro As String, Quantif As String, flg_imp As String, flg_tsq As String, _
                                            Cod_Gr_Antibio As String, Prova As String, flg_testes As String, flg_imprimir As String, cod_carac_micro As String, _
                                            flg_ve_micro As Integer)
    Dim i As Integer
    Dim sSql As String
    Dim RsM As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT x1.descr_microrg, x2.descr_frase_micro FROM sl_microrg x1 LEFT OUTER JOIN sl_cod_frase_micro x2 ON x1.cod_frase_micro = x2.cod_frase_micro "
    sSql = sSql & " WHERE cod_microrg = " & BL_TrataStringParaBD(Replace(cod_micro, "\\", ""))
    RsM.CursorLocation = adUseClient
    RsM.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsM.Open sSql, gConexao
    
    If RsM.RecordCount >= 1 Then
        If flg_existe = False Then
                estrutRes(indice).micro_codigo = Replace(cod_micro, "\\", "")
                estrutRes(indice).micro_descr = BL_HandleNull(RsM!descr_microrg, "")
                estrutRes(indice).micro_cod_prova = Prova
                estrutRes(indice).micro_descr_prova = BL_SelCodigo("SL_TBF_t_prova", "descr_t_prova", "cod_t_prova", estrutRes(indice).micro_cod_prova, "V")
                estrutRes(indice).micro_flg_imp = flg_imp
                estrutRes(indice).micro_flg_teste = flg_testes
                estrutRes(indice).micro_flg_tsa = flg_tsq
                estrutRes(indice).micro_quantif = Quantif
                estrutRes(indice).micro_gr_antib = Cod_Gr_Antibio
                estrutRes(indice).micro_obs = BL_HandleNull(RsM!descr_frase_micro, "")
                estrutRes(indice).Imprimir = BL_HandleNull(flg_imprimir, "1")
                estrutRes(indice).micro_cod_carac = BL_HandleNull(cod_carac_micro, "")
                estrutRes(indice).micro_descr_carac = BL_SelCodigo("sl_tbf_carac_micro", "DEscr_carac_micro", "cod_carac_micro", estrutRes(indice).micro_cod_carac)
            RsM.Close
            Set RsM = Nothing
            Exit Sub
        ElseIf flg_existe = True Then
            For i = indice To totalRes
                If Replace(cod_micro, "\\", "") = estrutRes(i).micro_codigo And _
                    estrutRes(i).n_req = estrutRes(indice).n_req And _
                    estrutRes(i).CodPerfil = estrutRes(indice).CodPerfil And _
                    estrutRes(i).CodAnaC = estrutRes(indice).CodAnaC And _
                    estrutRes(i).codAnaS = estrutRes(indice).codAnaS Then
                        RsM.Close
                        Set RsM = Nothing
                        Exit Sub
                End If
            Next
        End If
    
        totalRes = totalRes + 1
        ReDim Preserve estrutRes(totalRes)
    
        estrutRes(totalRes).n_req = estrutRes(indice).n_req
        estrutRes(totalRes).seq_utente = estrutRes(indice).seq_utente
        estrutRes(totalRes).idxU = estrutRes(indice).idxU
        estrutRes(totalRes).idxR = estrutRes(indice).idxR
        estrutRes(totalRes).idxP = estrutRes(indice).idxP
        estrutRes(totalRes).idxC = estrutRes(indice).idxC
        estrutRes(totalRes).idxS = estrutRes(indice).idxS
        estrutRes(totalRes).seqRealiza = estrutRes(indice).seqRealiza
        estrutRes(totalRes).codAnaS = estrutRes(indice).codAnaS
        estrutRes(totalRes).CodAnaC = estrutRes(indice).CodAnaC
        estrutRes(totalRes).CodPerfil = estrutRes(indice).CodPerfil
        estrutRes(totalRes).codAgrup = estrutRes(indice).codAgrup
        estrutRes(totalRes).Estado = estrutRes(indice).Estado
        estrutRes(totalRes).UserCri = estrutRes(indice).UserCri
        estrutRes(totalRes).DtCri = estrutRes(indice).DtCri
        estrutRes(totalRes).HrCri = estrutRes(indice).HrCri
        estrutRes(totalRes).UserAct = estrutRes(indice).UserAct
        estrutRes(totalRes).DtAct = estrutRes(indice).DtAct
        estrutRes(totalRes).HrAct = estrutRes(indice).HrAct
        estrutRes(totalRes).dtVal = estrutRes(indice).dtVal
        estrutRes(totalRes).userVal = estrutRes(indice).userVal
        estrutRes(totalRes).hrVal = estrutRes(indice).hrVal
        estrutRes(totalRes).DtValTec = estrutRes(indice).DtValTec
        estrutRes(totalRes).UserValTec = estrutRes(indice).UserValTec
        estrutRes(totalRes).HrValTec = estrutRes(indice).HrValTec
        estrutRes(totalRes).Flg_Facturado = estrutRes(indice).Flg_Facturado
        estrutRes(totalRes).flg_aparTrans = estrutRes(indice).flg_aparTrans
        estrutRes(totalRes).transmit_psm = estrutRes(indice).transmit_psm
        estrutRes(totalRes).n_envio = estrutRes(indice).n_envio
        estrutRes(totalRes).N_Folha_Trab = estrutRes(indice).N_Folha_Trab
        estrutRes(totalRes).OrdMarca = estrutRes(indice).OrdMarca
        estrutRes(totalRes).ordem = estrutRes(indice).ordem
        estrutRes(totalRes).OrdC = estrutRes(indice).OrdC
        estrutRes(totalRes).OrdP = estrutRes(indice).OrdP
        estrutRes(totalRes).DtChega = estrutRes(indice).DtChega
        estrutRes(totalRes).Tabela = estrutRes(indice).Tabela
        estrutRes(totalRes).seq_req_tubo = estrutRes(indice).seq_req_tubo
            
        estrutRes(totalRes).accao = ""
        estrutRes(totalRes).Editado = False

        estrutRes(totalRes).DC_DeltaCheck = estrutRes(indice).DC_DeltaCheck
        estrutRes(totalRes).DC_ToolTip = estrutRes(indice).DC_ToolTip
        estrutRes(totalRes).DC_descrFormula = estrutRes(indice).DC_descrFormula
        estrutRes(totalRes).DC_resFormula = estrutRes(indice).DC_resFormula
        estrutRes(totalRes).DC_Accao = estrutRes(indice).DC_Accao
        estrutRes(totalRes).DC_Activo = estrutRes(indice).DC_Activo
        
        estrutRes(totalRes).IF_Interferencia = estrutRes(indice).IF_Interferencia
        estrutRes(totalRes).IF_ToolTip = estrutRes(indice).IF_ToolTip
        estrutRes(totalRes).IF_descrFormula = estrutRes(indice).IF_descrFormula
        estrutRes(totalRes).IF_descrFormula = estrutRes(indice).IF_descrFormula
        estrutRes(totalRes).IF_Accao = estrutRes(indice).IF_Accao
        estrutRes(totalRes).IF_Activo = estrutRes(indice).IF_Activo
        estrutRes(totalRes).IF_Indice = estrutRes(indice).IF_Indice
        
        estrutRes(totalRes).SeqObsAuto = estrutRes(indice).SeqObsAuto
        estrutRes(totalRes).ObsAuto = estrutRes(indice).SeqObsAuto
        estrutRes(totalRes).ObsAutoAccao = estrutRes(indice).SeqObsAuto
        estrutRes(totalRes).ObsAutoActivo = estrutRes(indice).SeqObsAuto
        
        estrutRes(totalRes).FlgApar = estrutRes(indice).FlgApar
        estrutRes(totalRes).descrFlgApar = estrutRes(indice).descrFlgApar
        estrutRes(totalRes).Flg_GravaHistAna = estrutRes(indice).Flg_GravaHistAna
        estrutRes(totalRes).ResAnterActivo = estrutRes(indice).ResAnterActivo
        estrutRes(totalRes).FlgAdicionada = estrutRes(indice).FlgAdicionada
        estrutRes(totalRes).motivoValAuto = estrutRes(indice).motivoValAuto
    
        estrutRes(totalRes).micro_codigo = cod_micro
        estrutRes(totalRes).micro_descr = BL_HandleNull(RsM!descr_microrg, "")
        estrutRes(totalRes).micro_cod_prova = Prova
        estrutRes(totalRes).micro_descr_prova = BL_SelCodigo("SL_TBF_t_prova", "descr_t_prova", "cod_t_prova", estrutRes(indice).micro_cod_prova, "V")
        estrutRes(totalRes).micro_flg_imp = flg_imp
        estrutRes(totalRes).micro_gr_antib = Cod_Gr_Antibio
        estrutRes(totalRes).micro_flg_teste = flg_testes
        estrutRes(totalRes).micro_flg_tsa = flg_tsq
        estrutRes(totalRes).micro_quantif = Quantif
        estrutRes(totalRes).micro_obs = BL_HandleNull(RsM!descr_frase_micro, "")
        estrutRes(totalRes).Imprimir = flg_imprimir
        estrutRes(totalRes).micro_cod_carac = BL_HandleNull(cod_carac_micro, "")
        estrutRes(totalRes).micro_descr_carac = BL_SelCodigo("sl_tbf_carac_micro", "DEscr_carac_micro", "cod_carac_micro", estrutRes(totalRes).micro_cod_carac)
        estrutRes(totalRes).flg_ve = flg_ve_micro
        
    End If
    RsM.Close
    Set RsM = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Resultados MICRO" & Err.Description, "", "PreencheEstrutResultados_MICRO", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' PREENCHE ESTRUTURA COM RESULTADOS DOS ANTIBIOTICOS

' ----------------------------------------------------------------------------------
Private Sub PreencheResAntib(estrutRes() As Resultados, linha As Long)
    Dim sSql As String
    Dim rsTSQ As New ADODB.recordset
    Dim i As Long
    On Error GoTo TrataErro
    estrutRes(linha).totalAntib = 0
    ReDim estrutRes(linha).antibioticos(0)
    sSql = "SELECT x1.cod_antib, x2.descr_antibio, x1.res_sensib, x1.cmi,x1.flg_imp, x1.user_val, x1.dt_val, x1.hr_val "
    sSql = sSql & " FROM sl_Res_tsq x1 LEFT OUTER JOIN sl_antibio x2  ON x1.cod_antib = x2.cod_antibio  WHERE seq_realiza = " & estrutRes(linha).seqRealiza
    sSql = sSql & " AND cod_micro = " & BL_TrataStringParaBD(estrutRes(linha).micro_codigo)
    rsTSQ.CursorLocation = adUseClient
    rsTSQ.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTSQ.Open sSql, gConexao
    If rsTSQ.RecordCount >= 1 Then
        While Not rsTSQ.EOF
            estrutRes(linha).totalAntib = estrutRes(linha).totalAntib + 1
            ReDim Preserve estrutRes(linha).antibioticos(estrutRes(linha).totalAntib)
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).codAntib = BL_HandleNull(rsTSQ!cod_antib, "")
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).DescrAntib = BL_HandleNull(rsTSQ!descr_antibio, "")
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).sensibilidade = BL_HandleNull(rsTSQ!res_sensib, "")
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).CMI = BL_HandleNull(rsTSQ!CMI, "")
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).Imprimir = BL_HandleNull(rsTSQ!flg_imp, "N")
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).user_val = BL_HandleNull(rsTSQ!user_val, "")
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).dt_val = BL_HandleNull(rsTSQ!dt_val, "")
            estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).hr_val = BL_HandleNull(rsTSQ!hr_val, "")
            If Len(estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).Imprimir) > 1 Then
                estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).Imprimir = UCase(Mid(estrutRes(linha).antibioticos(estrutRes(linha).totalAntib).Imprimir, 1, 1))
            End If
            rsTSQ.MoveNext
        Wend
    End If
    rsTSQ.Close
    Set rsTSQ = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheResAntib " & Err.Description, "", "PreencheResAntib"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

'

' ----------------------------------------------------------------------------------
Private Function VE_VerificaAnaFraseRestricao(cod_regra_ve As String, cod_ana_s As String, cod_frase As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsFrase As New ADODB.recordset
    VE_VerificaAnaFraseRestricao = False
    sSql = "SELECT * FROM sl_cod_regras_ve_ana_env_frase WHERE cod_regra_ve = " & cod_regra_ve
    sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    rsFrase.CursorLocation = adUseClient
    rsFrase.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFrase.Open sSql, gConexao
    If rsFrase.RecordCount >= 1 Then
        While Not rsFrase.EOF
            If BL_HandleNull(rsFrase!cod_frase, "") = cod_frase Then
                VE_VerificaAnaFraseRestricao = True
            End If
            rsFrase.MoveNext
        Wend
    Else
        VE_VerificaAnaFraseRestricao = True
    End If
    rsFrase.Close
    Set rsFrase = Nothing
Exit Function
TrataErro:
    VE_VerificaAnaFraseRestricao = False
    BG_LogFile_Erros "Erro  VE_VerificaAnaFraseRestricao " & Err.Description, "", "VE_VerificaAnaFraseRestricao", False
    Exit Function
    Resume Next
End Function



Public Function VE_RetiraExcelBD(seq_ve_bin As String, flg_open As Boolean) As Boolean
    Dim ByteData() As Byte   'Byte array for picture file.
    Dim DestFileNum As Integer
    Dim DiskFile As String
    Dim rs As New ADODB.recordset
    Dim sSql As String
    Dim NomeFicheiro As String
    Dim xlApp As Object
    Dim xlWB As Object
    Dim FileLength As Long  'Used in Command1 and Command2 procedures.
    Dim Numblocks As Integer
    Dim LeftOver As Long
    Dim i As Integer
    Const BlockSize = 100000
    
    On Error GoTo TrataErro
      
    sSql = "SELECT dados, nome_ficheiro FROM sl_vig_epid_bin WHERE seq_ve_bin = " & BL_TrataStringParaBD(seq_ve_bin)
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenKeyset
    rs.LockType = adLockOptimistic
    rs.Open sSql, gConexao
    If rs.RecordCount = 1 Then
        NomeFicheiro = gDirCliente & "\" & BL_HandleNull(rs!nome_ficheiro, "")
        ' Remove any existing destination file.
        If Len(Dir$(NomeFicheiro)) > 0 Then
           Kill NomeFicheiro
        End If
    
        DestFileNum = FreeFile
        Open NomeFicheiro For Binary As DestFileNum
    
        Numblocks = FileLength / BlockSize
        LeftOver = FileLength Mod BlockSize
        ReDim ByteData(BlockSize)
        
        ByteData() = rs(0).GetChunk(rs(0).ActualSize)
        Put DestFileNum, , ByteData()
    
    
        Close DestFileNum
        DestFileNum = FreeFile

    End If
    rs.Close
    'Kill origemImagem
    DoEvents
    Debug.Print "Complete"
    If flg_open = True Then
        Set xlApp = CreateObject("Excel.Application")
        xlApp.Visible = True
        
        Set xlWB = xlApp.Workbooks.Open(NomeFicheiro)
    End If
    VE_RetiraExcelBD = True
Exit Function
TrataErro:
    VE_RetiraExcelBD = False
    BG_LogFile_Erros "Erro  VE_RetiraExcelBD " & Err.Description, "", "VE_RetiraExcelBD", False
    Exit Function
    Resume Next
End Function

Public Function VE_InsereRegistoEmail(email_para As String, email_cc As String, assunto As String, mensagem As String, anexos() As String) As String
    Dim sSql As String
    Dim seq_ve_email As String
    Dim iRegistos As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    seq_ve_email = GUID_GET()
    
    sSql = "INSERT INTO sl_vig_epid_email (seq_ve_email, email_para, email_cc, assunto, mensagem, user_cri, dt_cri) VALUES("
    sSql = sSql & BL_TrataStringParaBD(seq_ve_email) & ", "
    sSql = sSql & BL_TrataStringParaBD(email_para) & ", "
    sSql = sSql & BL_TrataStringParaBD(email_cc) & ", "
    sSql = sSql & BL_TrataStringParaBD(assunto) & ", "
    sSql = sSql & BL_TrataStringParaBD(mensagem) & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", SYSDATE) "
    iRegistos = BG_ExecutaQuery_ADO(sSql)
    If iRegistos <> 1 Then
        GoTo TrataErro
    End If
    
    For i = 0 To UBound(anexos)
        If anexos(i) <> "" Then
            sSql = "INSERT INTO sl_vig_epid_email_bin (Seq_ve_email, seq_ve_bin) VALUES("
            sSql = sSql & BL_TrataStringParaBD(seq_ve_email) & ", "
            sSql = sSql & BL_TrataStringParaBD(anexos(i)) & ") "
            iRegistos = BG_ExecutaQuery_ADO(sSql)
            If iRegistos <> 1 Then
                GoTo TrataErro
            End If
        End If
    Next i
    VE_InsereRegistoEmail = seq_ve_email
Exit Function
TrataErro:
    VE_InsereRegistoEmail = ""
    BG_LogFile_Erros "Erro  VE_InsereRegistoEmail " & Err.Description, "", "VE_InsereRegistoEmail", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' ASSOCIA UMA ANALISEA UMA REGRA MANUALMENTE

' ----------------------------------------------------------------------------------
Public Function VE_AssociaManualmente(ByRef estrutRes() As Resultados, indice As Long, cod_regra_ve As String, totalRes As Long) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim i As Integer
    Dim RsGr As New ADODB.recordset
    
    For i = 1 To estrutRes(indice).totalVE
        If estrutRes(indice).vigEpid(i).cod_regra_ve = cod_regra_ve Then
            Exit Function
        End If
    Next i
    
    sSql = "SELECT cod_gr_epid FROM sl_cod_gr_epid WHERE cod_gr_epid IN (SELECT cod_gr_epid FROM sl_cod_regras_ve WHERE cod_regra_ve = " & cod_regra_ve & ")"
    RsGr.CursorLocation = adUseClient
    RsGr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsGr.Open sSql, gConexao
    If RsGr.RecordCount >= 1 Then
        estrutRes(indice).totalVE = estrutRes(indice).totalVE + 1
        ReDim Preserve estrutRes(indice).vigEpid(estrutRes(indice).totalVE)
        estrutRes(indice).flg_ve = mediSim
        estrutRes(indice).vigEpid(estrutRes(indice).totalVE).cod_regra_ve = cod_regra_ve
        estrutRes(indice).vigEpid(estrutRes(indice).totalVE).descr_regra_ve = BL_SelCodigo("SL_COD_REGRAS_VE", "DESCR_REGRA_VE", "COD_REGRA_VE", cod_regra_ve)
        estrutRes(indice).vigEpid(estrutRes(indice).totalVE).cod_estado_ve = gEstadoVePendente
        estrutRes(indice).vigEpid(estrutRes(indice).totalVE).cod_micro = estrutRes(indice).micro_codigo
        estrutRes(indice).vigEpid(estrutRes(indice).totalVE).flg_invisivel = mediNao
        estrutRes(indice).vigEpid(estrutRes(indice).totalVE).flg_manual = mediSim
        estrutRes(indice).vigEpid(estrutRes(indice).totalVE).flg_mostra_aviso = BL_HandleNull(RsGr!FLG_AVALIACAO_CONTINUA, 0)
    End If
    RsGr.Close
    Set RsGr = Nothing
    VE_AssociaManualmente = True
Exit Function
TrataErro:
    VE_AssociaManualmente = False
    BG_LogFile_Erros "Erro  VE_AssociaManualmente " & Err.Description, "", "VE_AssociaManualmente", False
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------

' ASSOCIA UMA ANALISEA UMA REGRA MANUALMENTE

' ----------------------------------------------------------------------------------
Public Function VE_RemoveAssociaManual(ByRef estrutRes() As Resultados, indice As Long) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim i As Integer
    Dim RsGr As New ADODB.recordset
    
    For i = 1 To estrutRes(indice).totalVE
        If estrutRes(indice).vigEpid(i).flg_manual = mediSim Then
            estrutRes(indice).vigEpid(i).flg_invisivel = mediSim
            estrutRes(indice).vigEpid(i).seq_vig_epid_apagar = estrutRes(indice).vigEpid(i).seq_vig_epid
        End If
    Next i
    VE_RemoveAssociaManual = True
Exit Function
TrataErro:
    VE_RemoveAssociaManual = False
    BG_LogFile_Erros "Erro  VE_RemoveAssociaManual " & Err.Description, "", "VE_RemoveAssociaManual", False
    Exit Function
    Resume Next
End Function

Private Function VE_VerificaRegraJaDetetada(estrutRes() As Resultados, iRes As Long, iVe As Integer, cod_regra As String, _
                                            seq_realiza As Long, cod_micro As String) As String
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsVe As New ADODB.recordset
    VE_VerificaRegraJaDetetada = ""
    
    Dim strMicro As String
    
    If cod_micro <> "" Then
        strMicro = " = " & BL_TrataStringParaBD(cod_micro)
    Else
        strMicro = " IS NULL "
    End If
    
    sSql = "SELECT * FROM sl_vig_epid WHERE seq_realiza = " & seq_realiza & " AND cod_regra_ve = " & cod_regra
    sSql = sSql & " AND cod_micro " & strMicro & " AND (flg_invisivel IS NULL or flg_invisivel = 0)"
    rsVe.CursorLocation = adUseClient
    rsVe.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVe.Open sSql, gConexao
    If rsVe.RecordCount >= 1 Then
        estrutRes(iRes).vigEpid(iVe).user_cri = CStr(gCodUtilizador)
        estrutRes(iRes).vigEpid(iVe).dt_cri = Bg_DaData_ADO
        estrutRes(iRes).vigEpid(iVe).hr_cri = Bg_DaHora_ADO
        estrutRes(iRes).vigEpid(iVe).cod_estado_ve = gEstadoVePendente
        VE_VerificaRegraJaDetetada = BL_HandleNull(rsVe!seq_vig_epid, "")
    End If
    rsVe.Close
    Set rsVe = Nothing
    
Exit Function
TrataErro:
    VE_VerificaRegraJaDetetada = ""
    BG_LogFile_Erros "Erro  VE_VerificaRegraJaDetetada " & Err.Description, "", "VE_VerificaRegraJaDetetada", False
    Exit Function
    Resume Next
End Function

Private Function VE_ProcuraResAntibReferencia(seq_realiza As String, cod_micro As String, cod_antib As String) As String
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_res_tsq WHERE seq_realiza = " & seq_realiza & " AND cod_micro = " & BL_TrataStringParaBD(cod_micro) & " AND "
    sSql = sSql & " cod_antib IN (SELECT cod_antibio FROM sl_antibio WHERE cod_antibio_ref = " & BL_TrataStringParaBD(cod_antib) & ")"
    rsAntib.CursorType = adOpenStatic
    rsAntib.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAntib.Open sSql, gConexao
    If rsAntib.RecordCount > 0 Then
        VE_ProcuraResAntibReferencia = BL_HandleNull(rsAntib!res_sensib, "")
    Else
        VE_ProcuraResAntibReferencia = ""
    End If
    rsAntib.Close
    Set rsAntib = Nothing
    
Exit Function
TrataErro:
    VE_ProcuraResAntibReferencia = ""
    BG_LogFile_Erros "Erro  VE_ProcuraResAntibReferencia " & Err.Description, "", "VE_ProcuraResAntibReferencia", False
    Exit Function
    Resume Next
End Function


Public Function VE_VerificaUtenteComAlerta(seq_utente As String) As Boolean
    Dim sSql As String
    Dim rsVig As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT   x1.* from sl_vig_epid x1, sl_realiza x2, sl_requis x3, sl_cod_gr_epid x4, sl_cod_regras_ve x5 where x1.seq_realiza = x2.seq_realiza and"
    sSql = sSql & " x2.n_req = x3.n_req and x1.cod_regra_ve = x5.cod_regra_ve and x4.cod_gr_epid  =x5.cod_gr_epid  and (x1.flg_invisivel = 0 or "
    sSql = sSql & " x1.flg_invisivel is null) and x3.seq_utente = " & seq_utente
    rsVig.CursorType = adOpenStatic
    rsVig.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVig.Open sSql, gConexao
    If rsVig.RecordCount > 0 Then
        VE_VerificaUtenteComAlerta = True
    Else
        VE_VerificaUtenteComAlerta = False
    End If
    rsVig.Close
    Set rsVig = Nothing
Exit Function
TrataErro:
    VE_VerificaUtenteComAlerta = False
    BG_LogFile_Erros "Erro  VE_VerificaUtenteComAlerta " & Err.Description, "", "VE_VerificaUtenteComAlerta", False
    Exit Function
    Resume Next
End Function

'BRUNODSANTOS CHVNG-7389 - 03.06.2016
Public Function VE_RetornaNumAdmissoesHosp(ByVal n_proc_1 As String, ByVal Utente As String, ByVal seq_utente As String) As String
'Dim sSql As String
'Dim rs As New ADODB.recordset
Dim iBD_Aberta As Integer


On Error GoTo TrataErro

    If UCase(HIS.nome) = UCase("SONHO") Then
    
        iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
        If iBD_Aberta = 0 Then
           Exit Function
        End If
    
        VE_RetornaNumAdmissoesHosp = SONHO_RetornaNumAdmissoesHospitalares(Utente, seq_utente, n_proc_1)
        
    ElseIf UCase(HIS.nome) = UCase("GH") Then
    
        VE_RetornaNumAdmissoesHosp = RetornaNumAdmissoesHospitalares_GH(Utente, seq_utente, n_proc_1)
    
    End If
    
    If VE_RetornaNumAdmissoesHosp = "-1" Then
        VE_RetornaNumAdmissoesHosp = ""
    End If
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  VE_RetornaNumAdmissoesHosp " & Err.Description, "", "VE_RetornaNumAdmissoesHosp", False
    Exit Function
    Resume Next
    
End Function

'BRUNODSANTOS CHVNG - 7389
Public Function RetornaNumAdmissoesHospitalares_GH(ByVal Utente As String, ByVal seq_ute As String, ByVal n_proc_1 As String) As String
    
    Dim sSql As String
    Dim cmdRetornaNumAdmissoes As New ADODB.Command
    Dim pmt_ute As ADODB.Parameter
    Dim pmt_seq_ute As ADODB.Parameter
    Dim pmt_n_proc_1 As ADODB.Parameter
    Dim pmt_n_admissoes As ADODB.Parameter
    Dim rs As New ADODB.recordset
    
    With cmdRetornaNumAdmissoes
    
        .ActiveConnection = gConexao
        .CommandText = "devolve_num_admissoes_hosp_gh"
        .CommandType = ADODB.CommandTypeEnum.adCmdStoredProc
    End With
    
    Set pmt_ute = cmdRetornaNumAdmissoes.CreateParameter("p_n_utente", adVarChar, adParamInput, 10)
    Set pmt_seq_ute = cmdRetornaNumAdmissoes.CreateParameter("p_n_seq_utente", adVarChar, adParamInput, 10)
    Set pmt_n_proc_1 = cmdRetornaNumAdmissoes.CreateParameter("p_n_proc_1", adVarChar, adParamInput, 10)
    Set pmt_n_admissoes = cmdRetornaNumAdmissoes.CreateParameter("o_n_admissoes", adNumeric, adParamOutput, 10)
    
    cmdRetornaNumAdmissoes.Parameters.Append pmt_ute
    cmdRetornaNumAdmissoes.Parameters.Append pmt_seq_ute
    cmdRetornaNumAdmissoes.Parameters.Append pmt_n_proc_1
    cmdRetornaNumAdmissoes.Parameters.Append pmt_n_admissoes
    
    
    cmdRetornaNumAdmissoes.Parameters.Item("p_n_utente").value = Utente
    cmdRetornaNumAdmissoes.Parameters.Item("p_n_seq_utente").value = seq_ute
    cmdRetornaNumAdmissoes.Parameters.Item("p_n_proc_1").value = n_proc_1
    
    cmdRetornaNumAdmissoes.Execute
    
    RetornaNumAdmissoesHospitalares_GH = CStr(cmdRetornaNumAdmissoes("o_n_admissoes").value)
    
    GoTo fim

Exit Function
TrataErro:
    RetornaNumAdmissoesHospitalares_GH = "-1"
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Admissoes_Hosp_GH", "RetornaNumAdmissoesHospitalares_GH"
    Exit Function
    Resume Next
    

fim:
Set pmt_ute = Nothing
Set pmt_seq_ute = Nothing
Set pmt_n_proc_1 = Nothing
Set pmt_n_admissoes = Nothing
Exit Function

Resume Next

End Function

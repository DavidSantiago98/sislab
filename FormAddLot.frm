VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormAddLot 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   13425
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13215
   Icon            =   "FormAddLot.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   13425
   ScaleWidth      =   13215
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   " Registo  "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   120
      TabIndex        =   28
      Top             =   9360
      Width           =   12975
      Begin VB.Label LabelHourUpdate 
         Caption         =   "LabelHourUpdate"
         Height          =   255
         Left            =   11160
         TabIndex        =   34
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label LabelHourCreation 
         Caption         =   "LabelHourCreation"
         Height          =   255
         Left            =   11160
         TabIndex        =   33
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label LabelDateUpdate 
         Caption         =   "LabelDateUpdate"
         Height          =   255
         Left            =   9480
         TabIndex        =   32
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label LabelDateCreation 
         Caption         =   "LabelDateCreation"
         Height          =   255
         Left            =   9480
         TabIndex        =   31
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label LabelUserUpdate 
         Caption         =   "LabelUserUpdate"
         Height          =   255
         Left            =   720
         TabIndex        =   30
         Top             =   720
         Width           =   8415
      End
      Begin VB.Label LabelUserCreation 
         Caption         =   "LabelUserCreation"
         Height          =   255
         Left            =   720
         TabIndex        =   29
         Top             =   360
         Width           =   8415
      End
   End
   Begin VB.ComboBox ComboBoxAnalysis 
      Height          =   315
      Left            =   120
      Style           =   1  'Simple Combo
      TabIndex        =   27
      Top             =   12240
      Width           =   2175
   End
   Begin VB.ComboBox ComboBoxAnalyser 
      Height          =   315
      Left            =   120
      Style           =   1  'Simple Combo
      TabIndex        =   26
      Top             =   11880
      Width           =   2175
   End
   Begin VB.ComboBox ComboBoxApplication 
      Height          =   315
      Left            =   120
      Style           =   1  'Simple Combo
      TabIndex        =   25
      Top             =   11520
      Width           =   2175
   End
   Begin MSComctlLib.ImageList ImageListSearch 
      Left            =   2040
      Top             =   12600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":1CE6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":39C0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListTreeViewIcons 
      Left            =   1440
      Top             =   12600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":569A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":5A34
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":5DCE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":6168
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":6502
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListListViewIcons 
      Left            =   840
      Top             =   12600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":689C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAddLot.frx":6C36
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   495
      Left            =   120
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   12
      Top             =   12600
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Caption         =   " Lotes "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   9375
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   12975
      Begin VB.CommandButton CommandButtonLookRecord 
         Height          =   375
         Left            =   11400
         Picture         =   "FormAddLot.frx":6FD0
         Style           =   1  'Graphical
         TabIndex        =   24
         ToolTipText     =   "Procurar Registo"
         Top             =   6360
         Width           =   1455
      End
      Begin VB.CommandButton CommandButtonRefresh 
         Height          =   375
         Left            =   9960
         Picture         =   "FormAddLot.frx":773A
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Refrescar ecr�"
         Top             =   6360
         Width           =   1455
      End
      Begin VB.CommandButton CommandButtonActivate 
         Height          =   375
         Left            =   8520
         Picture         =   "FormAddLot.frx":7EA4
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Activar lote"
         Top             =   6360
         Width           =   1455
      End
      Begin VB.CommandButton CommandButtonInactivate 
         Height          =   375
         Left            =   8520
         Picture         =   "FormAddLot.frx":860E
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Inactivar lote"
         Top             =   6360
         Width           =   1455
      End
      Begin VB.CommandButton CommandButtonPreview 
         Height          =   375
         Left            =   7080
         Picture         =   "FormAddLot.frx":8D78
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Pr�-visualizar registos correntes"
         Top             =   6360
         Width           =   1455
      End
      Begin VB.CommandButton CommandButtonClean 
         Height          =   375
         Left            =   5640
         Picture         =   "FormAddLot.frx":94E2
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Limpar Campos"
         Top             =   6360
         Width           =   1455
      End
      Begin MSComctlLib.ListView ListViewUserLots 
         Height          =   6015
         Left            =   4200
         TabIndex        =   13
         Top             =   240
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   10610
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.CommandButton CommandButtonSave 
         Height          =   375
         Left            =   4200
         Picture         =   "FormAddLot.frx":9C4C
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Inserir Registo"
         Top             =   6360
         Width           =   1455
      End
      Begin VB.PictureBox PictureBoxLeftPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H80000008&
         Height          =   6495
         Left            =   120
         ScaleHeight     =   6465
         ScaleWidth      =   3945
         TabIndex        =   2
         Top             =   240
         Width           =   3975
         Begin VB.PictureBox PictureBoxAnalysis 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3600
            ScaleHeight     =   285
            ScaleMode       =   0  'User
            ScaleWidth      =   255
            TabIndex        =   19
            Top             =   840
            Width           =   255
         End
         Begin VB.PictureBox PictureBoxAnalyser 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3600
            ScaleHeight     =   285
            ScaleMode       =   0  'User
            ScaleWidth      =   255
            TabIndex        =   18
            Top             =   480
            Width           =   255
         End
         Begin VB.TextBox TextBoxApplication 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1080
            Locked          =   -1  'True
            TabIndex        =   17
            Top             =   120
            Width           =   2490
         End
         Begin VB.PictureBox PictureBoxApplication 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3600
            ScaleHeight     =   285
            ScaleMode       =   0  'User
            ScaleWidth      =   255
            TabIndex        =   16
            Top             =   120
            Width           =   255
         End
         Begin VB.TextBox TextBoxAnalyser 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1080
            Locked          =   -1  'True
            TabIndex        =   15
            Top             =   480
            Width           =   2490
         End
         Begin VB.TextBox TextBoxAnalysis 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1080
            Locked          =   -1  'True
            TabIndex        =   14
            Top             =   840
            Width           =   2490
         End
         Begin VB.TextBox TextBoxLot 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1080
            TabIndex        =   9
            Top             =   1200
            Width           =   2490
         End
         Begin MSComCtl2.MonthView MonthViewInitialDate 
            Height          =   2310
            Left            =   1080
            TabIndex        =   10
            Top             =   1560
            Width           =   2460
            _ExtentX        =   4339
            _ExtentY        =   4075
            _Version        =   393216
            ForeColor       =   -2147483630
            BackColor       =   16777215
            BorderStyle     =   1
            Appearance      =   0
            MonthBackColor  =   16777215
            StartOfWeek     =   147324930
            TitleBackColor  =   16574424
            TrailingForeColor=   16574424
            CurrentDate     =   39477
         End
         Begin MSComCtl2.MonthView MonthViewFinalDate 
            Height          =   2310
            Left            =   1080
            TabIndex        =   11
            Top             =   3960
            Width           =   2460
            _ExtentX        =   4339
            _ExtentY        =   4075
            _Version        =   393216
            ForeColor       =   -2147483630
            BackColor       =   16777215
            BorderStyle     =   1
            Appearance      =   0
            MonthBackColor  =   16777215
            StartOfWeek     =   147324930
            TitleBackColor  =   16574424
            TrailingForeColor=   16574424
            CurrentDate     =   39477
         End
         Begin VB.Label Label6 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Data Final"
            Height          =   255
            Left            =   120
            TabIndex        =   8
            Top             =   3960
            Width           =   1215
         End
         Begin VB.Label Label5 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Data Inicial"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   1560
            Width           =   975
         End
         Begin VB.Label Label4 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Lote"
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   1200
            Width           =   1215
         End
         Begin VB.Label Label3 
            BackColor       =   &H00FFFFFF&
            Caption         =   "An�lise"
            Height          =   255
            Left            =   120
            TabIndex        =   5
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label Label2 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Aparelho"
            Height          =   255
            Left            =   120
            TabIndex        =   4
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Aplica��o"
            Height          =   255
            Left            =   120
            TabIndex        =   3
            Top             =   120
            Width           =   1215
         End
      End
      Begin MSComctlLib.TreeView TreeViewAllLots 
         Height          =   2415
         Left            =   120
         TabIndex        =   1
         Top             =   6840
         Width           =   12735
         _ExtentX        =   22463
         _ExtentY        =   4260
         _Version        =   393217
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         FullRowSelect   =   -1  'True
         ImageList       =   "ImageListTreeViewIcons"
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "FormAddLot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2008.05.09   .
'     .        � 2007 CPC|HS        .
'      .............................

Option Explicit

' Constant to define lot state active.
Const cLotActive = True

' Constant to define lot state inactive./
Const cLotInactive = False

' Constant to define listview text color.
Const cLightGray = &H808080

' Constant representing the light yellow color.
Const cLightYellow = &H80000018

' Constant representing the white color.
Const cWhite = &HFFFFFF

' Constant representing the light blue color.
Const cLightBlue = &HFCE7D8

' Constant representing a state of inactive window.
Const cWindowInactive = 0

' Constant representing a state of active window.
Const cWindowActive = 1

' Constant representing a state of busy window.
Const cWindowBusy = 2

' Keep window state.
Private iWindowState As Integer

' Keep active field.
Private oActiveField As Object

' Keep focus field.
Private oFocusField   As Object

' Reagents lots structure.
Private tReagentsLots() As ReagentLot

' Sets a new data type named ReagentLot.
Private Type ReagentLot
        
    iApplicationCode As Integer
    iAnalyserCode As Integer
    iAnalysisCode As Integer
    sLotCode As String
    sInitialDate As String
    sFinalDate As String
    iUserCreation As Integer
    sDateCreation As String
    sHourCreation As String
    iUserUpdate As Integer
    sDateUpdate As String
    sHourUpdate As String
    bLotState As Boolean
        
End Type

' pferreira 2008.04.30
' Gotfocus event of the object ComboBoxApplication.
Private Sub ComboBoxApplication_GotFocus()
 
    On Error GoTo ErrorHandler
    TextBoxApplication.Text = ComboBoxApplication.List(ComboBoxApplication.ListIndex)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ComboBoxApplication_GotFocus' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Gotfocus event of the object ComboBoxAnalyser.
Private Sub ComboBoxAnalyser_GotFocus()
    On Error GoTo ErrorHandler
    TextBoxAnalyser.Text = ComboBoxAnalyser.List(ComboBoxAnalyser.ListIndex)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ComboBoxAnalyser_GotFocus' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Gotfocus event of the object ComboBoxAnalysis.
Private Sub ComboBoxAnalysis_GotFocus()
    On Error GoTo ErrorHandler
    TextBoxAnalysis.Text = ComboBoxAnalysis.List(ComboBoxAnalysis.ListIndex)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ComboBoxAnalysis_GotFocus' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Click event of the object CommandButtonActivate.
Private Sub CommandButtonActivate_Click()
    
    On Error GoTo ErrorHandler
    gMsgTitulo = " Activar Lote"
    gMsgMsg = " Tem a certeza que quer activar este lote ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbYes) Then
        If (ManageActiveRecords(ListViewUserLots.SelectedItem.Index)) Then
            ManageCommandButtonActivate False
            ChangeLotState ListViewUserLots.SelectedItem.Index, cLotActive
            RefreshWindow
        End If
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CommandButtonActivate_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Click event of the object CommandButtonInactivate.
Private Sub CommandButtonInactivate_Click()
    
    On Error GoTo ErrorHandler
    gMsgTitulo = " Inactivar Lote"
    gMsgMsg = " Tem a certeza que quer inactivar este lote ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If (gMsgResp = vbYes) Then
        ManageCommandButtonActivate True
        ChangeLotState ListViewUserLots.SelectedItem.Index, cLotInactive
        RefreshWindow
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CommandButtonInactivate_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.05.07
' Click event of the object CommandButtonLookRecord.
Private Sub CommandButtonLookRecord_Click()
 
    On Error GoTo ErrorHandler
    FindItemByLot
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CommandButtonLookRecord_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.05.07
' Click event of the object CommandButtonRefresh.
Private Sub CommandButtonRefresh_Click()
    
    On Error GoTo ErrorHandler
    RefreshWindow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CommandButtonRefresh_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Click event of the object CommandButtonPreview.
Private Sub CommandButtonPreview_Click()
    
    On Error GoTo ErrorHandler
    gImprimirDestino = 0
    FuncaoImprimir
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CommandButtonPreview_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Click event of the object CommandButtonClean.
Private Sub CommandButtonClean_Click()
    
    On Error GoTo ErrorHandler
    FuncaoLimpar
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CommandButtonClean_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Click event of the object CommandButtonSave.
Private Sub CommandButtonSave_Click()
    
    On Error GoTo ErrorHandler
    BL_Inserir_Modificar
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CommandButtonSave_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' DateClick event of the object MonthViewInitialDate.
Private Sub MonthViewInitialDate_DateClick(ByVal DateClicked As Date)

    On Error GoTo ErrorHandler
    CommandButtonSave.SetFocus
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'MonthViewInitialDate_DateClick' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub
   
' pferreira 2008.04.30
' DateClick event of the object MonthViewFinalDate.
Private Sub MonthViewFinalDate_DateClick(ByVal DateClicked As Date)

    On Error GoTo ErrorHandler
    CommandButtonSave.SetFocus
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'MonthViewFinalDate_DateClick' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.30
' Lostfocus event of the object TextBoxLot.
Private Sub TextBoxLot_LostFocus()
    
    On Error GoTo ErrorHandler
    TextBoxLot.Text = UCase(TextBoxLot.Text)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'TextBoxLot_LostFocus' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Click event of the object PictureBoxApplication.
Private Sub PictureBoxApplication_Click()
  
    Dim sSearchKeys(1 To 1) As String
    Dim sWindowFields(1 To 1) As String
    Dim lFielsLenght(1 To 1) As Long
    Dim sFielsHeaders(1 To 1) As String
    Dim vSearchResults(1 To 1)  As Variant
    Dim sTableName As String
    Dim sSearchField As String
    Dim oResultFields As New ClassPesqResultados
    Dim bSearchCancel As Boolean
    
    On Error GoTo ErrorHandler
    PictureBoxApplication.Picture = ImageListSearch.ListImages(3).Picture
    sSearchKeys(1) = "cod_aplic"
    sWindowFields(1) = "descr_aplic"
    lFielsLenght(1) = 4000
    sFielsHeaders(1) = "Descri��o"
    oResultFields.InicializaResultados (1)
    If gSGBD = gOracle Then
        sTableName = "gc_aplic"
    ElseIf gSGBD = gSqlServer Then
        sTableName = "dbo.gescom.gc_Aplic"
    End If
    sSearchField = "descr_aplic"
    If (FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, sSearchKeys, sWindowFields, oResultFields, lFielsLenght, _
            sFielsHeaders, Empty, sTableName, Empty, sSearchField, Empty, " Aplica��es") = True) Then
        FormPesqRapidaAvancada.Show vbModal
        oResultFields.RetornaResultados vSearchResults, bSearchCancel
        If (Not vSearchResults) Then: BG_MostraComboSel vSearchResults(1), ComboBoxApplication: ComboBoxApplication.SetFocus
    Else
        BG_Mensagem mediMsgBox, "N�o existem aplica��es codificadas", vbExclamation, " Aten��o"
    End If
    PictureBoxApplication.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PictureBoxApplication_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    PictureBoxApplication.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
        
End Sub

' pferreira 2008.04.29
' Click event of the object PictureBoxAnalyser.
Private Sub PictureBoxAnalyser_Click()
  
    Dim sSearchKeys(1 To 1) As String
    Dim sWindowFields(1 To 1) As String
    Dim lFielsLenght(1 To 1) As Long
    Dim sFielsHeaders(1 To 1) As String
    Dim vSearchResults(1 To 1)  As Variant
    Dim sTableName As String
    Dim sSearchField As String
    Dim oResultFields As New ClassPesqResultados
    Dim bSearchCancel As Boolean
    
    On Error GoTo ErrorHandler
    PictureBoxAnalyser.Picture = ImageListSearch.ListImages(3).Picture
    sSearchKeys(1) = "seq_apar"
    sWindowFields(1) = "descr_apar"
    lFielsLenght(1) = 4000
    sFielsHeaders(1) = "Descri��o"
    oResultFields.InicializaResultados (1)
    sTableName = "gc_apar"
    sSearchField = "descr_apar"
    If (FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, sSearchKeys, sWindowFields, oResultFields, lFielsLenght, _
            sFielsHeaders, Empty, sTableName, Empty, sSearchField, Empty, " Aparelhos") = True) Then
        FormPesqRapidaAvancada.Show vbModal
        oResultFields.RetornaResultados vSearchResults, bSearchCancel
        If (Not vSearchResults) Then: BG_MostraComboSel vSearchResults(1), ComboBoxAnalyser: ComboBoxAnalyser.SetFocus
    Else
        BG_Mensagem mediMsgBox, "N�o existem aparelhos codificados", vbExclamation, " Aten��o"
    End If
    PictureBoxAnalyser.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PictureBoxAnalyser_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    PictureBoxAnalyser.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
        
End Sub

' pferreira 2008.04.29
' Click event of the object PictureBoxAnalysis.
Private Sub PictureBoxAnalysis_Click()
  
    Dim sSearchKeys(1 To 1) As String
    Dim sWindowFields(1 To 2) As String
    Dim lFielsLenght(1 To 2) As Long
    Dim sFielsHeaders(1 To 2) As String
    Dim vSearchResults(1 To 1)  As Variant
    Dim sTableName As String
    Dim sSearchField As String
    Dim oResultFields As New ClassPesqResultados
    Dim bSearchCancel As Boolean
    Dim sWhere As String
    
    On Error GoTo ErrorHandler
    PictureBoxAnalysis.Picture = ImageListSearch.ListImages(3).Picture
    sSearchKeys(1) = "seq_ana_apar"
    sWindowFields(1) = "cod_simpl"
    sWindowFields(2) = "descr_ana"
    sFielsHeaders(1) = "C�digo"
    lFielsLenght(1) = 1000
    sFielsHeaders(2) = "Descri��o"
    lFielsLenght(2) = 4000
    oResultFields.InicializaResultados (1)
    sTableName = "gc_ana_apar"
    sSearchField = "descr_ana"
    sWhere = "cod_simpl is not null and descr_ana is not null"
    If (FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, sSearchKeys, sWindowFields, oResultFields, lFielsLenght, _
            sFielsHeaders, sWhere, sTableName, Empty, sSearchField, Empty, " An�lises") = True) Then
        FormPesqRapidaAvancada.Show vbModal
        oResultFields.RetornaResultados vSearchResults, bSearchCancel
        If (Not vSearchResults) Then: BG_MostraComboSel vSearchResults(1), ComboBoxAnalysis: ComboBoxAnalysis.SetFocus
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises codificadas", vbExclamation, " Aten��o"
    End If
    PictureBoxAnalysis.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PictureBoxAnalysis_Click' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    PictureBoxAnalysis.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
        
End Sub

' pferreira 2008.04.29
' Mouse Move event of the object PictureBoxLeftPanel.
Private Sub PictureBoxLeftPanel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    On Error GoTo ErrorHandler
    If (PictureBoxApplication.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxApplication.Picture = ImageListSearch.ListImages(1).Picture
    If (PictureBoxAnalyser.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxAnalyser.Picture = ImageListSearch.ListImages(1).Picture
    If (PictureBoxAnalysis.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxAnalysis.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PictureBoxLeftPanel_MouseMove' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Mouse Move event of the object PictureBoxApplication.
Private Sub PictureBoxApplication_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (PictureBoxApplication.Picture <> ImageListSearch.ListImages(2).Picture) Then: PictureBoxApplication.Picture = ImageListSearch.ListImages(2).Picture
    If (PictureBoxAnalyser.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxAnalyser.Picture = ImageListSearch.ListImages(1).Picture
    If (PictureBoxAnalysis.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxAnalysis.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PictureBoxApplication_MouseMove' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
        
End Sub

' pferreira 2008.04.29
' Mouse Move event of the object PictureBoxAnalyser.
Private Sub PictureBoxAnalyser_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (PictureBoxApplication.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxApplication.Picture = ImageListSearch.ListImages(1).Picture
    If (PictureBoxAnalyser.Picture <> ImageListSearch.ListImages(2).Picture) Then: PictureBoxAnalyser.Picture = ImageListSearch.ListImages(2).Picture
    If (PictureBoxAnalysis.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxAnalysis.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PictureBoxAnalyser_MouseMove' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
        
End Sub


' pferreira 2008.04.29
' Mouse Move event of the object PictureBoxAnalysis.
Private Sub PictureBoxAnalysis_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (PictureBoxApplication.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxApplication.Picture = ImageListSearch.ListImages(1).Picture
    If (PictureBoxAnalyser.Picture <> ImageListSearch.ListImages(1).Picture) Then: PictureBoxAnalyser.Picture = ImageListSearch.ListImages(1).Picture
    If (PictureBoxAnalysis.Picture <> ImageListSearch.ListImages(2).Picture) Then: PictureBoxAnalysis.Picture = ImageListSearch.ListImages(2).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PictureBoxAnalysis_MouseMove' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
        
End Sub

' pferreira 2008.04.29
' Item click event of the control ListViewUserLots.
Private Sub ListViewUserLots_ItemClick(ByVal �Item� As MSComctlLib.ListItem)
    
    On Error GoTo ErrorHandler
    ManageCommandButtonActivate (Not tReagentsLots(�Item�.Index).bLotState)
    WriteIntoFields (�Item�.Index)
    SetUserLabelsContent (�Item�.Index)
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "Activo"
    CommandButtonSave.ToolTipText = "Modificar registo"
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ListViewUserLots_ItemClick' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.05.07
' Item check event of the control ListViewUserLots.
Private Sub ListViewUserLots_ItemCheck(ByVal �Item� As MSComctlLib.ListItem)

    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    For Each vItem In ListViewUserLots.ListItems
        If (vItem.Index <> �Item�.Index) Then: vItem.Checked = False
    Next
    �Item�.Selected = True
    �Item�.EnsureVisible
    ListViewUserLots_ItemClick �Item�
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ListViewUserLots_ItemCheck' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Key down event of the control ListViewUserLots.
Private Sub ListViewUserLots_KeyDown(�KeyCode� As Integer, �Shift� As Integer)
    
    On Error GoTo ErrorHandler
    If (ListViewUserLots.ListItems.Count < 1) Then: Exit Sub
    Select Case �KeyCode�
        Case vbKeyUp, vbKeyDown: ListViewUserLots_ItemClick ListViewUserLots.SelectedItem
    End Select
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ListViewUserLots_KeyDown' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Form load event.
Private Sub Form_Load()
    
    On Error GoTo ErrorHandler
    LoadEvent
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'Form_Load' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Form activate event.
Private Sub Form_Activate()
    
    On Error GoTo ErrorHandler
    ActivateEvent
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'Form_Activate' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.29
' Form unload event.
Private Sub Form_Unload(�Cancel� As Integer)
    
    On Error GoTo ErrorHandler
    UnloadEvent
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'Form_Unload' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.29
' Initializations of the objects of the form.
Private Sub Initializations()
    
    On Error GoTo ErrorHandler
    Me.caption = " Registo de Lotes de Reagentes"
    Me.left = 100
    Me.top = 100
    Me.Width = 13305
    Me.Height = 11025
    InitiateListViewUserLots
    InitiateTreeViewAllLots
    InitiatePictureBoxes
    InitTReagentsLots
    Exit Sub
                  
ErrorHandler:
    BG_LogFile_Erros "Error in function 'Initializations' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.29
' Event function to load form objects.
Private Sub LoadEvent()
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, "Inicializar �cran."
    BL_ToolbarEstadoN cWindowBusy
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    Initializations
    SetDefaultValues
    BG_ParametrizaPermissoes_ADO Me.Name
    BG_BeginTransaction
    FuncaoProcurar
    iWindowState = cWindowActive
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    BL_ToolbarEstadoN cWindowActive
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'EventoLoad' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Event function to activate form objects.
Private Sub ActivateEvent()
    
    On Error GoTo ErrorHandler
    Set gFormActivo = Me
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    BL_ToolbarEstadoN iWindowState
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Anterior", "InActivo"
    BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
    BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
    CommandButtonSave.ToolTipText = "Inserir registo"
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EventoActivate' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.29
' Event function to unload form objects.
Private Sub UnloadEvent()
    
    On Error GoTo ErrorHandler
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN cWindowInactive
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EventoUnload' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.28
' Clean all objects of the form.
Private Sub CleanFields()
    
    On Error GoTo ErrorHandler
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    CommandButtonSave.ToolTipText = "Inserir registo"
    TextBoxLot.Text = Empty
    TextBoxApplication = Empty
    TextBoxAnalyser = Empty
    TextBoxAnalysis = Empty
    ListViewUserLots.ListItems.Clear
    TreeViewAllLots.Nodes.Clear
    LabelUserCreation.caption = Empty
    LabelUserUpdate.caption = Empty
    LabelDateCreation.caption = Empty
    LabelDateUpdate.caption = Empty
    LabelHourCreation.caption = Empty
    LabelHourUpdate.caption = Empty
    ComboBoxApplication.ListIndex = mediComboValorNull
    ComboBoxAnalyser.ListIndex = mediComboValorNull
    ComboBoxAnalysis.ListIndex = mediComboValorNull
    MonthViewInitialDate.value = Bg_DaData_ADO
    MonthViewFinalDate.value = Bg_DaData_ADO
    ManageCommandButtonActivate True
    InitTReagentsLots
    Exit Sub
            
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CleanFields' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

' pferreira 2008.04.28
' Function clean of the form.
Public Sub FuncaoLimpar()
    
    On Error GoTo ErrorHandler
    CleanFields
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoLimpar' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Function looking of the form.
Public Sub FuncaoProcurar()
    
    On Error GoTo ErrorHandler
    iWindowState = cWindowBusy
    BL_InicioProcessamento Me, " A procurar registos"
    BL_ToolbarEstadoN cWindowBusy
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    RefreshWindow
    BL_FimProcessamento Me
    BL_ToolbarEstadoN cWindowActive
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
        
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoProcurar' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Function previous of the form.
Public Sub FuncaoAnterior()

    On Error GoTo ErrorHandler
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoAnterior' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
        
End Sub

' pferreira 2008.04.29
' Function next of the form.
Public Sub FuncaoSeguinte()

    On Error GoTo ErrorHandler
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoSeguinte' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
     
End Sub

' pferreira 2008.04.29
' Function insert of the form.
Public Sub FuncaoInserir()
    
    On Error GoTo ErrorHandler
    gMsgTitulo = "Inserir"
    gMsgMsg = " Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    Me.SetFocus
    
    If (gMsgResp = vbYes) Then
        BL_InicioProcessamento Me, "A inserir registo."
        BL_ToolbarEstadoN cWindowBusy
        Me.MousePointer = vbArrowHourglass
        MDIFormInicio.MousePointer = vbArrowHourglass
        InsertData
        RefreshWindow
        If (gSQLError <> 0) Then: BG_Mensagem mediMsgBox, "Erro ao inserir registo!", vbError, "ERRO"
        BL_FimProcessamento Me
        BL_ToolbarEstadoN cWindowActive
        Me.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    End If
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoInserir' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.05.07
' Function update of the form.
Public Sub FuncaoModificar()
    
    On Error GoTo ErrorHandler
    gMsgTitulo = "Modificar"
    gMsgMsg = " Tem a certeza que quer Modificar estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    Me.SetFocus
    
    If (gMsgResp = vbYes) Then
        BL_InicioProcessamento Me, "A modificar registo."
        BL_ToolbarEstadoN cWindowBusy
        Me.MousePointer = vbArrowHourglass
        MDIFormInicio.MousePointer = vbArrowHourglass
        UpdateData
        RefreshWindow
        If (gSQLError <> 0) Then: BG_Mensagem mediMsgBox, "Erro ao modificar registo!", vbError, "ERRO"
        BL_FimProcessamento Me
        BL_ToolbarEstadoN cWindowActive
        Me.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    End If
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoModificar' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.30
' Function print of the form.
Public Sub FuncaoImprimir()
    
    On Error GoTo ErrorHandler
    gMsgTitulo = " Imprimir"
    gMsgMsg = "Tem a certeza que quer imprimir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    Me.SetFocus
    If (gMsgResp = vbYes) Then
        BL_InicioProcessamento Me, "A imprimir registos."
        BL_ToolbarEstadoN cWindowBusy
        Me.MousePointer = vbArrowHourglass
        MDIFormInicio.MousePointer = vbArrowHourglass
        PrintRecords
        BL_FimProcessamento Me
        BL_ToolbarEstadoN cWindowActive
        Me.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoImprimir' in form FormAplicativoSLO (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.28
' Set default values of the form objects.
Private Sub SetDefaultValues()
    
    On Error GoTo ErrorHandler
    BG_PreencheComboBD_ADO "gc_aplic", "cod_aplic", "descr_aplic", ComboBoxApplication, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gc_apar", "seq_apar", "descr_apar", ComboBoxAnalyser, mediAscComboCodigo
    BG_PreencheComboBD_ADO "select cod_simpl, descr_ana, seq_ana_apar from gc_ana_apar where cod_simpl is not null and descr_ana is not null", "seq_ana_apar", "descr_ana", ComboBoxAnalysis, mediAscComboCodigo
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SetDefaultValues' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.29
' Initiate the list view of results.
Private Sub InitiateListViewUserLots()
    
    On Error GoTo ErrorHandler
    With ListViewUserLots
        .ColumnHeaders.Add(, , "Lote", 1500, lvwColumnLeft).Key = "LOT"
        .ColumnHeaders.Add(, , "Aplica��o", 1000, lvwColumnLeft).Key = "APPLICATION"
        .ColumnHeaders.Add(, , "Aparelho", 1500, lvwColumnLeft).Key = "ANALYSER"
        .ColumnHeaders.Add(, , "An�lise", 2350, lvwColumnLeft).Key = "ANALYSIS"
        .ColumnHeaders.Add(, , "Data Inicial", 1000, lvwColumnLeft).Key = "INITIALDATE"
        .ColumnHeaders.Add(, , "Data Final", 1000, lvwColumnLeft).Key = "FINALDATE"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .SmallIcons = ImageListListViewIcons
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = True
        .ForeColor = cLightGray
        .MultiSelect = False
    End With
    SetListViewColor ListViewUserLots, PictureListColor, cLightBlue, cWhite
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'InitiateListViewUserLots' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Initiate the tree view of all lots.
Private Sub InitiateTreeViewAllLots()
    
    On Error GoTo ErrorHandler
    With TreeViewAllLots
        .FullRowSelect = True
        .Checkboxes = False
        .Style = tvwTreelinesPlusMinusPictureText
        .LineStyle = tvwRootLines
    End With
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'InitiateTreeViewAllLots' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Paint a list view with colored bars.
Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SetListViewColor' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.29
' Add an reagent lot record to the reagents lots structure.
Private Sub AddReagentLot(�iApplicationCode� As Integer, �iAnalyserCode� As Integer, �iAnalysisCode� As Integer, _
                          �sLotCode� As String, �sInitialDate� As String, �sFinalDate� As String, �iUserCreation� As Integer, _
                          �sDateCreation� As String, �sHourCreation� As String, Optional �iUserUpdate� As Integer, _
                          Optional �sDateUpdate� As String, Optional �sHourUpdate� As String, Optional �bLotState� As Boolean)

    On Error GoTo ErrorHandler
    ReDim Preserve tReagentsLots(UBound(tReagentsLots) + 1)
    tReagentsLots(UBound(tReagentsLots)).iApplicationCode = �iApplicationCode�
    tReagentsLots(UBound(tReagentsLots)).iAnalyserCode = �iAnalyserCode�
    tReagentsLots(UBound(tReagentsLots)).iAnalysisCode = �iAnalysisCode�
    tReagentsLots(UBound(tReagentsLots)).sLotCode = �sLotCode�
    tReagentsLots(UBound(tReagentsLots)).sInitialDate = �sInitialDate�
    tReagentsLots(UBound(tReagentsLots)).sFinalDate = �sFinalDate�
    tReagentsLots(UBound(tReagentsLots)).iUserCreation = �iUserCreation�
    tReagentsLots(UBound(tReagentsLots)).sDateCreation = �sDateCreation�
    tReagentsLots(UBound(tReagentsLots)).sHourCreation = �sHourCreation�
    tReagentsLots(UBound(tReagentsLots)).iUserUpdate = �iUserUpdate�
    tReagentsLots(UBound(tReagentsLots)).sDateUpdate = �sDateUpdate�
    tReagentsLots(UBound(tReagentsLots)).sHourUpdate = �sHourUpdate�
    tReagentsLots(UBound(tReagentsLots)).bLotState = �bLotState�
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AddReagentLot' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.29
' Clean reagents lots structure.
Private Sub InitTReagentsLots()
    
    On Error GoTo ErrorHandler
    ReDim tReagentsLots(0 To 0)
    tReagentsLots(0).iApplicationCode = Empty
    tReagentsLots(0).iAnalyserCode = Empty
    tReagentsLots(0).iAnalysisCode = Empty
    tReagentsLots(0).sLotCode = Empty
    tReagentsLots(0).sInitialDate = Empty
    tReagentsLots(0).sFinalDate = Empty
    tReagentsLots(0).iUserCreation = Empty
    tReagentsLots(0).sDateCreation = Empty
    tReagentsLots(0).sHourCreation = Empty
    tReagentsLots(0).iUserUpdate = Empty
    tReagentsLots(0).sDateUpdate = Empty
    tReagentsLots(0).sHourUpdate = Empty
    tReagentsLots(0).bLotState = Empty
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InitTReagentsLots' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2007.08.29
' Writes the reagents lots into the listview (only for the current user).
Private Sub WriteIntoListView()

    Dim lItem As Long
    Dim lCursor As Long
            
    On Error GoTo ErrorHandler
    lCursor = ListViewUserLots.MousePointer
    ListViewUserLots.MousePointer = vbHourglass
    LockWindowUpdate ListViewUserLots.hwnd
    For lItem = 1 To UBound(tReagentsLots)
'        If (tReagentsLots(LItem).iUserCreation = gCodUtilizador) Then
            With ListViewUserLots.ListItems.Add(, , tReagentsLots(lItem).sLotCode, , IIf(tReagentsLots(lItem).bLotState, 1, 2))
                .ListSubItems.Add , , GetApplicationDescription(tReagentsLots(lItem).iApplicationCode)
                .ListSubItems.Add , , GetAnalyserDescription(tReagentsLots(lItem).iAnalyserCode)
                .ListSubItems.Add , , GetAnalysisDescription(tReagentsLots(lItem).iAnalysisCode)
                .ListSubItems.Add , , tReagentsLots(lItem).sInitialDate
                .ListSubItems.Add , , tReagentsLots(lItem).sFinalDate
            End With
'        End If
    Next
    LockWindowUpdate 0&
    ListViewUserLots.MousePointer = lCursor
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'WriteIntoListView' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    LockWindowUpdate 0&
    ListViewUserLots.MousePointer = lCursor
    Exit Sub

End Sub

' pferreira 2007.08.30
' Writes the reagents lots into the treeview (for all year).
Private Sub WriteIntoTreeView()

    Dim vItem As Variant
    Dim iMonth As Integer
    Dim sLotCode As String
    Dim sApplicationDescription As String
    Dim sAnalyserDescription As String
    Dim sAnalysisDescription As String
    Dim sMothName As String
    Dim parent As Node
    Dim lCursor As Long
    Dim oMonthRecords As Collection
            
    On Error GoTo ErrorHandler
    lCursor = ListViewUserLots.MousePointer
    TreeViewAllLots.MousePointer = vbHourglass
    LockWindowUpdate TreeViewAllLots.hwnd
    For iMonth = mvwJanuary To mvwDecember
        Set oMonthRecords = SelectRecordsByMonth(iMonth)
        sMothName = GetMonthName(iMonth)
        Set parent = TreeViewAllLots.Nodes.Add(, , , sMothName & Space(10 - Len(sMothName)) & "(" & oMonthRecords.Count & ")", IIf(oMonthRecords.Count > 0, 4, 5))
        For Each vItem In oMonthRecords
            If (Year(tReagentsLots(vItem).sDateCreation) = Year(Bg_DaData_ADO)) Then
                sLotCode = tReagentsLots(vItem).sLotCode
                sApplicationDescription = GetApplicationDescription(tReagentsLots(vItem).iApplicationCode)
                sAnalyserDescription = GetAnalyserDescription(tReagentsLots(vItem).iAnalyserCode)
                sAnalysisDescription = GetAnalysisDescription(tReagentsLots(vItem).iAnalysisCode)
                TreeViewAllLots.Nodes.Add(parent.Index, tvwChild, , "[" & sLotCode & "]" & Space(15 - Len(sLotCode)) & " - [" & sApplicationDescription & "]" & Space(6 - Len(sApplicationDescription)) & " - [" & sAnalyserDescription & "]" & Space(15 - Len(sAnalyserDescription)) & " - [" & sAnalysisDescription & "]", IIf(tReagentsLots(vItem).bLotState, 3, 2), IIf(tReagentsLots(vItem).bLotState, 3, 2)).BackColor = cLightBlue
            End If
        Next
        If (parent.children = 0) Then: parent.Text = sMothName & Space(10 - Len(sMothName)): parent.Image = 5
    Next
    LockWindowUpdate 0&
    TreeViewAllLots.MousePointer = lCursor
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'WriteIntoTreeView' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    LockWindowUpdate 0&
    TreeViewAllLots.MousePointer = lCursor
    Exit Sub

End Sub

' pferreira 2007.08.29
' Writes the list view item content in the left fields.
Private Sub WriteIntoFields(�lItem� As Long)

    On Error GoTo ErrorHandler
    BG_MostraComboSel tReagentsLots(�lItem�).iApplicationCode, ComboBoxApplication
    BG_MostraComboSel tReagentsLots(�lItem�).iAnalyserCode, ComboBoxAnalyser
    BG_MostraComboSel tReagentsLots(�lItem�).iAnalysisCode, ComboBoxAnalysis
    TextBoxApplication = GetApplicationDescription(tReagentsLots(�lItem�).iApplicationCode)
    TextBoxAnalyser = GetAnalyserDescription(tReagentsLots(�lItem�).iAnalyserCode)
    TextBoxAnalysis = GetAnalysisDescription(tReagentsLots(�lItem�).iAnalysisCode)
    TextBoxLot.Text = tReagentsLots(�lItem�).sLotCode
    MonthViewInitialDate.value = BL_HandleNull(tReagentsLots(�lItem�).sInitialDate, Bg_DaData_ADO)
    MonthViewFinalDate.value = BL_HandleNull(tReagentsLots(�lItem�).sFinalDate, Bg_DaData_ADO)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'WriteIntoFields' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.04.29
' Initiate picture boxes of search buttons.
Private Sub InitiatePictureBoxes()
  
    On Error GoTo ErrorHandler
    PictureBoxApplication.Picture = ImageListSearch.ListImages(1).Picture
    PictureBoxAnalyser.Picture = ImageListSearch.ListImages(1).Picture
    PictureBoxAnalysis.Picture = ImageListSearch.ListImages(1).Picture
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InitiatePictureBoxes' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.04.30
' Sets user labels content.
Private Sub SetUserLabelsContent(�lItem� As Long)
    
    On Error GoTo ErrorHandler
    LabelUserCreation.caption = GetUserName(tReagentsLots(�lItem�).iUserCreation)
    LabelUserUpdate.caption = GetUserName(tReagentsLots(�lItem�).iUserUpdate)
    LabelDateCreation.caption = tReagentsLots(�lItem�).sDateCreation
    LabelDateUpdate.caption = tReagentsLots(�lItem�).sDateUpdate
    LabelHourCreation.caption = tReagentsLots(�lItem�).sHourCreation
    LabelHourUpdate.caption = tReagentsLots(�lItem�).sHourUpdate
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SetUserLabels' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.05.09
' Insert record in database.
Private Sub InsertData()
    
    On Error GoTo ErrorHandler
    If (Not MandatoryFields) Then: Exit Sub
    If (Not ManageActiveRecords) Then: BG_RollbackTransaction: BG_BeginTransaction: Exit Sub
    If (Not ManageRecordInsertion) Then: BG_RollbackTransaction: BG_BeginTransaction: Exit Sub
    BG_BeginTransaction
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'InsertData' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BG_RollbackTransaction
    BG_BeginTransaction
    Exit Sub

End Sub

' pferreira 2008.05.07
' Update record in database.
Private Sub UpdateData()
    
    On Error GoTo ErrorHandler
    If (Not MandatoryFields) Then: Exit Sub
    If (Not ManageRecordsDelete(ListViewUserLots.SelectedItem.Index)) Then: BG_RollbackTransaction: BG_BeginTransaction: Exit Sub
    If (Not ManageActiveRecords) Then: BG_RollbackTransaction: BG_BeginTransaction: Exit Sub
    If (Not ManageRecordInsertion(ListViewUserLots.SelectedItem.Index)) Then: BG_RollbackTransaction: BG_BeginTransaction: Exit Sub
    BG_BeginTransaction
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'UpdateData' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BG_RollbackTransaction
    BG_BeginTransaction
    Exit Sub

End Sub

' pferreira 2008.04.30
' Gets the user name.
Private Function GetUserName(�iUserCode� As Integer) As String
    
    Dim sSql As String
    Dim rUser As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rUser = New ADODB.recordset
    rUser.CursorLocation = adUseServer
    rUser.CursorType = adOpenStatic
    sSql = "select nome from sl_idutilizador where cod_utilizador = " & �iUserCode�
    rUser.Open sSql, gConexao
    If (rUser.RecordCount > 0) Then: GetUserName = BL_HandleNull(rUser!nome, CStr(�iUserCode�))
    If (rUser.state = adStateOpen) Then: rUser.Close
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GetUserName' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function

End Function

' pferreira 2008.04.30
' Manage records insertion.
Private Function ManageRecordInsertion(Optional �lItem� As Long) As Boolean

    Dim sSql As String

    On Error GoTo ErrorHandler
    If (�lItem� = Empty) Then
        sSql = "insert into sl_registo_lotes (cod_lote,cod_aplicacao,cod_aparelho,cod_analise,data_inicial,data_final,user_cri,dt_cri,hr_cri,user_act," & _
               "dt_act,hr_act,activo) values (" & BL_TrataStringParaBD(TextBoxLot.Text) & "," & BG_DaComboSel(ComboBoxApplication) & "," & _
               BG_DaComboSel(ComboBoxAnalyser) & "," & BG_DaComboSel(ComboBoxAnalysis) & "," & BL_TrataDataParaBD(MonthViewInitialDate.value) & ",null," & _
               gCodUtilizador & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ",null,null,null," & _
               BL_TrataStringParaBD(cLotActive) & ")"
    Else
        sSql = "insert into sl_registo_lotes (cod_lote,cod_aplicacao,cod_aparelho,cod_analise,data_inicial,data_final,user_cri,dt_cri,hr_cri,user_act," & _
               "dt_act,hr_act,activo) values (" & BL_TrataStringParaBD(TextBoxLot.Text) & "," & BG_DaComboSel(ComboBoxApplication) & "," & _
               BG_DaComboSel(ComboBoxAnalyser) & "," & BG_DaComboSel(ComboBoxAnalysis) & "," & BL_TrataDataParaBD(MonthViewInitialDate.value) & "," & _
               BL_TrataDataParaBD(MonthViewFinalDate.value) & "," & tReagentsLots(�lItem�).iUserCreation & "," & _
               BL_TrataDataParaBD(tReagentsLots(�lItem�).sDateCreation) & "," & BL_TrataStringParaBD(tReagentsLots(�lItem�).sHourCreation) & "," & _
               gCodUtilizador & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & _
               BL_TrataStringParaBD(CStr(tReagentsLots(�lItem�).bLotState)) & ")"
    End If
    BG_ExecutaQuery_ADO sSql
    If (gSQLError <> 0) Then: ManageRecordInsertion = False
    ManageRecordInsertion = True
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ManageRecordInsertion' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    ManageRecordInsertion = False
    Exit Function

End Function

' pferreira 2008.04.30
' Manage actives records.
Private Function ManageActiveRecords(Optional �lItem� As Long) As Boolean
    
    Dim vItem As Variant
    Dim sAskTitle As String
    Dim sAskMessage As String
    Dim oActiveRecords  As Collection
    
    On Error GoTo ErrorHandler
    sAskTitle = " Aten��o"
    sAskMessage = "Existem lotes activos!Pretende inactivar esses lotes?"
    Set oActiveRecords = GetActiveRecords(�lItem�)
    If (oActiveRecords.Count > 0) Then
        If (BG_Mensagem(mediMsgBox, sAskMessage, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo) = vbYes) Then
            For Each vItem In oActiveRecords: ChangeLotState CLng(vItem), cLotInactive: Next
        Else
            Exit Function
        End If
    End If
    ManageActiveRecords = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ManageActiveRecords' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    ManageActiveRecords = False
    Exit Function

End Function

' pferreira 2008.05.07
' Manages the deleting of a record.
Private Function ManageRecordsDelete(�lItem� As Long) As Boolean
    
    Dim sSql As String
        
    On Error GoTo ErrorHandler
    sSql = "delete sl_registo_lotes where " & _
           "cod_lote = " & BL_TrataStringParaBD(tReagentsLots(�lItem�).sLotCode) & " and " & _
           "cod_aplicacao = " & tReagentsLots(�lItem�).iApplicationCode & " and " & _
           "cod_aparelho = " & tReagentsLots(�lItem�).iAnalyserCode & " and " & _
           "cod_analise = " & tReagentsLots(�lItem�).iAnalysisCode & " and " & _
           "data_inicial = " & BL_TrataDataParaBD(tReagentsLots(�lItem�).sInitialDate) & " and " & _
           "user_cri = " & tReagentsLots(�lItem�).iUserCreation & " and " & _
           "dt_cri = " & BL_TrataDataParaBD(tReagentsLots(�lItem�).sDateCreation) & " and " & _
           "hr_cri = " & BL_TrataStringParaBD(tReagentsLots(�lItem�).sHourCreation) & " and " & _
           "activo = " & BL_TrataStringParaBD(CStr(tReagentsLots(�lItem�).bLotState))
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    If (gSQLError = 0) Then: ManageRecordsDelete = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ManageRecordsDelete' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    ManageRecordsDelete = False
    Exit Function

End Function

' pferreira 2008.04.30
' Gets the active records.
Private Function GetActiveRecords(Optional �lItem� As Long) As Collection

    Dim lItem As Long
    
    On Error GoTo ErrorHandler
    Set GetActiveRecords = New Collection
    If (UBound(tReagentsLots) = 0) Then: Exit Function
    For lItem = 1 To UBound(tReagentsLots)
        If (tReagentsLots(lItem).iApplicationCode = ComboBoxApplication.ItemData(ComboBoxApplication.ListIndex) And _
            tReagentsLots(lItem).iAnalyserCode = ComboBoxAnalyser.ItemData(ComboBoxAnalyser.ListIndex) And _
            tReagentsLots(lItem).iAnalysisCode = ComboBoxAnalysis.ItemData(ComboBoxAnalysis.ListIndex) And _
            tReagentsLots(lItem).bLotState = cLotActive And lItem <> �lItem�) Then: GetActiveRecords.Add lItem
    Next
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GetActiveRecords' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' pferreira 2008.04.30
' Populates records structure.
Private Sub PopulateRecordsStructure()
  
    Dim sSql As String
    Dim rRecords As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rRecords = New ADODB.recordset
    rRecords.CursorLocation = adUseServer
    rRecords.CursorType = adOpenStatic
    sSql = "select * from sl_registo_lotes order by data_inicial desc"
    rRecords.Open sSql, gConexao
    While (Not rRecords.EOF)
        AddReagentLot BL_HandleNull(rRecords!cod_aplicacao, Empty), BL_HandleNull(rRecords!cod_aparelho, Empty), _
                      BL_HandleNull(rRecords!cod_analise, Empty), BL_HandleNull(rRecords!cod_lote, Empty), _
                      BL_HandleNull(rRecords!data_inicial, Empty), BL_HandleNull(rRecords!data_final, Empty), _
                      BL_HandleNull(rRecords!user_cri, Empty), BL_HandleNull(rRecords!dt_cri, Empty), _
                      BL_HandleNull(rRecords!hr_cri, Empty), BL_HandleNull(rRecords!user_act, Empty), _
                      BL_HandleNull(rRecords!dt_act, Empty), BL_HandleNull(rRecords!hr_act, Empty), _
                      BL_HandleNull(CBool(rRecords!Activo), Empty)
        rRecords.MoveNext
    Wend
    If (rRecords.state = adStateOpen) Then: rRecords.Close
    Exit Sub
           
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PopulateRecordsStructure' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

' pferreira 2008.04.29
' Gets the application description.
Private Function GetApplicationDescription(�iApplicationCode� As Integer) As String
    
    Dim sSql As String
    Dim rApplication As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rApplication = New ADODB.recordset
    rApplication.CursorLocation = adUseServer
    rApplication.CursorType = adOpenStatic
    sSql = "select descr_aplic from gc_aplic where cod_aplic = " & �iApplicationCode�
    rApplication.Open sSql, gConexao
    If (rApplication.RecordCount > 0) Then: GetApplicationDescription = BL_HandleNull(rApplication!descr_aplic, Empty)
    If (rApplication.state = adStateOpen) Then: rApplication.Close
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GetApplicationDescription' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function

End Function

' pferreira 2008.04.29
' Gets the analyser description.
Private Function GetAnalyserDescription(�iAnalyserCode� As Integer) As String
    
    Dim sSql As String
    Dim rAnalyser As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rAnalyser = New ADODB.recordset
    rAnalyser.CursorLocation = adUseServer
    rAnalyser.CursorType = adOpenStatic
    sSql = "select descr_apar from gc_apar where seq_apar = " & �iAnalyserCode�
    rAnalyser.Open sSql, gConexao
    If (rAnalyser.RecordCount > 0) Then: GetAnalyserDescription = BL_HandleNull(rAnalyser!descr_apar, Empty)
    If (rAnalyser.state = adStateOpen) Then: rAnalyser.Close
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GetAnalyserDescription' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function

End Function

' pferreira 2008.04.29
' Gets the analysis description.
Private Function GetAnalysisDescription(�iAnalysisCode� As Integer) As String
    
    Dim sSql As String
    Dim rAnalysis As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rAnalysis = New ADODB.recordset
    rAnalysis.CursorLocation = adUseServer
    rAnalysis.CursorType = adOpenStatic
    sSql = "select descr_ana from gc_ana_apar where seq_ana_apar = " & �iAnalysisCode�
    rAnalysis.Open sSql, gConexao
    If (rAnalysis.RecordCount > 0) Then: GetAnalysisDescription = BL_HandleNull(rAnalysis!descr_ana, Empty)
    If (rAnalysis.state = adStateOpen) Then: rAnalysis.Close
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GetAnalysisDescription' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function

End Function

' pferreira 2008.04.30
' Selects records by given month.
Private Function SelectRecordsByMonth(�iMonth� As Integer) As Collection

    Dim lItem As Variant
    
    On Error GoTo ErrorHandler
    Set SelectRecordsByMonth = New Collection
    For lItem = 1 To UBound(tReagentsLots)
        If (Month(tReagentsLots(lItem).sDateCreation) = �iMonth�) Then: SelectRecordsByMonth.Add lItem
    Next
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SelectRecordsByMonth' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' pferreira 2008.04.30
' Gets month name.
Private Function GetMonthName(�iMonth� As Integer) As String

    On Error GoTo ErrorHandler
    Select Case (�iMonth�)
        Case 1: GetMonthName = "Janeiro"
        Case 2: GetMonthName = "Fevereiro"
        Case 3: GetMonthName = "Mar�o"
        Case 4: GetMonthName = "Abril"
        Case 5: GetMonthName = "Maio"
        Case 6: GetMonthName = "Junho"
        Case 7: GetMonthName = "Julho"
        Case 8: GetMonthName = "Agosto"
        Case 9: GetMonthName = "Setembro"
        Case 10: GetMonthName = "Outubro"
        Case 11: GetMonthName = "Novembro"
        Case 12: GetMonthName = "Dezembro"
        Case Else:  GetMonthName = "???"
    End Select
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GetMonthName' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' pferreira 2008.04.30
' Activates lot record.
Private Sub ChangeLotState(�lItem� As Long, �bLotState� As Boolean)

    On Error GoTo ErrorHandler
    tReagentsLots(�lItem�).bLotState = �bLotState�
    If (Not UpdateReagentsLotsState(�lItem�)) Then
        BG_RollbackTransaction
        tReagentsLots(ListViewUserLots.SelectedItem.Index).bLotState = Not �bLotState�
    End If
    BG_BeginTransaction
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ChangeLotState' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2008.05.07
' Updates reagents lots state.
Private Function UpdateReagentsLotsState(�lItem� As Long) As Boolean
    
    Dim sSql As String
        
    On Error GoTo ErrorHandler
    sSql = "update sl_registo_lotes set activo = " & BL_TrataStringParaBD(CStr(tReagentsLots(�lItem�).bLotState)) & " where " & _
           "cod_lote = " & BL_TrataStringParaBD(tReagentsLots(�lItem�).sLotCode) & " and " & _
           "cod_aplicacao = " & tReagentsLots(�lItem�).iApplicationCode & " and " & _
           "cod_aparelho = " & tReagentsLots(�lItem�).iAnalyserCode & " and " & _
           "cod_analise = " & tReagentsLots(�lItem�).iAnalysisCode
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    If (gSQLError = 0) Then: UpdateReagentsLotsState = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'UpdateReagentsLotsState' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    UpdateReagentsLotsState = False
    Exit Function

End Function

' pferreira 2008.05.07
' Refresh window to see lists data.
Private Sub RefreshWindow()

    On Error GoTo ErrorHandler
    CleanFields
    PopulateRecordsStructure
    WriteIntoListView
    WriteIntoTreeView
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'RefreshWindow' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.05.07
' Manages CommandButtonActivate/CommandButtonInactivate.
Private Sub ManageCommandButtonActivate(�bActivate� As Boolean)
    
    On Error GoTo ErrorHandler
    If (�bActivate�) Then
        CommandButtonActivate.Visible = True
        CommandButtonInactivate.Visible = False
    Else
        CommandButtonActivate.Visible = False
        CommandButtonInactivate.Visible = True
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ManageCommandButtonActivate' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.05.07
' Finds a item in the listview by the lot code.
Private Sub FindItemByLot()
    
    Dim sLotId As String
    Dim oListItem As MSComctlLib.ListItem
    
    On Error GoTo ErrorHandler
    If (ListViewUserLots.ListItems.Count = 0) Then: Exit Sub
    sLotId = InputBox("Insira o lote a procurar", " Procura de lotes")
    If (sLotId = Empty) Then: Exit Sub
    Set oListItem = ListViewUserLots.FindItem(sLotId, , 1, lvwPartial)
    If (oListItem Is Nothing) Then: BG_Mensagem mediMsgBox, "Lote n�o encontrado!", vbExclamation, " Procura de lotes": Exit Sub
    ListViewUserLots.SetFocus
    oListItem.Selected = True
    oListItem.EnsureVisible
    ListViewUserLots_ItemClick oListItem
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FindItemByLot' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.05.07
' Prints the lots recors report.
Private Sub PrintRecords()

    Dim Report As CrystalReport
    Dim bReportInitSuccess As Boolean

    On Error GoTo ErrorHandler
    If (Not FillTemporaryTable) Then: BG_RollbackTransaction: BG_BeginTransaction: Exit Sub
    BG_BeginTransaction
    If (gImprimirDestino = 1) Then: bReportInitSuccess = BL_IniciaReport("RegistoLotesIndividual", "Resultados ", crptToPrinter)
    If (gImprimirDestino = 0) Then: bReportInitSuccess = BL_IniciaReport("RegistoLotesIndividual", "Resultados ", crptToWindow)
    If (Not bReportInitSuccess) Then: Exit Sub
    Set Report = forms(0).Controls("Report")
    Report.formulas(1) = "USER=" & BL_TrataStringParaBD("" & BL_SelNomeUtil(CStr(gCodUtilizador)))
    Report.formulas(2) = "DATE=" & BL_TrataStringParaBD("" & Bg_DaData_ADO)
    Report.formulas(3) = "HOUR=" & BL_TrataStringParaBD("" & Bg_DaHora_ADO)
    Report.formulas(4) = "APLIC=" & BL_TrataStringParaBD("" & cAPLICACAO_NOME_CURTO)
    Me.SetFocus
    Report.Connect = "DSN=" & gDSN
    Call BL_ExecutaReport
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'PrintRecords' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2008.05.07
' Fills temporary table.
Private Function FillTemporaryTable() As Boolean
 
    Dim lItem As Long
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    sSql = "delete from sl_cr_registo_lotes"
    BG_ExecutaQuery_ADO sSql
    If (gSQLError <> 0) Then: Exit Function
    For lItem = 1 To UBound(tReagentsLots)
        sSql = "insert into sl_cr_registo_lotes (cod_lote,cod_aplicacao,cod_aparelho,cod_analise,data_inicial,data_final,activo) values (" & _
               BL_TrataStringParaBD(tReagentsLots(lItem).sLotCode) & "," & _
               BL_TrataStringParaBD(GetApplicationDescription(tReagentsLots(lItem).iApplicationCode)) & "," & _
               BL_TrataStringParaBD(GetAnalyserDescription(tReagentsLots(lItem).iAnalyserCode)) & "," & _
               BL_TrataStringParaBD(GetAnalysisDescription(tReagentsLots(lItem).iAnalysisCode)) & "," & _
               BL_TrataDataParaBD(MonthViewInitialDate.value) & "," & _
               BL_TrataDataParaBD(MonthViewFinalDate.value) & "," & _
               BL_TrataStringParaBD(CStr(tReagentsLots(lItem).bLotState)) & ")"
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
        If (gSQLError <> 0) Then: Exit Function
    Next
    FillTemporaryTable = True
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'FillTemporaryTable' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    FillTemporaryTable = False
    Exit Function

End Function

' pferreira 2008.05.07
' Check mandatory fields.
Private Function MandatoryFields() As Boolean

    On Error GoTo ErrorHandler
    If (TextBoxApplication.Text = Empty) Then: BG_Mensagem mediMsgBox, "Aplica��o obrigat�ria!", vbExclamation, " Aten��o": Exit Function
    If (TextBoxAnalyser.Text = Empty) Then: BG_Mensagem mediMsgBox, "Aparelho obrigat�rio!", vbExclamation, " Aten��o": Exit Function
    If (TextBoxAnalysis.Text = Empty) Then: BG_Mensagem mediMsgBox, "An�lise obrigat�ria!", vbExclamation, " Aten��o": Exit Function
    If (TextBoxLot.Text = Empty) Then: BG_Mensagem mediMsgBox, "Lote obrigat�rio!", vbExclamation, " Aten��o": Exit Function
    MandatoryFields = True
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'MandatoryFields' in form FormAddLot (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    MandatoryFields = False
    Exit Function
    
End Function

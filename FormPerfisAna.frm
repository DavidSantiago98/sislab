VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FormPerfisAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormPerfisAna"
   ClientHeight    =   7320
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7035
   Icon            =   "FormPerfisAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7320
   ScaleWidth      =   7035
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkNaoFact 
      Caption         =   "N�o Facturar "
      Height          =   195
      Left            =   1080
      TabIndex        =   35
      Top             =   960
      Width           =   1695
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4095
      Left            =   120
      TabIndex        =   27
      Top             =   1320
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   7223
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Perfis"
      TabPicture(0)   =   "FormPerfisAna.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label9"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "EcLista"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Locais"
      TabPicture(1)   =   "FormPerfisAna.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "EcLocal"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "EcAnaLocais"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Label13"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Label55"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.ComboBox EcLocal 
         Height          =   315
         Left            =   -74760
         Style           =   2  'Dropdown List
         TabIndex        =   34
         Top             =   3480
         Width           =   4095
      End
      Begin VB.ListBox EcAnaLocais 
         Height          =   2085
         Left            =   -74760
         Style           =   1  'Checkbox
         TabIndex        =   31
         Top             =   840
         Width           =   3495
      End
      Begin VB.ListBox EcLista 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2790
         Left            =   120
         TabIndex        =   28
         Top             =   660
         Width           =   6495
      End
      Begin VB.Label Label13 
         Caption         =   "Locais executante"
         Height          =   255
         Left            =   -74760
         TabIndex        =   33
         Top             =   3120
         Width           =   3375
      End
      Begin VB.Label Label55 
         Caption         =   "Locais de marca��o"
         Height          =   255
         Left            =   -74760
         TabIndex        =   32
         Top             =   480
         Width           =   3375
      End
      Begin VB.Label Label2 
         Caption         =   "C�digo"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   420
         Width           =   855
      End
      Begin VB.Label Label9 
         Caption         =   "Descri��o"
         Height          =   255
         Left            =   1560
         TabIndex        =   29
         Top             =   420
         Width           =   855
      End
   End
   Begin VB.CheckBox EcActivo 
      Caption         =   "EcActivo"
      Height          =   255
      Left            =   1050
      TabIndex        =   25
      Top             =   6720
      Width           =   255
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5400
      TabIndex        =   24
      Top             =   6840
      Width           =   1335
   End
   Begin VB.TextBox EcAbreviatura 
      Height          =   285
      Left            =   3345
      TabIndex        =   22
      Top             =   120
      Width           =   1785
   End
   Begin VB.CommandButton BtAnaPerfis 
      Caption         =   "An�lises do Perfil"
      Height          =   975
      Left            =   5520
      Picture         =   "FormPerfisAna.frx":0044
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   " Atribui��o de An�lises ao Perfil "
      Top             =   240
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   90
      TabIndex        =   7
      Top             =   4470
      Width           =   6865
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   14
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   13
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   10
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3600
      TabIndex        =   6
      Top             =   5520
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   5
      Top             =   5520
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3720
      TabIndex        =   4
      Top             =   5880
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   3
      Top             =   5880
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.TextBox EcCodigo 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1050
      TabIndex        =   0
      Top             =   120
      Width           =   1185
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1050
      TabIndex        =   1
      Top             =   540
      Width           =   4100
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6000
      TabIndex        =   11
      Top             =   5520
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Perfil activo"
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   6720
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Abreviatura"
      Height          =   255
      Index           =   0
      Left            =   2400
      TabIndex        =   23
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2400
      TabIndex        =   21
      Top             =   5880
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2280
      TabIndex        =   20
      Top             =   5520
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   5880
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   5520
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   1
      Left            =   150
      TabIndex        =   17
      Top             =   180
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   570
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   4560
      TabIndex        =   15
      Top             =   5520
      Visible         =   0   'False
      Width           =   1365
   End
End
Attribute VB_Name = "FormPerfisAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 30/03/2003
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

Sub PreencheValoresDefeito()

    BL_InicioProcessamento Me, "Carregar valores de defeito..."
    
    ' Locais
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcAnaLocais, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocal, mediAscComboCodigo
    CkNaoFact.value = vbGrayed
  
    BL_FimProcessamento Me
    
    
    EcActivo.value = 0
End Sub

Private Sub BtAnaPerfis_Click()
    
    If EcCodigo <> "" And EcDescricao <> "" Then
        Max = UBound(gFieldObjectProperties)
    
        ' Guardar as propriedades dos campos do form
        For Ind = 0 To Max
            ReDim Preserve FOPropertiesTemp(Ind)
            FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
            FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
            FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
            FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
            FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
            FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
        Next Ind
           
        'FormAnaPerfil.Show
        FormAnaExames.Show
    Else
        MsgBox "Para utilizar esta op��o, deve escolher um perfil.    ", vbInformation, " Membros do Perfil "
    End If

End Sub

Private Sub cmdOK_Click()

    EcCodigo.Text = UCase(Trim(EcCodigo.Text))
    If (Mid(EcCodigo.Text, 1, 1) <> "P") Then
        EcCodigo.Text = "P" & EcCodigo.Text
    End If
    
    Call FuncaoProcurar

End Sub

Private Sub EcAbreviatura_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcAbreviatura_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub


Private Sub EcCodigo_LostFocus()

    cmdOK.Default = False

End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    EcCodigo.Text = UCase(EcCodigo.Text)
    If Trim(EcCodigo.Text) = "P" Then
        BG_Mensagem mediMsgBox, "O [P] por si s�, n�o pode ser c�digo do perfil, pois � identificativo do mesmo!", vbExclamation, "C�digo do Perfil"
        Cancel = True
    ElseIf left(Trim(EcCodigo.Text), 1) <> "P" And Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = "P" & EcCodigo.Text
    ElseIf Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = left(Trim(EcCodigo.Text), 1) & Mid(EcCodigo.Text, 2, Len(EcCodigo.Text) - 1)
    End If
    
End Sub


Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
End Sub

Private Sub EcCodigo_GotFocus()

    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True
    
End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcLocal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcLocal.ListIndex = -1
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()

    Me.caption = " Perfis de Marca��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 7110
    Me.Height = 5820 ' Normal
    'Me.Height = 7800 ' Campos Extras
    
    NomeTabela = "sl_perfis"
    Set CampoDeFocus = EcCodigo
    
    'CHVNG-Espinho
    NumCampos = 11
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_perfis"
    CamposBD(1) = "cod_perfis"
    CamposBD(2) = "descr_perfis"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"
    CamposBD(7) = "flg_activo"
    CamposBD(8) = "abr_ana_p"
    CamposBD(9) = "cod_local"
    CamposBD(10) = "flg_nao_facturar"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcUtilizadorAlteracao
    Set CamposEc(6) = EcDataAlteracao
    Set CamposEc(7) = EcActivo
    Set CamposEc(8) = EcAbreviatura
    Set CamposEc(9) = EcLocal
    Set CamposEc(10) = CkNaoFact
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Perfis de An�lise"
    TextoCamposObrigatorios(2) = "Descri��o do Perfil de An�lise"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_perfis"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_perfis", "descr_perfis")
    NumEspacos = Array(12, 42)
    
    
    EcDescricao.ToolTipText = cMsgWilcards
    Call BL_FormataCodigo(EcCodigo)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    Max = -1
    ReDim FOPropertiesTemp(0)

    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    gF_PERFISANA = 1
    BL_FimProcessamento Me
    
    PreencheValoresDefeito

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    gF_PERFISANA = 0
    Set FormPerfisAna = Nothing

End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    CkNaoFact.value = vbGrayed
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcActivo.value = 0
    For i = 0 To EcAnaLocais.ListCount - 1
        EcAnaLocais.Selected(i) = False
    Next

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao

        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        BL_CarregaLocaisAna EcCodigo, Me
    End If

End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

    Me.EcCodigo.Locked = False

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function


Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
              
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                    "descr_perfis", _
                                    gPesquisaDentroCampo)
    
'    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY descr_perfis ASC, cod_perfis ASC "
'    End If
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        Call BG_PreencheListBoxMultipla_ADO(EcLista, _
                                            rs, _
                                            CamposBDparaListBox, _
                                            NumEspacos, _
                                            CamposBD, _
                                            CamposEc, _
                                            "SELECT")
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        Me.EcCodigo.Locked = True
        
        BL_FimProcessamento Me
    
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
'    EcOrdem.Text = BG_DaMAX(NomeTabela, "ordem") + 1
    
    EcCodSequencial = BG_DaMAX("SLV_ANALISES_APENAS", "seq_ana") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    BL_GravaLocaisAna EcCodigo, Me
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    BL_GravaLocaisAna EcCodigo, Me
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    
    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja apagar o registo ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    If gSQLError = 0 Then
        SQLQuery = "DELETE FROM sl_ana_perfis " & _
                   "WHERE cod_perfis=" & BL_TrataStringParaBD(EcCodigo.Text)
        BG_ExecutaQuery_ADO SQLQuery
    End If

    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub


VERSION 5.00
Begin VB.Form FormCancelarRequisicoes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCancelarRequisicoes"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10050
   Icon            =   "FormCancelarRequisicoes.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   10050
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcUtilizadorCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4560
      TabIndex        =   19
      Top             =   4320
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox EcDataCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   18
      Top             =   4320
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.TextBox EcHoraCanc 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   17
      Top             =   4680
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.TextBox EcNumReq 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   16
      Text            =   "wedwedwe"
      Top             =   240
      Width           =   735
   End
   Begin VB.TextBox EcTipoCanc 
      Height          =   285
      Left            =   2040
      TabIndex        =   15
      Top             =   3960
      Width           =   615
   End
   Begin VB.CommandButton BtCancelarCanc 
      Height          =   615
      Left            =   6960
      Picture         =   "FormCancelarRequisicoes.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Voltar"
      Top             =   1440
      Width           =   615
   End
   Begin VB.CommandButton BtConfirmarCanc 
      Height          =   615
      Left            =   6240
      Picture         =   "FormCancelarRequisicoes.frx":08D6
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Confirmar cancelamento"
      Top             =   1440
      Width           =   615
   End
   Begin VB.CommandButton BtPesquisacanc 
      Height          =   525
      Left            =   7080
      Picture         =   "FormCancelarRequisicoes.frx":1718
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Pesquisa R�pida de tipos cancelamento"
      Top             =   840
      Width           =   495
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   120
      TabIndex        =   10
      Top             =   2460
      Width           =   7490
      Begin VB.Label LbUtilizadorCanc 
         Height          =   255
         Left            =   840
         TabIndex        =   22
         Top             =   240
         Width           =   3015
      End
      Begin VB.Label LbHoraCanc 
         Height          =   255
         Left            =   2400
         TabIndex        =   21
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label LbDataCanc 
         Height          =   255
         Left            =   840
         TabIndex        =   20
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label7 
         Caption         =   "Utilizador"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label6 
         Caption         =   "Hora"
         Height          =   255
         Left            =   1920
         TabIndex        =   12
         Top             =   600
         Width           =   495
      End
      Begin VB.Label Label5 
         Caption         =   "Data"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.TextBox EcObsCanc 
      Height          =   975
      Left            =   1320
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1440
      Width           =   4815
   End
   Begin VB.TextBox EcDescricao 
      Height          =   525
      Left            =   1320
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   840
      Width           =   5750
   End
   Begin VB.Label Label14 
      Caption         =   "EcUtilizadorCanc"
      Height          =   255
      Left            =   3120
      TabIndex        =   25
      Top             =   4320
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label13 
      Caption         =   "EcHoraCanc"
      Height          =   255
      Left            =   360
      TabIndex        =   24
      Top             =   4680
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCanc"
      Height          =   255
      Left            =   240
      TabIndex        =   23
      Top             =   4320
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label8 
      Caption         =   "Cod tipo cancelamento"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   3960
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label4 
      Caption         =   "Observa��es"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   840
      Width           =   855
   End
   Begin VB.Label LbEpisodio 
      Caption         =   "wedwedwed"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4320
      TabIndex        =   7
      Top             =   240
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   " com epis�dio"
      Height          =   255
      Left            =   3240
      TabIndex        =   6
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Cancelamento da requisi��o n� "
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   240
      Width           =   2310
   End
End
Attribute VB_Name = "FormCancelarRequisicoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

'Flag que indica se � a 1� vez ou n�o que se faz o activate da form
Public Flg_Passou As Boolean

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    Dim sql As String
    Dim Tabela As New ADODB.recordset
    
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
        
    If Flg_Passou = False Then
        EcNumReq.text = gRequisicaoActiva
        If gTipoInstituicao = "HOSPITALAR" Then
            LbEpisodio.caption = Trim(FormGestaoRequisicao.EcEpisodio)
        End If
        sql = "SELECT n_req FROM sl_req_canc WHERE n_req=" & gRequisicaoActiva
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount > 0 Then
            FuncaoProcurar
        End If
        Tabela.Close
        Set Tabela = Nothing
        Flg_Passou = True
    End If
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    On Error Resume Next
    
    If gTipoInstituicao = gTipoInstituicaoHospitalar Then
        Set gFormActivo = FormGestaoRequisicao
    ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Then
        Set gFormActivo = FormGestaoRequisicaoPrivado
    ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
        Set gFormActivo = FormGestaoRequisicaoVet
    ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
        Set gFormActivo = FormGestaoRequisicaoAguas
    End If
    BL_ToolbarEstadoN 2
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormCancelarRequisicoes = Nothing
    
    If gTipoInstituicao = gTipoInstituicaoHospitalar Then
        If gF_REQUIS = 1 Then
            FormGestaoRequisicao.Enabled = True
            FormGestaoRequisicao.rs.Requery
            FormGestaoRequisicao.rs.Bookmark = FormGestaoRequisicao.MarcaLocal
        End If
    ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Then
        FormGestaoRequisicaoPrivado.Enabled = True
        FormGestaoRequisicaoPrivado.rs.Requery
        FormGestaoRequisicaoPrivado.rs.Bookmark = FormGestaoRequisicaoPrivado.MarcaLocal
    ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
        FormGestaoRequisicaoVet.Enabled = True
        FormGestaoRequisicaoVet.rs.Requery
        FormGestaoRequisicaoVet.rs.Bookmark = FormGestaoRequisicaoVet.MarcaLocal
    ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
        FormGestaoRequisicaoAguas.Enabled = True
        FormGestaoRequisicaoAguas.rs.Requery
        FormGestaoRequisicaoAguas.rs.Bookmark = FormGestaoRequisicaoAguas.MarcaLocal
    End If
        
End Sub

Private Sub BtCancelarCanc_Click()
    
    Unload Me

End Sub

Private Sub BtConfirmarCanc_Click()
    
    FuncaoInserir

End Sub

Private Sub BtPesquisacanc_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_canc"
    CamposEcran(1) = "descr_canc"
    Tamanhos(1) = 5000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_t_canc"
    CamposEcran(2) = "cod_t_canc"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_t_canc"
    CWhere = "t_util = " & BL_TrataStringParaBD(TIPOS_CANCELAMENTO.t_REQUISICAO)
    CampoPesquisa = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Cancelamentos")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescricao.text = Resultados(1)
            EcTipoCanc.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem tipos de cancelamentos para requisi��es", vbExclamation, "ATEN��O"
    End If

End Sub

Private Sub EcTipoCanc_Change()
    
    Dim sql As String
    Dim Tabela As New ADODB.recordset
    
    If EcTipoCanc.text <> "" Then
        sql = "SELECT descr_canc FROM sl_t_canc WHERE cod_t_canc=" & BL_TrataStringParaBD(EcTipoCanc.text)
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            EcDescricao.text = Tabela!descr_canc
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub

Private Sub EcUtilizadorCanc_LostFocus()
    
    LbUtilizadorCanc = BL_SelNomeUtil(EcUtilizadorCanc.text)

End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Cancelamento de Requisi��es"
    Me.left = 520
    Me.top = 1680
    Me.Width = 7770
    Me.Height = 3915 ' Normal
    'Me.Height = 3990 ' Campos Extras
    
    NomeTabela = "sl_req_canc"
    Set CampoDeFocus = EcNumReq
    
    NumCampos = 6
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "dt_canc"
    CamposBD(2) = "hr_canc"
    CamposBD(3) = "t_canc"
    CamposBD(4) = "obs_canc"
    CamposBD(5) = "user_canc"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcDataCanc
    Set CamposEc(2) = EcHoraCanc
    Set CamposEc(3) = EcTipoCanc
    Set CamposEc(4) = EcObsCanc
    Set CamposEc(5) = EcUtilizadorCanc
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(3) = "Tipo de cancelamento"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_req"
    Set ChaveEc = EcNumReq
    
    Flg_Passou = False

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_canc", EcDataCanc, mediTipoData
    'BG_DefTipoCampoEc_ADO NomeTabela, "hr_canc", EcHoraCanc, mediTipoHora
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub Funcao_DataActual()
    
    If Me.ActiveControl.Name = "EcDataCanc" Then
        BL_PreencheData EcDataCanc, Me.ActiveControl
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        If EcDescricao.text = "" Then
            MsgBox "Descri��o obrigat�ria"
            EcDescricao.SetFocus
            Exit Sub
        End If
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCanc = gCodUtilizador
        EcDataCanc = Bg_DaData_ADO
        EcHoraCanc = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        If gTipoInstituicao = "HOSPITALAR" Then
            FormGestaoRequisicao.SSTGestReq.TabEnabled(1) = False
            FormGestaoRequisicao.SSTGestReq.TabEnabled(2) = False
            FormGestaoRequisicao.EcEstadoReq = gEstadoReqCancelada
            FormGestaoRequisicao.EcDescrEstado = BL_HandleNull(gEstadoReqCancelada)
        ElseIf gTipoInstituicao = "PRIVADA" Then
            FormGestaoRequisicaoPrivado.SSTGestReq.TabEnabled(1) = False
            FormGestaoRequisicaoPrivado.SSTGestReq.TabEnabled(2) = False
            FormGestaoRequisicaoPrivado.EcEstadoReq = gEstadoReqCancelada
            FormGestaoRequisicaoPrivado.EcDescrEstado = BL_HandleNull(gEstadoReqCancelada)
        End If
        BL_FimProcessamento Me
        Unload Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim SQLUPDATE As String
    Dim SQLDELETE As String
    Dim HisAberto As Integer
    Dim sql As String
    Dim RsFact As ADODB.recordset
    Dim l As Integer
    
    'soliveira 25-07-2003
    '------------------------------------------------------------------------
    'Elimina an�lises do Sonho
    If gTipoInstituicao = "HOSPITALAR" Then
        If ((gEnviaFact_GesReq = 1) And _
            (Len(FormGestaoRequisicao.EcEpisodio.text) > 0) And _
            (FormGestaoRequisicao.CbSituacao.ListIndex <> -1)) Then
            
            If (UCase(HIS.nome) = UCase("SONHO")) Then
            
                HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
                
                If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
    
                    Set RsFact = New ADODB.recordset
                    sql = "SELECT * from sl_analises_in WHERE n_req = " & gRequisicaoActiva
                    RsFact.CursorType = adOpenStatic
                    RsFact.CursorLocation = adUseServer
                    RsFact.Open sql, gConexao
                    If RsFact.RecordCount > 0 Then
                        While Not RsFact.EOF
                            If (SONHO_Remove_ANALISES_IN(BL_HandleNull(RsFact!cod_modulo, ""), _
                                           BL_HandleNull(RsFact!num_episodio, ""), _
                                           BL_HandleNull(RsFact!num_processo, ""), _
                                           BL_HandleNull(RsFact!cod_especialidade, ""), _
                                           BL_HandleNull(RsFact!cod_analise, ""), _
                                           BL_HandleNull(RsFact!dta_analise, ""), _
                                           gRequisicaoActiva) = 1) Then
                            End If
                            RsFact.MoveNext
                        Wend
                    End If
                    RsFact.Close
                    Set RsFact = Nothing
                End If
                BL_Fecha_conexao_HIS
            ElseIf (UCase(HIS.nome) = UCase("SISBIT")) Then
    
                Set RsFact = New ADODB.recordset
                sql = "SELECT r.t_sit, r.n_epis,f.num_processo,f.cod_especialidade,f.n_req,f.cod_analise, f.dta_analise, "
                sql = sql & " r.cod_efr, f.cod_ana_sislab, f.cod_perfil, f.cod_ana_c, f.cod_ana_s, f.cod_agrup,f.centro_custo "
                sql = sql & " FROM sl_analises_in f, sl_requis r WHERE f.n_req = r.n_Req AND r.n_req = " & gRequisicaoActiva
                
                RsFact.CursorType = adOpenStatic
                RsFact.CursorLocation = adUseServer
                RsFact.Open sql, gConexao
                If RsFact.RecordCount > 0 Then
                    While Not RsFact.EOF
                        For l = 1 To 1000
                            If (SISBIT_Remove_ANALISES_IN(BL_HandleNull(RsFact!t_sit, ""), _
                                           BL_HandleNull(RsFact!n_epis, ""), _
                                           BL_HandleNull(RsFact!num_processo, ""), _
                                           BL_HandleNull(RsFact!cod_especialidade, ""), _
                                           BL_HandleNull(RsFact!cod_analise, ""), _
                                           BL_HandleNull(RsFact!dta_analise, ""), _
                                           BL_HandleNull(RsFact!n_req, -1), _
                                           BL_HandleNull(RsFact!cod_efr, ""), _
                                           BL_HandleNull(RsFact!cod_ana_sislab, ""), _
                                           BL_HandleNull(RsFact!Cod_Perfil, "0"), _
                                           BL_HandleNull(RsFact!cod_ana_c, "0"), _
                                           BL_HandleNull(RsFact!cod_ana_s, "0"), _
                                           BL_HandleNull(RsFact!cod_agrup, "0"), "", "", "", "", _
                                           BL_HandleNull(RsFact!centro_custo, ""), gCodLocal)) = 1 Then
                                Exit For
                            End If
                        Next l
                        RsFact.MoveNext
                    Wend
                End If
                RsFact.Close
                Set RsFact = Nothing
            ElseIf (UCase(HIS.nome) = UCase("GH")) Then
    
                    Set RsFact = New ADODB.recordset
                    sql = "select * from sl_dados_fact " & _
                            " where n_req = " & gRequisicaoActiva & " and n_seq not in " & _
                            " (select n_seq_anul from sl_dados_fact where n_req = " & gRequisicaoActiva & _
                            " and n_seq_anul is not null) and n_seq_anul is null "
                    RsFact.CursorType = adOpenStatic
                    RsFact.CursorLocation = adUseServer
                    RsFact.Open sql, gConexao
                    If RsFact.RecordCount > 0 Then
                        While Not RsFact.EOF
                            If (GH_Remove_ANALISES_IN(RsFact!t_episodio, _
                                                               BL_HandleNull(RsFact!episodio, "-1"), _
                                                               BL_HandleNull(RsFact!t_doente, ""), _
                                                               BL_HandleNull(RsFact!doente, ""), _
                                                               BL_HandleNull(RsFact!cod_acto, ""), _
                                                               RsFact!n_req, _
                                                               RsFact!dt_ini_acto, BL_HandleNull(RsFact!n_mecan, ""), , BL_HandleNull(RsFact!cod_serv_req, ""), , , , BL_HandleNull(RsFact!n_analise, ""), _
                                                               BL_HandleNull(RsFact!cod_agrup, ""), BL_HandleNull(RsFact!Cod_Perfil, ""), BL_HandleNull(RsFact!cod_ana_c, ""), BL_HandleNull(RsFact!cod_ana_s, ""), _
                                                               BL_HandleNull(RsFact!cod_micro, ""), BL_HandleNull(RsFact!Cod_Gr_Antibio, ""), BL_HandleNull(RsFact!cod_antibio, ""), BL_HandleNull(RsFact!Cod_Prova, ""), _
                                                               RsFact!n_seq) = 1) Then
                            End If
                            RsFact.MoveNext
                        Wend
                    End If
                    RsFact.Close
                    Set RsFact = Nothing
            End If
            
        End If
    End If
    '--------------------------------------------------------------------------
    
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    BG_BeginTransaction
    
    'Actualizar o estado da requisi��o
    SQLUPDATE = "UPDATE sl_requis SET estado_req='C' WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLUPDATE
    If gSQLError <> 0 Then
        'Erro a actualizar o estado da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar o estado da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Actualizar o estado dos tubos
    SQLUPDATE = "UPDATE sl_req_tubo SET dt_eliminacao = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", user_eliminacao = " & gCodUtilizador & _
    ", hr_eliminacao = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", estado_tubo = " & gEstadosTubo.cancelado & " WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLUPDATE
    If gSQLError <> 0 Then
        'Erro a actualizar o estado dos tubos
        BG_Mensagem mediMsgBox, "Erro a actualizar o estado dos tubos !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Apagar produtos da requisi��o (sl_req_prod)
    SQLDELETE = "DELETE FROM sl_req_prod WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLDELETE
    If gSQLError <> 0 Then
        'Erro a apagar produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a apagar produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
        
    'Apagar an�lises da requisi��o (sl_marcacoes)
    SQLDELETE = "DELETE FROM sl_marcacoes WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLDELETE
    If gSQLError <> 0 Then
        'Erro a apagar an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a apagar an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
        
    'Apagar diagn�sticos secund�rios (sl_diag_sec)
    SQLDELETE = "DELETE FROM sl_diag_sec WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLDELETE
    If gSQLError <> 0 Then
        'Erro a apagar diagn�sticos secund�rios
        BG_Mensagem mediMsgBox, "Erro a apagar diagn�sticos secund�rios !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Apagar terap�uticas e medica��es (sl_tm_ute)
    SQLDELETE = "DELETE FROM sl_tm_ute WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLDELETE
    If gSQLError <> 0 Then
        'Erro a apagar terap�uticas e medica��es
        BG_Mensagem mediMsgBox, "Erro a apagar terap�uticas e medica��es !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Apagar m�dicos da requisi��o
    SQLDELETE = "DELETE FROM sl_medicos_req WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLDELETE
    If gSQLError <> 0 Then
        'Erro a apagar m�dicos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a apagar m�dicos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Apagar motivos da requisi��o
    SQLDELETE = "DELETE FROM sl_motivo_req WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLDELETE
    If gSQLError <> 0 Then
        'Erro a apagar motivos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a apagar motivos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
            
    'BRUNODSANTOS 23.11.2017 UALIA-754
    'MUDA O ESTADO DA TABELA SL_EMAIL_COMPLETOS PARA 2 PARA NO ENVIO DE EMAILS
    'N�O TER EM CONTA AS REQUISI��ES CANCELADAS
    SQLDELETE = "UPDATE sl_email_completos SET flg_estado = 2, user_act = " & gCodUtilizador & _
    ", dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & " WHERE n_req=" & gRequisicaoActiva
    BG_ExecutaQuery_ADO SQLDELETE
    If gSQLError <> 0 Then
        'Erro a apagar motivos da requisi��o
        BG_Mensagem mediMsgBox, "Erro ao remover da tabela de emails!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    '
    'edgar.parada UALIA-891 22.02.2019 update fa_movi_fact flg_estado=2 e flg_estado_doe=2
    If gPassaRecFactus = mediSim Then
       
            'SQLDELETE = "DELETE FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(CStr(gRequisicaoActiva))
            SQLDELETE = "UPDATE fa_movi_fact SET flg_estado = 2, flg_estado_doe = 2 WHERE episodio = " & BL_TrataStringParaBD(CStr(gRequisicaoActiva))
            BG_ExecutaQuery_ADO SQLDELETE
            
            If gSQLError <> 0 Then
                'Erro ao fazer updade aos registos da fa_movi_fact
                BG_Mensagem mediMsgBox, "Erro ao remover da tabela detalhe de factura��o! (fa_movi_fact)", vbError, "ERRO"
                GoTo Trata_Erro
            End If
            
        'UALIA-873
        If RequisicaoESP(CLng(gRequisicaoActiva)) = True Then
            'Glintt-HS-21507 -> update estado para cancelado
            SQLDELETE = "UPDATE sl_credenciais SET estado = 'C' WHERE n_req_orig = " & BL_TrataStringParaBD(CStr(gRequisicaoActiva))
            BG_ExecutaQuery_ADO SQLDELETE
            If gSQLError <> 0 Then
                'Erro a apagar registos da sl_credenciais
                BG_Mensagem mediMsgBox, "Erro ao fazer update das credenciais! (sl_credenciais)", vbError, "ERRO"
                GoTo Trata_Erro
            End If
        End If
        
    End If
    '
    'Inserir na tabela de cancelamentos de requisi��es (sl_req_canc)
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    If gSQLError <> 0 Then
        'Erro a registar cancelamento
        BG_Mensagem mediMsgBox, "Erro a registar cancelamento !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    Call RegistaEliminadasCanc

    BG_CommitTransaction
    
    Exit Sub
    
Trata_Erro:
    BG_LogFile_Erros "FormCancelarRequisicoes: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY n_req"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Sub PreencheCampos()
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        LbDataCanc.caption = EcDataCanc.text
        LbHoraCanc.caption = EcHoraCanc.text
        LbUtilizadorCanc.caption = EcUtilizadorCanc.text
        BtConfirmarCanc.Enabled = False
        BtPesquisaCanc.Enabled = False
        EcDescricao.Enabled = False
        EcObsCanc.Enabled = False
    End If

End Sub

Sub FuncaoLimpar()
    
    LimpaCampos

End Sub

Sub LimpaCampos()
    
    BG_LimpaCampo_Todos CamposEc

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

' pferreira 2010.03.08
' TICKET GLINTTHS-263.
Sub RegistaEliminadasCanc()
    Dim i As Integer
    Dim sql As String
    
    If gTipoInstituicao = "HOSPITALAR" Then
        FormGestaoRequisicao.RegistaEliminadas True
    ElseIf gTipoInstituicao = "PRIVADA" Then
        FormGestaoRequisicaoPrivado.RegistaEliminadas
    End If
    
End Sub


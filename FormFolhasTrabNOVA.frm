VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormFolhasTrabNOVA 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFolhasTrabNOVA"
   ClientHeight    =   8385
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9870
   Icon            =   "FormFolhasTrabNOVA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   9870
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameReimprime 
      Caption         =   "Reimpressao"
      Height          =   4455
      Left            =   0
      TabIndex        =   28
      Top             =   5280
      Width           =   8415
      Begin VB.ListBox EcListaFolhasTrab 
         Height          =   2310
         Left            =   1560
         Style           =   1  'Checkbox
         TabIndex        =   32
         Top             =   720
         Width           =   6615
      End
      Begin VB.CommandButton BtSair 
         Height          =   495
         Left            =   6480
         Picture         =   "FormFolhasTrabNOVA.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   31
         ToolTipText     =   "Voltar"
         Top             =   3120
         Width           =   735
      End
      Begin VB.CommandButton Btimprime 
         Height          =   615
         Left            =   7560
         Picture         =   "FormFolhasTrabNOVA.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   30
         ToolTipText     =   "Imprimir folhas seleccionadas"
         Top             =   3000
         Width           =   735
      End
      Begin VB.CheckBox CkSelTodas 
         Height          =   195
         Left            =   1620
         TabIndex        =   29
         Top             =   480
         Width           =   255
      End
      Begin MSComCtl2.DTPicker EcData 
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147259393
         CurrentDate     =   39876
      End
   End
   Begin VB.CommandButton BtReimprimir 
      Caption         =   "Reimprimir"
      Enabled         =   0   'False
      Height          =   735
      Left            =   7560
      Picture         =   "FormFolhasTrabNOVA.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   3600
      Width           =   855
   End
   Begin VB.TextBox EcFolhaTrab 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6000
      TabIndex        =   8
      Top             =   1560
      Width           =   855
   End
   Begin VB.CommandButton BtImprimir 
      Caption         =   "Imprimir"
      Height          =   735
      Left            =   6720
      Picture         =   "FormFolhasTrabNOVA.frx":266A
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   3600
      Width           =   855
   End
   Begin VB.CommandButton BtPreVisualizar 
      Height          =   735
      Left            =   5760
      Picture         =   "FormFolhasTrabNOVA.frx":3334
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   3600
      Width           =   855
   End
   Begin VB.TextBox EcCodAreaTrab 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1800
      TabIndex        =   13
      Top             =   3120
      Width           =   1095
   End
   Begin VB.CommandButton BtPesquisaRapidaAreaTrab 
      Height          =   375
      Left            =   7830
      Picture         =   "FormFolhasTrabNOVA.frx":3FFE
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Pesquisa R�pida de Grupos de Trabalho"
      Top             =   3100
      Width           =   435
   End
   Begin VB.TextBox EcDescrAreaTrab 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   285
      Left            =   2880
      Locked          =   -1  'True
      TabIndex        =   25
      Top             =   3120
      Width           =   4935
   End
   Begin VB.TextBox EcCodGrTrab 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1800
      TabIndex        =   11
      Top             =   2640
      Width           =   1095
   End
   Begin VB.CommandButton BtPesquisaRapidaGrTrab 
      Height          =   375
      Left            =   7830
      Picture         =   "FormFolhasTrabNOVA.frx":4588
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Pesquisa R�pida de Grupos de Trabalho"
      Top             =   2600
      Width           =   435
   End
   Begin VB.TextBox EcDescrGrTrab 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   285
      Left            =   2880
      Locked          =   -1  'True
      TabIndex        =   23
      Top             =   2640
      Width           =   4935
   End
   Begin VB.Frame Frame3 
      Height          =   1575
      Left            =   4680
      TabIndex        =   21
      Top             =   960
      Width           =   3735
      Begin VB.TextBox EcNumReq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         TabIndex        =   7
         Top             =   240
         Width           =   855
      End
      Begin VB.CheckBox CkNovasAnalises 
         Caption         =   "Apenas Novas An�lises"
         Enabled         =   0   'False
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   1200
         Width           =   2895
      End
      Begin VB.CheckBox CkImpTodos 
         Caption         =   "Imprimir Autom�tica"
         Height          =   255
         Left            =   360
         TabIndex        =   9
         Top             =   960
         Width           =   3135
      End
      Begin VB.Label Label1 
         Caption         =   "Folha N�"
         Height          =   255
         Index           =   5
         Left            =   360
         TabIndex        =   27
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   22
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1575
      Left            =   120
      TabIndex        =   18
      Top             =   960
      Width           =   4455
      Begin VB.ComboBox CbLocal 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   1080
         Width           =   3135
      End
      Begin VB.CheckBox CkDatas 
         Caption         =   "Intervalo de datas"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   2655
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   1080
         TabIndex        =   5
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147259393
         CurrentDate     =   39638
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3000
         TabIndex        =   6
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147259393
         CurrentDate     =   39638
      End
      Begin VB.Label LbLocal 
         Caption         =   "Local"
         Height          =   255
         Left            =   240
         TabIndex        =   36
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "a"
         Height          =   255
         Index           =   1
         Left            =   2520
         TabIndex        =   20
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "De"
         Height          =   255
         Index           =   0
         Left            =   600
         TabIndex        =   19
         Top             =   720
         Width           =   375
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   17
      Top             =   0
      Width           =   8295
      Begin VB.OptionButton Opt 
         Caption         =   "Reimprimir Folha Trab"
         Height          =   255
         Index           =   3
         Left            =   5640
         TabIndex        =   3
         Top             =   240
         Width           =   2175
      End
      Begin VB.OptionButton Opt 
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   2
         Left            =   3960
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton Opt 
         Caption         =   "�rea Trabalho"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.OptionButton Opt 
         Caption         =   "Grupo Trabalho"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Label Label1 
      Caption         =   "�rea Trabalho"
      Height          =   255
      Index           =   4
      Left            =   240
      TabIndex        =   26
      Top             =   3120
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "&Grupo de Trabalho :"
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   24
      Top             =   2640
      Width           =   1575
   End
End
Attribute VB_Name = "FormFolhasTrabNOVA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/01/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public TipoFolha As String 'FolhaTrabalhoAna' ou 'FolhaTrabalhoTubo'

Public rs As ADODB.recordset

Private Type analises
    codAna As String
    descrAna As String
    abrAna As String
    ordem As Long
    Existe As Boolean
End Type

Private estrutAna() As analises
Dim totalAna As Long

Private Type requisicoes
    nome As String
    dt_nasc As String
    dt_previ As String
    Sexo As String
    n_req As String
    
    abrev As String
    linha As String
    obs As String
    
    resProteinas As String
    analises() As Boolean
End Type
Private estrutRequis() As requisicoes
Dim totalRequis As Long

Private Type Reimpressao
    seq_impr As String
    cod_gr_trab As String
    descr_gr_trab As String
End Type

Dim EstrutReImpr() As Reimpressao
Dim totalImpr As Long


Private Sub BtImprimir_Click()
    gImprimirDestino = 1
    If Opt(0).value = True Then
        If CkImpTodos.value = vbChecked Then
            ImprimeAutomaticos
        Else
            ImprimeGrupoTrab EcCodGrTrab, "", ""
        End If
    ElseIf Opt(1).value = True Then
        ImprimeAreaTrab EcCodAreaTrab
    ElseIf Opt(2).value = True Then
        ImprimeRequis EcNumReq
    ElseIf Opt(3).value = True Then
        ReImprime EcFolhaTrab
    End If
End Sub

Private Sub BtPesquisaRapidaGrTrab_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_trab"
    CamposEcran(1) = "cod_gr_trab"
    Tamanhos(1) = 2000
    ChavesPesq(2) = "descr_gr_trab"
    CamposEcran(2) = "descr_gr_trab"
    Tamanhos(2) = 3000
    
    
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    ClausulaFrom = "sl_gr_trab"
    CampoPesquisa1 = "descr_gr_trab"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " order by descr_gr_trab ", " Pesquisar Grupos de Trabalho")
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrTrab.Text = resultados(1)
            EcDescrGrTrab.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPreVisualizar_Click()
    gImprimirDestino = 0
    If Opt(0).value = True Then
        If CkImpTodos.value = vbChecked Then
            ImprimeAutomaticos
        Else
            ImprimeGrupoTrab EcCodGrTrab, "", ""
        End If
    ElseIf Opt(1).value = True Then
        ImprimeAreaTrab EcCodAreaTrab
    ElseIf Opt(2).value = True Then
        ImprimeRequis EcNumReq
    ElseIf Opt(3).value = True Then
        ReImprime EcFolhaTrab
    End If
End Sub

Private Sub BtReimprimir_Click()
    FrameReimprime.Visible = True
    FrameReimprime.top = 0
    FrameReimprime.left = 0
    EcListaFolhasTrab.Clear

End Sub


Private Sub CkDatas_Click()
    If CkDatas.value = vbChecked Then
        EcDtFim.Enabled = True
        EcDtIni.Enabled = True
    Else
        EcDtFim.Enabled = False
        EcDtIni.Enabled = False
    End If
End Sub

Private Sub EcCodGrTrab_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodGrTrab_Validate(Cancel As Boolean)
        
    Dim RsDescrGrTrab As ADODB.recordset
    
    'Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrTrab.Text) <> "" Then
        Set RsDescrGrTrab = New ADODB.recordset
        With RsDescrGrTrab
            .Source = "SELECT descr_gr_trab FROM sl_gr_trab WHERE cod_gr_trab= " & BL_TrataStringParaBD(EcCodGrTrab.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrTrab.RecordCount <> 0 Then
            EcDescrGrTrab.Text = "" & RsDescrGrTrab!descr_gr_trab
        Else
            Cancel = True
            EcDescrGrTrab.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo de Trabalho indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrTrab.Close
        Set RsDescrGrTrab = Nothing
    Else
        EcDescrGrTrab.Text = ""
    End If
    
End Sub

Private Sub BtPesquisaRapidaareatrab_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
        ChavesPesq(1) = "cod_area_trab"
        CamposEcran(1) = "cod_area_trab"
        Tamanhos(1) = 2000
        ChavesPesq(2) = "descr_area_trab"
        CamposEcran(2) = "descr_area_trab"
        Tamanhos(2) = 3000
    
    
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_area_trab"
    CampoPesquisa1 = "descr_area_trab"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " order by descr_area_trab ", " Pesquisar �reas de Trabalho")
    mensagem = "N�o foi encontrada nenhuma �rea de Trabalho."
    
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAreaTrab.Text = resultados(1)
            EcDescrAreaTrab.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodareaTrab_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodareaTrab_Validate(Cancel As Boolean)
        
    Dim RsDescrGrTrab As ADODB.recordset
    EcCodAreaTrab = UCase(EcCodAreaTrab)
    
    'Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrTrab.Text) <> "" Then
        Set RsDescrGrTrab = New ADODB.recordset
        With RsDescrGrTrab
            .Source = "SELECT descr_area_trab FROM sl_area_trab WHERE upper(cod_area_trab) = " & BL_TrataStringParaBD(EcCodAreaTrab.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrTrab.RecordCount <> 0 Then
            EcDescrAreaTrab.Text = "" & RsDescrGrTrab!Descr_area_Trab
        Else
            Cancel = True
            EcDescrAreaTrab.Text = ""
            BG_Mensagem mediMsgBox, "A �rea de Trabalho indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrTrab.Close
        Set RsDescrGrTrab = Nothing
    Else
        EcDescrAreaTrab.Text = ""
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.value) = "" Then
        EcDtFim.value = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub



Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.value) = "" Then
        EcDtIni.value = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()

    EventoActivate
    
End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
        
End Sub

Sub Inicializacoes()

    Me.caption = " Impress�o de Folhas de Trabalho "
    
    Me.left = 540
    Me.top = 450
    Me.Width = 8550
    Me.Height = 5040 ' Normal
    
    Set CampoDeFocus = Opt(0)
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormFolhasTrabNOVA = Nothing
    
    Call BL_FechaPreview("Folha Trabalho")
    
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    EcCodAreaTrab = ""
    EcCodGrTrab = ""
    EcCodGrTrab_Validate False
    Opt(0).value = True
    EcDtFim.value = Bg_DaData_ADO
    EcDtIni.value = Bg_DaData_ADO
    CkDatas.value = vbUnchecked
    EcDtIni.Enabled = False
    EcDtFim.Enabled = False
    EcNumReq = ""
    CkImpTodos.value = vbUnchecked
    CkNovasAnalises.value = Checked
    CbLocal.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()
    EcDtFim.Enabled = False
    EcDtIni.Enabled = False
    EcDtFim.value = Bg_DaData_ADO
    EcDtIni.value = Bg_DaData_ADO
    EcCodGrTrab.Tag = adVarChar
    EcCodAreaTrab.Tag = adVarChar
    EcCodAreaTrab.Enabled = False
    BtPesquisaRapidaAreaTrab.Enabled = False
    EcCodGrTrab.Enabled = True
    BtPesquisaRapidaGrTrab.Enabled = True
    CkNovasAnalises.value = vbChecked
    EcData.value = Bg_DaData_ADO
    'Opt(0).Value = True
    'Opt_Click 0
    BG_PreencheComboBD_ADO "gr_empr_inst", "empresa_id", "nome_empr", CbLocal
    CbLocal.ListIndex = mediComboValorNull
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub FuncaoProcurar()
    
End Sub

Sub FuncaoImprimir()
    gImprimirDestino = 1
    If Opt(0).value = True Then
        ImprimeGrupoTrab EcCodGrTrab, "", ""
    ElseIf Opt(1).value = True Then
        ImprimeAreaTrab EcCodAreaTrab
    ElseIf Opt(2).value = True Then
        ImprimeRequis EcNumReq
    ElseIf Opt(3).value = True Then
        ReImprime EcFolhaTrab
    End If
End Sub

Sub ImprimirVerAntes()
    

End Sub




Private Function DevolveProteinas(n_req As String) As String
    Dim sSql As String
    Dim RsRes As New ADODB.recordset
    
    sSql = "SELECT sl_res_alfan.result FROM sl_realiza, sl_res_alfan "
    sSql = sSql & " WHERE sl_realiza.seq_realiza = sl_res_alfan.seq_realiza"
    sSql = sSql & " AND sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(gCodAnaProteinas)
    sSql = sSql & " AND sl_realiza.n_req = " & n_req
    RsRes.CursorType = adOpenStatic
    RsRes.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        DevolveProteinas = BL_HandleNull(RsRes!result, "")
    Else
        DevolveProteinas = ""
    End If
    RsRes.Close
    Set RsRes = Nothing
End Function


Private Function DevolveAntibioticos() As String
    Dim sSql As String
    Dim rsTSQ As New ADODB.recordset
    Dim aux As String
    Dim contador As Integer
    
    sSql = "SELECT * FROM sl_antibio WHERE flg_imprime_folhatrab = 1 "
    
    rsTSQ.CursorType = adOpenStatic
    rsTSQ.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTSQ.Open sSql, gConexao
    aux = ""
    If rsTSQ.RecordCount > 0 Then
        contador = 0
        While Not rsTSQ.EOF
            aux = aux & BL_HandleNull(rsTSQ!descr_antibio, "") & Space(40 - Len(BL_HandleNull(rsTSQ!descr_antibio, "")))
            contador = contador + 1
            If contador Mod 3 = 0 Then
                aux = aux & vbCrLf
            End If
            rsTSQ.MoveNext
        Wend
    End If
    rsTSQ.Close
    Set rsTSQ = Nothing
    DevolveAntibioticos = aux
End Function




Private Sub RegistaImprFolhaTrab(CodGrupo As String, SeqImpr As Long)
    Dim sSql As String
    
    If gImprimirDestino <> 1 Then Exit Sub
    sSql = "INSERT INTO sl_impr_folhas_trab(seq_impr, cod_grupo, user_imp, dt_imp, hr_imp) VALUES("
    sSql = sSql & SeqImpr & ", " & BL_TrataStringParaBD(CodGrupo) & ", " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & " )"
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
End Sub

Private Sub Btimprime_Click()
    Dim i As Integer
    FrameReimprime.Visible = False
    
    For i = 0 To EcListaFolhasTrab.ListCount - 1
        gImprimirDestino = 1
        If EcListaFolhasTrab.Selected(i) = True Then
            EcFolhaTrab = EstrutReImpr(i + 1).seq_impr
            EcCodGrTrab = EstrutReImpr(i + 1).cod_gr_trab
            ImprimeGrupoTrab EcCodGrTrab, "", EcFolhaTrab
        End If
    Next
End Sub
Private Sub BtSair_Click()
    FrameReimprime.Visible = False
End Sub



Private Sub Opt_Click(Index As Integer)
    If Opt(0).value = True Then
        EcCodAreaTrab.Enabled = False
        BtPesquisaRapidaAreaTrab.Enabled = False
        EcCodAreaTrab = ""
        EcDescrAreaTrab = ""
        EcNumReq = ""
        EcNumReq.Enabled = False
        EcCodareaTrab_Validate True
        EcCodGrTrab.Enabled = True
        BtPesquisaRapidaGrTrab.Enabled = True
        EcFolhaTrab.Enabled = False
        CkNovasAnalises.value = vbChecked
        CkNovasAnalises.Enabled = True
        BtReimprimir.Enabled = False
    ElseIf Opt(1).value = True Then
        EcCodAreaTrab.Enabled = True
        BtPesquisaRapidaAreaTrab.Enabled = True
        EcCodGrTrab.Enabled = False
        BtPesquisaRapidaGrTrab.Enabled = False
        EcCodGrTrab = ""
        EcCodGrTrab_Validate True
        EcDescrGrTrab = ""
        EcNumReq = ""
        EcNumReq.Enabled = False
        EcFolhaTrab.Enabled = False
        CkNovasAnalises.value = vbChecked
        CkNovasAnalises.Enabled = False
        BtReimprimir.Enabled = False
    ElseIf Opt(2).value = True Then
        EcCodAreaTrab.Enabled = False
        BtPesquisaRapidaAreaTrab.Enabled = False
        EcCodAreaTrab = ""
        EcCodareaTrab_Validate True
        'EcCodGrTrab.Enabled = False
        'BtPesquisaRapidaGrTrab.Enabled = False
        EcCodGrTrab = ""
        EcCodGrTrab_Validate True
        EcNumReq = ""
        EcNumReq.Enabled = True
        EcFolhaTrab.Enabled = False
        CkNovasAnalises.value = vbUnchecked
        CkNovasAnalises.Enabled = True
        BtReimprimir.Enabled = False
    ElseIf Opt(3).value = True Then
        EcCodAreaTrab.Enabled = False
        BtPesquisaRapidaAreaTrab.Enabled = False
        EcCodAreaTrab = ""
        EcCodareaTrab_Validate True
        'BtPesquisaRapidaGrTrab.Enabled = False
        EcCodGrTrab = ""
        EcCodGrTrab_Validate True
        EcNumReq = ""
        EcNumReq.Enabled = False
        EcFolhaTrab.Enabled = True
        CkNovasAnalises.value = vbUnchecked
        CkNovasAnalises.Enabled = True
        BtReimprimir.Enabled = True
    End If
End Sub

Private Sub ImprimeRequis(n_req As String)
    Dim sSql As String
    Dim rsGrTrab As New ADODB.recordset
    Dim cond As String
    
    If n_req = "" Then
        Exit Sub
    End If
    
    cond = ""
    If EcCodGrTrab <> "" Then
        cond = " AND cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
    End If
    
    sSql = "SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_ana_s from sl_marcacoes where n_req = " & BL_TrataStringParaBD(n_req) & ")" & cond
    sSql = sSql & " UNION SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_ana_c from sl_marcacoes where n_req = " & BL_TrataStringParaBD(n_req) & ")" & cond
    sSql = sSql & " UNION SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_perfil from sl_marcacoes where n_req = " & BL_TrataStringParaBD(n_req) & ")" & cond
    sSql = sSql & " ORDER BY cod_gr_trab "
    rsGrTrab.CursorType = adOpenStatic
    rsGrTrab.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrTrab.Open sSql, gConexao
    If rsGrTrab.RecordCount > 0 Then
        While Not rsGrTrab.EOF
            ImprimeGrupoTrab (BL_HandleNull(rsGrTrab!cod_gr_trab, "")), n_req, ""
            rsGrTrab.MoveNext
        Wend
    Else
        Call BG_Mensagem(mediMsgBox, "N�o existem an�lises marcadas da requisi��o em causa", vbOKOnly + vbExclamation, App.ProductName)
    End If
End Sub

Private Sub ReImprime(NFolhaTrab As String)
    Dim sSql As String
    Dim rsGrTrab As New ADODB.recordset
    Dim cond As String
    
    If NFolhaTrab = "" Then
        Exit Sub
    End If
    
    sSql = "SELECT distinct cod_grupo from sl_impr_folhas_trab where seq_impr = " & NFolhaTrab
    rsGrTrab.CursorType = adOpenStatic
    rsGrTrab.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrTrab.Open sSql, gConexao
    If rsGrTrab.RecordCount > 0 Then
        ImprimeGrupoTrab (BL_HandleNull(rsGrTrab!cod_grupo, "")), "", NFolhaTrab
        rsGrTrab.MoveNext
    Else
        Call BG_Mensagem(mediMsgBox, "N�o existe nenhuma an�lise com essa folha de trabalho.", vbOKOnly + vbExclamation, App.ProductName)
    End If
End Sub

Private Sub ImprimeAreaTrab(codArea As String)
    Dim sSql As String
    Dim rsAreaTrab As New ADODB.recordset
    
    If codArea = "" Then
        Exit Sub
    End If
    
    sSql = "SELECT distinct cod_gr_trab FROM sl_gr_trab WHERE cod_area_trab = " & BL_TrataStringParaBD(codArea)
    If CkImpTodos.value = vbChecked Then
        sSql = sSql & " AND impressao_automatica = 1 "
    End If
    sSql = sSql & " ORDER BY cod_gr_trab "
    rsAreaTrab.CursorType = adOpenStatic
    rsAreaTrab.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAreaTrab.Open sSql, gConexao
    If rsAreaTrab.RecordCount > 0 Then
        While Not rsAreaTrab.EOF
            ImprimeGrupoTrab (BL_HandleNull(rsAreaTrab!cod_gr_trab, "")), "", ""
            rsAreaTrab.MoveNext
        Wend
    
    End If
    
End Sub

' ----------------------------------------------------------------------

' IMPRIME GRUPO DE TRABALHO

' ----------------------------------------------------------------------
Private Sub ImprimeGrupoTrab(CodGrupo As String, n_req As String, NFolhaTrab As String)
    Dim sSql As String
    Dim rsGrTrab As New ADODB.recordset
    Dim continua As Boolean
    
    ' ----------------------------------------------------------------------
    ' APAGA DADOS DA TABELA TEMPORARIA
    ' ----------------------------------------------------------------------
    sSql = " DELETE FROM sl_cr_folha_trab WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
    ' ----------------------------------------------------------------------
    ' DADOS DA CODIFICACAO DE GRUPOS
    ' ----------------------------------------------------------------------
    Dim DescrGrTrab As String
    Dim disposicao As String
    Dim modelo As String
    Dim codAnaAviso As String
    Dim QuebraReq As String
    Dim desdobraAna As String
    Dim ColunasFixas As String
    ' ----------------------------------------------------------------------
    
    If CodGrupo = "" Then
        Exit Sub
    End If
    
    ' ----------------------------------------------------------------------
    ' DEFINE ALGUNS DADOS DA FOLHA DE TRABALHO
    ' ----------------------------------------------------------------------
    sSql = "SELECT * FROM sl_gr_trab WHERE cod_gr_trab = " & BL_TrataStringParaBD(CodGrupo)
    rsGrTrab.CursorType = adOpenStatic
    rsGrTrab.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrTrab.Open sSql, gConexao
    If rsGrTrab.RecordCount = 1 Then
        DescrGrTrab = BL_HandleNull(rsGrTrab!descr_gr_trab, "")
        disposicao = BL_HandleNull(rsGrTrab!t_disposicao, "")
        modelo = BL_HandleNull(rsGrTrab!cod_modelo, "")
        codAnaAviso = BL_HandleNull(rsGrTrab!cod_ana_aviso, "")
        QuebraReq = BL_HandleNull(rsGrTrab!Quebra_Req, "")
        desdobraAna = BL_HandleNull(rsGrTrab!desdobra_ana, "")
        ColunasFixas = BL_HandleNull(rsGrTrab!Cols_Fixas, "")
    End If
    rsGrTrab.Close
    Set rsGrTrab = Nothing
    
    ImprimeGrupo CodGrupo, DescrGrTrab, disposicao, modelo, codAnaAviso, QuebraReq, desdobraAna, ColunasFixas, n_req, NFolhaTrab
End Sub


' ----------------------------------------------------------------------

' IMPRIME GRUPO DE TRABALHO - MODELO VERTICAL

' ----------------------------------------------------------------------
Private Sub ImprimeGrupo(CodGrupo As String, DescrGrTrab As String, disposicao As String, modelo As String, _
                                 codAnaAviso As String, QuebraReq As String, desdobraAna As String, ColunasFixas As String, _
                                 n_req As String, NFolhaTrab As String)
                                 
    Dim sSql As String
    Dim rsGrTrab As New ADODB.recordset
    Dim rsOBS As New ADODB.recordset
    Dim cond_s As String
    Dim cond_c As String
    Dim cond_p As String
    Dim cond As String
    Dim condAgrup As String
    Dim Cabecalho As String
    Dim codAnalises As String
    Dim resProteinas As String
    Dim seq As Long
    Dim k As Long
    Dim ObsReq As String
    Dim continua As Boolean
    
    On Error GoTo TrataErro
    
    ' ----------------------------------------------------------------------
    ' PREENCHE ESTRUTURA COM AS ANALISES DO GRUPO DE TRABALHO
    ' ----------------------------------------------------------------------
    sSql = "SELECT * FROM sl_ana_trab WHERE cod_gr_trab = " & BL_TrataStringParaBD(CodGrupo)
    sSql = sSql & " ORDER BY ordem "
    rsGrTrab.CursorType = adOpenStatic
    rsGrTrab.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrTrab.Open sSql, gConexao
    If rsGrTrab.RecordCount >= 0 Then
        totalAna = 0
        ReDim estrutAna(0)
        cond_s = ""
        cond_c = ""
        cond_p = ""
        cond = ""
        condAgrup = " AND (cod_agrup IS NULL OR cod_agrup IN ( "
        While Not rsGrTrab.EOF
            PreencheEstrutAna BL_HandleNull(rsGrTrab!cod_analise, ""), BL_HandleNull(rsGrTrab!ordem, 0), cond_s, cond_c, cond_p
            condAgrup = condAgrup & BL_TrataStringParaBD(BL_HandleNull(rsGrTrab!cod_analise, "")) & ","
            rsGrTrab.MoveNext
        Wend
        condAgrup = Mid(condAgrup, 1, Len(condAgrup) - 1) & "))"
        rsGrTrab.Close
        If cond_s <> "" Then
            cond_s = cond_s & ")"
            cond = "( m.cod_ana_s IN " & cond_s & ")"
        End If
        
        If cond_c <> "" Then
            cond_c = cond_c & ")"
            If cond = "" Then
                cond = "( m.cod_ana_c IN " & cond_c & ")"
            Else
                cond = cond & " OR ( m.cod_ana_c IN " & cond_c & ")"
            End If
        End If
        
        If cond_p <> "" Then
            cond_p = cond_p & ")"
            If cond = "" Then
                cond = "( m.cod_perfil IN " & cond_p & ")"
            Else
                cond = cond & " OR ( m.cod_perfil IN " & cond_p & ")"
            End If
        End If
    End If
    'rsGrTrab.Close
    
    
    If cond <> "" Then
        ' ----------------------------------------------------------------------
        ' RETORNA ANALISES MARCADAS PARA GRUPO TRABALHO EM CAUSA
        ' ----------------------------------------------------------------------
        sSql = RetornaSQLMarcacoes(cond, n_req, NFolhaTrab)
        
        rsGrTrab.CursorType = adOpenStatic
        rsGrTrab.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsGrTrab.Open sSql, gConexao
        If rsGrTrab.RecordCount > 0 Then
            
            If gImprimirDestino = 1 Then
                If disposicao <> gT_Livre Then
                    continua = BL_IniciaReport("FolhasTrabNOVO", "Folha de Trabalho", crptToPrinter, , , , , , , , "HORIZONTAL")
                Else
                    continua = BL_IniciaReport("FolhasTrabLivreNOVO", "Folha de Trabalho", crptToPrinter, , , , , , , , "HORIZONTAL")
                End If
            Else
                If disposicao <> gT_Livre Then
                    continua = BL_IniciaReport("FolhasTrabNOVO", "Folha de trabalho", crptToWindow, , , , , , , , "HORIZONTAL")
                Else
                    continua = BL_IniciaReport("FolhasTrabLivreNOVO", "Folha de trabalho", crptToWindow, , , , , , , , "HORIZONTAL")
                End If
            End If
            If continua = False Then
                Exit Sub
            End If
            
            totalRequis = 0
            ReDim estrutRequis(0)
            While Not rsGrTrab.EOF
                ObsReq = ""
                sSql = "SELECT * FROM SL_OBS_ANA_REQ WHERE n_req = " & rsGrTrab!n_req & condAgrup
                rsOBS.CursorType = adOpenStatic
                rsOBS.CursorLocation = adUseServer
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsOBS.Open sSql, gConexao
                If rsOBS.RecordCount > 0 Then
                    While Not rsOBS.EOF
                        ObsReq = ObsReq & rsOBS!descr_obs & vbCrLf
                        rsOBS.MoveNext
                    Wend
                End If
                rsOBS.Close
                Set rsOBS = Nothing
                
                If CodGrupo = gFolhaTrabElectroforese Then
                    resProteinas = DevolveProteinas(rsGrTrab!n_req)
                End If
                ' ----------------------------------------------------------------------
                ' PREENCHE ESTRUTURA AS REQUISICOES SELECCIONADAS
                ' ----------------------------------------------------------------------
                PreencheEstrutRequis rsGrTrab!n_req, rsGrTrab!Cod_Perfil, rsGrTrab!cod_ana_c, _
                                     rsGrTrab!cod_ana_s, rsGrTrab!nome_ute, BL_HandleNull(rsGrTrab!dt_nasc_ute, ""), _
                                     rsGrTrab!sexo_ute, rsGrTrab!dt_previ, ObsReq, resProteinas
                rsGrTrab.MoveNext
            Wend
            
            
            If EcFolhaTrab = "" Then
                k = 0
                seq = -1
                While seq = -1 And k <= 10
                    seq = BL_GeraNumero("N_FOLHA_TRAB")
                    k = k + 1
                Wend
                EcFolhaTrab = seq
            Else
                seq = EcFolhaTrab
            End If
            
        Else
            If CkImpTodos.value = vbUnchecked Then
                Call BG_Mensagem(mediMsgBox, "N�o foram encontradas nenhumas an�lises para iniciar uma Folha de Trabalho", vbOKOnly + vbExclamation, App.ProductName)
            End If
            Exit Sub
        End If
        rsGrTrab.Close
        Set rsGrTrab = Nothing
        
        codAnalises = ""
        Cabecalho = ""
        
        If disposicao = gT_Vertical Then
            Cabecalho = Vertical(CodGrupo, DescrGrTrab, disposicao, modelo, codAnaAviso, QuebraReq, desdobraAna, ColunasFixas, codAnalises)
        ElseIf disposicao = gT_Horizontal Then
            Cabecalho = Horizontal(CodGrupo, DescrGrTrab, disposicao, modelo, codAnaAviso, QuebraReq, desdobraAna, ColunasFixas)
        ElseIf disposicao = gT_Livre Then
            Cabecalho = livre(CodGrupo, DescrGrTrab, disposicao, modelo, codAnaAviso, QuebraReq, desdobraAna, ColunasFixas)
        End If
            
        'Atribui o resto dos dados ao Report=>F�rmulas e Queries
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        Report.WindowShowPrintBtn = False
        Report.WindowShowExportBtn = False
        'F�rmulas do Report
        
        If disposicao = gT_Vertical Then
            Report.formulas(0) = "Analises=" & BL_TrataStringParaBD("" & Cabecalho)
        End If
        Report.formulas(1) = "FolhaTrab=" & BL_TrataStringParaBD("" & seq)
        Report.formulas(2) = "CodGrTrab=" & BL_TrataStringParaBD("" & CodGrupo)
        Report.formulas(3) = "DescrGrTrab=" & BL_TrataStringParaBD("" & DescrGrTrab)
        Report.formulas(4) = "UserCri=" & BL_TrataStringParaBD("" & BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", gCodUtilizador))
        Report.formulas(5) = "Disposicao=" & BL_TrataStringParaBD("" & disposicao)
        Report.formulas(6) = "Modelo=" & BL_TrataStringParaBD("" & modelo)
        Report.formulas(7) = "CodAnalises=" & BL_TrataStringParaBD("" & codAnalises)
        If disposicao = gT_Livre Then
            Report.formulas(8) = "QuebraReq=" & BL_TrataStringParaBD("" & QuebraReq)
        End If
        If disposicao <> gT_Livre Then
            Report.SQLQuery = " SELECT SL_CR_FOLHA_TRAB.N_REQ,SELECT SL_CR_FOLHA_TRAB.ANALISES "
            Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_FOLHA_TRAB WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
            Report.SQLQuery = Report.SQLQuery & " AND NUM_SESSAO = " & gNumeroSessao & " ORDER BY n_req "
        Else
            Report.SQLQuery = " SELECT SL_CR_FOLHA_TRAB.N_REQ,SELECT SL_CR_FOLHA_TRAB.ANALISES,SL_CR_FOLHA_TRAB.COD_PERFIS,SL_CR_FOLHA_TRAB.COD_ANA_C,SL_CR_FOLHA_TRAB.COD_ANA_S "
            Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_FOLHA_TRAB WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
            Report.SQLQuery = Report.SQLQuery & " AND NUM_SESSAO = " & gNumeroSessao & " ORDER BY n_req,cod_perfis,cod_ana_c, ordem, ordem_p, ordem_c "
        End If
        If gImprimirDestino = 1 Then
            RegistaImprFolhaTrab CodGrupo, seq
        End If
        BL_ExecutaReport
                
    End If
Exit Sub
TrataErro:
    Exit Sub
    Resume Next

End Sub

' ----------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ANALISES

' ----------------------------------------------------------------------
Private Sub PreencheEstrutAna(codAna As String, ordem As Long, ByRef cond_s As String, ByRef cond_c As String, ByRef cond_p As String)
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    If Mid(codAna, 1, 1) = "S" Then
        sSql = "SELECT cod_ana_s cod_ana, abr_ana_s abr_ana, descr_ana_s descr_ana FROM sl_ana_s WHERE cod_ana_S = " & BL_TrataStringParaBD(codAna)
    ElseIf Mid(codAna, 1, 1) = "C" Then
        sSql = "SELECT cod_ana_c cod_ana, abr_ana_c abr_ana, descr_ana_c descr_ana FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(codAna)
    ElseIf Mid(codAna, 1, 1) = "P" Then
        sSql = "SELECT cod_perfis cod_ana, abr_ana_p abr_ana, descr_perfis descr_ana FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAna)
    End If
    
    If sSql <> "" Then
        rsAna.CursorType = adOpenStatic
        rsAna.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount = 1 Then
            totalAna = totalAna + 1
            ReDim Preserve estrutAna(totalAna)
            estrutAna(totalAna).codAna = codAna
            estrutAna(totalAna).ordem = ordem
            estrutAna(totalAna).descrAna = BL_HandleNull(rsAna!descr_ana, "")
            estrutAna(totalAna).abrAna = BL_HandleNull(rsAna!abr_ana, "")
            rsAna.Close
            Set rsAna = Nothing
            If Mid(codAna, 1, 1) = "S" Then
                If cond_s = "" Then
                    cond_s = "(" & BL_TrataStringParaBD(codAna)
                Else
                    cond_s = cond_s & ", " & BL_TrataStringParaBD(codAna)
                End If
            ElseIf Mid(codAna, 1, 1) = "C" Then
                If cond_c = "" Then
                    cond_c = "(" & BL_TrataStringParaBD(codAna)
                Else
                    cond_c = cond_c & ", " & BL_TrataStringParaBD(codAna)
                End If
            ElseIf Mid(codAna, 1, 1) = "P" Then
                If cond_p = "" Then
                    cond_p = "(" & BL_TrataStringParaBD(codAna)
                Else
                    cond_p = cond_p & ", " & BL_TrataStringParaBD(codAna)
                End If
            End If
        End If
    End If
End Sub

' ----------------------------------------------------------------------

' CONSTROI SQL PARA PESQUISA DE REQUISICOES PARA FOLHA DE TRABALHO

' ----------------------------------------------------------------------

Private Function RetornaSQLMarcacoes(cond As String, n_req As String, NFolhaTrab As String) As String
    Dim sSql As String
    If gSGBD = gOracle Then
        sSql = "SELECT m.n_req, m.cod_perfil, m.cod_ana_c, m.cod_ana_s, r.dt_previ, i.nome_ute, i.dt_nasc_ute, i.sexo_ute "
        sSql = sSql & " FROM sl_identif i, sl_requis r, Sl_marcacoes m , sl_req_tubo rt, sl_perfis ap, sl_ana_c ac, sl_ana_s s WHERE i.seq_utente = r.seq_utente AND m.n_req = r.n_req"
        
        sSql = sSql & " AND r.n_Req = rt.n_req and rt.dt_eliminacao is null "
        sSql = sSql & " AND m.cod_ana_s = s.cod_ana_s (+)"
        sSql = sSql & " AND m.cod_perfil = ap.cod_perfis(+) "
        sSql = sSql & " AND m.cod_ana_c = ac.cod_ana_c (+)  "
    ElseIf gSGBD = gSqlServer Then
        sSql = "SELECT m.n_req, m.cod_perfil, m.cod_ana_c, m.cod_ana_s, r.dt_previ, i.nome_ute, i.dt_nasc_ute, i.sexo_ute "
        sSql = sSql & " FROM sl_identif i, sl_requis r, Sl_marcacoes m left outer join sl_perfis ap on m.cod_perfil = ap.cod_perfis "
        sSql = sSql & " left outer join sl_ana_c ac on m.cod_ana_c = ac.cod_ana_c  left outer join sl_ana_s s on m.cod_ana_s = s.cod_ana_s, "
        sSql = sSql & " sl_req_tubo rt WHERE i.seq_utente = r.seq_utente AND m.n_req = r.n_req"
        
        sSql = sSql & " AND r.n_Req = rt.n_req and rt.dt_eliminacao is null "
    End If
    
    sSql = sSql & " AND m.n_Req = rt.n_req "
    sSql = sSql & " AND (   ap.cod_tubo = rt.cod_tubo "
    sSql = sSql & " OR ac.cod_tubo = rt.cod_tubo "
    sSql = sSql & " OR s.cod_tubo = rt.cod_tubo) "
    sSql = sSql & " AND r.estado_Req not in (" & BL_TrataStringParaBD(gEstadoReqCancelada) & "," & BL_TrataStringParaBD(gEstadoReqBloqueada) & " ) "
    
    If CkDatas.value = vbChecked Then
        sSql = sSql & " AND (rt.dt_chega BETWEEN  " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
    Else
        sSql = sSql & " AND rt.dt_chega >= " & BL_TrataDataParaBD(Bg_DaData_ADO - 90)
    End If
    sSql = sSql & " AND rt.dt_chega is not null "
    If CkNovasAnalises.value = vbChecked And NFolhaTrab = "" Then
        sSql = sSql & " AND n_folha_trab = 0 and m.cod_ana_s <> 'S99999' "
    End If
    
    'Local Preenchido?
    If (CbLocal.ListIndex <> -1) Then
        sSql = sSql & " AND r.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
    End If
        
    If n_req <> "" Then
        sSql = sSql & " AND r.n_req = " & BL_TrataStringParaBD(EcNumReq)
    End If
    If NFolhaTrab <> "" Then
        sSql = sSql & " AND m.n_folha_trab = " & NFolhaTrab
    End If
    sSql = sSql & " AND (" & cond & ")"
    sSql = sSql & " ORDER BY m.n_req, m.ord_ana, m.ord_ana_perf, m.ord_ana_compl  "
    RetornaSQLMarcacoes = sSql
End Function


' ----------------------------------------------------------------------

' PREENCHE ESTRUTURA DE Requisicoes

' ----------------------------------------------------------------------
Private Sub PreencheEstrutRequis(NReq As String, cod_perfis As String, cod_ana_c As String, cod_ana_s As String, _
                                 nome As String, dt_nasc As String, Sexo As String, dt_previ As String, obs As String, _
                                 resProteinas As String)
    Dim i As Long
    Dim j As Long
    
    Dim encontrou As Boolean
    encontrou = False
    
    For i = 1 To totalRequis
        If estrutRequis(i).n_req = NReq Then
            encontrou = True
            Exit For
        End If
    Next
    
    If encontrou = False Then
        totalRequis = totalRequis + 1
        ReDim Preserve estrutRequis(totalRequis)
        ReDim Preserve estrutRequis(totalRequis).analises(totalAna)
        estrutRequis(totalRequis).n_req = NReq
        estrutRequis(totalRequis).nome = nome
        estrutRequis(totalRequis).dt_nasc = dt_nasc
        estrutRequis(totalRequis).dt_previ = dt_previ
        estrutRequis(totalRequis).Sexo = Sexo
        estrutRequis(totalRequis).obs = obs
        estrutRequis(totalRequis).resProteinas = resProteinas
        
        For i = 1 To totalAna
            estrutRequis(totalRequis).analises(i) = False
        Next
        i = totalRequis
    End If
    For j = 1 To totalAna
    
        If estrutAna(j).codAna = cod_perfis Then
            estrutRequis(i).analises(j) = True
            estrutAna(j).Existe = True
        End If
        
        If estrutAna(j).codAna = cod_ana_c Then
            estrutRequis(i).analises(j) = True
            estrutAna(j).Existe = True
        End If
        
        If estrutAna(j).codAna = cod_ana_s Then
            estrutRequis(i).analises(j) = True
            estrutAna(j).Existe = True
        End If
    Next
    
End Sub


' ----------------------------------------------------------------------

' GERA FOLHA DE ACORDO COM ESTILO - VERTICAL

' ----------------------------------------------------------------------
Private Function Vertical(CodGrupo As String, DescrGrTrab As String, disposicao As String, modelo As String, _
                     codAnaAviso As String, QuebraReq As String, desdobraAna As String, ColunasFixas As String, _
                     ByRef codAnalises As String) As String
                                 
    Dim Cabecalho As String
    Dim k As Long
    Dim j  As Long
    
    ' ----------------------------------------------------------------------
    ' PREENCHE CABE�ALHO DA FOLHA DE TRABALHO
    ' ----------------------------------------------------------------------
    If ColunasFixas = "1" Then
        Cabecalho = ""
        codAnalises = ""
        For k = 1 To totalAna
            Cabecalho = Cabecalho & Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)
            If Len(Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)) < 8 Then
                Cabecalho = Cabecalho & Space(8 - Len(Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)))
            End If
            Cabecalho = Cabecalho & " "
            
            codAnalises = codAnalises & estrutAna(k).codAna
            If Len(estrutAna(k).codAna) < 9 Then
                codAnalises = codAnalises & Space(9 - Len(estrutAna(k).codAna))
            End If
            
        Next
    Else
        codAnalises = ""
        Cabecalho = ""
        For k = 1 To totalAna
            If estrutAna(k).Existe = True Then
                Cabecalho = Cabecalho & Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)
                If Len(Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)) < 8 Then
                    Cabecalho = Cabecalho & Space(8 - Len(Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)))
                End If
                Cabecalho = Cabecalho & " "
                
                codAnalises = codAnalises & estrutAna(k).codAna
                If Len(estrutAna(k).codAna) < 9 Then
                    codAnalises = codAnalises & Space(9 - Len(estrutAna(k).codAna))
                End If
            End If
        Next
    End If
    Vertical = Cabecalho
    
    ' ----------------------------------------------------------------------
    ' PREENCHE ANALISES PRESENTES NAS REQUISICOES
    ' ----------------------------------------------------------------------
    For j = 1 To totalRequis
        estrutRequis(j).linha = ""
        For k = 1 To totalAna
            If estrutRequis(j).analises(k) = True Then
                estrutRequis(j).linha = estrutRequis(j).linha & "_______. "
                
                If gImprimirDestino = 1 Then
                    ColocaNumFolhaTrab j, k
                End If
            Else
                If ColunasFixas = "1" Then
                    estrutRequis(j).linha = estrutRequis(j).linha & "       . "
                Else
                    If estrutAna(k).Existe = True Then
                        estrutRequis(j).linha = estrutRequis(j).linha & "       . "
                    End If
                End If
            End If
        Next
        ' ----------------------------------------------------------------------
        ' INSERE NA TABELA TEMPORARIA DO REPORT
        ' ----------------------------------------------------------------------
        InsereTabelaTemporaria j
    Next

End Function

' ----------------------------------------------------------------------

' GERA FOLHA DE ACORDO COM ESTILO - HORIZONTAL

' ----------------------------------------------------------------------
Private Function Horizontal(CodGrupo As String, DescrGrTrab As String, disposicao As String, modelo As String, _
                     codAnaAviso As String, QuebraReq As String, desdobraAna As String, ColunasFixas As String) As String
                                 
    Dim Cabecalho As String
    Dim k As Long
    Dim j  As Long
    Dim p As Long
    
    ' ----------------------------------------------------------------------
    ' PREENCHE ANALISES PRESENTES NAS REQUISICOES
    ' ----------------------------------------------------------------------
    For j = 1 To totalRequis
        estrutRequis(j).linha = ""
        estrutRequis(j).abrev = ""
        For k = 1 To totalAna
            If estrutRequis(j).analises(k) = True Then
                estrutRequis(j).abrev = estrutRequis(j).abrev & Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)
                If Len(Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)) < 7 Then
                    For p = Len(Mid(BL_HandleNull(estrutAna(k).abrAna, estrutAna(k).descrAna), 1, 7)) To 7
                        estrutRequis(j).abrev = estrutRequis(j).abrev & "   "
                    Next
                End If
                estrutRequis(j).abrev = estrutRequis(j).abrev & "   "
                estrutRequis(j).linha = estrutRequis(j).linha & "______.  "
                
                If gImprimirDestino = 1 Then
                    ColocaNumFolhaTrab j, k
                End If
                
            End If
        Next
        ' ----------------------------------------------------------------------
        ' INSERE NA TABELA TEMPORARIA DO REPORT
        ' ----------------------------------------------------------------------
        InsereTabelaTemporaria j
    Next

End Function

' ----------------------------------------------------------------------

' GERA FOLHA DE ACORDO COM ESTILO - LIVRE

' ----------------------------------------------------------------------
Private Function livre(CodGrupo As String, DescrGrTrab As String, disposicao As String, modelo As String, _
                     codAnaAviso As String, QuebraReq As String, desdobraAna As String, ColunasFixas As String) As String
                                 
    Dim Cabecalho As String
    Dim k As Long
    Dim j  As Long
    Dim p As Long
    Dim sSql As String
    Dim rsLivreC As New ADODB.recordset
    Dim rsLivreP As New ADODB.recordset
    On Error GoTo TrataErro
    ' ----------------------------------------------------------------------
    ' PREENCHE ANALISES PRESENTES NAS REQUISICOES
    ' ----------------------------------------------------------------------
    For j = 1 To totalRequis
        estrutRequis(j).linha = ""
        estrutRequis(j).abrev = ""
        For k = 1 To totalAna
            If estrutRequis(j).analises(k) = True Then
                If gImprimirDestino = 1 Then
                    ColocaNumFolhaTrab j, k
                End If
            
                ' SIMPLES
                ' ----------------------------------------------------------------------
                If Mid(estrutAna(k).codAna, 1, 1) = "S" Then
                    InsereTabelaTemporaria_LIVRE j, "0", "0", estrutAna(k).codAna, 0, 0, estrutAna(k).ordem
                
                ' COMPLEXAS
                ' ----------------------------------------------------------------------
                ElseIf Mid(estrutAna(k).codAna, 1, 1) = "C" Then
                    sSql = "SELECT * FROM SL_MEMBRO WHERE cod_ana_c = " & BL_TrataStringParaBD(estrutAna(k).codAna) & " ORDER BY ordem "
                    rsLivreC.CursorType = adOpenStatic
                    rsLivreC.CursorLocation = adUseServer
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    rsLivreC.Open sSql, gConexao
                    If rsLivreC.RecordCount >= 1 Then
                        While Not rsLivreC.EOF
                            If AnaliseMarcada(estrutRequis(j).n_req, BL_HandleNull(rsLivreC!cod_membro, "")) = True Then
                                InsereTabelaTemporaria_LIVRE j, "0", estrutAna(k).codAna, BL_HandleNull(rsLivreC!cod_membro, ""), 0, BL_HandleNull(rsLivreC!ordem, 0), estrutAna(k).ordem
                            End If
                            rsLivreC.MoveNext
                        Wend
                    End If
                    rsLivreC.Close
                    
                ' PERFIS
                ' ----------------------------------------------------------------------
                ElseIf Mid(estrutAna(k).codAna, 1, 1) = "P" Then
                    sSql = "SELECT * FROM SL_ANA_PERFIS WHERE cod_perfis = " & BL_TrataStringParaBD(estrutAna(k).codAna) & " ORDER BY ordem "
                    rsLivreP.CursorType = adOpenStatic
                    rsLivreP.CursorLocation = adUseServer
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    rsLivreP.Open sSql, gConexao
                    If rsLivreP.RecordCount >= 1 Then
                        While Not rsLivreP.EOF
                        
                            If Mid(BL_HandleNull(rsLivreP!cod_analise, ""), 1, 1) = "S" Then
                                InsereTabelaTemporaria_LIVRE j, estrutAna(k).codAna, "0", BL_HandleNull(rsLivreP!cod_analise, ""), BL_HandleNull(rsLivreP!ordem, 0), 0, estrutAna(k).ordem
                            ElseIf Mid(BL_HandleNull(rsLivreP!cod_analise, ""), 1, 1) = "C" Then
                                sSql = "SELECT * FROM SL_MEMBRO WHERE cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(rsLivreP!cod_analise, ""))
                                rsLivreC.CursorType = adOpenStatic
                                rsLivreC.CursorLocation = adUseServer
                                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                                rsLivreC.Open sSql, gConexao
                                If rsLivreC.RecordCount >= 1 Then
                                    While Not rsLivreC.EOF
                                        InsereTabelaTemporaria_LIVRE j, estrutAna(k).codAna, BL_HandleNull(rsLivreP!cod_analise, ""), BL_HandleNull(rsLivreC!cod_membro, ""), BL_HandleNull(rsLivreP!ordem, 0), BL_HandleNull(rsLivreC!ordem, 0), estrutAna(k).ordem
                                        rsLivreC.MoveNext
                                    Wend
                                End If
                                rsLivreC.Close
                            End If
                            rsLivreP.MoveNext
                        Wend
                    End If
                    rsLivreP.Close
                End If
            End If
        Next
        ' ----------------------------------------------------------------------
        ' INSERE NA TABELA TEMPORARIA DO REPORT
        ' ----------------------------------------------------------------------
        InsereTabelaTemporaria j
    Next
Exit Function
TrataErro:
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' ALTERA SL_MARCACOES COLOCANDO NUMERO DA FOLHA NA SL_MARCACOES

' ----------------------------------------------------------------------
Private Sub ColocaNumFolhaTrab(iReq As Long, iAna As Long)
    Dim sSql As String
    
    If Mid(estrutAna(iAna).codAna, 1, 1) = "S" Then
        sSql = "UPDATE sl_marcacoes SET n_folha_trab = " & EcFolhaTrab & " WHERE n_req =" & BL_TrataStringParaBD(estrutRequis(iReq).n_req)
        sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(estrutAna(iAna).codAna)
        BG_ExecutaQuery_ADO sSql
    ElseIf Mid(estrutAna(iAna).codAna, 1, 1) = "C" Then
        sSql = "UPDATE sl_marcacoes SET n_folha_trab = " & EcFolhaTrab & " WHERE n_req = " & BL_TrataStringParaBD(estrutRequis(iReq).n_req)
        sSql = sSql & " AND cod_ana_C = " & BL_TrataStringParaBD(estrutAna(iAna).codAna)
        BG_ExecutaQuery_ADO sSql
    ElseIf Mid(estrutAna(iAna).codAna, 1, 1) = "P" Then
        sSql = "UPDATE sl_marcacoes SET n_folha_trab = " & EcFolhaTrab & " WHERE n_req = " & BL_TrataStringParaBD(estrutRequis(iReq).n_req)
        sSql = sSql & " AND cod_perfil = " & BL_TrataStringParaBD(estrutAna(iAna).codAna)
        BG_ExecutaQuery_ADO sSql
    End If
    
End Sub

' ----------------------------------------------------------------------

' INSERE NA TABELA TEMPORARIA DO REPORT

' ----------------------------------------------------------------------
Private Sub InsereTabelaTemporaria(j As Long)
    Dim sSql As String
    
    sSql = "INSERT INTO sl_cr_folha_trab (nome_computador, num_sessao, n_req, analises, nome, dt_chega,idade,"
    sSql = sSql & " sexo, obs, abr_ana, res_proteinas) VALUES ("
    sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
    sSql = sSql & gNumeroSessao & ", "
    sSql = sSql & estrutRequis(j).n_req & ","
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(j).linha & "") & ","
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(j).nome & "") & ","
    sSql = sSql & BL_TrataDataParaBD(estrutRequis(j).dt_previ & "") & ","
    If BL_HandleNull(estrutRequis(j).dt_nasc, "") <> "" Then
        If InStr(1, UCase(BG_CalculaIdade(CDate(estrutRequis(j).dt_nasc), Bg_DaData_ADO)), "ANO") > 0 Then
            sSql = sSql & Trim(Mid(BG_CalculaIdade(CDate(estrutRequis(j).dt_nasc), Bg_DaData_ADO), 1, 2)) & ","
        Else
            sSql = sSql & "0 ,"
        End If
    Else
        sSql = sSql & "0 ,"
    End If
    
    If estrutRequis(j).Sexo = gT_Masculino Then
        sSql = sSql & BL_TrataStringParaBD("M") & ","
    ElseIf estrutRequis(j).Sexo = gT_Feminino Then
        sSql = sSql & BL_TrataStringParaBD("F") & ","
    Else
        sSql = sSql & BL_TrataStringParaBD("O") & ","
    End If
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(j).obs & "") & ","
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(j).abrev & "") & ","
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(j).resProteinas & "") & ")"
    BG_ExecutaQuery_ADO sSql
End Sub

' --------------------------------------------------------------------------------------------------------------------------------------------

' INSERE NA TABELA TEMPORARIA DO REPORT

' --------------------------------------------------------------------------------------------------------------------------------------------
Private Sub InsereTabelaTemporaria_LIVRE(idxR As Long, cod_perfis As String, cod_ana_c As String, cod_ana_s As String, _
                                         ordem_p As Long, ordem_c As Long, ordem As Long)
    Dim sSql As String


                                  
    sSql = "INSERT INTO sl_cr_folha_trab (nome_computador, num_sessao, n_req,  nome, dt_chega,idade,"
    sSql = sSql & " sexo, obs, cod_perfis, cod_ana_c, cod_ana_s, ordem_p, ordem_c, ordem, descr_perfis, descr_ana_c, "
    sSql = sSql & " descr_ana_s, res_proteinas) VALUES ("
    sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
    sSql = sSql & gNumeroSessao & ", "
    sSql = sSql & estrutRequis(idxR).n_req & ","
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(idxR).nome & "") & ","
    sSql = sSql & BL_TrataDataParaBD(estrutRequis(idxR).dt_previ & "") & ","
    If estrutRequis(idxR).dt_nasc <> "" Then
        If InStr(1, UCase(BG_CalculaIdade(CDate(estrutRequis(idxR).dt_nasc), Bg_DaData_ADO)), "ANO") > 0 Then
            sSql = sSql & Trim(Mid(BG_CalculaIdade(CDate(estrutRequis(idxR).dt_nasc), Bg_DaData_ADO), 1, 2)) & ","
        Else
            sSql = sSql & "0 ,"
        End If
    Else
        sSql = sSql & "0 ,"
    End If
    
    If estrutRequis(idxR).Sexo = gT_Masculino Then
        sSql = sSql & BL_TrataStringParaBD("M") & ","
    ElseIf estrutRequis(idxR).Sexo = gT_Feminino Then
        sSql = sSql & BL_TrataStringParaBD("F") & ","
    Else
        sSql = sSql & BL_TrataStringParaBD("O") & ","
    End If
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(idxR).obs & "") & ","
    sSql = sSql & BL_TrataStringParaBD(cod_perfis & "") & ","
    sSql = sSql & BL_TrataStringParaBD(cod_ana_c & "") & ","
    sSql = sSql & BL_TrataStringParaBD(cod_ana_s & "") & ","
    sSql = sSql & ordem_p & ", "
    sSql = sSql & ordem_c & ", "
    sSql = sSql & ordem & ", "
    If cod_perfis <> "0" Then
        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", cod_perfis) & "") & ","
    Else
        sSql = sSql & " NULL,"
    End If
    If cod_ana_c <> "0" Then
        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", cod_ana_c) & "") & ","
    Else
        sSql = sSql & " NULL,"
    End If
    sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", cod_ana_s) & "") & ","
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(idxR).resProteinas) & ")"
    BG_ExecutaQuery_ADO sSql
End Sub

Private Sub ImprimeAutomaticos()
    Dim sSql As String
    Dim rsAreaTrab As New ADODB.recordset
    
    
    sSql = "SELECT distinct cod_gr_trab FROM sl_gr_trab WHERE  impressao_automatica = 1 "
    sSql = sSql & " ORDER BY cod_gr_trab "
    rsAreaTrab.CursorType = adOpenStatic
    rsAreaTrab.CursorLocation = adUseServer
    rsAreaTrab.Open sSql, gConexao
    If rsAreaTrab.RecordCount > 0 Then
        While Not rsAreaTrab.EOF
            ImprimeGrupoTrab (BL_HandleNull(rsAreaTrab!cod_gr_trab, "")), "", ""
            rsAreaTrab.MoveNext
            EcFolhaTrab = ""
        Wend
    
    End If
    rsAreaTrab.Close
End Sub
Private Function AnaliseMarcada(n_req As String, codMembro As String) As Boolean
    Dim sSql As String
    Dim rsAreaTrab As New ADODB.recordset
    AnaliseMarcada = False
    sSql = "SELECT * FROM sl_marcacoes WHERE n_req = " & BL_TrataStringParaBD(n_req) & " AND cod_ana_S = " & BL_TrataStringParaBD(codMembro)
    rsAreaTrab.CursorType = adOpenStatic
    rsAreaTrab.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAreaTrab.Open sSql, gConexao
    If rsAreaTrab.RecordCount > 0 Then
        AnaliseMarcada = True
    End If
    rsAreaTrab.Close
    
End Function
Private Sub EcData_Change()
    Dim sSql As String
    Dim rsFolhasTrab As New ADODB.recordset
    totalImpr = 0
    ReDim EstrutReImpr(0)
    
    sSql = "select distinct seq_impr,cod_gr_trab,descr_gr_trab from sl_impr_folhas_trab, sl_gr_trab where cod_grupo = cod_gr_trab"
    sSql = sSql & " AND dt_imp = " & BL_TrataDataParaBD(EcData.value)
    EcListaFolhasTrab.Clear
    rsFolhasTrab.CursorLocation = adUseServer
    rsFolhasTrab.CursorType = adOpenForwardOnly
    rsFolhasTrab.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFolhasTrab.Open sSql, gConexao
    If rsFolhasTrab.RecordCount > 0 Then
        While Not rsFolhasTrab.EOF
            totalImpr = totalImpr + 1
            ReDim Preserve EstrutReImpr(totalImpr)
            EstrutReImpr(totalImpr).cod_gr_trab = BL_HandleNull(rsFolhasTrab!cod_gr_trab, "")
            EstrutReImpr(totalImpr).descr_gr_trab = BL_HandleNull(rsFolhasTrab!descr_gr_trab, "")
            EstrutReImpr(totalImpr).seq_impr = BL_HandleNull(rsFolhasTrab!seq_impr, -1)
            
            EcListaFolhasTrab.AddItem BL_HandleNull(rsFolhasTrab!cod_gr_trab, "") & BL_HandleNull(rsFolhasTrab!descr_gr_trab, "")
            EcListaFolhasTrab.ItemData(EcListaFolhasTrab.NewIndex) = BL_HandleNull(rsFolhasTrab!seq_impr, -1)
            
            rsFolhasTrab.MoveNext
        Wend
    End If
End Sub

Private Sub CkSelTodas_Click()
    Dim i As Long
    If CkSelTodas.value = vbChecked Then
        For i = 0 To EcListaFolhasTrab.ListCount - 1
            EcListaFolhasTrab.Selected(i) = True
        Next
    Else
        For i = 0 To EcListaFolhasTrab.ListCount - 1
            EcListaFolhasTrab.Selected(i) = False
        Next
    End If
End Sub


Private Sub CbLocal_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbLocal.ListIndex = -1
    
End Sub


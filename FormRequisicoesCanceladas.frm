VERSION 5.00
Begin VB.Form FormRequisicoesCanceladas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormRequisicoesCanceladas"
   ClientHeight    =   6060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9420
   Icon            =   "FormRequisicoesCanceladas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   9420
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcNomeUtilizadorCanc 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   285
      Left            =   5280
      TabIndex        =   20
      Top             =   840
      Width           =   3255
   End
   Begin VB.TextBox EcUtilCancelamento 
      Height          =   285
      Left            =   4560
      TabIndex        =   5
      Top             =   840
      Width           =   735
   End
   Begin VB.ComboBox CbTipoCancelamento 
      Height          =   315
      Left            =   4440
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   120
      Width           =   4095
   End
   Begin VB.TextBox EcTipoCancelamento 
      Height          =   285
      Left            =   1920
      TabIndex        =   17
      Top             =   4920
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ListBox EcLista 
      Height          =   2010
      Left            =   120
      TabIndex        =   7
      Top             =   2400
      Width           =   8415
   End
   Begin VB.TextBox EcObs 
      Height          =   615
      Left            =   1320
      MultiLine       =   -1  'True
      TabIndex        =   6
      Top             =   1440
      Width           =   7215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cancelamento"
      Height          =   855
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   3375
      Begin VB.TextBox EcHoraCancelamento 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2280
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox EcDataCancelamento 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   600
         TabIndex        =   3
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Hora"
         Height          =   255
         Left            =   1800
         TabIndex        =   10
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "Data"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   1560
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label11 
      Caption         =   "EcTipoCancelamento"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   4920
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label8 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   3720
      TabIndex        =   18
      Top             =   840
      Width           =   975
   End
   Begin VB.Label Label10 
      Caption         =   "Tipo cancelamento"
      Height          =   255
      Left            =   2880
      TabIndex        =   16
      Top             =   140
      Width           =   1575
   End
   Begin VB.Label Label9 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   7080
      TabIndex        =   15
      Top             =   2160
      Width           =   735
   End
   Begin VB.Label Label7 
      Caption         =   "Hora cancelamento"
      Height          =   255
      Left            =   4320
      TabIndex        =   14
      Top             =   2160
      Width           =   1455
   End
   Begin VB.Label Label6 
      Caption         =   "Data cancelamento"
      Height          =   255
      Left            =   1920
      TabIndex        =   13
      Top             =   2160
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "N�mero"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   2160
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "Observa��es "
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "N�mero requisi��o"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   140
      Width           =   1335
   End
End
Attribute VB_Name = "FormRequisicoesCanceladas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String
Dim VectorCamposBD

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormRequisicoesCanceladas = Nothing

End Sub

Sub Inicializacoes()
    
    Me.caption = " Requisi��es Canceladas"
    Me.left = 440
    Me.top = 350
    Me.Width = 8745
    Me.Height = 4905 ' Normal
    'Me.Height = 7330 ' Campos Extras
    
    NomeTabela = "sl_req_canc"
    Set CampoDeFocus = EcNumReq
    
    NumCampos = 6
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "dt_canc"
    CamposBD(2) = "hr_canc"
    CamposBD(3) = "t_canc"
    CamposBD(4) = "obs_canc"
    CamposBD(5) = "user_canc"
    
    ' Campos do Ecr�
    
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcDataCancelamento
    Set CamposEc(2) = EcHoraCancelamento
    Set CamposEc(3) = EcTipoCancelamento
    Set CamposEc(4) = EcObs
    Set CamposEc(5) = EcUtilCancelamento
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_req"
    Set ChaveEc = EcNumReq
    
    CamposBDparaListBox = Array("n_req", "dt_canc", "hr_canc", "user_canc")
    NumEspacos = Array(40, 50, 50, 40)

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_canc", EcDataCancelamento, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_canc", EcHoraCancelamento, mediTipoHora
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_t_canc", "seq_t_canc", "descr_canc", CbTipoCancelamento

End Sub

Private Sub CbTipoCancelamento_Click()
    
    BL_ColocaComboTexto "sl_t_canc", "seq_t_canc", "cod_t_canc", EcTipoCancelamento, CbTipoCancelamento

End Sub

Private Sub CbTipoCancelamento_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbTipoCancelamento.ListIndex = -1

End Sub

Private Sub EcDataCancelamento_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, EcDataCancelamento

End Sub

Private Sub EcHoraCancelamento_Change()
    
    BG_ValidaTipoCampo_ADO Me, EcHoraCancelamento

End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If

End Sub

Private Sub EcUtilCancelamento_Change()
    
    EcNomeUtilizadorCanc = BL_SelNomeUtil(EcUtilCancelamento)

End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY n_req ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        LimpaCampos
        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Sub LimpaCampos()
    
    BG_LimpaCampo_Todos CamposEc
    CbTipoCancelamento.Enabled = True
    CbTipoCancelamento.ListIndex = mediComboValorNull
    EcNomeUtilizadorCanc.Text = ""

End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_ColocaTextoCombo "sl_t_canc", "seq_t_canc", "cod_t_canc", EcTipoCancelamento, CbTipoCancelamento
        CbTipoCancelamento.Enabled = False
    End If
    
End Sub

Sub FuncaoLimpar()
    
    LimpaCampos
    EcLista.Clear
    EcNumReq.SetFocus

End Sub

Sub Funcao_DataActual()
    
    BL_PreencheData EcDataCancelamento, Me.ActiveControl

End Sub


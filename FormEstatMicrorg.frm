VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEstatMicrorg 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7620
   Icon            =   "FormEstatMicrorg.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8745
   ScaleWidth      =   7620
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkAgruparMicrorg 
      Caption         =   "Agrupar Design. Microrg."
      Height          =   255
      Left            =   5520
      TabIndex        =   51
      Top             =   7320
      Width           =   2055
   End
   Begin VB.TextBox EcPesqRapAntibio 
      Height          =   285
      Left            =   3360
      TabIndex        =   35
      Top             =   7680
      Width           =   975
   End
   Begin VB.CheckBox Flg_DescrRequis 
      Caption         =   "Descriminar Requisi��es e Utentes"
      Height          =   195
      Left            =   2760
      TabIndex        =   30
      Top             =   7320
      Width           =   2895
   End
   Begin VB.CheckBox Flg_ApresGrafico 
      Caption         =   "Apresentar gr�fico"
      Enabled         =   0   'False
      Height          =   195
      Left            =   480
      TabIndex        =   29
      Top             =   7320
      Width           =   2055
   End
   Begin VB.CheckBox Flg_OrderProd 
      Caption         =   "Agrupar por Produto"
      Height          =   195
      Left            =   2760
      TabIndex        =   28
      Top             =   7080
      Width           =   1815
   End
   Begin VB.CheckBox Flg_OrderProven 
      Caption         =   "Agrupar por Proveni�ncia"
      Height          =   195
      Left            =   5520
      TabIndex        =   27
      Top             =   7080
      Width           =   2295
   End
   Begin VB.TextBox EcPesqRapMicro 
      Height          =   285
      Left            =   2040
      TabIndex        =   26
      Top             =   7680
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapProduto 
      Height          =   285
      Left            =   600
      TabIndex        =   25
      Top             =   7680
      Width           =   975
   End
   Begin VB.CheckBox Flg_DescrAntibio 
      Caption         =   "Descriminar Antibi�ticos"
      Height          =   195
      Left            =   480
      TabIndex        =   24
      Top             =   7080
      Width           =   2055
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   7335
      Begin VB.CommandButton BtReportTabela 
         Caption         =   "Tabela"
         Height          =   725
         Left            =   5880
         Picture         =   "FormEstatMicrorg.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   120
         Width           =   735
      End
      Begin VB.CommandButton BtReportLista 
         Caption         =   "Lista"
         Height          =   725
         Left            =   6600
         Picture         =   "FormEstatMicrorg.frx":0156
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   17
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   16
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   19
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   18
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   5775
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   7335
      Begin VB.Frame Frame4 
         Caption         =   "Filtro de Exames"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   240
         TabIndex        =   43
         Top             =   3840
         Width           =   6975
         Begin VB.ListBox ListaPerfisS 
            Height          =   1230
            Left            =   3720
            MultiSelect     =   2  'Extended
            TabIndex        =   47
            Top             =   480
            Width           =   3045
         End
         Begin VB.CommandButton BtRetira 
            Height          =   400
            Left            =   3000
            Picture         =   "FormEstatMicrorg.frx":02A0
            Style           =   1  'Graphical
            TabIndex        =   46
            Top             =   1200
            Width           =   615
         End
         Begin VB.CommandButton BtInsere 
            Height          =   400
            Left            =   3000
            Picture         =   "FormEstatMicrorg.frx":05AA
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   600
            Width           =   615
         End
         Begin VB.ListBox ListaPerfis 
            Height          =   1230
            Left            =   120
            MultiSelect     =   2  'Extended
            TabIndex        =   44
            Top             =   480
            Width           =   2805
         End
         Begin VB.Label Label8 
            Caption         =   "Pesquisar Exames"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   4320
            TabIndex        =   50
            Top             =   240
            Width           =   1935
         End
         Begin VB.Label Label7 
            Caption         =   "Exames"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1200
            TabIndex        =   49
            Top             =   240
            Width           =   855
         End
         Begin VB.Label LbCapacidadeUtil 
            Height          =   255
            Left            =   9000
            TabIndex        =   48
            Top             =   1560
            Width           =   375
         End
      End
      Begin VB.ListBox ListaAntibio 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   31
         Top             =   3240
         Width           =   4800
      End
      Begin VB.ListBox ListaMicrorg 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   41
         Top             =   2640
         Width           =   4800
      End
      Begin VB.ComboBox CbSensib 
         Height          =   315
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   840
         Width           =   1455
      End
      Begin VB.ListBox ListaProven 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   39
         Top             =   2040
         Width           =   4800
      End
      Begin VB.ListBox ListaProduto 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   38
         Top             =   1440
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaAntibio 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrorg.frx":08B4
         Style           =   1  'Graphical
         TabIndex        =   33
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   3240
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaMicro 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrorg.frx":0E3E
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Microrganismos"
         Top             =   2640
         Width           =   375
      End
      Begin VB.TextBox EcDescrMicro 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   2640
         Width           =   4095
      End
      Begin VB.TextBox EcCodMicro 
         Height          =   315
         Left            =   1560
         TabIndex        =   20
         Top             =   2640
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrorg.frx":13C8
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncias "
         Top             =   2040
         Width           =   375
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   2040
         Width           =   4095
      End
      Begin VB.TextBox EcCodProveniencia 
         Height          =   315
         Left            =   1560
         TabIndex        =   7
         Top             =   2040
         Width           =   735
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   840
         Width           =   1455
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   240
         Width           =   1455
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   360
         Width           =   1455
      End
      Begin VB.CommandButton BtPesquisaProduto 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrorg.frx":1952
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   1440
         Width           =   375
      End
      Begin VB.TextBox EcDescrProduto 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   1440
         Width           =   4095
      End
      Begin VB.TextBox EcCodProduto 
         Height          =   315
         Left            =   1560
         TabIndex        =   1
         Top             =   1440
         Width           =   735
      End
      Begin MSFlexGridLib.MSFlexGrid FGAntibio 
         Height          =   855
         Left            =   240
         TabIndex        =   42
         Top             =   3240
         Visible         =   0   'False
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   1508
         _Version        =   393216
      End
      Begin VB.Label LaSensib 
         Caption         =   "Sensibilidade"
         Height          =   255
         Left            =   4200
         TabIndex        =   34
         Top             =   840
         Width           =   975
      End
      Begin VB.Label LaAntibio 
         Caption         =   "&Antibi�tico"
         Height          =   255
         Left            =   360
         TabIndex        =   32
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "&Microrganismo"
         Height          =   255
         Left            =   360
         TabIndex        =   23
         Top             =   2640
         Width           =   1215
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   14
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   4200
         TabIndex        =   12
         Top             =   240
         Width           =   735
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   11
         Top             =   360
         Width           =   360
      End
      Begin VB.Label Label1 
         Caption         =   "Pro&duto"
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   1440
         Width           =   975
      End
   End
End
Attribute VB_Name = "FormEstatMicrorg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim ListaOrigem As Control
Dim ListaDestino As Control

Public rs As ADODB.recordset

Sub Preenche_Estatistica(TipoReport As String)
    
    Dim sql As String
    Dim continua As Boolean
    Dim i As Integer
    Dim totalMicro As Long
            
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Estat�stica de Microrganismos") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    gImprimirDestino = 0
    If gImprimirDestino = 1 Then
        If TipoReport = "Lista" Then
            continua = BL_IniciaReport("EstatisticaMicrorg", "Estat�stica de Microrganismos", crptToPrinter)
        ElseIf TipoReport = "Tabela" Then
            continua = BL_IniciaReport("EstatisticaMicrorg_Tabela", "Estat�stica de Microrganismos", crptToPrinter)
        End If
    Else
        If TipoReport = "Lista" Then
            continua = BL_IniciaReport("EstatisticaMicrorg", "Estat�stica de Microrganismos", crptToWindow)
        ElseIf TipoReport = "Tabela" Then
            continua = BL_IniciaReport("EstatisticaMicrorg_Tabela", "Estat�stica de Microrganismos", crptToWindow)
        End If
    End If
    If continua = False Then Exit Sub
        
    Call Cria_TmpRec_Estatistica
    
    totalMicro = PreencheTabelaTemporaria
    If totalMicro = 0 Then
        BG_Mensagem mediMsgBox, "N�o existem registos para abrir a estat�stica!", vbInformation, Me.caption
        Exit Sub
    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT SL_CR_ESTATMICRORG.DESCR_MICRORG, SL_CR_ESTATMICRORG.DESCR_ANTIBIO, SL_CR_ESTATMICRORG.RES_SENSIB, SL_CR_ESTATMICRORG.DESCR_PROVEN, SL_CR_ESTATMICRORG.DESCR_PRODUTO " & _
        " FROM SL_CR_ESTATMICRORG" & gNumeroSessao & " SL_CR_ESTATMICRORG ORDER BY DESCR_PROVEN,DESCR_PRODUTO,DESCR_MICRORG, DESCR_ANTIBIO,RES_SENSIB,NOME_UTE  "

    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    
    Dim StrProven As String
    For i = 0 To ListaProven.ListCount - 1
        StrProven = StrProven & ListaProven.List(i) & "; "
    Next i
    If StrProven <> "" Then
        StrProven = Mid(StrProven, 1, Len(StrProven) - 2)
        If Len(StrProven) > 200 Then
            StrProven = left(StrProven, 200) & "..."
        End If
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & StrProven)
    Else
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    End If
    
    Dim STRProduto As String
    For i = 0 To ListaProduto.ListCount - 1
        STRProduto = STRProduto & ListaProduto.List(i) & "; "
    Next i
    If STRProduto <> "" Then
        STRProduto = Mid(STRProduto, 1, Len(STRProduto) - 2)
        If Len(STRProduto) > 200 Then
            STRProduto = left(STRProduto, 200) & "..."
        End If
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("" & STRProduto)
    Else
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("Todos")
    End If
    
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("-")
    Else
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    End If
    If CbSituacao.ListIndex = -1 Then
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.Text)
    End If
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
    End If
    
    If Flg_DescrAntibio.value = 1 Then
        Report.formulas(8) = "DescrAntibio=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(8) = "DescrAntibio=" & BL_TrataStringParaBD("N")
    End If

    Dim StrMicrorg As String
    For i = 0 To ListaMicrorg.ListCount - 1
        StrMicrorg = StrMicrorg & ListaMicrorg.List(i) & "; "
    Next i
    If StrMicrorg <> "" Then
        StrMicrorg = Mid(StrMicrorg, 1, Len(StrMicrorg) - 2)
        If Len(StrMicrorg) > 200 Then
            StrMicrorg = left(StrMicrorg, 200) & "..."
        End If
        Report.formulas(9) = "Microrganismo=" & BL_TrataStringParaBD("" & StrMicrorg)
    Else
        Report.formulas(9) = "Microrganismo=" & BL_TrataStringParaBD("Todos")
    End If
    
    If Flg_OrderProd.value = 1 Then
        Report.formulas(10) = "OrderProd=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(10) = "OrderProd=" & BL_TrataStringParaBD("N")
    End If
    If Flg_OrderProven.value = 1 Then
        Report.formulas(11) = "OrderProven=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(11) = "OrderProven=" & BL_TrataStringParaBD("N")
    End If
    If Flg_ApresGrafico.value = 1 Then
        Report.formulas(12) = "ApresGrafico=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(12) = "ApresGrafico=" & BL_TrataStringParaBD("N")
    End If
    If Flg_DescrRequis.value = 1 Then
        Report.formulas(13) = "DescrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(13) = "DescrRequis=" & BL_TrataStringParaBD("N")
    End If
    Report.formulas(14) = "TotalMicro=" & BL_TrataStringParaBD("" & totalMicro)
    'Me.SetFocus
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Call BL_RemoveTabela("SL_CR_ESTATMICRORG" & gNumeroSessao)
    
End Sub

Sub Cria_TmpRec_Estatistica()

    Dim TmpRec(9) As DefTable
    
    TmpRec(1).NomeCampo = "DESCR_MICRORG"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 60
    
    TmpRec(2).NomeCampo = "DESCR_ANTIBIO"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 60
    
    TmpRec(3).NomeCampo = "RES_SENSIB"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 1
    
    TmpRec(4).NomeCampo = "N_REQ"
    TmpRec(4).tipo = "INTEGER"
    TmpRec(4).tamanho = 9
    
    TmpRec(5).NomeCampo = "SEQ_REALIZA"
    TmpRec(5).tipo = "INTEGER"
    TmpRec(5).tamanho = 20
    
    TmpRec(6).NomeCampo = "N_RES"
    TmpRec(6).tipo = "INTEGER"
    TmpRec(6).tamanho = 1
    
    TmpRec(7).NomeCampo = "DESCR_PROVEN"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 40
    
    TmpRec(8).NomeCampo = "DESCR_PRODUTO"
    TmpRec(8).tipo = "STRING"
    TmpRec(8).tamanho = 40
    
    TmpRec(9).NomeCampo = "NOME_UTE"
    TmpRec(9).tipo = "STRING"
    TmpRec(9).tamanho = 80
    
    Call BL_CriaTabela("SL_CR_ESTATMICRORG" & gNumeroSessao, TmpRec)
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtCallGesDoc_Click()

End Sub

Private Sub BtImp_Click()

End Sub

Private Sub BtPesquisaAntibio_Click()
    FormPesquisaRapida.InitPesquisaRapida "sl_antibio", _
                        "descr_antibio", "seq_antibio", _
                         EcPesqRapAntibio
End Sub

'Private Sub BtPesquisaEntFin_Click()
'
'    Dim ChavesPesq(1 To 2) As String
'    Dim CampoPesquisa As String
'    Dim CamposEcran(1 To 2) As String
'    Dim CWhere As String
'    Dim CFrom As String
'    Dim CamposRetorno As New ClassPesqResultados
'    Dim Tamanhos(1 To 2) As Long
'    Dim Headers(1 To 2) As String
'    Dim CancelouPesquisa As Boolean
'    Dim Resultados(1 To 2)  As Variant
'    Dim PesqRapida As Boolean
'
'    PesqRapida = False
'
'    ChavesPesq(1) = "descr_efr"
'    CamposEcran(1) = "descr_efr"
'    Tamanhos(1) = 4000
'    Headers(1) = "Descri��o"
'
'    ChavesPesq(2) = "cod_efr"
'    CamposEcran(2) = "cod_efr"
'    Tamanhos(2) = 1000
'    Headers(2) = "C�digo"
'
'    CamposRetorno.InicializaResultados 2
'
'    CFrom = "sl_efr"
'    CampoPesquisa = "descr_efr"
'
'    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
'            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Entidades Finaceiras")
'
'    If PesqRapida = True Then
'        FormPesqRapidaAvancada.Show vbModal
'        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
'        If Not CancelouPesquisa Then
'            EcCodEFR.Text = Resultados(2)
'            EcDescrEFR.Text = Resultados(1)
'            BtPesquisaEntFin.SetFocus
'        End If
'    Else
'        BG_Mensagem mediMsgBox, "N�o existem entidades financeiras", vbExclamation, "EFR"
'        EcCodEFR.SetFocus
'    End If
'
'End Sub

Private Sub BtPesquisaMicro_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_microrg", _
'                        "descr_microrg", "seq_microrg", _
'                         EcPesqRapMicro

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_microrg"
    CamposEcran(1) = "cod_microrg"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_microrg"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_microrg"
    CWhere = ""
    CampoPesquisa = "descr_microrg"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_microrg ", _
                                                                           " Microrganismos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaMicrorg.ListCount = 0 Then
                ListaMicrorg.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", resultados(i))
                ListaMicrorg.ItemData(0) = resultados(i)
            Else
                ListaMicrorg.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", resultados(i))
                ListaMicrorg.ItemData(ListaMicrorg.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaProduto_click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto "
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produto")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProduto.ListCount = 0 Then
                ListaProduto.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProduto.ItemData(0) = resultados(i)
            Else
                ListaProduto.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProduto.ItemData(ListaProduto.NewIndex) = resultados(i)
            End If
        Next i
        

    End If
    



End Sub

Private Sub BtPesquisaProveniencia_Click()
'
'    'Campos do RecordSet
'    Dim ChavesPesq(1 To 2) As String
'
'    'Campo do Crit�rio na Query do RecordSet
'    Dim CampoPesquisa1 As String
'
'    'Campos do RecordSet a Visualizar no ecran
'    Dim CamposEcran(1 To 2) As String
'
'    'Contru��o da Query
'    Dim ClausulaWhere As String
'    Dim ClausulaFrom As String
'
'    'Classe
'    Dim CamposRetorno As New ClassPesqResultados
'
'    'Tamanho dos campos Ecran da List
'    Dim Tamanhos(1 To 2) As Long
'
'    'Cabe�alhos
'    Dim Headers(1 To 2) As String
'
'    'Mensagem do resultado da pesquisa
'    Dim Mensagem As String
'
'    Dim PesqRapida As Boolean
'    Dim CancelouPesquisa As Boolean
'
'    Dim Resultados(1 To 2) As Variant
'
'    'Defini��o dos campos a retornar
'    ChavesPesq(1) = "cod_proven"
'    CamposEcran(1) = "cod_proven"
'    Tamanhos(1) = 2000
'
'    ChavesPesq(2) = "descr_proven"
'    CamposEcran(2) = "descr_proven"
'    Tamanhos(2) = 3000
'
'    'Cabe�alhos
'    Headers(1) = "C�digo"
'    Headers(2) = "Descri��o"
'
'    'N� de Campos a pesquisar na Classe=>Resultaods
'    CamposRetorno.InicializaResultados 2
'
'    'Query
'    ClausulaFrom = "sl_proven"
'    CampoPesquisa1 = "descr_proven"
'
'    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
'
'    Mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
'
'    If PesqRapida = True Then
'        FormPesqRapidaAvancada.Show vbModal
'        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
'        If Not CancelouPesquisa Then
'            EcCodProveniencia.Text = Resultados(1)
'            EcDescrProveniencia.Text = Resultados(2)
'        End If
'    Else
'        BG_Mensagem mediMsgBox, Mensagem, vbExclamation, "..."
'    End If

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProven.ListCount = 0 Then
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(0) = resultados(i)
            Else
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(ListaProven.NewIndex) = resultados(i)
            End If
        Next i
        

    End If
        
End Sub

Private Sub BtReportLista_Click()
    Call Preenche_Estatistica("Lista")
End Sub

Private Sub BtReportTabela_Click()
    Call Preenche_Estatistica("Tabela")
End Sub

Private Sub CbSensib_Click()

    FGAntibio.Text = CbSensib.Text

End Sub

Private Sub CbSensib_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbSensib_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbSensib.ListIndex = -1
    
End Sub

Private Sub CbSituacao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbSituacao.ListIndex = -1
    
End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub



Private Sub EcCodMicro_Validate(Cancel As Boolean)
    Dim RsDescrMicrorg As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodMicro.Text) <> "" Then
        Set RsDescrMicrorg = New ADODB.recordset
        
        With RsDescrMicrorg
            .Source = "SELECT cod_microrg, descr_microrg FROM sl_microrg WHERE upper(cod_microrg)= " & BL_TrataStringParaBD(UCase(EcCodMicro.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrMicrorg.RecordCount > 0 Then
            EcCodMicro.Text = "" & RsDescrMicrorg!cod_microrg
            EcDescrMicro.Text = "" & RsDescrMicrorg!descr_microrg
        Else
            Cancel = True
            EcCodMicro.Text = ""
            EcDescrMicro.Text = ""
            BG_Mensagem mediMsgBox, "O microorganismo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrMicrorg.Close
        Set RsDescrMicrorg = Nothing
    Else
        EcDescrMicro.Text = ""
    End If

End Sub

Private Sub EcCodProduto_Validate(Cancel As Boolean)
    Dim RsDescrProd As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProduto.Text) <> "" Then
        Set RsDescrProd = New ADODB.recordset
        
        With RsDescrProd
            .Source = "SELECT cod_produto, descr_produto FROM sl_produto WHERE upper(cod_produto)= " & BL_TrataStringParaBD(UCase(EcCodProduto.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProd.RecordCount > 0 Then
            EcCodProduto.Text = "" & RsDescrProd!cod_produto
            EcDescrProduto.Text = "" & RsDescrProd!descr_produto
        Else
            Cancel = True
            EcDescrProduto.Text = ""
            BG_Mensagem mediMsgBox, "O Produto indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProd.Close
        Set RsDescrProd = Nothing
    Else
        EcDescrProduto.Text = ""
    End If

End Sub

Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

'Private Sub EcCodEFR_Validate(Cancel As Boolean)
'
'    Dim RsDescrEFR As ADODB.recordset
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
'    If Cancel = True Then
'        Exit Sub
'    End If
'
'    If Trim(EcCodEFR.Text) <> "" Then
'        Set RsDescrEFR = New ADODB.recordset
'
'        With RsDescrEFR
'            .Source = "SELECT descr_efr FROM sl_efr WHERE cod_efr= " & BL_TrataStringParaBD(EcCodEFR.Text)
'            .CursorLocation = adUseServer
'            .CursorType = adOpenStatic
'            .ActiveConnection = gConexao
'            .Open
'        End With
'
'        If RsDescrEFR.RecordCount > 0 Then
'            EcDescrEFR.Text = "" & RsDescrEFR!descr_efr
'        Else
'            Cancel = True
'            EcDescrEFR.Text = ""
'            BG_Mensagem mediMsgBox, "Entidade Financeira inexistente!", vbOKOnly + vbExclamation, App.ProductName
'            SendKeys ("{HOME}+{END}")
'        End If
'        RsDescrEFR.Close
'        Set RsDescrEFR = Nothing
'    Else
'        EcDescrEFR.Text = ""
'    End If
'
'End Sub
      
Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.Text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.Text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Private Sub EcDescrSexo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDescrSexo_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then EcDescrSexo.ListIndex = -1
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcPesqRapAntibio_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapAntibio.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT seq_antibio, descr_antibio FROM sl_antibio WHERE seq_antibio = " & EcPesqRapAntibio, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar o antibi�tico!", vbExclamation, "Pesquisa r�pida"
        Else
            If ListaAntibio.ListCount = 0 Then
                ListaAntibio.AddItem rsCodigo!descr_antibio
                ListaAntibio.ItemData(0) = rsCodigo!seq_antibio
            Else
                ListaAntibio.AddItem rsCodigo!descr_antibio
                ListaAntibio.ItemData(ListaAntibio.NewIndex) = rsCodigo!seq_antibio
            End If
        End If
    End If
End Sub

Private Sub EcPesqRapMicro_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapMicro.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_microrg,descr_microrg FROM sl_microrg WHERE seq_microrg = " & EcPesqRapMicro, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do microrganismo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodMicro.Text = BL_HandleNull(rsCodigo!cod_microrg, "")
            EcDescrMicro.Text = BL_HandleNull(rsCodigo!descr_microrg, "")
        End If
    End If
End Sub

Private Sub EcPesqRapProduto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapProduto.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_produto,descr_produto FROM sl_produto WHERE seq_produto = " & EcPesqRapProduto, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do produto!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodProduto.Text = BL_HandleNull(rsCodigo!cod_produto, "")
            EcDescrProduto.Text = BL_HandleNull(rsCodigo!descr_produto, "")
        End If
    End If

End Sub

Private Sub FGAntibio_Click()
  CbSensib.ListIndex = -1
  
  If Not FGAntibio.row = 0 And FGAntibio.Col = 1 Then
    CbSensib.Visible = True
    CbSensib.top = FGAntibio.top + FGAntibio.CellTop
    CbSensib.left = FGAntibio.left + FGAntibio.CellLeft
    CbSensib.Width = FGAntibio.CellWidth
    If FGAntibio.Text <> "" Then
        CbSensib.Text = FGAntibio.Text
    End If
    CbSensib.SetFocus
  End If
End Sub

Private Sub FGAntibio_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And FGAntibio.row = FGAntibio.rows - 1 And FGAntibio.TextMatrix(FGAntibio.row, 0) <> "" And FGAntibio.TextMatrix(FGAntibio.row, 1) <> "" Then
        FGAntibio.rows = FGAntibio.rows + 1
    End If
End Sub

Private Sub FGAntibio_RowColChange()
    CbSensib.Visible = False
End Sub

Private Sub Flg_DescrAntibio_Click()
    If Flg_DescrAntibio.value = 1 Then
        Flg_ApresGrafico.Enabled = True
    Else
        Flg_ApresGrafico.value = 0
        Flg_ApresGrafico.Enabled = False
    End If
End Sub

Private Sub Flg_DescrRequis_Click()
    If Flg_DescrRequis.value = 1 Then
'        CbSensib.ListIndex = mediComboValorNull
'        ListaAntibio.Clear
'        CbSensib.Enabled = False
'        ListaAntibio.Enabled = False
'        BtPesquisaAntibio.Enabled = False
'        LaAntibio.Enabled = False
'        LaSensib.Enabled = False
        Flg_DescrAntibio.value = 0
        Flg_DescrAntibio.Enabled = False
    Else
        CbSensib.Enabled = True
        ListaAntibio.Enabled = True
        BtPesquisaAntibio.Enabled = True
        LaAntibio.Enabled = True
        LaSensib.Enabled = True
        Flg_DescrAntibio.Enabled = True
        Flg_DescrAntibio.Enabled = True
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Microrganismos"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7725
    Me.Height = 8085 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodProveniencia
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
'    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
'    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Microrganismos")
    
    Set FormEstatMicrorg = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    'CbAnalises.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    EcDescrSexo.ListIndex = mediComboValorNull
    'CbGrupo.ListIndex = mediComboValorNull
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    EcCodProduto.Text = ""
    EcDescrProduto.Text = ""
    EcCodMicro.Text = ""
    EcDescrMicro.Text = ""
'    EcCodEFR.Text = ""
'    EcDescrEFR.Text = ""
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    Flg_DescrAntibio.value = 0
    Flg_OrderProd.value = 0
    Flg_OrderProven.value = 0
    Flg_DescrRequis.value = 0
    ListaAntibio.Clear
    CbSensib.ListIndex = mediComboValorNull
    ListaProduto.Clear
    ListaProven.Clear
    ListaMicrorg.Clear
    ListaPerfisS.Clear
    CkAgruparMicrorg.value = vbUnchecked
    
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodProveniencia.MaxLength = 5
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao

    'Preenche Combo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
    With CbSensib
        .AddItem "Resistente"
        .AddItem "Interm�dio"
        .AddItem "Sens�vel"
        .AddItem "+"
        .AddItem "-"
    End With
    
    With FGAntibio
        .Cols = 2
        .rows = 2
        .FixedRows = 1
        .FixedCols = 0
        .ColWidth(0) = 5000
        .ColWidth(1) = 1200
        .TextMatrix(0, 0) = "Antibiotico"
        .TextMatrix(0, 1) = "Sensibilidade"
    End With
    
    PreencheListaPerfis
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica("Lista")
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica("Lista")

End Sub


Function PreencheTabelaTemporaria() As Long
    Dim sql As String
    Dim ListaAntibioticos As String
    Dim i As Integer
    
    sql = "INSERT INTO SL_CR_ESTATMICRORG" & gNumeroSessao & "("
    
    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
        sql = sql & " DESCR_PRODUTO,DESCR_PROVEN,"
    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
        sql = sql & " DESCR_PRODUTO,"
    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
        sql = sql & " DESCR_PROVEN,"
    End If
    
    If Flg_DescrRequis.value = 1 Then
        sql = sql & " N_REQ,SEQ_REALIZA,DESCR_MICRORG, N_RES, NOME_UTE) select distinct "
    Else
        sql = sql & " N_REQ,SEQ_REALIZA,DESCR_MICRORG, N_RES, DESCR_ANTIBIO, RES_SENSIB, NOME_UTE) select distinct "
    End If
    
    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
        sql = sql & " DESCR_PRODUTO,DESCR_PROVEN,"
    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
        sql = sql & " DESCR_PRODUTO,"
    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
        sql = sql & " DESCR_PROVEN,"
    End If
    
    If Flg_DescrRequis.value = 1 Then
        If CkAgruparMicrorg.value = vbChecked Then
            sql = sql & " sl_requis.n_req,sl_res_micro.seq_realiza,descr_microrg 'Microganismos Agrupados',n_res,nome_ute " & _
                  " from sl_requis,sl_realiza, sl_req_prod, sl_res_micro, sl_microrg,sl_identif, sl_produto,sl_proven "
        Else
            sql = sql & " sl_requis.n_req,sl_res_micro.seq_realiza,descr_microrg,n_res,nome_ute " & _
                  " from sl_requis,sl_realiza, sl_req_prod, sl_res_micro, sl_microrg,sl_identif, sl_produto,sl_proven "
        End If
        If ListaPerfisS.ListCount > 0 Then
            sql = sql & " , sl_perfis "
        End If
        sql = sql & " where " & _
              " sl_requis.n_req = sl_realiza.n_req and sl_realiza.seq_realiza = sl_res_micro.seq_realiza and " & _
              " sl_requis.n_req = sl_req_prod.n_req(+) and " & _
              " sl_req_prod.cod_prod = sl_produto.cod_produto(+) and " & _
              " sl_requis.seq_utente = sl_identif.seq_utente and " & _
              " sl_res_micro.cod_micro = sl_microrg.cod_microrg and " & _
              " sl_requis.cod_proven = sl_proven.cod_proven "
        If ListaPerfisS.ListCount > 0 Then
            sql = sql & " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis AND sl_perfis.seq_perfis IN ( "
            For i = 0 To ListaPerfisS.ListCount - 1
                sql = sql & ListaPerfisS.ItemData(i) & ", "
            Next i
            sql = Mid(sql, 1, Len(sql) - 2) & ") "
        End If
    Else
        sql = sql & " sl_requis.n_req,sl_res_micro.seq_realiza,descr_microrg,sl_res_tsq.n_res,descr_antibio,res_sensib,nome_ute " & _
              " from sl_requis,sl_realiza, sl_req_prod, sl_res_micro, sl_microrg, sl_res_tsq, sl_antibio,sl_identif, sl_produto,sl_proven "
        If ListaPerfisS.ListCount > 0 Then
            sql = sql & " , sl_perfis "
        End If
        sql = sql & " where " & _
              " sl_requis.n_req = sl_realiza.n_req and sl_realiza.seq_realiza = sl_res_micro.seq_realiza and " & _
              " sl_requis.n_req = sl_req_prod.n_req(+) and " & _
              " sl_req_prod.cod_prod = sl_produto.cod_produto(+) and " & _
              " sl_requis.seq_utente = sl_identif.seq_utente and " & _
              " sl_res_micro.cod_micro = sl_microrg.cod_microrg and " & _
              " sl_res_tsq.cod_antib = sl_antibio.cod_antibio(+) and " & _
              " sl_res_micro.seq_realiza = sl_res_tsq.seq_realiza(+) and " & _
              " sl_res_micro.n_res = sl_res_tsq.n_res(+) and " & _
              " sl_res_micro.cod_micro = sl_res_tsq.cod_micro(+) and " & _
              " sl_requis.cod_proven = sl_proven.cod_proven "
        If ListaPerfisS.ListCount > 0 Then
            sql = sql & " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis AND sl_perfis.seq_perfis IN ( "
            For i = 0 To ListaPerfisS.ListCount - 1
                sql = sql & ListaPerfisS.ItemData(i) & ", "
            Next i
            sql = Mid(sql, 1, Len(sql) - 2) & ") "
        End If
    End If

    'verifica os campos preenchidos
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        sql = sql & " AND sl_identif.sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'Data preenchida
    sql = sql & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    
    'Situa��o preenchida?
    If (CbSituacao.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    Dim StrProven As String
    StrProven = ""
    For i = 0 To ListaProven.ListCount - 1
        StrProven = StrProven & BL_TrataStringParaBD(BL_SelCodigo("sl_proven", "cod_proven", "seq_proven", ListaProven.ItemData(i))) & ","
    Next i
    If StrProven <> "" Then
        'Remove a virgula do final da express�o
        StrProven = Mid(StrProven, 1, Len(StrProven) - 1)
        sql = sql & " AND sl_requis.cod_proven in ( " & StrProven & " )"
    End If
    
    'C�digo do produto preenchido?
    Dim STRProduto As String
    STRProduto = ""
    For i = 0 To ListaProduto.ListCount - 1
        STRProduto = STRProduto & BL_TrataStringParaBD(BL_SelCodigo("sl_produto", "cod_produto", "seq_produto", ListaProduto.ItemData(i))) & ","
    Next i
    If STRProduto <> "" Then
        'Remove a virgula do final da express�o
        STRProduto = Mid(STRProduto, 1, Len(STRProduto) - 1)
        sql = sql & " AND sl_req_prod.cod_prod in ( " & STRProduto & " )"
    End If
    
    'C�digo do microrganismo preenchido?
    Dim StrMicrorg As String
    StrMicrorg = ""
    For i = 0 To ListaMicrorg.ListCount - 1
        StrMicrorg = StrMicrorg & BL_TrataStringParaBD(BL_SelCodigo("sl_microrg", "cod_microrg", "seq_microrg", ListaMicrorg.ItemData(i))) & ","
    Next i
    If StrMicrorg <> "" Then
        'Remove a virgula do final da express�o
        StrMicrorg = Mid(StrMicrorg, 1, Len(StrMicrorg) - 1)
        sql = sql & " AND sl_res_micro.cod_micro in ( " & StrMicrorg & " )"
    End If
    
    
    'If Flg_DescrRequis.Value = 0 Then
        'Sensibilidade preenchida?
        If CbSensib.ListIndex <> mediComboValorNull Or ListaAntibio.ListCount > 0 Then
            sql = sql & " AND sl_res_micro.seq_realiza in ( " & _
                    " select distinct seq_realiza from sl_res_tsq, sl_antibio where " & _
                    " sl_res_tsq.cod_antib = sl_antibio.cod_antibio(+) "
                    
            If CbSensib.ListIndex <> mediComboValorNull Then
                sql = sql & " AND sl_res_tsq.res_sensib = " & BL_TrataStringParaBD(UCase(Mid(CbSensib.Text, 1, 1)))
            End If
    
            If ListaAntibio.ListCount > 0 Then
                'LISTA DE ANTIBIOTICOS:
    
                'Forma a Lista de antibioticos seleccionados para o crit�rio do sql
                ListaAntibioticos = ""
                For i = 0 To ListaAntibio.ListCount - 1
                    ListaAntibioticos = ListaAntibioticos & ListaAntibio.ItemData(i) & ","
                Next i
                'Remove a virgula do final da express�o
                ListaAntibioticos = Mid(ListaAntibioticos, 1, Len(ListaAntibioticos) - 1)
    
                sql = sql & " AND sl_antibio.seq_antibio in (" & ListaAntibioticos & ")"
            End If
            
            sql = sql & " )"
        End If
    'End If
    
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
    Dim RsTotalMicro As ADODB.recordset
    Set RsTotalMicro = New ADODB.recordset
    RsTotalMicro.CursorLocation = adUseServer
    RsTotalMicro.CursorType = adOpenStatic
    RsTotalMicro.ActiveConnection = gConexao
    RsTotalMicro.Source = "select distinct descr_microrg, n_req from sl_cr_estatmicrorg" & gNumeroSessao
    RsTotalMicro.Open
    PreencheTabelaTemporaria = RsTotalMicro.RecordCount
    
End Function

Private Sub ListaAntibio_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaAntibio.ListCount > 0 Then     'Delete
        ListaAntibio.RemoveItem (ListaAntibio.ListIndex)
    End If
End Sub

Private Sub ListaMicrorg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaMicrorg.ListCount > 0 Then     'Delete
        ListaMicrorg.RemoveItem (ListaMicrorg.ListIndex)
    End If
End Sub

Private Sub ListaProduto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProduto.ListCount > 0 Then     'Delete
        ListaProduto.RemoveItem (ListaProduto.ListIndex)
    End If
End Sub

Private Sub ListaProven_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProven.ListCount > 0 Then     'Delete
        ListaProven.RemoveItem (ListaProven.ListIndex)
    End If
End Sub
Private Sub BtInsere_Click()

    Dim i As Integer
            
    Set ListaOrigem = ListaPerfis
    Set ListaDestino = ListaPerfisS
       
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i
              
End Sub

Private Sub BtRetira_Click()

    Set ListaDestino = ListaPerfisS
    If ListaDestino.ListIndex <> -1 Then
        ListaDestino.RemoveItem (ListaDestino.ListIndex)
    End If

End Sub

Private Sub PreencheListaPerfis()
    'preenche ListaPerfis
    Dim i As Integer
    Dim k As Integer
    Dim TSQLQueryAux As String
    Dim TCampoChave As String
    Dim TCampoPesquisa As String
    Dim TCamposSel As String
    Dim TNomeTabela As String
    Dim TClausulaWhereAuxiliar As String
    Dim TTabelaAux As ADODB.recordset
    Dim NomeControl As Control
    
    Set NomeControl = ListaPerfis
    TCampoChave = "seq_perfis"
    TCamposSel = "cod_perfis, descr_perfis"
    TCampoPesquisa = "descr_perfis"
    TNomeTabela = "sl_perfis"
    
    TSQLQueryAux = "SELECT " & TCampoChave & " ," & (TCamposSel) & " FROM " & TNomeTabela & _
                    " , sl_gr_ana where sl_perfis.gr_ana = sl_gr_ana.cod_gr_ana and " & _
                    " sl_gr_ana.descr_gr_ana = 'MICROBIOLOGIA' " & _
                    " ORDER BY " & TCampoPesquisa
    
    Set TTabelaAux = New ADODB.recordset
    TTabelaAux.CursorLocation = adUseServer
    TTabelaAux.CursorType = adOpenStatic
    TTabelaAux.Open TSQLQueryAux, gConexao
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    
    i = 0
    Do Until TTabelaAux.EOF
        k = InStr(1, TCamposSel, ",")
        NomeControl.AddItem Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1)))) & Space(10 - Len(Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1))))) + 1) & Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))) & Space(60 - Len(Mid(Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))), 1, 40)) + 1)
        NomeControl.ItemData(i) = CInt(TTabelaAux(TCampoChave))
        TTabelaAux.MoveNext
        i = i + 1
    Loop
    TTabelaAux.Close
    Set TTabelaAux = Nothing
    
Trata_Erro:
End Sub

Private Sub ListaPerfis_Click()
    ListaPerfis.ToolTipText = Trim(ListaPerfis.List(ListaPerfis.ListIndex))
End Sub

Private Sub ListaPerfisS_Click()
    ListaPerfisS.ToolTipText = Trim(ListaPerfisS.List(ListaPerfisS.ListIndex))
End Sub

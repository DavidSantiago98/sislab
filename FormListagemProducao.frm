VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormListagemProducao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   6180
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7560
   Icon            =   "FormListagemProducao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6180
   ScaleWidth      =   7560
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameDatas 
      Height          =   1095
      Left            =   2160
      TabIndex        =   23
      Top             =   4920
      Width           =   3375
      Begin VB.OptionButton OptData 
         Caption         =   "Data Cria��o da requisi��o"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   26
         Top             =   240
         Width           =   3135
      End
      Begin VB.OptionButton OptData 
         Caption         =   "Data da Opera��o"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   25
         Top             =   495
         Width           =   2415
      End
      Begin VB.OptionButton OptData 
         Caption         =   "Data Chegada da Requisi��o"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   24
         Top             =   720
         Width           =   2895
      End
   End
   Begin VB.CheckBox CkExcluiAnalisesRetiradas 
      Caption         =   "Excluir An�lises Retiradas"
      Height          =   255
      Left            =   2880
      TabIndex        =   22
      Top             =   4440
      Width           =   2535
   End
   Begin VB.CheckBox CkExcluiAcrescentadas 
      Caption         =   "Excluir An�lises Acrescentadas"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   4440
      Width           =   2535
   End
   Begin VB.Frame Frame4 
      Height          =   615
      Left            =   0
      TabIndex        =   16
      Top             =   3720
      Width           =   6735
      Begin VB.OptionButton OptColunas 
         Caption         =   "Detalhe"
         Height          =   255
         Index           =   3
         Left            =   5400
         TabIndex        =   20
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton OptColunas 
         Caption         =   "Agrupar por Ano"
         Height          =   255
         Index           =   2
         Left            =   3720
         TabIndex        =   19
         Top             =   240
         Width           =   1455
      End
      Begin VB.OptionButton OptColunas 
         Caption         =   "Agrupar por M�s"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   18
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton OptColunas 
         Caption         =   "Agrupar por Dia"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame Frame3 
      Height          =   615
      Left            =   0
      TabIndex        =   13
      Top             =   3120
      Width           =   6735
      Begin VB.OptionButton OptLinhas 
         Caption         =   "Agrupar por Postos"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   15
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton OptLinhas 
         Caption         =   "Agrupar por EFR"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   6735
      Begin VB.CommandButton BtDatas 
         Height          =   375
         Left            =   6000
         Picture         =   "FormListagemProducao.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Datas"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton BtReportLista 
         Height          =   480
         Left            =   6000
         Picture         =   "FormListagemProducao.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Gerar Listagem"
         Top             =   120
         Width           =   495
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   300
         Left            =   1200
         TabIndex        =   9
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   34734081
         CurrentDate     =   39300
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   300
         Left            =   3720
         TabIndex        =   10
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   34734081
         CurrentDate     =   39300
      End
      Begin VB.Label Label1 
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Data Final"
         Height          =   255
         Index           =   1
         Left            =   2760
         TabIndex        =   11
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   2175
      Left            =   0
      TabIndex        =   0
      Top             =   960
      Width           =   6735
      Begin VB.CommandButton BtPesquisaEntidades 
         Height          =   315
         Left            =   6120
         Picture         =   "FormListagemProducao.frx":04E0
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   360
         Width           =   375
      End
      Begin VB.ListBox ListaEntidades 
         Height          =   645
         Left            =   1320
         MultiSelect     =   2  'Extended
         TabIndex        =   3
         Top             =   360
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaPostos 
         Height          =   315
         Left            =   6120
         Picture         =   "FormListagemProducao.frx":0A6A
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   1200
         Width           =   375
      End
      Begin VB.ListBox ListaPostos 
         Height          =   645
         Left            =   1320
         MultiSelect     =   2  'Extended
         TabIndex        =   1
         Top             =   1200
         Width           =   4800
      End
      Begin VB.Label Label1 
         Caption         =   "Entidades"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Postos"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   1215
      End
   End
End
Attribute VB_Name = "FormListagemProducao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim ListaOrigem As Control
Dim ListaDestino As Control
Public CamposBDparaListBoxF
Dim NumEspacos
Public RSListaDestino As ADODB.recordset
Public RSListaOrigem As ADODB.recordset




Sub Preenche_Estatistica()
    Dim sql As String
    Dim continua As Boolean
    Dim i As Integer
    Dim TotalRegistos As Long
    Dim nomeReport As String
    
    
    sql = "DELETE FROM    sl_cr_Estat_fact_det WHERE num_sessao =  " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Listagem de Produ��o") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    If OptColunas(0).Value = True Then
        If OptLinhas(0).Value = True Then
            nomeReport = "ListagemProducao_efr_dia"
        ElseIf OptLinhas(1).Value = True Then
            nomeReport = "ListagemProducao_posto_dia"
        End If
    ElseIf OptColunas(1).Value = True Then
        If OptLinhas(0).Value = True Then
            nomeReport = "ListagemProducao_efr_mes"
        ElseIf OptLinhas(1).Value = True Then
            nomeReport = "ListagemProducao_posto_mes"
        End If
    ElseIf OptColunas(2).Value = True Then
        If OptLinhas(0).Value = True Then
            nomeReport = "ListagemProducao_efr_ano"
        ElseIf OptLinhas(1).Value = True Then
            nomeReport = "ListagemProducao_posto_ano"
        End If
    ElseIf OptColunas(3).Value = True Then
        If OptLinhas(0).Value = True Then
            nomeReport = "ListagemProducao_det_efr"
        ElseIf OptLinhas(1).Value = True Then
            nomeReport = "ListagemProducao_posto_efr"
        End If
    End If
    
    'Printer Common Dialog
    'gImprimirDestino = 0
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Listagem de Produ��o", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Listagem de Produ��o", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    
    PreencheTabelaCrystal
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.WindowState = crptMaximized
    Report.SQLQuery = "SELECT * "
    Report.SQLQuery = Report.SQLQuery & " From sl_cr_Estat_fact_det   "
    Report.SQLQuery = Report.SQLQuery & " WHERE num_sessao = " & gNumeroSessao
    If OptLinhas(0).Value = True Then
        Report.SQLQuery = Report.SQLQuery & "  ORDER BY cod_efr ASC "
    ElseIf OptLinhas(1).Value = True Then
        Report.SQLQuery = Report.SQLQuery & "  ORDER BY cod_sala ASC "
    End If
    If OptColunas(3).Value = True Then
        Report.SQLQuery = Report.SQLQuery & ", n_req ASC"
    Else
        Report.SQLQuery = Report.SQLQuery & ", dt_chega ASC"
    End If
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.Value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Value)
    
    ' ---------------------------------------------------------------------------------
    ' ENTIDADES
    ' ---------------------------------------------------------------------------------
    Dim StrEntidades As String
    For i = 0 To ListaEntidades.ListCount - 1
        StrEntidades = StrEntidades & ListaEntidades.List(i) & "; "
    Next i
    If StrEntidades <> "" Then
        StrEntidades = Mid(StrEntidades, 1, Len(StrEntidades) - 2)
        If Len(StrEntidades) > 200 Then
            StrEntidades = Left(StrEntidades, 200) & "..."
        End If
        Report.formulas(3) = "Entidades=" & BL_TrataStringParaBD("" & StrEntidades)
    Else
        Report.formulas(3) = "Entidades=" & BL_TrataStringParaBD("Todas")
    End If

    ' ---------------------------------------------------------------------------------
    ' POSTOS
    ' ---------------------------------------------------------------------------------
    Dim StrPostos As String
    For i = 0 To ListaPostos.ListCount - 1
        StrPostos = StrPostos & ListaPostos.List(i) & "; "
    Next i
    If StrPostos <> "" Then
        StrPostos = Mid(StrPostos, 1, Len(StrPostos) - 2)
        If Len(StrPostos) > 200 Then
            StrPostos = Left(StrPostos, 200) & "..."
        End If
        Report.formulas(4) = "Postos=" & BL_TrataStringParaBD("" & StrPostos)
    Else
        Report.formulas(4) = "Postos=" & BL_TrataStringParaBD("Todos")
    End If

    Call BL_ExecutaReport
 
    sql = "DELETE FROM    sl_cr_Estat_fact_det WHERE num_sessao =  " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
End Sub



Private Sub BtDatas_Click()
    If FrameDatas.Visible Then
        FrameDatas.Visible = False
    Else
        FrameDatas.Visible = True
        FrameDatas.Top = 1080
        FrameDatas.Left = 2760
    End If
End Sub
Private Sub BtPesquisaentidades_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_efr "
    CWhere = ""
    CampoPesquisa = "descr_efr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_efr ", _
                                                                           "Entidades")
    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaEntidades.ListCount = 0 Then
                ListaEntidades.AddItem BL_SelCodigo("sl_efr", "descr_efr", "seq_efr", resultados(i))
                ListaEntidades.ItemData(0) = resultados(i)
            Else
                ListaEntidades.AddItem BL_SelCodigo("sl_efr", "descr_efr", "seq_efr", resultados(i))
                ListaEntidades.ItemData(ListaEntidades.NewIndex) = resultados(i)
            End If
        Next i
        

    End If

End Sub


Private Sub BtPesquisaPostos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CWhere = ""
    CampoPesquisa = "descr_sala"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_sala ", _
                                                                           " Postos Colheita")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPostos.ListCount = 0 Then
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(0) = resultados(i)
            Else
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(ListaPostos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub




Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Listagem sobre a Produ��o"
    
    Me.Left = 5
    Me.Top = 5
    Me.Width = 7065
    Me.Height = 5325 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    If Not RSListaOrigem Is Nothing Then
        RSListaOrigem.Close
        Set RSListaOrigem = Nothing
    End If
    
    Call BL_FechaPreview("Listagem de Produ��o")
    
    Set FormListagemProducao = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    OptColunas(0).Value = True
    OptLinhas(0).Value = True
    ListaEntidades.Clear
    ListaPostos.Clear
    CkExcluiAcrescentadas.Value = vbUnchecked
    CkExcluiAnalisesRetiradas.Value = vbUnchecked
'    CkAgrupEntidade.Value = vbUnchecked
'    CkAgrupOperador.Value = vbUnchecked
'    CkAgrupPosto.Value = vbUnchecked
'    CkDescrAnalises.Value = vbUnchecked
'    CkAgruparData.Value = vbUnchecked
End Sub

Sub DefTipoCampos()
    EcDtInicio.Value = Bg_DaData_ADO
    EcDtFim.Value = Bg_DaData_ADO
    OptColunas(0).Value = True
    OptLinhas(0).Value = True
    CkExcluiAcrescentadas.Value = vbUnchecked
    CkExcluiAnalisesRetiradas.Value = vbUnchecked
    FrameDatas.Visible = False
    OptData(1).Value = True
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    If BL_HandleNull(gCodSalaAssocUser, "-1") <> "-1" And gCodSalaAssocUser <> 0 Then
        ListaPostos.AddItem BL_SelCodigo("SL_COD_SALAS", "DESCR_SALA", "COD_SALA", gCodSalaAssocUser)
        ListaPostos.ItemData(ListaPostos.NewIndex) = gCodSalaAssocUser
        ListaPostos.Enabled = False
        BtPesquisaPostos.Enabled = False
    Else
        ListaPostos.Enabled = True
        BtPesquisaPostos.Enabled = True
    End If
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub



Private Sub Listaentidades_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaEntidades.ListCount > 0 Then     'Delete
        ListaEntidades.RemoveItem (ListaEntidades.ListIndex)
    End If
End Sub

Private Sub ListaPostos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPostos.ListCount > 0 Then     'Delete
        ListaPostos.RemoveItem (ListaPostos.ListIndex)
    End If
End Sub


Private Sub BtReportLista_Click()
    gImprimirDestino = 0
    Call Preenche_Estatistica
End Sub

Private Sub PreencheTabelaCrystal()
    Dim sSql As String
    Dim i As Integer
    Dim data As String
    
    ' DATA QUE UTILIZADOR ESCOLHEU
    If OptData(0).Value = True Then
        data = "dt_cri "
    ElseIf OptData(1).Value = True Then
        data = "dt_cri_recibo "
    ElseIf OptData(2).Value = True Then
        data = "dt_chega "
    End If
    
    sSql = "INSERT INTO sl_cr_estat_fact_det( num_sessao, n_req, dt_chega, dt_cri, n_rec, serie_rec, cod_efr, descr_efr, cod_sala, descr_sala,"
    sSql = sSql & " nome_ute, dt_nasc_ute, user_emi, dt_pagamento, hr_pagamento, estado, venda_dinheiro, desconto, cod_empresa, cod_ana, p1, taxa_recibos_det,"
    sSql = sSql & "  taxa_unitario_recibos_det, isencao, cor_p1, ordem_marcacao, cod_ana_efr, quantidade, flg_doc_caixa,dt_anul, hr_anul, t_utente, utente, valor_efr_unit,"
    sSql = sSql & "   valor_efr, erro, data, seq_estat_fact, valor_doe_unit, valor_doe , Valor_Taxa, valor_taxa_unit, dt_chega_mes, dt_chega_ano,"
    sSql = sSql & "   saida_mapa, flg_usa_preco_ent,val_efr,val_ute_recebido,val_ute_divida) "
    sSql = sSql & " SELECT " & gNumeroSessao & " num_sessao, n_req, dt_chega, dt_cri, n_rec, serie_rec, cod_efr, descr_efr, cod_sala, descr_sala,"
    sSql = sSql & " nome_ute, dt_nasc_ute, user_emi, dt_pagamento, hr_pagamento, estado, venda_dinheiro, desconto, cod_empresa, cod_ana, p1, taxa_recibos_det,"
    sSql = sSql & "  taxa_unitario_recibos_det, isencao, cor_p1, ordem_marcacao, cod_ana_efr, quantidade, flg_doc_caixa,dt_anul, hr_anul, t_utente, utente, valor_efr_unit,"
    sSql = sSql & "   valor_efr, erro, data, seq_estat_fact, valor_doe_unit, valor_doe , Valor_Taxa, valor_taxa_unit, dt_chega_mes, dt_chega_ano, "
    sSql = sSql & "   saida_mapa, flg_usa_preco_ent,val_efr,val_ute_recebido,val_ute_divida "
    sSql = sSql & " FROM sl_estat_fact_det WHERE " & data & " BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Value) & "  AND " & BL_TrataDataParaBD(EcDtFim.Value)
    If ListaEntidades.ListCount > 0 Then
        sSql = sSql & " AND cod_efr IN (select x2.cod_efr FROM sl_efr x2 WHERE x2.seq_efr IN ("
        For i = 0 To ListaEntidades.ListCount - 1
            sSql = sSql & ListaEntidades.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & "))"
    End If
    If ListaPostos.ListCount > 0 Then
        sSql = sSql & " AND cod_sala IN (select x2.cod_sala FROM sl_cod_salas x2 WHERE x2.seq_sala IN ("
        For i = 0 To ListaPostos.ListCount - 1
            sSql = sSql & ListaPostos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & "))"
    End If
    If CkExcluiAcrescentadas.Value = vbChecked Then
        sSql = sSql & " AND (flg_adicionado = 0 OR flg_adicionado IS NULL)"
    End If
    If CkExcluiAnalisesRetiradas.Value = vbChecked Then
        sSql = sSql & " AND (flg_retirado = 0 OR flg_retirado IS NULL)"
    End If
        
    BG_ExecutaQuery_ADO sSql
End Sub


VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormAdministracaoProfiles 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Administracao de Profiles"
   ClientHeight    =   6060
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   14880
   Icon            =   "FAdministracaoProfiles.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   14880
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameAccao 
      Height          =   1215
      Left            =   5280
      TabIndex        =   23
      Top             =   1920
      Visible         =   0   'False
      Width           =   2895
      Begin VB.Label LbAccao 
         Height          =   495
         Left            =   120
         TabIndex        =   24
         Top             =   360
         Width           =   2655
      End
   End
   Begin VB.CommandButton BtExpande 
      Caption         =   "�"
      Height          =   255
      Left            =   8640
      TabIndex        =   22
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton BtVisivelObj 
      Caption         =   "Vis�vel"
      Height          =   540
      Left            =   10200
      Picture         =   "FAdministracaoProfiles.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Estado Menu  Vis�vel"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtInvisivelObj 
      Caption         =   "Invis�vel"
      Height          =   540
      Left            =   10920
      Picture         =   "FAdministracaoProfiles.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   20
      ToolTipText     =   "Estado Menu  Invis�vel"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtDefeitoObj 
      Caption         =   "Defeito"
      Height          =   540
      Left            =   11640
      Picture         =   "FAdministracaoProfiles.frx":0B20
      Style           =   1  'Graphical
      TabIndex        =   19
      ToolTipText     =   "Estado Menu  por Defeito"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtExpandeObj 
      Caption         =   "Expandir"
      Height          =   540
      Left            =   12600
      Picture         =   "FAdministracaoProfiles.frx":10AA
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "Expandir Menu"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtEncolheObj 
      Caption         =   "Encolher"
      Height          =   540
      Left            =   13320
      Picture         =   "FAdministracaoProfiles.frx":1634
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Encolher Menu"
      Top             =   5400
      Width           =   735
   End
   Begin MSComctlLib.ImageList ImageListProfileObjectos 
      Left            =   9480
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":1BBE
            Key             =   "Key1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":2158
            Key             =   "Key2"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":26F2
            Key             =   "Key3"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":2C8C
            Key             =   "Key4"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":3226
            Key             =   "KeyForm"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":9A88
            Key             =   "KeyObj"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton BtNovo 
      Caption         =   "Criar"
      Height          =   540
      Left            =   120
      Picture         =   "FAdministracaoProfiles.frx":102EA
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Criar Profile"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtApagar 
      Caption         =   "Apagar"
      Height          =   540
      Left            =   840
      Picture         =   "FAdministracaoProfiles.frx":10874
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Remover Profile"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtRenomear 
      Caption         =   "Renomear"
      Height          =   540
      Left            =   1560
      Picture         =   "FAdministracaoProfiles.frx":10DFE
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Renomear Profile"
      Top             =   5400
      Width           =   855
   End
   Begin VB.CommandButton BtGravar 
      Caption         =   "Guardar"
      Height          =   540
      Left            =   8400
      Picture         =   "FAdministracaoProfiles.frx":11C40
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Guardar Altera��es"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtExpandirProfiles 
      Caption         =   "Expandir"
      Height          =   540
      Left            =   2760
      Picture         =   "FAdministracaoProfiles.frx":121CA
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Expandir Profile"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtEncolherProfiles 
      Caption         =   "Encolher"
      Height          =   540
      Left            =   3480
      Picture         =   "FAdministracaoProfiles.frx":12754
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Encolher Profile"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtEncolherDet 
      Caption         =   "Encolher"
      Height          =   540
      Left            =   7560
      Picture         =   "FAdministracaoProfiles.frx":12CDE
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Encolher Menu"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtExapandirDet 
      Caption         =   "Expandir"
      Height          =   540
      Left            =   6840
      Picture         =   "FAdministracaoProfiles.frx":13268
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Expandir Menu"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtDefeito 
      Caption         =   "Defeito"
      Height          =   540
      Left            =   5880
      Picture         =   "FAdministracaoProfiles.frx":137F2
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Estado Menu  por Defeito"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtInvisivel 
      Caption         =   "Invis�vel"
      Height          =   540
      Left            =   5160
      Picture         =   "FAdministracaoProfiles.frx":13D7C
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Estado Menu  Invis�vel"
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton BtVisivel 
      Caption         =   "Vis�vel"
      Height          =   540
      Left            =   4440
      Picture         =   "FAdministracaoProfiles.frx":14306
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Estado Menu  Vis�vel"
      Top             =   5400
      Width           =   735
   End
   Begin MSComctlLib.ImageList ImageListProfileDetalhes 
      Left            =   4560
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":14890
            Key             =   "Key1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":14E2A
            Key             =   "Key2"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":153C4
            Key             =   "Key3"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":1595E
            Key             =   "Key4"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListProfiles 
      Left            =   3120
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":15EF8
            Key             =   "Key1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FAdministracaoProfiles.frx":16492
            Key             =   "Key2"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeViewProfileDet 
      Height          =   4875
      Left            =   4440
      TabIndex        =   12
      Top             =   480
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   8599
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   354
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeViewProfiles 
      DragIcon        =   "FAdministracaoProfiles.frx":1689C
      Height          =   4875
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   8599
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   354
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
   End
   Begin MSComctlLib.TreeView TreeViewProfileObj 
      Height          =   4875
      Left            =   9360
      TabIndex        =   15
      Top             =   480
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   8599
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   354
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Objectos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9360
      TabIndex        =   16
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label LabelProfileDetalhes 
      Caption         =   "Menus"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4440
      TabIndex        =   14
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label LabelProfiles 
      Caption         =   "Lista de Profiles"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   240
      Width           =   1815
   End
   Begin VB.Menu IDM_GESTAO_PROFILE 
      Caption         =   "Gest�o de Profiles"
      Visible         =   0   'False
      Begin VB.Menu IDM_CRIAR_PROFILE 
         Caption         =   "Criar Profile"
      End
      Begin VB.Menu IDM_APAGAR_PROFILE 
         Caption         =   "Apagar Profile"
      End
      Begin VB.Menu IDM_SEP_0 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_RENOMEAR 
         Caption         =   "Renomear"
      End
      Begin VB.Menu IDM_SEP_1 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_GRAVAR 
         Caption         =   "Guardar Altera��es"
      End
      Begin VB.Menu IDM_SEP_2 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_EXPANDIR_PROFILE 
         Caption         =   "Expandir Profile"
      End
      Begin VB.Menu IDM_RECOLHER_PROFILE 
         Caption         =   "Recolher Profile"
      End
   End
   Begin VB.Menu IDM_DET_PROFILE 
      Caption         =   "Detalhes do Profile"
      Visible         =   0   'False
      Begin VB.Menu IDM_VISIVEL 
         Caption         =   "Estado Vis�vel"
      End
      Begin VB.Menu IDM_INVISIVEL 
         Caption         =   "Estado Invis�vel"
      End
      Begin VB.Menu IDM_DEFEITO 
         Caption         =   "Estado por Defeito"
      End
      Begin VB.Menu IDM_SEP1 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_EXPANDIR_MENU 
         Caption         =   "Expandir Menu"
      End
      Begin VB.Menu IDM_RECOLHER_MENU 
         Caption         =   "Recolher Menu"
      End
      Begin VB.Menu IDM_SEP2 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_REFRESH 
         Caption         =   "Actualizar �rvore Menus"
         Visible         =   0   'False
      End
   End
End
Attribute VB_Name = "FormAdministracaoProfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object


' ---------------------------------------
' ESTRUTURA COM OS MENUS
' ---------------------------------------
Private Type menu
    Codigo As String
    indice As Long
    ordem As Long
    id As String
    id_pai As String
    nome As String
    estado As Long
End Type
Dim totalEstrutMenu As Long
Dim estrutMenu() As menu

' --------------------------------------
' ESTRUTURA COM OS OBJECTOS
' --------------------------------------
Private Type Objectos
    Codigo As String
    cod_form As String
    indice As String
    id As String
    id_pai As String
    tipo As Long
    estado As Long
End Type
Dim totalEstrutObjectos As Long
Dim estrutObjectos() As Objectos

' --------------------------------------
' ESTRUTURA COM OS PROFILES
' --------------------------------------
Private Type profile
    Codigo As String
    ordem As Long
    descricao As String
    codigo_pai As String
    
    totalEstrutMenu As Long
    estrutMenu() As menu
    
    totalEstrutObjectos As Long
    estrutObjectos() As Objectos

End Type
Dim totalEstrutProfile As Long
Dim estrutProfile() As profile

Const lObjForms = 1
Const lObjTipoObjecto = 2
Const lObjObjecto = 6
Const lObjDefeito = 3
Const lObjInvisivel = 4
Const lObjVisivel = 5

Private Sub BtApagar_Click()
    RemoveProfile TreeViewProfiles.SelectedItem.Index
End Sub

Private Sub BtDefeito_Click()
    AlteraEstado gProfileDefeito
End Sub

Private Sub BtDefeitoObj_Click()
    AlteraEstadoObjecto lObjDefeito
End Sub

Private Sub BtEncolheObj_Click()
    BL_expandeNo TreeViewProfileObj.SelectedItem, False
End Sub

Private Sub BtEncolherDet_Click()
    BL_expandeNo TreeViewProfileDet.SelectedItem, False
End Sub

Private Sub BtEncolherProfiles_Click()
  BL_expandeNo TreeViewProfiles.SelectedItem, False
End Sub

Private Sub BtExapandirDet_Click()
    BL_expandeNo TreeViewProfileDet.SelectedItem, True
End Sub

Private Sub BtExpande_Click()
    If BtExpande.caption = "�" Then
        Me.Width = 14340
        BtExpande.caption = "�"
        If totalEstrutObjectos = 0 Then
            FrameAccao.Visible = True
            TreeViewProfileDet.Enabled = False
            TreeViewProfiles.Enabled = False
            TreeViewProfileObj.Enabled = False
            
            InicializaTreeObjectos
            PreencheProfilesObj
            
            FrameAccao.Visible = False
            TreeViewProfileDet.Enabled = True
            TreeViewProfiles.Enabled = True
            TreeViewProfileObj.Enabled = True
        End If
    Else
        Me.Width = 9375
        BtExpande.caption = "�"
    End If
End Sub

Private Sub BtExpandeObj_Click()
    BL_expandeNo TreeViewProfileObj.SelectedItem, True
End Sub

Private Sub BtExpandirProfiles_Click()
    BL_expandeNo TreeViewProfiles.SelectedItem, True
End Sub

Private Sub BtGravar_Click()
    GravaProfilesDet
End Sub

Private Sub BtInvisivel_Click()
    AlteraEstado gProfileInvisivel
    TreeViewProfileDet.SetFocus
    TreeViewProfileDet_Click
    
End Sub

Private Sub BtInvisivelObj_Click()
    AlteraEstadoObjecto lObjInvisivel
End Sub

Private Sub BtNovo_Click()
    CriaProfile
End Sub

Private Sub BtRenomear_Click()
    RenomearProfile
End Sub

Private Sub BtVisivel_Click()
    AlteraEstado gProfileVisivel
End Sub

Private Sub BtVisivelObj_Click()
    AlteraEstadoObjecto lObjVisivel
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()

    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = "Administra��o de Profiles"
    Me.left = 540
    Me.top = 450
    Me.Width = 9375 ' 14340
    Me.Height = 6450 ' Normal
    Set CampoDeFocus = BtGravar
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAdministracaoProfiles = Nothing
    
    Set gFormActivo = FormAssociacaoProfiles

End Sub

Sub LimpaCampos()
    Me.SetFocus
End Sub

Sub DefTipoCampos()
End Sub

Sub PreencheCampos()
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
End Function

Sub FuncaoProcurar()
End Sub

Sub FuncaoAnterior()
End Sub

Sub FuncaoSeguinte()
End Sub

Sub FuncaoInserir()
End Sub

Sub BD_Insert()
End Sub

Sub FuncaoModificar()
End Sub

Sub BD_Update()
End Sub

Sub FuncaoRemover()
End Sub

Sub BD_Delete()
End Sub


Sub PreencheValoresDefeito()
    TreeViewProfileDet.Enabled = False
    TreeViewProfiles.Enabled = False
    TreeViewProfileObj.Enabled = False
    FrameAccao.Visible = True
    InicializaTreeMenus
    InicializaTreeProfiles
    PreencheProfilesDet
    PreencheProfilesObj
    FrameAccao.Visible = False
    
    TreeViewProfileDet.Enabled = True
    TreeViewProfiles.Enabled = True
    TreeViewProfileObj.Enabled = True

End Sub

' ----------------------------------------------------------------------------------

' ADICIONA UM NOVO PERFIL

' ----------------------------------------------------------------------------------
Private Sub CriaProfile()
    Dim root As Node
    Dim no As Node
    Dim novoCodigo As Integer
    Dim Pai As Integer
    Dim novoProfile As Node
    Dim maximoCod As Integer
    Dim i As Integer
    On Error GoTo TrataErro
 
    
    novoCodigo = BG_DaMAX(gBD_PREFIXO_TAB & "profiles", "COD_PROFILE") + 1
    For i = 1 To totalEstrutProfile
        If estrutProfile(i).Codigo > maximoCod Then
            maximoCod = estrutProfile(i).Codigo
        End If
    Next
    If maximoCod >= novoCodigo Then
        novoCodigo = maximoCod + 1
    End If
    
    Set no = TreeViewProfiles.SelectedItem
    If (no Is Nothing) Then
        Pai = 0
        
        totalEstrutProfile = totalEstrutProfile + 1
        ReDim Preserve estrutProfile(totalEstrutProfile)
        estrutProfile(totalEstrutProfile).Codigo = novoCodigo
        estrutProfile(totalEstrutProfile).descricao = "Novo Profile"
        estrutProfile(totalEstrutProfile).ordem = totalEstrutProfile
        estrutProfile(totalEstrutProfile).codigo_pai = Pai
        estrutProfile(totalEstrutProfile).estrutMenu = estrutMenu
        estrutProfile(totalEstrutProfile).totalEstrutMenu = totalEstrutMenu
        Set root = TreeViewProfiles.Nodes.Add(, tvwChild, "KeyPR" & novoCodigo, estrutProfile(totalEstrutProfile).descricao, "Key1")
        root.Expanded = True
    Else
        Pai = DevolveCodigoProfile(no.Key)
        
        totalEstrutProfile = totalEstrutProfile + 1
        ReDim Preserve estrutProfile(totalEstrutProfile)
        estrutProfile(totalEstrutProfile).Codigo = novoCodigo
        estrutProfile(totalEstrutProfile).descricao = "Novo Profile"
        estrutProfile(totalEstrutProfile).ordem = totalEstrutProfile
        estrutProfile(totalEstrutProfile).codigo_pai = Pai
        estrutProfile(totalEstrutProfile).estrutMenu = estrutMenu
        estrutProfile(totalEstrutProfile).totalEstrutMenu = totalEstrutMenu
        Set root = TreeViewProfiles.Nodes.Add(Pai, tvwChild, "KeyPR" & novoCodigo, estrutProfile(totalEstrutProfile).descricao, "Key1")
        root.Expanded = True
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  criaProfile " & Err.Description, Me.Name, "criaProfile", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' REMOVE UM NOVO PROFILE

' ----------------------------------------------------------------------------------
Private Sub RemoveProfile(indiceP As Long)
    Dim i As Long
    
    On Error GoTo TrataErro
    
    ' REMOVE FILHOS
    For i = 1 To totalEstrutProfile
        If i <= totalEstrutProfile Then
            If estrutProfile(i).codigo_pai = estrutProfile(indiceP).Codigo Then
                RemoveProfile i
                i = 0
            End If
        Else
            Exit For
        End If
    Next
    
    For i = indiceP To totalEstrutProfile - 1
        estrutProfile(i).Codigo = estrutProfile(i + 1).Codigo
        estrutProfile(i).codigo_pai = estrutProfile(i + 1).codigo_pai
        estrutProfile(i).descricao = estrutProfile(i + 1).descricao
        estrutProfile(i).estrutMenu = estrutProfile(i + 1).estrutMenu
        estrutProfile(i).ordem = estrutProfile(i + 1).ordem
        estrutProfile(i).totalEstrutMenu = estrutProfile(i + 1).totalEstrutMenu
    Next
    estrutProfile(totalEstrutProfile).Codigo = ""
    estrutProfile(totalEstrutProfile).codigo_pai = ""
    estrutProfile(totalEstrutProfile).descricao = ""
    ReDim estrutProfile(totalEstrutProfile).estrutMenu(0)
    estrutProfile(totalEstrutProfile).ordem = 0
    estrutProfile(totalEstrutProfile).totalEstrutMenu = 0
    
    totalEstrutProfile = totalEstrutProfile - 1
    ReDim Preserve estrutProfile(totalEstrutProfile)
    TreeViewProfiles.Nodes.Remove indiceP
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RemoveProfile " & Err.Description, Me.Name, "RemoveProfile", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' CARREGA A ARVORE DOS PROFILES

' ----------------------------------------------------------------------------------
Private Sub InicializaTreeProfiles()
    Dim sSql As String
    Dim rsProfiles As New ADODB.recordset
    On Error GoTo TrataErro
    TreeViewProfiles.LineStyle = tvwRootLines
    TreeViewProfiles.ImageList = ImageListProfiles
    
    LbAccao.caption = "A preencher estrutura de Profiles"
    DoEvents
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles ORDER BY cod_profile"
    rsProfiles.CursorLocation = adUseServer
    rsProfiles.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfiles.Open sSql, gConexao
    If rsProfiles.RecordCount >= 1 Then
        totalEstrutProfile = 0
        ReDim estrutProfile(0)
        
        While Not rsProfiles.EOF
            LbAccao.caption = "A preencher estrutura de Profiles: " & totalEstrutProfile
            DoEvents
            totalEstrutProfile = totalEstrutProfile + 1
            ReDim Preserve estrutProfile(totalEstrutProfile)
            estrutProfile(totalEstrutProfile).Codigo = BL_HandleNull(rsProfiles!cod_profile, "")
            estrutProfile(totalEstrutProfile).codigo_pai = BL_HandleNull(rsProfiles!cod_pai, "0")
            estrutProfile(totalEstrutProfile).descricao = BL_HandleNull(rsProfiles!descr_profile, "")
            estrutProfile(totalEstrutProfile).ordem = BL_HandleNull(rsProfiles!ordem, "")
            estrutProfile(totalEstrutProfile).totalEstrutMenu = totalEstrutMenu
            estrutProfile(totalEstrutProfile).estrutMenu = estrutMenu
            
            If (estrutProfile(totalEstrutProfile).codigo_pai = "0") Then
              Dim root As Node
              Set root = TreeViewProfiles.Nodes.Add(, tvwChild, "KeyPR" & estrutProfile(totalEstrutProfile).Codigo, estrutProfile(totalEstrutProfile).descricao, "Key1")
              root.Expanded = True
            Else
              Set root = TreeViewProfiles.Nodes.Add(DevolvePaiProfile(totalEstrutProfile), tvwChild, "KeyPR" & estrutProfile(totalEstrutProfile).Codigo, estrutProfile(totalEstrutProfile).descricao, "Key1")
            End If
            rsProfiles.MoveNext
        Wend
    End If
    rsProfiles.Close

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InicializaTreeProfiles " & Err.Description, Me.Name, "InicializaTreeProfiles", True
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------

' CARREGA A ARVORE DOS MENUS

' ----------------------------------------------------------------------------------
Private Sub InicializaTreeMenus()
    Dim sSql As String
    Dim rsMenus As New ADODB.recordset
    On Error GoTo TrataErro
    totalEstrutMenu = 0
    ReDim estrutMenu(0)
    LbAccao.caption = "A preencher estrutura de menus"
    DoEvents
    TreeViewProfileDet.LineStyle = tvwRootLines
    TreeViewProfileDet.ImageList = ImageListProfileDetalhes
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "menus ORDER BY ordem"
    rsMenus.CursorLocation = adUseServer
    rsMenus.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMenus.Open sSql, gConexao
    If rsMenus.RecordCount >= 1 Then
        While Not rsMenus.EOF
        
            LbAccao.caption = "A preencher estrutura de menus: " & totalEstrutMenu
            DoEvents
            totalEstrutMenu = totalEstrutMenu + 1
            ReDim Preserve estrutMenu(totalEstrutMenu)
            estrutMenu(totalEstrutMenu).Codigo = BL_HandleNull(rsMenus!Codigo, -1)
            estrutMenu(totalEstrutMenu).indice = BL_HandleNull(rsMenus!indice, -1)
            estrutMenu(totalEstrutMenu).ordem = BL_HandleNull(rsMenus!ordem, -1)
            estrutMenu(totalEstrutMenu).id = BL_HandleNull(rsMenus!Codigo, "") & " (" & BL_HandleNull(rsMenus!indice, "-1") & ")"
            estrutMenu(totalEstrutMenu).nome = BL_HandleNull(rsMenus!descricao, "")
            If BL_HandleNull(rsMenus!cod_pai, "") <> "" Then
                estrutMenu(totalEstrutMenu).id_pai = BL_HandleNull(rsMenus!cod_pai, "") & " (" & BL_HandleNull(rsMenus!indice_cod_pai, "-1") & ")"
            End If
            estrutMenu(totalEstrutMenu).estado = gProfileDefeito
            
            If (BL_HandleNull(rsMenus!cod_pai, "") = "") Then
              Dim root As Node
              Set root = TreeViewProfileDet.Nodes.Add(, tvwChild, estrutMenu(totalEstrutMenu).id, estrutMenu(totalEstrutMenu).nome, DevolveImagemMenu(gProfileDefeito))
              root.Expanded = False
            Else
              Set root = TreeViewProfileDet.Nodes.Add(DevolvePaiMenu(totalEstrutMenu), tvwChild, estrutMenu(totalEstrutMenu).id, estrutMenu(totalEstrutMenu).nome, DevolveImagemMenu(gProfileDefeito))
            End If
            rsMenus.MoveNext
        Wend
    End If
    rsMenus.Close

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InicializaTreeProfiles " & Err.Description, Me.Name, "InicializaTreeProfiles", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' CARREGA A ARVORE DOS MENUS

' ----------------------------------------------------------------------------------
Private Sub InicializaTreeObjectos()
    Dim sSql As String
    Dim rsForms As New ADODB.recordset
    Dim root As Node
    Dim Pai As Long
    On Error GoTo TrataErro
    totalEstrutObjectos = 0
    ReDim estrutObjectos(0)
    
    LbAccao.caption = "A preencher estrutura de Objectos"
    DoEvents
    ' INSERE OS FORMS
    TreeViewProfileObj.LineStyle = tvwRootLines
    TreeViewProfileObj.ImageList = ImageListProfileObjectos
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "forms ORDER BY cod_form "
    rsForms.CursorLocation = adUseServer
    rsForms.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsForms.Open sSql, gConexao
    If rsForms.RecordCount >= 1 Then
        While Not rsForms.EOF
        
            totalEstrutObjectos = totalEstrutObjectos + 1
            ReDim Preserve estrutObjectos(totalEstrutObjectos)
            estrutObjectos(totalEstrutObjectos).Codigo = BL_HandleNull(rsForms!cod_form, "")
            estrutObjectos(totalEstrutObjectos).cod_form = BL_HandleNull(rsForms!cod_form, "")
            estrutObjectos(totalEstrutObjectos).id = "KeyForm_" & estrutObjectos(totalEstrutObjectos).Codigo
            estrutObjectos(totalEstrutObjectos).id_pai = ""
            estrutObjectos(totalEstrutObjectos).indice = -1
            estrutObjectos(totalEstrutObjectos).tipo = lObjForms
            
            Set root = TreeViewProfileObj.Nodes.Add(, tvwChild, estrutObjectos(totalEstrutObjectos).id, estrutObjectos(totalEstrutObjectos).Codigo, DevolveImagemObjecto(lObjForms))
            root.Expanded = False
            rsForms.MoveNext
        Wend
    End If
    rsForms.Close

    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "objectos ORDER BY cod_form, tipo, cod_objecto, indice_objecto "
    rsForms.CursorLocation = adUseServer
    rsForms.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsForms.Open sSql, gConexao
    If rsForms.RecordCount >= 1 Then
        While Not rsForms.EOF
        
            ' VERIFICA SE EXISTE O TIPO DE OBJECTO
            Pai = ExisteTipoObjecto(BL_HandleNull(rsForms!cod_form, ""), BL_HandleNull(rsForms!tipo, ""))
            If Pai > 0 Then
                
                totalEstrutObjectos = totalEstrutObjectos + 1
                ReDim Preserve estrutObjectos(totalEstrutObjectos)
                estrutObjectos(totalEstrutObjectos).Codigo = BL_HandleNull(rsForms!tipo, "")
                estrutObjectos(totalEstrutObjectos).cod_form = BL_HandleNull(rsForms!cod_form, "")
                estrutObjectos(totalEstrutObjectos).id = BL_HandleNull(rsForms!cod_form, "") & "_" & estrutObjectos(totalEstrutObjectos).Codigo
                estrutObjectos(totalEstrutObjectos).id_pai = "KeyForm_" & BL_HandleNull(rsForms!cod_form, "")
                estrutObjectos(totalEstrutObjectos).indice = -1
                estrutObjectos(totalEstrutObjectos).tipo = lObjTipoObjecto
                Set root = TreeViewProfileObj.Nodes.Add(Pai, tvwChild, estrutObjectos(totalEstrutObjectos).id, estrutObjectos(totalEstrutObjectos).Codigo, DevolveImagemObjecto(lObjTipoObjecto))
                root.Expanded = False
                Pai = totalEstrutObjectos
            End If
            
            
            If Pai <= 0 Then
                Pai = RetornaPaiObjecto(BL_HandleNull(rsForms!cod_form, "") & "_" & BL_HandleNull(rsForms!tipo, ""))
            End If
            
            totalEstrutObjectos = totalEstrutObjectos + 1
            ReDim Preserve estrutObjectos(totalEstrutObjectos)
            estrutObjectos(totalEstrutObjectos).Codigo = BL_HandleNull(rsForms!cod_objecto, "")
            estrutObjectos(totalEstrutObjectos).cod_form = BL_HandleNull(rsForms!cod_form, "")
            estrutObjectos(totalEstrutObjectos).indice = BL_HandleNull(rsForms!indice_objecto, "-1")
            estrutObjectos(totalEstrutObjectos).id = BL_HandleNull(rsForms!cod_form, "") & "_" & estrutObjectos(totalEstrutObjectos).Codigo & "(" & _
                                                     BL_HandleNull(rsForms!indice_objecto, "-1") & ")"
            estrutObjectos(totalEstrutObjectos).id_pai = "KeyForm_" & BL_HandleNull(rsForms!cod_form, "")
            estrutObjectos(totalEstrutObjectos).tipo = lObjObjecto
            estrutObjectos(totalEstrutObjectos).estado = lObjDefeito
            Set root = TreeViewProfileObj.Nodes.Add(Pai, tvwChild, estrutObjectos(totalEstrutObjectos).id, estrutObjectos(totalEstrutObjectos).Codigo, DevolveImagemObjecto(lObjDefeito))
            root.Expanded = False
            
            rsForms.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InicializaTreeObjectos " & Err.Description, Me.Name, "InicializaTreeObjectos", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' INSERE PROFILE NA BD

' ----------------------------------------------------------------------------------
Private Function InsereProfile(Codigo As Integer, descricao As String, cod_pai As Integer, ordem As Integer) As Integer
    Dim sSql As String
    On Error GoTo TrataErro
    
    
    sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "profiles (cod_profile, descr_profile, cod_pai, ordem) VALUES( "
    sSql = sSql & Codigo & ", "
    sSql = sSql & BL_TrataStringParaBD(descricao) & ", "
    sSql = sSql & cod_pai & ", "
    sSql = sSql & ordem & ") "
    BG_ExecutaQuery_ADO sSql
    InsereProfile = Codigo
    
Exit Function
TrataErro:
    InsereProfile = -1
    BG_LogFile_Erros "Erro  InsereProfile " & Err.Description, Me.Name, "InsereProfile", True
    Exit Function
    Resume Next
End Function


Private Sub TreeViewProfileDet_Click()
    'nada
End Sub

' ----------------------------------------------------------------------------------

' DEPOIS DE ALTERAR O NOME NA ARVORE, GRAVA NA BD

' ----------------------------------------------------------------------------------
Private Sub TreeViewProfiles_AfterLabelEdit(Cancel As Integer, NewString As String)
    Dim Codigo As Integer
    Dim indice_pai As Integer
    On Error GoTo TrataErro
    
    NewString = Trim(NewString)
    If Len(NewString) = 0 Or _
        NewString = "Novo Profile" Or NewString = "Profile Base" Then
        Cancel = True
        
        Exit Sub
    End If
    estrutProfile(TreeViewProfiles.SelectedItem.Index).descricao = NewString
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  TreeViewProfiles_AfterLabelEdit " & Err.Description, Me.Name, "TreeViewProfiles_AfterLabelEdit"
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------

' RENOMEIA O NOME DA ARVORE

' ----------------------------------------------------------------------------------
Private Sub RenomearProfile()
    Dim no As Node
    Dim novoProfile As Node
    Dim Codigo As Integer
    On Error GoTo TrataErro
    
    Set no = TreeViewProfiles.SelectedItem
    If (no Is Nothing) Then
        BG_Mensagem mediMsgBox, "Necess�rio seleccionar um profile"
    Exit Sub
    End If
    TreeViewProfiles.StartLabelEdit
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RenomearProfile " & Err.Description, Me.Name, "RenomearProfile"
    Exit Sub
    Resume Next
End Sub






' ----------------------------------------------------------------------------------

' DEVOLVE A IMAGEM DE ACORDO COM O ESTADO DO MENU

' ----------------------------------------------------------------------------------
Private Function DevolveImagemMenu(state As Long) As String
    On Error GoTo TrataErro
    
    If (state = gProfileDefeito) Then
      DevolveImagemMenu = "Key1"
    ElseIf (state = gProfileVisivel) Then
      DevolveImagemMenu = "Key2"
    ElseIf (state = gProfileInvisivel) Then
      DevolveImagemMenu = "Key3"
    End If
Exit Function
TrataErro:
    DevolveImagemMenu = ""
    BG_LogFile_Erros "Erro  DevolveImagemMenu " & Err.Description, Me.Name, "DevolveImagemMenu"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------

' DEVOLVE A IMAGEM DE ACORDO COM O TipoObjecto

' ----------------------------------------------------------------------------------
Private Function DevolveImagemObjecto(state As Long) As String
    On Error GoTo TrataErro
    
    If (state = lObjDefeito) Then
      DevolveImagemObjecto = "Key1"
    ElseIf (state = lObjVisivel) Then
      DevolveImagemObjecto = "Key2"
    ElseIf (state = lObjInvisivel) Then
      DevolveImagemObjecto = "Key3"
    ElseIf (state = lObjForms) Then
      DevolveImagemObjecto = "KeyForm"
    ElseIf (state = lObjTipoObjecto) Then
      DevolveImagemObjecto = "KeyObj"
    End If
Exit Function
TrataErro:
    DevolveImagemObjecto = ""
    BG_LogFile_Erros "Erro  DevolveImagemObjecto " & Err.Description, Me.Name, "DevolveImagemObjecto"
    Exit Function
    Resume Next
End Function



' ----------------------------------------------------------------------------------

' DEVOLVE INDICE DO NO PAI

' ----------------------------------------------------------------------------------
Private Function DevolvePaiMenu(indice As Long) As Long
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To indice
        If estrutMenu(i).id = estrutMenu(indice).id_pai Then
            DevolvePaiMenu = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolvePaiMenu = -1
    BG_LogFile_Erros "Erro  DevolvePaiMenu " & Err.Description, Me.Name, "DevolvePaiMenu"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------

' DEVOLVE O INDICE DO NO PAI

' ----------------------------------------------------------------------------------
Private Function DevolvePaiProfile(indice As Long) As Long
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To indice
        If estrutProfile(i).Codigo = estrutProfile(indice).codigo_pai Then
            DevolvePaiProfile = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolvePaiProfile = -1
    BG_LogFile_Erros "Erro  DevolvePaiProfile " & Err.Description, Me.Name, "DevolvePaiProfile"
    Exit Function
    Resume Next
End Function



' ----------------------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ASSOCIACAO DE PROFILES E MENUS

' ----------------------------------------------------------------------------------
Private Sub PreencheProfilesDet()
    Dim sSql As String
    Dim rsProfilesDet As New ADODB.recordset
    Dim i As Long
    Dim j As Long
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles_det ORDER BY cod_profile ASC, cod_menu ASC, indice_menu ASC "
    rsProfilesDet.CursorLocation = adUseServer
    rsProfilesDet.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfilesDet.Open sSql, gConexao
    If rsProfilesDet.RecordCount >= 1 Then
        
        While Not rsProfilesDet.EOF
            For j = 1 To totalEstrutMenu
                If estrutMenu(j).Codigo = BL_HandleNull(rsProfilesDet!cod_menu, "") And _
                    estrutMenu(j).indice = BL_HandleNull(rsProfilesDet!indice_menu, "") Then
                    Exit For
                End If
            Next
            
            For i = 1 To totalEstrutProfile
                If j <= totalEstrutMenu Then
                    If estrutProfile(i).Codigo = BL_HandleNull(rsProfilesDet!cod_profile, "") Then
                        estrutProfile(i).estrutMenu(j).estado = BL_HandleNull(rsProfilesDet!estado, gProfileDefeito)
                        Exit For
                    End If
                End If
            Next
            rsProfilesDet.MoveNext
        Wend
    End If
    rsProfilesDet.Close
    Set rsProfilesDet = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheProfilesDet " & Err.Description, Me.Name, "PreencheProfilesDet"
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------

' COLOCA UM NO E TODOS OS FILHOS NUM DETERMINADO ESTADO

' ----------------------------------------------------------------------------------
Private Sub ColocaNoEstado(iProfile As Long, iMenu As Long, estado As Long, PropagaFilhos As Boolean)
  On Error GoTo TrataErro
  Dim Node As Node
  Dim i As Long
  Set Node = TreeViewProfileDet.Nodes.item(estrutMenu(iMenu).id)
  Node.Image = DevolveImagemMenu(estado)
  estrutProfile(iProfile).estrutMenu(iMenu).estado = estado
    If PropagaFilhos = True Then
        For i = 1 To totalEstrutMenu
            If estrutMenu(i).id_pai = estrutMenu(iMenu).id Then
                ColocaNoEstado iProfile, i, estado, True
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ColocaNoEstado " & Err.Description, Me.Name, "ColocaNoEstado"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' COLOCA UM NO E TODOS OS FILHOS NUM DETERMINADO ESTADO - Objectos

' ----------------------------------------------------------------------------------
Private Sub ColocaNoEstadoObjecto(iProfile As Long, iObjecto As Long, estado As Long, PropagaFilhos As Boolean)
  On Error GoTo TrataErro
  Dim Node As Node
  Dim i As Long
    If estrutObjectos(iObjecto).tipo = lObjObjecto Then
        Set Node = TreeViewProfileObj.Nodes.item(iObjecto)
        Node.Image = DevolveImagemObjecto(estado)
        estrutProfile(iProfile).estrutObjectos(iObjecto).estado = estado
    End If
    If PropagaFilhos = True Then
        For i = 1 To totalEstrutObjectos
            If estrutObjectos(i).id_pai = estrutObjectos(iObjecto).id Then
                ColocaNoEstadoObjecto iProfile, i, estado, True
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ColocaNoEstadoObjecto " & Err.Description, Me.Name, "ColocaNoEstadoObjecto"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' DADO UM ID DEVOLVE CODIGO DO PROFILE

' ----------------------------------------------------------------------------------
Private Function DevolveCodigoProfile(id As String) As Integer
    Dim i As Long
    On Error GoTo TrataErro
    
    For i = 1 To totalEstrutProfile
        If "KeyPR" & estrutProfile(i).Codigo = id Then
            DevolveCodigoProfile = estrutProfile(i).Codigo
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  DevolveCodigoProfile " & Err.Description, Me.Name, "DevolveCodigoProfile"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------

' DADO UM ID DEVOLVE INDICE DO PROFILE

' ----------------------------------------------------------------------------------
Private Function DevolveIndiceProfile(id As String) As Long
    Dim i As Long
    On Error GoTo TrataErro
    
    For i = 1 To totalEstrutProfile
        If "KeyPR" & estrutProfile(i).Codigo = id Then
            DevolveIndiceProfile = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolveIndiceProfile = -1
    BG_LogFile_Erros "Erro  DevolveIndiceProfile " & Err.Description, Me.Name, "DevolveIndiceProfile"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------

' DADO UM ID DEVOLVE INDICE DO MENU

' ----------------------------------------------------------------------------------
Private Function DevolveIndiceMenu(id As String) As Long
    Dim i As Long
    On Error GoTo TrataErro
    
    For i = 1 To totalEstrutMenu
        If estrutMenu(i).id = id Then
            DevolveIndiceMenu = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevolveIndiceMenu = -1
    BG_LogFile_Erros "Erro  DevolveIndiceMenu " & Err.Description, Me.Name, "DevolveIndiceMenu"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' GRAVA PERMISSOES DOS PERFIS

' ----------------------------------------------------------------------------------
Private Sub GravaProfilesDet()
    Dim sSql As String
    Dim i As Long
    Dim j As Long
    On Error GoTo TrataErro
    
    BG_BeginTransaction
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "profiles_Det "
    BG_ExecutaQuery_ADO sSql
    
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "profiles "
    BG_ExecutaQuery_ADO sSql
    
    If totalEstrutObjectos > 0 Then
        sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "profiles_obj "
        BG_ExecutaQuery_ADO sSql
    End If
    FrameAccao.Visible = True
    TreeViewProfileDet.Enabled = False
    TreeViewProfiles.Enabled = False
    TreeViewProfileObj.Enabled = False
    
    For i = 1 To totalEstrutProfile
        LbAccao.caption = "Gravar Profile..."
        DoEvents
        'INSERE PERFIL
        InsereProfile CInt(estrutProfile(i).Codigo), estrutProfile(i).descricao, CInt(estrutProfile(i).codigo_pai), CInt(estrutProfile(i).ordem)
            
        'INSERE O DETALHE
        For j = 1 To totalEstrutMenu
            LbAccao.caption = "Gravar Associa��o de menus(" & estrutProfile(i).descricao & " ) " & j & " ..."
            DoEvents
            sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "profiles_Det (cod_profile, cod_menu, indice_menu, estado) VALUES("
            sSql = sSql & estrutProfile(i).Codigo & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutProfile(i).estrutMenu(j).Codigo) & ", "
            sSql = sSql & estrutProfile(i).estrutMenu(j).indice & ", "
            sSql = sSql & estrutProfile(i).estrutMenu(j).estado & ") "
            BG_ExecutaQuery_ADO sSql
        Next
        'INSERE OS OBJECTOS
        LbAccao.caption = "Gravar Associa��o de objectos (" & estrutProfile(i).descricao & " ) ..."
        DoEvents
        For j = 1 To totalEstrutObjectos
            If estrutProfile(i).estrutObjectos(j).tipo = lObjObjecto And estrutProfile(i).estrutObjectos(j).estado <> lObjDefeito Then
                LbAccao.caption = "Gravar Associa��o de objectos (" & estrutProfile(i).descricao & " ) " & j & " ..."
                DoEvents
                sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "profiles_obj (cod_profile, cod_objecto, cod_form, estado,indice_objecto) VALUES("
                sSql = sSql & estrutProfile(i).Codigo & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutProfile(i).estrutObjectos(j).Codigo) & ", "
                sSql = sSql & BL_TrataStringParaBD(estrutProfile(i).estrutObjectos(j).cod_form) & ", "
                sSql = sSql & estrutProfile(i).estrutObjectos(j).estado & ", "
                sSql = sSql & estrutProfile(i).estrutObjectos(j).indice & ") "
                BG_ExecutaQuery_ADO sSql
            End If
        Next
    Next
    FrameAccao.Visible = False
    TreeViewProfileDet.Enabled = True
    TreeViewProfiles.Enabled = True
    TreeViewProfileObj.Enabled = True
    
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_RollbackTransaction
    FrameAccao.Visible = False
    TreeViewProfileDet.Enabled = True
    TreeViewProfiles.Enabled = True
    TreeViewProfileObj.Enabled = True
    BG_LogFile_Erros "Erro  GravaProfilesDet " & Err.Description, Me.Name, "GravaProfilesDet"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' GRAVA PERMISSOES DOS PERFIS

' ----------------------------------------------------------------------------------
Private Sub GravaProfilesObjectos()
    Dim sSql As String
    Dim i As Long
    Dim j As Long
    On Error GoTo TrataErro
    
    BG_BeginTransaction
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "profiles_obj "
    BG_ExecutaQuery_ADO sSql
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "profiles "
    BG_ExecutaQuery_ADO sSql
    For i = 1 To totalEstrutProfile
    
        'INSERE PERFIL
        InsereProfile CInt(estrutProfile(i).Codigo), estrutProfile(i).descricao, CInt(estrutProfile(i).codigo_pai), CInt(estrutProfile(i).ordem)
            
        'INSERE O DETALHE
        For j = 1 To totalEstrutObjectos
            sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "profiles_obj (cod_profile, cod_form,cod_objecto, indice_objecto, estado) VALUES("
            sSql = sSql & estrutProfile(i).Codigo & ", "
            BG_ExecutaQuery_ADO sSql
        Next
    Next
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  GravaProfilesObjectos " & Err.Description, Me.Name, "GravaProfilesObjectos"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' ACTUALIZA AS PERMISSOES PARA PERFIL SELECCIONADO

' ----------------------------------------------------------------------------------
Private Sub TreeViewProfiles_Click()
    Dim j As Long
    Dim i As Long
    On Error GoTo TrataErro
    i = DevolveIndiceProfile(TreeViewProfiles.SelectedItem.Key)
    
    For j = 1 To totalEstrutMenu
        ColocaNoEstado i, j, estrutProfile(i).estrutMenu(j).estado, False
    Next
    
    For j = 1 To totalEstrutObjectos
        ColocaNoEstadoObjecto i, j, estrutProfile(i).estrutObjectos(j).estado, False
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  TreeViewProfiles_Click " & Err.Description, Me.Name, "TreeViewProfiles_Click"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' ALTERA ESTADO DA PROPRIEDADE EM CAUSA

' ----------------------------------------------------------------------------------
Private Sub AlteraEstado(estado As Long)
    Dim i As Long
    Dim iProfile As Long
    Dim iMenu As Long
    On Error GoTo TrataErro
    If TreeViewProfiles.SelectedItem Is Nothing Or TreeViewProfileDet.SelectedItem Is Nothing Then
        Exit Sub
    End If
    
    For i = 1 To totalEstrutProfile
        If TreeViewProfiles.SelectedItem.Key = "KeyPR" & estrutProfile(i).Codigo Then
            iProfile = i
            Exit For
        End If
    Next
    For i = 1 To totalEstrutMenu
        If TreeViewProfileDet.SelectedItem.Key = estrutMenu(i).id Then
            iMenu = i
            Exit For
        End If
    Next
    ColocaNoEstado iProfile, iMenu, estado, True
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AlteraEstado " & Err.Description, Me.Name, "AlteraEstado"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' ALTERA ESTADO DA PROPRIEDADE EM CAUSA - Objectos

' ----------------------------------------------------------------------------------
Private Sub AlteraEstadoObjecto(estado As Long)
    Dim i As Long
    Dim iProfile As Long
    Dim iObjecto As Long
    On Error GoTo TrataErro
    If TreeViewProfiles.SelectedItem Is Nothing Or TreeViewProfileObj.SelectedItem Is Nothing Then
        Exit Sub
    End If
    
    For i = 1 To totalEstrutProfile
        If TreeViewProfiles.SelectedItem.Key = "KeyPR" & estrutProfile(i).Codigo Then
            iProfile = i
            Exit For
        End If
    Next
    For i = 1 To totalEstrutObjectos
        If TreeViewProfileObj.SelectedItem.Key = estrutObjectos(i).id Then
            iObjecto = i
            Exit For
        End If
    Next
    ColocaNoEstadoObjecto iProfile, iObjecto, estado, True
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AlteraEstadoObjecto " & Err.Description, Me.Name, "AlteraEstadoObjecto"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' VERIFICA SE J� EXISTE TIPO DE OBJECTO ASSOCIADO AO FORM

' ----------------------------------------------------------------------------------
Private Function ExisteTipoObjecto(nomeForm As String, tipoObj As String) As Long
    Dim i As Long
    For i = 1 To totalEstrutObjectos
        If estrutObjectos(i).id = "KeyForm_" & nomeForm Then
            ExisteTipoObjecto = i
        End If
        If estrutObjectos(i).id_pai = "KeyForm_" & nomeForm Then
            If estrutObjectos(i).Codigo = tipoObj Then
                ExisteTipoObjecto = -1
                Exit Function
            End If
        End If
    Next
Exit Function
TrataErro:
    ExisteTipoObjecto = -1
    BG_LogFile_Erros "Erro  ExisteTipoObjecto " & Err.Description, Me.Name, "ExisteTipoObjecto"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

'

' ----------------------------------------------------------------------------------
Private Function RetornaPaiObjecto(id As String) As Long
    Dim i As Long
    For i = 1 To totalEstrutObjectos
        If estrutObjectos(i).id = id Then
            RetornaPaiObjecto = i
            Exit Function
        End If
    Next
    RetornaPaiObjecto = -1
Exit Function
TrataErro:
    RetornaPaiObjecto = -1
    BG_LogFile_Erros "Erro  RetornaPaiObjecto " & Err.Description, Me.Name, "RetornaPaiObjecto"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' PREENCHE ASSOCIACAO DE OBJECTOS A PROFILES

' ----------------------------------------------------------------------------------
Private Sub PreencheProfilesObj()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsProfilesObj As New ADODB.recordset
    Dim i As Long
    Dim j As Long
    On Error GoTo TrataErro
    
    If totalEstrutObjectos = 0 Then
        Exit Sub
    End If
    
    For i = 1 To totalEstrutProfile
        estrutProfile(i).estrutObjectos = estrutObjectos
        estrutProfile(i).totalEstrutObjectos = totalEstrutObjectos
    Next
    
    LbAccao.caption = " Preencher associa��o do objectos e profiles "
    DoEvents
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles_obj ORDER BY cod_profile ASC, cod_form ASC, cod_objecto ASC, indice_objecto ASC "
    rsProfilesObj.CursorLocation = adUseServer
    rsProfilesObj.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfilesObj.Open sSql, gConexao
    If rsProfilesObj.RecordCount >= 1 Then
        
        While Not rsProfilesObj.EOF
            If BL_HandleNull(rsProfilesObj!estado, lObjDefeito) <> lObjDefeito Then
                For j = 1 To totalEstrutObjectos
                    If estrutObjectos(j).Codigo = BL_HandleNull(rsProfilesObj!cod_objecto, "") And _
                        estrutObjectos(j).indice = BL_HandleNull(rsProfilesObj!indice_objecto, "") And _
                        estrutObjectos(j).cod_form = BL_HandleNull(rsProfilesObj!cod_form, "") And estrutObjectos(j).tipo = lObjObjecto Then
                        Exit For
                    End If
                Next
                
                For i = 1 To totalEstrutProfile
                    If estrutProfile(i).Codigo = BL_HandleNull(rsProfilesObj!cod_profile, "") Then
                        estrutProfile(i).estrutObjectos(j).estado = BL_HandleNull(rsProfilesObj!estado, lObjDefeito)
                        Exit For
                    End If
                Next
            End If
            rsProfilesObj.MoveNext
        Wend
    End If
    rsProfilesObj.Close
    Set rsProfilesObj = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheProfilesObj " & Err.Description, Me.Name, "PreencheProfilesObj"
    Exit Sub
    Resume Next
End Sub

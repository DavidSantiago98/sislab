Attribute VB_Name = "Integracao_FACTUS"
Option Explicit

Public Function FACTUS_Nova_Analises_IN(tEpisodio As String, _
                                       episodio As String, _
                                       TDoente As String, _
                                       doente As String, _
                                       CodAnaFACTUS As String, _
                                       NReq As Long, _
                                       DtChega As String, _
                                       Optional sServico As String, _
                                       Optional sIsento As String, _
                                       Optional cod_ana_sislab As String, _
                                       Optional quantidade As String) As Integer
                                       
                                       
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim rs_aux As ADODB.recordset
    Dim i As Integer
    Dim lNrFact As Long
    Dim iCodTipRubr As Integer, iCodGrupo As Integer
    Dim lCount As Long
    Dim iRes As Integer
    
    On Error GoTo TrataErro
    
    
    'Determinar o novo n�mero da factura��p (TENTA 10 VEZES - LOCK!)
    i = 0
    lNrFact = -1
    While lNrFact = -1 And i <= 10
        lNrFact = BL_GeraNumeroFact("SEQ_FACT")
        i = i + 1
    Wend
    
    If lNrFact = -1 Then
        BG_LogFile_Erros "Gera_Dados_Fact: Erro a gerar n�mero de factura! " & "Integracao_FACTUS"
        FACTUS_Nova_Analises_IN = -1
        Exit Function
    End If
                                       
    '-----------------------------------------------------------------------------------
    sSql = "SELECT cod_grupo FROM fa_rubr WHERE cod_rubr = " & CodAnaFACTUS & ""
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    
    If rs_aux.RecordCount <> 0 Then
        iCodGrupo = rs_aux!cod_grupo
    Else
        iCodGrupo = 0
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    '------------------------------------------------------------------------------------
    
    sSql = "SELECT cod_tip_rubr FROM fa_grup_rubr WHERE cod_grupo = " & iCodGrupo & ""
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    
    If rs_aux.RecordCount <> 0 Then
        iCodTipRubr = rs_aux!cod_tip_rubr
    Else
        iCodTipRubr = 0
    End If
    '-------------------------------------------------------------------------------------
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    If quantidade = "" Then quantidade = "1"
    
    sSql = "INSERT INTO fa_movi_fact (" & _
        "n_seq_prog, cod_prog, t_doente, doente, t_episodio, episodio, n_ord, " & _
        "dt_ini_real, cod_tip_rubr, cod_grupo, cod_rubr, " & _
        "cod_serv_req, cod_serv_exec, qtd, cod_isen_doe, nr_req_ars, " & _
        "flg_estado, chave_prog, user_cri, dt_cri) " & _
        "VALUES ('" & _
        lNrFact & "', '" & gCodProg & "', '" & TDoente & "', '" & doente & "', '" & _
        tEpisodio & "', '" & episodio & "', '1', " & _
        BL_TrataDataParaBD(DtChega) & ", '" & iCodTipRubr & "', " & _
        BG_VfValor(iCodGrupo, "'") & ", '" & CodAnaFACTUS & "', '" & _
        Replace(sServico, "#", "") & "', '" & gCodServExecFACTUS & "', '" & quantidade & "', " & BG_VfValor(sIsento) & ", '" & _
        NReq & "','1', '" & _
        NReq & "', '" & gIdUtilizador & "', " & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
    
    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
    iRes = BL_ExecutaQuery_Secundaria(sSql)
    If iRes < 0 Then
        GoTo TrataErro
    End If
     FACTUS_Nova_Analises_IN = 1
     Exit Function
                                       
TrataErro:
    BG_LogFile_Erros "Factus_Nova_Analises_IN: Erro a inserir an�lise no FACTUS! " & "Integracao_FACTUS: " & sSql, "INTEGRACAO_FACTUS", "FACTUS_Nova_Analises_IN", False
    FACTUS_Nova_Analises_IN = -1
    Exit Function
    Resume Next
End Function


Public Sub FACTUS_Insere_Movi_Resp(NReq As Long, _
                                    sTEpisodio As String, _
                                    sEpisodio As String, _
                                    sBenef As String, _
                                    sEFR As String, _
                                    sTUtente As String, _
                                    sUtente As String)
    
    Dim sSql As String
    Dim rs_aux As ADODB.recordset
    Dim HisAberto As Integer
    Dim iRes As Integer
    
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM fa_movi_resp WHERE n_ord = 1 AND t_episodio = '" & sTEpisodio & "' AND " & _
        "episodio = '" & sEpisodio & "'"
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    
    If sEFR = "0" Then
        
        HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    
        If HisAberto = 1 And gHIS_Import = 1 And UCase(HIS.uID) = UCase("GH") Then
            Dim RsCodEFR As ADODB.recordset
            Set RsCodEFR = New ADODB.recordset
            With RsCodEFR
                .Source = "SELECT obter_cod_resp('" & sTUtente & "','" & sUtente & "','Ficha-ID','" & sUtente & "') resultado FROM dual"
                .CursorType = adOpenStatic
                .CursorLocation = adUseServer
                .Open , gConnHIS
            End With
            If RsCodEFR.RecordCount > 0 Then
                sEFR = BL_HandleNull(RsCodEFR!resultado, 0)
                sSql = "update sl_requis set cod_efr = " & BL_TrataStringParaBD(sEFR) & " where n_req = " & NReq
                BG_ExecutaQuery_ADO sSql
            Else
                sEFR = "0"
            End If
        Else
            sEFR = "0"
        End If
    Else
        sEFR = sEFR
    End If
    
    
'    If rs_aux.RecordCount <> 0 Then
'
'        sSql = "UPDATE fa_movi_resp SET n_benef_doe = " & BG_VfValor(sBenef, "'") & _
'            ", cod_efr = '" & sEFR & "', user_act = '" & _
'            gIdUtilizador & "', dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
'            " WHERE n_ord = 1 AND t_episodio = '" & sTEpisodio & "' AND episodio = '" & _
'            sEpisodio & "'"
'    Else
'        sSql = "INSERT INTO fa_movi_resp (n_ord, t_episodio, episodio, " & _
'            "n_benef_doe, cod_efr, user_cri, dt_cri) VALUES ('1', '" & _
'            sTEpisodio & "', '" & sEpisodio & "', " & BG_VfValor(sBenef, "'") & ", '" & _
'            sEFR & "', '" & gIdUtilizador & "', " & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
'
'    End If
    
    If rs_aux.RecordCount <> 0 Then

        sSql = "UPDATE fa_movi_resp SET n_benef_doe = " & BG_VfValor(sBenef, "'") & _
            ", cod_efr = '" & sEFR & "', user_act = '" & _
            gIdUtilizador & "', dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
            ", t_doente = '" & sTUtente & "', doente = '" & sUtente & "' " & _
            " WHERE n_ord = 1 AND t_episodio = '" & sTEpisodio & "' AND episodio = '" & _
            sEpisodio & "'"
    Else
        sSql = "INSERT INTO fa_movi_resp (n_ord, t_episodio, episodio, " & _
            "n_benef_doe, cod_efr, user_cri, dt_cri, t_doente, doente) VALUES ('1', '" & _
            sTEpisodio & "', '" & sEpisodio & "', " & BG_VfValor(sBenef, "'") & ", '" & _
            sEFR & "', '" & gIdUtilizador & "', " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
            ",'" & sTUtente & "','" & sUtente & "')"

    End If
    
    iRes = BL_ExecutaQuery_Secundaria(sSql)
    If iRes < 0 Then
        GoTo TrataErro
    End If
    
    rs_aux.Close
    Set rs_aux = Nothing
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Factus_Insere_Movi_Resp: Erro a inserir entidade no FACTUS! " & "Integracao_FACTUS"
    Exit Sub
End Sub

Public Sub FACTUS_Insere_Doe_Fact_Ext(sTEpisodio As String, _
                                        sEpisodio As String, _
                                        sTUtente As String, _
                                        sUtente As String, _
                                        sNome As String, _
                                        sDataNasc As String, _
                                        sMorada As String, _
                                        sCodPostal As String, _
                                        sTelefone As String)

    Dim sSql As String
    Dim rs_aux As ADODB.recordset
    Dim iRes As Integer
    
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM fa_doe_fact_ext WHERE t_doente = '" & sTUtente & "' AND doente = '" & _
        sUtente & "'"
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    
    If rs_aux.RecordCount = 0 Then
        sSql = "INSERT INTO fa_doe_fact_ext (t_doente, doente, nome_doe, " & _
            "dt_nasc, mor_doe, cod_post_doe, telef_doe, " & _
            "user_cri, dt_cri) VALUES ('" & sTUtente & "', '" & _
            sUtente & "', '" & sNome & "', " & BL_TrataDataParaBD(sDataNasc) & ", " & _
            BG_VfValor(sMorada, " '") & ", " & BG_VfValor(sCodPostal, "'") & ", " & _
            BG_VfValor(sTelefone, "'") & ", '" & _
            gIdUtilizador & "', " & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
    Else
        sSql = "UPDATE fa_doe_fact_ext SET nome_doe = '" & sNome & "', dt_nasc = " & _
            BL_TrataDataParaBD(sDataNasc) & ", mor_doe = " & BG_VfValor(sMorada, "'") & _
            ", cod_post_doe = " & BG_VfValor(sCodPostal, "'") & ", telef_doe = " & _
            BG_VfValor(sTelefone, "'") & _
            ", user_act = '" & gIdUtilizador & "', dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
            " WHERE t_doente = '" & sTUtente & "' AND doente = '" & sUtente & "'"
    End If
    
    iRes = BL_ExecutaQuery_Secundaria(sSql)
    
    rs_aux.Close
    Set rs_aux = Nothing
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Factus_Insere_Doe_Fact_Ext: Erro a inserir doente no FACTUS! " & "Integracao_FACTUS"
    Exit Sub
End Sub


Public Function GH_Nova_ANALISES_IN(ByVal tEpisodio As String, ByVal episodio As String, ByVal TDoente As String, _
                                       ByVal doente As String, ByVal CodAnaGH As String, ByVal NReq As Long, ByVal DtChega As String, _
                                       ByVal n_mec As String, Optional DtFim As String, Optional CodServReq As String, _
                                       Optional codServExec As String, Optional NCred As String, _
                                       Optional CodResp As String, Optional analise As String, Optional cod_agrup As String, _
                                       Optional quantidade As String, Optional ByVal Cod_Perfil As String, Optional ByVal cod_ana_c As String, _
                                       Optional ByVal cod_ana_s As String, Optional ByVal cod_micro As String, Optional ByVal Cod_Gr_Antibio As String, _
                                       Optional ByVal cod_antibio As String, Optional ByVal Cod_Prova As String, Optional ByVal NReqARS As String, _
                                       Optional ByVal centroCusto As String, Optional ByVal n_acto As Integer, _
                                       Optional codMed As String, Optional ByVal cod_u_saude As String, _
                                       Optional ByVal n_mecan_ext As String, Optional ByVal numMarcadores As Integer) As Integer
                                       
                                       
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim rs_aux As ADODB.recordset
    Dim i As Integer
    Dim lCount As Long
    Dim iRes As Integer
    Dim registos As Integer
    Dim sMsg As String
    Dim rsSeq As ADODB.recordset
    Dim NSeq As Long
    Dim tEpisodioPrescr As String
    Dim episodioPrescr As String
    Dim codDom As Integer
    Dim convencao As Integer
    Dim km As Integer
    Dim codPostal As String
    Dim localidade As String
    Dim natureza As String
    Dim resp_aux As String
    Dim rsDom As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    'verifica se e ars das novas
    If Len(NReqARS) = 19 Then
        sSql = "SELECT cod_urbano, convencao, km, cod_postal FROM sl_requis WHERE n_Req = " & NReq
        natureza = "A"
        rsDom.CursorLocation = adUseServer
        rsDom.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsDom.Open sSql, gConexao
        If rsDom.RecordCount = 1 Then
            If BL_HandleNull(rsDom!cod_urbano, mediComboValorNull) > 0 And Len(BL_HandleNull(rsDom!cod_postal, "")) = 8 Then
                codDom = BL_HandleNull(rsDom!cod_urbano, mediComboValorNull)
                convencao = BL_HandleNull(rsDom!convencao, mediComboValorNull)
                codPostal = BL_HandleNull(rsDom!cod_postal, "")
                localidade = BL_SeleccionaDescrPostal(codPostal)
                km = BL_HandleNull(rsDom!km, mediComboValorNull)
            Else
                codDom = mediComboValorNull
                convencao = mediComboValorNull
                codPostal = ""
                localidade = ""
                km = mediComboValorNull
            End If
        Else
            codDom = mediComboValorNull
            convencao = mediComboValorNull
            codPostal = ""
            localidade = ""
            km = mediComboValorNull
        End If
        rsDom.Close
        Set rsDom = Nothing
    Else
        codDom = mediComboValorNull
        convencao = mediComboValorNull
        codPostal = ""
        localidade = ""
        km = mediComboValorNull
        'GH_Nova_ANALISES_IN = mediComboValorNull
        'Exit Function
    End If
    If IF_InsereRespDadosFact(CStr(CodResp)) = True Then
        resp_aux = CodResp
    Else
        resp_aux = " NULL "
    End If
    
    'VERIFICA SE TEM PRESCRICAO ASSOCIADA. SE TIVER USA ESSE EPISODIO
    GH_MapeiaEpisodios NReq, cod_agrup, tEpisodioPrescr, episodioPrescr
    If tEpisodioPrescr <> "" And episodioPrescr <> "" Then
        tEpisodio = tEpisodioPrescr
        episodio = episodioPrescr
        tEpisodioPrescr = ""
        episodioPrescr = ""
    End If
    
    ' Mapeia a situa��o do SISLAB para a GH.
    Select Case gLAB
        Case "CITO", "BIO", "GM"
            tEpisodio = "Consultas"
        Case Else
            Select Case tEpisodio
                Case gT_Urgencia
                    tEpisodio = "Urgencias"
                Case gT_Consulta
                    tEpisodio = "Consultas"
                Case gT_Internamento
                    tEpisodio = "Internamentos"
                Case gT_Externo
                    tEpisodio = "Consultas"
                Case gT_Ficha_ID
                    tEpisodio = "Ficha-ID"
                Case gT_Prescricoes
                    tEpisodio = "Prescricoes"
                Case gT_LAB
                    tEpisodio = "Consultas"
                Case gT_Ambulatorio
                    tEpisodio = "Ambulatorio"
                Case gT_Acidente
                    tEpisodio = "Acidente"
                Case gT_Ambulatorio
                    tEpisodio = "Ambulatorio"
                Case gT_Cons_Inter
                    tEpisodio = "Cons-Inter"
                Case gT_Cons_Telef
                    tEpisodio = "Cons-Telef"
                Case gT_Consumos
                    tEpisodio = "Consumos"
                Case gT_Credenciais
                    tEpisodio = "Credenciais"
                Case gT_Diagnosticos
                    tEpisodio = "Diagnosticos"
                Case gT_Exame
                    tEpisodio = "Exame"
                Case gT_Fisio
                    tEpisodio = "Fisio"
                Case gT_HDI
                    tEpisodio = "Hosp-Dia"
                Case gT_Intervencao
                    tEpisodio = "Intervencao"
                Case gT_MCDT
                    tEpisodio = "Mcdt"
                Case gT_Plano_Oper
                    tEpisodio = "Plano-Oper"
                Case gT_Pre_Intern
                    tEpisodio = "Pre-Intern"
                Case gT_Prog_Cirugico
                    tEpisodio = "Prog-Cirurgico"
                Case gT_Protoc
                    tEpisodio = "Protoc"
                Case gT_Referenciacao
                    tEpisodio = "Referenciacao"
                Case gT_Reg_Oper
                    tEpisodio = "Reg-Oper"
                Case gT_Tratamentos
                    tEpisodio = "Tratamentos"
                Case Else
                    tEpisodio = ""
            End Select
    End Select
    
    If quantidade = "" Then quantidade = "1"
    
    If gLAB = "HMP" Then
        gCodServExec = "52200"
    End If
    If gLAB = "HMP-SS" Then
        gCodServExec = "52300"
    End If
    If gLAB = "SCMVC" Then
        CodServReq = "233"
    End If
    
    If IsMissing(centroCusto) Or centroCusto = "" Then
        centroCusto = gCodServExec
    End If
    
    'Gera sequencial SEQ_DADOS_LAB do GH
    NSeq = -1
    Set rsSeq = New ADODB.recordset
    rsSeq.CursorLocation = adUseServer
    rsSeq.CursorType = adOpenStatic
    rsSeq.ActiveConnection = gConexao
    rsSeq.Source = "select seq_dados_lab.nextval n_seq from dual"
    rsSeq.Open
    If rsSeq.RecordCount > 0 Then
        NSeq = rsSeq!n_seq
    Else
        MsgBox "Erro a gerar a sequencia SEQ_DADOS_LAB!"
        BG_LogFile_Erros "GH_Nova_Analises_IN: Erro a gerar a sequencia SEQ_DADOS_LAB! " & "Integracao_GH"
        GH_Nova_ANALISES_IN = mediComboValorNull
        Exit Function
    End If
    rsSeq.Close
    Set rsSeq = Nothing
    

    sSql = "INSERT INTO sl_dados_fact " & vbCrLf
    sSql = sSql & "(  cod_aplic,  t_doente, doente, t_episodio, episodio,   dt_ini_acto,    dt_fim_acto, "
    sSql = sSql & " cod_resp, cod_serv_req, cod_serv_exec, n_cred, cod_acto, n_analise, qtd, flg_realizado, user_cri, dt_cri, cod_agrup, "
    sSql = sSql & " n_mecan, n_req , cod_perfil, cod_ana_c, cod_ana_s, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, "
    sSql = sSql & " n_seq, n_credencial, n_acto,cod_u_saude, N_MECAN_EXT, req_cod_dom, req_km_dom, req_cod_post_dom, req_localidade_dom) "
    sSql = sSql & "VALUES ( 'SISLAB',"
    sSql = sSql & BL_TrataStringParaBD(TDoente) & ", " & BL_TrataStringParaBD(doente) & ","
    sSql = sSql & BL_TrataStringParaBD(tEpisodio) & ", " & BL_TrataStringParaBD(episodio) & ","
    sSql = sSql & BL_TrataDataParaBD(DtChega) & ", " & BL_TrataDataParaBD(DtChega) & ","
    sSql = sSql & resp_aux & "," & BL_TrataStringParaBD(CodServReq) & ", " & BL_TrataStringParaBD(CStr(centroCusto)) & ", "
    sSql = sSql & BL_TrataStringParaBD(NReqARS) & "," & BL_TrataStringParaBD(CodAnaGH) & ","
    sSql = sSql & BL_TrataStringParaBD(analise) & ", " & quantidade & ",'S','" & gCodUtilizador & "',sysdate,"
    sSql = sSql & BL_TrataStringParaBD(cod_agrup) & ", "
    If gLAB = "ICIL" Then
        sSql = sSql & BL_TrataStringParaBD(codMed) & ", " & NReq & ",'" & Cod_Perfil & "', '" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & Cod_Gr_Antibio & "','" & cod_antibio & "','" & Cod_Prova & "'," & IIf(NSeq = -1, "Null", NSeq) & "," & BL_TrataStringParaBD(NReqARS) & " "
    Else
        sSql = sSql & BL_TrataStringParaBD(n_mec) & ", " & NReq & ",'" & Cod_Perfil & "', '" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & Cod_Gr_Antibio & "','" & cod_antibio & "','" & Cod_Prova & "'," & IIf(NSeq = -1, "Null", NSeq) & "," & BL_TrataStringParaBD(NReqARS) & " "
    End If
    
    If n_acto > 0 Then
        sSql = sSql & ", " & n_acto & ","
    Else
        sSql = sSql & ",null, "
    End If
    sSql = sSql & BL_TrataStringParaBD(cod_u_saude) & ","
    sSql = sSql & BL_TrataStringParaBD(n_mecan_ext) & ","
    If codDom = mediComboValorNull Then
        sSql = sSql & " NULL, NULL, NULL, NULL"
    ElseIf codDom >= mediComboValorNull Then
        sSql = sSql & codDom & ", " & km & ","
        sSql = sSql & BL_TrataStringParaBD(codPostal) & ","
        sSql = sSql & BL_TrataStringParaBD(localidade)
    End If
    sSql = sSql & ")"
    
    registos = BG_ExecutaQuery_ADO(sSql)
    If registos <= 0 Then
        GoTo TrataErro
    Else
        GH_Nova_ANALISES_IN = 1
    End If
    
Exit Function
TrataErro:
    BG_LogFile_Erros "GH_Nova_Analises_IN: Erro a inserir an�lise no GH: " & sSql & "Integracao_GH", "Integracao_FACTUS", "GH_Nova_AnaliseS_IN", True
    GH_Nova_ANALISES_IN = -1
    Exit Function
    Resume Next
End Function

 Public Function GH_Analise_ja_facturada(n_req As String, ByVal situacao As String, episodio As String, codAna As String) As Boolean
    Dim sSql As String
    Dim rs_aux As New ADODB.recordset
    
    Select Case situacao
        Case gT_Urgencia
            situacao = "Urgencias"
        Case gT_Consulta
            situacao = "Consultas"
        Case gT_Internamento
            situacao = "Internamentos"
        Case gT_Externo
            situacao = "Consultas"
        Case gT_Acidente
            situacao = "Acidente"
        Case gT_Ambulatorio
            situacao = "Ambulatorio"
        Case gT_Cons_Inter
            situacao = "Cons-Inter"
        Case gT_Cons_Telef
            situacao = "Cons-Telef"
        Case gT_Consumos
            situacao = "Consumos"
        Case gT_Credenciais
            situacao = "Credenciais"
        Case gT_Diagnosticos
            situacao = "Diagnosticos"
        Case gT_Exame
            situacao = "Exame"
        Case gT_Fisio
            situacao = "Fisio"
        Case gT_HDI
            situacao = "Hosp-Dia"
        Case gT_Intervencao
            situacao = "Intervencao"
        Case gT_MCDT
            situacao = "Mcdt"
        Case gT_Plano_Oper
            situacao = "Plano-Oper"
        Case gT_Pre_Intern
            situacao = "Pre-Intern"
        Case gT_Prog_Cirugico
            situacao = "Prog-Cirurgico"
        Case gT_Protoc
            situacao = "Protoc"
        Case gT_Referenciacao
            situacao = "Referenciacao"
        Case gT_Reg_Oper
            situacao = "Reg-Oper"
        Case gT_Tratamentos
            situacao = "Tratamentos"
        Case Else
            situacao = ""
    End Select
    
    sSql = "SELECT * FROM sl_dados_fact WHERE t_episodio = " & BL_TrataStringParaBD(situacao) & " AND episodio = " & episodio
    sSql = sSql & " AND qtd = 1 AND n_analise = " & BL_TrataStringParaBD(codAna) & " AND n_req = " & n_req
    
    rs_aux.CursorLocation = adUseServer
    rs_aux.Open sSql, gConexao, adOpenStatic
    
    If rs_aux.RecordCount > 0 Then
        GH_Analise_ja_facturada = True
    ElseIf rs_aux.RecordCount = 0 Then
        GH_Analise_ja_facturada = False
    End If
    rs_aux.Close
    Set rs_aux = Nothing
 End Function
Public Function GH_Remove_ANALISES_IN(tEpisodio As String, _
                                       episodio As String, _
                                       TDoente As String, _
                                       doente As String, _
                                       CodAnaGH As String, _
                                       NReq As Long, _
                                       DtChega As String, _
                                       n_mec As String, _
                                       Optional DtFim As String, _
                                       Optional CodServReq As String, _
                                       Optional codServExec As String, _
                                       Optional NCred As String, _
                                       Optional CodResp As String, _
                                       Optional analise As String, _
                                       Optional codAgrup As String, _
                                       Optional CodPerfil As String, _
                                       Optional CodAnaC As String, _
                                       Optional codAnaS As String, _
                                       Optional CodMicro As String, _
                                       Optional CodGrAntibio As String, _
                                       Optional CodAntibio As String, _
                                       Optional CodProva As String, _
                                       Optional NSeqAnul As Long) As Integer
                                       
                                       
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim rs_aux As ADODB.recordset
    Dim i As Integer
    Dim lCount As Long
    Dim iRes As Integer
    Dim registos As Integer
    Dim sMsg As String
    Dim quantidade As Integer
    Dim rsSeq As ADODB.recordset
    Dim NSeq As Long
    On Error GoTo TrataErro
    
    ' Mapeia a situa��o do SISLAB para a GH.
    Select Case gLAB
        Case "CITO", "BIO", "GM"
            tEpisodio = "Consultas"
        Case Else
            Select Case tEpisodio
                Case gT_Urgencia
                    tEpisodio = "Urgencias"
                Case gT_Consulta
                    tEpisodio = "Consultas"
                Case gT_Internamento
                    tEpisodio = "Internamentos"
                Case gT_Externo
                    tEpisodio = "Consultas"
                Case gT_LAB
                    tEpisodio = "Consultas"
                Case "Ficha-ID"
                    tEpisodio = "Ficha-ID"
                Case gT_Prescricoes
                    tEpisodio = "Prescricoes"
                Case gT_Ambulatorio
                    tEpisodio = "Ambulatorio"
                Case gT_Acidente
                    tEpisodio = "Acidente"
                Case gT_Ambulatorio
                    tEpisodio = "Ambulatorio"
                Case gT_Cons_Inter
                    tEpisodio = "Cons-Inter"
                Case gT_Cons_Telef
                    tEpisodio = "Cons-Telef"
                Case gT_Consumos
                    tEpisodio = "Consumos"
                Case gT_Credenciais
                    tEpisodio = "Credenciais"
                Case gT_Diagnosticos
                    tEpisodio = "Diagnosticos"
                Case gT_Exame
                    tEpisodio = "Exame"
                Case gT_Fisio
                    tEpisodio = "Fisio"
                Case gT_HDI
                    tEpisodio = "Hosp-Dia"
                Case gT_Intervencao
                    tEpisodio = "Intervencao"
                Case gT_MCDT
                    tEpisodio = "Mcdt"
                Case gT_Plano_Oper
                    tEpisodio = "Plano-Oper"
                Case gT_Pre_Intern
                    tEpisodio = "Pre-Intern"
                Case gT_Prog_Cirugico
                    tEpisodio = "Prog-Cirurgico"
                Case gT_Protoc
                    tEpisodio = "Protoc"
                Case gT_Referenciacao
                    tEpisodio = "Referenciacao"
                Case gT_Reg_Oper
                    tEpisodio = "Reg-Oper"
                Case gT_Tratamentos
                    tEpisodio = "Tratamentos"
                Case Else
                    tEpisodio = tEpisodio
            End Select
    End Select
    
    quantidade = "-1"
    
    If gLAB = "HMP" Then
        gCodServExec = "52200"
    End If
    If gLAB = "SCMVC" Then
        CodServReq = "233"
    End If
    
    'Gera sequencial SEQ_DADOS_LAB do GH
    NSeq = -1
    Set rsSeq = New ADODB.recordset
    rsSeq.CursorLocation = adUseServer
    rsSeq.CursorType = adOpenStatic
    rsSeq.ActiveConnection = gConexao
    rsSeq.Source = "select seq_dados_lab.nextval n_seq from dual"
    rsSeq.Open
    If rsSeq.RecordCount > 0 Then
        NSeq = rsSeq!n_seq
    Else
        MsgBox "Erro a gerar a sequencia SEQ_DADOS_LAB!"
        BG_LogFile_Erros "GH_Nova_Remove_IN: Erro a gerar a sequencia SEQ_DADOS_LAB! " & "Integracao_GH"
        GH_Remove_ANALISES_IN = -1
        Exit Function
    End If
    rsSeq.Close
    Set rsSeq = Nothing

    sSql = "INSERT INTO sl_dados_fact " & vbCrLf & _
          "( " & vbCrLf & _
          "     cod_aplic,  t_doente, " & vbCrLf & _
          "     doente,     t_episodio, " & vbCrLf & _
          "     episodio,   dt_ini_acto,    dt_fim_acto, " & vbCrLf & _
          "     cod_resp,   cod_serv_req,   cod_serv_exec,   " & vbCrLf & _
          "     n_cred,     cod_acto,       n_analise,    " & vbCrLf & _
          "     qtd,        flg_realizado, user_cri, dt_cri, " & vbCrLf & _
          "     n_mecan,    n_req   ,cod_agrup,  cod_perfil, cod_ana_c, cod_ana_s,  cod_micro, cod_gr_antibio,cod_antibio, cod_prova, n_seq, n_seq_anul " & vbCrLf & _
          ") " & vbCrLf & _
          "VALUES " & vbCrLf & _
          "( " & vbCrLf & _
          "     'SISLAB'," & _
          "     " & BL_TrataStringParaBD(TDoente) & ", " & BL_TrataStringParaBD(doente) & "," & vbCrLf & _
          "     " & BL_TrataStringParaBD(tEpisodio) & ", " & BL_TrataStringParaBD(episodio) & "," & vbCrLf & _
          "     " & BL_TrataDataParaBD(DtChega) & ", " & BL_TrataDataParaBD(DtChega) & "," & vbCrLf & _
          "     " & BL_TrataStringParaBD(CodResp) & "," & BL_TrataStringParaBD(CodServReq) & ", " & BL_TrataStringParaBD(CStr(gCodServExec)) & ", " & vbCrLf & _
          "     " & "Null" & "," & BL_TrataStringParaBD(CodAnaGH) & "," & vbCrLf & _
          "     " & BL_TrataStringParaBD(analise) & ", " & quantidade & ",'S','" & gCodUtilizador & "',sysdate," & _
          "     " & BL_TrataStringParaBD(n_mec) & ", " & NReq & ",'" & codAgrup & "','" & CodPerfil & "', '" & CodAnaC & "','" & codAnaS & "','" & CodMicro & "','" & CodGrAntibio & "','" & CodAntibio & "','" & CodProva & "'," & IIf(NSeq = -1, "Null", NSeq) & "," & IIf(NSeqAnul = -1, "Null", NSeqAnul) & ")"
    
    
    registos = BG_ExecutaQuery_ADO(sSql)
    If registos <= 0 Then
        GoTo TrataErro
    Else
        GH_Remove_ANALISES_IN = 1
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "GH_Nova_Remove_IN: Erro a inserir an�lise no GH: " & sSql, "Integracao_FACTUS", "GH_Remove_Analises_in", True
    GH_Remove_ANALISES_IN = -1
    Exit Function
End Function

Public Function GH_Factura_Microrg(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                t_utente As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                cod_micro As String, _
                                                Prova As String, _
                                                centroCusto As String) As Boolean
    'fgoncalves: 03.11.2006
    'nova fun��o para facturar microrganismos
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    GH_Factura_Microrg = False
    
    If BL_HandleNull(Prova, "") <> "" Then
    
                
        'prova: campo para facturar diferentes rubricas(identifica��es de microrganismo) conforme id de prova
        Set RsFact = New ADODB.recordset
        RsFact.CursorLocation = adUseServer
        RsFact.CursorType = adOpenStatic
        
        sql = "SELECT cod_rubr from sl_tbf_t_prova where cod_t_prova = '" & Prova & "'"
    
        RsFact.Open sql, gConexao
        
        If RsFact.RecordCount > 0 Then
            While Not RsFact.EOF
                If (BL_HandleNull(RsFact!cod_rubr, "") <> "") Then
                    If (GH_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                t_utente, _
                                                processo, _
                                                RsFact!cod_rubr, _
                                                CLng(n_req), _
                                                dt_chega, gNumMecanMedExec, , cod_proven, CStr(gCodServExec), , , cod_micro, "1" _
                                                , , , , , , , , , , centroCusto, Empty, "", "", "") = 1) Then
                    
                                                            
                        GH_Factura_Microrg = True
                    End If
                End If
                RsFact.MoveNext
            Wend
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsFact.Close
        Set RsFact = Nothing
    Else
        sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro) VALUES (" & _
                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "')"
        BG_ExecutaQuery_ADO sql
    End If

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : GH_Factura_Microrg (Integracao_FACTUS) -> " & Err.Description)
            GH_Factura_Microrg = False
            Exit Function
    End Select
End Function


Public Function GH_Factura_Antibiograma(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                t_utente As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                CodGrAntibio As String, _
                                                Metodo_Tsq As String, _
                                                cod_micro As String, _
                                                seq_realiza As String, _
                                                centroCusto As String) As Boolean
    'soliveira: 23.09.2006
    'nova fun��o para facturar antibiograma do microrganismo
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    GH_Factura_Antibiograma = False
    
    'Verifica se tem grupo de antibi�ticos registados
    If CodGrAntibio <> "" Then
    
        If UCase(HIS.nome) = UCase("GH") Then
            '-------------------------------------------------------------
            'verifica se a an�lise j� foi inserida no DADOS_FACT
            If GH_Analise_ja_facturada(n_req, t_sit, n_epis, CodGrAntibio) = True Then
                'A an�lise n�o � para facturar, nem para colocar como j� facturada
                GH_Factura_Antibiograma = False
                Exit Function
            End If
            '-------------------------------------------------------------
        End If
    
        Dim RsMetodoTsq As ADODB.recordset
        
        'a rubrica associada a carta esta registada na codifca��o de grupo de antibioticos
        Set RsMetodoTsq = New ADODB.recordset
        'selecciona o metodo e a rubrica associdada a carta na codifica��o de cartas (grupo de antibioticos)
        sql = " SELECT cod_metodo,sl_gr_antibio.cod_rubr " & _
              " FROM sl_gr_antibio" & _
              " WHERE cod_gr_antibio = " & BL_TrataStringParaBD(CodGrAntibio)
        RsMetodoTsq.CursorLocation = adUseServer
        RsMetodoTsq.CursorType = adOpenStatic
        RsMetodoTsq.Open sql, gConexao
        If (RsMetodoTsq.RecordCount > 0) Then
            'Os m�todos E-Test e BK s�o analisados no fim
            If BL_HandleNull(RsMetodoTsq!cod_metodo, "") <> "2" Or BL_HandleNull(RsMetodoTsq!cod_metodo, "") <> "3" Then
                If (BL_HandleNull(RsMetodoTsq!cod_rubr, "") <> "") Then
                    
                    'insere carta no sonho
                    If (GH_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                t_utente, _
                                                processo, _
                                                RsMetodoTsq!cod_rubr, _
                                                CLng(n_req), _
                                                dt_chega, gNumMecanMedExec, dt_chega, _
                                                cod_proven, CStr(gCodServExec), , , _
                                                CodGrAntibio, "", "1", , , , , , , , , centroCusto, Empty, "", "", "") = 1) Then
                                           
                        GH_Factura_Antibiograma = True
                    End If
                
                Else
                    sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_gr_antibio) VALUES (" & _
                            n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & CodGrAntibio & "')"
                    BG_ExecutaQuery_ADO sql
                End If
            End If
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_gr_antibio) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & CodGrAntibio & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsMetodoTsq.Close
        Set RsMetodoTsq = Nothing
        
    'Se n�o tem carta, verifica se tem o m�todo tsq a facturar,por defeito, para este microrganismo
    ElseIf BL_HandleNull(Metodo_Tsq, "") <> "" Then
            
        Set RsFact = New ADODB.recordset
        RsFact.CursorLocation = adUseServer
        RsFact.CursorType = adOpenStatic
        
        sql = "SELECT " & vbCrLf & _
              "     cod_rubr " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_tbf_metodo_tsq " & vbCrLf & _
              "WHERE " & vbCrLf & _
              " cod_metodo_tsq = " & BL_TrataStringParaBD(Metodo_Tsq)
    
        RsFact.Open sql, gConexao
                    
        If RsFact.RecordCount > 0 Then
            While Not RsFact.EOF
                If BL_HandleNull(RsFact!cod_rubr, "") <> "" Then
                    If (GH_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                t_utente, _
                                                processo, _
                                                RsFact!cod_rubr, _
                                                CLng(n_req), _
                                                dt_chega, gNumMecanMedExec, dt_chega, _
                                                cod_proven, CStr(gCodServExec), , , _
                                                cod_agrup, "1", , , , , , , , , , centroCusto, Empty, "", "", "") = 1) Then
                                           
                        GH_Factura_Antibiograma = True
                    End If
                End If
                RsFact.MoveNext
            Wend
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro,cod_gr_antibio) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & Metodo_Tsq & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsFact.Close
        Set RsFact = Nothing

        If BL_HandleNull(Metodo_Tsq, "") = "" Then
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro,cod_gr_antibio) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & CodGrAntibio & "')"
            BG_ExecutaQuery_ADO sql

        End If
    End If


Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : GH_Factura_Antibiograma (Integracao_FACTUS) -> " & Err.Description)
            GH_Factura_Antibiograma = False
            Exit Function
    End Select
End Function



Public Function GH_Factura_Antibioticos(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                t_utente As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                CodGrAntibio As String, _
                                                Metodo_Tsq As String, _
                                                cod_micro As String, _
                                                seq_realiza As String, _
                                                centroCusto As String) As Boolean
    Dim RsFactAntib As ADODB.recordset
    Dim sql As String


    'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
    'n�o � poss�vel colocar estes antibioticos em grupos de antibioticos? op��o de cima
    Set RsFactAntib = New ADODB.recordset
    sql = " SELECT sl_res_tsq.cod_antib, cod_metodo, cod_rubr " & _
          " from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
          " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
          " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
          " and seq_realiza = " & seq_realiza & " and n_res = " & 1 & _
          " and cod_micro = " & BL_TrataStringParaBD(cod_micro) & "" & _
          " and cod_metodo in ( 2,3) "
    RsFactAntib.CursorType = adOpenStatic
    RsFactAntib.CursorLocation = adUseServer
    RsFactAntib.Open sql, gConexao
    If RsFactAntib.RecordCount > 0 Then
        While Not RsFactAntib.EOF
            If (GH_Nova_ANALISES_IN(t_sit, _
                                    n_epis, _
                                    t_utente, _
                                    processo, _
                                    RsFactAntib!cod_rubr, _
                                    CLng(n_req), _
                                    dt_chega, gNumMecanMedExec, dt_chega, _
                                    cod_proven, CStr(gCodServExec), , , _
                                    cod_agrup, cod_agrup, "1", , , , , , , , , centroCusto, Empty, "", "", "") = 1) Then

                GH_Factura_Antibioticos = True
            End If
            RsFactAntib.MoveNext
        Wend
    End If
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : GH_Factura_Antibioticos (Integracao_FACTUS) -> " & Err.Description)
            GH_Factura_Antibioticos = False
            Exit Function
    End Select
End Function

' pferreira 2010.04.19
' Executa procedimento <preenche_listagem_facturacao>.
Public Function FACTUS_PreencheListagemFacturacao(data_inicial As Variant, data_final As Variant, criterio_data As Variant, codigo_sala As Variant, codigo_entidade As Variant, ByVal tipo_isencao As Variant, ByVal numero_factura As Variant, codigo_computador As Variant, ByRef mensagem_retorno As Variant) As Boolean

    ' Variables Declarations
    ' ======================
    Dim cmdResult As New ADODB.Command          ' Command procedure object
    Dim pmtInitialDate As ADODB.Parameter       ' Parameter procedure object for initial date
    Dim pmtFinalDate As ADODB.Parameter         ' Parameter procedure object for final date
    Dim pmtCriterionDate As ADODB.Parameter     ' Parameter procedure object for criterion date
    Dim pmtRoomCode As ADODB.Parameter          ' Parameter procedure object for room code
    Dim pmtEntityCode As ADODB.Parameter        ' Parameter procedure object for entity code
    Dim pmtComputerCode As ADODB.Parameter      ' Parameter procedure object for computer code
    Dim pmtExemptionType As ADODB.Parameter     ' Parameter procedure object for exemption type
    Dim pmtBillingNumber As ADODB.Parameter     ' Parameter procedure object for billing number
    Dim pmtMessageFeeback As ADODB.Parameter    ' Parameter procedure object for message feedback
                 
    On Error GoTo ErrorHandler
    
    ' Begin database transaction
    ' ==========================
    gConexaoSecundaria.BeginTrans
    
    ' Set command procedure properties
    ' ================================
    With cmdResult
        .ActiveConnection = gConexaoSecundaria                  ' Set connection string
        .CommandText = "pck_sl_billing.fills_billing_table"     ' Set procedure name
        .CommandType = adCmdStoredProc                          ' Set command type
    End With
    
    ' Set parameters procedure properties
    ' ===================================
    Set pmtInitialDate = cmdResult.CreateParameter("in_initial_date", adDate, adParamInput, 7)          ' Set initial date properties
    Set pmtFinalDate = cmdResult.CreateParameter("in_final_date", adDate, adParamInput, 7)              ' Set final date properties
    Set pmtCriterionDate = cmdResult.CreateParameter("in_criterion_date", adVarChar, adParamInput, 15)  ' Set criterion date properties
    Set pmtRoomCode = cmdResult.CreateParameter("in_room_code", adVarChar, adParamInput, 300)           ' Set room code properties
    Set pmtEntityCode = cmdResult.CreateParameter("in_entity_code", adVarChar, adParamInput, 300)       ' Set entity code properties
    Set pmtComputerCode = cmdResult.CreateParameter("in_computer_code", adVarChar, adParamInput, 40)    ' Set computer code properties
    Set pmtExemptionType = cmdResult.CreateParameter("in_exemption_type", adVarChar, adParamInput, 40)  ' Set exemption type properties
    Set pmtBillingNumber = cmdResult.CreateParameter("in_billing_number", adVarChar, adParamInput, 40)  ' Set billing number properties
    Set pmtMessageFeeback = cmdResult.CreateParameter("out_message", adVarChar, adParamOutput, 4000)    ' Set message feedback properties
      
    ' Add parameters to command procedure object
    ' ==========================================
    cmdResult.Parameters.Append pmtInitialDate      ' Add initial date parameter
    cmdResult.Parameters.Append pmtFinalDate        ' Add final date parameter
    cmdResult.Parameters.Append pmtCriterionDate    ' Add criterion date parameter
    cmdResult.Parameters.Append pmtRoomCode         ' Add room code parameter
    cmdResult.Parameters.Append pmtEntityCode       ' Add entity code parameter
    cmdResult.Parameters.Append pmtComputerCode     ' Add computer code parameter
    cmdResult.Parameters.Append pmtExemptionType    ' Add exemption type parameter
    cmdResult.Parameters.Append pmtBillingNumber    ' Add billing number parameter
    cmdResult.Parameters.Append pmtMessageFeeback   ' Add message feedback parameter
       
    ' Add parameters inputs to command procedure object
    ' =================================================
    cmdResult.Parameters.item("in_initial_date").value = data_inicial           ' Add parameters input (initial date)
    cmdResult.Parameters.item("in_final_date").value = data_final               ' Add parameters input (final date)
    cmdResult.Parameters.item("in_criterion_date").value = criterio_data        ' Add parameters input (criterion date)
    cmdResult.Parameters.item("in_room_code").value = codigo_sala               ' Add parameters input (room code)
    cmdResult.Parameters.item("in_entity_code").value = codigo_entidade         ' Add parameters input (entity code)
    cmdResult.Parameters.item("in_computer_code").value = codigo_computador     ' Add parameters input (computer code)
    cmdResult.Parameters.item("in_exemption_type").value = tipo_isencao         ' Add parameters input (exemption type)
    cmdResult.Parameters.item("in_billing_number").value = numero_factura       ' Add parameters input (exemption type)
    
    ' Execute command procedure
    ' =========================
    cmdResult.Execute
    
    ' Get parameters output from command procedure
    ' ============================================
    mensagem_retorno = BL_HandleNull(cmdResult.Parameters.item("out_message").value, Empty)      ' Add message feedback parameter
    
    ' Clear memory of the used objects
    ' ================================
    Set cmdResult = Nothing             ' Clear command procedure object
    Set pmtInitialDate = Nothing        ' Clear parameter procedure object for initial date
    Set pmtFinalDate = Nothing          ' Clear parameter procedure object for final date
    Set pmtCriterionDate = Nothing      ' Clear parameter procedure object for criterion date
    Set pmtRoomCode = Nothing           ' Clear parameter procedure object for room code
    Set pmtEntityCode = Nothing         ' Clear parameter procedure object for entity code
    Set pmtComputerCode = Nothing       ' Clear parameter procedure object for computer code
    Set pmtExemptionType = Nothing      ' Clear parameter procedure object for exemption type
    Set pmtBillingNumber = Nothing      ' Clear parameter procedure object for billing number
    Set pmtMessageFeeback = Nothing     ' Clear parameter procedure object for message feedback
    
    ' Function execution run well (if execution status succeeded), therefore return true and exit function
    ' ====================================================================================================
    FACTUS_PreencheListagemFacturacao = (mensagem_retorno = Empty)
    
    ' Commit database transaction
    ' ===========================
    If (gConexaoSecundaria.state = adStateOpen) Then: gConexaoSecundaria.CommitTrans
    
    Exit Function

ErrorHandler:

    ' Clear memory of the used objects
    ' ================================
    Set cmdResult = Nothing             ' Clear command procedure object
    Set pmtInitialDate = Nothing        ' Clear parameter procedure object for initial date
    Set pmtFinalDate = Nothing          ' Clear parameter procedure object for final date
    Set pmtCriterionDate = Nothing      ' Clear parameter procedure object for criterion date
    Set pmtRoomCode = Nothing           ' Clear parameter procedure object for room code
    Set pmtEntityCode = Nothing         ' Clear parameter procedure object for entity code
    Set pmtComputerCode = Nothing       ' Clear parameter procedure object for computer code
    Set pmtExemptionType = Nothing      ' Clear parameter procedure object for exemption type
    Set pmtBillingNumber = Nothing      ' Clear parameter procedure object for billing number
    Set pmtMessageFeeback = Nothing     ' Clear parameter procedure object for message feedback
  
    ' Function execution run wrong therefore return false and exit function
    ' =====================================================================
    FACTUS_PreencheListagemFacturacao = False
    
    ' Rollack database transaction
    ' ============================
     If (gConexaoSecundaria.state = adStateOpen) Then: gConexaoSecundaria.RollbackTrans
    
    Exit Function
    
End Function

' ---------------------------------------------------------------------

' DADO UTENTE E EPISODIO DEVOLVE CODIGO SERVICO

' ---------------------------------------------------------------------
Public Function GH_RetornaServicoEpisodio(t_utente As String, Utente As String, t_sit As String, n_epis As String) As String
    On Error GoTo TrataErro
    
    Dim CmdServ As New ADODB.Command
    Dim PmtServ As ADODB.Parameter
    Dim PmtVTDoente As ADODB.Parameter
    Dim PmtVDoente As ADODB.Parameter
    Dim PmtVTEpisodio As ADODB.Parameter
    Dim PmtVEpisodio As ADODB.Parameter
        
    On Error GoTo TrataErro
    t_sit = GH_RetornaDescrEpisodio(t_sit)
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdServ
        .ActiveConnection = gConexao
        .CommandText = "SLP_DEVOLVE_SERVICO_GH"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtServ = CmdServ.CreateParameter("COD_SERV", adVarChar, adParamOutput, 10)
    Set PmtVTDoente = CmdServ.CreateParameter("VT_DOENTE", adVarChar, adParamInput, 30)
    Set PmtVDoente = CmdServ.CreateParameter("VDOENTE", adVarChar, adParamInput, 30)
    Set PmtVTEpisodio = CmdServ.CreateParameter("VT_EPISODIO", adVarChar, adParamInput, 30)
    Set PmtVEpisodio = CmdServ.CreateParameter("VEPISODIO", adVarChar, adParamInput, 30)
    CmdServ.Parameters.Append PmtVTDoente
    CmdServ.Parameters.Append PmtVDoente
    CmdServ.Parameters.Append PmtVTEpisodio
    CmdServ.Parameters.Append PmtVEpisodio
    CmdServ.Parameters.Append PmtServ
    
    CmdServ.Parameters("VT_DOENTE") = t_utente
    CmdServ.Parameters("VDOENTE") = Utente
    CmdServ.Parameters("VT_EPISODIO") = t_sit
    CmdServ.Parameters("VEPISODIO") = n_epis
    CmdServ.Execute
    GH_RetornaServicoEpisodio = Trim(BL_HandleNull(CmdServ.Parameters.item("COD_SERV").value, ""))
Exit Function
TrataErro:
    GH_RetornaServicoEpisodio = ""
    BG_LogFile_Erros "Integracao_FACTUS: GH_RetornaServicoEpisodio: " & Err.Description, "Integracao_FACTUS", "GH_RetornaServicoEpisodio", False
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------

' DADO O TIPO DE EPISODIO SISLAB RETORNA TIPO DE EPISODIO GH

' ---------------------------------------------------------------------
Public Function GH_RetornaDescrEpisodio(situacao As String)
    On Error GoTo TrataErro
    Select Case situacao
        Case gT_Urgencia
            GH_RetornaDescrEpisodio = "Urgencias"
        Case gT_Consulta
            GH_RetornaDescrEpisodio = "Consultas"
        Case gT_Internamento
            GH_RetornaDescrEpisodio = "Internamentos"
        Case gT_Ficha_ID
            GH_RetornaDescrEpisodio = "Ficha-ID"
        Case gT_Ambulatorio
            GH_RetornaDescrEpisodio = "Ambulatorio"
        Case gT_Prescricoes
            GH_RetornaDescrEpisodio = "Prescricoes"
        
        Case gT_Acidente
            GH_RetornaDescrEpisodio = "Acidente"
        Case gT_Ambulatorio
            GH_RetornaDescrEpisodio = "Ambulatorio"
        Case gT_Cons_Inter
            GH_RetornaDescrEpisodio = "Cons-Inter"
        Case gT_Cons_Telef
            GH_RetornaDescrEpisodio = "Cons-Telef"
        Case gT_Consumos
            GH_RetornaDescrEpisodio = "Consumos"
        Case gT_Credenciais
            GH_RetornaDescrEpisodio = "Credenciais"
        Case gT_Diagnosticos
            GH_RetornaDescrEpisodio = "Diagnosticos"
        Case gT_Exame
            GH_RetornaDescrEpisodio = "Exame"
        Case gT_Fisio
            GH_RetornaDescrEpisodio = "Fisio"
        Case gT_HDI
            GH_RetornaDescrEpisodio = "Hosp-Dia"
        Case gT_Intervencao
            GH_RetornaDescrEpisodio = "Intervencao"
        Case gT_MCDT
            GH_RetornaDescrEpisodio = "Mcdt"
        Case gT_Plano_Oper
            GH_RetornaDescrEpisodio = "Plano-Oper"
        Case gT_Pre_Intern
            GH_RetornaDescrEpisodio = "Pre-Intern"
        Case gT_Prog_Cirugico
            GH_RetornaDescrEpisodio = "Prog-Cirurgico"
        Case gT_Protoc
            GH_RetornaDescrEpisodio = "Protoc"
        Case gT_Referenciacao
            GH_RetornaDescrEpisodio = "Referenciacao"
        Case gT_Reg_Oper
            GH_RetornaDescrEpisodio = "Reg-Oper"
        Case gT_Tratamentos
            GH_RetornaDescrEpisodio = "Tratamentos"
        Case Else
            GH_RetornaDescrEpisodio = ""
    End Select

Exit Function
TrataErro:
    GH_RetornaDescrEpisodio = ""
    BG_LogFile_Erros "Integracao_FACTUS: GH_RetornaDescrEpisodio: " & Err.Description, "Integracao_FACTUS", "GH_RetornaDescrEpisodio", False
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------

' DADO O EPISODIO DE INTERNAMENTO RETORNA VALENCIA E TAMBEM CAMA

' ---------------------------------------------------------------------
Public Function GH_RetornaValencia(t_doente As String, doente As String, n_epis As String, ByRef cama As String) As String
    Dim sSql As String
    Dim rsVal As New ADODB.recordset
    Dim iBD_Aberta As Integer
    On Error Resume Next
    GH_RetornaValencia = ""
    iBD_Aberta = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    If iBD_Aberta = 0 Then
        Exit Function
    End If
    sSql = " SELECT * FROM sd_serv_int WHERE t_doente =  " & BL_TrataStringParaBD(t_doente) & " AND doente = "
    sSql = sSql & BL_TrataStringParaBD(doente) & " AND n_int = " & BL_TrataStringParaBD(n_epis)
    rsVal.CursorLocation = adUseServer
    rsVal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVal.Open sSql, gConnHIS
    If (rsVal.RecordCount > 0) Then
        GH_RetornaValencia = BL_HandleNull(rsVal!cod_serv_valencia, "")
        cama = BL_HandleNull(rsVal!cama, "")
    End If
    rsVal.Close
    Set rsVal = Nothing
    BL_Fecha_conexao_HIS
    
End Function

' ---------------------------------------------------------------------

' DADO ACTO MEDICO RETORNA DESCRICAO

' ---------------------------------------------------------------------
Public Function GH_DevolveDescrActoMed(n_act_med As String) As String
    On Error GoTo TrataErro
    
    Dim CmdAct As New ADODB.Command
    Dim PmtDescr As ADODB.Parameter
    Dim PmtVNAct As ADODB.Parameter
        
    On Error GoTo TrataErro
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdAct
        .ActiveConnection = gConexao
        .CommandText = "SLP_DEVOLVE_DESCR_ACT_MED_GH"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtDescr = CmdAct.CreateParameter("DESCR_ACT_MED", adVarChar, adParamOutput, 80)
    Set PmtVNAct = CmdAct.CreateParameter("N_ACT_MED", adVarChar, adParamInput, 30)
    CmdAct.Parameters.Append PmtVNAct
    CmdAct.Parameters.Append PmtDescr
    
    CmdAct.Parameters("N_ACT_MED") = n_act_med
    CmdAct.Execute
    GH_DevolveDescrActoMed = Trim(BL_HandleNull(CmdAct.Parameters.item("DESCR_ACT_MED").value, ""))
Exit Function
TrataErro:
    GH_DevolveDescrActoMed = ""
    BG_LogFile_Erros "Integracao_FACTUS: GH_DevolveDescrActoMed: " & Err.Description, "Integracao_FACTUS", "GH_DevolveDescrActoMed", False
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------

'  QUANDO UTILIZADOR QUER FACTURAR NOVAMENTE ENTAO ENVIA O QUE JA EXISTIA PARA HISTORICO

' --------------------------------------------------------------------------------------
Public Function GH_EnviaParaHistorico(n_req As String) As Boolean
    Dim sSql As String
    Dim rsGH As New ADODB.recordset
    Dim iRegistos As Integer
    On Error GoTo TrataErro
    GH_EnviaParaHistorico = False
    
    sSql = "SELECT * FROM sl_dados_fact WHERE n_req = " & n_req
    rsGH.CursorLocation = adUseServer
    rsGH.CursorType = adOpenStatic
    rsGH.Source = sSql
    rsGH.ActiveConnection = gConexao
    rsGH.Open
    If rsGH.RecordCount > 0 Then
        While Not rsGH.EOF
            sSql = "INSERT INTO sl_dados_fact_h (cod_aplic , t_doente, doente, t_episodio, episodio, dt_ini_acto,"
            sSql = sSql & " dt_fim_acto, cod_resp, cod_serv_req, cod_serv_exec, cod_acto, qtd, flg_realizado, "
            sSql = sSql & " n_cred, n_analise, user_cri, dt_cri, n_mecan, n_req, cod_agrup, Cod_Perfil, cod_ana_c,"
            sSql = sSql & " cod_ana_s, cod_micro, Cod_Gr_Antibio, cod_antibio, Cod_Prova, n_seq, n_seq_anul, "
            sSql = sSql & " n_credencial, dt_hist, user_hist) VALUES ("
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_aplic, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!t_doente, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!doente, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!t_episodio, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!episodio, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsGH!dt_ini_acto, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsGH!dt_fim_acto, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_resp, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_serv_req, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_serv_exec, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_acto, "")) & ", "
            sSql = sSql & BL_HandleNull(rsGH!qtd, "1") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!flg_realizado, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!n_cred, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!n_analise, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!user_cri, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsGH!dt_cri, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!n_mecan, "")) & ", "
            sSql = sSql & BL_HandleNull(rsGH!n_req, "null") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_agrup, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!Cod_Perfil, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_ana_c, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_ana_s, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_micro, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!Cod_Gr_Antibio, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!cod_antibio, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!Cod_Prova, "")) & ", "
            sSql = sSql & BL_HandleNull(rsGH!n_seq, "null") & ", "
            sSql = sSql & BL_HandleNull(rsGH!n_seq_anul, "null") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsGH!n_credencial, "")) & ", "
            sSql = sSql & "sysdate, "
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ") "
            
            iRegistos = BG_ExecutaQuery_ADO(sSql)
            If iRegistos < 1 Then
                GoTo TrataErro
            End If
            rsGH.MoveNext
        Wend
        
        'APAGA DA SL_DADOS_FACT
        sSql = "DELETE FROM sl_Dados_fact WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 1 Then
            GoTo TrataErro
        End If
        
        'ALTERA ESTADO NA SL_REALIZA
        sSql = "UPDATE sl_realiza SET flg_facturado = 0 WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 0 Then
            GoTo TrataErro
        End If
        
        'ALTERA ESTADO NA SL_MARCACOES
        sSql = "UPDATE sl_marcacoes SET flg_facturado = 0 WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 0 Then
            GoTo TrataErro
        End If
        
        End If
    rsGH.Close
    Set rsGH = Nothing
    GH_EnviaParaHistorico = True
Exit Function
TrataErro:
    GH_EnviaParaHistorico = False
    BG_LogFile_Erros "GH_EnviaParaHistorico: " & Err.Description & " " & sSql, "Integracao_FACTUS", "GH_EnviaParaHistorico", False
    Exit Function
End Function

' -----------------------------------------------------------------------------------------------------

'  VERIFICA SE REQUISI��O + ANALISE TEM ALGUMA PRESCRI��O ASSOCIADA, SE TIVER ENVIA COM ESSE EPIS�DIO

' ------------------------------------------------------------------------------------------------------
Private Sub GH_MapeiaEpisodios(ByVal n_req As String, ByVal cod_agrup As String, ByRef tEpisodio As String, ByRef episodio As String)
    Dim sSql As String
    Dim rsGH As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_req_episodio WHERE n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
    rsGH.CursorLocation = adUseServer
    rsGH.CursorType = adOpenStatic
    rsGH.ActiveConnection = gConexao
    rsGH.Source = sSql
    rsGH.Open
    If rsGH.RecordCount > 0 Then
        episodio = BL_HandleNull(rsGH!n_epis, "")
        tEpisodio = BL_HandleNull(rsGH!t_sit, "")
    Else
        tEpisodio = ""
        episodio = ""
    End If
    rsGH.Close
    Set rsGH = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "GH_MapeiaEpisodios: " & Err.Description & " " & sSql, "Integracao_FACTUS", "GH_MapeiaEpisodios", False
    Exit Sub
End Sub

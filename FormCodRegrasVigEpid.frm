VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FormCodRegrasVigEpid 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCodRegrasVigEpid"
   ClientHeight    =   9600
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   12045
   Icon            =   "FormCodRegrasVigEpid.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9600
   ScaleWidth      =   12045
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcDoenca_SNV 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6960
      TabIndex        =   85
      Top             =   1200
      Width           =   735
   End
   Begin VB.CheckBox CkSuspeita 
      Appearance      =   0  'Flat
      Caption         =   "Apresenta Suspeita de Colon."
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   8040
      TabIndex        =   81
      Top             =   1080
      Width           =   2415
   End
   Begin VB.TextBox EcDescrExcel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   74
      Top             =   1200
      Width           =   4095
   End
   Begin VB.TextBox EcNumClasses 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   7200
      MaxLength       =   2
      TabIndex        =   72
      Top             =   720
      Width           =   495
   End
   Begin VB.CheckBox CkNaoAvaliarSesib 
      Appearance      =   0  'Flat
      Caption         =   "N�o AvaliarSensib"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   10440
      TabIndex        =   71
      Top             =   720
      Width           =   1695
   End
   Begin VB.CheckBox CkMostrarMicro 
      Appearance      =   0  'Flat
      Caption         =   "Apresentar Esp�cie Bact."
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   8040
      TabIndex        =   70
      Top             =   750
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipo Regra"
      Height          =   615
      Left            =   7800
      TabIndex        =   51
      Top             =   0
      Width           =   2775
      Begin VB.OptionButton OptTipoRegra 
         Caption         =   "Microrganismos"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   53
         Top             =   240
         Width           =   1455
      End
      Begin VB.OptionButton OptTipoRegra 
         Caption         =   "An�lises"
         Height          =   255
         Index           =   1
         Left            =   1680
         TabIndex        =   52
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.TextBox EcTipoRegra 
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   49
      Top             =   10800
      Width           =   855
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   120
      TabIndex        =   36
      Top             =   8640
      Width           =   11805
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   42
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   41
         Top             =   195
         Width           =   2175
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   40
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   39
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   38
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcHoraCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   31
      Top             =   12120
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcHoraAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   30
      Top             =   12480
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   29
      Top             =   12120
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   28
      Top             =   12120
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   27
      Top             =   12480
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   26
      Top             =   11400
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.CheckBox CkInvisivel 
      Appearance      =   0  'Flat
      Caption         =   "Cancelado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   10680
      TabIndex        =   25
      Top             =   240
      Width           =   1335
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      Height          =   1590
      Left            =   120
      TabIndex        =   9
      Top             =   6960
      Width           =   11805
   End
   Begin TabDlg.SSTab SSTab 
      Height          =   5295
      Left            =   120
      TabIndex        =   8
      Top             =   1560
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   9340
      _Version        =   393216
      Style           =   1
      Tabs            =   6
      Tab             =   2
      TabsPerRow      =   9
      TabHeight       =   520
      TabCaption(0)   =   "Microrganismos && Antibi�ticos"
      TabPicture(0)   =   "FormCodRegrasVigEpid.frx":000C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Label1(4)"
      Tab(0).Control(1)=   "Label1(5)"
      Tab(0).Control(2)=   "Label1(6)"
      Tab(0).Control(3)=   "Label1(10)"
      Tab(0).Control(4)=   "Label1(11)"
      Tab(0).Control(5)=   "EcListaMicrorg"
      Tab(0).Control(6)=   "EcListaAntib"
      Tab(0).Control(7)=   "EcListaSensib"
      Tab(0).Control(8)=   "EcCodGrMicro"
      Tab(0).Control(9)=   "EcDescrGrMicro"
      Tab(0).Control(10)=   "BtPesqRapGrMicro"
      Tab(0).Control(11)=   "BtPesqRapSubClasseAntibio"
      Tab(0).Control(12)=   "EcDescrSubClasseAntibio"
      Tab(0).Control(13)=   "EcCodSubClasseAntibio"
      Tab(0).ControlCount=   14
      TabCaption(1)   =   "Produtos"
      TabPicture(1)   =   "FormCodRegrasVigEpid.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label1(7)"
      Tab(1).Control(1)=   "EcListaProdutos"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Resultados An�lises"
      TabPicture(2)   =   "FormCodRegrasVigEpid.frx":0044
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Label1(3)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "lbResSinave"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "lbValorSegundo"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "EcCodAna"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "EcDescrAna"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "BtPesquisaRapidaAna"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "OptTRes(0)"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "BtPesquisaRapidaFrases"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "EcDescrFrase"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "OptTRes(2)"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).Control(10)=   "CbSinal"
      Tab(2).Control(10).Enabled=   0   'False
      Tab(2).Control(11)=   "EcResNum"
      Tab(2).Control(11).Enabled=   0   'False
      Tab(2).Control(12)=   "EcListaAna"
      Tab(2).Control(12).Enabled=   0   'False
      Tab(2).Control(13)=   "EcCodFrase"
      Tab(2).Control(13).Enabled=   0   'False
      Tab(2).Control(14)=   "BtAdicionarAna"
      Tab(2).Control(14).Enabled=   0   'False
      Tab(2).Control(15)=   "EcListaFrases"
      Tab(2).Control(15).Enabled=   0   'False
      Tab(2).Control(16)=   "Frame3"
      Tab(2).Control(16).Enabled=   0   'False
      Tab(2).Control(17)=   "cbSegResEnviar"
      Tab(2).Control(17).Enabled=   0   'False
      Tab(2).ControlCount=   18
      TabCaption(3)   =   "Antibi�ticos a Apresentar"
      TabPicture(3)   =   "FormCodRegrasVigEpid.frx":0060
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Label1(9)"
      Tab(3).Control(1)=   "EcListaAntibApres"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Provas a Apresentar"
      TabPicture(4)   =   "FormCodRegrasVigEpid.frx":007C
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Label1(12)"
      Tab(4).Control(1)=   "EcListaProvasApresentar"
      Tab(4).ControlCount=   2
      TabCaption(5)   =   "An�lises a Apresentar"
      TabPicture(5)   =   "FormCodRegrasVigEpid.frx":0098
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Label1(13)"
      Tab(5).Control(0).Enabled=   0   'False
      Tab(5).Control(1)=   "Label1(15)"
      Tab(5).Control(1).Enabled=   0   'False
      Tab(5).Control(2)=   "EcListaAnaApresentar"
      Tab(5).Control(2).Enabled=   0   'False
      Tab(5).Control(3)=   "EcPesquisaAnaApres"
      Tab(5).Control(3).Enabled=   0   'False
      Tab(5).Control(4)=   "BtRetira"
      Tab(5).Control(4).Enabled=   0   'False
      Tab(5).Control(5)=   "BtBaixo"
      Tab(5).Control(5).Enabled=   0   'False
      Tab(5).Control(6)=   "BtCima"
      Tab(5).Control(6).Enabled=   0   'False
      Tab(5).Control(7)=   "BtInsere"
      Tab(5).Control(7).Enabled=   0   'False
      Tab(5).Control(8)=   "EcListaAnaEnvioSel"
      Tab(5).Control(8).Enabled=   0   'False
      Tab(5).Control(9)=   "BtPesquisaRapidaFrasesEnvio"
      Tab(5).Control(9).Enabled=   0   'False
      Tab(5).Control(10)=   "EcListaFrasesEnvio"
      Tab(5).Control(10).Enabled=   0   'False
      Tab(5).ControlCount=   11
      Begin VB.ComboBox cbSegResEnviar 
         Height          =   315
         Left            =   1920
         TabIndex        =   92
         Top             =   2760
         Width           =   2175
      End
      Begin VB.Frame Frame3 
         Height          =   615
         Left            =   240
         TabIndex        =   88
         Top             =   1920
         Width           =   5775
         Begin VB.CheckBox chkSegundo 
            Caption         =   "2� Resultado (Qualitativo)"
            Height          =   375
            Left            =   2880
            TabIndex        =   90
            Top             =   120
            Width           =   2775
         End
         Begin VB.CheckBox chkPrimeiro 
            Caption         =   "1� Resultado (Quantitativo)"
            Height          =   375
            Left            =   240
            TabIndex        =   89
            Top             =   120
            Width           =   2535
         End
      End
      Begin VB.ListBox EcListaFrasesEnvio 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   -68280
         TabIndex        =   83
         Top             =   3720
         Width           =   4845
      End
      Begin VB.CommandButton BtPesquisaRapidaFrasesEnvio 
         Height          =   340
         Left            =   -67680
         Picture         =   "FormCodRegrasVigEpid.frx":00B4
         Style           =   1  'Graphical
         TabIndex        =   82
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   3360
         Width           =   375
      End
      Begin VB.ListBox EcListaAnaEnvioSel 
         Appearance      =   0  'Flat
         Height          =   2175
         Left            =   -68280
         TabIndex        =   80
         Top             =   1080
         Width           =   4785
      End
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   -69360
         Picture         =   "FormCodRegrasVigEpid.frx":043E
         Style           =   1  'Graphical
         TabIndex        =   79
         ToolTipText     =   " Inserir no Perfil "
         Top             =   1755
         Width           =   495
      End
      Begin VB.CommandButton BtCima 
         Height          =   495
         Left            =   -69360
         Picture         =   "FormCodRegrasVigEpid.frx":07C8
         Style           =   1  'Graphical
         TabIndex        =   78
         ToolTipText     =   " Alterar ordem no Perfil "
         Top             =   1080
         Width           =   495
      End
      Begin VB.CommandButton BtBaixo 
         Height          =   495
         Left            =   -69360
         Picture         =   "FormCodRegrasVigEpid.frx":0B52
         Style           =   1  'Graphical
         TabIndex        =   77
         ToolTipText     =   " Alterar ordem no Perfil "
         Top             =   2835
         Width           =   495
      End
      Begin VB.CommandButton BtRetira 
         Height          =   495
         Left            =   -69360
         Picture         =   "FormCodRegrasVigEpid.frx":0EDC
         Style           =   1  'Graphical
         TabIndex        =   76
         ToolTipText     =   " Retirar do Perfil "
         Top             =   2190
         Width           =   495
      End
      Begin VB.ListBox EcListaFrases 
         Appearance      =   0  'Flat
         Height          =   1590
         Left            =   6360
         TabIndex        =   69
         Top             =   1680
         Width           =   3885
      End
      Begin VB.TextBox EcPesquisaAnaApres 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -74880
         TabIndex        =   68
         Top             =   840
         Width           =   5055
      End
      Begin VB.ListBox EcListaAnaApresentar 
         Appearance      =   0  'Flat
         Height          =   3930
         Left            =   -74880
         TabIndex        =   66
         Top             =   1200
         Width           =   5055
      End
      Begin VB.ListBox EcListaProvasApresentar 
         Appearance      =   0  'Flat
         Height          =   4305
         Left            =   -74760
         Style           =   1  'Checkbox
         TabIndex        =   64
         Top             =   840
         Width           =   8415
      End
      Begin VB.TextBox EcCodSubClasseAntibio 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -70200
         TabIndex        =   62
         Top             =   990
         Width           =   735
      End
      Begin VB.TextBox EcDescrSubClasseAntibio 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -69480
         Locked          =   -1  'True
         TabIndex        =   61
         Top             =   990
         Width           =   1935
      End
      Begin VB.CommandButton BtPesqRapSubClasseAntibio 
         Height          =   310
         Left            =   -67560
         Picture         =   "FormCodRegrasVigEpid.frx":1266
         Style           =   1  'Graphical
         TabIndex        =   60
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   960
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapGrMicro 
         Height          =   310
         Left            =   -71760
         Picture         =   "FormCodRegrasVigEpid.frx":15F0
         Style           =   1  'Graphical
         TabIndex        =   58
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   930
         Width           =   375
      End
      Begin VB.TextBox EcDescrGrMicro 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -73680
         Locked          =   -1  'True
         TabIndex        =   57
         Top             =   960
         Width           =   1935
      End
      Begin VB.TextBox EcCodGrMicro 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74400
         TabIndex        =   56
         Top             =   960
         Width           =   735
      End
      Begin VB.ListBox EcListaAntibApres 
         Appearance      =   0  'Flat
         Height          =   4305
         Left            =   -74880
         Style           =   1  'Checkbox
         TabIndex        =   54
         Top             =   960
         Width           =   8415
      End
      Begin VB.CommandButton BtAdicionarAna 
         Height          =   615
         Left            =   10920
         Picture         =   "FormCodRegrasVigEpid.frx":197A
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   2640
         Width           =   615
      End
      Begin VB.TextBox EcCodFrase 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   6360
         TabIndex        =   47
         Top             =   1200
         Width           =   735
      End
      Begin VB.ListBox EcListaAna 
         Appearance      =   0  'Flat
         Height          =   1590
         Left            =   360
         TabIndex        =   24
         Top             =   3480
         Width           =   11205
      End
      Begin VB.TextBox EcResNum 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2760
         TabIndex        =   23
         Top             =   1200
         Width           =   1335
      End
      Begin VB.ComboBox CbSinal 
         Height          =   315
         Left            =   1920
         TabIndex        =   22
         Text            =   "Combo1"
         Top             =   1200
         Width           =   855
      End
      Begin VB.OptionButton OptTRes 
         Caption         =   "Resultado Alfan."
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   21
         Top             =   1200
         Width           =   1695
      End
      Begin VB.TextBox EcDescrFrase 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   7080
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   1200
         Width           =   3135
      End
      Begin VB.CommandButton BtPesquisaRapidaFrases 
         Height          =   340
         Left            =   10200
         Picture         =   "FormCodRegrasVigEpid.frx":1C84
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   1200
         Width           =   375
      End
      Begin VB.OptionButton OptTRes 
         Caption         =   "Resultado Frase"
         Height          =   255
         Index           =   0
         Left            =   4680
         TabIndex        =   18
         Top             =   1200
         Width           =   1695
      End
      Begin VB.CommandButton BtPesquisaRapidaAna 
         Height          =   340
         Left            =   7080
         Picture         =   "FormCodRegrasVigEpid.frx":200E
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox EcDescrAna 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   600
         Width           =   5295
      End
      Begin VB.TextBox EcCodAna 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   14
         Top             =   600
         Width           =   735
      End
      Begin VB.ListBox EcListaProdutos 
         Appearance      =   0  'Flat
         Height          =   4305
         Left            =   -74880
         Style           =   1  'Checkbox
         TabIndex        =   13
         Top             =   960
         Width           =   11535
      End
      Begin VB.ListBox EcListaSensib 
         Appearance      =   0  'Flat
         Height          =   1830
         Left            =   -66840
         Style           =   1  'Checkbox
         TabIndex        =   12
         Top             =   960
         Width           =   3495
      End
      Begin VB.ListBox EcListaAntib 
         Appearance      =   0  'Flat
         Height          =   3630
         Left            =   -71040
         Style           =   1  'Checkbox
         TabIndex        =   11
         Top             =   1320
         Width           =   3855
      End
      Begin VB.ListBox EcListaMicrorg 
         Appearance      =   0  'Flat
         Height          =   3630
         Left            =   -74880
         Style           =   1  'Checkbox
         TabIndex        =   10
         Top             =   1320
         Width           =   3495
      End
      Begin VB.Label lbValorSegundo 
         Caption         =   "Resultado qualitativo:"
         Height          =   375
         Left            =   240
         TabIndex        =   91
         Top             =   2760
         Width           =   2055
      End
      Begin VB.Label lbResSinave 
         Caption         =   "Resultados a enviar para SINAVE:"
         Height          =   255
         Left            =   240
         TabIndex        =   87
         Top             =   1680
         Width           =   2775
      End
      Begin VB.Label Label1 
         Caption         =   "Frases"
         Height          =   255
         Index           =   15
         Left            =   -68280
         TabIndex        =   84
         Top             =   3480
         Width           =   1150
      End
      Begin VB.Label Label1 
         Caption         =   "An�lises"
         Height          =   255
         Index           =   13
         Left            =   -74880
         TabIndex        =   67
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Provas"
         Height          =   255
         Index           =   12
         Left            =   -74760
         TabIndex        =   65
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "SubClasse"
         Height          =   255
         Index           =   11
         Left            =   -71040
         TabIndex        =   63
         Top             =   990
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo"
         Height          =   255
         Index           =   10
         Left            =   -74880
         TabIndex        =   59
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Antibi�ticos"
         Height          =   255
         Index           =   9
         Left            =   -74880
         TabIndex        =   55
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Produtos"
         Height          =   255
         Index           =   7
         Left            =   -74880
         TabIndex        =   46
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Sensibilidade"
         Height          =   255
         Index           =   6
         Left            =   -66840
         TabIndex        =   45
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Antibi�ticos"
         Height          =   255
         Index           =   5
         Left            =   -70800
         TabIndex        =   44
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Microrganismos"
         Height          =   255
         Index           =   4
         Left            =   -74880
         TabIndex        =   43
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   735
      End
   End
   Begin VB.TextBox EcDescrRegra 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   2880
      TabIndex        =   1
      Top             =   240
      Width           =   4815
   End
   Begin VB.TextBox EcCodRegra 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   0
      Top             =   240
      Width           =   735
   End
   Begin VB.TextBox EcCodGrEpid 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   2
      Top             =   720
      Width           =   735
   End
   Begin VB.TextBox EcDescrEpid 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   720
      Width           =   3015
   End
   Begin VB.CommandButton BtPesqRapGrEpid 
      Height          =   340
      Left            =   4800
      Picture         =   "FormCodRegrasVigEpid.frx":2398
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
      Top             =   720
      Width           =   375
   End
   Begin VB.Label LbDoencaSNV 
      Caption         =   "C�d. Doen�a SINAVE"
      Height          =   375
      Left            =   5280
      TabIndex        =   86
      Top             =   1200
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Titulo"
      Height          =   255
      Index           =   14
      Left            =   120
      TabIndex        =   75
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "N�Sub Classes Sens�veis"
      Height          =   255
      Index           =   8
      Left            =   5280
      TabIndex        =   73
      Top             =   750
      Width           =   1935
   End
   Begin VB.Label Label5 
      Caption         =   "EcTipoRegra"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   50
      Top             =   10800
      Width           =   705
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataAlteracao"
      Height          =   375
      Left            =   3000
      TabIndex        =   35
      Top             =   12480
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCriacao"
      Height          =   375
      Left            =   3120
      TabIndex        =   34
      Top             =   12120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   375
      Left            =   0
      TabIndex        =   33
      Top             =   11400
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   375
      Left            =   120
      TabIndex        =   32
      Top             =   12120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   2
      Left            =   2040
      TabIndex        =   7
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   6
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Grupo Epid."
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   720
      Width           =   975
   End
End
Attribute VB_Name = "FormCodRegrasVigEpid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 24/04/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As ADODB.recordset

Private Type Sensib
    seq_sensib As String
    cod_sensib As String
    descr_sensib As String
    flg_seleccionado As Boolean
End Type
Dim sensibilidades() As Sensib
Dim totalSensib As Integer

Private Type antib
    seq_antib As Integer
    cod_antibio As String
    descr_antibio As String
    cod_subclasse_antibio As String
    flg_seleccionado As Boolean
    ordem As Integer
End Type
Dim antibioticos() As antib
Dim TotalAntibioticos As Integer

Private Type AnaApresentar
    seq_ana As String
    cod_ana As String
    descr_ana As String
End Type
Dim AnaImpr() As AnaApresentar
Dim TotalAnaImpr As Integer

Private Type ProvaApresentar
    seq_prova As String
    Cod_Prova As String
    descr_prova As String
    flg_seleccionado As Boolean
End Type

Private Type Produto
    seq_produto As Integer
    cod_produto As String
    descr_produto As String
    flg_seleccionado As Boolean
End Type

Private Type frase
    seq_frase As String
    cod_frase As String
    descr_frase As String
End Type

Private Type analises
    cod_ana_s As String
    descr_ana_s As String
    t_res As Integer
    frases() As frase
    totalFrases As Integer
    cod_sinal As Integer
    resNum As String
    'EDGARPARADA Glintt-HS-19848
    enviar1res As Integer
    enviar2res As Integer
    resNum2  As Integer
    '
End Type

Private Type AntibSel
    iAntib As Integer
    
    sens() As Sensib
    totalSens As Integer
End Type

Private Type Microrganismo
    seq_microrg As Integer
    cod_microrg As String
    descr_microrg As String
    cod_gr_microrg As String
    ordem As Integer
    antibRegra() As AntibSel
    totalAntibRegra As Integer
    flg_seleccionado As Boolean
End Type

Private Type AnaAux
    indice As Integer
    frases() As frase
    totalFrases As Integer
    
End Type
Private Type regras
    micro() As Microrganismo
    totalMicro As Integer
    
    AnaImprSel() As AnaAux
    TotalAnaImprSel As Integer

    provasImpr() As ProvaApresentar
    totalProvasImpr As Integer
    
    antibImpr() As AntibSel
    totalAntibImpr As Integer

    ana() As analises
    totalAna As Integer
    
    prod() As Produto
    totalProd As Integer
    
    num_classes As Integer
End Type
Dim Regra As regras

Dim frasesAux() As frase
Dim totalFrasesAux As Integer

Dim flg_atualizaMicro As Boolean
Private Sub BtAdicionarAna_Click()
    Dim iAna As Integer
    Dim indice As Integer
    Dim flg_add As Boolean
    If EcCodAna.text = "" Then Exit Sub
    indice = mediComboValorNull
    For iAna = 1 To Regra.totalAna
        If EcCodAna.text = Regra.ana(iAna).cod_ana_s Then
            indice = iAna
            Exit For
        End If
    Next
    If indice = mediComboValorNull Then
        Regra.totalAna = Regra.totalAna + 1
        ReDim Preserve Regra.ana(Regra.totalAna)
        indice = Regra.totalAna
    End If
    
    Regra.ana(iAna).cod_ana_s = EcCodAna.text
    Regra.ana(iAna).descr_ana_s = EcDescrAna.text
    
    If OptTRes(0).value = True Then
        Regra.ana(iAna).t_res = gT_Frase
    ElseIf OptTRes(2).value = True Then
        Regra.ana(iAna).t_res = gT_Alfanumerico
        totalFrasesAux = 0
        ReDim frasesAux(0)
        
    End If
    Regra.ana(iAna).totalFrases = 0
    ReDim Regra.ana(iAna).frases(0)
    Regra.ana(iAna).resNum = EcResNum.text
    If CbSinal.ListIndex > mediComboValorNull Then
        Regra.ana(iAna).cod_sinal = CbSinal.ItemData(CbSinal.ListIndex)
    End If
    Regra.ana(iAna).resNum = EcResNum.text
    Regra.ana(iAna).totalFrases = totalFrasesAux
    Regra.ana(iAna).frases = frasesAux
    'EDGARPARADA Glintt-HS-19848
    Regra.ana(iAna).enviar1res = chkPrimeiro.value
    Regra.ana(iAna).enviar2res = chkSegundo.value
    
    If chkSegundo.value = vbChecked Then
       Regra.ana(iAna).resNum2 = cbSegResEnviar.ItemData(cbSegResEnviar.ListIndex)
    End If
    '
    ActualizaListaAna

End Sub


Private Sub BtInsere_Click()
Dim iAna As Integer
Dim iAna2 As Integer
    iAna = DevolveIndiceAnaApres(EcListaAnaApresentar.ListIndex)
    
    For iAna2 = 1 To Regra.TotalAnaImprSel
        If Regra.AnaImprSel(iAna2).indice = iAna Then
            Exit Sub
        End If
    Next iAna2
    Regra.TotalAnaImprSel = Regra.TotalAnaImprSel + 1
    ReDim Preserve Regra.AnaImprSel(Regra.TotalAnaImprSel)
    Regra.AnaImprSel(Regra.TotalAnaImprSel).indice = iAna
    Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases = 0
    ReDim Regra.AnaImprSel(Regra.TotalAnaImprSel).frases(0)
    
    EcListaAnaEnvioSel.AddItem AnaImpr(Regra.AnaImprSel(Regra.TotalAnaImprSel).indice).cod_ana & " - " & AnaImpr(Regra.AnaImprSel(Regra.TotalAnaImprSel).indice).descr_ana
    EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.NewIndex) = AnaImpr(Regra.AnaImprSel(Regra.TotalAnaImprSel).indice).seq_ana
    
End Sub

Private Sub BtPesqRapGrEpid_Click()
    PA_PesquisaGruposEpid EcCodGrEpid, EcDescrEpid, CStr(gCodLocal)
End Sub

Private Sub BtPesqRapGrMicro_Click()
    PA_PesquisaGruposMicro EcCodGrMicro, EcDescrGrMicro, CStr(gCodLocal)
    ActualizaMicro
End Sub

Private Sub BtPesqRapSubClasseAntibio_Click()
    Dim iMicro As Integer
    PA_PesquisaSubClasseAntibio EcCodSubClasseAntibio, EcDescrSubClasseAntibio, CStr(gCodLocal)
    iMicro = DevolveIndiceMicro(EcListaMicrorg.ListIndex)
    ActualizaAntib iMicro
End Sub

Private Sub BtPesquisaRapidaAna_Click()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises ")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAna.text = resultados(2)
            EcCodAna.text = resultados(1)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAna.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises ", vbExclamation, "ATEN��O"
        EcCodAna.SetFocus
    End If

End Sub

Private Sub BtPesquisaRapidaFrases_Click()
    Dim ChavesPesq(1 To 3) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 3) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_frase"
    CamposEcran(1) = "cod_frase"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_frase"
    CamposEcran(2) = "descr_frase"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    ChavesPesq(3) = "seq_frase"
    CamposEcran(3) = "seq_frase"
    Tamanhos(3) = 0
    Headers(3) = ""
    
    CamposRetorno.InicializaResultados 3
    
    CFrom = "sl_dicionario"
    CampoPesquisa = "descr_frase"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Dicion�rio de frases ")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            'EcDescrFrase.text = Resultados(2)
            'EcCodFrase.text = Resultados(1)
            'EcCodFrase.SetFocus
        
            totalFrasesAux = totalFrasesAux + 1
            ReDim Preserve frasesAux(totalFrasesAux)
            frasesAux(totalFrasesAux).cod_frase = resultados(1)
            frasesAux(totalFrasesAux).descr_frase = resultados(2)
            frasesAux(totalFrasesAux).seq_frase = resultados(3)
            EcListaFrases.AddItem frasesAux(totalFrasesAux).descr_frase
            EcListaFrases.ItemData(EcListaFrases.NewIndex) = frasesAux(totalFrasesAux).seq_frase
        
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Frases ", vbExclamation, "ATEN��O"
        EcCodFrase.SetFocus
    End If


End Sub

Private Sub BtPesquisaRapidaFrasesEnvio_Click()
    Dim ChavesPesq(1 To 3) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 3) As Variant
    Dim PesqRapida As Boolean

    If EcListaAnaEnvioSel.ListIndex <= mediComboValorNull Then Exit Sub

    PesqRapida = False
    
    ChavesPesq(1) = "cod_frase"
    CamposEcran(1) = "cod_frase"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_frase"
    CamposEcran(2) = "descr_frase"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    ChavesPesq(3) = "seq_frase"
    CamposEcran(3) = "seq_frase"
    Tamanhos(3) = 0
    Headers(3) = ""
    
    CamposRetorno.InicializaResultados 3
    
    CFrom = "sl_dicionario"
    CampoPesquisa = "descr_frase"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Dicion�rio de frases ")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases = Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases + 1
            
            ReDim Preserve Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases)
            Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases).cod_frase = resultados(1)
            Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases).descr_frase = resultados(2)
            Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases).seq_frase = resultados(3)
            EcListaFrasesEnvio.AddItem Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases).descr_frase
            EcListaFrasesEnvio.ItemData(EcListaFrasesEnvio.NewIndex) = Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases).seq_frase
        
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Frases ", vbExclamation, "ATEN��O"
        EcListaFrasesEnvio.SetFocus
    End If


End Sub


Private Sub BtRetira_Click()
    Dim iAna As Integer
    If EcListaAnaEnvioSel.ListCount > 0 Then
        If EcListaAnaEnvioSel.ListIndex = mediComboValorNull Then
            EcListaAnaEnvioSel.ListIndex = EcListaAnaEnvioSel.ListCount - 1
        End If
        For iAna = EcListaAnaEnvioSel.ListIndex + 1 To Regra.TotalAnaImprSel - 1
            Regra.AnaImprSel(iAna).indice = Regra.AnaImprSel(iAna + 1).indice
            Regra.AnaImprSel(iAna).totalFrases = Regra.AnaImprSel(iAna + 1).totalFrases
            Regra.AnaImprSel(iAna).frases = Regra.AnaImprSel(iAna + 1).frases
        Next iAna
        Regra.TotalAnaImprSel = Regra.TotalAnaImprSel - 1
        ReDim Preserve Regra.AnaImprSel(Regra.TotalAnaImprSel)
        EcListaAnaEnvioSel.RemoveItem EcListaAnaEnvioSel.ListIndex
    End If
End Sub

'EDGARPARADA Glintt-HS-19848
Private Sub chkSegundo_Click()
    If chkSegundo.value = vbChecked Then
        cbSegResEnviar.Enabled = True
    ElseIf chkSegundo.value = vbUnchecked Then
       cbSegResEnviar.Enabled = False
    End If
End Sub
'

Private Sub EcCodana_Validate(Cancel As Boolean)
    If EcCodAna.text <> "" Then
        
        Dim RsDescrAnaS As ADODB.recordset
        Set RsDescrAnaS = New ADODB.recordset
        If Mid(EcCodAna.text, 1, 1) <> "S" Then
            EcCodAna.text = "S" & EcCodAna.text
        End If
        
        EcCodAna.text = UCase(EcCodAna.text)
        With RsDescrAnaS
            .Source = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s= " & UCase(BL_TrataStringParaBD(EcCodAna.text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaS.RecordCount > 0 Then
            EcDescrAna.text = "" & RsDescrAnaS!descr_ana_s
        Else
            Cancel = True
            EcDescrAna.text = ""
            BG_Mensagem mediMsgBox, "An�lise  inexistente!", vbOKOnly + vbExclamation, App.ProductName
            EcCodAna.text = ""
        End If
        RsDescrAnaS.Close
        Set RsDescrAnaS = Nothing
    Else
        EcCodAna.text = ""
        EcDescrAna.text = ""
    End If
End Sub

Private Sub EcCodfrase_Validate(Cancel As Boolean)
    If EcCodFrase.text <> "" Then
        
        Dim rsFrase As ADODB.recordset
        Set rsFrase = New ADODB.recordset
        
        EcCodFrase.text = UCase(EcCodFrase.text)
        With rsFrase
            .Source = "SELECT seq_frase, cod_frase, descr_frase FROM sl_dicionario WHERE cod_frase= " & UCase(BL_TrataStringParaBD(EcCodFrase.text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If rsFrase.RecordCount > 0 Then
            EcDescrFrase.text = "" & rsFrase!descr_frase
            
            totalFrasesAux = totalFrasesAux + 1
            ReDim Preserve frasesAux(totalFrasesAux)
            frasesAux(totalFrasesAux).cod_frase = BL_HandleNull(rsFrase!cod_frase, "")
            frasesAux(totalFrasesAux).descr_frase = BL_HandleNull(rsFrase!descr_frase, "")
            frasesAux(totalFrasesAux).seq_frase = BL_HandleNull(rsFrase!seq_frase, "")
            EcListaFrases.AddItem frasesAux(totalFrasesAux).descr_frase
            EcListaFrases.ItemData(EcListaFrases.NewIndex) = frasesAux(totalFrasesAux).seq_frase
        Else
            Cancel = True
            EcDescrFrase.text = ""
            BG_Mensagem mediMsgBox, "Frase  inexistente!", vbOKOnly + vbExclamation, App.ProductName
            EcCodFrase.text = ""
        End If
        rsFrase.Close
        Set rsFrase = Nothing
   End If
    EcCodFrase.text = ""
    EcDescrFrase.text = ""
    
End Sub

Private Sub EcCodGrMicro_Validate(Cancel As Boolean)
    PA_ValidateGrupoMicro EcCodGrMicro, EcDescrGrMicro, CStr(gCodLocal)
    ActualizaMicro
End Sub

Private Sub EcCodSubClasseAntibio_Validate(Cancel As Boolean)
    Dim iMicro As Integer
    PA_ValidateSubClasseAntibio EcCodSubClasseAntibio, EcDescrSubClasseAntibio, CStr(gCodLocal)
    iMicro = DevolveIndiceMicro(EcListaMicrorg.ListIndex)
    ActualizaAntib iMicro
End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If

End Sub

Private Sub EcCodRegra_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodRegra_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodRegra.text = UCase(EcCodRegra.text)

End Sub

Private Sub EcDescrRegra_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcListaAna_Click()
    Dim i As Integer
    If EcListaAna.ListCount > 0 Then
        If EcListaAna.ListIndex > mediComboValorNull Then
            EcCodAna.text = Regra.ana(EcListaAna.ListIndex + 1).cod_ana_s
            EcCodana_Validate False
            
            'EcCodFrase.text = Regra.ana(EcListaAna.ListIndex + 1).cod_frase
            EcCodfrase_Validate False
            If Regra.ana(EcListaAna.ListIndex + 1).t_res = gT_Frase Then
                OptTRes(0).value = True
            Else
                OptTRes(2).value = True
            End If
            EcResNum.text = Regra.ana(EcListaAna.ListIndex + 1).resNum
            CbSinal.ListIndex = mediComboValorNull
            For i = 0 To CbSinal.ListCount - 1
                If CbSinal.ItemData(i) = Regra.ana(EcListaAna.ListIndex + 1).cod_sinal Then
                    CbSinal.ListIndex = i
                    Exit For
                End If
            Next i
            'EDGARPARADA Glintt-HS-19848
            If Regra.ana(EcListaAna.ListIndex + 1).enviar1res = mediSim Then
               chkPrimeiro.value = vbChecked
            End If
            
            If Regra.ana(EcListaAna.ListIndex + 1).enviar1res = mediSim Then
               chkSegundo.value = vbChecked
               cbSegResEnviar.Enabled = True
            End If
            
             For i = 0 To cbSegResEnviar.ListCount - 1
                If cbSegResEnviar.ItemData(i) = Regra.ana(EcListaAna.ListIndex + 1).resNum2 Then
                    cbSegResEnviar.ListIndex = i
                    Exit For
                End If
            Next i
            '
        End If
    End If
    
End Sub

Private Sub EcListaAnaEnvioSel_Click()
    EcListaFrasesEnvio.Clear
    Dim iFrase As Integer
    
    If EcListaAnaEnvioSel.ListIndex > mediComboValorNull Then
        For iFrase = 1 To Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).totalFrases
            EcListaFrasesEnvio.AddItem Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(iFrase).descr_frase
            EcListaFrasesEnvio.ItemData(EcListaFrasesEnvio.NewIndex) = Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).frases(iFrase).seq_frase
        Next iFrase
    End If
End Sub

Private Sub EcListaAntib_Click()
    Dim iMicro As Integer
    Dim iAntib As Integer
    iMicro = DevolveIndiceMicro(EcListaMicrorg.ListIndex)
    If iMicro > mediComboValorNull Then
        iAntib = DevolveIndiceAntib(iMicro, EcListaAntib.ListIndex)
        If iAntib > mediComboValorNull Then
            If EcListaAntib.ListIndex > mediComboValorNull Then
                ActualizaSensib iMicro, iAntib
            End If
        Else
            LimpaSensib
        End If
    Else
        LimpaSensib
    End If
End Sub

Private Sub EcListaAntib_ItemCheck(item As Integer)
    Dim iMicro As Integer
    Dim iAntib As Integer
    Dim i As Integer
    iMicro = DevolveIndiceMicro(EcListaMicrorg.ListIndex)
    iAntib = DevolveIndiceAntib(iMicro, EcListaAntib.ListIndex)

    If EcListaAntib.Selected(item) = True Then
        If iAntib = mediComboValorNull And iMicro > mediComboValorNull Then
            AdicionaNovoAntibSel iMicro, item
            iAntib = DevolveIndiceAntib(iMicro, EcListaAntib.ListIndex)
        End If
    Else
    End If
    If iMicro > mediComboValorNull And iAntib > mediComboValorNull Then
        ActualizaSensib iMicro, iAntib
    End If
End Sub

Private Sub EcListaAntibApres_ItemCheck(item As Integer)
    Dim iMicro As Integer
    Dim iAntibApres As Integer
    Dim i As Integer
    iAntibApres = DevolveIndiceAntibApres(iMicro, EcListaAntibApres.ListIndex)
    If EcListaAntibApres.Selected(item) = True Then
        If iAntibApres = mediComboValorNull Then
            AdicionaNovoAntibApres item
            iAntibApres = DevolveIndiceAntibApres(iMicro, EcListaAntib.ListIndex)
        End If
    Else
        If iAntibApres > mediComboValorNull Then
            For i = iAntibApres To Regra.totalAntibImpr - 1
                Regra.antibImpr(i).iAntib = Regra.antibImpr(i + 1).iAntib
            Next i
            Regra.antibImpr(Regra.totalAntibImpr).iAntib = mediComboValorNull
            Regra.totalAntibImpr = Regra.totalAntibImpr - 1
            ReDim Preserve Regra.antibImpr(Regra.totalAntibImpr)
            
        End If
    End If

End Sub

Private Sub EcListaFrases_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iFrase As Integer
    If KeyCode = vbKeyDelete Then
        For iFrase = EcListaFrases.ListIndex + 1 To totalFrasesAux - 1
            frasesAux(iFrase).cod_frase = frasesAux(iFrase + 1).cod_frase
            frasesAux(iFrase).seq_frase = frasesAux(iFrase + 1).seq_frase
            frasesAux(iFrase).descr_frase = frasesAux(iFrase + 1).descr_frase
        Next iFrase
        
        frasesAux(totalFrasesAux).seq_frase = ""
        frasesAux(totalFrasesAux).cod_frase = ""
        frasesAux(totalFrasesAux).descr_frase = ""
        totalFrasesAux = totalFrasesAux - 1
        ReDim Preserve frasesAux(totalFrasesAux)
        EcListaFrases.RemoveItem EcListaFrases.ListIndex
    End If
End Sub

Private Sub EcListaProvasApresentar_ItemCheck(item As Integer)
    Dim iProvasApres As Integer
    Dim i As Integer
    iProvasApres = DevolveIndiceProvaApres(EcListaProvasApresentar.ListIndex)
    If EcListaProvasApresentar.Selected(item) = True Then
        Regra.provasImpr(iProvasApres).flg_seleccionado = True
    Else
        Regra.provasImpr(iProvasApres).flg_seleccionado = False
    End If
End Sub

Private Sub EcListaMicrorg_Click()
    Dim iMicro As Integer
    If flg_atualizaMicro = True Then
        If EcListaMicrorg.ListIndex > mediComboValorNull Then
            iMicro = DevolveIndiceMicro(EcListaMicrorg.ListIndex)
            If iMicro > mediComboValorNull Then
                ActualizaAntib iMicro
            Else
                LimpaAntib
            End If
        End If
    End If
End Sub

Private Sub EcListaMicrorg_ItemCheck(item As Integer)
    Dim iMicro As Integer
    If flg_atualizaMicro = True Then
        iMicro = DevolveIndiceMicro(EcListaMicrorg.ListIndex)
        If iMicro > mediComboValorNull Then
            If EcListaMicrorg.Selected(item) = True Then
                Regra.micro(iMicro).flg_seleccionado = True
            Else
                Regra.micro(iMicro).flg_seleccionado = False
            End If
            ActualizaAntib iMicro
        End If
    End If
End Sub

Private Sub EcListaProdutos_ItemCheck(item As Integer)
    If EcListaProdutos.Selected(item) = True Then
        Regra.prod(item + 1).flg_seleccionado = True
    Else
        Regra.prod(item + 1).flg_seleccionado = False
    End If
End Sub

Private Sub EcListaSensib_ItemCheck(item As Integer)
    Dim iMicro As Integer
    Dim iAntib As Integer
    iMicro = DevolveIndiceMicro(EcListaMicrorg.ListIndex)
    iAntib = DevolveIndiceAntib(iMicro, EcListaAntib.ListIndex)
    If iAntib = mediComboValorNull Then
        AdicionaNovoAntibSel iMicro, EcListaAntib.ListIndex
        iAntib = DevolveIndiceAntib(iMicro, EcListaAntib.ListIndex)
    End If
    If iMicro > mediComboValorNull And iAntib > mediComboValorNull Then
        If EcListaSensib.Selected(item) = True Then
            Regra.micro(iMicro).antibRegra(iAntib).sens(item + 1).flg_seleccionado = True
        Else
            Regra.micro(iMicro).antibRegra(iAntib).sens(item + 1).flg_seleccionado = False
        End If
    End If
    'VeriricaListaMicro iMicro
    'VeriricaListaAntib iMicro, iAntib
End Sub

Private Sub EcNumClasses_Change()
    Regra.num_classes = BL_HandleNull(EcNumClasses.text, 0)
End Sub

Private Sub EcPesquisaAnaApres_Change()
    ActualizaAnaImpr
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Codifica��o de Regras de Vigil�ncia Epidemiol�gica"
    Me.left = 50
    Me.top = 50
    Me.Width = 12135
    Me.Height = 9105 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_cod_regras_ve"
    Set CampoDeFocus = EcCodRegra
    
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    'NumCampos = 16
    NumCampos = 17
    '
    
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_regra_ve"
    CamposBD(1) = "cod_gr_epid"
    CamposBD(2) = "descr_regra_ve"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "hr_Cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "hr_act"
    CamposBD(9) = "flg_invisivel"
    CamposBD(10) = "tipo_regra"
    CamposBD(11) = "num_classes"
    CamposBD(12) = "flg_nao_avalia_sensib"
    CamposBD(13) = "flg_mostra_micro"
    CamposBD(14) = "descr_excel"
    CamposBD(15) = "flg_suspeita"
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    CamposBD(16) = "cod_doenca_snv"
    '
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodRegra
    Set CamposEc(1) = EcCodGrEpid
    Set CamposEc(2) = EcDescrRegra
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcHoraCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = EcHoraAlteracao
    Set CamposEc(9) = CkInvisivel
    Set CamposEc(10) = EcTipoRegra
    Set CamposEc(11) = EcNumClasses
    Set CamposEc(12) = CkNaoAvaliarSesib
    Set CamposEc(13) = CkMostrarMicro
    Set CamposEc(14) = EcDescrExcel
    Set CamposEc(15) = CkSuspeita
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    Set CamposEc(16) = EcDoenca_SNV
    '
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(2) = "Descri��o do Regra"
    TextoCamposObrigatorios(1) = "C�digo do Grupo"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_regra_ve"
    Set ChaveEc = EcCodRegra
    
    CamposBDparaListBox = Array("cod_regra_ve", "descr_regra_ve")
    'CamposBDparaListBox = Array("cod_postal", "area_geografica")
    NumEspacos = Array(13, 100)
    
    'EDGARPARADA Glintt-HS-19848
     cbSegResEnviar.Enabled = False
    '

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormCodRegrasVigEpid = Nothing
    ReDim micro(0)
End Sub

Sub LimpaCampos()
    Me.SetFocus
    CkInvisivel.value = vbGrayed
    CkNaoAvaliarSesib.value = vbGrayed
    OptTRes_Click 0
    EcCodFrase.text = ""
    EcDescrFrase.text = ""
    EcResNum.text = ""
    CbSinal.text = ""
    EcCodAna.text = ""
    EcDescrAna.text = ""
    EcCodGrMicro.text = ""
    EcDescrGrMicro.text = ""
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcDescrEpid.text = ""
    LimpaMicrorg
    LimpaProd
    totalFrasesAux = 0
    ReDim frasesAux(0)
    EcListaFrases.Clear
    CkMostrarMicro.value = vbGrayed
    CkSuspeita.value = vbGrayed
    EcListaAnaEnvioSel.Clear
    EcListaFrasesEnvio.Clear
    EcPesquisaAnaApres.text = ""
    ActualizaAnaImpr
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    EcDoenca_SNV.text = ""
    '
    'BRUNODSANTOS 19.04.2017
    SSTab.TabEnabled(0) = True
    SSTab.TabEnabled(1) = True
    SSTab.TabEnabled(2) = True
    SSTab.TabEnabled(3) = True
    SSTab.TabEnabled(4) = True
    SSTab.TabEnabled(5) = True
    '
    
    'EDGARPARADA Glintt-HS-19848
    cbSegResEnviar = ""
    chkSegundo.value = vbUnchecked
    chkPrimeiro.value = vbUnchecked
    '
End Sub

Private Sub LimpaProd()
    Dim i As Integer
    For i = 0 To EcListaProdutos.ListCount - 1
        EcListaProdutos.Selected(i) = False
    Next i
End Sub
Private Sub LimpaSensib()
    Dim i As Integer
    For i = 0 To EcListaSensib.ListCount - 1
        EcListaSensib.Selected(i) = False
    Next i
End Sub
Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

    
End Sub

Sub PreencheValoresDefeito()
    'EDGARPARADA Glintt-HS-19848
    BG_PreencheComboBD_ADO "sl_tbf_seg_res_sinave", "cod_sinave", "descr_sinave", cbSegResEnviar, mediAscComboCodigo
    '
    BG_PreencheComboBD_ADO "sl_tbf_sinais", "cod_sinal", "descr_sinal", CbSinal, mediAscComboCodigo
    OptTRes.item(0).value = True
    OptTRes_Click 0
    SSTab.Tab = 0
    CkInvisivel.value = vbGrayed
    CkNaoAvaliarSesib.value = vbGrayed
    CkMostrarMicro.value = vbGrayed
    CkSuspeita.value = vbGrayed
    DoEvents
    PreencheSensibDefeito
    PreencheAntibDefeito
    PreencheMicrorgDefeito
    PreencheProdDefeito
    PreencheProvasDefeito
    PreencheAnaDefeito
    totalFrasesAux = 0
    ReDim frasesAux(0)
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & EcHoraCriacao
        LaDataAlteracao = EcDataAlteracao & EcHoraAlteracao
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
        EcCodGrEpid_Validate False
        
        EcListaFrases.Clear

        ReDim Regra.antibImpr(0)
        Regra.totalAntibImpr = 0
    
        ReDim Regra.ana(0)
        Regra.totalAna = 0
        
        
        Regra.num_classes = BL_HandleNull(EcNumClasses.text, mediComboValorNull)
        
        ProcuraAntibEnvio CLng(EcCodRegra.text)
        ProcuraAnaEnvio CLng(EcCodRegra.text)
        ProcuraProvasEnvio CLng(EcCodRegra.text)
        ProcuraMicro CLng(EcCodRegra.text)
        ProcuraProd CLng(EcCodRegra.text)
        ProcuraAna CLng(EcCodRegra.text)
        
        ActualizaMicro
        ActualizaProd
        ActualizaAntibEnvio
        ActualizaAnaImpr
        ActualizaProvasImpr
        If EcTipoRegra.text = "0" Then
            If OptTipoRegra(0) = False Then
                OptTipoRegra(0) = True
            End If
            EcListaMicrorg_Click
            EcListaAntib_Click
        ElseIf EcTipoRegra.text = "1" Then
            If OptTipoRegra(1) = False Then
                OptTipoRegra(1) = True
            End If
            
        End If
    End If
    SSTab.Tab = 0
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim str_aux1 As String
    Dim str_aux2 As String
    Dim str_aux3 As String
    
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
               
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    CriterioTabela = CriterioTabela & " ORDER BY cod_regra_ve "
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        
        BG_PreencheListBoxMultipla_ADO EcLista, _
                                       rs, _
                                       CamposBDparaListBox, _
                                       NumEspacos, _
                                       CamposBD, _
                                       CamposEc, _
                                       "SELECT"
        
        ' Fim do preenchimento de 'EcLista'
        'BRUNODSANTOS 19.03.2017
        If OptTipoRegra(1).value = True Then
            SSTab.Tab = 2
        End If
        GrupoTipoSINAVE
        '
        
        
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
    
    'BRUNODSANTOS 19.03.2017
    If OptTipoRegra(1).value = True Then
        SSTab.Tab = 2
    End If
    GrupoTipoSINAVE
    '
    
End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

    'BRUNODSANTOS 19.03.2017
    If OptTipoRegra(1).value = True Then
        SSTab.Tab = 2
    End If
    GrupoTipoSINAVE
    '
End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = CDate(Bg_DaData_ADO)
        EcHoraCriacao = CDate(Bg_DaHora_ADO)
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
            estado = 2
            BL_ToolbarEstadoN estado
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_FimProcessamento Me
        End If
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    EcCodRegra.text = BL_HandleNull(BG_DaMAX(NomeTabela, ChaveBD), 0) + 1
    If EcTipoRegra.text = "" Then
        EcTipoRegra.text = "0"
    End If
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    If OptTipoRegra(0) = True Then
        GravaMicro CLng(EcCodRegra.text)
        GravaAntibEnvio CLng(EcCodRegra.text)
    ElseIf OptTipoRegra(1) = True Then
        GravaAnalises CLng(EcCodRegra.text)
    End If
    GravaProd CLng(EcCodRegra.text)
    GravaAnaEnvio CLng(EcCodRegra.text)
    GravaProvasEnvio CLng(EcCodRegra.text)
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = CDate(Bg_DaData_ADO)
        EcHoraAlteracao = Bg_DaHora_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = CDate(Bg_DaHora_ADO)
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    If OptTipoRegra(0) = True Then
        GravaMicro CLng(EcCodRegra.text)
        GravaAntibEnvio CLng(EcCodRegra.text)
    ElseIf OptTipoRegra(1) = True Then
        GravaAnalises CLng(EcCodRegra.text)
    End If
    GravaProd CLng(EcCodRegra.text)
    GravaAnaEnvio CLng(EcCodRegra.text)
    GravaProvasEnvio CLng(EcCodRegra.text)
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    Dim iRes As Boolean
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer cancelar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        CkInvisivel.value = vbChecked
        EcDataAlteracao = CDate(Bg_DaData_ADO & " " & Bg_DaHora_ADO)
        EcHoraAlteracao = CDate(Bg_DaHora_ADO)
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If
    
End Sub




Private Sub PreenchesListaAntib(cod_gr_epid As String)
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    On Error GoTo TrataErro
    
    EcListaAntib.Clear
    If cod_gr_epid <> "" Then
        sSql = "SELECT x2.seq_antibio, x2.cod_antibio, x2.descR_antibio, x1.ordem FROM sl_cod_gr_epid_antib x1 RIGHT OUTER JOIN sl_antibio x2 ON x1.cod_antib = x2.cod_antibio"
        sSql = sSql & " AND cod_gr_epid = " & BL_TrataStringParaBD(cod_gr_epid)
        sSql = sSql & " ORDER BY ordem, descr_antibio "
    Else
        sSql = "SELECT x1.seq_antibio, x1.cod_antibio, x1.descR_antibio, 0 ordem FROM sl_antibio x1 "
        sSql = sSql & " ORDER BY ordem, descr_antibio "
    End If
    rsAntib.CursorType = adOpenStatic
    rsAntib.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAntib.Open sSql, gConexao
    While Not rsAntib.EOF
        EcListaAntib.AddItem BL_HandleNull(rsAntib!cod_antibio, "") & " - " & BL_HandleNull(rsAntib!descr_antibio, "")
        EcListaAntib.ItemData(EcListaAntib.NewIndex) = BL_HandleNull(rsAntib!seq_antibio, mediComboValorNull)
        If BL_HandleNull(rsAntib!ordem, 0) > 0 Then
            EcListaAntib.Selected(EcListaAntib.NewIndex) = True
        Else
            EcListaAntib.Selected(EcListaAntib.NewIndex) = False
        End If
        rsAntib.MoveNext
    Wend
    rsAntib.Close
    Set rsAntib = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Lista Antibioticos: " & Err.Description, Me.Name, "PreencheListaAntib", True
    Exit Sub
    Resume Next
End Sub



Private Sub PreencheProdDefeito()
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    On Error GoTo TrataErro
    
    Regra.totalProd = 0
    ReDim Regra.prod(0)
    EcListaProdutos.Clear
    
    sSql = "SELECT * FROM sl_produto ORDER BY descr_produto "
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    While Not RsProd.EOF
        Regra.totalProd = Regra.totalProd + 1
        ReDim Preserve Regra.prod(Regra.totalProd)
        Regra.prod(Regra.totalProd).cod_produto = BL_HandleNull(RsProd!cod_produto, "")
        Regra.prod(Regra.totalProd).seq_produto = BL_HandleNull(RsProd!seq_produto, mediComboValorNull)
        Regra.prod(Regra.totalProd).descr_produto = BL_HandleNull(RsProd!descr_produto, "")
        Regra.prod(Regra.totalProd).flg_seleccionado = False
        EcListaProdutos.AddItem Regra.prod(Regra.totalProd).descr_produto
        EcListaProdutos.ItemData(EcListaProdutos.NewIndex) = Regra.prod(Regra.totalProd).seq_produto
        EcListaProdutos.Selected(EcListaProdutos.NewIndex) = False
        
        RsProd.MoveNext
    Wend
    RsProd.Close
    Set RsProd = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Lista Produtos: " & Err.Description, Me.Name, "PreencheProdDefeito", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheSensibDefeito()
    Dim sSql As String
    Dim rsSensib As New ADODB.recordset
    On Error GoTo TrataErro
    
    totalSensib = 0
    ReDim sensibilidades(0)
    
    sSql = "SELECT * FROM sl_tbf_t_sensib ORDER BY descr_t_sensib "
    rsSensib.CursorType = adOpenStatic
    rsSensib.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSensib.Open sSql, gConexao
    While Not rsSensib.EOF
        totalSensib = totalSensib + 1
        ReDim Preserve sensibilidades(totalSensib)
        sensibilidades(totalSensib).cod_sensib = Mid(BL_HandleNull(rsSensib!descr_t_sensib, ""), 1, 1)
        sensibilidades(totalSensib).flg_seleccionado = False
        sensibilidades(totalSensib).descr_sensib = BL_HandleNull(rsSensib!descr_t_sensib, "")
        sensibilidades(totalSensib).seq_sensib = BL_HandleNull(rsSensib!Cod_t_sensib, "")
        
        EcListaSensib.AddItem sensibilidades(totalSensib).descr_sensib
        EcListaSensib.ItemData(EcListaSensib.NewIndex) = sensibilidades(totalSensib).seq_sensib
        rsSensib.MoveNext
    Wend
    rsSensib.Close
    Set rsSensib = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Lista Sensibilidades: " & Err.Description, Me.Name, "PreencheSensibDefeito", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheProvasDefeito()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsProvas As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_prova ORDER by descr_prova "
    rsProvas.CursorType = adOpenStatic
    rsProvas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProvas.Open sSql, gConexao
    While Not rsProvas.EOF
        Regra.totalProvasImpr = Regra.totalProvasImpr + 1
        ReDim Preserve Regra.provasImpr(Regra.totalProvasImpr)
        Regra.provasImpr(Regra.totalProvasImpr).seq_prova = BL_HandleNull(rsProvas!seq_prova, "")
        Regra.provasImpr(Regra.totalProvasImpr).Cod_Prova = BL_HandleNull(rsProvas!Cod_Prova, "")
        Regra.provasImpr(Regra.totalProvasImpr).descr_prova = BL_HandleNull(rsProvas!descr_prova, "")
        
        EcListaProvasApresentar.AddItem Regra.provasImpr(Regra.totalProvasImpr).Cod_Prova & " - " & Regra.provasImpr(Regra.totalProvasImpr).descr_prova
        EcListaProvasApresentar.ItemData(EcListaProvasApresentar.NewIndex) = Regra.provasImpr(Regra.totalProvasImpr).seq_prova
        
        rsProvas.MoveNext
    Wend
    rsProvas.Close
    Set rsProvas = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PreencheProvasDefeito: " & Err.Description, Me.Name, "PreencheProvasDefeito", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheAnaDefeito()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_ana_S WHERE (flg_invisivel = 0 or flg_invisivel IS NULL) ORDER by descr_ana_s "
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    While Not rsAna.EOF
        TotalAnaImpr = TotalAnaImpr + 1
        ReDim Preserve AnaImpr(TotalAnaImpr)
        AnaImpr(TotalAnaImpr).seq_ana = BL_HandleNull(rsAna!seq_ana_s, "")
        AnaImpr(TotalAnaImpr).cod_ana = BL_HandleNull(rsAna!cod_ana_s, "")
        AnaImpr(TotalAnaImpr).descr_ana = BL_HandleNull(rsAna!descr_ana_s, "")
        
        EcListaAnaApresentar.AddItem AnaImpr(TotalAnaImpr).cod_ana & " - " & AnaImpr(TotalAnaImpr).descr_ana
        EcListaAnaApresentar.ItemData(EcListaAnaApresentar.NewIndex) = AnaImpr(TotalAnaImpr).seq_ana
        
        rsAna.MoveNext
    Wend
    rsAna.Close
    Set rsAna = Nothing
    
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PreencheAnaDefeito: " & Err.Description, Me.Name, "PreencheAnaDefeito", True
    Exit Sub
    Resume Next
End Sub
Private Sub PreencheAntibDefeito()
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    On Error GoTo TrataErro
    
    TotalAntibioticos = 0
    ReDim antibioticos(0)
    
    sSql = "SELECT * FROM sl_antibio ORDER BY descr_antibio "
    rsAntib.CursorType = adOpenStatic
    rsAntib.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAntib.Open sSql, gConexao
    While Not rsAntib.EOF
        TotalAntibioticos = TotalAntibioticos + 1
        ReDim Preserve antibioticos(TotalAntibioticos)
        antibioticos(TotalAntibioticos).cod_antibio = BL_HandleNull(rsAntib!cod_antibio, "")
        antibioticos(TotalAntibioticos).flg_seleccionado = False
        antibioticos(TotalAntibioticos).descr_antibio = BL_HandleNull(rsAntib!descr_antibio, "")
        antibioticos(TotalAntibioticos).cod_subclasse_antibio = BL_HandleNull(rsAntib!cod_subclasse_antibio, "")
        antibioticos(TotalAntibioticos).cod_subclasse_antibio = BL_HandleNull(rsAntib!cod_subclasse_antibio, "")
        antibioticos(TotalAntibioticos).seq_antib = BL_HandleNull(rsAntib!seq_antibio, mediComboValorNull)
        
        EcListaAntib.AddItem antibioticos(TotalAntibioticos).cod_antibio & " - " & antibioticos(TotalAntibioticos).descr_antibio
        EcListaAntib.ItemData(EcListaAntib.NewIndex) = antibioticos(TotalAntibioticos).seq_antib
        
        EcListaAntibApres.AddItem antibioticos(TotalAntibioticos).cod_antibio & " - " & antibioticos(TotalAntibioticos).descr_antibio
        EcListaAntibApres.ItemData(EcListaAntibApres.NewIndex) = antibioticos(TotalAntibioticos).seq_antib
        rsAntib.MoveNext
    Wend
    rsAntib.Close
    Set rsAntib = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Lista antibioticos: " & Err.Description, Me.Name, "PreencheAntibDefeito", True
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaMicrorg()
    Dim i As Integer
    EcListaMicrorg.Clear
    EcListaMicrorg.Visible = False
    For i = 1 To Regra.totalMicro
        Regra.micro(i).flg_seleccionado = False
        Regra.micro(i).totalAntibRegra = 0
        ReDim Regra.micro(i).antibRegra(0)
        EcListaMicrorg.AddItem Regra.micro(i).descr_microrg
        EcListaMicrorg.ItemData(EcListaMicrorg.NewIndex) = Regra.micro(i).seq_microrg
    Next i
    EcListaMicrorg.Visible = True
End Sub
Private Sub LimpaAntib()
    Dim i As Integer
    EcListaAntib.Clear
    EcListaAntib.Visible = False
    For i = 1 To TotalAntibioticos
        EcListaAntib.AddItem antibioticos(i).cod_antibio & " - " & antibioticos(i).descr_antibio
        EcListaAntib.ItemData(EcListaAntib.NewIndex) = antibioticos(i).seq_antib
    Next i
    EcListaAntib.Visible = True
    
End Sub

Private Sub PreencheMicrorgDefeito()
    Dim sSql As String
    Dim rsMicro As New ADODB.recordset
    On Error GoTo TrataErro
    
    Regra.totalMicro = 0
    ReDim Regra.micro(0)
    EcListaMicrorg.Clear
    
    sSql = "SELECT * FROM sl_microrg ORDER BY descr_microrg "
    rsMicro.CursorType = adOpenStatic
    rsMicro.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMicro.Open sSql, gConexao
    While Not rsMicro.EOF
        Regra.totalMicro = Regra.totalMicro + 1
        ReDim Preserve Regra.micro(Regra.totalMicro)
        Regra.micro(Regra.totalMicro).cod_microrg = BL_HandleNull(rsMicro!cod_microrg, "")
        Regra.micro(Regra.totalMicro).descr_microrg = BL_HandleNull(rsMicro!descr_microrg, "")
        Regra.micro(Regra.totalMicro).seq_microrg = BL_HandleNull(rsMicro!seq_microrg, mediComboValorNull)
        Regra.micro(Regra.totalMicro).cod_gr_microrg = BL_HandleNull(rsMicro!cod_gr_microrg, "")
        Regra.micro(Regra.totalMicro).ordem = 0
        
        EcListaMicrorg.AddItem Regra.micro(Regra.totalMicro).descr_microrg
        EcListaMicrorg.ItemData(EcListaMicrorg.NewIndex) = Regra.micro(Regra.totalMicro).seq_microrg
        
        rsMicro.MoveNext
    Wend
    rsMicro.Close
    Set rsMicro = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Lista Microrganismos: " & Err.Description, Me.Name, "PreencheMicrorgDefeito", True
    Exit Sub
    Resume Next
End Sub



Private Sub PreencheListaAntib()
    Dim i As Integer
    EcListaAntib.Clear
    EcListaAntibApres.Clear
    For i = 1 To TotalAntibioticos
        EcListaAntib.AddItem antibioticos(i).cod_antibio & " - " & antibioticos(i).descr_antibio
        EcListaAntib.ItemData(EcListaAntib.NewIndex) = antibioticos(i).seq_antib
        EcListaAntib.Selected(EcListaAntib.NewIndex) = False
    
        EcListaAntibApres.AddItem antibioticos(i).cod_antibio & " - " & antibioticos(i).descr_antibio
        EcListaAntibApres.ItemData(EcListaAntibApres.NewIndex) = antibioticos(i).seq_antib
        EcListaAntibApres.Selected(EcListaAntibApres.NewIndex) = False
    Next i
    EcListaAntib.ListIndex = 0
End Sub

Private Sub PreencheListaSensib()
    Dim i As Integer
    EcListaSensib.Clear
    For i = 1 To totalSensib
        EcListaSensib.AddItem sensibilidades(i).descr_sensib
        EcListaSensib.ItemData(EcListaSensib.NewIndex) = sensibilidades(i).seq_sensib
        EcListaSensib.Selected(EcListaSensib.NewIndex) = False
    Next i
End Sub

Private Sub EcCodGrEpid_Validate(Cancel As Boolean)
    PA_ValidateGrupoEpid EcCodGrEpid, EcDescrEpid, CStr(gCodLocal)
End Sub


Private Sub ActualizaAntib(iMicro As Integer)
    Dim iAntib As Integer
    Dim flg_existe As Boolean
    Dim i As Integer
    EcListaAntib.Visible = False
    EcListaAntib.Clear
    
    For iAntib = 1 To Regra.micro(iMicro).totalAntibRegra
        If EcCodSubClasseAntibio.text <> "" Then
            If EcCodSubClasseAntibio.text = antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).cod_subclasse_antibio Then
                EcListaAntib.AddItem antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).cod_antibio & " - " & antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).descr_antibio
                EcListaAntib.ItemData(EcListaAntib.NewIndex) = antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).seq_antib
                EcListaAntib.Selected(EcListaAntib.NewIndex) = True
            End If
        Else
            EcListaAntib.AddItem antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).cod_antibio & " - " & antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).descr_antibio
            EcListaAntib.ItemData(EcListaAntib.NewIndex) = antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).seq_antib
            EcListaAntib.Selected(EcListaAntib.NewIndex) = True
        End If
    Next iAntib
    
    ' preenche N�o seleccionados
    For iAntib = 1 To TotalAntibioticos
        flg_existe = False
        For i = 1 To Regra.micro(iMicro).totalAntibRegra
            If iAntib = Regra.micro(iMicro).antibRegra(i).iAntib Then
                flg_existe = True
                Exit For
            End If
        Next i
        
        If flg_existe = False Then
            If EcCodSubClasseAntibio.text <> "" Then
                If EcCodSubClasseAntibio.text = antibioticos(iAntib).cod_subclasse_antibio Then
                    EcListaAntib.AddItem antibioticos(iAntib).cod_antibio & " - " & antibioticos(iAntib).descr_antibio
                    EcListaAntib.ItemData(EcListaAntib.NewIndex) = antibioticos(iAntib).seq_antib
                End If
            Else
                EcListaAntib.AddItem antibioticos(iAntib).cod_antibio & " - " & antibioticos(iAntib).descr_antibio
                EcListaAntib.ItemData(EcListaAntib.NewIndex) = antibioticos(iAntib).seq_antib
            End If
        End If
    Next iAntib
    EcListaAntib.ListIndex = 0
    EcListaAntib.Visible = True
End Sub

Private Sub ActualizaAntibEnvio()
    Dim iAntib As Integer
    Dim flg_existe As Boolean
    Dim i As Integer
    EcListaAntibApres.Visible = False
    EcListaAntibApres.Clear
    
    For iAntib = 1 To Regra.totalAntibImpr
        
        EcListaAntibApres.AddItem antibioticos(Regra.antibImpr(iAntib).iAntib).cod_antibio & " - " & antibioticos(Regra.antibImpr(iAntib).iAntib).descr_antibio
        EcListaAntibApres.ItemData(EcListaAntibApres.NewIndex) = antibioticos(Regra.antibImpr(iAntib).iAntib).seq_antib
        EcListaAntibApres.Selected(EcListaAntibApres.NewIndex) = True
    Next iAntib
    
    ' preenche N�o seleccionados
    For iAntib = 1 To TotalAntibioticos
        flg_existe = False
        For i = 1 To Regra.totalAntibImpr
            If iAntib = Regra.antibImpr(i).iAntib Then
                flg_existe = True
                Exit For
            End If
        Next i
        
        If flg_existe = False Then
            EcListaAntibApres.AddItem antibioticos(iAntib).cod_antibio & " - " & antibioticos(iAntib).descr_antibio
            EcListaAntibApres.ItemData(EcListaAntibApres.NewIndex) = antibioticos(iAntib).seq_antib
            
        End If
    Next iAntib
    EcListaAntibApres.ListIndex = 0
    EcListaAntibApres.Visible = True
End Sub


Private Sub ActualizaSensib(iMicro As Integer, iAntib As Integer)
    Dim i As Integer
    
    For i = 1 To Regra.micro(iMicro).antibRegra(iAntib).totalSens
        If Regra.micro(iMicro).antibRegra(iAntib).sens(i).flg_seleccionado = True Then
            EcListaSensib.Selected(i - 1) = True
        ElseIf EcListaSensib.Selected(i - 1) = True Then
            EcListaSensib.Selected(i - 1) = False
        End If
    Next i
    EcListaSensib.ListIndex = 0
End Sub

Private Sub ActualizaMicro()
    Dim iMicro As Integer
    Dim i As Integer
    Dim flg_existe As Boolean
    
    EcListaMicrorg.Visible = False
    EcListaMicrorg.Clear
    flg_atualizaMicro = False
    ' Preenche Seleccionados
    For iMicro = 1 To Regra.totalMicro
        If Regra.micro(iMicro).flg_seleccionado = True Then
            If EcCodGrMicro.text <> "" Then
                If Regra.micro(iMicro).cod_gr_microrg = EcCodGrMicro.text Then
                    EcListaMicrorg.AddItem Regra.micro(iMicro).descr_microrg
                    EcListaMicrorg.ItemData(EcListaMicrorg.NewIndex) = Regra.micro(iMicro).seq_microrg
                    EcListaMicrorg.Selected(EcListaMicrorg.NewIndex) = True
                
                End If
            Else
                EcListaMicrorg.AddItem Regra.micro(iMicro).descr_microrg
                EcListaMicrorg.ItemData(EcListaMicrorg.NewIndex) = Regra.micro(iMicro).seq_microrg
                EcListaMicrorg.Selected(EcListaMicrorg.NewIndex) = True
            End If
        End If
    Next iMicro
    ' preenche N�o seleccionados
    For iMicro = 1 To Regra.totalMicro
        If Regra.micro(iMicro).flg_seleccionado = False Then
            If EcCodGrMicro.text <> "" Then
                If Regra.micro(iMicro).cod_gr_microrg = EcCodGrMicro.text Then
                    EcListaMicrorg.AddItem Regra.micro(iMicro).descr_microrg
                    EcListaMicrorg.ItemData(EcListaMicrorg.NewIndex) = Regra.micro(iMicro).seq_microrg
                End If
            Else
                EcListaMicrorg.AddItem Regra.micro(iMicro).descr_microrg
                EcListaMicrorg.ItemData(EcListaMicrorg.NewIndex) = Regra.micro(iMicro).seq_microrg
            End If
        End If
    Next iMicro
    EcListaMicrorg.ListIndex = 0
    DoEvents
    EcListaMicrorg.Visible = True
    flg_atualizaMicro = True
End Sub

Private Sub ActualizaAnaImpr()
    Dim iAnaImpr As Integer
    Dim i As Integer
    Dim flg_existe As Boolean
    
    EcListaAnaApresentar.Visible = False
    EcListaAnaApresentar.Clear
    
    For iAnaImpr = 1 To TotalAnaImpr
        If EcPesquisaAnaApres.text <> "" Then
            If InStr(1, UCase(AnaImpr(iAnaImpr).descr_ana), UCase(EcPesquisaAnaApres.text)) > 0 Then
                EcListaAnaApresentar.AddItem AnaImpr(iAnaImpr).cod_ana & " - " & AnaImpr(iAnaImpr).descr_ana
                EcListaAnaApresentar.ItemData(EcListaAnaApresentar.NewIndex) = AnaImpr(iAnaImpr).seq_ana
            End If
        Else
            EcListaAnaApresentar.AddItem AnaImpr(iAnaImpr).cod_ana & " - " & AnaImpr(iAnaImpr).descr_ana
            EcListaAnaApresentar.ItemData(EcListaAnaApresentar.NewIndex) = AnaImpr(iAnaImpr).seq_ana
        End If
    Next iAnaImpr
    If EcListaAnaApresentar.ListCount > 0 Then
        EcListaAnaApresentar.ListIndex = 0
    End If
    DoEvents
    EcListaAnaApresentar.Visible = True

End Sub

Private Sub ActualizaProvasImpr()
    Dim iProvasImpr As Integer
    Dim i As Integer
    Dim flg_existe As Boolean
    
    EcListaProvasApresentar.Visible = False
    EcListaProvasApresentar.Clear
    ' Preenche Seleccionados
    For iProvasImpr = 1 To Regra.totalProvasImpr
        If Regra.provasImpr(iProvasImpr).flg_seleccionado = True Then
            EcListaProvasApresentar.AddItem Regra.provasImpr(iProvasImpr).Cod_Prova & " - " & Regra.provasImpr(iProvasImpr).descr_prova
            EcListaProvasApresentar.ItemData(EcListaProvasApresentar.NewIndex) = Regra.provasImpr(iProvasImpr).seq_prova
            EcListaProvasApresentar.Selected(EcListaProvasApresentar.NewIndex) = True
        End If
    Next iProvasImpr
    ' preenche N�o seleccionados
    For iProvasImpr = 1 To Regra.totalProvasImpr
        If Regra.provasImpr(iProvasImpr).flg_seleccionado = False Then
            EcListaProvasApresentar.AddItem Regra.provasImpr(iProvasImpr).Cod_Prova & " - " & Regra.provasImpr(iProvasImpr).descr_prova
            EcListaProvasApresentar.ItemData(EcListaProvasApresentar.NewIndex) = Regra.provasImpr(iProvasImpr).seq_prova
        End If
    Next iProvasImpr
    EcListaProvasApresentar.ListIndex = 0
    DoEvents
    EcListaProvasApresentar.Visible = True

End Sub



Private Sub ActualizaProd()
    Dim iProd As Integer
    For iProd = 1 To Regra.totalProd
        If Regra.prod(iProd).flg_seleccionado = True Then
            EcListaProdutos.Selected(iProd - 1) = True
        Else
            EcListaProdutos.Selected(iProd - 1) = False
        End If
    Next iProd
End Sub

Private Function GravaMicro(cod_regra_ve As Long) As Boolean
    On Error GoTo TrataErro
    Dim flgAntib As Boolean
    Dim iMicro As Integer
    Dim iAntib As Integer
    Dim iSensib As Integer
    Dim sSql As String
    GravaMicro = False
    sSql = "DELETE FROM sl_cod_regras_ve_micro WHERE cod_regra_ve = " & cod_regra_ve
    BG_ExecutaQuery_ADO sSql

    For iMicro = 1 To Regra.totalMicro
        If Regra.micro(iMicro).totalAntibRegra > 0 Then
            For iAntib = 1 To Regra.micro(iMicro).totalAntibRegra
                For iSensib = 1 To Regra.micro(iMicro).antibRegra(iAntib).totalSens
                    If Regra.micro(iMicro).antibRegra(iAntib).sens(iSensib).flg_seleccionado = True Then
                        sSql = "INSERT INTO sl_cod_regras_ve_micro(cod_regra_ve, cod_micro, cod_antib, sensibilidade) VALUES("
                        sSql = sSql & cod_regra_ve & ", "
                        sSql = sSql & BL_TrataStringParaBD(Regra.micro(iMicro).cod_microrg) & ", "
                        sSql = sSql & BL_TrataStringParaBD(antibioticos(Regra.micro(iMicro).antibRegra(iAntib).iAntib).cod_antibio) & ", "
                        sSql = sSql & BL_TrataStringParaBD(Regra.micro(iMicro).antibRegra(iAntib).sens(iSensib).cod_sensib) & ") "
                        BG_ExecutaQuery_ADO sSql
                    End If
                Next iSensib
            Next iAntib
        Else
            If Regra.micro(iMicro).flg_seleccionado = True Then
                sSql = "INSERT INTO sl_cod_regras_ve_micro(cod_regra_ve, cod_micro) VALUES("
                sSql = sSql & cod_regra_ve & ", "
                sSql = sSql & BL_TrataStringParaBD(Regra.micro(iMicro).cod_microrg) & ") "
                BG_ExecutaQuery_ADO sSql
            End If
        End If
    Next iMicro
    GravaMicro = True
Exit Function
TrataErro:
    GravaMicro = False
    BG_LogFile_Erros "Erro  ao Gravar Lista Microrganismos: " & Err.Description, Me.Name, "GravaMicro", True
    Exit Function
    Resume Next
End Function

Private Function GravaProd(cod_regra_ve As Long) As Boolean
    On Error GoTo TrataErro
    Dim iProd As Integer
    Dim sSql As String
    GravaProd = False
    sSql = "DELETE FROM sl_cod_regras_ve_Prod WHERE cod_regra_ve = " & cod_regra_ve
    BG_ExecutaQuery_ADO sSql
    
    For iProd = 1 To Regra.totalProd
        If Regra.prod(iProd).flg_seleccionado = True Then
            sSql = "INSERT INTO sl_cod_regras_ve_Prod(cod_regra_ve, cod_Produto, ordem) VALUES("
            sSql = sSql & cod_regra_ve & ", "
            sSql = sSql & BL_TrataStringParaBD(Regra.prod(iProd).cod_produto) & ", "
            sSql = sSql & iProd & ") "
            BG_ExecutaQuery_ADO sSql
        End If
    Next iProd
    GravaProd = True
Exit Function
TrataErro:
    GravaProd = False
    BG_LogFile_Erros "Erro  ao Gravar Lista Produtos: " & Err.Description, Me.Name, "GravaProd", True
    Exit Function
    Resume Next
End Function

Private Function GravaAnaEnvio(cod_regra_ve As Integer) As Boolean
    On Error GoTo TrataErro
    Dim iAnaImpr As Integer
    Dim iFrase As Integer
    Dim sSql As String
    GravaAnaEnvio = False
    sSql = "DELETE FROM sl_cod_regras_ve_ana_envio Where cod_regra_ve = " & cod_regra_ve
    BG_ExecutaQuery_ADO sSql
    sSql = "DELETE FROM sl_cod_regras_ve_ana_env_frase Where cod_regra_ve = " & cod_regra_ve
    BG_ExecutaQuery_ADO sSql
    
    For iAnaImpr = 1 To Regra.TotalAnaImprSel
        sSql = "INSERT INTO sl_cod_regras_ve_ana_envio(cod_regra_ve, cod_ana, ordem) VALUES("
        sSql = sSql & cod_regra_ve & ", "
        sSql = sSql & BL_TrataStringParaBD(AnaImpr(Regra.AnaImprSel(iAnaImpr).indice).cod_ana) & ", " & iAnaImpr & ") "
        BG_ExecutaQuery_ADO sSql
        
        For iFrase = 1 To Regra.AnaImprSel(iAnaImpr).totalFrases
            sSql = "INSERT INTO sl_cod_regras_ve_ana_env_frase(cod_regra_ve, cod_ana_s, cod_frase) VALUES("
            sSql = sSql & cod_regra_ve & ", "
            sSql = sSql & BL_TrataStringParaBD(AnaImpr(Regra.AnaImprSel(iAnaImpr).indice).cod_ana) & ", "
            sSql = sSql & BL_TrataStringParaBD(Regra.AnaImprSel(iAnaImpr).frases(iFrase).cod_frase) & ") "
            BG_ExecutaQuery_ADO sSql
        Next iFrase
    Next iAnaImpr
    GravaAnaEnvio = True
Exit Function
TrataErro:
    GravaAnaEnvio = False
    BG_LogFile_Erros "Erro  ao Gravar Lista AnalisesImpr: " & Err.Description, Me.Name, "GravaAnaEnvio", True
    Exit Function
    Resume Next
End Function
Private Function GravaProvasEnvio(cod_regra_ve As Long) As Boolean
    On Error GoTo TrataErro
    Dim iProvasImpr As Integer
    Dim sSql As String
    GravaProvasEnvio = False
    sSql = "DELETE FROM sl_cod_regras_ve_Provas_envio Where cod_regra_ve = " & cod_regra_ve
    BG_ExecutaQuery_ADO sSql
    
    For iProvasImpr = 1 To Regra.totalProvasImpr
        If Regra.provasImpr(iProvasImpr).flg_seleccionado = True Then
            sSql = "INSERT INTO sl_cod_regras_ve_Provas_envio(cod_regra_ve, cod_Prova) VALUES("
            sSql = sSql & cod_regra_ve & ", "
            sSql = sSql & BL_TrataStringParaBD(Regra.provasImpr(iProvasImpr).Cod_Prova) & ") "
            BG_ExecutaQuery_ADO sSql
        End If
    Next iProvasImpr
    GravaProvasEnvio = True
Exit Function
TrataErro:
    GravaProvasEnvio = False
    BG_LogFile_Erros "Erro  ao Gravar Lista ProvaslisesImpr: " & Err.Description, Me.Name, "GravaProvasEnvio", True
    Exit Function
    Resume Next
End Function

Private Function GravaAntibEnvio(cod_regra_ve As Long) As Boolean
    On Error GoTo TrataErro
    Dim iAnt As Integer
    Dim sSql As String
    GravaAntibEnvio = False
    sSql = "DELETE FROM sl_cod_regras_ve_antib_envio WHERE cod_regra_ve = " & cod_regra_ve
    BG_ExecutaQuery_ADO sSql

    For iAnt = 1 To Regra.totalAntibImpr
        sSql = "INSERT INTO sl_cod_regras_ve_antib_envio(cod_regra_ve, cod_antib) VALUES("
        sSql = sSql & cod_regra_ve & ", "
        sSql = sSql & BL_TrataStringParaBD(antibioticos(Regra.antibImpr(iAnt).iAntib).cod_antibio) & ") "
        BG_ExecutaQuery_ADO sSql
    Next iAnt
    GravaAntibEnvio = True
Exit Function
TrataErro:
    GravaAntibEnvio = False
    BG_LogFile_Erros "Erro  ao Gravar Lista Antibioticos a enviar: " & Err.Description, Me.Name, "GravaAntibEnvio", True
    Exit Function
    Resume Next
End Function

Private Function GravaAnalises(cod_regra_ve As Long) As Boolean
    On Error GoTo TrataErro
    Dim iAna As Integer
    Dim sSql As String
    GravaAnalises = False
    sSql = "DELETE FROM sl_cod_regras_ve_ana WHERE cod_regra_ve = " & cod_regra_ve
    BG_ExecutaQuery_ADO sSql
    
   'EDGARPARADA Glintt-HS-19848 * Acrescentado os campos enivar1res, enviar2res e resultado2
    For iAna = 1 To Regra.totalAna
        sSql = "INSERT INTO sl_cod_regras_ve_ana(cod_regra_ve, cod_ana_s, t_res, cod_sinal, resultado, enviar1res, enviar2res, resultado2) VALUES("
        sSql = sSql & cod_regra_ve & ", "
        sSql = sSql & BL_TrataStringParaBD(Regra.ana(iAna).cod_ana_s) & ", "
        sSql = sSql & Regra.ana(iAna).t_res & ", "
        If Regra.ana(iAna).cod_sinal = mediComboValorNull Then
            sSql = sSql & " NULL, "
        Else
            sSql = sSql & Regra.ana(iAna).cod_sinal & ", "
        End If
        sSql = sSql & BL_TrataStringParaBD(CStr(Regra.ana(iAna).resNum)) & ", "
        sSql = sSql & Regra.ana(iAna).enviar1res & ", " & Regra.ana(iAna).enviar2res & ","
        sSql = sSql & Regra.ana(iAna).resNum2 & ")"
        BG_ExecutaQuery_ADO sSql
    '
        GravaAnalisesFrases cod_regra_ve, iAna
    Next iAna
    GravaAnalises = True
Exit Function
TrataErro:
    GravaAnalises = False
    BG_LogFile_Erros "Erro  ao Gravar Lista Analises: " & Err.Description, Me.Name, "GravaAnalises", True
    Exit Function
    Resume Next
End Function


Private Function GravaAnalisesFrases(cod_regra_ve As Long, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    Dim iFrase As Integer
    Dim sSql As String
    GravaAnalisesFrases = False
    sSql = "DELETE FROM sl_cod_regras_ve_ana_frase WHERE cod_regra_ve = " & cod_regra_ve & " AND cod_ana_s = " & BL_TrataStringParaBD(Regra.ana(iAna).cod_ana_s)
    BG_ExecutaQuery_ADO sSql
    
    For iFrase = 1 To Regra.ana(iAna).totalFrases
        sSql = "INSERT INTO sl_cod_regras_ve_ana_frase(cod_regra_ve, cod_ana_s, cod_frase) VALUES("
        sSql = sSql & cod_regra_ve & ", "
        sSql = sSql & BL_TrataStringParaBD(Regra.ana(iAna).cod_ana_s) & ", "
        sSql = sSql & BL_TrataStringParaBD(Regra.ana(iAna).frases(iFrase).cod_frase) & ") "
        BG_ExecutaQuery_ADO sSql
    Next iFrase
    GravaAnalisesFrases = True
Exit Function
TrataErro:
    GravaAnalisesFrases = False
    BG_LogFile_Erros "Erro  ao Gravar Lista frases: " & Err.Description, Me.Name, "GravaAnalisesFrases", True
    Exit Function
    Resume Next
End Function
Private Function ProcuraMicro(cod_regra_ve As Long) As Boolean
    Dim sSql As String
    Dim rsMicro As New ADODB.recordset
    Dim iMicro As Integer
    Dim iAntib As Integer
    Dim iSensib As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim flg_existe As Boolean
    On Error GoTo TrataErro
    ProcuraMicro = False
    LimpaMicrorg
    sSql = "SELECT * from sl_cod_regras_ve_micro WHERE cod_regra_Ve = " & cod_regra_ve
    sSql = sSql & " ORDER by cod_micro, cod_antib, sensibilidade"
    rsMicro.CursorType = adOpenStatic
    rsMicro.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMicro.Open sSql, gConexao
    While Not rsMicro.EOF
        For j = 1 To Regra.totalMicro
            If Regra.micro(j).cod_microrg = BL_HandleNull(rsMicro!cod_micro, "") Then
                Regra.micro(j).flg_seleccionado = True
                iMicro = j
                For k = 1 To TotalAntibioticos
                    If antibioticos(k).cod_antibio = BL_HandleNull(rsMicro!cod_antib, "") Then
                        flg_existe = False
                        For i = 1 To Regra.micro(iMicro).totalAntibRegra
                            If Regra.micro(iMicro).antibRegra(i).iAntib = k Then
                                flg_existe = True
                                iAntib = i
                                Exit For
                            End If
                        Next i
                        If flg_existe = False Then
                            Regra.micro(iMicro).totalAntibRegra = Regra.micro(iMicro).totalAntibRegra + 1
                            iAntib = Regra.micro(iMicro).totalAntibRegra
                            ReDim Preserve Regra.micro(iMicro).antibRegra(iAntib)
                            Regra.micro(iMicro).antibRegra(iAntib).iAntib = k
                            Regra.micro(iMicro).antibRegra(iAntib).totalSens = totalSensib
                            Regra.micro(iMicro).antibRegra(iAntib).sens = sensibilidades
                        End If
                        
                        For iSensib = 1 To totalSensib
                            If sensibilidades(iSensib).cod_sensib = BL_HandleNull(rsMicro!sensibilidade, "") Then
                                Regra.micro(iMicro).antibRegra(iAntib).sens(iSensib).flg_seleccionado = True
                            End If
                        Next iSensib
                    End If
                Next k
            End If
        Next j
        rsMicro.MoveNext
    Wend
    rsMicro.Close
    Set rsMicro = Nothing
    
    ProcuraMicro = True
Exit Function
TrataErro:
    ProcuraMicro = False
    BG_LogFile_Erros "Erro ao Procurar Lista Microrganismos: " & Err.Description, Me.Name, "ProcuraMicro", True
    Exit Function
    Resume Next
End Function

Private Function ProcuraAna(cod_regra_ve As Long) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim iAna As Integer
    On Error GoTo TrataErro
    ProcuraAna = False
    Regra.totalAna = 0
    ReDim Regra.ana(0)
     'EDGARPARADA Glintt-HS-19848 * add enviar1res, enviar2res e resultado2
    sSql = "SELECT sl_ana_s.cod_ana_s, sl_ana_s.descr_Ana_s,sl_cod_regras_ve_Ana.t_res, cod_sinal, resultado, enviar1res, enviar2res, resultado2"
    sSql = sSql & " FROM sl_cod_regras_ve_Ana, sl_ana_s WHERE cod_regra_Ve = " & cod_regra_ve
    sSql = sSql & " AND sl_cod_regras_ve_ana.cod_ana_s = sl_ana_s.cod_ana_s ORDER by descr_ana_s "
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    While Not rsAna.EOF
        Regra.totalAna = Regra.totalAna + 1
        ReDim Preserve Regra.ana(Regra.totalAna)
        totalFrasesAux = 0
        ReDim frasesAux(0)
        
        Regra.ana(Regra.totalAna).cod_ana_s = BL_HandleNull(rsAna!cod_ana_s, "")
        Regra.ana(Regra.totalAna).descr_ana_s = BL_HandleNull(rsAna!descr_ana_s, "")
        Regra.ana(Regra.totalAna).t_res = BL_HandleNull(rsAna!t_res, "")
        If Regra.ana(Regra.totalAna).t_res = gT_Frase Then
            Regra.ana(Regra.totalAna).resNum = ""
            Regra.ana(Regra.totalAna).cod_sinal = mediComboValorNull
            ProcuraAnaFrase cod_regra_ve, Regra.totalAna
        Else
            Regra.ana(Regra.totalAna).resNum = BL_HandleNull(rsAna!resultado, "")
            Regra.ana(Regra.totalAna).cod_sinal = BL_HandleNull(rsAna!cod_sinal, mediComboValorNull)
            'EDGARPARADA Glintt-HS-19848
            Regra.ana(Regra.totalAna).enviar1res = BL_HandleNull(rsAna!enviar1res, 0)
            Regra.ana(Regra.totalAna).enviar2res = BL_HandleNull(rsAna!enviar2res, 0)
            Regra.ana(Regra.totalAna).resNum2 = BL_HandleNull(rsAna!resultado2, -1)
            '
        End If
        
     '
        rsAna.MoveNext
    Wend
    rsAna.Close
    Set rsAna = Nothing
    ActualizaListaAna
    ProcuraAna = True
Exit Function
TrataErro:
    ProcuraAna = False
    BG_LogFile_Erros "Erro ao Procurar Lista An�lises: " & Err.Description, Me.Name, "ProcuraAna", True
    Exit Function
    Resume Next
End Function

Private Function ProcuraAnaFrase(cod_regra_ve As Long, iAna As Integer) As Boolean
    Dim sSql As String
    Dim rsFrase As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    ProcuraAnaFrase = False
    sSql = "SELECT x2.cod_frase, x2.descr_frase, x2.seq_frase from sl_cod_regras_ve_ana_frase x1, sl_dicionario x2 "
    sSql = sSql & " WHERE cod_regra_Ve = " & cod_regra_ve & " AND cod_ana_S = " & BL_TrataStringParaBD(Regra.ana(iAna).cod_ana_s)
    sSql = sSql & " AND x1.cod_frase =x2.cod_frase "
    
    rsFrase.CursorType = adOpenStatic
    rsFrase.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFrase.Open sSql, gConexao
    While Not rsFrase.EOF
        Regra.ana(iAna).totalFrases = Regra.ana(iAna).totalFrases + 1
        ReDim Preserve Regra.ana(iAna).frases(Regra.ana(iAna).totalFrases)
        Regra.ana(iAna).frases(Regra.ana(iAna).totalFrases).cod_frase = BL_HandleNull(rsFrase!cod_frase, "")
        Regra.ana(iAna).frases(Regra.ana(iAna).totalFrases).descr_frase = BL_HandleNull(rsFrase!descr_frase, "")
        Regra.ana(iAna).frases(Regra.ana(iAna).totalFrases).seq_frase = BL_HandleNull(rsFrase!seq_frase, mediComboValorNull)
        AtualizaFrases iAna
        rsFrase.MoveNext
    Wend
    frasesAux = Regra.ana(iAna).frases
    totalFrasesAux = Regra.ana(iAna).totalFrases
    
    rsFrase.Close
    Set rsFrase = Nothing
    
    ProcuraAnaFrase = True
Exit Function
TrataErro:
    ProcuraAnaFrase = False
    BG_LogFile_Erros "Erro ao Procurar Lista Frases de An�lises: " & Err.Description, Me.Name, "ProcuraAnaFrase", True
    Exit Function
    Resume Next
End Function
Private Sub AtualizaFrases(iAna As Integer)
    Dim iFrase As Integer
    EcListaFrases.Clear
    For iFrase = 1 To Regra.ana(iAna).totalFrases
        EcListaFrases.AddItem Regra.ana(iAna).frases(iFrase).descr_frase
        EcListaFrases.ItemData(EcListaFrases.NewIndex) = Regra.ana(iAna).frases(iFrase).seq_frase
    Next iFrase
End Sub
Private Function ProcuraProd(cod_regra_ve As Long) As Boolean
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    Dim iProd As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    ProcuraProd = False
    For i = 0 To EcListaProdutos.ListCount - 1
        If EcListaProdutos.Selected(i) = True Then
            EcListaProdutos.Selected(i) = False
        End If
    Next i
    sSql = "SELECT * from sl_cod_regras_ve_prod WHERE cod_regra_Ve = " & cod_regra_ve
    sSql = sSql & " ORDER by cod_produto"
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    While Not RsProd.EOF
        For iProd = 1 To Regra.totalProd
            If Regra.prod(iProd).cod_produto = BL_HandleNull(RsProd!cod_produto, "") Then
                Regra.prod(iProd).flg_seleccionado = True
                EcListaProdutos.Selected(iProd - 1) = True
            End If
        Next iProd
        RsProd.MoveNext
    Wend
    RsProd.Close
    Set RsProd = Nothing
    
    ProcuraProd = True
    EcListaProdutos.ListIndex = 0
Exit Function
TrataErro:
    ProcuraProd = False
    BG_LogFile_Erros "Erro ao Procurar Lista Produtos: " & Err.Description, Me.Name, "ProcuraProd", True
    Exit Function
    Resume Next
End Function

Private Function ProcuraAntibEnvio(cod_regra_ve As Long) As Boolean
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    Dim iAntib As Integer
    On Error GoTo TrataErro
    
    ProcuraAntibEnvio = False
    sSql = "SELECT * from sl_cod_regras_ve_antib_envio WHERE cod_regra_Ve = " & cod_regra_ve
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    While Not RsProd.EOF
        For iAntib = 1 To TotalAntibioticos
            If antibioticos(iAntib).cod_antibio = BL_HandleNull(RsProd!cod_antib, "") Then
                Regra.totalAntibImpr = Regra.totalAntibImpr + 1
                ReDim Preserve Regra.antibImpr(Regra.totalAntibImpr)
                Regra.antibImpr(Regra.totalAntibImpr).iAntib = iAntib
            End If
        Next iAntib
        RsProd.MoveNext
    Wend
    RsProd.Close
    Set RsProd = Nothing
    
    ProcuraAntibEnvio = True
Exit Function
TrataErro:
    ProcuraAntibEnvio = False
    BG_LogFile_Erros "Erro ao Procurar Lista Antib: " & Err.Description, Me.Name, "ProcuraAntibEnvio", True
    Exit Function
    Resume Next
End Function

Private Sub ProcuraAnaEnvio(cod_regra_ve As Integer)
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    Dim rsFrase As New ADODB.recordset
    
    Dim iAnaImpr As Integer
    
    On Error GoTo TrataErro
    Regra.TotalAnaImprSel = 0
    ReDim Regra.AnaImprSel(0)
    
    sSql = "SELECT * from sl_cod_regras_ve_ana_envio WHERE cod_regra_Ve = " & cod_regra_ve
    sSql = sSql & " ORDER by ordem "
    
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    While Not RsProd.EOF
        Regra.TotalAnaImprSel = Regra.TotalAnaImprSel + 1
        ReDim Preserve Regra.AnaImprSel(Regra.TotalAnaImprSel)
        
        For iAnaImpr = 1 To TotalAnaImpr
            If AnaImpr(iAnaImpr).cod_ana = BL_HandleNull(RsProd!cod_ana, "") Then
                Regra.AnaImprSel(Regra.TotalAnaImprSel).indice = iAnaImpr
                EcListaAnaEnvioSel.AddItem AnaImpr(iAnaImpr).cod_ana & " - " & AnaImpr(iAnaImpr).descr_ana
                EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.NewIndex) = AnaImpr(iAnaImpr).seq_ana
                Exit For
            End If
        Next iAnaImpr
        
        Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases = 0
        ReDim Regra.AnaImprSel(Regra.TotalAnaImprSel).frases(0)
        sSql = "SELECT x2.cod_frase, x2.descr_frase, x2.seq_frase FROM  sl_cod_regras_ve_ana_env_frase x1, sl_dicionario x2 "
        sSql = sSql & " WHERE x1.cod_frase = x2.cod_frase AND x1.cod_regra_ve = " & cod_regra_ve
        sSql = sSql & " AND x1.cod_ana_s = " & BL_TrataStringParaBD(RsProd!cod_ana)
        rsFrase.CursorType = adOpenStatic
        rsFrase.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsFrase.Open sSql, gConexao
        While Not rsFrase.EOF
            Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases = Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases + 1
            ReDim Preserve Regra.AnaImprSel(Regra.TotalAnaImprSel).frases(Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases)
            Regra.AnaImprSel(Regra.TotalAnaImprSel).frases(Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases).cod_frase = BL_HandleNull(rsFrase!cod_frase, "")
            Regra.AnaImprSel(Regra.TotalAnaImprSel).frases(Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases).descr_frase = BL_HandleNull(rsFrase!descr_frase, "")
            Regra.AnaImprSel(Regra.TotalAnaImprSel).frases(Regra.AnaImprSel(Regra.TotalAnaImprSel).totalFrases).seq_frase = BL_HandleNull(rsFrase!seq_frase, mediComboValorNull)
            
            rsFrase.MoveNext
        Wend
        rsFrase.Close
        
        RsProd.MoveNext
    Wend
    RsProd.Close
    Set RsProd = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar Lista Ana Envio: " & Err.Description, Me.Name, "ProcuraAnaEnvio", True
    Exit Sub
    Resume Next
End Sub

Private Sub ProcuraProvasEnvio(cod_regra_ve As Long)
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    Dim iProvasImpr As Integer
    On Error GoTo TrataErro
    For iProvasImpr = 1 To Regra.totalProvasImpr
        Regra.provasImpr(iProvasImpr).flg_seleccionado = False
    Next iProvasImpr
    
    sSql = "SELECT * from sl_cod_regras_ve_Provas_envio WHERE cod_regra_Ve = " & cod_regra_ve
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao
    While Not RsProd.EOF
        For iProvasImpr = 1 To Regra.totalProvasImpr
            If Regra.provasImpr(iProvasImpr).Cod_Prova = BL_HandleNull(RsProd!Cod_Prova, "") Then
                Regra.provasImpr(iProvasImpr).flg_seleccionado = True
            End If
        Next iProvasImpr
        RsProd.MoveNext
    Wend
    RsProd.Close
    Set RsProd = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar Lista Provas Envio: " & Err.Description, Me.Name, "ProcuraProvasEnvio", True
    Exit Sub
    Resume Next
End Sub

Private Sub OptTipoRegra_Click(Index As Integer)
    If OptTipoRegra(0).value = True Then
        EcResNum.text = ""
        EcCodFrase.text = ""
        EcDescrFrase.text = ""
        CbSinal.ListIndex = mediComboValorNull
        SSTab.Tab = 0
        SSTab.TabEnabled(2) = False
        SSTab.TabEnabled(3) = True
        SSTab.TabEnabled(0) = True
        EcTipoRegra.text = "0"
        
        
    Else
        SSTab.Tab = 2
        'SSTab.Tab = 1
        SSTab.TabEnabled(0) = False
        SSTab.TabEnabled(2) = True
        SSTab.TabEnabled(3) = False
        EcTipoRegra.text = "1"
      
    End If
    
End Sub

Private Sub OptTRes_Click(Index As Integer)
    If Index = 0 Then
        EcDescrFrase.Enabled = False
        BtPesquisaRapidaFrases.Enabled = True
        EcResNum.Enabled = False
        CbSinal.Enabled = False
        CbSinal.ListIndex = mediComboValorNull
        EcResNum.text = ""
    ElseIf Index = 2 Then
        EcDescrFrase.Enabled = False
        BtPesquisaRapidaFrases.Enabled = False
        EcResNum.Enabled = True
        CbSinal.Enabled = True

        EcCodFrase.text = ""
        EcDescrFrase.text = ""
    End If
End Sub

Private Sub ActualizaListaAna()
    Dim iAna As Integer
    Dim iFrase As Integer
    EcListaAna.Clear
    
    For iAna = 1 To Regra.totalAna
        EcListaAna.AddItem Regra.ana(iAna).cod_ana_s & "      " & Regra.ana(iAna).descr_ana_s
        If Regra.ana(iAna).t_res = gT_Alfanumerico Then
            EcListaAna.List(EcListaAna.NewIndex) = EcListaAna.List(EcListaAna.NewIndex) & "(" & Regra.ana(iAna).cod_sinal & " " & Regra.ana(iAna).resNum & ")"
        Else
            EcListaAna.List(EcListaAna.NewIndex) = EcListaAna.List(EcListaAna.NewIndex) & "("
            For iFrase = 1 To Regra.ana(iAna).totalFrases
                EcListaAna.List(EcListaAna.NewIndex) = EcListaAna.List(EcListaAna.NewIndex) & Regra.ana(iAna).frases(iFrase).descr_frase & " "
            Next iFrase
            EcListaAna.List(EcListaAna.NewIndex) = EcListaAna.List(EcListaAna.NewIndex) & ")"
        End If
    Next
End Sub


Private Function DevolveIndiceMicro(indice As Integer) As Integer
    Dim i As Integer
    Dim j As Integer
    
    DevolveIndiceMicro = mediComboValorNull
    If indice = mediComboValorNull Then
        Exit Function
    End If
    For i = 1 To Regra.totalMicro
        If Regra.micro(i).seq_microrg = EcListaMicrorg.ItemData(indice) Then
            DevolveIndiceMicro = i
            Exit Function
        End If
    Next i
    DevolveIndiceMicro = mediComboValorNull
End Function

Private Function DevolveIndiceAntib(iMicro As Integer, indice As Integer) As Integer
    Dim i As Integer
    Dim j As Integer
    DevolveIndiceAntib = mediComboValorNull
    If iMicro = mediComboValorNull Then
        Exit Function
    End If
    For i = 1 To TotalAntibioticos
        If antibioticos(i).seq_antib = EcListaAntib.ItemData(indice) Then
            For j = 1 To Regra.micro(iMicro).totalAntibRegra
                If Regra.micro(iMicro).antibRegra(j).iAntib = i Then
                    DevolveIndiceAntib = j
                    Exit For
                End If
            Next j
            Exit Function
        End If
    Next i
    DevolveIndiceAntib = mediComboValorNull
End Function

Private Function DevolveIndiceAntibApres(iMicro As Integer, indice As Integer) As Integer
    Dim i As Integer
    Dim j As Integer
    DevolveIndiceAntibApres = mediComboValorNull
    For i = 1 To TotalAntibioticos
        If antibioticos(i).seq_antib = EcListaAntibApres.ItemData(indice) Then
            For j = 1 To Regra.totalAntibImpr
                If Regra.antibImpr(j).iAntib = i Then
                    DevolveIndiceAntibApres = j
                    Exit For
                End If
            Next j
            Exit Function
        End If
    Next i
    DevolveIndiceAntibApres = mediComboValorNull
End Function

Private Function DevolveIndiceAnaApres(indice As Integer) As Integer
    Dim i As Integer
    Dim j As Integer
    DevolveIndiceAnaApres = mediComboValorNull
    For i = 1 To TotalAnaImpr
        If AnaImpr(i).seq_ana = EcListaAnaApresentar.ItemData(indice) Then
            DevolveIndiceAnaApres = i
            Exit Function
        End If
    Next i
    DevolveIndiceAnaApres = mediComboValorNull
End Function
Private Function DevolveIndiceProvaApres(indice As Integer) As Integer
    Dim i As Integer
    Dim j As Integer
    DevolveIndiceProvaApres = mediComboValorNull
    For i = 1 To Regra.totalProvasImpr
        If Regra.provasImpr(i).seq_prova = EcListaProvasApresentar.ItemData(indice) Then
            DevolveIndiceProvaApres = i
            Exit Function
        End If
    Next i
    DevolveIndiceProvaApres = mediComboValorNull
End Function
Private Sub AdicionaNovoAntibSel(iMicro As Integer, item As Integer)
    Dim i As Integer
    Regra.micro(iMicro).totalAntibRegra = Regra.micro(iMicro).totalAntibRegra + 1
    ReDim Preserve Regra.micro(iMicro).antibRegra(Regra.micro(iMicro).totalAntibRegra)
    For i = 1 To TotalAntibioticos
        If antibioticos(i).seq_antib = EcListaAntib.ItemData(item) Then
            Regra.micro(iMicro).antibRegra(Regra.micro(iMicro).totalAntibRegra).iAntib = i
            Regra.micro(iMicro).antibRegra(Regra.micro(iMicro).totalAntibRegra).totalSens = totalSensib
            Regra.micro(iMicro).antibRegra(Regra.micro(iMicro).totalAntibRegra).sens = sensibilidades
            Exit For
        End If
    Next i

End Sub

Private Sub AdicionaNovoAntibApres(item As Integer)
    Dim i As Integer
    Regra.totalAntibImpr = Regra.totalAntibImpr + 1
    ReDim Preserve Regra.antibImpr(Regra.totalAntibImpr)
    For i = 1 To TotalAntibioticos
        If antibioticos(i).seq_antib = EcListaAntibApres.ItemData(item) Then
            Regra.antibImpr(Regra.totalAntibImpr).iAntib = i
            Exit For
        End If
    Next i

End Sub



Private Sub BtBaixo_Click()

    Dim sdummy As String
    Dim ldummy As Long
    Dim iAux As Integer
    If EcListaAnaEnvioSel.ListIndex < EcListaAnaEnvioSel.ListCount - 1 Then
        sdummy = EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex + 1)
        ldummy = EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex + 1)
        iAux = Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 2).indice
        
        EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex + 1) = EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex)
        EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex + 1) = EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex)
        Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 2).indice = Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).indice
        
        EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex) = sdummy
        EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex) = ldummy
        Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).indice = iAux
        
        EcListaAnaEnvioSel.ListIndex = EcListaAnaEnvioSel.ListIndex + 1
    End If
    
End Sub
Private Sub BtCima_Click()

    Dim sdummy As String
    Dim ldummy As Long
    Dim iAux As Integer
    
    If EcListaAnaEnvioSel.ListIndex > 0 Then
        sdummy = EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex - 1)
        ldummy = EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex - 1)
        iAux = Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex).indice
        
        EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex - 1) = EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex)
        EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex - 1) = EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex)
        Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex).indice = Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).indice
        
        EcListaAnaEnvioSel.List(EcListaAnaEnvioSel.ListIndex) = sdummy
        EcListaAnaEnvioSel.ItemData(EcListaAnaEnvioSel.ListIndex) = ldummy
        Regra.AnaImprSel(EcListaAnaEnvioSel.ListIndex + 1).indice = iAux
        
        EcListaAnaEnvioSel.ListIndex = EcListaAnaEnvioSel.ListIndex - 1
    End If
    
End Sub

Private Sub GrupoTipoSINAVE()
    Dim sSql As String
    Dim rs As New ADODB.recordset
    
    On Error GoTo TrataErro
  
    
    sSql = "SELECT tipo_notificacao from sl_cod_gr_epid WHERE cod_gr_epid = " & EcCodGrEpid
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    While Not rs.EOF
        If rs.Fields("tipo_notificacao").value = tipo_notificacao.ws_sinave Then
            SSTab.TabEnabled(3) = False
            SSTab.TabEnabled(4) = False
            SSTab.TabEnabled(5) = False
        Else
            If EcTipoRegra.text = "0" Then
                SSTab.TabEnabled(2) = False
                SSTab.TabEnabled(3) = True
                SSTab.TabEnabled(0) = True
            ElseIf EcTipoRegra.text = "1" Then
                SSTab.TabEnabled(0) = False
                SSTab.TabEnabled(2) = True
                SSTab.TabEnabled(3) = False
            End If
        End If
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no GrupoTipoSINAVE: " & Err.Description, Me.Name, "GrupoTipoSINAVE", True
    Exit Sub
    Resume Next
End Sub

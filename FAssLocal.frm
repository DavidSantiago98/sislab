VERSION 5.00
Begin VB.Form FormAssLocal 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionFormAssLocal"
   ClientHeight    =   1695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5475
   Icon            =   "FAssLocal.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1695
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox EcLocal 
      Height          =   315
      Left            =   240
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   480
      Width           =   5055
   End
   Begin VB.CommandButton ButtonOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   1080
      Width           =   1095
   End
   Begin VB.CommandButton ButtonCancelar 
      Caption         =   "Sair"
      Height          =   375
      Left            =   4200
      TabIndex        =   1
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Local"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "FormAssLocal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim bContinuar As Boolean
Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes
    PreencheValoresDefeito
End Sub
Private Sub EventoUnload()
    If bContinuar = True Then
        gCodLocal = BG_DaComboSel(EcLocal)
        If Dir(gDirCliente & "\bin\Local.ini") <> "" Then
            Kill gDirCliente & "\bin\Local.ini"
        End If
        
        'Gravar valor
        Dim iLocal As Integer

        iLocal = FreeFile
        Open gDirCliente & "\bin\" & "Local.ini" For Append As #iLocal
        Print #iLocal, "[Local]"
        Print #iLocal, ""
        Print #iLocal, vbTab & "Codigo=" & gCodLocal
        Close #iLocal
    Else
        BG_Fecha_ConexaoBD_ADO
        End
    End If
End Sub

Private Sub Inicializacoes()
    Me.Width = 5565
    Me.Height = 2070
    Me.Caption = "Local de " & BG_SYS_GetComputerName
    Me.Left = 1665
    Me.Top = 2160
End Sub

Private Sub PreencheValoresDefeito()
    Dim sSql As String
    
    bContinuar = False
    
    BL_PreencheSelComboLocais EcLocal, False
End Sub

Private Sub ButtonCancelar_Click()
    Unload Me
End Sub

Private Sub ButtonOK_Click()
    If EcLocal.ListIndex <> -1 Then
        gMsgResp = MsgBox("Confirma que pertence ao local:" & vbCrLf & vbCrLf & EcLocal.List(EcLocal.ListIndex), vbQuestion + vbYesNo)
        If gMsgResp = vbNo Then
            Exit Sub
        End If
        
        gCodLocal = 1
        bContinuar = True
    
        Unload Me
    Else
        MsgBox "Ter� de indicar um local.", vbInformation
    End If
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub


Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub


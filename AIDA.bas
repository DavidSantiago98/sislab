Attribute VB_Name = "AIDA"
Option Explicit

Public Type IdentifAIDA
    Codigo As String
    cod_apl As String
    num_sequencial As String
    num_processo As String
    num_utente As String
    nome As String
    Sexo As String
    data_nasc As String
    morada As String
    cod_postal As String
    cod_subsistema As String
    csubsistema As String
    dsubsistema As String
    ccustos As String
    dcustos As String
    cespfis As String
    despfis As String
    cunifis As String
    dunifis As String
    cespresp As String
    despresp As String
    cuniresp As String
    duniresp As String
    dataadm As String
End Type


Public Function AIDA_InsereUtente(e As Long, m As String, t_sit As String) As Long
    Dim sSql As String
    Dim rsAida As New ADODB.recordset
    Dim Utente As IdentifAIDA
    Dim t_utente As String
    Dim i As Integer
    Dim seq As Long
    Utente = AIDA_lerSonho3(e, m, "dilnova2008")
    If Utente.num_sequencial <> "" Then
        If Utente.num_sequencial > 0 Then
            t_utente = "CHP"
        Else
            t_utente = "DTR"
        End If
        sSql = "SELECT * FROM sl_identif WHERE t_utente =" & BL_TrataStringParaBD(t_utente) & " AND utente =" & Utente.num_sequencial
        rsAida.CursorLocation = adUseServer
        rsAida.CursorType = adOpenStatic
        rsAida.Open sSql, gConexao
        If rsAida.RecordCount >= 1 Then
            AIDA_InsereUtente = BL_HandleNull(rsAida!seq_utente, mediComboValorNull)
            sSql = "UPDATE sl_identif set t_sit = " & t_sit & ", n_epis = " & BL_TrataStringParaBD(CStr(e))
            sSql = sSql & ", n_proc_1 = " & BL_TrataStringParaBD(Utente.num_processo)
            sSql = sSql & " WHERE Seq_utente =" & BL_HandleNull(rsAida!seq_utente, mediComboValorNull)
            BG_ExecutaQuery_ADO sSql
        Else
            'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
            i = 0
            seq = -1
            While seq = -1 And i <= 10
                seq = BL_GeraNumero("SEQ_UTENTE")
                i = i + 1
            Wend
            
            If seq = -1 Then
                BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
                Exit Function
            End If
            
            
            gSQLError = 0
            gSQLISAM = 0
            
            
            sSql = "INSERT INTO sl_identif (seq_utente,t_utente,utente,n_proc_1,n_proc_2," & _
                    " dt_inscr,n_cartao_ute,nome_ute,dt_nasc_ute,sexo_ute,est_civ_ute, " & _
                    " descr_mor_ute,cod_postal_ute,telef_ute, cod_efr_ute,user_cri,dt_cri,nome_mae,nome_pai, n_epis, t_sit )" & _
                    " values (" & seq & "," & BL_TrataStringParaBD(t_utente) & "," & BL_TrataStringParaBD(Utente.num_sequencial) & "," & BL_TrataStringParaBD(Utente.num_processo) & ",null" & _
                    "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Utente.num_utente) & "," & BL_TrataStringParaBD(Utente.nome) & "," & BL_TrataStringParaBD(Utente.data_nasc) & "," & BL_TrataStringParaBD(Utente.Sexo) & ",null" & _
                    "," & BL_TrataStringParaBD(Utente.morada) & "," & BL_TrataStringParaBD(Utente.cod_postal) & ",null," & BL_TrataStringParaBD(BL_HandleNull(Utente.cod_subsistema, "990004")) & ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(Bg_DaData_ADO) & ",null,null,"
            sSql = sSql & BL_TrataStringParaBD(CStr(e)) & "," & t_sit & ")"
                    
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro
            AIDA_InsereUtente = seq
        End If
        rsAida.Close
        Set rsAida = Nothing
    Else
        AIDA_InsereUtente = mediComboValorNull
    End If
End Function

' ----------------------------------------------------------------------------------------------

' CONSTRU��O DO XML

' ----------------------------------------------------------------------------------------------
Public Function AIDA_lerSonho3(e As Long, m As String, palavra As String) As IdentifAIDA
    Dim seq_log_aida As Long
    Dim strSoapAction As String
    Dim strXml As String
    Dim RetornoXML As String
    Dim utenteAIDA As IdentifAIDA
    On Error GoTo TrataErro
    If AIDA_InicializaEstrut(utenteAIDA) = False Then
        Exit Function
    End If
    
    If BL_HandleNull(gAIDAWebService, mediComboValorNull) = mediComboValorNull Then
        Exit Function
    End If
    strSoapAction = "http://tempuri.org/aidaweb/Service1/lerSonho3"
    
    strXml = ""
    strXml = strXml & "<?xml version=""1.0"" encoding=""utf-8""?>"
    strXml = strXml & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
    strXml = strXml & "     <soap:Body>"
    strXml = strXml & "         <lerSonho3 xmlns=""http://tempuri.org/aidaweb/Service1"">"
    strXml = strXml & "             <E>" & e & "</E>"
    strXml = strXml & "             <M>" & m & "</M>"
    strXml = strXml & "             <palavra>" & palavra & "</palavra>"
    strXml = strXml & "         </lerSonho3>"
    strXml = strXml & "     </soap:Body>"
    strXml = strXml & "</soap:Envelope>"

    seq_log_aida = AIDA_InsereLog(strXml, gAIDAWebService)
    If seq_log_aida > mediComboValorNull Then
        RetornoXML = AIDA_PostWebservice(gAIDAWebService, strSoapAction, strXml)
        AIDA_ConstroiEstrutura utenteAIDA, UCase(RetornoXML)
        AIDA_ActualizaLog seq_log_aida, RetornoXML
    End If
    AIDA_lerSonho3 = utenteAIDA
Exit Function
TrataErro:
    BG_LogFile_Erros "AIDA_lerSonho3: " & Err.Description, "AIDA", "AIDA_lerSonho3", True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------

' INSERE LOG NA TABELA DE LOG COM A CHAMADA AO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Private Function AIDA_InsereLog(xml_request As String, xml_url As String) As Long
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsLog As New ADODB.recordset
    Dim seq_log As Long
    Dim iReg As Integer
    AIDA_InsereLog = mediComboValorNull
    sSql = "SELECT seq_log_aida.nextval proximo FROM dual"
    rsLog.CursorLocation = adUseServer
    rsLog.CursorType = adOpenStatic
    rsLog.Open sSql, gConexao
    If rsLog.RecordCount = 1 Then
        seq_log = BL_HandleNull(rsLog!proximo, "")
    End If
    rsLog.Close
    Set rsLog = Nothing
    If seq_log > mediComboValorNull Then
        sSql = "INSERT INTO sl_log_aida(Seq_log_aida, xml_request,xml_url, dt_cri, user_Cri) VALUES("
        sSql = sSql & seq_log & "," & BL_TrataStringParaBD(xml_request) & "," & BL_TrataStringParaBD(xml_url) & ","
        sSql = sSql & " sysdate," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg = 1 Then
            AIDA_InsereLog = seq_log
        End If
    End If
Exit Function
TrataErro:
    AIDA_InsereLog = mediComboValorNull
    BG_LogFile_Erros "AIDA_InsereLog: " & Err.Description, "AIDA", "AIDA_InsereLog", True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------

' CHAMADA AO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Private Function AIDA_PostWebservice(ByVal AsmxUrl As String, ByVal SoapActionUrl As String, ByVal XmlBody As String) As String
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim strRet As String
    Dim intPos1 As Long
    Dim intPos2 As Long
    
    On Error GoTo TrataErro
    
    ' Create objects to DOMDocument and XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    ' Load XML
    objDom.async = False
    objDom.loadXML XmlBody
    
    ' Open the webservice
    objXmlHttp.Open "POST", AsmxUrl, False
    
    ' Create headings
    objXmlHttp.SetRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.SetRequestHeader "SOAPAction", SoapActionUrl
    ' Send XML command
    objXmlHttp.Send objDom.xml

    ' Get all response text from webservice
    strRet = objXmlHttp.ResponseText
    
    ' Close object
    Set objXmlHttp = Nothing
    
    ' Return result
    strRet = Replace(strRet, "&lt;", "<")
    strRet = Replace(strRet, "&gt;", ">")
    AIDA_PostWebservice = strRet
        
    
Exit Function
TrataErro:
    AIDA_PostWebservice = ""
    BG_LogFile_Erros "AIDA_PostWebservice: " & Err.Description, "AIDA", "AIDA_PostWebservice", True
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------------------

' ACTUALIZA A TABELA DE LOG COM A RESPOSTA DO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Private Function AIDA_ActualizaLog(seq_log As Long, xml_response As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsLog As New ADODB.recordset
    Dim iReg As Integer
    
    AIDA_ActualizaLog = False
    If seq_log > mediComboValorNull Then
        sSql = "UPDATE sl_log_aida SET xml_response = " & BL_TrataStringParaBD(xml_response) & ","
        sSql = sSql & " dt_act = sysdate, user_act = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
        sSql = sSql & " WHERE seq_log_aida = " & seq_log
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg = 1 Then
            AIDA_ActualizaLog = True
        End If
    End If
Exit Function
TrataErro:
    AIDA_ActualizaLog = False
    BG_LogFile_Erros "AIDA_ActualizaLog: " & Err.Description, "AIDA", "AIDA_ActualizaLog", True
    Exit Function
    Resume Next
End Function

Private Function AIDA_ConstroiEstrutura(utenteAIDA As IdentifAIDA, textoXML As String) As Boolean
     On Error GoTo TrataErro
     AIDA_ConstroiEstrutura = False
     If AIDA_ConstroiEstruturaIdentif(utenteAIDA, textoXML) = False Then
        GoTo TrataErro
    End If
    AIDA_ConstroiEstrutura = True
Exit Function
TrataErro:
    AIDA_ConstroiEstrutura = False
    BG_LogFile_Erros "AIDA_ConstroiEstrutura: " & Err.Description, "AIDA", "AIDA_ConstroiEstrutura", True
    Exit Function
    Resume Next
End Function

Private Function AIDA_ConstroiEstruturaIdentif(utenteAIDA As IdentifAIDA, textoXML As String) As Boolean
    Dim varString As String
    On Error GoTo TrataErro
    AIDA_ConstroiEstruturaIdentif = False
    
    utenteAIDA.ccustos = AIDA_RetornaConteudoTag(textoXML, "CCUSTOS")
    
    utenteAIDA.Codigo = AIDA_RetornaConteudoTag(textoXML, "CODITO")
    utenteAIDA.cod_apl = AIDA_RetornaConteudoTag(textoXML, "COD_APL")
    utenteAIDA.num_sequencial = AIDA_RetornaConteudoTag(textoXML, "NUM_SEQUENCIAL")
    utenteAIDA.num_processo = AIDA_RetornaConteudoTag(textoXML, "NUM_PROCESSO")
    utenteAIDA.num_utente = AIDA_RetornaConteudoTag(textoXML, "NUM_UTENTE")
    utenteAIDA.nome = AIDA_RetornaConteudoTag(textoXML, "NOME")
    utenteAIDA.Sexo = AIDA_RetornaConteudoTag(textoXML, "SEXO")
    If utenteAIDA.Sexo = 2 Then
        utenteAIDA.Sexo = 0
    ElseIf utenteAIDA.Sexo = 1 Then
        utenteAIDA.Sexo = 1
    End If
    utenteAIDA.data_nasc = AIDA_RetornaConteudoTag(textoXML, "DATA_NASC")
    utenteAIDA.morada = AIDA_RetornaConteudoTag(textoXML, "MORADA")
    utenteAIDA.cod_postal = AIDA_RetornaConteudoTag(textoXML, "COD_POSTAL")
    utenteAIDA.cod_subsistema = AIDA_RetornaConteudoTag(textoXML, "COD_SUBSISTEMA")
    utenteAIDA.csubsistema = BL_HandleNull(AIDA_RetornaConteudoTag(textoXML, "CSUBSISTEMA"), "990004")
    utenteAIDA.dsubsistema = AIDA_RetornaConteudoTag(textoXML, "DSUBSISTEMA")
    utenteAIDA.ccustos = AIDA_RetornaConteudoTag(textoXML, "CCUSTOS")
    utenteAIDA.dcustos = AIDA_RetornaConteudoTag(textoXML, "DCUSTOS")
    utenteAIDA.cespfis = AIDA_RetornaConteudoTag(textoXML, "CESPFIS")
    utenteAIDA.despfis = AIDA_RetornaConteudoTag(textoXML, "DESPFIS")
    utenteAIDA.cunifis = AIDA_RetornaConteudoTag(textoXML, "CUNIFIS")
    utenteAIDA.dunifis = AIDA_RetornaConteudoTag(textoXML, "DUNIFIS")
    utenteAIDA.cespresp = AIDA_RetornaConteudoTag(textoXML, "CESPRESP")
    utenteAIDA.despresp = AIDA_RetornaConteudoTag(textoXML, "DESPRESP")
    utenteAIDA.cuniresp = AIDA_RetornaConteudoTag(textoXML, "CUNIRESP")
    utenteAIDA.duniresp = AIDA_RetornaConteudoTag(textoXML, "DUNIRESP")
    utenteAIDA.dataadm = AIDA_RetornaConteudoTag(textoXML, "DATAADM")
    AIDA_ConstroiEstruturaIdentif = True
Exit Function
TrataErro:
    AIDA_ConstroiEstruturaIdentif = False
    BG_LogFile_Erros "AIDA_ConstroiEstruturaIdentif: " & Err.Description, "AIDA", "AIDA_ConstroiEstruturaIdentif", True
    Exit Function
    Resume Next
End Function

Private Function AIDA_RetornaConteudoTag(textoXML As String, variavel As String) As String
    On Error GoTo TrataErro
    Dim iInicio As Long
    Dim iTamanho As Long
    If InStr(1, textoXML, "<" & variavel & ">") > 0 Then
       iInicio = InStr(1, textoXML, "<" & variavel & ">") + Len("<" & variavel & ">")
       iTamanho = InStr(1, textoXML, "</" & variavel & ">") - iInicio
       AIDA_RetornaConteudoTag = Mid(textoXML, iInicio, iTamanho)
    End If
    
Exit Function
TrataErro:
    AIDA_RetornaConteudoTag = ""
    BG_LogFile_Erros "AIDA_RetornaConteudoTag: " & Err.Description, "AIDA", "AIDA_RetornaConteudoTag", True
    Exit Function
    Resume Next
End Function


Private Function AIDA_InicializaEstrut(utenteAIDA As IdentifAIDA) As Boolean
    On Error GoTo TrataErro
    AIDA_InicializaEstrut = False
    utenteAIDA.Codigo = ""
    utenteAIDA.cod_apl = ""
    utenteAIDA.num_sequencial = ""
    utenteAIDA.num_processo = ""
    utenteAIDA.num_utente = ""
    utenteAIDA.nome = ""
    utenteAIDA.Sexo = ""
    utenteAIDA.data_nasc = ""
    utenteAIDA.morada = ""
    utenteAIDA.cod_postal = ""
    utenteAIDA.cod_subsistema = ""
    utenteAIDA.csubsistema = ""
    utenteAIDA.dsubsistema = ""
    utenteAIDA.ccustos = ""
    utenteAIDA.dcustos = ""
    utenteAIDA.cespfis = ""
    utenteAIDA.despfis = ""
    utenteAIDA.cunifis = ""
    utenteAIDA.dunifis = ""
    utenteAIDA.cespresp = ""
    utenteAIDA.despresp = ""
    utenteAIDA.cuniresp = ""
    utenteAIDA.duniresp = ""
    utenteAIDA.dataadm = ""
    AIDA_InicializaEstrut = True
Exit Function
TrataErro:
    AIDA_InicializaEstrut = False
    BG_LogFile_Erros "AIDA_InicializaEstrut: " & Err.Description, "AIDA", "AIDA_InicializaEstrut", True
    Exit Function
    Resume Next
End Function

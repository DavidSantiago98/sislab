Attribute VB_Name = "Colheitas"
Option Explicit

Public Type colheita
    seq_colheita As String
    dt_ini As String
    dt_fim As String
    dt_cri As String
    user_cri As String
    seq_utente As Long
End Type

'NELSONPSILVA CHVNG-7461 29.10.2018
Public Type PedidoReutil
    cod_ana As String
    descr_ana As String
    nome_med As String
    nome_tecnico As String
    n_prescricao As String
    dt_cri As String
    hr_cri As String
    descr_proven As String
    seq_colheita As String
    n_req As String
End Type

Public analisesReutil() As PedidoReutil
Dim TotalAnaReutil As Integer

'NELSONPSILVA CHVNG-7461 29.10.2018
Public Type PedidoColheita
    cod_ana As String
    descr_ana As String
    dt_cri As String
    hr_cri As String
    nome_med As String
    nome_tecnico As String
    n_prescricao As String
    seq_colheita As String
End Type

Public analisesColh() As PedidoColheita
Dim TotalAnaColh As Integer

'NELSONPSILVA CHVNG-7461 29.10.2018
Public Type RequisColheita
    n_req As String
    seq_colheita As String
End Type
Public reqColh() As RequisColheita
Public TotalReqColh As Long
'

Public Function COLH_InsereNovaColheita(seq_utente As Long) As String
    On Error GoTo TrataErro
    Dim sSql As String
    Dim seq_colheita As String
    Dim iReg As Integer
    seq_colheita = CStr(BL_RetornaSequencia("SEQ_COLHEITAS"))
    If seq_colheita <> "" And seq_colheita <> "-1" Then
        sSql = "INSERT INTO sl_colheitas ( seq_colheita, dt_ini, dt_fim, dt_cri, user_cri, seq_utente) VALUES("
        sSql = sSql & BL_TrataStringParaBD(seq_colheita) & ", sysdate, sysdate + " & gMaxTempoColheita & ", sysdate, "
        sSql = sSql & CStr(gCodUtilizador) & "," & seq_utente & ")"
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg = 1 Then
            COLH_InsereNovaColheita = seq_colheita
        Else
            COLH_InsereNovaColheita = ""
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "COLH_InsereNovaColheita - ERRO: " & Err.Description, "COLH", "COLH_InsereNovaColheita", True
    COLH_InsereNovaColheita = ""
    Exit Function
    Resume Next
End Function

Public Function COLH_AssociaReqColheita(n_req As String, seq_colheita As String, seq_utente As Long) As String
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    If seq_colheita = "" Then
        seq_colheita = COLH_InsereNovaColheita(seq_utente)
    End If
    If seq_colheita <> "" And seq_colheita <> "-1" Then
        sSql = "INSERT INTO sl_req_colheitas ( n_Req, seq_colheita) VALUES("
        sSql = sSql & n_req & "," & BL_TrataStringParaBD(seq_colheita) & ")"
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg = 1 Then
            COLH_AssociaReqColheita = seq_colheita
        Else
            COLH_AssociaReqColheita = ""
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "COLH_AssociaReqColheita - ERRO: " & Err.Description, "COLH", "COLH_AssociaReqColheita", True
    COLH_AssociaReqColheita = ""
    Exit Function
    Resume Next
End Function

Public Function COLH_DevolveNumColheita(ByVal n_req As String) As String
    Dim sSql As String
    Dim rsColh As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_req_colheitas where n_Req = " & n_req
    rsColh.CursorType = adOpenStatic
    rsColh.CursorLocation = adUseServer
    rsColh.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsColh.Open sSql, gConexao
        If rsColh.RecordCount > 0 Then
            COLH_DevolveNumColheita = BL_HandleNull(rsColh!seq_colheita, "")
        Else
            COLH_DevolveNumColheita = ""
        End If
    rsColh.Close
    Set rsColh = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "COLH_DevolveNumColheita - ERRO: " & Err.Description, "COLH", "COLH_DevolveNumColheita", True
    COLH_DevolveNumColheita = ""
    Exit Function
    Resume Next
End Function

Public Function COLH_DevolveColheitaPresc(n_presc As String) As String
    Dim sSql As String
    Dim rsColh As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_req_colheitas where n_req in (select n_req from sl_requis where n_prescricao = " & n_presc & ")"
    rsColh.CursorType = adOpenStatic
    rsColh.CursorLocation = adUseServer
    rsColh.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsColh.Open sSql, gConexao
        If rsColh.RecordCount > 0 Then
            COLH_DevolveColheitaPresc = BL_HandleNull(rsColh!seq_colheita, "")
        Else
            COLH_DevolveColheitaPresc = ""
        End If
    rsColh.Close
    Set rsColh = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "COLH_DevolveColheitaPresc - ERRO: " & Err.Description, "COLH", "COLH_DevolveColheitaPresc", True
    COLH_DevolveColheitaPresc = ""
    Exit Function
    Resume Next
End Function

'NELSONPSILVA CHVNG-7461 29.10.2018
' DADO UMA ANALISE VERIFICA SE � REUTILIZDA
Public Sub COLH_EstrutReutil(requis As String, colheita As String)

    Dim sSql As String

    Dim rsAnaReutil As New ADODB.recordset
       
    ReDim analisesReutil(0)
    TotalAnaReutil = 0

    On Error GoTo TrataErro
    rsAnaReutil.CursorLocation = adUseServer
    rsAnaReutil.CursorType = adOpenStatic

   
     sSql = "(Select distinct x2.cod_ana, x2.descr_ana, x5.nome_med, x1.user_val_canc_reutil, x6.n_prescricao, "
     sSql = sSql & " TO_DATE (x6.dt_cri, 'dd/mm/yyyy') dt_cri, x6.hr_cri, x4.descr_proven, x1.seq_colheita, x1.n_req "
     sSql = sSql & " FROM sl_req_colheitas x1, slv_analises x2, sl_marcacoes x3, sl_proven x4, sl_medicos x5, sl_requis x6 "
     sSql = sSql & " WHERE "
     sSql = sSql & " x1.n_req = x3.n_req AND x1.n_req = x6.n_req AND nvl(x1.estado_reutil,'0') <> '2' " 'AND x1.flg_reutil = 1 "
     sSql = sSql & " AND x3.cod_agrup = x2.cod_ana AND x1.seq_colheita = " & BL_TrataStringParaBD(colheita) & ""
     sSql = sSql & " AND x6.cod_med = x5.cod_med (+) AND x6.cod_proven = x4.cod_proven (+)) "
     'sSql = sSql & " AND x1.n_req NOT IN (SELECT n_req FROM sl_req_colheitas WHERE n_req = " & BL_TrataStringParaBD(requis) & "))"
     'sSql = sSql & " AND x1.cod_local = " & gCodLocal & ""
     sSql = sSql & " UNION"
     sSql = sSql & " (Select distinct x2.cod_ana, x2.descr_ana, x5.nome_med, x1.user_val_canc_reutil, x6.n_prescricao, "
     sSql = sSql & " TO_DATE (x6.dt_cri, 'dd/mm/yyyy') dt_cri, x6.hr_cri, x4.descr_proven, x1.seq_colheita, x1.n_req "
     sSql = sSql & " FROM sl_req_colheitas x1, slv_analises x2, sl_realiza x3, sl_proven x4, sl_medicos x5, sl_requis x6 "
     sSql = sSql & " WHERE "
     sSql = sSql & " x1.n_req = x3.n_req AND x1.n_req = x6.n_req AND nvl(x1.estado_reutil,'0') <> '2' " 'AND x1.flg_reutil = 1 "
     sSql = sSql & " AND x3.cod_agrup = x2.cod_ana AND x1.seq_colheita = " & BL_TrataStringParaBD(colheita) & ""
     sSql = sSql & " AND x6.cod_med = x5.cod_med (+) AND x6.cod_proven = x4.cod_proven (+)) "
     'sSql = sSql & " AND x1.n_req NOT IN (SELECT n_req FROM sl_req_colheitas WHERE n_req = " & BL_TrataStringParaBD(requis) & "))"
     sSql = sSql & " ORDER BY n_prescricao, cod_ana"
    'Set rsReq = BG_ExecutaSELECT(sSql)

    rsAnaReutil.Open sSql, gConexao
    If rsAnaReutil.RecordCount > 0 Then
        While Not rsAnaReutil.EOF
        COLH_PreencheEstrutPedido BL_HandleNull(rsAnaReutil!cod_ana, ""), BL_HandleNull(rsAnaReutil!descr_ana, " "), _
                                   BL_HandleNull(rsAnaReutil!nome_med, ""), _
                                   BL_DevolveNomeUtilizadorActual(BL_HandleNull(rsAnaReutil!user_val_canc_reutil, "")), _
                                   BL_HandleNull(rsAnaReutil!n_prescricao, ""), BL_HandleNull(rsAnaReutil!dt_cri, ""), _
                                   BL_HandleNull(rsAnaReutil!hr_cri, ""), BL_HandleNull(rsAnaReutil!descr_proven, ""), _
                                   BL_HandleNull(rsAnaReutil!seq_colheita, ""), BL_HandleNull(rsAnaReutil!n_req, "")
                                   
        rsAnaReutil.MoveNext
'            While Not rsQuery.EOF
'
'            PreencheReutilAnalises BL_HandleNull(rsReq!descr_ana, ""), BL_HandleNull(rsReq!cod_tubo, ""), BL_HandleNull(rsReq!desc_tubo, ""), _
'                                   BL_HandleNull(rsReq!dt_chega, ""), BL_HandleNull(rsReq!hr_chega, ""), BL_HandleNull(rsReq!dt_colheita, ""), _
'                                   BL_HandleNull(rsReq!hr_colheita, "")
'
'            rsReq.MoveNext
        Wend

        rsAnaReutil.Close
        
    End If
    
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, "REUTILIZA��O", "PreencheEstrutReutil", True
    Exit Sub
    Resume Next
End Sub

Public Sub COLH_PreencheEstrutPedido(cod_ana As String, descr_ana As String, nome_med As String, _
                                 user_val_canc_reutil As String, n_prescricao As String, _
                                 dt_cri As String, hr_cri As String, descr_proven As String, seq_colheita As String, n_req As String)
                                 
    
    On Error GoTo TrataErro
                
    TotalAnaReutil = TotalAnaReutil + 1
    ReDim Preserve analisesReutil(TotalAnaReutil)
    analisesReutil(TotalAnaReutil).cod_ana = cod_ana
    analisesReutil(TotalAnaReutil).descr_ana = descr_ana
    analisesReutil(TotalAnaReutil).nome_med = nome_med
    analisesReutil(TotalAnaReutil).nome_tecnico = user_val_canc_reutil
    analisesReutil(TotalAnaReutil).n_prescricao = n_prescricao
    analisesReutil(TotalAnaReutil).dt_cri = dt_cri
    analisesReutil(TotalAnaReutil).hr_cri = hr_cri
    analisesReutil(TotalAnaReutil).descr_proven = descr_proven
    analisesReutil(TotalAnaReutil).seq_colheita = seq_colheita
    analisesReutil(TotalAnaReutil).n_req = n_req

Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro ao Preencher An�lises Pedidas:" & Err.Description, "REUTILIZA��O", "PreencheEstrutPedido", True
    Exit Sub
    Resume Next
End Sub

'NELSONPSILVA CHVNG-7461 29.10.2018
Public Function COLH_Existe_Ana_Reutil(ByVal analise As String, Optional colheita As String) As Boolean
    Dim i As Integer
    COLH_Existe_Ana_Reutil = False
    For i = 1 To UBound(analisesReutil)
        If colheita <> "" Then
             If analise = Trim(analisesReutil(i).cod_ana) And colheita = Trim(analisesReutil(i).seq_colheita) Then
                COLH_Existe_Ana_Reutil = True
                Exit Function
             End If
        Else
             If analise = Trim(analisesReutil(i).cod_ana) Then
                COLH_Existe_Ana_Reutil = True
                Exit Function
             End If
        End If
    Next i
End Function


Public Sub COLH_EstrutAnaReutil(requis As String, colheita As String)

    Dim sSql As String
    Dim rsAnaColh As New ADODB.recordset

    ReDim analisesColh(0)
    TotalAnaColh = 0

    On Error GoTo TrataErro
    rsAnaColh.CursorLocation = adUseServer
    rsAnaColh.CursorType = adOpenStatic


     sSql = "(Select distinct x2.cod_ana, x2.descr_ana, x5.nome_med, x1.user_val_canc_reutil, x6.n_prescricao, x1.dt_val_canc_reutil, x1.hr_val_canc_reutil, "
     sSql = sSql & " TO_DATE (x6.dt_cri, 'dd/mm/yyyy') dt_cri, x6.hr_cri, x4.descr_proven, x1.seq_colheita"
     sSql = sSql & " FROM sl_req_colheitas x1, slv_analises x2, sl_marcacoes x3, sl_proven x4, sl_medicos x5, sl_requis x6 "
     sSql = sSql & " WHERE "
     sSql = sSql & " x1.n_req = x3.n_req AND x1.n_req = x6.n_req AND x1.estado_reutil = 1 AND x1.flg_reutil = 1 "
     sSql = sSql & " AND x3.cod_agrup = x2.cod_ana AND x1.seq_colheita = " & BL_TrataStringParaBD(colheita) & ""
     sSql = sSql & " AND x6.cod_med = x5.cod_med (+) AND x6.cod_proven = x4.cod_proven (+) "
     sSql = sSql & " AND x1.n_req NOT IN (SELECT n_req FROM sl_req_colheitas WHERE n_req = " & BL_TrataStringParaBD(requis) & "))"

    rsAnaColh.Open sSql, gConexao
    If rsAnaColh.RecordCount > 0 Then
        While Not rsAnaColh.EOF

         TotalAnaColh = TotalAnaColh + 1
        ReDim Preserve analisesColh(TotalAnaColh)
        analisesColh(TotalAnaColh).cod_ana = BL_HandleNull(rsAnaColh!cod_ana, "")
        analisesColh(TotalAnaColh).descr_ana = BL_HandleNull(rsAnaColh!descr_ana, " ")
        analisesColh(TotalAnaColh).nome_med = BL_HandleNull(rsAnaColh!nome_med, "")
        analisesColh(TotalAnaColh).nome_tecnico = BL_DevolveNomeUtilizadorActual(BL_HandleNull(rsAnaColh!user_val_canc_reutil, ""))
        analisesColh(TotalAnaColh).dt_cri = BL_HandleNull(rsAnaColh!dt_val_canc_reutil, "")
        analisesColh(TotalAnaColh).hr_cri = BL_HandleNull(rsAnaColh!hr_val_canc_reutil, "")
        analisesColh(TotalAnaColh).n_prescricao = BL_HandleNull(rsAnaColh!n_prescricao, "")
        analisesColh(TotalAnaColh).seq_colheita = BL_HandleNull(rsAnaColh!seq_colheita, "")

        rsAnaColh.MoveNext
        Wend

        rsAnaColh.Close

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, "REUTILIZA��O", "IREtrutAnaReutil", True
    Exit Sub
    Resume Next
End Sub

'NELSONPSILVA CHVNG-7461 29.10.2018
Public Function COLH_Existe_Ana_Repetida(analise As String) As String
    Dim j As Integer
    COLH_Existe_Ana_Repetida = ""
    For j = 1 To UBound(analisesColh)
        If analise = Trim(analisesColh(j).cod_ana) Then
           COLH_Existe_Ana_Repetida = j
           Exit Function
        End If
    Next j
End Function

'NELSONPSILVA CHVNG-7461 29.10.2018
' DADO UMA COLHEITA RETORNA AS REQUISI��ES
' -----------------------------------------------------------------------------------------------
Public Sub COLH_DevolveReqColheita(colheita As String)
    Dim sSql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    ReDim reqColh(0)
    TotalReqColh = 0

    sSql = "SELECT * FROM sl_req_colheitas where seq_colheita = " & colheita & " AND nvl(estado_reutil,'0') <> '2' order by n_req asc"
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
        
    If rs.RecordCount > 0 Then
        While Not rs.EOF

        TotalReqColh = TotalReqColh + 1
        ReDim Preserve reqColh(TotalReqColh)
        reqColh(TotalReqColh).n_req = BL_HandleNull(rs!n_req, "")
        reqColh(TotalReqColh).seq_colheita = BL_HandleNull(rs!seq_colheita, " ")
        rs.MoveNext
        Wend

    End If
    rs.Close
    Set rs = Nothing
Exit Sub
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "DevolveReqColheita", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub

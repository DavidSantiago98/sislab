VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormReqUte 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormReqUte"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10110
   Icon            =   "FormReqUte.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   10110
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodGrupo 
      Height          =   285
      Left            =   720
      TabIndex        =   38
      Top             =   6480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   6720
      TabIndex        =   35
      Top             =   6120
      Width           =   975
   End
   Begin VB.TextBox EcTipoUtente 
      Height          =   285
      Left            =   3480
      TabIndex        =   31
      Top             =   5880
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Frame Frame6 
      Height          =   735
      Left            =   120
      TabIndex        =   25
      Top             =   960
      Width           =   9855
      Begin VB.TextBox EcReqAux 
         Height          =   285
         Left            =   8280
         TabIndex        =   40
         Top             =   240
         Visible         =   0   'False
         Width           =   1365
      End
      Begin VB.ComboBox CbGrupo 
         Height          =   315
         Left            =   5040
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   240
         Width           =   2325
      End
      Begin VB.TextBox EcEstado 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1560
         TabIndex        =   30
         Top             =   240
         Width           =   2655
      End
      Begin VB.TextBox EcEpisodio 
         Height          =   285
         Left            =   8280
         TabIndex        =   28
         Top             =   240
         Width           =   1365
      End
      Begin VB.TextBox EcNumReq 
         Height          =   285
         Left            =   720
         TabIndex        =   27
         Top             =   240
         Width           =   855
      End
      Begin VB.Label LaReqAux 
         AutoSize        =   -1  'True
         Caption         =   "N� An�lise"
         Height          =   195
         Left            =   7440
         TabIndex        =   39
         Top             =   240
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "Grupo "
         Height          =   255
         Left            =   4440
         TabIndex        =   36
         Top             =   240
         Width           =   855
      End
      Begin VB.Label LbAdmin 
         AutoSize        =   -1  'True
         Caption         =   "Epis�dio"
         Height          =   195
         Left            =   7440
         TabIndex        =   29
         Top             =   240
         Width           =   720
      End
      Begin VB.Label Label9 
         Caption         =   "Req."
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.TextBox EcCodUteSeq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   6000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   120
      TabIndex        =   11
      Top             =   0
      Width           =   9855
      Begin VB.ComboBox CbTipoUtente 
         Height          =   315
         Left            =   720
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton BtPesquisaUte 
         Height          =   320
         Left            =   3840
         Picture         =   "FormReqUte.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Pesquisa R�pida de Utentes"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcDataInscr 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   8640
         TabIndex        =   6
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcUtente 
         Height          =   285
         Left            =   1680
         TabIndex        =   0
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox EcProcHosp1 
         Height          =   285
         Left            =   5160
         TabIndex        =   2
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox EcNome 
         Height          =   285
         Left            =   720
         TabIndex        =   4
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   6360
         TabIndex        =   5
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcProcHosp2 
         Height          =   285
         Left            =   7320
         TabIndex        =   3
         Top             =   240
         Width           =   2295
      End
      Begin VB.Frame Frame3 
         Caption         =   "Frame3"
         Height          =   15
         Left            =   7320
         TabIndex        =   12
         Top             =   1080
         Width           =   2295
         Begin VB.Frame Frame5 
            Caption         =   "Frame5"
            Height          =   15
            Left            =   0
            TabIndex        =   13
            Top             =   0
            Width           =   2295
         End
      End
      Begin VB.Label Label5 
         Caption         =   "Data inscr."
         Height          =   255
         Left            =   7680
         TabIndex        =   18
         Top             =   600
         Width           =   855
      End
      Begin VB.Label LbUtente 
         AutoSize        =   -1  'True
         Caption         =   "Utente"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   480
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Pr. Hosp."
         Height          =   195
         Left            =   4440
         TabIndex        =   16
         Top             =   240
         Width           =   660
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   420
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data nasc."
         Height          =   195
         Left            =   5400
         TabIndex        =   14
         Top             =   600
         Width           =   780
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FGReq 
      Height          =   3135
      Left            =   120
      TabIndex        =   7
      ToolTipText     =   "Duplo Click para consultar an�lises da requisi��o."
      Top             =   2160
      Width           =   9240
      _ExtentX        =   16298
      _ExtentY        =   5530
      _Version        =   393216
      Rows            =   1
      Cols            =   1
      FixedRows       =   0
      FixedCols       =   0
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      TextStyle       =   1
      FocusRect       =   2
      HighLight       =   2
      GridLines       =   3
      SelectionMode   =   1
   End
   Begin MSFlexGridLib.MSFlexGrid FGProd 
      Height          =   3135
      Left            =   120
      TabIndex        =   8
      Top             =   2160
      Visible         =   0   'False
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   5530
      _Version        =   393216
      Rows            =   1
      Cols            =   1
      FixedRows       =   0
      FixedCols       =   0
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      TextStyle       =   1
      FocusRect       =   2
      HighLight       =   2
      GridLines       =   3
      SelectionMode   =   1
   End
   Begin VB.Frame FrBtProd 
      Height          =   3255
      Left            =   9375
      TabIndex        =   33
      Top             =   2070
      Width           =   585
      Begin VB.CommandButton BtImpRes 
         Height          =   375
         Left            =   50
         Picture         =   "FormReqUte.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   42
         ToolTipText     =   "Imprimir Resultados Com Valida��o M�dica"
         Top             =   2040
         Width           =   480
      End
      Begin VB.CommandButton BtMedicosReq 
         Height          =   480
         Left            =   45
         Picture         =   "FormReqUte.frx":06E0
         Style           =   1  'Graphical
         TabIndex        =   41
         ToolTipText     =   "M�dicos da Requisi��o"
         Top             =   120
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton BtProdReq 
         Height          =   615
         Left            =   45
         Picture         =   "FormReqUte.frx":09EA
         Style           =   1  'Graphical
         TabIndex        =   34
         ToolTipText     =   "Consultar produtos da requisi��o."
         Top             =   2520
         Width           =   500
      End
   End
   Begin VB.Frame FrHelpProd 
      Height          =   3255
      Left            =   8835
      TabIndex        =   9
      Top             =   2070
      Visible         =   0   'False
      Width           =   1140
      Begin VB.CommandButton BtSairProd 
         Caption         =   "&Voltar"
         Height          =   1215
         Left            =   120
         Picture         =   "FormReqUte.frx":12B4
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Voltar para as requisi��es do Utente."
         Top             =   1920
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.Label Label4 
         Caption         =   "Legenda:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   255
         Left            =   165
         TabIndex        =   24
         Top             =   360
         Width           =   795
      End
      Begin VB.Label Label3 
         Caption         =   "Falta."
         ForeColor       =   &H000000C0&
         Height          =   255
         Left            =   165
         TabIndex        =   23
         Top             =   1560
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Chegou."
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   165
         TabIndex        =   22
         Top             =   1200
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Por chegar."
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   165
         TabIndex        =   21
         Top             =   840
         Width           =   825
      End
   End
   Begin VB.Label LbeResults 
      BackColor       =   &H80000004&
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   6480
      TabIndex        =   43
      Top             =   1680
      Width           =   2895
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodUteSeq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   6000
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormReqUte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 31/01/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim VectorCamposBD

Dim NomeTabela As String
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object

Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

'Comando e parametro utilizados para seleccionar dados dos utentes
Dim CmdUtente As New ADODB.Command
Dim PmtSeqUte As ADODB.Parameter

'Comando e parametro utilizados para seleccionar requisicoes do utente
Dim CmdReqUte As New ADODB.Command

'Comando e parametro utilizados para seleccionar produtos da requisi��o
Dim CmdProdReq As New ADODB.Command
Dim PmtReq As ADODB.Parameter

'Comando e parametro utilizados para seleccionar designacao dos produtos
Dim CmdDescrProd As New ADODB.Command
Dim PmtCodProd As ADODB.Parameter

'Comando e parametro utilizados para seleccionar designacao das especificacoes
Dim CmdDescrEsp As New ADODB.Command
Dim PmtCodEsp As ADODB.Parameter

Private Type requisicoes
    n_req As String
    estado As String
    data As String
    Data_Cheg As String
    Data_Impr As String
    req_aux As String
    Produtos As String
End Type
Dim ReqUte() As requisicoes
Dim ReqIndex As Integer

Private Type Produtos
    descricao As String
    Especificacao As String
    Data_Prev As String
    Data_Cheg As String
    Cor_Estado As Variant
End Type
Dim ProdReq() As Produtos
Dim ProdIndex As Integer


Private Type Visualizado
    n_req As String
    Visualizada As Boolean
End Type
Dim Vis() As Visualizado
Dim VisIndex As Integer

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind As Integer
Dim Max  As Integer
Dim fReq As Long
Dim fCodGrupo As String
'vari�veis que guardam o nr. da an�lise 'IGM
Dim fReqAux As String

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    Max = -1
    ReDim FOPropertiesTemp(0)
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BL_FimProcessamento Me
    gF_REQUTE = 1
    If gF_REQUIS_PRIVADO = 0 Then
        gDUtente.seq_utente = ""
    End If
    
End Sub

Sub EventoActivate()

    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind

        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
    
    If gDUtente.seq_utente <> "" Then
        Call LimpaCampos
        EcCodUteSeq.Text = gDUtente.seq_utente
        Call FuncaoProcurar
    End If
    
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
            
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    Select Case gLAB
        Case "CITO", "BIO"
            LaReqAux.Visible = True
            EcReqAux.Visible = True
            LaReqAux.caption = "Nr. An�lise"
            BtMedicosReq.Visible = True
        Case "GM"
            LaReqAux.Visible = True
            EcReqAux.Visible = True
            LaReqAux.caption = "Nr. Caso"
            BtMedicosReq.Visible = True
    End Select
    
End Sub

Sub FuncaoSeguinte()
    
    Dim i As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
        
    If FgProd.Visible = True Then
        FgProd.Visible = False
        FgReq.Visible = True
        FrHelpProd.Visible = False
        BtSairProd.Visible = False
        FrBtProd.Visible = True
        BtProdReq.Visible = True
        ProdIndex = -1
        ReDim ProdReq(0)
        
        FgProd.Clear
        i = FgProd.rows - 2
        While i > 0
            FgProd.RemoveItem i
            i = i - 1
        Wend
        FgProd.TextMatrix(0, 0) = "Produtos"
        FgProd.TextMatrix(0, 1) = "Especifica��es"
        FgProd.TextMatrix(0, 2) = "Data Prevista"
        FgProd.TextMatrix(0, 3) = "Data Chegada"
    End If
        
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoAnterior()
    
    Dim i As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    
    If FgProd.Visible = True Then
        FgProd.Visible = False
        FgReq.Visible = True
        FrHelpProd.Visible = False
        BtSairProd.Visible = False
        BtProdReq.Visible = True
        ProdIndex = -1
        ReDim ProdReq(0)
        
        FgProd.Clear
        i = FgProd.rows - 2
        While i > 0
            FgProd.RemoveItem i
            i = i - 1
        Wend
        FgProd.TextMatrix(0, 0) = "Produtos"
        FgProd.TextMatrix(0, 1) = "Especifica��es"
        FgProd.TextMatrix(0, 2) = "Data Prevista"
        FgProd.TextMatrix(0, 3) = "Data Chegada"
    End If
        
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub EventoUnload()
    
    If gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.Enabled = True
        Set gFormActivo = FormGestaoRequisicaoPrivado
    Else
        Set gFormActivo = MDIFormInicio
    End If
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    gDUtente.seq_utente = ""
    
    Set CmdReqUte = Nothing
    Set PmtSeqUte = Nothing
    Set CmdProdReq = Nothing
    Set CmdDescrEsp = Nothing
    Set CmdDescrProd = Nothing
    Set PmtReq = Nothing
    Set PmtCodEsp = Nothing
    Set PmtCodProd = Nothing
    
    Set FormReqUte = Nothing
    gF_REQUTE = 0

End Sub

Sub Funcao_CopiaUte()
    
    If Trim(gDUtente.seq_utente) <> "" Then
        EcCodUteSeq = CLng(gDUtente.seq_utente)
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

End Sub

Sub Funcao_CopiaReq()
    
    If gRequisicaoActiva <> 0 Then
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_nasc_ute", EcDataNasc, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_inscr", EcDataInscr, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
        
    With FgReq
        .rows = 2
        .FixedRows = 1
        .Cols = 6
        .FixedCols = 0
        .AllowBigSelection = True
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 0
        .ColWidth(0) = 1000
        .ColWidth(1) = 3000
        .ColWidth(2) = 1150
        .ColWidth(3) = 1150
        .ColWidth(4) = 2000
        .ColWidth(5) = 550
        .ColAlignment(2) = 3
        .ColAlignment(3) = 3
        .ColAlignment(4) = 3
        .ColAlignment(5) = 3
        .TextMatrix(0, 0) = "Requisi��es"
        .TextMatrix(0, 1) = "Estado"
        .TextMatrix(0, 2) = "Data Prevista"
        .TextMatrix(0, 3) = "Data Chegada"
        .TextMatrix(0, 4) = "Datas Impress�o"
        .TextMatrix(0, 5) = "Prod"
        .WordWrap = False
    End With
    
    With FgProd
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = True
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 0
        .ColWidth(0) = 3000
        .ColWidth(1) = 3000
        .ColWidth(2) = 1200
        .ColWidth(3) = 1200
        .TextMatrix(0, 0) = "Produtos"
        .TextMatrix(0, 1) = "Especifica��es"
        .TextMatrix(0, 2) = "Data Prevista"
        .TextMatrix(0, 3) = "Data Chegada"
        .WordWrap = False
    End With

End Sub

Sub Inicializacoes()

    Me.caption = " Requisi��es por Utente"
    Me.left = 440
    Me.top = 350
    Me.Width = 10185
    Me.Height = 5760 ' Normal
    'Me.Height = 8500 ' Campos Extras
    
    'Set CampoDeFocus = EcNumReq
    Set CampoDeFocus = EcUtente
    
    NomeTabela = "sl_identif"
    NumCampos = 8
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "seq_utente"
    CamposBD(1) = "dt_inscr"
    CamposBD(2) = "dt_nasc_ute"
    CamposBD(3) = "nome_ute"
    CamposBD(4) = "n_proc_1"
    CamposBD(5) = "n_proc_2"
    CamposBD(6) = "t_utente"
    CamposBD(7) = "utente"

    ' Campos do Ecr�
    Set CamposEc(0) = EcCodUteSeq
    Set CamposEc(1) = EcDataInscr
    Set CamposEc(2) = EcDataNasc
    Set CamposEc(3) = EcNome
    Set CamposEc(4) = EcProcHosp1
    Set CamposEc(5) = EcProcHosp2
    Set CamposEc(6) = EcTipoUtente
    Set CamposEc(7) = EcUtente
    
    'Inicializacao do comando ADO para seleccao de dados do utente
    Set CmdUtente.ActiveConnection = gConexao
    CmdUtente.CommandType = adCmdText
    CmdUtente.CommandText = " SELECT utente,t_utente,n_proc_1,n_proc_2,nome_ute,dt_nasc_ute,dt_inscr " & _
                            " FROM sl_identif " & _
                            " WHERE seq_utente = ?"
    CmdUtente.Prepared = True
    Set PmtSeqUte = CmdUtente.CreateParameter("SEQ_UTENTE", adInteger, adParamInput, 20)
    CmdUtente.Parameters.Append PmtSeqUte
    
    'Inicializacao do comando ADO para seleccao das requisicoes do utente
    Set CmdReqUte.ActiveConnection = gConexao
    CmdReqUte.CommandType = adCmdText
    CmdReqUte.CommandText = " SELECT n_req,dt_previ,dt_cri, dt_imp,dt_imp2,dt_chega,estado_req,req_aux " & _
                            " FROM sl_requis " & _
                            " WHERE seq_utente = ? " & _
                            " ORDER BY n_req DESC"
    CmdReqUte.Prepared = True
    CmdReqUte.Parameters.Append PmtSeqUte
     
    'Inicializacao do comando ADO para seleccao dos produtos da requisicao
    Set CmdProdReq.ActiveConnection = gConexao
    CmdProdReq.CommandType = adCmdText
    CmdProdReq.CommandText = " SELECT cod_prod,cod_especif,dt_previ,dt_chega " & _
                             " FROM sl_req_prod " & _
                             " WHERE n_req = ?"
    CmdProdReq.Prepared = True
    Set PmtReq = CmdProdReq.CreateParameter("NUM_REQ", adDouble, adParamInput, 7)
    CmdProdReq.Parameters.Append PmtReq
          
    'Inicializacao do comando ADO para seleccao da descricao dos produtos
    Set CmdDescrProd.ActiveConnection = gConexao
    CmdDescrProd.CommandType = adCmdText
    CmdDescrProd.CommandText = " SELECT descr_produto " & _
                               " FROM sl_produto " & _
                               " WHERE cod_produto = ?"
    CmdDescrProd.Prepared = True
    Set PmtCodProd = CmdDescrProd.CreateParameter("COD_PROD", adVarChar, adParamInput, 5)
    CmdDescrProd.Parameters.Append PmtCodProd
     
    'Inicializacao do comando ADO para seleccao da descricao das especificacoes dos produtos
    Set CmdDescrEsp.ActiveConnection = gConexao
    CmdDescrEsp.CommandType = adCmdText
    CmdDescrEsp.CommandText = " SELECT descr_especif " & _
                              " FROM sl_especif " & _
                              " WHERE cod_especif = ?"
    CmdDescrEsp.Prepared = True
    Set PmtCodEsp = CmdDescrEsp.CreateParameter("COD_ESP", adVarChar, adParamInput, 5)
    CmdDescrEsp.Parameters.Append PmtCodEsp
    
    ReqIndex = -1
    ReDim ReqUte(0)
    ProdIndex = -1
    ReDim ProdReq(0)
     
End Sub

Sub FuncaoProcurar()

    Dim SelTotal As Boolean
    Dim sql As String
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If EcNumReq.Text <> "" Then
        If SelTotal = False Then
            CriterioTabela = CriterioTabela & " AND seq_utente IN (SELECT seq_utente FROM sl_requis WHERE n_req=" & CLng(EcNumReq.Text) & ")"
        Else
            CriterioTabela = CriterioTabela & " WHERE seq_utente IN (SELECT seq_utente FROM sl_requis WHERE n_req=" & CLng(EcNumReq.Text) & ") ORDER BY seq_utente DESC"
        End If
    ElseIf EcReqAux.Text <> "" Then
        If SelTotal = False Then
            CriterioTabela = CriterioTabela & " AND seq_utente IN (SELECT seq_utente FROM sl_requis WHERE req_aux='" & EcReqAux.Text & "')"
        Else
            CriterioTabela = CriterioTabela & " WHERE seq_utente IN (SELECT seq_utente FROM sl_requis WHERE req_aux='" & EcReqAux.Text & "') ORDER BY seq_utente DESC"
        End If
    Else
        If SelTotal = True Then
            
'            If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos os utentes ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
'                BL_FimProcessamento Me
'                Set Rs = Nothing
'                Exit Sub
'            End If
        End If
    End If
    
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                            "nome_ute", _
                            True)
    
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer

    rs.Open CriterioTabela, gConexao

    If rs.RecordCount = 0 Then
        fReq = 0
        fReqAux = 0
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        EcNumReq.Text = ""
        If EcNumReq.Text <> "" Then
            fReq = CLng(EcNumReq.Text)
        Else
            fReq = 0
        End If
        If EcCodGrupo.Text <> "" Then
            fCodGrupo = Trim(EcCodGrupo.Text)
        Else
            fCodGrupo = ""
        End If
        'guardar o nr. da an�lise   'IGM
        If EcReqAux.Text <> "" Then
            fReqAux = EcReqAux.Text
        Else
            fReqAux = 0
        End If
        LimpaCampos
        PreencheCampos
        estado = 2
        BL_Toolbar_BotaoEstado "Anterior", "Activo"
        BL_Toolbar_BotaoEstado "Seguinte", "Activo"
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoLimpar()
    'CbGrupo.ListIndex = mediComboValorNull
    'EcCodGrupo.Text = ""
    LimpaCampos
    CampoDeFocus.SetFocus
    
    VisIndex = 0
    ReDim Vis(VisIndex)

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        
        LimpaCampos
        CampoDeFocus.SetFocus
                
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        Set CmdReqUte = Nothing
        Set PmtSeqUte = Nothing
        Set CmdProdReq = Nothing
        Set CmdDescrEsp = Nothing
        Set CmdDescrProd = Nothing
        Set PmtReq = Nothing
        Set PmtCodEsp = Nothing
        Set PmtCodProd = Nothing
        
    Else
        Me.Hide
        'Unload Me
    End If

End Sub

Sub MudaLinhaReq()

End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        PreencheFlexGridReq
    End If
    
End Sub

Sub PreencheFlexGridReq()
    
    Dim i As Integer
    Dim j As Integer

    SelRequisicoes

    If ReqIndex <> -1 Then
        i = 0
        j = 1
        While i < ReqIndex
            FgReq.row = j
            FgReq.TextMatrix(j, 0) = ReqUte(i).n_req
            FgReq.TextMatrix(j, 1) = ReqUte(i).estado
            FgReq.TextMatrix(j, 2) = ReqUte(i).data
            FgReq.TextMatrix(j, 3) = ReqUte(i).Data_Cheg
            FgReq.TextMatrix(j, 4) = ReqUte(i).Data_Impr
            FgReq.TextMatrix(j, 5) = ReqUte(i).Produtos
            If Vis(i).Visualizada = True Then
                FgReq.Col = 0
                FgReq.CellBackColor = vbGrayText
                FgReq.Col = 1
                FgReq.CellBackColor = vbGrayText
                FgReq.Col = 2
                FgReq.CellBackColor = vbGrayText
                FgReq.Col = 3
                FgReq.CellBackColor = vbGrayText
                FgReq.Col = 4
                FgReq.CellBackColor = vbGrayText
                FgReq.Col = 5
                FgReq.CellBackColor = vbGrayText
            End If
            j = j + 1
            FgReq.AddItem "", j

            i = i + 1
        Wend
        If j <> 1 Then FgReq.RemoveItem j
    End If
        
    'Actualizar req. e estado
    EcNumReq.Text = FgReq.TextMatrix(1, 0)
    EcEstado.Text = FgReq.TextMatrix(1, 1)
    EcReqAux.Text = ReqUte(0).req_aux

End Sub

Sub PreencheFlexGridProd()
    
    Dim i As Integer
    Dim j As Integer

    SelProdutos

    If ProdIndex <> -1 Then
        i = 0
        j = 1
        FgProd.TextMatrix(0, 0) = "Produtos da Req. " & Trim(FgReq.TextMatrix(FgReq.row, 0))
        While i < ProdIndex
            FgProd.row = j
            FgProd.Col = 0
            FgProd.CellForeColor = ProdReq(i).Cor_Estado
            FgProd.TextMatrix(j, 0) = ProdReq(i).descricao
            FgProd.Col = 1
            FgProd.CellForeColor = ProdReq(i).Cor_Estado
            FgProd.TextMatrix(j, 1) = ProdReq(i).Especificacao
            FgProd.Col = 2
            FgProd.CellForeColor = ProdReq(i).Cor_Estado
            FgProd.TextMatrix(j, 2) = ProdReq(i).Data_Prev
            FgProd.Col = 3
            FgProd.CellForeColor = ProdReq(i).Cor_Estado
            FgProd.TextMatrix(j, 3) = ProdReq(i).Data_Cheg
            
            j = j + 1
            FgProd.AddItem "", j
            i = i + 1
        Wend
        If j <> 1 Then FgProd.RemoveItem j
    End If

End Sub

Sub LimpaCampos()
    
    Dim i As Integer
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc

    FgReq.Clear
    i = FgReq.rows - 2
    While i > 0
        FgReq.RemoveItem i
        i = i - 1
    Wend
    FgReq.TextMatrix(0, 0) = "Requisi��es"
    FgReq.TextMatrix(0, 1) = "Estado"
    FgReq.TextMatrix(0, 2) = "Data Prevista"
    FgReq.TextMatrix(0, 3) = "Data Chegada"
    FgReq.TextMatrix(0, 4) = "Datas Impress�o"
    FgReq.TextMatrix(0, 5) = "Prod"
    
    ReqIndex = -1
    ReDim ReqUte(0)
    
    
    FgProd.Clear
    i = FgProd.rows - 2
    While i > 0
        FgProd.RemoveItem i
        i = i - 1
    Wend
    FgProd.TextMatrix(0, 0) = "Produtos"
    FgProd.TextMatrix(0, 1) = "Especifica��es"
    FgProd.TextMatrix(0, 2) = "Data Prevista"
    FgProd.TextMatrix(0, 3) = "Data Chegada"
    
    ProdIndex = -1
    ReDim ProdReq(0)
            
    CbTipoUtente.ListIndex = mediComboValorNull
    EcNumReq.Text = ""
    EcEstado.Text = ""
    EcEpisodio.Text = ""
    EcReqAux.Text = ""
    LbeResults = ""

End Sub

Private Sub BtImpRes_Click()
    FuncaoImprimir
End Sub

Private Sub BtMedicosReq_Click()
    If gLAB = "BIO" Then
        FormReqUte.Enabled = False
        FormMedicosReq.Show
    End If
End Sub

Private Sub BtPesquisaUte_Click()
        
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind

    FormPesquisaUtentes.Show
    gRequisicaoActiva = 0

End Sub

Private Sub BtProdReq_Click()
    
    FgProd.Visible = True
    FgReq.Visible = False
    FrHelpProd.Visible = True
    BtSairProd.Visible = True
    BtProdReq.Visible = False
    FrBtProd.Visible = False
    PreencheFlexGridProd
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
    If estado = 2 Then
        BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
        BL_Toolbar_BotaoEstado "Anterior", "InActivo"
    End If
    
End Sub

Private Sub BtSairProd_Click()
    
    Dim i As Integer
    
    FgProd.Visible = False
    FgReq.Visible = True
    FrHelpProd.Visible = False
    BtSairProd.Visible = False
    FrBtProd.Visible = True
    BtProdReq.Visible = True
    ProdIndex = -1
    ReDim ProdReq(0)
    
    FgProd.Clear
    i = FgProd.rows - 2
    While i > 0
        FgProd.RemoveItem i
        i = i - 1
    Wend
    FgProd.TextMatrix(0, 0) = "Produtos"
    FgProd.TextMatrix(0, 1) = "Especifica��es"
    FgProd.TextMatrix(0, 2) = "Data Prevista"
    FgProd.TextMatrix(0, 3) = "Data Chegada"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "EstadoAnterior", "Activo"
    If estado = 2 Then
        BL_Toolbar_BotaoEstado "Seguinte", "Activo"
        BL_Toolbar_BotaoEstado "Anterior", "Activo"
    End If

End Sub

Private Sub CbGrupo_Click()
    
    BL_ColocaComboTexto "sl_gr_ana", "seq_gr_ana", "cod_gr_ana", EcCodGrupo, CbGrupo

End Sub
Private Sub CbGrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbGrupo.ListIndex = -1
    
End Sub
Private Sub CbGrupo_Validate(Cancel As Boolean)
    
    If CbGrupo.ListIndex = mediComboValorNull Then
        EcCodGrupo.Text = ""
        fCodGrupo = ""
    End If

End Sub

Private Sub CbTipoUtente_Click()
    
    EcTipoUtente.Text = CbTipoUtente.Text

End Sub

Private Sub CbTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbTipoUtente.ListIndex = -1

End Sub

Private Sub cmdOK_Click()
    
    FuncaoProcurar

End Sub

Private Sub EcCodUteSeq_Change()
    
    Dim rsUte As ADODB.recordset
    
    If Trim(EcCodUteSeq.Text) <> "" Then
    
        CmdUtente.Parameters(0).value = Trim(EcCodUteSeq.Text)
        Set rsUte = CmdUtente.Execute

        If Not rsUte.EOF Then
            EcUtente.Text = Trim(BL_HandleNull(rsUte!Utente, ""))
            If Trim(BL_HandleNull(rsUte!t_utente, "")) <> "" Then
                CbTipoUtente.Text = Trim(BL_HandleNull(rsUte!t_utente, ""))
            End If
            EcTipoUtente.Text = Trim(BL_HandleNull(rsUte!t_utente, ""))
            EcProcHosp1.Text = Trim(BL_HandleNull(rsUte!n_proc_1, ""))
            EcProcHosp2.Text = Trim(BL_HandleNull(rsUte!n_proc_2, ""))
            EcNome.Text = Trim(BL_HandleNull(rsUte!nome_ute, ""))
            EcDataNasc.Text = Trim(BL_HandleNull(rsUte!dt_nasc_ute, ""))
            EcDataInscr.Text = Trim(BL_HandleNull(rsUte!dt_inscr, ""))
        End If
        
        rsUte.Close
        Set rsUte = Nothing
    
    End If

End Sub

Private Sub EcDataInscr_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInscr)
    
End Sub

Private Sub EcDataNasc_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataNasc)
    
End Sub

Private Sub EcNumReq_GotFocus()
    
    cmdOK.Default = True

End Sub

Private Sub EcNumReq_LostFocus()
    
    cmdOK.Default = False

End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    If Len(EcNumReq) > 7 Then
        EcNumReq = Right(EcNumReq, 7)
    End If
End Sub

Private Sub EcReqAux_GotFocus()
    
    cmdOK.Default = True

End Sub

Private Sub EcReqAux_LostFocus()
    
    cmdOK.Default = False

End Sub


Private Sub EcUtente_Validate(Cancel As Boolean)
    EcUtente = UCase(EcUtente)
End Sub

Private Sub FgReq_DblClick()
        
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
        
    If EcNumReq.Text <> "" Then
        gRequisicaoActiva = EcNumReq
        BL_LimpaDadosUtente
        gDUtente.seq_utente = EcCodUteSeq
        Vis(FgReq.row - 1).Visualizada = True
        FormReqUte.Enabled = False
        FormConsRequis.Show
    End If

End Sub

Private Sub FGReq_EnterCell()
    
    On Error Resume Next
    
    If FgReq.row = 0 Then Exit Sub
    
    FgReq.CellBackColor = &H800000
    FgReq.CellForeColor = &HFFFFFF
    
    EcNumReq.Text = FgReq.TextMatrix(FgReq.row, 0)
    EcEstado.Text = FgReq.TextMatrix(FgReq.row, 1)
    EcReqAux.Text = ReqUte(FgReq.row - 1).req_aux
    LbeResults = ""
    PreencheEnvioEResults
End Sub

Private Sub FGReq_LeaveCell()
    
    On Error Resume Next
    
    If FgReq.row = 0 Then Exit Sub
    
    If Vis(FgReq.row - 1).Visualizada = False Then
        FgReq.CellBackColor = vbWhite
    Else
        FgReq.CellBackColor = vbGrayText
    End If
    FgReq.CellForeColor = vbBlack

End Sub

Private Sub FGReq_LostFocus()
    
    On Error Resume Next
    
    If FgReq.row = 0 Then Exit Sub
    
    If Vis(FgReq.row - 1).Visualizada = False Then
        FgReq.CellBackColor = vbWhite
    Else
        FgReq.CellBackColor = vbGrayText
    End If
    FgReq.CellForeColor = vbBlack

End Sub

Private Sub Form_Activate()
    
    EventoActivate
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind

        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub SelRequisicoes()
    
    Dim RsReqUte As ADODB.recordset
    Dim sql As String
    Dim s As String
    
    'Se a requisi��o est� preenchida restringir a pesquisa por requisi��o
    If fReq <> 0 Then
        EcNumReq.Text = fReq
        sql = "SELECT n_req, dt_cri, dt_imp, dt_imp2, dt_chega, estado_req, req_aux, dt_previ " & _
                " FROM sl_requis WHERE seq_utente =" & BL_String2Double(EcCodUteSeq.Text) & " AND n_req=" & CLng(EcNumReq.Text) & ""
        If fCodGrupo <> "" Then
            BG_MostraComboSel fCodGrupo, CbGrupo
            sql = sql & " AND (gr_ana LIKE '" & fCodGrupo & "' OR gr_ana LIKE '" & fCodGrupo & ";%" & "' OR gr_ana LIKE '" & "%;" & fCodGrupo & ";%" & "' OR gr_ana LIKE '" & "%;" & fCodGrupo & "')"
        End If
        Set RsReqUte = New ADODB.recordset
        RsReqUte.CursorLocation = adUseServer
        RsReqUte.CursorType = adOpenStatic
        RsReqUte.Open sql, gConexao
    'Se o nr. de an�lise est� preenchido restringir a pesquisa pelo nr. de an�lise
    ElseIf fReqAux <> "0" Then
        EcReqAux.Text = fReqAux
        sql = "SELECT n_req, dt_cri, dt_imp, dt_imp2, dt_chega, estado_req, req_aux, dt_previ " & _
                " FROM sl_requis WHERE seq_utente =" & BL_String2Double(EcCodUteSeq.Text) & " AND req_aux='" & EcReqAux.Text & "'"
        If fCodGrupo <> "" Then
            BG_MostraComboSel fCodGrupo, CbGrupo
            sql = sql & " AND (gr_ana LIKE '" & fCodGrupo & "' OR gr_ana LIKE '" & fCodGrupo & ";%" & "' OR gr_ana LIKE '" & "%;" & fCodGrupo & ";%" & "' OR gr_ana LIKE '" & "%;" & fCodGrupo & "')"
        End If
        Set RsReqUte = New ADODB.recordset
        RsReqUte.CursorLocation = adUseServer
        RsReqUte.CursorType = adOpenStatic
        RsReqUte.Open sql, gConexao
    Else
        If fCodGrupo <> "" Then
            BG_MostraComboSel fCodGrupo, CbGrupo
            CmdReqUte.CommandText = " SELECT n_req,dt_cri,dt_imp,dt_imp2,dt_chega,estado_req, req_aux, dt_previ " & _
                            " FROM sl_requis " & _
                            " WHERE seq_utente = ? " & _
                            " AND (gr_ana LIKE '" & fCodGrupo & "' OR gr_ana LIKE '" & fCodGrupo & ";%" & "' OR gr_ana LIKE '" & "%;" & fCodGrupo & ";%" & "' OR gr_ana LIKE '" & "%;" & fCodGrupo & "')" & _
                            " ORDER BY n_req DESC"

        Else
            CmdReqUte.CommandText = " SELECT n_req,dt_cri,dt_imp,dt_imp2,dt_chega,estado_req, req_aux,dt_previ " & _
                        " FROM sl_requis " & _
                        " WHERE seq_utente = ? " & _
                        " ORDER BY n_req DESC"
        End If
        CmdReqUte.Parameters(0).value = Trim(EcCodUteSeq.Text)
        Set RsReqUte = CmdReqUte.Execute
    End If

    If Not RsReqUte.EOF Then
        If ReqIndex = -1 Then ReqIndex = 0
        While Not RsReqUte.EOF
            ReqUte(ReqIndex).n_req = BL_HandleNull(RsReqUte!n_req, " ")
            ReqUte(ReqIndex).estado = BL_DevolveEstadoReq(BL_HandleNull(RsReqUte!estado_req, " "))
            ReqUte(ReqIndex).data = BL_HandleNull(RsReqUte!dt_cri, " ")
            ReqUte(ReqIndex).Data_Cheg = BL_HandleNull(RsReqUte!dt_chega, " ")
            ReqUte(ReqIndex).req_aux = BL_HandleNull(RsReqUte!req_aux, "")
                        
            s = "" & RsReqUte!dt_imp
            If Not BL_HandleNull(RsReqUte!dt_imp2, "") = "" Then
                If s <> "" Then s = s + ", "
                s = s & RsReqUte!dt_imp
            End If
            ReqUte(ReqIndex).Data_Impr = s
            
            '---------------------------------------------------------------------------
            'soliveira
            'seleccionar produtos da requisi��o
            Dim RsProdReq As ADODB.recordset
            Dim Produtos As String
            
            CmdProdReq.Parameters(0).value = CLng(ReqUte(ReqIndex).n_req)
            Set RsProdReq = CmdProdReq.Execute
            Produtos = ""
            If Not RsProdReq.EOF Then
                While Not RsProdReq.EOF
                    Produtos = Produtos & BL_HandleNull(RsProdReq!cod_prod, "") & " "
                    RsProdReq.MoveNext
                Wend
            End If
            ReqUte(ReqIndex).Produtos = Produtos
            '---------------------------------------------------------------------------
            
            ReqIndex = ReqIndex + 1
            RsReqUte.MoveNext
            ReDim Preserve ReqUte(ReqIndex)
            
            If VisIndex < (ReqIndex) Then
                VisIndex = VisIndex + 1
                ReDim Preserve Vis(VisIndex)
            End If
        Wend
    Else
        ReqIndex = -1
    End If

    RsReqUte.Close
    Set RsReqUte = Nothing

End Sub

Sub SelProdutos()
    
    Dim RsProdReq As ADODB.recordset
    
    If FgReq.rows = 0 Or Trim(FgReq.TextMatrix(FgReq.row, 0)) = "" Then Exit Sub
    
    CmdProdReq.Parameters(0).value = CLng(Trim(FgReq.TextMatrix(FgReq.row, 0)))
    Set RsProdReq = CmdProdReq.Execute

    If Not RsProdReq.EOF Then
        If ProdIndex = -1 Then ProdIndex = 0
        While Not RsProdReq.EOF
            ProdReq(ProdIndex).descricao = GetDescrProd(BL_HandleNull(RsProdReq!cod_prod, ""))
            ProdReq(ProdIndex).Especificacao = GetDescrEsp(BL_HandleNull(RsProdReq!cod_Especif, ""))
            ProdReq(ProdIndex).Data_Cheg = BL_HandleNull(RsProdReq!dt_chega, " ")
            ProdReq(ProdIndex).Data_Prev = BL_HandleNull(RsProdReq!dt_previ, " ")
            If BL_HandleNull(RsProdReq!dt_chega, "") = "" Then
                If BL_HandleNull(RsProdReq!dt_previ, "") = "" Then
                    ' Vermelho
                    ProdReq(ProdIndex).Cor_Estado = &HC0&
                ElseIf DateValue(RsProdReq!dt_previ) > DateValue(Bg_DaData_ADO) Then
                    ' Vermelho
                    ProdReq(ProdIndex).Cor_Estado = &H80FF&
                Else
                    ' Laranja
                    ProdReq(ProdIndex).Cor_Estado = &HC0&
                End If
            Else
                ' Verde
                ProdReq(ProdIndex).Cor_Estado = &H8000&
            End If

            ProdIndex = ProdIndex + 1
            RsProdReq.MoveNext
            ReDim Preserve ProdReq(ProdIndex)
        Wend
    Else
        ProdIndex = -1
    End If

    RsProdReq.Close
    Set RsProdReq = Nothing

End Sub

Function GetDescrProd(CodProd As String) As String
    
    Dim RsDescprod As ADODB.recordset
    
    If CodProd = "" Then
        GetDescrProd = " "
        Exit Function
    End If
    
    CmdDescrProd.Parameters(0).value = CodProd
    Set RsDescprod = CmdDescrProd.Execute
    If Not RsDescprod.EOF Then GetDescrProd = BL_HandleNull(RsDescprod!descr_produto, " ")
        
    RsDescprod.Close
    Set RsDescprod = Nothing

End Function

Function GetDescrEsp(CodEsp As String) As String
    
    Dim RsDescEsp As ADODB.recordset
    
    If CodEsp = "" Then
        GetDescrEsp = " "
        Exit Function
    End If
    
    CmdDescrEsp.Parameters(0).value = CodEsp
    Set RsDescEsp = CmdDescrEsp.Execute
    If Not RsDescEsp.EOF Then GetDescrEsp = BL_HandleNull(RsDescEsp!descr_especif, " ")
        
    RsDescEsp.Close
    Set RsDescEsp = Nothing
    
End Function

Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    
    'Preenche Combo de Grupos de An�lises
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
    
    If BL_SeleccionaGrupoAnalises = True Then
        If gCodGrAnaUtilizador <> "" And BL_SeleccionaGrupoAnalises = True Then
            BG_MostraComboSel gCodGrAnaUtilizador, CbGrupo
        End If
    Else
        CbGrupo.ListIndex = mediComboValorNull
    End If
    

End Sub

Private Sub FuncaoImprimir()
    Dim sSql As String
    Dim i As Integer
    Dim Produtos As String
    Dim continua As Boolean
    
    If ReqIndex = 0 Then
        Call BG_Mensagem(mediMsgBox, "N�o existem registos para imprimir.", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    End If
    
    sSql = "DELETE FROM SL_CR_REQ_UTE WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    For i = 0 To ReqIndex - 1
        Produtos = ""
        Produtos = ReqUte(i).Produtos

        sSql = "INSERT INTO SL_CR_REQ_UTE  (nome_computador,n_req, dt_chega, dt_impr, estado,produtos) VALUES( "
        sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
        sSql = sSql & BL_TrataStringParaBD(ReqUte(i).n_req) & ", "
        sSql = sSql & BL_TrataDataParaBD(ReqUte(i).Data_Cheg) & ", "
        sSql = sSql & BL_TrataStringParaBD(ReqUte(i).Data_Impr) & ", "
        sSql = sSql & BL_TrataStringParaBD(ReqUte(i).estado) & ", "
        sSql = sSql & BL_TrataStringParaBD(ReqUte(i).Produtos) & ") "
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
    
    Next
        'Printer Common Dialog
        If gImprimirDestino = 1 Then
            continua = BL_IniciaReport("ListagemRequisicoesUtente", "Listagem de Requisi��es por Utente", crptToPrinter)
        Else
            continua = BL_IniciaReport("ListagemRequisicoesUtente", "Listagem de Requisi��es por Utente", crptToWindow)
        End If
        
        If continua = False Then Exit Sub
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        
    
        Report.SQLQuery = "SELECT " & _
                          "     N_REQ, " & _
                          "     DT_CHEGA, " & _
                          "     DT_IMPR, " & _
                          "     ESTADO, " & _
                          "     PRODUTOS " & _
                          "FROM " & _
                          "     SL_CR_REQ_UTE " & _
                          "WHERE " & _
                          "     NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador) & _
                          "ORDER BY DT_CHEGA ASC "
        
        'F�rmulas do Report
        Report.formulas(0) = "Nome=" & BL_TrataStringParaBD("" & EcNome.Text)
        Report.formulas(1) = "NumSeq=" & BL_TrataStringParaBD("" & EcUtente.Text)
        Report.formulas(2) = "Processo=" & BL_TrataStringParaBD("" & EcProcHosp1.Text)
        Report.formulas(3) = "DtNasc=" & BL_TrataStringParaBD("" & EcDataNasc.Text)
        
        'Me.SetFocus
        
        Call BL_ExecutaReport
        'Report.PageShow Report.ReportLatestPage
        Report.PageZoom (100)
     

    
End Sub

' -----------------------------------------------------------------

' PREENCHE ESTADO DA REQUISI��O NO ERESULTS

' -----------------------------------------------------------------
Private Sub PreencheEnvioEResults()
    Dim estado As Integer
    On Error GoTo TrataErro
    If geResults <> True Then
        Exit Sub
    End If
    estado = BL_RetornaEstadoEnvioEresults(EcNumReq)
    If estado = -1 Then
        LbeResults.caption = "Requisi��o n�o disponibilizada online"
    ElseIf estado = 0 Then
        LbeResults.caption = "Requisi��o � espera de ser disponibilizada online"
    ElseIf estado = 1 Then
        LbeResults.caption = "Requisi��o disponivel online " & BL_RetornaDataEnvioEresults(EcNumReq)
    Else
        LbeResults.caption = "Erro ao disponibilizar online"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEnvioEResults: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEnvioEResults"
    Exit Sub
    Resume Next
End Sub



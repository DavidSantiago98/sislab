VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormTransferencia 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Transfer�ncia de N�meros"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6630
   Icon            =   "FormTransferencia.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3360
   ScaleWidth      =   6630
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameImp 
      Caption         =   "Impress�o"
      Height          =   1095
      Left            =   3600
      TabIndex        =   13
      Top             =   2160
      Width           =   2295
      Begin VB.CommandButton BtListaUtentesRepetidos 
         Height          =   375
         Left            =   1800
         Picture         =   "FormTransferencia.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Lista Utentes Repetidos"
         Top             =   480
         Width           =   375
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   480
         TabIndex        =   15
         Top             =   360
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   199950337
         CurrentDate     =   39980
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   480
         TabIndex        =   16
         Top             =   765
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   199950337
         CurrentDate     =   39980
      End
      Begin VB.Label Label4 
         Caption         =   "a"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label3 
         Caption         =   "De"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   375
      End
   End
   Begin VB.TextBox EcSeqDestino 
      Height          =   285
      Left            =   1680
      TabIndex        =   12
      Top             =   4680
      Width           =   1335
   End
   Begin VB.TextBox EcSeqOrigem 
      Height          =   285
      Left            =   240
      TabIndex        =   11
      Top             =   4680
      Width           =   1335
   End
   Begin VB.TextBox EcDescrDestino 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1920
      Width           =   5775
   End
   Begin VB.TextBox EcDescrOrigem 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   840
      Width           =   5775
   End
   Begin VB.CommandButton BtPesqRapDestUte 
      Height          =   375
      Left            =   6000
      Picture         =   "FormTransferencia.frx":0156
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Procurar c�digo de An�lises Simples"
      Top             =   1560
      Width           =   375
   End
   Begin VB.CommandButton BtPesqRapOrigUte 
      Height          =   375
      Left            =   6000
      Picture         =   "FormTransferencia.frx":06E0
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Procurar c�digo de An�lises Simples"
      Top             =   480
      Width           =   375
   End
   Begin VB.CommandButton BtActualiza 
      Caption         =   "&Confirmar"
      Height          =   735
      Left            =   2160
      Picture         =   "FormTransferencia.frx":0C6A
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Efectuar transfer�ncia"
      Top             =   2400
      Width           =   975
   End
   Begin VB.TextBox EcNumDestino 
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Top             =   1560
      Width           =   4455
   End
   Begin VB.TextBox EcNumOrigem 
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Top             =   480
      Width           =   4455
   End
   Begin VB.ComboBox CbDestino 
      Height          =   315
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1560
      Width           =   1335
   End
   Begin VB.ComboBox CbOrigem 
      Height          =   315
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   480
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "para"
      Height          =   255
      Left            =   0
      TabIndex        =   5
      Top             =   1200
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Transferir o utente"
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "FormTransferencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim FlagLoad As Boolean
Dim TipoInf As String

Dim estado As Integer

Private Sub DefTipoCampos()

    'Tipo VarChar
    EcNumOrigem.Tag = "200"
    EcNumDestino.Tag = "200"
    
    EcNumOrigem.MaxLength = 20
    EcNumDestino.MaxLength = 20
     
End Sub

Public Sub FuncaoLimpar()
    
    LimpaCampos
    
End Sub

Private Sub BtActualiza_Click()
        
    Call EfectuarTransferencia
    
End Sub

Private Sub BtListaUtentesRepetidos_Click()
    Dim continua As Boolean
    Dim sSql As String
    Dim registos As Long
    On Error GoTo TrataErro
    
    'Printer Common Dialog
    continua = BL_IniciaReport("UtentesRepetidos", "Listagem de Utentes Repetidos", crptToWindow)
    If continua = False Then Exit Sub

    BG_BeginTransaction
    sSql = "DELETE FROM sl_cr_utentes_repetidos WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    sSql = "INSERT INTO sl_cr_utentes_repetidos(nome_computador, num_sessao, nome_ute,dt_nasc_ute,t_utente,utente,descr_mor_ute,n_benef_ute) "
    sSql = sSql & " select " & BL_TrataStringParaBD(gComputador) & " nome_computador, " & gNumeroSessao & " num_sessao, nome_ute,dt_nasc_ute,t_utente,utente,descr_mor_ute,n_benef_ute "
    sSql = sSql & " from sl_identif "
    sSql = sSql & " where nome_ute in (select nome_ute from sl_identif "
    sSql = sSql & " group by nome_ute "
    sSql = sSql & " having count(*) >1) "
    sSql = sSql & " and nome_ute not like 'Transferid%' "
    sSql = sSql & " and (dt_inscr between  " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & ")"
    registos = BG_ExecutaQuery_ADO(sSql)
    BG_CommitTransaction
    If registos = 0 Then
        BG_Mensagem mediMsgBox, "N�o existem registos para imprimir" & "!", vbOKOnly
        Exit Sub
    End If
    BG_CommitTransaction
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = " SELECT  nome_ute,dt_nasc_ute,t_utente,utente,descr_mor_ute,n_benef_ute FROM sl_cr_utentes_repetidos WHERE num_sessao = " & gNumeroSessao
    Report.SQLQuery = Report.SQLQuery & " ORDER BY nome_ute, dt_nasc_ute "

    Me.SetFocus
    
    Call BL_ExecutaReport
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro Imprimir Lista Repetidos: " & Err.Description, Me.Name, "BtListaUtentesRepetidos_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqRapDestUte_Click()
        
    TipoInf = "Destino"
    FormPesquisaUtentes.Show

End Sub

Private Sub BtPesqRapOrigUte_Click()
        
    TipoInf = "Origem"
    BG_BeginTransaction
    FormPesquisaUtentes.Show
    BG_CommitTransaction
    
End Sub

Private Sub CbDestino_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbDestino.ListIndex = -1

End Sub

Private Sub CbDestino_Validate(Cancel As Boolean)
        
    Dim nome As String
    Dim seq As String
        
    If CbDestino.Text <> "" And Trim(EcNumDestino.Text) <> "" Then
        If ExisteUtente(CbDestino.Text, EcNumDestino, nome, seq) = False Then
            EcDescrDestino.Text = ""
            MsgBox "Utente n�o existente!"
            EcDescrDestino.Text = nome
            EcSeqDestino.Text = seq
            Cancel = True
'            SendKeys ("{HOME}+{END}")
        Else
            If InStr(1, nome, "Transferido para") <> 0 Then
                BG_Mensagem mediMsgBox, "O n� de destino especificado j� foi " & nome & "!", vbOKOnly
                Cancel = True
            Else
                EcDescrDestino.Text = nome
                EcSeqDestino.Text = seq
            End If
        End If
    Else
        EcDescrDestino.Text = ""
        EcSeqDestino.Text = ""
    End If
    
End Sub

Private Sub CbOrigem_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbOrigem.ListIndex = -1

End Sub

Private Sub CbOrigem_Validate(Cancel As Boolean)
    
    Dim nome As String
    Dim seq As String
    
    If CbOrigem.Text <> "" And Trim(EcNumOrigem.Text) <> "" Then
        If ExisteUtente(CbOrigem.Text, EcNumOrigem, nome, seq) = False Then
            EcDescrOrigem.Text = ""
            MsgBox "Utente n�o existente!", vbOKOnly + vbInformation, App.ProductName
            EcDescrOrigem.Text = nome
            EcSeqOrigem.Text = seq
            Cancel = True
'            SendKeys ("{HOME}+{END}")
        Else
            If InStr(1, nome, "Transferido para") <> 0 Then
                BG_Mensagem mediMsgBox, "O n� de origem especificado j� foi " & nome & "!", vbOKOnly + vbInformation, App.ProductName
                Cancel = True
            Else
                EcDescrOrigem.Text = nome
                EcSeqOrigem.Text = seq
            End If
        End If
    Else
        EcDescrOrigem.Text = ""
        EcSeqOrigem.Text = ""
    End If
    
End Sub

Private Sub EcNumDestino_Validate(Cancel As Boolean)
    
    Dim nome As String
    Dim seq As String
    
    If CbDestino.Text <> "" And Trim(EcNumDestino.Text) <> "" Then
        If ExisteUtente(CbDestino.Text, EcNumDestino, nome, seq) = False Then
            EcDescrDestino.Text = ""
            MsgBox "Utente n�o existente!", vbOKOnly + vbInformation, App.ProductName
            EcDescrDestino.Text = nome
            EcSeqDestino.Text = seq
            Cancel = True
'            SendKeys ("{HOME}+{END}")
        Else
            If InStr(1, nome, "Transferido para") <> 0 Then
                BG_Mensagem mediMsgBox, "O n� de destino especificado j� foi " & nome & "!", vbOKOnly + vbInformation, App.ProductName
                Cancel = True
            Else
                EcDescrDestino.Text = nome
                EcSeqDestino.Text = seq
            End If
        End If
    Else
        EcDescrDestino.Text = ""
        EcSeqDestino.Text = ""
    End If
        
End Sub

Private Sub EcNumOrigem_Validate(Cancel As Boolean)
        
    Dim nome As String
    Dim seq As String
    
    If CbOrigem.Text <> "" And Trim(EcNumOrigem.Text) <> "" Then
        If ExisteUtente(CbOrigem.Text, EcNumOrigem, nome, seq) = False Then
            EcDescrOrigem.Text = ""
            MsgBox "Utente n�o existente!", vbOKOnly + vbInformation, App.ProductName
            EcDescrOrigem.Text = nome
            EcSeqOrigem.Text = seq
            Cancel = True
'            SendKeys ("{HOME}+{END}")
        Else
            If InStr(1, nome, "Transferido para") <> 0 Then
                BG_Mensagem mediMsgBox, "O n� de origem especificado j� foi " & nome & "!", vbOKOnly + vbInformation, App.ProductName
                Cancel = True
            Else
                EcDescrOrigem.Text = nome
                EcSeqOrigem.Text = seq
            End If
        End If
    Else
        EcDescrOrigem.Text = ""
        EcSeqOrigem.Text = ""
    End If
    
End Sub

Private Sub Form_Activate()
    
    EventoActivate
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Private Sub Form_Load()
        
    EventoLoad
        
End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If FlagLoad = True Then
        FlagLoad = False
    Else
        'Descarrega a informa��o para a Origem
        If TipoInf = "Origem" And gDUtente.t_utente <> "" Then
            If InStr(1, gDUtente.nome_ute, "Transferido para") <> 0 Then
                BG_Mensagem mediMsgBox, "O n� de origem especificado j� foi " & gDUtente.nome_ute & "!", vbOKOnly
                CbOrigem.SetFocus
            Else
                CbOrigem.Text = gDUtente.t_utente
                EcNumOrigem.Text = gDUtente.Utente
                EcDescrOrigem.Text = gDUtente.nome_ute
                EcSeqOrigem.Text = gDUtente.seq_utente
            End If
        End If
            
        'Descarrega a informa��o para o Destino
        If TipoInf = "Destino" And gDUtente.t_utente <> "" Then
            If InStr(1, gDUtente.nome_ute, "Transferido para") <> 0 Then
                BG_Mensagem mediMsgBox, "O n� de destino especificado j� foi " & gDUtente.nome_ute & "!", vbOKOnly
                CbDestino.SetFocus
            Else
                CbDestino.Text = gDUtente.t_utente
                EcNumDestino.Text = gDUtente.Utente
                EcDescrDestino.Text = gDUtente.nome_ute
                EcSeqDestino.Text = gDUtente.seq_utente
            End If
        End If
    End If
    
    
    'Limpa os valores para a pr�xima pesquisa
    gDUtente.nome_ute = ""
    gDUtente.Utente = ""
    gDUtente.t_utente = ""
    gDUtente.seq_utente = ""
    
End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    PreencheValoresDefeito
    DefTipoCampos
    Set CampoActivo = Me.ActiveControl
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    BG_BeginTransaction
    
    'Para a Pesquisa R�pida Avan�ada
    'A informa��o � feita pelo Activate pois n�o tenho o controlo sobre a Form do Utente!!
    FlagLoad = True
    TipoInf = ""
    gDUtente.t_utente = ""
    gDUtente.Utente = ""
    gDUtente.nome_ute = ""
    gDUtente.seq_utente = ""
    
End Sub

Private Sub Inicializacoes()
    
    Me.caption = " Transfer�ncia de N�meros"
    Me.left = 540
    Me.top = 450
    Me.Width = 6720
    Me.Height = 3750 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    Set CampoDeFocus = CbOrigem
    
End Sub

Private Sub LimpaCampos()
    
    Me.SetFocus
    
    CbOrigem.ListIndex = mediComboValorNull
    CbDestino.ListIndex = mediComboValorNull
    EcNumOrigem.Text = ""
    EcNumDestino.Text = ""
    
    EcDescrOrigem.Text = ""
    EcDescrDestino.Text = ""
    
    CampoDeFocus.SetFocus
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    BG_StackJanelas_Pop
    
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    BG_RollbackTransaction
    
    Set FormTransferencia = Nothing
        
End Sub

Private Sub PreencheValoresDefeito()
            
    BL_InicioProcessamento Me, "Carregar valores de defeito..."
    Call BG_PreencheComboBD_ADO("sl_t_utente", "cod_t_utente", "descr_t_utente", CbOrigem)
    Call BG_PreencheComboBD_ADO("sl_t_utente", "cod_t_utente", "descr_t_utente", CbDestino)
    BL_FimProcessamento Me, ""
    
End Sub

Private Function ExisteUtente(ByVal Tipo_Ute As String, ByVal Numero_Ute As String, ByRef Descr_Ute As String, ByRef seq_ute As String) As Boolean
        
    Dim RegUtente As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If

    Set RegUtente = New ADODB.recordset
    RegUtente.CursorLocation = adUseClient
    RegUtente.LockType = adLockReadOnly
    RegUtente.CursorType = adOpenForwardOnly
    RegUtente.ActiveConnection = gConexao
    sql = "SELECT Seq_utente,Nome_ute FROM " & tabela_aux & " WHERE utente='" & Trim(Numero_Ute) & "' AND t_utente='" & Trim(Tipo_Ute) & "'"
    RegUtente.Source = sql
    RegUtente.Open
    
    If RegUtente.RecordCount = 0 Then
        ExisteUtente = False
        Descr_Ute = ""
        seq_ute = ""
    Else
        ExisteUtente = True
        Descr_Ute = RegUtente.Fields("Nome_ute").value
        seq_ute = RegUtente.Fields("Seq_utente").value
    End If
        
    RegUtente.Close
    Set RegUtente = Nothing

End Function

Private Sub EfectuarTransferencia()
    Dim sSql As String
    Dim sql As String
    Dim RsRequis As New ADODB.recordset
    
    'Verifica se os campos est�o nulos
    If Trim(CbOrigem.Text) = "" Then
        MsgBox "Indique o Tipo de Utente!", vbOKOnly + vbInformation, App.ProductName
        CbOrigem.SetFocus
        Exit Sub
    End If
    
    If Trim(EcNumOrigem.Text) = "" Then
        MsgBox "Indique o N� de Utente!", vbOKOnly + vbInformation, App.ProductName
        EcNumOrigem.SetFocus
        Exit Sub
    End If
    
    If Trim(CbDestino.Text) = "" Then
        MsgBox "Indique o Tipo de Utente!", vbOKOnly + vbInformation, App.ProductName
        CbDestino.SetFocus
        Exit Sub
    End If
    
    If Trim(EcNumDestino.Text) = "" Then
        MsgBox "Indique o N� de Utente!", vbOKOnly + vbInformation, App.ProductName
        EcNumDestino.SetFocus
        Exit Sub
    End If
    
    If (EcDescrDestino <> EcDescrOrigem) Then
        'MsgBox , vbOKOnly + vbInformation, App.ProductName
        gMsgTitulo = "Inserir"
        gMsgMsg = "Nomes s�o diferentes. Tem a certeza que quer actualizar dados?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
    
        If gMsgResp <> vbYes Then
            MsgBox "Registos N�O foram actualizados!", vbOKOnly + vbInformation, App.ProductName
            Exit Sub
        End If
    End If
    
    '*******************
    
    'Actualiza os Registos das Requisi��es,Diagn�sticos e Identifica��o associados
    BL_InicioProcessamento Me, "A transferir registos..."
    gSQLError = 0
                
    ' REGISTA AS REQUISICOES QUE VAI TRANSFERIR
    sSql = "SELECT * FROM sl_requis WHERE seq_utente = " & Trim(EcSeqOrigem)
    RsRequis.CursorLocation = adUseServer
    RsRequis.LockType = adLockReadOnly
    RsRequis.CursorType = adOpenForwardOnly
    RsRequis.ActiveConnection = gConexao
    RsRequis.Source = sSql
    RsRequis.Open
    If RsRequis.RecordCount > 0 Then
        While Not RsRequis.EOF
            sSql = "INSERT INTO sl_requis_transferidas (n_req, seq_utente_origem, seq_utente_destino,user_cri, dt_cri, hr_cri) VALUES("
            sSql = sSql & RsRequis!n_req & ", "
            sSql = sSql & Trim(EcSeqOrigem) & ", "
            sSql = sSql & Trim(EcSeqDestino) & ", "
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
            
            BG_ExecutaQuery_ADO (sSql)
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
                BG_RollbackTransaction
                BG_BeginTransaction
                BL_FimProcessamento Me, ""
                Exit Sub
            End If
            
            BL_InsereEResults RsRequis!n_req
            RsRequis.MoveNext
        Wend
    End If
    RsRequis.Close
    Set RsRequis = Nothing
    
    
    'Para a Identifica��o
    sql = "UPDATE sl_identif SET Nome_ute='" & "Transferido para o " & CbDestino.Text & "/" & Trim(EcNumDestino.Text) & "' WHERE seq_utente='" & Trim(EcSeqOrigem) & "'"
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If
    


    'Para os Diagn�sticos
    sql = "UPDATE sl_diag_pri SET Seq_utente=" & Trim(EcSeqDestino.Text) & " WHERE seq_utente=" & Trim(EcSeqOrigem) & ""
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If
    
    
    'Para as Requisi��es em Falta
    sql = "UPDATE sl_falt_req SET Seq_utente='" & Trim(EcSeqDestino.Text) & "' WHERE seq_utente=" & Trim(EcSeqOrigem) & ""
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If

    
    'Para os Coment�rios T�cnicos
    sql = "UPDATE sl_cm_tec SET Seq_utente='" & Trim(EcSeqDestino.Text) & "' WHERE seq_utente=" & Trim(EcSeqOrigem) & ""
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If
    
    'Para os Resultados
    sql = "UPDATE sl_realiza SET Seq_utente='" & Trim(EcSeqDestino.Text) & "' WHERE n_req IN (SELECT n_req FROM sl_requis WHERE sl_requis.seq_utente=" & Trim(EcSeqOrigem) & ")"
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If
    
    'SL_RES_ALFAN
    sql = "UPDATE sl_res_alfan sET res_ant1 = NULL,res_ant2 = NULL,res_ant3 = NULL,dt_res_ant1 = NULL,dt_res_ant2 = NULL,dt_res_ant3 = NULL WHERE "
    sql = sql & " seq_realiza IN (SELECT seq_realiza FROM sl_realiza WHERE n_req IN (SELECT n_req FROM sl_requis WHERE sl_requis.seq_utente=" & Trim(EcSeqOrigem) & "))"
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If
    
    'SL_RES_ALFAN_H
    sql = "UPDATE sl_res_alfan_h sET res_ant1 = NULL,res_ant2 = NULL,res_ant3 = NULL,dt_res_ant1 = NULL,dt_res_ant2 = NULL,dt_res_ant3 = NULL WHERE "
    sql = sql & " seq_realiza IN (SELECT seq_realiza FROM sl_realiza_h WHERE n_req IN (SELECT n_req FROM sl_requis WHERE sl_requis.seq_utente=" & Trim(EcSeqOrigem) & "))"
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If
        
    
    'Para os Resultados Hist�ricos
    sql = "UPDATE sl_realiza_h SET Seq_utente='" & Trim(EcSeqDestino.Text) & "' WHERE n_req IN (SELECT n_req FROM sl_requis WHERE sl_requis.seq_utente=" & Trim(EcSeqOrigem) & ")"
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If
    

    If gTipoInstituicao = "PRIVADA" And (UCase(HIS.nome) = UCase("FACTUS") Or gSISTEMA_FACTURACAO = UCase("FACTUS")) Then
        'Para os movimentos de factura��o no FACTUS
        sql = "UPDATE sl_movi_resp SET t_doente = " & BL_TrataStringParaBD(Trim(CbDestino.Text)) & ", doente = " & BL_TrataStringParaBD(Trim(EcNumDestino.Text)) & " WHERE t_doente = " & BL_TrataStringParaBD(Trim(CbOrigem.Text)) & " AND doente = " & BL_TrataStringParaBD(Trim(EcNumOrigem.Text))
        BG_ExecutaQuery_ADO (sql)
        If gSQLError <> 0 Then
            BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
            BG_RollbackTransaction
            BG_BeginTransaction
            BL_FimProcessamento Me, ""
            Exit Sub
        End If
        If gSGBD = gOracle Then
            sql = "UPDATE fa_movi_resp SET t_doente = " & BL_TrataStringParaBD(Trim(CbDestino.Text)) & ", doente = " & BL_TrataStringParaBD(Trim(EcNumDestino.Text)) & " WHERE t_doente = " & BL_TrataStringParaBD(Trim(CbOrigem.Text)) & " AND doente = " & BL_TrataStringParaBD(Trim(EcNumOrigem.Text))
            BG_ExecutaQuery_ADO (sql)
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
                BG_RollbackTransaction
                BG_BeginTransaction
                BL_FimProcessamento Me, ""
                Exit Sub
            End If
        ElseIf gSGBD = gSqlServer Then
            BL_Abre_Conexao_Secundaria gSGBD_SECUNDARIA
            sql = "UPDATE fa_movi_resp SET t_doente = " & BL_TrataStringParaBD(Trim(CbDestino.Text)) & ", doente = " & BL_TrataStringParaBD(Trim(EcNumDestino.Text)) & " WHERE t_doente = " & BL_TrataStringParaBD(Trim(CbOrigem.Text)) & " AND doente = " & BL_TrataStringParaBD(Trim(EcNumOrigem.Text))
            BL_ExecutaQuery_Secundaria (sql)
            gConexaoSecundaria.Close
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
                BG_RollbackTransaction
                BG_BeginTransaction
                BL_FimProcessamento Me, ""
                Exit Sub
            End If
        End If
        
        sql = "UPDATE sl_movi_fact SET t_doente = " & BL_TrataStringParaBD(Trim(CbDestino.Text)) & ", doente = " & BL_TrataStringParaBD(Trim(EcNumDestino.Text)) & " WHERE t_doente = " & BL_TrataStringParaBD(Trim(CbOrigem.Text)) & " AND doente = " & BL_TrataStringParaBD(Trim(EcNumOrigem.Text))
        BG_ExecutaQuery_ADO (sql)
        If gSQLError <> 0 Then
            BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
            BG_RollbackTransaction
            BG_BeginTransaction
            BL_FimProcessamento Me, ""
            Exit Sub
        End If
        
        If gSGBD = gOracle Then
            sql = "UPDATE fa_movi_fact SET t_doente = " & BL_TrataStringParaBD(Trim(CbDestino.Text)) & ", doente = " & BL_TrataStringParaBD(Trim(EcNumDestino.Text)) & " WHERE t_doente = " & BL_TrataStringParaBD(Trim(CbOrigem.Text)) & " AND doente = " & BL_TrataStringParaBD(Trim(EcNumOrigem.Text))
            BG_ExecutaQuery_ADO (sql)
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
                BG_RollbackTransaction
                BG_BeginTransaction
                BL_FimProcessamento Me, ""
                Exit Sub
            End If
        ElseIf gSGBD = gSqlServer Then
            sql = "UPDATE fa_movi_fact SET t_doente = " & BL_TrataStringParaBD(Trim(CbDestino.Text)) & ", doente = " & BL_TrataStringParaBD(Trim(EcNumDestino.Text)) & " WHERE t_doente = " & BL_TrataStringParaBD(Trim(CbOrigem.Text)) & " AND doente = " & BL_TrataStringParaBD(Trim(EcNumOrigem.Text))
            BL_Abre_Conexao_Secundaria gSGBD_SECUNDARIA
            BL_ExecutaQuery_Secundaria (sql)
            gConexaoSecundaria.Close
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
                BG_RollbackTransaction
                BG_BeginTransaction
                BL_FimProcessamento Me, ""
                Exit Sub
            End If
        End If
    End If
    
    'Para as Requisi��es
    sql = "UPDATE sl_requis SET Seq_utente='" & Trim(EcSeqDestino.Text) & "' WHERE seq_utente=" & Trim(EcSeqOrigem) & ""
    BG_ExecutaQuery_ADO (sql)
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a efectuar a transfer�ncia! !", vbOKOnly + vbError, "ERRO"
        BG_RollbackTransaction
        BG_BeginTransaction
        BL_FimProcessamento Me, ""
        Exit Sub
    End If

    'Se n�o deu nenhum erro!
    BG_CommitTransaction
    BG_BeginTransaction
    BL_FimProcessamento Me, ""
    
        
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        Dim requestJson As String
        Dim responseJson As String
        Dim CamposEcra() As Object
        Dim NumCampos As Integer
        NumCampos = 6
        ReDim CamposEcra(0 To NumCampos - 1)
         Set CamposEcra(0) = CbOrigem
         Set CamposEcra(1) = EcNumOrigem
         Set CamposEcra(2) = EcDescrOrigem
         Set CamposEcra(3) = CbDestino
         Set CamposEcra(4) = EcNumDestino
         Set CamposEcra(5) = EcDescrDestino
    
        BG_BeginTransaction
        requestJson = BL_TrataCamposRGPD(CamposEcra)
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Insercao) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        BG_CommitTransaction
    End If

    
    'Limpa os Campos
    BG_Mensagem mediMsgBox, "Transfer�ncia bem sucedida!", vbOKOnly + vbInformation, App.ProductName
    LimpaCampos
    TipoInf = ""
    gDUtente.t_utente = ""
    gDUtente.Utente = ""
    gDUtente.nome_ute = ""
    
End Sub



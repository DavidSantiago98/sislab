Attribute VB_Name = "ResBin"
Option Explicit

' ----------------------------------------------------------------------------

' ADICIONA FICHEIRO ASSOCIADO A UM SEQ_REALIZA

' ----------------------------------------------------------------------------

Public Function RB_AdicionaFicheiroBD(ByRef seq_anexo As Long) As Boolean
    On Error GoTo TrataErro
    Dim rsBin As New ADODB.recordset
    Dim ByteData() As Byte 'Byte array for Blob data.
    Dim sourceFile As Integer
    Dim FileLength As Long
    Dim Numblocks As Integer
    Dim LeftOver As Long
    Dim i As Integer
    Dim strFileName As String
    Dim sSql As String
    Const BlockSize = 10000 'This size can be experimented with for
    RB_AdicionaFicheiroBD = False
    
    If seq_anexo = mediComboValorNull Then
        RB_AdicionaFicheiroBD = True
        Exit Function
    End If
    
    sSql = "SELECT * FROM sl_anexos WHERE seq_anexo = " & seq_anexo
    rsBin.CursorLocation = adUseServer
    rsBin.CursorType = adOpenKeyset
    rsBin.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsBin.Open sSql, gConexao
    If rsBin.RecordCount < 1 Then
        Exit Function
    End If
    
    strFileName = BL_HandleNull(rsBin!Origem, "") & "\" & BL_HandleNull(rsBin!nome_ficheiro, "")
    sourceFile = FreeFile
    Open strFileName For Binary Access Read As sourceFile
    FileLength = LOF(sourceFile) ' Get the length of the file.
    
    
    If FileLength = 0 Then
        Close sourceFile
        RB_AdicionaFicheiroBD = False
        Exit Function
    Else
        Numblocks = FileLength \ BlockSize
        LeftOver = FileLength Mod BlockSize
        ReDim ByteData(LeftOver - 1)
        Get sourceFile, , ByteData()
        rsBin.Fields("dados").AppendChunk ByteData()
        ReDim ByteData(BlockSize - 1)
        For i = 1 To Numblocks
            Get sourceFile, , ByteData()
            rsBin.Fields("dados").AppendChunk ByteData()
            'rsBin.Fields("seq_res_bin") = 2
        Next i
        RB_AdicionaFicheiroBD = True
        Close sourceFile
    End If
    rsBin.Update
    rsBin.Close
Exit Function
TrataErro:
        RB_AdicionaFicheiroBD = False
        BG_LogFile_Erros "RB_AdicionaFicheiroBD: " & Err.Number & " - " & Err.Description, "ResBin", "RB_AdicionaFicheiroBD"
        Exit Function
        Resume Next
End Function

' ----------------------------------------------------------------------------

' RETIRA DA BD FICHEIRO ASSOCIADO A UM SEQ_REALIZA

' ----------------------------------------------------------------------------
Function RB_RetiraFicheiroBD(seq_anexo As Long) As String
    On Error GoTo TrataErro
    Dim rsBin As New ADODB.recordset
    Dim ByteData() As Byte 'Byte array for file.
    Dim DestFileNum As Integer
    Dim DiskFile As String
    Dim FileLength As Long
    Dim Numblocks As Integer
    Const BlockSize = 10000
    Dim LeftOver As Long
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
    RB_RetiraFicheiroBD = ""
        
    sSql = "SELECT * FROM sl_anexos WHERE seq_anexo = " & seq_anexo
    'NELSONPSILVA 16.01.2018 CHVNG-8972 (adUseServer -> adUseClient)
    rsBin.CursorLocation = adUseClient
    rsBin.CursorType = adOpenKeyset
    rsBin.LockType = adLockBatchOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsBin.Open sSql, gConexao
    If rsBin.RecordCount < 1 Then
        Exit Function
    End If
    FileLength = rsBin.Fields("dados").ActualSize
    
    ' Remove any existing destination file.
    DiskFile = gDirCliente & "\" & BL_HandleNull(rsBin!nome_ficheiro, "")
    If Len(Dir$(DiskFile)) > 0 Then
        Kill DiskFile
    End If
    
    DestFileNum = FreeFile
    Open DiskFile For Binary As DestFileNum
    Numblocks = FileLength \ BlockSize
    LeftOver = FileLength Mod BlockSize
    
    ByteData() = rsBin.Fields("dados").GetChunk(LeftOver)
    Put DestFileNum, , ByteData()
    For i = 1 To Numblocks
        ByteData() = rsBin.Fields("dados").GetChunk(BlockSize)
        Put DestFileNum, , ByteData()
    Next i
    Close DestFileNum
    rsBin.Close
    RB_RetiraFicheiroBD = DiskFile
Exit Function
TrataErro:
    RB_RetiraFicheiroBD = ""
    BG_LogFile_Erros "RB_RetiraFicheiroBD: " & Err.Number & " - " & Err.Description, "ResBin", "RB_RetiraFicheiroBD"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------

' PREENCHE TABELA SL_ANEXOS

' ----------------------------------------------------------------------------
Function RB_NovoRegisto(seq_utente As Long, n_req As Long, seq_realiza As Long, nome_ficheiro As String, _
                        caminho As String, extensao As String) As Long
    Dim sSql As String
    Dim iReg As Integer
    Dim rsAlt As New ADODB.recordset
    Dim resultado As Long
    On Error GoTo TrataErro
    
    RB_NovoRegisto = mediComboValorNull
    sSql = "SELECT seq_anexo.nextval resultado FROM dual"
    rsAlt.CursorLocation = adUseServer
    rsAlt.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAlt.Open sSql, gConexao
    If rsAlt.RecordCount > 0 Then
        resultado = BL_HandleNull(rsAlt!resultado, mediComboValorNull)
    End If
    rsAlt.Close
    Set rsAlt = Nothing
    
    sSql = "INSERT INTO sl_anexos (seq_anexo,seq_utente, n_req, seq_realiza,nome_computador, nome_ficheiro, origem, user_Cri, dt_cri, hr_cri, extensao) VALUES("
    sSql = sSql & resultado & ", "
    If seq_utente <> mediComboValorNull Then
        sSql = sSql & seq_utente & ","
    Else
        sSql = sSql & "null,"
    End If
    If n_req <> mediComboValorNull Then
        sSql = sSql & n_req & ","
    Else
        sSql = sSql & "null,"
    End If
    If seq_realiza <> mediComboValorNull Then
        sSql = sSql & seq_realiza & ","
    Else
        sSql = sSql & "null,"
    End If
    sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
    sSql = sSql & BL_TrataStringParaBD(nome_ficheiro) & ", "
    sSql = sSql & BL_TrataStringParaBD(caminho) & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(extensao) & ") "
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg = 1 Then
        If RB_AdicionaFicheiroBD(resultado) = False Then
            GoTo TrataErro
        End If
        If seq_realiza > mediComboValorNull Then
            sSql = "UPDATE sl_realiza SET flg_anexo  = 1 WHERE seq_realiza = " & seq_realiza
            BG_ExecutaQuery_ADO sSql
            sSql = "UPDATE sl_realiza_h SET flg_anexo  = 1 WHERE seq_realiza = " & seq_realiza
            BG_ExecutaQuery_ADO sSql
        ElseIf seq_realiza = mediComboValorNull And n_req > mediComboValorNull Then
            sSql = "UPDATE sl_requis set flg_anexo = 1 where n_Req = " & n_req
            BG_ExecutaQuery_ADO sSql
        ElseIf seq_realiza = mediComboValorNull And n_req = mediComboValorNull And seq_utente > 0 Then
            sSql = "UPDATE sl_identif set flg_anexo = 1 where seq_utente = " & seq_utente
            BG_ExecutaQuery_ADO sSql
        End If
        RB_NovoRegisto = resultado
    Else
        RB_NovoRegisto = mediComboValorNull
    End If
Exit Function
TrataErro:
        RB_NovoRegisto = mediComboValorNull
        BG_LogFile_Erros "RB_NovoRegisto: " & Err.Number & " - " & Err.Description, "ResBin", "RB_NovoRegisto"
        Exit Function
        Resume Next
End Function

' ----------------------------------------------------------------------------

' Apagar Registo

' ----------------------------------------------------------------------------
Function RB_ApagarRegisto(seq_utente As Long, n_req As Long, seq_realiza As Long, seq_anexo As Long) As Boolean
    Dim sSql As String
    Dim iReg As Integer
    Dim rsR As New ADODB.recordset
    On Error GoTo TrataErro
    
    RB_ApagarRegisto = False
    sSql = "UPDATE sl_anexos SET flg_invisivel = 1 WHERE seq_anexo = " & seq_anexo
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg = 1 Then
        If seq_realiza > mediComboValorNull Then
            sSql = "SELECT count(*) conta FROM sl_anexos WHERE seq_realiza = " & seq_realiza & " AND (flg_invisivel = 0 or flg_invisivel IS NULL)"
            rsR.CursorLocation = adUseServer
            rsR.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsR.Open sSql, gConexao
            If rsR.RecordCount > 0 Then
                If rsR!conta = 0 Then
                    sSql = "UPDATE sl_realiza SET flg_anexo = 0 where seq_realiza = " & seq_realiza
                Else
                    sSql = "UPDATE sl_realiza SET flg_anexo = 1 where seq_realiza = " & seq_realiza
                End If
                BG_ExecutaQuery_ADO sSql
            End If
            rsR.Close
            Set rsR = Nothing
        End If
        RB_ApagarRegisto = True
    Else
        RB_ApagarRegisto = False
    End If
Exit Function
TrataErro:
        RB_ApagarRegisto = False
        BG_LogFile_Erros "RB_ApagarRegisto: " & Err.Number & " - " & Err.Description, "ResBin", "RB_ApagarRegisto"
        Exit Function
        Resume Next
End Function



VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCorreio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionFormCorreio"
   ClientHeight    =   7530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12105
   Icon            =   "FCorreio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   12105
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   495
      Left            =   120
      TabIndex        =   7
      Top             =   4320
      Width           =   11895
      Begin VB.Label LaAviso 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   120
         Width           =   11655
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4335
      Left            =   10800
      TabIndex        =   3
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton BtListas 
         Caption         =   "Configurar Listas"
         Height          =   1335
         Left            =   120
         Picture         =   "FCorreio.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Listas de Utilizadores"
         Top             =   2880
         Width           =   975
      End
      Begin VB.CommandButton BtRefresh 
         Caption         =   "Actualizar Pastas"
         Height          =   1335
         Left            =   120
         Picture         =   "FCorreio.frx":1CD6
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Actualizar dados"
         Top             =   1560
         Width           =   975
      End
      Begin VB.CommandButton BtNovo 
         Caption         =   "Editar Mensagem"
         Height          =   1335
         Left            =   120
         Picture         =   "FCorreio.frx":39A0
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Elaborar nova mensagem"
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   65500
      Left            =   960
      Top             =   5520
   End
   Begin VB.PictureBox Picture1 
      Height          =   495
      Left            =   240
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   2
      Top             =   5520
      Width           =   615
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   840
      Top             =   4920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":566A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":5A04
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":5D9E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":6138
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":64D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":686C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   240
      Top             =   4920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":6C06
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":88E0
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":A5BA
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":C294
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":DF6E
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCorreio.frx":FC48
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView EcLtViwMensagens 
      Height          =   4215
      Left            =   2880
      TabIndex        =   1
      Top             =   120
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   7435
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList2"
      SmallIcons      =   "ImageList2"
      ColHdrIcons     =   "ImageList2"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.TreeView EcTreeTipos 
      Height          =   4215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   7435
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
End
Attribute VB_Name = "FormCorreio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

 ' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Private Declare Function LockWindowUpdate Lib "user32" _
        (ByVal hWndLock As Long) As Long


Private Sub ConfiguraListView()
    
    EcLtViwMensagens.View = lvwReport
    
    EcLtViwMensagens.ColumnHeaders.Add , "ICON", "", 520, , 6
    EcLtViwMensagens.ColumnHeaders.Add , "DATE", "Data", 1500
    EcLtViwMensagens.ColumnHeaders.Add , , "Assunto", 3500
    EcLtViwMensagens.ColumnHeaders.Add , , "Emissor", 1950
    SetListViewColor EcLtViwMensagens, Picture1, &HFFF2D9, &HFFFFFF
End Sub

Private Sub ConfiguraTreeView()
    EcTreeTipos.Nodes.Clear
    EcTreeTipos.Nodes.Add , , "Correio", "Mensagens", 1, 1
    EcTreeTipos.Nodes.Add "Correio", tvwChild, "Recebidas", "Recebidas", 2, 2
    EcTreeTipos.Nodes.Add "Correio", tvwChild, "Enviadas", "Enviadas", 3, 3
    EcTreeTipos.Nodes.Add "Correio", tvwChild, "Arquivo", "Arquivo", 4, 4
    EcTreeTipos.Nodes.Add "Correio", tvwChild, "Lixo", "Eliminadas", 5, 5
    EcTreeTipos.Nodes.item("Correio").Expanded = True
    
End Sub

Public Sub MarcaSelArquivadas()
    MudaEstadoMsg 3
End Sub

Public Sub MarcaSelEliminadas()
    If EcTreeTipos.SelectedItem.Key = "Lixo" Or EcTreeTipos.SelectedItem.Key = "Enviadas" Then
        'Marcar como removidas
        gMsgResp = MsgBox("Deseja eliminar difinitivamente esta(s) mensagem(s)?", vbQuestion + vbYesNo)
        If gMsgResp = vbYes Then
            If EcTreeTipos.SelectedItem.Key = "Enviadas" Then
                MudaEstadoMsg 5, True
            Else
                MudaEstadoMsg 5
            End If
        End If
    Else
        'marcar como eliminadas
        MudaEstadoMsg 4
    End If
End Sub


Public Sub MarcaSelLidas()
    MudaEstadoMsg 2
End Sub


Public Sub MarcaSelNaoLidas()
    MudaEstadoMsg 1
End Sub

Private Sub MudaEstadoMsg(iEstado As Integer, Optional bEnviadas As Boolean = False)
    Dim i As Integer
    Dim sSql As String
    Dim sSeparador() As String
    Dim bAlteracao As Boolean
    
    BL_InicioProcessamento Me
    
    On Error Resume Next
    
    bAlteracao = False
    
    If (VerificaMensagensNaoLidasSeleccionadas(iEstado)) Then
        BG_Mensagem mediMsgBox, "N�o pode mover mensagens n�o lidas!", vbExclamation, " Aten��o"
        Exit Sub
    End If
    
    For i = 1 To EcLtViwMensagens.ListItems.Count
        If EcLtViwMensagens.ListItems.item(i).Checked = True Then
            sSeparador = Split(EcLtViwMensagens.ListItems.item(i).Key, "-")
            
            bAlteracao = True
    
            If bEnviadas = True Then
                sSql = "UPDATE " & gTabelaCorreio
                sSql = sSql & " SET flg_eliminada=1 WHERE codigo=" & sSeparador(1)
                
                BG_ExecutaQuery_ADO sSql
            Else
                ' pferreira 2009.03.03
                ' Actualiza tamb�m o campo msg_lida.
                sSql = "UPDATE " & gTabelaCorreioUtil
                sSql = sSql & " SET cod_estado=" & iEstado
                sSql = sSql & ",msg_lida = 1 WHERE cod_mensagem=" & sSeparador(1)
                sSql = sSql & " AND cod_util=" & gCodUtilizador
                
                
                BG_ExecutaQuery_ADO sSql
            End If
        End If
    Next i
    
    'Actualizar os dados
    If bAlteracao = True Then

        Dim sItem As String
        
        sItem = EcTreeTipos.SelectedItem.Key
        
        PreencheDados
        
        EcTreeTipos_NodeClick EcTreeTipos.Nodes.item(sItem)
        EcTreeTipos.Nodes.item(sItem).Selected = True
    End If
    
    
    BL_FimProcessamento Me
End Sub

Private Function ParametrizaMenu() As Boolean
    Dim bMostraMenu As Boolean
        
    If EcLtViwMensagens.ListItems.Count = 0 Then
        ParametrizaMenu = False
        Exit Function
    End If
    
    Select Case EcTreeTipos.SelectedItem.Key
    Case "Correio"
        bMostraMenu = False
    Case "Recebidas"
        MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = True
        bMostraMenu = True
    Case "Enviadas"
        MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = False
        MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = False
        MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = False
        MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = False
        MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = True
        bMostraMenu = True
    Case "Arquivo"
        MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = False
        MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = True
        bMostraMenu = True
    Case "Lixo"
        MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = False
        MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = True
        MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = False
        bMostraMenu = True
    End Select
    
    ParametrizaMenu = bMostraMenu
    
End Function

Private Sub PreencheArquivo()
    Dim sSql As String
    Dim rs As ADODB.recordset
    
    EcLtViwMensagens.ListItems.Clear
    EcLtViwMensagens.Refresh
    EcLtViwMensagens.Checkboxes = True

    sSql = "SELECT * FROM " & gTabelaCorreio & " x1 ," & gTabelaCorreioUtil & " x2 "
    sSql = sSql & " WHERE codigo = cod_mensagem "
    sSql = sSql & " AND cod_util=" & gCodUtilizador & " "
    sSql = sSql & " AND cod_estado=3"
    sSql = sSql & " ORDER BY dt_cri desc , hr_cri desc "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            With EcLtViwMensagens.ListItems.Add(, "M-" & rs!Codigo, "", , 4)
                .ListSubItems.Add , , rs!dt_cri & Space(1) & rs!hr_cri
                '.ListSubItems.Add , , BG_CryptStringDecode(rs!assunto)
                .ListSubItems.Add , , rs!assunto
                .ListSubItems.Add , , BL_PesquisaCodDes(gTabelaUtilizadores, gCampoCodUtilizador, gCampoNomeUtilizador, rs!user_cri, "DESC")
            End With
            rs.MoveNext
        Loop
        
        
        EcLtViwMensagens.Refresh
    End If

    rs.Close
    Set rs = Nothing
    
    
End Sub
Public Function PreencheDados()
    Dim sSql As String
    Dim rs As ADODB.recordset
    
    On Error GoTo Trata_Erro

    EcLtViwMensagens.ListItems.Clear
       
    'enviadas
    sSql = "SELECT * FROM " & gTabelaCorreio
    sSql = sSql & " WHERE user_cri=" & gCodUtilizador
    sSql = sSql & " AND (flg_eliminada IS NULL OR flg_eliminada<>1)"
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic

    EcTreeTipos.Nodes.item("Enviadas").Text = "Enviadas (" & rs.RecordCount & ")"

    rs.Close
    Set rs = Nothing

    'arquivo
    sSql = "SELECT * FROM " & gTabelaCorreio & "," & gTabelaCorreioUtil
    sSql = sSql & " WHERE codigo = cod_mensagem "
    sSql = sSql & " AND cod_util=" & gCodUtilizador & " "
    sSql = sSql & " AND cod_estado=3"
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    
    EcTreeTipos.Nodes.item("Arquivo").Text = "Arquivo (" & rs.RecordCount & ")"

    rs.Close
    Set rs = Nothing
    
    'lixo
    sSql = "SELECT * FROM " & gTabelaCorreio & "," & gTabelaCorreioUtil
    sSql = sSql & " WHERE codigo = cod_mensagem "
    sSql = sSql & " AND cod_util=" & gCodUtilizador & " "
    sSql = sSql & " AND cod_estado=4"
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    
    EcTreeTipos.Nodes.item("Lixo").Text = "Eliminadas (" & rs.RecordCount & ")"
    If (rs.RecordCount > 0) Then: EcTreeTipos.Nodes("Lixo").Image = 6: EcTreeTipos.Nodes("Lixo").SelectedImage = 6
    If (rs.RecordCount = 0) Then: EcTreeTipos.Nodes("Lixo").Image = 5: EcTreeTipos.Nodes("Lixo").SelectedImage = 5
    rs.Close
    Set rs = Nothing
    
    'Recebidas
    sSql = "SELECT * FROM " & gTabelaCorreioUtil & " WHERE cod_util=" & gCodUtilizador
    sSql = sSql & " and cod_estado in (1,2)"
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    
    EcTreeTipos.Nodes.item("Recebidas").Text = "Recebidas (" & rs.RecordCount & ")"
    
    rs.Close
    Set rs = Nothing
    

    EcTreeTipos.Nodes.item("Correio").Selected = True
    AvisoMsgNaoLidas
    
    Exit Function
Trata_Erro:
    BG_LogFile_Erros "Erro: " & Err.Number & " - " & Err.Description & " *FormCorreio*"
    Resume Next
End Function

Private Sub PreencheEnviadas()
    Dim sSql As String
    Dim rs As ADODB.recordset

    EcLtViwMensagens.ListItems.Clear
    EcLtViwMensagens.Refresh
    EcLtViwMensagens.Checkboxes = True
    
    sSql = "SELECT * FROM " & gTabelaCorreio & " WHERE user_cri=" & gCodUtilizador
    sSql = sSql & " AND (flg_eliminada IS NULL OR flg_eliminada<>1)"
    sSql = sSql & " ORDER BY dt_cri desc , hr_cri desc "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            With EcLtViwMensagens.ListItems.Add(, "M-" & rs!Codigo, "", , 1)
                .ListSubItems.Add , , rs!dt_cri & Space(1) & rs!hr_cri
                '.ListSubItems.Add , , BG_CryptStringDecode(rs!assunto)
                .ListSubItems.Add , , rs!assunto
            End With
            rs.MoveNext
        Loop
    
        EcLtViwMensagens.Refresh
    End If
    rs.Close
    Set rs = Nothing
    
End Sub
Private Sub PreencheLixo()
    Dim sSql As String
    Dim rs As ADODB.recordset
    
    EcLtViwMensagens.ListItems.Clear
    EcLtViwMensagens.Refresh
    EcLtViwMensagens.Checkboxes = True

    sSql = "SELECT * FROM " & gTabelaCorreio & "," & gTabelaCorreioUtil
    sSql = sSql & " WHERE codigo = cod_mensagem "
    sSql = sSql & " AND cod_util=" & gCodUtilizador & " "
    sSql = sSql & " AND cod_estado=4"
    sSql = sSql & " ORDER BY dt_cri desc , hr_cri desc "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            With EcLtViwMensagens.ListItems.Add(, "M-" & rs!Codigo, "", , 3)
                .ListSubItems.Add , , rs!dt_cri & Space(1) & rs!hr_cri
                '.ListSubItems.Add , , BG_CryptStringDecode(rs!assunto)
                .ListSubItems.Add , , rs!assunto
                .ListSubItems.Add , , BL_PesquisaCodDes(gTabelaUtilizadores, gCampoCodUtilizador, gCampoNomeUtilizador, rs!user_cri, "DESC")
            End With
            rs.MoveNext
        Loop
        
        EcLtViwMensagens.Refresh
    End If
    If (rs.RecordCount > 0) Then:  EcTreeTipos.Nodes("Lixo").Image = 6: EcTreeTipos.Nodes("Lixo").SelectedImage = 6
    If (rs.RecordCount = 0) Then: EcTreeTipos.Nodes("Lixo").Image = 5: EcTreeTipos.Nodes("Lixo").SelectedImage = 5
    rs.Close
    Set rs = Nothing
End Sub


Private Sub PreencheRecebidas()
    
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim iCodEstado As Integer
    Dim i As Integer
    
    EcLtViwMensagens.ListItems.Clear
    EcLtViwMensagens.Refresh
    EcLtViwMensagens.Checkboxes = True
    
    sSql = "SELECT * FROM " & gTabelaCorreio & "," & gTabelaCorreioUtil
    sSql = sSql & " WHERE codigo = cod_mensagem"
    sSql = sSql & " AND cod_util=" & gCodUtilizador
    sSql = sSql & " AND cod_estado in (1,2) "
    sSql = sSql & " ORDER BY dt_cri desc , hr_cri desc "
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            iCodEstado = rs!cod_estado
            With EcLtViwMensagens.ListItems.Add(, "M-" & rs!Codigo, "", , IIf(BL_HandleNull(rs!cod_estado, 1) = 1, 2, 5))
                .ListSubItems.Add , , rs!dt_cri & Space(1) & rs!hr_cri
                '.ListSubItems.Add , , BG_CryptStringDecode(rs!assunto)
                .ListSubItems.Add , , rs!assunto
                .ListSubItems.Add , , BL_PesquisaCodDes(gTabelaUtilizadores, gCampoCodUtilizador, gCampoNomeUtilizador, BL_HandleNull(rs!user_cri, Empty), "DESC")
            End With
            rs.MoveNext
        Loop
        
        
        EcLtViwMensagens.Refresh
    End If
    rs.Close
    Set rs = Nothing

End Sub

Private Sub BtListas_Click()
    BG_AbreForm FormCodListasCorreio, "FormCodListasCorreio"
End Sub

Private Sub BtNovo_Click()
    FormCorreioMensagem.Show
    PreencheDados
    
End Sub

Public Sub BtRefresh_Click()
    BL_InicioProcessamento Me
    
    Dim sItem As String
    
    sItem = EcTreeTipos.SelectedItem.Key
    
    PreencheDados
    
    EcTreeTipos_NodeClick EcTreeTipos.Nodes.item(sItem)
    EcTreeTipos.Nodes.item(sItem).Selected = True
    AvisoMsgNaoLidas
    BL_FimProcessamento Me
End Sub

Private Sub EcLtViwMensagens_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    On Error GoTo ErrorHandler
    
    
    
    ' Commence sorting
    With EcLtViwMensagens
    
        ' Display the hourglass cursor whilst sorting
        
        Dim lngCursor As Long
        lngCursor = .MousePointer
        .MousePointer = vbHourglass
        
        ' Prevent the ListView control from updating on screen -
        ' this is to hide the changes being made to the listitems
        ' and also to speed up the sort
        
        LockWindowUpdate .hwnd
        
        ' Check the data type of the column being sorted,
        ' and act accordingly
        
        Dim l As Long
        Dim strFormat As String
        Dim strData() As String
        
        Dim lngIndex As Long
        lngIndex = ColumnHeader.Index - 1
    
        Select Case UCase$(ColumnHeader.Key)
        Case "DATE"
        
            ' Sort by date.
            
            strFormat = "YYYYMMDDHhNnSs"
        
            ' Loop through the values in this column. Re-format
            ' the dates so as they can be sorted alphabetically,
            ' having already stored their visible values in the
            ' tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
            
        Case "NUMBER"
        
            ' Sort Numerically
        
            strFormat = String(30, "0") & "." & String(30, "0")
        
            ' Loop through the values in this column. Re-format the values so as they
            ' can be sorted alphabetically, having already stored their visible
            ' values in the tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        strFormat)
                                Else
                                    .Text = "&" & InvNumber( _
                                        Format(0 - CDbl(.Text), _
                                        strFormat))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        strFormat)
                                Else
                                    .Text = "&" & InvNumber( _
                                        Format(0 - CDbl(.Text), _
                                        strFormat))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
       
        Case "ICON"
        
            ' Sort Numerically
        
            strFormat = String(30, "0") & "." & String(30, "0")
        
            ' Loop through the values in this column. Re-format the values so as they
            ' can be sorted alphabetically, having already stored their visible
            ' values in the tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .ReportIcon & Chr$(0) & .Tag
                            If IsNumeric(.ReportIcon) Then
                                If .ReportIcon >= 0 Then
                                    .ReportIcon = .ReportIcon
                                Else
                                    .ReportIcon = "&" & InvNumber(.ReportIcon)
                                End If
                            Else
                                .ReportIcon = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .SmallIcon & Chr$(0) & .Tag
                            If IsNumeric(.SmallIcon) Then
                                If .SmallIcon >= 0 Then
                                    .SmallIcon = .SmallIcon
                                Else
                                        .SmallIcon = "&" & InvNumber(.SmallIcon)
                                            
                                End If
                            Else
                                .SmallIcon = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
             
        Case Else   ' Assume sort by string
            
            ' Sort alphabetically. This is the only sort provided
            ' by the MS ListView control (at this time), and as
            ' such we don't really need to do much here
        
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
        End Select

        ' Unlock the list window so that the OCX can update it
        LockWindowUpdate 0&
        ' Restore the previous cursor
        .MousePointer = lngCursor
        
            
    End With
    Exit Sub
    ' Report time elapsed, in milliseconds
ErrorHandler:
    MsgBox "ERRO: " & Err.Description
    LockWindowUpdate 0&
    
End Sub

Private Sub EcLtViwMensagens_DblClick()
    If EcLtViwMensagens.ListItems.Count > 0 Then
        Dim sSeparaCodigo() As String
    
        Load FormCorreioMensagem
        sSeparaCodigo = Split(EcLtViwMensagens.SelectedItem.Key, "-")
        
        FormCorreioMensagem.PreencheMensagem sSeparaCodigo(1)
        FormCorreioMensagem.Show
        EcLtViwMensagens.SelectedItem.Checked = True
        MarcaSelLidas
        EcLtViwMensagens.SelectedItem.Checked = False
    End If
End Sub

Private Sub EcLtViwMensagens_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcLtViwMensagens_DblClick
    End If
End Sub


Private Sub EcLtViwMensagens_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        If ParametrizaMenu = True Then
            Me.PopupMenu MDIFormInicio.IDM_MENU_FERRAMENTAS(6)
        End If
    End If
End Sub

Public Sub EcTreeTipos_NodeClick(ByVal Node As MSComctlLib.Node)

    MDIFormInicio.IDM_CORREIO_MARCAR(6).caption = "Seleccionar Todos"
    Select Case Node.Key
    Case "Correio"
        EcLtViwMensagens.ListItems.Clear
    Case "Enviadas"
        PreencheEnviadas
    Case "Recebidas"
        PreencheRecebidas
    Case "Arquivo"
        PreencheArquivo
    Case "Lixo"
        PreencheLixo
    End Select
End Sub


Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Correio"
    Me.left = 100
    Me.top = 100
    Me.Width = 12195
    Me.Height = 5295
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    PreencheValoresDefeito
    AvisoMsgNaoLidas
End Sub

Sub EventoActivate()
    Set gFormActivo = Me
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    Set gFormActivo = MDIFormInicio
    
End Sub


Private Sub PreencheValoresDefeito()
    ConfiguraTreeView

    ConfiguraListView
    
    PreencheDados
End Sub

' pferreira 2007.08.27
' Paint a list view with colored bars.
Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SetListViewColor' in form FormCorreio (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

    
Private Function InvNumber(ByVal Number As String) As String
    Static i As Integer
    For i = 1 To Len(Number)
        Select Case Mid$(Number, i, 1)
        Case "-": Mid$(Number, i, 1) = " "
        Case "0": Mid$(Number, i, 1) = "9"
        Case "1": Mid$(Number, i, 1) = "8"
        Case "2": Mid$(Number, i, 1) = "7"
        Case "3": Mid$(Number, i, 1) = "6"
        Case "4": Mid$(Number, i, 1) = "5"
        Case "5": Mid$(Number, i, 1) = "4"
        Case "6": Mid$(Number, i, 1) = "3"
        Case "7": Mid$(Number, i, 1) = "2"
        Case "8": Mid$(Number, i, 1) = "1"
        Case "9": Mid$(Number, i, 1) = "0"
        End Select
    Next
    InvNumber = Number
End Function

Public Sub SelectAll()

    Dim bSelect As Boolean
    Dim oItem As Variant
    bSelect = True
    If (EcLtViwMensagens.ListItems.Count > 0) Then
        For Each oItem In EcLtViwMensagens.ListItems
           bSelect = bSelect And EcLtViwMensagens.ListItems.item(1).Checked
        Next
    End If
    
    If (EcLtViwMensagens.ListItems.Count > 0) Then
        For Each oItem In EcLtViwMensagens.ListItems
            oItem.Checked = Not bSelect
        Next
    End If
    If (Not bSelect) Then: MDIFormInicio.IDM_CORREIO_MARCAR(6).caption = "Desseleccionar Todos"
    If (bSelect) Then: MDIFormInicio.IDM_CORREIO_MARCAR(6).caption = "Seleccionar Todos"
End Sub

Private Sub timer1_Timer()
    BtRefresh_Click
End Sub

Private Function VerificaMensagensNaoLidasSeleccionadas(iEstado As Integer) As Boolean

    Dim oItem As Variant
    If (iEstado = 1 Or iEstado = 2) Then: Exit Function
    If EcTreeTipos.SelectedItem.Key <> "Recebidas" Then: Exit Function
    If (EcLtViwMensagens.ListItems.Count = 0) Then: Exit Function
    For Each oItem In EcLtViwMensagens.ListItems
        If (oItem.Checked And oItem.SmallIcon = 2) Then
            VerificaMensagensNaoLidasSeleccionadas = True
            Exit Function
        End If
    Next
End Function

Private Sub AvisoMsgNaoLidas()

    Dim sSql As String
    Dim rs As ADODB.recordset
    
    EcLtViwMensagens.ListItems.Clear
    EcLtViwMensagens.Refresh
    EcLtViwMensagens.Checkboxes = True
    
    sSql = "SELECT * FROM " & gTabelaCorreio & "," & gTabelaCorreioUtil
    sSql = sSql & " WHERE codigo = cod_mensagem"
    sSql = sSql & " AND cod_util=" & gCodUtilizador
    sSql = sSql & " AND cod_estado = 1 "
        
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        LaAviso.ForeColor = vbRed
        LaAviso.caption = "<< " & UCase$(BL_DevolveNomeUtilizadorActual) & " TEM " & rs.RecordCount & " MENSAGEN(S) N�O LIDA(S)!" & " >>"
    Else
        LaAviso.ForeColor = vbGreen
        LaAviso.caption = "<< " & UCase$(BL_DevolveNomeUtilizadorActual) & " N�O TEM MENSAGENS POR LER!" & " >>"
    End If
    
    If (rs.state = adStateOpen) Then: rs.Close
    
End Sub





VERSION 5.00
Begin VB.Form FormDeltaCheck 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormDeltaCheck"
   ClientHeight    =   6150
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7095
   Icon            =   "FormDeltaCheck.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6150
   ScaleWidth      =   7095
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPesqRapDC 
      Height          =   285
      Left            =   6120
      TabIndex        =   42
      Top             =   5520
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcPesqRapAux 
      Height          =   285
      Left            =   6120
      TabIndex        =   40
      Top             =   5160
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcTipoLimite 
      Height          =   285
      Left            =   3840
      TabIndex        =   38
      Top             =   5640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcLimiteDias 
      Height          =   285
      Left            =   1800
      TabIndex        =   36
      Top             =   5640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   31
      Top             =   5160
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   30
      Top             =   5160
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   29
      Top             =   4710
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3720
      TabIndex        =   28
      Top             =   4680
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Frame Frame4 
      Height          =   825
      Left            =   120
      TabIndex        =   22
      Top             =   3720
      Width           =   6885
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   50
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label14 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   240
         Width           =   675
      End
      Begin VB.Label Label13 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1440
         TabIndex        =   25
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1440
         TabIndex        =   24
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   23
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1455
      Left            =   120
      TabIndex        =   18
      Top             =   2280
      Width           =   6855
      Begin VB.OptionButton TipoVariacao 
         Caption         =   "Varia��o Percentual"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   48
         Top             =   360
         Width           =   1815
      End
      Begin VB.OptionButton TipoVariacao 
         Caption         =   "Varia��o Absoluta"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   47
         Top             =   360
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.TextBox EcValQuantit 
         Height          =   285
         Left            =   2040
         TabIndex        =   45
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox EcVariacaoPercentual 
         Height          =   285
         Left            =   4920
         TabIndex        =   21
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcVariacaoABS 
         Height          =   285
         Left            =   4920
         TabIndex        =   19
         Top             =   360
         Width           =   1695
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000E&
         X1              =   0
         X2              =   6840
         Y1              =   760
         Y2              =   760
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         X1              =   0
         X2              =   6840
         Y1              =   750
         Y2              =   750
      End
      Begin VB.Label Label9 
         Caption         =   "Valor"
         Height          =   255
         Left            =   4440
         TabIndex        =   49
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label6 
         Caption         =   "quando o valor � qualitativo."
         Height          =   255
         Left            =   3000
         TabIndex        =   46
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label5 
         Caption         =   "Resultados diferentes  de"
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   960
         Width           =   1935
      End
      Begin VB.Label Label11 
         Caption         =   "%"
         Height          =   255
         Left            =   6120
         TabIndex        =   20
         Top             =   360
         Width           =   135
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Filtro "
      Height          =   1455
      Left            =   120
      TabIndex        =   8
      Top             =   840
      Width           =   6855
      Begin VB.CheckBox CkObrigaResAnt 
         Caption         =   "Resultados anteriores obrigat�rios"
         Height          =   195
         Left            =   120
         TabIndex        =   51
         Top             =   1200
         Width           =   2895
      End
      Begin VB.ComboBox EcComboLimite 
         Height          =   315
         ItemData        =   "FormDeltaCheck.frx":000C
         Left            =   5640
         List            =   "FormDeltaCheck.frx":000E
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   720
         Width           =   855
      End
      Begin VB.TextBox EcLimite 
         Height          =   285
         Left            =   5040
         TabIndex        =   16
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox EcNumResultado 
         Height          =   285
         Left            =   960
         TabIndex        =   14
         Top             =   720
         Width           =   375
      End
      Begin VB.CommandButton EcPesqAnaS2 
         Height          =   375
         Left            =   6240
         Picture         =   "FormDeltaCheck.frx":0010
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Pesquisa R�pida �s An�lises Simples"
         Top             =   210
         Width           =   375
      End
      Begin VB.TextBox EcDescrAnaS 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3000
         TabIndex        =   11
         Top             =   240
         Width           =   3135
      End
      Begin VB.TextBox EcCodAnaS 
         Height          =   285
         Left            =   2160
         TabIndex        =   10
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "resultados anteriores, cuja data n�o ultrapasse os "
         Height          =   255
         Left            =   1440
         TabIndex        =   15
         Top             =   720
         Width           =   3495
      End
      Begin VB.Label Label7 
         Caption         =   "verificar os"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Para resultados da an�lise"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6240
      TabIndex        =   3
      Top             =   4680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6855
      Begin VB.CommandButton EcPesqRap 
         Height          =   375
         Left            =   6240
         Picture         =   "FormDeltaCheck.frx":059A
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Pesquisa R�pida �s varia��es existentes."
         Top             =   210
         Width           =   375
      End
      Begin VB.TextBox EcDescricao 
         Height          =   285
         Left            =   2880
         TabIndex        =   6
         Top             =   240
         Width           =   3255
      End
      Begin VB.TextBox EcCodigo 
         Height          =   285
         Left            =   840
         TabIndex        =   2
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Descri��o "
         Height          =   255
         Left            =   2040
         TabIndex        =   5
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo "
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Label Label22 
      Caption         =   "EcPesqRapDC"
      Height          =   255
      Left            =   4800
      TabIndex        =   43
      Top             =   5520
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lb 
      Caption         =   "EcPesqRapAux"
      Height          =   255
      Left            =   4680
      TabIndex        =   41
      Top             =   5160
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label20 
      Caption         =   "EcTipoLimite"
      Height          =   255
      Left            =   2880
      TabIndex        =   39
      Top             =   5640
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label19 
      Caption         =   "EcLimiteDias"
      Height          =   255
      Left            =   720
      TabIndex        =   37
      Top             =   5640
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label18 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   240
      TabIndex        =   35
      Top             =   4680
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label17 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   34
      Top             =   5160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label16 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2520
      TabIndex        =   33
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label15 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2520
      TabIndex        =   32
      Top             =   5160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Left            =   4920
      TabIndex        =   4
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormDeltaCheck"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    
    Call TipoVariacao_Click(0)
    
    BL_FimProcessamento Me
    
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            Select Case CInt(EcComboLimite.ListIndex)
                Case gT_Dias
                    EcTipoLimite.Text = gT_Dias
                Case gT_Meses
                    EcTipoLimite.Text = gT_Meses
                Case gT_Anos
                    EcTipoLimite.Text = gT_Anos
            End Select
            EcLimiteDias.Text = BL_Converte_Para_Dias(EcLimite.Text, EcTipoLimite.Text)
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY " & ChaveBD & " ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub
Sub PreencheCampos()
    
    Dim temp As String
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        If Trim(EcVariacaoABS.Text) <> "" Then
            temp = EcVariacaoABS.Text
            TipoVariacao(0).value = True
            EcVariacaoABS.Text = temp
        Else
            temp = EcVariacaoPercentual.Text
            TipoVariacao(1).value = True
            EcVariacaoPercentual.Text = temp
        End If
        
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        If rs!t_limite <> "" Then
            EcTipoLimite.Text = Trim(rs!t_limite)
            Select Case EcTipoLimite.Text
                Case "0"
                    EcComboLimite.ListIndex = 0
                Case "1"
                    EcComboLimite.ListIndex = 1
                Case "2"
                    EcComboLimite.ListIndex = 2
            End Select
        End If
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If
    ProcuraDescrAnaS
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            Select Case EcComboLimite.ListIndex
                Case 0
                    EcTipoLimite.Text = "0"
                Case 1
                    EcTipoLimite.Text = "1"
                Case 2
                    EcTipoLimite.Text = "2"
            End Select
            EcLimiteDias.Text = ""
            EcLimiteDias.Text = BL_Converte_Para_Dias(EcLimite.Text, EcTipoLimite.Text)
           BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub
Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
       BL_InicioProcessamento Me, "A eliminar registo."
       Select Case EcComboLimite.ListIndex
                Case 0
                    EcTipoLimite.Text = "0"
                Case 1
                    EcTipoLimite.Text = "1"
                Case 2
                    EcTipoLimite.Text = "2"
            End Select
        EcLimiteDias.Text = BL_Converte_Para_Dias(EcLimite.Text, EcTipoLimite.Text)
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    LimpaCampos
    PreencheCampos

End Sub


Sub ProcuraDescrAnaS()
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(EcCodAnaS.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbExclamation, ""
            EcCodAnaS.Text = ""
            EcDescrAnaS.Text = ""
            EcCodAnaS.SetFocus
        Else
            EcDescrAnaS.Text = Tabela!descr_ana_s
        End If
        Tabela.Close
        Set Tabela = Nothing

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    If EcComboLimite.ListIndex = -1 Then
        gMsgMsg = "Campo obrigat�rio: Tipo de limite " & vbCrLf & vbCrLf & vbCrLf & _
                 "Continuar ou Cancelar ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton1 + vbQuestion, gFormActivo.caption)
        EcComboLimite.SetFocus
        ValidaCamposEc = False
        Exit Function
    End If
        
    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function


Sub BD_Insert()
    
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_varia_dc") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormDeltaCheck = Nothing
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_t_data", "cod_t_data", "descr_t_data", EcComboLimite
    ' Retirar Crian�a e Adulto
    EcComboLimite.RemoveItem EcComboLimite.ListCount - 1
    EcComboLimite.RemoveItem EcComboLimite.ListCount - 1
    CkObrigaResAnt.value = vbGrayed
End Sub



Private Sub EcCodAnaS_LostFocus()
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodAnaS.Text = UCase(EcCodAnaS.Text)
    
    If EcCodAnaS.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(EcCodAnaS.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbExclamation, ""
            EcCodAnaS.Text = ""
            EcDescrAnaS.Text = ""
            EcCodAnaS.SetFocus
        Else
            EcDescrAnaS.Text = Tabela!descr_ana_s
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
End Sub



Private Sub EcCodigo_LostFocus()
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub


Private Sub EcComboLimite_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcComboLimite.ListIndex = -1
End Sub


Private Sub EcLimite_LostFocus()
    BG_ValidaTipoCampo_ADO Me, EcLimite
End Sub


Private Sub EcNumResultado_LostFocus()
    BG_ValidaTipoCampo_ADO Me, EcNumResultado
End Sub



Private Sub EcPesqAnaS2_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_ana_s", _
                        "descr_ana_s", "seq_ana_s", _
                        EcPesqRapAux
End Sub

Private Sub EcPesqRap_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_varia_dc", _
                        "descr_varia_dc", "seq_varia_dc", _
                        EcPesqRapDC
End Sub


Private Sub EcPesqRapAux_Change()
    Dim rsCodigo As ADODB.recordset
    
If EcPesqRapAux.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s,descr_ana_s FROM sl_ana_s WHERE seq_ana_s = " & EcPesqRapAux, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da an�lise!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodAnaS.Text = rsCodigo!cod_ana_s
            EcDescrAnaS.Text = rsCodigo!descr_ana_s
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
End If
End Sub


Private Sub EcPesqRapDC_Change()
    Dim rsCodigo As ADODB.recordset
    
If EcPesqRapDC.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_varia_dc,descr_varia_dc FROM sl_varia_dc WHERE seq_varia_dc = " & EcPesqRapDC, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do Delta Check!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodigo.Text = rsCodigo!cod_varia_dc
            EcDescricao.Text = rsCodigo!descr_varia_dc
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
End If
End Sub


Private Sub EcVariacaoABS_LostFocus()
    BG_ValidaTipoCampo_ADO Me, EcVariacaoABS
End Sub

Private Sub EcVariacaoPercentual_LostFocus()
    BG_ValidaTipoCampo_ADO Me, EcVariacaoPercentual
End Sub


Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub


Sub Inicializacoes()

    Me.caption = " Delta Check"
    Me.left = 540
    Me.top = 450
    Me.Width = 7185
    Me.Height = 5000 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_varia_dc"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 16
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_varia_dc"
    CamposBD(1) = "cod_varia_dc"
    CamposBD(2) = "cod_ana_s"
    CamposBD(3) = "descr_varia_dc"
    CamposBD(4) = "nr_resultado"
    CamposBD(5) = "limite_dias"
    CamposBD(6) = "limite"
    CamposBD(7) = "t_limite"
    CamposBD(8) = "variacao_abs"
    CamposBD(9) = "variacao_per"
    CamposBD(10) = "user_cri"
    CamposBD(11) = "dt_cri"
    CamposBD(12) = "user_act"
    CamposBD(13) = "dt_act"
    CamposBD(14) = "variacao_qual"
    CamposBD(15) = "flg_obriga_res_ant"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcCodAnaS
    Set CamposEc(3) = EcDescricao
    Set CamposEc(4) = EcNumResultado
    Set CamposEc(5) = EcLimiteDias
    Set CamposEc(6) = EcLimite
    Set CamposEc(7) = EcTipoLimite
    Set CamposEc(8) = EcVariacaoABS
    Set CamposEc(9) = EcVariacaoPercentual
    Set CamposEc(10) = EcUtilizadorCriacao
    Set CamposEc(11) = EcDataCriacao
    Set CamposEc(12) = EcUtilizadorAlteracao
    Set CamposEc(13) = EcDataAlteracao
    Set CamposEc(14) = EcValQuantit
    Set CamposEc(15) = CkObrigaResAnt
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Delta Check"
    TextoCamposObrigatorios(3) = "Descri��o do Delta Check"
    TextoCamposObrigatorios(4) = "N�mero de Resultados Anteriores"
    TextoCamposObrigatorios(6) = "Limite data"
    
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_varia_dc"
    Set ChaveEc = EcCodSequencial
    
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        TipoVariacao(0).value = True
        CampoDeFocus.SetFocus
    End If

End Sub
Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub
Sub LimpaCampos()

    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcDescrAnaS.Text = ""
    EcComboLimite.Clear
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcPesqRapAux.Text = ""
    EcPesqRapDC.Text = ""
    CkObrigaResAnt.value = vbGrayed
    PreencheValoresDefeito
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub





Private Sub TipoVariacao_Click(Index As Integer)
    
    Select Case Index
        'Varia��o Absoluta
        Case 0
            EcVariacaoPercentual.Visible = False
            EcVariacaoABS.Visible = True
            EcVariacaoPercentual.Text = ""
            Label11.Visible = False
            
        'Varia��o Percentual
        Case 1
            EcVariacaoABS.Visible = False
            EcVariacaoPercentual.Visible = True
            EcVariacaoABS.Text = ""
            Label11.Visible = True
            
    End Select
    
    
End Sub



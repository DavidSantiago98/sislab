VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FormFACTUSTrataRejeitados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionFactTrataRejeitados"
   ClientHeight    =   6240
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11625
   Icon            =   "FormFACTUSTrataRejeitados.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   11625
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcTipoIsencao 
      Height          =   285
      Left            =   2160
      TabIndex        =   35
      Top             =   5880
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.TextBox EcTipoUtente 
      Height          =   285
      Left            =   4920
      TabIndex        =   32
      Top             =   5760
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcDescrAna 
      Height          =   285
      Left            =   3960
      TabIndex        =   31
      Top             =   5760
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcHrCri 
      Height          =   285
      Left            =   8775
      TabIndex        =   27
      Top             =   5760
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcDtCri 
      Height          =   285
      Left            =   7800
      TabIndex        =   26
      Top             =   5760
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcCodAna 
      Height          =   285
      Left            =   5880
      TabIndex        =   12
      Top             =   5760
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcCodUtilizador 
      Height          =   285
      Left            =   6840
      TabIndex        =   11
      Top             =   5760
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton BtTrataInsc 
      Caption         =   "&Tratar Requisi��o"
      Default         =   -1  'True
      Height          =   375
      Left            =   9900
      TabIndex        =   5
      Top             =   5640
      Width           =   1530
   End
   Begin VB.Frame Frame1 
      Caption         =   "Crit�rio de Pesquisa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   11295
      Begin VB.CheckBox CkIsento 
         Caption         =   "&Isento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   9960
         TabIndex        =   30
         Top             =   1320
         Width           =   855
      End
      Begin VB.ComboBox EcProveniencia 
         Height          =   315
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   840
         Width           =   2775
      End
      Begin VB.TextBox EcNrReq 
         Height          =   285
         Left            =   1200
         TabIndex        =   24
         Top             =   840
         Width           =   1095
      End
      Begin VB.ComboBox EcDescrTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcBeneficiario 
         Height          =   285
         Left            =   7920
         TabIndex        =   21
         Top             =   1320
         Width           =   1695
      End
      Begin VB.ComboBox EcEntidade 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1320
         Width           =   5055
      End
      Begin VB.TextBox EcUtente 
         Height          =   315
         Left            =   2280
         TabIndex        =   0
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox EcNome 
         Height          =   285
         Left            =   4200
         TabIndex        =   1
         Top             =   360
         Width           =   4335
      End
      Begin VB.TextBox EcData 
         Height          =   285
         Left            =   9960
         TabIndex        =   2
         Top             =   360
         Width           =   1215
      End
      Begin VB.ComboBox EcMotivoRejeicao 
         Height          =   315
         Left            =   7920
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   840
         Width           =   3255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Proveni�ncia"
         Height          =   195
         Left            =   2400
         TabIndex        =   29
         Top             =   840
         Width           =   930
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Requisi��o"
         Height          =   195
         Left            =   240
         TabIndex        =   25
         Top             =   840
         Width           =   795
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Benefici�rio"
         Height          =   195
         Left            =   6600
         TabIndex        =   20
         Top             =   1320
         Width           =   1080
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Entidade"
         Height          =   195
         Left            =   240
         TabIndex        =   14
         Top             =   1320
         Width           =   630
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Utente"
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   480
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   3600
         TabIndex        =   8
         Top             =   360
         Width           =   420
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Data Chegada"
         Height          =   195
         Left            =   8640
         TabIndex        =   9
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Motivo Rejei��o"
         Height          =   195
         Left            =   6600
         TabIndex        =   10
         Top             =   840
         Width           =   1275
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3375
      Left            =   120
      TabIndex        =   15
      Top             =   2160
      Width           =   11325
      _ExtentX        =   19976
      _ExtentY        =   5953
      _Version        =   393216
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Resultados da Pesquisa (Utentes)"
      TabPicture(0)   =   "FormFACTUSTrataRejeitados.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "LaNrBenef"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "LaNome"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "LaNrUtente"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "LaNrReq"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "LaEntidade"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "EcLista"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      Begin VB.ListBox EcLista 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2160
         Left            =   240
         TabIndex        =   16
         Top             =   540
         Width           =   10815
      End
      Begin VB.Label LaEntidade 
         AutoSize        =   -1  'True
         Caption         =   "EFR"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   8640
         TabIndex        =   34
         Top             =   300
         Width           =   315
      End
      Begin VB.Label LaNrReq 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Req"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   240
         TabIndex        =   33
         Top             =   300
         Width           =   555
      End
      Begin VB.Label LaNrUtente 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Utente"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   1200
         TabIndex        =   19
         Top             =   300
         Width           =   735
      End
      Begin VB.Label LaNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   3360
         TabIndex        =   18
         Top             =   300
         Width           =   420
      End
      Begin VB.Label LaNrBenef 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Benef."
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   7200
         TabIndex        =   17
         Top             =   300
         Width           =   720
      End
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      Caption         =   "Inscrito por:"
      Height          =   195
      Left            =   240
      TabIndex        =   22
      Top             =   5640
      Width           =   825
   End
   Begin VB.Label LaUtilizador 
      Caption         =   "LaUtilizador"
      Height          =   195
      Left            =   1200
      TabIndex        =   13
      Top             =   5640
      Width           =   4065
   End
End
Attribute VB_Name = "FormFACTUSTrataRejeitados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim CriterioBase As String 'usado para os bot�es de ordenacao

Dim rsTabela As ADODB.recordset
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

'Dim CamposBDparaListBox_Aux
'Dim NumEspacos_Aux

Dim gFlag As Boolean

'Indica os estados da ordena��o
    '10 - Nr. Utente Ascendente     11 - Nr. Utente Descendente
    '20 - Nome Ascendente           21 - Nome Descendente
    '30 - Nr. Benef. Ascendente     31 - Nr. Benef. Descendente
    '40 - Isen��o Ascendente        41 - Isen��o Descendente
    '50 - Exame Ascendente          51 - Exame Descendente
    '60 - Entidade Ascendente       61 - Entidade Descendente
Dim OrdenacaoAnterior As Byte
Dim TipoOrdenacao As Byte

Sub BtTrataInsc_Click()
    TrataInscricao
End Sub

Sub TrataInscricao()

    If EcLista.ListCount > 0 Then
        BG_LimpaPassaParams
        gPassaParams.id = "TRATA_REJEITADOS"
        gPassaParams.Param(0) = EcNrReq
        gPassaParams.Param(1) = rsTabela!mot_fact_rej
        gPassaParams.Param(2) = EcMotivoRejeicao

        'BL_ConexaoBD_Secundaria_Fecha
        BL_Fecha_Conexao_Secundaria

        'FormInscricao.Show
        If gTipoInstituicao = "HOSPITALAR" Then
            FormGestaoRequisicao.Show
        ElseIf gTipoInstituicao = "PRIVADA" Then
            FormFactusEnvio.Show
        End If
        FormFACTUSTrataRejeitados.Enabled = False
    End If

End Sub

Sub EcBeneficiario_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcBeneficiario_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub EcNrReq_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcNrReq_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub EcData_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcData_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Private Sub EcEntidade_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao EcEntidade, KeyCode
End Sub


Sub EcLista_Click()
    Dim i As Integer

    If EcLista.ListCount > 0 Then
        rsTabela.Move EcLista.ListIndex, MarcaInicial

'        If EcListaAux.ListIndex <> -1 Then
'            If EcLista.ListIndex <> EcListaAux.ListIndex Then
'                i = EcLista.ListIndex
'                EcListaAux.Selected(i) = True
'            End If
'            EcLista.ToolTipText = EcLista.List(EcLista.ListIndex) & EcListaAux.List(EcListaAux.ListIndex)
'        End If

        LimpaCampos
        PreencheCampos
    End If

    gFlag = False

End Sub

Sub EcLista_DblClick()
    BtTrataInsc.value = True
End Sub

'Sub EcListaAux_Click()
'    Dim i As Integer
'
'    If EcListaAux.ListCount > 0 Then
'        If EcLista.ListIndex <> -1 Then
'            If EcLista.ListIndex <> EcListaAux.ListIndex Then
'                i = EcListaAux.ListIndex
'                EcLista.Selected(i) = True
'            End If
'            EcListaAux.ToolTipText = EcLista.List(EcLista.ListIndex) & EcListaAux.List(EcListaAux.ListIndex)
'        End If
'    End If
'
'End Sub

'Sub EcListaAux_DblClick()
'    BtTrataInsc.Value = True
'End Sub

Private Sub EcMotivoRejeicao_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao EcMotivoRejeicao, KeyCode
End Sub


Sub EcNome_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcNome_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Factura��o: Tratamento de Rejeitados"
    Me.left = 120
    Me.top = 180
    Me.Width = 11650
    Me.Height = 6495

    NomeTabela = "slv_fact_trat_rej"
    Set CampoDeFocus = EcUtente

    NumCampos = 12
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)

    ' Campos da Base de Dados
    CamposBD(0) = "t_utente"
    CamposBD(1) = "utente"
    CamposBD(2) = "nome_ute"
    CamposBD(3) = "n_req"
    CamposBD(4) = "dt_chega"
    CamposBD(5) = "n_benef"
    CamposBD(6) = "cod_efr"
    CamposBD(7) = "cod_proven"
    CamposBD(8) = "mot_fact_rej"
    CamposBD(9) = "user_cri"
    CamposBD(10) = "dt_cri"
    CamposBD(11) = "hr_cri"

    ' Campos do Ecr�
    Set CamposEc(0) = EcTipoUtente
    Set CamposEc(1) = EcUtente
    Set CamposEc(2) = EcNome
    Set CamposEc(3) = EcNrReq
    Set CamposEc(4) = EcData
    Set CamposEc(5) = EcBeneficiario
    Set CamposEc(6) = EcEntidade
    Set CamposEc(7) = EcProveniencia
    Set CamposEc(8) = EcMotivoRejeicao
    Set CamposEc(9) = EcCodUtilizador
    Set CamposEc(10) = EcDtCri
    Set CamposEc(11) = EcHrCri
    

    CamposBDparaListBox = Array("n_req", "t_utente", "utente", "nome_ute", "n_benef", "descr_efr")
    NumEspacos = Array(9, 5, 9, 40, 11, 30)

'    CamposBDparaListBox_Aux = Array("cod_agrup", "descr_ana", "descr_efr")
'    NumEspacos_Aux = Array(9, 40, 40)

    gFlag = True

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito

    estado = 1
    BG_StackJanelas_Push Me
    
    gF_TRATAREJ = 1

End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    'Met_Toolbar_EstadoN Estado
    BL_ToolbarEstadoN estado
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"

    If gPassaParams.id = "ABRIU_REQUIS" Then
        LimpaCampos
        FuncaoProcurar
        BG_LimpaPassaParams
    End If

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    'Met_Toolbar_EstadoN 0
    BL_ToolbarEstadoN 0

    If Not rsTabela Is Nothing Then
        rsTabela.Close
        Set rsTabela = Nothing
    End If

    'BL_ConexaoBD_Secundaria_Fecha
    BL_Fecha_Conexao_Secundaria

    gF_TRATAREJ = 0
    Set FormFACTUSTrataRejeitados = Nothing

End Sub

Sub LimpaCampos()

    BG_LimpaCampo_Todos CamposEc
    EcDescrTipoUtente.ListIndex = -1
    LaUtilizador.caption = ""
    Label16.Visible = False
    CkIsento.value = 0

    gFlag = True

End Sub

Sub FuncaoAnterior()

    rsTabela.MovePrevious

    If rsTabela.BOF Then
        rsTabela.MoveNext
        BG_MensagemAnterior
    Else
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

    gFlag = False

End Sub

Sub Funcao_DataActual()
    BL_PreencheData CampoActivo, Me.ActiveControl
End Sub

Sub FuncaoSeguinte()

    rsTabela.MoveNext

    If rsTabela.EOF Then
        rsTabela.MovePrevious
        BG_MensagemSeguinte
    Else
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

    gFlag = False

End Sub

Sub DefTipoCampos()

    BG_DefTipoCampoEc_ADO NomeTabela, "dt_chega", EcData, mediTipoData

    BG_DefTipoCampoEc_Todos NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    Dim iBD_Aberta As Integer
    Dim sSql As String

    iBD_Aberta = BL_Abre_Conexao_Secundaria(gSGBD)

    If iBD_Aberta = 0 Then
        Exit Sub
    End If

    'sSql = "SELECT cod_mot, descr_mot FROM fa_mot_rej_anul WHERE user_rem IS NULL"

    BG_PreencheComboBDSecundaria_ADO "fa_mot_rej_anul", "cod_mot", "descr_mot", EcMotivoRejeicao, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_efr", "cod_efr", "descr_efr", EcEntidade, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_proven", "cod_proven", "descr_proven", EcProveniencia, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", EcDescrTipoUtente
    
    LaUtilizador.caption = ""
    Label16.Visible = False

    TransfereRejeitados

End Sub

Sub TransfereRejeitados()
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim sMotRej As String
    Dim lCount, i As Long
    Dim lastRequis As Long
    Dim iCodProg As Integer
    Dim req As String
    
    sSql = "SELECT distinct x1.chave_prog, x1.cod_mot_rej_anul, x2.episodio, x2.nr_req_ars FROM Fa_movi_fact_rej x1, fa_hist_dados_rej x2 WHERE x1.cod_prog = " & gCodProg & " AND x1.n_seq_prog = x2.n_seq_prog "
    sSql = sSql & "UNION SELECT distinct x1.chave_prog, x1.cod_mot_rej_anul, x2.episodio, x2.nr_req_ars FROM Fa_movi_fact_rej x1, fa_movi_Fact x2 WHERE x1.cod_prog = " & gCodProg & " AND x1.n_seq_prog = x2.n_seq_prog ORDER BY chave_prog "
    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)

    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria, adOpenStatic

    On Error GoTo TrataErro

    lCount = 0
    i = 0
    If rs.RecordCount <> 0 Then
        rs.MoveFirst
        lastRequis = 0
        Do While Not rs.EOF
            If Not IsNull(rs!cod_mot_rej_anul) Then
                sMotRej = rs!cod_mot_rej_anul
            Else
                sMotRej = ""
            End If

            If lastRequis <> rs!chave_prog Then
                
                ' --------------------------------------------------------------------------------------------
                ' COLOCA NA TABELA SL_REQUIS COMO REQUISICAO TENDO SIDO REJEITADA
                ' --------------------------------------------------------------------------------------------
                sSql = "UPDATE sl_requis SET mot_fact_rej = " & BG_VfValor(sMotRej, "'") & ",user_fact_rej = "
                sSql = sSql & gCodUtilizador & ", dt_fact_rej = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
                sSql = sSql & " hr_fact_rej = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", flg_facturado = "
                sSql = sSql & gEstadoFactRequisRejeitado & " WHERE n_req = " & rs!episodio
                If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
                BG_ExecutaQuery_ADO sSql
                
                ' --------------------------------------------------------------------------------------------
                ' REJEITA TODAS AS ANALISES
                ' --------------------------------------------------------------------------------------------
                'soliveira correccao lacto rejeita todas as an�lises desse chave_prog (P1)
                sSql = "UPDATE sl_recibos_det set flg_facturado = " & gEstadoFactRequisRejeitado
                sSql = sSql & " WHERE n_req = " & rs!episodio
                sSql = sSql & " AND p1 = " & BL_TrataStringParaBD(Replace(rs!nr_req_ars, rs!episodio, ""))
                If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
                BG_ExecutaQuery_ADO sSql

                ' --------------------------------------------------------------------------------------------
                ' REMOVE TODAS AS LINHAS DA TABELA DE REJEITADOS DO FACTUS
                ' --------------------------------------------------------------------------------------------
                sSql = "DELETE FROM Fa_movi_fact_rej WHERE chave_prog = '" & rs!chave_prog & "'"
                If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
                BL_ExecutaQuery_Secundaria sSql

                IF_ApagaReqARS rs!episodio, Replace(rs!nr_req_ars, rs!episodio & "_", "")
                
                lCount = lCount + 1
            End If
            lastRequis = Mid(rs!chave_prog, 1, InStr(1, rs!chave_prog, "_") - 1)
            rs.MoveNext
        Loop
    Else
        If gModoDebug = 1 Then BG_LogFile_Erros ("TransfereRejeitados: N�o existem registos rejeitados na Fa_movi_fact_rej")

        rs.Close
        Set rs = Nothing

        Exit Sub
    End If

    BG_LogFile_Erros ("TransfereRejeitados: Foram transferidas " & lCount & " requisi��es rejeitadas da Fa_movi_fact_rej")

    rs.Close
    Set rs = Nothing

    Exit Sub

TrataErro:
    If Err = 3167 Then
        rs.MoveNext
        i = i + 1
        BG_LogFile_Erros "Passei por aqui " & i & " vezes."
        Resume Next
    Else
        BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactTrataRejeitados (TransfereRejeitados)"
        Exit Sub
    End If
    
End Sub

Sub PreencheCampos()
    Dim sNome As String
    Dim i As Integer

    BG_PreencheCampoEc_Todos_ADO rsTabela, CamposBD, CamposEc
    BL_ColocaTextoCombo "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente
    If EcCodUtilizador <> "" Then

        'sNome = BL_PesquisaCodDes("ris_tbm_util", "cod_util", "nome", EcCodUtilizador, "DES")
        sNome = BL_SelNomeUtil(EcCodUtilizador.Text)

        If sNome <> "" Then
            Label16.Visible = True
            LaUtilizador.caption = sNome & " " & EcDtCri & " " & EcHrCri
        End If

    End If
    If EcTipoIsencao.Text = "" Then
        CkIsento.value = 0
    Else
        CkIsento.value = 1
    End If
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"

End Sub

Sub FuncaoLimpar()

    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
        gFlag = True
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 1
        'Met_Toolbar_EstadoN Estado
        BL_ToolbarEstadoN estado

        LimpaCampos
        EcLista.Clear
        'EcListaAux.Clear
        EcLista.ToolTipText = ""

        CampoDeFocus.SetFocus

        If Not rsTabela Is Nothing Then
            rsTabela.Close
            Set rsTabela = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Sub FuncaoProcurar()
    Dim bSelTotal As Boolean

    BL_InicioProcessamento Me, "A pesquisar registos..."

    If gFlag = True Then
        CriterioBase = BG_ConstroiCriterio_SELECT(NomeTabela, CamposBD, CamposEc, bSelTotal)
        CriterioTabela = CriterioBase & " ORDER BY n_req"
    End If

    If gModoDebug = 1 Then BG_LogFile_Erros (CriterioTabela)

    Set rsTabela = New ADODB.recordset
    rsTabela.CursorLocation = adUseServer
    rsTabela.Open CriterioTabela, gConexao, adOpenStatic

    If rsTabela.RecordCount <> 0 Then
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rsTabela.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rsTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        'BG_PreencheListBoxMultipla_ADO EcListaAux, rsTabela, CamposBDparaListBox_Aux, NumEspacos_Aux, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        'BL_ToolbarEstadoN Estado
        
    Else
        BG_Mensagem mediMsgBox, "N�o existem registos para as condi��es de procura!", vbExclamation, "Procurar"
        FuncaoLimpar
        BL_FimProcessamento Me
        Exit Sub
    End If

    gFlag = False

    BL_FimProcessamento Me

End Sub

Sub FuncaoProcurar_Ord(TipoOrdenacao As Byte)

    If gFlag = True Then
        'Nada
    Else
        Select Case TipoOrdenacao

        Case 0
            'Nada
        Case 10
            CriterioTabela = CriterioBase & " ORDER BY t_utente ASC, utente ASC"
        Case 11
            CriterioTabela = CriterioBase & " ORDER BY t_utente DESC, utente DESC"
        'Nome do utente
        Case 20
            CriterioTabela = CriterioBase & " ORDER BY nome_ute ASC"
        Case 21
            CriterioTabela = CriterioBase & " ORDER BY nome_ute DESC"
        'N�mero de benefici�rio
        Case 30
            CriterioTabela = CriterioBase & " ORDER BY n_benef ASC"
        Case 31
            CriterioTabela = CriterioBase & " ORDER BY n_benef DESC"
        'Descri��o de isen��es
        Case 40
            CriterioTabela = CriterioBase & " ORDER BY descr_isencao ASC"
        Case 41
            CriterioTabela = CriterioBase & " ORDER BY descr_isencao DESC"
        'Descri��o de an�lises
        Case 50
            CriterioTabela = CriterioBase & " ORDER BY descr_ana ASC"
        Case 51
            CriterioTabela = CriterioBase & " ORDER BY descr_ana DESC"
        'Entidades Financeiras
        Case 60
            CriterioTabela = CriterioBase & " ORDER BY descr_efr ASC"
        Case 61
            CriterioTabela = CriterioBase & " ORDER BY descr_efr DESC"
        Case 70
            CriterioTabela = CriterioBase & " ORDER BY n_req ASC"
        Case 71
            CriterioTabela = CriterioBase & " ORDER BY n_req DESC"
        Case Else
            'Nada
        End Select

        FuncaoProcurar

        gFlag = False
    End If
End Sub

Sub LaEntidade_Click()

    If OrdenacaoAnterior = 60 Then
        FuncaoProcurar_Ord 61
        OrdenacaoAnterior = 61
    Else
        FuncaoProcurar_Ord 60
        OrdenacaoAnterior = 60
    End If

End Sub

'Sub LaExame_Click()
'
'    If OrdenacaoAnterior = 50 Then
'        FuncaoProcurar_Ord 51
'        OrdenacaoAnterior = 51
'    Else
'        FuncaoProcurar_Ord 50
'        OrdenacaoAnterior = 50
'    End If
'
'End Sub

Private Sub LaNrReq_Click()
    If OrdenacaoAnterior = 70 Then
        FuncaoProcurar_Ord 71
        OrdenacaoAnterior = 71
    Else
        FuncaoProcurar_Ord 70
        OrdenacaoAnterior = 70
    End If
End Sub

Sub LaIsencao_Click()

    If OrdenacaoAnterior = 40 Then
        FuncaoProcurar_Ord 41
        OrdenacaoAnterior = 41
    Else
        FuncaoProcurar_Ord 40
        OrdenacaoAnterior = 40
    End If

End Sub

Sub LaNome_Click()

    If OrdenacaoAnterior = 20 Then
        FuncaoProcurar_Ord 21
        OrdenacaoAnterior = 21
    Else
        FuncaoProcurar_Ord 20
        OrdenacaoAnterior = 20
    End If

End Sub

Sub LaNrBenef_Click()

    If OrdenacaoAnterior = 30 Then
        FuncaoProcurar_Ord 31
        OrdenacaoAnterior = 31
    Else
        FuncaoProcurar_Ord 30
        OrdenacaoAnterior = 30
    End If

End Sub

Sub LaNrUtente_Click()

    If OrdenacaoAnterior = 10 Then
        FuncaoProcurar_Ord 11
        OrdenacaoAnterior = 11
    Else
        FuncaoProcurar_Ord 10
        OrdenacaoAnterior = 10
    End If

End Sub

Sub FuncaoImprimir()

    Call Lista_Rejeitados

End Sub

Sub ImprimirVerAntes()

    Call Lista_Rejeitados

End Sub

Sub Lista_Rejeitados()

    Dim sql As String
    Dim continua As Boolean

    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Lista Rejeitados FACTUS") = True Then Exit Sub

    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListaRejeitadosFACTUS", "Listagem de Rejeitados do FACTUS", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListaRejeitadosFACTUS", "Listagem de Rejeitados do FACTUS", crptToWindow)
    End If
    If continua = False Then Exit Sub

    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")

    'Report.SelectionFormula = "{slv_fact_trata_rej.mot_fact_rej } is not null "
    
    'F�rmulas do Report
    'Report.Formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)

    'Me.SetFocus

    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage

End Sub
Private Sub EcDescrTipoUtente_Click()
    
    BL_ColocaComboTexto "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente

End Sub


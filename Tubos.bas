Attribute VB_Name = "Tubos"
Option Explicit

' ESTRUTURA USADA PARA TUBOS
Public Type estrutTubo
    'REQ_TUBO
    'FMG
    seq_req_tubo_local As Integer
    seq_req_tubo As Long
    Garrafa As String
    dt_previ As String
    dt_chega As String
    hr_chega As String
    local_chega As String
    user_chega As String
    dt_saida As String
    hr_saida As String
    local_saida As String
    user_saida As String
    dt_eliminacao As String
    hr_eliminacao As String
    user_eliminacao As String
    mot_novo As String
    dt_imp As String
    hr_imp As String
    user_imp As String
    dt_ult_imp As String
    hr_ult_imp As String
    user_ult_imp As String
    'CODIFICACAO TUBO
    CodTubo As String
    Designacao_tubo As String
    CodProd As String
    inf_complementar As String
    num_copias As Integer
    num_max_ana As Integer
    normaTubo As String
    'OUTROS
    Cheio As Boolean
    DescrEtiqAna As String
    GrAna As String
    codTuboBar As String
    QtMax As Double
    QtOcup As Double
    abrAna As String
    num_ana As Integer
    Descr_etiq_ana As String
    Especial As Boolean
    estado_tubo As Integer
    'RGONCALVES 15.12.2014 - n� de c�pias original de etiquetas
    num_copias_original As Integer
    cod_tempo_colheita As Integer
    tempo_colheita As Integer
    hora_colheita As String
    'NELSONPSILVA CHVNG-7461 29.10.2018
    dt_colheita As String
    hr_colheita As String
    etiqueta As String
    '
End Type
Global gEstruturaTubos() As estrutTubo
Global gTotalTubos As Integer

'NELSONPSILVA CHVNG-7461 29.10.2018
    Const gColTuboSeqTuboLocal_reutil = 0
    Const gColTuboDescrTubo_reutil = 1
    Const gColTuboDtColheita_reutil = 2
    Const gColTuboGarrafa_reutil = 3
    Const gColTuboDtPrevi_reutil = 4
    Const gColTuboDtChega_reutil = 5
    Const gColTuboHrChega_reutil = 6
    Const gColTuboUtilChega_reutil = 7
    Const gColTuboEstado_reutil = 8
'

' ------------------------------------------------------------------------------------------------

' Devolve tubo associado a analise

' ------------------------------------------------------------------------------------------------
Public Function TB_DevolveTubo(ByVal cod_ana As String) As String
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    TB_DevolveTubo = ""
    
    If Mid(cod_ana, 1, 1) = "P" Then
        sSql = "SELECT cod_tubo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
    ElseIf Mid(cod_ana, 1, 1) = "C" Then
        sSql = "SELECT cod_tubo FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
    ElseIf Mid(cod_ana, 1, 1) = "S" Then
        sSql = "SELECT cod_tubo FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
    End If
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount = 1 Then
         TB_DevolveTubo = BL_HandleNull(rsTubo!cod_tubo, "")
    End If
    rsTubo.Close
    Set rsTubo = Nothing
Exit Function
TrataErro:
    TB_DevolveTubo = ""
    BG_LogFile_Erros "Tubos: TB_DevolveTubo: " & Err.Description, "TB", "TB_DevolveTubo", True
    Exit Function
    Resume Next
End Function


' ---------------------------------------------------------------------------------------------------

' VERIFICA SE TUBO JA EXISTE, SE NAO EXISTIR ACRESCENTA

' ---------------------------------------------------------------------------------------------------

Public Function TB_AdicionaTuboEstrut(FGTubos As MSFlexGrid, indice As Integer, dt_previ As String) As Boolean
    Dim i As Integer
    Dim flg_encontrou As Boolean
    Dim flg_encontrou2 As Boolean
    Dim cod_tubo As String
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    Dim novoIndice As Integer
    On Error GoTo TrataErro
    
    TB_AdicionaTuboEstrut = False
    MaReq(indice).seq_req_tubo = mediComboValorNull
    MaReq(indice).indice_tubo = mediComboValorNull
    
    ' VERIFICA SE COD_AGRUP OU ALGUM MEMBRO SUPERIOR JA TEM TUBO
    
    For i = 1 To UBound(MaReq)
        If indice <> i Then
            If MaReq(i).cod_agrup = MaReq(indice).cod_agrup Then
                If MaReq(i).Cod_Perfil = MaReq(i).cod_agrup Then
                    If MaReq(i).cod_ana_c = gGHOSTMEMBER_C And MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                        If MaReq(i).indice_tubo > mediComboValorNull Then
                            MaReq(indice).indice_tubo = MaReq(i).indice_tubo
                            MaReq(indice).seq_req_tubo = MaReq(i).seq_req_tubo
                            flg_encontrou = True
                            Exit For
                        End If
                    ElseIf MaReq(i).cod_ana_c = MaReq(indice).cod_ana_c And MaReq(i).cod_ana_c <> "0" And MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                        If MaReq(i).indice_tubo > mediComboValorNull Then
                            MaReq(indice).indice_tubo = MaReq(i).indice_tubo
                            MaReq(indice).seq_req_tubo = MaReq(i).seq_req_tubo
                            flg_encontrou = True
                            Exit For
                        End If
                    End If
                ElseIf MaReq(i).cod_ana_c = MaReq(i).cod_agrup Then
                    If MaReq(i).cod_ana_c = MaReq(indice).cod_ana_c And MaReq(i).cod_ana_c <> "0" And MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                        If MaReq(i).indice_tubo > mediComboValorNull Then
                            MaReq(indice).indice_tubo = MaReq(i).indice_tubo
                            MaReq(indice).seq_req_tubo = MaReq(i).seq_req_tubo
                            flg_encontrou = True
                            Exit For
                        End If
                    End If
                End If
            End If
        End If
    Next i
    If flg_encontrou = False Then
        If MaReq(indice).Cod_Perfil = MaReq(indice).cod_agrup Then
            If MaReq(indice).cod_ana_c = gGHOSTMEMBER_C And MaReq(indice).cod_ana_s = gGHOSTMEMBER_S Then
                cod_tubo = TB_DevolveTubo(MaReq(indice).Cod_Perfil)
            ElseIf MaReq(indice).cod_ana_c <> gGHOSTMEMBER_C And MaReq(indice).cod_ana_s = gGHOSTMEMBER_S Then
                cod_tubo = TB_DevolveTubo(MaReq(indice).cod_ana_c)
            ElseIf MaReq(indice).cod_ana_s <> gGHOSTMEMBER_S Then
                cod_tubo = TB_DevolveTubo(MaReq(indice).cod_ana_s)
            End If
        ElseIf MaReq(indice).cod_ana_c = MaReq(indice).cod_agrup Then
            If MaReq(indice).cod_ana_c <> gGHOSTMEMBER_C And MaReq(indice).cod_ana_s = gGHOSTMEMBER_S Then
                cod_tubo = TB_DevolveTubo(MaReq(indice).cod_ana_c)
            ElseIf MaReq(indice).cod_ana_s <> gGHOSTMEMBER_S Then
                cod_tubo = TB_DevolveTubo(MaReq(indice).cod_ana_s)
            End If
        ElseIf MaReq(indice).cod_ana_s = MaReq(indice).cod_agrup Then
            cod_tubo = TB_DevolveTubo(MaReq(indice).cod_ana_s)
        End If
        
        flg_encontrou2 = False
        For i = 1 To gTotalTubos
        'BEDSIDE -> gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado
            If cod_tubo = gEstruturaTubos(i).CodTubo And gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado Then
                MaReq(indice).indice_tubo = i
                MaReq(indice).seq_req_tubo = gEstruturaTubos(i).seq_req_tubo
                gEstruturaTubos(i).num_ana = gEstruturaTubos(i).num_ana + 1
                flg_encontrou2 = True
                Exit For
            End If
        Next
        
        If flg_encontrou2 = False And cod_tubo <> "" Then
            sSql = "SELECT  t.cod_prod, t.cod_tubo,t.cod_etiq, t.CAPACI_UTIL, t.descr_tubo, t.inf_complementar, t.num_copias, t.num_max_ana,"
            sSql = sSql & " null dt_imp, null dt_ult_imp, null  hr_imp, null user_imp, null hr_ult_imp, null user_ult_imp, cod_tempo_colheita, "
            sSql = sSql & " null hr_previ_colheita FROM  sl_tubo t WHERE t.cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
            rsTubo.CursorType = adOpenStatic
            rsTubo.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsTubo.Open sSql, gConexao
            If rsTubo.RecordCount = 1 Then
            
                novoIndice = TB_PreencheEstrutTubos(rsTubo, mediComboValorNull, BL_HandleNull(rsTubo!cod_prod, ""), BL_HandleNull(rsTubo!cod_tubo, ""), BL_HandleNull(rsTubo!descR_tubo, ""), _
                                  BL_HandleNull(rsTubo!inf_complementar, ""), BL_HandleNull(rsTubo!num_copias, 1), BL_HandleNull(rsTubo!num_max_ana, 99), _
                                  BL_HandleNull(dt_previ, Bg_DaData_ADO), "", "", "", "", "", "", "", "", "", "", "", "", "", BL_HandleNull(rsTubo!cod_etiq, ""), gEstadosTubo.Pendente, _
                                   BL_HandleNull(rsTubo!cod_tempo_colheita, mediComboValorNull), BL_HandleNull(rsTubo!hr_previ_colheita, ""), mediComboValorNull, "", "", "")

                TB_PreencheFgTubo FGTubos, novoIndice
                MaReq(indice).indice_tubo = novoIndice
                MaReq(indice).seq_req_tubo = gEstruturaTubos(novoIndice).seq_req_tubo
            End If
        End If
    End If

    TB_AdicionaTuboEstrut = True
Exit Function
TrataErro:
    TB_AdicionaTuboEstrut = False
    BG_LogFile_Erros "Erro  TB_AdicionaTuboEstrut: " & Err.Description, "TB", "TB_AdicionaTuboEstrut", False
    Exit Function
    Resume Next
End Function

Public Sub TB_PreencheFgTubo(FGTubos As MSFlexGrid, indice As Integer)
    On Error GoTo TrataErro
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Reutilizacao_Colheitas = mediSim Then
        If gAtiva_Nova_Numeracao_Tubo = mediSim Then
            FGTubos.TextMatrix(indice, gColTuboSeqTuboLocal_reutil) = gEstruturaTubos(indice).etiqueta
        Else
            FGTubos.TextMatrix(indice, gColTuboSeqTuboLocal_reutil) = gEstruturaTubos(indice).seq_req_tubo
        End If
        FGTubos.TextMatrix(indice, gColTuboDescrTubo_reutil) = gEstruturaTubos(indice).Designacao_tubo
        FGTubos.TextMatrix(indice, gColTuboDtColheita_reutil) = gEstruturaTubos(indice).dt_colheita
        FGTubos.TextMatrix(indice, gColTuboGarrafa_reutil) = gEstruturaTubos(indice).Garrafa
        FGTubos.TextMatrix(indice, gColTuboDtPrevi_reutil) = gEstruturaTubos(indice).dt_previ
        FGTubos.TextMatrix(indice, gColTuboDtChega_reutil) = gEstruturaTubos(indice).dt_chega
        FGTubos.TextMatrix(indice, gColTuboHrChega_reutil) = gEstruturaTubos(indice).hr_chega
        FGTubos.TextMatrix(indice, gColTuboUtilChega_reutil) = gEstruturaTubos(indice).user_chega
        Select Case gEstruturaTubos(indice).estado_tubo
        Case gEstadosTubo.Pendente
            FGTubos.TextMatrix(indice, gColTuboEstado_reutil) = "Espera"
        Case gEstadosTubo.colhido
            FGTubos.TextMatrix(indice, gColTuboEstado_reutil) = "Colhido"
        Case gEstadosTubo.chegado
            FGTubos.TextMatrix(indice, gColTuboEstado_reutil) = "Chegou"
        Case gEstadosTubo.cancelado
            FGTubos.TextMatrix(indice, gColTuboEstado_reutil) = "Cancelado"
    End Select
    '
    Else
    FGTubos.TextMatrix(indice, gColTuboSeqTubo) = gEstruturaTubos(indice).seq_req_tubo
    FGTubos.TextMatrix(indice, gColTuboSeqTuboLocal) = gEstruturaTubos(indice).seq_req_tubo_local
    FGTubos.TextMatrix(indice, gColTuboCodTubo) = gEstruturaTubos(indice).CodTubo
    FGTubos.TextMatrix(indice, gColTuboDescrTubo) = gEstruturaTubos(indice).Designacao_tubo
    FGTubos.TextMatrix(indice, gColTuboGarrafa) = gEstruturaTubos(indice).Garrafa
    FGTubos.TextMatrix(indice, gColTuboDtPrevi) = gEstruturaTubos(indice).dt_previ
    FGTubos.TextMatrix(indice, gColTuboDtChega) = gEstruturaTubos(indice).dt_chega
    FGTubos.TextMatrix(indice, gColTuboHrChega) = gEstruturaTubos(indice).hr_chega
    FGTubos.TextMatrix(indice, gColTuboUtilChega) = gEstruturaTubos(indice).user_chega
    Select Case gEstruturaTubos(indice).estado_tubo
        Case gEstadosTubo.Pendente
            FGTubos.TextMatrix(indice, gColTuboEstado) = "Espera"
        Case gEstadosTubo.colhido
            FGTubos.TextMatrix(indice, gColTuboEstado) = "Colhido"
        Case gEstadosTubo.chegado
            FGTubos.TextMatrix(indice, gColTuboEstado) = "Chegou"
        Case gEstadosTubo.cancelado
            FGTubos.TextMatrix(indice, gColTuboEstado) = "Cancelado"
    End Select
    End If
    If FGTubos.TextMatrix(FGTubos.rows - 1, 1) <> "" Then
        FGTubos.AddItem ""
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro TB_PreencheFgTuboeencheFgTubo: " & Err.Description, "TB", "TB_PreencheFgTubo", False
    Exit Sub
    Resume Next
End Sub

Public Function TB_MoveEstrutTubos(Origem As Integer, destino As Integer) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    TB_MoveEstrutTubos = False
    gEstruturaTubos(destino).seq_req_tubo = gEstruturaTubos(Origem).seq_req_tubo
    gEstruturaTubos(destino).seq_req_tubo_local = gEstruturaTubos(Origem).seq_req_tubo_local
    gEstruturaTubos(destino).CodProd = gEstruturaTubos(Origem).CodProd
    gEstruturaTubos(destino).CodTubo = gEstruturaTubos(Origem).CodTubo
    gEstruturaTubos(destino).Designacao_tubo = gEstruturaTubos(Origem).Designacao_tubo
    gEstruturaTubos(destino).inf_complementar = gEstruturaTubos(Origem).inf_complementar
    gEstruturaTubos(destino).num_copias = gEstruturaTubos(Origem).num_copias
    gEstruturaTubos(destino).num_max_ana = gEstruturaTubos(Origem).num_max_ana
    gEstruturaTubos(destino).num_ana = gEstruturaTubos(Origem).num_ana
    gEstruturaTubos(destino).Garrafa = gEstruturaTubos(Origem).Garrafa
    gEstruturaTubos(destino).dt_previ = gEstruturaTubos(Origem).dt_previ
    gEstruturaTubos(destino).dt_chega = gEstruturaTubos(Origem).dt_chega
    gEstruturaTubos(destino).hr_chega = gEstruturaTubos(Origem).hr_chega
    gEstruturaTubos(destino).local_chega = gEstruturaTubos(Origem).local_chega
    gEstruturaTubos(destino).user_chega = gEstruturaTubos(Origem).user_chega
    gEstruturaTubos(destino).dt_saida = gEstruturaTubos(Origem).dt_saida
    gEstruturaTubos(destino).hr_saida = gEstruturaTubos(Origem).hr_saida
    gEstruturaTubos(destino).local_saida = gEstruturaTubos(Origem).local_saida
    gEstruturaTubos(destino).user_saida = gEstruturaTubos(Origem).user_saida
    gEstruturaTubos(destino).dt_eliminacao = gEstruturaTubos(Origem).dt_eliminacao
    gEstruturaTubos(destino).hr_eliminacao = gEstruturaTubos(Origem).hr_eliminacao
    gEstruturaTubos(destino).user_eliminacao = gEstruturaTubos(Origem).user_eliminacao
    gEstruturaTubos(destino).mot_novo = gEstruturaTubos(Origem).mot_novo
    gEstruturaTubos(destino).estado_tubo = gEstruturaTubos(Origem).estado_tubo
    gEstruturaTubos(destino).GrAna = gEstruturaTubos(Origem).GrAna
    gEstruturaTubos(destino).abrAna = gEstruturaTubos(Origem).abrAna
    gEstruturaTubos(destino).Especial = gEstruturaTubos(Origem).Especial
    gEstruturaTubos(destino).codTuboBar = gEstruturaTubos(Origem).codTuboBar
    gEstruturaTubos(destino).Descr_etiq_ana = gEstruturaTubos(Origem).Descr_etiq_ana
    gEstruturaTubos(destino).dt_imp = gEstruturaTubos(Origem).dt_imp
    gEstruturaTubos(destino).hr_imp = gEstruturaTubos(Origem).hr_imp
    gEstruturaTubos(destino).user_imp = gEstruturaTubos(Origem).user_imp
    gEstruturaTubos(destino).dt_ult_imp = gEstruturaTubos(Origem).dt_ult_imp
    gEstruturaTubos(destino).hr_ult_imp = gEstruturaTubos(Origem).hr_ult_imp
    gEstruturaTubos(destino).user_ult_imp = gEstruturaTubos(Origem).user_ult_imp
    gEstruturaTubos(destino).estado_tubo = gEstruturaTubos(Origem).estado_tubo
    'RGONCALVES 15.12.2014 - n� de c�pias original de etiquetas
    gEstruturaTubos(destino).num_copias_original = gEstruturaTubos(Origem).num_copias_original
    '
    'NELSONPSILVA CHVNG-7461 29.10.2018
    gEstruturaTubos(destino).dt_colheita = gEstruturaTubos(Origem).dt_colheita
    gEstruturaTubos(destino).hr_colheita = gEstruturaTubos(Origem).hr_colheita
    gEstruturaTubos(destino).etiqueta = gEstruturaTubos(Origem).etiqueta
    '
    For i = 1 To UBound(MaReq)
        If MaReq(i).indice_tubo = Origem And MaReq(i).seq_req_tubo = gEstruturaTubos(Origem).seq_req_tubo Then
            MaReq(i).indice_tubo = destino
        End If
    Next
    
    gEstruturaTubos(Origem).seq_req_tubo_local = mediComboValorNull
    gEstruturaTubos(Origem).seq_req_tubo = mediComboValorNull
    gEstruturaTubos(Origem).CodProd = ""
    gEstruturaTubos(Origem).CodTubo = ""
    gEstruturaTubos(Origem).Designacao_tubo = ""
    gEstruturaTubos(Origem).inf_complementar = ""
    gEstruturaTubos(Origem).num_copias = 0
    gEstruturaTubos(Origem).num_max_ana = 99
    gEstruturaTubos(Origem).num_ana = 0
    gEstruturaTubos(Origem).Garrafa = ""
    gEstruturaTubos(Origem).dt_previ = ""
    gEstruturaTubos(Origem).dt_chega = ""
    gEstruturaTubos(Origem).hr_chega = ""
    gEstruturaTubos(Origem).local_chega = ""
    gEstruturaTubos(Origem).user_chega = ""
    gEstruturaTubos(Origem).dt_saida = ""
    gEstruturaTubos(Origem).hr_saida = ""
    gEstruturaTubos(Origem).local_saida = ""
    gEstruturaTubos(Origem).user_saida = ""
    gEstruturaTubos(Origem).dt_eliminacao = ""
    gEstruturaTubos(Origem).hr_eliminacao = ""
    gEstruturaTubos(Origem).user_eliminacao = ""
    gEstruturaTubos(Origem).mot_novo = ""
    gEstruturaTubos(Origem).estado_tubo = ""
    gEstruturaTubos(Origem).GrAna = ""
    gEstruturaTubos(Origem).abrAna = ""
    gEstruturaTubos(Origem).Especial = False
    gEstruturaTubos(Origem).codTuboBar = ""
    gEstruturaTubos(Origem).Descr_etiq_ana = ""
    gEstruturaTubos(Origem).dt_imp = ""
    gEstruturaTubos(Origem).hr_imp = ""
    gEstruturaTubos(Origem).user_imp = ""
    gEstruturaTubos(Origem).dt_ult_imp = ""
    gEstruturaTubos(Origem).hr_ult_imp = ""
    gEstruturaTubos(Origem).user_ult_imp = ""
    gEstruturaTubos(Origem).estado_tubo = mediComboValorNull
    'RGONCALVES 15.12.2014 - n� de c�pias original de etiquetas
    gEstruturaTubos(Origem).num_copias_original = 0
    '
    'NELSONPSILVA CHVNG-7461 29.10.2018
    gEstruturaTubos(Origem).dt_colheita = ""
    gEstruturaTubos(Origem).hr_colheita = ""
    gEstruturaTubos(Origem).etiqueta = ""
    '
    TB_MoveEstrutTubos = True
 Exit Function
TrataErro:
    TB_MoveEstrutTubos = True
    BG_LogFile_Erros "Erro  TB_MoveEstrutTubos: " & Err.Description, "TB", "TB_MoveEstrutTubos", False
    Exit Function
    Resume Next
End Function



Public Function TB_PreencheEstrutTubos(rsTubo As ADODB.recordset, seqTubo As Long, CodProd As String, CodTubo As String, Designacao As String, inf_complementar As String, _
                      num_copias As Integer, num_max_ana As Integer, dt_previ As String, Garrafa As String, dt_chega As String, _
                       hr_chega As String, local_chega As String, user_chega As String, dt_saida As String, hr_saida As String, _
                       local_saida As String, user_saida As String, dt_eliminacao As String, hr_eliminacao As String, _
                       user_eliminacao As String, mot_novo As String, codTuboBar As String, estado_tubo As Integer, _
                       cod_tempo_colheita As Integer, hora_colheita As String, seq_req_tubos_local As Integer, dt_colheita As String, hr_colheita As String, etiqueta As String) As Integer
    On Error GoTo TrataErro
    TB_PreencheEstrutTubos = mediComboValorNull
    
    gTotalTubos = gTotalTubos + 1
    ReDim Preserve gEstruturaTubos(gTotalTubos)
    
    gEstruturaTubos(gTotalTubos).seq_req_tubo_local = seq_req_tubos_local
    gEstruturaTubos(gTotalTubos).seq_req_tubo = seqTubo
    gEstruturaTubos(gTotalTubos).CodProd = CodProd
    gEstruturaTubos(gTotalTubos).CodTubo = CodTubo
    gEstruturaTubos(gTotalTubos).codTuboBar = codTuboBar
    gEstruturaTubos(gTotalTubos).Designacao_tubo = Designacao
    gEstruturaTubos(gTotalTubos).inf_complementar = inf_complementar
    gEstruturaTubos(gTotalTubos).num_copias = num_copias
    gEstruturaTubos(gTotalTubos).num_max_ana = num_max_ana
    gEstruturaTubos(gTotalTubos).num_ana = 0
    
    gEstruturaTubos(gTotalTubos).Garrafa = Garrafa
    gEstruturaTubos(gTotalTubos).dt_previ = dt_previ
    gEstruturaTubos(gTotalTubos).dt_chega = dt_chega
    gEstruturaTubos(gTotalTubos).hr_chega = hr_chega
    gEstruturaTubos(gTotalTubos).local_chega = local_chega
    gEstruturaTubos(gTotalTubos).user_chega = user_chega
    gEstruturaTubos(gTotalTubos).dt_saida = dt_saida
    gEstruturaTubos(gTotalTubos).hr_saida = hr_saida
    gEstruturaTubos(gTotalTubos).local_saida = local_saida
    gEstruturaTubos(gTotalTubos).user_saida = user_saida
    gEstruturaTubos(gTotalTubos).dt_eliminacao = dt_eliminacao
    gEstruturaTubos(gTotalTubos).hr_eliminacao = hr_eliminacao
    gEstruturaTubos(gTotalTubos).user_eliminacao = user_eliminacao
    gEstruturaTubos(gTotalTubos).mot_novo = mot_novo
    
    
    gEstruturaTubos(gTotalTubos).GrAna = ""
    gEstruturaTubos(gTotalTubos).abrAna = ""
    gEstruturaTubos(gTotalTubos).Especial = False
    gEstruturaTubos(gTotalTubos).Descr_etiq_ana = ""
    
    gEstruturaTubos(gTotalTubos).dt_imp = BL_HandleNull(rsTubo!dt_imp, "")
    gEstruturaTubos(gTotalTubos).hr_imp = BL_HandleNull(rsTubo!hr_imp, "")
    gEstruturaTubos(gTotalTubos).user_imp = BL_HandleNull(rsTubo!user_imp, "")
    gEstruturaTubos(gTotalTubos).dt_ult_imp = BL_HandleNull(rsTubo!dt_ult_imp, "")
    gEstruturaTubos(gTotalTubos).hr_ult_imp = BL_HandleNull(rsTubo!hr_ult_imp, "")
    gEstruturaTubos(gTotalTubos).user_ult_imp = BL_HandleNull(rsTubo!user_ult_imp, "")
    gEstruturaTubos(gTotalTubos).estado_tubo = estado_tubo
    'RGONCALVES 15.12.2014 - n� de c�pias original de etiquetas
    gEstruturaTubos(gTotalTubos).num_copias_original = num_copias
    'NELSONPSILVA CHVNG-7461 29.10.2018
    gEstruturaTubos(gTotalTubos).dt_colheita = dt_colheita
    gEstruturaTubos(gTotalTubos).hr_colheita = hr_colheita
    gEstruturaTubos(gTotalTubos).etiqueta = etiqueta
    '
    gEstruturaTubos(gTotalTubos).cod_tempo_colheita = cod_tempo_colheita
    If cod_tempo_colheita > mediComboValorNull Then
        gEstruturaTubos(gTotalTubos).tempo_colheita = BL_HandleNull(BL_SelCodigo("SL_TBF_TEMPOs_COLHEITA", "MINUTOS", "COD_TEMPO_COLHEITA", cod_tempo_colheita), 0)
    End If
    gEstruturaTubos(gTotalTubos).hora_colheita = hora_colheita
    TB_PreencheEstrutTubos = gTotalTubos
 Exit Function
TrataErro:
    TB_PreencheEstrutTubos = mediComboValorNull
    BG_LogFile_Erros "TB_PreencheEstrutTubos: " & Err.Number & " - " & Err.Description, "TB", "TB_PreencheEstrutTubos"
    Exit Function
    Resume Next
End Function

Public Function TB_ProcuraTubos(FGTubos As MSFlexGrid, NumReq As Long, flgPreencheGrid As Boolean)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    Dim indice As Integer
    Dim j As Long
    Dim n_requis As String
    
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Reutilizacao_Colheitas = mediSim And TotalReqColh > 0 Then
        If UBound(reqColh) > 0 Then
            For j = 1 To UBound(reqColh)
                n_requis = n_requis & reqColh(j).n_req & "','"
            Next j
            n_requis = "('" & Mid(n_requis, 1, Len(n_requis) - 2) & ")"
        Else
            n_requis = NumReq
        End If
    Else
        n_requis = NumReq
    End If
    '
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    If Not FGTubos Is Nothing Then
        TB_LimpaFgTubos FGTubos
    End If
    
    sSql = "SELECT x1.seq_req_tubo, x2.cod_prod, x2.cod_tubo, x2.descr_tubo,x2.inf_complementar, x2.num_copias, x2.num_max_ana, "
    sSql = sSql & " x1.dt_previ, x1.id_garrafa, x1.dt_chega, x1.hr_chega, x1.local_chega, x1.user_chega, x1.dt_saida, x1.hr_saida,"
    sSql = sSql & " x1.local_saida, x1.user_saida, x1.dt_eliminacao, x1.hr_eliminacao, x1.user_eliminacao, x1.mot_novo, x2.cod_etiq, "
    sSql = sSql & " x1.estado_tubo,dt_imp, hr_imp, dt_ult_imp, hr_ult_imp, user_imp, user_ult_imp, x1.hr_previ_colheita, x2.cod_tempo_colheita,"
    sSql = sSql & " x1.seq_req_tubo_local, x1.dt_colheita, x1.hr_colheita, x1.etiqueta"
    sSql = sSql & " FROM sl_req_tubo x1, sl_tubo x2 LEFT OUTER JOIN sl_produto x3 on  x2.cod_prod = x3.cod_produto "
    sSql = sSql & " WHERE n_req in " & n_requis & " AND x1.cod_tubo = x2.cod_tubo "
    sSql = sSql & " ORDER BY seq_req_tubo "
    rsTubo.CursorType = adOpenStatic
    rsTubo.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount >= 1 Then
        While Not rsTubo.EOF
        
            indice = TB_PreencheEstrutTubos(rsTubo, BL_HandleNull(rsTubo!seq_req_tubo, mediComboValorNull), BL_HandleNull(rsTubo!cod_prod, ""), BL_HandleNull(rsTubo!cod_tubo, ""), BL_HandleNull(rsTubo!descR_tubo, ""), _
                              BL_HandleNull(rsTubo!inf_complementar, ""), BL_HandleNull(rsTubo!num_copias, 0), BL_HandleNull(rsTubo!num_max_ana, 99), _
                              BL_HandleNull(rsTubo!dt_previ, ""), BL_HandleNull(rsTubo!id_garrafa, ""), BL_HandleNull(rsTubo!dt_chega, ""), _
                              BL_HandleNull(rsTubo!hr_chega, ""), BL_HandleNull(rsTubo!local_chega, ""), BL_HandleNull(rsTubo!user_chega, ""), _
                              BL_HandleNull(rsTubo!dt_saida, ""), BL_HandleNull(rsTubo!hr_saida, ""), BL_HandleNull(rsTubo!local_saida, ""), _
                              BL_HandleNull(rsTubo!user_saida, ""), BL_HandleNull(rsTubo!dt_eliminacao, ""), BL_HandleNull(rsTubo!hr_eliminacao, ""), _
                              BL_HandleNull(rsTubo!user_eliminacao, ""), BL_HandleNull(rsTubo!mot_novo, ""), BL_HandleNull(rsTubo!cod_etiq, ""), _
                              BL_HandleNull(rsTubo!estado_tubo, mediComboValorNull), BL_HandleNull(rsTubo!cod_tempo_colheita, mediComboValorNull), BL_HandleNull(rsTubo!hr_previ_colheita, ""), _
                               BL_HandleNull(rsTubo!seq_req_tubo_local, mediComboValorNull), BL_HandleNull(rsTubo!dt_colheita, ""), BL_HandleNull(rsTubo!hr_colheita, ""), BL_HandleNull(rsTubo!etiqueta, ""))
            If flgPreencheGrid = True Then
                If Not FGTubos Is Nothing Then
                    TB_PreencheFgTubo FGTubos, indice
                End If
            End If
            rsTubo.MoveNext
        Wend
    End If
    rsTubo.Close
    Set rsTubo = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "TB_ProcuraTubos: " & Err.Number & " - " & Err.Description, "TB", "TB_ProcuraTubos"
    Exit Function
    Resume Next
End Function


Public Sub TB_LimpaFgTubos(FGTubos As MSFlexGrid)
     On Error GoTo TrataErro
   
    Dim j As Long
    Dim i As Integer
    
    j = FGTubos.rows - 1
    While j > 0
        If j > 1 Then
            FGTubos.RemoveItem j
        Else
            For i = 0 To FGTubos.Cols - 1
                FGTubos.TextMatrix(j, i) = ""
            Next i
        End If
        
        j = j - 1
    Wend
    
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "TB_LimpaFgTubos: " & Err.Number & " - " & Err.Description, "TB", "TB_LimpaFgTubos"
    Exit Sub
    Resume Next

End Sub


Public Sub TB_DefTipoFGTubos(FGTubos As MSFlexGrid)
    With FGTubos
        .rows = 2
        .FixedRows = 1
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If gAtiva_Reutilizacao_Colheitas = mediSim Then
            .Cols = 9
        Else
            .Cols = 10
        End If
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If gAtiva_Reutilizacao_Colheitas = mediSim Then
            .ColWidth(gColTuboSeqTuboLocal_reutil) = 900
            .Col = gColTuboSeqTuboLocal_reutil
            .ColAlignment(gColTuboSeqTuboLocal_reutil) = flexAlignLeftCenter
            .TextMatrix(0, gColTuboSeqTuboLocal_reutil) = "Tubo"
            
            .ColWidth(gColTuboDescrTubo_reutil) = 4300
            .Col = gColTuboDescrTubo_reutil
            .TextMatrix(0, gColTuboDescrTubo_reutil) = "Descri��o"
            .ColAlignment(gColTuboDescrTubo_reutil) = flexAlignLeftCenter
            
            .ColWidth(gColTuboDtColheita_reutil) = 950
            .Col = gColTuboDtColheita_reutil
            .TextMatrix(0, gColTuboDtColheita_reutil) = "Colheita"
            
            .ColWidth(gColTuboGarrafa_reutil) = 1100
            .Col = gColTuboGarrafa_reutil
            .TextMatrix(0, gColTuboGarrafa_reutil) = "Garrafa"
            
            .ColWidth(gColTuboDtPrevi_reutil) = 950
            .Col = gColTuboDtPrevi_reutil
            .TextMatrix(0, gColTuboDtPrevi_reutil) = "Previsto"
            
            .ColWidth(gColTuboDtChega_reutil) = 950
            .Col = gColTuboDtChega_reutil
            .TextMatrix(0, gColTuboDtChega_reutil) = "Chegada"
            
            .ColWidth(gColTuboHrChega_reutil) = 950
            .Col = gColTuboHrChega_reutil
            .TextMatrix(0, gColTuboHrChega_reutil) = "Hr.Chegada"
            
            .ColWidth(gColTuboUtilChega_reutil) = 900
            .Col = gColTuboUtilChega_reutil
            .TextMatrix(0, gColTuboUtilChega_reutil) = "Util Chegada"
            
            .ColWidth(gColTuboEstado_reutil) = 1000
            .Col = gColTuboEstado_reutil
            .TextMatrix(0, gColTuboEstado_reutil) = "Estado"
        '
        Else
        .ColWidth(gColTuboSeqTubo) = 900
        .Col = gColTuboSeqTubo
        .ColAlignment(gColTuboSeqTubo) = flexAlignLeftCenter
        .TextMatrix(0, gColTuboSeqTubo) = "ID"
        
        .ColWidth(gColTuboSeqTuboLocal) = 500
        .Col = gColTuboSeqTuboLocal
        .ColAlignment(gColTuboSeqTuboLocal) = flexAlignLeftCenter
        .TextMatrix(0, gColTuboSeqTuboLocal) = "#"
        
        .ColWidth(gColTuboCodTubo) = 800
        .Col = gColTuboCodTubo
        .ColAlignment(gColTuboCodTubo) = flexAlignLeftCenter
        .TextMatrix(0, gColTuboCodTubo) = "Tubo"
        
        .ColWidth(gColTuboDescrTubo) = 4300
        .Col = gColTuboDescrTubo
        .TextMatrix(0, gColTuboDescrTubo) = "Descri��o"
        .ColAlignment(gColTuboDescrTubo) = flexAlignLeftCenter
        
        .ColWidth(gColTuboGarrafa) = 1100
        .Col = gColTuboGarrafa
        .TextMatrix(0, gColTuboGarrafa) = "Garrafa"
        
        .ColWidth(gColTuboDtPrevi) = 950
        .Col = gColTuboDtPrevi
        .TextMatrix(0, gColTuboDtPrevi) = "Previsto"
        
        .ColWidth(gColTuboDtChega) = 950
        .Col = gColTuboDtChega
        .TextMatrix(0, gColTuboDtChega) = "Chegada"
        
        .ColWidth(gColTuboHrChega) = 950
        .Col = gColTuboHrChega
        .TextMatrix(0, gColTuboHrChega) = "Hr.Chegada"
        
        .ColWidth(gColTuboUtilChega) = 900
        .Col = gColTuboUtilChega
        .TextMatrix(0, gColTuboUtilChega) = "Util Chegada"
        
        .ColWidth(gColTuboEstado) = 1000
        .Col = gColTuboEstado
        .TextMatrix(0, gColTuboEstado) = "Estado"
        End If
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "DefTipoFGTubos: " & Err.Number & " - " & Err.Description, "TB", "DefTipoFGTubos"
    Exit Sub
    Resume Next
End Sub

'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
Public Sub TB_GravaTubosReq(NReq As String, ByVal dt_chega As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsT As New ADODB.recordset
    Dim r_seq_req_tubo As Long
    Dim i As Integer
    Dim j As Integer
    Dim codAna() As String
    For i = 1 To gTotalTubos
        'RGONCALVES 15.12.2014
        'If gRegChegaTubos <> mediSim And gEstruturaTubos(i).dt_chega = "" And gEstruturaTubos(i).seq_req_tubo <= 0 Then
        If gRegChegaTubos <> mediSim And dt_chega <> "" And gEstruturaTubos(i).seq_req_tubo <= 0 Then
        '
            gEstruturaTubos(i).dt_chega = Bg_DaData_ADO
            gEstruturaTubos(i).hr_chega = Bg_DaHora_ADO
            gEstruturaTubos(i).local_chega = gCodLocal
            gEstruturaTubos(i).user_chega = CStr(gCodUtilizador)
            gEstruturaTubos(i).estado_tubo = gEstadosTubo.chegado
            
        End If
        If gEstruturaTubos(i).seq_req_tubo_local <= mediComboValorNull Then
            gEstruturaTubos(i).seq_req_tubo_local = TB_GetSeqReqTuboLocal(NReq)
        End If
        
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If gAtiva_Nova_Numeracao_Tubo = mediSim Then
            If gEstruturaTubos(i).etiqueta = "" Then
                gEstruturaTubos(i).etiqueta = Right("00" & gEstruturaTubos(i).seq_req_tubo_local, 2) & Right("0000000" & NReq, 7)
            End If
        End If
        
        r_seq_req_tubo = TB_InsereTuboBD(gEstruturaTubos(i).seq_req_tubo, NReq, gEstruturaTubos(i).CodTubo, gEstruturaTubos(i).dt_previ, gEstruturaTubos(i).dt_chega, _
                          gEstruturaTubos(i).hr_chega, gEstruturaTubos(i).Garrafa, gEstruturaTubos(i).user_chega, gEstruturaTubos(i).local_chega, codAna, _
                          gEstruturaTubos(i).estado_tubo, gEstruturaTubos(i).seq_req_tubo_local, gEstruturaTubos(i).etiqueta)
                          
        If gEstruturaTubos(i).seq_req_tubo = mediComboValorNull Then
            gEstruturaTubos(i).seq_req_tubo = r_seq_req_tubo
            Select Case gTipoInstituicao
                Case gTipoInstituicaoHospitalar
                    TB_PreencheFgTubo FormGestaoRequisicao.FGTubos, i
                Case gTipoInstituicaoPrivada
                    TB_PreencheFgTubo FormGestaoRequisicaoPrivado.FGTubos, i
                Case gTipoInstituicaoVet
                    TB_PreencheFgTubo FormGestaoRequisicaoVet.FGTubos, i
                Case gTipoInstituicaoAguas
                    TB_PreencheFgTubo FormGestaoRequisicaoAguas.FGTubos, i
            End Select
            For j = 1 To UBound(MaReq)
                If MaReq(j).indice_tubo = i And MaReq(j).seq_req_tubo = mediComboValorNull Then
                    MaReq(j).seq_req_tubo = r_seq_req_tubo
                End If
            Next j
        End If
    Next i
    

Exit Sub
TrataErro:
    BG_LogFile_Erros "TB_GravaTubosReq: " & Err.Number & " - " & Err.Description, "TB", "TB_GravaTubosReq"
    Exit Sub
    Resume Next
End Sub



Private Function TB_VerificaTuboJaMarcado(n_req As String, cod_tubo As String) As Long
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    TB_VerificaTuboJaMarcado = mediComboValorNull
    sSql = "SELECT * FROM SL_REQ_TUBO WHERE n_req = " & n_req & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        TB_VerificaTuboJaMarcado = BL_HandleNull(rsAna!seq_req_tubo, mediComboValorNull)
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    TB_VerificaTuboJaMarcado = mediComboValorNull
    BG_LogFile_Erros "TB_VerificaTuboJaMarcado: " & Err.Number & " - " & Err.Description, "TB", "TB_VerificaTuboJaMarcado"
    Exit Function
    Resume Next
End Function


Public Function TB_ImprimeEtiqueta(NumRequis As String, indiceTubo As Integer, numAdmin As Integer, cod_local As String, impressora As String) As Boolean
    On Error GoTo TrataErro
    Dim k As Integer
    Dim j As Integer
    Dim jReqBar As Integer
    Dim sql As String
    Dim sqlApagaCrystal As String
    Dim NumReqBar As String
    Dim EtqCrystal As String
    Dim PrinterEtiq As String
    Dim numEtiq As Long
    Dim nomeComputador As String
    Dim numeroSessao As String
    Dim idade As String
    Dim CodProd_aux As String
    Dim sqlInfoAux As String
    Dim rsInfoAux As ADODB.recordset
    Dim dataImpressao As String
    TB_ImprimeEtiqueta = False
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 1"
    NumReqBar = ""
    For jReqBar = 1 To (7 - Len(NumRequis))
        NumReqBar = NumReqBar & "0"
    Next jReqBar
    NumReqBar = NumReqBar & NumRequis
    
    idade = ""
    If impressora = "" Then
        PrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    Else
        PrinterEtiq = impressora
    End If
    
    nomeComputador = gComputador
    numeroSessao = gNumeroSessao

    'Extrair a informa��o auxiliar a ser usada na impress�o (utente e requisi��o)
    sqlInfoAux = "SELECT n_req, t_utente, utente, nome_ute, abrev_ute, t_urg, descr_t_urg, descr_t_sit, sl_requis.n_epis, dt_previ, n_req_assoc, sl_requis.dt_chega, sl_identif.doc_identif, sl_identif.n_proc_1 "
    sqlInfoAux = sqlInfoAux & " FROM sl_requis, sl_identif, sl_tbf_t_urg, sl_tbf_t_sit WHERE "
    sqlInfoAux = sqlInfoAux & " sl_requis.seq_utente = sl_identif.seq_utente AND cod_t_urg = t_urg AND sl_requis.t_sit = cod_t_sit(+) "
    sqlInfoAux = sqlInfoAux & " AND n_req = " & NumRequis & ""
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 2"
                
    'Carregar sql para o RecordSet correspondente
    Set rsInfoAux = New ADODB.recordset
    rsInfoAux.CursorLocation = adUseServer
    rsInfoAux.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sqlInfoAux
    rsInfoAux.Open sqlInfoAux, gConexao
                        
    
    
    ' -------------------------------------------------------------------------------------------
    ' ETIQUETAS TUBOS
    ' -------------------------------------------------------------------------------------------
    If indiceTubo > mediComboValorNull Then
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 3"
        If cod_local <> "" Then
            gEstruturaTubos(indiceTubo).Descr_etiq_ana = BL_SelCodigo("GR_EMPR_INST", "NOME_EMPR", "EMPRESA_ID", cod_local, "V")
        End If
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 4"
        For j = 1 To gEstruturaTubos(indiceTubo).num_copias
            If Not BL_LerEtiqIni("ETIQ.INI") Then
                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                Exit Function
            End If
            If Not BL_EtiqOpenPrinter(PrinterEtiq) Then
                MsgBox "Imposs�vel abrir impressora etiquetas"
                Exit Function
            End If
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 5"
            'Emanuel Sousa - Verificar o calculo do numero de etiquetas
            numEtiq = gEstruturaTubos(indiceTubo).num_copias
            If gLAB = "CHVNG" Then
                dataImpressao = BL_HandleNull(gEstruturaTubos(indiceTubo).dt_chega, "")
            Else
                dataImpressao = BL_HandleNull(gEstruturaTubos(indiceTubo).dt_chega, BL_HandleNull(rsInfoAux!dt_chega, rsInfoAux!dt_previ))
            End If
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 6"
            Call TB_EtiqPrint_Tubos(gEstruturaTubos(indiceTubo).GrAna, gEstruturaTubos(indiceTubo).abrAna, _
                           rsInfoAux!t_utente, rsInfoAux!Utente, _
                           BL_HandleNull(rsInfoAux!n_proc_1, ""), NumRequis, IIf(rsInfoAux!t_urg <> -1, Mid(rsInfoAux!descr_t_urg, 1, 3), " "), _
                           dataImpressao, gEstruturaTubos(indiceTubo).CodProd & IIf(gEstruturaTubos(indiceTubo).CodTubo <> "", "  (", " ") & gEstruturaTubos(indiceTubo).CodTubo & IIf(gEstruturaTubos(indiceTubo).CodTubo <> "", ")", ""), _
                           rsInfoAux!nome_ute, BL_HandleNull(rsInfoAux!abrev_ute, Empty), gEstruturaTubos(indiceTubo).codTuboBar & NumReqBar, _
                            BG_CvPlica(gEstruturaTubos(indiceTubo).Designacao_tubo), gEstruturaTubos(indiceTubo).inf_complementar, numEtiq, _
                            gEstruturaTubos(indiceTubo).Descr_etiq_ana, BL_HandleNull(rsInfoAux!doc_identif, ""), gEstruturaTubos(indiceTubo).seq_req_tubo, _
                            gEstruturaTubos(indiceTubo).seq_req_tubo_local, gEstruturaTubos(indiceTubo).etiqueta, , BL_HandleNull(rsInfoAux!n_Req_assoc, ""), _
                            gEstruturaTubos(indiceTubo).codTuboBar)
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 7"

            BL_EtiqClosePrinter
            BL_RegistaImprEtiq NumRequis, gEstruturaTubos(indiceTubo).CodTubo
            
            If gEstruturaTubos(indiceTubo).dt_imp = "" Then
                gEstruturaTubos(indiceTubo).dt_imp = Bg_DaData_ADO
                gEstruturaTubos(indiceTubo).hr_imp = Bg_DaHora_ADO
                gEstruturaTubos(indiceTubo).user_imp = gCodUtilizador
                gEstruturaTubos(indiceTubo).dt_ult_imp = gEstruturaTubos(indiceTubo).dt_imp
                gEstruturaTubos(indiceTubo).hr_ult_imp = gEstruturaTubos(indiceTubo).hr_imp
                gEstruturaTubos(indiceTubo).user_ult_imp = gEstruturaTubos(indiceTubo).user_imp
            Else
                gEstruturaTubos(indiceTubo).dt_ult_imp = Bg_DaData_ADO
                gEstruturaTubos(indiceTubo).hr_ult_imp = Bg_DaHora_ADO
                gEstruturaTubos(indiceTubo).user_ult_imp = gCodUtilizador
            End If
                
        Next j
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 8"

    ' -------------------------------------------------------------------------------------------
    ' ETIQUETAS ADMINISTRATIVAS
    ' -------------------------------------------------------------------------------------------
    For k = 1 To numAdmin

        If Not BL_LerEtiqIni("ETIQ.INI") Then
            MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
            Exit Function
        End If
        If Not BL_EtiqOpenPrinter(PrinterEtiq) Then
            MsgBox "Imposs�vel abrir impressora etiquetas"
            Exit Function
        End If
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 9"
        
        dataImpressao = BL_HandleNull(rsInfoAux!dt_chega, rsInfoAux!dt_previ)
        Call TB_EtiqPrint_Tubos("", _
                       "", _
                       BL_HandleNull(rsInfoAux!t_utente, ""), _
                       BL_HandleNull(rsInfoAux!Utente, ""), BL_HandleNull(rsInfoAux!n_proc_1, ""), _
                       NumRequis, _
                       IIf(rsInfoAux!t_urg <> -1, Mid(rsInfoAux!descr_t_urg, 1, 3), " "), _
                       BL_HandleNull(dataImpressao, ""), _
                       "", BL_HandleNull(rsInfoAux!nome_ute, ""), BL_HandleNull(rsInfoAux!abrev_ute, Empty), "", "Administrativa", "", 1, "", _
                       BL_HandleNull(rsInfoAux!doc_identif, ""), mediComboValorNull, mediComboValorNull, "")

        BL_EtiqClosePrinter
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 10"

        ' -------------------------------------------------------------------------------------------
        ' ETIQUETAS NO FIM
        ' -------------------------------------------------------------------------------------------
        'chave que indica se queremos etiqueta de fim ou n�o
        If gEtiqFim = 1 Then
            If Not BL_LerEtiqIni("Etiq_fim.INI") Then
                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                Exit Function
            End If
            If Not BL_EtiqOpenPrinter(PrinterEtiq) Then
                MsgBox "Imposs�vel abrir impressora etiquetas"
                Exit Function
            End If
            dataImpressao = BL_HandleNull(rsInfoAux!dt_chega, rsInfoAux!dt_previ)

    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 11"
            Call TB_EtiqPrint_Tubos("", _
                           "", _
                           BL_HandleNull(rsInfoAux!t_utente, ""), _
                           BL_HandleNull(rsInfoAux!Utente, ""), "", _
                           NumRequis, _
                           IIf(rsInfoAux!t_urg <> -1, Mid(rsInfoAux!descr_t_urg, 1, 3), " "), _
                           BL_HandleNull(dataImpressao, ""), _
                           "", BL_HandleNull(rsInfoAux!nome_ute, ""), BL_HandleNull(rsInfoAux!abrev_ute, Empty), _
                           "", "Administrativa", "", "1", "", BL_HandleNull(rsInfoAux!doc_identif, ""), "", _
                           BL_HandleNull(rsInfoAux!n_Req_assoc, ""), "")

            BL_EtiqClosePrinter
        End If
    If gModoDebug = mediSim Then BG_LogFile_Erros "Passa aki 11"
    Next k
    rsInfoAux.Close
    Set rsInfoAux = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "TB_ImprimeEtiqueta: " & Err.Number & " - " & Err.Description, "TB", "TB_ImprimeEtiqueta", True
    Exit Function
    Resume Next
End Function



Public Function TB_EtiqPrint_Tubos( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal abrev_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String, _
    ByVal num_copias As Integer, _
    ByVal Descr_etiq_ana As String, _
    ByVal doc_identif As String, _
    ByVal seq_req_tubo As Long, _
    ByVal seq_req_tubo_local As Integer, _
    ByVal etiqueta As String, _
    Optional ByVal N_REQ_ARS As String, _
    Optional ByVal n_Req_assoc As String, _
    Optional ByVal cod_etiq As String _
    ) As Boolean
     On Error GoTo TrataErro
    TB_EtiqPrint_Tubos = False
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_AbreviaNome(BL_RemovePortuguese(StrConv(nome_ute, vbProperCase)), 40))
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", abrev_ute)
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", N_REQ_ARS)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", Descr_etiq_ana)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ASSOC}", n_Req_assoc)
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", num_copias)
    sWrittenData = Replace(sWrittenData, "{QUANTIDADE}", num_copias)
    sWrittenData = Replace(sWrittenData, "{COD_ETIQ}", cod_etiq)
    sWrittenData = Replace(sWrittenData, "{DOC_IDENTIF}", doc_identif)
    sWrittenData = Replace(sWrittenData, "{SEQ_REQ_TUBO}", seq_req_tubo)
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Nova_Numeracao_Tubo = mediSim Then
        sWrittenData = Replace(sWrittenData, "{SEQ_REQ_TUBO_LOCAL}", etiqueta)
    Else
        sWrittenData = Replace(sWrittenData, "{SEQ_REQ_TUBO_LOCAL}", Right("00" & seq_req_tubo_local, 2) & Right("0000000" & n_req, 7))
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros sWrittenData

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    TB_EtiqPrint_Tubos = True
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", num_copias)
        EtiqEndJob = Replace(EtiqEndJob, "{QUANTIDADE}", num_copias)
    End If
Exit Function
TrataErro:
    TB_EtiqPrint_Tubos = False
    BG_LogFile_Erros "TB_EtiqPrint_Tubos: " & Err.Number & " - " & Err.Description, "TB", "TB_EtiqPrint_Tubos"
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------------------------------------------------

' INSERE TUBOS E PRODUTOS PARA A ANALISE EM CAUSA

' ------------------------------------------------------------------------------------------------
Public Function TB_InsereTubo(n_req As String, cod_agrup As String, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String, _
                              dt_previ As String, dt_chega As String, hr_chega As String, user_chega As String, local_chega As String, _
                              estado_tubo As Integer) As Long
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim cod_tubo_aux As String
    Dim cod_Agrup2() As String
    On Error GoTo TrataErro
    
    TB_InsereTubo = mediComboValorNull
    Select Case Mid(cod_agrup, 1, 1)
        Case "P"
            sSql = "SELECT cod_tubo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_agrup)
        Case "C"
            sSql = "SELECT cod_tubo FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_agrup)
        Case "S"
            sSql = "SELECT cod_tubo FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_agrup)
    End Select
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount = 1 Then
        cod_tubo_aux = BL_HandleNull(rsAna!cod_tubo, "")
    End If
    rsAna.Close
    
    'SE ANALISE TEM TUBO ASSOCIADO
    If cod_tubo_aux <> "" Then
        TB_InsereTubo = TB_VerificaTuboJaMarcado(n_req, cod_tubo_aux)
        If TB_InsereTubo = mediComboValorNull Then
            TB_InsereTubo = TB_InsereTuboBD(mediComboValorNull, n_req, cod_tubo_aux, dt_previ, dt_chega, hr_chega, "", _
                                            user_chega, local_chega, cod_Agrup2, estado_tubo, TB_GetSeqReqTuboLocal(n_req), "")
        End If
    Else
        ' SE ANALISE FOR UM PERFIL E NAO TIVER TUBO ATRIBUIDO
        If cod_agrup = Cod_Perfil Then
            'VERIFICA SE COMPLEXA TEM TUBO ATRIBUIDO
            If cod_ana_c <> gGHOSTMEMBER_C And cod_ana_c <> "0" Then
                sSql = "SELECT cod_tubo FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c)
                rsAna.CursorLocation = adUseServer
                rsAna.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsAna.Open sSql, gConexao
                If rsAna.RecordCount = 1 Then
                    cod_tubo_aux = BL_HandleNull(rsAna!cod_tubo, "")
                End If
                rsAna.Close
                If cod_tubo_aux <> "" Then
                    TB_InsereTubo = TB_VerificaTuboJaMarcado(n_req, cod_tubo_aux)
                    If TB_InsereTubo = mediComboValorNull Then
                        TB_InsereTubo = TB_InsereTuboBD(mediComboValorNull, n_req, cod_tubo_aux, dt_previ, dt_chega, hr_chega, _
                                                        "", user_chega, local_chega, cod_Agrup2, estado_tubo, TB_GetSeqReqTuboLocal(n_req), "")
                    End If
                ElseIf cod_ana_s <> gGHOSTMEMBER_S Then
                    ' SE NAO TEM TUBO NO PERFIL NEM NA COMPLEXA, VERIFICA NA SIMPLES
                    sSql = "SELECT cod_tubo FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
                    rsAna.CursorLocation = adUseServer
                    rsAna.CursorType = adOpenStatic
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    rsAna.Open sSql, gConexao
                    If rsAna.RecordCount = 1 Then
                        cod_tubo_aux = BL_HandleNull(rsAna!cod_tubo, "")
                    End If
                    rsAna.Close
                    If cod_tubo_aux <> "" Then
                        TB_InsereTubo = TB_VerificaTuboJaMarcado(n_req, cod_tubo_aux)
                        If TB_InsereTubo = mediComboValorNull Then
                            TB_InsereTubo = TB_InsereTuboBD(mediComboValorNull, n_req, cod_tubo_aux, dt_previ, dt_chega, hr_chega, "", user_chega, local_chega, _
                                                            cod_Agrup2, estado_tubo, TB_GetSeqReqTuboLocal(n_req), "")
                        End If
                    End If
                End If
            ElseIf cod_ana_s <> gGHOSTMEMBER_S Then
                ' SE NAO TEM TUBO NO PERFIL NEM NA COMPLEXA, VERIFICA NA SIMPLES
                sSql = "SELECT cod_tubo FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
                rsAna.CursorLocation = adUseServer
                rsAna.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsAna.Open sSql, gConexao
                If rsAna.RecordCount = 1 Then
                    cod_tubo_aux = BL_HandleNull(rsAna!cod_tubo, "")
                End If
                rsAna.Close
                If cod_tubo_aux <> "" Then
                    TB_InsereTubo = TB_VerificaTuboJaMarcado(n_req, cod_tubo_aux)
                    If TB_InsereTubo = mediComboValorNull Then
                        TB_InsereTubo = TB_InsereTuboBD(mediComboValorNull, n_req, cod_tubo_aux, dt_previ, dt_chega, hr_chega, _
                                                        "", user_chega, local_chega, cod_Agrup2, estado_tubo, TB_GetSeqReqTuboLocal(n_req), "")
                    End If
                End If
            End If
        'SE FOR UMA COMPLEXA
        ElseIf cod_agrup = cod_ana_c And cod_ana_s <> gGHOSTMEMBER_S Then
            ' SE NAO TEM TUBO NA COMPLEXA, VERIFICA NA SIMPLES
            sSql = "SELECT cod_tubo FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount = 1 Then
                cod_tubo_aux = BL_HandleNull(rsAna!cod_tubo, "")
            End If
            rsAna.Close
            If cod_tubo_aux <> "" Then
                TB_InsereTubo = TB_VerificaTuboJaMarcado(n_req, cod_tubo_aux)
                If TB_InsereTubo = mediComboValorNull Then
                    TB_InsereTubo = TB_InsereTuboBD(mediComboValorNull, n_req, cod_tubo_aux, dt_previ, dt_chega, hr_chega, "", _
                                                    user_chega, local_chega, cod_Agrup2, estado_tubo, TB_GetSeqReqTuboLocal(n_req), "")
                End If
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: TB_InsereTubo: " & Err.Description, "TB", "TB_InsereTubo", False
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------------------------------------------------

' INSERE TUBOS NA BD PARA A ANALISE EM CAUSA

' ------------------------------------------------------------------------------------------------
Public Function TB_InsereTuboBD(ByVal seq_req_tubo As Long, ByVal n_req As String, ByVal cod_tubo As String, ByVal dt_previ As String, ByVal dt_chega As String, _
                                ByVal hr_chega As String, ByVal id_garrafa As String, ByVal user_chega As String, _
                                ByVal local_chega As String, cod_ana() As String, estado_tubo As Integer, seq_req_tubo_local As Integer, etiqueta As String) As Long
    Dim sSql As String
    Dim iReg As Integer
    Dim i As Integer
    Dim rsT As New ADODB.recordset
    Dim strEstado As String
    
    
    On Error GoTo TrataErro
    TB_InsereTuboBD = mediComboValorNull
    If seq_req_tubo_local = mediComboValorNull Then
        seq_req_tubo_local = TB_GetSeqReqTuboLocal(n_req)
    End If
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Nova_Numeracao_Tubo = mediSim Then
        If etiqueta = "" Then
            etiqueta = Right("00" & seq_req_tubo_local, 2) & Right("0000000" & n_req, 7)
        End If
    End If
    If estado_tubo = mediComboValorNull Then
        strEstado = "NULL"
    Else
        strEstado = CStr(estado_tubo)
    End If
    If seq_req_tubo <= 0 Then
        If gSGBD = gOracle Then
            sSql = "SELECT seq_req_tubo.nextval seq_req_tubo FROM dual "
        ElseIf gSGBD = gPostGres Then
            sSql = "SELECT nextval('seq_req_tubo') seq_req_tubo "
        End If
        rsT.CursorLocation = adUseServer
        rsT.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsT.Open sSql, gConexao
        If rsT.RecordCount >= 1 Then
            seq_req_tubo = BL_HandleNull(rsT!seq_req_tubo, mediComboValorNull)
        Else
            GoTo TrataErro
        End If
        rsT.Close
        
        
        sSql = "INSERT INTO sl_req_tubo(seq_req_tubo_local, seq_req_tubo, etiqueta, n_req, cod_tubo, dt_previ, dt_chega, hr_chega, id_garrafa, "
        sSql = sSql & " user_chega,local_chega, estado_tubo) values("
        sSql = sSql & seq_req_tubo_local & "," & seq_req_tubo & ", " & BL_TrataStringParaBD(etiqueta) & ","
        sSql = sSql & n_req & "," & BL_TrataStringParaBD(cod_tubo) & "," & BL_TrataDataParaBD(dt_previ) & ","
        sSql = sSql & BL_TrataDataParaBD(dt_chega) & "," & BL_TrataStringParaBD(hr_chega) & "," & BL_TrataStringParaBD(id_garrafa) & ","
        sSql = sSql & BL_TrataStringParaBD(user_chega) & "," & BL_TrataStringParaBD(local_chega) & "," & strEstado & ")"
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
   
    End If
    TB_InsereTuboBD = seq_req_tubo
Exit Function
TrataErro:
    TB_InsereTuboBD = mediComboValorNull
    BG_LogFile_Erros "TB_InsereTuboBD: " & Err.Description, "TB", "TB_InsereTuboBD", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' APAGA TUBOS DA BASE DADOS

' ------------------------------------------------------------------------------------------------
Public Function TB_ApagaTuboBD(n_req As String, seq_req_tubo As Long) As Boolean
    Dim sSql As String
    TB_ApagaTuboBD = False

    'ssql = "DELETE FROM sl_Req_tubo WHERE n_Req = " & n_req & " AND seq_req_tubo = " & seq_req_tubo
    sSql = "UPDATE sl_req_tubo SET dt_eliminacao = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", user_eliminacao = " & gCodUtilizador & _
    ", hr_eliminacao = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", estado_tubo = " & gEstadosTubo.cancelado & " WHERE n_req=" & n_req & " AND seq_req_tubo = " & seq_req_tubo
    BG_ExecutaQuery_ADO sSql
    TB_ApagaTuboBD = True
Exit Function
TrataErro:
    TB_ApagaTuboBD = False
    BG_LogFile_Erros "TB_ApagaTuboBD: " & Err.Description, "TB", "TB_ApagaTuboBD", True
    Exit Function
    Resume Next
End Function


Function TB_DevolveEstadoTubo(cod_estado As Integer) As String
    TB_DevolveEstadoTubo = BL_SelCodigo("sl_tbf_estado_tubo", "DESCR_ESTADO", "COD_ESTADO", cod_estado)
End Function

Public Function TB_ExecutaColheita(ByVal codigo_etiqueta As String, ByVal requisicao As Long, ByVal codigo_tubo As String) As Boolean

    Dim sql As String
    Dim data_colheita As String
    Dim hora_colheita As String
    Dim user_colheita As String
    Dim rsAna As ADODB.recordset
    
    On Error GoTo TrataErro
    data_colheita = Bg_DaData_ADO
    hora_colheita = Bg_DaHora_ADO
    user_colheita = gCodUtilizador
    
    If (Len(CStr(codigo_etiqueta)) = 1) Then: codigo_etiqueta = "0" & codigo_etiqueta
    If (codigo_tubo <> Empty) Then
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.colhido & ", dt_colheita = " & BL_TrataDataParaBD(data_colheita) & ", hr_colheita = " & BL_TrataStringParaBD(hora_colheita) & ", user_colheita = " & BL_TrataStringParaBD(user_colheita) & ", local_chega = " & gCodLocal & " where n_req = " & requisicao & " and cod_tubo = " & BL_TrataStringParaBD(codigo_tubo) & " AND dt_chega IS NULL AND dt_eliminacao IS NULL "
    ElseIf (Mid(codigo_etiqueta, 1, 1) = "0") Then
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.colhido & ", dt_colheita = " & BL_TrataDataParaBD(data_colheita) & ", hr_colheita = " & BL_TrataStringParaBD(hora_colheita) & ", user_colheita = " & BL_TrataStringParaBD(user_colheita) & ", local_chega = " & gCodLocal & " where n_req = " & requisicao & " and cod_tubo in (" & BL_TrataStringParaBD(codigo_etiqueta) & "," & BL_TrataStringParaBD(Mid(codigo_etiqueta, 2, 1)) & " AND dt_chega IS NULL AND dt_eliminacao IS NULL "
    Else
        sql = "update sl_req_tubo set estado_tubo = " & gEstadosTubo.colhido & ", dt_colheita = " & BL_TrataDataParaBD(data_colheita) & ", hr_colheita = " & BL_TrataStringParaBD(hora_colheita) & ", user_colheita = " & BL_TrataStringParaBD(user_colheita) & ", local_chega = " & gCodLocal & " where n_req = " & requisicao & " and cod_tubo = " & BL_TrataStringParaBD(codigo_etiqueta) & " AND dt_chega IS NULL AND dt_eliminacao IS NULL "
    End If
    BG_ExecutaQuery_ADO sql
    
    If FE_AtualizaEstadoFilaEspera2(CStr(requisicao), gEstadoFilaEspera.concluido, "", "") = True Then
        If TB_RegistaHoraPrevistaColheita(requisicao) = True Then
            If FE_AtualizaProximaColheita(CStr(requisicao)) = True Then
            End If
        End If
        TB_ExecutaColheita = True
    
    End If
    
Exit Function
TrataErro:
    TB_ExecutaColheita = False
    BG_LogFile_Erros "Erro  ao dar colheita tubo : " & Err.Description, "TB", "TB_ExecutaColheita", True
    Exit Function
    Resume Next
End Function

Private Function TB_RegistaHoraPrevistaColheita(n_req As Long) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsT As New ADODB.recordset
    Dim rsC As New ADODB.recordset
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_req_tubo WHERE n_req = " & n_req & " AND (dt_chega IS NOT NULL OR dt_colheita IS NOT NULL)"
    rsT.CursorLocation = adUseServer
    rsT.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsT.Open sSql, gConexao
    If rsT.RecordCount = 1 Then
        sSql = "SELECT x1.seq_ReQ_tubo, x1.hr_previ_colheita, x3.minutos FROM SL_REQ_TUBO x1, sl_tubo x2, sl_tbf_tempos_colheita x3 "
        sSql = sSql & " WHERE x1.n_req = " & n_req & " AND x1.dt_colheita IS NULL AND "
        sSql = sSql & " x1.cod_tubo = x2.cod_tubo AND x2.cod_tempo_colheita = x3.cod_tempo_colheita AND x3.minutos > 0 "
        rsC.CursorLocation = adUseServer
        rsC.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsC.Open sSql, gConexao
        If rsC.RecordCount > 0 Then
            While Not rsC.EOF
                 For i = 1 To gTotalTubos
                    If gEstruturaTubos(i).seq_req_tubo = BL_HandleNull(rsC!seq_req_tubo, "") Then
                        gEstruturaTubos(i).hora_colheita = Format(DateAdd("n", BL_HandleNull(rsC!minutos, 0), Format(Bg_DaHora_ADO, "HH:MM")), "HH:MM")
                        sSql = "UPDATE sl_req_tubo SET hr_previ_colheita = " & BL_TrataStringParaBD(gEstruturaTubos(i).hora_colheita)
                        sSql = sSql & " WHERE seq_req_tubo = " & gEstruturaTubos(i).seq_req_tubo
                        BG_ExecutaQuery_ADO sSql
                        Exit For
                    End If
                Next i
                rsC.MoveNext
            Wend
            rsC.Close
            Set rsC = Nothing
        End If
    End If
    rsT.Close
    Set rsT = Nothing
    TB_RegistaHoraPrevistaColheita = True
Exit Function
TrataErro:
    TB_RegistaHoraPrevistaColheita = False
    BG_LogFile_Erros "Erro  ao registar hora prevista de colheita : " & Err.Description, "TB", "TB_RegistaHoraPrevistaColheita", True
    Exit Function
    Resume Next
End Function

Public Function TB_AnulaHoraPrevistaColheita(n_req As Long) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsT As New ADODB.recordset
    Dim rsC As New ADODB.recordset
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_req_tubo WHERE n_req = " & n_req & " AND dt_colheita IS NOT NULL"
    rsT.CursorLocation = adUseServer
    rsT.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsT.Open sSql, gConexao
    If rsT.RecordCount = 0 Then
        sSql = "UPDATE SL_REQ_TUBO set hr_previ_colheita = NULL WHERE n_req = " & n_req & " AND hr_previ_colheita IS NOT NULL"
        BG_ExecutaQuery_ADO sSql
    End If
    rsT.Close
    Set rsT = Nothing
    TB_AnulaHoraPrevistaColheita = True
Exit Function
TrataErro:
    TB_AnulaHoraPrevistaColheita = False
    BG_LogFile_Erros "Erro  ao desativar hora prevista de colheita : " & Err.Description, "TB", "TB_AnulaHoraPrevistaColheita", True
    Exit Function
    Resume Next
End Function
Public Function TB_EnviaMensagemPedidoNovaAmostra(n_req As String, cod_tubo As String, descr_motivo As String)
    Dim lMessageID As Long
    Dim mensagem As String
    Dim cmMessage As ClassModuleMessage
    Dim cc As New Collection
    Dim anexos As New Collection
    On Error GoTo TrataErro
    
    mensagem = "Foi feito um pedido de nova amostra para a requisi��o: " & n_req & " tubo: " & cod_tubo & vbCrLf
    mensagem = mensagem & " Motivo: " & descr_motivo
    
    lMessageID = BG_DaMAX("SL_mensagens", "id") + 1
    Set cmMessage = MESS_CreateMessage(lMessageID, "Pedido Nova Amostra - " & n_req, CLng(gCodUtilizador), TB_GetRecipientsTO, cc, ModuleMessages.MessageStateUnread, mensagem, anexos)
    If (Not MESS_InsertMessage(cmMessage, 0)) Then
        GoTo TrataErro
    End If
    If (Not MESS_InsertRecipients(cmMessage)) Then
        GoTo TrataErro
    End If
    If (Not MESS_InsertAttachments(cmMessage)) Then
        GoTo TrataErro
    End If
    
Exit Function
TrataErro:
    TB_EnviaMensagemPedidoNovaAmostra = False
    BG_LogFile_Erros "Erro  ao enviar mensagem pedido nova amostra : " & Err.Description, "TB", "TB_EnviaMensagemPedidoNovaAmostra", True
    Exit Function
    Resume Next
End Function

' Get collection of message recipients TO.
Private Function TB_GetRecipientsTO() As Collection
    
    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    Set TB_GetRecipientsTO = New Collection
    For Each vItem In Split(gUtilEnvioMensagemPedNovaAmostra, ";")
        If (Trim(vItem) <> Empty) Then: TB_GetRecipientsTO.Add vItem
    Next
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function TB_GetSeqReqTuboLocal(n_req As String) As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsT As New ADODB.recordset
    
    sSql = "SELECT max(seq_req_tubo_local)  maximo FROM sl_req_tubo WHERE n_req = " & n_req & " AND seq_req_tubo_local is not null"
    rsT.CursorLocation = adUseServer
    rsT.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsT.Open sSql, gConexao
    If rsT.RecordCount >= 1 Then
        TB_GetSeqReqTuboLocal = BL_HandleNull(rsT!maximo, 0) + 1
    End If
    rsT.Close
    Set rsT = Nothing
    
Exit Function
TrataErro:
    TB_GetSeqReqTuboLocal = False
    BG_LogFile_Erros "Erro  TB_GetSeqReqTuboLocal: " & Err.Description, "TB", "TB_GetSeqReqTuboLocal", True
    Exit Function
    Resume Next
End Function

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormTemposMaxAnalises 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8595
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   12600
   Icon            =   "FormTemposMaxAnalises.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   12600
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcSeqTempos 
      Height          =   285
      Left            =   2160
      TabIndex        =   22
      Top             =   7410
      Width           =   1455
   End
   Begin VB.Frame FrFooter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   120
      TabIndex        =   18
      Top             =   5400
      Width           =   10695
      Begin VB.Shape Shape2 
         BorderColor     =   &H8000000C&
         Height          =   495
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   10095
      End
      Begin VB.Label LaUtilCriacao 
         Height          =   255
         Left            =   2040
         TabIndex        =   21
         Top             =   120
         Width           =   2895
      End
      Begin VB.Label Label1 
         Caption         =   "Cria��o"
         Height          =   255
         Index           =   31
         Left            =   240
         TabIndex        =   20
         Top             =   120
         Width           =   615
      End
      Begin VB.Label LaDataCriacao 
         Height          =   255
         Left            =   5400
         TabIndex        =   19
         Top             =   120
         Width           =   2535
      End
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   255
      Left            =   840
      ScaleHeight     =   195
      ScaleWidth      =   435
      TabIndex        =   17
      Top             =   8160
      Width           =   495
   End
   Begin VB.Frame FrBody 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   3135
      Left            =   120
      TabIndex        =   14
      Top             =   2160
      Width           =   10695
      Begin MSComctlLib.ListView LvTempos 
         Height          =   3135
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   5530
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
   End
   Begin VB.Frame FrHeader 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1935
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   10695
      Begin VB.ComboBox CbSituacao 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   8040
         TabIndex        =   3
         Text            =   "CbSituacao"
         Top             =   120
         Width           =   1815
      End
      Begin VB.ListBox EcLocais 
         Appearance      =   0  'Flat
         Height          =   705
         Left            =   1080
         Style           =   1  'Checkbox
         TabIndex        =   4
         Top             =   840
         Width           =   4455
      End
      Begin VB.TextBox EcDescrAnalise 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   120
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaAnalise 
         Height          =   285
         Left            =   6360
         Picture         =   "FormTemposMaxAnalises.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Pesquisa requisi��es pendentes"
         Top             =   120
         Width           =   555
      End
      Begin VB.TextBox EcCodAnalise 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   120
         Width           =   1215
      End
      Begin VB.TextBox EcTMAXValidacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8040
         TabIndex        =   6
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox EcTMAXColheita 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8040
         TabIndex        =   5
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "min."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   9480
         TabIndex        =   16
         Top             =   840
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "min."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   9480
         TabIndex        =   15
         Top             =   1320
         Width           =   495
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H8000000C&
         Height          =   1095
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   720
         Width           =   10095
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "&Locais"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   13
         Top             =   840
         Width           =   540
      End
      Begin VB.Label Label1 
         Caption         =   "&Epis�dio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   7200
         TabIndex        =   12
         Top             =   120
         Width           =   855
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H8000000C&
         Height          =   615
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   10095
      End
      Begin VB.Label Label1 
         Caption         =   "&Tempo m�ximo Valida��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   5760
         TabIndex        =   11
         Top             =   1320
         Width           =   2415
      End
      Begin VB.Label Label1 
         Caption         =   "&Tempo m�ximo Colheita"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   5760
         TabIndex        =   10
         Top             =   840
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "&An�lise"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   9
         Top             =   120
         Width           =   735
      End
   End
   Begin MSComctlLib.ImageList ImageListIcons 
      Left            =   120
      Top             =   7800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   18
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":06F6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":0850
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":09AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":0DAA
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":11AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":1544
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":18DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":1C78
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":3952
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":3D14
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":440E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":AC70
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":114D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":1184B
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":180AD
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":1E90F
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":1ECCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormTemposMaxAnalises.frx":1F08A
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormTemposMaxAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2011.06.16   .
'     .       � 2011 Glintt-HS      .
'      .............................

Option Explicit

Private estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Private linhaActual As Long
Private CampoActivo As Object
Private CampoDeFocus As Object
Private lhwndListView As Long

Private Enum ListViewIcons
    
    e_IconCollapsed = 1
    e_IconExpanded = 2
    e_IconLeftGray = 3
    e_IconRightGray = 4
    e_IconRightYellow = 5
    e_IconCantDrop = 6
    e_IconRightBlue = 7
    e_IconForbidden = 8
    e_IconReturn = 9
    e_IconSearch = 10
    e_IconForbidden2 = 11
    e_IconOk = 12
    e_IconDown = 13
    e_IconIn = 14
    e_IconOut = 15
    e_IconGraphExpanded = 16
    e_IconGraphCollapsed = 17
    e_IconRight = 18
    
End Enum

Private Type Tempos
    
    Index As Long
    seq_tempos As Long
    cod_ana As String
    t_sit As Integer
    t_colheita As Long
    t_validacao As Long
    user_cri As Integer
    dt_cri As String
    cod_local As Integer
    
End Type

Private eTempos() As Tempos
Private TotalTempos As Long

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Tempos m�ximos de an�lises"
    Me.left = 540
    Me.top = 450
    Me.Width = 10455
    Me.Height = 6480 ' Normal
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    PreencheValoresDefeito
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    Set CampoDeFocus = EcCodAnalise
    lhwndListView = LvTempos.hwnd
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormTemposMaxAnalises = Nothing
    
End Sub

Private Sub LimpaCampos()
    Me.SetFocus
    LaUtilCriacao.caption = Empty
    LaDataCriacao.caption = Empty
    LvTempos.ListItems.Clear
    TotalTempos = Empty
    ReDim eTempos(TotalTempos)
    LimpaListaLocais
    CbSituacao.ListIndex = mediComboValorNull
    EcTMAXColheita.Text = Empty
    EcTMAXValidacao.Text = Empty
    EcCodAnalise.Text = Empty
    EcDescrAnalise.Text = Empty
    EcSeqTempos.Text = Empty
    EcCodAnalise.Enabled = True
    EcDescrAnalise.Enabled = True
    CbSituacao.Enabled = True
    EcLocais.Enabled = True

End Sub

Private Sub LimpaListaLocais()
    
    Dim i As Long
    
    On Error GoTo ErrorHandler
    For i = 0 To EcLocais.ListCount - 1
        If (EcLocais.ItemData(i) = gCodLocal) Then
            EcLocais.Selected(i) = True
        Else
            EcLocais.Selected(i) = False
        End If
    Next
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LimpaListaLocais' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Sub DefTipoCampos()
    
    DefineLista
    
End Sub

Sub PreencheCampos()
    
End Sub

Private Sub PreencheValoresDefeito()
    
    On Error GoTo ErrorHandler
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao, mediAscComboCodigo
    LimpaListaLocais
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheValoresDefeito' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
        BL_Toolbar_BotaoEstado "Inserir", "Activo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    End If

End Sub

Sub FuncaoEstadoAnterior()
   
End Sub

Public Sub FuncaoProcurar()
      
    Dim cod_ana As String
    Dim sql As String
    Dim Index As Long
    Dim rsTempos As ADODB.recordset

    On Error GoTo ErrorHandler
    If (Not ValidaPesquisa) Then: Exit Sub
    If (Not ConstroiCriterio(sql)) Then: Exit Sub
    
    BL_InicioProcessamento Me, " A procurar registos..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    
    Set rsTempos = New ADODB.recordset
    rsTempos.CursorLocation = adUseServer
    rsTempos.CursorType = adOpenStatic
    rsTempos.Open sql, gConexao
    
    If (rsTempos.RecordCount > Empty) Then
        While (Not rsTempos.EOF)
            If (cod_ana <> BL_HandleNull(rsTempos!cod_ana, Empty)) Then: Index = Empty: PreencheEstruturaTempos Index, BL_HandleNull(rsTempos!seq_tempos, Empty), BL_HandleNull(rsTempos!cod_ana, Empty)
            cod_ana = BL_HandleNull(rsTempos!cod_ana, Empty)
            Index = Index + 1
            PreencheEstruturaTempos Index, BL_HandleNull(rsTempos!seq_tempos, Empty), BL_HandleNull(rsTempos!cod_ana, Empty), _
                                           BL_HandleNull(rsTempos!t_sit, Empty), BL_HandleNull(rsTempos!t_colheita, Empty), _
                                           BL_HandleNull(rsTempos!t_validacao, Empty), BL_HandleNull(rsTempos!user_cri, Empty), _
                                           BL_HandleNull(rsTempos!dt_cri, Empty), BL_HandleNull(rsTempos!cod_local, Empty)
            rsTempos.MoveNext
        Wend
        
        GeraLV
        BL_Toolbar_BotaoEstado "Imprimir", "Activo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
        
    Else
        BG_Mensagem mediMsgBox, "N�o existem registos!", vbInformation, " Procura"
    
    End If
    
    If (rsTempos.state = adStateOpen) Then: rsTempos.Close
    Set rsTempos = Nothing
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoProcurar' in form FuncaoProcurar (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub
  
Sub FuncaoAnterior()
  
End Sub

Sub FuncaoSeguinte()
   
End Sub

Sub FuncaoInserir()
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        BD_Insert
        BL_FimProcessamento Me
    End If

End Sub

Private Sub BD_Insert()
    
    Dim i As Integer
    Dim sql As String
    
    On Error GoTo ErrorHandler
    If (EcLocais.SelCount > Empty) Then
        For i = 0 To EcLocais.ListCount - 1
            If (EcLocais.Selected(i)) Then
                sql = "insert into sl_ana_tempos_maximos (seq_tempos,cod_ana,t_sit,t_colheita,t_validacao,user_cri,dt_cri,cod_local) values (" & _
                      BG_DaMAX("sl_ana_tempos_maximos", "seq_tempos") + 1 & "," & BL_TrataStringParaBD(EcCodAnalise.Text) & "," & _
                      BG_DaComboSel(CbSituacao) & "," & EcTMAXColheita & "," & EcTMAXValidacao & "," & gCodUtilizador & "," & _
                      "sysdate" & "," & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sql
            If (gSQLError <> Empty) Then: Exit For
            End If
        Next
    End If
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Erro ao inserir dados", vbExclamation, " Inserir"
    If (gSQLError = Empty) Then: BG_Mensagem mediMsgBox, "Dados inseridos", vbInformation, " Inserir"
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'BD_Insert' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Sub FuncaoModificar()
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        BD_Update
        BL_FimProcessamento Me
    End If

End Sub

Private Function RegistoJaExiste(seq_tempos As Long) As Boolean
    
    Dim sql As String
    Dim rsTempos As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rsTempos = New ADODB.recordset
    rsTempos.CursorLocation = adUseServer
    rsTempos.CursorType = adOpenStatic
    sql = "select * from sl_ana_tempos_maximos where seq_tempos = " & seq_tempos
    rsTempos.Open sql, gConexao
    RegistoJaExiste = CBool(rsTempos.RecordCount > Empty)
    If (rsTempos.state = adStateOpen) Then: rsTempos.Close
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'RegistoJaExiste' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Public Sub BD_Update()

    Dim i As Integer
    Dim sql As String
    
    On Error GoTo ErrorHandler
    If (Not RegistoJaExiste(EcSeqTempos.Text)) Then
        sql = "insert into sl_ana_tempos_maximos (seq_tempos,cod_ana,t_sit,t_colheita,t_validacao,user_cri,dt_cri,cod_local) values (" & _
              BG_DaMAX("sl_ana_tempos_maximos", "seq_tempos") + 1 & "," & BL_TrataStringParaBD(EcCodAnalise.Text) & "," & _
              BG_DaComboSel(CbSituacao) & "," & EcTMAXColheita & "," & EcTMAXValidacao & "," & gCodUtilizador & "," & _
              "sysdate" & "," & EcLocais.ItemData(i) & ")"
        Else
            sql = "update sl_ana_tempos_maximos set t_colheita = " & EcTMAXColheita.Text & "," & _
                  "t_validacao = " & EcTMAXValidacao.Text & "," & " user_cri = " & gCodUtilizador & "," & _
                  "dt_cri = sysdate where  seq_tempos = " & EcSeqTempos.Text
    End If
    BG_ExecutaQuery_ADO sql
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Erro ao modificar dados", vbExclamation, " Modificar"
    If (gSQLError = Empty) Then: BG_Mensagem mediMsgBox, "Dados modificados", vbInformation, " Modificar"
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'BD_Update' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
        
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Private Sub BD_Delete()
    
    Dim i As Integer
    Dim cod_local As Integer
    Dim sql As String
    
    On Error GoTo ErrorHandler
    If (EcLocais.SelCount > Empty) Then
        For i = 0 To EcLocais.ListCount - 1
            If (EcLocais.Selected(i)) Then: cod_local = EcLocais.ItemData(i)
        Next
    End If
    
    sql = "delete from sl_ana_tempos_maximos where  seq_tempos = " & EcSeqTempos.Text & " and cod_local = " & cod_local
    BG_ExecutaQuery_ADO sql
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Erro ao eliminar dados", vbExclamation, " Eliminar"
    If (gSQLError = Empty) Then: BG_Mensagem mediMsgBox, "Dados eliminados", vbInformation, " Eliminar"
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'BD_Delete' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Private Sub DefineLista()

    On Error GoTo ErrorHandler
    With LvTempos
        .ColumnHeaders.Add(, , "An�lise", 2500, lvwColumnLeft).Key = "ANALISE"
        .ColumnHeaders.Add(, , "Situa��o", 1600, lvwColumnLeft).Key = "COL%NAO"
        .ColumnHeaders.Add(, , "Tempo colheita", 1500, lvwColumnLeft).Key = "COL%SIM"
        .ColumnHeaders.Add(, , "Tempo valida��o", 1500, lvwColumnLeft).Key = "VAL%SIM"
        .ColumnHeaders.Add(, , "Local", 2700, lvwColumnLeft).Key = "VAL%NAO"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .SmallIcons = ImageListIcons
        .FullRowSelect = True
        .Checkboxes = False
        .ForeColor = ApplicationHexadecimalColors.e_Gray
        .MultiSelect = False
    End With
    BG_SetListViewColor LvTempos, PictureListColor, ApplicationHexadecimalColors.e_LightGray, vbWhite
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'DefineLista' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next

End Sub

Private Function ValidaPesquisa() As Boolean
 
    On Error GoTo ErrorHandler
    If (EcLocais.SelCount = Empty) Then: BG_Mensagem mediMsgBox, "Obrigat�rio indicar local", vbExclamation, " Procura"
    ValidaPesquisa = True
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ValidaPesquisa' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next

End Function

Private Function ConstroiCriterio(ByRef sql As String) As Boolean

    Dim i As Integer
    
    On Error GoTo ErrorHandler
    sql = "select * from sl_ana_tempos_maximos where "
    
     If (EcLocais.SelCount > Empty) Then
        sql = sql & " cod_local in ("
        For i = 0 To EcLocais.ListCount - 1
            If (EcLocais.Selected(i)) Then: sql = sql & EcLocais.ItemData(i) & ","
        Next
        sql = Mid(sql, 1, Len(sql) - 1) & ")"
    End If
    If (EcCodAnalise <> Empty) Then: sql = sql & " and cod_ana = " & BL_TrataStringParaBD(EcCodAnalise)
    If (CbSituacao.ListIndex <> mediComboValorNull) Then: sql = sql & " and t_sit = " & BG_DaComboSel(CbSituacao)
    sql = sql & " order by cod_ana"
    ConstroiCriterio = True
    Exit Function

ErrorHandler:
    BG_LogFile_Erros "Error in function 'ConstroiCriterio' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next

End Function

Private Sub PreencheEstruturaTempos(Index As Long, seq_tempos As Long, cod_ana As String, Optional t_sit As Integer, _
                                    Optional t_colheita As Long, Optional t_validacao As Long, _
                                    Optional user_cri As Integer, Optional dt_cri As String, _
                                    Optional cod_local As Integer)
                                    
    On Error GoTo ErrorHandler
    TotalTempos = TotalTempos + 1
    ReDim Preserve eTempos(TotalTempos)
    eTempos(TotalTempos).Index = Index
    eTempos(TotalTempos).seq_tempos = seq_tempos
    eTempos(TotalTempos).cod_ana = cod_ana
    eTempos(TotalTempos).t_sit = t_sit
    eTempos(TotalTempos).t_colheita = t_colheita
    eTempos(TotalTempos).t_validacao = t_validacao
    eTempos(TotalTempos).user_cri = user_cri
    eTempos(TotalTempos).dt_cri = dt_cri
    eTempos(TotalTempos).cod_local = cod_local
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheEstruturaTempos' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

Private Function AdicionaItemLV(lItem As Long, lIndex As Long, Optional bParent As Boolean, Optional bAutomatic As Boolean, Optional bShowDirection As Boolean) As Boolean

    On Error GoTo ErrorHandler
    If (bParent) Then
        Dim descr_ana() As String
        descr_ana = Split(BL_DevolveDescricaoAnalise(eTempos(lItem).cod_ana, True), "_")
        If (Trim(descr_ana(0)) = Empty) Then
            descr_ana(0) = ""
        End If
        With LvTempos.ListItems.Add(lIndex, "KEY_" & eTempos(lItem).cod_ana & "_" & eTempos(lItem).Index & "_" & lItem, descr_ana(1), Empty, IIf(bParent, IIf(bAutomatic, ListViewIcons.e_IconExpanded, ListViewIcons.e_IconCollapsed), Empty))
            .ListSubItems.Add , , Empty
            .ListSubItems.Add , , Empty
            .ListSubItems.Add , , Empty
            .ListSubItems.Add , , Empty
        End With
        LvTempos.ListItems(lIndex).Bold = True
        LvTempos.ListItems(lIndex).ToolTipText = descr_ana(0)
    Else
        With LvTempos.ListItems.Add(lIndex, "KEY_" & eTempos(lItem).cod_ana & "_" & eTempos(lItem).Index & "_" & lItem, Empty, Empty, IIf(bParent, IIf(bAutomatic, ListViewIcons.e_IconExpanded, ListViewIcons.e_IconCollapsed), Empty))
            .ListSubItems.Add , , BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", eTempos(lItem).t_sit), ListViewIcons.e_IconRight
            .ListSubItems.Add , , eTempos(lItem).t_colheita & " minutos"
            .ListSubItems.Add , , eTempos(lItem).t_validacao & " minutos"
            .ListSubItems.Add , , BL_DevolveDescrLocal(CStr(eTempos(lItem).cod_local))
        End With
    End If
    DoEvents
    AdicionaItemLV = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'AdicionaItemLV' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next
    
End Function

Private Sub EcCodAnalise_Validate(Cancel As Boolean)

    Dim Tabela As ADODB.recordset
    Dim sql As String

    On Error GoTo ErrorHandler
    If (EcCodAnalise.Text <> Empty) Then
        EcCodAnalise.Text = UCase(EcCodAnalise.Text)
        Set Tabela = New ADODB.recordset

        sql = "select descr_ana from slv_analises where cod_ana = " & BL_TrataStringParaBD(EcCodAnalise.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao

        If (Tabela.RecordCount <= 0) Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodAnalise.Text = Empty
            EcDescrAnalise.Text = Empty
        Else
            EcDescrAnalise.Text = BL_HandleNull(Tabela!descr_ana, Empty)
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrAnalise.Text = Empty
    End If
 Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EcCodAnalise_Validate' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub LvTempos_ItemClick(ByVal item As MSComctlLib.ListItem)

    Dim bParent As Boolean
    Dim lStructure  As Long
    
    If (LvTempos.ListItems.Count = Empty) Then: Exit Sub
    lStructure = RetornaIndice(LvTempos.SelectedItem.Index, bParent)
    If (Not bParent) Then
        LaUtilCriacao.caption = BL_DevolveNomeUtilizador(CStr(eTempos(lStructure).user_cri), "", "")
        LaDataCriacao.caption = eTempos(lStructure).dt_cri
        EcCodAnalise.Text = eTempos(lStructure).cod_ana
        EcTMAXColheita.Text = eTempos(lStructure).t_colheita
        EcTMAXValidacao.Text = eTempos(lStructure).t_validacao
        BG_MostraComboSel eTempos(lStructure).t_sit, CbSituacao
        BG_MostraListaSel eTempos(lStructure).cod_local, EcLocais
        EcSeqTempos.Text = eTempos(lStructure).seq_tempos
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        EcCodAnalise.Enabled = False
        EcDescrAnalise.Enabled = False
        CbSituacao.Enabled = False
        EcLocais.Enabled = False
    Else
        LaUtilCriacao.caption = Empty
        LaDataCriacao.caption = Empty
        EcCodAnalise.Text = Empty
        EcTMAXColheita.Text = Empty
        EcTMAXValidacao.Text = Empty
        CbSituacao.ListIndex = mediComboValorNull
        EcSeqTempos.Text = Empty
        BL_Toolbar_BotaoEstado "Inserir", "Activo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        LimpaListaLocais
        EcCodAnalise.Enabled = True
        EcDescrAnalise.Enabled = True
        CbSituacao.Enabled = True
        EcLocais.Enabled = True
    End If

End Sub

Private Sub LvTempos_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Long
    
    On Error GoTo ErrorHandler
    linhaActual = 0
    For i = 1 To LvTempos.ListItems.Count
        If Y >= LvTempos.ListItems(i).top And Y < LvTempos.ListItems(i).top + LvTempos.ListItems(i).Height Then
            linhaActual = i
            Exit For
        End If
    Next
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvTempos_MouseMove' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub LvTempos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (linhaActual = Empty) Then: Exit Sub
    LvTempos.ListItems.item(linhaActual).Selected = True
    DoEvents
    Select Case (Button)
        Case MouseButtonConstants.vbLeftButton: Call BeforeHandleListViewTree(X, Y)
        Case Else: ' Empty
    End Select
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvTempos_MouseDown' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub BeforeHandleListViewTree(X As Single, Y As Single)
    
    Dim lvhti As LVHITTESTINFO
    
    On Error GoTo ErrorHandler
    lvhti.pt.X = X / Screen.TwipsPerPixelX
    lvhti.pt.Y = Y / Screen.TwipsPerPixelY
    If (ListView_ItemHitTest(lhwndListView, lvhti) = LVI_NOITEM) Then: Exit Sub
    If (lvhti.flags = LVHT_ONITEMICON) Then: Call HandleListViewTree(LvTempos.SelectedItem, Empty)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BeforeHandleListViewTree' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

Private Sub HandleListViewTree(ByVal item As MSComctlLib.ListItem, iManualKey As Variant)
   
    On Error GoTo ErrorHandler
    If (item.SmallIcon = ListViewIcons.e_IconCollapsed And BL_HandleNull(iManualKey, vbKeyRight) = vbKeyRight) Then: ExpandirLV item: Exit Sub
    If (item.SmallIcon = ListViewIcons.e_IconExpanded And BL_HandleNull(iManualKey, vbKeyLeft) = vbKeyLeft) Then: ComprimirLV item: Exit Sub
Exit Sub
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HandleListViewTree' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
End Sub

Private Sub ExpandirLV(ByVal item As MSComctlLib.ListItem)

    Dim lItem As Long
    Dim iShift As Integer
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim lStructure As Long
    
    On Error GoTo ErrorHandler
    lStructure = RetornaIndice(item.Index)
    Call LimitesEstrutura(eTempos(lStructure).cod_ana, lLowerBound, lUpperBound)
    iShift = item.Index + 1
    For lItem = lLowerBound To lUpperBound: Call AdicionaItemLV(lItem, CLng(iShift), , , True): iShift = iShift + 1: Next
    item.SmallIcon = ListViewIcons.e_IconExpanded

Exit Sub
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExpandirLV' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
End Sub


Private Sub ComprimirLV(ByVal item As MSComctlLib.ListItem)
   
    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim lStructureIndex As Long
    
    On Error GoTo ErrorHandler
    lStructureIndex = RetornaIndice(item.Index)
    Call LimitesEstrutura(eTempos(lStructureIndex).cod_ana, lLowerBound, lUpperBound)
    For lItem = lLowerBound To lUpperBound: Call RemoveItemLV(item): Next
    item.SmallIcon = ListViewIcons.e_IconCollapsed

Exit Sub
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ComprimirLV' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
End Sub

Private Sub RemoveItemLV(ByVal item As MSComctlLib.ListItem)
   
    On Error GoTo ErrorHandler
    LvTempos.ListItems.Remove item.Index + 1
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'RemoveItemLV' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Private Function GeraLV() As Boolean

    Dim lItem As Long
    Dim vSubItem As Variant
    Dim bAutomatic As Boolean
        
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eTempos)
        If (eTempos(lItem).Index = Empty And Not bAutomatic) Then: If (Not AdicionaItemLV(lItem, LvTempos.ListItems.Count + 1, True, bAutomatic, False)) Then Exit Function
        If (bAutomatic) Then: If (Not AdicionaItemLV(lItem, LvTempos.ListItems.Count + 1, CBool(eTempos(lItem).Index = Empty), bAutomatic, False)) Then Exit Function
    Next
    GeraLV = True
    Exit Function
    
ErrorHandler:
    GeraLV = False
    BG_LogFile_Erros "Error in function 'GeraLV' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next
    
End Function

Private Function RetornaIndice(lItem As Long, Optional ByRef bParent As Boolean, Optional cod_ana As String) As Integer
    
    Dim i As Integer

    On Error GoTo ErrorHandler
    If (lItem > 0) Then
        RetornaIndice = lItem
        RetornaIndice = CInt(Split(LvTempos.ListItems(lItem).Key, "_")(3))
        bParent = CBool(eTempos(RetornaIndice).Index = Empty)
    
    Else
        For i = 0 To TotalTempos
            If (eTempos(i).cod_ana = cod_ana And cod_ana <> Empty) Then: RetornaIndice = i
        Next
    End If
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'RetornaIndice' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next

End Function

Private Sub LimitesEstrutura(cod_ana As String, Optional ByRef lLowerBound As Long, Optional ByRef lUpperBound As Long)

    Dim lItem As Long
    
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eTempos)
       If (eTempos(lItem).cod_ana = cod_ana) Then
            If (eTempos(lItem).Index = Empty) Then: lLowerBound = lItem + 1: lUpperBound = lLowerBound
            If (eTempos(lItem).Index <> Empty) Then: lUpperBound = lItem
        End If
    Next
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LimitesEstrutura' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    Resume Next
    
End Sub

Private Sub BtPesquisaAnalise_Click()

       
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    On Error GoTo ErrorHandler
    
    If (Not EcCodAnalise.Enabled) Then: Exit Sub
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_ana"
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "slv_analises"
    CampoPesquisa1 = "descr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhuma an�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnalise.Text = resultados(1)
            EcDescrAnalise.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
     Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtPesquisaAnalise_Click' in form FormTemposMaxAnalises (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
End Sub

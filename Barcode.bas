Attribute VB_Name = "Barcode"
'=========================================
' DTKBarcode.BAS
' Copyright (c) 2004-2007, DTK Software.
' All rights reserved.
' http://www.dtksoft.com
'=========================================

'    Image formats
' --------------------------------
Public Const IMAGE_FORMAT_UNKNOWN = 0
Public Const IMAGE_FORMAT_BMP = 1
Public Const IMAGE_FORMAT_PNG = 2
Public Const IMAGE_FORMAT_JPG = 3
Public Const IMAGE_FORMAT_TIF = 4
Public Const IMAGE_FORMAT_PDF = 5

'    Barcode directions
' --------------------------------
Public Const BARCODE_DIR_UNKNOWN = 0
Public Const BARCODE_DIR_LEFT = 1
Public Const BARCODE_DIR_RIGHT = 2
Public Const BARCODE_DIR_UP = 3
Public Const BARCODE_DIR_DOWN = 4

'    Barcode Types
' --------------------------------
Public Const CODE11 = 1
Public Const CODE39 = 2
Public Const CODE93 = 3
Public Const CODE128 = 4
Public Const CODABAR = 5
Public Const INTER2OF5 = 6
Public Const EAN8 = 7
Public Const UPCE = 8
Public Const EAN13 = 9
Public Const UPCA = 10
Public Const Plus2 = 11
Public Const Plus5 = 12
Public Const PDF417 = 13
Public Const DATAMATRIX = 14


Declare Function ReadBarcode Lib "DTKBarcode.dll" ( _
                ByVal ImageFile As String) As Long
                
Declare Function ReadBarcodeBitmap Lib "DTKBarcode.dll" ( _
                ByVal hBmp As Long) As Long

Declare Function ReadBarcodeBuffer Lib "DTKBarcode.dll" ( _
                ByVal fileFata As Long, _
                ByVal bufferSize As Long, _
                ByVal imageFormat As Long) As Long
                
Declare Function ReadBarcodeDIB Lib "DTKBarcode.dll" ( _
                ByVal hDIB As Long) As Long
               
               
Declare Sub SetScanRect Lib "DTKBarcode.dll" ( _
                ByVal lpLeft As Long, _
                ByVal lpTop As Long, _
                ByVal lpRight As Long, _
                ByVal lpBottom As Long)
                
Declare Sub SetRegistrationCode Lib "DTKBarcode.dll" ( _
                ByVal RegCode As String)
                
                
' Properties
' --------------------------------

' Scan page
Declare Sub SetScanPage Lib "DTKBarcode.dll" (ByVal nScanInterval As Long)
Declare Function GetScanPage Lib "DTKBarcode.dll" () As Long

' Scan interval
Declare Sub SetScanInterval Lib "DTKBarcode.dll" (ByVal nScanInterval As Long)
Declare Function GetScanInterval Lib "DTKBarcode.dll" () As Long

' Color threshold level
Declare Sub SetThresholdLevel Lib "DTKBarcode.dll" (ByVal nThresholdLevel As Long)
Declare Function GetThresholdLevel Lib "DTKBarcode.dll" () As Long
            
' Barcodes to read
Declare Sub SetBarcodesToRead Lib "DTKBarcode.dll" (ByVal nScanInterval As Long)
Declare Function GetBarcodesToRead Lib "DTKBarcode.dll" () As Long

' Scan direction
Declare Sub SetScanRight Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetScanRight Lib "DTKBarcode.dll" () As Byte

Declare Sub SetScanLeft Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetScanLeft Lib "DTKBarcode.dll" () As Byte

Declare Sub SetScanUp Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetScanUp Lib "DTKBarcode.dll" () As Byte

Declare Sub SetScanDown Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetScanDown Lib "DTKBarcode.dll" () As Byte

' Barcode types
Declare Sub SetReadCode11 Lib "DTKBarcode.dll" (bSet As Long)
Declare Function GetReadCode11 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadCode39 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadCode39 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadCode93 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadCode93 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadCode128 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadCode128 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadCodabar Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadCodabar Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadInter2of5 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadInter2of5 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadEAN13 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadEAN13 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadEAN8 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadEAN8 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadUPCA Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadUPCA Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadUPCE Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadUPCE Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadAdd2 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadAdd2 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadAdd5 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadAdd5 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadPDF417 Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadPDF417 Lib "DTKBarcode.dll" () As Byte

Declare Sub SetReadDataMatrix Lib "DTKBarcode.dll" (ByVal bSet As Long)
Declare Function GetReadDataMatrix Lib "DTKBarcode.dll" () As Byte

Declare Function GetNumBarcodes Lib "DTKBarcode.dll" () As Long

Declare Function GetBarcodeString Lib "DTKBarcode.dll" ( _
            ByVal nBarcode As Long, _
            ByVal sBarcode As String, _
            ByVal bufSize As Long) As Long
            
Declare Function GetBarcodeType Lib "DTKBarcode.dll" ( _
            ByVal nBarcode As Long) As Long

Declare Function GetBarcodeDirection Lib "DTKBarcode.dll" ( _
            ByVal nBarcode As Long) As Long
            
Declare Sub GetBarcodeBoundStart Lib "DTKBarcode.dll" ( _
            ByVal nBarcode As Long, _
            ByVal lpX1 As Long, _
            ByVal lpY1 As Long, _
            ByVal lpX2 As Long, _
            ByVal lpY2 As Long)

Declare Sub GetBarcodeBoundEnd Lib "DTKBarcode.dll" ( _
            ByVal nBarcode As Long, _
            ByVal lpX1 As Long, _
            ByVal lpY1 As Long, _
            ByVal lpX2 As Long, _
            ByVal lpY2 As Long)
            
Declare Function GetBarcodePage Lib "DTKBarcode.dll" ( _
            ByVal nBarcode As Long) As Long
            




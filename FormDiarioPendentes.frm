VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormDiarioPendentes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormDiarioPendentes"
   ClientHeight    =   9045
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15150
   Icon            =   "FormDiarioPendentes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9045
   ScaleWidth      =   15150
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtActualizar 
      Height          =   615
      Left            =   14280
      Picture         =   "FormDiarioPendentes.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Actualizar"
      Top             =   8400
      Width           =   735
   End
   Begin TabDlg.SSTab SSTabFiltros 
      Height          =   2175
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   3836
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      Tab             =   1
      TabsPerRow      =   8
      TabHeight       =   520
      TabCaption(0)   =   "Grupo de An�lises"
      TabPicture(0)   =   "FormDiarioPendentes.frx":0CD6
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "BtPesquisaRapidaGrAnalises"
      Tab(0).Control(1)=   "EcListaGrAna"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Proveni�ncias"
      TabPicture(1)   =   "FormDiarioPendentes.frx":0CF2
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "BtPesquisaProveniencia"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "EcListaProven"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Produtos"
      TabPicture(2)   =   "FormDiarioPendentes.frx":0D0E
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "EcListaProd"
      Tab(2).Control(1)=   "BtPesquisaProdutos"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "An�lises"
      TabPicture(3)   =   "FormDiarioPendentes.frx":0D2A
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "BtPesquisaAnalises"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "EcListaAnalises"
      Tab(3).ControlCount=   2
      Begin VB.CommandButton BtPesquisaAnalises 
         Height          =   315
         Left            =   -60840
         Picture         =   "FormDiarioPendentes.frx":0D46
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de An�lises"
         Top             =   480
         Width           =   375
      End
      Begin VB.ListBox EcListaAnalises 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   -74880
         TabIndex        =   8
         Top             =   480
         Width           =   14055
      End
      Begin VB.ListBox EcListaProd 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   -74880
         TabIndex        =   7
         Top             =   480
         Width           =   14055
      End
      Begin VB.CommandButton BtPesquisaProdutos 
         Height          =   315
         Left            =   -60840
         Picture         =   "FormDiarioPendentes.frx":12D0
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos"
         Top             =   480
         Width           =   375
      End
      Begin VB.ListBox EcListaProven 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   0
         TabIndex        =   5
         Top             =   480
         Width           =   14295
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   14280
         Picture         =   "FormDiarioPendentes.frx":185A
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
         Height          =   375
         Left            =   -60720
         Picture         =   "FormDiarioPendentes.frx":1DE4
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
         Top             =   480
         Width           =   375
      End
      Begin VB.ListBox EcListaGrAna 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   -74880
         TabIndex        =   2
         Top             =   480
         Width           =   14055
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FGReqPendRes 
      Height          =   5745
      Left            =   120
      TabIndex        =   0
      Top             =   2640
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   10134
      _Version        =   393216
      Rows            =   22
      Cols            =   10
      FixedRows       =   0
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin MSComCtl2.DTPicker EcDtIni 
      Height          =   255
      Left            =   720
      TabIndex        =   10
      Top             =   2280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   450
      _Version        =   393216
      Format          =   147193857
      CurrentDate     =   39573
   End
   Begin MSComCtl2.DTPicker EcDtFim 
      Height          =   255
      Left            =   4440
      TabIndex        =   11
      Top             =   2280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   450
      _Version        =   393216
      Format          =   147193857
      CurrentDate     =   39573
   End
   Begin VB.Label Label10 
      Caption         =   "Dt. Final"
      Height          =   255
      Index           =   1
      Left            =   3720
      TabIndex        =   13
      Top             =   2280
      Width           =   855
   End
   Begin VB.Label Label10 
      Caption         =   "Dt. Inicial"
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   12
      Top             =   2325
      Width           =   855
   End
End
Attribute VB_Name = "FormDiarioPendentes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Vari�veis Globais para este Forr.
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Const lColRequis = 0
Const lColDiario = 1
Const lColData = 2
Const lColExame = 3
Const lColAnalise = 4

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Const vermelhoClaro = &H8080FF
Const Amarelo = &H80FFFF
Const Branco = &HFFFFFF
Const cinzento = &HC0C0C0              '&HE0E0E0
Const Verde = &HC0FFC0
Const azul = &HFEA381                          '&HFF8080
Const laranja = &H80FF&
Const vermelho = &HFF&
Const AzulClaro = &HFF8080

Const colunas = 10
Const colunasMeios = 6
Dim linhaActual As Long
Dim tempo As Long
Public rs As ADODB.recordset
Dim estrutDiario() As tDiario
Dim totalDiario As Integer

Dim linhas() As Integer

Private Sub BtActualizar_Click()
    FuncaoProcurar
    
End Sub

Private Sub BtPesquisaAnalises_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "slv_analises "
    'CWhere = "  (flg_invisivel is null or flg_invisivel = 0) "
    CampoPesquisa = "descr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaAnalises.ListCount = 0 Then
                EcListaAnalises.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                EcListaAnalises.ItemData(0) = resultados(i)
            Else
                EcListaAnalises.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                EcListaAnalises.ItemData(EcListaAnalises.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub
Private Sub BtPesquisaProdutos_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto"
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProd.ListCount = 0 Then
                EcListaProd.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                EcListaProd.ItemData(0) = resultados(i)
            Else
                EcListaProd.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                EcListaProd.ItemData(EcListaProd.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub FGReqPendRes_DblClick()
    If FGReqPendRes.row <= UBound(linhas) Then
        FormDiario.Show
        FormDiario.InicializaDiario estrutDiario(linhas(FGReqPendRes.row)).n_req, estrutDiario(linhas(FGReqPendRes.row)).cod_agrup, CLng(estrutDiario(linhas(FGReqPendRes.row)).seq_utente)
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    DefTipoCampos

    BG_ParametrizaPermissoes_ADO Me.Name
    estado = 1
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    gF_DIARIOPEND = 1
End Sub

Sub Inicializacoes()

    Me.caption = " Di�rio - Gest�o de Actividades Pendentes"
    Me.left = 5
    Me.top = 30
    Me.Width = 15240  ' 12235
    Me.Height = 9465  ' Normal

End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    gF_DIARIOPEND = 1
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    gF_DIARIOPEND = 0
    Set FormDiarioPendentes = Nothing
    
End Sub

Sub LimpaCampos()
    ReDim linhas(0)
    totalDiario = 0
    ReDim estrutDiario(0)
    LimpaFGReq
    SSTabFiltros.Tab = 0
End Sub

Sub DefTipoCampos()
    Dim i As Integer
    SSTabFiltros.Tab = 0
    EcDtIni.value = CDate(Bg_DaData_ADO - tempo)
    EcDtFim.value = Bg_DaData_ADO
    ReDim linhas(0)
    totalDiario = 0
    ReDim estrutDiario(0)
    With FGReqPendRes
        .rows = 23
        .FixedRows = 0
        .Cols = colunas
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(lColRequis) = 1000
        .Col = lColRequis
        .CellAlignment = flexAlignCenterCenter
        .MergeCol(lColRequis) = True
        .ColWidth(lColDiario) = 1000
        .Col = lColDiario
        .CellAlignment = flexAlignCenterCenter
        .MergeCol(lColDiario) = True
        .ColWidth(lColExame) = 5000
        .Col = lColExame
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColData) = 1500
        .Col = lColData
        .CellAlignment = flexAlignCenterCenter
        For i = lColAnalise To colunas - 1
            .ColWidth(i) = 990
            .Col = i
            .CellAlignment = flexAlignCenterCenter
        Next
        .row = 1
        .Col = 0
        
    End With
    
    
    If gCodGrAnaUtilizador <> "" And BL_SeleccionaGrupoAnalises = True Then
        Dim rsGrAna As ADODB.recordset
        Dim sql As String
        sql = "SELECT seq_gr_ana, cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana=" & BL_TrataStringParaBD(gCodGrAnaUtilizador)
        Set rsGrAna = New ADODB.recordset
        rsGrAna.CursorLocation = adUseServer
        rsGrAna.CursorType = adOpenStatic
        rsGrAna.Open sql, gConexao
        If rsGrAna.RecordCount > 0 Then
            While Not rsGrAna.EOF
                EcListaGrAna.AddItem BL_HandleNull(rsGrAna!descr_gr_ana, "")
                EcListaGrAna.ItemData(EcListaGrAna.NewIndex) = BL_HandleNull(rsGrAna!seq_gr_ana, -1)
                rsGrAna.MoveNext
            Wend
        End If
        rsGrAna.Close
        Set rsGrAna = Nothing
        
    End If
    
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        Me.caption = " Requisi��es Pendentes Por An�lise"
        LimpaCampos
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
    
End Sub

Sub PreencheValoresDefeito()
    ' nada
End Sub

Sub FuncaoProcurar()
    On Error GoTo TrataErro
    Me.Enabled = False
    LimpaFGReq
    linhaActual = 0
    ReDim linhas(0)
    
    If DIA_PreencheEstrutDiarioPendentes(estrutDiario, totalDiario, gPermResUtil, ConstroiSQL) = False Then
        GoTo TrataErro
    End If
    PreencheCampos
    Me.Enabled = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub
Private Sub PreencheCampos()
    On Error GoTo TrataErro
    Dim linhaActual As Integer
    Dim colunaActual As Integer
    Dim i As Integer
    Dim j As Integer
    
    linhaActual = 0
    If totalDiario > 0 Then
        For i = 1 To totalDiario
            
            ReDim Preserve linhas(linhaActual)
            linhas(linhaActual) = i
            FGReqPendRes.TextMatrix(linhaActual, lColRequis) = estrutDiario(i).n_req
            FGReqPendRes.TextMatrix(linhaActual, lColDiario) = estrutDiario(i).seq_diario
            FGReqPendRes.TextMatrix(linhaActual, lColData) = estrutDiario(i).dt_chega
            FGReqPendRes.TextMatrix(linhaActual, lColExame) = estrutDiario(i).descr_ana
            colunaActual = colunas - colunasMeios
            For j = 1 To estrutDiario(i).totalMeios
                If colunaActual > FGReqPendRes.Cols - 1 Then
                    colunaActual = colunas - colunasMeios
                    'SE CHEGOU LIMITE ACRESCENTA LINHA
                    If linhaActual = FGReqPendRes.rows - 1 Then
                        FGReqPendRes.AddItem ""
                    End If
                    linhaActual = linhaActual + 1
                    ReDim Preserve linhas(linhaActual)
                    linhas(linhaActual) = i
                    FGReqPendRes.TextMatrix(linhaActual, lColRequis) = estrutDiario(i).n_req
                    FGReqPendRes.TextMatrix(linhaActual, lColDiario) = estrutDiario(i).seq_diario
                    FGReqPendRes.TextMatrix(linhaActual, lColData) = estrutDiario(i).dt_chega
                    FGReqPendRes.TextMatrix(linhaActual, lColExame) = estrutDiario(i).descr_ana
                End If
                FGReqPendRes.row = linhaActual
                FGReqPendRes.Col = colunaActual
                FGReqPendRes.TextMatrix(linhaActual, colunaActual) = estrutDiario(i).meios(j).descr_meio
                If estrutDiario(i).meios(j).flg_estado_final = mediSim And gPermResUtil = cValMedRes Then
                    FGReqPendRes.CellBackColor = Verde
                ElseIf estrutDiario(i).meios(j).flg_estado_final_tec = mediSim And gPermResUtil = cValTecRes Then
                    FGReqPendRes.CellBackColor = Verde
                Else
                    FGReqPendRes.CellBackColor = Amarelo
                End If
                colunaActual = colunaActual + 1
            Next j
            'SE CHEGOU LIMITE ACRESCENTA LINHA
            If linhaActual = FGReqPendRes.rows - 1 Then
                FGReqPendRes.AddItem ""
            End If
            linhaActual = linhaActual + 1
        Next
    End If
    DoEvents
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCampos: " & Err.Description, Me.Name, "PreencheCampos", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaProveniencia_Click()
    PesquisaProven
End Sub

Private Sub BtPesquisaRapidaGrAnalises_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_ana"
    CWhere = ""
    CampoPesquisa = "descr_gr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_ana ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(resultados(i) <> "") Then
                If EcListaGrAna.ListCount = 0 Then
                    EcListaGrAna.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                    EcListaGrAna.ItemData(0) = resultados(i)
                Else
                    EcListaGrAna.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                    EcListaGrAna.ItemData(EcListaGrAna.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If
End Sub

Private Sub EclistaGrAna_keydown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    If KeyCode = 46 And EcListaGrAna.ListCount > 0 Then     'Delete
        i = EcListaGrAna.ListIndex
        If EcListaGrAna.ListIndex > mediComboValorNull Then
            EcListaGrAna.RemoveItem (EcListaGrAna.ListIndex)
        End If
        If i < EcListaGrAna.ListCount - 1 Then
            EcListaGrAna.ListIndex = i
        End If
    End If
End Sub
Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaProven.ListCount > 0 Then     'Delete
        If EcListaProven.ListIndex > mediComboValorNull Then
            EcListaProven.RemoveItem (EcListaProven.ListIndex)
        End If
    End If
End Sub

Private Sub EclistaProd_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaProd.ListCount > 0 Then     'Delete
        If EcListaProd.ListIndex > mediComboValorNull Then
            EcListaProd.RemoveItem (EcListaProd.ListIndex)
        End If
    End If
End Sub
Private Sub PesquisaProven()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(0) = resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub
Private Sub Eclistaanalises_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaAnalises.ListCount > 0 Then     'Delete
        If EcListaAnalises.ListIndex > mediComboValorNull Then
            EcListaAnalises.RemoveItem (EcListaAnalises.ListIndex)
        End If
    End If
End Sub

Private Sub LimpaFGReq()
    FGReqPendRes.Clear
    FGReqPendRes.Cols = colunas
    FGReqPendRes.rows = 23
    FGReqPendRes.row = 0
    linhaActual = 0
End Sub



Private Function ConstroiSQL() As String
    Dim i As Integer
    ConstroiSQL = ""

    ' ------------------------------------
    ' SE  FOI INDICADO ALGUMGRUPO ANALISES
    ' ------------------------------------
    If EcListaGrAna.ListCount > 0 Then
        ConstroiSQL = ConstroiSQL & " AND x2.gr_ana = x9.cod_gr_ana AND x9.seq_gr_ana in ( "
        For i = 0 To EcListaGrAna.ListCount - 1
            ConstroiSQL = ConstroiSQL & EcListaGrAna.ItemData(i) & ", "
        Next i
        ConstroiSQL = Mid(ConstroiSQL, 1, Len(ConstroiSQL) - 2) & ") "
    End If
    
    
    If EcDtIni.value <> "" And EcDtFim.value <> "" Then
        ConstroiSQL = ConstroiSQL & " AND x5.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    End If
        
    
    If EcListaProven.ListCount > 0 Then
        ' -----------------------------------
        ' SE FOI INDICADO ALGUMA PROVINIENCIA
        ' -----------------------------------
        ConstroiSQL = ConstroiSQL & " AND  x6.seq_proven in ( "
        For i = 0 To EcListaProven.ListCount - 1
            ConstroiSQL = ConstroiSQL & EcListaProven.ItemData(i) & ", "
        Next i
        ConstroiSQL = Mid(ConstroiSQL, 1, Len(ConstroiSQL) - 2) & ") "
        
    End If
    
    If EcListaProd.ListCount > 0 Then                  'SE FOI INDICADO ALGUM PRODUTO
        ConstroiSQL = ConstroiSQL & "  AND x4.seq_produto in ( "
        For i = 0 To EcListaProd.ListCount - 1
            ConstroiSQL = ConstroiSQL & EcListaProd.ItemData(i) & ", "
        Next i
        ConstroiSQL = Mid(ConstroiSQL, 1, Len(ConstroiSQL) - 2) & ") "
    End If
        
    If EcListaAnalises.ListCount > 0 Then                  'SE FOI INDICADO ALGUMA ANALISE
        ConstroiSQL = ConstroiSQL & " AND x2.seq_ana in ( "
        For i = 0 To EcListaAnalises.ListCount - 1
            ConstroiSQL = ConstroiSQL & EcListaAnalises.ItemData(i) & ", "
        Next i
        ConstroiSQL = Mid(ConstroiSQL, 1, Len(ConstroiSQL) - 2) & ") "
                
    End If

End Function





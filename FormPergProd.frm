VERSION 5.00
Begin VB.Form FormPergProd 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormPergProd"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6375
   Icon            =   "FormPergProd.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   6375
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrDefeito 
      Height          =   4455
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6375
      Begin VB.ListBox ListaP 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2220
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   0
         Top             =   840
         Width           =   6135
      End
      Begin VB.CheckBox CkUltima 
         Caption         =   "N�o &perguntar de novo."
         Height          =   255
         Left            =   240
         TabIndex        =   3
         ToolTipText     =   $"FormPergProd.frx":000C
         Top             =   4050
         Value           =   1  'Checked
         Width           =   2895
      End
      Begin VB.CommandButton BtNao 
         Caption         =   "&N�o Marcar"
         Height          =   375
         Left            =   3360
         TabIndex        =   2
         Top             =   3360
         Width           =   1455
      End
      Begin VB.PictureBox PicProd 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   480
         Left            =   120
         Picture         =   "FormPergProd.frx":00A2
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   240
         Width           =   480
      End
      Begin VB.CommandButton BtSim 
         Caption         =   "&Marcar Produtos"
         Height          =   375
         Left            =   1440
         TabIndex        =   1
         Top             =   3360
         Width           =   1455
      End
      Begin VB.Line Line1 
         BorderColor     =   &H8000000C&
         X1              =   45
         X2              =   6360
         Y1              =   3945
         Y2              =   3945
      End
      Begin VB.Label LblInfo 
         Caption         =   "()"
         Height          =   375
         Left            =   720
         TabIndex        =   5
         Top             =   240
         Width           =   5415
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000009&
         BorderWidth     =   2
         X1              =   45
         X2              =   6360
         Y1              =   3960
         Y2              =   3960
      End
   End
End
Attribute VB_Name = "FormPergProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

'Defini��es necess�rias para a Rotina PerguntaProduto
Public Enum TipoPergunta
    ConfirmarDefeito = 0
    ConfirmarDtChega = 1
End Enum


Dim gTexto() As String
Dim gLista() As String
Dim gEscolha As Integer
Dim gUltima As Boolean

Function PerguntaProduto(Tipo As TipoPergunta, UltimaVez As Boolean, descrAna As String, Texto() As String, ByRef EscolhaL() As String) As Integer
    Dim i As Integer
    Dim k As Integer
    Dim Txt() As String
    Dim Txt1() As String
    
    
    ReDim gTexto(UBound(Texto))
    For i = 1 To UBound(Texto)
        gTexto(i) = Texto(i)
    Next i
    gEscolha = -1
    FormPergProd.Width = 6375
    FormPergProd.Height = 4875
    FormPergProd.Caption = descrAna
    
    If Tipo = TipoPergunta.ConfirmarDefeito Then
        CkUltima.Value = UltimaVez
        LblInfo.Caption = "ATEN��O: N�o � possivel registar a(s) seguinte(s) an�lise(s) sem marcar o seu produto respectivo :"
        
        For i = 1 To UBound(Texto)
            If Trim(Texto(i)) <> "" Then
                Txt = Split(Texto(i), ";")
                ListaP.AddItem Txt(2)
                ListaP.Selected(ListaP.ListCount - 1) = True
            End If
        Next i
        
        BtSim.Caption = "&Marcar Produtos"
        BtNao.Caption = "&N�o Marcar"
        ListaP.ToolTipText = "Pode escolher quais os produtos a marcar (ir� condicionar a marca��o das an�lises)."
        BtSim.ToolTipText = "Marcar os produtos seleccionados."
        BtNao.ToolTipText = "N�o marcar os produtos, e por conseguinte n�o marcar as an�lises associadas."
        
        If gIgnoraPerguntaProdutoTubo <> 1 Then
            FormPergProd.Show vbModal
        Else
            gEscolha = 0
            Unload Me
        End If
    
        UltimaVez = gUltima
        PerguntaProduto = gEscolha
        
        If Tipo = TipoPergunta.ConfirmarDefeito Then
            ReDim EscolhaL(UBound(gLista))
            For i = 1 To UBound(gLista)
                EscolhaL(i) = gLista(i)
            Next i
        End If
            
        Set FormPergProd = Nothing

    ElseIf Tipo = TipoPergunta.ConfirmarDtChega Then
    
        If FormGestaoRequisicao.EcDataChegada.text <> "" And gLAB <> "HFM" And gTipoInstituicao = "HOSPITALAR" Then
            LblInfo.Caption = " Deseja efectuar a chegada do(s) seguinte(s) produto(s) com a data de hoje (" & Bg_DaData_ADO & ") ?"
    
            For i = 1 To UBound(Texto)
                For k = 1 To UBound(EscolhaL)
                    If InStr(1, Texto(i), EscolhaL(k)) <> 0 Then
                        If Trim(Texto(i)) <> "" Then
                            Txt = Split(Texto(i), ";")
                            Txt1 = Split(Txt(2), "-")
                            ListaP.AddItem Txt1(0)
                            ListaP.Selected(ListaP.ListCount - 1) = True
                        End If
                    End If
                Next k
            Next i
    
            BtSim.Caption = "&Sim"
            BtNao.Caption = "&N�o"
            ListaP.ToolTipText = "Pode escolher quais os produtos quer dar chegada."
            BtSim.ToolTipText = "Colocar a data de hoje na data de chegada dos produtos."
            BtNao.ToolTipText = "N�o efectuar a chegada dos produtos."
            
            FormPergProd.Show vbModal
        
            UltimaVez = gUltima
            PerguntaProduto = gEscolha
            
            If Tipo = TipoPergunta.ConfirmarDefeito Then
                ReDim EscolhaL(UBound(gLista))
                For i = 1 To UBound(gLista)
                    EscolhaL(i) = gLista(i)
                Next i
            End If
                
            Set FormPergProd = Nothing
        ElseIf gLAB <> "HFM" And gTipoInstituicao = "PRIVADA" Then
            LblInfo.Caption = " Deseja efectuar a chegada do(s) seguinte(s) produto(s) com a data de hoje (" & Bg_DaData_ADO & ") ?"
    
            For i = 1 To UBound(Texto)
                For k = 1 To UBound(EscolhaL)
                    If InStr(1, Texto(i), EscolhaL(k)) <> 0 Then
                        If Trim(Texto(i)) <> "" Then
                            Txt = Split(Texto(i), ";")
                            Txt1 = Split(Txt(2), "-")
                            ListaP.AddItem Txt1(0)
                            ListaP.Selected(ListaP.ListCount - 1) = True
                        End If
                    End If
                Next k
            Next i
    
            BtSim.Caption = "&Sim"
            BtNao.Caption = "&N�o"
            ListaP.ToolTipText = "Pode escolher quais os produtos quer dar chegada."
            BtSim.ToolTipText = "Colocar a data de hoje na data de chegada dos produtos."
            BtNao.ToolTipText = "N�o efectuar a chegada dos produtos."
            
            FormPergProd.Show vbModal
        
            UltimaVez = gUltima
            PerguntaProduto = gEscolha
            
            If Tipo = TipoPergunta.ConfirmarDefeito Then
                ReDim EscolhaL(UBound(gLista))
                For i = 1 To UBound(gLista)
                    EscolhaL(i) = gLista(i)
                Next i
            End If
                
            Set FormPergProd = Nothing
        Else
'            For i = 0 To ListaP.ListCount - 1
'                ListaP.Selected(i) = False
'            Next i
            
            UltimaVez = gUltima
            PerguntaProduto = -1
            
        End If
    End If


    
End Function


Private Sub BtNao_Click()
    Dim i As Integer
    
    For i = 0 To ListaP.ListCount - 1
        ListaP.Selected(i) = False
    Next i
    gEscolha = -1
    Unload Me
End Sub



Private Sub BtSim_Click()
    gEscolha = 0
    Unload Me
End Sub



Private Sub CkUltima_Click()
    If CkUltima.Value = 0 Then
        gUltima = False
    Else
        gUltima = True
    End If
End Sub


Private Sub Form_Activate()
    BtSim.SetFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim k As Integer
    Dim i As Integer
    Dim Txt() As String
    
    ReDim gLista(0)
    k = 0
    For i = 0 To ListaP.ListCount - 1
        If ListaP.Selected(i) = True Then
            k = k + 1
            ReDim Preserve gLista(k)
            Txt = Split(gTexto(i + 1), ";")
            gLista(k) = Txt(0) & ";" & Txt(1)
        End If
    Next i

    If gF_REQUIS = 1 Then
        FormGestaoRequisicao.Enabled = True
    End If
    If gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.Enabled = True
    End If
End Sub







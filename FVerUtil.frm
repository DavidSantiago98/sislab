VERSION 5.00
Begin VB.Form FormVerificarUtilizador 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   Caption         =   "CaptionVerificarUtilizador"
   ClientHeight    =   6780
   ClientLeft      =   2040
   ClientTop       =   2235
   ClientWidth     =   14520
   Icon            =   "FVerUtil.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MousePointer    =   1  'Arrow
   PaletteMode     =   1  'UseZOrder
   Picture         =   "FVerUtil.frx":000C
   ScaleHeight     =   6780
   ScaleWidth      =   14520
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton ButtonOK 
      Default         =   -1  'True
      Height          =   375
      Left            =   1920
      Picture         =   "FVerUtil.frx":5BF7
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   3000
      Width           =   1095
   End
   Begin VB.CommandButton ButtonCancelar 
      Height          =   375
      Left            =   3120
      Picture         =   "FVerUtil.frx":6272
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   3000
      Width           =   1095
   End
   Begin VB.CommandButton BtOpcoes 
      Height          =   375
      Left            =   4320
      Picture         =   "FVerUtil.frx":6908
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   3000
      Width           =   1095
   End
   Begin VB.PictureBox EcImagem 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   5070
      Left            =   13080
      Picture         =   "FVerUtil.frx":6F25
      ScaleHeight     =   5010
      ScaleWidth      =   7515
      TabIndex        =   7
      Top             =   0
      Visible         =   0   'False
      Width           =   7575
   End
   Begin VB.ComboBox EcLocal 
      Height          =   315
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   4680
      Width           =   3495
   End
   Begin VB.TextBox EcSenha 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1920
      TabIndex        =   3
      Top             =   2760
      Visible         =   0   'False
      Width           =   3495
   End
   Begin VB.TextBox EcSenhaAux 
      Appearance      =   0  'Flat
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1920
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   2520
      Width           =   3495
   End
   Begin VB.TextBox EcUtilizador 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1920
      TabIndex        =   0
      Top             =   2040
      Width           =   3495
   End
   Begin VB.Label LaLocal 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Local"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   6
      Top             =   4680
      Width           =   615
   End
   Begin VB.Label Label3 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Senha"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   5
      Top             =   2520
      Width           =   615
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Utilizador"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   840
      TabIndex        =   4
      Top             =   2040
      Width           =   810
   End
End
Attribute VB_Name = "FormVerificarUtilizador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.


Dim NumTentativas As Integer
Dim bContinuar As Boolean
Private m_cDibR As New cDIBSectionRegion


Private Sub BtOpcoes_Click()
    If Me.Height > 4140 Then
        'minimizar tamanmho
        'BtOpcoes.caption = "O&p��es >>"
        Me.Height = 4140
        ButtonOK.top = 3000
        ButtonCancelar.top = 3000
        BtOpcoes.top = 3000
        LaLocal.top = 4800
        EcLocal.top = 4800
    Else
        'maximizar tamanho
        'BtOpcoes.caption = "O&p��es <<"
        Me.Height = 4620
        ButtonOK.top = 3800
        ButtonCancelar.top = 3800
        BtOpcoes.top = 3800
        LaLocal.top = 3000
        EcLocal.top = 3000
    End If
End Sub

Private Sub ButtonCancelar_Click()
    bContinuar = False
    Unload Me
End Sub

Private Sub ButtonOK_Click()
    Dim rsTabela As adodb.recordset
    Dim ssql As String
    Dim sSenha As String
    Dim bErro As Boolean
    
    'edgar.parada Glintt-HS-17250 LOGIN GA_ACCESS
    Dim utilizadorAutenticadoGA As Boolean
    Dim exceptionGa As Boolean
    Dim msg As String
    'edgar.parada HPP-49475 LOGIN GA_ACCESS Carregar com o valor digitado na text
    Dim g_user As String
    Dim var_user As String
    var_user = UCase(EcUtilizador)
    '
    utilizadorAutenticadoGA = False
    exceptionGa = False
        'edgar.parada HPP-49475 - Variavel var_user
        If gUsa_Autenticacao_GA_ACCESS = mediSim Then
            var_user = vbNullString
            If BL_UsaGaAccess(EcUtilizador.text, EcSenhaAux.text, exceptionGa, g_user) = "OK" Then
                utilizadorAutenticadoGA = True
                'edgar.parada HPP-49475 LOGIN GA_ACCESS Utilizador da API
                var_user = g_user
                '
            End If
        End If
        '
    'RGONCALVES 07.07.2015 CHSJ-2082
    Dim utilizadorAutenticadoAD As Boolean
    utilizadorAutenticadoAD = False
    If gUsaAutenticacaoActiveDirectory >= 1 Then
        If BL_AuthenticateActiveDirectoryUser(gDominioActiveDirectory, EcUtilizador.text, EcSenhaAux.text) = True Then
            utilizadorAutenticadoAD = True
        Else
            If gUsaAutenticacaoActiveDirectory <= 1 Then
                NumTentativas = NumTentativas + 1
                If NumTentativas < 3 Then
                    BG_Mensagem mediMsgBox, "Entrada n�o validada !", vbExclamation, "Verificar Utilizador"
                    EcUtilizador.SetFocus
                    Exit Sub
                Else
                    BG_Mensagem mediMsgBox, "Fim das tentativas permitidas!", vbExclamation, "Verificar Utilizador"
                    End
                End If
                Exit Sub
            End If
        End If
    End If
    '
    
    Set rsTabela = New adodb.recordset
    
    On Error GoTo TrataErro
    
    bErro = False
    NumTentativas = NumTentativas + 1
    
    ' Case sensitive ou n�o.
    ssql = "SELECT cod_utilizador, utilizador, utilizador_antigo, senha," & _
       " cod_gr_util, nome, funcao, departamento, flg_removido, user_cri, " & _
       " dt_cri, user_act, dt_act, gr_ana, titulo, permissao_res, e_mail," & _
       " telefone, permissao_assinar, email, cod_posto, computador, " & _
       " flg_electronico, permissao_alt_preco, cod_profile, cod_sala, " & _
       " gr_trab, cod_util_colh, cod_t_agenda "
    If (gLoginCaseSensitive) Then
        ssql = ssql & " from sl_idutilizador LEFT OUTER JOIN sl_computador ON sl_idutilizador.computador = sl_computador.descr_computador "
        ssql = ssql & " WHERE (utilizador) = " & BL_TrataStringParaBD(var_user) & " AND flg_removido = 0 "
    Else
        ssql = ssql & " from sl_idutilizador LEFT OUTER JOIN sl_computador ON sl_idutilizador.computador = sl_computador.descr_computador "
        ssql = ssql & " WHERE UPPER(utilizador) = " & BL_TrataStringParaBD(var_user) & " AND flg_removido = 0 "
    End If
    
    rsTabela.CursorLocation = adUseClient
    rsTabela.CursorType = adOpenStatic
    
    rsTabela.Open ssql, gConexao
    
    If rsTabela.RecordCount <= 0 Or IsNull(rsTabela!senha) Then
        sSenha = ""
    Else
        sSenha = Trim(rsTabela!senha)
    End If
        
    EcSenha = BL_Desencripta(Trim(sSenha))
    
    'edgar.parada Glintt-HS-17250 LOGIN GA_ACCESS
    msg = vbNullString
    If (gUsa_Autenticacao_GA_ACCESS = mediSim And utilizadorAutenticadoGA = False) Or bErro = True Then
    
        If ((exceptionGa = True And (rsTabela.RecordCount > 0 And EcSenha.text <> Trim(EcSenhaAux.text)) _
        Or (rsTabela.RecordCount <= 0))) Or exceptionGa = False Then
            msg = BL_ValidaLogin(NumTentativas)
            If msg <> vbNullString Then
                BG_Mensagem mediMsgBox, msg, vbExclamation, "Verificar Utilizador(GA)"
                EcUtilizador.SetFocus
                rsTabela.Close
                 If NumTentativas >= 3 Then
                    Unload Me
                 End If
                Exit Sub
            End If
        End If

    ElseIf (gUsaAutenticacaoActiveDirectory = mediSim And utilizadorAutenticadoAD = False And EcSenha.text <> Trim(EcSenhaAux.text)) Or bErro = True Then

        msg = BL_ValidaLogin(NumTentativas)
        If msg <> vbNullString Then
            BG_Mensagem mediMsgBox, msg, vbExclamation, "Verificar Utilizador"
            EcUtilizador.SetFocus
            rsTabela.Close
             If NumTentativas >= 3 Then
                Unload Me
            End If
            Exit Sub
        End If

    ElseIf gUsa_Autenticacao_GA_ACCESS <> mediSim And gUsaAutenticacaoActiveDirectory <> mediSim _
                And ((rsTabela.RecordCount <= 0) Or bErro = True Or (EcSenha.text <> Trim(EcSenhaAux.text) And rsTabela.RecordCount > 0)) Then

        msg = BL_ValidaLogin(NumTentativas)
        If msg <> vbNullString Then
            BG_Mensagem mediMsgBox, msg, vbExclamation, "Verificar Utilizador"
            EcUtilizador.SetFocus
            rsTabela.Close
            If NumTentativas >= 3 Then
               Unload Me
            End If
            Exit Sub
        End If

    End If
      '
    If rsTabela.RecordCount <= 0 Then
          msg = BL_ValidaLogin(NumTentativas)
          If msg <> vbNullString Then
              BG_Mensagem mediMsgBox, msg & vbCrLf & "Utilizador n�o encontrado!", vbExclamation, "Verificar Utilizador" & IIf(gUsa_Autenticacao_GA_ACCESS = mediSim, "(GA)", vbNullString)
              EcUtilizador.SetFocus
              rsTabela.Close
               If NumTentativas >= 3 Then
                  Unload Me
               End If
              Exit Sub
          End If
    End If
      
        gIdUtilizador = rsTabela!utilizador
        gCodUtilizador = rsTabela!cod_utilizador
        gCodUtilizadorColheita = BL_HandleNull(rsTabela!cod_util_colh, "")
        gSenhaUtilizador = BL_HandleNull(rsTabela!senha)
        gNomeUtilizador = BL_HandleNull(rsTabela!nome, "")
        gTituloUtilizador = BL_HandleNull(rsTabela!Titulo, "")
        gCodGrupo = rsTabela!cod_gr_util
        gCodGrAnaUtilizador = "" & rsTabela!gr_ana
        gCodGrTrabUtilizador = "" & BL_HandleNull(rsTabela!gr_trab, "")
        gPermResUtil = BL_HandleNull(rsTabela!permissao_res, 0)
        gPermReqAssinar = BL_HandleNull(rsTabela!permissao_assinar, 0)
        gComputador = BG_SYS_GetComputerName
        gTerminal = BG_SYS_GetTerminalName
        gCodSalaAssocComp = BL_HandleNull(rsTabela!cod_sala, 0)
        gCodSalaAssocUser = BL_HandleNull(rsTabela!cod_posto, 0)
        gPermAltPreco = BL_HandleNull(rsTabela!permissao_alt_preco, 0)
        gCodTAgendaDefeito = BL_HandleNull(rsTabela!cod_t_agenda, "")
        If MDIFormInicio.Visible = True Then
            LimpaFormsAbertos
        End If
        If gUsaProfiles = mediSim Then
            If BL_HandleNull(rsTabela!cod_profile, "") = "" Then
                BG_Mensagem mediMsgBox, "Utilizador tem que estar associado a um profile!", vbExclamation, "Verificar Utilizador"
                EcUtilizador.SetFocus
                rsTabela.Close
                Exit Sub
            Else
                gProfileUtilizador = BL_HandleNull(rsTabela!cod_profile, "")
            End If
        Else
            gProfileUtilizador = BL_HandleNull(rsTabela!cod_profile, "")
        End If
        
        rsTabela.Close

        
        ssql = "SELECT * FROM " & "sl_ass_util_locais"
        ssql = ssql & " WHERE cod_util=" & gCodUtilizador
        ssql = ssql & " AND cod_local=" & BG_DaComboSel(EcLocal)
        Set rsTabela = New adodb.recordset
        rsTabela.CursorLocation = adUseServer
        rsTabela.Open ssql, gConexao, adOpenStatic
        If rsTabela.RecordCount = 0 Then
            MsgBox "O utilizador indicado n�o pertence ao local seleccionado.", vbInformation
            rsTabela.Close
            Set rsTabela = Nothing
            Exit Sub
        End If
        rsTabela.Close
        Set rsTabela = Nothing
        
        If gCodLocal <> BG_DaComboSel(EcLocal) Then
            gCodLocal = BG_DaComboSel(EcLocal)
            CarregaParamAmbiente
        End If
        gEmpresaIdFactDefault = BL_SelCodigo("GR_EMPR_INST", "empresa_fact_default", "empresa_id", CStr(gCodLocal), "V")
        If gOracleServerSec = "" Then
            gOracleServerSec = DevolveValorINI(App.path & "\SISLAB_REG.ini", "SGBD", "SERVER_SEC" & gCodLocal)
        End If
        
        bContinuar = True
        Unload Me
        
        
   ' End If
        
    Exit Sub

TrataErro:
    bErro = True
    Resume Next
End Sub

Private Sub EcLocal_Click()
    If gCodLocal <> BG_DaComboSel(EcLocal) Then
        gCodLocal = BG_DaComboSel(EcLocal)
        CarregaParamAmbiente
    End If
End Sub

Private Sub EcSenhaAux_GotFocus()
    Dim EsteControl As Control

    Set EsteControl = EcSenhaAux
    
    EsteControl.SelStart = 0
    EsteControl.SelLength = Len(EsteControl)
    
    '---------------------------------------
    
    EcSenhaAux.MaxLength = EcSenha.MaxLength
End Sub

Private Sub EcUtilizador_GotFocus()
    Dim EsteControl As Control

    Set EsteControl = EcUtilizador
    
    EsteControl.SelStart = 0
    EsteControl.SelLength = Len(EsteControl)
End Sub


Private Sub Form_Activate()
    EcUtilizador.SetFocus
End Sub

Private Sub Form_Load()
    Me.caption = "Bem-vindo ao " & cAPLICACAO_NOME_CURTO
    Me.left = 1665
    Me.top = 2160
    Me.Width = 6570
    Me.Height = 4250
    
    EcUtilizador.MaxLength = 20
    EcSenha.MaxLength = 14

    NumTentativas = 0
    bContinuar = False
    BL_PreencheSelComboLocais EcLocal
    CarregaFundo
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_cDibR = Nothing
    If bContinuar = False Then
        BG_Mensagem mediMsgBox, "Fim da aplica��o!", vbExclamation
        
        If gConexao.state <> adStateClosed Then
            BG_Fecha_ConexaoBD_ADO
        End If
        
        Set FormVerificarUtilizador = Nothing
        
        End ' Fim da Aplica��o
    End If
End Sub

Private Sub CarregaFundo()
    Dim sCaminhoFicheiro As String
    Dim sFicheiro As String
    Dim sCor As Variant
    
    Dim cDib As New cDIBSection
    
    On Error GoTo TrataErro
    
    sCaminhoFicheiro = App.path & "\SISLAB_REG.ini"
    
    sFicheiro = DevolveValorINI(sCaminhoFicheiro, "Arranque", "Imagem")
    If Dir(sFicheiro) <> "" And sFicheiro <> "" Then
        'Carregar imagem
        
        Set EcImagem.Picture = LoadPicture(sFicheiro)
    End If
    

    cDib.CreateFromPicture EcImagem.Picture
    m_cDibR.Create cDib
    m_cDibR.Applied(Me.hwnd) = True
    
    Set Me.Picture = EcImagem
    
    sCor = DevolveValorINI(sCaminhoFicheiro, "Login", "Cor")
    If sCor <> "(Desconhecido)" And sCor <> "" Then
        'preencher cor
        
        Me.BackColor = CLng(sCor)
        Label2.BackColor = CLng(sCor)
        Label3.BackColor = CLng(sCor)
        LaLocal.BackColor = CLng(sCor)
    End If
    
    Set cDib = Nothing

    Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro em <CarregaFundo>: " & Err.Number & " - " & Err.Description
    Resume Next
End Sub

Private Sub LimpaFormsAbertos()
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To pStackJanelas
        Unload StackJanelas(i)
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro em LimpaFormsAbertos: " & Err.Number & " - " & Err.Description
    Exit Sub
    Resume Next
End Sub

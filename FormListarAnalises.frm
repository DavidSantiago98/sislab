VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FormListarAnalises 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormListarAnalises"
   ClientHeight    =   4080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5475
   Icon            =   "FormListarAnalises.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   120
      TabIndex        =   7
      Top             =   2520
      Width           =   5295
      Begin VB.OptionButton OptOrdena 
         Caption         =   "Ordena por ordem de an�lise"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   960
         Width           =   2415
      End
      Begin VB.CheckBox CkMembros 
         Caption         =   "Descriminar membros"
         Height          =   255
         Left            =   2640
         TabIndex        =   11
         Top             =   600
         Width           =   2415
      End
      Begin VB.CheckBox CkCanceladas 
         Caption         =   "Incluir Canceladas"
         Height          =   255
         Left            =   2640
         TabIndex        =   10
         Top             =   240
         Width           =   2415
      End
      Begin VB.OptionButton OptOrdena 
         Caption         =   "Ordena por c�digo"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   2055
      End
      Begin VB.OptionButton OptOrdena 
         Caption         =   "Ordena por descri��o"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame Frame1 
      Height          =   2535
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      Begin VB.ComboBox CbGrupo 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1080
         Width           =   3135
      End
      Begin VB.OptionButton OptPorGrupo 
         Caption         =   "Por Grupo :"
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   1080
         Width           =   1215
      End
      Begin VB.OptionButton OptTodosGrupos 
         Caption         =   "Todos os Grupos"
         Height          =   255
         Left            =   480
         TabIndex        =   2
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox EcCodGrupo 
         Height          =   285
         Left            =   3480
         TabIndex        =   1
         Top             =   600
         Visible         =   0   'False
         Width           =   855
      End
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   480
         TabIndex        =   6
         Top             =   1800
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   450
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
      End
      Begin VB.Label Label1 
         Caption         =   "EcCodGrupo"
         Height          =   255
         Left            =   2520
         TabIndex        =   5
         Top             =   600
         Visible         =   0   'False
         Width           =   975
      End
   End
End
Attribute VB_Name = "FormListarAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2002
' T�cnico Paulo Costa

'Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim rsGrupo As ADODB.recordset
Dim RsSGrupo As ADODB.recordset
Dim sql As String

Dim CmdP As New ADODB.Command
Dim PmtP As ADODB.Parameter
Dim RsP As ADODB.recordset

Dim CmdS1 As New ADODB.Command
Dim CmdS2 As New ADODB.Command
Dim RsAnaS As ADODB.recordset

Dim CmdIR As New ADODB.Command
Dim RsIR As ADODB.recordset

Dim CmdVR As New ADODB.Command
Dim PmtVR As ADODB.Parameter
Dim RsVR As ADODB.recordset

Dim CmdC As New ADODB.Command
Dim PmtC As ADODB.Parameter
Dim rsC As ADODB.recordset

Dim CmdMemb As New ADODB.Command
Dim PmtMemb As ADODB.Parameter
Dim RsMemb As ADODB.recordset

Dim CmdGr As New ADODB.Command
Dim PmtGr As ADODB.Parameter
Dim RsGr As ADODB.recordset

Dim CmdSgr As New ADODB.Command
Dim PmtSGr As ADODB.Parameter
Dim RsSgr As ADODB.recordset

Dim CmdClasse As New ADODB.Command
Dim PmtClasse As ADODB.Parameter
Dim RsClasse As ADODB.recordset

Dim CmdSelC As New ADODB.Command
Dim PmtSelC As ADODB.Parameter
Dim PmtSelG As ADODB.Parameter

Dim CmdSelS As New ADODB.Command
Dim PmtSelS As ADODB.Parameter

Dim CmdInsTmpLa As New ADODB.Command
Dim CmdInsCrLa As New ADODB.Command

Dim TemValoresRef As Boolean

Private Sub CbGrupo_Click()

    BL_ColocaComboTexto "sl_gr_ana", "seq_gr_ana", "cod_gr_ana", EcCodGrupo, CbGrupo
    
End Sub

Private Sub CbGrupo_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbGrupo.ListIndex = -1
    
End Sub

Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()

    EventoLoad
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    'DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    If MDIFormInicio.Tag = "ESCREFZONAS" Then
        'ESCAL�ES E ZONAS DE REFER�NCIA
        Call BL_FechaPreview("Mapa de Escal�es/Zonas de Refer�ncia")
    Else
        'AN�LISES
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        Set CmdMemb = Nothing
        If Not RsMemb Is Nothing Then
            RsMemb.Close
            Set RsMemb = Nothing
        End If
        Set CmdP = Nothing
        If Not RsP Is Nothing Then
            RsP.Close
            Set RsP = Nothing
        End If
        Set CmdC = Nothing
        If Not rsC Is Nothing Then
            rsC.Close
            Set rsC = Nothing
        End If
        Set CmdS1 = Nothing
        Set CmdS2 = Nothing
        If Not RsAnaS Is Nothing Then
            RsAnaS.Close
            Set RsAnaS = Nothing
        End If
        Set CmdIR = Nothing
        If Not RsIR Is Nothing Then
            RsIR.Close
            Set RsIR = Nothing
        End If
        Set CmdVR = Nothing
        If Not RsVR Is Nothing Then
            RsVR.Close
            Set RsVR = Nothing
        End If
        Set CmdGr = Nothing
        If Not RsGr Is Nothing Then
            RsGr.Close
            Set RsGr = Nothing
        End If
        Set CmdSgr = Nothing
        If Not RsSgr Is Nothing Then
            RsSgr.Close
            Set RsSgr = Nothing
        End If
        Set CmdClasse = Nothing
        If Not RsClasse Is Nothing Then
            RsClasse.Close
            Set RsClasse = Nothing
        End If
        Set CmdSelC = Nothing
        Set CmdSelS = Nothing
        Set CmdInsCrLa = Nothing
        Set CmdInsTmpLa = Nothing
        
        Call BL_FechaPreview("Mapa de An�lises")
    End If
    
    'Unload  da Form
    MDIFormInicio.Tag = ""
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormListarAnalises = Nothing
     
End Sub

Sub Inicializacoes()
        
    Dim p As ADODB.Parameter
    
    Me.left = 540
    Me.top = 450
    Me.Width = 5625
    Me.Height = 4530 ' Normal
    'Me.Height = 2330 ' Campos Extras
    
    OptTodosGrupos.value = True
    CbGrupo.Enabled = False
        
    If MDIFormInicio.Tag = "ESCREFZONAS" Then
        'ESCAL�ES E ZONAS DE REFER�NCIA
        Me.caption = "Listagem de Escal�es/Zonas de Refer�ncia"
    Else
        'AN�LISES
        Me.caption = "Listagem de An�lises"
        
        'Inicializar o comando que selecciona a descri��o de um Grupo
        CmdGr.ActiveConnection = gConexao
        CmdGr.CommandType = adCmdText
        CmdGr.CommandText = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana=?"
        CmdGr.Prepared = True
        Set PmtGr = CmdGr.CreateParameter("COD_GR_ANA", adVarChar, adParamInput, 5)
        CmdGr.Parameters.Append PmtGr
        
        'Inicializar o comando que selecciona a descri��o de um SubGrupo
        CmdSgr.ActiveConnection = gConexao
        CmdSgr.CommandType = adCmdText
        CmdSgr.CommandText = "SELECT descr_sgr_ana FROM sl_sgr_ana WHERE cod_sgr_ana=?"
        CmdSgr.Prepared = True
        Set PmtSGr = CmdSgr.CreateParameter("COD_SGR_ANA", adVarChar, adParamInput, 5)
        CmdSgr.Parameters.Append PmtSGr
        
        'Inicializar o comando que selecciona a descri��o de uma Classe
        CmdClasse.ActiveConnection = gConexao
        CmdClasse.CommandType = adCmdText
        CmdClasse.CommandText = "SELECT descr_classe_ana FROM sl_classe_ana WHERE cod_classe_ana=?"
        CmdClasse.Prepared = True
        Set PmtClasse = CmdClasse.CreateParameter("COD_CLASSE_ANA", adVarChar, adParamInput, 5)
        CmdClasse.Parameters.Append PmtClasse
        
        'Inicializar o comamdo que selecciona a descri��o de um perfil
        CmdP.ActiveConnection = gConexao
        CmdP.CommandType = adCmdText
        CmdP.CommandText = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis=?"
        CmdP.Prepared = True
        Set PmtP = CmdP.CreateParameter("COD_PERFIS", adVarChar, adParamInput, 8)
        CmdP.Parameters.Append PmtP
        
        'Inicializa��o do comando que vai buscar os dados de uma an�lise simples
        'Primeiro Resultado
        CmdS1.ActiveConnection = gConexao
        CmdS1.CommandType = adCmdText
        CmdS1.CommandText = "SELECT descr_ana_s,unid_1,t_unid_1,unid_2,t_unid_2, ordem FROM sl_ana_s WHERE cod_ana_s=?"
        CmdS1.Prepared = True
        CmdS1.Parameters.Append CmdS1.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
        
        'Segundo Resultado
        CmdS2.ActiveConnection = gConexao
        CmdS2.CommandType = adCmdText
        Select Case gSGBD
            Case gOracle
                CmdS2.CommandText = "SELECT r1.descr_ana_s,r2.unid_1,r2.t_unid_1,r2.unid_2,r2.t_unid_2, r1.ordem FROM sl_ana_s r1, sl_segundo_res r2 WHERE r1.cod_ana_s=? AND r1.cod_ana_s = r2.cod_ana_s (+)"
            Case gSqlServer
                CmdS2.CommandText = "SELECT r1.descr_ana_s,r2.unid_1,r2.t_unid_1,r2.unid_2,r2.t_unid_2, r1.ordem FROM sl_ana_s r1, sl_segundo_res r2 WHERE r1.cod_ana_s=? AND r1.cod_ana_s *= r2.cod_ana_s "
        End Select
        CmdS2.Prepared = True
        CmdS2.Parameters.Append CmdS2.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    
        'Inicializa��o do comando que vai buscar os valores de refer�ncia para uma an�lise simples
        CmdIR.ActiveConnection = gConexao
        CmdIR.CommandType = adCmdText
        CmdIR.CommandText = "SELECT seq_ana_ref FROM sl_ana_ref WHERE cod_ana_s=? AND n_res = ? AND dt_valid_sup is null"
        CmdIR.Prepared = True
        CmdIR.Parameters.Append CmdIR.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
        Set p = CmdIR.CreateParameter("N_RES", adNumeric, adParamInput, 2)
        p.Precision = 2
        CmdIR.Parameters.Append p
    
        'Inicializa��o do comando que vai buscar os valores de refer�ncia para um intervalo de refer�ncia
        CmdVR.ActiveConnection = gConexao
        CmdVR.CommandType = adCmdText
        CmdVR.CommandText = "SELECT h_ref_min,h_ref_max,f_ref_min,f_ref_max FROM sl_val_ref WHERE cod_ana_ref=?"
        CmdVR.Prepared = True
        Set PmtVR = CmdVR.CreateParameter("COD_ANA_REF", adVarChar, adParamInput, 8)
        CmdVR.Parameters.Append PmtVR
    
        'Inicializa��o do comando que selecciona a descricao da an�lise complexa
        CmdC.ActiveConnection = gConexao
        CmdC.CommandType = adCmdText
        CmdC.CommandText = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c=?"
        CmdC.Prepared = True
        Set PmtC = CmdC.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
        CmdC.Parameters.Append PmtC
    
        'Inicializa��o do comando que vai buscar os membros de uma complexa
        CmdMemb.ActiveConnection = gConexao
        CmdMemb.CommandType = adCmdText
        CmdMemb.CommandText = "select cod_ana_c,cod_membro,ordem from sl_membro where cod_ana_c=? AND t_membro='A' ORDER BY cod_ana_c, ordem"
        CmdMemb.Prepared = True
        Set PmtMemb = CmdMemb.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
        CmdMemb.Parameters.Append PmtMemb
          
        'Inicializa��o do comando para seleccionar complexas que n�o s�o membros de perfis
        CmdSelC.ActiveConnection = gConexao
        CmdSelC.CommandType = adCmdText
        'CmdSelC.CommandText = "SELECT cod_ana_c,gr_ana FROM sl_ana_c WHERE cod_ana_c NOT IN (SELECT cod_analise as cod_ana_c FROM sl_ana_perfis) AND gr_ana=? AND cod_ana_c=?"
        CmdSelC.CommandText = "SELECT cod_ana_c,gr_ana FROM sl_ana_c WHERE gr_ana=? AND cod_ana_c=?"
        CmdSelC.Prepared = True
        Set PmtSelG = CmdSelC.CreateParameter("GR_ANA", adVarChar, adParamInput, 5)
        Set PmtSelC = CmdSelC.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
        CmdSelC.Parameters.Append PmtSelG
        CmdSelC.Parameters.Append PmtSelC
        
        'Inicializa��o do comando para seleccionar simples que n�o s�o membros de perfis nem de complexas
        CmdSelS.ActiveConnection = gConexao
        CmdSelS.CommandType = adCmdText
        'CmdSelS.CommandText = "SELECT cod_ana_s,descr_ana_s,gr_ana,ordem FROM sl_ana_s WHERE cod_ana_s NOT IN(SELECT cod_analise as cod_ana_s FROM sl_ana_perfis) AND cod_ana_s NOT IN (SELECT cod_membro as cod_ana_s FROM sl_membro) AND gr_ana=? AND cod_ana_s=?"
        CmdSelS.CommandText = "SELECT cod_ana_s,descr_ana_s,gr_ana,ordem FROM sl_ana_s WHERE gr_ana=? AND cod_ana_s=?"
        CmdSelS.Prepared = True
        Set PmtSelS = CmdSelS.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
        CmdSelS.Parameters.Append PmtSelG
        CmdSelS.Parameters.Append PmtSelS
        
        CmdInsCrLa.ActiveConnection = gConexao
        CmdInsCrLa.CommandType = adCmdText
        
        CmdInsCrLa.CommandText = "INSERT INTO SL_CR_LA" & gNumeroSessao & _
                                 "  (grupo,subgrupo,classe,cod_perfil,perfil,cod_complexa,complexa,cod_simples,simples,unidade_1,unidade_2,h_ref_min,h_ref_max,f_ref_min,f_ref_max,ordem,ord_ana_p,ord_ana_c, cod_ana_gh) " & _
                                 "VALUES " & _
                                 "  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        
        CmdInsCrLa.Prepared = True
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("grupo", adVarChar, adParamInput, 60)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("subgrupo", adVarChar, adParamInput, 40)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("classe", adVarChar, adParamInput, 100)
        
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("cod_perfil", adVarChar, adParamInput, 10)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("perfil", adVarChar, adParamInput, 100)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("cod_complexa", adVarChar, adParamInput, 10)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("complexa", adVarChar, adParamInput, 100)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("cod_simples", adVarChar, adParamInput, 10)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("simples", adVarChar, adParamInput, 100)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("unidade_1", adVarChar, adParamInput, 20)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("unidade_2", adVarChar, adParamInput, 10)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("h_ref_min", adVarChar, adParamInput, 20)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("h_ref_max", adVarChar, adParamInput, 20)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("f_ref_min", adVarChar, adParamInput, 20)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("f_ref_max", adVarChar, adParamInput, 20)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("ordem", adInteger, adParamInput, 12)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("ord_ana_p", adInteger, adParamInput, 12)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("ord_ana_c", adInteger, adParamInput, 12)
        CmdInsCrLa.Parameters.Append CmdInsCrLa.CreateParameter("cod_ana_gh", adVarChar, adParamInput, 20)
        
        CmdInsTmpLa.ActiveConnection = gConexao
        CmdInsTmpLa.CommandType = adCmdText
        
        CmdInsTmpLa.CommandText = "INSERT INTO SL_TMP_LA" & gNumeroSessao & _
                                  " (grupo,subgrupo,perfil,complexa,simples,ordem,ord_ana_p,ord_ana_c,classe, cod_ana_gh) " & _
                                  "VALUES " & _
                                  " (?,?,?,?,?,?,?,?,?,?)"
        
        CmdInsTmpLa.Prepared = True
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("grupo", adVarChar, adParamInput, 5)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("subgrupo", adVarChar, adParamInput, 5)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("perfil", adVarChar, adParamInput, 10)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("complexa", adVarChar, adParamInput, 10)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("simples", adVarChar, adParamInput, 10)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("ordem", adInteger, adParamInput, 12)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("ord_ana_p", adInteger, adParamInput, 12)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("ord_ana_c", adInteger, adParamInput, 12)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("classe", adVarChar, adParamInput, 5)
        CmdInsTmpLa.Parameters.Append CmdInsTmpLa.CreateParameter("cod_ana_gh", adVarChar, adParamInput, 20)
    
        ProgressBar1.Min = 0
        ProgressBar1.Max = 6
    
    End If
    
    
End Sub

Sub PreencheValoresDefeito()

    ' Grupo.
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo, _
                           mediAscComboDesignacao
    OptOrdena(0).value = True
End Sub

Sub FuncaoLimpar()

    OptTodosGrupos.value = True
    CbGrupo.ListIndex = mediComboValorNull
    
End Sub

Sub FuncaoImprimir()
        
    If MDIFormInicio.Tag = "ESCREFZONAS" Then
        'ESCAL�ES E ZONAS DE REFER�NCIA
        Call FuncaoImprimirEscRefZonas
    Else
        ProgressBar1.value = ProgressBar1.Min
        DoEvents
        
        'AN�LISES
        Call FuncaoImprimirAnalises
    
        ProgressBar1.value = ProgressBar1.Min
        DoEvents
    End If
    
End Sub

Private Sub Buscar_Dados_Ana_Simples1(codAnalise As String)

    On Error GoTo Trata_Erro
    
    CmdS1.Parameters(0).value = codAnalise
    Set RsAnaS = CmdS1.Execute
    
    CmdIR.Parameters(0).value = codAnalise
    CmdIR.Parameters(1).value = 1
    Set RsIR = CmdIR.Execute
    TemValoresRef = False
    If Not RsIR.EOF Then
        CmdVR.Parameters(0).value = RsIR!seq_ana_ref
        Set RsVR = CmdVR.Execute
        If Not RsVR.EOF Then
            TemValoresRef = True
        End If
    End If
    
    Exit Sub

Trata_Erro:
    Resume Next
End Sub

Private Sub Buscar_Dados_Ana_Simples2(codAnalise As String)

    On Error GoTo Trata_Erro
    
    CmdS2.Parameters(0).value = codAnalise
    Set RsAnaS = CmdS2.Execute
    
    CmdIR.Parameters(0).value = codAnalise
    CmdIR.Parameters(1).value = 2
    Set RsIR = CmdIR.Execute
    TemValoresRef = False
    If Not RsIR.EOF Then
        CmdVR.Parameters(0).value = RsIR!seq_ana_ref
        Set RsVR = CmdVR.Execute
        If Not RsVR.EOF Then
            TemValoresRef = True
        End If
    End If
    
    Exit Sub

Trata_Erro:
    Resume Next
End Sub

Private Sub Cria_TmpRec()
    
    Dim TmpRec(10) As DefTable
    
    TmpRec(1).NomeCampo = "grupo"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 5
    
    TmpRec(2).NomeCampo = "subgrupo"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 5
    
    TmpRec(3).NomeCampo = "perfil"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 10
    
    TmpRec(4).NomeCampo = "complexa"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 10
    
    TmpRec(5).NomeCampo = "simples"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 10
    
    TmpRec(6).NomeCampo = "ordem"
    TmpRec(6).tipo = "DECIMAL"
    TmpRec(6).PInteira = 12
    TmpRec(6).PDecimal = 0
    
    TmpRec(7).NomeCampo = "ord_ana_p"
    TmpRec(7).tipo = "DECIMAL"
    TmpRec(7).PInteira = 12
    TmpRec(7).PDecimal = 0
    
    TmpRec(8).NomeCampo = "ord_ana_c"
    TmpRec(8).tipo = "DECIMAL"
    TmpRec(8).PInteira = 12
    TmpRec(8).PDecimal = 0
    
    TmpRec(9).NomeCampo = "Classe"
    TmpRec(9).tipo = "STRING"
    TmpRec(9).tamanho = 5
    
    TmpRec(10).NomeCampo = "COD_ANA_GH"
    TmpRec(10).tipo = "STRING"
    TmpRec(10).tamanho = 20
    
    Call BL_CriaTabela("SL_TMP_LA" & gNumeroSessao, TmpRec)
    gConexao.Execute "CREATE INDEX sl_tmp_la" & gNumeroSessao & "_1 ON sl_tmp_la" & gNumeroSessao & "(grupo,subgrupo,perfil,complexa,simples)"
    
End Sub

Private Sub Cria_TmpRec_Temporaria()
    
    Dim TmpRec(19) As DefTable
    
    TmpRec(1).NomeCampo = "grupo"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 60
    
    TmpRec(2).NomeCampo = "subgrupo"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 40
    
    TmpRec(3).NomeCampo = "cod_perfil"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 10
    
    TmpRec(4).NomeCampo = "perfil"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 100
    
    TmpRec(5).NomeCampo = "cod_complexa"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 10
    
    TmpRec(6).NomeCampo = "complexa"
    TmpRec(6).tipo = "STRING"
    TmpRec(6).tamanho = 100
    
    TmpRec(7).NomeCampo = "cod_simples"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 10
    
    TmpRec(8).NomeCampo = "simples"
    TmpRec(8).tipo = "STRING"
    TmpRec(8).tamanho = 100
    
    TmpRec(9).NomeCampo = "unidade_1"
    TmpRec(9).tipo = "STRING"
    TmpRec(9).tamanho = 20

    TmpRec(10).NomeCampo = "unidade_2"
    TmpRec(10).tipo = "STRING"
    TmpRec(10).tamanho = 10

    TmpRec(11).NomeCampo = "h_ref_min"
    TmpRec(11).tipo = "STRING"
    TmpRec(11).tamanho = 20

    TmpRec(12).NomeCampo = "h_ref_max"
    TmpRec(12).tipo = "STRING"
    TmpRec(12).tamanho = 20

    TmpRec(13).NomeCampo = "f_ref_min"
    TmpRec(13).tipo = "STRING"
    TmpRec(13).tamanho = 20

    TmpRec(14).NomeCampo = "f_ref_max"
    TmpRec(14).tipo = "STRING"
    TmpRec(14).tamanho = 20
    
    TmpRec(15).NomeCampo = "ordem"
    TmpRec(15).tipo = "DECIMAL"
    TmpRec(15).PInteira = 12
    TmpRec(15).PDecimal = 0
    
    TmpRec(16).NomeCampo = "ord_ana_p"
    TmpRec(16).tipo = "DECIMAL"
    TmpRec(16).PInteira = 12
    TmpRec(16).PDecimal = 0
    
    TmpRec(17).NomeCampo = "ord_ana_c"
    TmpRec(17).tipo = "DECIMAL"
    TmpRec(17).PInteira = 12
    TmpRec(17).PDecimal = 0
    
    TmpRec(18).NomeCampo = "classe"
    TmpRec(18).tipo = "STRING"
    TmpRec(18).tamanho = 100
    
    TmpRec(19).NomeCampo = "cod_ana_gh"
    TmpRec(19).tipo = "STRING"
    TmpRec(19).tamanho = 20
    Call BL_CriaTabela("SL_CR_LA" & gNumeroSessao, TmpRec)

End Sub

Private Sub OptPorGrupo_Click()
    
    CbGrupo.Enabled = True

End Sub

Private Sub OptTodosGrupos_Click()
    
    CbGrupo.ListIndex = mediComboValorNull
    CbGrupo.Enabled = False

End Sub

Private Sub FuncaoImprimirAnalises()
    
    Dim sql As String
    Dim RsS As ADODB.recordset
    Dim RsTmp As ADODB.recordset
    Dim DescrGr As String
    Dim DescrSGr As String
    Dim DescrClasse As String
    Dim DescrP As String
    Dim DescrC As String
    Dim continua As Boolean
    Dim RsSelC As ADODB.recordset
    Dim RsSelS As ADODB.recordset
    Dim ordenacao As String
    Dim CanceladasPerfis As String
    Dim CanceladasCompl As String
    Dim canceladasSimpl As String
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Flag que indica se foram encontradas an�lises para o grupo
    Dim Flg_Existe_Ana_Grupo As Boolean
    
    Flg_Existe_Ana_Grupo = False
    If CkCanceladas.value = vbChecked Then
        CanceladasPerfis = ""
        CanceladasCompl = ""
        canceladasSimpl = ""
    Else
        CanceladasPerfis = " AND (sl_perfis.flg_invisivel is null or sl_perfis.flg_invisivel = 0) "
        CanceladasCompl = " AND (sl_ana_c.flg_invisivel is null or sl_ana_c.flg_invisivel = 0) "
        canceladasSimpl = " AND (sl_ana_s.flg_invisivel is null or sl_ana_s.flg_invisivel = 0) "
    End If
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Mapa de An�lises") = True Then Exit Sub
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListarAnalises", "Mapa de An�lises", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListarAnalises", "Mapa de An�lises", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    Call Cria_TmpRec
    
    '************Preencher a tabela interm�dia ***************
    
    'Perfis
    If OptTodosGrupos.value = True Then
        sql = "SELECT " & _
              "     gr_ana,sgr_ana,sl_perfis.cod_perfis,descr_perfis,cod_analise," & _
              "     sl_perfis.ordem, sl_ana_perfis.ordem as ord_ana_p,classe_ana, anaf.cod_ana_gh " & _
              "FROM " & _
              "     sl_ana_perfis, sl_perfis, sl_ana_facturacao anaf " & _
              "WHERE " & _
              "     sl_perfis.cod_perfis = sl_ana_perfis.cod_perfis  " & _
              "     AND flg_activo = 1 " & CanceladasPerfis & _
              "     AND sl_perfis.cod_perfis = anaf.cod_ana (+) " & _
              "ORDER BY " & _
              "     gr_ana, sgr_ana, classe_ana, " & _
              "     sl_perfis.cod_perfis, sl_perfis.ordem, ord_ana_p"
    Else
        sql = "SELECT " & _
              "     gr_ana,sgr_ana,sl_perfis.cod_perfis,descr_perfis,cod_analise," & _
              "     sl_perfis.ordem ,sl_ana_perfis.ordem as ord_ana_p,classe_ana, anaf.cod_ana_gh  " & _
              "FROM " & _
              "     sl_ana_perfis, sl_perfis, sl_ana_facturacao anaf  " & _
              "WHERE sl_ana_perfis.cod_perfis = sl_perfis.cod_perfis " & _
              "     AND flg_activo = 1 " & CanceladasPerfis & _
              "     AND sl_perfis.gr_ana     = " & BL_TrataStringParaBD(EcCodGrupo.Text) & _
              "     AND sl_perfis.cod_perfis = anaf.cod_ana (+) " & _
              "ORDER BY " & _
              "     gr_ana, sgr_ana, classe_ana, " & _
              "     sl_perfis.cod_perfis, sl_perfis.ordem, ord_ana_p"
    End If
    
    Set RsP = New ADODB.recordset
    RsP.CursorLocation = adUseServer
    RsP.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsP.Open sql, gConexao
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    If RsP.RecordCount > 0 Then
        Flg_Existe_Ana_Grupo = True
        While Not RsP.EOF
            'seleccionar membros dos perfis
            If Mid(RsP!cod_analise, 1, 1) = "C" Then
                'Seleccionar membros da complexa
                CmdMemb.Parameters(0).value = RsP!cod_analise
                Set RsMemb = CmdMemb.Execute
                While Not RsMemb.EOF
                    'O membro � uma complexa
                    CmdInsTmpLa.Parameters("grupo") = BL_HandleNull(RsP!gr_ana, "")
                    CmdInsTmpLa.Parameters("subgrupo") = Mid(BL_HandleNull(RsP!sgr_ana, ""), 1, 40)
                    CmdInsTmpLa.Parameters("perfil") = BL_HandleNull(RsP!cod_perfis, "")
                    CmdInsTmpLa.Parameters("complexa") = BL_HandleNull(RsP!cod_analise, "")
                    CmdInsTmpLa.Parameters("simples") = BL_HandleNull(RsMemb!cod_membro, "")
                    CmdInsTmpLa.Parameters("ordem") = BL_HandleNull(RsP!ordem, 0)
                    CmdInsTmpLa.Parameters("ord_ana_p") = BL_HandleNull(RsP!ORD_ANA_P, 0)
                    CmdInsTmpLa.Parameters("ord_ana_c") = BL_HandleNull(RsMemb!ordem, 0)
                    CmdInsTmpLa.Parameters("classe") = BL_HandleNull(RsP!classe_ana, "")
                    CmdInsTmpLa.Parameters("cod_ana_gh") = BL_HandleNull(RsP!cod_ana_gh, "")
                    CmdInsTmpLa.Execute
                    RsMemb.MoveNext
                Wend
            Else
                'O membro � uma simples
                CmdInsTmpLa.Parameters("grupo") = BL_HandleNull(RsP!gr_ana, "")
                CmdInsTmpLa.Parameters("subgrupo") = Mid(BL_HandleNull(RsP!sgr_ana, ""), 1, 40)
                CmdInsTmpLa.Parameters("perfil") = BL_HandleNull(RsP!cod_perfis, "")
                CmdInsTmpLa.Parameters("complexa") = ""
                CmdInsTmpLa.Parameters("simples") = BL_HandleNull(RsP!cod_analise, "")
                CmdInsTmpLa.Parameters("ordem") = BL_HandleNull(RsP!ordem, 0)
                CmdInsTmpLa.Parameters("ord_ana_p") = BL_HandleNull(RsP!ORD_ANA_P, 0)
                CmdInsTmpLa.Parameters("ord_ana_c") = 0
                CmdInsTmpLa.Parameters("classe") = BL_HandleNull(RsP!classe_ana, "")
                CmdInsTmpLa.Parameters("cod_ana_gh") = BL_HandleNull(RsP!cod_ana_gh, "")
                CmdInsTmpLa.Execute
            End If
            RsP.MoveNext
        Wend
    End If
    
    'Seleccionar Complexas e seus membros
    If OptTodosGrupos.value = True Then
        sql = "SELECT " & _
              "     gr_ana,sgr_ana,sl_ana_c.cod_ana_c,cod_membro, sl_ana_c.ordem, " & _
              "     sl_membro.ordem as ord_ana_c,sl_ana_c.classe_ana, anaf.cod_ana_gh  " & _
              "FROM " & _
              "     sl_ana_c,sl_membro, sl_ana_facturacao anaf  " & _
              "WHERE " & _
              "     sl_ana_c.cod_ana_c = sl_membro.cod_ana_c  " & _
              "     AND sl_membro.t_membro = 'A' " & CanceladasCompl & _
              "     AND sl_ana_c.cod_ana_c = anaf.cod_ana (+) " & _
              "ORDER BY " & _
              "     gr_ana,sgr_ana,sl_ana_c.classe_ana,sl_ana_c.cod_ana_c,ord_ana_c"
    Else
        sql = "SELECT " & _
              "     gr_ana,sgr_ana,sl_ana_c.cod_ana_c,cod_membro, sl_ana_c.ordem, " & _
              "     sl_membro.ordem as ord_ana_c,sl_ana_c.classe_ana, anaf.cod_ana_gh " & _
              "FROM " & _
              "     sl_ana_c,sl_membro, sl_ana_facturacao anaf " & _
              "WHERE " & _
              "     sl_ana_c.cod_ana_c = sl_membro.cod_ana_c AND " & _
              "     sl_ana_c.gr_ana    = " & BL_TrataStringParaBD(EcCodGrupo.Text) & " AND " & _
              "     sl_membro.t_membro = 'A' " & CanceladasCompl & _
              "     AND sl_ana_c.cod_ana_c = anaf.cod_ana (+) " & _
              "ORDER BY " & _
              "     gr_ana,sgr_ana,sl_ana_c.classe_ana,sl_ana_c.cod_ana_c,ord_ana_c"
    End If
    Set rsC = New ADODB.recordset
    rsC.CursorLocation = adUseServer
    rsC.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rsC.Open sql, gConexao
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    If rsC.RecordCount > 0 Then
        While Not rsC.EOF
            CmdSelC.Parameters(0).value = rsC!gr_ana
            CmdSelC.Parameters(1).value = rsC!cod_ana_c
            Set RsSelC = CmdSelC.Execute
            If Not RsSelC.EOF Then
                'A an�lise complexa n�o existe dentro de perfis
                Flg_Existe_Ana_Grupo = True
            End If
            If Not RsSelC Is Nothing Then
                RsSelC.Close
                Set RsSelC = Nothing
            End If
           
            CmdInsTmpLa.Parameters("grupo") = BL_HandleNull(rsC!gr_ana, "")
            CmdInsTmpLa.Parameters("subgrupo") = BL_HandleNull(rsC!sgr_ana, "")
            CmdInsTmpLa.Parameters("perfil") = ""
            CmdInsTmpLa.Parameters("complexa") = BL_HandleNull(rsC!cod_ana_c, "")
            CmdInsTmpLa.Parameters("simples") = BL_HandleNull(rsC!cod_membro, "")
            CmdInsTmpLa.Parameters("ordem") = BL_HandleNull(rsC!ordem, 0)
            CmdInsTmpLa.Parameters("ord_ana_p") = 0
            CmdInsTmpLa.Parameters("ord_ana_c") = BL_HandleNull(rsC!ord_ana_c, 0)
            CmdInsTmpLa.Parameters("classe") = BL_HandleNull(rsC!classe_ana, "")
            CmdInsTmpLa.Parameters("cod_ana_gh") = BL_HandleNull(rsC!cod_ana_gh, "")
            
            CmdInsTmpLa.Execute
            
            rsC.MoveNext
        Wend
    End If
    
    'Seleccionar an�lises simples
    If OptTodosGrupos.value = True Then
        'SQL = "SELECT gr_ana,sgr_ana,cod_ana_s, ordem FROM sl_ana_s WHERE cod_ana_s NOT IN ( SELECT cod_analise FROM sl_ana_perfis ) AND cod_ana_s NOT IN ( SELECT cod_membro FROM sl_membro ) ORDER BY gr_ana, sgr_ana"
        sql = "SELECT " & _
              "     gr_ana,sgr_ana,cod_ana_s,ordem,classe_ana, anaf.cod_ana_gh " & _
              "FROM " & _
              "     sl_ana_s, sl_ana_facturacao anaf " & _
              "WHERE " & _
              "     cod_ana_s NOT IN " & _
              "     ( " & _
              "         SELECT " & _
              "             cod_membro " & _
              "         FROM " & _
              "             sl_membro " & _
              "         WHERE " & _
              "             t_membro='A' " & _
              "     ) AND " & _
              "     cod_ana_s NOT IN" & _
              "     (" & _
              "         SELECT " & _
              "             x1.cod_analise " & _
              "         FROM " & _
              "             sl_ana_perfis x1, sl_perfis x3 WHERE x1.cod_perfis = x3.cod_perfis and x3.flg_activo = 1" & _
              "     ) " & canceladasSimpl & _
              "     AND sl_ana_s.cod_ana_s = anaf.cod_ana (+) " & _
              "ORDER BY " & _
              "     gr_ana,sgr_ana,classe_ana"
    Else
        'SQL = "SELECT gr_ana,sgr_ana,cod_ana_s, ordem FROM sl_ana_s WHERE gr_ana=" & BL_TrataStringParaBD(EcCodGrupo.Text) & " AND cod_ana_s NOT IN ( SELECT cod_analise FROM sl_ana_perfis ) AND cod_ana_s NOT IN ( SELECT cod_membro FROM sl_membro ) ORDER BY sgr_ana"
        sql = "SELECT " & _
              "     gr_ana,sgr_ana,cod_ana_s,ordem,classe_ana, anaf.cod_ana_gh " & _
              "FROM " & _
              "     sl_ana_s, sl_ana_facturacao anaf " & _
              "WHERE " & _
              "     gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text) & " AND " & _
              "     cod_ana_s NOT IN " & _
              "     ( " & _
              "         SELECT " & _
              "             cod_membro " & _
              "         FROM " & _
              "             sl_membro " & _
              "         WHERE " & _
              "             t_membro='A' " & _
              "     ) AND " & _
              "     cod_ana_s NOT IN" & _
              "     (" & _
              "         SELECT " & _
              "             x1.cod_analise " & _
              "         FROM " & _
              "             sl_ana_perfis x1, sl_perfis x3 WHERE x1.cod_perfis = x3.cod_perfis and x3.flg_activo = 1" & _
              "     ) " & canceladasSimpl & _
              "     AND sl_ana_s.cod_ana_s = anaf.cod_ana (+) " & _
              "ORDER BY sgr_ana,classe_ana"
    End If
    
    Set RsS = New ADODB.recordset
    RsS.CursorLocation = adUseServer
    RsS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsS.Open sql, gConexao
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    If RsS.RecordCount > 0 Then
        While Not RsS.EOF
            CmdSelS.Parameters(0).value = RsS!gr_ana
            CmdSelS.Parameters(1).value = RsS!cod_ana_s
            Set RsSelS = CmdSelS.Execute
            If Not RsSelS.EOF Then
                Flg_Existe_Ana_Grupo = True
            End If
            If Not RsSelS Is Nothing Then
                RsSelS.Close
                Set RsSelS = Nothing
            End If
            CmdInsTmpLa.Parameters("grupo") = BL_HandleNull(RsS!gr_ana, "")
            CmdInsTmpLa.Parameters("subgrupo") = BL_HandleNull(RsS!sgr_ana, "")
            CmdInsTmpLa.Parameters("perfil") = ""
            CmdInsTmpLa.Parameters("complexa") = ""
            CmdInsTmpLa.Parameters("simples") = BL_HandleNull(RsS!cod_ana_s, "")
            CmdInsTmpLa.Parameters("ordem") = BL_HandleNull(RsS!ordem, 0)
            CmdInsTmpLa.Parameters("ord_ana_p") = 0
            CmdInsTmpLa.Parameters("ord_ana_c") = 0
            CmdInsTmpLa.Parameters("classe") = BL_HandleNull(RsS!classe_ana, "")
            CmdInsTmpLa.Parameters("cod_ana_gh") = BL_HandleNull(RsS!cod_ana_gh, "")
            
            CmdInsTmpLa.Execute
            
            RsS.MoveNext
        Wend
    End If
    RsS.Close
    Set RsS = Nothing

    If Flg_Existe_Ana_Grupo Then
        
        '*******Fim preenchimento da tabela interm�dia********
        
        Call Cria_TmpRec_Temporaria
        
        sql = "SELECT * FROM SL_TMP_LA" & gNumeroSessao & " ORDER BY grupo DESC"
        
        Set RsTmp = New ADODB.recordset
        RsTmp.CursorLocation = adUseServer
        RsTmp.CursorType = adOpenStatic
        RsTmp.CacheSize = 100
        RsTmp.PageSize = 100
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsTmp.Open sql, gConexao
        
        While Not RsTmp.EOF
            'Descri��o do grupo
            DescrGr = ""
            If BL_HandleNull(RsTmp!Grupo, "") <> "" Then
                CmdGr.Parameters(0).value = RsTmp!Grupo
                Set RsGr = CmdGr.Execute
                If Not RsGr.EOF Then
                    DescrGr = "" & RsGr!descr_gr_ana
                End If
            End If
            
            'Descri��o do SubGrupo
            DescrSGr = ""
            If BL_HandleNull(RsTmp!subGrupo, "") <> "" Then
                CmdSgr.Parameters(0).value = RsTmp!subGrupo
                Set RsSgr = CmdSgr.Execute
                If Not RsSgr.EOF Then
                    DescrSGr = "" & RsSgr!descr_sgr_ana
                End If
            End If
            
            'Descri��o da Classe
            DescrClasse = ""
            If BL_HandleNull(RsTmp!Classe, "") <> "" Then
                CmdClasse.Parameters(0).value = RsTmp!Classe
                Set RsClasse = CmdClasse.Execute
                If Not RsClasse.EOF Then
                    DescrClasse = "" & RsClasse!descr_classe_ana
                End If
            End If
            
            'Descri��o do perfil
            DescrP = ""
            If BL_HandleNull(RsTmp!Perfil, "") <> "" Then
                CmdP.Parameters(0).value = RsTmp!Perfil
                Set RsP = CmdP.Execute
                If Not RsP.EOF Then
                    DescrP = "" & RsP!descr_perfis
                End If
            End If
            'Descri��o da complexa
            DescrC = ""
            If BL_HandleNull(RsTmp!complexa, "") <> "" Then
                CmdC.Parameters(0).value = RsTmp!complexa
                Set rsC = CmdC.Execute
                If Not rsC.EOF Then
                    DescrC = "" & rsC!Descr_Ana_C
                End If
            End If
            
            'Dados da simples
            
            'Resultado 1
            If BL_HandleNull(RsTmp!simples, "") <> "" Then
                Buscar_Dados_Ana_Simples1 RsTmp!simples
            End If
            
            CmdInsCrLa.Parameters("grupo") = IIf((DescrGr <> ""), DescrGr, "SEM GRUPO")
            CmdInsCrLa.Parameters("subgrupo") = IIf((DescrSGr <> ""), Mid(DescrSGr, 1, 40), "SEM SUBGRUPO")
            CmdInsCrLa.Parameters("classe") = IIf((DescrClasse <> ""), DescrClasse, "SEM CLASSE")
            
            CmdInsCrLa.Parameters("cod_perfil") = IIf(BL_HandleNull(RsTmp!Perfil, "") = "", "", RsTmp!Perfil)
            CmdInsCrLa.Parameters("perfil") = IIf(DescrP = "", "", DescrP)
            CmdInsCrLa.Parameters("cod_complexa") = IIf(BL_HandleNull(RsTmp!complexa, "") = "", "", RsTmp!complexa)
            CmdInsCrLa.Parameters("complexa") = IIf(DescrC = "", "", DescrC)
            CmdInsCrLa.Parameters("cod_simples") = IIf(BL_HandleNull(RsTmp!simples, "") = "", "", RsTmp!simples)
            CmdInsCrLa.Parameters("simples") = ""
            CmdInsCrLa.Parameters("unidade_1") = ""
            CmdInsCrLa.Parameters("unidade_2") = ""
            CmdInsCrLa.Parameters("ordem") = BL_HandleNull(RsTmp!ordem, 0)
            CmdInsCrLa.Parameters("ord_ana_p") = BL_HandleNull(RsTmp!ORD_ANA_P, 0)
            CmdInsCrLa.Parameters("ord_ana_c") = BL_HandleNull(RsTmp!ord_ana_c, 0)
            CmdInsCrLa.Parameters("cod_ana_gh") = BL_HandleNull(RsTmp!cod_ana_gh, "")
            
            If Not (RsAnaS Is Nothing) Then
                If Not RsAnaS.EOF Then
                    CmdInsCrLa.Parameters("simples") = IIf(BL_HandleNull(RsAnaS!descr_ana_s, "") = "", "", RsAnaS!descr_ana_s)
                    CmdInsCrLa.Parameters("unidade_1") = IIf(BL_HandleNull(RsAnaS!unid_1, "") = "", "", RsAnaS!unid_1)
                    CmdInsCrLa.Parameters("unidade_2") = IIf(BL_HandleNull(RsAnaS!unid_2, "") = "", "", RsAnaS!unid_2)
                End If
            End If
            
            If TemValoresRef Then
                CmdInsCrLa.Parameters("h_ref_min") = IIf(BL_HandleNull(RsVR!h_ref_min, "") = "", "", RsVR!h_ref_min)
                CmdInsCrLa.Parameters("h_ref_max") = IIf(BL_HandleNull(RsVR!h_ref_max, "") = "", "", RsVR!h_ref_max)
                CmdInsCrLa.Parameters("f_ref_min") = IIf(BL_HandleNull(RsVR!f_ref_min, "") = "", "", RsVR!f_ref_min)
                CmdInsCrLa.Parameters("f_ref_max") = IIf(BL_HandleNull(RsVR!f_ref_max, "") = "", "", RsVR!f_ref_max)
            Else
                CmdInsCrLa.Parameters("h_ref_min") = ""
                CmdInsCrLa.Parameters("h_ref_max") = ""
                CmdInsCrLa.Parameters("f_ref_min") = ""
                CmdInsCrLa.Parameters("f_ref_max") = ""
            End If
            
            CmdInsCrLa.Execute
            
            'Resultado2
            If BL_HandleNull(RsTmp!simples, "") <> "" Then
                Buscar_Dados_Ana_Simples2 RsTmp!simples
            End If
            
            CmdInsCrLa.Parameters("grupo") = IIf((DescrGr <> ""), DescrGr, "SEM GRUPO")
            CmdInsCrLa.Parameters("subgrupo") = IIf((DescrSGr <> ""), Mid(DescrSGr, 1, 40), "SEM SUBGRUPO")
            CmdInsCrLa.Parameters("cod_perfil") = IIf(BL_HandleNull(RsTmp!Perfil, "") = "", "", RsTmp!Perfil)
            CmdInsCrLa.Parameters("perfil") = IIf(DescrP = "", "", DescrP)
            CmdInsCrLa.Parameters("cod_complexa") = IIf(BL_HandleNull(RsTmp!complexa, "") = "", "", RsTmp!complexa)
            CmdInsCrLa.Parameters("complexa") = IIf(DescrC = "", "", DescrC)
            CmdInsCrLa.Parameters("cod_simples") = IIf(BL_HandleNull(RsTmp!simples, "") = "", "", RsTmp!simples)
            CmdInsCrLa.Parameters("simples") = ""
            CmdInsCrLa.Parameters("unidade_1") = ""
            CmdInsCrLa.Parameters("unidade_2") = ""
            CmdInsCrLa.Parameters("ordem") = BL_HandleNull(RsTmp!ordem, 0)
            CmdInsCrLa.Parameters("ord_ana_p") = BL_HandleNull(RsTmp!ORD_ANA_P, 0)
            CmdInsCrLa.Parameters("ord_ana_c") = BL_HandleNull(RsTmp!ord_ana_c, 0)
            CmdInsCrLa.Parameters("cod_ana_gh") = BL_HandleNull(RsTmp!cod_ana_gh, "")
            
            If Not (RsAnaS Is Nothing) Then
                If Not RsAnaS.EOF Then
                    CmdInsCrLa.Parameters("simples") = IIf(BL_HandleNull(RsAnaS!descr_ana_s, "") = "", "", RsAnaS!descr_ana_s)
                    CmdInsCrLa.Parameters("unidade_1") = IIf(BL_HandleNull(RsAnaS!unid_1, "") = "", "", RsAnaS!unid_1)
                    CmdInsCrLa.Parameters("unidade_2") = IIf(BL_HandleNull(RsAnaS!unid_2, "") = "", "", RsAnaS!unid_2)
                End If
            End If
            
            If TemValoresRef Then
                CmdInsCrLa.Parameters("h_ref_min") = IIf(BL_HandleNull(RsVR!h_ref_min, "") = "", "", RsVR!h_ref_min)
                CmdInsCrLa.Parameters("h_ref_max") = IIf(BL_HandleNull(RsVR!h_ref_max, "") = "", "", RsVR!h_ref_max)
                CmdInsCrLa.Parameters("f_ref_min") = IIf(BL_HandleNull(RsVR!f_ref_min, "") = "", "", RsVR!f_ref_min)
                CmdInsCrLa.Parameters("f_ref_max") = IIf(BL_HandleNull(RsVR!f_ref_max, "") = "", "", RsVR!f_ref_max)
            Else
                CmdInsCrLa.Parameters("h_ref_min") = ""
                CmdInsCrLa.Parameters("h_ref_max") = ""
                CmdInsCrLa.Parameters("f_ref_min") = ""
                CmdInsCrLa.Parameters("f_ref_max") = ""
            End If
            
            If CmdInsCrLa.Parameters("h_ref_min") <> "" Or _
                CmdInsCrLa.Parameters("h_ref_max") <> "" Or _
                CmdInsCrLa.Parameters("f_ref_min") <> "" Or _
                CmdInsCrLa.Parameters("f_ref_max") <> "" Or _
                CmdInsCrLa.Parameters("unidade_1") <> "" Or _
                CmdInsCrLa.Parameters("unidade_2") <> "" Then
                CmdInsCrLa.Execute
            End If
            
            RsTmp.MoveNext
        Wend
        RsTmp.Close
        Set RsTmp = Nothing
        
        'Imprime o relat�rio no Crystal Reports
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        
        ProgressBar1.value = ProgressBar1.value + 1
        DoEvents
        
        If OptOrdena(0).value = True Then
            ordenacao = ", SL_CR_LA.PERFIL, SL_CR_LA.COMPLEXA, SL_CR_LA.SIMPLES "
        ElseIf OptOrdena(1).value = True Then
            ordenacao = ", SL_CR_LA.COD_PERFIL, SL_CR_LA.COD_COMPLEXA, SL_CR_LA.COD_SIMPLES "
        ElseIf OptOrdena(2).value = True Then
            ordenacao = ", SL_CR_LA.ORDEM "
        End If
        
        Report.SQLQuery = " SELECT " & vbCrLf & _
                          "     SL_CR_LA.GRUPO, SL_CR_LA.SUBGRUPO, SL_CR_LA.CLASSE, " & vbCrLf & _
                          "     SL_CR_LA.COD_PERFIL, SL_CR_LA.PERFIL, SL_CR_LA.COD_COMPLEXA, " & vbCrLf & _
                          "     SL_CR_LA.COMPLEXA, SL_CR_LA.COD_SIMPLES, SL_CR_LA.SIMPLES, " & vbCrLf & _
                          "     SL_CR_LA.UNIDADE_1, SL_CR_LA.UNIDADE_2, SL_CR_LA.H_REF_MIN, " & vbCrLf & _
                          "     SL_CR_LA.H_REF_MAX, SL_CR_LA.F_REF_MIN, SL_CR_LA.F_REF_MAX,SL_CR_LA.COD_ANA_GH " & vbCrLf & _
                          " FROM " & vbCrLf & _
                          "     SL_CR_LA" & gNumeroSessao & " SL_CR_LA " & vbCrLf & _
                          " ORDER BY " & vbCrLf & _
                          "     SL_CR_LA.GRUPO  " & ordenacao
        
        If CkMembros.value = vbChecked Then
            Report.formulas(0) = "MostraMembros='S'"
        Else
            Report.formulas(0) = "MostraMembros='N'"
        End If
        
        Call BL_ExecutaReport
        Report.PageShow Report.ReportLatestPage
        Call BL_RemoveTabela("SL_CR_LA" & gNumeroSessao)
    
        ProgressBar1.value = ProgressBar1.value + 1
        
        DoEvents
    
    Else
        BG_Mensagem mediMsgBox, "N�o foram encontrados registos para o grupo escolhido!", vbInformation + vbOKOnly, App.ProductName
    End If
    
    Call BL_RemoveTabela("SL_TMP_LA" & gNumeroSessao)
    
End Sub

Private Sub FuncaoImprimirEscRefZonas()

    Dim RegAnaS As ADODB.recordset
    Dim CmdEscRef As ADODB.Command
'   Dim CmdCutOffRef As ADODB.Command
    Dim CmdZonasRef As ADODB.Command
    Dim RegCmd As ADODB.recordset

    Dim sql As String
    Dim continua As Boolean
    
    Dim ActualAnalise As String
    
    Dim cod_grupo As String
    Dim descr_grupo As String
    Dim Cod_SubGrupo As String
    Dim Descr_SubGrupo As String
    Dim cod_ana_s As String
    Dim descr_ana_s As String
    Dim N_Res As String
    Dim unidade As String
    
    Dim cod_ana_ref As String
    Dim PeriodoInf As String
    Dim NPeriodoInf As String
    Dim PeriodoSup As String
    Dim NPeriodoSup As String
    Dim HEMin As String
    Dim HEMax As String
    Dim MEMin As String
    Dim MEMax As String
    Dim ZMin As String
    Dim ZMax As String
    Dim Sexo As String
    Dim Descr_Diag As String
    Dim Descr_Outros As String
    Dim Texto_Livre As String
    Dim NDias As String
        
    Dim total As Integer
    Dim i As Integer
    
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Mapa de Escal�es/Zonas de Refer�ncia") = True Then
        Exit Sub
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListarEscZonas", "Mapa de Escal�es/Zonas de Refer�ncia", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListarEscZonas", "Mapa de Escal�es/Zonas de Refer�ncia", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    'Verifica se selecionou algum grupo
    If OptPorGrupo.value = True Then
        If CbGrupo.ListIndex = -1 Then
            Call BG_Mensagem(mediMsgBox, "Indique o grupo de an�lises pretendido!", vbOKOnly + vbExclamation, App.ProductName)
            If CbGrupo.Enabled = True Then
                CbGrupo.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    'Retorna apenas as an�lises simples que tenham Escal�es\Zonas de Refer�ncia
    Set RegAnaS = New ADODB.recordset
    RegAnaS.CursorLocation = adUseClient
    RegAnaS.CursorType = adOpenForwardOnly
    RegAnaS.LockType = adLockReadOnly
    RegAnaS.ActiveConnection = gConexao
    sql = "SELECT " & _
          "sl_ana_s.gr_ana,sl_ana_s.sgr_ana,sl_ana_s.cod_ana_s,sl_ana_s.descr_ana_s,sl_ana_s.ordem,sl_ana_s.unid_1," & _
          "sl_gr_ana.descr_gr_ana," & _
          "sl_sgr_ana.descr_sgr_ana," & _
          "sl_ana_ref.seq_ana_ref,sl_ana_ref.n_res " & _
          "FROM sl_ana_s,sl_ana_ref," & IIf(gSGBD = gInformix, "OUTER sl_gr_ana,OUTER sl_sgr_ana ", "sl_gr_ana,sl_sgr_ana ") & _
          "WHERE " & _
          "sl_ana_s.cod_ana_s=sl_ana_ref.cod_ana_s " & _
          "AND sl_ana_ref.dt_valid_sup IS NULL AND " & _
          " (sl_ana_s.flg_invisivel is null or sl_ana_s.flg_invisivel = 0) AND "
          Select Case gSGBD
            Case gOracle
                sql = sql & "sl_ana_s.gr_ana=sl_gr_ana.cod_gr_ana(+) AND sl_ana_s.sgr_ana=sl_sgr_ana.cod_sgr_ana(+) "
            Case gInformix
                sql = sql & " sl_ana_s.gr_ana=sl_gr_ana.cod_gr_ana AND sl_ana_s.sgr_ana=sl_sgr_ana.cod_sgr_ana "
            Case gSqlServer
                sql = sql & " sl_ana_s.gr_ana*=sl_gr_ana.cod_gr_ana AND sl_ana_s.sgr_ana*=sl_sgr_ana.cod_sgr_ana "
          End Select
          If OptPorGrupo.value = True Then
             sql = sql & "AND sl_ana_s.gr_ana=" & BL_TrataStringParaBD(Trim(EcCodGrupo.Text))
          End If
          sql = sql & " ORDER BY sl_ana_s.gr_ana,sl_ana_s.sgr_ana,sl_ana_s.ordem,sl_ana_ref.n_res "
    RegAnaS.Source = sql
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RegAnaS.Open
    total = RegAnaS.RecordCount
    If total = 0 Then
        RegAnaS.Close
        Set RegAnaS = Nothing
        Call BG_Mensagem(mediMsgBox, "N�o existem an�lises para iniciar impress�o!", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    End If
            
    'Descri��es dos CUT-OFF:
'    Set CmdCutOffRef = New ADODB.Command
'    CmdCutOffRef.ActiveConnection = gConexao
'    CmdCutOffRef.CommandType = adCmdText
'    SQL = "SELECT " & _
'          "sl_cut_off.val_neg_min,sl_cut_off.val_neg_max,sl_cut_off.t_comp_nmin,sl_cut_off.t_comp_nmax,sl_cut_off.val_pos_min,sl_cut_off.val_pos_max,sl_cut_off.t_comp_pmin,sl_cut_off.t_comp_pmax,sl_cut_off.val_duv_min,sl_cut_off.val_duv_max,sl_cut_off.t_comp_dmin,sl_cut_off.t_comp_dmax " & _
'          "FROM sl_cut_off " & _
'          "WHERE sl_cut_off.cod_ana_ref=?"
'    CmdCutOffRef.CommandText = SQL
'    CmdCutOffRef.Prepared = True
'    CmdCutOffRef.Parameters.Append CmdCutOffRef.CreateParameter("SEQ_ANA_REF", adInteger, adParamInput, 9)
            
    
    'Descri��es dos Escal�es de Refer�ncia:
    'T_esc_inf=>Tipo de per�odo (Ano,M�s,Dias)
    'Esc_inf=>N� de per�odos
    'Esc_inf_dias=>N� equivalente de dias
    Set CmdEscRef = New ADODB.Command
    CmdEscRef.ActiveConnection = gConexao
    CmdEscRef.CommandType = adCmdText
    sql = "SELECT " & _
          "A.descr_t_data PERIODOINF,sl_esc_ref.esc_inf NPERIODOINF,sl_esc_ref.esc_inf_dias," & _
          "B.descr_t_data PERIODOSUP,sl_esc_ref.esc_sup NPERIODOSUP,sl_esc_ref.esc_sup_dias," & _
          "sl_esc_ref.f_ref_min AS MEMIN,sl_esc_ref.f_ref_max AS MEMAX,sl_esc_ref.h_ref_min AS HEMIN,sl_esc_ref.h_ref_max AS HEMAX " & _
          "FROM sl_esc_ref," & IIf(gSGBD = gInformix, "OUTER sl_tbf_t_data A,OUTER sl_tbf_t_data B ", "sl_tbf_t_data A,sl_tbf_t_data B ") & _
          "WHERE " & _
          "sl_esc_ref.cod_ana_ref=? AND "
          Select Case gSGBD
            Case gOracle
                sql = sql & "sl_esc_ref.t_esc_inf=A.cod_t_data(+) AND sl_esc_ref.t_esc_sup=B.cod_t_data(+) "
            Case gInformix
                sql = sql & "sl_esc_ref.t_esc_inf=A.cod_t_data AND sl_esc_ref.t_esc_sup=B.cod_t_data "
            Case gSqlServer
                sql = sql & "sl_esc_ref.t_esc_inf*=A.cod_t_data AND sl_esc_ref.t_esc_sup*=B.cod_t_data "
         End Select
          sql = sql & "ORDER BY sl_esc_ref.esc_inf_dias"
    CmdEscRef.CommandText = sql
    CmdEscRef.Prepared = True
    CmdEscRef.Parameters.Append CmdEscRef.CreateParameter("SEQ_ANA_REF", adInteger, adParamInput, 9)


    'Descri��es das ZONAS (Inclu�ndo Outras Zonas (sl_or_zonas) - Ex� "N�o Fumadores")
    'T_idade_inf e T_idade_sup=C�digo para Anos,Meses ou Dias
    Set CmdZonasRef = New ADODB.Command
    CmdZonasRef.ActiveConnection = gConexao
    CmdZonasRef.CommandType = adCmdText
    sql = "SELECT " & _
           "A.descr_t_data PERIODOINF,sl_zona_ref.idade_inf NPERIODOINF,sl_zona_ref.idade_inf_dias," & _
           "B.descr_t_data PERIODOSUP,sl_zona_ref.idade_sup NPERIODOSUP,sl_zona_ref.idade_sup_dias," & _
           "sl_zona_ref.ref_min ZMIN,sl_zona_ref.ref_max ZMAX," & _
           "sl_tbf_sexo.descr_sexo," & _
           "sl_diag.descr_diag," & _
           "sl_or_zonas.descr_or_zonas," & _
           "sl_zona_ref.txt_livre " & _
           "FROM sl_zona_ref," & IIf(gSGBD = gInformix, "OUTER sl_or_zonas,OUTER sl_tbf_t_data A,OUTER sl_tbf_t_data B,OUTER sl_diag,OUTER sl_tbf_sexo ", "sl_or_zonas,sl_tbf_t_data A,sl_tbf_t_data B,sl_diag,sl_tbf_sexo ") & _
           "WHERE " & _
           "sl_zona_ref.cod_ana_ref=? AND "
         Select Case gSGBD
            Case gOracle
                sql = sql & "sl_zona_ref.cod_or_zonas=sl_or_zonas.cod_or_zonas(+) AND sl_zona_ref.t_idade_inf=A.cod_t_data(+) AND sl_zona_ref.t_idade_sup=B.cod_t_data(+) AND sl_zona_ref.cod_diag=sl_diag.cod_diag(+) AND sl_zona_ref.sexo=sl_tbf_sexo.cod_sexo(+) "
            Case gInformix
                sql = sql & "sl_zona_ref.cod_or_zonas=sl_or_zonas.cod_or_zonas AND sl_zona_ref.t_idade_inf=A.cod_t_data AND sl_zona_ref.t_idade_sup=B.cod_t_data AND sl_zona_ref.cod_diag=sl_diag.cod_diag AND sl_zona_ref.sexo=sl_tbf_sexo.cod_sexo "
            Case gSqlServer
                sql = sql & "sl_zona_ref.cod_or_zonas*=sl_or_zonas.cod_or_zonas AND sl_zona_ref.t_idade_inf*=A.cod_t_data AND sl_zona_ref.t_idade_sup*=B.cod_t_data AND sl_zona_ref.cod_diag*=sl_diag.cod_diag AND sl_zona_ref.sexo*=sl_tbf_sexo.cod_sexo"
         End Select
         sql = sql & " ORDER BY sl_zona_ref.idade_inf_dias"
    CmdZonasRef.CommandText = sql
    CmdZonasRef.Prepared = True
    CmdZonasRef.Parameters.Append CmdZonasRef.CreateParameter("SEQ_ANA_REF", adInteger, adParamInput, 9)

    'Cria a tabela com os valores
    Call Cria_TabelaEscZonas
    
    ActualAnalise = ""
    For i = 1 To total
        'An�lises
        If "" & RegAnaS!cod_ana_s <> ActualAnalise Then
            ActualAnalise = "" & RegAnaS!cod_ana_s
            'Campos obrigat�rios
            cod_ana_s = BL_TrataStringParaBD("" & RegAnaS!cod_ana_s)
            descr_ana_s = BL_TrataStringParaBD("" & RegAnaS!descr_ana_s)
            N_Res = "" & RegAnaS!N_Res
            unidade = BL_TrataStringParaBD("" & RegAnaS!unid_1)
            'Campos opcionais
            cod_grupo = IIf(BL_HandleNull(RegAnaS!gr_ana, "") = "", "Null", BL_TrataStringParaBD("" & RegAnaS!gr_ana))
            descr_grupo = IIf(BL_HandleNull(RegAnaS!descr_gr_ana, "") = "", "Null", BL_TrataStringParaBD("" & RegAnaS!descr_gr_ana))
            Cod_SubGrupo = IIf(BL_HandleNull(RegAnaS!sgr_ana, "") = "", "Null", BL_TrataStringParaBD("" & RegAnaS!sgr_ana))
            Descr_SubGrupo = IIf(BL_HandleNull(RegAnaS!descr_sgr_ana, "") = "", "Null", BL_TrataStringParaBD("" & RegAnaS!descr_sgr_ana))
            'Introduz os valores
            sql = "INSERT INTO SL_CR_REFANA" & gNumeroSessao & _
                  " (COD_GRUPO,DESCR_GRUPO,COD_SUBGRUPO,DESCR_SUBGRUPO,COD_ANA_S,DESCR_ANA_S,N_RES, UNIDADE) " & _
                  " VALUES (" & cod_grupo & "," & descr_grupo & "," & Cod_SubGrupo & "," & Descr_SubGrupo & "," & cod_ana_s & "," & descr_ana_s & "," & N_Res & "," & unidade & ")"
            Call BG_ExecutaQuery_ADO(sql)
        End If
        
        'Escal�es da an�lise actual
        CmdEscRef.Parameters("SEQ_ANA_REF").value = RegAnaS!seq_ana_ref
        Set RegCmd = CmdEscRef.Execute
        While Not RegCmd.EOF
            'Campos obrigat�rios
            cod_ana_s = BL_TrataStringParaBD("" & RegAnaS!cod_ana_s)
            cod_ana_ref = "" & RegAnaS!seq_ana_ref
            PeriodoInf = BL_TrataStringParaBD("" & RegCmd!PeriodoInf)
            NPeriodoInf = RegCmd!NPeriodoInf
            PeriodoSup = BL_TrataStringParaBD("" & RegCmd!PeriodoSup)
            NPeriodoSup = RegCmd!NPeriodoSup
            NDias = RegCmd!esc_inf_dias
            
            'Campos opcionais
            HEMin = IIf(BL_HandleNull(RegCmd!HEMin, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!HEMin))
            HEMax = IIf(BL_HandleNull(RegCmd!HEMax, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!HEMax))
            MEMin = IIf(BL_HandleNull(RegCmd!MEMin, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!MEMin))
            MEMax = IIf(BL_HandleNull(RegCmd!MEMax, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!MEMax))
            'Introduz os valores
            sql = "INSERT INTO SL_CR_ESCZON" & gNumeroSessao & _
                  " (COD_ANA_S,COD_ANA_REF,TIPO,PERIODOINF,NPERIODOINF,PERIODOSUP,NPERIODOSUP,HEMIN,HEMAX,MEMIN,MEMAX,NDIAS) " & _
                  " VALUES (" & cod_ana_s & "," & cod_ana_ref & ",'E'," & PeriodoInf & "," & NPeriodoInf & "," & PeriodoSup & "," & NPeriodoSup & "," & HEMin & "," & HEMax & "," & MEMin & "," & MEMax & "," & NDias & ")"
            Call BG_ExecutaQuery_ADO(sql)
            RegCmd.MoveNext
        Wend
        RegCmd.Close
        Set RegCmd = Nothing
        
        'Zonas
        CmdZonasRef.Parameters("SEQ_ANA_REF").value = RegAnaS!seq_ana_ref
        Set RegCmd = CmdZonasRef.Execute
        While Not RegCmd.EOF
            'Campos Obrigat�rios
            cod_ana_s = BL_TrataStringParaBD("" & RegAnaS!cod_ana_s)
            cod_ana_ref = "" & RegAnaS!seq_ana_ref
            'Campos Opcionais
            If "" & RegCmd!PeriodoInf = gT_Crianca Or "" & RegCmd!PeriodoInf = gT_Adulto Then
                PeriodoInf = ""
                NPeriodoInf = ""
                NPeriodoSup = ""
                PeriodoSup = IIf(BL_HandleNull(RegCmd!PeriodoSup, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!PeriodoSup))
            Else
                PeriodoInf = IIf(BL_HandleNull(RegCmd!PeriodoInf, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!PeriodoInf))
                NPeriodoInf = IIf(BL_HandleNull(RegCmd!NPeriodoInf, "") = "", "Null", "" & RegCmd!NPeriodoInf)
                NPeriodoSup = IIf(BL_HandleNull(RegCmd!NPeriodoSup, "") = "", "Null", "" & RegCmd!NPeriodoSup)
                PeriodoSup = IIf(BL_HandleNull(RegCmd!PeriodoSup, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!PeriodoSup))
            End If
            NDias = IIf(BL_HandleNull(RegCmd!idade_inf_dias, "") = "", "Null", RegCmd!idade_inf_dias)
            
            ZMin = IIf(BL_HandleNull(RegCmd!ZMin, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!ZMin))
            ZMax = IIf(BL_HandleNull(RegCmd!ZMax, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!ZMax))
            Sexo = IIf(BL_HandleNull(RegCmd!descr_sexo, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!descr_sexo))
            Descr_Diag = IIf(BL_HandleNull(RegCmd!Descr_Diag, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!Descr_Diag))
            Descr_Outros = IIf(BL_HandleNull(RegCmd!descr_or_zonas, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!descr_or_zonas))
            Texto_Livre = IIf(BL_HandleNull(RegCmd!txt_livre, "") = "", "Null", BL_TrataStringParaBD("" & RegCmd!txt_livre))
            
            'Introduz os valores
            sql = "INSERT INTO SL_CR_ESCZON" & gNumeroSessao & _
                  " (COD_ANA_S,COD_ANA_REF,TIPO,PERIODOINF,NPERIODOINF,PERIODOSUP,NPERIODOSUP,ZMIN,ZMAX,SEXO,DESCR_DIAG,DESCR_OUTROS,TEXTO_LIVRE,NDIAS) " & _
                  " VALUES (" & cod_ana_s & "," & cod_ana_ref & ",'Z'," & PeriodoInf & "," & NPeriodoInf & "," & PeriodoSup & "," & NPeriodoSup & "," & ZMin & "," & ZMax & "," & Sexo & "," & Descr_Diag & "," & Descr_Outros & "," & Texto_Livre & "," & NDias & ")"
            Call BG_ExecutaQuery_ADO(sql)
            RegCmd.MoveNext
        Wend
        RegCmd.Close
        Set RegCmd = Nothing
        
        RegAnaS.MoveNext
    Next i
    
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    sql = "SELECT " & _
          "SL_CR_REFANA.COD_GRUPO,SL_CR_REFANA.DESCR_GRUPO,SL_CR_REFANA.COD_SUBGRUPO,SL_CR_REFANA.DESCR_SUBGRUPO,SL_CR_REFANA.COD_ANA_S,SL_CR_REFANA.DESCR_ANA_S," & _
          "SL_CR_ESCZON.Tipo,SL_CR_ESCZON.PeriodoInf,SL_CR_ESCZON.NPeriodoInf,SL_CR_ESCZON.PeriodoSup,SL_CR_ESCZON.NPeriodoSup,SL_CR_ESCZON.HEMin,SL_CR_ESCZON.HEMax,SL_CR_ESCZON.MEMin,SL_CR_ESCZON.MEMax,SL_CR_ESCZON.ZMin,SL_CR_ESCZON.ZMax,SL_CR_ESCZON.Sexo,SL_CR_ESCZON.Descr_Diag,SL_CR_ESCZON.Descr_Outros,SL_CR_ESCZON.Texto_Livre " & _
          "FROM " & _
          "SL_CR_REFANA" & gNumeroSessao & " SL_CR_REFANA," & _
          "SL_CR_ESCZON" & gNumeroSessao & " SL_CR_ESCZON " & _
          "WHERE " & _
          "SL_CR_REFANA.cod_ana_s = SL_CR_ESCZON.cod_ana_s " & _
          "ORDER BY SL_CR_REFANA.COD_GRUPO,SL_CR_REFANA.COD_SUBGRUPO,SL_CR_REFANA.COD_ANA_S,SL_CR_REFANA.N_RES,SL_CR_ESCZON.TIPO,SL_CR_ESCZON.NDIAS"
    Report.SQLQuery = sql
    
    Call BL_ExecutaReport
    
    Report.PageShow Report.ReportLatestPage
        
    Call BL_RemoveTabela("SL_CR_REFANA" & gNumeroSessao)
    Call BL_RemoveTabela("SL_CR_ESCZON" & gNumeroSessao)
    
End Sub

Private Sub Cria_TabelaEscZonas()
    
    Dim analises(1 To 8) As DefTable
    Dim EscZonas(1 To 18) As DefTable
        
    'AN�LISES
    analises(1).NomeCampo = "COD_GRUPO"
    analises(1).tipo = "STRING"
    analises(1).tamanho = 5
    
    analises(2).NomeCampo = "DESCR_GRUPO"
    analises(2).tipo = "STRING"
    analises(2).tamanho = 40
    
    analises(3).NomeCampo = "COD_SUBGRUPO"
    analises(3).tipo = "STRING"
    analises(3).tamanho = 5
    
    analises(4).NomeCampo = "DESCR_SUBGRUPO"
    analises(4).tipo = "STRING"
    analises(4).tamanho = 80
    
    analises(5).NomeCampo = "COD_ANA_S"
    analises(5).tipo = "STRING"
    analises(5).tamanho = 10
    
    analises(6).NomeCampo = "DESCR_ANA_S"
    analises(6).tipo = "STRING"
    analises(6).tamanho = 65
    
    analises(7).NomeCampo = "N_RES"
    analises(7).tipo = "INTEGER"
    
    analises(8).NomeCampo = "UNIDADE"
    analises(8).tipo = "STRING"
    analises(8).tamanho = 20
    
    
    '------------------------------------------------------------------------------------------
    
    'ESCAL�ES E EscZonas DE REFER�NCIA
    EscZonas(1).NomeCampo = "COD_ANA_S"
    EscZonas(1).tipo = "STRING"
    EscZonas(1).tamanho = 10
    
    EscZonas(2).NomeCampo = "COD_ANA_REF"
    EscZonas(2).tipo = "INTEGER"
    
    EscZonas(3).NomeCampo = "TIPO"
    EscZonas(3).tipo = "STRING"
    EscZonas(3).tamanho = 1
    
    EscZonas(4).NomeCampo = "PERIODOINF"
    EscZonas(4).tipo = "STRING"
    EscZonas(4).tamanho = 20
    
    EscZonas(5).NomeCampo = "NPERIODOINF"
    EscZonas(5).tipo = "INTEGER"
        
    EscZonas(6).NomeCampo = "PERIODOSUP"
    EscZonas(6).tipo = "STRING"
    EscZonas(6).tamanho = 20
    
    EscZonas(7).NomeCampo = "NPERIODOSUP"
    EscZonas(7).tipo = "INTEGER"
    
    EscZonas(8).NomeCampo = "HEMIN"
    EscZonas(8).tipo = "STRING"
    EscZonas(8).tamanho = 20
    
    EscZonas(9).NomeCampo = "HEMAX"
    EscZonas(9).tipo = "STRING"
    EscZonas(9).tamanho = 20
    
    EscZonas(10).NomeCampo = "MEMIN"
    EscZonas(10).tipo = "STRING"
    EscZonas(10).tamanho = 20
    
    EscZonas(11).NomeCampo = "MEMAX"
    EscZonas(11).tipo = "STRING"
    EscZonas(11).tamanho = 20
    
    EscZonas(12).NomeCampo = "ZMIN"
    EscZonas(12).tipo = "STRING"
    EscZonas(12).tamanho = 20
    
    EscZonas(13).NomeCampo = "ZMAX"
    EscZonas(13).tipo = "STRING"
    EscZonas(13).tamanho = 20
    
    EscZonas(14).NomeCampo = "SEXO"
    EscZonas(14).tipo = "STRING"
    EscZonas(14).tamanho = 20
    
    EscZonas(15).NomeCampo = "DESCR_DIAG"
    EscZonas(15).tipo = "STRING"
    EscZonas(15).tamanho = 50
    
    EscZonas(16).NomeCampo = "DESCR_OUTROS"
    EscZonas(16).tipo = "STRING"
    EscZonas(16).tamanho = 40
    
    EscZonas(17).NomeCampo = "TEXTO_LIVRE"
    EscZonas(17).tipo = "STRING"
    EscZonas(17).tamanho = 2500
    
    EscZonas(18).NomeCampo = "NDIAS"
    EscZonas(18).tipo = "INTEGER"
    
    
    '------------------------------------------------------------------------------------------
    
    Call BL_CriaTabela("SL_CR_REFANA" & gNumeroSessao, analises)
    Call BL_CriaTabela("SL_CR_ESCZON" & gNumeroSessao, EscZonas)
    
End Sub



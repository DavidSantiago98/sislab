VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCQRegras 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCQRegras"
   ClientHeight    =   6900
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8040
   Icon            =   "FormCQRegras.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   8040
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PictureListColor 
      Height          =   495
      Left            =   7200
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   7
      Top             =   6240
      Width           =   615
   End
   Begin VB.TextBox EcSeqRegra 
      Height          =   285
      Left            =   480
      TabIndex        =   6
      Top             =   5880
      Width           =   855
   End
   Begin VB.TextBox EcCodControlo 
      Height          =   285
      Left            =   3480
      TabIndex        =   5
      Top             =   5880
      Width           =   855
   End
   Begin VB.ComboBox CbAccoes 
      Height          =   315
      Left            =   720
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   600
      Width           =   2895
   End
   Begin VB.ComboBox CbRegras 
      Height          =   315
      Left            =   720
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   6375
   End
   Begin MSComctlLib.ListView LvAnalise 
      Height          =   3495
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   6165
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label Label2 
      Caption         =   "Ac��o"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Regra"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormCQRegras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rsAna As ADODB.recordset


Private Type regras
    seq_regra As Integer
    cod_regra As String
    descr_regra As String
    cod_accao As String
    descr_accao As String
End Type
Dim estrutRegra() As regras
Dim totalRegra As Integer
Dim controlo As String

Const cLightGray = &H808080

Const cLightGreen = &HC0FFC0
Const cLightYellow = &H80000018
Const cWhite = &HFFFFFF
Const cLightBlue = &HFCE7D8

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub
Sub EventoUnload()
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    If Not rsAna Is Nothing Then
        rsAna.Close
        Set rsAna = Nothing
    End If
    Set gFormActivo = FormCQcontrolos
    Set FormCQRegras = Nothing
End Sub

Sub Inicializacoes()

    Me.caption = "Regras Westgard"
    Me.left = 5
    Me.top = 5
    Me.Width = 8130
    Me.Height = 5385 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_cq_regras"
    Set CampoDeFocus = CbRegras
    
    NumCampos = 4
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_controlo"
    CamposBD(1) = "cod_regra"
    CamposBD(2) = "cod_accao"
    CamposBD(3) = "seq_regra"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodControlo
    Set CamposEc(1) = CbRegras
    Set CamposEc(2) = CbAccoes
    Set CamposEc(3) = EcSeqRegra
   
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "Controlo"
    TextoCamposObrigatorios(1) = "Regra"
    TextoCamposObrigatorios(2) = "Ac��o "
        ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_regra"
    Set ChaveEc = EcSeqRegra
        
End Sub
Sub DefTipoCampos()
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    InicializaTreeView
    
End Sub
Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_regras_westgard", "cod_regra", "descr_regra", CbRegras
    BG_PreencheComboBD_ADO "sl_accoes_interferencias", "cod_accao", "descr_accao", CbAccoes

End Sub

Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        LvAnalise.ListItems.Clear
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        LvAnalise.ListItems.Clear
        CampoDeFocus.SetFocus
        If Not rsAna Is Nothing Then
            rsAna.Close
            Set rsAna = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Sub LimpaCampos()
    Me.SetFocus
    CbAccoes.ListIndex = mediComboValorNull
    CbRegras.ListIndex = mediComboValorNull
    EcSeqRegra = ""
End Sub
Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
        LimpaCampos
        FuncaoProcurar
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(ChaveEc)
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    rsAna.Requery
    PreencheCampos
End Sub


Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcSeqRegra = BG_DaMAX(NomeTabela, "seq_regra") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer remover este registo ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub
Private Sub BD_Delete()
    Dim sSql As String
    Dim sCondicao As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    gSQLISAM = 0
    
    sCondicao = "seq_regra=" & EcSeqRegra
    sSql = "DELETE FROM " & NomeTabela & " WHERE " & sCondicao
    BG_ExecutaQuery_ADO sSql
    
    BG_Trata_BDErro
        
    MarcaLocal = rsAna.Bookmark
    rsAna.Requery
        
    If rsAna.BOF And rsAna.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rsAna.EOF Then
        rsAna.MovePrevious
    Else
        If MarcaLocal <> 1 Then
            rsAna.Bookmark = MarcaLocal - 1
        End If
    End If
    
    LimpaCampos
    PreencheCampos
    

End Sub
Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rsAna = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY SEQ_REGRA ASC "
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    
    rsAna.Open CriterioTabela, gConexao
    
    If rsAna.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Procurar", "InActivo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rsAna.RecordCount = 0 Then
        FuncaoLimpar
    Else
        PreencheLV
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoAnterior()
    Exit Sub
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rsAna.MovePrevious
    
    If rsAna.BOF Then
        rsAna.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoSeguinte()
    Exit Sub
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rsAna.MoveNext
    
    If rsAna.EOF Then
        rsAna.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        BL_FimProcessamento Me
        PreencheCampos
    End If
End Sub


Private Sub InicializaTreeView()
    With LvAnalise
        .ColumnHeaders.Add(, , "N�", 500, lvwColumnLeft).Key = "SEQ_REGRA"
        .ColumnHeaders.Add(, , "Descr. Regra", 5000, lvwColumnLeft).Key = "DESCR_REGRA"
        .ColumnHeaders.Add(, , "Descr. Acc��o", 2000, lvwColumnLeft).Key = "DESCR_ACCAO"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .Checkboxes = False
        .FullRowSelect = True
        .ForeColor = cLightGray
        .MultiSelect = False
    End With
    ColocaCores LvAnalise, PictureListColor, cLightGreen, cWhite
    Exit Sub

End Sub

Public Sub ColocaCores(lv As ListView, pb As PictureBox, cor1 As Long, Optional cor2 As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo TrataErro
    
    lColor1 = cor1
    lColor2 = cor2
    lv.Picture = LoadPicture("")
    lv.Refresh
    pb.Cls
    pb.AutoRedraw = True
    pb.BorderStyle = vbBSNone
    pb.ScaleMode = vbTwips
    pb.Visible = False
    lv.PictureAlignment = lvwTile
    pb.Font = lv.Font
    pb.top = lv.top
    pb.Font = lv.Font
    With pb.Font
        .Size = lv.Font.Size + 2
        .Bold = lv.Font.Bold
        .Charset = lv.Font.Charset
        .Italic = lv.Font.Italic
        .Name = lv.Font.Name
        .Strikethrough = lv.Font.Strikethrough
        .Underline = lv.Font.Underline
        .Weight = lv.Font.Weight
    End With
    pb.Refresh
    lLineHeight = pb.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = lv.Width
    pb.Height = lBarHeight * 2
    pb.Width = lBarWidth
    pb.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    pb.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    pb.AutoSize = True
    lv.Picture = pb.Image
    lv.Refresh
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "ColocaCores", False
    Exit Sub
End Sub



Private Sub PreencheLV()
    Dim i As Integer
    LvAnalise.ListItems.Clear
    totalRegra = 0
    ReDim estrutRegra(0)
    While Not rsAna.EOF
        totalRegra = totalRegra + 1
        ReDim Preserve estrutRegra(totalRegra)
        estrutRegra(totalRegra).cod_accao = BL_HandleNull(rsAna!cod_accao, -1)
        estrutRegra(totalRegra).cod_regra = BL_HandleNull(rsAna!cod_regra, -1)
        estrutRegra(totalRegra).descr_accao = BL_SelCodigo("SL_ACCOES_INTERFERENCIAS", "DESCR_ACCAO", "COD_ACCAO", estrutRegra(totalRegra).cod_accao, "V")
        estrutRegra(totalRegra).descr_regra = BL_SelCodigo("sl_tbf_regras_westgard", "descr_regra", "cod_regra", estrutRegra(totalRegra).cod_regra, "V")
        estrutRegra(totalRegra).seq_regra = BL_HandleNull(rsAna!seq_regra, -1)
        
        With LvAnalise.ListItems.Add(totalRegra, "KEY_" & rsAna!seq_regra, rsAna!seq_regra)
            .ListSubItems.Add , , estrutRegra(totalRegra).descr_regra
            .ListSubItems.Add , , estrutRegra(totalRegra).descr_accao
        End With
        rsAna.MoveNext
    Wend
    rsAna.MoveFirst
End Sub



Private Sub LvAnalise_Click()
    Dim i As Integer
    LimpaCampos
    If LvAnalise.ListItems.Count = 0 Then Exit Sub
    EcSeqRegra = estrutRegra(LvAnalise.SelectedItem.Index).seq_regra
    For i = 0 To CbAccoes.ListCount - 1
        If CbAccoes.ItemData(i) = estrutRegra(LvAnalise.SelectedItem.Index).cod_accao Then
            CbAccoes.ListIndex = i
            Exit For
        End If
    Next
    For i = 0 To CbRegras.ListCount - 1
        If CbRegras.ItemData(i) = estrutRegra(LvAnalise.SelectedItem.Index).cod_regra Then
            CbRegras.ListIndex = i
            Exit For
        End If
    Next
    estado = 2
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
End Sub


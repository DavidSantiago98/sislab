Attribute VB_Name = "Dicionario"
Option Explicit

Private Type textos
    id As Long
    texto As String
End Type
Public DicEstrutTextos() As textos
Public DicTotalTextos As Long

Private Type traducao
    cultura As String
    id_texto As Long
    Texto_traduzido As String
End Type
Public DicEstrutTraducao() As traducao
Public DicTotalTraducao As Long

Private Type forms
    cod_form As String
    id_texto As Long
End Type
Public DicEstrutForms()  As forms
Public DicTotalForms As Integer

Private Type menus
    cod_pai As String
    indice_cod_pai As String
    Codigo As String
    indice As Integer
    descricao As String
    id_texto As Long
    ordem As Integer
End Type
Public DicEstrutMenusCodif() As menus
Public DicTotalMenusCodif As Long

Private Type Objecto
    cod_form As String
    tipo As String
    cod_objecto As String
    indice_objecto As Integer
    id_texto_caption As Long
    id_texto_tooltip As Long
End Type
Public DicEstrutObjectos() As Objecto
Public DicTotalObjectos As Long


' ---------------------------------------------------------------------------------------------------------

' METODO USADO PARA INSERIR TEXTOS NA BD. VERIFICA SE EXISTE, SENAO INSERE

' ---------------------------------------------------------------------------------------------------------
Public Function DIC_DevolveIndiceTexto(texto As String) As Long
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    
    DIC_DevolveIndiceTexto = mediComboValorNull
    If texto = "" Then
        Exit Function
    End If
    sSql = "SELECT * FROM sl_dic_textos WHERE trim(texto_portugues) = " & BL_TrataStringParaBD(Trim(texto))
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount = 1 Then
        DIC_DevolveIndiceTexto = BL_HandleNull(rsDic!id, mediComboValorNull)
    Else
        DIC_DevolveIndiceTexto = DIC_InsereNovoTexto(texto)
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Function
TrataErro:
    DIC_DevolveIndiceTexto = mediComboValorNull
    BG_LogFile_Erros "DIC_DevolveIndiceTexto " & Err.Description, "DIC", "DIC_DevolveIndiceTexto"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' INSERE NOVO TEXTO NA BD

' ---------------------------------------------------------------------------------------------------------
Private Function DIC_InsereNovoTexto(texto As String) As Long
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    Dim indice As Long
    Dim reg As Integer
    
    DIC_InsereNovoTexto = mediComboValorNull
    sSql = "SELECT seq_dic_textos.nextval proximo FROM dual "
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount = 1 Then
        indice = BL_HandleNull(rsDic!proximo, mediComboValorNull)
        sSql = "INSERT INTO sl_dic_textos (id, texto_portugues,dt_cri,user_cri) VALUES("
        sSql = sSql & indice & "," & BL_TrataStringParaBD(texto) & ", sysdate, " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
        reg = BG_ExecutaQuery_ADO(sSql)
        If reg = 1 Then
            DIC_InsereNovoTexto = indice
        End If
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Function
TrataErro:
    DIC_InsereNovoTexto = mediComboValorNull
    BG_LogFile_Erros "DIC_InsereNovoTexto " & Err.Description, "DIC", "DIC_InsereNovoTexto"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' NO ARRANQUE DA APLICACAO. CARREGA ESTRUTURAS PARA SEREM USADAS NA TRADU��O

' ---------------------------------------------------------------------------------------------------------
Public Sub DIC_InicializaEstruturasGlobais()
    DIC_InicializaTextos
    DIC_InicializaForms
    DIC_InicializaMenus
    DIC_InicializaTraducoes
    DIC_InicializaObjectos
End Sub

' ---------------------------------------------------------------------------------------------------------

' CARREGA ESTRUTURA DE TEXTOS

' ---------------------------------------------------------------------------------------------------------
Private Sub DIC_InicializaTextos()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_dic_textos "
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount >= 1 Then
        DicTotalTextos = 0
        ReDim DicEstrutTextos(0)
        While Not rsDic.EOF
            DicTotalTextos = DicTotalTextos + 1
            ReDim Preserve DicEstrutTextos(DicTotalTextos)
            DicEstrutTextos(DicTotalTextos).id = BL_HandleNull(rsDic!id, mediComboValorNull)
            DicEstrutTextos(DicTotalTextos).texto = BL_HandleNull(rsDic!texto_portugues, "")
            rsDic.MoveNext
        Wend
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIC_InicializaTextos " & Err.Description, "DIC", "DIC_InicializaTextos"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' CARREGA ESTRUTURA DE MENUS

' ---------------------------------------------------------------------------------------------------------
Private Sub DIC_InicializaMenus()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_menus "
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount >= 1 Then
        DicTotalMenusCodif = 0
        ReDim DicEstrutMenusCodif(DicTotalMenusCodif)
        While Not rsDic.EOF
            DicTotalMenusCodif = DicTotalMenusCodif + 1
            ReDim Preserve DicEstrutMenusCodif(DicTotalMenusCodif)
            DicEstrutMenusCodif(DicTotalMenusCodif).cod_pai = BL_HandleNull(rsDic!cod_pai, "")
            DicEstrutMenusCodif(DicTotalMenusCodif).Codigo = BL_HandleNull(rsDic!Codigo, "")
            DicEstrutMenusCodif(DicTotalMenusCodif).descricao = BL_HandleNull(rsDic!descricao, "")
            DicEstrutMenusCodif(DicTotalMenusCodif).id_texto = BL_HandleNull(rsDic!id_texto, mediComboValorNull)
            DicEstrutMenusCodif(DicTotalMenusCodif).indice = BL_HandleNull(rsDic!indice, mediComboValorNull)
            DicEstrutMenusCodif(DicTotalMenusCodif).ordem = BL_HandleNull(rsDic!ordem, mediComboValorNull)
            DicEstrutMenusCodif(DicTotalMenusCodif).indice_cod_pai = BL_HandleNull(rsDic!indice_cod_pai, mediComboValorNull)
            rsDic.MoveNext
        Wend
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIC_InicializaMenus " & Err.Description, "DIC", "DIC_InicializaMenus"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' CARREGA ESTRUTURA DE FORMS

' ---------------------------------------------------------------------------------------------------------
Private Sub DIC_InicializaForms()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_forms "
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount >= 1 Then
        DicTotalForms = 0
        ReDim DicEstrutForms(DicTotalForms)
        While Not rsDic.EOF
            DicTotalForms = DicTotalForms + 1
            ReDim Preserve DicEstrutForms(DicTotalForms)
            DicEstrutForms(DicTotalForms).cod_form = BL_HandleNull(rsDic!cod_form, "")
            DicEstrutForms(DicTotalForms).id_texto = BL_HandleNull(rsDic!id_texto, mediComboValorNull)
            rsDic.MoveNext
        Wend
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIC_InicializaMenus " & Err.Description, "DIC", "DIC_InicializaMenus"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' CARREGA ESTRUTURA DE TRADUCOES

' ---------------------------------------------------------------------------------------------------------
Private Sub DIC_InicializaTraducoes()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_dic_traducoes ORDER BY cultura "
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount >= 1 Then
        DicTotalTraducao = 0
        ReDim DicEstrutTraducao(DicTotalTraducao)
        While Not rsDic.EOF
            DicTotalTraducao = DicTotalTraducao + 1
            ReDim Preserve DicEstrutTraducao(DicTotalTraducao)
            DicEstrutTraducao(DicTotalTraducao).cultura = BL_HandleNull(rsDic!cultura, "")
            DicEstrutTraducao(DicTotalTraducao).id_texto = BL_HandleNull(rsDic!id_texto, mediComboValorNull)
            DicEstrutTraducao(DicTotalTraducao).Texto_traduzido = BL_HandleNull(rsDic!Texto_traduzido, "")
            rsDic.MoveNext
        Wend
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIC_InicializaTraducoes " & Err.Description, "DIC", "DIC_InicializaTraducoes"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' CARREGA ESTRUTURA DE OBJECTOS

' ---------------------------------------------------------------------------------------------------------
Private Sub DIC_InicializaObjectos()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_objectos ORDER BY cod_form, tipo, cod_objecto"
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount >= 1 Then
        DicTotalObjectos = 0
        ReDim DicEstrutObjectos(DicTotalObjectos)
        While Not rsDic.EOF
            DicTotalObjectos = DicTotalObjectos + 1
            ReDim Preserve DicEstrutObjectos(DicTotalObjectos)
            DicEstrutObjectos(DicTotalObjectos).cod_form = BL_HandleNull(rsDic!cod_form, "")
            DicEstrutObjectos(DicTotalObjectos).cod_objecto = BL_HandleNull(rsDic!cod_objecto, "")
            DicEstrutObjectos(DicTotalObjectos).id_texto_caption = BL_HandleNull(rsDic!id_texto_caption, mediComboValorNull)
            DicEstrutObjectos(DicTotalObjectos).id_texto_tooltip = BL_HandleNull(rsDic!id_texto_tooltip, mediComboValorNull)
            DicEstrutObjectos(DicTotalObjectos).indice_objecto = BL_HandleNull(rsDic!indice_objecto, mediComboValorNull)
            DicEstrutObjectos(DicTotalObjectos).tipo = BL_HandleNull(rsDic!tipo, "")
            rsDic.MoveNext
        Wend
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIC_InicializaObjectos " & Err.Description, "DIC", "DIC_InicializaObjectos"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' METODO NAO CHAMADO. USADO APENAS PARA FAZER ALGUM TRATAMENTO DE STRINGS

' ---------------------------------------------------------------------------------------------------------
Public Sub DIC_TrataTextosPendentes()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDic As New ADODB.recordset
    Dim frases() As String
    Dim seqTexto As Long
    Dim listaCodFrases  As String
    Dim i As Integer
    sSql = "SELECT * FROM sl_texto_aux where seq_texto not in (Select seq_texto_aux from sl_texto_aux_det) ORDER BY nome_form,metodo, tipo "
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount >= 1 Then
        seqTexto = mediComboValorNull
        While Not rsDic.EOF
            ReDim frases(0)
            
            If InStr(1, rsDic!texto, ";") = 0 Then
                seqTexto = DIC_DevolveIndiceTexto(BL_HandleNull(rsDic!texto, ""))
                If seqTexto > mediComboValorNull Then
                    sSql = "DELETE FROM sl_texto_aux_det WHERE seq_texto_aux = " & BL_HandleNull(rsDic!seq_texto, mediComboValorNull)
                    BG_ExecutaQuery_ADO sSql
                    sSql = "INSERT INTO sl_texto_aux_det (Seq_texto_aux, id_texto) VALUES(" & BL_HandleNull(rsDic!seq_texto, mediComboValorNull) & "," & seqTexto & ")"
                    BG_ExecutaQuery_ADO sSql
                End If
            Else
                sSql = "DELETE FROM sl_texto_aux_det WHERE seq_texto_aux = " & BL_HandleNull(rsDic!seq_texto, mediComboValorNull)
                BG_ExecutaQuery_ADO sSql
                BL_Tokenize rsDic!texto, ";", frases
                For i = 0 To UBound(frases)
                    seqTexto = DIC_DevolveIndiceTexto(frases(i))
                    sSql = "INSERT INTO sl_texto_aux_det (Seq_texto_aux, id_texto) VALUES(" & BL_HandleNull(rsDic!seq_texto, mediComboValorNull) & "," & seqTexto & ")"
                    BG_ExecutaQuery_ADO sSql
                Next
                sSql = "UPDATE sl_texto_aux set cod_texto = " & BL_TrataStringParaBD(listaCodFrases) & " WHERE seq_texto = " & BL_HandleNull(rsDic!seq_texto, mediComboValorNull)
                BG_ExecutaQuery_ADO sSql
            End If
            rsDic.MoveNext
        Wend
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIC_TrataTextosPendentes " & Err.Description, "DIC", "DIC_TrataTextosPendentes"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' CHAMADO A PARTIR DE CADA FORM PARA TRADUZIR. PERCORRE OBJECTOS E TRADUZ.TRADUZ PROPRIO FORM

' ---------------------------------------------------------------------------------------------------------
Public Function DIC_Inicializacao_Idioma(Form As Form, idioma As String) As Boolean
    On Error GoTo TrataErro
    Dim Objecto As Control
    Dim i As Integer
    Dim traducao As String
    If idioma = "PT-PT" Then
        Exit Function
    End If
    DIC_TraduzForm Form, idioma
    For Each Objecto In Form.Controls
        If DIC_ObjectoArray(Objecto) = False Then
            DIC_Traduz Form, Objecto, mediComboValorNull, idioma
        Else
            DIC_Traduz Form, Objecto, Objecto.Index, idioma
        End If
    Next
Exit Function
TrataErro:
    DIC_Inicializacao_Idioma = False
    BG_LogFile_Erros "DIC_Inicializacao_Idioma " & Err.Description, "DIC", "DIC_Inicializacao_Idioma"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' VERIFICA QUE TIPO DE OBJECTO � PARA TRADUZIR

' ---------------------------------------------------------------------------------------------------------
Private Function DIC_Traduz(Form As Form, controlo As Control, indice As Integer, idioma As String) As Boolean
    On Error GoTo TrataErro
    DIC_Traduz = False
    
    If TypeOf controlo Is menu Then
        DIC_Traduz = DIC_TraduzMenu(controlo, indice, idioma)
    Else
        DIC_Traduz = DIC_TraduzObjecto(Form, controlo, indice, idioma)
    End If
Exit Function
TrataErro:
    DIC_Traduz = False
    BG_LogFile_Erros "DIC_Traduz " & Err.Description, "DIC", "DIC_Traduz"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' TRADUZ OBJECTOS (TEXTBOX, FRAMES, LABELS, ETC)

' ---------------------------------------------------------------------------------------------------------
Private Function DIC_TraduzObjecto(Form As Form, controlo As Control, indice As Integer, idioma As String) As Boolean
    Dim i As Integer
    Dim traducao_caption As String
    Dim traducao_tooltip As String
    On Error GoTo TrataErro
    For i = 1 To DicTotalObjectos
        If DicEstrutObjectos(i).cod_form = Form.Name And DicEstrutObjectos(i).cod_objecto = controlo.Name And DicEstrutObjectos(i).indice_objecto = indice Then
            If DicEstrutObjectos(i).id_texto_caption > mediComboValorNull Then
                traducao_caption = DIC_TraduzTexto(DicEstrutObjectos(i).id_texto_caption, idioma)
                If traducao_caption <> "" Then
                    If TypeOf controlo Is TextBox Or TypeOf controlo Is RichTextBox Then
                        controlo.Text = traducao_caption
                    ElseIf TypeOf controlo Is CheckBox Or TypeOf controlo Is Label Or TypeOf controlo Is Frame Then
                        controlo.caption = traducao_caption
                    End If
                End If
            End If
            If DicEstrutObjectos(i).id_texto_tooltip > mediComboValorNull Then
                traducao_tooltip = DIC_TraduzTexto(DicEstrutObjectos(i).id_texto_tooltip, idioma)
                If traducao_tooltip <> "" Then
                    controlo.ToolTipText = traducao_tooltip
                End If
            End If
            Exit For
        End If
    Next i
    DIC_TraduzObjecto = True
Exit Function
TrataErro:
    DIC_TraduzObjecto = False
    BG_LogFile_Erros "DIC_TraduzObjecto " & Err.Description, "DIC", "DIC_TraduzObjecto"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' TRADUZ DESIGNA��O DO FORM

' ---------------------------------------------------------------------------------------------------------

Private Function DIC_TraduzForm(Form As Form, idioma As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim traducao As String
    DIC_TraduzForm = False
    
    For i = 1 To DicTotalForms
        If DicEstrutForms(i).cod_form = Form.Name Then
            If DicEstrutForms(i).id_texto > mediComboValorNull Then
                traducao = DIC_TraduzTexto(DicEstrutForms(i).id_texto, idioma)
                If traducao <> "" Then
                    Form.caption = traducao
                End If
            End If
            Exit For
        End If
    Next i
    DIC_TraduzForm = True
Exit Function
TrataErro:
    DIC_TraduzForm = False
    BG_LogFile_Erros "DIC_TraduzForm " & Err.Description, "DIC", "DIC_TraduzForm"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' TRADUZ TEXTO

' ---------------------------------------------------------------------------------------------------------

Private Function DIC_TraduzTexto(id_texto As Long, idioma As String) As String
    On Error GoTo TrataErro
    Dim i As Integer
    
    For i = 1 To DicTotalTraducao
        If DicEstrutTraducao(i).id_texto = id_texto And DicEstrutTraducao(i).cultura = idioma Then
            DIC_TraduzTexto = DicEstrutTraducao(i).Texto_traduzido
            Exit Function
        End If
    Next i
Exit Function
TrataErro:
    DIC_TraduzTexto = ""
    BG_LogFile_Erros "DIC_TraduzTexto " & Err.Description, "DIC", "DIC_TraduzTexto"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' VERIFICA SE OBJECTO � UM ARRAY DE OBJECTOS

' ---------------------------------------------------------------------------------------------------------
Private Function DIC_ObjectoArray(Objecto As Control) As Boolean
    On Error GoTo TrataErro
    If Objecto.Index >= 0 Then
        DIC_ObjectoArray = True
    End If
Exit Function
TrataErro:
    DIC_ObjectoArray = False
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' TRADUZ MENUS DO MDIFORMINICIO

' ---------------------------------------------------------------------------------------------------------
Private Function DIC_TraduzMenu(controlo As Control, indice As Integer, idioma As String) As Boolean
    Dim i As Integer
    Dim traducao As String
    On Error GoTo TrataErro
    
    For i = 1 To DicTotalMenusCodif
        If DicEstrutMenusCodif(i).Codigo = controlo.Name And indice = DicEstrutMenusCodif(i).indice Then
            If DicEstrutMenusCodif(i).id_texto > mediComboValorNull Then
                traducao = DIC_TraduzTexto(DicEstrutMenusCodif(i).id_texto, idioma)
                If traducao <> "" Then
                    controlo.caption = traducao
                End If
            End If
        
        End If
    Next i
Exit Function
TrataErro:
    DIC_TraduzMenu = False
    BG_LogFile_Erros "DIC_TraduzMenu " & Err.Description, "DIC", "DIC_TraduzMenu"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------------

' TRADUZ MENSAGENS DOS FORMS

' ---------------------------------------------------------------------------------------------------------
Public Function DIC_DevolveMensagemTraduzida(ByVal idMensagem As String, idioma As String, ByRef argumentos() As String, ByVal tamanhoArg As Integer) As String
    Dim rsDic As New ADODB.recordset
    On Error GoTo TrataErro
    Dim sSql As String
    Dim traducao As String
    DIC_DevolveMensagemTraduzida = ""
    
    sSql = "SELECT * FROM sl_dic_mensagem_texto WHERE upper(id_mensagem) =" & BL_TrataStringParaBD(UCase(idMensagem))
    rsDic.CursorLocation = adUseServer
    rsDic.CursorType = adOpenStatic
    rsDic.Open sSql, gConexao
    If rsDic.RecordCount = 1 Then
        traducao = DIC_TraduzTexto(BL_HandleNull(rsDic!id_texto, mediComboValorNull), idioma)
        If traducao = "" Then
            traducao = DIC_RetornaTextoByID(rsDic!id_texto)
        End If
        DIC_DevolveMensagemTraduzida = traducao
    End If
    rsDic.Close
    Set rsDic = Nothing
Exit Function
TrataErro:
    DIC_DevolveMensagemTraduzida = ""
    BG_LogFile_Erros "DIC_DevolveMensagemTraduzida " & Err.Description, "DIC", "DIC_DevolveMensagemTraduzida"
    Exit Function
    Resume Next
End Function

Public Function DIC_RetornaTextoByID(id_texto As Long) As String
    Dim i As Integer
    For i = 1 To DicTotalTextos
        If id_texto = DicEstrutTextos(i).id Then
            DIC_RetornaTextoByID = DicEstrutTextos(i).texto
            Exit Function
        End If
    Next
    DIC_RetornaTextoByID = ""
End Function

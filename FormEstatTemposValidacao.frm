VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormEstatTemposValidacao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstatTemposValidacao"
   ClientHeight    =   6405
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   6060
   Icon            =   "FormEstatTemposValidacao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   6060
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "Agrupamento 2"
      Height          =   615
      Left            =   120
      TabIndex        =   27
      Top             =   5760
      Width           =   5655
      Begin VB.OptionButton Opt2 
         Caption         =   "Operador"
         Height          =   195
         Index           =   0
         Left            =   720
         TabIndex        =   31
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Opt2 
         Caption         =   "Grupo Ana"
         Height          =   195
         Index           =   1
         Left            =   1920
         TabIndex        =   30
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton Opt2 
         Caption         =   "Proven."
         Height          =   195
         Index           =   2
         Left            =   3360
         TabIndex        =   29
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Opt2 
         Caption         =   "An�lise"
         Height          =   195
         Index           =   3
         Left            =   4440
         TabIndex        =   28
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Agrupamento 1"
      Height          =   615
      Left            =   120
      TabIndex        =   22
      Top             =   5160
      Width           =   5655
      Begin VB.OptionButton Opt1 
         Caption         =   "An�lise"
         Height          =   195
         Index           =   3
         Left            =   4440
         TabIndex        =   26
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Proven."
         Height          =   195
         Index           =   2
         Left            =   3360
         TabIndex        =   25
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Grupo Ana"
         Height          =   195
         Index           =   1
         Left            =   1920
         TabIndex        =   24
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Operador"
         Height          =   195
         Index           =   0
         Left            =   720
         TabIndex        =   23
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Height          =   5055
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5775
      Begin VB.ListBox EcListaAna 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   20
         Top             =   1440
         Width           =   3615
      End
      Begin VB.CommandButton BtPesquisaAna 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatTemposValidacao.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   1440
         Width           =   375
      End
      Begin VB.ListBox EcListaMedicos 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   9
         Top             =   3600
         Width           =   3615
      End
      Begin VB.ListBox EcListaEFR 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   8
         Top             =   2880
         Width           =   3615
      End
      Begin VB.ListBox EcListaLocais 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   7
         Top             =   2160
         Width           =   3615
      End
      Begin VB.ListBox EcListaProven 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   6
         Top             =   720
         Width           =   3615
      End
      Begin VB.CommandButton BtPesquisaLocais 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatTemposValidacao.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   2160
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatTemposValidacao.frx":0720
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   720
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaEFR 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatTemposValidacao.frx":0AAA
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   2880
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaMedicos 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatTemposValidacao.frx":0E34
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   3600
         Width           =   375
      End
      Begin VB.ComboBox CbUrgencia 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   4440
         Width           =   3615
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   1560
         TabIndex        =   10
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   542900225
         CurrentDate     =   39588
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3840
         TabIndex        =   11
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   542900225
         CurrentDate     =   39588
      End
      Begin VB.Label Label1 
         Caption         =   "An�lises"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   21
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "M�dicos"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   18
         Top             =   3600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Entidades Finac."
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   17
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Locais"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   16
         Top             =   2160
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncias"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "a"
         Height          =   255
         Left            =   3240
         TabIndex        =   14
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label7 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label5 
         Caption         =   "Urg�ncia"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   4440
         Width           =   855
      End
   End
End
Attribute VB_Name = "FormEstatTemposValidacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const lOptOperador = 0
Const lOptGrupo = 1
Const lOptProven = 2
Const lOptAna = 3

Public rs As ADODB.recordset






Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Estat�stica de Tempos de Valida��o"
    
    Me.Left = 5
    Me.Top = 5
    Me.Width = 6150
    Me.Height = 6840 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Tempos de Valida��o")
    
    Set FormEstatTemposValidacao = Nothing
    
End Sub


Sub LimpaCampos()
    Me.SetFocus
    
    EcListaProven.Clear
    EcListaAna.Clear
    EcListaLocais.Clear
    EcListaEFR.Clear
    EcListaMedicos.Clear
    EcDtFim.Value = Bg_DaData_ADO
    EcDtInicio.Value = Bg_DaData_ADO

End Sub

Sub DefTipoCampos()
    Opt1(0).Value = True
    Opt2(1).Value = True
   'nada
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    EcDtFim.Value = Bg_DaData_ADO
    EcDtInicio.Value = Bg_DaData_ADO
   
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub


Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProven, KeyCode, Shift
End Sub

Private Sub EclistaAna_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaAna, KeyCode, Shift
End Sub
Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocais, KeyCode, Shift
End Sub

Private Sub EclistaEFR_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaEFR, KeyCode, Shift
End Sub
Private Sub EcListaMedicos_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaMedicos, KeyCode, Shift
End Sub


Private Sub BtPesquisaMedicos_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_utilizador"
    CamposEcran(1) = "cod_utilizador"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_idutilizador"
    CWhere = " permissao_res = " & cValMedRes
    CampoPesquisa = "nome_med"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome ", _
                                                                           " Utilizadores com Permiss�o Valida��o")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(Resultados(i) <> "") Then
                If EcListaMedicos.ListCount = 0 Then
                    EcListaMedicos.AddItem BL_SelCodigo("sl_idutilizador", "nome", "cod_utilizador", Resultados(i))
                    EcListaMedicos.ItemData(0) = Resultados(i)
                Else
                    EcListaMedicos.AddItem BL_SelCodigo("sl_idutilizador", "nome", "cod_utilizador", Resultados(i))
                    EcListaMedicos.ItemData(EcListaMedicos.NewIndex) = Resultados(i)
                End If
            End If
        Next i
    End If
End Sub



Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                EcListaProven.ItemData(0) = Resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaAna_Click()
    PA_PesquisaAnaMultiSel EcListaAna
End Sub

Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", Resultados(i))
                EcListaLocais.ItemData(0) = Resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", Resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub BtPesquisaEFR_Click()
    PA_PesquisaEFRMultiSel EcListaEFR
End Sub


Sub Preenche_Estatistica()
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim nomeReport As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    nomeReport = "EstatisticaTemposValidacao"
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Estat�stica de Tempos de Valida��o", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Estat�stica de Tempos de Valida��o", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    PreencheTabelaTemporaria
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = ""
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.Value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Value)
    

       
    'PROVENIENCIAS
    StrTemp = ""
    For i = 0 To EcListaProven.ListCount - 1
        StrTemp = StrTemp & EcListaProven.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(2) = "Proven=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(2) = "Proven=" & BL_TrataStringParaBD("Todas")
    End If
    
    'Analises
    StrTemp = ""
    For i = 0 To EcListaAna.ListCount - 1
        StrTemp = StrTemp & EcListaAna.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(3) = "Analises=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(3) = "Analises=" & BL_TrataStringParaBD("Todas")
    End If
    
    'LOCAIS
    StrTemp = ""
    For i = 0 To EcListaLocais.ListCount - 1
        StrTemp = StrTemp & EcListaLocais.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(4) = "Locais=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(4) = "Locais=" & BL_TrataStringParaBD("Todos")
    End If
    
    'ENTIDADES
    StrTemp = ""
    For i = 0 To EcListaEFR.ListCount - 1
        StrTemp = StrTemp & EcListaEFR.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(5) = "EFR=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(5) = "EFR=" & BL_TrataStringParaBD("Todas")
    End If
    
    'ANALISES
    StrTemp = ""
    For i = 0 To EcListaAna.ListCount - 1
        StrTemp = StrTemp & EcListaAna.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(6) = "Analises=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(6) = "Analises=" & BL_TrataStringParaBD("Todas")
    End If

    Report.formulas(7) = "Urgencia=" & BL_TrataStringParaBD(CbUrgencia)
    
    'MEDICOS
    StrTemp = ""
    For i = 0 To EcListaMedicos.ListCount - 1
        StrTemp = StrTemp & EcListaMedicos.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(8) = "MEDICOS=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(8) = "MEDICOS=" & BL_TrataStringParaBD("Todos")
    End If
    

    'Report.SubreportToChange = ""
    Call BL_ExecutaReport
 
    
End Sub

Private Sub PreencheTabelaTemporaria()
    Dim sSql As String
    Dim sSqlCONDICOES As String
    Dim iAna As Integer
    Dim rsReq As New ADODB.recordset
    Dim ssql2 As String
    On Error GoTo TrataErro
    
    sSql = " DELETE FROM sl_cr_tempos_val WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
        
    ssql2 = ConstroiSELECT
    ssql2 = ssql2 & ConstroiFROM
    ssql2 = ssql2 & ConstroiWHERE

            
    sSql = "INSERT INTO sl_cr_tempos_val (nome_computador, num_sessao, n_req, dt_chega,"
    sSql = sSql & "cod_proven, descr_proven, cod_medico, descr_medico,"
    sSql = sSql & "cod_ana, descr_ana, cod_gr_ana, descr_gr_ana,"
    sSql = sSql & "tempo_validacao, campo_agrup1, campo_agrup2, cod_local)"
    sSql = sSql & ssql2

    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_Mensagem mediMsgBox, "N�o � poss�vel gerar a estat�stica de an�lises.", vbCritical, "Erro de Base Dados"
    BG_LogFile_Erros "FormEstatResAna - PreencheTabelaTemporaria: " & Err.Number & " (" & Err.Description & ")"
    BG_LogFile_Erros sSql
    Exit Sub
    Resume Next
End Sub

Private Function ConstroiFROM() As String
    Dim sSql As String
    Dim i As Integer
    Dim iAna As Integer
    sSql = ""
    sSql = sSql & " FROM slv_analises_apenas ana, sl_idutilizador util, sl_bi_resultados res "
    
    ' ------------------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ---------------------------------------------------------------------------------------
    sSql = sSql & " LEFT OUTER JOIN sl_proven proven ON res.cod_proven = proven.cod_proven"
    
    ' ------------------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ---------------------------------------------------------------------------------------
    sSql = sSql & " LEFT OUTER JOIN SL_EFR efr ON res.cod_efr= efr.cod_efr "


    
    ConstroiFROM = sSql
End Function


Private Function ConstroiWHERE() As String
    Dim iAna As Integer
    Dim sSql As String
    Dim sSqlAny As String
    Dim sSqlAll As String
    Dim item As Variant
    Dim i As Integer
    
    sSql = ""
    sSql = sSql & " WHERE (ana.cod_ana = res.cod_ana_s or ana.cod_ana = res.cod_agrup) AND util.cod_utilizador = res.user_val AND res.estado_ana in ('3','4') "
    sSql = sSql & " AND (res.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Value) & " AND " & BL_TrataDataParaBD(EcDtFim.Value) & " )"
    
    ' ------------------------------------------------------------------------------
    ' SE URGENCIA PREENCHIDA
    ' ------------------------------------------------------------------------------
    If CbUrgencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND res.t_urg = " & BG_DaComboSel(CbUrgencia)
    End If
    
    ' ------------------------------------------------------------------------------
    ' UTILIZADOR PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaMedicos.ListCount > 0 Then
        sSql = sSql & " AND util.cod_utilizador IN ("
        For i = 0 To EcListaMedicos.ListCount - 1
            sSql = sSql & EcListaMedicos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------

    If EcListaProven.ListCount > 0 Then
        sSql = sSql & " AND proven.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & EcListaProven.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

   
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        sSql = sSql & " AND res.cod_local IN ("
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' ENTIDADES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaEFR.ListCount > 0 Then
        sSql = sSql & " AND efr.seq_efr  IN ("
        For i = 0 To EcListaEFR.ListCount - 1
            sSql = sSql & EcListaEFR.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' ANALISES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaAna.ListCount > 0 Then
        sSql = sSql & " AND ana.seq_ana  IN ("
        For i = 0 To EcListaAna.ListCount - 1
            sSql = sSql & EcListaAna.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    ConstroiWHERE = sSql
End Function

Private Function ConstroiSELECT() As String
    Dim sSql As String
    sSql = ""
    sSql = sSql & " SELECT " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", res.n_req, res.dt_chega,"
    sSql = sSql & " res.cod_proven , res.descr_proven, util.cod_utilizador, util.nome, res.cod_ana_s, res.descr_ana_s,"
    sSql = sSql & " res.cod_gr_ana, res.descr_gr_Ana, res.tempo_validacao "
    
    If Opt1(lOptOperador).Value = True Then
        sSql = sSql & ", util.nome campo_agrup1"
    ElseIf Opt1(lOptGrupo).Value = True Then
        sSql = sSql & ", res.descr_gr_ana campo_agrup1"
    ElseIf Opt1(lOptAna).Value = True Then
        sSql = sSql & ", res.descr_ana_s campo_agrup1"
    ElseIf Opt1(lOptProven).Value = True Then
        sSql = sSql & ", res.descr_proven campo_agrup1"
    End If
    
    If Opt2(lOptOperador).Value = True Then
        sSql = sSql & ", util.nome campo_agrup2"
    ElseIf Opt2(lOptGrupo).Value = True Then
        sSql = sSql & ", res.descr_gr_ana campo_agrup2"
    ElseIf Opt2(lOptAna).Value = True Then
        sSql = sSql & ", res.descr_ana_s campo_agrup2"
    ElseIf Opt2(lOptProven).Value = True Then
        sSql = sSql & ", res.descr_proven campo_agrup2"
    End If
    sSql = sSql & ", res.cod_local "
    ConstroiSELECT = sSql

End Function

Private Sub Opt1_Click(Index As Integer)
    Dim i As Integer
    If Opt2(Index).Value = True Then
        If Index > 0 Then
            Opt2(0).Value = True
        Else
            Opt2(1).Value = True
        End If
    End If
    For i = 0 To Opt2.UBound
        If i <> Index Then
            Opt2(i).Enabled = True
        Else
            Opt2(i).Enabled = False
        End If
    Next i
End Sub

Private Sub Opt2_Click(Index As Integer)
    Dim i As Integer
    If Opt1(Index).Value = True Then
        If Index > 0 Then
            Opt1(0).Value = True
        Else
            Opt1(1).Value = True
        End If
    End If
    For i = 0 To Opt1.UBound
        If i <> Index Then
            Opt1(i).Enabled = True
        Else
            Opt1(i).Enabled = False
        End If
    Next i
End Sub

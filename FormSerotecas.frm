VERSION 5.00
Begin VB.Form FormSerotecas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormSerotecas"
   ClientHeight    =   8610
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7020
   Icon            =   "FormSerotecas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8610
   ScaleWidth      =   7020
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   240
      TabIndex        =   18
      Top             =   6600
      Width           =   495
   End
   Begin VB.CommandButton BtImprEtiq 
      Height          =   495
      Left            =   6360
      Picture         =   "FormSerotecas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   480
      Width           =   615
   End
   Begin VB.TextBox EcNumCaixa 
      Height          =   285
      Left            =   2880
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   6120
      Width           =   735
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   13
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2880
      TabIndex        =   12
      Top             =   120
      Width           =   4095
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      Height          =   3345
      Left            =   120
      TabIndex        =   11
      Top             =   1080
      Width           =   6855
   End
   Begin VB.Frame Frame1 
      Height          =   795
      Left            =   0
      TabIndex        =   2
      Top             =   4440
      Width           =   6885
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   195
         Width           =   615
      End
      Begin VB.Label Label7 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   765
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   8
         Top             =   120
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   7
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   6
         Top             =   195
         Width           =   1695
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   3000
         TabIndex        =   5
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label LaHoraAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   4920
         TabIndex        =   4
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label LaHoraCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   4920
         TabIndex        =   3
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   240
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   6240
      Width           =   975
   End
   Begin VB.TextBox EcUserAct 
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   6120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   1
      Left            =   2040
      TabIndex        =   14
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FormSerotecas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset




Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Codifica��o de Arquivo de Amostras"
    Me.left = 540
    Me.top = 450
    Me.Width = 7110
    Me.Height = 6000 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "SL_SEROTECAS"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 9
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_seroteca"
    CamposBD(1) = "descr_seroteca"
    CamposBD(2) = "user_cri"
    CamposBD(3) = "dt_cri"
    CamposBD(4) = "hr_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"
    CamposBD(7) = "hr_act"
    CamposBD(8) = "num_caixa"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcUserCri
    Set CamposEc(3) = LaDataCriacao
    Set CamposEc(4) = LaHoraCriacao
    Set CamposEc(5) = EcUserAct
    Set CamposEc(6) = LaDataAlteracao
    Set CamposEc(7) = LaHoraAlteracao
    Set CamposEc(8) = EcNumCaixa
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Descri��o do Arquivo"
      
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_seroteca"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("cod_seroteca", "descr_seroteca")
    NumEspacos = Array(22, 32)
    EcPrinterEtiq.Text = BL_SelImpressora("Etiqueta.rpt")
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormSerotecas = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcCodigo.Enabled = True
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
End Sub

Sub DefTipoCampos()
    LaDataAlteracao.Tag = adDate
    LaDataCriacao.Tag = adDate
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUserCri, EcUserAct, LaUtilCriacao, LaUtilAlteracao
        EcCodigo.Enabled = False
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_seroteca ASC "
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'
        
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUserCri = gCodUtilizador
        LaDataCriacao = Bg_DaData_ADO
        LaHoraCriacao = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
        
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    
    EcCodigo = BG_DaMAX(NomeTabela, "cod_seroteca") + 1
    EcNumCaixa = "0"
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
        
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        LaDataAlteracao = Bg_DaData_ADO
        EcUserAct = gCodUtilizador
        LaHoraAlteracao = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'

    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos

End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub




Private Function EtiqPrint(ByVal cod_seroteca As Long, descr_seroteca As String) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{COD_SEROTECA}", cod_seroteca)
    sWrittenData = Replace(sWrittenData, "{DESCR_SEROTECA}", descr_seroteca)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function




Private Sub BtImprEtiq_Click()
    Dim i As Integer
    Dim j As Integer
    Dim NReq As Long
    Dim qtd As Integer
    Dim EtqCrystal As String
    
    If EcUserCri = "" Then
        Exit Sub
    End If
    If Not BL_LerEtiqIni("Seroteca.ini") Then
        MsgBox "Ficheiro de inicializa��o de etiquetas seroteca ausente!"
        Exit Sub
    End If
    If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
        MsgBox "Imposs�vel abrir impressora etiquetas seroteca"
        Exit Sub
    End If
    
    Call EtiqPrint(EcCodigo, EcDescricao)
    
    BL_EtiqClosePrinter
End Sub

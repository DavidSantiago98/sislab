VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClDados2Res"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Public TRes As String
Public UnidActiva As String
Public Unid1 As String
Public Unid2 As String
Public ConvU1U2 As String
Public ValRef As String
Public Res As String
Public ResAnter As String
Public DescrFrAlf As String
Public Aviso As String
Public Formula As String
Public CasasDec As String
Public LimInf As String
Public LimSup As String
Public ValDefeito As String
Public Info As String
Public CorRes As Variant
Public CorResSel As Variant
Public FrasesIndex As Long
Public MicroIndex As Long
Public Check As String
Public ComMa As String
Public ComMi As String
Public ResMicro As New Collection
Public ResFrases As New Collection
Public AntibIndex As Long
Public ResAntib As New Collection

VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form FormAnaPorReq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   6225
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7650
   Icon            =   "FormAnaPorReq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6225
   ScaleWidth      =   7650
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   5895
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      Begin VB.Frame Frame3 
         Caption         =   "Pesquisa R�pida "
         Height          =   1815
         Left            =   360
         TabIndex        =   17
         Top             =   3960
         Width           =   6615
         Begin VB.CommandButton BtPesqRapP 
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   320
            Left            =   5880
            Style           =   1  'Graphical
            TabIndex        =   29
            ToolTipText     =   "Procurar An�lises Simples"
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton BtPesqRapC 
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   320
            Left            =   5880
            Style           =   1  'Graphical
            TabIndex        =   28
            ToolTipText     =   "Procurar An�lises Simples"
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton BtPesqRapS 
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   320
            Left            =   5880
            Style           =   1  'Graphical
            TabIndex        =   27
            ToolTipText     =   "Procurar An�lises Simples"
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox EcDescrAnaP 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2280
            TabIndex        =   26
            Top             =   1320
            Width           =   3615
         End
         Begin VB.TextBox EcDescrAnaC 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2280
            TabIndex        =   25
            Top             =   840
            Width           =   3615
         End
         Begin VB.TextBox EcCodAnaP 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            TabIndex        =   24
            Top             =   1320
            Width           =   855
         End
         Begin VB.TextBox EcCodAnaC 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            TabIndex        =   23
            Top             =   840
            Width           =   855
         End
         Begin VB.TextBox EcCodAnaS 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            TabIndex        =   22
            Top             =   360
            Width           =   855
         End
         Begin VB.TextBox EcDescrAnaS 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2280
            TabIndex        =   21
            Top             =   360
            Width           =   3615
         End
         Begin VB.OptionButton OptPerfis 
            Caption         =   "Perfis"
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   1320
            Width           =   1095
         End
         Begin VB.OptionButton OptComplexas 
            Caption         =   "Complexas"
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   840
            Width           =   1095
         End
         Begin VB.OptionButton OptSimples 
            Caption         =   "Simples"
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.Frame Frame2 
         Height          =   1095
         Left            =   360
         TabIndex        =   2
         Top             =   240
         Width           =   6615
         Begin VB.ComboBox CbAnalises 
            Height          =   315
            Left            =   4920
            Style           =   2  'Dropdown List
            TabIndex        =   34
            Top             =   720
            Width           =   1455
         End
         Begin VB.TextBox EcDtIni 
            Height          =   285
            Left            =   1080
            TabIndex        =   5
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox EcDtFim 
            Height          =   285
            Left            =   2640
            TabIndex        =   4
            Top             =   360
            Width           =   1095
         End
         Begin VB.ComboBox CbUrgencia 
            Height          =   315
            Left            =   4920
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label5 
            Caption         =   "An�lises"
            Height          =   255
            Left            =   4200
            TabIndex        =   35
            Top             =   720
            Width           =   615
         End
         Begin VB.Label Label2 
            Caption         =   "a"
            Height          =   255
            Left            =   2400
            TabIndex        =   8
            Top             =   360
            Width           =   255
         End
         Begin VB.Label Label3 
            Caption         =   "Da Data "
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label7 
            Caption         =   "&Urg�ncia"
            Height          =   255
            Left            =   4200
            TabIndex        =   6
            Top             =   240
            Width           =   735
         End
         Begin VB.Line Line2 
            BorderColor     =   &H80000010&
            X1              =   3960
            X2              =   3960
            Y1              =   120
            Y2              =   1050
         End
      End
      Begin VB.Frame Frame4 
         Height          =   2655
         Left            =   360
         TabIndex        =   1
         Top             =   1320
         Width           =   6615
         Begin VB.CommandButton BtPesquisaGrupo 
            Height          =   315
            Left            =   5880
            Picture         =   "FormAnaPorReq.frx":000C
            Style           =   1  'Graphical
            TabIndex        =   40
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
            Top             =   1680
            Width           =   375
         End
         Begin VB.TextBox EcDescrGrupo 
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   1680
            Width           =   3855
         End
         Begin VB.TextBox EcCodGrupo 
            Height          =   315
            Left            =   1320
            TabIndex        =   38
            Top             =   1680
            Width           =   735
         End
         Begin VB.ComboBox CbLocal 
            Height          =   315
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   36
            Top             =   2040
            Width           =   4815
         End
         Begin VB.TextBox EcCodPosto 
            Height          =   315
            Left            =   1320
            TabIndex        =   32
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox EcDescrPosto 
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   1200
            Width           =   3855
         End
         Begin VB.CommandButton BtPesquisaPosto 
            Height          =   315
            Left            =   5880
            Picture         =   "FormAnaPorReq.frx":0596
            Style           =   1  'Graphical
            TabIndex        =   30
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
            Top             =   1200
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaProveniencia 
            Height          =   315
            Left            =   5880
            Picture         =   "FormAnaPorReq.frx":0B20
            Style           =   1  'Graphical
            TabIndex        =   14
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox EcDescrProveniencia 
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   720
            Width           =   3855
         End
         Begin VB.TextBox EcCodProveniencia 
            Height          =   315
            Left            =   1320
            TabIndex        =   12
            Top             =   720
            Width           =   735
         End
         Begin VB.CommandButton BtPesquisaEntFin 
            Height          =   315
            Left            =   5880
            Picture         =   "FormAnaPorReq.frx":10AA
            Style           =   1  'Graphical
            TabIndex        =   11
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox EcDescrEFR 
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   240
            Width           =   3855
         End
         Begin VB.TextBox EcCodEFR 
            Height          =   315
            Left            =   1320
            TabIndex        =   9
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   41
            Top             =   1680
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Local"
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   37
            Top             =   2040
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "&Sala/Posto"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   33
            Top             =   1200
            Width           =   975
         End
         Begin VB.Label LaProven 
            Caption         =   "&Proveni�ncia"
            Height          =   255
            Left            =   240
            TabIndex        =   16
            Top             =   720
            Width           =   975
         End
         Begin VB.Label Label10 
            Caption         =   "E.&F.R."
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   240
            Width           =   495
         End
      End
   End
   Begin Crystal.CrystalReport FT 
      Left            =   120
      Top             =   5280
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog CmdDlgPrint 
      Left            =   720
      Top             =   5280
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "FormAnaPorReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesqRapC_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_c"
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_c"
    CampoPesquisa = "descr_ana_c"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaC.Text = resultados(2)
            EcCodAnaC.Text = resultados(1)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaC.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises complexas", vbExclamation, "ATEN��O"
        EcCodAnaC.SetFocus
    End If

End Sub

Private Sub BtPesqRapP_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_perfis"
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis"
    CampoPesquisa = "descr_perfis"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaP.Text = resultados(1)
            EcDescrAnaP.Text = resultados(2)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaP.Enabled = True
            EcCodAnaP.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem perfis", vbExclamation, "ATEN��O"
        EcCodAnaP.SetFocus
    End If

End Sub


Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.Text = resultados(2)
            EcCodAnaS.Text = resultados(1)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If

End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEfr, EcDescrEfr, ""
End Sub

Private Sub BtPesquisaProveniencia_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
        
End Sub

Private Sub BtPesquisaPosto_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_sala"
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_cod_salas"
    CampoPesquisa1 = "descr_sala"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Salas/Postos")
    
    mensagem = "N�o foi encontrada nenhuma Sala/Posto de Colheita."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodPosto.Text = resultados(1)
            EcDescrPosto.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
        
End Sub
Private Sub BtPesquisaGrupo_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "Descr_Gr_ana"
    CamposEcran(2) = "Descr_Gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "Descr_Gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.Text = resultados(1)
            EcDescrGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
        
End Sub
Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub EcCodAnaC_Validate(Cancel As Boolean)
    If EcCodAnaC.Text <> "" Then
        Dim RsDescrAnaC As ADODB.recordset
        Set RsDescrAnaC = New ADODB.recordset
        EcCodAnaC.Text = UCase(EcCodAnaC.Text)
        With RsDescrAnaC
            .Source = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c= " & UCase(BL_TrataStringParaBD(EcCodAnaC.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaC.RecordCount > 0 Then
            EcDescrAnaC.Text = "" & RsDescrAnaC!Descr_Ana_C
            OptComplexas.value = True
        Else
            Cancel = True
            EcDescrAnaC.Text = ""
            BG_Mensagem mediMsgBox, "An�lise complexa inexistente!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
            EcCodAnaC.Text = ""
        End If
        RsDescrAnaC.Close
        Set RsDescrAnaC = Nothing
    Else
        OptComplexas.value = False
        EcCodAnaC.Text = ""
        EcDescrAnaC.Text = ""
    End If
End Sub

Private Sub EcCodAnaP_Validate(Cancel As Boolean)
    If EcCodAnaP.Text <> "" Then
        Dim RsDescrAnaP As ADODB.recordset
        Set RsDescrAnaP = New ADODB.recordset
        EcCodAnaP.Text = UCase(EcCodAnaP.Text)
        With RsDescrAnaP
            .Source = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis= " & UCase(BL_TrataStringParaBD(EcCodAnaP.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaP.RecordCount > 0 Then
            EcDescrAnaP.Text = "" & RsDescrAnaP!descr_perfis
            OptPerfis.value = True
        Else
            Cancel = True
            EcDescrAnaP.Text = ""
            BG_Mensagem mediMsgBox, "Perfil inexistente!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
            EcCodAnaP.Text = ""
        End If
        RsDescrAnaP.Close
        Set RsDescrAnaP = Nothing
    Else
        OptPerfis.value = False
        EcCodAnaP.Text = ""
        EcDescrAnaP.Text = ""
    End If
End Sub

Private Sub EcCodAnaS_Validate(Cancel As Boolean)
    If EcCodAnaS.Text <> "" Then
        
        Dim RsDescrAnaS As ADODB.recordset
        Set RsDescrAnaS = New ADODB.recordset
        
        EcCodAnaS.Text = UCase(EcCodAnaS.Text)
        With RsDescrAnaS
            .Source = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s= " & UCase(BL_TrataStringParaBD(EcCodAnaS.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaS.RecordCount > 0 Then
            EcDescrAnaS.Text = "" & RsDescrAnaS!descr_ana_s
            OptSimples.value = True
        Else
            Cancel = True
            EcDescrAnaS.Text = ""
            BG_Mensagem mediMsgBox, "An�lise simples inexistente!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
            EcCodAnaS.Text = ""
        End If
        RsDescrAnaS.Close
        Set RsDescrAnaS = Nothing
    Else
        OptSimples.value = False
        EcCodAnaS.Text = ""
        EcDescrAnaS.Text = ""
    End If
End Sub

Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub
Private Sub EcCodPosto_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub
Private Sub EcCodGrupo_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEfr, EcDescrEfr, "")
End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.Text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.Text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub
Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrupo.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcCodGrupo.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrGrupo.Text = "" & RsDescrProv!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrGrupo.Text = ""
    End If
    
End Sub

Private Sub EcCodposto_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodPosto.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_sala FROM sl_cod_salas WHERE cod_sala= " & BL_TrataStringParaBD(EcCodPosto.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrPosto.Text = "" & RsDescrProv!descr_sala
        Else
            Cancel = True
            EcDescrPosto.Text = ""
            BG_Mensagem mediMsgBox, "A Sala/Posto indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrPosto.Text = ""
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
        
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Requisi��es"
    Me.left = 540
    Me.top = 450
    Me.Width = 7770
    Me.Height = 6615 ' Normal
    'Me.Height = 3900 ' Campos Extras
    
    Set CampoDeFocus = EcCodProveniencia
    
    'Preenche Combo Analises
    CbAnalises.AddItem "Com Resultado"
    CbAnalises.AddItem cSemResultado
    CbAnalises.AddItem "Todas"
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Folha An�lises por Requisi��o")
    
    Set FormAnaPorReq = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    CbUrgencia.ListIndex = mediComboValorNull
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    EcCodEfr.Text = ""
    EcDescrEfr.Text = ""
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    CbAnalises.ListIndex = mediComboValorNull
    EcCodPosto.Text = ""
    EcDescrPosto.Text = ""
    CbLocal.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"
    EcCodEfr.Tag = "200"
    EcCodPosto.Tag = "200"
    EcCodGrupo.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodProveniencia.MaxLength = 10
    EcCodEfr.MaxLength = 9
    EcCodPosto.MaxLength = 6
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, "1"
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Imprime_FolhaTrabalho

End Sub

Sub ImprimirVerAntes()
    
    Call Imprime_FolhaTrabalho

End Sub

Public Sub Imprime_FolhaTrabalho()
     
    Dim RegReq As ADODB.recordset
    Dim sql As String
    Dim sSql As String
    Dim continua As Boolean
    Dim QueryAnaR  As String
    Dim QueryAnaM As String
    Dim QueryAnaH As String
    Dim QueryC As String
    Dim Query As String
    
    On Error GoTo ErrorHandler
    
    '1. Verifica se a Form Preview j� est� aberta
    'If BL_PreviewAberto("Folha M�dia An�lises por Requisi��o") = True Then Exit Sub
        
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    
    '3. Obt�m total de requisi�oes perante os campos preenchidos
    'S� interessam as an�lises filtradas das requisi��es que sejam poss�veis de iniciar (Data e Produtos Chegados!!)
    Set RegReq = New ADODB.recordset
    'NELSONPSILVA 28.12.2017 HDES-4877  (adUseServer -> adUseClient)
    RegReq.CursorLocation = adUseClient
    RegReq.LockType = adLockReadOnly
    RegReq.CursorType = adOpenForwardOnly
    RegReq.ActiveConnection = gConexao

    'Datas preenchidas?
     QueryC = " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        QueryC = QueryC & " AND T_urg=" & BL_TrataStringParaBD(BG_DaComboSel(CbUrgencia))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If EcCodProveniencia.Text <> "" Then
        QueryC = QueryC & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    
    'C�digo do Posto preenchido?
    If EcCodPosto.Text <> "" Then
        QueryC = QueryC & " AND sl_requis.Cod_sala=" & BL_TrataStringParaBD(EcCodPosto.Text)
    End If
    
    'C�digo da EFR preenchido?
    If EcCodEfr.Text <> "" Then
        QueryC = QueryC & " AND sl_requis.Cod_efr=" & BL_TrataStringParaBD(EcCodEfr.Text)
    End If
        
    'C�digo da GRUPO preenchido?
    If EcCodGrupo.Text <> "" Then
        QueryC = QueryC & " AND slv_analises.cod_gr_ana =" & BL_TrataStringParaBD(EcCodGrupo.Text)
    End If
    
    'local preenchido?
    If CbLocal.ListIndex <> -1 Then
        QueryC = QueryC & " AND sl_requis.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
    End If
    
    QueryAnaR = "SELECT distinct sl_requis.dt_chega, sl_requis.n_req, sl_requis.seq_utente " & _
                "FROM sl_requis, sl_realiza,slv_analises " & _
                "WHERE sl_requis.n_req = sl_realiza.n_req " & _
                " AND slv_analises.cod_ana = sl_realiza.cod_agrup "
    
    'Analises preenchidas?
    If OptSimples.value = True Then
        QueryAnaR = QueryAnaR & " AND sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.Text)
    ElseIf OptComplexas.value = True Then
        QueryAnaR = QueryAnaR & " AND sl_realiza.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.Text)
    ElseIf OptPerfis.value = True Then
        QueryAnaR = QueryAnaR & " AND sl_realiza.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.Text)
    End If
    
    
    QueryAnaH = "SELECT distinct sl_requis.dt_chega, sl_requis.n_req, sl_requis.seq_utente " & _
                "FROM sl_requis, sl_realiza_h, slv_analises " & _
                "WHERE sl_requis.n_req = sl_realiza_h.n_req " & _
                " AND slv_analises.cod_ana = sl_realiza_h.cod_agrup "
    
    'Analises preenchidas?
    If OptSimples.value = True Then
        QueryAnaH = QueryAnaH & " AND sl_realiza_h.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.Text)
    ElseIf OptComplexas.value = True Then
        QueryAnaH = QueryAnaH & " AND sl_realiza_h.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.Text)
    ElseIf OptPerfis.value = True Then
        QueryAnaH = QueryAnaH & " AND sl_realiza_h.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.Text)
    End If
    
    QueryAnaM = "SELECT  distinct sl_requis.dt_chega, sl_requis.n_req, sl_requis.seq_utente " & _
            "FROM sl_requis, sl_marcacoes, slv_analises " & _
            "WHERE sl_requis.n_req = sl_marcacoes.n_req " & _
                " AND slv_analises.cod_ana = sl_marcacoes.cod_agrup "
    
    'Analises preenchidas?
    If OptSimples.value = True Then
        QueryAnaM = QueryAnaM & " AND sl_marcacoes.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.Text)
    ElseIf OptComplexas.value = True Then
        QueryAnaM = QueryAnaM & " AND sl_marcacoes.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.Text)
    ElseIf OptPerfis.value = True Then
        QueryAnaM = QueryAnaM & " AND sl_marcacoes.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.Text)
    End If
    
    
    If CbAnalises.ListIndex = 0 Then
        Query = QueryAnaR & QueryC & " UNION " & QueryAnaH & QueryC
    ElseIf CbAnalises.ListIndex = 1 Then
        Query = QueryAnaM & QueryC
    Else
        Query = QueryAnaR & QueryC & " UNION " & QueryAnaH & QueryC & " UNION " & QueryAnaM & QueryC
    End If

    
    RegReq.Source = Query
    RegReq.Open
    
    'Verifica se existem registos para imprimir
    If RegReq.RecordCount <= 0 Then
        RegReq.Close
        Set RegReq = Nothing
        Call BG_Mensagem(mediMsgBox, "N�o foram encontradas nenhumas requisi��es para iniciar a Estat�stica!", vbOKOnly + vbExclamation, App.ProductName)
        
        Exit Sub
    End If
    
    Cria_TmpRec_Relatorio
    
    Call BG_BeginTransaction
    
    Query = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DT_CHEGA, N_REQ, SEQ_UTENTE) (" & Query & ")"
    BG_ExecutaQuery_ADO Query
    BG_Trata_BDErro
    
    Call BG_CommitTransaction
            
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("FolhaAnaReq", "Folha An�lises por Requisi��o", crptToPrinter)
    Else
        continua = BL_IniciaReport("FolhaAnaReq", "Folha An�lises por Requisi��o", crptToWindow)
    End If
    If continua = False Then
        If Not RegReq Is Nothing Then
            If RegReq.state <> adStateClosed Then
                RegReq.Close
            End If
            Set RegReq = Nothing
        End If
        Exit Sub
    End If
    
    RegReq.Close
    Set RegReq = Nothing
    
    '___________________________________________________________________
    
    'Imprime o relat�rio no Crystal Reports
    'Atribui o resto dos dados ao Report=>F�rmulas e Queries
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT SL_CR_ANAREQDIA.N_REQ, SL_CR_ANAREQDIA.DT_CHEGA " & _
        " FROM SL_CR_REL" & gNumeroSessao & " SL_CR_ANAREQDIA"
    
    'F�rmulas do Report
    Report.formulas(0) = "DataIni=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    Report.formulas(2) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEfr.Text)
    Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & EcDescrProveniencia.Text)
    Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    If EcCodAnaS.Text <> "" Then
        Report.formulas(5) = "Analise=" & BL_TrataStringParaBD("" & EcCodAnaS.Text & " - " & EcDescrAnaS.Text)
    ElseIf EcCodAnaC.Text <> "" Then
        Report.formulas(5) = "Analise=" & BL_TrataStringParaBD("" & EcCodAnaC.Text & " - " & EcDescrAnaC.Text)
    Else
        Report.formulas(5) = "Analise=" & BL_TrataStringParaBD("" & EcCodAnaP.Text & " - " & EcDescrAnaP.Text)
    End If
    If EcCodPosto.Text <> "" Then
        Report.formulas(6) = "Posto=" & BL_TrataStringParaBD("" & EcDescrPosto.Text)
    End If
    
    If EcDescrGrupo.Text <> "" Then
            Report.formulas(7) = "Grupo=" & BL_TrataStringParaBD("" & EcDescrGrupo.Text)
    End If
    
    Call BL_ExecutaReport
    
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Call BL_RemoveTabela("SL_CR_REL" & gNumeroSessao)
    
    
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : Imprime_FolhaTrabalho (FormAnaPorReq) -> " & Err.Description
            Exit Sub
            Resume Next
    End Select
       
End Sub

Private Sub OptComplexas_Click()
    
    EcCodAnaS.Text = ""
    EcDescrAnaS.Text = ""
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Enabled = True
    BtPesqRapC.Enabled = True
    
    EcCodAnaP.Text = ""
    EcDescrAnaP.Text = ""
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaC.SetFocus
    
End Sub

Private Sub OptPerfis_Click()

    EcCodAnaS.Text = ""
    EcDescrAnaS.Text = ""
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Text = ""
    EcDescrAnaC.Text = ""
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Enabled = True
    BtPesqRapP.Enabled = True
    
    EcCodAnaP.SetFocus
    
End Sub

Private Sub OptSimples_Click()
    
    EcCodAnaS.Enabled = True
    BtPesqRapS.Enabled = True
    
    EcCodAnaC.Text = ""
    EcDescrAnaC.Text = ""
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Text = ""
    EcDescrAnaP.Text = ""
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaS.SetFocus
    
End Sub
Sub Cria_TmpRec_Relatorio()

    Dim TmpRec(4) As DefTable
    
    TmpRec(1).NomeCampo = "N_REQ"
    TmpRec(1).tipo = "INTEGER"
    TmpRec(1).tamanho = 9
    
    TmpRec(2).NomeCampo = "DT_CHEGA"
    TmpRec(2).tipo = "DATE"
    
    TmpRec(3).NomeCampo = "HR_CHEGA"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 10
    
    TmpRec(4).NomeCampo = "SEQ_UTENTE"
    TmpRec(4).tipo = "INTEGER"
    TmpRec(4).tamanho = 9
    
    Call BL_CriaTabela("SL_CR_REL" & gNumeroSessao, TmpRec)
    
End Sub

VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormConsNovo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormConsNovo"
   ClientHeight    =   9165
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   14040
   Icon            =   "FormConsNovo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9165
   ScaleWidth      =   14040
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrOutrosCriterios 
      Caption         =   "Outros Crit�rios Pesquisa"
      Height          =   9135
      Left            =   0
      TabIndex        =   61
      Top             =   1200
      Width           =   13935
      Begin VB.CommandButton BtInsereDiagP 
         Height          =   495
         Left            =   5280
         Picture         =   "FormConsNovo.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   81
         ToolTipText     =   "Adicionar Prim�rio"
         Top             =   4680
         Width           =   495
      End
      Begin VB.CommandButton BtRetiraDiagP 
         Height          =   495
         Left            =   5280
         Picture         =   "FormConsNovo.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   80
         ToolTipText     =   " Retirar da Complexa "
         Top             =   5280
         Width           =   495
      End
      Begin VB.CommandButton BtInsereDiagS 
         Height          =   495
         Left            =   5280
         Picture         =   "FormConsNovo.frx":0720
         Style           =   1  'Graphical
         TabIndex        =   79
         ToolTipText     =   " Inserir na Complexa "
         Top             =   6360
         Width           =   495
      End
      Begin VB.CommandButton BtRetiraDiagS 
         Height          =   495
         Left            =   5280
         Picture         =   "FormConsNovo.frx":0AAA
         Style           =   1  'Graphical
         TabIndex        =   78
         ToolTipText     =   " Retirar da Complexa "
         Top             =   6960
         Width           =   495
      End
      Begin VB.CommandButton BtFecharFrOutras 
         Height          =   495
         Left            =   13080
         Picture         =   "FormConsNovo.frx":0E34
         Style           =   1  'Graphical
         TabIndex        =   77
         ToolTipText     =   "Voltar"
         Top             =   8400
         Width           =   615
      End
      Begin VB.ListBox EcListaPrim 
         Appearance      =   0  'Flat
         Height          =   1395
         ItemData        =   "FormConsNovo.frx":1AFE
         Left            =   6120
         List            =   "FormConsNovo.frx":1B00
         TabIndex        =   72
         Top             =   4560
         Width           =   4455
      End
      Begin VB.ListBox EcListaDiag 
         Appearance      =   0  'Flat
         Height          =   2955
         ItemData        =   "FormConsNovo.frx":1B02
         Left            =   360
         List            =   "FormConsNovo.frx":1B04
         TabIndex        =   71
         Top             =   4920
         Width           =   4695
      End
      Begin VB.TextBox EcPesqDiag 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   360
         TabIndex        =   70
         Top             =   4560
         Width           =   4695
      End
      Begin VB.ListBox EcListaSec 
         Appearance      =   0  'Flat
         Height          =   1785
         ItemData        =   "FormConsNovo.frx":1B06
         Left            =   6120
         List            =   "FormConsNovo.frx":1B08
         TabIndex        =   69
         Top             =   6240
         Width           =   4455
      End
      Begin VB.TextBox EcInfCompl 
         Appearance      =   0  'Flat
         Height          =   855
         Left            =   360
         TabIndex        =   68
         Top             =   8160
         Width           =   10215
      End
      Begin VB.ListBox ListaAnaSimples 
         Appearance      =   0  'Flat
         Height          =   3150
         Left            =   360
         TabIndex        =   66
         Top             =   780
         Width           =   4695
      End
      Begin VB.ListBox ListaMembros 
         Appearance      =   0  'Flat
         Height          =   3540
         Left            =   6120
         TabIndex        =   65
         Top             =   480
         Width           =   4455
      End
      Begin VB.TextBox EcPesquisa 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   360
         TabIndex        =   64
         Top             =   480
         Width           =   4695
      End
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   5280
         Picture         =   "FormConsNovo.frx":1B0A
         Style           =   1  'Graphical
         TabIndex        =   63
         ToolTipText     =   " Inserir na Complexa "
         Top             =   1680
         Width           =   495
      End
      Begin VB.CommandButton BtRetira 
         Height          =   495
         Left            =   5280
         Picture         =   "FormConsNovo.frx":1E94
         Style           =   1  'Graphical
         TabIndex        =   62
         ToolTipText     =   " Retirar da Complexa "
         Top             =   2280
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Secund�rios"
         Height          =   255
         Index           =   20
         Left            =   6120
         TabIndex        =   76
         Top             =   6000
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Diagn�sticos"
         Height          =   255
         Index           =   19
         Left            =   360
         TabIndex        =   75
         Top             =   4320
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Prim�rios"
         Height          =   255
         Index           =   18
         Left            =   6120
         TabIndex        =   74
         Top             =   4380
         Width           =   735
      End
      Begin VB.Label Label17 
         Caption         =   "Informa��o Complementar"
         Height          =   255
         Left            =   480
         TabIndex        =   73
         Top             =   7980
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "An�lises "
         Height          =   255
         Index           =   17
         Left            =   480
         TabIndex        =   67
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.CommandButton BtResultados 
      Enabled         =   0   'False
      Height          =   600
      Left            =   2520
      Picture         =   "FormConsNovo.frx":221E
      Style           =   1  'Graphical
      TabIndex        =   25
      ToolTipText     =   "Resultados"
      Top             =   8520
      Width           =   615
   End
   Begin VB.Frame FrameAnalises 
      Caption         =   "An�lises"
      Height          =   5655
      Left            =   120
      TabIndex        =   21
      Top             =   2880
      Width           =   13815
      Begin VB.CommandButton BtFecharFrame 
         Height          =   495
         Left            =   13080
         Picture         =   "FormConsNovo.frx":2EE8
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Voltar"
         Top             =   5040
         Width           =   615
      End
      Begin MSFlexGridLib.MSFlexGrid FgAna 
         Height          =   5295
         Left            =   240
         TabIndex        =   23
         Top             =   240
         Width           =   12735
         _ExtentX        =   22463
         _ExtentY        =   9340
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   375
      Left            =   1080
      TabIndex        =   19
      Top             =   9600
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"FormConsNovo.frx":3BB2
   End
   Begin VB.CommandButton BtImprimir 
      Enabled         =   0   'False
      Height          =   600
      Left            =   3360
      Picture         =   "FormConsNovo.frx":3C34
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "Imprimir Resultados"
      Top             =   8520
      Width           =   615
   End
   Begin VB.CommandButton BtEntrega 
      Enabled         =   0   'False
      Height          =   600
      Left            =   1680
      Picture         =   "FormConsNovo.frx":48FE
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Entrega Resultados"
      Top             =   8520
      Width           =   615
   End
   Begin VB.CommandButton BtGestReq 
      Enabled         =   0   'False
      Height          =   600
      Left            =   840
      Picture         =   "FormConsNovo.frx":55C8
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Requisi��o"
      Top             =   8520
      Width           =   615
   End
   Begin VB.CommandButton BtIdentif 
      Enabled         =   0   'False
      Height          =   600
      Left            =   120
      Picture         =   "FormConsNovo.frx":6292
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Identifica��o"
      Top             =   8520
      Width           =   615
   End
   Begin MSFlexGridLib.MSFlexGrid FgReq 
      Height          =   5655
      Left            =   120
      TabIndex        =   14
      Top             =   2880
      Width           =   13815
      _ExtentX        =   24368
      _ExtentY        =   9975
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pesquisa"
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   13815
      Begin VB.CommandButton BtExtra 
         Height          =   495
         Left            =   13200
         Picture         =   "FormConsNovo.frx":6F5C
         Style           =   1  'Graphical
         TabIndex        =   60
         ToolTipText     =   "Outros Crit�rios"
         Top             =   2280
         Width           =   495
      End
      Begin VB.ComboBox CbEstado 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5400
         Style           =   2  'Dropdown List
         TabIndex        =   58
         Top             =   840
         Width           =   2175
      End
      Begin VB.TextBox EcProcesso 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4200
         TabIndex        =   56
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDescrGrAna 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   48
         TabStop         =   0   'False
         Top             =   2280
         Width           =   5175
      End
      Begin VB.TextBox EcDescrProd 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9720
         Locked          =   -1  'True
         TabIndex        =   54
         TabStop         =   0   'False
         Top             =   1920
         Width           =   3615
      End
      Begin VB.CommandButton BtPesquisaProd 
         Height          =   315
         Left            =   13320
         Picture         =   "FormConsNovo.frx":7C26
         Style           =   1  'Graphical
         TabIndex        =   53
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1920
         Width           =   375
      End
      Begin VB.TextBox EcCodProd 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9000
         TabIndex        =   52
         Top             =   1920
         Width           =   735
      End
      Begin VB.TextBox EcCodGrAna 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   50
         Top             =   2280
         Width           =   855
      End
      Begin VB.CommandButton BtPesquisaGrAna 
         Height          =   315
         Left            =   7200
         Picture         =   "FormConsNovo.frx":81B0
         Style           =   1  'Graphical
         TabIndex        =   49
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   2280
         Width           =   375
      End
      Begin VB.TextBox EcDtConclusao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12120
         TabIndex        =   46
         Top             =   2880
         Width           =   1335
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   1920
         Width           =   5175
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   7200
         Picture         =   "FormConsNovo.frx":873A
         Style           =   1  'Graphical
         TabIndex        =   43
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1920
         Width           =   375
      End
      Begin VB.TextBox EcCodEFR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   42
         Top             =   1920
         Width           =   855
      End
      Begin VB.TextBox EcEstadoReq 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2040
         TabIndex        =   41
         Top             =   840
         Width           =   2655
      End
      Begin VB.ComboBox CbUrgencia 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   12360
         Style           =   2  'Dropdown List
         TabIndex        =   39
         Top             =   1200
         Width           =   1335
      End
      Begin VB.ComboBox CbLocal 
         Height          =   315
         Left            =   9000
         Style           =   2  'Dropdown List
         TabIndex        =   38
         Top             =   1560
         Width           =   4695
      End
      Begin VB.ComboBox CbSituacao 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9000
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   1200
         Width           =   1335
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   1560
         Width           =   5175
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   31
         Top             =   1560
         Width           =   855
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   29
         Top             =   1200
         Width           =   855
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   1200
         Width           =   5175
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   7200
         Picture         =   "FormConsNovo.frx":8CC4
         Style           =   1  'Graphical
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1200
         Width           =   375
      End
      Begin VB.TextBox EcNumRequis 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   1200
         TabIndex        =   12
         Top             =   840
         Width           =   855
      End
      Begin VB.TextBox EcDataChega 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9000
         TabIndex        =   10
         Top             =   840
         Width           =   1335
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12360
         TabIndex        =   9
         Top             =   840
         Width           =   1335
      End
      Begin VB.TextBox EcDataNasc 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12360
         TabIndex        =   4
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   6360
         TabIndex        =   3
         Top             =   360
         Width           =   4815
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2280
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.ComboBox CbTipoUtente 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   7200
         Picture         =   "FormConsNovo.frx":924E
         Style           =   1  'Graphical
         TabIndex        =   34
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1560
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Estado"
         Height          =   255
         Index           =   16
         Left            =   4800
         TabIndex        =   59
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Proc."
         Height          =   255
         Index           =   15
         Left            =   3480
         TabIndex        =   57
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Produtos"
         Height          =   255
         Index           =   14
         Left            =   7920
         TabIndex        =   55
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo Ana."
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   51
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Data Entrega"
         Height          =   255
         Index           =   12
         Left            =   11040
         TabIndex        =   47
         Top             =   2880
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Entidade"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   45
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Urg�ncia"
         Height          =   255
         Index           =   11
         Left            =   11280
         TabIndex        =   40
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Situa��o"
         Height          =   255
         Index           =   10
         Left            =   7920
         TabIndex        =   36
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Sala/Posto"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   33
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label LbeResults 
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   7800
         TabIndex        =   30
         Top             =   2280
         Width           =   3855
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncia."
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   26
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Local"
         Height          =   255
         Index           =   7
         Left            =   7920
         TabIndex        =   24
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   13
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Data Requis."
         Height          =   255
         Index           =   4
         Left            =   7920
         TabIndex        =   11
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "N� Benef"
         Height          =   255
         Index           =   3
         Left            =   11280
         TabIndex        =   8
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Data Nasc."
         Height          =   255
         Index           =   2
         Left            =   11280
         TabIndex        =   7
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Nome"
         Height          =   255
         Index           =   1
         Left            =   5640
         TabIndex        =   6
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Utente"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   615
      End
      Begin VB.Label LbAssinatura 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   10680
         TabIndex        =   37
         Top             =   2280
         Width           =   3015
      End
   End
   Begin VB.Label LbTotalRegistos 
      Height          =   255
      Left            =   9360
      TabIndex        =   20
      Top             =   8520
      Width           =   2895
   End
End
Attribute VB_Name = "FormConsNovo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim rs As ADODB.recordset
Const lColTUtente = 0
Const lColUtente = 1
Const lColDataNasc = 2
Const lColNomeUtente = 3
Const lColNumRequis = 4
Const lColDataChega = 5
Const lColEstadoReq = 6
Const lColEntidade = 7
Const lColNumBenef = 8
Const lColGrAna = 9
Const lColAssinatura = 10
Const lColProd = 11

Const lColDescricao = 0
Const lColRes1 = 1
Const lColRes2 = 2
Const lColEstadoAna = 3
Const lColRepetida = 4
Const lColDtColheita = 5
Const lColDtChega = 6
Const lColFolhaTrab = 7

Private Type requisicoes
    SeqUtente As String
    TUtente As String
    Utente As String
    DataNasc As String
    NomeUtente As String
    processo As String
    
    codEfr As String
    DescrEFR As String
    NumRequis As String
    NumBenef As String
    DtChega As String
    DtConclusao As String
    EstadoReq As String
    codLocal As String
    descrLocal As String
    CodProven As String
    DescrProven As String
    t_sit As String
    gr_ana As String
    t_urg As String
    tipo_urgencia As String
    flg_assinado As String
    Produtos As String
    tipo As String
End Type
Dim estrutRequis() As requisicoes
Dim totalRequis As Long

Private Type analises
    seqRealiza As String
    CodPerfil As String
    CodAnaC As String
    codAnaS As String
    
    DescrPerfil As String
    DescrAnaC As String
    DescrAnaS As String
    
    estado As String
    Res1 As String
    Res2 As String
    
    seq_req_tubo As Long
    dt_chega As String
    dt_colheita As String
    Flg_Repetido As Integer
    folha_trab As String
End Type
Dim estrutAnalises() As analises
Dim totalAnalises As Long
Dim executaFgReq As Boolean

Public PesquisaUtente As Boolean

Private Type PesqAna
    seq_ana As Long
    cod_ana As String
    descr_ana As String
End Type
Dim anaSel() As PesqAna
Dim totalAnaSel As Integer
Private Type PesqDiag
    seq_diag As Long
    cod_diag As String
    Descr_Diag As String
End Type
Dim diagP() As PesqDiag
Dim totalDiagP As Integer
Dim diagS() As PesqDiag
Dim totalDiagS As Integer

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    Call WheelHook(Me.hwnd)
    
End Sub

Sub EventoActivate()
    gF_REQCONS_NOVO = mediSim
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    
    'BRUNODSANTOS AC-307 10.01.207
    If gDUtente.Utente <> "" And gDUtente.t_utente <> "" Then
        Call LimpaCampos
        EcUtente.Text = gDUtente.Utente
        CbTipoUtente.ListIndex = gDUtente.t_utente
        Call FuncaoProcurar
    End If
    '
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If gF_RESULT = 1 Or gF_REQUIS = 1 Or gF_RESMICRO = 1 Or gF_RESULT_NOVO = 1 Or gF_SEROTECA_GESTAO = 1 Or gF_REQUIS_VET = 1 Then
        EcNumRequis = gRequisicaoActiva
        FuncaoProcurar
    ElseIf gF_IDENTIF_VET = mediSim Then
        EcUtente.Text = gDUtente.Utente
        CbTipoUtente.Text = gDUtente.t_utente
        If gDUtente.seq_utente <> "" Then
            FuncaoProcurar
        End If
    End If
    
End Sub
Function Funcao_DataActual()
    
    Select Case UCase(CampoActivo.Name)
        Case "ECDATACHEGA"
            EcDataChega = Bg_DaData_ADO
        Case "ECDATANASC"
            EcDataNasc = Bg_DaData_ADO
    End Select
    
End Function


Sub FuncaoSeguinte()
    
End Sub

Sub FuncaoAnterior()
End Sub

Sub EventoUnload()

    
    BG_StackJanelas_Pop
    
    'BRUNODSANTOS AC-307 10.01.2017
    'Set gFormActivo = MDIFormInicio
    If gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.Enabled = True
        Set gFormActivo = FormGestaoRequisicaoPrivado
    Else
        Set gFormActivo = MDIFormInicio
    End If
    '
    
    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"

    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
        
        
    If gF_RESULT = 1 Then
        FormResultados.Enabled = True
    ElseIf gF_RESULT_NOVO = 1 Then
        FormResultadosNovo.Enabled = True
    ElseIf gF_RESMICRO = 1 Then
        FormResMicro.Enabled = True
    ElseIf gF_REQUIS = 1 Then
        FormGestaoRequisicao.Enabled = True
    ElseIf gF_SEROTECA_GESTAO = 1 Then
        FormSerGestaoCaixas.Enabled = True
    ElseIf gF_IDENTIF_VET = mediSim Then
        FormIdentificaVet.Enabled = True
    ElseIf gF_REQUIS_VET = mediSim Then
        FormGestaoRequisicaoVet.Enabled = True
    ElseIf gF_REQUIS_AGUAS = mediSim Then
        FormGestaoRequisicaoAguas.Enabled = True
    End If
    
    gF_REQCONS_NOVO = mediNao

    
    Set FormConsNovo = Nothing
    
End Sub

Sub PreencheValoresDefeito()
    If gF_REQUIS <> 1 Then
        PesquisaUtente = True
    End If
    BG_PreencheComboBD_ADO "gr_empr_inst", "empresa_id", "nome_empr", CbLocal
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao, "1"
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, "1"
    BG_PreencheComboBD_ADO "sl_tbf_estado_Req", "seq_estado", "descr_estado", CbEstado, "1"
    If gCodSalaAssocUser > 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate False
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
        BtResultados.Enabled = False
        BtImprimir.Enabled = False
    Else
        EcCodSala.Enabled = True
        BtPesquisaSala.Enabled = True
        BtResultados.Enabled = True
        BtImprimir.Enabled = True
    End If
    If gF_RESULT_NOVO = 1 = mediSim Then
        BtResultados.Enabled = False
    End If
    If gCodGrAnaUtilizador = "" Then
        EcCodGrAna.Text = ""
        EcDescrGrAna.Text = ""
    Else
        EcCodGrAna.Text = gCodGrAnaUtilizador
        Eccodgrana_Validate False
    End If
    FrOutrosCriterios.Visible = False
End Sub
Sub DefTipoCampos()
    executaFgReq = True
    BG_DefTipoCampoEc_ADO "SL_REQUIS", "dt_chega", EcDataChega, mediTipoData
    BG_DefTipoCampoEc_ADO "SL_IDENTIF", "dt_nasc_ute", EcDataNasc, mediTipoData
    EcNumRequis.Tag = adInteger
    With FgReq
        .Cols = 12
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .SelectionMode = flexSelectionByRow
        .Col = lColTUtente
        .ColWidth(lColTUtente) = 500
        .TextMatrix(0, lColTUtente) = "Tipo"
        
        .Col = lColUtente
        .ColWidth(lColUtente) = 700
        .TextMatrix(0, lColUtente) = "Utente"
        
        .Col = lColDataNasc
        .ColWidth(lColDataNasc) = 1000
        .TextMatrix(0, lColDataNasc) = "Data Nasc."
        
        .Col = lColNomeUtente
        .ColWidth(lColNomeUtente) = 3000
        .TextMatrix(0, lColNomeUtente) = "Nome"
        
        .Col = lColNumRequis
        .ColWidth(lColNumRequis) = 800
        .TextMatrix(0, lColNumRequis) = "Requis."
        
        .Col = lColEstadoReq
        .ColWidth(lColEstadoReq) = 2200
        .TextMatrix(0, lColEstadoReq) = "Estado Req."
        
        .Col = lColDataChega
        .ColWidth(lColDataChega) = 1000
        .TextMatrix(0, lColDataChega) = "Data Cheg"
        
        .Col = lColEntidade
        If gTipoInstituicao = gTipoInstituicaoVet Then
            .ColWidth(lColEntidade) = 2000
        Else
            .ColWidth(lColEntidade) = 1600
        End If
        If gTipoInstituicao = gTipoInstituicaoHospitalar Then
            .TextMatrix(0, lColEntidade) = "Local"
        Else
            .TextMatrix(0, lColEntidade) = "Entidade"
        End If
        .Col = lColNumBenef
        .ColWidth(lColNumBenef) = 1100
        .TextMatrix(0, lColNumBenef) = "N� Benef."
        
        .Col = lColGrAna
        If gTipoInstituicao = gTipoInstituicaoVet Then
            .ColWidth(lColGrAna) = 0
        Else
            .ColWidth(lColGrAna) = 800
        End If
        .TextMatrix(0, lColGrAna) = "Gr.Ana."
        
        .Col = lColProd
        If gTipoInstituicao = gTipoInstituicaoVet Then
            .ColWidth(lColProd) = 0
        Else
            .ColWidth(lColProd) = 800
        End If
        .TextMatrix(0, lColProd) = "Prod."
        
        .Col = lColAssinatura
        If gAssinaturaActivo = mediSim Then
            .ColWidth(lColAssinatura) = 600
        Else
            .ColWidth(lColAssinatura) = 0
        End If
        .TextMatrix(0, lColAssinatura) = "Assin."
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColAlignment(1) = flexAlignLeftCenter
        .ColAlignment(2) = flexAlignLeftCenter
        .ColAlignment(3) = flexAlignLeftCenter
        .ColAlignment(4) = flexAlignLeftCenter
        .ColAlignment(5) = flexAlignLeftCenter
        .ColAlignment(6) = flexAlignLeftCenter
        .ColAlignment(7) = flexAlignLeftCenter
        .ColAlignment(8) = flexAlignLeftCenter
        .ColAlignment(9) = flexAlignLeftCenter
        .ColAlignment(10) = flexAlignLeftCenter
    End With

    With FgAna
        .Cols = 8
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .Col = lColDescricao
        .ColWidth(lColDescricao) = 3000
        .TextMatrix(0, lColDescricao) = "An�lise"
        
        .Col = lColRes1
        .ColWidth(lColRes1) = 1000
        .TextMatrix(0, lColRes1) = "Resultado 1"
        
        .Col = lColRes2
        .ColWidth(lColRes2) = 1000
        .TextMatrix(0, lColRes2) = "Resultado 2"
        
        .Col = lColEstadoAna
        .ColWidth(lColEstadoAna) = 2000
        .TextMatrix(0, lColEstadoAna) = "Estado"
        
        .Col = lColRepetida
        .ColWidth(lColRepetida) = 0
        .TextMatrix(0, lColRepetida) = "Repet."
        
        .Col = lColDtColheita
        .ColWidth(lColDtColheita) = 1500
        .TextMatrix(0, lColDtColheita) = "Colheita"
        
        .Col = lColDtChega
        .ColWidth(lColDtChega) = 1500
        .TextMatrix(0, lColDtChega) = "Chegada"
        
         .Col = lColFolhaTrab
        .ColWidth(lColFolhaTrab) = 1500
        .TextMatrix(0, lColFolhaTrab) = "Folha Trab."
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColAlignment(1) = flexAlignLeftCenter
        .ColAlignment(2) = flexAlignLeftCenter
        .ColAlignment(3) = flexAlignLeftCenter
        .ColAlignment(lColDtColheita) = flexAlignLeftCenter
        .ColAlignment(lColDtChega) = flexAlignLeftCenter
        .ColAlignment(lColFolhaTrab) = flexAlignLeftCenter
    End With
    FrameAnalises.Visible = False
    If (gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos) And gNovoEcraResultados = mediSim Then
        BtResultados.Enabled = True
    End If

    If gF_RESULT_NOVO = 1 = mediSim Then
        BtResultados.Enabled = False
    End If
End Sub

Sub Inicializacoes()

    Me.caption = " Consulta de Requisi��es"
    Me.left = 440
    Me.top = 10
    Me.Width = 14130
    Me.Height = 9600 ' Normal
    'Me.Height = 8500 ' Campos Extras
    
    Set CampoDeFocus = EcNome
    
     
End Sub

Sub FuncaoProcurar()
    Dim j As Integer
    Dim SelTotal As Boolean
    Dim sSql As String
    Dim reqAtiva As String
    Dim k As Integer
    
    On Error GoTo TrataErro
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    totalRequis = 0
    ReDim estrutRequis(totalRequis)
    sSql = ConstroiCriterioREQ
    If sSql = "" Then
        BG_Mensagem mediMsgBox, "N�o foi indicado nenhum crit�rio !", vbExclamation, "Procurar"
        Exit Sub
    End If
    If EcNumRequis.Text <> "" Then
        reqAtiva = EcNumRequis.Text
    End If
    
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao

    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        LimpaCampos
        PreencheCampos
        BL_Toolbar_BotaoEstado "Anterior", "Activo"
        BL_Toolbar_BotaoEstado "Seguinte", "Activo"
        BL_FimProcessamento Me
    End If
    If reqAtiva <> "" Then
        For k = 0 To FgReq.rows - 1
            If FgReq.TextMatrix(k, 4) = reqAtiva Then
                FgReq.row = k
                For j = 1 To FgReq.Cols - 1
                    FgReq.Col = j
                    FgReq.CellForeColor = vbRed
                Next j
                Exit For
            End If
        Next k
    End If
    If gF_RESULT_NOVO = 1 Or PesquisaUtente = False Then
        FgReq_Click
        FgReq_DblClick
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro Procurar Requisi��es: " & sSql & Err.Description, Me.Name, "FuncaoProcurar"
    BG_Mensagem mediMsgBox, "Erro ao Procurar Requisi��es", vbExclamation, "Procurar"
    BL_FimProcessamento Me
    Exit Sub
    Resume Next
End Sub

Sub FuncaoLimpar()

    EcCodProd.Text = ""
    EcDescrProd.Text = ""
    LimpaCampos
    CampoDeFocus.SetFocus
        
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        
        LimpaCampos
        CampoDeFocus.SetFocus
                
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Sub PreencheCampos()
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    
    On Error GoTo TrataErro
    Me.SetFocus
     
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        'BRUNODSANTOS AC-307 10.01.2017
        If gDUtente.Utente <> "" And gDUtente.t_utente <> "" Then
            EcUtente.Text = gDUtente.Utente
            CbTipoUtente.ListIndex = gDUtente.t_utente
            EcNumBenef.Text = gDUtente.nr_benef
            EcNome.Text = gDUtente.nome_ute
            EcDataNasc.Text = gDUtente.dt_nasc_ute
'            gDUtente.nr_benef = ""
            gDUtente.nome_ute = ""
            gDUtente.dt_nasc_ute = ""
            gDUtente.Utente = ""
            gDUtente.t_utente = ""
        End If
        '
        
        executaFgReq = False
        FgReq.Visible = False
        While Not rs.EOF
            totalRequis = totalRequis + 1
            ReDim Preserve estrutRequis(totalRequis)
            LbTotalRegistos.caption = "A preencher Registo " & totalRequis & "de " & rs.RecordCount & " Registos"
            estrutRequis(totalRequis).DataNasc = BL_HandleNull(rs!dt_nasc_ute, "")
            estrutRequis(totalRequis).DtChega = BL_HandleNull(rs!dt_chega, "")
            estrutRequis(totalRequis).NomeUtente = BL_HandleNull(rs!nome_ute, "")
            estrutRequis(totalRequis).processo = BL_HandleNull(rs!n_proc_1, "")
            estrutRequis(totalRequis).NumBenef = BL_HandleNull(rs!n_benef, "")
            estrutRequis(totalRequis).NumRequis = BL_HandleNull(rs!n_req, "")
            estrutRequis(totalRequis).TUtente = BL_HandleNull(rs!t_utente, "")
            estrutRequis(totalRequis).Utente = BL_HandleNull(rs!Utente, "")
            estrutRequis(totalRequis).EstadoReq = BL_HandleNull(rs!estado_req, "")
            estrutRequis(totalRequis).SeqUtente = BL_HandleNull(rs!seq_utente, "")
            estrutRequis(totalRequis).codEfr = BL_HandleNull(rs!cod_efr, "")
            estrutRequis(totalRequis).DescrEFR = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", estrutRequis(totalRequis).codEfr, "V")
            estrutRequis(totalRequis).codLocal = BL_HandleNull(rs!cod_local, "")
            estrutRequis(totalRequis).descrLocal = BL_SelCodigo("GR_EMPR_INST", "nome_empr", "Empresa_id", estrutRequis(totalRequis).codLocal, "V")
            estrutRequis(totalRequis).CodProven = BL_HandleNull(rs!cod_proven, "")
            estrutRequis(totalRequis).DescrProven = BL_HandleNull(rs!descr_proven, "")
            estrutRequis(totalRequis).t_sit = BL_HandleNull(rs!t_sit, "")
            estrutRequis(totalRequis).t_urg = BL_HandleNull(rs!t_urg, "")
            estrutRequis(totalRequis).tipo_urgencia = BL_HandleNull(rs!tipo_urgencia, "")
            estrutRequis(totalRequis).gr_ana = BL_HandleNull(rs!gr_ana, "")
            estrutRequis(totalRequis).DtConclusao = BL_HandleNull(rs!dt_conclusao, "")
            estrutRequis(totalRequis).tipo = BL_HandleNull(rs!tipo, "")
            If gAssinaturaActivo = mediSim Then
                estrutRequis(totalRequis).flg_assinado = BL_RetornaEstadoAssinatura(estrutRequis(totalRequis).NumRequis)
            Else
                estrutRequis(totalRequis).flg_assinado = mediComboValorNull
            End If
            
            ' produtos
            estrutRequis(totalRequis).Produtos = ""
            
            'BRUNODSANTOS 11.07.2016 - ULSNE-1261
            Dim Criterio As String
            Criterio = ""
            If estrutRequis(totalRequis).tipo = "M" Then
                Criterio = "_consultas"
            End If
            '
            
            sSql = "SELECT cod_prod FROM sl_req_prod" & Criterio & " WHERE n_req = " & estrutRequis(totalRequis).NumRequis
            RsProd.CursorType = adOpenStatic
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            RsProd.Open sSql, gConexao
            If RsProd.RecordCount > 0 Then
                While Not RsProd.EOF
                    estrutRequis(totalRequis).Produtos = estrutRequis(totalRequis).Produtos & BL_HandleNull(RsProd!cod_prod, "") & " "
                    RsProd.MoveNext
                Wend
            End If
            RsProd.Close
            Set RsProd = Nothing
            rs.MoveNext
            PreencheFGReq totalRequis
            
            If gCodSalaAssocUser > 0 Then
                BtImprimir.Enabled = False
            Else
                BtImprimir.Enabled = True
            End If
            
            BtEntrega.Enabled = True
            BtGestReq.Enabled = True
            BtIdentif.Enabled = True
            If gCodSalaAssocUser > 0 Then
                BtResultados.Enabled = False
            Else
                If (gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos) And gNovoEcraResultados = mediSim Then
                    BtResultados.Enabled = True
                End If
            End If
        Wend
        executaFgReq = True
        DoEvents
        FgReq.Visible = True
        LbTotalRegistos.caption = totalRequis & " Registos"
    End If
    If gF_RESULT_NOVO = 1 = mediSim Then
        BtResultados.Enabled = False
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Campos " & Err.Description, Me.Name, "PreencheCampos"
    BG_Mensagem mediMsgBox, "Erro ao Preencher Campos !", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

Sub LimpaCampos()
    executaFgReq = True
    CbEstado.ListIndex = mediComboValorNull
    CbLocal.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbTipoUtente.ListIndex = mediComboValorNull
    EcDtConclusao.Text = ""
    EcUtente.Text = ""
    EcNome.Text = ""
    EcDataChega.Text = ""
    EcDataNasc.Text = ""
    EcNumBenef.Text = ""
    EcNumRequis.Text = ""
    EcDescrEFR.Text = ""
    EcCodEFR.Text = ""
    EcCodProveniencia.Text = ""
    EcCodProveniencia_Validate False
    EcCodSala.Text = ""
    EcCodsala_Validate False
    EcCodProveniencia.Text = ""
    EcCodProveniencia_Validate False
    EcProcesso.Text = ""
    
    EcEstadoReq.Text = ""
    LbAssinatura.caption = ""

    
    If gCodSalaAssocUser > 0 Then
        EcCodSala.Text = gCodSalaAssocUser
        EcCodsala_Validate False
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
        BtResultados.Enabled = False
        BtImprimir.Enabled = False
    Else
        EcCodSala.Enabled = True
        BtPesquisaSala.Enabled = True
        BtResultados.Enabled = True
        BtImprimir.Enabled = True
    End If
    If gF_RESULT_NOVO = 1 = mediSim Then
        BtResultados.Enabled = False
    End If
    totalRequis = 0
    ReDim estrutRequis(totalRequis)
    
    LimpaFGReq
    BtImprimir.Enabled = False
    BtEntrega.Enabled = False
    BtGestReq.Enabled = False
    BtIdentif.Enabled = False
    FrameAnalises.Visible = False
    BtResultados.Enabled = False
    LbeResults.caption = ""
End Sub

Private Sub BtEntrega_Click()
    If FgReq.row <= totalRequis And FgReq.row > 0 Then
        FormEntregaResultados.Show
        Set gFormActivo = FormEntregaResultados
        FormEntregaResultados.EcNumReq = estrutRequis(FgReq.row).NumRequis
        FormEntregaResultados.EcNumReq_Validate False
        FormEntregaResultados.FuncaoProcurar
    End If

End Sub

Private Sub BtExtra_Click()
    FrOutrosCriterios.Visible = True
    FrOutrosCriterios.top = 0
    FrOutrosCriterios.left = 0
End Sub

Private Sub BtFecharFrame_Click()
    FrameAnalises.Visible = False
End Sub

Private Sub BtFecharFrOutras_Click()
    Dim i As Integer
    FrOutrosCriterios.Visible = False
    totalAnaSel = 0
    ReDim anaSel(totalAnaSel)
    
    totalDiagP = 0
    totalDiagS = 0
    
    ReDim diagP(totalDiagP)
    ReDim diagS(totalDiagS)
    
    For i = 0 To ListaMembros.ListCount - 1
        totalAnaSel = totalAnaSel + 1
        ReDim Preserve anaSel(totalAnaSel)
        anaSel(totalAnaSel).seq_ana = ListaMembros.ItemData(i)
        anaSel(totalAnaSel).descr_ana = ListaMembros.List(i)
        anaSel(totalAnaSel).cod_ana = BL_SelCodigo("SLV_ANALISES_APENAS", "COD_ANA", "SEQ_ANA", anaSel(totalAnaSel).seq_ana)
    Next
    
    
    For i = 0 To EcListaPrim.ListCount - 1
        totalDiagP = totalDiagP + 1
        ReDim Preserve diagP(totalDiagP)
        diagP(totalDiagP).seq_diag = EcListaPrim.ItemData(i)
        diagP(totalDiagP).Descr_Diag = EcListaPrim.List(i)
        diagP(totalDiagP).cod_diag = BL_SelCodigo("SL_DIAG", "COD_DIAG", "SEQ_DIAG", diagP(totalDiagP).seq_diag)
    Next
    
    For i = 0 To EcListaSec.ListCount - 1
        totalDiagS = totalDiagS + 1
        ReDim Preserve diagS(totalDiagS)
        diagS(totalDiagS).seq_diag = EcListaSec.ItemData(i)
        diagS(totalDiagS).Descr_Diag = EcListaSec.List(i)
        diagS(totalDiagS).cod_diag = BL_SelCodigo("SL_DIAG", "COD_DIAG", "SEQ_DIAG", diagS(totalDiagS).seq_diag)
    Next
End Sub

Private Sub BtGestReq_Click()
    If FgReq.row <= totalRequis And FgReq.row > 0 Then
        If estrutRequis(FgReq.row).tipo = "R" Then
            If gTipoInstituicao = "PRIVADA" Then
                FormGestaoRequisicaoPrivado.Show
                Set gFormActivo = FormGestaoRequisicaoPrivado
                FormGestaoRequisicaoPrivado.EcNumReq = estrutRequis(FgReq.row).NumRequis
                FormGestaoRequisicaoPrivado.FuncaoProcurar
            ElseIf gTipoInstituicao = "HOSPITALAR" Then
                FormGestaoRequisicao.Show
                Set gFormActivo = FormGestaoRequisicao
                FormGestaoRequisicao.EcNumReq = estrutRequis(FgReq.row).NumRequis
                FormGestaoRequisicao.FuncaoProcurar
            ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
                FormGestaoRequisicaoVet.Show
                Set gFormActivo = FormGestaoRequisicaoVet
                FormGestaoRequisicaoVet.EcNumReq = estrutRequis(FgReq.row).NumRequis
                FormGestaoRequisicaoVet.FuncaoProcurar
            ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
                FormGestaoRequisicaoAguas.Show
                Set gFormActivo = FormGestaoRequisicaoAguas
                FormGestaoRequisicaoAguas.EcNumReq = estrutRequis(FgReq.row).NumRequis
                FormGestaoRequisicaoAguas.FuncaoProcurar
            End If
        Else
            FormGesReqCons.Show
            Set gFormActivo = FormGesReqCons
            FormGesReqCons.EcNumReq = estrutRequis(FgReq.row).NumRequis
            FormGesReqCons.FuncaoProcurar
        End If
    End If
End Sub


Private Sub BtIdentif_Click()
    Dim i As Long
    If FgReq.row <= totalRequis And FgReq.row > 0 Then
        If gTipoInstituicao = gTipoInstituicaoVet Then
            FormIdentificaVet.Show
            Set gFormActivo = FormIdentificaVet
            FormIdentificaVet.EcCodSequencial = estrutRequis(FgReq.row).SeqUtente
            FormIdentificaVet.FuncaoProcurarPesquisa
        Else
            FormIdentificaUtente.Show
            Set gFormActivo = FormIdentificaUtente
            FormIdentificaUtente.EcUtente = estrutRequis(FgReq.row).Utente
            For i = 0 To FormIdentificaUtente.EcDescrTipoUtente.ListCount - 1
                If FormIdentificaUtente.EcDescrTipoUtente.List(i) = estrutRequis(FgReq.row).TUtente Then
                    FormIdentificaUtente.EcDescrTipoUtente.ListIndex = i
                End If
            Next
            FormIdentificaUtente.FuncaoProcurar
        End If
    End If
End Sub


Private Sub BtImprimir_Click()
    If FgReq.row <= totalRequis And FgReq.row > 0 Then
        gImprimirDestino = crptToPrinter
        Call IR_ImprimeResultados(False, False, estrutRequis(FgReq.row).NumRequis, _
                                  estrutRequis(FgReq.row).EstadoReq, estrutRequis(FgReq.row).SeqUtente, , , , False, , , , , , , , , , , False)
                                  
        ' PARA ENTIDADES CODIFICADAS IMPRIME RELAT�RIO PR�PRIO PARA ARS
        If gTipoInstituicao = "PRIVADA" Then
            If BL_VerificaEfrImpARS(estrutRequis(FgReq.row).codEfr) = True Then
                Call IR_ImprimeResultados(False, False, estrutRequis(FgReq.row).NumRequis, estrutRequis(FgReq.row).EstadoReq, _
                                         estrutRequis(FgReq.row).SeqUtente, , , , False, , , , , , , , , , _
                                         True, False, False, False)
        
            End If
        End If
        BL_RegistaImprimeReq estrutRequis(FgReq.row).NumRequis, -1
    End If
End Sub

Private Sub BtInsere_Click()
    
    Dim i As Integer
    Dim CountAntes As Integer
    Dim CountDepois As Integer
            
    
    If Not ListaAnaSimples Is Nothing Then
           
        For i = 0 To ListaAnaSimples.ListCount - 1
            If ListaAnaSimples.Selected(i) Then
                CountAntes = ListaMembros.ListCount
                BG_PassaElementoEntreListas ListaAnaSimples, ListaMembros, i, False, False
                CountDepois = ListaMembros.ListCount
                ListaAnaSimples.Selected(i) = False
            End If
        Next i
    End If
       
End Sub
Private Sub BtRetira_Click()

    If ListaMembros.ListIndex <> -1 Then
        ListaMembros.RemoveItem (ListaMembros.ListIndex)
    End If

End Sub
Private Sub BtInsereDiagP_Click()
    
    Dim indice, i As Integer
    Dim ListaOrigem As Object
    Dim ListaDestino As Object
    Dim aux As String
    Dim CountAntes As Integer
    Dim CountDepois As Integer
    
    
    Set ListaDestino = EcListaPrim
    Set ListaOrigem = EcListaDiag
   
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i

End Sub

Private Sub BtInsereDiagS_Click()
    
    Dim indice, i As Integer
    Dim ListaOrigem As Object
    Dim ListaDestino As Object
    Dim aux As String
    Dim CountAntes As Integer
    Dim CountDepois As Integer
    
    Set ListaDestino = EcListaSec
    Set ListaOrigem = EcListaDiag
   
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i

End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub BtPesquisaGrAna_Click()
    PA_PesquisaGrAna EcCodGrAna, EcDescrGrAna
End Sub
Private Sub BtPesquisaprod_Click()
    PA_PesquisaProdutos EcCodProd, EcDescrProd
End Sub

Private Sub BtPesquisaProveniencia_Click()
    PA_PesquisaProven EcCodProveniencia, EcDescrProveniencia, ""
    

End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
End Sub


Private Sub BtResultados_Click()
    If EcNumRequis <> "" Then
        FormResultadosNovo.Show
        FormResultadosNovo.EcPesqNumReq = EcNumRequis
        FormResultadosNovo.EcCodLocal = gCodLocal
        FormResultadosNovo.FuncaoProcurar
    End If
End Sub


Private Sub BtRetiraDiagP_Click()
    
    If EcListaPrim.ListIndex <> -1 Then
        EcListaPrim.RemoveItem (EcListaPrim.ListIndex)
    End If

End Sub



Private Sub BtRetiraDiagS_Click()
    
    If EcListaSec.ListIndex <> -1 Then
        EcListaSec.RemoveItem (EcListaSec.ListIndex)
    End If

End Sub

Private Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub
Private Sub Eccodgrana_Validate(Cancel As Boolean)
    Cancel = PA_ValidateGrAna(EcCodGrAna, EcDescrGrAna)
End Sub
Private Sub EcCodProd_Validate(Cancel As Boolean)
    Cancel = PA_ValidateProduto(EcCodProd, EcDescrProd)
End Sub
Private Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
End Sub

Private Sub EcDataChega_GotFocus()
    EcDataChega = Bg_DaData_ADO
    Set CampoActivo = Me.ActiveControl
End Sub


Private Sub EcDataChega_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDataNasc_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDataNasc_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtConclusao_GotFocus()
    EcDtConclusao = Bg_DaData_ADO
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcNumRequis_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And EcNumRequis.Text <> "" Then
        FuncaoProcurar
    End If
End Sub

Private Sub EcNumRequis_Validate(Cancel As Boolean)
    If Len(EcNumRequis) > 7 Then
        EcNumRequis = Right(EcNumRequis, 7)
    End If
End Sub

Private Sub EcPesqDiag_Change()

    PA_RefinaPesquisa EcPesqDiag, "sl_diag", "descr_diag", "seq_diag", EcListaDiag
    
End Sub


Private Sub EcPesquisa_Change()
    
    PA_RefinaPesquisa EcPesquisa, "slv_analises_apenas", "descr_ana", "seq_ana", ListaAnaSimples

End Sub

Private Sub FgReq_Click()
    Dim i As Integer
    On Error GoTo TrataErro
    If executaFgReq = False Then Exit Sub

    If FgReq.row > 0 And FgReq.row <= totalRequis Then
        CbTipoUtente.ListIndex = mediComboValorNull
        For i = 0 To CbTipoUtente.ListCount - 1
            If CbTipoUtente.List(i) = estrutRequis(FgReq.row).TUtente Then
                CbTipoUtente.ListIndex = i
                Exit For
            End If
        Next
        CbSituacao.ListIndex = mediComboValorNull
        If estrutRequis(FgReq.row).t_sit <> "" Then
            For i = 0 To CbSituacao.ListCount - 1
                If CbSituacao.ItemData(i) = estrutRequis(FgReq.row).t_sit Then
                    CbSituacao.ListIndex = i
                    Exit For
                End If
            Next
        End If
        CbUrgencia.ListIndex = mediComboValorNull
        If estrutRequis(FgReq.row).t_urg <> "" Then
            For i = 0 To CbUrgencia.ListCount - 1
                If CbUrgencia.ItemData(i) = estrutRequis(FgReq.row).t_urg Then
                    CbUrgencia.ListIndex = i
                    Exit For
                End If
            Next
        End If
        gRequisicaoActiva = estrutRequis(FgReq.row).NumRequis
        gDUtente.seq_utente = estrutRequis(FgReq.row).SeqUtente
        
        EcUtente.Text = estrutRequis(FgReq.row).Utente
        EcNome.Text = estrutRequis(FgReq.row).NomeUtente
        EcProcesso.Text = estrutRequis(FgReq.row).processo
        EcDataNasc.Text = estrutRequis(FgReq.row).DataNasc
        'RGONCALVES 25.01.2016 ULSNE-1030
        'EcEstadoReq.Text = BL_DevolveEstadoReq(estrutRequis(FgReq.row).EstadoReq)
        If estrutRequis(FgReq.row).tipo = "M" Then
            EcEstadoReq.Text = "Marca��o"
        Else
            EcEstadoReq.Text = BL_DevolveEstadoReq(estrutRequis(FgReq.row).EstadoReq)
        End If
        '
        EcNumBenef.Text = estrutRequis(FgReq.row).NumBenef
        EcNumRequis.Text = estrutRequis(FgReq.row).NumRequis
        EcDataChega.Text = estrutRequis(FgReq.row).DtChega
        EcCodEFR.Text = estrutRequis(FgReq.row).codEfr
        EcDescrEFR.Text = estrutRequis(FgReq.row).DescrEFR
        EcDtConclusao.Text = estrutRequis(FgReq.row).DtConclusao
        If estrutRequis(FgReq.row).descrLocal <> "" Then
            CbLocal = estrutRequis(FgReq.row).descrLocal
        End If
        EcCodProveniencia.Text = estrutRequis(FgReq.row).CodProven
        EcDescrProveniencia.Text = estrutRequis(FgReq.row).DescrProven
        LbAssinatura.caption = ""
        'RGONCALVES 25.01.2016 ULSNE-1030
'        If BL_VerificaRequisAssinada(estrutRequis(FgReq.row).NumRequis) Then
'            LbAssinatura.caption = "Requisi��o J� Assinada"
'        End If
'        PreencheEnvioEResults
        LbeResults.caption = ""
        If estrutRequis(FgReq.row).tipo <> "M" Then
            If BL_VerificaRequisAssinada(estrutRequis(FgReq.row).NumRequis) Then
                LbAssinatura.caption = "Requisi��o J� Assinada"
            End If
            PreencheEnvioEResults
        End If
        '
    Else
        If totalRequis > 0 Then
            CbTipoUtente.ListIndex = mediComboValorNull
            CbSituacao.ListIndex = mediComboValorNull
            CbUrgencia.ListIndex = mediComboValorNull

            EcUtente = ""
            EcNome = ""
            EcDataNasc = ""
            
            EcNumBenef = ""
            EcNumRequis = ""
            EcDataChega = ""
            EcDescrEFR = ""
            CbLocal.ListIndex = mediComboValorNull
            EcCodProveniencia = ""
            EcCodProveniencia_Validate False
            'RGONCALVES 25.01.2016 ULSNE-1030
            LbeResults.caption = ""
            EcCodEFR.Text = ""
            EcEstadoReq.Text = ""
            EcProcesso.Text = ""
            '
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Dados do Utente " & Err.Description, Me.Name, "FgReq_Click"
    BG_Mensagem mediMsgBox, "Erro ao Preencher Dados do Utente !", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------------------

' LIMPA FLEX GRID

' ----------------------------------------------------------------------------------------------
Private Sub LimpaFGReq()
    On Error GoTo TrataErro
    FgReq.rows = 1
    FgReq.rows = 2
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Limpar Grelha" & Err.Description, Me.Name, "LimpaFGReq"
    BG_Mensagem mediMsgBox, "Erro ao Limpar Grelha!", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------------------

' LIMPA FLEX GRID

' ----------------------------------------------------------------------------------------------
Private Sub LimpaFGAna()
    On Error GoTo TrataErro
    FgAna.rows = 1
    FgAna.rows = 2
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Limpar Grelha An�lises" & Err.Description, Me.Name, "LimpaFGAna"
    BG_Mensagem mediMsgBox, "Erro ao Limpar Grelha An�lises!", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

Private Sub FgReq_DblClick()
    If FgReq.row > 0 And FgReq.row <= totalRequis Then
        totalAnalises = 0
        ReDim estrutAnalises(0)
        PreeencheAnalises FgReq.row
    End If
End Sub

Private Sub FgReq_RowColChange()

    FgReq_Click
End Sub

Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()

    EventoLoad
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Public Sub FuncaoImprimir()
    If gImprimirDestino = crptToWindow Then
        If FgReq.row <= totalRequis And FgReq.row > 0 Then
            'RGONCALVES 25.01.2016 ULSNE-1030
            If estrutRequis(FgReq.row).tipo <> "M" Then
            '
                Call IR_ImprimeResultados(False, False, estrutRequis(FgReq.row).NumRequis, , , , , , False, , , , True, True)
            End If
        End If
    End If
End Sub

' ----------------------------------------------------------------------------------------------

' CONSTRI CRITERIO DE PESQUISA INDICADO PELO UTILIZADOR

' ----------------------------------------------------------------------------------------------
Private Function ConstroiCriterioREQ() As String
    Dim sSql As String
    Dim Order As String
    Dim flg_criterio As Boolean
    Dim sWhere As String
    flg_criterio = False
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    
    On Error GoTo TrataErro
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    
    sWhere = ConstroiWHERE("")
    
    sSql = "SELECT i.dt_nasc_ute, req.estado_req, Req.dt_chega, Req.n_benef, i.n_benef_ute, Req.n_req, i.nome_ute, "
    sSql = sSql & " i.t_utente, i.Utente, i.seq_utente, req.cod_Efr, req.cod_local, req.cod_proven, prov.descr_proven,"
    sSql = sSql & " REQ.t_sit, req.gr_ana, req.t_urg, req.tipo_urgencia, req.dt_conclusao, i.n_proc_1, 'R' tipo  "
    sSql = sSql & " FROM " & tabela_aux & " i, sl_Requis req LEFT OUTER JOIN sl_proven prov ON req.cod_proven = prov.cod_proven WHERE i.seq_utente = req.seq_utente "
    sSql = sSql & sWhere
    
    sWhere = ConstroiWHERE("_consultas")
    sSql = sSql & " union SELECT i.dt_nasc_ute, req.estado_req, Req.dt_chega, Req.n_benef, i.n_benef_ute, Req.n_req, i.nome_ute, "
    sSql = sSql & " i.t_utente, i.Utente, i.seq_utente, req.cod_Efr, req.cod_local, req.cod_proven, prov.descr_proven,"
    sSql = sSql & " REQ.t_sit, req.gr_ana, req.t_urg, req.tipo_urgencia, req.dt_conclusao, i.n_proc_1, 'M' tipo  "
    sSql = sSql & " FROM " & tabela_aux & " i, sl_Requis_consultas req LEFT OUTER JOIN sl_proven prov ON req.cod_proven = prov.cod_proven "
    sSql = sSql & " WHERE i.seq_utente = req.seq_utente  AND (req.flg_invisivel is null or req.flg_invisivel = 0 )"
    
    sSql = sSql & sWhere
    
    If PesquisaUtente = False And EcNumRequis.Text = "" Then
         ConstroiCriterioREQ = ""
    ElseIf sWhere <> "" Then
        ConstroiCriterioREQ = sSql
        If EcNome.Text <> "" Then
            Order = " Order by nome_ute ASC, n_req DESC"
        End If
        If Order = "" Then
            Order = " ORDER BY dt_chega DESC, n_req DESC "
            'Order = " ORDER BY n_req DESC "
        End If
        ConstroiCriterioREQ = ConstroiCriterioREQ & Order
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Construir Crit�rio " & Err.Description, Me.Name, "ConstroiCriterioREQ"
    BG_Mensagem mediMsgBox, "Erro ao Construir Crit�rio", vbExclamation, "Procurar"
    Exit Function
    Resume Next
End Function
Private Function ConstroiWHERE(sufixo As String) As String
    Dim sSql As String
    Dim flg_criterio As Boolean
    Dim i As Integer
    flg_criterio = False
    ' TIPO UTENTE + UTENTE
    If CbTipoUtente.ListIndex <> mediComboValorNull And EcUtente.Text <> "" Then
        sSql = sSql & " AND i.t_utente = " & BL_TrataStringParaBD(CbTipoUtente) & " AND i.utente = " & BL_TrataStringParaBD(EcUtente.Text)
        flg_criterio = True
    End If
    
    If EcProcesso.Text <> "" Then
     sSql = sSql & " AND i.n_proc_1 = " & BL_TrataStringParaBD(EcProcesso.Text)
        flg_criterio = True
    End If
    ' estado requisicao
    If CbEstado.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND req.estado_req = (select cod_estado from sl_tbf_estado_req where seq_estado = " & CbEstado.ItemData(CbEstado.ListIndex) & ")"
        flg_criterio = True
    End If
    ' REQUISICAO
    If EcNumRequis.Text <> "" Then
        If PesquisaUtente = True Then
            sSql = sSql & " AND req.SEQ_UTENTE IN(SELECT rr.seq_utente  FROM sl_requis" & sufixo & " rr WHERE rr.n_req= " & BL_TrataStringParaBD(EcNumRequis.Text) & ")"
        Else
            sSql = sSql & " AND req.n_Req = " & BL_TrataStringParaBD(EcNumRequis.Text)
        End If
        flg_criterio = True
    End If
    
    ' NUMERO BENEFICIARIO
    If EcNumBenef.Text <> "" Then
        sSql = sSql & " AND req.n_benef = " & BL_TrataStringParaBD(EcNumBenef.Text)
        flg_criterio = True
    End If
    
    ' DATA CHEGADA
    If EcDataChega <> "" Then
        If sufixo <> "" Then
            sSql = sSql & " AND req.dt_previ = " & BL_TrataDataParaBD(EcDataChega)
        Else
            sSql = sSql & " AND req.dt_chega = " & BL_TrataDataParaBD(EcDataChega)
        End If
        flg_criterio = True
    End If
    
    ' DATA Conclusao
    If EcDtConclusao <> "" Then
        sSql = sSql & " AND req.dt_conclusao = " & BL_TrataDataParaBD(EcDtConclusao)
        flg_criterio = True
    End If
    
    ' DATA NASCIMENTO
    If EcDataNasc.Text <> "" Then
        sSql = sSql & " AND i.dt_nasc_ute = " & BL_TrataDataParaBD(EcDataNasc.Text)
        flg_criterio = True
    End If
    
    ' COD LOCAL
    If CbLocal.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND req.cod_local = " & CbLocal.ItemData(CbLocal.ListIndex)
        flg_criterio = True
    End If
    
    ' NOME
    If EcNome.Text <> "" Then
        If InStr(1, EcNome.Text, "%") > 0 Or InStr(1, EcNome.Text, "*") > 0 Then
            sSql = sSql & " AND i.nome_ute LIKE  " & BL_TrataStringParaBD(UCase(Replace(EcNome.Text, "*", "%")))
        Else
            sSql = sSql & " AND i.nome_ute = " & BL_TrataStringParaBD(UCase(EcNome.Text))
        End If
        flg_criterio = True
        'Order = " Order by nome_ute ASC, n_req DESC"
    End If
    
    ' SITUACAO
    If CbSituacao.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND req.t_sit = " & CbSituacao.ItemData(CbSituacao.ListIndex)
        flg_criterio = True
    End If
    
    ' SITUACAO
    If CbUrgencia.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND req.t_urg = " & BL_TrataStringParaBD(CbUrgencia.ItemData(CbUrgencia.ListIndex))
        flg_criterio = True
    End If
    
    If EcCodProveniencia.Text <> "" Then
        sSql = sSql & " AND req.cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia.Text)
        flg_criterio = True
    End If
    
    ' SALA
    If EcCodSala.Text <> "" Then
        sSql = sSql & " AND req.cod_sala = " & BL_TrataStringParaBD(EcCodSala.Text)
        flg_criterio = True
    End If
    
    ' ENTIDADE
    If EcCodEFR.Text <> "" Then
        sSql = sSql & " AND req.cod_efr = " & EcCodEFR.Text
        flg_criterio = True
    End If
    
    'GRUPO DE ANALISES
    If EcCodGrAna.Text <> "" Then
        sSql = sSql & " AND req.n_Req in "
        sSql = sSql & "(select n_req from sl_marcacoes m, slv_analises v WHERE m.n_Req = req.n_Req and v.cod_Ana = m.cod_agrup and v.gr_ana = " & BL_TrataStringParaBD(EcCodGrAna.Text)
        If EcCodProd.Text <> "" Then
            sSql = sSql & " AND v.cod_produto = " & BL_TrataStringParaBD(EcCodProd.Text)
        End If
        sSql = sSql & " UNION select n_req from sl_realiza r, slv_analises v WHERE r.n_Req = req.n_Req and v.cod_Ana = r.cod_agrup and v.gr_ana = " & BL_TrataStringParaBD(EcCodGrAna.Text)
        If EcCodProd.Text <> "" Then
            sSql = sSql & " AND v.cod_produto = " & BL_TrataStringParaBD(EcCodProd.Text)
        End If
        sSql = sSql & " UNION select n_req from sl_realiza_h rh, slv_analises v WHERE rh.n_Req = req.n_Req and v.cod_Ana = rh.cod_agrup and v.gr_ana = " & BL_TrataStringParaBD(EcCodGrAna.Text)
        If EcCodProd.Text <> "" Then
            sSql = sSql & " AND v.cod_produto = " & BL_TrataStringParaBD(EcCodProd.Text)
        End If
        sSql = sSql & ")"
    End If
    
    'PRODUTOS
    If EcCodProd.Text <> "" Then
        sSql = sSql & " AND req.n_Req in "
        sSql = sSql & "(select n_req from sl_Req_prod rp WHERE rp.n_Req = req.n_Req and rp.cod_prod = " & BL_TrataStringParaBD(EcCodProd.Text) & ")"
    End If
    
    'ANALISES SELECCIONADAS
    If totalAnaSel > 0 Then
        sSql = sSql & " AND req.n_req in ( SELECT result.n_req FROM slv_resultados result WHERE  result.n_req = req.n_req AND cod_agrup IN( "
        For i = 1 To totalAnaSel
            sSql = sSql & BL_TrataStringParaBD(anaSel(i).cod_ana) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
    End If

    'DIAG P SELECCIONADAS
    If totalDiagP > 0 Then
        sSql = sSql & " AND i.seq_utente in ( SELECT diagp.seq_utente from sl_diag_pri diagp WHERE cod_diag IN("
        For i = 1 To totalDiagP
            sSql = sSql & BL_TrataStringParaBD(diagP(i).cod_diag) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
    End If
    'DIAG S SELECCIONADAS
    If totalDiagS > 0 Then
        sSql = sSql & " AND req.n_Req in ( SELECT diags.n_req from sl_diag_sec diags WHERE  diags.n_Req = req.n_req AND cod_diag IN("
        For i = 1 To totalDiagS
            sSql = sSql & BL_TrataStringParaBD(diagS(i).cod_diag) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ")) "
    End If
    
    If flg_criterio = True Then
        ConstroiWHERE = sSql
    End If
End Function

Private Sub PreeencheAnalises(linha As Long)
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim RsRes As New ADODB.recordset
    Dim Res1 As String
    Dim Res2 As String
    On Error GoTo TrataErro
    
    'RGONCALVES 25.01.2016 ULSNE-1030
    If estrutRequis(linha).tipo = "M" Then
        Exit Sub
    End If
    '
    
    sSql = ConstroiCriterioANA(linha)
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            Res1 = ""
            Res2 = ""
            totalAnalises = totalAnalises + 1
            ReDim Preserve estrutAnalises(totalAnalises)
            LimpaFGAna
            If BL_HandleNull(rsAna!seq_realiza, "") > 0 Then
                ' PRIMEIRO RESULTADO
                sSql = ConstroiCriterioRES(BL_HandleNull(rsAna!seq_realiza, ""), 1)
                RsRes.CursorType = adOpenStatic
                RsRes.CursorLocation = adUseServer
                RsRes.Open sSql, gConexao
                If RsRes.RecordCount > 0 Then
                    Res1 = BL_HandleNull(RsRes!res, "")
                End If
                RsRes.Close
                
                ' SEGUNDO RESULTADO
                sSql = ConstroiCriterioRES(BL_HandleNull(rsAna!seq_realiza, ""), 2)
                RsRes.CursorType = adOpenStatic
                RsRes.CursorLocation = adUseServer
                RsRes.Open sSql, gConexao
                If RsRes.RecordCount > 0 Then
                    Res2 = BL_HandleNull(RsRes!res, "")
                End If
                RsRes.Close
            End If
            
            PreencheEstrutAna BL_HandleNull(rsAna!seq_realiza, ""), BL_HandleNull(rsAna!Cod_Perfil, ""), BL_HandleNull(rsAna!cod_ana_c, ""), _
                              BL_HandleNull(rsAna!cod_ana_s, ""), BL_HandleNull(rsAna!flg_estado, ""), Res1, Res2, _
                              BL_HandleNull(rsAna!seq_req_tubo, mediComboValorNull), BL_HandleNull(rsAna!dt_colheita, ""), _
                              BL_HandleNull(rsAna!hr_colheita, ""), BL_HandleNull(rsAna!dt_chega, ""), _
                              BL_HandleNull(rsAna!hr_chega, ""), BL_HandleNull(rsAna!N_Folha_Trab, "")
            rsAna.MoveNext
        Wend
        PreencheFGAna
        FrameAnalises.Visible = True
        FrameAnalises.top = 2880
        FrameAnalises.left = 120
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher An�lises : " & sSql & Err.Description, Me.Name, "PreeencheAnalises"
    BG_Mensagem mediMsgBox, "Erro ao Preencher An�lises !", vbExclamation, "PreeencheAnalises"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------------------

' CONSTRI CRITERIO DE PESQUISA PARA AS ANALISES

' ----------------------------------------------------------------------------------------------
Private Function ConstroiCriterioANA(linha As Long) As String
    Dim sSql As String
    Dim Order As String
        
    On Error GoTo TrataErro
    
    ' APENAS TABELA DE MARCACOES
    If estrutRequis(linha).EstadoReq = gEstadoReqEsperaResultados Or estrutRequis(linha).EstadoReq = gEstadoReqEsperaProduto Then
        sSql = "SELECT -1 seq_Realiza, sl_marcacoes.n_req, cod_perfil, cod_ana_c, cod_ana_s, '-1' flg_estado, ord_ana, ord_ana_perf, ord_ana_compl "
        sSql = sSql & ", rt.seq_req_tubo, rt.dt_colheita, rt.hr_colheita, rt.dt_chega, rt.hr_chega, n_folha_trab "
        sSql = sSql & " FROM sl_marcacoes LEFT OUTER JOIN sl_req_tubo rt ON rt.seq_req_tubo = sl_marcacoes.seq_req_tubo, slv_analises "
        sSql = sSql & " WHERE sl_marcacoes.n_Req = " & BL_TrataStringParaBD(estrutRequis(linha).NumRequis)
        sSql = sSql & "  AND sl_marcacoes.cod_Agrup = slv_analises.cod_ana "
        sSql = sSql & ConstroiCriterioAnaDet
        
    'APENAS TABELA DE HISTORICO
    ElseIf estrutRequis(linha).EstadoReq = gEstadoReqHistorico Then
        sSql = "SELECT  seq_Realiza,  sl_realiza_h.n_req, cod_perfil, cod_ana_c, cod_ana_s,  flg_estado, ord_ana, ord_ana_perf, ord_ana_compl "
        sSql = sSql & ", rt.seq_req_tubo, rt.dt_colheita, rt.hr_colheita, rt.dt_chega, rt.hr_chega, n_folha_trab "
        sSql = sSql & " FROM sl_realiza_h  LEFT OUTER JOIN sl_req_tubo rt ON rt.seq_req_tubo = sl_realiza_h.seq_req_tubo, slv_analises "
        sSql = sSql & " WHERE sl_realiza_h.n_Req = " & BL_TrataStringParaBD(estrutRequis(linha).NumRequis)
        sSql = sSql & "  AND sl_realiza_h.cod_Agrup = slv_analises.cod_ana "
        sSql = sSql & ConstroiCriterioAnaDet
    
    'APENAS TABELA SL_REALIZA
    ElseIf estrutRequis(linha).EstadoReq = gEstadoReqTodasImpressas Or estrutRequis(linha).EstadoReq = gEstadoReqValicacaoMedicaCompleta Or _
           estrutRequis(linha).EstadoReq = gEstadoReqEsperaValidacao Then
        sSql = "SELECT  seq_Realiza,  sl_realiza.n_req, cod_perfil, cod_ana_c, cod_ana_s,  flg_estado, ord_ana, ord_ana_perf, ord_ana_compl "
        sSql = sSql & ", rt.seq_req_tubo, rt.dt_colheita, rt.hr_colheita, rt.dt_chega, rt.hr_chega, n_folha_trab "
        sSql = sSql & " FROM sl_realiza LEFT OUTER JOIN sl_req_tubo rt ON rt.seq_req_tubo = sl_realiza.seq_req_tubo, slv_analises "
        sSql = sSql & "  WHERE sl_realiza.n_Req = " & BL_TrataStringParaBD(estrutRequis(linha).NumRequis)
        sSql = sSql & "  AND sl_realiza.cod_Agrup = slv_analises.cod_ana "
        sSql = sSql & ConstroiCriterioAnaDet
    
    'ENTRA EM CONTA COM SL_MARCACOES E SL_REALIZA
    Else
        sSql = "SELECT -1 seq_Realiza,  sl_marcacoes.n_req, cod_perfil, cod_ana_c, cod_ana_s, '-1' flg_estado, ord_ana, ord_ana_perf, ord_ana_compl "
        sSql = sSql & ", rt.seq_req_tubo, rt.dt_colheita, rt.hr_colheita, rt.dt_chega, rt.hr_chega, n_folha_trab "
        sSql = sSql & " FROM sl_marcacoes LEFT OUTER JOIN sl_req_tubo rt ON rt.seq_req_tubo = sl_marcacoes.seq_req_tubo, slv_analises "
        sSql = sSql & "  WHERE sl_marcacoes.n_Req = " & BL_TrataStringParaBD(estrutRequis(linha).NumRequis)
        sSql = sSql & "  AND sl_marcacoes.cod_Agrup = slv_analises.cod_ana "
        sSql = sSql & ConstroiCriterioAnaDet
        sSql = sSql & " UNION SELECT  seq_Realiza,  sl_realiza.n_req, cod_perfil, cod_ana_c, cod_ana_s,  flg_estado, ord_ana, ord_ana_perf, ord_ana_compl "
        sSql = sSql & ", rt.seq_req_tubo, rt.dt_colheita, rt.hr_colheita, rt.dt_chega, rt.hr_chega, n_folha_trab "
        sSql = sSql & " FROM sl_realiza LEFT OUTER JOIN sl_req_tubo rt ON rt.seq_req_tubo = sl_realiza.seq_req_tubo, slv_analises "
        sSql = sSql & "  WHERE sl_realiza.n_Req = " & BL_TrataStringParaBD(estrutRequis(linha).NumRequis)
        sSql = sSql & "  AND sl_realiza.cod_Agrup = slv_analises.cod_ana "
        sSql = sSql & ConstroiCriterioAnaDet
    End If
    
    ConstroiCriterioANA = sSql & " ORDER BY ORD_ANA, ORD_ANA_PERF, ORD_ANA_COMPL "
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Construir Crit�rio An�lises" & Err.Description, Me.Name, "ConstroiCriterioANA"
    BG_Mensagem mediMsgBox, "Erro ao Construir Crit�rio", vbExclamation, "Procurar"
    Exit Function
    Resume Next
End Function

Private Function ConstroiCriterioAnaDet() As String
    Dim sSql As String
    If EcCodGrAna.Text <> "" Then
        sSql = sSql & " AND slv_analises.cod_gr_ana = " & BL_TrataStringParaBD(EcCodGrAna.Text)
    End If
    If EcCodProd.Text <> "" Then
        sSql = sSql & " AND slv_analises.cod_produto = " & BL_TrataStringParaBD(EcCodProd.Text)
    End If
    ConstroiCriterioAnaDet = sSql
    
End Function

' ----------------------------------------------------------------------------------------------

' CONSTRI CRITERIO DE PESQUISA PARA DE RESULTADOS

' ----------------------------------------------------------------------------------------------
Private Function ConstroiCriterioRES(seqRealiza As String, nres As Integer) As String
    Dim sSql As String
    Dim Order As String
        
    On Error GoTo TrataErro
    sSql = sSql & " SELECT result  res FROM sl_res_alfan where seq_realiza = " & seqRealiza
    sSql = sSql & " AND n_res =  " & nres
    sSql = sSql & " UNION SELECT descr_frase  res FROM sl_res_frase, sl_dicionario  where seq_realiza = " & seqRealiza & " AND sl_reS_frase.cod_frase = sl_dicionario.cod_frase "
    sSql = sSql & " AND n_res =  " & nres
    sSql = sSql & " UNION SELECT cod_micro  res FROM sl_res_micro where seq_realiza = " & seqRealiza
    sSql = sSql & " AND n_res =  " & nres
    ConstroiCriterioRES = sSql
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Construir Crit�rio Resultado" & Err.Description, Me.Name, "ConstroiCriterioRES"
    BG_Mensagem mediMsgBox, "Erro ao Construir Crit�rio Resultado", vbExclamation, "Procurar"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------

' PREENCHE FLEX GRID

' ----------------------------------------------------------------------------------------------

Private Sub PreencheFGReq(linha As Long)
    Const vermelhoClaro = &HBDC8F4
    On Error GoTo TrataErro
    
    FgReq.TextMatrix(linha, lColDataChega) = estrutRequis(linha).DtChega
    FgReq.TextMatrix(linha, lColDataNasc) = estrutRequis(linha).DataNasc
    FgReq.TextMatrix(linha, lColNomeUtente) = estrutRequis(linha).NomeUtente
    FgReq.Col = lColNomeUtente
    FgReq.CellBackColor = BL_DevolveCorTipoUrgencia(estrutRequis(linha).tipo_urgencia)
    FgReq.TextMatrix(linha, lColNumBenef) = estrutRequis(linha).NumBenef
    FgReq.TextMatrix(linha, lColNumRequis) = estrutRequis(linha).NumRequis
    FgReq.row = linha
    FgReq.Col = lColNumRequis
    FgReq.CellBackColor = BL_DevolveCorTURG(estrutRequis(linha).t_urg)
    FgReq.TextMatrix(linha, lColTUtente) = estrutRequis(linha).TUtente
    FgReq.TextMatrix(linha, lColUtente) = estrutRequis(linha).Utente
    If gTipoInstituicao = gTipoInstituicaoHospitalar Then
        FgReq.TextMatrix(linha, lColEntidade) = estrutRequis(linha).descrLocal
    Else
        FgReq.TextMatrix(linha, lColEntidade) = estrutRequis(linha).DescrEFR
    End If
    'estrutRequis(linha).EstadoReq = BL_MudaEstadoReq(CLng(estrutRequis(linha).NumRequis))
    FgReq.Col = lColEstadoReq
    FgReq.CellBackColor = BL_DevolveCorEstadoReq(estrutRequis(linha).EstadoReq)
    If estrutRequis(linha).tipo = "M" Then
        FgReq.TextMatrix(linha, lColEstadoReq) = "Marca��o"
    Else
        FgReq.TextMatrix(linha, lColEstadoReq) = BL_DevolveEstadoReq(estrutRequis(linha).EstadoReq)
    End If
    FgReq.TextMatrix(linha, lColGrAna) = estrutRequis(linha).gr_ana
    FgReq.TextMatrix(linha, lColProd) = estrutRequis(linha).Produtos
    If estrutRequis(linha).flg_assinado = 0 Then
        FgReq.TextMatrix(linha, lColAssinatura) = "NA"
    ElseIf estrutRequis(linha).flg_assinado = 1 Then
        FgReq.TextMatrix(linha, lColAssinatura) = "A"
    ElseIf estrutRequis(linha).flg_assinado = 2 Then
        FgReq.TextMatrix(linha, lColAssinatura) = "PA"
    Else
        FgReq.TextMatrix(linha, lColAssinatura) = ""
    End If
    
    FgReq.AddItem ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Grelha " & Err.Description, Me.Name, "PreencheFGAna"
    BG_Mensagem mediMsgBox, "Erro ao Preencher Grelha !", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ANALISES

' ----------------------------------------------------------------------------------------------

Private Sub PreencheEstrutAna(seq_realiza As String, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String, _
                              flg_estado As String, Res1 As String, Res2 As String, seq_req_tubo As Long, dt_colheita As String, _
                              hr_colheita As String, dt_chega As String, hr_chega As String, N_Folha_Trab As String)
    On Error GoTo TrataErro
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 1"
    estrutAnalises(totalAnalises).seqRealiza = seq_realiza
    estrutAnalises(totalAnalises).CodPerfil = Cod_Perfil
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 2"
    estrutAnalises(totalAnalises).CodAnaC = cod_ana_c
    estrutAnalises(totalAnalises).codAnaS = cod_ana_s
    estrutAnalises(totalAnalises).estado = flg_estado
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 3"
    estrutAnalises(totalAnalises).Res1 = Res1
    estrutAnalises(totalAnalises).Res2 = Res2
    estrutAnalises(totalAnalises).seq_req_tubo = seq_req_tubo
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 4"
    estrutAnalises(totalAnalises).dt_colheita = dt_colheita & " " & hr_colheita
    estrutAnalises(totalAnalises).dt_chega = dt_chega & " " & hr_chega
    estrutAnalises(totalAnalises).folha_trab = N_Folha_Trab
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 5"
    If Cod_Perfil <> "0" Then
        estrutAnalises(totalAnalises).DescrPerfil = BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", Cod_Perfil, "V")
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 6"
    If cod_ana_c <> "0" And cod_ana_c <> "C99999" Then
        estrutAnalises(totalAnalises).DescrAnaC = BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", cod_ana_c, "V")
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 7"
    If cod_ana_s <> "S99999" Then
        estrutAnalises(totalAnalises).DescrAnaS = BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", cod_ana_s, "V")
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros " Passa aki 8"
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Estrutura de An�lises " & Err.Description, Me.Name, "PreencheEstrutAna"
    BG_Mensagem mediMsgBox, "Erro ao Preencher Estrutura de An�lises !", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------------------

' PREENCHE FLEX GRID DE ANALISES

' ----------------------------------------------------------------------------------------------
Private Sub PreencheFGAna()
    Dim estado As String
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To totalAnalises
        If estrutAnalises(i).CodPerfil <> "0" And estrutAnalises(i).CodAnaC = "C99999" And estrutAnalises(i).codAnaS = "S99999" Then
            FgAna.TextMatrix(i, lColDescricao) = estrutAnalises(i).DescrPerfil
        ElseIf estrutAnalises(i).CodPerfil <> "0" And estrutAnalises(i).CodAnaC <> "C99999" And estrutAnalises(i).CodAnaC <> "0" And estrutAnalises(i).codAnaS = "S99999" Then
            FgAna.TextMatrix(i, lColDescricao) = "      " & estrutAnalises(i).DescrAnaC
        ElseIf estrutAnalises(i).CodPerfil <> "0" And estrutAnalises(i).CodAnaC <> "C99999" And estrutAnalises(i).CodAnaC <> "0" And estrutAnalises(i).codAnaS <> "S99999" Then
            FgAna.TextMatrix(i, lColDescricao) = "              " & estrutAnalises(i).DescrAnaS
        ElseIf estrutAnalises(i).CodPerfil <> "0" And estrutAnalises(i).CodAnaC = "0" And estrutAnalises(i).codAnaS <> "S99999" Then
            FgAna.TextMatrix(i, lColDescricao) = "      " & estrutAnalises(i).DescrAnaS
        ElseIf estrutAnalises(i).CodPerfil = "0" And estrutAnalises(i).CodAnaC <> "0" And estrutAnalises(i).codAnaS = "S99999" Then
            FgAna.TextMatrix(i, lColDescricao) = estrutAnalises(i).DescrAnaC
        ElseIf estrutAnalises(i).CodPerfil = "0" And estrutAnalises(i).CodAnaC <> "0" And estrutAnalises(i).codAnaS <> "S99999" Then
            FgAna.TextMatrix(i, lColDescricao) = "      " & estrutAnalises(i).DescrAnaS
        ElseIf estrutAnalises(i).CodPerfil = "0" And estrutAnalises(i).CodAnaC = "0" And estrutAnalises(i).codAnaS <> "S99999" Then
            FgAna.TextMatrix(i, lColDescricao) = estrutAnalises(i).DescrAnaS
        Else
            FgAna.TextMatrix(i, lColDescricao) = "---"
        End If
        
        If gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos Or gTipoInstituicao = "HOSPITALAR" Then
            If (estrutAnalises(i).estado = gEstadoAnaImpressa Or estrutAnalises(i).estado = gEstadoAnaValidacaoMedica) And gCodGrupo = gGrupoSecretariado Then
                FgAna.TextMatrix(i, lColRes1) = estrutAnalises(i).Res1
                FgAna.TextMatrix(i, lColRes2) = estrutAnalises(i).Res2
            ElseIf gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos Then
                FgAna.TextMatrix(i, lColRes1) = estrutAnalises(i).Res1
                FgAna.TextMatrix(i, lColRes2) = estrutAnalises(i).Res2
            End If
        ElseIf (estrutAnalises(i).estado = gEstadoAnaImpressa Or estrutAnalises(i).estado = gEstadoAnaValidacaoMedica) And gCodGrupo = gGrupoSecretariado Then
            FgAna.TextMatrix(i, lColRes1) = estrutAnalises(i).Res1
            FgAna.TextMatrix(i, lColRes2) = estrutAnalises(i).Res2
        End If
        FgAna.TextMatrix(i, lColRepetida) = ""
        FgAna.TextMatrix(i, lColDtChega) = estrutAnalises(i).dt_chega
        FgAna.TextMatrix(i, lColDtColheita) = estrutAnalises(i).dt_colheita
        FgAna.TextMatrix(i, lColFolhaTrab) = estrutAnalises(i).folha_trab
        FgAna.Col = lColEstadoAna
        FgAna.row = i
        FgAna.CellBackColor = BL_DevolveCorEstadoAna(estrutAnalises(i).estado)
        estado = BL_DevolveEstadoAna(estrutAnalises(i).estado)
        FgAna.TextMatrix(i, lColEstadoAna) = estado
        FgAna.AddItem ""
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Grelha de An�lises " & Err.Description, Me.Name, "PreencheFGAna"
    BG_Mensagem mediMsgBox, "Erro ao Preencher Grelha de An�lises !", vbExclamation, "Procurar"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodProveniencia_Validate(Cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodProveniencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven, t_sit, t_urg FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.Text = ""
            EcDescrProveniencia.Text = ""
        Else
            EcDescrProveniencia.Text = Tabela!descr_proven
        
        End If
        
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

' -----------------------------------------------------------------

' PREENCHE ESTADO DA REQUISI��O NO ERESULTS

' -----------------------------------------------------------------
Private Sub PreencheEnvioEResults()
    Dim estado As Integer
    On Error GoTo TrataErro
    'RGONCALVES 25.01.2016 ULSNE-1030
    LbeResults.caption = ""
    '
    If geResults <> True Then
        Exit Sub
    End If
    estado = BL_RetornaEstadoEnvioEresults(EcNumRequis)
    If estado = -1 Then
        LbeResults.caption = "Requisi��o n�o disponibilizada online"
    ElseIf estado = 0 Then
        LbeResults.caption = "Requisi��o � espera de ser disponibilizada online"
    ElseIf estado = 1 Then
        LbeResults.caption = "Requisi��o disponivel online " & BL_RetornaDataEnvioEresults(EcNumRequis)
    Else
    
        LbeResults.caption = "Erro ao disponibilizar online"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEnvioEResults: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEnvioEResults"
    Exit Sub
    Resume Next
End Sub


Sub Funcao_CopiaUte()
    Dim sSql As String
    Dim rsID As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    If Trim(gDUtente.seq_utente) <> "" Then
        sSql = "SELECT  * FROM sl_identif WHERE seq_utente = " & CLng(gDUtente.seq_utente)
        rsID.CursorType = adOpenStatic
        rsID.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsID.Open sSql, gConexao
        If rsID.RecordCount = 1 Then
            EcUtente = BL_HandleNull(rsID!Utente, "")
            CbTipoUtente = BL_HandleNull(rsID!t_utente, "")
            FuncaoProcurar
        End If
        rsID.Close
        Set rsID = Nothing
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaUte: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaUte"
    Exit Sub
    Resume Next
End Sub

Sub Funcao_CopiaReq()
    On Error GoTo TrataErro
    
    If gRequisicaoActiva <> 0 Then
        EcNumRequis = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaReq"
    Exit Sub
    Resume Next
End Sub

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  Dim ctl As Control
  
  For Each ctl In Me.Controls
    If TypeOf ctl Is MSFlexGrid Then
      If IsOver(ctl.hwnd, Xpos, Ypos) Then FlexGridScroll ctl, MouseKeys, Rotation, Xpos, Ypos
    End If
  Next ctl
End Sub

Private Sub ListaAnaSimples_DblClick()
    BtInsere_Click
End Sub

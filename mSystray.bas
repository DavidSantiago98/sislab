Attribute VB_Name = "mSystray"
'     .................................................................
'    .                  Module for notification icon.                  .
'   .                                                                   .
'   .                    Paulo Ferreira 2010.03.14                      .
'    .                         � 2010 GLINTT-HS                        .
'     .................................................................
Option Explicit
Private Declare Sub InitCommonControls Lib "Comctl32" ()

Public Function LoadSysTray(ByVal iconHandle As Long) As cSystray
    If gLAB = "HOSPOR" Then
        Exit Function
    End If
    InitCommonControls
    Dim f_cSystray As cSystray
    Set f_cSystray = New cSystray
    With f_cSystray
        .SysTrayIconFromHandle iconHandle
        .SysTrayToolTip = cAPLICACAO_NOME
        .SysTrayShow True
    End With
    
    ShowBalloon f_cSystray, TTIconInfo, "Bem-vindo " & gNomeUtilizador
    Set LoadSysTray = f_cSystray
    
End Function

Public Sub ShowMenu(f_cSystray As cSystray, MDIForm As MDIForm, menu As Object)

    f_cSystray.BeforePopup
    MDIForm.PopupMenu menu

End Sub

Public Sub ShowBalloon(f_cSystray As cSystray, iType As ttIconType, text As String)
    If gLAB = "HOSPOR" Then
        Exit Sub
    End If
    With f_cSystray
        If (.IsBalloonCapable) Then
            .BalloonIcon = iType
            .BalloonText = text
            .BalloonTitle = "SISLAB"
        End If
    If (.IsBalloonCapable) Then: .BalloonShow True, 10000, True
    End With

End Sub

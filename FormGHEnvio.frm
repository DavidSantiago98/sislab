VERSION 5.00
Begin VB.Form FormGHEnvio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormGHEnvio"
   ClientHeight    =   1185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5655
   Icon            =   "FormGHEnvio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1185
   ScaleWidth      =   5655
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPrinterListaAnaSemCorresp 
      Height          =   285
      Left            =   1800
      TabIndex        =   7
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   4335
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   1
         Top             =   360
         Width           =   975
      End
      Begin VB.Label lblDataInicio 
         Caption         =   "Data Inicio"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   855
      End
      Begin VB.Label lblDataFim 
         Caption         =   "Data Fim"
         Height          =   255
         Left            =   2280
         TabIndex        =   5
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.CommandButton BtValida 
      Caption         =   "Enviar"
      Height          =   855
      Left            =   4560
      Picture         =   "FormGHEnvio.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   " Enviar para a Factura��o "
      Top             =   240
      Width           =   975
   End
   Begin VB.ListBox EcListaAna 
      Height          =   1035
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "FormGHEnvio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 09/04/2003
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset
Dim ContaRubFact As Long

Private Sub DefTipoCampos()

    'Tipo Data
    EcDtIni.Tag = adDate
    EcDtFim.Tag = adDate
    
    'Tipo Inteiro
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
    
End Sub

Public Function Funcao_DataActual()
       
    Select Case CampoActivo.Name
        Case "EcDtIni", "EcDtFim"
            CampoActivo.Text = Bg_DaData_ADO
    End Select

End Function

Private Sub BtValida_Click()
    Dim RsVerf As ADODB.recordset
    Dim sql As String
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da factura��o de an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da factura��o de an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    gMsgTitulo = "Enviar An�lises para a Factura��o"
    gMsgMsg = "Tem a certeza que quer enviar as an�lises realizadas " & vbCrLf & _
              "entre as datas seleccionadas para a factura��o?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A enviar an�lises para a factura��o."
        Call Preenche_Tabela_GH
        BL_FimProcessamento Me
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDtIni_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtIni)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub



Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar ecran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Private Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    'EcDtIni.SetFocus
    'Set CampoActivo = EcDtIni
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormGHEnvio = Nothing
     
End Sub

Private Sub Inicializacoes()

    Me.caption = " Envio de an�lises para a Factura��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 5955
    Me.Height = 1620
    
    EcPrinterListaAnaSemCorresp = BL_SelImpressora("ListaAnaSemCorresp.rpt")
    
End Sub

Sub FuncaoLimpar()
    
    Me.SetFocus
    EcDtIni.SetFocus
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    EcListaAna.Clear
    
End Sub


Public Function GH_Nova_ANALISE_IN(n_req As String, _
                                       situacao As String, _
                                       num_episodio As String, _
                                       t_utente As String, _
                                       Utente As String, _
                                       cod_serv_req As String, _
                                       cod_resp As String, _
                                       cod_analise As String, _
                                       dt_ini_acto As String) As Integer
                                       

    Dim t_episodio As String
    Dim sql As String
    Dim rsHIS As ADODB.recordset
    Dim Cod_Analise_Aux As String
    Dim registos As Integer
    Dim sMsg As String
    
    cod_analise = Trim(cod_analise)
    situacao = Trim(situacao)
    num_episodio = Trim(num_episodio)
    t_utente = Trim(t_utente)
    Utente = Trim(Utente)
    cod_serv_req = Trim(cod_serv_req)
    cod_resp = Trim(cod_resp)
    dt_ini_acto = Format(Trim(dt_ini_acto), gFormatoData)
    n_req = Trim(n_req)
    
    'Enquanto n�o guardarmos o numero de espisodio da GH, passamos o espisodio = "000"
    num_episodio = "000"
    
    ' Por defeito a quantidade de an�lises pedidas � 1
    ' e flg_realizado = 'S'
    
    If (Len(cod_analise) = 0) Or (Len(t_utente) = 0) Or (Len(Utente) = 0) Or _
       (Len(dt_ini_acto) = 0) Or (Len(situacao) = 0) Then
        ' Par�metros incompletos.
        GH_Nova_ANALISE_IN = -2
        BG_Mensagem mediMsgBox, "Par�metros incompletos na inser��o da an�lise " & gCodAna_aux & " da requisi��o " & n_req, vbExclamation
        Exit Function
    End If
    
    ' Mapeia a situa��o do SISLAB para a GH.
    Select Case situacao
        Case gT_Urgencia
            t_episodio = "URGENCIAS"
        Case gT_Consulta
            t_episodio = "CONSULTAS"
        Case gT_Internamento
            t_episodio = "INTERNAMENTOS"
        Case gT_Externo
            t_episodio = "CONSULTAS"
        
        Case gT_Acidente
            t_episodio = "Acidente"
        Case gT_Ambulatorio
            t_episodio = "Ambulatorio"
        Case gT_Cons_Inter
            t_episodio = "Cons-Inter"
        Case gT_Cons_Telef
            t_episodio = "Cons-Telef"
        Case gT_Consumos
            t_episodio = "Consumos"
        Case gT_Credenciais
            t_episodio = "Credenciais"
        Case gT_Diagnosticos
            t_episodio = "Diagnosticos"
        Case gT_Exame
            t_episodio = "Exame"
        Case gT_Fisio
            t_episodio = "Fisio"
        Case gT_HDI
            t_episodio = "Hosp-Dia"
        Case gT_Intervencao
            t_episodio = "Intervencao"
        Case gT_MCDT
            t_episodio = "Mcdt"
        Case gT_Plano_Oper
            t_episodio = "Plano-Oper"
        Case gT_Pre_Intern
            t_episodio = "Pre-Intern"
        Case gT_Prog_Cirugico
            t_episodio = "Prog-Cirurgico"
        Case gT_Protoc
            t_episodio = "Protoc"
        Case gT_Referenciacao
            t_episodio = "Referenciacao"
        Case gT_Reg_Oper
            t_episodio = "Reg-Oper"
        Case gT_Tratamentos
            t_episodio = "Tratamentos"
        Case Else
            t_episodio = ""
    End Select
    
    ' Mapeia a an�lise so SISLAB para a GH.
    
    If (Len(t_episodio) > 0) And _
       (num_episodio <> "000") Then
    
        
        If Not (IsNumeric(num_episodio)) Then
            ' N� de epis�dio n�o � um n�mero.
            GH_Nova_ANALISE_IN = -3
            Exit Function
        End If
        
        ' Caso 1 : Situa��o (t_episodio) + num_episodio
    
        t_episodio = "'" & t_episodio & "'"
        num_episodio = "'" & num_episodio & "'"
        t_utente = "NULL"
        Utente = "NULL"
        cod_serv_req = "NULL"
        cod_resp = "NULL"
        dt_ini_acto = "NULL"
    
    Else
    
        ' Caso 2 : Situa��o (t_episodio) + t_doente + doente + data de inicio + cod_serv_req
        
        num_episodio = "'" & num_episodio & "'"
        t_episodio = "'" & t_episodio & "'"
        Utente = "'" & Utente & "'"
        t_utente = "'" & t_utente & "'"
        dt_ini_acto = "'" & Format(Trim(dt_ini_acto), gFormatoData) & "'"
        cod_serv_req = "'" & cod_serv_req & "'"

    
    End If
    
    sql = "INSERT INTO dados_fact " & vbCrLf & _
          "( " & vbCrLf & _
          "     cod_aplic,  t_doente, " & vbCrLf & _
          "     doente,     t_episodio, " & vbCrLf & _
          "     episodio,   dt_ini_acto, " & vbCrLf & _
          "     cod_resp,   cod_serv_req, " & vbCrLf & _
          "     n_cred,     cod_acto, " & vbCrLf & _
          "     qtd,        flg_realizado " & vbCrLf & _
          ") " & vbCrLf & _
          "VALUES " & vbCrLf & _
          "( " & vbCrLf & _
          "     'SISLAB',           " & t_utente & ", " & vbCrLf & _
          "     " & Utente & ",     " & t_episodio & ", " & vbCrLf & _
          "     " & num_episodio & "," & dt_ini_acto & ", " & vbCrLf & _
          "     " & cod_resp & ",   " & cod_serv_req & ", " & vbCrLf & _
          "     " & n_req & ",      " & cod_analise & ", " & vbCrLf & _
          "     " & 1 & ",'S')"
    
    gConnHIS.Execute sql, registos, adCmdText + adExecuteNoRecords
    If (registos <= 0) Then
        If gModoDebug = 1 Then BG_LogFile_Erros "(BG_ExecutaQuery: RecordsAffected = 0) -> " & sql
        sMsg = "Esta opera��o pode n�o ter sido efectuada." & vbCrLf & vbCrLf & "Raz�o prov�vel: Integridade dos Dados com a GH."
        If gModoDebug = 1 Then BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
        GH_Nova_ANALISE_IN = -1
        Exit Function
    End If
    
    GH_Nova_ANALISE_IN = 1
      

End Function

Sub Preenche_Tabela_GH()
    Dim CodAnaGH As String
    Dim CodAnaSONHO As String
    Dim HisAberto As Integer
    Dim sql As String
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = "SELECT sl_requis.n_req,sl_requis.dt_chega,sl_requis.n_epis,cod_efr,cod_proven, " & _
                     " sl_requis.t_sit,n_benef,estado_req,t_utente,utente, " & _
                     " seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, sl_realiza.flg_facturado " & _
                     " FROM sl_requis, sl_identif, sl_realiza " & _
                     " WHERE " & _
                     " sl_requis.seq_utente = sl_identif.seq_utente and " & _
                      " sl_requis.n_req = sl_realiza.n_req and " & _
                     " sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDtIni) & " and " & BL_TrataDataParaBD(EcDtFim) & _
                     " ORDER BY sl_requis.n_req"

              
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation
        FuncaoLimpar
    Else
        
        'Envia an�lises para a GH
        If (gSISTEMA_FACTURACAO = "GH") Then
            
            HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
            
            If (HisAberto = 1) Then
                ContaRubFact = 0
                While Not rs.EOF
                
                    If (BL_HandleNull(rs!Flg_Facturado, 0) = 0) Then
                        
                        CodAnaGH = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req)
                        
                        If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                        
                            BL_InicioProcessamento Me, "A enviar an�lises para a factura��o."
                            'inserir a analise no sonho o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                            Dim l As Integer
                            For l = 1 To gQuantidadeAFact
                                If (GH_Nova_ANALISE_IN(rs!n_req, rs!t_sit, BL_HandleNull(rs!n_epis, ""), _
                                    rs!t_utente, rs!Utente, BL_HandleNull(rs!cod_proven, ""), _
                                    BL_HandleNull(rs!cod_efr, ""), CodAnaGH, rs!dt_chega) = 1) Then
                                    
                                    sql = "UPDATE " & _
                                          "     sl_realiza " & _
                                          "SET " & _
                                          "     flg_facturado = 1 " & _
                                          "WHERE " & _
                                          "     seq_realiza = " & rs!seq_realiza
                                     
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    
                                    ContaRubFact = ContaRubFact + 1
                                    
                                End If
                            
                            If l < gQuantidadeAFact Then
                                gRsFact.MoveNext
                                CodAnaGH = BL_HandleNull(gRsFact!cod_ana_gh, "")
                            End If
                            Next l
                            
                            BL_FimProcessamento Me
                        
                        'An�lise ja facturada ou para n�o facturar(com codigoGH = null)
                        ElseIf CodAnaGH = "-2" Then
                            sql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sql
                            BG_Trata_BDErro
                        
                        'An�lise sem correspond�ncia no GH
                        ElseIf CodAnaGH = "-1" Then
                            EcListaAna.AddItem rs!cod_ana_s
                        End If
                    End If
                    rs.MoveNext
                Wend
            End If
            
        'Envia an�lises para o SONHO
        ElseIf gSISTEMA_FACTURACAO = "SONHO" Then
            
            HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
            
            If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
            
                While Not rs.EOF
                
                    If (BL_HandleNull(rs!Flg_Facturado, 0) = 0) Then
                    
                        CodAnaSONHO = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req)
                        
                        If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                            
                            BL_InicioProcessamento Me, "A enviar an�lises para o " & HIS.nome
                            
                            'inserir a analise no sonho o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                            For l = 1 To gQuantidadeAFact
                            If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                       rs!n_epis, _
                                                       rs!Utente, _
                                                       rs!cod_proven, _
                                                       CodAnaSONHO, _
                                                       rs!dt_chega, _
                                                       rs!n_req) = 1) Then
                                
                                sql = "UPDATE " & _
                                      "     sl_realiza " & _
                                      "SET " & _
                                      "     flg_facturado = 1 " & _
                                      "WHERE " & _
                                      "     seq_realiza = " & rs!seq_realiza
                                
                                BG_ExecutaQuery_ADO sql
                                BG_Trata_BDErro
                                
                                ContaRubFact = ContaRubFact + 1
                            
                            Else
'                                Sql = "Update sl_realiza set flg_facturado = 0 where seq_realiza = " & Rs!seq_realiza
'                                BG_ExecutaQuery_ADO Sql
'                                BG_Trata_BDErro
                            End If
                            If l < gQuantidadeAFact Then
                                gRsFact.MoveNext
                                CodAnaSONHO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                            End If
                            Next l
                            
                            BL_FimProcessamento Me
                        'An�lise j� facturada ou para n�o facturar (com codigo_GH = Null)
                        ElseIf CodAnaSONHO = "-2" Then
                            sql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sql
                            BG_Trata_BDErro
                        
                        'An�lise sem correspond�ncia no sonho
                        ElseIf CodAnaSONHO = "-1" Then
                            EcListaAna.AddItem rs!cod_ana_s
                        End If

                    End If
                    rs.MoveNext
                Wend
            End If

        End If
        
        BL_FimProcessamento Me
        
        'Imprime Lista de an�lises sem correspond�ncia
        If EcListaAna.ListCount > 0 Then
            'Imprime_AnaSemCorrespondencia
        End If
        
        Call BG_Mensagem(mediMsgBox, ContaRubFact & " rubricas enviadas para o " & UCase(HIS.nome) & ".", vbOKOnly + vbInformation, App.ProductName)
        
    End If
    
End Sub

Sub Imprime_AnaSemCorrespondencia()
    
    Dim Mg As Long
    Dim i As Long
    Dim LineHeight As Long
    Dim idade As String
    
    For i = 0 To Printers.Count - 1
        If Printers(i).DeviceName = EcPrinterListaAnaSemCorresp Then
            Set Printer = Printers(i)
            Exit For
        End If
    Next
    
    Mg = 1000
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    Printer.CurrentY = Mg
    Printer.CurrentX = Mg
    Printer.FontBold = True
    Printer.Print "Listagem de an�lises sem correspond�ncia no " & HIS.nome; ";"
    Printer.FontBold = False
    

    Printer.Print
    Printer.Line (Mg, Printer.CurrentY)-(Printer.Width - Mg, Printer.CurrentY)
    Printer.Print
    Printer.CurrentX = Mg
    Printer.Print "An�lises:"
    Printer.FontBold = True
    If EcListaAna.ListCount > 0 Then
        For i = 0 To EcListaAna.ListCount - 1
            Printer.CurrentX = 2 * Mg
            Printer.Print EcListaAna.List(i);
            'Printer.CurrentX = Printer.Width / 4
            'Printer.Print FGAna.TextMatrix(i, 1)
            Printer.CurrentY = Printer.CurrentY + LineHeight / 2
        Next
    Else
        Printer.CurrentX = 2 * Mg
        Printer.Print "(nenhuma)"
    End If
    Printer.FontBold = False
    Printer.EndDoc
    
End Sub


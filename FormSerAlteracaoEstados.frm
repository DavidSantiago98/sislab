VERSION 5.00
Begin VB.Form FormSerAlteracaoEstados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormSerAlteracaoEstados"
   ClientHeight    =   2475
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6315
   Icon            =   "FormSerAlteracaoEstados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2475
   ScaleWidth      =   6315
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtOk 
      Height          =   315
      Left            =   4920
      Picture         =   "FormSerAlteracaoEstados.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Adicionar R�brica ao FACTUS"
      Top             =   120
      Width           =   375
   End
   Begin VB.ComboBox CbEstado 
      Height          =   315
      Left            =   3120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   120
      Width           =   1575
   End
   Begin VB.TextBox EcAmostra 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   960
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.Label LbEstado 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Width           =   7335
   End
   Begin VB.Label LbNReq 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   5055
   End
   Begin VB.Label LbNome 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   7335
   End
   Begin VB.Label Label1 
      Caption         =   "Ac��o"
      Height          =   255
      Index           =   1
      Left            =   2280
      TabIndex        =   2
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Amostra"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormSerAlteracaoEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Private Sub EcNumReq_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub


Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub


Private Sub BtOk_Click()
    If EcAmostra <> "" And CbEstado.ListIndex <> mediComboValorNull Then
        AlteraEstado
    End If
End Sub


Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()
    Me.caption = " Alterar Estado de Amostras"
    Me.left = 5
    Me.top = 5
    Me.Width = 6405
    Me.Height = 2955 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcAmostra
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormSerAlteracaoEstados = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcAmostra.Text = ""
    CbEstado.ListIndex = mediComboValorNull
    LbEstado = ""
    LbNome = ""
    LbNReq = ""
End Sub

Sub DefTipoCampos()
'nada
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "SELECT cod_estado, accao FROM sl_tbf_estado_seroteca WHERE flg_alterar_estado = 1 ", "cod_estado", "accao", CbEstado, mediAscComboDesignacao
    
End Sub

Sub FuncaoProcurar()
    'nada
End Sub

Private Sub AlteraEstado()
    Dim dados() As String
    Dim sSql As String
    Dim rsID As New ADODB.recordset
    Dim rsEst As New ADODB.recordset
    Dim estados_Accao As String
    Dim estados() As String
    Dim estado_query As String
    Dim arquivos As String
    Dim i As Integer
    Dim seq_arquivo As String
    'VERIFICA SE VALOR INTRODUZIDO� VALIDO
    dados() = SER_DevolveReqTuboAli(EcAmostra)
    If dados(0) = "" Then
        BG_Mensagem mediMsgBox, "Amostra inv�lida", vbCritical, "Erro"
        Exit Sub
    End If
    
    If CbEstado.ListIndex = mediComboValorNull Then
        Exit Sub
    End If
    
    
    'VERIFICA QUE ESTADOS SAO ADMISSIVEIS PARA A ACCAO SELECCIONADA
    sSql = "SELECT * FROM sl_tbf_estado_seroteca WHERE cod_estado = " & CbEstado.ItemData(CbEstado.ListIndex)
    rsEst.CursorType = adOpenStatic
    rsEst.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEst.Open sSql, gConexao, adOpenStatic
    estado_query = ""
    If rsEst.RecordCount = 1 Then
        estados_Accao = BL_HandleNull(rsEst!estados_Accao, "")
        BL_Tokenize estados_Accao, ";", estados
        estado_query = "("
        For i = 0 To UBound(estados)
            estado_query = estado_query & estados(i) & ","
        Next
        estado_query = Mid(estado_query, 1, Len(estado_query) - 1) & ")"
    Else
        Exit Sub
    End If
    rsEst.Close
    
    'VERIFICA QUANTOS REGISTOS EXISTEM POSSIVEIS DA ACCAO SELECCIONADA
    sSql = "SELECT * FROM sl_ser_arquivo WHERE n_req = " & dados(0)
    If dados(1) <> "" Then
        sSql = sSql & " AND cod_tubo = " & BL_TrataStringParaBD(dados(1))
    End If
    If dados(2) <> "" Then
        sSql = sSql & " AND aliquota = " & BL_TrataStringParaBD(dados(2))
    End If
    sSql = sSql & " AND estado in " & estado_query
    rsEst.CursorType = adOpenStatic
    rsEst.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEst.Open sSql, gConexao, adOpenStatic
    If rsEst.RecordCount = 1 Then
        sSql = "SELECT x1.nome_ute, x2.n_req, x2.dt_chega FROM sl_identif x1, sl_Requis x2 WHERE x1.seq_utente =X2.seq_utente AND x2.n_req = " & dados(0)
        rsID.CursorType = adOpenStatic
        rsID.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsID.Open sSql, gConexao, adOpenStatic
        If rsID.RecordCount = 1 Then
            LbNReq = "Req.:" & dados(0) & " Tubo: " & dados(1)
            LbNReq = LbNReq & " Aliq.:" & dados(2)
            LbNome = BL_HandleNull(rsID!nome_ute, "")
        End If
        rsID.Close
        Set rsID = Nothing
        
        'APENAS EXISTE UM REGISTO, REGISTA LOGO MOVIMENTO
        If SER_RegistaAltArquivo(BL_HandleNull(rsEst!cod_seroteca, mediComboValorNull), BL_HandleNull(rsEst!cod_caixa, mediComboValorNull), _
                                    BL_HandleNull(rsEst!seq_reutil, mediComboValorNull), BL_HandleNull(rsEst!seq_arquivo, mediComboValorNull), _
                                    BL_HandleNull(rsEst!n_req, mediComboValorNull), CbEstado.ItemData(CbEstado.ListIndex), _
                                    BL_HandleNull(rsEst!cod_tubo, ""), BL_HandleNull(rsEst!aliquota, "")) = False Then
            GoTo TrataErro
        End If
    ElseIf rsEst.RecordCount > 1 Then
        'EXISTEM MAIS QUE UM MOVIMENTO, PERGUNTA QUAL �
        rsEst.MoveFirst
        arquivos = "("
        While Not rsEst.EOF
            arquivos = arquivos & BL_HandleNull(rsEst!seq_arquivo, mediComboValorNull) & ","
            rsEst.MoveNext
        Wend
        arquivos = Mid(arquivos, 1, Len(arquivos) - 1) & ")"
        seq_arquivo = PesquisaArquivos(arquivos)
        If seq_arquivo <> "" Then
            rsEst.MoveFirst
            While Not rsEst.EOF
                If seq_arquivo = BL_HandleNull(rsEst!seq_arquivo, mediComboValorNull) Then
                    If SER_RegistaAltArquivo(BL_HandleNull(rsEst!cod_seroteca, mediComboValorNull), BL_HandleNull(rsEst!cod_caixa, mediComboValorNull), _
                                                BL_HandleNull(rsEst!seq_reutil, mediComboValorNull), BL_HandleNull(rsEst!seq_arquivo, mediComboValorNull), _
                                                BL_HandleNull(rsEst!n_req, mediComboValorNull), CbEstado.ItemData(CbEstado.ListIndex), _
                                                BL_HandleNull(rsEst!cod_tubo, ""), BL_HandleNull(rsEst!aliquota, "")) = False Then
                        GoTo TrataErro
                    End If
                End If
                rsEst.MoveNext
            Wend
        End If
    ElseIf rsEst.RecordCount = 0 Then
        'N�O FOI ENCONTRADO QUALQUER REGISTO
        BG_Mensagem mediMsgBox, "N�o foi encontrado qualquer registo", vbCritical, "Erro"
    Else
        'ERRO
        GoTo TrataErro
    End If
    rsEst.Close
    Set rsEst = Nothing
    LbEstado = "Amostra " & BL_SelCodigo("sl_tbf_estado_seroteca", "descR_estado", "cod_estado", CbEstado.ItemData(CbEstado.ListIndex))
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraEstado: " & Err.Description & " " & sSql, Me.Name, "AlteraEstado", True
    Exit Sub
    Resume Next
End Sub

Private Function PesquisaArquivos(arquivos As String) As String
    
    Dim ChavesPesq(1 To 5) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 5) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 5) As Long
    Dim Headers(1 To 5) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 5)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_caixa"
    CamposEcran(1) = "Descr_caixa"
    Tamanhos(1) = 2000
    Headers(1) = "Suporte"
    
    ChavesPesq(2) = "sl_ser_arquivo.seq_reutil"
    CamposEcran(2) = "sl_ser_arquivo.seq_reutil"
    Tamanhos(2) = 1000
    Headers(2) = "Reutiliza��o"
    
    ChavesPesq(3) = "sl_ser_arquivo.seq_arquivo"
    CamposEcran(3) = "sl_ser_arquivo.seq_arquivo"
    Tamanhos(3) = 0
    Headers(3) = ""
    
    ChavesPesq(4) = "pos_x"
    CamposEcran(4) = "pos_x"
    Tamanhos(4) = 600
    Headers(4) = "Col"
    
    ChavesPesq(5) = "pos_y"
    CamposEcran(5) = "pos_y"
    Tamanhos(5) = 600
    Headers(5) = "Lin"
    
    CamposRetorno.InicializaResultados 5
    
    CFrom = "sl_ser_arquivo, sl_ser_caixas"
    CampoPesquisa = "descr_caixa"
    CWhere = " sl_ser_arquivo.cod_caixa = sl_ser_caixas.cod_caixa and seq_arquivo IN " & arquivos
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Suportes")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            PesquisaArquivos = resultados(3)
        End If
    Else
        PesquisaArquivos = ""
    End If
End Function



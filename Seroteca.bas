Attribute VB_Name = "Seroteca"

' --------------------------------------------------------------------------------------------------------------------------------------

' DEVOLVE SEQUENCIA PARA CAIXA E REUTILIZACAO

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function SER_DevolveIndice(tipo As String, cod_caixa As Long, cod_seroteca As Long) As Long
    Dim sSql As String
    Dim retorno As Long
    Dim rsSeroteca As New ADODB.recordset
    Dim registos As Integer
    On Error GoTo TrataErro
    SER_DevolveIndice = mediComboValorNull
    If tipo = "CAIXA" Then
        sSql = "SELECT max(cod_caixa) maximo FROM sl_ser_caixas "
        rsSeroteca.CursorLocation = adUseServer
        rsSeroteca.CursorType = adOpenStatic
        rsSeroteca.Open sSql, gConexao
        If rsSeroteca.RecordCount >= 1 Then
            retorno = BL_HandleNull(rsSeroteca!maximo, 0) + 1
            sSql = "UPDATE sl_serotecas SET num_caixa = " & retorno & " WHERE cod_seroteca = " & cod_seroteca
            registos = BG_ExecutaQuery_ADO(sSql)
        Else
            SER_DevolveIndice = mediComboValorNull
        End If
        rsSeroteca.Close
        Set rsSeroteca = Nothing
        SER_DevolveIndice = retorno
    ElseIf tipo = "REUTIL" Then
        sSql = "SELECT max(seq_reutil) maximo FROM sl_ser_reutil_caixa WHERE cod_seroteca = " & cod_seroteca
        rsSeroteca.CursorLocation = adUseServer
        rsSeroteca.CursorType = adOpenStatic
        rsSeroteca.Open sSql, gConexao
        If rsSeroteca.RecordCount >= 1 Then
            retorno = BL_HandleNull(rsSeroteca!maximo, 0) + 1
            sSql = "UPDATE sl_ser_caixas SET seq_reutil = " & retorno & " WHERE cod_seroteca = " & cod_seroteca & " AND cod_caixa = " & cod_caixa
            registos = BG_ExecutaQuery_ADO(sSql)
        Else
            SER_DevolveIndice = mediComboValorNull
        End If
        rsSeroteca.Close
        Set rsSeroteca = Nothing
        SER_DevolveIndice = retorno
    End If
Exit Function
TrataErro:
    SER_DevolveIndice = -1
    BG_LogFile_Erros "Erro  SER_DevolveIndice: " & sSql & " " & Err.Description, "SEOTECA", "SER_DevolveIndice"
    Exit Function
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' DADO VALOR DEVOLVE REQUISICAO, TUBO, ALIQUOTA

' --------------------------------------------------------------------------------------------------------------------------------------

Public Function SER_DevolveReqTuboAli(valor As String) As String()
    Dim req As String
    Dim tubo As String
    Dim aliquota As String
    Dim retorno(3) As String
    On Error GoTo TrataErro
    If valor <> "" Then
        If Len(valor) <= 7 And InStr(1, valor, ".") = 0 Then
            req = valor
            tubo = ""
            aliquota = ""
        ElseIf InStr(1, valor, ".") > 0 Then
            If Len(Mid(valor, 1, InStr(1, valor, ".") - 1)) <= 7 Then
                req = Mid(valor, 1, InStr(1, valor, ".") - 1)
                aliquota = Mid(valor, InStr(1, valor, ".") + 1)
                tubo = ""
            ElseIf Len(Mid(valor, 1, InStr(1, valor, ".") - 1)) = 9 Then
                req = Mid(valor, 1, InStr(1, valor, ".") - 1)
                aliquota = Mid(valor, InStr(1, valor, ".") + 1)
                tubo = Mid(req, 1, 2)
                req = CStr(CLng(Mid(req, 3)))
            End If
        ElseIf Len(valor) = 9 And InStr(1, valor, ".") = 0 Then
            req = Mid(valor, 3)
            tubo = Mid(valor, 1, 2)
            aliquota = ""
        End If
    End If
    retorno(0) = req
    retorno(1) = tubo
    retorno(2) = aliquota
    SER_DevolveReqTuboAli = retorno
Exit Function
TrataErro:
    SER_DevolveReqTuboAli = Empty
    BG_LogFile_Erros "Erro  SER_DevolveReqTuboAli: " & " " & Err.Description, "BL", "SER_DevolveReqTuboAli"
    Exit Function
End Function


' --------------------------------------------------------------------------------------------------------------------------------------

' REGISTA ALTERACOES DO ESTADO DA SEROTECA

' --------------------------------------------------------------------------------------------------------------------------------------

Public Function SER_RegistaAlteracao(cod_seroteca As Long, cod_caixa As Long, seq_reutil As Long, seq_arquivo As Long, n_req As Long, estado As Integer, _
                                  cod_tubo As String, aliquota As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    SER_RegistaAlteracao = False
    
    sSql = "INSERT INTO sl_ser_alteracoes (seq_alteracao,cod_seroteca, cod_caixa,seq_reutil,seq_arquivo, n_req, estado, dt_cri, hr_cri, user_cri,"
    sSql = sSql & " cod_tubo,aliquota) VALUES(seq_alteracao.nextval, "
    sSql = sSql & cod_seroteca & ", "
    sSql = sSql & cod_caixa & ", "
    sSql = sSql & seq_reutil & ", "
    sSql = sSql & seq_arquivo & ", "
    sSql = sSql & n_req & ", "
    sSql = sSql & estado & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_tubo) & ", "
    sSql = sSql & BL_TrataStringParaBD(aliquota) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    SER_RegistaAlteracao = True
Exit Function
TrataErro:
    SER_RegistaAlteracao = False
    BG_LogFile_Erros "SER_RegistaAlteracao: " & Err.Description & " " & sSql, "BL", "SER_RegistaAlteracao", True
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' REGISTA ALTERACOES DO ESTADO DA SEROTECA - SL_SER_ARQUIVO

' --------------------------------------------------------------------------------------------------------------------------------------

Public Function SER_RegistaAltArquivo(cod_seroteca As Long, cod_caixa As Long, seq_reutil As Long, seq_arquivo As Long, n_req As Long, estado As Integer, _
                                  cod_tubo As String, aliquota As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    SER_RegistaAltArquivo = False
    
    sSql = "UPDATE sl_ser_arquivo SET estado = " & estado & ", user_act = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
    sSql = sSql & ", dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", hr_act = " & BL_TrataStringParaBD(Bg_DaHora_ADO)
    sSql = sSql & " WHERE "
    sSql = sSql & " cod_seroteca = " & cod_seroteca
    sSql = sSql & " AND cod_caixa = " & cod_caixa
    sSql = sSql & " AND seq_arquivo = " & seq_arquivo
    sSql = sSql & " AND n_req  = " & n_req
    If cod_tubo <> "" Then
        sSql = sSql & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    Else
        sSql = sSql & " AND cod_tubo IS NULL"
    End If
    If aliquota <> "" Then
        sSql = sSql & " AND aliquota = " & BL_TrataStringParaBD(aliquota)
    Else
        sSql = sSql & " AND aliquota  IS NULL"
    End If
    iReg = BG_ExecutaQuery_ADO(sSql)
    
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    
    If SER_RegistaAlteracao(cod_seroteca, cod_caixa, seq_reutil, seq_arquivo, n_req, estado, cod_tubo, aliquota) = False Then
        GoTo TrataErro
    End If
    
    SER_RegistaAltArquivo = True
Exit Function
TrataErro:
    SER_RegistaAltArquivo = False
    BG_LogFile_Erros "SER_RegistaAltArquivo: " & Err.Description & " " & sSql, "BL", "SER_RegistaAltArquivo", True
    Exit Function
    Resume Next
End Function



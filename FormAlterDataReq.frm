VERSION 5.00
Begin VB.Form FormAlterDataReq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAlterDataReq"
   ClientHeight    =   2685
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5595
   Icon            =   "FormAlterDataReq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2685
   ScaleWidth      =   5595
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcHrChega 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4320
      TabIndex        =   8
      Top             =   1560
      Width           =   1095
   End
   Begin VB.TextBox EcDtChega 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4320
      TabIndex        =   7
      Top             =   960
      Width           =   1095
   End
   Begin VB.TextBox EcDtPrevi 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1560
      TabIndex        =   6
      Top             =   960
      Width           =   1095
   End
   Begin VB.TextBox EcNumReq 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1560
      TabIndex        =   5
      Top             =   360
      Width           =   1095
   End
   Begin VB.CommandButton BtConfirmar 
      Caption         =   "Validar"
      Height          =   735
      Left            =   2040
      Picture         =   "FormAlterDataReq.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1920
      Width           =   1215
   End
   Begin VB.CheckBox CkAlterTubos 
      Caption         =   "Alterar Produtos e Tubos"
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   1560
      Width           =   2055
   End
   Begin VB.Label LbHrChegada 
      Caption         =   "Hora Chegada"
      Height          =   255
      Left            =   3120
      TabIndex        =   9
      Top             =   1560
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Data Prevista"
      Height          =   255
      Index           =   2
      Left            =   360
      TabIndex        =   2
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Data Chegada"
      Height          =   255
      Index           =   1
      Left            =   3120
      TabIndex        =   1
      Top             =   960
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   975
   End
End
Attribute VB_Name = "FormAlterDataReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim dtPrevi  As String
Dim DtChega As String

Public rs As ADODB.recordset


Private Sub BtConfirmar_Click()

    'BRUNODSANTOS 01.06.2016 - ULSNE-1225
    If Trim(EcNumReq.Text) = "" Then
        EcNumReq.SetFocus
        Sendkeys ("{HOME}+{END}")
        Exit Sub
    ElseIf Trim(EcDtPrevi.Text) = "" Then
        EcDtPrevi.SetFocus
        Sendkeys ("{HOME}+{END}")
        Exit Sub
    ElseIf Trim(EcDtChega.Text) = "" Then
        EcDtChega.SetFocus
        Sendkeys ("{HOME}+{END}")
        Exit Sub
    End If
    '
    
    If ValidaDataHora(True) = False Then
        Exit Sub
    End If
    
    If CkAlterTubos.value = vbChecked Then
                
        'BRUNODSANTOS 01.06.2016 - ULSNE-1225
        If Trim(EcHrChega.Text) = "" Then
             BG_Mensagem mediMsgBox, "Tem de inserir uma hora de chegada.", vbInformation, "ATEN��O"
            EcHrChega.SetFocus
            Sendkeys ("{HOME}+{END}")
            Exit Sub
        Else
            If ValidaDataHora(False) = False Then
                Exit Sub
            End If
        End If
    End If
    
    On Error GoTo TrataErro
    Dim sSql As String
    Dim seq_alteracao As Integer
    BG_BeginTransaction
    sSql = "UPDATE sl_requis SET dt_chega = " & BL_TrataDataParaBD(EcDtChega) & ", dt_previ =" & BL_TrataDataParaBD(EcDtPrevi)
    sSql = sSql & ", user_act = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", dt_act =" & BL_TrataDataParaBD(Bg_DaData_ADO)
    sSql = sSql & ", hr_act = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & " WHERE n_req = " & EcNumReq
    BG_ExecutaQuery_ADO sSql
    seq_alteracao = BL_ContaAltRequis(EcNumReq) + 1

    If EcDtChega <> DtChega Then
        BL_RegistaAltRequis EcNumReq, "dt_chega", DtChega, EcDtChega, seq_alteracao
    End If
    If EcDtPrevi <> dtPrevi Then
        BL_RegistaAltRequis EcNumReq, "dt_previ", dtPrevi, EcDtPrevi, seq_alteracao
    End If
    
    If CkAlterTubos.value = vbChecked Then
                
        'BRUNODSANTOS 18.07.2016 - ULSNE-1225
        Dim dtHrReq() As String
        Dim hr_req As String
        Dim dt_req As String
        
        dtHrReq = DevolveDataHoraCriReq(EcNumReq.Text)
        hr_req = dtHrReq(1)
        dt_req = dtHrReq(2)
        
        'SE A DATA DE CHEGADA FOR SUPERIOR � DATA DE CRIA��O DE REQUISI��O N�O ATUALIZA DADOS
        If CDate(EcDtChega.Text) < CDate(dt_req) Then
            BG_Mensagem mediMsgBox, "A data de chegada do tubo � inferior � data da cria��o da requisi��o", vbInformation, "ATEN��O"
            EcDtChega.SetFocus
            Sendkeys ("{HOME}+{END}")
            Exit Sub
         End If
         
         'SE A DATA DE CHEGADA A MESMA QUE DATA DE CRIA��O DE REQUISI��O, VALIDA A HORA DE CHEGADA
         If CDate(EcDtChega.Text) = CDate(dt_req) Then
         
            'SE HORA DE CHEGADA FOR INFERIOR � HORA DE REQUISI��O, N�O ATUALIZA DADOS
            If left(EcHrChega.Text, 2) < left(hr_req, 2) Then
               BG_Mensagem mediMsgBox, "A hora de chegada do tubo � inferior � hora da cria��o da requisi��o", vbInformation, "ATEN��O"
               EcHrChega.SetFocus
               Sendkeys ("{HOME}+{END}")
               Exit Sub
           Else
               'SE HORA DE CHEGADA N�O FOR INFERIOR � HORA DE REQUISI��O, VALIDA MINUTOS
               If left(EcHrChega.Text, 2) = left(hr_req, 2) Then
                   If Right(EcHrChega.Text, 2) < Right(hr_req, 2) Then
                   BG_Mensagem mediMsgBox, "A hora de chegada do tubo � inferior � hora da cria��o da requisi��o", vbInformation, "ATEN��O"
                   EcHrChega.SetFocus
                   Sendkeys ("{HOME}+{END}")
                   Exit Sub
                   End If
               End If
               
           End If
           
        End If
        '
                
        'BRUNODSANTOS 01.06.2016 - ULSNE-1225
        
        sSql = "UPDATE sl_req_tubo SET dt_chega = " & BL_TrataDataParaBD(EcDtChega) & ", dt_previ =" & BL_TrataDataParaBD(EcDtPrevi) & ", hr_chega = " & BL_TrataStringParaBD(EcHrChega.Text)
        sSql = sSql & ", user_chega = " & gCodUtilizador & " WHERE n_req = " & EcNumReq & " AND dt_chega is not null "
        BG_ExecutaQuery_ADO sSql
        
        sSql = "UPDATE sl_req_prod SET dt_chega = " & BL_TrataDataParaBD(EcDtChega) & ", dt_previ =" & BL_TrataDataParaBD(EcDtPrevi)
        sSql = sSql & " WHERE n_req = " & EcNumReq
        BG_ExecutaQuery_ADO sSql
    
        sSql = "UPDATE SL_MARCACOES SET dt_chega = " & BL_TrataDataParaBD(EcDtChega)
        sSql = sSql & " WHERE n_req = " & EcNumReq
        BG_ExecutaQuery_ADO sSql
        
        sSql = "UPDATE SL_REALIZA SET dt_chega = " & BL_TrataDataParaBD(EcDtChega)
        sSql = sSql & " WHERE n_req = " & EcNumReq
        BG_ExecutaQuery_ADO sSql
        '
    End If
    
    BG_CommitTransaction
    BG_Mensagem mediMsgBox, "Dados alterados com sucesso.", vbInformation, "SISLAB"

Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro ao gravar altera��es" & Err.Description, Me.Name, "BtConfirmar_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub CkAlterTubos_Click()

'BRUNODSANTOS 01.06.2016 - ULSNE-1225
If CkAlterTubos.value = vbChecked Then
    LbHrChegada.Visible = True
    EcHrChega.Visible = True
Else
    LbHrChegada.Visible = False
    EcHrChega.Visible = False
End If
'
End Sub


Private Sub EcDtchega_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtChega)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtPrevi_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtPrevi)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcHrChega_Change()

    'BRUNODSANTOS 01.06.2016 - ULSNE-1225
'    If Len(EcHrChega.Text) = 2 Then
'    EcHrChega.Text = EcHrChega.Text & ":"
'    EcHrChega.SelStart = Len(EcHrChega.Text)
'    EcHrChega.SetFocus
'    End If
    '
End Sub

'BRUNODSANTOS 01.06.2016 - ULSNE-1225
Private Sub EcHrChega_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyBack Then
'        EcHrChega.Text = ""
'    End If
End Sub

Private Sub EcNumReq_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub


Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub


Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Alterar Datas das Requisi��es"
    
    Me.left = 5
    Me.top = 5
    Me.Width = 5685
    Me.Height = 3105 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
      
      
    'BRUNODSANTOS 01.06.2016 - ULSNE-1225
    LbHrChegada.Visible = False
    EcHrChega.Visible = False
    EcHrChega.MaxLength = 5
    '
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormAlterDataReq = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcNumReq.Text = ""
    EcDtChega = ""
    EcDtPrevi = ""
    EcNumReq.Locked = False
    dtPrevi = ""
    DtChega = ""
    CkAlterTubos.value = vbUnchecked
    'BRUNODSANTOS 01.06.2016 - ULSNE-1225
    EcHrChega = ""
    '
End Sub

Sub DefTipoCampos()
    EcDtChega.Tag = adDate
    EcDtPrevi.Tag = adDate
    EcNumReq.Tag = adInteger
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    
End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    On Error GoTo TrataErro
    If EcNumReq = "" Then
        BG_Mensagem mediMsgBox, "Campo N� de Requisi��o � obrigat�rio.", vbCritical, "ATEN��O"
        Exit Sub
    End If
    sSql = "SELECT dt_previ, dt_chega FROM sl_requis WHERE n_req = " & EcNumReq
    Set rs = New ADODB.recordset
    With rs
        .Source = sSql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Open , gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    End With
    If rs.RecordCount = 1 Then
        
        dtPrevi = BL_HandleNull(rs!dt_previ, "")
        DtChega = BL_HandleNull(rs!dt_chega, "")
        EcDtPrevi = dtPrevi
        EcDtChega = DtChega
        
        EcNumReq.Locked = True
    End If
    rs.Close
    Set rs = Nothing
    
 'BRUNODSANTOS 01.06.2016 - ULSNE-1225
 EcDtChega.Tag = adDate
 EcDtPrevi.Tag = adDate
 '
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar Requisi��o" & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub

'BRUNODSANTOS 01.06.2016 - ULSNE-1225
Private Function ValidaDataHora(flg_validaData As Boolean) As Boolean

    Dim dataActual As Date
    Dim dataAlteradaPrevista As Date
    Dim dataAlteradaChegada As Date
    Dim horaActual As String
    Dim horaAlterada As String
    
    ValidaDataHora = True
    dataActual = Bg_DaData_ADO
    dataAlteradaChegada = CDate(EcDtChega.Text)
    dataAlteradaPrevista = CDate(EcDtPrevi.Text)
    horaActual = Bg_DaHora_ADO
    horaAlterada = EcHrChega.Text
    
    If flg_validaData = True Then
            
        If dataAlteradaChegada > dataActual Then
            BG_Mensagem mediMsgBox, "N�o pode inserir uma data superior � data actual!", vbCritical, "ATEN��O"
            EcDtChega.SetFocus
            Sendkeys ("{HOME}+{END}")
            ValidaDataHora = False
            
        End If
    Else
       
    
        Dim horas As String
        Dim minutos As String
        Dim separador As String
        
        horas = left(EcHrChega.Text, 2)
        minutos = Right(EcHrChega.Text, 2)
        separador = Mid(EcHrChega.Text, 3, 1)
        
        If IsNumeric(horas) = False Or IsNumeric(minutos) = False Or separador <> ":" Then
            BG_Mensagem mediMsgBox, "Formato hora inv�lido(hh:mm)", vbInformation, "ATEN��O"
            EcHrChega.SetFocus
            Sendkeys ("{HOME}+{END}")
            ValidaDataHora = False
            
        ElseIf horas >= 24 Or horas < 0 Or minutos > 59 Or minutos < 0 Then
        
            BG_Mensagem mediMsgBox, "Formato hora inv�lido(hh:mm)", vbInformation, "ATEN��O"
            EcHrChega.SetFocus
            Sendkeys ("{HOME}+{END}")
            ValidaDataHora = False
            
        ElseIf CInt(left(horaAlterada, 2)) > CInt(left(horaActual, 2)) Then
            If dataAlteradaChegada >= dataActual Then
                BG_Mensagem mediMsgBox, "N�o pode inserir uma hora superior � hora actual!", vbCritical, "ATEN��O"
                EcHrChega.SetFocus
                Sendkeys ("{HOME}+{END}")
                ValidaDataHora = False
            End If
            
        ElseIf CInt(Right(horaAlterada, 2)) > CInt(Right(horaActual, 2)) Then
            If dataAlteradaChegada >= dataActual Then
                BG_Mensagem mediMsgBox, "N�o pode inserir uma hora superior � hora actual!", vbCritical, "ATEN��O"
                EcHrChega.SetFocus
                Sendkeys ("{HOME}+{END}")
                ValidaDataHora = False
            End If
        End If
                
    End If
End Function


'BRUNODSANTOS 18.07.2016 - ULSNE-1225
Public Function DevolveDataHoraCriReq(ByVal NumReq As String) As String()

    Dim sql As String
    Dim rs As ADODB.recordset
    Dim arrStr() As String
    ReDim arrStr(2)
    On Error GoTo TrataErro
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
     
    sql = "SELECT hr_cri,dt_cri FROM sl_requis WHERE n_req = " & NumReq
    
    rs.Open sql, gConexao
     
    
   If rs.RecordCount > 0 Then
        arrStr(1) = BL_HandleNull(rs.Fields("hr_cri").value, "")
        arrStr(2) = BL_HandleNull(rs.Fields("dt_cri").value, "")
    End If
    
     DevolveDataHoraCriReq = arrStr
    
    If rs.state = adStateOpen Then
        rs.Close
    End If
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
    
    Exit Function
    
  
TrataErro:
    BG_LogFile_Erros "DevolveHoraCriReq" & Err.Description, Me.Name, "DevolveHoraCriReq"
    Exit Function
    Resume Next
    
End Function

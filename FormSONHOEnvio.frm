VERSION 5.00
Begin VB.Form FormSONHOEnvio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormSONHOEnvio"
   ClientHeight    =   1560
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5880
   Icon            =   "FormSONHOEnvio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   5880
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   1320
      TabIndex        =   0
      Top             =   120
      Width           =   1335
   End
   Begin VB.TextBox EcPrinterListaAnaSemCorresp 
      Height          =   285
      Left            =   1440
      TabIndex        =   8
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Data de Valida��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   4575
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1200
         TabIndex        =   1
         Top             =   400
         Width           =   975
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3240
         TabIndex        =   2
         Top             =   400
         Width           =   975
      End
      Begin VB.Label lblDataInicio 
         Caption         =   "Data Inicial"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   405
         Width           =   855
      End
      Begin VB.Label lblDataFim 
         Caption         =   "Data Final"
         Height          =   255
         Left            =   2400
         TabIndex        =   6
         Top             =   405
         Width           =   735
      End
   End
   Begin VB.CommandButton BtValida 
      Caption         =   "Enviar"
      Height          =   855
      Left            =   4800
      Picture         =   "FormSONHOEnvio.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   " Enviar para a Factura��o "
      Top             =   580
      Width           =   975
   End
   Begin VB.ListBox EcListaAna 
      Height          =   1035
      Left            =   120
      TabIndex        =   4
      Top             =   1920
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label LaNrInscricao 
      AutoSize        =   -1  'True
      Caption         =   "Nr. Requisi��o"
      Height          =   195
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   1050
   End
End
Attribute VB_Name = "FormSONHOEnvio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 09/04/2003
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset
Dim ContaRubFact As Long

Private Sub DefTipoCampos()

'    'Tipo Data
'    EcDtIni.Tag = adDate
'    EcDtFim.Tag = adDate
'
'    'Tipo Inteiro
'    EcDtIni.MaxLength = 10
'    EcDtFim.MaxLength = 10
    
    EcDtIni.Tag = mediTipoData
    EcDtFim.Tag = mediTipoData
    EcNumReq.Tag = adInteger
    
End Sub

Public Function Funcao_DataActual()
       
    Select Case CampoActivo.Name
        Case "EcDtIni", "EcDtFim"
            CampoActivo.Text = Bg_DaData_ADO
    End Select

End Function

Private Sub BtValida_Click()
    Dim RsVerf As ADODB.recordset
    Dim sql As String
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" And EcNumReq.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da factura��o de an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" And EcNumReq.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da factura��o de an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    gMsgTitulo = "Enviar An�lises para a Factura��o"
    gMsgMsg = "Tem a certeza que quer enviar as an�lises realizadas " & vbCrLf & _
              "entre as datas seleccionadas para a factura��o?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A enviar an�lises para a factura��o."
        Call Envia_SONHO
        BL_FimProcessamento Me
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDtIni_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtIni)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar ecran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Private Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    'EcDtIni.SetFocus
    'Set CampoActivo = EcDtIni
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormGHEnvio = Nothing
     
End Sub

Private Sub Inicializacoes()

    Me.caption = " Envio de an�lises para a Factura��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 5955
    Me.Height = 1935
    
    EcPrinterListaAnaSemCorresp = BL_SelImpressora("ListaAnaSemCorresp.rpt")
    
End Sub

Sub FuncaoLimpar()
    
    Me.SetFocus
    EcNumReq.SetFocus
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    EcListaAna.Clear
    
End Sub

Sub Envia_SONHO()
    Dim CodAnaGH As String
    Dim CodAnaSONHO As String
    Dim HisAberto As Integer
    Dim sql As String
    Dim SQLQuery As String
    Dim l As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = "SELECT distinct sl_requis.n_req,sl_requis.dt_chega,sl_requis.n_epis,cod_efr,cod_proven, " & _
                     " sl_requis.t_sit,n_benef,estado_req,t_utente,utente, " & _
                     " sl_realiza.seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, cod_agrup, sl_realiza.flg_facturado, " & _
                     " sl_res_micro.n_res,sl_res_micro.cod_micro,sl_res_micro.flg_tsq,sl_res_micro.prova prova,sl_res_micro.cod_gr_antibio,sl_microrg.metodo_tsq, sl_microrg.prova prova_micro " & _
                     " FROM sl_requis, sl_identif, sl_realiza, sl_res_micro, sl_microrg " & _
                     " WHERE " & _
                     " sl_requis.seq_utente = sl_identif.seq_utente and " & _
                     " sl_requis.n_req = sl_realiza.n_req and " & _
                     " sl_realiza.seq_realiza = sl_res_micro.seq_realiza(+) and " & _
                     " sl_res_micro.cod_micro = sl_microrg.cod_microrg(+) and " & _
                     " sl_realiza.flg_estado in (3,4) and (sl_realiza.flg_facturado is null or sl_realiza.flg_facturado = 0) "

    If EcNumReq <> "" Then
        CriterioTabela = CriterioTabela & " AND sl_requis.n_req = " & EcNumReq & " "
    Else
        CriterioTabela = CriterioTabela & " AND sl_realiza.dt_val between " & BL_TrataDataParaBD(EcDtIni) & " and " & BL_TrataDataParaBD(EcDtFim)
    End If
    
    CriterioTabela = CriterioTabela & " ORDER BY sl_requis.n_req, sl_realiza.seq_realiza, sl_realiza.cod_perfil, sl_realiza.cod_ana_c, sl_realiza.cod_ana_s "
                       
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation
        FuncaoLimpar
    Else

        'Envia an�lises para o SONHO
        If gSISTEMA_FACTURACAO = "SONHO" Then
            
            HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
            
            If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
            
                While Not rs.EOF
                        
                    If BL_HandleNull(rs!cod_micro, "") <> "" Then
                        'Facturar a identifica��o do microrganismo
                        
                        CodAnaSONHO = Mapeia_Microrg_para_FACTURACAO(rs!cod_micro, BL_HandleNull(rs!Prova, BL_HandleNull(rs!Prova_micro, "")), rs!n_req)
                        
                        If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                            For l = 1 To gQuantidadeAFact
                                If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                            BL_HandleNull(rs!n_epis, ""), _
                                                            rs!Utente, _
                                                            rs!cod_proven, _
                                                            CodAnaSONHO, _
                                                            rs!dt_chega, _
                                                            rs!n_req, _
                                                            rs!cod_agrup) = 1) Then
                                                            
                                    'actualiza flg_facturado
                                    sql = "UPDATE " & _
                                          "     sl_realiza " & _
                                          "SET " & _
                                          "     flg_facturado = 1 " & _
                                          "WHERE " & _
                                          "     seq_realiza = " & rs!seq_realiza
                                    
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    
                                    ContaRubFact = ContaRubFact + 1
                                End If
                                
                                If l < gQuantidadeAFact Then
                                    gRsFact.MoveNext
                                    CodAnaSONHO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                                End If
                            Next l
                        End If
                        gRsFact.Close
                        Set gRsFact = Nothing
                        
                        If BL_HandleNull(rs!flg_tsq, "") = "S" Then
                            
                            'Facturar o antibiograma de acordo com o(s) grupo(s) de antibioticos registado(s)
                            If BL_HandleNull(rs!Cod_Gr_Antibio, "") <> "" Then
                                Dim PosAux As Integer
                                Dim CodGrAntibio As String
                                Dim CodGrAntibioAux As String
                                Dim i As Integer
                                Dim AntibFacturado As Boolean
                                Dim UltimaCarta As String
                                CodGrAntibio = rs!Cod_Gr_Antibio
                                PosAux = 1
                                UltimaCarta = ""
                                While Not PosAux = 0
                                    If UltimaCarta <> CodGrAntibio Then
                                    PosAux = InStr(1, CodGrAntibio, ";")
                                    If PosAux <> 0 Then
                                        CodGrAntibioAux = Mid(CodGrAntibio, 1, PosAux - 1)
                                    Else
                                        CodGrAntibioAux = CodGrAntibio
                                    End If
                                    CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, CodGrAntibioAux, , rs!n_req)
                                    
                                    If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                                        If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                    BL_HandleNull(rs!n_epis, ""), _
                                                                    rs!Utente, _
                                                                    rs!cod_proven, _
                                                                    CodAnaSONHO, _
                                                                    rs!dt_chega, _
                                                                    rs!n_req, _
                                                                    rs!cod_agrup) = 1) Then
                                            ContaRubFact = ContaRubFact + 1
                                            
                                        End If
                                    End If
                                    UltimaCarta = CodGrAntibioAux
                                    CodGrAntibio = Mid(CodGrAntibio, PosAux + 1)
                                    End If
                                Wend
                                
                            'Facturar o antibiograma de acordo com o m�todo da codifica��o de microorganismos
                            ElseIf BL_HandleNull(rs!Metodo_Tsq, "") <> "" And BL_HandleNull(rs!Metodo_Tsq, "") <> "2" And BL_HandleNull(rs!Metodo_Tsq, "") <> "3" Then
                            
                                CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , BL_HandleNull(rs!Metodo_Tsq, ""), rs!n_req)
                                
                                If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                                    If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                BL_HandleNull(rs!n_epis, ""), _
                                                                rs!Utente, _
                                                                rs!cod_proven, _
                                                                CodAnaSONHO, _
                                                                rs!dt_chega, _
                                                                rs!n_req, _
                                                                rs!cod_agrup) = 1) Then
                                        ContaRubFact = ContaRubFact + 1
                                    End If
                                End If
                                
                            End If
                            
                            'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
                            Dim RsFactAntib As ADODB.recordset
                            Set RsFactAntib = New ADODB.recordset
                            sql = " SELECT sl_res_tsq.cod_antib, cod_metodo from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
                                  " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
                                  " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
                                  " and seq_realiza = " & rs!seq_realiza & " and n_res = " & rs!N_Res & _
                                  " and cod_micro = " & BL_TrataStringParaBD(rs!cod_micro) & "" & _
                                  " and cod_metodo in ( 2,3) "
                            RsFactAntib.CursorType = adOpenStatic
                            RsFactAntib.CursorLocation = adUseServer
                            RsFactAntib.Open sql, gConexao
                            If RsFactAntib.RecordCount > 0 Then
                                While Not RsFactAntib.EOF
                                    CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , RsFactAntib!cod_metodo, rs!n_req)
                                    If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then

                                        If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                    BL_HandleNull(rs!n_epis, ""), _
                                                                    rs!Utente, _
                                                                    rs!cod_proven, _
                                                                    CodAnaSONHO, _
                                                                    rs!dt_chega, _
                                                                    rs!n_req, _
                                                                    rs!cod_agrup) = 1) Then
                                            
                                            ContaRubFact = ContaRubFact + 1
                                            
                                        End If
                                    End If
                                    RsFactAntib.MoveNext
                                Wend
                            End If
                        End If
                    
                    Else
                        
                        CodAnaSONHO = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req, rs!cod_agrup)
                        
                        If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                            
                            'Facturar a an�lise mapeada
                            BL_InicioProcessamento Me, "A enviar an�lises para o " & HIS.nome
                            
                            'inserir a analise no sonho o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                            For l = 1 To gQuantidadeAFact
                                If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                           BL_HandleNull(rs!n_epis, ""), _
                                                           rs!Utente, _
                                                           rs!cod_proven, _
                                                           CodAnaSONHO, _
                                                           rs!dt_chega, _
                                                           rs!n_req, _
                                                           rs!cod_agrup) = 1) Then
                                    
                                    sql = "UPDATE " & _
                                          "     sl_realiza " & _
                                          "SET " & _
                                          "     flg_facturado = 1 " & _
                                          "WHERE " & _
                                          "     seq_realiza = " & rs!seq_realiza
                                    
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    
                                    ContaRubFact = ContaRubFact + 1
                                
                                End If
                                If l < gQuantidadeAFact Then
                                    gRsFact.MoveNext
                                    CodAnaSONHO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                                End If
                            Next l
                            
                            BL_FimProcessamento Me
                            
                            gRsFact.Close
                            Set gRsFact = Nothing
                        
                        'An�lise j� facturada ou para n�o facturar (com codigo_GH = Null)
                        ElseIf CodAnaSONHO = "-2" Then
                            sql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sql
                            BG_Trata_BDErro
                        
                        'An�lise sem correspond�ncia no sonho
                        ElseIf CodAnaSONHO = "-1" Then
                            EcListaAna.AddItem rs!cod_ana_s
                        End If
                    
                    End If
                    rs.MoveNext
                Wend
            End If

        End If
        BL_Fecha_conexao_HIS
        BL_FimProcessamento Me
        
        Call BG_Mensagem(mediMsgBox, ContaRubFact & " rubricas enviadas para o " & UCase(gSISTEMA_FACTURACAO) & ".", vbOKOnly + vbInformation, App.ProductName)
        
    End If
    
End Sub

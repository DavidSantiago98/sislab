Attribute VB_Name = "ModuloOrdenacaoVectores"

Public Function SORT_Array(ByRef dataArray() As String, ByRef indexArray() As String, ByVal sortType As String, ByVal orderArray As String) As Integer
    On Error GoTo TrataErro
    
    Dim auxDataArray() As String
    Dim auxIndexArray() As String
    
    auxDataArray = dataArray
    auxIndexArray = indexArray
    
    If SORT_MergeSort1(auxDataArray, auxIndexArray, sortType) = 1 Then
        If orderArray = cSortOrderAsc Then
            dataArray = auxDataArray
            indexArray = auxIndexArray
            SORT_Array = 1
            Exit Function
        ElseIf orderArray = cSortOrderDesc Then
            If SORT_InvertArray(auxDataArray, auxIndexArray) = 1 Then
                dataArray = auxDataArray
                indexArray = auxIndexArray
                SORT_Array = 1
                Exit Function
            Else
                SORT_Array = -1
                Exit Function
            End If
        Else
            SORT_Array = -1
            Exit Function
        End If
    Else
    
    End If
    
    SORT_Array = -1
    Exit Function
TrataErro:
    SORT_Array = -1
    BG_LogFile_Erros "SORT_Array: " & Err.Number & " - " & Err.Description
    Exit Function
    Resume Next
End Function

Public Function SORT_InvertArray(ByRef dataArray() As String, ByRef indexArray() As String, Optional sortType As String) As Integer

    On Error GoTo TrataErro
    
    Dim i As Integer
    Dim auxDataArray() As String
    Dim auxIndexArray() As String
    
    ReDim auxDataArray(UBound(dataArray)) As String
    ReDim auxIndexArray(UBound(indexArray)) As String
    
    
    For i = LBound(dataArray) To UBound(dataArray)
        auxDataArray(i) = dataArray(UBound(dataArray) - i)
        auxIndexArray(i) = indexArray(UBound(dataArray) - i)
    Next i
    
    dataArray = auxDataArray
    indexArray = auxIndexArray
    SORT_InvertArray = 1
    
    Exit Function
TrataErro:
    SORT_InvertArray = -1
    BG_LogFile_Erros "SORT_InvertArray: " & Err.Number & " - " & Err.Description
    Exit Function
End Function

Public Function SORT_MergeSort1(ByRef dataArray() As String, ByRef indexArray() As String, ByVal sortType As String, Optional pvarMirror As Variant, _
    Optional pvarIndexMirror As Variant, Optional ByVal plngLeft As Long, Optional ByVal plngRight As Long) As Integer
    
    On Error GoTo TrataErro
    
    Dim lngMid As Long
    Dim l As Long
    Dim r As Long
    Dim O As Long
    Dim varSwap As String
    
    If plngRight = 0 Then
        plngLeft = LBound(dataArray)
        plngRight = UBound(dataArray)
        ReDim pvarMirror(plngLeft To plngRight)
        ReDim pvarIndexMirror(plngLeft To plngRight)
    End If
    lngMid = plngRight - plngLeft
    Select Case lngMid
        Case 0
        Case 1
            Select Case sortType
                Case cSortTypeDate
                    If IsDate(dataArray(plngLeft)) And IsDate(dataArray(plngRight)) Then
                        If CDate(dataArray(plngLeft)) > CDate(dataArray(plngRight)) Then
                            varSwap = dataArray(plngLeft)
                            dataArray(plngLeft) = dataArray(plngRight)
                            dataArray(plngRight) = varSwap
                            varSwap = indexArray(plngLeft)
                            indexArray(plngLeft) = indexArray(plngRight)
                            indexArray(plngRight) = varSwap
                        End If
                    Else ' ordena como string
                        If SORT_MergeSort1(dataArray, indexArray, cSortTypeString) <= 0 Then
                            SORT_MergeSort1 = -1
                            Exit Function
                        End If
                    End If
                Case cSortTypeString
                    If UCase(CStr(dataArray(plngLeft))) > UCase(CStr(dataArray(plngRight))) Then
                        varSwap = dataArray(plngLeft)
                        dataArray(plngLeft) = dataArray(plngRight)
                        dataArray(plngRight) = varSwap
                        varSwap = indexArray(plngLeft)
                        indexArray(plngLeft) = indexArray(plngRight)
                        indexArray(plngRight) = varSwap
                    End If
                Case cSortTypeLong
                    If IsNumeric(dataArray(plngLeft)) And IsNumeric(dataArray(plngRight)) Then
                        If CLng(dataArray(plngLeft)) > CLng(dataArray(plngRight)) Then
                            varSwap = dataArray(plngLeft)
                            dataArray(plngLeft) = dataArray(plngRight)
                            dataArray(plngRight) = varSwap
                            varSwap = indexArray(plngLeft)
                            indexArray(plngLeft) = indexArray(plngRight)
                            indexArray(plngRight) = varSwap
                        End If
                    Else ' ordena como string
                        If SORT_MergeSort1(dataArray, indexArray, cSortTypeString) <= 0 Then
                            SORT_MergeSort1 = -1
                            Exit Function
                        End If
                    End If
                Case cSortTypeDouble
                    If IsNumeric(dataArray(plngLeft)) And IsNumeric(dataArray(plngRight)) Then
                        If BL_String2Double(CStr(dataArray(plngLeft))) > BL_String2Double(CStr(dataArray(plngRight))) Then
                            varSwap = dataArray(plngLeft)
                            dataArray(plngLeft) = dataArray(plngRight)
                            dataArray(plngRight) = varSwap
                            varSwap = indexArray(plngLeft)
                            indexArray(plngLeft) = indexArray(plngRight)
                            indexArray(plngRight) = varSwap
                        End If
                    Else ' ordena como string
                        If SORT_MergeSort1(dataArray, indexArray, cSortTypeString) <= 0 Then
                            SORT_MergeSort1 = -1
                            Exit Function
                        End If
                    End If
                Case Else
                    SORT_MergeSort1 = -1
                    Exit Function
            End Select
            
        Case Else
            lngMid = lngMid \ 2 + plngLeft
            If SORT_MergeSort1(dataArray, indexArray, sortType, pvarMirror, pvarIndexMirror, plngLeft, lngMid) <= 0 Then
                SORT_MergeSort1 = -1
                Exit Function
            End If
            If SORT_MergeSort1(dataArray, indexArray, sortType, pvarMirror, pvarIndexMirror, lngMid + 1, plngRight) <= 0 Then
                SORT_MergeSort1 = -1
                Exit Function
            End If
            ' Merge the resulting halves
            l = plngLeft ' start of first (left) half
            r = lngMid + 1 ' start of second (right) half
            O = plngLeft ' start of output (mirror array)
            Do
                Select Case sortType
                    Case cSortTypeDate
                        If Not (IsDate(dataArray(r)) And IsDate(dataArray(l))) Then
                            pvarMirror(O) = dataArray(l)
                            pvarIndexMirror(O) = indexArray(l)
                            l = l + 1
                            If l > lngMid Then
                                For r = r To plngRight
                                    O = O + 1
                                    pvarMirror(O) = dataArray(r)
                                    pvarIndexMirror(O) = indexArray(r)
                                Next
                                Exit Do
                            End If
                        Else
                            If CDate(dataArray(r)) < CDate(dataArray(l)) Then
                                pvarMirror(O) = dataArray(r)
                                pvarIndexMirror(O) = indexArray(r)
                                r = r + 1
                                If r > plngRight Then
                                    For l = l To lngMid
                                        O = O + 1
                                        pvarMirror(O) = dataArray(l)
                                        pvarIndexMirror(O) = indexArray(l)
                                    Next
                                    Exit Do
                                End If
                            Else
                                pvarMirror(O) = dataArray(l)
                                pvarIndexMirror(O) = indexArray(l)
                                l = l + 1
                                If l > lngMid Then
                                    For r = r To plngRight
                                        O = O + 1
                                        pvarMirror(O) = dataArray(r)
                                        pvarIndexMirror(O) = indexArray(r)
                                    Next
                                    Exit Do
                                End If
                            End If
                        End If
                    Case cSortTypeString
                        If UCase(CStr(dataArray(r))) < UCase(CStr(dataArray(l))) Then
                            pvarMirror(O) = dataArray(r)
                            pvarIndexMirror(O) = indexArray(r)
                            r = r + 1
                            If r > plngRight Then
                                For l = l To lngMid
                                    O = O + 1
                                    pvarMirror(O) = dataArray(l)
                                    pvarIndexMirror(O) = indexArray(l)
                                Next
                                Exit Do
                            End If
                        Else
                            pvarMirror(O) = dataArray(l)
                            pvarIndexMirror(O) = indexArray(l)
                            l = l + 1
                            If l > lngMid Then
                                For r = r To plngRight
                                    O = O + 1
                                    pvarMirror(O) = dataArray(r)
                                    pvarIndexMirror(O) = indexArray(r)
                                Next
                                Exit Do
                            End If
                        End If
                    Case cSortTypeLong
                        If Not (IsNumeric(dataArray(r)) And IsNumeric(dataArray(l))) Then
                            pvarMirror(O) = dataArray(l)
                            pvarIndexMirror(O) = indexArray(l)
                            l = l + 1
                            If l > lngMid Then
                                For r = r To plngRight
                                    O = O + 1
                                    pvarMirror(O) = dataArray(r)
                                    pvarIndexMirror(O) = indexArray(r)
                                Next
                                Exit Do
                            End If
                        Else
                            If CLng(dataArray(r)) < CLng(dataArray(l)) Then
                                pvarMirror(O) = dataArray(r)
                                pvarIndexMirror(O) = indexArray(r)
                                r = r + 1
                                If r > plngRight Then
                                    For l = l To lngMid
                                        O = O + 1
                                        pvarMirror(O) = dataArray(l)
                                        pvarIndexMirror(O) = indexArray(l)
                                    Next
                                    Exit Do
                                End If
                            Else
                                pvarMirror(O) = dataArray(l)
                                pvarIndexMirror(O) = indexArray(l)
                                l = l + 1
                                If l > lngMid Then
                                    For r = r To plngRight
                                        O = O + 1
                                        pvarMirror(O) = dataArray(r)
                                        pvarIndexMirror(O) = indexArray(r)
                                    Next
                                    Exit Do
                                End If
                            End If
                        End If
                    Case cSortTypeDouble
                        If Not (IsNumeric(dataArray(r)) And IsNumeric(dataArray(l))) Then
                            pvarMirror(O) = dataArray(l)
                            pvarIndexMirror(O) = indexArray(l)
                            l = l + 1
                            If l > lngMid Then
                                For r = r To plngRight
                                    O = O + 1
                                    pvarMirror(O) = dataArray(r)
                                    pvarIndexMirror(O) = indexArray(r)
                                Next
                                Exit Do
                            End If
                        Else
                            If BL_String2Double(CStr(dataArray(r))) < BL_String2Double(CStr(dataArray(l))) Then
                                pvarMirror(O) = dataArray(r)
                                pvarIndexMirror(O) = indexArray(r)
                                r = r + 1
                                If r > plngRight Then
                                    For l = l To lngMid
                                        O = O + 1
                                        pvarMirror(O) = dataArray(l)
                                        pvarIndexMirror(O) = indexArray(l)
                                    Next
                                    Exit Do
                                End If
                            Else
                                pvarMirror(O) = dataArray(l)
                                pvarIndexMirror(O) = indexArray(l)
                                l = l + 1
                                If l > lngMid Then
                                    For r = r To plngRight
                                        O = O + 1
                                        pvarMirror(O) = dataArray(r)
                                        pvarIndexMirror(O) = indexArray(r)
                                    Next
                                    Exit Do
                                End If
                            End If
                        End If
                    Case Else
                        SORT_MergeSort1 = -1
                        Exit Function
                End Select
                
                O = O + 1
            Loop
            For O = plngLeft To plngRight
                dataArray(O) = pvarMirror(O)
                indexArray(O) = pvarIndexMirror(O)
            Next
    End Select
    
    SORT_MergeSort1 = 1
    Exit Function
TrataErro:
    SORT_MergeSort1 = -1
    BG_LogFile_Erros "SORT_MergeSort1: " & Err.Number & " - " & Err.Description
    Exit Function
    Resume Next
End Function



VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEmissaoRecibos 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   5130
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8775
   Icon            =   "FormEmissaoRecibos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   8775
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAlterarUtente 
      Height          =   615
      Left            =   960
      Picture         =   "FormEmissaoRecibos.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   26
      ToolTipText     =   "Alterar Utente do Recibo"
      Top             =   4440
      Width           =   615
   End
   Begin VB.ComboBox CbModoPag 
      Height          =   315
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   25
      Top             =   3480
      Width           =   2055
   End
   Begin VB.TextBox EcAdiantamento 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   7320
      TabIndex        =   21
      Top             =   3480
      Width           =   1095
   End
   Begin VB.CommandButton BtAdiantamento 
      Height          =   615
      Left            =   3360
      Picture         =   "FormEmissaoRecibos.frx":0776
      Style           =   1  'Graphical
      TabIndex        =   20
      ToolTipText     =   "Emitir Adiantamento"
      Top             =   4440
      Width           =   735
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Left            =   240
      TabIndex        =   12
      Top             =   6720
      Visible         =   0   'False
      Width           =   2895
      Begin VB.TextBox EcDesconto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   14
         Top             =   120
         Width           =   615
      End
      Begin VB.TextBox EcDesconto2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   13
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Desconto:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Image1 
         Height          =   240
         Index           =   1
         Left            =   2520
         Picture         =   "FormEmissaoRecibos.frx":1440
         Top             =   480
         Width           =   240
      End
      Begin VB.Image Image2 
         Height          =   240
         Index           =   1
         Left            =   2520
         Picture         =   "FormEmissaoRecibos.frx":17CA
         Top             =   120
         Width           =   240
      End
   End
   Begin VB.TextBox EcTotalPagarPerc 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5280
      TabIndex        =   11
      Top             =   7320
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcTotalPagar 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5280
      TabIndex        =   10
      Top             =   6960
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcNumCopias 
      Height          =   285
      Left            =   4200
      TabIndex        =   8
      Top             =   4650
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CheckBox CkPausaImpressao 
      Caption         =   "Pausa impress�o de factura recibos(10s)"
      Height          =   255
      Left            =   5520
      TabIndex        =   7
      Top             =   4680
      Width           =   3255
   End
   Begin VB.CommandButton BtDocCaixa 
      Height          =   615
      Left            =   2520
      Picture         =   "FormEmissaoRecibos.frx":1B54
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Documento Caixa"
      Top             =   4440
      Width           =   735
   End
   Begin VB.TextBox EcAux 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   5160
      Visible         =   0   'False
      Width           =   1300
   End
   Begin VB.CommandButton BtValidar 
      Height          =   615
      Left            =   1680
      Picture         =   "FormEmissaoRecibos.frx":281E
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Gerar Factura Recibo(s)"
      Top             =   4440
      Width           =   735
   End
   Begin VB.CommandButton BtFechar 
      Height          =   615
      Left            =   120
      Picture         =   "FormEmissaoRecibos.frx":34E8
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   4440
      Width           =   735
   End
   Begin MSFlexGridLib.MSFlexGrid FgEntidades 
      Height          =   1815
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   3201
      _Version        =   393216
      Rows            =   1
      FixedRows       =   0
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin VB.Label LbUtente 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   600
      Width           =   4335
   End
   Begin VB.Label Label3 
      Caption         =   "Modo Pagamento"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   24
      Top             =   3480
      Width           =   1335
   End
   Begin VB.Label LbAdiantamento 
      Alignment       =   1  'Right Justify
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   4200
      TabIndex        =   23
      Top             =   2880
      Width           =   4335
   End
   Begin VB.Label LbAdianta 
      Caption         =   "Adiantamento"
      Height          =   255
      Left            =   6120
      TabIndex        =   22
      Top             =   3480
      Width           =   1215
   End
   Begin VB.Label Label1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Valor a Pagar:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   3720
      TabIndex        =   19
      Top             =   6960
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Total em D�vida:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   3510
      TabIndex        =   18
      Top             =   6120
      Width           =   1575
   End
   Begin VB.Label Label1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Taxa a Pagar:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   3720
      TabIndex        =   17
      Top             =   7320
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image1 
      Height          =   240
      Index           =   0
      Left            =   6000
      Picture         =   "FormEmissaoRecibos.frx":41B2
      Top             =   6960
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Image1 
      Height          =   240
      Index           =   3
      Left            =   6000
      Picture         =   "FormEmissaoRecibos.frx":453C
      Top             =   6120
      Width           =   240
   End
   Begin VB.Image Image2 
      Height          =   240
      Index           =   0
      Left            =   6000
      Picture         =   "FormEmissaoRecibos.frx":48C6
      Top             =   7320
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label LbTotalDebito 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      Caption         =   "0.0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5040
      TabIndex        =   16
      Top             =   6120
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "C�pias"
      Height          =   255
      Left            =   4800
      TabIndex        =   9
      Top             =   4680
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Line Line1 
      X1              =   8520
      X2              =   2040
      Y1              =   4080
      Y2              =   4080
   End
   Begin VB.Label LbTotal2 
      Alignment       =   1  'Right Justify
      Caption         =   "LbTotal2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5400
      TabIndex        =   5
      Top             =   4200
      Width           =   3015
   End
   Begin VB.Label LbTotal 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   495
      Left            =   1680
      TabIndex        =   4
      Top             =   0
      Width           =   5415
   End
End
Attribute VB_Name = "FormEmissaoRecibos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim rs As ADODB.recordset
Const vermelho = &HC0C0FF
Const Verde = &HC0FFC0
Const Amarelo = &HC0FFFF
Const azul = &HFFDDDD

Public linhaRegistosR As Long
Public linha_ant As Integer
Public coluna_ant As Integer

Dim flg_desconto As Boolean
Dim flg_desconto2 As Boolean
Dim flg_TotalPagar As Boolean
Dim flg_TotalPagarPerc As Boolean

Public Form As String
Dim formActivo As Form


Const lDocCaixa = 1
Const lRecibo = 2

Const lColCodEfr = 0
Const lcolDescrEfr = 1
Const lColAnalise = 2
Const lColDesconto = 2
Const lColVD = 3
Const lColNumAna = 4
Const lColPreco = 5

Private Type Recibo
     NumDoc As String
     SerieDoc As String
     codEntidade As String
     DescrEntidade As String
     ValorPagar As Double
     Caucao As Double
     UserCri As String
     DtCri As String
     HrCri As String
     UserEmi As String
     DtEmi As String
     HrEmi As String
     DtPagamento As String
     HrPagamento As String
     flg_impressao As Integer
     Estado As String
     NumVendaDinheiro As String
     Desconto As Double
     NumAnalises As Integer
     flg_DocCaixa As Integer
     ValorOriginal As Double
     n_ord As Integer
     codEmpresa As String
     flg_FdsFixo As Integer
     NumCopias As Integer
     Certificado As String
     
     linhaRecibosR As Integer
End Type
Dim recibosE() As Recibo
Dim totalRecibosE As Long
Public flg_adiantamentos As Boolean

Private Sub BtAdiantamento_Click()
    Dim resAdiantamento As Boolean
    If flg_adiantamentos = True Then
        BG_Mensagem mediMsgBox, "N�o � poss�vel gerar adiantamentos, j� existem outros adiantamentos emitidos.", vbOKOnly + vbCritical, "RECIBO_GeraRecibo"
        Exit Sub
    End If
    If BL_HandleNull(EcAdiantamento, 0) = 0 Then
        BG_Mensagem mediMsgBox, "N�o � poss�vel gerar adiantamentos com valor 0.", vbOKOnly + vbCritical, "RECIBO_GeraRecibo"
        Exit Sub
    ElseIf EcAdiantamento > EcTotalPagar Then
        gMsgTitulo = "Adiantamento"
        gMsgMsg = "Deseja emitir um adiantamento com valor superior ao total em d�vida?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp <> vbYes Then
            Exit Sub
        End If
    End If
    gMsgTitulo = "Adiantamento"
    gMsgMsg = "Deseja emitir um adiantamento no valor de " & EcAdiantamento & "� ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        resAdiantamento = RECIBO_GeraAdiantamento(FormGestaoRequisicaoPrivado.EcNumReq, EcAdiantamento, FormGestaoRequisicaoPrivado.RegistosAD)
    End If
    
    Unload Me
End Sub

Private Sub BtAlterarUtente_Click()
    FormIdUtentePesq.Show
    FormEmissaoRecibos.Enabled = False
    Set gFormActivo = FormIdUtentePesq
End Sub

Private Sub BtFechar_Click()
    If gPassaRecFactus <> mediSim Then
        FormGestaoRequisicaoPrivado.PreencheFgRecibos
    ElseIf gPassaRecFactus = mediSim Then
        FormGestaoRequisicaoPrivado.AtualizaFgRecibosNew
    End If
    Unload Me
End Sub

Private Sub BtValidar_Click()
    Dim i As Long
    Dim j As Integer
    Dim res As Boolean
    On Error GoTo TrataErro
    
    
    EcAdiantamento = "0"
    If gPassaRecFactus <> mediSim Then
        For i = 1 To totalRecibosE
            If recibosE(i).codEntidade <> "" Then
                If CDbl(FgEntidades.TextMatrix(i, lColPreco)) = 0 Then
                    BG_Mensagem mediMsgBox, "Recibo para " & FgEntidades.TextMatrix(i, lcolDescrEfr) & " n�o pode ser emitido com valor a zero.", vbOKOnly + vbCritical, "RECIBO_GeraRecibo"
                Else
                        If BL_HandleNull(FormGestaoRequisicaoPrivado.RegistosR(CLng(recibosE(i).linhaRecibosR)).NumDoc, "0") = "0" Then
                            res = RECIBO_GeraReciboDetalhe(FormGestaoRequisicaoPrivado.EcNumReq, CLng(recibosE(i).linhaRecibosR), lRecibo, FormGestaoRequisicaoPrivado.RegistosA, _
                                              FormGestaoRequisicaoPrivado.RegistosR, FormGestaoRequisicaoPrivado.RegistosRM, flg_adiantamentos)
                            If res = False Then
                                BG_Mensagem mediMsgBox, "Erro ao gerar recibo.", vbOKOnly + vbCritical, "RECIBO_GeraRecibo"
                                'FormGestaoRequisicaoPrivado.FuncaoLimpar
                            End If
                
                            If i < (FgEntidades.row - 1) And CkPausaImpressao.value = vbChecked Then
                                Sleep (10000)
                            End If
                        End If
                End If
            End If
        Next
        FormGestaoRequisicaoPrivado.PreencheFgRecibos
    Else
           
         'BRUNODSANTOS LJMANSO-303
         Dim strEFR As String
         Dim StrAna As String
         'NELOSNPSILVA LJMANSO-303
        'For j = 1 To FgEntidades.rows - 2
        '    If FgEntidades.TextMatrix(j, lColPreco) <> "0" Then
        '        strEFR = FgEntidades.TextMatrix(j, lColCodEfr) & "," & strEFR
        '    End If
        'Next j
        
        'strEFR = Mid(strEFR, 1, Len(strEFR) - 1)
        
        'If VerificaDuplicacaoRubricas(strEFR, StrAna) Then
        '
        If VerificaDuplicacaoRubricas(StrAna) Then
            If (BG_Mensagem(mediMsgBox, "Foram encontradas rubricas repetidas para a requisi��o em causa para a(s) an�lise(s): " & vbNewLine & StrAna & vbNewLine & "Continuar?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
                Exit Sub
            End If
        End If
        '
        
        BG_BeginTransaction
        For i = 1 To fa_movi_resp_tot
            For j = 1 To FgEntidades.rows - 2
                If fa_movi_resp(i).cod_efr = FgEntidades.TextMatrix(j, lColCodEfr) And FgEntidades.TextMatrix(j, lColPreco) <> "0" Then
                    If FACTUS_FaturaDoente(CLng(i), CbModoPag.ItemData(CbModoPag.ListIndex)) = False Then
                        GoTo TrataErro
                    End If
                End If
            Next j
        Next i
    
        AtualizaDadosDocumentosDoente
        BG_CommitTransaction
        FormGestaoRequisicaoPrivado.PreencheDadosDocumentos
        FACTUS_PreencheRequis FormGestaoRequisicaoPrivado.EcNumReq.text, ""
        FormGestaoRequisicaoPrivado.AtualizaFgRecibosNew
        
        For i = 1 To totalRecibosNew
            For j = 1 To FgEntidades.rows - 2
                If RecibosNew(i).cod_efr = FgEntidades.TextMatrix(j, lColCodEfr) And FgEntidades.TextMatrix(j, lColPreco) <> "0" And RecibosNew(i).valor = FgEntidades.TextMatrix(j, lColPreco) And RecibosNew(i).flg_estado = "R" Then
                    'FGONCALVES_UALIA
                    FACTUS_ImprimeRecibo "", FormGestaoRequisicaoPrivado.EcNumReq.text, RecibosNew(i).n_Fac_doe, RecibosNew(i).serie_fac_doe, RecibosNew(i).cod_efr, FormGestaoRequisicaoPrivado.EcPrinterRecibo, mediComboValorNull, "R"
                End If
            Next j
        Next i
    End If
    
    Unload Me
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro ao gerar fatura: " & Err.Number & " - " & Err.Description, Me.Name, "BtValidar_Click", True
    Exit Sub
    Resume Next
End Sub
Private Sub BtDocCaixa_Click()
    Dim i As Long
    Dim res As Boolean
    
    For i = 1 To FgEntidades.rows - 1
        If FgEntidades.TextMatrix(i, lColCodEfr) <> "" Then
            res = RECIBO_GeraReciboDetalhe(FormGestaoRequisicaoPrivado.EcNumReq, CLng(recibosE(i).linhaRecibosR), lDocCaixa, FormGestaoRequisicaoPrivado.RegistosA, _
                              FormGestaoRequisicaoPrivado.RegistosR, FormGestaoRequisicaoPrivado.RegistosRM, flg_adiantamentos)
            If res = False Then
                BG_Mensagem mediMsgBox, "Erro ao gerar recibo.", vbOKOnly + vbCritical, "RECIBO_GeraRecibo"
            End If
        End If
    Next
    
    Unload Me
End Sub




Private Sub CbModoPag_Click()
    Dim i As Integer
    For i = 1 To totalRecibosE
        If CbModoPag.ListIndex > mediComboValorNull Then
            'edgar.parada LJMANSO-253 09.04.2018 descomentada a linha seguinte
            If gPassaRecFactus <> mediSim Then
                FormGestaoRequisicaoPrivado.RegistosR(recibosE(i).linhaRecibosR).CodFormaPag = CbModoPag.ItemData(CbModoPag.ListIndex)
            End If
        End If
    Next
End Sub

Private Sub EcAdiantamento_Validate(Cancel As Boolean)
    If EcAdiantamento = "" Then Exit Sub
    If Not IsNumeric(EcAdiantamento) Then
        BG_Mensagem mediMsgBox, "Valor tem que ser num�rico.", vbExclamation + vbOKOnly, "Adiantamento"
        EcAdiantamento = "0"
        EcAdiantamento.SetFocus
        Sendkeys ("{HOME}+{END}")

    End If
    
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 13 Then
    EcAux_LostFocus
End If

End Sub

Private Sub EcAux_LostFocus()
    EcAux = Replace(EcAux, ".", ",")
    FgEntidades.TextMatrix(linha_ant, coluna_ant) = EcAux
    If coluna_ant = lColDesconto Then
        'Altera��o do Desconto %
        'Valor a pagar
        If EcAux = "" Then EcAux = "0"
        FgEntidades.TextMatrix(linha_ant, lColPreco) = Round(CDbl(recibosE(linha_ant).ValorOriginal) - EcAux * CDbl(recibosE(linha_ant).ValorOriginal) / 100, 2)
        FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).Desconto = FgEntidades.TextMatrix(linha_ant, lColDesconto)
        FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).ValorPagar = FgEntidades.TextMatrix(linha_ant, lColPreco)
        recibosE(linha_ant).Desconto = FgEntidades.TextMatrix(linha_ant, lColDesconto)
        recibosE(linha_ant).ValorPagar = FgEntidades.TextMatrix(linha_ant, lColPreco)
    ElseIf coluna_ant = lColVD Then
        FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).NumVendaDinheiro = EcAux

    ElseIf coluna_ant = lColPreco Then
        'Altera��o do valor total a pagar
        'Desconto
        If recibosE(linha_ant).ValorOriginal <> 0 Then
            FgEntidades.TextMatrix(linha_ant, lColDesconto) = 100 - Round((CDbl(EcAux) * 100) / recibosE(linha_ant).ValorOriginal, 3)
            FgEntidades.TextMatrix(linha_ant, lColDesconto) = Round(FgEntidades.TextMatrix(linha_ant, lColDesconto), 5)
            FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).Desconto = FgEntidades.TextMatrix(linha_ant, lColDesconto)
            FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).ValorPagar = FgEntidades.TextMatrix(linha_ant, lColPreco)
            recibosE(linha_ant).Desconto = FgEntidades.TextMatrix(linha_ant, lColDesconto)
            recibosE(linha_ant).ValorPagar = FgEntidades.TextMatrix(linha_ant, lColPreco)
        End If
    End If
    If recibosE(linha_ant).ValorPagar > 0 Then
        FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).Estado = gEstadoReciboNaoEmitido
    ElseIf recibosE(linha_ant).ValorPagar = 0 Then
        FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).Estado = gEstadoReciboNulo
    End If
    If FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).ValorPagar > 0 Then
        FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).Estado = gEstadoReciboNaoEmitido
    Else
        FormGestaoRequisicaoPrivado.RegistosR(recibosE(linha_ant).linhaRecibosR).Estado = gEstadoReciboNulo
    End If
    
    EcAux.Visible = False
    ReCalculaTotal
End Sub




Private Sub EcDesconto_Change()
    If flg_desconto = True Then
        flg_desconto2 = False
        flg_TotalPagarPerc = False
        flg_TotalPagar = False
        If EcDesconto <> "" Then
            If IsNumeric(EcDesconto) Then
                EcTotalPagar = Round(CDbl(LbTotalDebito) - ((EcDesconto * CDbl(LbTotalDebito)) / 100), 2)
                EcDesconto2 = Round(CDbl(((EcDesconto * LbTotalDebito) / 100)), 2)
                EcTotalPagarPerc = CDbl(100 - EcDesconto)
            End If
        Else
            EcDesconto2 = ""
            EcTotalPagar = LbTotalDebito
        End If
        flg_desconto2 = True
        flg_TotalPagarPerc = True
        flg_TotalPagar = True
    End If
End Sub
Private Sub Ectotalpagar_Change()
    If flg_TotalPagar = True Then
        flg_TotalPagarPerc = False
        flg_desconto = False
        flg_desconto2 = False
        If EcTotalPagar <> "" Then
            If IsNumeric(EcTotalPagar) And LbTotalDebito <> 0 Then
                EcTotalPagarPerc = ((EcTotalPagar * 100) / LbTotalDebito)
                EcDesconto2 = CDbl(LbTotalDebito) - CDbl(EcTotalPagar)
                EcDesconto = Round(CDbl(100 - EcTotalPagarPerc), 0)
            End If
        Else
        End If
        flg_TotalPagarPerc = True
        flg_desconto = True
        flg_desconto2 = True
    End If
    LbTotal = "Total em D�vida: " & EcTotalPagar & " �"
    LbTotal2 = LbTotal
End Sub

Private Sub EcTotalPagar_LostFocus()
    If EcTotalPagar = "" Then
        EcTotalPagar = LbTotalDebito
    End If
End Sub

Private Sub Ectotalpagarperc_Change()
    If flg_TotalPagarPerc = True Then
        flg_TotalPagar = False
        flg_desconto = False
        flg_desconto2 = False
        If EcTotalPagarPerc <> "" Then
            If IsNumeric(EcTotalPagarPerc) Then
                EcTotalPagar = ((EcTotalPagarPerc * LbTotalDebito) / 100)
                EcDesconto = CDbl(100 - EcTotalPagarPerc)
                EcDesconto2 = CDbl(LbTotalDebito - EcTotalPagar)
            End If
        End If
        flg_TotalPagar = True
        flg_desconto = True
        flg_desconto2 = True
    End If
End Sub
Private Sub EcDesconto2_Change()
    EcDesconto2 = Replace(EcDesconto2, ".", ",")
    'SendKeys ("{END}")
    If flg_desconto2 = True Then
        flg_desconto = False
        flg_TotalPagarPerc = False
        flg_TotalPagar = False
        If EcDesconto2 <> "" Then
            If IsNumeric(EcDesconto2) Then
                EcTotalPagar = CDbl(LbTotalDebito) - CDbl(EcDesconto2)
                EcDesconto = CDbl(Round(((EcDesconto2 * 100) / LbTotalDebito), 2))
                EcTotalPagarPerc = CDbl(100 - EcDesconto)
            End If
        Else
            EcTotalPagar = LbTotalDebito
            EcDesconto = ""
        End If
        flg_desconto = True
        flg_TotalPagarPerc = True
        flg_TotalPagar = True
    End If
End Sub


Private Sub EcTotalPagarPerc_LostFocus()
    EcTotalPagar = LbTotalDebito
End Sub

Private Sub FgEntidades_dblClick()
    Dim i As Integer
    Dim StrAna As String
    If gPassaRecFactus = mediSim Then
    
        'BRUNODSANTOS LJMANSO-303
        'If VerificaDuplicacaoRubricas(FgEntidades.TextMatrix(FgEntidades.row, lColCodEfr), StrAna) Then
        If VerificaDuplicacaoRubricas(StrAna) Then
            If (BG_Mensagem(mediMsgBox, "Foram encontradas rubricas repetidas para a requisi��o em causa para a(s) an�lise(s): " & vbNewLine & StrAna & vbNewLine & "Continuar?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
                Exit Sub
            End If

        End If
        '
        BG_BeginTransaction
        For i = 1 To fa_movi_resp_tot
            If fa_movi_resp(i).cod_efr = FgEntidades.TextMatrix(FgEntidades.row, lColCodEfr) And FgEntidades.TextMatrix(FgEntidades.row, lColPreco) <> "0" Then
                If FACTUS_FaturaDoente(CLng(i), CbModoPag.ItemData(CbModoPag.ListIndex)) = False Then
                    
                End If
            End If
        Next i
        AtualizaDadosDocumentosDoente
        BG_CommitTransaction
        FormGestaoRequisicaoPrivado.PreencheDadosDocumentos
        FACTUS_PreencheRequis FormGestaoRequisicaoPrivado.EcNumReq.text, ""
        FormGestaoRequisicaoPrivado.AtualizaFgRecibosNew
    
        Unload Me
    Else
    
        linha_ant = FgEntidades.row
        linhaRegistosR = recibosE(linha_ant).linhaRecibosR
        coluna_ant = FgEntidades.Col
        If (FgEntidades.Col = lColDesconto Or FgEntidades.Col = lColVD Or FgEntidades.Col = lColPreco) And FgEntidades.TextMatrix(linha_ant, lColCodEfr) <> "" And gPermAltPreco = 1 Then
            EcAux.top = FgEntidades.top + FgEntidades.CellTop
            EcAux.left = FgEntidades.left + FgEntidades.CellLeft
            EcAux.Width = FgEntidades.CellWidth
            EcAux = FgEntidades.text
            EcAux.Visible = True
            EcAux.SelStart = Len(EcAux.text)
            EcAux.SetFocus
        End If
    End If
End Sub

Private Sub FgEntidades_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Integer
    If gPassaRecFactus <> mediSim Then
        If Button = 2 Then
            linha_ant = FgEntidades.row
            linhaRegistosR = recibosE(linha_ant).linhaRecibosR
            coluna_ant = FgEntidades.Col
            If FgEntidades.Col = lcolDescrEfr And FgEntidades.TextMatrix(linha_ant, lColCodEfr) <> "" Then
                MDIFormInicio.PopupMenu MDIFormInicio.HIDE_REC
                Unload Me
            End If
        End If
    End If
End Sub

Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()

    EventoLoad
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    If Form = "FormGestaoRequisicaoPrivado" Then
        Set formActivo = FormGestaoRequisicaoPrivado
    End If
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    gF_EMISSAO_RECIBOS = 1
    DefineTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    FuncaoProcurar
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = formActivo
    FormGestaoRequisicaoPrivado.Enabled = True
    BL_ToolbarEstadoN 2
    Set FormEmissaoRecibos = Nothing
    gF_EMISSAO_RECIBOS = 0
End Sub

Sub Inicializacoes()
        
    Dim p As ADODB.Parameter
    
    Set CampoDeFocus = BtValidar
    
    Me.caption = " Emiss�o de Recibos"
    
    Me.left = 20
    Me.top = 20
    Me.Width = 8865
    Me.Height = 5505
    flg_desconto = True
    flg_desconto2 = True
    flg_TotalPagar = True
    flg_TotalPagarPerc = True
    CbModoPag.ListIndex = mediComboValorNull
End Sub

Sub PreencheValoresDefeito()
    If gPassaRecFactus = mediSim Then
        BtAdiantamento.Visible = False
        BtDocCaixa.Visible = False
        LbAdiantamento.Visible = False
        EcAdiantamento.Visible = False
    End If
    BG_PreencheComboBD_ADO "SELECT cod_forma_pag, descr_forma_pag FROM sl_forma_pag WHERE flg_invisivel = 0 ", "cod_forma_pag", "descr_forma_pag", CbModoPag, mediAscComboDesignacao
    'BG_PreencheComboBD_ADO "sl_forma_pag", "cod_forma_pag", "descr_forma_pag", CbModoPag
    EcNumCopias = BL_HandleNull(gNumCopiasRecibo)
    If EcNumCopias = "-1" Then
        EcNumCopias = "0"
    End If
    EcAdiantamento = "0"
End Sub

Sub FuncaoLimpar()
    totalRecibosE = 0
    ReDim recibosE(0)
    
End Sub

Sub FuncaoImprimir()
    
End Sub




Private Sub DefineTipoCampos()
    With FgEntidades
        .rows = 2
        .FixedRows = 1
        .Cols = 6
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridFlat
        .GridColor = &HC0C0C0
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(lColCodEfr) = 700
        .TextMatrix(0, lColCodEfr) = "C�digo"
        
        .ColWidth(lcolDescrEfr) = 3500
        .TextMatrix(0, lcolDescrEfr) = "Entidade"
        
        .ColWidth(lColDesconto) = 1000
        .TextMatrix(0, lColDesconto) = "Desconto%"
        
        .ColWidth(lColVD) = 1300
        .TextMatrix(0, lColVD) = "VendaDinh."
        
        .ColWidth(lColNumAna) = 700
        .TextMatrix(0, lColNumAna) = "N� Ana."
        
        .ColWidth(lColPreco) = 800
        .TextMatrix(0, lColPreco) = "Pre�o"
        

        .Col = 1
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
    
    If gDocCaixa = mediSim Then
        BtDocCaixa.Visible = True
    Else
        BtDocCaixa.Visible = False
    End If
    LbAdiantamento.Visible = False
    FuncaoLimpar
End Sub
Private Sub FuncaoProcurar()
    If gPassaRecFactus <> mediSim Then
        FuncaoProcurar_old
    Else
        FuncaoProcurar_new
    End If
End Sub

Private Sub FuncaoProcurar_old()
    Dim i As Integer
    Dim t As Integer
    Dim j As Integer
    Dim TotalEnt As Double
    Dim totalDebito As Double
    Dim totalAdiantado As Double
    
    totalDebito = 0
    t = FormGestaoRequisicaoPrivado.RegistosR.Count - 1
    totalRecibosE = 0
    LimpaFgEntidades
    ReDim recibosE(0)
    
    For i = 1 To t
        If FormGestaoRequisicaoPrivado.RegistosR(i).codEntidade <> "" And (FormGestaoRequisicaoPrivado.RegistosR(i).Estado = gEstadoReciboNaoEmitido Or _
                FormGestaoRequisicaoPrivado.RegistosR(i).Estado = gEstadoReciboPerdido) And BL_HandleNull(FormGestaoRequisicaoPrivado.RegistosR(i).NumDoc) = "0" Then
            totalRecibosE = totalRecibosE + 1
            ReDim Preserve recibosE(totalRecibosE)
            
            recibosE(totalRecibosE).SerieDoc = FormGestaoRequisicaoPrivado.RegistosR(i).SerieDoc
            recibosE(totalRecibosE).NumDoc = FormGestaoRequisicaoPrivado.RegistosR(i).NumDoc
            recibosE(totalRecibosE).codEntidade = FormGestaoRequisicaoPrivado.RegistosR(i).codEntidade
            recibosE(totalRecibosE).DescrEntidade = FormGestaoRequisicaoPrivado.RegistosR(i).DescrEntidade
            recibosE(totalRecibosE).ValorOriginal = FormGestaoRequisicaoPrivado.RegistosR(i).ValorOriginal
            recibosE(totalRecibosE).Desconto = FormGestaoRequisicaoPrivado.RegistosR(i).Desconto
            recibosE(totalRecibosE).ValorPagar = Round(CDbl((recibosE(totalRecibosE).ValorOriginal * (100 - recibosE(totalRecibosE).Desconto)) / 100), 2)
            recibosE(totalRecibosE).NumVendaDinheiro = FormGestaoRequisicaoPrivado.RegistosR(i).NumVendaDinheiro
            recibosE(totalRecibosE).NumAnalises = FormGestaoRequisicaoPrivado.RegistosR(i).NumAnalises
            If FormGestaoRequisicaoPrivado.RegistosR(i).SeqUtente <> -1 Then
                lbUtente = "Emiss�o de Recibo para Outro Utente"
            Else
                lbUtente = ""
            End If
            recibosE(totalRecibosE).Certificado = ""
            recibosE(totalRecibosE).linhaRecibosR = i
            
            FgEntidades.TextMatrix(totalRecibosE, lColCodEfr) = recibosE(totalRecibosE).codEntidade
            FgEntidades.TextMatrix(totalRecibosE, lcolDescrEfr) = recibosE(totalRecibosE).DescrEntidade
            FgEntidades.TextMatrix(totalRecibosE, lColDesconto) = recibosE(totalRecibosE).Desconto
            FgEntidades.TextMatrix(totalRecibosE, lColNumAna) = BG_CvDecimalParaCalculo(recibosE(totalRecibosE).NumAnalises)
            FgEntidades.TextMatrix(totalRecibosE, lColPreco) = BG_CvDecimalParaCalculo(recibosE(totalRecibosE).ValorPagar)
            
            totalDebito = BL_String2Double(BG_CvDecimalParaCalculo(CStr(totalDebito))) + BL_String2Double(BG_CvDecimalParaCalculo(CStr(recibosE(totalRecibosE).ValorPagar)))
            FgEntidades.AddItem ""
        End If
    Next
    LbTotalDebito = BG_CvDecimalParaCalculo(totalDebito)
    EcTotalPagar = BG_CvDecimalParaCalculo(LbTotalDebito)
    totalAdiantado = RECIBO_RetornaTotalAdiantamentos(FormGestaoRequisicaoPrivado.RegistosAD)
    For j = 0 To CbModoPag.ListCount - 1
        If CbModoPag.ItemData(j) = gModoPagDinheiro Then
            CbModoPag.ListIndex = j
            Exit For
        End If
    Next
    If totalAdiantado > 0 Then
        LbAdiantamento.caption = "Total j� adiantado: " & totalAdiantado & " �"
        LbAdiantamento.Visible = True
    Else
        LbAdiantamento.Visible = False
    End If
            
End Sub

Private Sub FuncaoProcurar_new()
    Dim i As Integer
    Dim t As Integer
    Dim j As Integer
    Dim TotalEnt As Double
    Dim totalDebito As Double
    Dim totalAdiantado As Double
    
    totalDebito = 0
    totalRecibosE = 0
    LimpaFgEntidades
    ReDim recibosE(0)
    
    For i = 1 To totalRecibosNew
        RecibosNew(i).iEmiRec = mediComboValorNull
        If RecibosNew(i).cod_efr > 0 And RecibosNew(i).flg_estado = gEstadoFactNaoFaturado And BL_HandleNull(RecibosNew(i).n_Fac_doe) = "-1" Then
            totalRecibosE = totalRecibosE + 1
            ReDim Preserve recibosE(totalRecibosE)
            
            If RecibosNew(i).seq_utente > 0 Then
                lbUtente = "Emiss�o de Recibo para Outro Utente"
            Else
                lbUtente = ""
            End If
            RecibosNew(i).iEmiRec = FgEntidades.rows - 1
            FgEntidades.TextMatrix(FgEntidades.rows - 1, lColCodEfr) = RecibosNew(i).cod_efr
            FgEntidades.TextMatrix(FgEntidades.rows - 1, lcolDescrEfr) = RecibosNew(i).descr_efr
            FgEntidades.TextMatrix(FgEntidades.rows - 1, lColDesconto) = RecibosNew(i).Desconto
            FgEntidades.TextMatrix(FgEntidades.rows - 1, lColNumAna) = RecibosNew(i).num_analises
            FgEntidades.TextMatrix(totalRecibosE, lColPreco) = RecibosNew(i).valor
            
            totalDebito = totalDebito + RecibosNew(i).valor
            FgEntidades.AddItem ""
        End If
    Next
    LbTotalDebito = BG_CvDecimalParaCalculo(totalDebito)
    EcTotalPagar = BG_CvDecimalParaCalculo(LbTotalDebito)
    'totalAdiantado = RECIBO_RetornaTotalAdiantamentos(FormGestaoRequisicaoPrivado.RegistosAD)
    For j = 0 To CbModoPag.ListCount - 1
        If CbModoPag.ItemData(j) = gModoPagDinheiro Then
            CbModoPag.ListIndex = j
            Exit For
        End If
    Next
    If totalAdiantado > 0 Then
        LbAdiantamento.caption = "Total j� adiantado: " & totalAdiantado & " �"
        LbAdiantamento.Visible = True
    Else
        LbAdiantamento.Visible = False
    End If
            
End Sub


Private Sub LimpaFgEntidades()
    Dim i As Integer
    Dim j As Integer
    For i = 1 To FgEntidades.rows - 1
        For j = 1 To FgEntidades.Cols - 1
            FgEntidades.TextMatrix(i, j) = ""
        Next
    Next

End Sub

Sub ReCalculaTotal()
    Dim i As Integer
    Dim totalDebito As Double
    'soliveira Recalcula o total em funcao de alteracoes efectuadas nas linhas da flexgrid
    '(altera��o desconto ou valor total a pagar)
    totalDebito = 0
    For i = 1 To FgEntidades.rows - 1
        totalDebito = BL_String2Double(BG_CvDecimalParaCalculo(CStr(totalDebito))) + BL_String2Double(BG_CvDecimalParaCalculo(CStr(FgEntidades.TextMatrix(i, lColPreco))))
    Next i
    LbTotalDebito = BG_CvDecimalParaCalculo(totalDebito)
    EcTotalPagar = BG_CvDecimalParaCalculo(LbTotalDebito)
End Sub

Public Sub AlteraUtenteRecibo_new(seq_utente As Long, linha As Long, nome_ute As String)
    Dim i As Integer
    lbUtente = ""
    If (BG_Mensagem(mediMsgBox, "Deseja emitir o recibo seleccionado para o Utente: " & nome_ute & "?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
        For i = 1 To totalRecibosNew
            If RecibosNew(i).iEmiRec > mediComboValorNull Then
                RecibosNew(i).seq_utente = seq_utente
            End If
        Next i
        lbUtente = " Emiss�o de Recibos para " & nome_ute
    Else
        For i = 1 To totalRecibosNew
        
            RecibosNew(i).seq_utente = mediComboValorNull
        Next
        lbUtente = " Emiss�o de Recibos para " & nome_ute
    End If
End Sub

Public Sub AlteraUtenteRecibo_OLD(seq_utente As Long, linha As Long, nome_ute As String)
    Dim i As Integer
    lbUtente = ""
    If (BG_Mensagem(mediMsgBox, "Deseja emitir o recibo seleccionado para o Utente: " & nome_ute & "?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
        For i = 1 To totalRecibosE
            FormGestaoRequisicaoPrivado.RegistosR(recibosE(i).linhaRecibosR).SeqUtente = seq_utente
        Next
        lbUtente = " Emiss�o de Recibos para " & nome_ute
    Else
        For i = 1 To totalRecibosE
        
            FormGestaoRequisicaoPrivado.RegistosR(recibosE(i).linhaRecibosR).SeqUtente = -1
        Next
        lbUtente = " Emiss�o de Recibos para " & nome_ute
    End If
End Sub
Private Sub AtualizaDadosDocumentosDoente()
    Dim sSql As String
    sSql = "INSERT INTO sl_dados_doc (tipo, serie_doc, n_doc, desconto, dt_pagamento, flg_borla) "
    sSql = sSql & " SELECT tipo, serie_doc,n_doc,0, trunc(sysdate), flg_borla  from slv_documentos_doente WHERE (tipo, serie_doc, n_doc) NOT IN ("
    sSql = sSql & " SELECT tipo, serie_doc, n_doc FROM sl_dados_doc) AND episodio = " & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.EcNumReq.text)
    BG_ExecutaQuery_ADO sSql
    
End Sub

'BRUNODSANTOS LJMANSO-303
'Private Function VerificaDuplicacaoRubricas(ByVal codRubr As String, ByRef StrAna As String) As Boolean
Private Function VerificaDuplicacaoRubricas(ByRef StrAna As String) As Boolean

    Dim sSql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    
    'NELSONPSILVA LJMANSO-303
    sSql = "SELECT COD_RUBR FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.EcNumReq.text) & " AND flg_estado = 1"
    sSql = sSql & " GROUP BY COD_RUBR"
    sSql = sSql & " HAVING COUNT(*) > 1"
    'If codRubr <> "" Then
    '    sSql = sSql & " AND cod_rubr_efr in (" & BL_TrataStringParaBD(codRubr) & ")"
    'End If
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open sSql, gConexao
    
    If rs.RecordCount > 0 Then
        VerificaDuplicacaoRubricas = True
        While Not rs.EOF
           StrAna = StrAna & BL_HandleNull(rs.Fields("cod_rubr").value, "") & vbNewLine
           rs.MoveNext
        Wend
    End If
    
    Set rs = Nothing
    
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "VerificaDuplicacaoRubricas", False
    Exit Function
    
End Function

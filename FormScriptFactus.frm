VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FormScriptFactus 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormScriptFactus"
   ClientHeight    =   3075
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   6960
   Icon            =   "FormScriptFactus.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcFicheiro 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1320
      TabIndex        =   10
      Top             =   1800
      Width           =   1215
   End
   Begin VB.CommandButton BtProcessar 
      Caption         =   "Processa"
      Height          =   375
      Left            =   4560
      TabIndex        =   9
      Top             =   1800
      Width           =   1095
   End
   Begin VB.TextBox EcVersaoFinal 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4560
      TabIndex        =   7
      Top             =   840
      Width           =   1095
   End
   Begin VB.TextBox EcVersaoInicial 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1320
      TabIndex        =   6
      Top             =   840
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   2820
      Width           =   6960
      _ExtentX        =   12277
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Key             =   "a"
         EndProperty
      EndProperty
   End
   Begin VB.TextBox EcDestino 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1320
      TabIndex        =   3
      Top             =   1320
      Width           =   4335
   End
   Begin VB.TextBox EcOrigem 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1320
      TabIndex        =   2
      Top             =   360
      Width           =   4335
   End
   Begin VB.Label Label1 
      Caption         =   "Ficheiro"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   11
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Vers�o final"
      Height          =   255
      Index           =   3
      Left            =   3600
      TabIndex        =   8
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Vers�o Atual"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Destino"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Origem"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   735
   End
End
Attribute VB_Name = "FormScriptFactus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Estado As Integer

Dim iMajor As String
Dim iMinor As String
Dim iPatch As String

Dim fMajor As String
Dim fMinor As String
Dim fPatch As String


Private Sub BtProcessar_Click()
    If EcDestino.text = "" Then
        BG_Mensagem mediMsgBox, "Diretoria Destino vazia", vbExclamation, "Destino"
        Exit Sub
    End If
    If EcOrigem.text = "" Then
        BG_Mensagem mediMsgBox, "Diretoria Origem vazia", vbExclamation, "Origem"
        Exit Sub
    End If
    If EcFicheiro.text = "" Then
        BG_Mensagem mediMsgBox, "Ficheiro vazio", vbExclamation, "Ficheiro"
        Exit Sub
    End If
    
    If Dir(EcDestino.text, vbDirectory) = vbNullString Then
        BG_Mensagem mediMsgBox, "Diretoria Destino inv�lida", vbExclamation, "Destino"
        Exit Sub
    End If
    If Dir(EcOrigem.text, vbDirectory) = vbNullString Then
        BG_Mensagem mediMsgBox, "Diretoria Origem inv�lida", vbExclamation, "Origem"
        Exit Sub
    End If
    BG_LogFile_Erros " INICIO Carregamento Scripts FACTUS "
    BG_LogFile_Erros "====================================="
    ListFolder EcOrigem.text
    BG_LogFile_Erros " Fim Carregamento Scripts FACTUS "
    BG_LogFile_Erros "====================================="
    BG_Mensagem mediMsgBox, "Fim", vbExclamation, "Origem"

End Sub

Private Sub EcVersaoInicial_Validate(Cancel As Boolean)
    If EcVersaoInicial.text <> "" Then
        If Len(EcVersaoInicial) <> 11 Then
            BG_Mensagem mediMsgBox, "A vers�o deve ser da forma AARJJ.NN.PP", vbExclamation, "Vers�o Inicial"
            EcVersaoInicial.text = ""
            iMajor = "11R01"
            iMinor = "01"
            iPatch = "01"
        Else
            iMajor = Mid(EcVersaoInicial.text, 1, 5)
            iMinor = Mid(EcVersaoInicial.text, 7, 2)
            iPatch = Mid(EcVersaoInicial.text, 10, 2)
        End If
    Else
        EcVersaoInicial.text = "11R01.01.01"
        iMajor = "11R01"
        iMinor = "01"
        iPatch = "01"
    End If
    
End Sub
Private Sub EcVersaoFinal_Validate(Cancel As Boolean)
    If EcVersaoFinal.text <> "" Then
        If Len(EcVersaoFinal) <> 11 Then
            BG_Mensagem mediMsgBox, "A vers�o deve ser da forma AARJJ.NN.PP", vbExclamation, "Vers�o Final"
            EcVersaoFinal.text = ""
            fMajor = "99R99"
            fMinor = "99"
            fPatch = "99"
        Else
            fMajor = Mid(EcVersaoFinal.text, 1, 5)
            fMinor = Mid(EcVersaoFinal.text, 7, 2)
            fPatch = Mid(EcVersaoFinal.text, 10, 2)
        End If
    Else
        EcVersaoFinal.text = "99R99.99.99"
        fMajor = "99R99"
        fMinor = "99"
        fPatch = "99"
    End If
    
End Sub
Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Cria��o de Scripts para Upgrade Factus"
    Me.Left = 50
    Me.Top = 50
    Me.Width = 7050
    Me.Height = 3510 ' Normal
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormScriptFactus = Nothing
End Sub



Sub DefTipoCampos()

End Sub

Sub PreencheCampos()
    

End Sub


Sub FuncaoLimpar()
    

End Sub

Sub FuncaoEstadoAnterior()

End Sub



Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub
Private Sub ListFolder(sFolderPath As String)
    Dim FS As New FileSystemObject
    Dim FSfolder As Folder
    Dim subfolder As Folder
    Dim i As Integer
    
    Set FSfolder = FS.GetFolder(sFolderPath)
    For Each subfolder In FSfolder.SubFolders
         Me.StatusBar1.Panels.item("a").text = subfolder.Path
        
        i = i + 1
        If subfolder.Attributes <> 18 Then
            If UCase(subfolder.Name) = "DATA BASE" Then
                ProcessaBaseDados (subfolder.Path)
            Else
                ListFolder subfolder.Path
            End If
        End If
    Next subfolder
    Set FSfolder = Nothing
End Sub

Private Sub ProcessaBaseDados(caminho As String)
    Dim FS As New FileSystemObject
    Dim FSfolder As Folder
    Dim subfolder As Folder
   
    Set FSfolder = FS.GetFolder(caminho)
    BG_LogFile_Erros "A processar diretoria:       " & FSfolder.Path
 
    For Each subfolder In FSfolder.SubFolders
        
        Me.StatusBar1.Panels.item("a").text = subfolder.Path
        
        ProcessaFicheiros (subfolder.Path)
        
    Next subfolder
    Set FSfolder = Nothing

End Sub

Private Sub ProcessaFicheiros(caminho As String)
    Dim FS As New FileSystemObject
    Dim FSfolder As Folder
    Dim subfolder As Folder
    Dim user As String
    Dim pos1 As Integer
    Dim pos2 As Integer
    Set FSfolder = FS.GetFolder(caminho)
    BG_LogFile_Erros "A processar diretoria:       " & FSfolder.Path
    
    Dim fsFile As File
        For Each fsFile In FSfolder.Files
             Me.StatusBar1.Panels.item("a").text = fsFile.Path
            If fsFile.Type = "SQL File" Then
                BG_LogFile_Erros "A processar ficheiro:       " & fsFile.Path
                pos1 = InStr(1, fsFile.Name, "-")
                pos2 = InStr(pos1 + 1, fsFile.Name, "-")
                If pos2 > pos1 Then
                    user = UCase(Trim(Mid(fsFile.Name, pos1 + 1, pos2 - (pos1 + 1))))
                End If
                TrataFicheiro fsFile.Path, EcDestino.text & "\" & user & "_" & EcFicheiro.text
            Else
                BG_LogFile_Erros "Ficheiro Ignorado:       " & fsFile.Path
            End If
            Debug.Print fsFile.Path
        Next
End Sub

Private Sub TrataFicheiro(Origem As String, destino As String)
    Dim FSO As FileSystemObject
    Dim TS As TextStream
    Dim TempS As String
    Dim Final As String
    Set FSO = New FileSystemObject
    Set TS = FSO.OpenTextFile(Origem, ForReading)
    Dim cabecalho As String
    cabecalho = ""
    cabecalho = cabecalho & "/******************************************************************************************************************************" & vbCrLf & vbCrLf
    cabecalho = cabecalho & Origem & vbCrLf & vbCrLf
    cabecalho = cabecalho & "******************************************************************************************************************************\" & vbCrLf
    'Use this for reading everything in one shot
    Final = TS.ReadAll
    'OR use this if you need to process each line
    Do Until TS.AtEndOfStream
        TempS = TS.ReadLine
        Final = Final & TempS & vbCrLf
    Loop
    TS.Close
    
    Set TS = FSO.OpenTextFile(destino, ForAppending, True)
        TS.Write cabecalho
        TS.Write Final
    TS.Close
    Set TS = Nothing
    Set FSO = Nothing
End Sub


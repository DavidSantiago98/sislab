VERSION 5.00
Object = "{2200CD23-1176-101D-85F5-0020AF1EF604}#1.7#0"; "barcod32.ocx"
Begin VB.Form FormIdentificaUtente 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionIdentificaUtente"
   ClientHeight    =   19530
   ClientLeft      =   -45
   ClientTop       =   585
   ClientWidth     =   11970
   ClipControls    =   0   'False
   Icon            =   "FormIdUtente.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   19530
   ScaleWidth      =   11970
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrEnviarSMS 
      Caption         =   "Enviar SMS"
      Height          =   1935
      Left            =   7080
      TabIndex        =   158
      Top             =   10800
      Width           =   4575
      Begin VB.CommandButton BtCancelarSMS 
         Caption         =   "Voltar"
         Height          =   615
         Left            =   3360
         TabIndex        =   161
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton BtEnviarSMS 
         Caption         =   "Enviar SMS"
         Height          =   615
         Left            =   3360
         TabIndex        =   160
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox EcEnviarSMS 
         Height          =   1335
         Left            =   360
         MaxLength       =   160
         MultiLine       =   -1  'True
         TabIndex        =   159
         Text            =   "FormIdUtente.frx":000C
         Top             =   360
         Width           =   2775
      End
      Begin VB.Label LaCaracteresFaltaSMS 
         AutoSize        =   -1  'True
         Caption         =   "160"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   2760
         TabIndex        =   162
         Top             =   1680
         Width           =   375
      End
   End
   Begin VB.TextBox EcDtEr 
      Height          =   285
      Left            =   9000
      Locked          =   -1  'True
      TabIndex        =   153
      Top             =   10440
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcFlgAnexo 
      Height          =   285
      Left            =   1320
      TabIndex        =   149
      Top             =   12120
      Width           =   375
   End
   Begin VB.TextBox EcFlgDiagPrim 
      Height          =   285
      Left            =   1320
      TabIndex        =   147
      Top             =   11760
      Width           =   375
   End
   Begin VB.Frame FrameFeedback 
      Height          =   1815
      Left            =   240
      TabIndex        =   139
      Top             =   9720
      Width           =   6255
      Begin VB.CommandButton BtSairFeedback 
         Height          =   495
         Left            =   4920
         Picture         =   "FormIdUtente.frx":00B5
         Style           =   1  'Graphical
         TabIndex        =   142
         ToolTipText     =   "Sair Sem Gravar"
         Top             =   840
         Width           =   615
      End
      Begin VB.ComboBox CbFeedback 
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   141
         Top             =   840
         Width           =   2655
      End
      Begin VB.Label Label23 
         Caption         =   "Como tomou conhecimento do nosso Laborat�rio?"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   140
         Top             =   360
         Width           =   3735
      End
   End
   Begin VB.TextBox EcPrescricao 
      Height          =   285
      Left            =   9000
      TabIndex        =   116
      Top             =   10080
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcAbrevUte 
      Height          =   285
      Left            =   3840
      TabIndex        =   115
      Top             =   9000
      Width           =   1335
   End
   Begin VB.TextBox EcImprCartao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   104
      Top             =   9000
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   120
      TabIndex        =   54
      Top             =   6480
      Width           =   11775
      Begin VB.CommandButton BtSMS 
         BackColor       =   &H80000004&
         Height          =   495
         Left            =   8760
         MaskColor       =   &H00FFFFFF&
         Picture         =   "FormIdUtente.frx":0D7F
         Style           =   1  'Graphical
         TabIndex        =   155
         ToolTipText     =   "Enviar SMS Avulso"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton BtCriarUtilER 
         BackColor       =   &H80000004&
         Height          =   495
         Left            =   6840
         MaskColor       =   &H00FFFFFF&
         Picture         =   "FormIdUtente.frx":1363
         Style           =   1  'Graphical
         TabIndex        =   152
         ToolTipText     =   "Criar utilizador no eResults"
         Top             =   720
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CheckBox CkInibeSMS 
         Caption         =   "Inibe SMS promocionais"
         Height          =   195
         Left            =   9600
         TabIndex        =   145
         Top             =   1080
         Width           =   2055
      End
      Begin VB.CommandButton BtAnexos 
         Height          =   495
         Left            =   4920
         Picture         =   "FormIdUtente.frx":202D
         Style           =   1  'Graphical
         TabIndex        =   143
         ToolTipText     =   "Anexos Associados ao Utente"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton BtFeedback 
         Height          =   495
         Left            =   3000
         Picture         =   "FormIdUtente.frx":2797
         Style           =   1  'Graphical
         TabIndex        =   137
         ToolTipText     =   "Como ouviu Falar de N�s?"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton BtNotas 
         Enabled         =   0   'False
         Height          =   480
         Left            =   1200
         Picture         =   "FormIdUtente.frx":3461
         Style           =   1  'Graphical
         TabIndex        =   119
         ToolTipText     =   "Notas"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton BtNotasVRM 
         Enabled         =   0   'False
         Height          =   480
         Left            =   1200
         Picture         =   "FormIdUtente.frx":3BCB
         Style           =   1  'Graphical
         TabIndex        =   118
         ToolTipText     =   "Notas"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton BtRequisicoesFDS 
         Height          =   495
         Left            =   3000
         Picture         =   "FormIdUtente.frx":4335
         Style           =   1  'Graphical
         TabIndex        =   99
         ToolTipText     =   " Abrir �cran de Requisi��es "
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtMarcacoes 
         Height          =   495
         Left            =   4920
         Picture         =   "FormIdUtente.frx":4CA7
         Style           =   1  'Graphical
         TabIndex        =   88
         ToolTipText     =   " Abrir �cran de Marca��es de Consultas "
         Top             =   120
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton BtCartao 
         Height          =   495
         Left            =   11040
         Picture         =   "FormIdUtente.frx":5619
         Style           =   1  'Graphical
         TabIndex        =   84
         ToolTipText     =   " Imprimir cart�o de utente "
         Top             =   120
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton BtInfClinica 
         Height          =   495
         Left            =   6840
         Picture         =   "FormIdUtente.frx":5F8B
         Style           =   1  'Graphical
         TabIndex        =   45
         ToolTipText     =   " Informa��o Cl�nica do Utente "
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtActualizar 
         Height          =   495
         Left            =   8760
         Picture         =   "FormIdUtente.frx":68FD
         Style           =   1  'Graphical
         TabIndex        =   46
         ToolTipText     =   " Importar dados de utentes da Gest�o de Utentes "
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtRequisicoes 
         Height          =   495
         Left            =   1200
         Picture         =   "FormIdUtente.frx":726F
         Style           =   1  'Graphical
         TabIndex        =   43
         ToolTipText     =   " Abrir �cran de Requisi��es "
         Top             =   120
         Width           =   495
      End
      Begin VB.Label LaEnviarSMS 
         Caption         =   "Enviar SMS"
         Height          =   255
         Left            =   7605
         TabIndex        =   157
         Top             =   840
         Width           =   855
      End
      Begin VB.Label LaActual 
         Caption         =   "A&ctualiza��o"
         Height          =   255
         Left            =   7605
         TabIndex        =   156
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label LaER 
         Caption         =   "Criar Utilizador"
         Height          =   255
         Left            =   5760
         TabIndex        =   151
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Anexos"
         Height          =   255
         Index           =   2
         Left            =   3720
         TabIndex        =   144
         ToolTipText     =   "Informa��o Cl�nica do Utente"
         Top             =   840
         Width           =   975
      End
      Begin VB.Label LbFeedback 
         Caption         =   "FeedBack"
         Height          =   255
         Left            =   2040
         TabIndex        =   138
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Notas"
         Height          =   255
         Index           =   17
         Left            =   240
         TabIndex        =   120
         ToolTipText     =   "Informa��o Cl�nica do Utente"
         Top             =   840
         Width           =   975
      End
      Begin VB.Label LaRequisFDS 
         Caption         =   "&Fim Semana"
         Height          =   255
         Left            =   2040
         TabIndex        =   100
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label LaMarcacoes 
         Caption         =   "Marc. Colheitas"
         Height          =   255
         Left            =   3720
         TabIndex        =   89
         Top             =   240
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.Label LaCartao 
         Alignment       =   1  'Right Justify
         Caption         =   "Cart�o Utente"
         Height          =   255
         Left            =   9855
         TabIndex        =   85
         Top             =   240
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label LaImportado 
         Alignment       =   1  'Right Justify
         Caption         =   "LaImportado"
         ForeColor       =   &H000000C0&
         Height          =   255
         Left            =   9600
         TabIndex        =   83
         Top             =   720
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.Label LaInfCli 
         Caption         =   "In&form. Cl�nica"
         Height          =   255
         Left            =   5760
         TabIndex        =   44
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label LaRequis 
         Caption         =   "&Requisi��es"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame7 
      Height          =   855
      Left            =   120
      TabIndex        =   47
      Top             =   7800
      Width           =   11775
      Begin VB.CheckBox CkDirEsquecimento 
         Caption         =   "Direito ao Esquecimento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   9120
         TabIndex        =   165
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   960
         TabIndex        =   53
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   52
         Top             =   240
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   51
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   960
         TabIndex        =   50
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   3000
         TabIndex        =   49
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   3000
         TabIndex        =   48
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.TextBox EcSituacao 
      Height          =   285
      Left            =   6600
      TabIndex        =   91
      Top             =   9960
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcEpisodio 
      Height          =   285
      Left            =   6600
      TabIndex        =   90
      Top             =   9600
      Visible         =   0   'False
      Width           =   495
   End
   Begin BarcodLib.Barcod MBarcode 
      Height          =   375
      Left            =   9960
      TabIndex        =   87
      Top             =   9360
      Width           =   1335
      _Version        =   65543
      _ExtentX        =   2355
      _ExtentY        =   661
      _StockProps     =   75
      BackColor       =   16777215
      BarWidth        =   0
      Direction       =   0
      Style           =   3
      UPCNotches      =   3
      Alignment       =   0
      Extension       =   ""
   End
   Begin VB.TextBox EcPrinterCartoes 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   86
      Top             =   9480
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Frame Frame3 
      Height          =   2055
      Left            =   120
      TabIndex        =   82
      Top             =   0
      Width           =   11775
      Begin VB.TextBox EcDocIdentif 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9960
         TabIndex        =   164
         Top             =   1680
         Width           =   1695
      End
      Begin VB.TextBox EcProcHosp1 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9960
         TabIndex        =   6
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox EcCartaoUte 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7080
         TabIndex        =   10
         Top             =   720
         Width           =   1335
      End
      Begin VB.CommandButton BtPesquisaRNU 
         BackColor       =   &H00FFC0C0&
         Height          =   280
         Left            =   8400
         Picture         =   "FormIdUtente.frx":7BE1
         Style           =   1  'Graphical
         TabIndex        =   146
         ToolTipText     =   "  Pesquisa Utente (RNU)"
         Top             =   720
         Width           =   350
      End
      Begin VB.TextBox EcNomeDono 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   127
         Top             =   1200
         Width           =   4695
      End
      Begin VB.TextBox EcAbreviatura 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   114
         Top             =   1680
         Width           =   4695
      End
      Begin VB.TextBox EcNumExt 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7080
         TabIndex        =   107
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox EcIdade 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11160
         TabIndex        =   98
         Top             =   1200
         Width           =   495
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2040
         TabIndex        =   2
         Top             =   240
         Width           =   3375
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   720
         Width           =   4695
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "d/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   3
         EndProperty
         Height          =   285
         Left            =   9960
         TabIndex        =   18
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox EcProcHosp2 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9960
         TabIndex        =   7
         Top             =   240
         Width           =   1695
      End
      Begin VB.ComboBox EcDescrTipoUtente 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton BtPesquisaUtente 
         BackColor       =   &H8000000D&
         Height          =   315
         Left            =   5400
         Picture         =   "FormIdUtente.frx":7F6B
         Style           =   1  'Graphical
         TabIndex        =   0
         ToolTipText     =   "Pesquisa R�pida de Utentes"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcDataInscr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9960
         TabIndex        =   12
         Top             =   720
         Width           =   1095
      End
      Begin VB.ComboBox EcDescrSexo 
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   7080
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1200
         Width           =   1695
      End
      Begin VB.ComboBox EcDescrEstado 
         Height          =   315
         Left            =   7080
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1680
         Width           =   1695
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Identifica��o"
         Height          =   195
         Index           =   1
         Left            =   9000
         TabIndex        =   163
         ToolTipText     =   "Data Nascimento"
         Top             =   1680
         Width           =   915
      End
      Begin VB.Label LbNomeDono 
         AutoSize        =   -1  'True
         Caption         =   "&Nome Dono"
         Height          =   195
         Left            =   120
         TabIndex        =   128
         Top             =   1200
         Width           =   855
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "&Abreviatura"
         Height          =   195
         Left            =   120
         TabIndex        =   113
         Top             =   1680
         Width           =   810
      End
      Begin VB.Label LbNumExt 
         Caption         =   "Nr.� &Externo"
         Height          =   255
         Left            =   6000
         TabIndex        =   106
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Proc. &Hosp."
         Height          =   195
         Index           =   1
         Left            =   9000
         TabIndex        =   103
         ToolTipText     =   "Processos Hospitalares"
         Top             =   240
         Width           =   840
      End
      Begin VB.Label LbUtente 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   480
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Proc. &Hosp."
         Height          =   195
         Index           =   0
         Left            =   9000
         TabIndex        =   5
         ToolTipText     =   "Processos Hospitalares"
         Top             =   240
         Width           =   840
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "&Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   420
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data &Nasc."
         Height          =   195
         Index           =   0
         Left            =   9000
         TabIndex        =   17
         ToolTipText     =   "Data Nascimento"
         Top             =   1200
         Width           =   810
      End
      Begin VB.Label LbCartaoUte 
         AutoSize        =   -1  'True
         Caption         =   "&Cart�o Utente"
         Height          =   195
         Left            =   6000
         TabIndex        =   9
         Top             =   720
         Width           =   990
      End
      Begin VB.Label Label12 
         Caption         =   "Dt &Inscr."
         Height          =   255
         Left            =   9000
         TabIndex        =   11
         ToolTipText     =   "Data de Inscri��o"
         Top             =   720
         Width           =   735
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   6000
         TabIndex        =   13
         Top             =   1200
         Width           =   360
      End
      Begin VB.Label LbEstado 
         AutoSize        =   -1  'True
         Caption         =   "&Estado Civil"
         Height          =   195
         Left            =   6000
         TabIndex        =   15
         Top             =   1680
         Width           =   825
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4455
      Left            =   120
      TabIndex        =   81
      Top             =   2040
      Width           =   11775
      Begin VB.TextBox EcDescrRaca 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   135
         TabStop         =   0   'False
         Top             =   3480
         Width           =   3975
      End
      Begin VB.TextBox EcCodRaca 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   134
         Top             =   3480
         Width           =   1095
      End
      Begin VB.CommandButton BtPesqRapRaca 
         Height          =   315
         Left            =   6120
         Picture         =   "FormIdUtente.frx":84F5
         Style           =   1  'Graphical
         TabIndex        =   133
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras Respons�veis"
         Top             =   3480
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapEspecie 
         Height          =   315
         Left            =   6120
         Picture         =   "FormIdUtente.frx":8A7F
         Style           =   1  'Graphical
         TabIndex        =   132
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras Respons�veis"
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox EcCodEspecie 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   130
         Top             =   3000
         Width           =   1095
      End
      Begin VB.TextBox EcDescrEspecie 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   129
         TabStop         =   0   'False
         Top             =   3000
         Width           =   3975
      End
      Begin VB.TextBox EcDoenteProf 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8160
         TabIndex        =   125
         Top             =   2880
         Width           =   3315
      End
      Begin VB.CommandButton BtPesqPais 
         Height          =   315
         Left            =   6120
         Picture         =   "FormIdUtente.frx":9009
         Style           =   1  'Graphical
         TabIndex        =   122
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   2040
         Width           =   375
      End
      Begin VB.TextBox EcCodPais 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   121
         Top             =   2040
         Width           =   1095
      End
      Begin VB.CheckBox CkNaoImprCartao 
         Caption         =   "N�o imprimir cart�o"
         Height          =   255
         Left            =   9480
         TabIndex        =   112
         Top             =   2400
         Width           =   1935
      End
      Begin VB.TextBox EcFax 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         MultiLine       =   -1  'True
         TabIndex        =   110
         Top             =   1080
         Width           =   975
      End
      Begin VB.TextBox EcDtValidade 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         TabIndex        =   108
         Top             =   2400
         Width           =   1095
      End
      Begin VB.TextBox EcNrContrib 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         MaxLength       =   20
         TabIndex        =   101
         Top             =   240
         Width           =   3255
      End
      Begin VB.TextBox EcEmail 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         TabIndex        =   96
         Top             =   1440
         Width           =   3255
      End
      Begin VB.TextBox EcTelemovel 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10440
         MaxLength       =   9
         MultiLine       =   -1  'True
         TabIndex        =   95
         Top             =   765
         Width           =   975
      End
      Begin VB.CommandButton BtPesqProfissao 
         Height          =   315
         Left            =   6120
         Picture         =   "FormIdUtente.frx":9593
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Profiss�es"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcCodProfissao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1080
         TabIndex        =   20
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDescrProfissao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         Height          =   315
         Left            =   2160
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   240
         Width           =   3975
      End
      Begin VB.TextBox EcMorada 
         Appearance      =   0  'Flat
         Height          =   525
         Left            =   1080
         MultiLine       =   -1  'True
         TabIndex        =   24
         Top             =   765
         Width           =   5415
      End
      Begin VB.CommandButton BtPesqCodPostal 
         Height          =   315
         Left            =   6120
         Picture         =   "FormIdUtente.frx":9B1D
         Style           =   1  'Graphical
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   1485
         Width           =   375
      End
      Begin VB.TextBox EcDescrPostal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   2160
         TabIndex        =   28
         Top             =   1485
         Width           =   3975
      End
      Begin VB.TextBox EcCodPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   26
         Top             =   1485
         Width           =   615
      End
      Begin VB.TextBox EcRuaPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1680
         MaxLength       =   3
         TabIndex        =   27
         Top             =   1485
         Width           =   490
      End
      Begin VB.TextBox EcTelefone 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         MultiLine       =   -1  'True
         TabIndex        =   31
         Top             =   765
         Width           =   975
      End
      Begin VB.CommandButton BtPesqEntFinR 
         Height          =   315
         Left            =   6120
         Picture         =   "FormIdUtente.frx":A0A7
         Style           =   1  'Graphical
         TabIndex        =   35
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras Respons�veis"
         Top             =   2520
         Width           =   375
      End
      Begin VB.TextBox EcCodEntFinR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   1080
         TabIndex        =   33
         Top             =   2520
         Width           =   1095
      End
      Begin VB.TextBox EcDescrEntFinR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         Height          =   315
         Left            =   2160
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   2520
         Width           =   3975
      End
      Begin VB.TextBox EcNrBenef 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         TabIndex        =   37
         Top             =   1965
         Width           =   3255
      End
      Begin VB.ComboBox EcDescrIsencao 
         Height          =   315
         Left            =   8160
         Style           =   2  'Dropdown List
         TabIndex        =   39
         Top             =   3405
         Width           =   3375
      End
      Begin VB.TextBox EcObs 
         Appearance      =   0  'Flat
         Height          =   495
         Left            =   1080
         MultiLine       =   -1  'True
         TabIndex        =   41
         Top             =   3840
         Width           =   10455
      End
      Begin VB.TextBox EcDescrPais 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   123
         TabStop         =   0   'False
         Top             =   2040
         Width           =   3975
      End
      Begin VB.Label LbRaca 
         Caption         =   "Ra�a"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   136
         Top             =   3480
         Width           =   735
      End
      Begin VB.Label LbEspecie 
         Caption         =   "Esp�cie"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   131
         Top             =   3000
         Width           =   735
      End
      Begin VB.Label Labe41 
         Caption         =   "N� Doente Prof."
         Height          =   255
         Index           =   2
         Left            =   6960
         TabIndex        =   126
         Top             =   2880
         Width           =   1500
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pa�s"
         Height          =   195
         Index           =   36
         Left            =   120
         TabIndex        =   124
         Top             =   2040
         Width           =   330
      End
      Begin VB.Label LbTelefone 
         AutoSize        =   -1  'True
         Caption         =   "Fax :"
         Height          =   195
         Index           =   2
         Left            =   6960
         TabIndex        =   111
         ToolTipText     =   "N�mero de Fax"
         Top             =   1080
         Width           =   345
      End
      Begin VB.Label Label24 
         Caption         =   "Dt Validade"
         Height          =   255
         Left            =   6960
         TabIndex        =   109
         ToolTipText     =   "Data Validade Cart�o ADSE"
         Top             =   2400
         Width           =   855
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "Nr.� &Contrib."
         Height          =   195
         Left            =   6960
         TabIndex        =   102
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "Email"
         Height          =   195
         Left            =   6960
         TabIndex        =   97
         Top             =   1440
         Width           =   375
      End
      Begin VB.Label LbTelefone 
         AutoSize        =   -1  'True
         Caption         =   "Telem�vel :"
         Height          =   195
         Index           =   1
         Left            =   9360
         TabIndex        =   94
         Top             =   765
         Width           =   825
      End
      Begin VB.Label LbProfissao 
         AutoSize        =   -1  'True
         Caption         =   "&Profiss�o"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   645
      End
      Begin VB.Label LbMorada 
         AutoSize        =   -1  'True
         Caption         =   "&Morada"
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   765
         Width           =   540
      End
      Begin VB.Label LbCodPostal 
         AutoSize        =   -1  'True
         Caption         =   "C. Pos&tal"
         Height          =   195
         Left            =   120
         TabIndex        =   25
         Top             =   1485
         Width           =   630
      End
      Begin VB.Label LbTelefone 
         AutoSize        =   -1  'True
         Caption         =   "Telefone :"
         Height          =   195
         Index           =   0
         Left            =   6960
         TabIndex        =   30
         Top             =   765
         Width           =   720
      End
      Begin VB.Label LbEntFinResp 
         AutoSize        =   -1  'True
         Caption         =   "E. &F.R."
         Height          =   195
         Left            =   120
         TabIndex        =   32
         Top             =   2520
         Width           =   495
      End
      Begin VB.Label LbNrBenef 
         AutoSize        =   -1  'True
         Caption         =   "Nr.� &Benef."
         Height          =   195
         Left            =   6960
         TabIndex        =   36
         Top             =   1965
         Width           =   780
      End
      Begin VB.Label LbIsencao 
         AutoSize        =   -1  'True
         Caption         =   "I&sen��o"
         Height          =   195
         Left            =   6960
         TabIndex        =   38
         Top             =   3405
         Width           =   570
      End
      Begin VB.Label Label14 
         Caption         =   "&Obs."
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   3885
         Width           =   855
      End
   End
   Begin VB.TextBox EcSexo 
      Height          =   285
      Left            =   9960
      TabIndex        =   78
      Top             =   9120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEstCiv 
      Height          =   285
      Left            =   9960
      TabIndex        =   77
      Top             =   8880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodIsencao 
      Height          =   285
      Left            =   4200
      TabIndex        =   75
      Top             =   9360
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Timer TimerRS 
      Interval        =   60000
      Left            =   360
      Top             =   6240
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   9000
      TabIndex        =   73
      Top             =   9120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   9000
      TabIndex        =   71
      Top             =   9240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodPostalCompleto 
      Height          =   285
      Left            =   6600
      TabIndex        =   69
      Top             =   9240
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcTipoUtente 
      Height          =   285
      Left            =   8760
      TabIndex        =   67
      Top             =   9120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapEFR 
      Height          =   285
      Left            =   6600
      TabIndex        =   65
      Top             =   8880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   60
      Top             =   9240
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   59
      Top             =   9270
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   58
      Top             =   9000
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   57
      Top             =   9000
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcPesqRapProfis 
      Height          =   285
      Left            =   6600
      TabIndex        =   55
      Top             =   9240
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "EcDtEr"
      Height          =   255
      Index           =   3
      Left            =   7560
      TabIndex        =   154
      Top             =   10440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgAnexo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   150
      Top             =   12120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgDiagPrim"
      Enabled         =   0   'False
      Height          =   255
      Index           =   29
      Left            =   0
      TabIndex        =   148
      Top             =   11760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcPrescricao"
      Height          =   255
      Index           =   1
      Left            =   7560
      TabIndex        =   117
      Top             =   10080
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label23 
      Caption         =   "EcImprCartao"
      Height          =   255
      Index           =   0
      Left            =   600
      TabIndex        =   105
      Top             =   9000
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label19 
      Caption         =   "EcSituacao"
      Height          =   255
      Left            =   5280
      TabIndex        =   93
      Top             =   9960
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "EcEpisodio"
      Height          =   255
      Index           =   0
      Left            =   5160
      TabIndex        =   92
      Top             =   9600
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label9 
      Caption         =   "EcSexo"
      Height          =   255
      Left            =   10440
      TabIndex        =   80
      Top             =   9120
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label10 
      Caption         =   "EcEstCiv"
      Height          =   255
      Left            =   10440
      TabIndex        =   79
      Top             =   8880
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label13 
      Caption         =   "EcCodIsencao"
      Height          =   255
      Left            =   2880
      TabIndex        =   76
      Top             =   9240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label11 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Left            =   7560
      TabIndex        =   74
      Top             =   9120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodPostalAux"
      Height          =   255
      Left            =   7560
      TabIndex        =   72
      Top             =   9240
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "EcCodPostalCompleto"
      Height          =   255
      Left            =   4800
      TabIndex        =   70
      Top             =   9240
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label6 
      Caption         =   "EctipoUtente"
      Height          =   255
      Left            =   7680
      TabIndex        =   68
      Top             =   9120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "EcPesqRapEFR"
      Height          =   255
      Left            =   5160
      TabIndex        =   66
      Top             =   8880
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label15 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2880
      TabIndex        =   64
      Top             =   9000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label16 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2880
      TabIndex        =   63
      Top             =   9240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label17 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   480
      TabIndex        =   62
      Top             =   9000
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label18 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   600
      TabIndex        =   61
      Top             =   9240
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "EcPesqRapProfis"
      Height          =   255
      Left            =   5160
      TabIndex        =   56
      Top             =   9240
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormIdentificaUtente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

'vari�veis que guardam o formato do n�mero de beneficiario que est� na BD
Dim Formato1 As String
Dim Formato2 As String

Public rs As ADODB.recordset

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind As Integer
Dim Max  As Integer

Public Flg_PesqUtente As Boolean
Public InfoComplementarID As String

'NELSONPSILVA 11.07.2018 - Glintt-Hs-18011
Dim UserPermissaoRGPD As Integer
'

Sub FuncaoInserir()
    
    Dim iRes As Integer
    Dim msgRespAux As String
    Dim processo As Boolean
    Dim Flg_abortar As Boolean
    Dim i As Long
    On Error GoTo TrataErro
    LaImportado.caption = ""
    LaImportado.Visible = False
    If gFeedBackUtente = mediSim Then
        If CbFeedback.ListIndex = mediComboValorNull Then
            BG_Mensagem mediMsgStatus, "Campo Obrigat�rio!", , "Feedback do Utente"
            BtFeedback_Click
            Exit Sub
        End If
    End If
    If EcSexo <> "" Then
        If EcSexo <> gT_Masculino And EcSexo <> gT_Feminino Then
            If MsgBox("Sexo do Utente indetermindado. Deseja gravar?   ", vbYesNo + vbQuestion, "Sislab") <> vbYes Then
                Exit Sub
            End If
        End If
    End If
    If EcCodPais.text = "" Then
        If BL_ValidaNIF(EcNrContrib.text) = False Then
            EcNrContrib.SetFocus
            Sendkeys ("{HOME}+{END}")
            Exit Sub
        End If
    End If
    
    If EcUtente.Visible = True And EcUtente.Enabled = True Then
        EcUtente.SetFocus
    Else
        EcCodProfissao.SetFocus
    End If
    
    Flg_abortar = False
    
    EcUtilizadorCriacao = gCodUtilizador
    EcDataCriacao = Bg_DaData_ADO
    
    If EcCodEspecie = "" Then
        If gCodGeneroDefeito > mediComboValorNull Then
            EcCodEspecie = gCodGeneroDefeito
            EcCodespecie_Validate False
        End If
    End If
    
    'se a data de inscri��o estiver vazia
    If EcDataInscr.text = "" Then
        EcDataInscr.text = Format(Bg_DaData_ADO, gFormatoData)
    End If
   
    If Trim(EcDataNasc.text) = "" Then
        'EcDataNasc.text = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If EcDescrTipoUtente.ListIndex = -1 Then
        If gLAB = "BIO" Then
            EcDescrTipoUtente.text = "PRO"
        ElseIf gTipoInstituicao = gTipoInstituicaoHospitalar Then
            EcDescrTipoUtente.text = "LAB"          'obrigar a que seja por defeito criado um utente LAB
        ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoAguas Then
            EcDescrTipoUtente.text = gInstituicao
        End If
    End If
    
    '  VALIDAR NUMERO DE BENEFICI�RIO & ENTIDADE
    If gTipoInstituicao = gTipoInstituicaoPrivada Then
        If UTENTE_VerificaNrBenef(EcNrBenef, EcCodEntFinR) = True Then
            gMsgTitulo = "Validar Nr Benefici�rio"
            gMsgMsg = "J� existe um  N� de Benefici�rio para essa entidade. Deseja continuar? "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton2 + vbExclamation, gMsgTitulo)
            If gMsgResp = vbCancel Then
                EcProcHosp1.SetFocus
                Flg_abortar = True
            End If
        End If
    End If
    
    
'    'validar processos
'    processo = UTENTE_VerificaProcesso("1", _
'                                       EcCodSequencial.text, _
'                                       EcProcHosp1.text, _
'                                       EcProcHosp2.text)
'
'    If processo = True Then
'        gMsgTitulo = "Validar Processo"
'        gMsgMsg = "J� existe um processo N� " & EcProcHosp1.text
'        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton2 + vbExclamation, gMsgTitulo)
'        If gMsgResp = vbCancel Then
'            EcProcHosp1.SetFocus
'            Flg_abortar = True
'        End If
'    End If
'
'    If Flg_abortar = False Then
'        'valida processo 2
'        processo = UTENTE_VerificaProcesso("2", _
'                                           EcCodSequencial.text, _
'                                           EcProcHosp1.text, _
'                                           EcProcHosp2.text)
'
'        If processo = True Then
'            gMsgTitulo = "Validar Processo"
'            gMsgMsg = "J� existe um processo N� " & EcProcHosp2.text
'            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton2 + vbExclamation, gMsgTitulo)
'            If gMsgResp = vbCancel Then
'                EcProcHosp2.SetFocus
'                Flg_abortar = True
'            End If
'        End If
'    End If
    
'    If Flg_abortar = False Then
'
'        Dim RsNUtente As ADODB.recordset
'        Dim Sql As String
'        Set RsNUtente = New ADODB.recordset
'        RsNUtente.CursorLocation = adUseServer
'        RsNUtente.Open "SELECT seq_utente, t_utente,utente, nome_ute, dt_nasc_ute,descr_mor_ute,n_benef_ute FROM sl_identif WHERE nome_ute = " & UCase(BL_TrataStringParaBD(EcNome)) & " AND dt_nasc_ute = " & BL_TrataStringParaBD(EcDataNasc), gConexao, adOpenStatic, adLockReadOnly
'
'        If RsNUtente.RecordCount > 0 Then
'            gMsgResp = BG_Mensagem(mediMsgBox, "J� existe um utente do tipo " & RsNUtente!t_utente & " com o n�mero " & RsNUtente!utente & "!" & Chr(13) & "Nome : " & RsNUtente!nome_ute & Chr(13) & "Dt. Nasc : " & RsNUtente!dt_nasc_ute & Chr(13) & "Morada : " & BL_HandleNull(RsNUtente!descr_mor_ute, "") & Chr(13) & "N. Benef : " & BL_HandleNull(RsNUtente!n_benef_ute, "") & Chr(13) & "Deseja substituir o utente?", vbYesNo + vbQuestion, "Inserir Utente")
'            If gMsgResp = vbYes Then
'                If EcTipoUtente <> "LAB" Then
'                'FN 01-09-2009
'                    Sql = "UPDATE sl_identif SET t_utente = " & BL_TrataStringParaBD(EcTipoUtente.text) & ", utente = " & BL_TrataStringParaBD(EcUtente.text) & ", n_proc_1 = " & BL_TrataStringParaBD(EcProcHosp1.text) & ", n_epis = " & BL_HandleNull(EcEpisodio, "Null") & ", t_sit = " & BL_HandleNull(EcSituacao, -1) & "  WHERE seq_utente = " & RsNUtente!seq_utente
'                    BG_ExecutaQuery_ADO Sql
'                End If
'                LimpaCampos
'                EcCodSequencial = RsNUtente!seq_utente
'                FuncaoProcurar (True)
'                RsNUtente.Close
'                Set RsNUtente = Nothing
'                Exit Sub
'            Else
'                RsNUtente.Close
'                Set RsNUtente = Nothing
'            End If
'        Else
'            RsNUtente.Close
'            Set RsNUtente = Nothing
'        End If
'    End If

    
    'o numero do utente e o tipo do utente foram preenchidos e os processos validados
    If Flg_abortar = False Then
        gMsgTitulo = "Inserir"
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            If ValidaNumBenef = True Then
                BL_InicioProcessamento Me, "A inserir registo."
                iRes = ValidaCamposEc
                If iRes = True Then
                    BD_Insert
                    If BtRequisicoes.Enabled = True Then
                        BtRequisicoes.SetFocus
                    End If
                End If
                BL_FimProcessamento Me
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoInserir: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoInserir"
    Exit Sub
    Resume Next
End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer
    On Error GoTo TrataErro
        
    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    ValidaCamposEc = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaCamposEc: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaCamposEc"
    Exit Function
    Resume Next
End Function

Sub FuncaoProcurar(Optional SuprimeMsg As Boolean)
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    On Error GoTo TrataErro
    
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos os utentes ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set rs = Nothing
            Exit Sub
        End If
        CriterioTabela = CriterioTabela & "  ORDER BY " & ChaveBD & " DESC"
    Else
    End If
              
    ' Permite que a pesquisa n�o seja case sensitive.
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                    "nome_ute", _
                                    True)
    
    BL_InicioProcessamento Me, "A pesquisar registos."
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.EOF Then
        If SuprimeMsg = True Then Exit Sub
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        EcDescrTipoUtente.Enabled = False
        EcUtente.Enabled = False
        If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AlterarDoente") <> "1" Then
            EcNome.Enabled = False
        End If
        EcDataInscr.Enabled = False
        BtPesquisaUtente.Enabled = False
        
        LimpaCampos
        PreencheCampos
        gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
        BL_ToolbarEstadoN Estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
    Set gFormActivo = Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar"
    Exit Sub
    Resume Next
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    On Error GoTo TrataErro
    
    If rs.EOF Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_VerificaAnexosUtente Me, BL_HandleNull(EcFlgAnexo.text, mediComboValorNull)
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
        BL_ColocaTextoCombo "sl_tbf_estciv", "cod_est_civ", "cod_est_civ", EcEstCiv, EcDescrEstado
        BL_ColocaTextoCombo "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente
        BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, EcDescrIsencao
        EcCodpais_Validate False
        EcCodraca_Validate False
        EcCodespecie_Validate False
        PreencheDescProfissao
        PreencheDescEntFinanceira
        PreencheCodigoPostal
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        BtInfClinica.Enabled = True
        LaInfCli.Enabled = True
        BtRequisicoes.Enabled = True
        
        BtRequisicoes.SetFocus
        BtRequisicoesFDS.Enabled = True
        BtMarcacoes.Enabled = True
        LaRequis.Enabled = True
        LaRequisFDS.Enabled = True
        LaMarcacoes.Enabled = True
        BtCartao.Enabled = True
        LaCartao.Enabled = True
        If gHIS_Import = 0 Then
            BtActualizar.Enabled = False
            LaActual.Enabled = False
        Else
            BtActualizar.Enabled = True
            LaActual.Enabled = True
        End If
        If EcDataNasc <> "" Then
            PreencheIdade
        End If
        BtNotas.Enabled = True
        BtNotasVRM.Enabled = True
        PreencheNotas
        
        'RGONCALVES 16.06.2013 ICIL-470
        BtSMS.Enabled = True
        LaEnviarSMS.Enabled = True
        
        'NELSONPSILVA Glintt-HS-18011 STANDALONE
        If UserPermissaoRGPD = 1 Then
            If UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
            CkDirEsquecimento.Enabled = True
            End If
        End If
        '
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCampos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCampos"
    Exit Sub
    Resume Next
End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim RsNUtente As ADODB.recordset
    Dim Query As String
    Dim i As Integer
    Dim seq As Long
    
    On Error GoTo Trata_Erro
        
    If Trim(EcUtente) = "" Then
        If BG_Mensagem(mediMsgBox, "Quer gerar um n�mero sequencial para o utente do tipo " & EcDescrTipoUtente.text & "?", vbYesNo + vbDefaultButton2 + vbQuestion, "Gerar Sequencial") = vbNo Then
            Exit Sub
        End If
        
        'Determinar o novo numero para o Tipo Utente (TENTA 10 VEZES - LOCK!)
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraUtente(EcTipoUtente)
            i = i + 1
        Wend
        
        If seq = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero do utente!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
        
        EcUtente = seq
    Else
        Set RsNUtente = New ADODB.recordset
        RsNUtente.CursorLocation = adUseServer
        RsNUtente.Open "SELECT nome_ute, dt_nasc_ute FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(EcTipoUtente) & " AND utente = " & BL_TrataStringParaBD(EcUtente), gConexao, adOpenStatic, adLockReadOnly
        
        If RsNUtente.RecordCount > 0 Then
            
            If BG_Mensagem(mediMsgBox, "J� existe um utente do tipo " & EcDescrTipoUtente.text & " com o n�mero " & EcUtente & "!" & Chr(13) & "Nome : " & BL_HandleNull(RsNUtente!nome_ute) & Chr(13) & "Dt. Nasc : " & BL_HandleNull(RsNUtente!dt_nasc_ute) & Chr(13) & "Deseja actualizar o utente?", vbYesNo + vbExclamation, "Inserir Utente") = vbYes Then
                Dim TUtenteAux As String
                Dim UtenteAux As String
                Dim tSitAux As String
                Dim episAux As String
                TUtenteAux = EcDescrTipoUtente.text
                UtenteAux = EcUtente.text
                tSitAux = EcSituacao
                episAux = EcEpisodio
                LimpaCampos
                EcDescrTipoUtente.text = TUtenteAux
                EcUtente.text = UtenteAux
                FuncaoProcurar (True)
                EcSituacao = tSitAux
                EcEpisodio = episAux
                ActualizaUtente (False)
            End If
            RsNUtente.Close
            Set RsNUtente = Nothing
            Exit Sub
        Else
            RsNUtente.Close
            Set RsNUtente = Nothing
        End If
    End If
        
    'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
    i = 0
    seq = -1
    While seq = -1 And i <= 10
        seq = BL_GeraNumero("SEQ_UTENTE")
        i = i + 1
    Wend
    
    If seq = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
    EcCodSequencial = seq
    
    gSQLError = 0
    gSQLISAM = 0
    
    If gNomeCapitalizado = 1 Then
        EcNome.text = StrConv(EcNome.text, vbProperCase)
    Else
        EcNome.text = UCase(EcNome.text)
    End If
    
    If EcCodEspecie = "" Then
        If gCodGeneroDefeito > mediComboValorNull Then
            EcCodEspecie = gCodGeneroDefeito
            EcCodespecie_Validate False
        End If
    End If
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    If gSQLError <> 0 Then
        'Erro a inserir utente
        
        GoTo Trata_Erro
    End If
    
    BtInfClinica.Enabled = True
    LaInfCli.Enabled = True
    BtRequisicoes.Enabled = True
    
    BtMarcacoes.Enabled = True
    LaRequis.Enabled = True
    LaMarcacoes.Enabled = True
    BtCartao.Enabled = True
    LaCartao.Enabled = True
    gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
    
    BG_CommitTransaction
    
    If gPreencheEpis = 1 Then
        GravaEpisodio seq, gEpisodioActivo, gSituacaoActiva
    End If
    
    Exit Sub
    
Trata_Erro:
    BG_LogFile_Erros "FormIdentificaUtente: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos

End Sub

Sub FuncaoModificar(Optional supress_msg As Boolean)
    
    Dim iRes As Integer
    Dim processo As Boolean
    Dim Flg_abortar As Boolean
    On Error GoTo TrataErro
    
    Flg_abortar = False
    If EcSexo <> "" Then
        If EcSexo <> gT_Masculino And EcSexo <> gT_Feminino Then
            If MsgBox("Sexo do Utente indetermindado. Deseja gravar?   ", vbYesNo + vbQuestion, "Sislab") <> vbYes Then
                Exit Sub
            End If
        End If
    End If
        
    If EcCodPais.text = "" Then
        If BL_ValidaNIF(EcNrContrib.text) = False Then
            EcNrContrib.SetFocus
            Sendkeys ("{HOME}+{END}")
            Exit Sub
        End If
    End If
    
    If EcUtente.Visible = True And EcUtente.Enabled = True Then
        EcUtente.SetFocus
    Else
        EcCodProfissao.SetFocus
    End If
    
    EcUtilizadorAlteracao = gCodUtilizador
    EcDataAlteracao = Bg_DaData_ADO
    
    'se a data de inscri��o estiver vazia
    If EcDataInscr.text = "" Then
        EcDataInscr.text = Format(Bg_DaData_ADO, gFormatoData)
    End If
   
    If Trim(EcDataNasc.text) = "" Then
        EcDataNasc.text = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If EcDescrTipoUtente.ListIndex = -1 Then
        EcDescrTipoUtente.ListIndex = 0
    End If
    
    'validar processos
    processo = UTENTE_VerificaProcesso("1", _
                                       EcCodSequencial.text, _
                                       EcProcHosp1.text, _
                                       EcProcHosp2.text)
    
    If processo = True Then
        gMsgTitulo = "Validar Processo"
        gMsgMsg = "J� existe um processo N� " & EcProcHosp1.text
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton2 + vbExclamation, gMsgTitulo)
        If gMsgResp = vbCancel Then
            EcProcHosp1.SetFocus
            Flg_abortar = True
        End If
    End If
    
    If Flg_abortar = False Then
        'valida processo 2
        processo = UTENTE_VerificaProcesso("2", _
                                           EcCodSequencial.text, _
                                           EcProcHosp1.text, _
                                           EcProcHosp2.text)
        
        If processo = True Then
            gMsgTitulo = "Validar Processo"
            gMsgMsg = "J� existe um processo N� " & EcProcHosp2.text
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKCancel + vbDefaultButton2 + vbExclamation, gMsgTitulo)
            If gMsgResp = vbCancel Then
                EcProcHosp2.SetFocus
                Flg_abortar = True
            End If
        End If
    End If
    
    'o numero do utente e o tipo do utente foram preenchidos e os processos validados
    If Flg_abortar = False Then
        If supress_msg = False Then
            gMsgTitulo = "Modificar"
            gMsgMsg = "Tem a certeza que quer Modificar estes dados ?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        Else
            gMsgResp = vbYes
        End If
        If gMsgResp = vbYes Then
            If ValidaNumBenef = True Then
                BL_InicioProcessamento Me, "A modificar registo."
                iRes = ValidaCamposEc
                If iRes = True Then
                    BD_Update
                End If
                BL_FimProcessamento Me
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoModificar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoModificar"
    Exit Sub
    Resume Next
End Sub

Sub BD_Update()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim i As Long
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    BG_BeginTransaction
    
    If gNomeCapitalizado = 1 Then
        EcNome.text = StrConv(EcNome.text, vbProperCase)
    Else
        EcNome.text = UCase(EcNome.text)
    End If
    
    If EcCodEspecie = "" Then
        If gCodGeneroDefeito > mediComboValorNull Then
            EcCodEspecie = gCodGeneroDefeito
            EcCodespecie_Validate False
        End If
    End If
    
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    If gSQLError <> 0 Then
        
        GoTo Trata_Erro
    End If
        
    BG_CommitTransaction
    
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If rs.BOF And rs.EOF Then
        EcDescrTipoUtente.Enabled = True
        EcUtente.Enabled = True
        EcNome.Enabled = True
        EcDataNasc.Enabled = True
        EcDataInscr.Enabled = True
        BtPesquisaUtente.Enabled = True
        Set CampoDeFocus = BtPesquisaUtente
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos
    
    If gPreencheEpis = 1 Then
        GravaEpisodio CLng(EcCodSequencial.text), gEpisodioActivo, gSituacaoActiva
    End If
    
    Exit Sub

Trata_Erro:
    BG_LogFile_Erros "FormIdentificaUtente: BD_update -> " & Err.Description & " " & SQLQuery
    BG_RollbackTransaction

End Sub

Sub FuncaoLimpar()
    On Error GoTo TrataErro
    
    Set CampoDeFocus = BtPesquisaUtente
    EcDescrTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcNome.Enabled = True
    EcDataNasc.Enabled = True
    EcDataInscr.Enabled = True
    BtPesquisaUtente.Enabled = True
        
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        Set CampoDeFocus = BtPesquisaUtente
        CampoDeFocus.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoLimpar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoLimpar"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoEstadoAnterior()
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        'CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Sub FuncaoRemover()
    On Error GoTo TrataErro
    Exit Sub
    If EcCodSequencial <> "" Then
        If BL_VfUteResultados(EcCodSequencial) = True Then
            gMsgTitulo = "Remo��o n�o autorizada."
            gMsgMsg = "Utente " & EcDescrTipoUtente.List(EcDescrTipoUtente.ListIndex) & "/" & EcUtente & " j� tem resultados!"
            BG_Mensagem mediMsgBox, gMsgMsg, vbOKOnly + vbInformation, gMsgTitulo
            Exit Sub
        End If
    End If
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?" & Chr(13) & "Todos os dados pertencentes ao utente ser�o eliminados!"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoRemover: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoRemover"
    Exit Sub
    Resume Next
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLRmRel As String
    Dim RsRmRel As ADODB.recordset
    Dim ReqsUte As String
    Dim MarcaLocal As Variant
           
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    BG_BeginTransaction
    
    'Seleccionar as requisi��es do utente sem resultado
    Set RsRmRel = New ADODB.recordset
    SQLRmRel = "( SELECT n_req FROM sl_requis WHERE seq_utente = " & rs(ChaveBD) & " )" & _
            "UNION (SELECT n_req FROM sl_falt_req WHERE seq_utente = " & rs(ChaveBD) & " )"
    With RsRmRel
        .Source = SQLRmRel
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLRmRel
        .Open , gConexao
    End With
    If RsRmRel.EOF Then
        ReqsUte = ""
    Else
        ReqsUte = " ( "
        While Not RsRmRel.EOF
            ReqsUte = ReqsUte & BL_HandleNull(RsRmRel!n_req, 0) & ","
            RsRmRel.MoveNext
        Wend
        ReqsUte = Mid(ReqsUte, 1, Len(ReqsUte) - 1) & ")"
    End If
    
    RsRmRel.Close
    Set RsRmRel = Nothing
    
    gSQLError = 0
    If ReqsUte <> "" Then
        'Remover requisi��es do utente
        SQLRmRel = "DELETE FROM sl_requis WHERE seq_utente = " & rs(ChaveBD)
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
    
        'Remover diagn�sticos principais do utente
        SQLRmRel = "DELETE FROM sl_diag_pri WHERE seq_utente = " & rs(ChaveBD)
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover requisi��es em que o utente faltou
        SQLRmRel = "DELETE FROM sl_falt_req WHERE seq_utente = " & rs(ChaveBD)
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro

        'Remover marca��es do utente
        SQLRmRel = "DELETE FROM sl_marcacoes WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover diagn�sticos secund�rios do utente
        SQLRmRel = "DELETE FROM sl_diag_sec WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover produtos de requisi��es em que o utente faltou
        SQLRmRel = "DELETE FROM sl_falt_prod WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover recibos do utente
        SQLRmRel = "DELETE FROM " & _
                   "        sl_recibos " & _
                   "WHERE " & _
                   "        n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        
        'Remover requisi��es canceladas do utente
        SQLRmRel = "DELETE FROM sl_req_canc WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover produtos das requisi��es do utente
        SQLRmRel = "DELETE FROM sl_req_prod WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover terap�uticas e medica��o do utente
        SQLRmRel = "DELETE FROM sl_tm_ute WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
    End If
        
    condicao = ChaveBD & " = " & rs(ChaveBD)
    SQLQuery = "DELETE FROM sl_identif WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    If gSQLError <> 0 Then GoTo Trata_Erro

    BG_CommitTransaction
    
    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        EcDescrTipoUtente.Enabled = True
        EcUtente.Enabled = True
        EcNome.Enabled = True
        EcDataNasc.Enabled = True
        EcDataInscr.Enabled = True
        BtPesquisaUtente.Enabled = True
        Set CampoDeFocus = BtPesquisaUtente
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

    Exit Sub
    
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a eliminar utente!", vbOKOnly + vbCritical, App.ProductName
    BG_LogFile_Erros "FormIdentificaUtente: BD_Delete -> SQL(" & gSQLError & ") " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
        BL_FimProcessamento Me
    End If

End Sub

Sub LimpaCampos()
    On Error GoTo TrataErro
    
    Me.SetFocus

    Set CampoDeFocus = BtPesquisaUtente
    BG_LimpaCampo_Todos CamposEc
    EcDescrSexo.ListIndex = -1
    EcDescrEstado.ListIndex = -1
    EcDescrIsencao.ListIndex = -1
    EcDescrTipoUtente.ListIndex = -1
    
    EcCodSequencial.text = ""
    EcCodPostal.text = ""
    EcRuaPostal.text = ""
    EcDescrPostal.text = ""
    EcDescrProfissao.text = ""
    EcDescrEntFinR.text = ""
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcPesqRapProfis.text = ""
    EcPesqRapEFR.text = ""
    EcCodPostalCompleto.text = ""
    EcCodPostalAux.text = ""
    EcDescrPais = ""
    Formato1 = ""
    Formato2 = ""
    
    BtInfClinica.Enabled = False
    LaInfCli.Enabled = False
    BtActualizar.Enabled = False
    LaActual.Enabled = False
    BtRequisicoes.Enabled = False
    
    BtRequisicoesFDS.Enabled = False
    BtMarcacoes.Enabled = False
    LaRequis.Enabled = False
    LaRequisFDS.Enabled = False
    LaMarcacoes.Enabled = False
    BtCartao.Enabled = False
    LaCartao.Enabled = False
    LaImportado.caption = ""
    LaImportado.Visible = False
    EcUtente.text = ""
    gEpisodioActivo = ""
    gEpisodioActivoAux = ""
    EcIdade.text = ""
    CkNaoImprCartao.value = vbGrayed
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    EcDescrEspecie = ""
    EcDescrRaca = ""
    FrameFeedback.Visible = False
    CkInibeSMS.value = vbGrayed
    BL_VerificaAnexosUtente Me, mediComboValorNull
    
    'RGONCALVES 16.06.2013 ICIL-470
    EcEnviarSMS.text = ""
    LaCaracteresFaltaSMS.caption = EcEnviarSMS.MaxLength
    FrEnviarSMS.Visible = False
    LaEnviarSMS.Enabled = False
    BtSMS.Enabled = False
    '
    'BRUNODSANTOS 21.10.2016 LACTO-336
    'EcinfComplementar.Text = ""
    '
    
    'NELSONPSILVA Glintt-HS-18011 STANDALONE
    If UserPermissaoRGPD = 1 Then
        If UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
            CkDirEsquecimento.Enabled = False
        End If
    End If
    '
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos"
    Exit Sub
    Resume Next
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    Max = -1
    ReDim FOPropertiesTemp(0)
    gF_IDENTIF = 1
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
        
    Estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me
    
    Flg_PesqUtente = False
    
End Sub

Sub EventoActivate()
    Dim RsCodEFR As ADODB.recordset
    
    On Error GoTo Trata_Erro
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    If CampoDeFocus.Enabled = True Then
        CampoDeFocus.SetFocus
    End If
    
    If FormIdentificaUtente.Enabled = False Then
        FormIdentificaUtente.Enabled = True
    End If
    
    LaImportado.caption = ""
    LaImportado.Visible = False
    
    'Foi pesquisado um utente na form requisicoes mas n�o foi encontrado
    If gDUtente.seq_utente = "" And Flg_PesqUtente = True Then
        If gDUtente.descr_sexo_ute <> "" Or gDUtente.dt_nasc_ute <> "" Or _
            gDUtente.n_cartao_ute <> "" Or gDUtente.n_proc_1 <> "" Or _
            gDUtente.n_proc_2 <> "" Or gDUtente.nome_ute <> "" Or _
            gDUtente.sexo_ute <> "" Or gDUtente.t_utente <> "" Or gDUtente.Utente <> "" Then
            
            'Pesquisa de utente falhada
            FuncaoLimpar
            EcDataNasc = gDUtente.dt_nasc_ute
            EcCartaoUte = gDUtente.n_cartao_ute
            EcProcHosp1 = gDUtente.n_proc_1
            EcProcHosp2 = gDUtente.n_proc_2
            EcNome = gDUtente.nome_ute
            EcSexo = gDUtente.sexo_ute
            BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
            EcTipoUtente = gDUtente.t_utente
            EcDescrTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
            EcPrescricao = gDUtente.n_prescricao
            EcCodIsencao = gDUtente.cod_isencao_ute
            EcEmail = gDUtente.email
            EcCodIsencao_Change
            EcCodEspecie = gDUtente.cod_genero
            EcCodespecie_Validate False
            'ICIL-930
            EcTelefone.text = gDUtente.telefone
            EcTelemovel.text = gDUtente.telemovel
            EcNrContrib.text = gDUtente.num_contribuinte
            Flg_PesqUtente = False
        End If
    ElseIf gDUtente.seq_utente <> "" And Flg_PesqUtente = True Then
        If gDUtente.seq_utente = "-1" Then
            ' UTENTE HIS
            FuncaoLimpar
            EcDataNasc = gDUtente.dt_nasc_ute
            EcNome = gDUtente.nome_ute
            EcSexo = gDUtente.sexo_ute
            BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
            EcTipoUtente = gDUtente.t_utente
            EcDescrTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
            FuncaoProcurar (True)     'soliveira Procurar o doente mas deixar inserir se n existir
            EcNome = UCase(gDUtente.nome_ute)
            EcProcHosp1 = gDUtente.n_proc_1
            EcProcHosp2 = gDUtente.n_proc_2
            EcCartaoUte = gDUtente.n_cartao_ute
            EcEstCiv = gDUtente.est_civil
            EcPrescricao = gDUtente.n_prescricao
            EcEmail = gDUtente.email
            EcCodEspecie = gDUtente.cod_genero
            EcCodespecie_Validate False

            BL_ColocaTextoCombo "sl_tbf_estciv", "cod_est_civ", "cod_est_civ", EcEstCiv, EcDescrEstado
            If gHIS_Import = 1 And UCase(HIS.uID) = UCase("GH") And gDUtente.cod_efr = "" Then
                Set RsCodEFR = New ADODB.recordset
                With RsCodEFR
                    If gDUtente.situacao <> "" And gDUtente.situacao <> "-1" And gDUtente.episodio <> "" Then
                        .Source = "SELECT obter_cod_resp('" & gDUtente.t_utente & "','" & gDUtente.Utente & "','" & GH_RetornaDescrEpisodio(gDUtente.situacao) & "','" & gDUtente.episodio & "') resultado FROM dual"
                    Else
                        .Source = "SELECT obter_cod_resp('" & gDUtente.t_utente & "','" & gDUtente.Utente & "','Ficha-ID','" & gDUtente.Utente & "') resultado FROM dual"
                    End If
                    .CursorType = adOpenStatic
                    .CursorLocation = adUseServer
                    If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                    .Open , gConnHIS
                End With
                If RsCodEFR.RecordCount > 0 Then
                    EcCodEntFinR = BL_HandleNull(RsCodEFR!resultado, 0)
                Else
                    EcCodEntFinR = gDUtente.cod_efr
                End If
                RsCodEFR.Close
                Set RsCodEFR = Nothing
            Else
                EcCodEntFinR = gDUtente.cod_efr
            End If
            EcCodEntFinR_Validate False
            'EcCodEntFinR = gDUtente.cod_efr
            EcNrBenef = gDUtente.nr_benef
            EcMorada = gDUtente.morada
            EcCodPostalAux = gDUtente.cod_postal
            PreencheCodigoPostal
            EcCodProfissao = gDUtente.cod_profis
            EcEpisodio = gDUtente.episodio
            EcSituacao = gDUtente.situacao
            EcCodPais = gDUtente.cod_pais
            EcCodpais_Validate False
            EcDoenteProf = gDUtente.num_doe_prof
            EcCodIsencao = gDUtente.cod_isencao_ute
            EcCodIsencao_Change
            EcEmail = gDUtente.email
            EcCodEspecie = gDUtente.cod_genero
            EcCodespecie_Validate False
            'ICIL-930
            EcTelefone.text = gDUtente.telefone
            EcTelemovel.text = gDUtente.telemovel
            EcNrContrib.text = gDUtente.num_contribuinte
            
            
            LaImportado.caption = "Utente importado do " & HIS.nome
            LaImportado.Visible = True
            'FuncaoProcurar (True)     'soliveira Procurar o doente mas deixar inserir se n existir
        Else
            FuncaoLimpar
            EcCodSequencial = CLng(gDUtente.seq_utente)
            FuncaoProcurar
            If gHIS_Import = 1 Then
                If BL_Abre_Conexao_HIS(gConnHIS, gOracle) = 1 And UCase(HIS.uID) = UCase("GH") And EcTipoUtente.text <> "" And EcUtente.text <> "" Then
                    Set RsCodEFR = New ADODB.recordset
                    With RsCodEFR
                        .Source = "SELECT obter_cod_resp('" & EcTipoUtente.text & "','" & EcUtente.text & "','Ficha-ID','" & EcUtente.text & "') resultado FROM dual"
                        .CursorType = adOpenStatic
                        .CursorLocation = adUseServer
                        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                        .Open , gConnHIS
                    End With
                    If RsCodEFR.RecordCount > 0 Then
                        EcCodEntFinR = BL_HandleNull(RsCodEFR!resultado, 0)
                        PreencheDescEntFinanceira
                    End If
                    RsCodEFR.Close
                    Set RsCodEFR = Nothing
                    BL_Fecha_conexao_HIS
                End If
            End If
        End If
        Flg_PesqUtente = False
    End If
    
    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    If gMarcacoesPrevias = 1 Then
        LaMarcacoes.Visible = True
        BtMarcacoes.Visible = True
    Else
        LaMarcacoes.Visible = False
        BtMarcacoes.Visible = False
    End If

    If gUsaFimSemana = 1 Then
        LaRequisFDS.Visible = True
        BtRequisicoesFDS.Visible = True
    Else
        LaRequisFDS.Visible = False
        BtRequisicoesFDS.Visible = False
    End If
    If EcCodSequencial <> "" Then
        PreencheNotas
    End If
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    If gTipoInstituicao = "HOSPITALAR" Then
        If BtPesquisaUtente.Enabled = True Then
            BtPesquisaUtente.SetFocus
        ElseIf BtRequisicoes.Enabled = True Then
            BtRequisicoes.SetFocus
        End If
    End If
    
    
    Exit Sub
Trata_Erro:
    BG_LogFile_Erros "FormIdentificaUtente: EventoActivate - " & Err.Description
    Resume Next

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    If gF_IDENTIF_PESQ = 1 Then
        FormIdentificaUtente.Enabled = True
        Set gFormActivo = FormIdentificaUtente
    ElseIf gF_FILA_ESPERA = mediSim Then
        FormFilaEspera.Enabled = True
        Set gFormActivo = FormFilaEspera
    Else
        Set gFormActivo = MDIFormInicio
    End If
    BL_ToolbarEstadoN 0
    
    'Hoje
    gEpisodioActivo = ""
    gSituacaoActiva = mediComboValorNull
    gReqAssocActiva = ""
    
    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then
            rs.Close
        End If
        Set rs = Nothing
    End If
    
    Set FormIdentificaUtente = Nothing
    
    gF_IDENTIF = 0

End Sub

Sub DefTipoCampos()
        
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_nasc_ute", EcDataNasc, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_inscr", EcDataInscr, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_impr_cartao", EcImprCartao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_validade", EcDtValidade, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_er", EcDtEr, mediTipoData
    CkNaoImprCartao.value = vbGrayed
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    EcTelemovel.Tag = adNumeric
    EcNrContrib.Tag = adNumeric
    EcNrContrib.MaxLength = 20
    LbNomeDono.Visible = False
    EcNomeDono.Visible = False
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoAguas Then
        EcProcHosp1.Visible = False
        LbProcesso(0).Visible = False
        LbProcesso(1).Visible = False
        EcProcHosp2.Visible = False
        'LbCartaoUte.Visible = False
        'EcCartaoUte.Visible = False
        LaActual.Visible = False
        BtActualizar.Visible = False
        LaMarcacoes.Visible = False
        BtMarcacoes.Visible = False
        ' pferreira 2009.07.27
        LbNumExt.Visible = True
        EcNumExt.Visible = True
    End If
    LbRaca.Visible = False
    EcCodRaca.Visible = False
    EcDescrRaca.Visible = False
    BtPesqRapRaca.Visible = False
    If gFeedBackUtente <> mediSim Then
        LbFeedback.Visible = False
        BtFeedback.Visible = False
    End If
    FrameFeedback.Visible = False
    CkInibeSMS.value = vbGrayed
    If gLAB <> "CL" Then
        CkInibeSMS.Visible = False
    End If
    BL_VerificaAnexosUtente Me, mediComboValorNull
    If gLAB = "ICIL" Then
        LbTelefone(0).caption = "Telefone 1"
        LbTelefone(1).caption = "Telefone 2"
    End If
    
End Sub

Sub Funcao_CopiaUte()
    
    If Trim(gDUtente.seq_utente) <> "" Then
        FuncaoLimpar
        EcCodSequencial = CLng(gDUtente.seq_utente)
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

End Sub

Sub PreencheCodigoPostal()
    On Error GoTo TrataErro
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostalAux)
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
            EcCodPostalAux.text = ""
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.text = s1
            EcRuaPostal.text = s2
            EcDescrPostal.text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCodigoPostal: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCodigoPostal"
    Exit Sub
    Resume Next
End Sub

Sub PreencheDescEntFinanceira()
    On Error GoTo TrataErro
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodEntFinR.text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_efr FROM sl_efr WHERE cod_efr=" & Trim(EcCodEntFinR.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade financeira inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodEntFinR.text = ""
            EcDescrEntFinR.text = ""
            
            If (gLAB = cHSMARTA) Then
                EcCodEntFinR.text = "0"
                Call EcCodEntFinR_Validate(True)
            End If
            
            EcCodEntFinR.SetFocus
        Else
            EcDescrEntFinR.text = Tabela!descr_efr
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescEntFinanceira: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescEntFinanceira"
    Exit Sub
    Resume Next
End Sub

Sub PreencheDescProfissao()
    On Error GoTo TrataErro
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodProfissao.text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_profis FROM sl_profis WHERE cod_profis=" & BL_TrataStringParaBD(EcCodProfissao.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Profiss�o inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodProfissao.text = ""
            EcDescrProfissao.text = ""
            
            If (gLAB = cHSMARTA) Then
                Me.EcCodProfissao = "0"
                Call EcCodProfissao_Validate(True)
            End If
            
            EcCodProfissao.SetFocus
        Else
            EcDescrProfissao.text = BL_HandleNull(Tabela!descr_profis, "")
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescProfissao: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescProfissao"
    Exit Sub
    Resume Next
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", EcDescrTipoUtente
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_estciv", "cod_est_civ", "descr_est_civ", EcDescrEstado
    BG_PreencheComboBD_ADO "sl_isencao", "seq_isencao", "descr_isencao", EcDescrIsencao
    BG_PreencheComboBD_ADO "sl_tbf_feedback", "cod_feedback", "descr_feedback", CbFeedback
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    If Trim(BL_DevolveURLWebService("ERESULTS")) <> "" Then
        LaER.Visible = True
        BtCriarUtilER.Visible = True
    Else
        LaER.Visible = False
        BtCriarUtilER.Visible = False
    End If
End Sub

Private Sub BtActualizar_Click()
    
    ActualizaUtente (True)

    
End Sub


Private Sub BtAnexos_Click()
    FormCommonDialog.Show
    FormCommonDialog.EcSeqUtente = EcCodSequencial
    FormCommonDialog.FuncaoProcurar
    Set gFormActivo = FormCommonDialog
End Sub

'RGONCALVES 16.06.2013 ICIL-470
Private Sub BtCancelarSMS_Click()
    EcEnviarSMS.text = ""
    FrEnviarSMS.Visible = False
End Sub

Private Sub BtCartao_Click()
    On Error GoTo TrataErro
    Dim lImprCartoesEltron As Integer
    Dim lImprCartoesCrystal As Integer
    Dim nome As String
    nome = EcNome
    If BL_HandleNull(EcCodEspecie, -1) <> gCodGeneroDefeito Or gCodGeneroDefeito = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "S� � permitido imprimir cart�es para Utentes definidos com genero por defeito.", vbInformation, App.ProductName
    End If
    ' IMPRIME CARTOES DE GRUPOS SANGUE EM IMPRESSORA ELTRON
    lImprCartoesEltron = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_CARTOES_ELTRON")
    lImprCartoesCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_CARTOES_CRYSTAL")
    
    If MsgBox("Deseja imprimir o cart�o de utente ?   ", vbYesNo + vbQuestion, "Sislab") <> vbYes Then
        Exit Sub
    End If
    
    'REIMPRESSAO
    If EcImprCartao <> "" Then
        If IsDate(EcImprCartao) Then
            If MsgBox("Cart�o do Utente j� foi impresso em: " & EcImprCartao & ". Deseja Reimprimir o cart�o de utente ?   ", vbYesNo + vbQuestion, "Sislab") <> vbYes Then
                Exit Sub
            End If
        End If
    End If
    
    If lImprCartoesEltron = mediSim Then
        BL_ImprimeCartaoEltron EcPrinterCartoes.text, nome, EcDataNasc, EcCodSequencial, EcTipoUtente, EcUtente, MBarcode, True, EcNrBenef
    ElseIf lImprCartoesCrystal = mediSim Then
        BL_ImprimeCartaoCrystal EcPrinterCartoes.text, nome, EcDataNasc, EcCodSequencial, EcTipoUtente, EcUtente, MBarcode, True, EcNrBenef
    Else
        BL_ImprimeCartao EcPrinterCartoes.text, nome, EcDataNasc, EcCodSequencial, EcTipoUtente, EcUtente, MBarcode, True, EcNrBenef
    End If
    EcImprCartao = Bg_DaData_ADO
 Exit Sub
TrataErro:
    BG_LogFile_Erros "BtCartao_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtCartao_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtCriarUtilER_Click()
    If EcDtEr.text = "" Then
        If EcEmail.text <> "" And EcCartaoUte.text <> "" And EcCodSequencial.text <> "" Then
            ER_CriacaoUtilizador EcCartaoUte.text, EcEmail.text, EcNome.text, "DOENTE"
            EcDtEr.text = Bg_DaData_ADO
            BD_Update
            BG_Mensagem mediMsgBox, "Utilizador criado com sucesso.", vbInformation, App.ProductName
        Else
            BG_Mensagem mediMsgBox, "Dados obrigat�rios por preencher (Email, Cart�o de UTente).", vbInformation, App.ProductName
        End If
    Else
      'RGONCALVES 29.07.2013 LCC-301
        'BG_Mensagem mediMsgBox, "Utilizador j� gerado em:" & EcDtEr.text & ". Gerar nova password no eResults. ", vbInformation, App.ProductName
        gMsgMsg = "Utilizador j� gerado em: " & EcDtEr.text & "." & vbCrLf & "Deseja gerar nova password (a ser enviada por Email) ? "
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, App.ProductName)
        If gMsgResp = vbYes Then
            ER_ResetPasswordUtilizador EcCartaoUte.text
            EcDtEr.text = Bg_DaData_ADO
            BD_Update
            BG_Mensagem mediMsgBox, "Nova password gerada com sucesso.", vbInformation, App.ProductName
        End If
        '
    End If
End Sub

'RGONCALVES 16.06.2013 ICIL-470
Private Sub BtEnviarSMS_Click()
    
    FrEnviarSMS.Enabled = False
    
    If SMS_NovaMensagem_Avulso(EcTelemovel.text, EcEnviarSMS.text) = True Then
        BG_Mensagem mediMsgBox, "Mensagem enviada.", vbInformation, App.ProductName
    Else
        BG_Mensagem mediMsgBox, "Erro ao enviar mensagem !", vbCritical, App.ProductName
    End If
    FrEnviarSMS.Enabled = True
End Sub

Private Sub BtFeedback_Click()
    FrameFeedback.Visible = True
    FrameFeedback.top = 2400
    FrameFeedback.left = 2880
    
End Sub

Private Sub BtInfClinica_Click()
    
    'for�ar a fazer o procurar da inf. clinica para o utente em causa
    'caso j� exista um
    gDUtente.seq_utente = EcCodSequencial.text
    FormIdentificaUtente.Enabled = False
    FormInformacaoClinica.Show
    If EcUtente.text <> "" Then
        FormInformacaoClinica.FuncaoProcurar
        'BRUNODSANTOS 21.10.2016 - LACTO-336
'        If EcinfComplementar.text = "" Then
'            EcinfComplementar.text = FormInformacaoClinica.EcInfCompl.text
'        Else
            FormInformacaoClinica.EcInfCompl.text = InfoComplementarID
'            InfoComplementarID = EcinfComplementar.text
'        End If
        '
        'FormInformacaoClinica.EcInfCompl = EcinfComplementar.text
        'BRUNODSANTOS 21.10.2016 - LACTO-336
        'FormGestaoRequisicaoPrivado.EcinfComplementar.text = EcinfComplementar.text
        'FormIdentificaUtente.EcinfComplementar = FormInformacaoClinica.EcInfCompl
        '
    End If

End Sub

Private Sub BtMarcacoes_Click()
    
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    
    
    FuncaoModificar (True)
    BL_LimpaDadosUtente
    gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
    FormIdentificaUtente.Enabled = False
    gEpisodioActivo = EcEpisodio
    If EcSituacao <> "" Then
        gSituacaoActiva = EcSituacao
    End If
    FormGesReqCons.ExternalAccess = True
    FormGesReqCons.Show


End Sub

Private Sub BtPesqCodPostal_Click()
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" And EcCodPostal = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    If EcDescrPostal <> "" Then
        ChavesPesq(1) = "descr_postal"
        CamposEcran(1) = "descr_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_postal"
        CamposEcran(2) = "cod_postal"
        Tamanhos(2) = 2000
        Headers(2) = "C�digo"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        ChavesPesq(1) = "cod_postal"
        CamposEcran(1) = "cod_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Codigo"
        
        ChavesPesq(2) = "descr_postal"
        CamposEcran(2) = "descr_postal"
        Tamanhos(2) = 2000
        Headers(2) = "Descr��o"
    End If
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_postal"
    If EcDescrPostal <> "" And EcCodPostal = "" Then
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        CWhere = "UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    Else
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
        CWhere = CWhere & " AND UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    End If
    CampoPesquisa = "descr_postal"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, Resultados(2), "-") - 1
            If i = -1 Then
                i = InStr(1, Resultados(1), "-") - 1
            End If
            If EcDescrPostal <> "" Then
                If i = -1 Then
                    s1 = Resultados(2)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(2), 1, i)
                    s2 = Mid(Resultados(2), InStr(1, Resultados(2), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(2))
                EcDescrPostal.text = Resultados(1)
                EcDescrPostal.SetFocus
            ElseIf EcDescrPostal = "" Then
                If i = -1 Then
                    s1 = Resultados(1)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(1), 1, i)
                    s2 = Mid(Resultados(1), InStr(1, Resultados(1), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(1))
                EcDescrPostal.text = Resultados(2)
                EcDescrPostal.SetFocus
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If

 Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqCodPostal_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqCodPostal_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqEntFinR_Click()
    PA_PesquisaEFR EcCodEntFinR, EcDescrEntFinR, CStr(gCodLocal)
End Sub

Private Sub BtPesqProfissao_Click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_profis", _
                        "descr_profis", "seq_profis", _
                        EcPesqRapProfis
End Sub
Private Sub BtPesqPais_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_pais", _
                        "descr_pais", "cod_pais", _
                        EcCodPais
    EcCodpais_Validate False
End Sub

Private Sub BtPesqRapEspecie_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_genero"
    CamposEcran(1) = "cod_genero"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_genero"
    CamposEcran(2) = "descr_genero"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_genero"
    CampoPesquisa1 = "descr_genero"
    ClausulaWhere = ""
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Esp�cies")
    
    mensagem = "N�o foi encontrada nenhuma Esp�cie."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodEspecie.text = Resultados(1)
            EcDescrEspecie.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqRapEspecie_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqRapEspecie_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqRapraca_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_raca"
    CamposEcran(1) = "cod_raca"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_raca"
    CamposEcran(2) = "descr_raca"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_racas"
    CampoPesquisa1 = "descr_raca"
    ClausulaWhere = ""
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Ra�as")
    
    mensagem = "N�o foi encontrada nenhuma Ra�a."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodRaca.text = Resultados(1)
            EcDescrRaca.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqRapraca_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqRapraca_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtPesquisaRNU_Click()
    Dim identif As identifRNU
    Dim i As Integer
    Dim diferencas As String
    'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
    Dim DescrPais As String
    Dim DescrProfissao As String
    '
    If EcCartaoUte <> "" Then
        If IsNumeric(EcCartaoUte) Then
            BL_MudaCursorRato mediMP_Espera, Me
            DoEvents
            identif = RNU_GetPatientBySNS(EcCartaoUte)
            If identif.identificacao.NomeCompleto <> "" Then
                If EcCodSequencial <> "" Then
                    diferencas = ""
                    If EcNome <> identif.identificacao.NomeCompleto And EcNome <> "" Then
                        diferencas = diferencas & " sexo,"
                    End If
                    If EcDataNasc <> identif.identificacao.DataNascimento And EcDataNasc <> "" Then
                        diferencas = diferencas & " data de nascimento,"
                    End If
                    If EcDescrSexo.ListIndex > mediComboValorNull Then
                        If EcDescrSexo.ItemData(EcDescrSexo.ListIndex) <> identif.identificacao.Sexo Then
                            diferencas = diferencas & " sexo,"
                        End If
                    End If
                     'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
                    If EcCodProfissao.text <> "" Then
                       If EcCodProfissao.text <> DevolveCodTabelaLocal(identif.identificacao.CodProfissao, "SL_PROFIS", "COD_PROFIS", "DESCR_PROFIS", "COD_RNU", DescrProfissao) Then
                           diferencas = diferencas & " profissao,"
                       End If
                    End If
                    '
                    'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
                    If EcCodPais.text <> "" Then
                       If EcCodPais.text <> DevolveCodTabelaLocal(identif.identificacao.codPais, "SL_PAIS", "COD_PAIS", "DESCR_PAIS", "COD_RNU", DescrPais) Then
                           diferencas = diferencas & " pa�s,"
                       End If
                    End If
                    '
                    'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
                    If EcDocIdentif.text <> "" Then
                       If EcDocIdentif.text <> identif.identificacao.NDocIdentif Then
                           diferencas = diferencas & " identifica��o,"
                       End If
                    End If
                    '
                    If diferencas <> "" Then
                        diferencas = Mid(diferencas, 1, Len(diferencas) - 1)
                        gMsgTitulo = "RNU"
                        gMsgMsg = "Existem dados diferentes entre o SISLAB e o RNU (" & diferencas & "). deseja Actualizar?"
                        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                        If gMsgResp <> vbYes Then
                            Exit Sub
                        End If
                    End If
                End If
                EcNome = identif.identificacao.NomeCompleto
                EcDataNasc = identif.identificacao.DataNascimento
                 'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
                EcDocIdentif.text = identif.identificacao.NDocIdentif
                EcCodProfissao.text = DevolveCodTabelaLocal(identif.identificacao.CodProfissao, "SL_PROFIS", "COD_PROFIS", "DESCR_PROFIS", "COD_RNU", DescrProfissao)
                EcDescrProfissao.text = DescrProfissao
                EcCodPais.text = DevolveCodTabelaLocal(identif.identificacao.codPais, "SL_PAIS", "COD_PAIS", "DESCR_PAIS", "COD_RNU", DescrPais)
                EcDescrPais.text = DescrPais
                '
                For i = 0 To EcDescrSexo.ListCount - 1
                    If EcDescrSexo.ItemData(i) = identif.identificacao.Sexo Then
                        EcDescrSexo.ListIndex = i
                        Exit For
                    End If
                Next i
                
                If identif.beneficios.IsencaoTaxa.Motivo <> "" Then
                    EcObs = "ISEN��O: " & identif.beneficios.IsencaoTaxa.descricao & "(" & identif.beneficios.IsencaoTaxa.DataInicio & " at� " & identif.beneficios.IsencaoTaxa.DataFim & ")"
                End If
                
                For i = 0 To EcDescrIsencao.ListCount - 1
                    If identif.beneficios.IsencaoTaxa.Motivo <> "" Then
                        If EcDescrIsencao.ItemData(i) = gTipoIsencaoIsento Then
                            EcDescrIsencao.ListIndex = i
                            Exit For
                        End If
                    Else
                        If EcDescrIsencao.ItemData(i) = gTipoIsencaoNaoIsento Then
                            EcDescrIsencao.ListIndex = i
                            Exit For
                        End If
                    End If
                Next i
            Else
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbInformation, "Procurar"
            End If
        Else
            BG_Mensagem mediMsgBox, "N�mero Inv�lido!", vbInformation, "Procurar"
        End If
    Else
        BG_Mensagem mediMsgBox, "Cart�o de Utente n�o preenchido!", vbInformation, "Procurar"
    End If
    BL_MudaCursorRato mediMP_Activo, Me

End Sub

Private Sub BtPesquisaUtente_Click()
    
    LimpaCampos
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
        
    FormPesquisaUtentes.Show
    Flg_PesqUtente = True

End Sub

Private Sub BtRequisicoes_Click()
    
    If (1 = 1) Then
    'Hoje
        'gEpisodioActivo = ""
    End If
    
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    
    FuncaoModificar (True)
    BL_LimpaDadosUtente
    gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
    FormIdentificaUtente.Enabled = False
    gEpisodioActivo = EcEpisodio
    If EcSituacao <> "" Then
        gSituacaoActiva = EcSituacao
    End If
    If gF_FACTUSENVIO = 1 Then
        FormFactusEnvio.Enabled = True
        FormFactusEnvio.EventoActivate
        Set gFormActivo = FormFactusEnvio
        EF_AlteraUtenteFact CLng(gDUtente.seq_utente), FormFactusEnvio.FgReq.row, True
    ElseIf gTipoInstituicao = gTipoInstituicaoHospitalar Then
        FormGestaoRequisicao.ExternalAccess = True
        FormGestaoRequisicao.Show
    ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
        FormGestaoRequisicaoVet.ExternalAccess = True
        FormGestaoRequisicaoVet.Show
    ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Then
        'edgar.parada Glintt-HS-19165 09.08.2018 * alertar para o preenchimento do SNS
       ' If gAtivaESP = mediSim And EcCartaoUte.text = "" Then
          ' BG_Mensagem mediMsgBox, "Preencha o N� de Cart�o de Utente para avan�ar para a requisi��o!", vbInformation
          ' FormIdentificaUtente.Enabled = True
          ' Exit Sub
      '  Else
           FormGestaoRequisicaoPrivado.ExternalAccess = True
           FormGestaoRequisicaoPrivado.Show
     '   End If
    ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
        FormGestaoRequisicaoAguas.ExternalAccess = True
        FormGestaoRequisicaoAguas.Show
    End If
    If gLAB = "HPD" And BL_GetComputerCodigo = 50 Then
        FormGestaoRequisicao.CbUrgencia.ListIndex = 1
        
    End If
    
End Sub

Private Sub BtRequisicoesFDS_Click()
    If (1 = 1) Then
    'Hoje
        'gEpisodioActivo = ""
    End If
    
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    
    FuncaoModificar (True)
    BL_LimpaDadosUtente
    gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
    FormIdentificaUtente.Enabled = False
    gEpisodioActivo = EcEpisodio
    
    If gTipoInstituicao = gTipoInstituicaoHospitalar Then
        FormGestaoRequisicao.ExternalAccess = True
        FormGestaoRequisicao.Show
    ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoAguas Then
        FormGestaoRequisicaoPrivado.ExternalAccess = True
        FormGestaoRequisicaoPrivado.Show
        FormGestaoRequisicaoPrivado.Req_FDS = True
        FormGestaoRequisicaoPrivado.EcFimSemana = "1"
        FormGestaoRequisicaoPrivado.caption = " Gest�o de Requisi��es - FIM DE SEMANA"
    End If
    
End Sub

Private Sub BtSairFeedback_Click()
    FrameFeedback.Visible = False
End Sub


'RGONCALVES 16.06.2013 ICIL-470
Private Sub BtSMS_Click()
    EcTelemovel.text = Trim(EcTelemovel.text)
    If left(EcTelemovel.text, 2) <> "91" And left(EcTelemovel.text, 2) <> "92" And left(EcTelemovel.text, 2) <> "93" And left(EcTelemovel.text, 2) <> "96" Then
        BG_Mensagem mediMsgBox, "N� de telem�vel inv�lido.", vbInformation, App.ProductName
        Exit Sub
    End If
    FrEnviarSMS.left = 7080
    FrEnviarSMS.top = 3840
    FrEnviarSMS.Visible = True
    EcEnviarSMS.SetFocus
End Sub

'NELSONPSILVA Glintt-HS-18011 STANDALONE
Private Sub CkDirEsquecimento_Click()
    Dim res As String
    
    If CkDirEsquecimento.value <> vbEmpty And EcTipoUtente.text <> "" And EcUtente.text <> "" Then
        gMsgTitulo = "Direito ao Esquecimento"
        gMsgMsg = "Esta a��o n�o � revert�vel!" & vbNewLine & "Deseja esquecer e eliminar permanentemente o processo do doente " & EcTipoUtente.text & "-" & EcUtente.text & " ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            res = BL_RGPD_Stand(EcTipoUtente.text, EcUtente.text)
            If res = "SUCESSO" Then
                BG_Mensagem mediMsgBox, "O utilizador foi esquecido e eliminado com sucesso.", vbInformation, App.ProductName
                BG_StackJanelas_Reset
                Set gFormActivo = MDIFormInicio
            Else
                BG_Mensagem mediMsgBox, res, vbCritical + vbOKOnly, App.ProductName
                CkDirEsquecimento.value = vbEmpty
            End If
         Else
            CkDirEsquecimento.value = vbEmpty
         End If
    End If
End Sub

Private Sub EcCartaoUte_LostFocus()
    EcCartaoUte = UCase(EcCartaoUte)
End Sub

Private Sub EcCodEntFinR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Public Sub EcCodEntFinR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEntFinR, EcDescrEntFinR, "")
        
End Sub

Private Sub EcCodIsencao_Change()
    
    If EcCodIsencao <> "" Then
        BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, EcDescrIsencao
    End If

End Sub

Private Sub EcCodPostal_Change()
    
    EcCodPostal.text = UCase(EcCodPostal.text)

End Sub

Private Sub EcCodPostalCompleto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalCompleto.text <> "" Then
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     seq_postal = " & EcCodPostalCompleto
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcCodPostal.SetFocus
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.text = s1
            EcRuaPostal.text = s2
            EcDescrPostal.text = Trim(rsCodigo!descr_postal)
            EcCodPostalAux.text = Trim(rsCodigo!cod_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcCodProfissao_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodProfissao.text = UCase(EcCodProfissao.text)
    If EcCodProfissao.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_profis FROM sl_profis WHERE cod_profis='" & Trim(EcCodProfissao.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Profiss�o inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodProfissao.text = ""
            EcDescrProfissao.text = ""
        Else
            EcDescrProfissao.text = BL_HandleNull(Tabela!descr_profis)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProfissao.text = ""
    End If
    
End Sub
Private Sub EcCodpais_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodPais.text = UCase(EcCodPais.text)
    If EcCodPais.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_pais FROM sl_pais WHERE cod_pais='" & Trim(EcCodPais.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Pa�s inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodPais.text = ""
            EcDescrPais.text = ""
        Else
            EcDescrPais.text = BL_HandleNull(Tabela!descr_pais)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPais.text = ""
    End If
End Sub

Private Sub EcDataInscr_GotFocus()
    
    If EcDataInscr.text = "" Then
        EcDataInscr.text = Format(Bg_DaData_ADO, gFormatoData)
    End If
    EcDataInscr.SelStart = 0
    EcDataInscr.SelLength = Len(EcDataInscr)

End Sub

Private Sub EcDataInscr_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInscr)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataNasc_Change()
    
    ' PAULO  : Afinar

    EcDataNasc.text = Trim(EcDataNasc.text)

    If (Len(EcDataNasc.text) = 2) Then
        EcDataNasc.text = EcDataNasc.text & "-"
        EcDataNasc.SelStart = Len(EcDataNasc.text)
    End If
    
    If (Len(EcDataNasc.text) = 5) Then
        EcDataNasc.text = EcDataNasc.text & "-"
        EcDataNasc.SelStart = Len(EcDataNasc.text)
    End If
    '
    
    ' FIM PAULO  : Afinar

End Sub

Private Sub EcDataNasc_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = 8) Then
        EcDataNasc.text = ""
    End If
    
'    If ((KeyAscii = Asc("-")) Or _
'        (KeyAscii = Asc("/")) Or _
'        (KeyAscii = Asc(":"))) Then
'    End If

End Sub


Private Sub EcDataNasc_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataNasc)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    PreencheIdade
End Sub

Private Sub EcDescrEstado_Click()
    
    BL_ColocaComboTexto "sl_tbf_estciv", "cod_est_civ", "cod_est_civ", EcEstCiv, EcDescrEstado

End Sub

Private Sub EcDescrEstado_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDescrEstado.ListIndex = -1

End Sub

Private Sub EcDescrIsencao_Click()
    
    BL_ColocaComboTexto "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, EcDescrIsencao

End Sub

Private Sub EcDescrIsencao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDescrIsencao.ListIndex = -1

End Sub

Private Sub EcDescrSexo_Click()
    
    BL_ColocaComboTexto "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo

End Sub

Private Sub EcDescrSexo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDescrSexo.ListIndex = -1

End Sub

Private Sub EcDescrTipoUtente_Click()
    
    BL_ColocaComboTexto "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente
    If EcDescrTipoUtente.ListIndex <> -1 And EcUtente.Enabled Then EcUtente.SetFocus

End Sub

Private Sub EcDescrTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDescrTipoUtente.ListIndex = -1

End Sub








Private Sub EcEmail_Validate(Cancel As Boolean)
    If EcEmail <> "" Then
        If InStr(1, EcEmail, "@") = 0 Then
            BG_Mensagem mediMsgBox, "Email inv�lido.", vbExclamation, "Valida��o de email"
            EcEmail = ""
            EcEmail.SetFocus
        End If
    End If
End Sub


'RGONCALVES 16.06.2013 ICIL-470
Private Sub EcEnviarSMS_Change()
    LaCaracteresFaltaSMS.caption = EcEnviarSMS.MaxLength - Len(EcEnviarSMS.text)
End Sub

Private Sub EcIdade_Validate(Cancel As Boolean)
    PreencheDataNasc
End Sub

Private Sub EcNome_LostFocus()
    
    If gNomeCapitalizado = 1 Then
        EcNome.text = StrConv(EcNome.text, vbProperCase)
    Else
        EcNome.text = UCase(EcNome.text)
    End If
    
    ' pferreira 2010.08.10
    If (DeveSugerirAbreviatura(EcNome.text, EcAbreviatura)) Then: EcAbreviatura.text = SugereAbreviatura(EcNome.text, False)
     
End Sub
Private Sub EcNomedono_LostFocus()
    
    If gNomeCapitalizado = 1 Then
        EcNomeDono.text = StrConv(EcNomeDono.text, vbProperCase)
    Else
        EcNomeDono.text = UCase(EcNomeDono.text)
    End If
    
End Sub
Private Sub EcNrBenef_GotFocus()
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodEntFinR.text <> "" And EcNrBenef.text = "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEntFinR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = Trim(BL_HandleNull(Tabela!Formato1, ""))
            Formato2 = Trim(BL_HandleNull(Tabela!Formato2, ""))
            EcNrBenef.text = Trim(BL_HandleNull(Tabela!sigla, ""))
            'SendKeys ("{END}")
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub

Function ValidaNumBenef() As Boolean

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim Formato As Boolean

    If EcCodEntFinR.text <> "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEntFinR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = BL_HandleNull(Tabela!Formato1, "")
            Formato2 = BL_HandleNull(Tabela!Formato2, "")
            'SendKeys ("{END}")
        End If
        Formato = Verifica_Formato(UCase(EcNrBenef.text), Formato1, Formato2)
        If Formato = False And EcNrBenef.text <> "" Then
            Sendkeys ("{END}")
            EcNrBenef.SetFocus
            ValidaNumBenef = False
        Else
            ValidaNumBenef = True
        End If
    End If

    ValidaNumBenef = True

End Function

Private Sub EcNrBenef_LostFocus()

    Dim Formato As Boolean
    EcNrBenef = UCase(EcNrBenef)
    Formato = Verifica_Formato(UCase(EcNrBenef.text), Formato1, Formato2)
    If Formato = False And EcNrBenef.text <> "" Then
        Sendkeys ("{END}")
        EcNrBenef.SetFocus
    End If

End Sub

Private Sub EcNrContrib_Validate(Cancel As Boolean)
    If EcNrContrib = "" Then Exit Sub
    If EcCodPais.text <> "" Then Exit Sub
    If BL_ValidaNIF(EcNrContrib.text) = False Then
        EcNrContrib.SetFocus
        Cancel = True
        Sendkeys ("{HOME}+{END}")
    End If
End Sub

Private Sub EcPesqRapEFR_Change()
    
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapEFR.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_efr,descr_efr FROM sl_efr WHERE seq_efr =" & EcPesqRapEFR, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da entidade financeira!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodEntFinR.text = Trim(rsCodigo!cod_efr)
            EcDescrEntFinR.text = Trim(rsCodigo!descr_efr)
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcPesqRapProfis_Change()
    
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapProfis.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_profis,descr_profis FROM sl_profis WHERE seq_profis = " & EcPesqRapProfis, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da profiss�o!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodProfissao.text = Trim(rsCodigo!cod_profis)
            EcDescrProfissao.text = Trim(rsCodigo!descr_profis)
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcRuaPostal_Validate(Cancel As Boolean)
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset
    
    EcRuaPostal.text = UCase(EcRuaPostal.text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.text) & "-" & Trim(EcRuaPostal.text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcRuaPostal = ""
            EcCodPostal = ""
        Else
            EcCodPostalAux.text = Postal
            EcDescrPostal.text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.text = ""
        EcCodPostalAux.text = ""
        EcCodPostal.text = ""
    End If
    
End Sub





Private Sub EcTelemovel_Validate(Cancel As Boolean)
    Dim prefixo As String
    If EcTelemovel <> "" Then
        prefixo = Mid(EcTelemovel, 1, 2)
        If prefixo <> "91" And prefixo <> "93" And prefixo <> "96" And prefixo <> "92" Then
            BG_Mensagem mediMsgBox, "N�mero de telem�vel n�o � v�lido! 9xxxxxxxxx", vbExclamation, "Procurar"
            Sendkeys ("{HOME}+{END}")
            Exit Sub
        End If
        If Len(EcTelemovel) <> 9 Then
            BG_Mensagem mediMsgBox, "N�mero de telem�vel n�o � v�lido! 9xxxxxxxxx", vbExclamation, "Procurar"
            Sendkeys ("{HOME}+{END}")
            Exit Sub
        End If
    End If
End Sub

Private Sub EcUtente_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call FuncaoProcurar
    End If

End Sub

Private Sub EcUtente_LostFocus()

    EcUtente.text = UCase(EcUtente.text)
    
End Sub

Private Sub Form_Activate()
    
    EventoActivate
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub Inicializacoes()
    
    Me.caption = " Identifica��o de Utentes"
    Me.left = 500
    Me.top = 50
    Me.Width = 12120
    'Me.Height = 6275 ' Normal
    Me.Height = 9150 '' Campos Extras
    
    NomeTabela = "sl_identif"
    Set CampoDeFocus = BtPesquisaUtente
    
    NumCampos = 46
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_utente"
    CamposBD(1) = "utente"
    CamposBD(2) = "t_utente"
    CamposBD(3) = "n_proc_1"
    CamposBD(4) = "n_proc_2"
    CamposBD(5) = "n_cartao_ute"
    CamposBD(6) = "dt_inscr"
    CamposBD(7) = "nome_ute"
    CamposBD(8) = "dt_nasc_ute"
    CamposBD(9) = "sexo_ute"
    CamposBD(10) = "est_civ_ute"
    CamposBD(11) = "descr_mor_ute"
    CamposBD(12) = "cod_postal_ute"
    CamposBD(13) = "telef_ute"
    CamposBD(14) = "cod_profis_ute"
    CamposBD(15) = "cod_efr_ute"
    CamposBD(16) = "n_benef_ute"
    CamposBD(17) = "cod_isencao_ute"
    CamposBD(18) = "obs"
    CamposBD(19) = "user_cri"
    CamposBD(20) = "dt_cri"
    CamposBD(21) = "user_act"
    CamposBD(22) = "dt_act"
    CamposBD(23) = "n_epis"
    CamposBD(24) = "t_sit"
    CamposBD(25) = "telemovel"
    CamposBD(26) = "email"
    CamposBD(27) = "n_contrib_ute"
    CamposBD(28) = "cod_genero"
    CamposBD(29) = "dt_impr_cartao"
    CamposBD(30) = "num_ext"
    CamposBD(31) = "dt_validade"
    CamposBD(32) = "num_fax"
    CamposBD(33) = "nao_impr_cartao"
    ' pferreira 2010.08.10
    CamposBD(34) = "abrev_ute"
    CamposBD(35) = "n_prescricao"
    CamposBD(36) = "cod_pais"
    CamposBD(37) = "num_doe_prof"
    CamposBD(38) = "nome_dono"
    CamposBD(39) = "cod_raca"
    CamposBD(40) = "cod_feedback"
    CamposBD(41) = "flg_inibe_sms"
    CamposBD(42) = "flg_diag_pri"
    CamposBD(43) = "flg_anexo"
    CamposBD(44) = "dt_er"
    CamposBD(45) = "doc_identif"
    
    ' Campos do Ecr�
    
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcUtente
    Set CamposEc(2) = EcTipoUtente
    Set CamposEc(3) = EcProcHosp1
    Set CamposEc(4) = EcProcHosp2
    Set CamposEc(5) = EcCartaoUte
    Set CamposEc(6) = EcDataInscr
    Set CamposEc(7) = EcNome
    Set CamposEc(8) = EcDataNasc
    Set CamposEc(9) = EcSexo
    Set CamposEc(10) = EcEstCiv
    Set CamposEc(11) = EcMorada
    Set CamposEc(12) = EcCodPostalAux
    Set CamposEc(13) = EcTelefone
    Set CamposEc(14) = EcCodProfissao
    Set CamposEc(15) = EcCodEntFinR
    Set CamposEc(16) = EcNrBenef
    Set CamposEc(17) = EcCodIsencao
    Set CamposEc(18) = EcObs
    Set CamposEc(19) = EcUtilizadorCriacao
    Set CamposEc(20) = EcDataCriacao
    Set CamposEc(21) = EcUtilizadorAlteracao
    Set CamposEc(22) = EcDataAlteracao
    Set CamposEc(23) = EcEpisodio
    Set CamposEc(24) = EcSituacao
    Set CamposEc(25) = EcTelemovel
    Set CamposEc(26) = EcEmail
    Set CamposEc(27) = EcNrContrib
    Set CamposEc(28) = EcCodEspecie
    Set CamposEc(29) = EcImprCartao
    Set CamposEc(30) = EcNumExt
    Set CamposEc(31) = EcDtValidade
    Set CamposEc(32) = EcFax
    Set CamposEc(33) = CkNaoImprCartao
    ' pferreira 2010.08.10
    Set CamposEc(34) = EcAbreviatura
    Set CamposEc(35) = EcPrescricao
    Set CamposEc(36) = EcCodPais
    Set CamposEc(37) = EcDoenteProf
    Set CamposEc(38) = EcNomeDono
    Set CamposEc(39) = EcCodRaca
    Set CamposEc(40) = CbFeedback
    Set CamposEc(41) = CkInibeSMS
    Set CamposEc(42) = EcFlgDiagPrim
    Set CamposEc(43) = EcFlgAnexo
    Set CamposEc(44) = EcDtEr
    Set CamposEc(45) = EcDocIdentif
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(7) = "Nome do utente"
    TextoCamposObrigatorios(8) = "Data de nascimento do utente"
    TextoCamposObrigatorios(9) = "Sexo do utente"
    TextoCamposObrigatorios(15) = "Entidade financeira"
    TextoCamposObrigatorios(28) = "Esp�cie"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_utente"
    Set ChaveEc = EcUtente
    
    Formato1 = ""
    Formato2 = ""
    
    If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo") <> "-1" Then
        BtCartao.Visible = True
        LaCartao.Visible = True
    End If
    
    BtInfClinica.Enabled = False
    LaInfCli.Enabled = False
    BtActualizar.Enabled = False
    LaActual.Enabled = False
    BtRequisicoes.Enabled = False
    
    BtMarcacoes.Enabled = False
    LaRequis.Enabled = False
    LaMarcacoes.Enabled = False
    BtCartao.Enabled = False
    LaCartao.Enabled = False
    LaRequisFDS.Enabled = False
    BtRequisicoesFDS.Enabled = False
    
    EcPrinterCartoes = BL_SelImpressora("Cartoes.rpt")
    
    'RGONCALVES 16.06.2013 ICIL-470
    BtSMS.Enabled = False
    LaEnviarSMS.Enabled = False
    If gEnvioResultadosSMSActivo <> mediSim Then
        BtSMS.Visible = False
        LaEnviarSMS.Visible = False
    End If
        '
    
    'NELSONPSILVA 11.07.2018 - Glintt-Hs-18011
    UserPermissaoRGPD = IIf(BL_SelCodigo("sl_idutilizador", "acesso_rgpd", "cod_utilizador", gCodUtilizador, "N") = "", 0, _
                            BL_SelCodigo("sl_idutilizador", "acesso_rgpd", "cod_utilizador", gCodUtilizador, "N"))
    '
    CkDirEsquecimento.Visible = False
    'NELSONPSILVA Glintt-HS-18011 STANDALONE
    If UserPermissaoRGPD = 1 Then
        If UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
            CkDirEsquecimento.Visible = True
            CkDirEsquecimento.Enabled = False
        Else
            CkDirEsquecimento.Visible = False
        End If
    End If
    '
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Funcao_DataActual()
    
    If Me.ActiveControl.Name = EcDataInscr.Name Then
        BL_PreencheData EcDataInscr, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = EcDtValidade.Name Then
        BL_PreencheData EcDtValidade, Me.ActiveControl
    Else
        If Me.ActiveControl.Name = EcDataNasc.Name Then
            BL_PreencheData EcDataNasc, Me.ActiveControl
        End If
    End If
    
End Sub

Function Verifica_Formato(NumBenef As String, Formato1 As String, Formato2 As String) As Boolean
    
    If Trim(Formato2) = "" And Trim(Formato1) = "" Then
        Verifica_Formato = True
        Exit Function
    End If
    
    If Trim(Formato1) = "" Then
        Verifica_Formato = False
    End If
    If Len(NumBenef) <> Len(Formato1) Then
        Verifica_Formato = False
    Else
        Verifica_Formato = Percorre_Formato(NumBenef, Formato1)
    End If
    
    'caso o formato 1 n�o esteja correcto vai verificar o formato 2
    If Verifica_Formato = False Then
        If Trim(Formato2) = "" Then
            Verifica_Formato = False
        End If
        If Len(NumBenef) <> Len(Formato2) Then
            Verifica_Formato = False
        Else
            Verifica_Formato = Percorre_Formato(NumBenef, Formato2)
        End If
        If Verifica_Formato = False And EcNrBenef.text <> "" Then
            gMsgTitulo = "Aten��o"
            gMsgMsg = "N�mero Benefici�rio incorrecto." & Chr(13) & "Deseja continuar ?" & Chr(13) & Chr(13) & "Exemplo de formato(s) correcto(s): " & Chr(13) & "       " & Formato1 & IIf(Trim(Formato1) <> "" And Trim(Formato2) <> "", ", ", "") & Formato2
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbExclamation, gMsgTitulo)
            If gMsgResp = vbYes Then
                Verifica_Formato = True
            Else
                Verifica_Formato = False
            End If
        End If
    End If

End Function

Function Percorre_Formato(NumBenef As String, Formato As String) As Boolean
    
    Dim pos As Integer
    Dim comp As Integer
    Dim CharActualF As String
    Dim CharActualN As String
    
    Percorre_Formato = True
    comp = Len(Formato)
    pos = 1
    Do While pos <= comp
        CharActualF = Mid(Formato, pos, 1)
        CharActualN = Mid(NumBenef, pos, 1)
        Select Case CharActualF
            Case "A"
                If CharActualN < "A" Or CharActualN > "Z" Then
                    Percorre_Formato = False
                End If
            Case "9"
                If CharActualN < "0" Or CharActualN > "9" Then
                    Percorre_Formato = False
                End If
            Case " "
                If CharActualN <> " " Then
                    Percorre_Formato = False
                End If
        End Select
        pos = pos + 1
    Loop

End Function

Sub GravaEpisodio(SeqUtente As Long, episodio As String, situacao As Integer)

    'Actualiza episodio e situa��o para um utente (SeqUtente)

    Dim rsUte As ADODB.recordset
    Dim sql As String
    
    On Error GoTo TrataErro
    
    If Trim(episodio) <> "" Or situacao <> -1 Then
        sql = "UPDATE sl_identif SET n_epis=" & BL_TrataStringParaBD(episodio) & _
            ", t_sit=" & situacao & " WHERE seq_utente=" & SeqUtente
        BG_ExecutaQuery_ADO sql
    End If
    
    Exit Sub
    
TrataErro:

    BG_LogFile_Erros "Erro ao actualizar episodio - " & Err.Description
    
End Sub

Sub ActualizaUtente(mostra_msg As Boolean)
    On Error GoTo TrataErro
    Dim rsHIS As ADODB.recordset
    Dim HisAberto  As Integer
    Dim sSql As String
    Dim RsCodEFR As ADODB.recordset
    Dim RetPesquisaSONHO As Integer
    
    If mostra_msg = True Then
        If BG_Mensagem(mediMsgBox, "Confirma que deseja actualizar os dados do utente " & EcTipoUtente.text & "/" & EcUtente.text & " ?", vbQuestion + vbYesNo, "Actualizar Utente") = vbNo Then Exit Sub
    End If
    
    BL_InicioProcessamento Me, "A pesquisar " & HIS.nome
    
    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    
    sSql = "SELECT * FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(EcTipoUtente.text) & _
                    " AND utente = " & BL_TrataStringParaBD(EcUtente.text)
    If HisAberto = 1 Then
    
        If UCase(HIS.nome) = UCase("SONHO") Then
            RetPesquisaSONHO = SONHO_PesquisaUtente(EcUtente.text)
        End If
        If UCase(HIS.nome) = "NOVAHIS" Then
            sSql = sSql & " AND n_prescricao = " & EcPrescricao
        End If
        Set rsHIS = New ADODB.recordset
    
        With rsHIS
            .Source = sSql
            .CursorType = adOpenStatic
            .CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            .Open , gConnHIS
        End With
        
        If rsHIS.RecordCount <= 0 Then
            BL_FimProcessamento Me
            BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo do " & HIS.nome & "!", vbExclamation, "Procurar"
        Else
            BL_PreencheDadosUtente BL_HandleNull(rsHIS!seq_utente, "-1"), BL_HandleNull(rsHIS!Utente, ""), _
                BL_HandleNull(rsHIS!t_utente, ""), BL_HandleNull(rsHIS!n_proc_1, ""), BL_HandleNull(rsHIS!n_proc_2, ""), _
                BL_HandleNull(rsHIS!n_cartao_ute, ""), BL_HandleNull(rsHIS!dt_inscr, ""), BL_HandleNull(rsHIS!nome_ute, ""), _
                BL_HandleNull(rsHIS!dt_nasc_ute, ""), BL_HandleNull(rsHIS!sexo_ute, ""), "", BL_HandleNull(rsHIS!telef_ute, ""), _
                BL_HandleNull(rsHIS!est_civ_ute, ""), "", BL_HandleNull(rsHIS!cod_efr_ute, ""), BL_HandleNull(rsHIS!n_benef_ute, ""), _
                BL_HandleNull(rsHIS!descr_mor_ute, ""), BL_HandleNull(rsHIS!cod_postal_ute, ""), _
                BL_HandleNull(rsHIS!cod_profis_ute, ""), , , , , , BL_HandleNull(rsHIS!email, "")
            
            EcDataNasc = gDUtente.dt_nasc_ute
            EcCartaoUte = gDUtente.n_cartao_ute
            EcProcHosp1 = gDUtente.n_proc_1
            EcProcHosp2 = gDUtente.n_proc_2
            EcNome = gDUtente.nome_ute
            EcSexo = gDUtente.sexo_ute
            BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
            EcTipoUtente = gDUtente.t_utente
            EcDescrTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
            EcEstCiv = gDUtente.est_civil
            BL_ColocaTextoCombo "sl_tbf_estciv", "cod_est_civ", "cod_est_civ", EcEstCiv, EcDescrEstado
            If gHIS_Import = 1 And UCase(HIS.uID) = UCase("GH") Then
                Set RsCodEFR = New ADODB.recordset
                With RsCodEFR
                    .Source = "SELECT obter_cod_resp('" & gDUtente.t_utente & "','" & gDUtente.Utente & "','Ficha-ID','" & gDUtente.Utente & "') resultado FROM dual"
                    .CursorType = adOpenStatic
                    .CursorLocation = adUseServer
                    .Open , gConnHIS
                End With
                If RsCodEFR.RecordCount > 0 Then
                    EcCodEntFinR = BL_HandleNull(RsCodEFR!resultado, 0)
                Else
                    EcCodEntFinR = gDUtente.cod_efr
                End If
            ElseIf UCase(HIS.nome) = "NOVAHIS" Then
                EcCodEntFinR = IN_DevolveCodigo("sl_efr", "sl_ass_efr_locais", "cod_efr", gDUtente.cod_efr)
            Else
            
                EcCodEntFinR = gDUtente.cod_efr
            End If
            EcNrBenef = gDUtente.nr_benef
            EcMorada = gDUtente.morada
            EcCodPostalAux = gDUtente.cod_postal
            PreencheCodigoPostal
            EcTelefone = gDUtente.telefone
            EcCodProfissao = gDUtente.cod_profis
            EcCodPais = gDUtente.cod_pais
            EcCodpais_Validate False
            EcDoenteProf = gDUtente.num_doe_prof
            EcEmail.text = gDUtente.email
            
            LaImportado.caption = "Utente actualizado com dados do " & HIS.nome
            LaImportado.Visible = True
            BL_FimProcessamento Me
            
            ' Apaga o registo da tabela tempor�ria do SONHO.
            If UCase(HIS.nome) = UCase("SONHO") Then
                 Dim rsAux As ADODB.recordset
                 Set rsAux = New ADODB.recordset
                 
                 sSql = "DELETE FROM sl_identif " & _
                       "WHERE cod_apl = " & gNumeroSessao
                
                 With rsAux
                     .Source = sSql
                     .CursorType = adOpenForwardOnly
                     .CursorLocation = adUseServer
                     If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                     .Open , gConnHIS
                 End With
                 
                 Set rsAux = Nothing
            End If
            FuncaoModificar (True)
        End If
        rsHIS.Close
        Set rsHIS = Nothing
        BL_Fecha_conexao_HIS
    Else
        BL_FimProcessamento Me
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "ActualizaUtente: " & Err.Number & " - " & Err.Description, Me.Name, "ActualizaUtente"
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheIdade()
    On Error GoTo TrataErro
    If EcDataNasc <> "" And IsDate(EcDataNasc) Then
         EcIdade = BG_CalculaIdade(CDate(EcDataNasc.text))
         EcIdade = Mid(EcIdade, 1, InStr(1, EcIdade, " "))
    Else
        EcIdade = ""
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDataNasc: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDataNasc"
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheDataNasc()
    If EcIdade <> "" And EcDataNasc.text = "" Then
        If IsNumeric(EcIdade) Then
            EcDataNasc = "01-01-" & (Mid(Bg_DaData_ADO, 7) - EcIdade)
        Else
            EcDataNasc = ""
        End If
    End If
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDataNasc: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDataNasc"
    Exit Sub
    Resume Next
End Sub

' pferreira 2010.08.10
' Sugere abreviatura do nome do utente.
Private Function SugereAbreviatura(nome As String, Optional abrevia_todos As Boolean) As String

    Dim i As Integer
    Dim inicio  As Integer
    Dim nomes() As String
    Dim retorno As String
    On Error GoTo TrataErro
    nomes = Split(nome, " ")
    If (Not abrevia_todos) Then: inicio = 1

    For i = 0 To UBound(nomes)
        If (Not WordsException(nomes(i)) And i >= inicio And i < UBound(nomes)) Then
            retorno = retorno & Mid$(Trim$(nomes(i)), 1, 1) + ". "
        ElseIf (Not WordsException(nomes(i))) Then
            retorno = retorno & Trim$(nomes(i)) + " "
        End If
    Next

    SugereAbreviatura = Trim$(retorno)
 Exit Function
TrataErro:
    BG_LogFile_Erros "SugereAbreviatura: " & Err.Number & " - " & Err.Description, Me.Name, "SugereAbreviatura"
    Exit Function
    Resume Next
End Function

' pferreira 2010.08.10
' Test words exception in the string.
Public Function WordsException(�sString� As String) As Boolean
    On Error GoTo TrataErro
    �sString� = UCase$(Space(1) & Trim$(�sString�) & Space(1))
    WordsException = TestRegularExpression("( DA $| DE $| DO $| DAS $| DOS $)", �sString�)
Exit Function
TrataErro:
    BG_LogFile_Erros "DeveSugerirAbreviatura: " & Err.Number & " - " & Err.Description, Me.Name, "DeveSugerirAbreviatura"
    Exit Function
    Resume Next
End Function

' pferreira 2010.08.10
' Test a regular expression string.
Private Function TestRegularExpression(�sPattern� As String, �sString� As String) As Boolean
   
    Dim objRegExp As RegExp
    Dim objMatch As Match
    Dim colMatches As MatchCollection
    Dim sResult As String
   
    On Error GoTo ErrorHandler
    Set objRegExp = New RegExp
    objRegExp.Pattern = �sPattern�
    objRegExp.IgnoreCase = True
    objRegExp.Global = True
    TestRegularExpression = objRegExp.Test(�sString�): Exit Function
    Set colMatches = objRegExp.Execute(�sString�)
    For Each objMatch In colMatches
        sResult = sResult & "Match found at position "
        sResult = sResult & objMatch.FirstIndex & ". Match Value is '"
        sResult = sResult & objMatch.value & "'." & vbCrLf
    Next
    TestRegularExpression = True
    Exit Function
      
ErrorHandler:
    BG_LogFile_Erros "Error in function 'TestRegularExpression' in FormIdentificaUtente (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' pferreira 2010.08.10
Private Function DeveSugerirAbreviatura(nome As String, Abreviatura As String) As Boolean
    
    Dim nomes_abreviatura() As String
    Dim nomes_nome() As String
    On Error GoTo TrataErro
    If (nome = Empty) Then: DeveSugerirAbreviatura = False: Exit Function
    If (Abreviatura = Empty) Then: DeveSugerirAbreviatura = True: Exit Function
    nomes_abreviatura = Split(Abreviatura, " ")
    nomes_nome = Split(nome, " ")
    If (UBound(nomes_abreviatura) = Empty Or UBound(nomes_nome) = Empty) Then: DeveSugerirAbreviatura = True: Exit Function
    If (nomes_abreviatura(0) = nomes_nome(0) And nomes_abreviatura(UBound(nomes_abreviatura)) = nomes_nome(UBound(nomes_nome))) Then: DeveSugerirAbreviatura = False: Exit Function
    DeveSugerirAbreviatura = True
Exit Function
TrataErro:
    BG_LogFile_Erros "DeveSugerirAbreviatura: " & Err.Number & " - " & Err.Description, Me.Name, "DeveSugerirAbreviatura"
    Exit Function
    Resume Next
End Function

Private Sub BtNotas_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicao.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcSeqUtente = EcCodSequencial
    FormNotasReq.grupoNota = gGrupoNotasUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotas_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotas_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotasVRM_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicao.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcSeqUtente = EcCodSequencial
    FormNotasReq.grupoNota = gGrupoNotasUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotasVRM_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotasVRM_Click"
    Exit Sub
    Resume Next
End Sub
    
' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset
    
    If EcCodSequencial = "" Then Exit Sub
    sSql = "SELECT count(*) total FROM sl_obs_ana_req WHERE seq_utente = " & EcCodSequencial & " AND cod_gr_nota = " & gGrupoNotasUtente

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True
            
        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotas: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheNotas"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodespecie_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodEspecie.text = UCase(EcCodEspecie.text)
    If EcCodEspecie.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_genero FROM sl_genero WHERE cod_genero=" & BL_TrataStringParaBD(Trim(EcCodEspecie.text))
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Esp�cie inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodEspecie.text = ""
            EcDescrEspecie.text = ""
        Else
            EcDescrEspecie.text = BL_HandleNull(Tabela!descr_genero)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrEspecie.text = ""
    End If
End Sub
Private Sub EcCodraca_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodRaca.text = UCase(EcCodRaca.text)
    If EcCodRaca.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_raca FROM sl_racas WHERE cod_raca=" & EcCodRaca.text
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Ra�a inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodRaca.text = ""
            EcDescrRaca.text = ""
        Else
            EcDescrRaca.text = BL_HandleNull(Tabela!descr_raca)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrRaca.text = ""
    End If
End Sub

'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
Private Function DevolveCodTabelaLocal(ByVal codRNU As String, ByVal sTabela As String, ByVal CFrom As String, ByVal cFROM2 As String, CWhere As String, ByRef Descr As String) As String
    Dim rs As ADODB.recordset
    Dim sSql As String
    On Error GoTo TrataErro
    
    DevolveCodTabelaLocal = ""
    
    sSql = "SELECT " & CFrom & "," & cFROM2 & " FROM " & sTabela & " WHERE " & CWhere & " = " & BL_TrataStringParaBD(codRNU)
    
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    
    If rs.RecordCount > 0 Then
        Descr = BL_HandleNull(rs.Fields(1).value, "")
        DevolveCodTabelaLocal = BL_HandleNull(rs.Fields(0).value, "")
    End If
    
    rs.Close
    Set rs = Nothing
    
    Exit Function
    
TrataErro:
      BG_LogFile_Erros "DevolveCodTabelaLocal: " & Err.Number & " - " & Err.Description, Me.Name, "DevolveCodTabelaLocal"
    Exit Function
    Resume Next
End Function


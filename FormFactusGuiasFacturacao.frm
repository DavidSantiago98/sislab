VERSION 5.00
Begin VB.Form FormFactusGuiasFacturacao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactusGuiasFacturacao"
   ClientHeight    =   4365
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8205
   Icon            =   "FormFactusGuiasFacturacao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4365
   ScaleWidth      =   8205
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame4 
      Caption         =   "Requisi��o"
      Height          =   735
      Left            =   2400
      TabIndex        =   11
      Top             =   1200
      Width           =   5055
      Begin VB.TextBox EcRequis 
         Height          =   285
         Left            =   1680
         TabIndex        =   12
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "N� de Requisi��o"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   1575
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Guias de Facturas"
      Height          =   4095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7935
      Begin VB.Frame Frame3 
         Caption         =   "Tipo de agrupamento"
         Height          =   735
         Left            =   2280
         TabIndex        =   8
         Top             =   1920
         Width           =   5055
         Begin VB.OptionButton OptResumo 
            Caption         =   "Resumo"
            Height          =   255
            Left            =   240
            TabIndex        =   10
            Top             =   360
            Width           =   1335
         End
         Begin VB.OptionButton OptTratamento 
            Caption         =   "Tratamento"
            Height          =   255
            Left            =   1680
            TabIndex        =   9
            Top             =   360
            Width           =   1335
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Factura"
         Height          =   735
         Left            =   2280
         TabIndex        =   3
         Top             =   240
         Width           =   5055
         Begin VB.TextBox EcNFac 
            Height          =   285
            Left            =   3120
            TabIndex        =   5
            Top             =   360
            Width           =   975
         End
         Begin VB.TextBox EcSerieFac 
            Height          =   285
            Left            =   720
            TabIndex        =   4
            Top             =   360
            Width           =   1455
         End
         Begin VB.Label Label2 
            Caption         =   "N�mero"
            Height          =   255
            Left            =   2400
            TabIndex        =   7
            Top             =   360
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "S�rie"
            Height          =   255
            Left            =   240
            TabIndex        =   6
            Top             =   360
            Width           =   615
         End
      End
      Begin VB.TextBox EcNGuia 
         Height          =   285
         Left            =   960
         TabIndex        =   1
         Top             =   600
         Width           =   975
      End
      Begin VB.Label LbDescricao 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   240
         TabIndex        =   14
         Top             =   3120
         Width           =   7455
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label6 
         Caption         =   "Descri��o da Factura"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   2880
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "Guia N�"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   615
      End
   End
End
Attribute VB_Name = "FormFactusGuiasFacturacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'rcoelho 1.08.2013 ljmanso-115
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim CriterioBase As String 'usado para os botoes de ordenacao
Dim CriterioAnterior As String 'usado para os botoes de ordenacao

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object


Private Sub EcSerieFac_LostFocus()
    EcSerieFac.Text = UCase(EcSerieFac.Text)
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Guias de Factura��o"
    Me.left = 500
    Me.top = 400
    'Me.Width = 8295
    'Me.Height = 4320 ' Normal
    
    EcNGuia.Text = ""
    EcSerieFac.Text = ""
    EcNFac.Text = ""
    EcRequis.Text = ""
    OptResumo.value = False
    OptTratamento.value = False
    LbDescricao.caption = ""
    EcNGuia.Enabled = True
    EcSerieFac.Enabled = True
    EcNFac.Enabled = True
    
    Set CampoDeFocus = EcSerieFac
    
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
        
    Set FormFactusGuiasFacturacao = Nothing
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    EcNGuia.Text = ""
    EcSerieFac.Text = ""
    EcNFac.Text = ""
    EcRequis.Text = ""
    OptResumo.value = False
    OptTratamento.value = False
    LbDescricao.caption = ""
    EcNGuia.Enabled = True
    EcSerieFac.Enabled = True
    EcNFac.Enabled = True
    
    CampoDeFocus.SetFocus

End Sub

Sub DefTipoCampos()
    
    EcNGuia.Tag = adNumeric
    EcSerieFac.Tag = adVarChar
    EcNFac.Tag = adNumeric
    EcRequis.Tag = adNumeric
    
End Sub

Sub PreencheCampos()
    'nada
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    'nada
End Sub

Sub FuncaoProcurar()
    'nada
End Sub

Function Pesquisar() As Boolean
    Dim sql As String
    Dim RsFact As ADODB.recordset
    Dim rsLinFact As ADODB.recordset
    Dim RsNGuia As ADODB.recordset
    Dim OrdemImp As Integer
    Dim rsRec As New ADODB.recordset
    Dim serieRec As String
    Dim Recibo As Long
    
    Pesquisar = False
    
    Set RsFact = New ADODB.recordset
    RsFact.CursorLocation = adUseServer
    RsFact.CursorType = adOpenStatic
    
    If EcNGuia.Text <> "" Then
        sql = "SELECT DISTINCT serie_fac, n_fac, ano, mes, descr_efr FROM sl_guias_facturacao WHERE n_guia = " & EcNGuia.Text
        RsFact.Open sql, gConexao
        If RsFact.RecordCount > 0 Then
            EcSerieFac.Text = BL_HandleNull(RsFact!serie_fac, "")
            EcNFac.Text = BL_HandleNull(RsFact!n_fac, 0)
            LbDescricao.caption = BL_HandleNull(RsFact!descr_efr, "") & vbCrLf & "Ano " & BL_HandleNull(RsFact!ano, 0) & "   Mes " & BL_HandleNull(RsFact!mes, 0)
            EcSerieFac.Enabled = False
            EcNFac.Enabled = False
            Pesquisar = True
        Else
            BG_Mensagem mediMsgBox, "N�o foram seleccionados registos", vbInformation, App.ProductName
        End If
        RsFact.Close
        Set RsFact = Nothing
        Exit Function
    End If
    
    If EcRequis.Text <> "" Then
        sql = "SELECT DISTINCT serie_fac, n_fac FROM fa_lin_fact WHERE episodio = " & BL_TrataStringParaBD(EcRequis.Text)
        RsFact.Open sql, gConexao
        If RsFact.RecordCount > 0 Then
            EcSerieFac.Text = BL_HandleNull(RsFact!serie_fac, "")
            EcNFac.Text = BL_HandleNull(RsFact!n_fac, 0)
        Else
            BG_Mensagem mediMsgBox, "N�o foram seleccionados registos", vbInformation, App.ProductName
            RsFact.Close
            Set RsFact = Nothing
            LimpaCampos
            Exit Function
        End If
        RsFact.Close
    End If
    
    sql = "SELECT ano, mes, descr_efr FROM fa_fact WHERE serie_fac = " & BL_TrataStringParaBD(EcSerieFac.Text) & _
            " AND n_fac = " & EcNFac.Text & " AND flg_estado <> 'C'"
    RsFact.Open sql, gConexao
    If RsFact.RecordCount > 0 Then
        LbDescricao.caption = BL_HandleNull(RsFact!descr_efr, "") & vbCrLf & "Ano " & BL_HandleNull(RsFact!ano, 0) & "   Mes " & BL_HandleNull(RsFact!mes, 0)
        
        Set rsLinFact = New ADODB.recordset
        rsLinFact.CursorLocation = adUseServer
        rsLinFact.CursorType = adOpenStatic
        
        sql = "SELECT lf.doente, lf.serie_fac, lf.n_fac, lf.nome_doente, lf.n_benef_doe, lf.episodio, lf.dt_ini_real, lf.cod_rubr_efr," & _
                " lf.descr_rubr, lf.val_pr_unit, lf.val_taxa, lf.qtd, lf.perc_desc_lin, lf.val_total, lf.cod_isen_doe, lf.val_pag_doe," & _
                " lf.sld_qtd_cred, lf.sld_val_cred, mf.serie_fac_doe, mf.n_fac_doe" & _
                " FROM fa_lin_fact lf, fa_movi_fact mf" & _
                " WHERE lf.n_seq_prog = mf.n_seq_prog" & _
                " AND lf.serie_fac = " & BL_TrataStringParaBD(EcSerieFac.Text) & _
                " AND lf.n_fac = " & EcNFac.Text
        If EcRequis.Text <> "" Then
            sql = sql & " AND lf.episodio = " & BL_TrataStringParaBD(EcRequis.Text)
        End If
        sql = sql & " ORDER BY lf.n_lin_fac, lf.n_ord_ins"
        rsLinFact.Open sql, gConexao
        If rsLinFact.RecordCount > 0 Then
            Set RsNGuia = New ADODB.recordset
            RsNGuia.CursorLocation = adUseServer
            RsNGuia.CursorType = adOpenStatic
            
            sql = "SELECT seq_n_guia_facturacao.nextval SeqNGuia FROM dual"
            RsNGuia.Open sql, gConexao
            If RsNGuia.RecordCount > 0 Then
                EcNGuia.Text = BL_HandleNull(RsNGuia!SeqNGuia, 0)
                EcNGuia.Enabled = False
            Else
                BG_Mensagem mediMsgBox, "Erro ao gerar sequencia do n� da guia", vbError, App.ProductName
                RsNGuia.Close
                Set RsNGuia = Nothing
                LimpaCampos
                Exit Function
            End If
            RsNGuia.Close
            Set RsNGuia = Nothing
            
            OrdemImp = 0
            While Not rsLinFact.EOF
                sql = "SELECT serie_rec, n_rec FROM sl_recibos_det WHERE n_req = " & BL_HandleNull(rsLinFact!episodio, "")
                sql = sql & " AND cod_ana_efr = " & BL_TrataStringParaBD(BL_HandleNull(rsLinFact!cod_rubr_efr, "")) & " AND flg_facturado = 1 "
                rsRec.CursorLocation = adUseServer
                rsRec.CursorType = adOpenStatic
                rsRec.Open sql, gConexao
                If rsRec.RecordCount > 0 Then
                    Recibo = BL_HandleNull(rsRec!n_rec, 0)
                    serieRec = BL_HandleNull(rsRec!serie_rec, "")
                Else
                    Recibo = 0
                    serieRec = ""
                End If
                rsRec.Close
                Set rsRec = Nothing
                
                
                OrdemImp = OrdemImp + 1
                PreencheEstutDados OrdemImp, BL_HandleNull(rsLinFact!serie_fac, ""), BL_HandleNull(rsLinFact!n_fac, 0), _
                    BL_HandleNull(RsFact!ano, 0), BL_HandleNull(RsFact!mes, 0), BL_HandleNull(RsFact!descr_efr, ""), _
                    BL_HandleNull(rsLinFact!doente, ""), BL_HandleNull(rsLinFact!nome_doente, ""), BL_HandleNull(rsLinFact!n_benef_doe, ""), _
                    BL_HandleNull(rsLinFact!episodio, ""), BL_HandleNull(rsLinFact!dt_ini_real, ""), BL_HandleNull(rsLinFact!cod_rubr_efr, ""), _
                    BL_HandleNull(rsLinFact!descr_rubr, ""), BL_HandleNull(rsLinFact!val_pr_unit, 0), BL_HandleNull(rsLinFact!val_Taxa, 0), _
                    BL_HandleNull(rsLinFact!qtd, 0), BL_HandleNull(rsLinFact!perc_desc_lin, 0), BL_HandleNull(rsLinFact!val_total, 0), _
                    BL_HandleNull(rsLinFact!cod_isen_doe, ""), BL_HandleNull(rsLinFact!val_pag_doe, 0), BL_HandleNull(rsLinFact!sld_qtd_cred, 0), _
                    BL_HandleNull(rsLinFact!sld_val_cred, 0), serieRec, Recibo
                rsLinFact.MoveNext
            Wend
            Pesquisar = True
        End If
        rsLinFact.Close
        Set rsLinFact = Nothing
    Else
        BG_Mensagem mediMsgBox, "N�o foram seleccionados registos", vbInformation, App.ProductName
        RsFact.Close
        Set RsFact = Nothing
        LimpaCampos
        Exit Function
    End If
    
    RsFact.Close
    Set RsFact = Nothing
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Erro na fun��o Pesquisar: " & Err.Description, Me.Name, "Pesquisar", True
End Function

Sub PreencheEstutDados(OrdemImp As Integer, SerieFac As String, Nfac As Long, ano As Integer, mes As Integer, DescrEFR As String, doente As String, NomeDoente As String, NBenefDoe As String, _
        episodio As String, DtIniReal As String, codRubrEfr As String, DescrRubr As String, ValPrUnit As Double, ValTaxa As Double, _
        qtd As Double, PercDescLin As Double, ValTotal As Double, CodIsenDoe As String, ValPagDoe As Double, SldQtdCred As Double, _
        SldValCred As Double, SerieFacDoe As String, NFacDoe As Long)
        
    Dim sql As String
    
    sql = "INSERT INTO sl_guias_facturacao (n_guia, ordem, serie_fac, n_fac, ano, mes, descr_efr, doente, nome_doente, n_benef_doe," & _
            " episodio, dt_ini_real, cod_rubr_efr, descr_rubr, val_pr_unit, val_taxa, qtd, perc_desc_lin, val_total, cod_isen_doe," & _
            " val_pag_doe, sld_qtd_cred, sld_val_cred, serie_fac_doe, n_fac_doe) VALUES (" & _
            EcNGuia.Text & ", " & OrdemImp & ", " & BL_TrataStringParaBD(SerieFac) & ", " & Nfac & ", " & ano & ", " & mes & ", " & BL_TrataStringParaBD(DescrEFR) & ", " & _
            BL_TrataStringParaBD(doente) & ", " & BL_TrataStringParaBD(NomeDoente) & ", " & BL_TrataStringParaBD(NBenefDoe) & ", " & _
            BL_TrataStringParaBD(episodio) & ", " & BL_TrataDataParaBD(DtIniReal) & ", " & BL_TrataStringParaBD(codRubrEfr) & ", " & _
            BL_TrataStringParaBD(DescrRubr) & ", " & Replace(ValPrUnit, ",", ".") & ", " & Replace(ValTaxa, ",", ".") & ", " & _
            Replace(qtd, ",", ".") & ", " & Replace(PercDescLin, ",", ".") & ", " & Replace(ValTotal, ",", ".") & ", " & _
            BL_TrataStringParaBD(CodIsenDoe) & ", " & Replace(ValPagDoe, ",", ".") & ", " & Replace(SldQtdCred, ",", ".") & ", " & _
            Replace(SldValCred, ",", ".") & ", " & BL_TrataStringParaBD(SerieFacDoe) & ", " & NFacDoe & ")"
    BG_ExecutaQuery_ADO sql
End Sub

Sub FuncaoAnterior()
    'nada
End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub BD_Insert()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub BD_Update()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub

Sub BD_Delete()
    'nada
End Sub

Sub Funcao_DataActual()
    'nada
End Sub

Sub FuncaoImprimir()
    Dim nomeReport As String
    Dim continua As Boolean
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Guia de Factura��o") = True Then Exit Sub
    
    If EcNGuia.Text = "" And EcSerieFac.Text = "" And EcNFac.Text = "" And EcRequis.Text = "" Then
        BG_Mensagem mediMsgBox, "Deve escolher um dos par�metros:" & vbCrLf & "Guia N�" & vbCrLf & "S�rie e N�mero da Factura" & vbCrLf & "Requisi��o", vbExclamation, App.ProductName
        Exit Sub
    End If
    
    If EcNGuia.Text = "" And EcRequis.Text = "" Then
        If EcSerieFac.Text = "" Or EcNFac.Text = "" Then
            BG_Mensagem mediMsgBox, "Deve preencher a S�rie e o N�mero da Factura", vbExclamation, App.ProductName
            Exit Sub
        End If
    End If
    
    If OptResumo.value = False And OptTratamento.value = False Then
        BG_Mensagem mediMsgBox, "Deve escolher uma das op��es:" & vbCrLf & "Resumo" & vbCrLf & "Tratamento", vbExclamation, App.ProductName
        Exit Sub
    End If
    
    If Not Pesquisar Then Exit Sub
    
    nomeReport = ""
    If OptResumo.value = True Then
        nomeReport = "GuiaFactResumo"
    ElseIf OptTratamento.value = True Then
        nomeReport = "GuiaFactTratamento"
    End If
    
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Guia de Factura��o", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Guia de Factura��o", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.WindowState = crptMaximized
    
    Report.SQLQuery = "SELECT * "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_guias_facturacao WHERE n_guia = " & EcNGuia.Text
    If EcRequis.Text <> "" Then
        Report.SQLQuery = Report.SQLQuery & " AND episodio = " & BL_TrataStringParaBD(EcRequis.Text)
    End If
    Report.SQLQuery = Report.SQLQuery & " ORDER BY episodio, ordem"
    
    Call BL_ExecutaReport

End Sub




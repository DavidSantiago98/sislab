VERSION 5.00
Begin VB.Form FormEstatAnaFact 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3600
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5910
   Icon            =   "FormEstatAnaFact.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   5910
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPesqRapProven 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   17
      Top             =   3960
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CheckBox Flg_OrderProven 
      Caption         =   "Ordenar por proveni�ncia"
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   3240
      Width           =   2295
   End
   Begin VB.CheckBox Flg_DescrAna 
      Caption         =   "Descriminar an�lises"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   2880
      Width           =   2295
   End
   Begin VB.Frame Frame1 
      Height          =   2775
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   5655
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   14
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   1200
         Width           =   3015
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   5040
         Picture         =   "FormEstatAnaFact.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1200
         Width           =   435
      End
      Begin VB.ComboBox CbUrgencia 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   2280
         Width           =   1335
      End
      Begin VB.ComboBox CbSituacao 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   2880
         TabIndex        =   4
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1320
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
      Begin VB.ComboBox CbGrupo 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   720
         Width           =   2655
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2280
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Situa��o"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2610
         TabIndex        =   5
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo An�lises"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1095
      End
   End
   Begin VB.Label Label22 
      Caption         =   "EcPesqRapProven"
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   3960
      Visible         =   0   'False
      Width           =   1455
   End
End
Attribute VB_Name = "FormEstatAnaFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Sub Preenche_Estatistica()
    
    Dim sql As String
    Dim continua As Boolean
            
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
        If gImprimirDestino = 1 Then
            If Flg_DescrAna.value = 1 Then
                continua = BL_IniciaReport("EstatisticaAnaFact", "Estat�stica de An�lises Enviadas", crptToPrinter)
            ElseIf Flg_OrderProven.value = 1 Then
                continua = BL_IniciaReport("EstatisticaSonho_Proven", "Estat�stica de An�lises Enviadas", crptToPrinter)
            Else
                continua = BL_IniciaReport("EstatisticaSonho", "Estat�stica de An�lises Enviadas", crptToPrinter)
            End If
        Else
            If Flg_DescrAna.value = 1 Then
                continua = BL_IniciaReport("EstatisticaAnaFact", "Estat�stica de An�lises Enviadas", crptToWindow)
            ElseIf Flg_OrderProven.value = 1 Then
                continua = BL_IniciaReport("EstatisticaSonho_Proven", "Estat�stica de An�lises Enviadas", crptToWindow)
            Else
                continua = BL_IniciaReport("EstatisticaSonho", "Estat�stica de An�lises Enviadas", crptToWindow)
            End If
        End If
        If continua = False Then Exit Sub
    
    Call Cria_TmpRec_Estatistica
    
    PreencheTabelaTemporaria
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT SL_CR_ESTAT_SONHO.DESCR_GRUPO, SL_CR_ESTAT_SONHO.COD_ANA_SISLAB, SL_CR_ESTAT_SONHO.DESCR_ANA_SISLAB,SL_CR_ESTAT_SONHO.COD_ANA_SONHO, SL_CR_ESTAT_SONHO.DESCR_ANA_SONHO " & _
        " FROM SL_CR_ESTAT_SONHO" & gNumeroSessao & " SL_CR_ESTAT_SONHO ORDER BY DESCR_GRUPO, SL_CR_ESTAT_SONHO.COD_ANA_SISLAB, SL_CR_ESTAT_SONHO.COD_ANA_SONHO "
        
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    If CbGrupo.ListIndex <> -1 Then
        Report.formulas(2) = "Grupo=" & BL_TrataStringParaBD("" & CbGrupo.Text)
    End If
    If CbSituacao.ListIndex <> -1 Then
        Report.formulas(3) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.Text)
    End If
    If CbUrgencia.ListIndex <> -1 Then
        Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    End If
    

'    Me.SetFocus
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    'Call BL_RemoveTabela("SL_CR_ESTAT_SONHO" & gNumeroSessao)
    sql = "Drop table sl_cr_estat_sonho" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
End Sub

Sub PreencheTabelaTemporaria()

    Dim sql As String
    Dim SqlC As String
    
    'verifica os campos preenchidos
    'se grupo preenchido
    If (CbGrupo.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_gr_ana.descr_gr_ana = " & BL_TrataStringParaBD(CbGrupo)
    End If
    
    'se situa��o preenchida
    If CbSituacao.ListIndex <> -1 Then
        SqlC = SqlC & " AND sl_requis.t_sit = " & CbSituacao.ListIndex
    End If
    
    'se prioridade preenchida
    If CbUrgencia.ListIndex <> -1 Then
        SqlC = SqlC & " AND sl_requis.t_urg = " & BL_TrataStringParaBD(UCase(Mid(CbUrgencia.Text, 1, 1)))
    End If
    
    'se proveniencia preenchida
    If EcCodProveniencia.Text <> "" Then
        SqlC = SqlC & " AND sl_requis.cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    
    
    'Data preenchida
    SqlC = SqlC & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim) & " AND sl_requis.dt_chega is not null "
    
    
    sql = "INSERT INTO SL_CR_ESTAT_SONHO" & gNumeroSessao & " ( " & _
        " N_REQ, COD_ANA_SISLAB,DESCR_ANA_SISLAB,COD_ANA_SONHO,DESCR_ANA_SONHO,DESCR_GRUPO,DESCR_T_SIT, DESCR_PROVEN) " & _
        " SELECT sl_requis.n_req,cod_ana_sislab,descr_ana_s descricao,cod_analise,descr_sonho, descr_gr_ana, descr_t_sit, descr_proven " & _
        " From sl_analises_in, sl_ana_s, sl_gr_ana, sl_analises_sonho, sl_requis, sl_tbf_t_sit, sl_proven " & _
        " where cod_ana_sislab = sl_ana_s.cod_ana_s and " & _
        " sl_requis.n_req = sl_analises_in.n_req and " & _
        " sl_requis.t_sit = sl_tbf_t_sit.cod_t_sit and " & _
        " sl_requis.cod_proven = sl_proven.cod_proven and " & _
        " sl_ana_s.gr_ana = sl_gr_ana.cod_gr_ana and " & _
        " cod_analise = sl_analises_sonho.cod_sonho(+) "
    
    sql = sql & SqlC
        
    sql = sql & " UNION all " & _
        " select sl_requis.n_req,cod_ana_sislab,descr_ana_c descricao,cod_analise,descr_sonho, descr_gr_ana, descr_t_sit, descr_proven " & _
        " From sl_analises_in, sl_ana_c, sl_gr_ana, sl_analises_sonho, sl_requis, sl_tbf_t_sit, sl_proven " & _
        " where cod_ana_sislab = sl_ana_c.cod_ana_c and " & _
        " sl_requis.n_req = sl_analises_in.n_req and " & _
        " sl_requis.t_sit = sl_tbf_t_sit.cod_t_sit and " & _
        " sl_requis.cod_proven = sl_proven.cod_proven and " & _
        " sl_ana_c.gr_ana = sl_gr_ana.cod_gr_ana  and " & _
        " cod_analise = sl_analises_sonho.cod_sonho(+) "
    sql = sql & SqlC
    
    sql = sql & " Union all " & _
        " select sl_requis.n_req,cod_ana_sislab,descr_perfis descricao,cod_analise,descr_sonho, descr_gr_ana, descr_t_sit,descr_proven " & _
        " From sl_analises_in, sl_perfis, sl_gr_ana, sl_analises_sonho, sl_requis, sl_tbf_t_sit, sl_proven " & _
        " where cod_ana_sislab = sl_perfis.cod_perfis and " & _
        " sl_requis.n_req = sl_analises_in.n_req and " & _
        " sl_requis.t_sit = sl_tbf_t_sit.cod_t_sit and " & _
        " sl_requis.cod_proven = sl_proven.cod_proven and " & _
        " sl_perfis.gr_ana = sl_gr_ana.cod_gr_ana and " & _
        " cod_analise = sl_analises_sonho.cod_sonho(+) "
    sql = sql & SqlC

    sql = sql & " Union all " & _
        " select sl_requis.n_req,cod_ana_sislab,descr_microrg descricao,cod_analise,descr_sonho, 'MICROBIOLOGIA', descr_t_sit, descr_proven " & _
        " From sl_analises_in, sl_microrg, sl_analises_sonho,sl_gr_ana, sl_requis, sl_tbf_t_sit, sl_proven " & _
        " where cod_ana_sislab = sl_microrg.cod_microrg and " & _
        " sl_requis.n_req = sl_analises_in.n_req and " & _
        " sl_requis.t_sit = sl_tbf_t_sit.cod_t_sit and " & _
        " sl_requis.cod_proven = sl_proven.cod_proven and " & _
        " cod_analise = sl_analises_sonho.cod_sonho(+) "
    
    sql = sql & SqlC

    BG_ExecutaQuery_ADO sql, gConexao
    BG_Trata_BDErro

    
End Sub

Sub Cria_TmpRec_Estatistica()

    Dim TmpRec(8) As DefTable
    
    TmpRec(1).NomeCampo = "DESCR_GRUPO"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 40
    
    TmpRec(2).NomeCampo = "N_REQ"
    TmpRec(2).tipo = "INTEGER"
    TmpRec(2).tamanho = 9
    
    TmpRec(3).NomeCampo = "COD_ANA_SISLAB"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 8
    
    TmpRec(4).NomeCampo = "DESCR_ANA_SISLAB"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 65
    
    TmpRec(5).NomeCampo = "COD_ANA_SONHO"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 10
    
    TmpRec(6).NomeCampo = "DESCR_ANA_SONHO"
    TmpRec(6).tipo = "STRING"
    TmpRec(6).tamanho = 100
    
    TmpRec(7).NomeCampo = "DESCR_T_SIT"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 20
    
    TmpRec(8).NomeCampo = "DESCR_PROVEN"
    TmpRec(8).tipo = "STRING"
    TmpRec(8).tamanho = 60
    
    Call BL_CriaTabela("SL_CR_ESTAT_SONHO" & gNumeroSessao, TmpRec)
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesquisaProveniencia_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_proven", _
                        "descr_proven", "seq_proven", _
                        EcPesqRapProven
End Sub

Private Sub CbGrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao CbGrupo, KeyCode
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbSituacao, KeyCode

End Sub

Public Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodProveniencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven, t_sit FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.Text = ""
            EcDescrProveniencia.Text = ""
        Else
            EcDescrProveniencia.Text = Tabela!descr_proven
        
            If Not (IsNull(Tabela!t_sit)) Then
                CbSituacao.ListIndex = Tabela!t_sit
            End If
        
        End If
        
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub


Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Flg_DescrAna_Click()
    If Flg_DescrAna.value = 1 Then
        Flg_OrderProven.Enabled = False
    Else
        Flg_OrderProven.Enabled = True
    End If
End Sub

Private Sub Flg_OrderProven_Click()
    If Flg_OrderProven.value = 1 Then
        Flg_DescrAna.Enabled = False
    Else
        Flg_DescrAna.Enabled = True
    End If
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de An�lises enviadas para o GH"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 6000
    Me.Height = 3975 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtFim
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica por Laborat�rio")
    
    Set FormEstatAnaFact = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    

    CbGrupo.ListIndex = mediComboValorNull

    EcDtFim.Text = ""
    EcDtIni.Text = ""
    Flg_DescrAna.value = 0

    
End Sub

Sub DefTipoCampos()

    

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    
    'Preenche Combo Tipo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub







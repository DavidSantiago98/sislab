VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormOrdenaAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormOrdenaAna"
   ClientHeight    =   6780
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8775
   Icon            =   "FormOrdenaAna.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   8775
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtExpande 
      Height          =   495
      Left            =   120
      Picture         =   "FormOrdenaAna.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   " Expande "
      Top             =   600
      Width           =   495
   End
   Begin VB.CommandButton BtColapse 
      Height          =   495
      Left            =   120
      Picture         =   "FormOrdenaAna.frx":066A
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   " Reagrupa "
      Top             =   1200
      Visible         =   0   'False
      Width           =   495
   End
   Begin MSComctlLib.ImageList ListaImagensAna 
      Left            =   8160
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":0CC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":101A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":136C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":16BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":1A10
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":1D62
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":20B4
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":2406
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":2758
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":2AAA
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":2DFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAna.frx":314E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton BtCima 
      Height          =   495
      Left            =   120
      Picture         =   "FormOrdenaAna.frx":34A0
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   " Mover An�lise Para Cima "
      Top             =   1800
      Width           =   495
   End
   Begin VB.CommandButton BtBaixo 
      Height          =   495
      Left            =   120
      Picture         =   "FormOrdenaAna.frx":37AA
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   " Mover An�lise Para Baixo "
      Top             =   2400
      Width           =   495
   End
   Begin VB.ListBox LsCodGrup 
      Height          =   1620
      Left            =   9000
      TabIndex        =   4
      Top             =   480
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.ComboBox CbGrupo 
      Height          =   315
      Left            =   4080
      Locked          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   7935
   End
   Begin MSComctlLib.TreeView TrAna1 
      Height          =   6255
      Left            =   840
      TabIndex        =   1
      Top             =   120
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   11033
      _Version        =   393217
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ListaImagensAna"
      Appearance      =   1
   End
   Begin VB.Label Label1 
      Caption         =   "(N�o se pode alterar ordens entre subgrupos ou classes)."
      Height          =   255
      Left            =   840
      TabIndex        =   5
      Top             =   6480
      Width           =   5415
   End
End
Attribute VB_Name = "FormOrdenaAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/03/2003
' T�cnico      : Paulo Costa

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim TodosGrp As Long

Private Const ImgGr = 1
Private Const ImgGrS = 2

Private Const ImgSGr = 3
Private Const ImgSGrS = 4

Private Const ImgClasse = 5
Private Const ImgClasseS = 6

Private Const ImgP = 7
Private Const ImgPS = 8

Private Const ImgC = 9
Private Const ImgCS = 10

Private Const ImgS = 11
Private Const ImgSS = 12

Sub FuncaoModificar()
    
    BG_CommitTransaction
    Call ModificaOrdens
    'Call ActualizaArvore(CbGrupo.ListIndex)
    Call ActualizaRequisicoes
    Call ActualizaArvore(TodosGrp)
    estado = 0
    BL_ToolbarEstadoN estado
    
End Sub

Sub Inicializacoes()

    Me.caption = " Ordena��o de An�lises"
    
    Me.left = 540
    Me.top = 45
    Me.Width = 8865
    Me.Height = 7155 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    
    PreencheValoresDefeito
    
    BG_BeginTransaction
    
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    
'    CbGrupo.SetFocus

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    Call BtExpande_Click
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    
    BG_RollbackTransaction
    
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormOrdenaAna = Nothing

End Sub

Private Sub BtBaixo_Click()

    TrAna1.SetFocus
    Call TrAna1_KeyDown(40, 2)
    
End Sub

Private Sub BtCima_Click()

    TrAna1.SetFocus
    Call TrAna1_KeyDown(38, 2)
    
End Sub

Private Sub BtColapse_Click()
    
    Call BL_ColapsaTreeview(Me.TrAna1)

End Sub

Private Sub BtExpande_Click()
    
    Call BL_ExpandeTreeview(Me.TrAna1)

End Sub

Private Sub CbGrupo_Click()

    TrAna1.Nodes.Clear
    Call ActualizaArvore(CbGrupo.ListIndex)
    
End Sub

Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()
    
    EventoLoad
    
    Call BL_ColapsaTreeview(Me.TrAna1)

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Public Sub PreencheValoresDefeito()

    Dim RsGrupos As ADODB.recordset
    Dim i As Long
    
    Set RsGrupos = New ADODB.recordset
    
    With RsGrupos
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        
        .Source = "SELECT " & _
                  "     cod_gr_ana, " & _
                  "     descr_gr_ana " & _
                  "FROM " & _
                  "     sl_gr_ana " & _
                  "ORDER BY " & _
                  "     ORDEM_IMP asc, descr_gr_ana asc"
        
        .ActiveConnection = gConexao
    End With
    RsGrupos.Open
    
    i = 0
    While Not RsGrupos.EOF
        LsCodGrup.AddItem "" & RsGrupos!cod_gr_ana, i
        CbGrupo.AddItem "" & RsGrupos!descr_gr_ana, i
        i = i + 1
        RsGrupos.MoveNext
    Wend
    RsGrupos.Close
    Set RsGrupos = Nothing
    
    CbGrupo.AddItem "Todos os Grupos"
    CbGrupo.ItemData(0) = i
    TodosGrp = i

    'CbGrupo.ListIndex = i
    
    Call ActualizaArvore(TodosGrp)
        
    'CbGrupo.Text = "Todos os Grupos"
    
End Sub

Private Sub ActualizaArvore(Grupo As Long)

    Dim rsAna As ADODB.recordset
    Dim RsSgr As ADODB.recordset
    Dim RsClasse As ADODB.recordset
    Dim CmdSgr As ADODB.Command
    Dim CmdClasse As ADODB.Command
    Dim CodGrupo As String
    Dim Pai As String
    Dim Imagem As Integer
    Dim LastSgr As String
    Dim LastClasse As String
    Dim i As Long
    Dim k As Long
    
    BL_InicioProcessamento Me, "A actualizar lista de an�lises"
    
    If Grupo = TodosGrp Then
        'Todas as An�lises
        For i = 0 To LsCodGrup.ListCount - 1
            Call ActualizaArvore(i)
        Next i
    Else
        If Grupo = -1 Then
            CodGrupo = "null"
        Else
            CodGrupo = LsCodGrup.List(Grupo)
        End If
        
        'An�lises do grupo CodGrupo
        BL_AcrescentaNodoPai TrAna1, IIf(CodGrupo = "null", "Sem Grupo", CbGrupo.List(Grupo)), "G" & CodGrupo, ImgGr, ImgGrS
        TrAna1.Nodes("G" & CodGrupo).Expanded = True
        
        Set rsAna = New ADODB.recordset
        With rsAna
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
                      
            .Source = "SELECT " & _
                      "     'S' as tipo, descr_ana_s as descricao, sgr_ana, classe_ana, ordem, cod_ana_s as codigo " & _
                      "FROM " & _
                      "     sl_ana_s " & _
                      "WHERE " & _
                      "     gr_ana " & IIf(CodGrupo = "null", "is " & CodGrupo, "= " & BL_TrataStringParaBD(CodGrupo)) & _
                      " UNION " & _
                      "SELECT " & _
                      "     'C' as tipo, descr_ana_c as descricao, sgr_ana, classe_ana,  ordem, cod_ana_c as codigo " & _
                      "FROM " & _
                      "     sl_ana_c " & _
                      "WHERE " & _
                      "     gr_ana " & IIf(CodGrupo = "null", "is " & CodGrupo, "= " & BL_TrataStringParaBD(CodGrupo)) & _
                      " UNION " & _
                      "SELECT " & _
                      "     'P' as tipo, descr_perfis as descricao, sgr_ana, classe_ana,  ordem, cod_perfis as codigo " & _
                      "FROM " & _
                      "     sl_perfis " & _
                      "WHERE " & _
                      "     gr_ana " & IIf(CodGrupo = "null", "is " & CodGrupo, "= " & BL_TrataStringParaBD(CodGrupo)) & " AND flg_activo = '1' " & _
                      "ORDER BY " & _
                      "     3, 4, 5, 6"
                      
            .ActiveConnection = gConexao
        End With
        rsAna.Open
        
        ' Subgrupo
        Set CmdSgr = New ADODB.Command
        Set CmdSgr.ActiveConnection = gConexao
        CmdSgr.CommandType = adCmdText
        
        CmdSgr.CommandText = "SELECT " & _
                             "      descr_sgr_ana " & _
                             "FROM " & _
                             "      sl_sgr_ana " & _
                             "WHERE " & _
                             "      cod_sgr_ana = ?"
        CmdSgr.Prepared = True
        
        CmdSgr.Parameters.Append CmdSgr.CreateParameter("COD_SGR_ANA", adVarChar, adParamInput, 5)
        
        Set RsSgr = New ADODB.recordset
        
        ' Classe
        Set CmdClasse = New ADODB.Command
        Set CmdClasse.ActiveConnection = gConexao
        CmdClasse.CommandType = adCmdText
        
        CmdClasse.CommandText = "SELECT " & _
                                "       descr_classe_ana " & _
                                "FROM " & _
                                "       sl_classe_ana " & _
                                "WHERE " & _
                                "       cod_classe_ana = ?"
        CmdClasse.Prepared = True
        
        CmdClasse.Parameters.Append CmdClasse.CreateParameter("COD_CLASSE_ANA", adVarChar, adParamInput, 5)
        
        Set RsClasse = New ADODB.recordset
        
        ' ---------------
        
        LastSgr = ""
        LastClasse = ""
        Pai = "G" & CodGrupo
        k = 1
        While Not rsAna.EOF
            
            ' Subgrupo
            If (LastSgr <> "" & rsAna!sgr_ana) Then
            
                LastSgr = "" & rsAna!sgr_ana
                CmdSgr.Parameters("COD_SGR_ANA") = "" & rsAna!sgr_ana
                Set RsSgr = CmdSgr.Execute
                
                If RsSgr.EOF Then
                    Pai = "G" & CodGrupo
                Else
                    k = k + 1
                    BL_AcrescentaNodoFilho TrAna1, _
                                           "" & RsSgr!descr_sgr_ana, _
                                           "G" & CodGrupo, _
                                           "G" & CodGrupo & "S" & "" & rsAna!sgr_ana, _
                                           ImgSGr, _
                                           ImgSGrS
                    Pai = "G" & CodGrupo & "S" & "" & rsAna!sgr_ana
                End If
                
                RsSgr.Close
            
            End If
            ' Classe
            If (LastClasse <> "" & rsAna!classe_ana) Then
            
                LastClasse = "" & rsAna!classe_ana
                CmdClasse.Parameters("COD_CLASSE_ANA") = "" & rsAna!classe_ana
                Set RsClasse = CmdClasse.Execute

                If RsClasse.EOF Then
                    Pai = "G" & CodGrupo & "S" & "" & rsAna!sgr_ana
                Else
                    k = k + 1
                    BL_AcrescentaNodoFilho TrAna1, _
                                           "" & RsClasse!descr_classe_ana, _
                                           Pai, _
                                           Pai & "CL" & rsAna!classe_ana, _
                                           ImgClasse, _
                                           ImgClasseS
                    Pai = Pai & "CL" & rsAna!classe_ana
                End If

                RsClasse.Close

            End If
            
            
            Imagem = 0
            If "" & rsAna!tipo = "S" Then
                Imagem = ImgS
            ElseIf "" & rsAna!tipo = "C" Then
                Imagem = ImgC
            ElseIf "" & rsAna!tipo = "P" Then
                Imagem = ImgP
            End If
            
            k = k + 1
            BL_AcrescentaNodoFilho TrAna1, "[" & rsAna!Codigo & "]" & Space(8 - Len("" & rsAna!Codigo)) & rsAna!descricao, Pai, "" & rsAna!Codigo, Imagem, Imagem + 1
                
            If BL_HandleNull(rsAna!ordem, 0) = 0 Then
                'Se a ordem vier nula actualiza com o index
                gSQLError = 0
                If "" & rsAna!tipo = "S" Then
                    BG_ExecutaQuery_ADO "UPDATE sl_ana_s SET ordem = " & k & " WHERE cod_ana_s = " & BL_TrataStringParaBD(rsAna!Codigo), "Erro a actualizar ordem!"
                ElseIf "" & rsAna!tipo = "C" Then
                    BG_ExecutaQuery_ADO "UPDATE sl_ana_c SET ordem = " & k & " WHERE cod_ana_c = " & BL_TrataStringParaBD(rsAna!Codigo), "Erro a actualizar ordem!"
                ElseIf "" & rsAna!tipo = "P" Then
                    BG_ExecutaQuery_ADO "UPDATE sl_perfis SET ordem = " & k & " WHERE cod_perfis = " & BL_TrataStringParaBD(rsAna!Codigo), "Erro a actualizar ordem!"
                End If
            End If
        
            rsAna.MoveNext
        Wend
        rsAna.Close
    
    End If
    
    BL_FimProcessamento Me
    
End Sub

Private Sub TrAna1_KeyDown(KeyCode As Integer, Shift As Integer)

    On Error GoTo ErrorHandler

    Dim lAntigo As Long
    Dim lNovo As Long
    Dim TabelaAntigo As String
    Dim CodigoAntigo As String
    Dim TabelaNovo As String
    Dim CodigoNovo As String
    Dim sTempText As String
    Dim sTempKeyAntigo As String
    Dim sTempKeyNovo As String
    
    Dim iTempImage As Integer
    Dim iTempImageSelect As Integer
    
    If KeyCode = 38 And Shift = 2 Then 'Ctrl + Up: mover para cima
        'N�o permitir ultrapassar o inicio
        If TrAna1.SelectedItem.Index <= 2 Then Exit Sub
        
        'N�o permitir mover grupos e subgrupos
        ' --------------------------------------------------
        On Error Resume Next
        Err.Clear
        If (TrAna1.SelectedItem.parent <> TrAna1.Nodes(TrAna1.SelectedItem.Index - 1).parent) Then
            Exit Sub
        End If
        If (Err.Number <> 0) Then
            Err.Clear
            Exit Sub
        End If
        On Error GoTo ErrorHandler
        ' --------------------------------------------------
        
        If Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), 1, 1) = "S" Then
            TabelaAntigo = "sl_ana_s"
            CodigoAntigo = "cod_ana_s"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), 1, 1) = "C" Then
            TabelaAntigo = "sl_ana_c"
            CodigoAntigo = "cod_ana_c"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), 1, 1) = "P" Then
            TabelaAntigo = "sl_perfis"
            CodigoAntigo = "cod_perfis"
        Else
            Exit Sub
        End If
        
        If Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index - 1).Key), 1, 1) = "S" Then
            TabelaNovo = "sl_ana_s"
            CodigoNovo = "cod_ana_s"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index - 1).Key), 1, 1) = "C" Then
            TabelaNovo = "sl_ana_c"
            CodigoNovo = "cod_ana_c"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index - 1).Key), 1, 1) = "P" Then
            TabelaNovo = "sl_perfis"
            CodigoNovo = "cod_perfis"
        Else
            Exit Sub
        End If
        
        lAntigo = TrAna1.SelectedItem.Index
        lNovo = TrAna1.SelectedItem.Index - 1
        
        gSQLError = 0
        BG_ExecutaQuery_ADO "UPDATE " & TabelaAntigo & " SET ordem = " & lNovo & " WHERE " & CodigoAntigo & " = " & BL_TrataStringParaBD(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), "Erro a actualizar ordem!"
        If gSQLError <> 0 Then
            Exit Sub
        End If
        
        BG_ExecutaQuery_ADO "UPDATE " & TabelaNovo & " SET ordem = " & lAntigo & " WHERE " & CodigoNovo & " = " & BL_TrataStringParaBD(TrAna1.Nodes(TrAna1.SelectedItem.Index - 1).Key), "Erro a actualizar ordem!"
        If gSQLError <> 0 Then
            Exit Sub
        End If
        
        If estado = 0 Then
            estado = 2
            BL_ToolbarEstadoN estado
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Limpar", "InActivo"
            BL_Toolbar_BotaoEstado "Procurar", "InActivo"
            BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
            BL_Toolbar_BotaoEstado "Anterior", "InActivo"
            BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
        End If
        
        'Trocar o Texto
        sTempText = TrAna1.Nodes(lNovo).Text
        TrAna1.Nodes(lNovo).Text = TrAna1.Nodes(lAntigo).Text
        TrAna1.Nodes(lAntigo).Text = sTempText
        'Trocar as keys
        sTempKeyAntigo = TrAna1.Nodes(lAntigo).Key
        sTempKeyNovo = TrAna1.Nodes(lNovo).Key
        TrAna1.Nodes(lAntigo).Key = "ANALISE DUMMY"
        TrAna1.Nodes(lNovo).Key = sTempKeyAntigo
        TrAna1.Nodes(lAntigo).Key = sTempKeyNovo
        'Trocar as imagens
        'A imagem n�o est� seleccionada
        iTempImage = TrAna1.Nodes(lNovo).Image
        iTempImageSelect = TrAna1.Nodes(lNovo).SelectedImage
        'Guarda a imagem para os itens n�o seleccionados
        TrAna1.Nodes(lNovo).Image = TrAna1.Nodes(lAntigo).Image
        TrAna1.Nodes(lNovo).SelectedImage = TrAna1.Nodes(lAntigo).SelectedImage
        TrAna1.Nodes(lAntigo).Image = iTempImage
        TrAna1.Nodes(lAntigo).SelectedImage = iTempImageSelect
        TrAna1.Refresh
        
        TrAna1.Nodes(lNovo).Selected = True
        TrAna1.SetFocus
    ElseIf KeyCode = 40 And Shift = 2 Then 'Ctrl + Down: mover para baixo
        'N�o permitir alterar a raiz
        If TrAna1.SelectedItem.Index < 2 Then Exit Sub
        
        'N�o permitir ultrapassar o fim
        If TrAna1.SelectedItem.Index = TrAna1.Nodes.Count Then Exit Sub

        'N�o permitir mover grupos e subgrupos
        ' --------------------------------------------------
        On Error Resume Next
        Err.Clear
        If TrAna1.SelectedItem.parent <> TrAna1.Nodes(TrAna1.SelectedItem.Index + 1).parent Then
            Exit Sub
        End If
        If (Err.Number <> 0) Then
            Err.Clear
            Exit Sub
        End If
        On Error GoTo ErrorHandler
        ' --------------------------------------------------
        
        If Mid(TrAna1.Nodes(TrAna1.SelectedItem.Index + 1).Key, 1, 1) = "G" Then Exit Sub

        If Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), 1, 1) = "S" Then
            TabelaAntigo = "sl_ana_s"
            CodigoAntigo = "cod_ana_s"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), 1, 1) = "C" Then
            TabelaAntigo = "sl_ana_c"
            CodigoAntigo = "cod_ana_c"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), 1, 1) = "P" Then
            TabelaAntigo = "sl_perfis"
            CodigoAntigo = "cod_perfis"
        Else
            Exit Sub
        End If
        
        If Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index + 1).Key), 1, 1) = "S" Then
            TabelaNovo = "sl_ana_s"
            CodigoNovo = "cod_ana_s"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index + 1).Key), 1, 1) = "C" Then
            TabelaNovo = "sl_ana_c"
            CodigoNovo = "cod_ana_c"
        ElseIf Mid(Trim(TrAna1.Nodes(TrAna1.SelectedItem.Index + 1).Key), 1, 1) = "P" Then
            TabelaNovo = "sl_perfis"
            CodigoNovo = "cod_perfis"
        Else
            Exit Sub
        End If
        
        lAntigo = TrAna1.SelectedItem.Index
        lNovo = TrAna1.SelectedItem.Index + 1
        
        gSQLError = 0
        BG_ExecutaQuery_ADO "UPDATE " & TabelaAntigo & " SET ordem = " & lNovo & " WHERE " & CodigoAntigo & " = " & BL_TrataStringParaBD(TrAna1.Nodes(TrAna1.SelectedItem.Index).Key), "Erro a actualizar ordem!"
        If gSQLError <> 0 Then
            Exit Sub
        End If
        
        BG_ExecutaQuery_ADO "UPDATE " & TabelaNovo & " SET ordem = " & lAntigo & " WHERE " & CodigoNovo & " = " & BL_TrataStringParaBD(TrAna1.Nodes(TrAna1.SelectedItem.Index + 1).Key), "Erro a actualizar ordem!"
        If gSQLError <> 0 Then
            Exit Sub
        End If
        
        If estado = 0 Then
            estado = 2
            BL_ToolbarEstadoN estado
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Limpar", "InActivo"
            BL_Toolbar_BotaoEstado "Procurar", "InActivo"
            BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
            BL_Toolbar_BotaoEstado "Anterior", "InActivo"
            BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
        End If
        
        'Trocar o Texto
        sTempText = TrAna1.Nodes(lNovo).Text
        TrAna1.Nodes(lNovo).Text = TrAna1.Nodes(lAntigo).Text
        TrAna1.Nodes(lAntigo).Text = sTempText
        'Trocar as keys
        sTempKeyAntigo = TrAna1.Nodes(lAntigo).Key
        sTempKeyNovo = TrAna1.Nodes(lNovo).Key
        TrAna1.Nodes(lAntigo).Key = "ANALISE DUMMY"
        TrAna1.Nodes(lNovo).Key = sTempKeyAntigo
        TrAna1.Nodes(lAntigo).Key = sTempKeyNovo
         'Trocar as imagens
         'A imagem n�o est� seleccionada
        iTempImage = TrAna1.Nodes(lNovo).Image
        iTempImageSelect = TrAna1.Nodes(lNovo).SelectedImage
        'Guarda a imagem para os itens n�o seleccionados
        TrAna1.Nodes(lNovo).Image = TrAna1.Nodes(lAntigo).Image
        TrAna1.Nodes(lNovo).SelectedImage = TrAna1.Nodes(lAntigo).SelectedImage
        TrAna1.Nodes(lAntigo).Image = iTempImage
        TrAna1.Nodes(lAntigo).SelectedImage = iTempImageSelect
        TrAna1.Refresh
        TrAna1.Nodes(lNovo).Selected = True
        TrAna1.SetFocus
    End If
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            MsgBox "Erro : FormOrdenaAna (TrAna1_KeyDown)       ", vbInformation, " Controlo de Qualidade"
            Call BG_LogFile_Erros("Erro Inesperado : TrAna1_KeyDown (FormOrdenaAna) -> " & Err.Description)
            Exit Sub
    End Select
End Sub

Sub ModificaOrdens()
    Dim i As Long
    Dim sql As String
    Dim Tabela As String
    Dim campo As String
    
    For i = 1 To TrAna1.Nodes.Count
    
        If Mid(Trim(TrAna1.Nodes(i).Key), 1, 1) = "S" Then
            Tabela = "sl_ana_s"
            campo = "cod_ana_s"
        ElseIf Mid(Trim(TrAna1.Nodes(i).Key), 1, 1) = "C" Then
            Tabela = "sl_ana_c"
            campo = "cod_ana_c"
        ElseIf Mid(Trim(TrAna1.Nodes(i).Key), 1, 1) = "P" Then
            Tabela = "sl_perfis"
            campo = "cod_perfis"
        Else
            GoTo fim
        End If
        
        gSQLError = 0
        sql = "UPDATE " & Tabela & " SET ordem = " & i & " WHERE " & campo & " = " & BL_TrataStringParaBD(TrAna1.Nodes(i).Key)
        BG_ExecutaQuery_ADO sql, "Erro a actualizar ordem!"
        If gSQLError <> 0 Then
            Exit Sub
        End If
fim:
    Next i

End Sub

Private Sub ActualizaRequisicoes()
    Dim sSql As String
    
    'PERFIS
    sSql = "UPDATE sl_realiza SET ord_ana = (select ordem from sl_perfis where sl_realiza.cod_perfil=sl_perfis.cod_perfis)"
    sSql = sSql & " WHERE n_req in (select distinct n_req from sl_marcacoes) and cod_perfil <> '0' "
    BG_ExecutaQuery_ADO sSql
    
    sSql = "UPDATE sl_marcacoes SET ord_ana = (select ordem from sl_perfis where sl_marcacoes.cod_perfil=sl_perfis.cod_perfis)"
    sSql = sSql & " WHERE cod_perfil <> '0' "
    BG_ExecutaQuery_ADO sSql
    
    'COMPLEXAS
    sSql = " update sl_realiza set ord_ana = (select ordem from sl_ana_c where sl_realiza.cod_ana_c=sl_ana_c.cod_ana_c) "
    sSql = sSql & " where n_req in (select distinct n_req from sl_marcacoes) and cod_perfil = '0' and cod_ana_c <>'0' "
    BG_ExecutaQuery_ADO sSql

    sSql = " update sl_marcacoes set ord_ana = (select ordem from sl_ana_c where sl_marcacoes.cod_ana_c=sl_ana_c.cod_ana_c) "
    sSql = sSql & " where cod_perfil = '0' and cod_ana_c <>'0'"
    BG_ExecutaQuery_ADO sSql

    'SIMPLES
    sSql = " update sl_realiza set ord_ana = (select ordem from sl_ana_s where sl_realiza.cod_ana_s=sl_ana_s.cod_ana_s) "
    sSql = sSql & " where n_req in (select distinct n_req from sl_marcacoes) and cod_perfil = '0' and cod_ana_c ='0' "
    BG_ExecutaQuery_ADO sSql

    sSql = " update sl_marcacoes set ord_ana = (select ordem from sl_ana_s where sl_marcacoes.cod_ana_s=sl_ana_s.cod_ana_s) "
    sSql = sSql & " where cod_perfil = '0' and cod_ana_c ='0'"
    BG_ExecutaQuery_ADO sSql
End Sub

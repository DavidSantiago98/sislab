VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormListarResMult 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7050
   Icon            =   "FormListarResMult.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5025
   ScaleWidth      =   7050
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkIncluirEmail 
      Caption         =   "Incluir Destino Email"
      Height          =   195
      Left            =   3360
      TabIndex        =   45
      Top             =   4800
      Width           =   1815
   End
   Begin VB.TextBox EcDescrLocal 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   2880
      Width           =   3975
   End
   Begin VB.TextBox EcCodLocal 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1560
      TabIndex        =   42
      Top             =   2880
      Width           =   975
   End
   Begin VB.CommandButton BtPesqLocal 
      Height          =   315
      Left            =   6480
      Picture         =   "FormListarResMult.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   41
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Locais"
      Top             =   2880
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   6480
      Picture         =   "FormListarResMult.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   25
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Locais"
      Top             =   2400
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaRapidaSGrAnalises 
      Height          =   375
      Left            =   6480
      Picture         =   "FormListarResMult.frx":0B20
      Style           =   1  'Graphical
      TabIndex        =   19
      ToolTipText     =   "Pesquisa R�pida de Sub Grupos de An�lises"
      Top             =   1485
      Width           =   375
   End
   Begin VB.CheckBox CkAnexos 
      Caption         =   "N�o Imprimir Anexos"
      Height          =   195
      Left            =   3360
      TabIndex        =   40
      Top             =   4440
      Width           =   1815
   End
   Begin VB.ComboBox CbGenero 
      Height          =   315
      Left            =   5520
      Style           =   2  'Dropdown List
      TabIndex        =   38
      Top             =   600
      Width           =   1335
   End
   Begin VB.Frame FrameReq 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Requisi��o a ser impressa"
      Height          =   975
      Left            =   240
      TabIndex        =   36
      Top             =   5640
      Width           =   3015
      Begin VB.Label LbRequisicao 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   120
         TabIndex        =   37
         Top             =   360
         Width           =   2775
      End
   End
   Begin VB.CheckBox CkInibeImpRelARS 
      Caption         =   "N�o Imprimir Relat�rio ARS"
      Height          =   195
      Left            =   240
      TabIndex        =   35
      Top             =   4800
      Width           =   2295
   End
   Begin VB.TextBox EcDataFinal 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3480
      TabIndex        =   33
      Top             =   600
      Width           =   1215
   End
   Begin VB.CheckBox CkApenasNaoImpressos 
      Caption         =   "N�o imprimir os resultados j� impressos"
      Height          =   195
      Left            =   240
      TabIndex        =   32
      Top             =   4440
      Width           =   3135
   End
   Begin VB.OptionButton OptImprimir 
      Caption         =   "Apenas LAB"
      Height          =   255
      Index           =   2
      Left            =   3000
      TabIndex        =   31
      Top             =   4080
      Width           =   1335
   End
   Begin VB.OptionButton OptImprimir 
      Caption         =   "Apenas Postos"
      Height          =   255
      Index           =   1
      Left            =   1560
      TabIndex        =   30
      Top             =   4080
      Width           =   1455
   End
   Begin VB.OptionButton OptImprimir 
      Caption         =   "Todas Req."
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   29
      Top             =   4080
      Width           =   1215
   End
   Begin VB.TextBox EcCodSala 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1560
      TabIndex        =   27
      Top             =   2400
      Width           =   975
   End
   Begin VB.TextBox EcDescrSala 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   2400
      Width           =   3975
   End
   Begin VB.TextBox EcSeqImpr 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1560
      TabIndex        =   23
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcSGrAnalises 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1560
      TabIndex        =   21
      Top             =   1485
      Width           =   975
   End
   Begin VB.TextBox EcDescrSGrAnalises 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   1485
      Width           =   4095
   End
   Begin VB.ComboBox CbUrgencia 
      Height          =   315
      Left            =   3480
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   120
      Width           =   1215
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   5520
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   120
      Width           =   1335
   End
   Begin VB.TextBox EcDataInicial 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1560
      TabIndex        =   12
      Top             =   600
      Width           =   975
   End
   Begin VB.CommandButton BtPesquisaProveniencia 
      Height          =   315
      Left            =   6480
      Picture         =   "FormListarResMult.frx":10AA
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1920
      Width           =   375
   End
   Begin VB.TextBox EcDescrProveniencia 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1920
      Width           =   3975
   End
   Begin VB.TextBox EcCodProveniencia 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1560
      TabIndex        =   4
      Top             =   1920
      Width           =   975
   End
   Begin VB.CommandButton BtSair 
      Height          =   495
      Left            =   6120
      Picture         =   "FormListarResMult.frx":1634
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Sair"
      Top             =   4440
      Width           =   735
   End
   Begin VB.CommandButton BtImprimir 
      Height          =   495
      Left            =   5280
      Picture         =   "FormListarResMult.frx":1EFE
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Imprimir"
      Top             =   4440
      Width           =   855
   End
   Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
      Height          =   375
      Left            =   6480
      Picture         =   "FormListarResMult.frx":2BC8
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
      Top             =   1080
      Width           =   375
   End
   Begin VB.TextBox EcDescrGrAnalises 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1080
      Width           =   3975
   End
   Begin VB.TextBox EcGrAnalises 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1560
      TabIndex        =   0
      Top             =   1080
      Width           =   975
   End
   Begin VB.PictureBox ProgressBar 
      Appearance      =   0  'Flat
      BackColor       =   &H80000002&
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      ScaleHeight     =   345
      ScaleWidth      =   6705
      TabIndex        =   11
      Top             =   3600
      Visible         =   0   'False
      Width           =   6735
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   375
      Left            =   3600
      TabIndex        =   10
      Top             =   6480
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"FormListarResMult.frx":3152
   End
   Begin VB.Label Label5 
      Caption         =   "Local"
      Height          =   255
      Left            =   120
      TabIndex        =   44
      Top             =   2880
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Esp�cie"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   4800
      TabIndex        =   39
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Data Final"
      Height          =   255
      Index           =   2
      Left            =   2640
      TabIndex        =   34
      Top             =   600
      Width           =   855
   End
   Begin VB.Label LbPosto 
      Caption         =   "Posto"
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   2400
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Seq Impress�o"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   24
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Sub Grupo An�lises"
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   1485
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "Grupo de An�lises"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "&Situa��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4800
      TabIndex        =   15
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label6 
      Caption         =   "Prioridade"
      Height          =   255
      Left            =   2640
      TabIndex        =   14
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicial"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   13
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "&Proveni�ncia"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label LblTitReq 
      Alignment       =   2  'Center
      Caption         =   "LblTitReq"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   3240
      Visible         =   0   'False
      Width           =   4335
   End
End
Attribute VB_Name = "FormListarResMult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

'Defini��es necess�rias para a Rotina ImprimeReqMult
Public Enum TipoImpMult
    Completas = 0
    Prontos = 1
    ParcialmenteListados = 2
End Enum

'Variavel que controla o cancelamento da impress�o
Dim Flg_CancelaImp As Boolean
Public TipoImp As TipoImpMult

'Estrutura para protocolo
Private Type Tipo_Protocolo
    n_req As String
    DescrProven As String
    DescrReq As String
    ContaReq As Long
    dt_conclusao As String
    dt_pretend As String
End Type

Dim StProt() As Tipo_Protocolo
Dim NProt As Long
Dim CampoActivo As Object
Dim ImpComp As Integer

Public Sub ImprimeReqMult(tipo As TipoImpMult)
    
    Dim continua As Boolean
    Dim sql As String
    Dim ssql As String
    Dim flg_gerou As Boolean
    Dim FlgImprAnexos  As Boolean
    Dim total As Long
    Dim i As Long
    Dim j As Long
    Dim s As String
    Dim EncontrouProt As Boolean
    Dim reqs As String
    Dim seqRealiza As String
    Dim TemRes As Boolean
        
    'RecordSet principal
    Dim RegReq As adodb.recordset
    Dim rsImpr As New adodb.recordset
    
    'Constantes passadas para o Report
    Dim SitReq As String
    Dim ProvReq As String
    
    Dim SQLEstadoReq  As String
    Dim SQlEstadoAnalise As String
    Dim SQLImprimeUrgentes As String
    Dim SQLProv As String
    
    Dim ListaProv As String
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    '
    On Error GoTo Trata_Erro

    DoEvents
    
    Set RegReq = New adodb.recordset
    RegReq.CursorType = adOpenStatic
    RegReq.CursorLocation = adUseServer
    RegReq.ActiveConnection = gConexao
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    Flg_CancelaImp = False
    
    If gTipoInstituicao = "PRIVADO" Then
        If tipo = Prontos And EcSeqImpr = "" Then
            ActualizaEstados
        End If
    End If
    
    If EcSeqImpr <> "" Then
        SQLEstadoReq = " AND sl_requis.n_req in (select sl_impr_completas.n_req FROM sl_impr_completas WHERE seq_impr = " & EcSeqImpr & ") "
        SQlEstadoAnalise = ""
        SQLImprimeUrgentes = ""
    ElseIf tipo = Completas Then
        SQLEstadoReq = " AND (sl_requis.estado_req in ( 'D', '3','2' )) AND n_req not in (select n_req from sl_marcacoes where n_req = sl_requis.n_req and cod_ana_s <> 'S99999') "
        SQLEstadoReq = SQLEstadoReq & " AND n_req not in (select n_req from sl_realiza where n_req = sl_requis.n_req and (flg_estado not in (" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica) & ", "
        SQLEstadoReq = SQLEstadoReq & BL_TrataStringParaBD(gEstadoAnaImpressa) & ") )) "
        SQlEstadoAnalise = ""
        SQLImprimeUrgentes = ""
        
        If gLAB = "HPOVOA" Then
            SQLEstadoReq = SQLEstadoReq & " AND sl_requis.t_sit = " & gT_Consulta
            SQLEstadoReq = SQLEstadoReq & " AND sl_requis.cod_proven <> '10000' AND sl_requis.cod_proven <> '20007' "
        End If
        
    ElseIf tipo = Prontos Then
        'Verifica se existem Requisi��es com resultados prontos
        If gLAB = "HPD" Then
            SQLEstadoReq = " AND sl_requis.estado_req IN ( 'D', '3', '2' ) "
            SQlEstadoAnalise = " AND '3' IN ( SELECT flg_estado FROM sl_realiza WHERE sl_realiza.n_req = sl_requis.n_req ) "
            SQLImprimeUrgentes = " AND ( ( sl_requis.t_sit  = " & gT_Internamento & " OR sl_requis.t_sit  = " & gT_Urgencia & ") " & _
                                 " OR ( sl_requis.t_sit  IN ( " & gT_Consulta & "," & gT_Externo & ") AND sl_requis.t_urg  = 'U' ) ) "
        ElseIf gLAB = "CHVNG" Then
            SQLEstadoReq = " AND sl_requis.estado_req IN ( 'D', '3', '2' ) "
            SQlEstadoAnalise = " AND '3' IN ( SELECT flg_estado FROM sl_realiza WHERE sl_realiza.n_req = sl_requis.n_req ) "
            SQLImprimeUrgentes = " AND ( sl_requis.t_sit  IN ( " & gT_Internamento & "," & gT_Urgencia & "," & gT_Externo & "," & gT_LAB & " ) ) "
            SQLImprimeUrgentes = SQLImprimeUrgentes & "  "
        Else
            SQLEstadoReq = " AND ((sl_requis.t_sit IN (" & gT_Internamento & "," & gT_Urgencia & ") AND sl_requis.estado_req IN ( 'D', '3', '2' ) "
            SQlEstadoAnalise = " AND '3' IN ( SELECT flg_estado FROM sl_realiza WHERE sl_realiza.n_req = sl_requis.n_req and cod_ana_s <> 'S99999') )"
            
            
            SQLImprimeUrgentes = " OR ((sl_requis.t_sit IN ( " & gT_Consulta & "," & gT_Externo & _
                                 "," & gT_LAB & " )  " & _
                                 "  AND  (sl_requis.estado_req = 'D' OR sl_proven.flg_imprime_prontos = 1) ) " & _
                                 " or ( (sl_requis.t_sit IN ( " & gT_Consulta & "," & gT_Externo & _
                                 "," & gT_LAB & ")    AND sl_requis.estado_req='3' and " & _
                                 " sl_requis.n_req not in (select n_req from sl_realiza where (flg_estado in ('0','1','2','5') or cod_Ana_s = 'S99999') " & _
                                 " and sl_requis.n_req = sl_realiza.n_req))))) " & _
                                 " AND (not exists (select 1 from sl_marcacoes where n_req = sl_requis.n_req and sl_requis.t_sit in ('0','3','4')) OR sl_proven.flg_imprime_prontos = 1) "
        End If
    ElseIf tipo = ParcialmenteListados Then
        'Verifica se existem Requisi��es j� parcialmente listadas
        SQLEstadoReq = " AND sl_requis.estado_req='3' "
        SQlEstadoAnalise = " AND '3' IN ( SELECT flg_estado FROM sl_realiza WHERE sl_realiza.n_req = sl_requis.n_req ) "
        SQLImprimeUrgentes = ""
    End If
        
    'Verifica se o Utilizador tem Proveni�ncias associadas
    ListaProv = ""
    sql = "SELECT cod_proven FROM sl_util_prov WHERE cod_util=" & gCodUtilizador
    RegReq.Source = sql
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RegReq.Open
    total = RegReq.RecordCount
    For i = 1 To total
        ListaProv = ListaProv & BL_TrataStringParaBD(RegReq!cod_proven) & ","
        RegReq.MoveNext
    Next i
    RegReq.Close
    If Len(ListaProv) <> 0 Then
        ListaProv = left(ListaProv, Len(ListaProv) - 1)
    End If
    If ListaProv <> "" Then
        SQLProv = " AND sl_requis.cod_proven IS NOT NULL AND sl_requis.cod_proven IN (" & ListaProv & ")"
    Else
        SQLProv = ""
    End If
    If gLAB = "HPD" Then
        sql = "SELECT sl_requis.n_req,sl_requis.seq_utente,sl_requis.estado_req," & _
              "" & tabela_aux & ".seq_utente," & tabela_aux & ".utente," & tabela_aux & ".t_utente," & tabela_aux & ".nome_ute," & _
              "sl_proven.descr_proven, sl_Requis.dt_conclusao, sl_Requis.dt_pretend, sl_requis.cod_Efr " & _
              "FROM sl_requis," & tabela_aux & "," & IIf(gSGBD = gInformix, "OUTER sl_proven ", "sl_proven ") & ",sl_cod_salas " & _
              "WHERE sl_requis.seq_utente=" & tabela_aux & ".seq_utente " & _
              " AND (sl_proven.flg_nao_imprimir IS NULL OR sl_proven.flg_nao_imprimir = 0) " & _
              SQLEstadoReq & SQlEstadoAnalise & SQLProv & " "
    Else
        sql = "SELECT "
        sql = sql & "sl_requis.n_req,sl_requis.seq_utente,sl_requis.estado_req," & _
              "" & tabela_aux & ".seq_utente," & tabela_aux & ".utente," & tabela_aux & ".t_utente," & tabela_aux & ".nome_ute," & _
              "sl_proven.descr_proven, sl_cod_salas.descr_sala, sl_cod_salas.flg_colheita,"
        sql = sql & " sl_Requis.dt_conclusao, sl_Requis.dt_pretend, sl_requis.cod_Efr " & _
              "FROM sl_requis," & tabela_aux & "," & IIf(gSGBD = gInformix, "OUTER sl_proven ", "sl_proven ") & ",sl_cod_salas "
        sql = sql & " WHERE sl_requis.seq_utente=" & tabela_aux & ".seq_utente " & _
              " AND (sl_proven.flg_nao_imprimir IS NULL OR sl_proven.flg_nao_imprimir = 0) " & _
              SQLEstadoReq & SQlEstadoAnalise & SQLImprimeUrgentes & SQLProv & " "
    End If
    
    Select Case gSGBD
        Case gInformix
            sql = sql & "AND sl_requis.cod_proven=sl_proven.cod_proven  "
            sql = sql & "AND sl_requis.cod_sala=sl_cod_salas.cod_sala  "
        Case gOracle
            sql = sql & "AND sl_requis.cod_proven=sl_proven.cod_proven(+) "
            sql = sql & "AND sl_requis.cod_sala=sl_cod_salas.cod_sala(+)  "
        Case gSqlServer
            sql = sql & "AND sl_requis.cod_proven*=sl_proven.cod_proven  "
            sql = sql & "AND sl_requis.cod_sala*=sl_cod_salas.cod_sala  "
    End Select
    
    If Trim(EcCodProveniencia.text) <> "" Then
        sql = sql + " AND sl_requis.cod_proven='" + EcCodProveniencia.text + "'"
    End If
    If Trim(EcCodLocal.text) <> "" Then
        sql = sql + " AND sl_requis.cod_local='" + EcCodLocal.text + "'"
    End If
    
    If EcDataInicial.text <> "" Then
        sql = sql & " AND sl_requis.dt_chega >= " & BL_TrataDataParaBD(EcDataInicial.text)
    End If
    If EcDataFinal.text <> "" Then
        sql = sql & " AND sl_requis.dt_chega <= " & BL_TrataDataParaBD(EcDataFinal.text)
    End If
    
    If EcCodSala.text <> "" And gTipoInstituicao = "PRIVADA" Then
        sql = sql & " AND sl_requis.cod_sala = " & BL_TrataStringParaBD(EcCodSala.text)
    ElseIf EcCodSala.text = "" And gTipoInstituicao = "PRIVADA" Then
        sql = sql & " AND (sl_requis.cod_sala is NULL OR sl_requis.cod_sala in (SELECT cod_sala from sl_cod_salas)) "
    End If
    
    If CbUrgencia.ListIndex <> mediComboValorNull Then
        sql = sql & " AND sl_requis.t_urg = " & BL_TrataStringParaBD(CbUrgencia.ItemData(CbUrgencia.ListIndex))
    End If
    
    If CbSituacao.ListIndex <> mediComboValorNull Then
        sql = sql & " AND sl_requis.t_sit = " & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    
    If CbGenero.ListIndex <> mediComboValorNull Then
        sql = sql & " AND " & tabela_aux & ".cod_genero = " & CbGenero.ItemData(CbGenero.ListIndex)
    End If
    sql = sql & " AND " & tabela_aux & ".cod_genero IN (SELECT cod_genero FROM sl_genero WHERE flg_impr_completos = 1) "
    
    If CkIncluirEmail.value <> vbChecked Then
        sql = sql & " AND NVL(sl_requis.cod_destino, '0') not in (select cod_t_dest from sl_tbf_t_destino where flg_email = 1) "
    End If
    
    ' FGONCALVES
    ' APENAS IMPRIME REQUISI��ES ASSINADASS
    If gAssinaturaActivo = mediSim Then
        sql = sql & " AND sl_requis.n_req in (SELECT sl_req_assinatura.n_Req from sl_req_assinatura)"
    Else
        sql = sql & " AND (sl_requis.cod_destino IN (SELECT cod_t_dest FROM sl_tbf_t_destino WHERE flg_usa_assinatura IS NULL or flg_usa_assinatura = 0) "
        sql = sql & " OR (sl_requis.cod_destino IN (SELECT cod_t_dest FROM sl_tbf_t_destino WHERE flg_usa_assinatura  = 1) "
        sql = sql & " AND sl_requis.n_req in (SELECT sl_req_assinatura.n_Req from sl_req_assinatura))"
        sql = sql & " OR sl_requis.cod_destino IS NULL )"
    End If
    
    If OptImprimir(1).value = True Then
        sql = sql & " AND sl_requis.cod_sala in (SELECT sl_cod_salas.cod_sala FROM sl_cod_salas WHERE flg_interna = 0 or flg_interna is null) "
        If gLAB <> "LHL" Then
            sql = sql & " AND sl_requis.cod_destino <> " & gCodDestinoDomic
        End If
    ElseIf OptImprimir(2).value = True Then
        sql = sql & " AND (sl_requis.cod_sala in (SELECT sl_cod_salas.cod_sala FROM sl_cod_salas WHERE flg_interna = 1) "
        If gLAB <> "LHL" Then
            sql = sql & " OR sl_requis.cod_destino = " & gCodDestinoDomic
        End If
    sql = sql & ")"
    End If
    
    If gLAB = "HPD" Then
        sql = sql & " ORDER BY sl_requis.n_req"
    Else
        sql = sql & " and decode (sl_requis.cod_destino, null, -1,sl_requis.cod_destino) not in (select cod_t_dest from sl_tbf_t_destino where flg_impr_completas = 1)"
        
        If gTipoInstituicao = "PRIVADA" Then
            If gSGBD = gSqlServer Then
                If OptImprimir(1).value = True Then
                    sql = sql & " ORDER BY sl_requis.cod_sala,sl_requis.n_Req, descr_proven"
                Else
                    sql = sql & " ORDER BY sl_requis.n_Req, descr_proven"
                End If
            Else
                If OptImprimir(1).value = True Then
                    sql = sql & " ORDER BY sl_requis.cod_sala,sl_requis.n_Req, descr_proven"
                Else
                    sql = sql & " ORDER BY sl_requis.n_Req, descr_proven"
                End If
            End If
        Else
            If gLAB = "SCMVC" Then
                sql = sql & " ORDER BY sl_requis.n_req "
            Else
                sql = sql & " ORDER BY descr_proven,sl_requis.n_req"
            End If
        End If
    End If
    
    RegReq.Source = sql
    RegReq.Open
    total = RegReq.RecordCount
    
    'Verifica se existem registos para imprimir
    If total = 0 Then
        RegReq.Close
        Set RegReq = Nothing
        Call BG_Mensagem(mediMsgBox, "N�o existem requisi��es para listar!", vbOKOnly + vbInformation, App.ProductName)
        Exit Sub
    Else
        sql = "DELETE FROM sl_cr_prot WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        sql = "DELETE FROM sl_cr_notas WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        sql = "DELETE FROM sl_cr_avisos_pagamento WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        flg_gerou = False
        If EcSeqImpr = "" Then
            ssql = "SELECT max(seq_impr) maximo FROM sl_impr_completas "
            Set rsImpr = New adodb.recordset
            rsImpr.CursorType = adOpenStatic
            rsImpr.CursorLocation = adUseServer
            rsImpr.ActiveConnection = gConexao
            rsImpr.Open ssql, gConexao
            EcSeqImpr = BL_HandleNull(rsImpr!maximo + 1, 1)
            flg_gerou = True
        End If
        
        Me.Show
        Call BL_ActualizaProgressBar(ProgressBar, 0)
        
        If tipo = Completas Then
            LblTitReq.caption = "A pesquisar requisi��es completas..."
        ElseIf tipo = Prontos Then
            LblTitReq.caption = "A pesquisar requisi��es com resultados prontos..."
        ElseIf tipo = ParcialmenteListados Then
            LblTitReq.caption = "A pesquisar requisi��es parcialmente listadas..."
        End If
        
    End If
       
    '____________________________________________________________________
    
    'Cria as tabelas tempor�rias para a Impress�o dos Resultados (S� a 1� vez)
    'Call BL_ImprimeResultTabela
    
    '_________________________________________________________________________
    
    ProgressBar.ScaleWidth = total * 100
    ReDim StProt(1)
    NProt = 1
    StProt(NProt).n_req = ""
    StProt(NProt).DescrProven = ""
    StProt(NProt).dt_pretend = ""
    StProt(NProt).dt_conclusao = ""
    StProt(NProt).DescrReq = ""
    StProt(NProt).ContaReq = 1
    reqs = ""
    seqRealiza = ""
    
    FrameReq.Visible = True
    FrameReq.top = 1320
    FrameReq.left = 2040
    
    'Insere os registos nas tabelas criadas
    For i = 1 To total
    
        'VERIFICAR RESULTADOS DA REQUISI��O
        TemRes = False
        
        If gTipoInstituicao = "PRIVADA" Then
            If BL_HandleNull(RegReq!flg_colheita, "0") = 0 Then
               ProvReq = "" & BL_HandleNull(RegReq!descr_sala, "")
            Else
                ProvReq = "Laborat�rio"
            End If
        Else
            ProvReq = "" & RegReq!descr_proven
        End If
        DoEvents
        DoEvents
        DoEvents
        LbRequisicao = "Vai imprimir Req : " & RegReq!n_req
        DoEvents
        BG_LogFile_Erros "Vai imprimir Req : " & RegReq!n_req
        If CkAnexos.value = vbChecked Then
            FlgImprAnexos = False
        Else
            FlgImprAnexos = True
        End If
        gImprimirDestino = crptToPrinter
        If CkApenasNaoImpressos.value = vbChecked Then
            Call IR_ImprimeResultados(TemRes, False, RegReq!n_req, RegReq!estado_req, RegReq!seq_utente, , , , True, EcGrAnalises, , EcSGrAnalises, , , tipo, True, , , False, False, False, FlgImprAnexos, , , True)
        Else
            Call IR_ImprimeResultados(TemRes, False, RegReq!n_req, RegReq!estado_req, RegReq!seq_utente, , , , True, EcGrAnalises, , EcSGrAnalises, , , tipo, False, , , False, False, False, FlgImprAnexos, , , True)
        
        End If
        
        'SE UTENTE TIVER NOTA COM INDICACAO DE SEGUNDA VIA ENTAO IMPRIME AUTOMATICAMENTE SEGUNDA VIA
        If BL_VerificaNotas(RegReq!seq_utente, RegReq!n_req, gGrupoNotasSegVia) = True Then
            Call IR_ImprimeResultados(TemRes, False, RegReq!n_req, RegReq!estado_req, RegReq!seq_utente, , , , True, EcGrAnalises, , EcSGrAnalises, , , tipo, False, , , False, True, False, , , , True)
        End If
        
        ' PARA ENTIDADES CODIFICADAS IMPRIME RELAT�RIO PR�PRIO PARA ARS
        If gTipoInstituicao = "PRIVADA" And CkInibeImpRelARS.value = vbUnchecked And gImprimirDestino = 1 Then
            If BL_VerificaEfrImpARS(BL_HandleNull(RegReq!cod_efr, "")) = True Then
                If CkApenasNaoImpressos.value = vbChecked Then
                    Call IR_ImprimeResultados(TemRes, False, RegReq!n_req, RegReq!estado_req, RegReq!seq_utente, , , , True, EcGrAnalises, , EcSGrAnalises, , , tipo, True, , , True, False, False, FlgImprAnexos, , , True)
                Else
                    Call IR_ImprimeResultados(TemRes, False, RegReq!n_req, RegReq!estado_req, RegReq!seq_utente, , , , True, EcGrAnalises, , EcSGrAnalises, , , tipo, False, , , True, False, False, FlgImprAnexos, , , True)
                End If
            End If
        End If
        
        Call BL_ImprimeNotasRequisicao(RegReq!n_req)
        Call BL_AvisosPagamento(RegReq!n_req)
        
        LbRequisicao = "Imprimiu Req : " & RegReq!n_req
        DoEvents
        BG_LogFile_Erros "Imprimu Req: " & RegReq!n_req
        
        ' FGONCALVES
        ' PERMITE REIMPRIMIR COMPLETAS
        If flg_gerou = True Then
            BL_RegistaImprimeReq RegReq!n_req, EcSeqImpr
            'BRUNODSANTOS - SCMVC-1073
            Call BG_ExecutaQuery_ADO("UPDATE sl_realiza SET flg_estado='4' WHERE n_req = " & BL_TrataStringParaBD(RegReq!n_req))
            'IR_MudaEstadoAnaliseEcraCompletas ImprimeDoEcraReqCompletas, NumReq
            '
        Else
            ssql = "UPDATE sl_impr_completas SET "
            ssql = ssql & " user_imp2 = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            ssql = ssql & " dt_imp2 = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", hr_imp2 = " & BL_TrataStringParaBD(Bg_DaHora_ADO)
            ssql = ssql & " WHERE seq_impr = " & EcSeqImpr & " AND n_Req = " & RegReq!n_req
            BG_ExecutaQuery_ADO ssql
            BG_Trata_BDErro
        End If
        
        Sleep 100
        DoEvents
        DoEvents
        DoEvents
        

        'Dados da requisi��o:
        If TemRes = True Then
            reqs = reqs & RegReq!n_req & ","
            
            If tipo = Completas Then
                LblTitReq.caption = "Requisi��o completa n�: " & RegReq!n_req & " (" & i & "/" & total & ")"
            ElseIf tipo = Prontos Then
                LblTitReq.caption = "Requisi��o com resultados prontos n�: " & RegReq!n_req & " (" & i & "/" & total & ")"
            ElseIf tipo = ParcialmenteListados Then
                LblTitReq.caption = "Requisi��o parcialmente listada n�: " & RegReq!n_req & " (" & i & "/" & total & ")"
            End If
            
                
            '********************************************************
            'Registar Protocolo
            If Trim(ProvReq) = "" Then
                ProvReq = "Sem proveni�ncia"
            End If
            
            EncontrouProt = False
            For j = 1 To NProt
                If StProt(j).DescrProven = ProvReq And StProt(j).ContaReq < cMAX_PROT Then
                    EncontrouProt = True
                    Exit For
                End If
            Next j
            
'            If EncontrouProt = True Then
'                StProt(J).DescrReq = StProt(J).DescrReq & RegReq!n_req & " (" & BL_HandleNull(RegReq!dt_conclusao, "") & " - " & BL_HandleNull(RegReq!dt_pretend, "") & ")" & Replace(Space(9 - Len("" & RegReq!n_req)), " ", ".") & "(" & RegReq!t_utente & "/" & RegReq!Utente & ") " & Space(5 - Len("" & RegReq!t_utente)) & Space(10 - Len("" & RegReq!Utente)) & RegReq!nome_ute & Space(80 - Len(Mid(RegReq!nome_ute, 1, 79)))
'                StProt(J).ContaReq = StProt(J).ContaReq + 1
'                'StProt(j).NomeUte = StProt(j).NomeUte
'            Else
            NProt = NProt + 1
            ReDim Preserve StProt(NProt)
            StProt(NProt).DescrProven = ProvReq
            StProt(NProt).ContaReq = 1
            StProt(NProt).DescrReq = RegReq!n_req & " (" & BL_HandleNull(RegReq!dt_conclusao, "") & " - " & BL_HandleNull(RegReq!dt_pretend, "") & ")" & Replace(Space(9 - Len("" & RegReq!n_req)), " ", ".") & "(" & RegReq!t_utente & "/" & RegReq!Utente & ") " & Space(5 - Len("" & RegReq!t_utente)) & Space(10 - Len("" & RegReq!Utente)) & RegReq!nome_ute & Space(80 - Len(Mid(RegReq!nome_ute, 1, 79)))
            StProt(NProt).n_req = RegReq!n_req
            'StProt(NProt).NomeUte = RegReq!nome_ute
                
        End If
        
        'Cancelar impress�o ?
        If Flg_CancelaImp = True Then
            If BG_Mensagem(mediMsgBox, "Deseja cancelar a impress�o ?", vbYesNo + vbQuestion, App.ProductName) = vbYes Then
                Exit For
            Else
                Flg_CancelaImp = False
            End If
        End If
        
        'Actualiza a Progress Bar
        Call BL_ActualizaProgressBar(ProgressBar, 100)
        
        
        RegReq.MoveNext
        
    Next i
        
    '__________________________________________________________________________________
        
    If Flg_CancelaImp = True Then
        LblTitReq.caption = "Impress�o cancelada."
    End If
    
    If gSQLError = 0 Then
        
        'Registar Protocolo
        gSQLError = 0
        For i = 1 To NProt
            If Trim(StProt(i).DescrProven) <> "" Then
                If Trim(StProt(i).DescrReq) <> "" Then
                    gSQLError = 0
                    'RGONCALVES 11-07-2014
                    'sql = "INSERT INTO SL_CR_PROT " & _
                        " (DESCR_PROVEN, DESCR_REQ,SEQ_IMPR, NOME_COMPUTADOR, NUM_SESSAO, N_REQ) VALUES " & _
                        " (" & BL_TrataStringParaBD(StProt(i).DescrProven) & "," & StProt(i).ContaReq & "," & _
                        BL_TrataStringParaBD(StProt(i).DescrReq) & ", " & EcSeqImpr & ", " & _
                        BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & "," & _
                        StProt(i).n_req & ")"
                    sql = "INSERT INTO SL_CR_PROT " & _
                        " (DESCR_PROVEN, DESCR_REQ,SEQ_IMPR, NOME_COMPUTADOR, NUM_SESSAO, N_REQ) VALUES " & _
                        " (" & BL_TrataStringParaBD(StProt(i).DescrProven) & "," & _
                        BL_TrataStringParaBD(StProt(i).DescrReq) & ", " & EcSeqImpr & ", " & _
                        BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & "," & _
                        StProt(i).n_req & ")"
                    '
                        
                    BG_ExecutaQuery_ADO sql
                    If gSQLError <> 0 Then
                        Beep
                        Call BG_Mensagem(mediMsgBox, "Erro a criar o protocolo! ", vbOKOnly + vbCritical, "ERRO")
                        Exit For
                    End If
                End If
            End If
        Next i
        
        Dim Report As CrystalReport
        
        Set Report = MDIFormInicio.Report
        
        If gSQLError = 0 Then
            If gLAB <> "LGAS" And gLAB <> "CL" Then
                continua = BL_IniciaReport("Protocolo", "Protocolo de impress�o", crptToPrinter, , , , , , , "Imprimir Protocolo")
                If continua = True Then
                    Report.SQLQuery = "SELECT DESCR_PROVEN, DESCR_REQ, N_REQ " & _
                                      " FROM SL_CR_PROT WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
                    If gLAB = "SCMVC" Then
                        Report.SQLQuery = Report.SQLQuery & " ORDER BY N_REQ ASC "
                        Report.formulas(0) = "OrderReq=" & BL_TrataStringParaBD("S")
                    Else
                        Report.SQLQuery = Report.SQLQuery & " ORDER BY DESCR_PROVEN ASC , N_REQ ASC "
                        Report.formulas(0) = "OrderReq=" & BL_TrataStringParaBD("N")
                    End If
                
                    Call BL_ExecutaReport
                End If
                
                If gTipoInstituicao = "PRIVADA" Then
                    'soliveira ualia criar chaves para imprimir report Notas e Avisos de Pagamento
                    If gLAB <> "UALIA" Then
                    
                        If gLAB = "LHL" Then
                            Set Report = MDIFormInicio.Report
                            continua = BL_IniciaReport("Notas", "Notas", crptToPrinter, , , , , , , "Notas")
                            Report.SQLQuery = " SELECT " & _
                                             " SL_CR_NOTAS.n_req , SL_CR_NOTAS.descr_ana, SL_CR_NOTAS.descr_obs, SL_CR_NOTAS.dt_cri " & _
                                             " From SL_CR_NOTAS Where nome_computador = " & BL_TrataStringParaBD(gComputador) & _
                                             " AND num_sessao = " & gNumeroSessao & " order By SL_CR_NOTAS.n_req Asc "
                            Call BL_ExecutaReport
                            
                        End If
                    
                        ' AVISOS PAGAMENTO
                        Set Report = MDIFormInicio.Report
                        continua = BL_IniciaReport("AvisosPagamento", "Avisos Pagamento", crptToPrinter, , , , , , , "AvisosPagamento")
                        Report.SQLQuery = " SELECT SL_CR_AVISOS_PAGAMENTO.N_REQ, SL_CR_AVISOS_PAGAMENTO.COD_EFR, SL_CR_AVISOS_PAGAMENTO.TAXA, SL_CR_AVISOS_PAGAMENTO.TOTAL_PAGAR, "
                        Report.SQLQuery = Report.SQLQuery & " SL_COD_SALAS.DESCR_SALA, SL_EFR.DESCR_EFR, SLV_ANALISES_FACTUS.descr_ana "
                        Report.SQLQuery = Report.SQLQuery & " FROM (SL_CR_AVISOS_PAGAMENTO SL_CR_AVISOS_PAGAMENTO INNER JOIN SL_EFR SL_EFR ON SL_CR_AVISOS_PAGAMENTO.COD_EFR = SL_EFR.COD_EFR) "
                        Report.SQLQuery = Report.SQLQuery & " INNER JOIN SLV_ANALISES_FACTUS SLV_ANALISES_FACTUS ON SL_CR_AVISOS_PAGAMENTO.COD_FACTURAVEL = SLV_ANALISES_FACTUS.cod_ana "
                        Report.SQLQuery = Report.SQLQuery & " INNER JOIN SL_COD_SALAS SL_COD_SALAS ON SL_CR_AVISOS_PAGAMENTO.cod_sala = SL_COD_SALAS.cod_sala "
                        Report.SQLQuery = Report.SQLQuery & " WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
                        Report.SQLQuery = Report.SQLQuery & " order By SL_CR_AVISOS_PAGAMENTO.N_REQ ASC,"
                        Report.SQLQuery = Report.SQLQuery & " SL_CR_AVISOS_PAGAMENTO.cod_efr Asc"
                        Call BL_ExecutaReport
                    End If
                End If
            End If
            
            LblTitReq.caption = "Impress�o completa."
        Else
            GoTo Trata_Erro
        End If
    
    End If
    
    Report.Reset
        
    
    sql = "DELETE FROM sl_cr_notas WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    sql = "DELETE FROM sl_cr_avisos_pagamento WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql

    'Desaloca RecordSet principal
    RegReq.Close
    Set RegReq = Nothing
    
    If gSQLError <> 0 Then GoTo Trata_Erro

    BtSair.Enabled = False
    
    Exit Sub
    
Trata_Erro:
    
    BG_LogFile_Erros "FormListarResMult (ImprimeReqMult) :" & Err.Number & "/" & Err.Description & " --- SQL(" & gSQLError & ")"
    If Flg_CancelaImp = False Then
        Beep
    End If
    If Not Report Is Nothing Then
        Report.Reset
    End If
    Call BG_Mensagem(mediMsgStatus, "Erro na Impress�o!")
    BtSair.Enabled = False
    'Desaloca RecordSet principal
    Set RegReq = Nothing
    Exit Sub
    Resume Next
End Sub

Private Sub BtImprimir_Click()
    
    BtSair.SetFocus
    BtImprimir.Enabled = False
    EcGrAnalises.Enabled = False
    BtPesquisaRapidaGrAnalises.Enabled = False
    LblTitReq.Visible = True
    ProgressBar.Visible = True
    
    ImprimeReqMult TipoImp
    FrameReq.Visible = False
    LbRequisicao = ""
    
    If BG_Mensagem(mediMsgBox, "Pretende voltar a imprimir resultados?", vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo) = vbYes Then
        EcSeqImpr = ""
        BtImprimir.Enabled = True
        EcGrAnalises.Enabled = True
        BtPesquisaRapidaGrAnalises.Enabled = True
        LblTitReq.Visible = False
        ProgressBar.Visible = False
        BtSair.Enabled = True
    End If
End Sub

Private Sub BtPesqLocal_Click()
    PA_PesquisaLocal EcCodLocal, EcDescrLocal
End Sub

Private Sub BtPesquisaProveniencia_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar Proveniencias")
    
    mensagem = "N�o foi encontrada nenhuma proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.text = Resultados(1)
            EcDescrProveniencia.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcCodProveniencia.SetFocus
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao CbSituacao, KeyCode
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUrgencia, KeyCode

End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim Tabela As adodb.recordset
    Dim sql As String
    
    If EcCodProveniencia.text <> "" Then
        Set Tabela = New adodb.recordset
    
        sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.text = ""
            EcDescrProveniencia.text = ""
        Else
            EcDescrProveniencia.text = Tabela!descr_proven
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.text = ""
    End If
    
End Sub

Private Sub BtPesquisaRapidaGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcGrAnalises.text = Resultados(1)
            EcDescrGrAnalises.text = Resultados(2)
            EcSGrAnalises.Enabled = True
            BtPesquisaRapidaSGrAnalises.Enabled = True
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcGrAnalises.SetFocus
    
End Sub

Private Sub BtPesquisaRapidaSGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_sgr_ana"
    CamposEcran(1) = "cod_sgr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_sgr_ana"
    CamposEcran(2) = "descr_sgr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_sgr_ana"
    CampoPesquisa1 = "descr_sgr_ana"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Sub Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcSGrAnalises.text = Resultados(1)
            EcDescrSGrAnalises.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcSGrAnalises.SetFocus
    
End Sub


Private Sub BtSair_Click()
    
    If BtImprimir.Enabled Then
        Unload Me
    Else
        Flg_CancelaImp = True
    End If

End Sub


Private Sub EcDataInicial_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDataInicial_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub

Private Sub EcDataFinal_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDataFinal_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub

Private Sub EcGrAnalises_Change()
    
    EcDescrGrAnalises.text = ""

End Sub

Private Sub EcGrAnalises_Validate(Cancel As Boolean)
    
    Dim RsDescrGrAnalises As adodb.recordset
       
    If Trim(EcGrAnalises.text) <> "" Then
        Set RsDescrGrAnalises = New adodb.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcGrAnalises.text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount <> 0 Then
            EcDescrGrAnalises.text = RsDescrGrAnalises!descr_gr_ana
            EcSGrAnalises.Enabled = True
            BtPesquisaRapidaSGrAnalises.Enabled = True
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrGrAnalises.text = ""
            BG_Mensagem mediMsgBox, "O Grupo de An�lise indicado n�o existe!", vbOKOnly, App.ProductName
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrAnalises.text = ""
        EcSGrAnalises.text = ""
        EcSGrAnalises.Enabled = False
        BtPesquisaRapidaSGrAnalises.Enabled = False
    End If
    
End Sub

Private Sub EcSGrAnalises_Change()
    
    EcDescrSGrAnalises.text = ""

End Sub

Private Sub EcSGrAnalises_Validate(Cancel As Boolean)
    
    Dim RsDescrSGrAnalises As adodb.recordset
       
    If Trim(EcSGrAnalises.text) <> "" Then
        Set RsDescrSGrAnalises = New adodb.recordset
        
        With RsDescrSGrAnalises
            .Source = "SELECT descr_sgr_ana FROM sl_sgr_ana WHERE cod_sgr_ana= " & BL_TrataStringParaBD(EcSGrAnalises.text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrSGrAnalises.RecordCount <> 0 Then
            EcDescrSGrAnalises.text = RsDescrSGrAnalises!descr_sgr_ana
            RsDescrSGrAnalises.Close
            Set RsDescrSGrAnalises = Nothing
        Else
            RsDescrSGrAnalises.Close
            Set RsDescrSGrAnalises = Nothing
            EcDescrSGrAnalises.text = ""
            BG_Mensagem mediMsgBox, "O Sub Grupo de An�lise indicado n�o existe!", vbOKOnly, App.ProductName
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrSGrAnalises.text = ""
    End If
    
End Sub

Private Sub Form_Activate()
    
    BG_StackJanelas_Actualiza Me
    
    Set gFormActivo = Me
    EcDataInicial.SetFocus
    
End Sub

Private Sub Form_Load()
    Dim lNaoImprDefeitoImpressas As Integer
   
    lNaoImprDefeitoImpressas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NAO_IMPR_DEFEITO_IMPRESSAS")
    
    If lNaoImprDefeitoImpressas = mediSim Then
        CkApenasNaoImpressos.value = vbChecked
    Else
        CkApenasNaoImpressos.value = vbUnchecked
    End If
    
    gF_RESMULT = 1
    
    If TipoImp = Completas Then
        Me.caption = "Impress�o de requisi��es completas"
    ElseIf TipoImp = ParcialmenteListados Then
        Me.caption = "Impress�o de requisi��es parcialmente listadas"
    ElseIf TipoImp = Prontos Then
        Me.caption = "Impress�o de requisi��es prontas"
    End If
    
    If gCodGrAnaUtilizador <> "" And BL_SeleccionaGrupoAnalises = True Then
        EcGrAnalises = gCodGrAnaUtilizador
        EcGrAnalises_Validate False
    End If
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    
    
    LblTitReq.caption = ""
    
    BG_StackJanelas_Push Me
    
    'Inicializar a barra de progresso
    ProgressBar.FontBold = True
    ProgressBar.AutoRedraw = True
    ProgressBar.BackColor = vbWhite
    ProgressBar.DrawMode = 10
    ProgressBar.FillStyle = 0
    ProgressBar.ForeColor = vbWhite
    ProgressBar.ForeColor = &H800000
    ProgressBar.ScaleWidth = 100
    
    'Tipo Data
    EcDataInicial.Tag = "104"
    EcDataFinal.Tag = "104"
    EcDataFinal = Bg_DaData_ADO
    
    EcSGrAnalises.Enabled = False
    BtPesquisaRapidaSGrAnalises.Enabled = False
    If gTipoInstituicao = "PRIVADA" Then
        LbPosto.Visible = True
        EcCodSala.Visible = True
        EcDescrSala.Visible = True
        BtPesquisaSala.Visible = True
    Else
        LbPosto.Visible = False
        EcCodSala.Visible = False
        EcDescrSala.Visible = False
        BtPesquisaSala.Visible = False
    End If
    If gCodSalaAssocUser <> 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate False
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
        OptImprimir(1).value = True
        OptImprimir(0).Enabled = False
        OptImprimir(2).Enabled = False
        OptImprimir(1).Enabled = False
    Else
        EcCodSala = ""
        EcCodsala_Validate True
        EcCodSala.Enabled = True
        BtPesquisaSala.Enabled = True
        OptImprimir(0).value = True
    End If
    EcCodLocal.text = gCodLocal
    EcCodlocal_Validate False
    If gLAB <> "CL" Then
        OptImprimir(0).value = True
    End If
    FrameReq.Visible = False
    LbRequisicao = ""
    BG_PreencheComboBD_ADO "sl_genero", "cod_genero", "descr_genero", CbGenero
End Sub

Private Sub Form_Unload(Cancel As Integer)
    gF_RESMULT = 0
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormListarResMult = Nothing
    
End Sub

Private Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
End Sub
Private Sub EcCodlocal_Validate(Cancel As Boolean)
    Cancel = PA_ValidateLocal(EcCodLocal, EcDescrLocal, "")
End Sub
Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
    
End Sub


Sub ActualizaEstados()
    Dim rs As adodb.recordset
    Dim sql As String
    Dim i As Long
    
    BL_InicioProcessamento Me, "A actualiza estados..."
    
    sql = "SELECT n_req FROM sl_requis WHERE estado_req <> 'F' AND dt_cri BETWEEN sysdate - 30 and sysdate "
    Set rs = New adodb.recordset
    With rs
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .ActiveConnection = gConexao
        .Open
    
        i = 0
        While Not .EOF
            i = i + 1
            Call BL_MudaEstadoReq(BL_HandleNull(!n_req, 0))
            .MoveNext
        Wend
            
        .Close
    End With
    
    Set rs = Nothing
    
    BL_FimProcessamento Me, "Processamento completo."
End Sub

Private Sub CbGenero_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbGenero.ListIndex = -1

End Sub

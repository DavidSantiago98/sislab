Attribute VB_Name = "ModuleFilesHandle"
'      .............................
'     .                             .
'    .   Paulo Ferreira 2010.01.22   .
'     .      � 2010 GLINTT-HS       .
'      .............................

Option Explicit

Private Const GENERIC_READ As Long = &H80000000
Private Const INVALID_HANDLE_VALUE As Long = -1
Private Const OPEN_EXISTING As Long = 3
Private Const FILE_ATTRIBUTE_NORMAL As Long = &H80
Private Const MAX_PATH As Long = 260

Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * MAX_PATH
   cAlternate As String * 14
End Type

Public Type tUserDetails
    Id As Integer
    Name As String * 20
    Content As String
End Type

Public Enum FILE_FEEDBACK
   FILE_IN_USE = -1
   FILE_FREE = 0
   FILE_DOESNT_EXIST = -999
End Enum

Public Enum eOpenType
    eOpenUpdate
    eOpenRead
    eOpenNone
End Enum

Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hFile As Long) As Long
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long

  
Public Function FileLock(sFilePathName As String, Optional eUpdateFile As eOpenType = eOpenUpdate, Optional lRecordLength As Long = -1, Optional lRecordNumber As Long = -1, Optional iFreeFile As Integer = 0) As Integer
    
    On Error GoTo ErrFailed
    
    'If (IsFileInUse(sFilePathName)) Then: GoTo ErrFailed
    If (IsFileInUse(sFilePathName) = FILE_IN_USE) Then: GoTo ErrFailed
    Select Case (eUpdateFile)
    Case eOpenUpdate:
        iFreeFile = FreeFile
        If (lRecordLength = -1) Then
            Open sFilePathName For Append As #iFreeFile
            'Open sFilePathName For Random Shared As iFreeFile
        Else
            'Open sFilePathName For Random Shared As iFreeFile Len = lRecordLength
             Open sFilePathName For Append As #iFreeFile Len = lRecordLength
        End If
    Case eOpenRead:
        iFreeFile = FreeFile
        If (lRecordLength = -1) Then
            Open sFilePathName For Input As #iFreeFile
            ' Open sFilePathName For Random Shared As iFreeFile
        Else
            ' Open sFilePathName For Random Shared As iFreeFile Len = lRecordLength
             Open sFilePathName For Input As #iFreeFile Len = lRecordLength
        End If
    Case eOpenNone:
        ' Empty
    End Select
    If (lRecordNumber = -1) Then
        Lock #iFreeFile
    Else
        Lock #iFreeFile, lRecordNumber
    End If
    FileLock = iFreeFile
    Exit Function

ErrFailed:
    FileLock = -1
    Exit Function
    Resume Next
End Function

Public Function FileUnLock(ByRef iFileHandle As Integer, Optional lRecordNumber As Long = -1, Optional bCloseFile As Boolean = True) As Boolean

    On Error GoTo ErrFailed
    
    If (iFileHandle) Then
        If (lRecordNumber = -1) Then
            Unlock iFileHandle%
        Else
            Unlock iFileHandle%, lRecordNumber
        End If
        If (bCloseFile) Then
            Close #iFileHandle
            iFileHandle = 0
        End If
        FileUnLock = True
    End If
    Exit Function

ErrFailed:
    FileUnLock = -1
    
End Function

Private Function IsFileInUse(sFile As String) As FILE_FEEDBACK

    Dim hFile As Long
   
    If (FileExists(sFile)) Then
        hFile = CreateFile(sFile, GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0&)
        IsFileInUse = hFile = INVALID_HANDLE_VALUE
        CloseHandle hFile
    Else
        IsFileInUse = FILE_DOESNT_EXIST
    End If
   
End Function

Private Function FileExists(sSource As String) As Boolean

   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
   
   hFile = FindFirstFile(sSource, WFD)
   FileExists = (hFile <> INVALID_HANDLE_VALUE)
   Call FindClose(hFile)
   
End Function



VERSION 5.00
Begin VB.Form FormEtiqAvulso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5475
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3480
   Icon            =   "FormEtiqAvulso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   3480
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Top             =   4200
      Width           =   735
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   1335
   End
   Begin VB.ListBox EcLista 
      Height          =   2400
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   1815
   End
   Begin VB.CommandButton BtImprimirEtiquetas 
      Caption         =   "Imprimir"
      Height          =   615
      Left            =   2160
      Picture         =   "FormEtiqAvulso.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2880
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   2040
      Picture         =   "FormEtiqAvulso.frx":0156
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   720
      Width           =   495
   End
   Begin VB.TextBox txtFraccao 
      Height          =   285
      Left            =   1560
      TabIndex        =   0
      Top             =   720
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "Imprime uma etiqueta com um n� e o respectivo c�digo de barras"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   3255
   End
End
Attribute VB_Name = "FormEtiqAvulso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object


Dim erroTmp As Double

Private Type Dados_Etiqueta
    codigo As String
    Nome As String
End Type

Dim estrutEtiq() As Dados_Etiqueta
Dim tamEstrutEtiqueta As Integer

Dim NomeDoente As String

Private Sub BtImprimirEtiquetas_Click()

    FuncaoImprimirEtiquetas
    
End Sub

Private Sub Command1_Click()

    If Len(Trim(EcCodigo.text)) > 0 Then
            
        NomeDoente = ""

        ' n�o passam resultados, logo n�o � necess�rio verificar nada
        '
        Call PreencheEstrutura(EcCodigo.text, NomeDoente)
        Call EcLista.AddItem(EcCodigo.text)
        
        EcCodigo.text = ""
        txtFraccao.text = ""
        BtImprimirEtiquetas.Enabled = True
          
    End If

End Sub

Private Sub EcCodigo_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 And Len(Trim(EcCodigo.text)) > 0 Then

        ' logo n�o � necess�rio verificar nada
        '
        Call PreencheEstrutura(EcCodigo.text, "")
        Call EcLista.AddItem(EcCodigo.text)
        
        EcCodigo.text = ""
        txtFraccao.text = ""
        BtImprimirEtiquetas.Enabled = True

    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Gerar etiquetas"
    Me.left = 540
    Me.top = 50
    Me.Width = 3555
    Me.Height = 4065 ' Normal
    'Me.Height = 5000 ' Campos Extras
    
    Set CampoDeFocus = EcCodigo
    
    BtImprimirEtiquetas.Enabled = False
    
    EcPrinterEtiq.text = BL_SelImpressora("Etiqueta.rpt")
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    DefTipoCampos
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormEtiqAvulso = Nothing
    
End Sub

Sub LimpaCampos()
    
    BtImprimirEtiquetas.Enabled = False
    EcLista.Clear
    EcCodigo.text = ""
    tamEstrutEtiqueta = 0
    ReDim estrutEtiq(tamEstrutEtiqueta)
    
End Sub

Sub DefTipoCampos()
    
    EcCodigo.Tag = adInteger
    
End Sub

Sub PreencheCampos()
    'nada
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    'nada
End Sub

Function ValidaCamposEc() As Integer
    'nada
End Function

Sub FuncaoProcurar()
    'nada
End Sub

Sub FuncaoAnterior()
    'nada
End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub BD_Insert()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub BD_Update()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub

Sub BD_Delete()
    'nada
End Sub

Sub PreencheValoresDefeito()
    'nada
End Sub

Sub FuncaoImprimirEtiquetas()

    Dim i As Integer
    Dim Impressora As String
    Dim nCopias As Integer
    Dim Nome1 As String
    Dim Nome2 As String
    Dim sBufferDefeito As String
    Dim sSistemaDefeito As String
    Dim sSistema As String
    Dim sBuffer As String
    Dim sBufferWININI As String
    
    On Error GoTo TrataErro
    
    If EcLista.ListCount = 0 Then
        BG_Mensagem mediMsgBox, "Introduza um c�digo!", vbExclamation
        EcCodigo.SetFocus
        Exit Sub
    End If


    BG_Mensagem mediMsgStatus, "A Imprimir...", mediMsgBeep + mediMsgPermanece
    DoEvents

    If Not BL_LerEtiqIni("ETIQ") Then
        BG_Mensagem mediMsgBox, "Ficheiro de impressao n�o foi encontrado.", vbExclamation, App.ProductName
    Else
        For i = 1 To tamEstrutEtiqueta
            If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
                BG_Mensagem mediMsgBox, "Impressora n�o est� disponivel.", vbExclamation, App.ProductName
                Exit Sub
            Else
                EtiqPrint estrutEtiq(i).codigo, estrutEtiq(i).Nome
                
                BL_EtiqClosePrinter
            End If
        Next i
    End If
    
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    
    Exit Sub
    
TrataErro:
    
    BG_Mensagem mediMsgBox, "Erro ao imprimir." & vbCrLf & Err.Description, vbCritical, App.ProductName
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece

End Sub

Private Function EtiqPrint(ByVal CodColheita As String, Nome As String) As Boolean

    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{COD_COLHEITA}", CodColheita)
    sWrittenData = Replace(sWrittenData, "{NOME}", Nome)
    
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function



Private Sub PreencheEstrutura(codigo As String, Nome As String)

    tamEstrutEtiqueta = tamEstrutEtiqueta + 1
    ReDim Preserve estrutEtiq(tamEstrutEtiqueta)
    
    estrutEtiq(tamEstrutEtiqueta).codigo = codigo
    estrutEtiq(tamEstrutEtiqueta).Nome = Nome

End Sub





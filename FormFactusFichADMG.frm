VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form FormFactusFichADMG 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactusFichADMG"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15060
   Icon            =   "FormFactusFichADMG.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   15060
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAbrir 
      Caption         =   "Abrir"
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   855
   End
   Begin VB.CommandButton BtGravar 
      Caption         =   "Gravar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1080
      TabIndex        =   3
      Top             =   0
      Width           =   975
   End
   Begin VB.TextBox EcAux 
      Height          =   375
      Left            =   480
      TabIndex        =   0
      Top             =   1800
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid FgAnalises 
      Height          =   10335
      Left            =   9240
      TabIndex        =   1
      Top             =   2280
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   18230
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      BorderStyle     =   0
   End
   Begin MSFlexGridLib.MSFlexGrid FgRequisicoes 
      Height          =   10335
      Left            =   360
      TabIndex        =   2
      Top             =   720
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   18230
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      BorderStyle     =   0
   End
   Begin MSComDlg.CommonDialog NomeFicheiro 
      Left            =   12000
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label LbFactura 
      Caption         =   "Factura: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2640
      TabIndex        =   7
      Top             =   120
      Width           =   3255
   End
   Begin VB.Label LbTotalRequisicoes 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10680
      TabIndex        =   6
      Top             =   1080
      Width           =   5175
   End
   Begin VB.Label LbTotalUtente 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10680
      TabIndex        =   5
      Top             =   1560
      Width           =   5175
   End
End
Attribute VB_Name = "FormFactusFichADMG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim sNomeFicheiro As String
Dim CabH1 As String
Dim RodT1 As String
Dim gColuna As Integer

Private Type Analises
    Campo1 As String
    Campo2 As String
    Campo3 As String
    Campo4 As String
    Campo5 As String
    Campo6 As String
    Campo7 As String
    Campo8 As String
    Campo9 As String
    Campo10 As String
End Type

Private Type Requisicao
    Campo1 As String
    Campo2 As String
    Campo3 As String
    Campo4 As String
    Campo5 As String
    Campo6 As String
    Campo7 As String
    Campo8 As String
    Campo9 As String
    EstrutAnalises() As Analises
    TotalAnalises As Integer

End Type

Dim EstrutRequisicao() As Requisicao
Dim TotalRequisicao As Integer


Public Sub Main()

End Sub

Private Sub BtAbrir_Click()
    Dim iFile As Integer
    Dim sStr1 As String
    Dim conteudo() As String
    Dim TotAna As Integer
    Dim CompH2 As Integer
    Dim DifH2 As Integer

    NomeFicheiro.Filter = "Ficheiro Texto (*.TXT)|*.TXT"

    sNomeFicheiro = DevolveNomeFicheiro
    If sNomeFicheiro = "" Then
        Exit Sub
    End If

    BtGravar.Enabled = True
    
    TotalRequisicao = 0
    TotAna = 0
    ReDim EstrutRequisicao(TotalRequisicao)
    
    CompH2 = 65
    FgRequisicoes.rows = 2
    FgRequisicoes.TextMatrix(1, 0) = ""
    FgRequisicoes.TextMatrix(1, 1) = ""
    FgRequisicoes.TextMatrix(1, 2) = ""
    FgRequisicoes.TextMatrix(1, 3) = ""
    FgRequisicoes.TextMatrix(1, 4) = ""
    FgRequisicoes.TextMatrix(1, 5) = ""
    FgRequisicoes.TextMatrix(1, 6) = ""
    
    iFile = FreeFile
    Open sNomeFicheiro For Input As #iFile
        Do While Not EOF(iFile)
            Line Input #1, sStr1

            If Mid(sStr1, 1, 2) = "H1" Then
                CabH1 = sStr1
                LbFactura.caption = "Factura: " & Mid(sStr1, 35, 4) & "/" & Mid(sStr1, 39, 5)
            ElseIf Mid(sStr1, 1, 2) = "H2" Then
                TotalRequisicao = TotalRequisicao + 1
                ReDim Preserve EstrutRequisicao(TotalRequisicao)
                
                DifH2 = CompH2 - Len(sStr1)
                EstrutRequisicao(TotalRequisicao).Campo1 = Mid(sStr1, 1, 2)
                EstrutRequisicao(TotalRequisicao).Campo2 = Mid(sStr1, 3, 9)
                EstrutRequisicao(TotalRequisicao).Campo3 = Mid(sStr1, 12, 8)
                EstrutRequisicao(TotalRequisicao).Campo4 = Mid(sStr1, 20, 10 - DifH2)
                EstrutRequisicao(TotalRequisicao).Campo5 = Mid(sStr1, 30 - DifH2, 11)
                EstrutRequisicao(TotalRequisicao).Campo6 = Mid(sStr1, 41 - DifH2, 11)
                EstrutRequisicao(TotalRequisicao).Campo7 = Mid(sStr1, 52 - DifH2, 11)
                EstrutRequisicao(TotalRequisicao).Campo8 = Mid(sStr1, 63 - DifH2, 3)

                EstrutRequisicao(TotalRequisicao).TotalAnalises = 0
                ReDim EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises)

                FgRequisicoes.TextMatrix(TotalRequisicao, 0) = EstrutRequisicao(TotalRequisicao).Campo2
                FgRequisicoes.TextMatrix(TotalRequisicao, 1) = Mid(EstrutRequisicao(TotalRequisicao).Campo3, 1, 4) & "/" & Mid(EstrutRequisicao(TotalRequisicao).Campo3, 5, 2) & "/" & Mid(EstrutRequisicao(TotalRequisicao).Campo3, 7)
                FgRequisicoes.TextMatrix(TotalRequisicao, 2) = EstrutRequisicao(TotalRequisicao).Campo4
                FgRequisicoes.TextMatrix(TotalRequisicao, 3) = CDbl(EstrutRequisicao(TotalRequisicao).Campo5) / 100 & " �"
                FgRequisicoes.TextMatrix(TotalRequisicao, 4) = CDbl(EstrutRequisicao(TotalRequisicao).Campo6) / 100 & " �"
                FgRequisicoes.TextMatrix(TotalRequisicao, 5) = CDbl(EstrutRequisicao(TotalRequisicao).Campo7) / 100 & " �"
                If DifH2 = 0 Then
                    FgRequisicoes.TextMatrix(TotalRequisicao, 6) = ""
                Else
                    FgRequisicoes.TextMatrix(TotalRequisicao, 6) = "NB"
                End If
                FgRequisicoes.AddItem ""

            ElseIf Mid(sStr1, 1, 2) = "D2" Then
                EstrutRequisicao(TotalRequisicao).TotalAnalises = EstrutRequisicao(TotalRequisicao).TotalAnalises + 1
                ReDim Preserve EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo1 = Mid(sStr1, 1, 2)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo2 = Mid(sStr1, 3, 3)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo3 = Mid(sStr1, 6, 10)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo4 = Mid(sStr1, 16, 8)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo5 = Mid(sStr1, 24, 3)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo6 = Mid(sStr1, 27, 11)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo7 = Mid(sStr1, 38, 11)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo8 = Mid(sStr1, 49, 11)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo9 = Mid(sStr1, 60, 3)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo10 = Mid(sStr1, 63, 64)

                TotAna = TotAna + CInt(EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo5)
            ElseIf Mid(sStr1, 1, 2) = "T2" Then
                EstrutRequisicao(TotalRequisicao).Campo9 = Mid(sStr1, 1, 7)
            ElseIf Mid(sStr1, 1, 2) = "T1" Then
                RodT1 = sStr1
            End If
        Loop
    Close #iFile

    LbTotalRequisicoes.caption = TotalRequisicao & " Requisi��es " & TotAna & " An�lises "
    FgRequisicoes.Row = 1
    FgRequisicoes.Col = 0
    FgRequisicoes_Click
    FgRequisicoes.SetFocus
    
End Sub

Private Function DevolveNomeFicheiro() As String
    DevolveNomeFicheiro = ""
    
    NomeFicheiro.FilterIndex = 1
    NomeFicheiro.CancelError = True
    NomeFicheiro.InitDir = "C:\CPCHS\"
    NomeFicheiro.FileName = ""
    On Error Resume Next
    NomeFicheiro.DialogTitle = "Nome do ficheiro"
    NomeFicheiro.ShowOpen
    
    DevolveNomeFicheiro = NomeFicheiro.FileName

End Function

Private Sub FGRequisicoes_DblClick()
    
    gColuna = FgRequisicoes.Col
    If gColuna = 0 Then
        EcAux = Trim(EstrutRequisicao(FgRequisicoes.Row).Campo2)
        EcAux.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAux.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAux.Width = FgRequisicoes.CellWidth + 10
        EcAux.Height = FgRequisicoes.CellHeight + 20
        EcAux.Visible = True
        EcAux.SetFocus
    ElseIf gColuna = 2 Then
        EcAux = Trim(EstrutRequisicao(FgRequisicoes.Row).Campo4)
        EcAux.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAux.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAux.Width = FgRequisicoes.CellWidth + 10
        EcAux.Height = FgRequisicoes.CellHeight + 20
        EcAux.Visible = True
        EcAux.SetFocus
    End If
End Sub

Private Sub FgRequisicoes_Click()
    Dim i As Integer
    Dim TotAna As Integer
    
    gColuna = -1
    TotAna = 0
    FgAnalises.rows = 2
    FgAnalises.TextMatrix(1, 0) = ""
    FgAnalises.TextMatrix(1, 1) = ""
    FgAnalises.TextMatrix(1, 2) = ""
    FgAnalises.TextMatrix(1, 3) = ""
    FgAnalises.TextMatrix(1, 4) = ""
    FgAnalises.TextMatrix(1, 5) = ""
    If FgRequisicoes.Row <= TotalRequisicao Then
        For i = 1 To EstrutRequisicao(FgRequisicoes.Row).TotalAnalises
            FgAnalises.TextMatrix(i, 0) = EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo3
            FgAnalises.TextMatrix(i, 1) = Mid(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo4, 1, 4) & "/" & Mid(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo4, 5, 2) & "/" & Mid(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo4, 7)
            FgAnalises.TextMatrix(i, 2) = CInt(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo5)
            FgAnalises.TextMatrix(i, 3) = CDbl(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo6) / 100 & " �"
            FgAnalises.TextMatrix(i, 4) = CDbl(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo7) / 100 & " �"
            FgAnalises.TextMatrix(i, 5) = CDbl(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo8) / 100 & " �"
            TotAna = TotAna + CInt(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo5)
            FgAnalises.AddItem ""
        Next
    End If
    LbTotalUtente = TotAna & " An�lises "
End Sub


Private Sub FgRequisicoes_RowColChange()
    FgRequisicoes_Click
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcAux_LostFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    Dim i As Integer
    
    If gColuna = 0 Then
        EcAux = left(UCase(EcAux) & "         ", 9)
        EstrutRequisicao(FgRequisicoes.Row).Campo2 = EcAux
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 0) = EcAux
    ElseIf gColuna = 2 Then
        EcAux = left(UCase(EcAux) & "         ", 10)
        EstrutRequisicao(FgRequisicoes.Row).Campo4 = EcAux
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 2) = EcAux
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) = ""
    End If
    EcAux.Visible = False
    FgRequisicoes.SetFocus
End Sub

Private Sub FgRequisicoes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FGRequisicoes_DblClick
    End If
End Sub

Private Sub BtGravar_Click()
    Dim iFile As Integer
    Dim i As Integer
    Dim j As Integer
    Dim sStr1 As String
        
    On Error GoTo TrataErro

    FileCopy sNomeFicheiro, Replace(sNomeFicheiro, ".TXT", ".BAK")
    Kill sNomeFicheiro
    
    iFile = FreeFile
    Open sNomeFicheiro For Append As #iFile
        Print #iFile, CabH1
        For i = 1 To TotalRequisicao
            sStr1 = EstrutRequisicao(i).Campo1 & EstrutRequisicao(i).Campo2 & _
                EstrutRequisicao(i).Campo3 & EstrutRequisicao(i).Campo4 & _
                EstrutRequisicao(i).Campo5 & EstrutRequisicao(i).Campo6 & _
                EstrutRequisicao(i).Campo7 & EstrutRequisicao(i).Campo8
            Print #iFile, sStr1
            For j = 1 To EstrutRequisicao(i).TotalAnalises
                sStr1 = EstrutRequisicao(i).EstrutAnalises(j).Campo1 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo2 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo3 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo4 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo5 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo6 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo7 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo8 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo9 & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo10
                Print #iFile, sStr1
            Next
            Print #iFile, EstrutRequisicao(i).Campo9
        Next
    Print #iFile, RodT1
    Close #iFile
    BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Gravar"
Exit Sub

TrataErro:
    BG_Mensagem mediMsgBox, "Erro ao gravar o ficheiro - " & Err.Description, vbInformation, "Gravar"

End Sub






Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub



Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call BL_InicioProcessamento(Me, "Inicializar �cran.")
    
    'Preenche Combos
    Call PreencheValoresDefeito
    
    'Define Campos do ecran e das tabelas
    Call Inicializacoes
    
    'Define o tipo de campos
    Call DefTipoCampos
    
    Call BG_ParametrizaPermissoes_ADO(Me.Name)
    
    Estado = 1
    Call BG_StackJanelas_Push(Me)
    Call BL_FimProcessamento(Me)
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormFactusFichADMG = Nothing

End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    Me.caption = " ADMG "
    
    Me.left = 800
    Me.top = 800
    Me.Width = 17960
    Me.Height = 8610
    
    
    Set CampoDeFocus = BtAbrir
    
    NomeTabela = ""
    
    NumCampos = 0
        
End Sub
Sub DefTipoCampos()
        With FgRequisicoes
        .Cols = 7
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(0) = 1065
        .TextMatrix(0, 0) = "N� Recibo"
        
        .ColAlignment(1) = flexAlignLeftCenter
        .ColWidth(1) = 1005
        .TextMatrix(0, 1) = "Data Recibo"
        
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(2) = 1140
        .TextMatrix(0, 2) = "N� Benefici�rio"
    
        .ColAlignment(3) = flexAlignLeftCenter
        .ColWidth(3) = 1425
        .TextMatrix(0, 3) = "Total Act.M�dicos"
    
        .ColAlignment(4) = flexAlignLeftCenter
        .ColWidth(4) = 1335
        .TextMatrix(0, 4) = "Total Benefici�rio"
    
        .ColAlignment(5) = flexAlignLeftCenter
        .ColWidth(5) = 1005
        .TextMatrix(0, 5) = "Total ADMG"
        
        .ColAlignment(6) = flexAlignLeftCenter
        .ColWidth(6) = 375
        .TextMatrix(0, 6) = "Erro"
        
        .Width = .ColWidth(0) + .ColWidth(1) + .ColWidth(2) + .ColWidth(3) + .ColWidth(4) + .ColWidth(5) + .ColWidth(6) + 275
    End With
    
    With FgAnalises
        .Cols = 6
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(0) = 1245
        .TextMatrix(0, 0) = "C�d.Act.M�dico"
        
        .ColAlignment(1) = flexAlignLeftCenter
        .ColWidth(1) = 1320
        .TextMatrix(0, 1) = "Data Act.M�dico"
        
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(2) = 930
        .TextMatrix(0, 2) = "Quantidade"
        
        .ColAlignment(3) = flexAlignLeftCenter
        .ColWidth(3) = 1335
        .TextMatrix(0, 3) = "Valor Act.M�dico"
        
        .ColAlignment(4) = flexAlignLeftCenter
        .ColWidth(4) = 1335
        .TextMatrix(0, 4) = "Valor Benefici�rio"
        
        .ColAlignment(5) = flexAlignLeftCenter
        .ColWidth(5) = 975
        .TextMatrix(0, 5) = "Valor ADMG"
        
        .Width = .ColWidth(0) + .ColWidth(1) + .ColWidth(2) + .ColWidth(3) + .ColWidth(4) + .ColWidth(5) + 275
    End With
End Sub

Public Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
End Sub

Public Sub FuncaoInserir()
    
  ' nada

    
End Sub
Private Function ValidaCamposEc() As Integer

' nada


End Function
Public Sub FuncaoProcurar()
    
' nada

End Sub

Public Sub FuncaoModificar()
    
' nada

    
End Sub

Public Sub FuncaoRemover()
' nada

    
End Sub

Public Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoEstadoAnterior()
    
     If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Private Sub PreencheValoresDefeito()
    
' nada
End Sub

Private Sub BD_Insert()
' nada
    

End Sub

Private Sub BD_Delete()
' nada
    

    
End Sub

Private Sub BD_Update()
' nada
    

End Sub

Private Sub LimpaCampos()
' nada

    
End Sub

Private Sub PreencheCampos()
    ' nada

End Sub





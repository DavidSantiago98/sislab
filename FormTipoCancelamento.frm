VERSION 5.00
Begin VB.Form FormTipoCancelamento 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormTipoCancelamento"
   ClientHeight    =   5895
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5910
   Icon            =   "FormTipoCancelamento.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5895
   ScaleWidth      =   5910
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3720
      TabIndex        =   19
      Top             =   4200
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   18
      Top             =   4230
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   17
      Top             =   4680
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   16
      Top             =   4680
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   120
      TabIndex        =   9
      Top             =   2160
      Width           =   4125
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label6 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   13
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2760
         TabIndex        =   11
         Top             =   195
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2760
         TabIndex        =   10
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.TextBox EctipoUtil 
      Height          =   285
      Left            =   3600
      TabIndex        =   7
      Top             =   3720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   1920
      TabIndex        =   5
      Top             =   3720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CbTipoUtil 
      Height          =   315
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   360
      Width           =   2535
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   1125
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   960
      Width           =   4095
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   1455
   End
   Begin VB.Label Label9 
      Caption         =   "Tipo"
      Height          =   255
      Left            =   1680
      TabIndex        =   24
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2520
      TabIndex        =   23
      Top             =   4680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2520
      TabIndex        =   22
      Top             =   4200
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   4680
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   4200
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "EctipoUtil"
      Height          =   255
      Left            =   2760
      TabIndex        =   8
      Top             =   3720
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   480
      TabIndex        =   6
      Top             =   3720
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label3 
      Caption         =   "Descri��o"
      Height          =   615
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormTipoCancelamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub
Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormTipoCancelamento = Nothing
End Sub

Sub Inicializacoes()

    Me.caption = " Tipos de cancelamento"
    Me.left = 540
    Me.top = 450
    Me.Width = 4455
    Me.Height = 3500 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_t_canc"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 8
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_t_canc"
    CamposBD(1) = "cod_t_canc"
    CamposBD(2) = "t_util"
    CamposBD(3) = "descr_canc"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = CbTipoUtil
    Set CamposEc(3) = EcDescricao
    Set CamposEc(4) = EcUtilizadorCriacao
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do tipo de cancelamento"
    TextoCamposObrigatorios(2) = "Tipo de cancelamento"
    TextoCamposObrigatorios(3) = "Descri��o do tipo de cancelamento"
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_t_canc"
    Set ChaveEc = EcCodSequencial
        
End Sub
Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub
Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_tbf_tipo_cancelamento", "cod_cancelamento", "sigla_cancelamento", CbTipoUtil
    
End Sub

Private Sub CbTipoUtil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then CbTipoUtil.ListIndex = -1
End Sub


Private Sub EcCodigo_LostFocus()
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub


Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub
Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    CbTipoUtil.ListIndex = mediComboValorNull
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
End Sub
Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcUtilizadorAlteracao = gCodUtilizador
        EcDataAlteracao = Bg_DaData_ADO
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
End Sub


Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_t_canc") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    rs.Requery
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    LimpaCampos
    PreencheCampos

End Sub
Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_t_canc ASC, descr_canc ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        BL_FimProcessamento Me
        PreencheCampos
    End If
End Sub


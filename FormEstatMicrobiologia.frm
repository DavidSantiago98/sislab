VERSION 5.00
Begin VB.Form FormEstatExMicro 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   6870
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7575
   Icon            =   "FormEstatMicrobiologia.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   7575
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcUtente 
      Height          =   285
      Left            =   5880
      TabIndex        =   37
      Top             =   6600
      Width           =   1095
   End
   Begin VB.CheckBox CkDescriminaReq 
      Caption         =   "Descriminar Requisi��es"
      Height          =   255
      Left            =   120
      TabIndex        =   36
      Top             =   6600
      Width           =   2295
   End
   Begin VB.CheckBox CkAgruparProdutos 
      Caption         =   "Agrupar por Produto"
      Height          =   255
      Left            =   5160
      TabIndex        =   35
      Top             =   6240
      Width           =   1935
   End
   Begin VB.CheckBox CkAgruparGrExames 
      Caption         =   "Agrupar por Grupo de Exames"
      Height          =   255
      Left            =   2520
      TabIndex        =   34
      Top             =   6240
      Width           =   2535
   End
   Begin VB.CheckBox Flg_OrderProven 
      Caption         =   "Agrupar por Proveni�ncia"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   6240
      Width           =   2295
   End
   Begin VB.TextBox EcPesqRapEspecif 
      Height          =   285
      Left            =   120
      TabIndex        =   17
      Top             =   7800
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapGrupo 
      Height          =   285
      Left            =   120
      TabIndex        =   14
      Top             =   7320
      Width           =   1095
   End
   Begin VB.TextBox EcPesqRapProduto 
      Height          =   285
      Left            =   120
      TabIndex        =   12
      Top             =   7200
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   5295
      Left            =   120
      TabIndex        =   5
      Top             =   960
      Width           =   7335
      Begin VB.CommandButton BtPesquisaSituacao 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrobiologia.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   40
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   720
         Width           =   375
      End
      Begin VB.ListBox ListaSituacao 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   39
         Top             =   720
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaGrupos 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrobiologia.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   33
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   1200
         Width           =   375
      End
      Begin VB.ListBox ListaGrupos 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   32
         Top             =   1200
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaProdutos 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrobiologia.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   31
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   1920
         Width           =   375
      End
      Begin VB.ListBox ListaProdutos 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   30
         Top             =   1920
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaEspecif 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrobiologia.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   2640
         Width           =   375
      End
      Begin VB.ListBox ListaEspecif 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   28
         Top             =   2640
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaProven 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrobiologia.frx":1634
         Style           =   1  'Graphical
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   3240
         Width           =   375
      End
      Begin VB.ListBox ListaProven 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   26
         Top             =   3240
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaGrPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrobiologia.frx":1BBE
         Style           =   1  'Graphical
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   3960
         Width           =   375
      End
      Begin VB.ListBox ListaGrPerfis 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   22
         Top             =   3960
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicrobiologia.frx":2148
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   4680
         Width           =   375
      End
      Begin VB.ListBox ListaPerfis 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   18
         Top             =   4680
         Width           =   4800
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   315
         Width           =   1455
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   25
         Top             =   3240
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "&Grupo Exames"
         Height          =   255
         Left            =   360
         TabIndex        =   24
         Top             =   3960
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "&Exames"
         Height          =   255
         Left            =   360
         TabIndex        =   20
         Top             =   4680
         Width           =   1215
      End
      Begin VB.Label Label11 
         Caption         =   "Especifica��o"
         Height          =   255
         Left            =   360
         TabIndex        =   16
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "&Grupo An�lises"
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Pro&duto"
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   10
         Top             =   315
         Width           =   360
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   4440
         TabIndex        =   9
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   720
         Width           =   735
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   7335
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Valida��o"
         Height          =   255
         Index           =   1
         Left            =   4560
         TabIndex        =   42
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Chegada"
         Height          =   255
         Index           =   0
         Left            =   4560
         TabIndex        =   41
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton BtReportLista 
         Caption         =   "Lista"
         Height          =   725
         Left            =   6600
         Picture         =   "FormEstatMicrobiologia.frx":26D2
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   3
         Top             =   360
         Width           =   255
      End
   End
   Begin VB.Label Label10 
      Caption         =   "Utente:"
      Height          =   255
      Left            =   5160
      TabIndex        =   38
      Top             =   6600
      Width           =   615
   End
End
Attribute VB_Name = "FormEstatExMicro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim ListaOrigem As Control
Dim ListaDestino As Control
Public CamposBDparaListBoxF
Dim NumEspacos
Public RSListaDestino As ADODB.recordset
Public RSListaOrigem As ADODB.recordset


Sub Preenche_Estatistica()
    Dim proveniencias As String
    Dim Produtos As String
    Dim GrupoPerfis As String
    Dim GrupoAna As String
    Dim perfis As String
    Dim especif As String
    Dim i As Integer
    Dim sql As String
    Dim continua As Boolean
    Dim nomeReport As String
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Estat�stica de Exames Microbiol�gicos") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    If ListaGrupos.ListCount = 0 Then
        Call BG_Mensagem(mediMsgBox, "Indique o grupo de an�lises que pertende pesquisar.", vbOKOnly + vbExclamation, App.ProductName)
        ListaGrupos.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    If CkAgruparProdutos = vbChecked Then
        nomeReport = "EstatisticaExMicro_OrderProduto"
    Else
        nomeReport = "EstatisticaExMicro"
    End If
    
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Estat�stica de Exames Microbiol�gicos", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Estat�stica de Exames Microbiol�gicos", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    Call Cria_TmpRec_Estatistica
    
    BG_BeginTransaction
    PreencheTabelaTemporaria
    BG_CommitTransaction
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    If CkAgruparProdutos.value = vbChecked Then
        Report.SQLQuery = "SELECT DESCR_PERFIS, DESCR_PRODUTO, DESCR_PROVEN, DESCR_GR_PERFIS,N_REQ, UTENTE, NOME_UTE " & _
            " FROM SL_CR_REL" & gNumeroSessao & " SL_CR_ESTATMICRO ORDER BY DESCR_PRODUTO,DESCR_PROVEN, DESCR_GR_PERFIS, DESCR_PERFIS, DESCR_ESPECIF "
    Else
        Report.SQLQuery = "SELECT DESCR_PERFIS, DESCR_PRODUTO, DESCR_PROVEN, DESCR_GR_PERFIS,N_REQ, UTENTE, NOME_UTE " & _
            " FROM SL_CR_REL" & gNumeroSessao & " SL_CR_ESTATMICRO ORDER BY DESCR_PROVEN, DESCR_GR_PERFIS, DESCR_PERFIS,DESCR_PRODUTO, DESCR_ESPECIF "
    End If
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    
    If ListaProven.ListCount = 0 Then
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    Else
        proveniencias = ""
        For i = 0 To ListaProven.ListCount - 1
            proveniencias = proveniencias & ListaProven.List(i) & "; "
        Next
        If Len(proveniencias) > 200 Then
            proveniencias = left(proveniencias, 200) & "..."
        End If
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & proveniencias)
    End If
    
    If ListaProdutos.ListCount = 0 Then
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("Todos")
    Else
        Produtos = ""
        For i = 0 To ListaProdutos.ListCount - 1
           Produtos = Produtos & ListaProdutos.List(i) & "; "
        Next
        If Len(Produtos) > 200 Then
            Produtos = left(Produtos, 200) & "..."
        End If
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("" & Produtos)
    End If
    
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("-")
    Else
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    End If
    If ListaSituacao.ListCount = 0 Then
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("Todas")
    Else
        Dim sit As String
        For i = 0 To ListaSituacao.ListCount - 1
            sit = sit & ListaSituacao.List(i) & ";"
        Next
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("" & sit)
    End If
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
    End If
    Report.formulas(8) = "Order_Proven=" & BL_TrataStringParaBD("" & Flg_OrderProven.value)
    
    If ListaGrPerfis.ListCount = 0 Then
        Report.formulas(9) = "GrupoPerfis=" & BL_TrataStringParaBD("Todos")
    Else
        GrupoPerfis = ""
        For i = 0 To ListaGrPerfis.ListCount - 1
            GrupoPerfis = GrupoPerfis & ListaGrPerfis.List(i) & "; "
        Next
        If Len(GrupoPerfis) > 200 Then
            GrupoPerfis = left(GrupoPerfis, 200) & "..."
        End If
        Report.formulas(9) = "GrupoPerfis=" & BL_TrataStringParaBD("" & GrupoPerfis)
    End If
    
    If ListaPerfis.ListCount = 0 Then
        Report.formulas(10) = "Perfis=" & BL_TrataStringParaBD("Todos")
    Else
        perfis = ""
        For i = 0 To ListaPerfis.ListCount - 1
            perfis = perfis & ListaPerfis.List(i) & "; "
        Next
        If Len(perfis) > 200 Then
            perfis = left(perfis, 200) & "..."
        End If
        Report.formulas(10) = "Perfis=" & BL_TrataStringParaBD("" & perfis)
    End If
    
    If ListaEspecif.ListCount = 0 Then
        Report.formulas(11) = "Especificacao=" & BL_TrataStringParaBD("Todas")
    Else
        especif = ""
        For i = 0 To ListaEspecif.ListCount - 1
            especif = especif & ListaEspecif.List(i) & "; "
        Next
        If Len(especif) > 200 Then
            especif = left(especif, 200) & "..."
        End If
        Report.formulas(11) = "Especificacao=" & BL_TrataStringParaBD("" & especif)
    End If
    
    If ListaGrupos.ListCount = 0 Then
        Report.formulas(12) = "GrupoAna=" & BL_TrataStringParaBD("Todos")
    Else
        GrupoAna = ""
        For i = 0 To ListaGrupos.ListCount - 1
           GrupoAna = GrupoAna & ListaGrupos.List(i) & "; "
        Next
        If Len(GrupoAna) > 200 Then
            GrupoAna = left(GrupoAna, 200) & "..."
        End If
        Report.formulas(12) = "GrupoAna=" & BL_TrataStringParaBD("" & GrupoAna)
    End If
    Report.formulas(13) = "Order_GrExames=" & BL_TrataStringParaBD("" & CkAgruparGrExames.value)
    Report.formulas(14) = "Order_Produto=" & BL_TrataStringParaBD("" & CkAgruparProdutos.value)
    
    If CkDescriminaReq.value = vbChecked Then
        Report.formulas(15) = "FlgReq=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(15) = "FlgReq=" & BL_TrataStringParaBD("N")
    End If
    Report.formulas(16) = "Utente=" & BL_TrataStringParaBD(EcUtente)
    'Me.SetFocus
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Call BL_RemoveTabela("SL_CR_REL" & gNumeroSessao)
    
End Sub

Sub Cria_TmpRec_Estatistica()

    Dim TmpRec(8) As DefTable
    
    TmpRec(1).NomeCampo = "DESCR_PERFIS"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 80
    
    TmpRec(2).NomeCampo = "DESCR_PRODUTO"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 60
    
    TmpRec(3).NomeCampo = "N_REQ"
    TmpRec(3).tipo = "INTEGER"
    TmpRec(3).tamanho = 9
    
    TmpRec(4).NomeCampo = "DESCR_PROVEN"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 60
    
    TmpRec(5).NomeCampo = "DESCR_ESPECIF"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 20
    
    TmpRec(6).NomeCampo = "DESCR_GR_PERFIS"
    TmpRec(6).tipo = "STRING"
    TmpRec(6).tamanho = 40
    
    TmpRec(7).NomeCampo = "UTENTE"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 20
    
    TmpRec(8).NomeCampo = "NOME_UTE"
    TmpRec(8).tipo = "STRING"
    TmpRec(8).tamanho = 80
    
    Call BL_CriaTabela("SL_CR_REL" & gNumeroSessao, TmpRec)
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesquisaGrPerfis_click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_perfis"
    CamposEcran(1) = "cod_gr_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_perfis "
    CWhere = ""
    CampoPesquisa = "descr_gr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaGrPerfis.ListCount = 0 Then
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(0) = resultados(i)
            Else
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(ListaGrPerfis.NewIndex) = resultados(i)
            End If
        Next i
        

    End If

End Sub


Private Sub BtPesquisaGrupos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_ana "
    CWhere = ""
    CampoPesquisa = "descr_gr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_ana ", _
                                                                           " Grupo de An�lises")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaGrupos.ListCount = 0 Then
                ListaGrupos.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                ListaGrupos.ItemData(0) = resultados(i)
            Else
                ListaGrupos.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "seq_gr_ana", resultados(i))
                ListaGrupos.ItemData(ListaGrupos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub
Private Sub BtPesquisaSituacao_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_t_sit"
    CamposEcran(1) = "cod_t_sit"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_t_sit"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tbf_t_sit "
    CWhere = ""
    CampoPesquisa = "descr_t_sit"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_t_sit ", _
                                                                           " Situa��o")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaSituacao.ListCount = 0 Then
                ListaSituacao.AddItem BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", resultados(i))
                ListaSituacao.ItemData(0) = resultados(i)
            Else
                ListaSituacao.AddItem BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", resultados(i))
                ListaSituacao.ItemData(ListaGrupos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub




Private Sub BtPesquisaProdutos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto "
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProdutos.ListCount = 0 Then
                ListaProdutos.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProdutos.ItemData(0) = resultados(i)
            Else
                ListaProdutos.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProdutos.ItemData(ListaProdutos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaProven_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(300) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven "
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Provini�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProven.ListCount = 0 Then
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(0) = resultados(i)
            Else
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(ListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub BtReportLista_Click()
    'MsgBox "Queria...mas s� amanh�!! " & vbCrLf & "Para j� tem que usar o velho bot�o..."
    
    
    Call Preenche_Estatistica
End Sub


Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub




Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Exames Microbiol�gicos"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7665
    Me.Height = 7350 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtIni
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    If Not RSListaOrigem Is Nothing Then
        RSListaOrigem.Close
        Set RSListaOrigem = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Exames Microbiol�gicos")
    
    Set FormEstatExMicro = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    'CbAnalises.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    ListaSituacao.Clear
    EcDescrSexo.ListIndex = mediComboValorNull
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    Flg_OrderProven.value = vbUnchecked
    CkAgruparGrExames.value = vbUnchecked
    CkAgruparProdutos.value = vbUnchecked
    ListaGrPerfis.Clear
    ListaPerfis.Clear
    ListaProven.Clear
    ListaEspecif.Clear
    ListaProdutos.Clear
    ListaGrupos.Clear
    Option1(0).value = True
    
End Sub

Sub DefTipoCampos()

    'Tipo VarChar

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10

        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    'BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    'BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
    
    
    'Preenche Combo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
    
    ListaGrupos.AddItem "MICROBIOLOGIA"
    ListaGrupos.ItemData(0) = BL_SelCodigo("SL_GR_ANA", "SEQ_GR_ANA", "COD_GR_ANA", CStr(gCodGrupoMicrobiologia), "V")
    Option1(0).value = True
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub



Sub PreencheTabelaTemporaria()
    Dim sql As String
    Dim sql1 As String
    Dim Sql2 As String
    Dim Sql3 As String
    Dim proveniencias As String
    Dim situacao As String
    Dim Produtos As String
    Dim perfis As String
    Dim grPerfis As String
    
    Dim i As Integer
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    If gLAB = "HSMARTA" Then
    Else
        If gUsaProdutoPorExame = mediSim Then
            sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_PROVEN, DESCR_PERFIS, DESCR_PRODUTO, N_REQ, DESCR_ESPECIF, DESCR_GR_PERFIS, UTENTE, NOME_UTE) "
            If Flg_OrderProven.value = 1 Then
                sql = sql & " select distinct descr_proven, descr_perfis,descr_produto, sl_requis.n_req, sl_especif.descr_especif,sl_gr_perfis.descr_gr_perfis, " & tabela_aux & ".utente, " & tabela_aux & ".nome_ute  "
            Else
                sql = sql & " select distinct '' descr_proven, descr_perfis,descr_produto, sl_requis.n_req, sl_especif.descr_especif ,sl_gr_perfis.descr_gr_perfis, " & tabela_aux & ".utente, " & tabela_aux & ".nome_ute "
            End If
            sql = sql & " From " & _
              " " & tabela_aux & ", sl_requis, sl_realiza, sl_req_prod, sl_perfis, sl_gr_ana, sl_produto, sl_proven, sl_especif, sl_gr_perfis " & _
              " where " & _
              " sl_requis.seq_utente = " & tabela_aux & ".seq_utente and " & _
              " sl_requis.n_req = sl_req_prod.n_req and  " & _
              " sl_req_prod.cod_prod = sl_produto.cod_produto and " & _
              " sl_requis.n_req = sl_realiza.n_req and " & _
              " sl_realiza.cod_perfil = sl_perfis.cod_perfis and " & _
              " sl_req_prod.cod_prod = sl_perfis.cod_produto and " & _
              " sl_perfis.gr_ana  = sl_gr_ana.cod_gr_ana and " & _
              " sl_requis.cod_proven = sl_proven.cod_proven (+) " & _
              "  AND sl_req_prod.cod_especif = sl_especif.cod_especif(+) " & _
              " AND sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis (+) "
        Else
            sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_PROVEN, DESCR_PERFIS, DESCR_PRODUTO, N_REQ, DESCR_ESPECIF, DESCR_GR_PERFIS, UTENTE, NOME_UTE) "
            If Flg_OrderProven.value = 1 Then
                sql = sql & " select distinct descr_proven, descr_perfis, null descr_produto, sl_requis.n_req, null descr_especif,sl_gr_perfis.descr_gr_perfis, " & tabela_aux & ".utente, " & tabela_aux & ".nome_ute    "
            Else
                sql = sql & " select distinct '' descr_proven, descr_perfis,null descr_produto, sl_requis.n_req, null descr_especif ,sl_gr_perfis.descr_gr_perfis, " & tabela_aux & ".utente, " & tabela_aux & ".nome_ute  "
            End If
            sql = sql & " From " & _
              " " & tabela_aux & ", sl_requis, sl_realiza,  sl_perfis, sl_gr_ana, sl_proven, sl_gr_perfis " & _
              " where " & _
              " sl_requis.seq_utente = " & tabela_aux & ".seq_utente and " & _
              " sl_requis.n_req = sl_realiza.n_req and " & _
              " sl_realiza.cod_perfil = sl_perfis.cod_perfis and " & _
              " sl_perfis.gr_ana  = sl_gr_ana.cod_gr_ana and " & _
              " sl_requis.cod_proven = sl_proven.cod_proven (+) " & _
              " AND sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis (+) "
        End If
        If gLAB = "HPD" Then
        Else
            sql = sql & " AND sl_requis.n_req NOT IN (SELECT n_req FROM sl_marcacoes) "
        End If

    End If

    If ListaPerfis.ListCount > 0 Then
        sql = sql & " AND sl_perfis.seq_perfis IN ("
        For i = 0 To ListaPerfis.ListCount - 1
            sql = sql & ListaPerfis.ItemData(i) & ", "
        Next i
        sql = Mid(sql, 1, Len(sql) - 2) & ") "
    End If
        
    If ListaGrPerfis.ListCount > 0 Then
        sql = sql & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            sql = sql & ListaGrPerfis.ItemData(i) & ", "
        Next i
        sql = Mid(sql, 1, Len(sql) - 2) & ") "
    End If
        
    'verifica os campos preenchidos
    'se grupo preenchido
    If ListaGrupos.ListCount > 0 Then
        sql = sql & " AND  sl_gr_ana.seq_gr_ana IN ("
        For i = 0 To ListaGrupos.ListCount - 1
            sql = sql & ListaGrupos.ItemData(i) & ", "
        Next i
        sql = Mid(sql, 1, Len(sql) - 2) & ") "
    End If
    
    
    'se especificacao preenchida
    If ListaEspecif.ListCount > 0 And gUsaProdutoPorExame = mediSim Then
        sql = sql & " AND sl_especif.seq_especif IN ( "
        For i = 0 To ListaEspecif.ListCount - 1
            sql = sql & ListaEspecif.ItemData(i) & ", "
        Next i
        sql = Mid(sql, 1, Len(sql) - 2) & ") "
    End If
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        sql = sql & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
        sql1 = sql1 & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
        Sql2 = Sql2 & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
        Sql3 = Sql3 & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    If Trim(EcUtente) <> "" Then
        sql = sql & " AND " & tabela_aux & ".utente = " & BL_TrataStringParaBD(EcUtente)
        sql1 = sql1 & " AND " & tabela_aux & ".utente = " & BL_TrataStringParaBD(EcUtente)
        Sql2 = Sql2 & " AND " & tabela_aux & ".utente = " & BL_TrataStringParaBD(EcUtente)
        Sql3 = Sql3 & " AND " & tabela_aux & ".utente = " & BL_TrataStringParaBD(EcUtente)
    End If
    'Data preenchida
    If Option1(0).value = True Then
        sql = sql & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
        sql1 = sql1 & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
        Sql2 = Sql2 & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
        Sql3 = Sql3 & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    ElseIf Option1(1).value = True Then
        sql = sql & " AND sl_realiza.dt_val BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
        sql1 = sql1 & " AND sl_realiza.dt_val  BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
        Sql2 = Sql2 & " AND sl_realiza.dt_val  BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
        Sql3 = Sql3 & " AND sl_realiza.dt_val  BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    End If
    'Situa��o preenchida?
    If (ListaSituacao.ListCount > 0) Then
        situacao = " AND sl_requis.t_sit IN ("
        For i = 0 To ListaSituacao.ListCount - 1
            situacao = situacao & ListaSituacao.ItemData(i) & ", "
        Next i
        situacao = Mid(situacao, 1, Len(situacao) - 2) & ") "
        
        sql = sql & situacao
        sql1 = sql1 & situacao
        Sql2 = Sql2 & situacao
        Sql3 = Sql3 & situacao
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
        sql1 = sql1 & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
        Sql2 = Sql2 & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
        Sql3 = Sql3 & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If ListaProven.ListCount > 0 Then
        proveniencias = " AND sl_proven.seq_proven IN ("
        For i = 0 To ListaProven.ListCount - 1
            proveniencias = proveniencias & ListaProven.ItemData(i) & ", "
        Next i
        proveniencias = Mid(proveniencias, 1, Len(proveniencias) - 2) & ") "
    
        sql = sql & proveniencias
        sql1 = sql1 & proveniencias
        Sql2 = Sql2 & proveniencias
        Sql3 = Sql3 & proveniencias
    End If
    
    
    'C�digo do produto preenchido?
    If ListaProdutos.ListCount > 0 And gUsaProdutoPorExame = mediSim Then
        Produtos = " AND sl_produto.seq_produto IN ("
        For i = 0 To ListaProdutos.ListCount - 1
            Produtos = Produtos & ListaProdutos.ItemData(i) & ", "
        Next i
        Produtos = Mid(Produtos, 1, Len(Produtos) - 2) & ") "
        
        sql = sql & Produtos
        sql1 = sql1 & Produtos
        Sql2 = Sql2 & Produtos
        Sql3 = Sql3 & Produtos
    End If
    
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
    If gLAB = "HSMARTA" Then
        BG_ExecutaQuery_ADO sql1
        BG_Trata_BDErro
        BG_ExecutaQuery_ADO Sql2
        BG_Trata_BDErro
        BG_ExecutaQuery_ADO Sql3
        BG_Trata_BDErro
    End If
End Sub




Private Sub BtPesquisaEspecif_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_especif"
    CamposEcran(1) = "cod_especif"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_especif"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_especif "
    CWhere = ""
    CampoPesquisa = "descr_especif"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_especif ", _
                                                                           " Especifica��es")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaEspecif.ListCount = 0 Then
                ListaEspecif.AddItem BL_SelCodigo("sl_especif", "descr_especif", "seq_especif", resultados(i))
                ListaEspecif.ItemData(0) = resultados(i)
            Else
                ListaEspecif.AddItem BL_SelCodigo("sl_especif", "descr_especif", "seq_especif", resultados(i))
                ListaEspecif.ItemData(ListaEspecif.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub


Private Sub BtPesquisaPerfis_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    If ListaGrPerfis.ListCount > 0 Then
        CFrom = "sl_perfis, sl_gr_perfis "
    Else
        CFrom = "sl_perfis "
    End If
    If ListaGrPerfis.ListCount > 0 Then
        CWhere = " flg_activo = 1 AND sl_gr_perfis.cod_gr_perfis = sl_perfis.cod_gr_perfis "
        CWhere = CWhere & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            CWhere = CWhere & ListaGrPerfis.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
    Else
        CWhere = " flg_activo = 1  "
    End If
    CampoPesquisa = "descr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPerfis.ListCount = 0 Then
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(0) = resultados(i)
            Else
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(ListaPerfis.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub List1_Click()

End Sub

Private Sub Listaperfis_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPerfis.ListCount > 0 Then     'Delete
        ListaPerfis.RemoveItem (ListaPerfis.ListIndex)
    End If
End Sub

Private Sub ListaGrPerfis_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaGrPerfis.ListCount > 0 Then     'Delete
        ListaGrPerfis.RemoveItem (ListaGrPerfis.ListIndex)
    End If
End Sub

Private Sub ListaProven_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProven.ListCount > 0 Then     'Delete
        ListaProven.RemoveItem (ListaProven.ListIndex)
    End If
End Sub
Private Sub Listaespecif_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaEspecif.ListCount > 0 Then     'Delete
        ListaEspecif.RemoveItem (ListaEspecif.ListIndex)
    End If
End Sub
Private Sub ListaProdutos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProdutos.ListCount > 0 Then     'Delete
        ListaProdutos.RemoveItem (ListaProdutos.ListIndex)
    End If
End Sub

Private Sub Listagrupos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaGrupos.ListCount > 0 Then     'Delete
        ListaGrupos.RemoveItem (ListaGrupos.ListIndex)
    End If
End Sub
Private Sub ListaSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaSituacao.ListCount > 0 Then     'Delete
        ListaSituacao.RemoveItem (ListaSituacao.ListIndex)
    End If
End Sub


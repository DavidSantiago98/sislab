Attribute VB_Name = "Integracao_Facturacao"
Option Explicit

Private Type ana
    codAna As String
    CodFacturavel As String
    descrAna As String
    CodAnaGH As String
    descrAnaGH As String
    ordem As Integer
    preco As Double
    PrecoUnitario As Double
    precoUte As Double
    precoUnitarioUte As Double
    nRec As String
    serieRec As String
    estadoRec As String
    Flg_Facturada As Integer
    Flg_Retirada As Boolean
    Flg_adicionada As Boolean
    cod_rubr_efr As String
    descr_rubr_efr As String
    nr_c As String
    Peso As Integer
    Qtd_Ana As Integer
    fds As String
    flg_ObrigaRecibo As Boolean
    tratado As Boolean
    
    flg_percentagem As Boolean
    PercDescCodif As String
    PercDescUtente As String
    PercDescEnvio As String
    flg_controlaIsencao As Boolean
    
    deducaoUtente As String
    Portaria As String
    data As String
    flg_recibo_manual As Integer
End Type

Private Type credencial
    n_credencial As String
    N_REQ_ARS As String
    cor As String
    cod_isencao As Integer
    cod_medico As String
    analises() As ana
    totalAna As Integer
End Type

Private Type Entidade
    cod_efr As String
    descr_efr As String
    n_benef As String
    flg_perc_facturar As String
    flg_controla_isencao As String
    flg_obriga_recibo As String
    deducao_ute As String
    flg_acto_med As String
    cod_empresa As String
    flg_compart_dom As String
    flg_dom_defeito As String
    flg_nao_urbano As String
    flg_65anos As String
    codDom As Integer
    flg_separa_dom As String
    flg_insere_cod_rubr_efr As String
    flg_FdsValFixo As String
    flg_novaArs As Integer
    n_ord As Long
    cRed() As credencial
    totalCred As Integer
End Type

Private Type Requis
    NReq As String
    user_cri As String
    dt_chega As String
    hr_chega As String
    n_benef As String
    codIsencao As String
    estadoFact As Integer
    EstadoReq As String
    codUrbano As Long
    km As Long
    codEfr As String
    DescrEFR As String
    SeqUtente As Long
    NomeUtente As String
    dt_nasc_ute As String
    Sexo As String
    numCartaoUtente As String
    hemodialise As Integer
    convencao As Integer
    fimSemana As Integer
    cod_sala As String
    Utente As String
    TipoUtente As String
    DataNasc As String
    NReqAssoc As String
    codMed As String
    entidades() As Entidade
    totalEntidades As Integer
End Type
Dim estrutRequis() As Requis
Dim totalReq As Integer
Const lCodUrbano = 2
Const lCodNaoUrbano = 3
Const lCodUrbanoPortoLisboa = 1
Const lCodUrbanoBiDiario = 4

Const lCodInternacional = 2
Const lCodNormal = 1
Public Function IF_Factura_Microrg(ByVal n_req As String, _
                                                ByVal t_sit As String, _
                                                ByVal n_epis As String, _
                                                ByVal dt_chega As String, _
                                                ByVal processo As String, _
                                                ByVal cod_proven As String, _
                                                ByVal Cod_Perfil As String, _
                                                ByVal cod_ana_c As String, _
                                                ByVal cod_ana_s As String, _
                                                ByVal cod_agrup As String, _
                                                ByVal cod_micro As String, _
                                                ByVal Prova As String, _
                                                Optional ByVal TDoente As String, _
                                                Optional ByVal doente As String, _
                                                Optional ByVal CodLocalFact As Long) As Boolean
    'soliveira: 23.09.2006
    'nova fun��o para facturar a identifica��o do microrganismos
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    IF_Factura_Microrg = False
    
    If BL_HandleNull(Prova, "") <> "" Then
    

        If IF_Verifica_Se_Facturada(n_req, "", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, "", "", "") = True Then
            'An�lise j� facturada
            IF_Factura_Microrg = True
            Exit Function
        End If
                
        'prova: campo para facturar diferentes rubricas(identifica��es de microrganismo) conforme id de prova
        Set RsFact = New ADODB.recordset
        RsFact.CursorLocation = adUseServer
        RsFact.CursorType = adOpenStatic
        
        sql = "SELECT cod_rubr from sl_tbf_t_prova where cod_t_prova = '" & Prova & "'"
    
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsFact.Open sql, gConexao
        
        If RsFact.RecordCount > 0 Then
            While Not RsFact.EOF
                If (BL_HandleNull(RsFact!cod_rubr, "") <> "") Then
                    If UCase(HIS.nome) = UCase("SONHO") Then
                        If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                    n_epis, _
                                                    processo, _
                                                    cod_proven, _
                                                    RsFact!cod_rubr, _
                                                    dt_chega, _
                                                    CLng(n_req), _
                                                    cod_agrup, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, _
                                                    , , , , CodLocalFact) = 1) Then
                        
                                                                
                            IF_Factura_Microrg = True
                        End If
                    ElseIf UCase(HIS.nome) = UCase("GH") Then

                        If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                                        RsFact!cod_rubr, CLng(n_req), dt_chega, _
                                        gNumMecanMedExec, , cod_proven, , , , cod_agrup, cod_agrup, "1", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro) = 1 Then
                                   
                            IF_Factura_Microrg = True
                        End If
                    End If
                End If
                RsFact.MoveNext
            Wend
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsFact.Close
        Set RsFact = Nothing
    Else
        sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro) VALUES (" & _
                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "')"
        BG_ExecutaQuery_ADO sql
    End If

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_Microrg"
            IF_Factura_Microrg = False
            Exit Function
    End Select
End Function

Public Function IF_Factura_Antibiograma(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                CodGrAntibio As String, _
                                                cod_micro As String, _
                                                seq_realiza As String, _
                                                Optional TDoente As String, _
                                                Optional doente As String, _
                                                Optional ByVal CodLocalFact As Long) As Boolean
    'soliveira: 23.09.2006
    'nova fun��o para facturar antibiograma do microrganismo
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    IF_Factura_Antibiograma = False
    
    'Verifica se tem grupo de antibi�ticos registados
    If CodGrAntibio <> "" Then

        If IF_Verifica_Se_Facturada(n_req, "", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, CodGrAntibio, "", "") = True Then
            IF_Factura_Antibiograma = True
            Exit Function
        End If
    
        Dim RsMetodoTsq As ADODB.recordset
        
        'a rubrica associada a carta esta registada na codifca��o de grupo de antibioticos
        Set RsMetodoTsq = New ADODB.recordset

        'selecciona o metodo e a rubrica associdada a carta na codifica��o de cartas (grupo de antibioticos)
        sql = " SELECT sl_gr_antibio.cod_metodo, sl_gr_antibio.cod_rubr, flg_fact_antib "
        sql = sql & " FROM sl_gr_antibio, sl_tbf_metodo_tsq"
        sql = sql & " WHERE cod_metodo = cod_metodo_tsq AND cod_gr_antibio = " & BL_TrataStringParaBD(CodGrAntibio)
        RsMetodoTsq.CursorLocation = adUseServer
        RsMetodoTsq.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsMetodoTsq.Open sql, gConexao
        If (RsMetodoTsq.RecordCount > 0) Then
            'Os m�todos E-Test e BK s�o analisados no fim
            If BL_HandleNull(RsMetodoTsq!flg_fact_antib, 0) = 0 Then
                If (BL_HandleNull(RsMetodoTsq!cod_rubr, "") <> "") Then
                    
                    'insere carta no sonho
                    If UCase(HIS.nome) = "SONHO" Then
                        If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                    n_epis, _
                                                    processo, _
                                                    cod_proven, _
                                                    RsMetodoTsq!cod_rubr, _
                                                    dt_chega, _
                                                    CLng(n_req), _
                                                    cod_agrup, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, CodGrAntibio, _
                                                    , , , CodLocalFact) = 1) Then
                                               
                            IF_Factura_Antibiograma = True
                        End If
                    ElseIf UCase(HIS.nome) = "GH" Then
                    
                        If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                                        RsMetodoTsq!cod_rubr, CLng(n_req), dt_chega, _
                                        gNumMecanMedExec, , cod_proven, , , , cod_agrup, cod_agrup, "1", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, CodGrAntibio) = 1 Then
                                               
                            IF_Factura_Antibiograma = True
                        End If
                    End If
                
                
                Else
                    sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_gr_antibio) VALUES (" & _
                            n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & CodGrAntibio & "')"
                    BG_ExecutaQuery_ADO sql
                End If
            End If
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_gr_antibio) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & CodGrAntibio & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsMetodoTsq.Close
        Set RsMetodoTsq = Nothing
        
    'Se n�o tem carta, verifica se tem o m�todo tsq a facturar,por defeito, para este microrganismo
    Else
        
        sql = "SELECT " & vbCrLf & _
              "     cod_metodo_tsq, cod_rubr " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_tbf_metodo_tsq, sl_microrg " & vbCrLf & _
              "WHERE " & vbCrLf & _
              " cod_metodo_tsq = metodo_tsq AND cod_microrg = " & BL_TrataStringParaBD(cod_micro)
    
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsFact.Open sql, gConexao
                    
        If RsFact.RecordCount > 0 Then
            If BL_HandleNull(RsFact!cod_rubr, "") <> "" Then
                While Not RsFact.EOF
                    If BL_HandleNull(RsFact!cod_rubr, "") <> "" Then
                        If UCase(HIS.nome) = "SONHO" Then
                            If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                        n_epis, _
                                                        processo, _
                                                        cod_proven, _
                                                        RsFact!cod_rubr, _
                                                        dt_chega, _
                                                        CLng(n_req), _
                                                        cod_agrup, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, RsFact!cod_metodo_tsq, _
                                                        , , , CodLocalFact) = 1) Then
                                                   
                                IF_Factura_Antibiograma = True
                            End If
                        ElseIf UCase(HIS.nome) = "GH" Then
                            If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                                            RsMetodoTsq!cod_rubr, CLng(n_req), dt_chega, _
                                            gNumMecanMedExec, , cod_proven, , , , cod_agrup, cod_agrup, "1", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, RsFact!cod_metodo_tsq) = 1 Then
                                                   
                                IF_Factura_Antibiograma = True
                            End If
                        End If
                    End If
                    RsFact.MoveNext
                Wend
            End If
        End If
        RsFact.Close
        Set RsFact = Nothing

    End If


Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_Antibiograma"
            IF_Factura_Antibiograma = False
            Exit Function
    End Select
End Function

Public Function IF_Factura_Antibioticos(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                CodGrAntibio As String, _
                                                Metodo_Tsq As String, _
                                                cod_micro As String, _
                                                seq_realiza As String, _
                                                Optional TDoente As String, _
                                                Optional doente As String, _
                                                Optional CodLocalFact As Long) As Boolean
    Dim RsFactAntib As ADODB.recordset
    Dim sql As String


    'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
    'n�o � poss�vel colocar estes antibioticos em grupos de antibioticos? op��o de cima
    Set RsFactAntib = New ADODB.recordset
    sql = " SELECT distinct sl_res_tsq.cod_antib, cod_metodo, cod_rubr " & _
          " from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
          " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
          " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
          " and seq_realiza = " & seq_realiza & " and n_res = " & 1 & _
          " and cod_micro = " & BL_TrataStringParaBD(cod_micro) & "" & _
          " and cod_metodo in (SELECT cod_metodo_tsq sl_tbf_metodo_tsq  WHERE flg_fact_antib = 1)"
    RsFactAntib.CursorType = adOpenStatic
    RsFactAntib.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsFactAntib.Open sql, gConexao
    If RsFactAntib.RecordCount > 0 Then
        While Not RsFactAntib.EOF
            IF_Factura_Antibioticos = IF_Factura_Antibiotico(n_req, t_sit, n_epis, dt_chega, processo, cod_proven, Cod_Perfil, cod_ana_c, cod_ana_s, _
                                   cod_agrup, CodGrAntibio, Metodo_Tsq, cod_micro, seq_realiza, BL_HandleNull(RsFactAntib!cod_antib, ""), _
                                   BL_HandleNull(RsFactAntib!cod_rubr, ""), TDoente, doente, CodLocalFact)
            RsFactAntib.MoveNext
        Wend
    End If
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_Antibioticos"
            IF_Factura_Antibioticos = False
            Exit Function
    End Select
End Function

Public Function IF_Factura_Antibiotico(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                CodGrAntibio As String, _
                                                Metodo_Tsq As String, _
                                                cod_micro As String, _
                                                seq_realiza As String, _
                                                cod_antibiotico As String, _
                                                cod_rubr As String, _
                                                TDoente As String, _
                                                doente As String, _
                                                CodLocalFact As Long) As Boolean

    'verifica 1� se o antibiotio j� foi facturado
    If cod_rubr = "" Then Exit Function
    If IF_Verifica_Se_Facturada(n_req, "", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, CodGrAntibio, cod_antibiotico, "") = False Then
    
        'se n�o foi facturado ainda o antibiotico
        If UCase(HIS.nome) = "SONHO" Then
            If (SONHO_Nova_ANALISES_IN(t_sit, _
                                    n_epis, _
                                    processo, _
                                    cod_proven, _
                                    cod_rubr, _
                                    dt_chega, _
                                    CLng(n_req), _
                                    cod_agrup, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, CodGrAntibio, _
                                    cod_antibiotico, , , CodLocalFact) = 1) Then

                IF_Factura_Antibiotico = True
            End If
        ElseIf UCase(HIS.nome) = "GH" Then

            If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                            cod_rubr, CLng(n_req), dt_chega, _
                            gNumMecanMedExec, , cod_proven, , , , cod_agrup, cod_agrup, "1", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, CodGrAntibio, cod_antibiotico) = 1 Then
                       
                IF_Factura_Antibiotico = True
            End If

        End If
        
    End If
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_Antibiotico"
            IF_Factura_Antibiotico = False
            Exit Function
    End Select
End Function


Public Function IF_Factura_Teste(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                cod_micro As String, _
                                                seq_realiza As String, _
                                                Optional TDoente As String, _
                                                Optional doente As String, _
                                                Optional CodLocalFact As Long) As Boolean
    'soliveira: 17.10.2006
    'nova fun��o para facturar teste do microrganismo
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    IF_Factura_Teste = False
                
    'a rubrica associada ao teste do microrganismo encontra-se na codifica��o de provas
    Set RsFact = New ADODB.recordset
    RsFact.CursorLocation = adUseServer
    RsFact.CursorType = adOpenStatic
        
    sql = "select distinct sl_res_prova.cod_prova,sl_prova.cod_rubr " & _
            "from sl_res_prova, sl_prova " & _
            "where sl_res_prova.cod_prova = sl_prova.cod_prova and " & _
            "sl_res_prova.seq_realiza = " & seq_realiza & _
            " "
    If cod_micro <> "" Then
        sql = sql & " and sl_res_prova.cod_micro = " & BL_TrataStringParaBD(cod_micro)
    End If

    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsFact.Open sql, gConexao
        
    If RsFact.RecordCount > 0 Then
        While Not RsFact.EOF
            If (BL_HandleNull(RsFact!cod_rubr, "") <> "") Then
                
                'verifica se prova j� facturada
                If IF_Verifica_Se_Facturada(n_req, "", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, "", "", RsFact!Cod_Prova) = False Then
                
                    If UCase(HIS.nome) = "SONHO" Then
                        'If SONHO_Verifica_Ana_ja_Existe(RsFact!cod_prova, n_req) = False Then
                        
                            If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                        n_epis, _
                                                        processo, _
                                                        cod_proven, _
                                                        RsFact!cod_rubr, _
                                                        dt_chega, _
                                                        CLng(n_req), _
                                                        cod_agrup, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, , , _
                                                        RsFact!Cod_Prova, , CodLocalFact) = 1) Then
                            
                                                                    
                                IF_Factura_Teste = True
                            End If
                        'End If
                    ElseIf UCase(HIS.nome) = "GH" Then
                        'If GH_Analise_ja_facturada(n_req, t_sit, n_epis, RsFact!cod_prova) = False Then
                
                            If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                                            RsFact!cod_rubr, CLng(n_req), dt_chega, _
                                            gNumMecanMedExec, , cod_proven, , , , RsFact!Cod_Prova, "1") = 1 Then
                                       
                            
                            
                                                                    
                                IF_Factura_Teste = True
                            End If
                        'End If
                    End If
                
                End If
            
            Else
                'prova nao mapeada
                sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro, cod_prova) VALUES (" & _
                        n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & RsFact!Cod_Prova & "')"
                BG_ExecutaQuery_ADO sql
            End If
            
            RsFact.MoveNext
        Wend
        
    End If
    RsFact.Close
    Set RsFact = Nothing

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_Teste"
            IF_Factura_Teste = False
            Exit Function
    End Select
End Function

Public Function IF_Factura_Analise(ByVal n_req As String, _
                                                ByVal cod_efr As String, _
                                                ByVal t_sit As String, _
                                                ByVal n_epis As String, _
                                                ByVal dt_chega As String, _
                                                ByVal processo As String, _
                                                ByVal cod_proven As String, _
                                                ByVal Cod_Perfil As String, _
                                                ByVal cod_ana_c As String, _
                                                ByVal cod_ana_s As String, _
                                                ByVal cod_agrup As String, _
                                                ByVal seq_realiza As String, _
                                                ByVal TDoente As String, _
                                                ByVal doente As String, _
                                                ByVal NReqARS As String, _
                                                ByVal CodLocalFact As Long, _
                                                ByVal n_acto As Integer, ByVal qtd As Integer, _
                                                ByVal codMed As String, _
                                                ByVal cod_u_saude As String, _
                                                ByVal n_mecan_ext As String) As Boolean
    'soliveira: 23.09.2006
    'nova fun��o para facturar analises
    
    Dim RsVerifInsercao As ADODB.recordset
    Dim RsFact As ADODB.recordset
    Dim RsRegra As ADODB.recordset
    Dim quantidade As Integer
    Dim numMarcadores As Integer
    'Dim RsTestaExDirecto As ADODB.Recordset
    On Error GoTo ErrorHandler
    
    gQuantidadeAFact = 1
    gAnalise_Ja_Facturada = False
    
    ' Mapeia uma an�lise do SISLAB para o SONHO.
    
    Dim sql As String
    Dim SqlP As String
    
    Dim i As Integer
    Dim CriterioTabela As String

    cod_efr = BL_HandleNull(cod_efr, "-1")
    Cod_Perfil = BL_HandleNull(Trim(Cod_Perfil), "0")
    cod_ana_c = BL_HandleNull(Trim(cod_ana_c), "0")
    cod_ana_s = BL_HandleNull(Trim(cod_ana_s), "0")
    
    gInsereAnaFact = False
    IF_Factura_Analise = False
    
    For i = 1 To 3
FacturaMembro:
        Select Case i
            Case 1
                CriterioTabela = Cod_Perfil
            Case 2
                CriterioTabela = cod_ana_c
            Case 3
                CriterioTabela = cod_ana_s
        End Select
        
        If gCodPerfil_aux <> Cod_Perfil Then
            gCodAna_aux = ""
        End If
    
        If (Trim(gCodAna_aux) <> Trim(CriterioTabela)) Or (gReq_aux <> n_req) Then
            
            If (CriterioTabela <> "0") Then
                
                Set RsFact = New ADODB.recordset
                RsFact.CursorLocation = adUseServer
                RsFact.CursorType = adOpenStatic
                
                'TENTA ENCONTRAR A AN�LISE POR ENTIDADE
                sql = "SELECT x1.cod_ana_gh, x1.flg_regra, x1.cod_ana_regra, x1.factura_membros, x1.centro_custo, x1.qtd, x1.flg_conta_membros, "
                sql = sql & " x1.flg_marcador, x1.min_marcadores, x1.media_marcadores "
                sql = sql & " FROM  sl_ana_facturacao x1, slv_analiseS_factus x2 WHERE x1.cod_ana = x2.cod_ana "
                sql = sql & " AND  x1.cod_ana = '" & CriterioTabela & "' "
                sql = sql & " AND x1.COD_EFR = " & cod_efr
                sql = sql & " AND x1.cod_prod IS NULL "
                sql = sql & " AND x1.cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(BL_HandleNull(dt_chega, Bg_DaData_ADO)) & " BETWEEN "
                sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
                RsFact.Open sql, gConexao
                
                
                'SE NAO ENCONTRAR TENTA GENERICA
                If RsFact.RecordCount = 0 Then
                    RsFact.Close
                    sql = "SELECT x1.cod_ana_gh, x1.flg_regra, x1.cod_ana_regra, x1.factura_membros, x1.centro_custo, x1.qtd, x1.flg_conta_membros, "
                    sql = sql & " x1.flg_marcador, x1.min_marcadores, x1.media_marcadores "
                    sql = sql & " FROM  sl_ana_facturacao x1, slv_analiseS_factus x2 WHERE x1.cod_ana = x2.cod_ana "
                    sql = sql & " AND  x1.cod_ana = '" & CriterioTabela & "' "
                    sql = sql & " AND x1.COD_EFR IS NULL "
                    sql = sql & " AND x1.cod_prod IS NULL "
                    sql = sql & " AND x1.cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(BL_HandleNull(dt_chega, Bg_DaData_ADO)) & " BETWEEN "
                    sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
                    RsFact.Open sql, gConexao
                        
                End If

                If RsFact.RecordCount > 0 Or (Cod_Perfil = "0" And cod_ana_c = "0" And cod_ana_s = "S99999" And gLAB = "HFM") Then
                    '-------------------------------------------------------------
                    ' cod_ana_c = 0
                    ' cod_perfil = 0
                    ' cod_ana_s = S99999
                    ' ISTO ACONTECE QUANDO QUEREMOS FACTURAR A ANALISE A APAGAR NO SONHO Z00901 EM FAMALICAO
                    '-------------------------------------------------------------
                    
                    ' ---------------------------------------------------------------------------
                    ' calcula a quantidade a enviar
                    ' ---------------------------------------------------------------------------
                    If BL_HandleNull(RsFact!qtd, 0) <> 0 Then
                        quantidade = BL_HandleNull(RsFact!qtd, 0)
                    Else
                        quantidade = BL_HandleNull(qtd, 1)
                    End If
                    
                    ' ---------------------------------------------------------------------------
                    ' SE ANALISE DO TIPO MARCADORES CALCULA NUMERO DE MARCADORES
                    ' ---------------------------------------------------------------------------
                    If BL_HandleNull(RsFact!flg_marcador, mediNao) = mediSim Then
                        numMarcadores = IF_RetornaNumMarcadores(CriterioTabela, n_req)
                    Else
                        numMarcadores = 0
                    End If
                    
                    'verifica se a an�lise j� foi facturada
                    'If SONHO_Verifica_Ana_ja_Existe(CriterioTabela, n_req) = True Then
                    If IF_Verifica_Se_Facturada(n_req, CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, "", "", "", "") = True Then
                        'A an�lise j� foi facturada, colocar como facturada
                        gAnalise_Ja_Facturada = True
                        IF_Factura_Analise = True
                        gCodAna_aux = CriterioTabela
                        gCodPerfil_aux = Cod_Perfil
                        gReq_aux = n_req
                        'como � para facturar os membros, n�o sai da funcao para ir facturar com i = 3 o membro da complexa
                        If BL_HandleNull(RsFact!factura_membros, "0") = "1" Then
                            i = i + 1
                            GoTo FacturaMembro
                        Else
                            Exit Function
                        End If
                    End If

                    '-------------------------------------------------------------
                    
                    While Not RsFact.EOF
                        If (BL_HandleNull(RsFact!cod_ana_gh, "") <> "") Then
                            

                            'Verifica se existe regra para facturar a an�lise, se a regra se verifica, n�o factura
                            If IF_Verifica_Regra(BL_HandleNull(RsFact!Flg_Regra, "0"), BL_HandleNull(RsFact!Cod_Ana_Regra, ""), n_req) = True Then
                                '� para colocar como facturado
                                'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                                gAnalise_Ja_Facturada = True
                                gCodAna_aux = CriterioTabela
                                gCodPerfil_aux = Cod_Perfil
                                gReq_aux = n_req
                                IF_Factura_Analise = False
                                Exit Function
                            Else
                                If UCase(HIS.nome) = "SONHO" Then
                                    If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                        n_epis, _
                                                        processo, _
                                                        cod_proven, _
                                                        RsFact!cod_ana_gh, _
                                                        dt_chega, _
                                                        CLng(n_req), _
                                                        CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, , , , _
                                                        , BL_HandleNull(RsFact!centro_custo, ""), CodLocalFact) = 1) Then
                                        IF_Factura_Analise = True
                                        'se quero que facture os proximos membros da complexa, n�o posso indicar a analise anterior facturada
                                        'para entrar no if inicial: "if gcodana_aux <> criteriotabela"
                                        If BL_HandleNull(RsFact!factura_membros, "0") <> "1" Then
                                            gCodAna_aux = CriterioTabela
                                            gCodPerfil_aux = Cod_Perfil
                                        End If
                                        gReq_aux = n_req
                                        gInsereAnaFact = True
                                    End If
                                ElseIf UCase(HIS.nome) = "GH" Then

                                    If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                                        RsFact!cod_ana_gh, CLng(n_req), dt_chega, _
                                        gNumMecanMedExec, , cod_proven, , , cod_efr, CriterioTabela, _
                                        cod_agrup, CStr(quantidade), Cod_Perfil, cod_ana_c, cod_ana_s, , , , , NReqARS, BL_HandleNull(RsFact!centro_custo, ""), n_acto, codMed, cod_u_saude, n_mecan_ext) = 1 Then
                                   
                                        IF_Factura_Analise = True
                                        'se quero que facture os proximos membros da complexa, n�o posso indicar a analise anterior facturada
                                        'para entrar no if inicial: "if gcodana_aux <> criteriotabela"
                                        If BL_HandleNull(RsFact!factura_membros, "0") <> "1" Then
                                            gCodAna_aux = CriterioTabela
                                            gCodPerfil_aux = Cod_Perfil
                                        End If
                                        gReq_aux = n_req
                                        gInsereAnaFact = True
                                    End If
                                ElseIf UCase(HIS.nome) = "SISBIT" Then
                                    If (SISBIT_Nova_ANALISES_IN(t_sit, _
                                                        n_epis, _
                                                        processo, _
                                                        cod_proven, _
                                                        RsFact!cod_ana_gh, _
                                                        dt_chega, _
                                                        CLng(n_req), cod_efr, _
                                                        CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, , , , _
                                                        , BL_HandleNull(RsFact!centro_custo, ""), CodLocalFact) = 1) Then
                                        IF_Factura_Analise = True
                                        'se quero que facture os proximos membros da complexa, n�o posso indicar a analise anterior facturada
                                        'para entrar no if inicial: "if gcodana_aux <> criteriotabela"
                                        If BL_HandleNull(RsFact!factura_membros, "0") <> "1" Then
                                            gCodAna_aux = CriterioTabela
                                            gCodPerfil_aux = Cod_Perfil
                                        End If
                                        gReq_aux = n_req
                                        gInsereAnaFact = True
                                    End If
                                End If
                            End If
                        Else
                            '� para colocar como facturado
                            'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                            gAnalise_Ja_Facturada = True
                            IF_Factura_Analise = False
                            Exit Function
                        End If
                        RsFact.MoveNext
                    Wend
                    
                    
                    If (gCodAnaSONHO <> "-1" And Cod_Perfil = "0" And cod_ana_c = "0" And cod_ana_s = "S99999" And gLAB = "HFM") Then
                        '-------------------------------------------------------------
                        ' cod_ana_c = 0
                        ' cod_perfil = 0
                        ' cod_ana_s = S99999
                        ' ISTO ACONTECE QUANDO QUEREMOS FACTURAR A ANALISE A APAGAR NO SONHO Z00901 EM FAMALICAO
                        '-------------------------------------------------------------
                        If UCase(HIS.nome) = "SONHO" Then
                            If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                processo, _
                                                cod_proven, _
                                                gCodAnaSONHO, _
                                                dt_chega, _
                                                CLng(n_req), _
                                                CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, , , , _
                                                , BL_HandleNull(RsFact!centro_custo, ""), CodLocalFact, numMarcadores) = 1) Then
                                IF_Factura_Analise = True
                                gReq_aux = n_req
                                gInsereAnaFact = True
                                gCodAna_aux = "S99999"
                            End If
                        ElseIf UCase(HIS.nome) = "GH" Then
                            If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                                gCodAnaSONHO, CLng(n_req), dt_chega, _
                                gNumMecanMedExec, , cod_proven, , , cod_efr, CriterioTabela, cod_agrup, CStr(quantidade), Cod_Perfil, cod_ana_c, _
                                cod_ana_s, , , , , NReqARS, BL_HandleNull(RsFact!centro_custo, ""), n_acto, codMed, cod_u_saude, n_mecan_ext, _
                                numMarcadores) = 1 Then
                           
                                IF_Factura_Analise = True
                                gReq_aux = n_req
                                gInsereAnaFact = True
                            End If
                        ElseIf UCase(HIS.nome) = "SISBIT" Then
                            If (SISBIT_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                processo, _
                                                cod_proven, _
                                                gCodAnaSONHO, _
                                                dt_chega, _
                                                CLng(n_req), cod_efr, _
                                                CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, , , , _
                                                , BL_HandleNull(RsFact!centro_custo, ""), CodLocalFact, numMarcadores) = 1) Then
                                IF_Factura_Analise = True
                                gReq_aux = n_req
                                gInsereAnaFact = True
                                gCodAna_aux = "S99999"
                            End If
                        End If
                    End If
                    
                    
                    If IF_Factura_Analise = True Then Exit Function
                    
                ElseIf RsFact.RecordCount = 0 And gF_REQUIS = 1 Then
            
                    sql = sql & " AND cod_prod IS NULL "
                
                    RsFact.Close
                    Set RsFact = Nothing
                    
                    Set RsFact = New ADODB.recordset
                    RsFact.CursorLocation = adUseServer
                    RsFact.CursorType = adOpenStatic
                    
                    RsFact.Open sql, gConexao
                    If (RsFact.RecordCount > 0) Then
                        While Not RsFact.EOF
                            If (BL_HandleNull(RsFact!cod_ana_gh, "") <> "") Then
                                '-------------------------------------------------------------
                                'verifica se a an�lise j� foi inserida na facturacao
                                    If IF_Verifica_Se_Facturada(n_req, CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, "", "", "", "") = True Then
                                        'A an�lise j� foi facturada, colocar como facturada
                                        gAnalise_Ja_Facturada = True
                                        IF_Factura_Analise = False
                                        gCodAna_aux = CriterioTabela
                                        gCodPerfil_aux = Cod_Perfil
                                        gReq_aux = n_req
                                        If BL_HandleNull(RsFact!factura_membros, "0") = "1" Then
                                            i = i + 1
                                            GoTo FacturaMembro
                                        Else
                                            Exit Function
                                        End If
                                    End If

                                '-------------------------------------------------------------
                                
                                'Verifica se existe regra para facturar a an�lise, se a regra se verifica, n�o factura
                                If IF_Verifica_Regra(BL_HandleNull(RsFact!Flg_Regra, "0"), BL_HandleNull(RsFact!Cod_Ana_Regra, ""), n_req) = True Then
                                    '� para colocar como facturado
                                    'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                                    gAnalise_Ja_Facturada = True
                                    IF_Factura_Analise = False
                                Else
                                        If UCase(HIS.nome) = "SONHO" Then
                                            If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                            n_epis, _
                                                            processo, _
                                                            cod_proven, _
                                                            RsFact!cod_ana_gh, _
                                                            dt_chega, _
                                                            CLng(n_req), _
                                                            CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, , , _
                                                            , , BL_HandleNull(RsFact!centro_custo, ""), CodLocalFact, numMarcadores) = 1) Then
                                                            
                                                IF_Factura_Analise = True
                                            End If
                                            
                                        ElseIf UCase(HIS.nome) = "GH" Then
                                            If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                                                        RsFact!cod_ana_gh, CLng(n_req), dt_chega, _
                                                        gNumMecanMedExec, , cod_proven, , , cod_efr, CriterioTabela, cod_agrup, CStr(quantidade), Cod_Perfil, _
                                                        cod_ana_c, cod_ana_s, , , , , NReqARS, BL_HandleNull(RsFact!centro_custo, ""), n_acto, codMed, cod_u_saude, _
                                                        n_mecan_ext, numMarcadores) = 1 Then
                                                
                                                IF_Factura_Analise = True
                                            End If
                                        ElseIf UCase(HIS.nome) = "SISBIT" Then
                                            If (SISBIT_Nova_ANALISES_IN(t_sit, _
                                                                n_epis, _
                                                                processo, _
                                                                cod_proven, _
                                                                gCodAnaSONHO, _
                                                                dt_chega, _
                                                                CLng(n_req), cod_efr, _
                                                                CriterioTabela, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, , , , _
                                                                , BL_HandleNull(RsFact!centro_custo, ""), CodLocalFact, numMarcadores) = 1) Then
                                                IF_Factura_Analise = True
                                            End If
                                        End If
                                        
                                        'se quero que facture os proximos membros da complexa, n�o posso indicar a analise anterior facturada
                                        'para entrar no if inicial: "if gcodana_aux <> criteriotabela"
                                        If BL_HandleNull(RsFact!factura_membros, "0") <> "1" Then
                                            gCodAna_aux = CriterioTabela
                                            gCodPerfil_aux = Cod_Perfil
                                        End If
                                        gReq_aux = n_req
                                        gInsereAnaFact = True
                                End If
                            
                            Else
                                '� para colocar como facturado
                                'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                                gAnalise_Ja_Facturada = True
                                IF_Factura_Analise = False
                                Exit Function
                            End If
                            RsFact.MoveNext
                        Wend
                        If IF_Factura_Analise = True Then Exit Function
                                        
                    Else
                        If i = 3 Then
                        sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s) VALUES (" & _
                            n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "')"
                        BG_ExecutaQuery_ADO sql
                        End If
                    End If
                
                Else
                    ' Logs.
'                    If (((cod_ana_s <> "S485") And _
'                          (cod_ana_s <> "S99999"))) Then
                    If i = 3 Then
'                          Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
'                                            Format(Date, gFormatoData) & " " & Format(Time, gFormatoHora) & _
'                                            " [R:" & n_req & "][P:" & Cod_Perfil & "][C:" & Cod_Ana_C & "][S:" & cod_ana_s & "]")
                            
                            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s) VALUES (" & _
                                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "')"
                            BG_ExecutaQuery_ADO sql
                            
                    End If
                End If
                
            End If
        Else
            'A an�lise j� foi facturada, mas n�o faz parte da contagem
            gAnalise_Ja_Facturada = True
            IF_Factura_Analise = False
            Exit Function
        End If
    Next i
    

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_Analise"
            IF_Factura_Analise = False
            Exit Function
            Resume Next
    End Select
End Function

Function IF_Verifica_Regra(Flg_Regra As String, Cod_Ana_Regra As String, n_req As String) As Boolean
    'Verifica se tem regra, e se tem, se a regra � verdadeira
    'True - n�o factura an�lise
    'False - factura an�lise
    Dim RsRegra As ADODB.recordset
    Dim CriterioTabela As String
    
    On Error GoTo TrataErro

    IF_Verifica_Regra = True
    
    If Flg_Regra = "1" And Cod_Ana_Regra <> "" Then
        If Mid(Cod_Ana_Regra, 1, 1) = "S" Then
            CriterioTabela = " cod_ana_s = "
        ElseIf Mid(Cod_Ana_Regra, 1, 1) = "C" Then
            CriterioTabela = " cod_ana_c = "
        ElseIf Mid(Cod_Ana_Regra, 1, 1) = "P" Then
            CriterioTabela = " cod_perfil = "
        End If
        
        If Cod_Ana_Regra = "C455" Then
            CriterioTabela = ""
        End If
        
        Set RsRegra = New ADODB.recordset

        RsRegra.CursorLocation = adUseServer
        RsRegra.CursorType = adOpenStatic
        RsRegra.Source = "select N_rEQ from sl_realiza where n_req = " & BL_TrataStringParaBD(n_req) & _
                        " and " & IIf(Cod_Ana_Regra = "C455", " cod_ana_c in ('C455', 'C482') ", CriterioTabela & BL_TrataStringParaBD(Cod_Ana_Regra))
        RsRegra.Source = RsRegra.Source & " union select N_REQ from SL_MARCACOES where n_req = " & BL_TrataStringParaBD(n_req) & _
                        " and " & IIf(Cod_Ana_Regra = "C455", " cod_ana_c in ('C455', 'C482') ", CriterioTabela & BL_TrataStringParaBD(Cod_Ana_Regra))
        RsRegra.ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros RsRegra.Source
        RsRegra.Open
        If Not RsRegra.EOF Then
            'se a an�lise da regra est� presenta na requisi��o, n�o factura
            IF_Verifica_Regra = True
        Else
            IF_Verifica_Regra = False
        End If
    Else
        IF_Verifica_Regra = False
    End If
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Verifica_Regra"
    IF_Verifica_Regra = True
    Exit Function
End Function


Public Function IF_Factura_Rubrica(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                cod_micro As String, _
                                                CodGrAntibio As String, _
                                                Antibio As String, _
                                                CodProva As String, _
                                                cod_rubr As String, _
                                                Optional TDoente As String, _
                                                Optional doente As String, _
                                                Optional ByVal CodLocalFact As Long) As Boolean
'soliveira 18.09.2007
'utilizada para facturar cartas e antibioticos ETest na valida��o de resultados
    Dim sql As String
    
    If (BL_HandleNull(cod_rubr, "") <> "") Then
        
        'verifica se a rubrica j� foi facturada
        If IF_Verifica_Se_Facturada(n_req, "", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, CodGrAntibio, Antibio, CodProva) = True Then
            'rubrica j� facturada
            IF_Factura_Rubrica = True
            Exit Function
        End If
        
        'insere carta no sonho
        If UCase(HIS.nome) = "SONHO" Then
            If (SONHO_Nova_ANALISES_IN(t_sit, _
                                        n_epis, _
                                        processo, _
                                        cod_proven, _
                                        cod_rubr, _
                                        dt_chega, _
                                        CLng(n_req), _
                                        cod_agrup, Cod_Perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, _
                                        CodGrAntibio, Antibio, CodProva, , CodLocalFact) = 1) Then
                                   
                IF_Factura_Rubrica = True
            End If
        ElseIf UCase(HIS.nome) = "GH" Then
        
            If GH_Nova_ANALISES_IN(t_sit, n_epis, TDoente, doente, _
                            cod_rubr, CLng(n_req), dt_chega, _
                            gNumMecanMedExec, , cod_proven, , , , cod_agrup, cod_agrup, "1", Cod_Perfil, cod_ana_c, cod_ana_s, cod_micro, CodGrAntibio, Antibio, CodProva, Empty, Empty, Empty, Empty) = 1 Then
                                   
                IF_Factura_Rubrica = True
            End If
        End If
    
    
    Else
        sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro, cod_gr_antibio) VALUES (" & _
                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & CodGrAntibio & "')"
        BG_ExecutaQuery_ADO sql
    End If

End Function


Function IF_Verifica_Se_Facturada(NReq As String, CodMapeado As String, CodPerfil As String, CodAnaC As String, codAnaS As String, CodMicro As String, CodGrAntibio As String, CodAntibio As String, CodProva As String, Optional ByVal situacao As String, Optional ByVal episodio As String) As Boolean
    'Fun��o que verifica se a an�lise j� foi inserida na tabela sl_analises_in
    
    Dim RsVerifInsercao As ADODB.recordset
    Dim sql As String
    Dim sSql As String
    Dim rs_aux As New ADODB.recordset


    '-------------------------------------------------------------
    'soliveira - 03-10-2007 - verifica se a an�lise j� foi inserida no sonho
    Set RsVerifInsercao = New ADODB.recordset
    RsVerifInsercao.CursorLocation = adUseServer
    RsVerifInsercao.CursorType = adOpenStatic
    
    If CodMapeado <> "" Then
        'soliveira 28.09.2007
        If UCase(HIS.nome) = UCase("SONHO") Or UCase(HIS.nome) = UCase("SISBIT") Then
            sql = "SELECT cod_analise FROM sl_analises_in WHERE n_req = " & NReq & " and cod_ana_sislab = '" & CodMapeado & "'"
        ElseIf UCase(HIS.nome) = UCase("GH") Then
            sql = "SELECT * FROM sl_dados_fact WHERE n_req = " & NReq & " and n_analise = " & BL_TrataStringParaBD(CodMapeado)
            
            'Sql = Sql & " AND n_seq not in (select n_seq from sl_dados_fact where n_req = " & NReq & " and n_seq_anul = n_seq) "
             sql = sql & " AND n_seq not in (select n_seq_anul from sl_dados_fact where n_req = " & NReq & " and n_seq_anul is not null) and n_seq_anul is null "
        End If
        
        Select Case Mid(CodMapeado, 1, 1)
            Case "P"
                sql = sql & " AND cod_perfil = '" & CodPerfil & "'"
            Case "C"
                sql = sql & " AND cod_perfil = '" & CodPerfil & "' and cod_ana_c = '" & CodAnaC & "'"
            Case "S"
                sql = sql & " AND cod_perfil = '" & CodPerfil & "' and cod_ana_c = '" & CodAnaC & "' and cod_ana_s = '" & codAnaS & "'"
        End Select
    Else

        If UCase(HIS.nome) = UCase("SONHO") Or UCase(HIS.nome) = UCase("SISBIT") Then
        
            sql = "SELECT " & vbCrLf & _
                    "     cod_analise " & vbCrLf & _
                    "FROM " & vbCrLf & _
                    "     sl_analises_in " & vbCrLf & _
                    "WHERE " & vbCrLf & _
                    "     n_req = " & NReq & " and cod_perfil = '" & CodPerfil & "' and  cod_ana_c = '" & CodAnaC & "' and cod_ana_s = '" & codAnaS & _
                    "' and cod_micro " & IIf(CodMicro = "", " is null ", "=" & BL_TrataStringParaBD(CodMicro)) & " and cod_gr_antibio " & IIf(CodGrAntibio = "", " is null ", "=" & BL_TrataStringParaBD(CodGrAntibio)) & " and cod_antibio " & IIf(CodAntibio = "", " is null ", "=" & BL_TrataStringParaBD(CodAntibio)) & " and cod_prova " & IIf(CodProva = "", " is null ", "=" & BL_TrataStringParaBD(CodProva)) & ""
                    
        ElseIf UCase(HIS.nome) = UCase("GH") Then
        
            sql = "SELECT " & vbCrLf & _
                    "     *     " & vbCrLf & _
                    "FROM " & vbCrLf & _
                    "     sl_dados_fact " & vbCrLf & _
                    "WHERE " & vbCrLf & _
                    "     n_req = " & NReq & " and cod_perfil = '" & CodPerfil & "' and  cod_ana_c = '" & CodAnaC & "' and cod_ana_s = '" & codAnaS & _
                    "' and cod_micro " & IIf(CodMicro = "", " is null ", "=" & BL_TrataStringParaBD(CodMicro)) & " and cod_gr_antibio " & IIf(CodGrAntibio = "", " is null ", "=" & BL_TrataStringParaBD(CodGrAntibio)) & " and cod_antibio " & IIf(CodAntibio = "", " is null ", "=" & BL_TrataStringParaBD(CodAntibio)) & " and cod_prova " & IIf(CodProva = "", " is null ", "=" & BL_TrataStringParaBD(CodProva)) & ""
                    
            sql = sql & " AND n_seq not in (select n_seq from sl_dados_fact where n_req = " & NReq & " and n_seq_anul = n_seq) "
        End If
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsVerifInsercao.Open sql, gConexao
    If RsVerifInsercao.RecordCount > 0 Then
        'A an�lise j� se encontra na tabela sl_analises_in
        IF_Verifica_Se_Facturada = True
    Else
        IF_Verifica_Se_Facturada = False
    End If
    '-------------------------------------------------------------



End Function


' -----------------------------------------------------------------------------------------------

' RETORNA A TAXA PAGA PELO UTENTE DE UMA ANALISE PARA UMA ENTIDADE (FACTUS)

' -----------------------------------------------------------------------------------------------

Public Function IF_RetornaTaxaAnalise(codAna As String, Entidade As String, t_utente As String, Utente As String, Optional p_data As String, _
                                      Optional ByRef p_cod_rubr_efr As String, Optional ByRef p_descr_rubr_efr As String, Optional Obriga65 As Boolean) As String
    Dim sSql As String
    Dim iBD_Aberta As Long
    Dim rsAnaFact As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    Dim rsTaxas As New ADODB.recordset
    Dim rsEntidades As New ADODB.recordset
    Dim qtd As Integer
    Dim Flg_UsaPrecoEntidade As Boolean
    
    ' --------------------------------------
    ' PARAMETROS PARA PROCEDIMENTO FACTUS
    ' --------------------------------------
    Dim p_cod_rubr As String
    Dim p_valor_ent As String
    Dim p_valor_adic As String
    Dim p_valor_doe As String
    Dim p_valor_taxa As String
    Dim p_perc_desc As String
    Dim p_valor_total As String
    'Dim p_cod_rubr_efr As String
    Dim p_alt_prec As String
    Dim p_w_msg As String
    
    Dim ResFACTUS As Integer
    Dim subprecario  As String
    
    On Error GoTo TrataErro
    
    ' -----------------------------------------------------------------------------------------------
    ' PROCURA A RUBRICA ASSOCIADA A ANALISE
    ' -----------------------------------------------------------------------------------------------
    If Entidade = "" Then
        Exit Function
    End If
    sSql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE cod_efr = " & Entidade & " AND  cod_ana = " & BL_TrataStringParaBD(codAna)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaFact.Open sSql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        sSql = "SELECT cod_ana_gh, flg_conta_membros FROM sl_ana_facturacao WHERE cod_efr IS NULL AND cod_ana = " & BL_TrataStringParaBD(codAna)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaFact.Open sSql, gConexao
        If rsAnaFact.RecordCount <= 0 Then
            ' ANALISE NAO ESTA MAPEADA PARA FACTUS
            IF_RetornaTaxaAnalise = "-1"
            rsAnaFact.Close
            Set rsAnaFact = Nothing
            Exit Function
        Else
            If BL_HandleNull(rsAnaFact!flg_conta_membros, 0) = 1 Then
            Else
                qtd = 1
            End If
        End If
    End If
    

    ' -----------------------
    ' ABRE CONEXAO COM FACTUS
    ' -----------------------
    iBD_Aberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iBD_Aberta = 0 Then
        ' NAO EXISTE CONEXAO FACTUS
        IF_RetornaTaxaAnalise = "-2"
    End If
    
    subprecario = IF_VerificaUsaSubPrecario(Entidade, p_data)
    If subprecario <> "" Then
        IF_RetornaTaxaAnalise = IF_RetornaTaxaAnaliseSubPrec(True, codAna, subprecario, Entidade, t_utente, Utente, p_data, p_cod_rubr_efr, , , , , p_descr_rubr_efr)
        If CDbl(IF_RetornaTaxaAnalise) < 0 Then
            IF_RetornaTaxaAnalise = 0
        Else
            Exit Function
        End If
    End If
    ' -------------------------------------------------------
    ' PARA ALGUMAS ENTIDADES O PRECO PARA RECIBO = ENTIDADE
    ' -------------------------------------------------------
    If BL_HandleNull(BL_SelCodigo("SL_EFR", "FLG_USA_PRECO_ENT", "COD_EFR", Entidade, "V"), "0") = "1" Then
        Flg_UsaPrecoEntidade = True
    Else
        Flg_UsaPrecoEntidade = False
    End If
        
    ' -------------------------------------------------------
    ' SE ENVIA ENTIDADE E DATA CHAMA O PROCEDIMENTO DO FACTUS
    ' -------------------------------------------------------
    ResFACTUS = 0
    If gNaoUsaProcFactusTaxas <> mediSim Then
        If p_data <> "" Then
            ResFACTUS = IF_SetDoente(t_utente, Utente)
            ResFACTUS = IF_ObtemPrecoRubr(Entidade, BL_HandleNull(rsAnaFact!cod_ana_gh, ""), p_data, p_valor_ent, p_valor_adic, p_valor_doe, p_valor_taxa, p_perc_desc, _
                              p_valor_total, p_cod_rubr_efr, p_descr_rubr_efr, p_alt_prec, p_w_msg)
        End If
    End If
    
    
    If ResFACTUS > 0 And (p_valor_doe <> "0" Or p_valor_taxa <> "0") And Flg_UsaPrecoEntidade = False Then
        If p_valor_doe <> "0" Then
            If Mid(p_valor_doe, 1, 1) = "," Or Mid(p_valor_doe, 1, 1) = "." Then
                p_valor_doe = "0" & p_valor_doe
            End If
            IF_RetornaTaxaAnalise = p_valor_doe
        Else
            If Mid(p_valor_taxa, 1, 1) = "," Or Mid(p_valor_taxa, 1, 1) = "." Then
                p_valor_taxa = "0" & p_valor_taxa
            End If
            IF_RetornaTaxaAnalise = p_valor_taxa
        End If
    ElseIf ResFACTUS > 0 And (p_valor_ent <> "0") And Flg_UsaPrecoEntidade = True Then
        If Mid(p_valor_ent, 1, 1) = "," Or Mid(p_valor_ent, 1, 1) = "." Then
            p_valor_ent = "0" & p_valor_ent
        End If
        IF_RetornaTaxaAnalise = p_valor_ent
    
    Else
        ' -------------------------------
        ' VERIFICA QUAL A ENTIDADE ACTIVA
        ' -------------------------------
        sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & Entidade
        rsEntidades.CursorLocation = adUseServer
        rsEntidades.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsEntidades.Open sSql, gConexaoSecundaria
        If rsEntidades.RecordCount <= 0 Then
            ' NAO EXISTE ENTIDADE CODIFICADA
            IF_RetornaTaxaAnalise = "-5"
        Else
            
            ' --------------------------------------------------------
            ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
            ' --------------------------------------------------------
            sSql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
            sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
            sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "

            rsPortarias.CursorLocation = adUseServer
            rsPortarias.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsPortarias.Open sSql, gConexaoSecundaria
            If rsPortarias.RecordCount <= 0 Then
                ' NAO EXISTE PORTARIA ACTIVA
                IF_RetornaTaxaAnalise = "-3"
            Else
                ' --------------------------------------
                ' RETORNA A TAXA PARA A ANALISE EM CAUSA
                ' --------------------------------------
                sSql = "SELECT val_taxa,val_pag_doe, cod_rubr_efr, val_pag_ent,descr_rubr_efr FROM fa_pr_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(BL_HandleNull(rsAnaFact!cod_ana_gh, ""))
                sSql = sSql & " AND portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria) & " AND user_rem IS NULL "
                rsTaxas.CursorLocation = adUseServer
                rsTaxas.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsTaxas.Open sSql, gConexaoSecundaria
                If rsTaxas.RecordCount <= 0 Then
                    ' NAO EXISTE TAXA CODIFICADA
                    IF_RetornaTaxaAnalise = "-4"
                Else
                    If (BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Or BL_HandleNull(rsTaxas!val_Taxa, "0") <> "0") And Flg_UsaPrecoEntidade = False Then
                        If BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Then
                            If Mid(BL_HandleNull(rsTaxas!val_pag_doe, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_pag_doe, "0"), 1, 1) = "." Then
                                IF_RetornaTaxaAnalise = "0" & BL_HandleNull(rsTaxas!val_pag_doe, "0")
                            Else
                                IF_RetornaTaxaAnalise = BL_HandleNull(rsTaxas!val_pag_doe, "0")
                            End If
                        Else
                            If Mid(BL_HandleNull(rsTaxas!val_Taxa, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_Taxa, "0"), 1, 1) = "." Then
                                IF_RetornaTaxaAnalise = "0" & BL_HandleNull(rsTaxas!val_Taxa, "0")
                            Else
                                IF_RetornaTaxaAnalise = BL_HandleNull(rsTaxas!val_Taxa, "0")
                            End If
                        End If
                    ElseIf (BL_HandleNull(rsTaxas!val_pag_ent, "0") <> "0") And Flg_UsaPrecoEntidade = True Then
                        If Mid(BL_HandleNull(rsTaxas!val_pag_ent, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_pag_ent, "0"), 1, 1) = "." Then
                            p_valor_ent = "0" & BL_HandleNull(rsTaxas!val_pag_ent, "0")
                        Else
                            IF_RetornaTaxaAnalise = BL_HandleNull(rsTaxas!val_pag_ent, "0")
                        End If
                    Else
                        IF_RetornaTaxaAnalise = "0"
                    End If
                    
                    If Flg_UsaPrecoEntidade = False Then
                        'VERIFICA SE DEVE FAZER DESCONTO DE 50% PARA >65 ANOS
                        If IF_VerificaRegraArs(t_utente, Utente, Entidade, p_data) = True Or Obriga65 = True Then
                            IF_RetornaTaxaAnalise = CStr(BL_HandleNull(IF_RetornaTaxaAnalise, 0) / 2)
                            IF_RetornaTaxaAnalise = Round(IF_RetornaTaxaAnalise, 2)
                        End If
                    End If
                    p_cod_rubr_efr = CStr(BL_HandleNull(rsTaxas!cod_rubr_efr, 0))
                    p_descr_rubr_efr = CStr(BL_HandleNull(rsTaxas!descr_rubr_efr, ""))
                End If
                rsTaxas.Close
                Set rsTaxas = Nothing
            End If
            rsPortarias.Close
            Set rsPortarias = Nothing
        End If
        rsEntidades.Close
        Set rsEntidades = Nothing
    End If
    rsAnaFact.Close
    Set rsAnaFact = Nothing
    If IF_RetornaTaxaAnalise > "0" And qtd > 0 Then
        IF_RetornaTaxaAnalise = qtd * Replace(IF_RetornaTaxaAnalise, ".", ",")
        IF_RetornaTaxaAnalise = Replace(IF_RetornaTaxaAnalise, ",", ".")
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaTaxaAnalise"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' RETORNA A TAXA PAGA PELO UTENTE DE UMA ANALISE PARA UMA ENTIDADE (FACTUS)

' -----------------------------------------------------------------------------------------------

Public Function IF_RetornaTaxaAnaliseSub(codAna As String, Entidade As String, t_utente As String, Utente As String, Optional p_data As String, _
                                      Optional ByRef p_cod_rubr_efr As String, Optional ByRef p_descr_rubr_efr As String) As String
    Dim sSql As String
    Dim iBD_Aberta As Long
    Dim rsAnaFact As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    Dim rsTaxas As New ADODB.recordset
    Dim rsEntidades As New ADODB.recordset
    Dim qtd As Integer
    Dim Flg_UsaPrecoEntidade As Boolean
    
    ' --------------------------------------
    ' PARAMETROS PARA PROCEDIMENTO FACTUS
    ' --------------------------------------
    Dim p_cod_rubr As String
    Dim p_valor_ent As String
    Dim p_valor_adic As String
    Dim p_valor_doe As String
    Dim p_valor_taxa As String
    Dim p_perc_desc As String
    Dim p_valor_total As String
    'Dim p_cod_rubr_efr As String
    Dim p_alt_prec As String
    Dim p_w_msg As String
    
    Dim ResFACTUS As Integer
    Dim subprecario  As String
    
    On Error GoTo TrataErro
    
    ' -----------------------------------------------------------------------------------------------
    ' PROCURA A RUBRICA ASSOCIADA A ANALISE
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE cod_efr = " & Entidade & " AND  cod_ana = " & BL_TrataStringParaBD(codAna)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaFact.Open sSql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        sSql = "SELECT cod_ana_gh, flg_conta_membros FROM sl_ana_facturacao WHERE cod_efr IS NULL AND cod_ana = " & BL_TrataStringParaBD(codAna)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaFact.Open sSql, gConexao
        If rsAnaFact.RecordCount <= 0 Then
            ' ANALISE NAO ESTA MAPEADA PARA FACTUS
            IF_RetornaTaxaAnaliseSub = "-1"
            rsAnaFact.Close
            Set rsAnaFact = Nothing
            Exit Function
        Else
            If BL_HandleNull(rsAnaFact!flg_conta_membros, 0) = 1 Then
            Else
                qtd = 1
            End If
        End If
    End If
    

    ' -----------------------
    ' ABRE CONEXAO COM FACTUS
    ' -----------------------
    iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
    If iBD_Aberta = 0 Then
        ' NAO EXISTE CONEXAO FACTUS
        IF_RetornaTaxaAnaliseSub = "-2"
    End If
    
    ' -------------------------------------------------------
    ' PARA ALGUMAS ENTIDADES O PRECO PARA RECIBO = ENTIDADE
    ' -------------------------------------------------------
    If BL_HandleNull(BL_SelCodigo("SL_EFR", "FLG_USA_PRECO_ENT", "COD_EFR", Entidade, "V"), "0") = "1" Then
        Flg_UsaPrecoEntidade = True
    Else
        Flg_UsaPrecoEntidade = False
    End If
        
    ' -------------------------------------------------------
    ' SE ENVIA ENTIDADE E DATA CHAMA O PROCEDIMENTO DO FACTUS
    ' -------------------------------------------------------
    ResFACTUS = 0
    If gNaoUsaProcFactusTaxas <> mediSim Then
        If p_data <> "" Then
            ResFACTUS = IF_SetDoente(t_utente, Utente)
            ResFACTUS = IF_ObtemPrecoRubr(Entidade, BL_HandleNull(rsAnaFact!cod_ana_gh, ""), p_data, p_valor_ent, p_valor_adic, p_valor_doe, p_valor_taxa, p_perc_desc, _
                              p_valor_total, p_cod_rubr_efr, p_descr_rubr_efr, p_alt_prec, p_w_msg)
        End If
    End If
    
    
    If ResFACTUS > 0 And (p_valor_doe <> "0" Or p_valor_taxa <> "0") And Flg_UsaPrecoEntidade = False Then
        If p_valor_doe <> "0" Then
            If Mid(p_valor_doe, 1, 1) = "," Or Mid(p_valor_doe, 1, 1) = "." Then
                p_valor_doe = "0" & p_valor_doe
            End If
            IF_RetornaTaxaAnaliseSub = p_valor_doe
        Else
            If Mid(p_valor_taxa, 1, 1) = "," Or Mid(p_valor_taxa, 1, 1) = "." Then
                p_valor_taxa = "0" & p_valor_taxa
            End If
            IF_RetornaTaxaAnaliseSub = p_valor_taxa
        End If
    ElseIf ResFACTUS > 0 And (p_valor_ent <> "0") And Flg_UsaPrecoEntidade = True Then
        If Mid(p_valor_ent, 1, 1) = "," Or Mid(p_valor_ent, 1, 1) = "." Then
            p_valor_ent = "0" & p_valor_ent
        End If
        IF_RetornaTaxaAnaliseSub = p_valor_ent
    
    Else
        ' -------------------------------
        ' VERIFICA QUAL A ENTIDADE ACTIVA
        ' -------------------------------
        sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & Entidade
        rsEntidades.CursorLocation = adUseServer
        rsEntidades.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsEntidades.Open sSql, gConexaoSecundaria
        If rsEntidades.RecordCount <= 0 Then
            ' NAO EXISTE ENTIDADE CODIFICADA
            IF_RetornaTaxaAnaliseSub = "-5"
        Else
            
            ' --------------------------------------------------------
            ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
            ' --------------------------------------------------------
            sSql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
            sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
            sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "

            rsPortarias.CursorLocation = adUseServer
            rsPortarias.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsPortarias.Open sSql, gConexaoSecundaria
            If rsPortarias.RecordCount <= 0 Then
                ' NAO EXISTE PORTARIA ACTIVA
                IF_RetornaTaxaAnaliseSub = "-3"
            Else
                ' --------------------------------------
                ' RETORNA A TAXA PARA A ANALISE EM CAUSA
                ' --------------------------------------
                sSql = "SELECT val_taxa,val_pag_doe, cod_rubr_efr, val_pag_ent,descr_rubr_efr FROM fa_pr_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(BL_HandleNull(rsAnaFact!cod_ana_gh, ""))
                sSql = sSql & " AND portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria) & " AND user_rem IS NULL "
                rsTaxas.CursorLocation = adUseServer
                rsTaxas.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsTaxas.Open sSql, gConexaoSecundaria
                If rsTaxas.RecordCount <= 0 Then
                    ' NAO EXISTE TAXA CODIFICADA
                    IF_RetornaTaxaAnaliseSub = "-4"
                Else
                    If (BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Or BL_HandleNull(rsTaxas!val_Taxa, "0") <> "0") And Flg_UsaPrecoEntidade = False Then
                        If BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Then
                            If Mid(BL_HandleNull(rsTaxas!val_pag_doe, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_pag_doe, "0"), 1, 1) = "." Then
                                IF_RetornaTaxaAnaliseSub = "0" & BL_HandleNull(rsTaxas!val_pag_doe, "0")
                            Else
                                IF_RetornaTaxaAnaliseSub = BL_HandleNull(rsTaxas!val_pag_doe, "0")
                            End If
                        Else
                            If Mid(BL_HandleNull(rsTaxas!val_Taxa, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_Taxa, "0"), 1, 1) = "." Then
                                IF_RetornaTaxaAnaliseSub = "0" & BL_HandleNull(rsTaxas!val_Taxa, "0")
                            Else
                                IF_RetornaTaxaAnaliseSub = BL_HandleNull(rsTaxas!val_Taxa, "0")
                            End If
                        End If
                    ElseIf (BL_HandleNull(rsTaxas!val_pag_ent, "0") <> "0") And Flg_UsaPrecoEntidade = True Then
                        If Mid(BL_HandleNull(rsTaxas!val_pag_ent, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_pag_ent, "0"), 1, 1) = "." Then
                            p_valor_ent = "0" & BL_HandleNull(rsTaxas!val_pag_ent, "0")
                        Else
                            IF_RetornaTaxaAnaliseSub = BL_HandleNull(rsTaxas!val_pag_ent, "0")
                        End If
                    Else
                        IF_RetornaTaxaAnaliseSub = "0"
                    End If
                    
                    If Flg_UsaPrecoEntidade = False Then
                        'VERIFICA SE DEVE FAZER DESCONTO DE 50% PARA >65 ANOS
                        If IF_VerificaRegraArs(t_utente, Utente, Entidade, p_data) = True Then
                            IF_RetornaTaxaAnaliseSub = CStr(BL_HandleNull(IF_RetornaTaxaAnaliseSub, 0) / 2)
                            IF_RetornaTaxaAnaliseSub = Round(IF_RetornaTaxaAnaliseSub, 2)
                        End If
                    End If
                    p_cod_rubr_efr = CStr(BL_HandleNull(rsTaxas!cod_rubr_efr, 0))
                    p_descr_rubr_efr = CStr(BL_HandleNull(rsTaxas!descr_rubr_efr, ""))
                End If
                rsTaxas.Close
                Set rsTaxas = Nothing
            End If
            rsPortarias.Close
            Set rsPortarias = Nothing
        End If
        rsEntidades.Close
        Set rsEntidades = Nothing
    End If
    rsAnaFact.Close
    Set rsAnaFact = Nothing
    If IF_RetornaTaxaAnaliseSub > 0 And qtd > 0 Then
        IF_RetornaTaxaAnaliseSub = qtd * Replace(IF_RetornaTaxaAnaliseSub, ".", ",")
        IF_RetornaTaxaAnaliseSub = Replace(IF_RetornaTaxaAnaliseSub, ",", ".")
    End If
    BL_Fecha_conexao_HIS
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaTaxaAnaliseSub"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' RETORNA A TAXA PAGA PELO UTENTE DE UMA ANALISE PARA UMA ENTIDADE (FACTUS)

' -----------------------------------------------------------------------------------------------

Private Function IF_RetornaTaxaAnaliseSubPrec(Flg_utente As Boolean, codAna As String, cod_sub_prec As String, efr_origem As String, t_utente As String, _
                                       Utente As String, Optional p_data As String, Optional ByRef p_cod_rubr_efr As String, _
                                       Optional ByRef preco_unitario As Double, Optional qtd As Integer, Optional nr_c As String, _
                                       Optional controlaIsentos As Boolean, Optional ByRef p_descr_rubr_efr As String) As String
    Dim tab_utilizada As String
    Dim NumErro As String
    Dim perc_desc As Double
    Dim perc_pag_doe As Double
    Dim taxa_ref As String
    On Error GoTo TrataErro
    NumErro = ""
    tab_utilizada = IF_RetornaEntidadeRef(cod_sub_prec, efr_origem, perc_desc, perc_pag_doe)
    If tab_utilizada = "" Then
         GoTo TrataErro
    End If
    If Flg_utente = True Then
        taxa_ref = IF_RetornaTaxaAnaliseSub(codAna, tab_utilizada, t_utente, Utente, p_data, p_cod_rubr_efr, p_descr_rubr_efr)
        If perc_pag_doe = 0 Then
            IF_RetornaTaxaAnaliseSubPrec = 0
        Else
            IF_RetornaTaxaAnaliseSubPrec = CStr(CDbl(Replace(taxa_ref, ".", ",")) * CDbl(perc_pag_doe / 100))
        End If
    ElseIf Flg_utente = False Then
        taxa_ref = IF_RetornaTaxaAnaliseEntidadeSub(codAna, tab_utilizada, p_data, p_cod_rubr_efr, t_utente, Utente, preco_unitario, qtd, nr_c, controlaIsentos, p_descr_rubr_efr)
        If perc_desc = 100 Then
            IF_RetornaTaxaAnaliseSubPrec = 0
        Else
            IF_RetornaTaxaAnaliseSubPrec = CStr(CDbl(Replace(taxa_ref, ".", ",")) * CDbl((100 - perc_desc) / 100))
        End If
    
    End If
    
Exit Function
TrataErro:
    IF_RetornaTaxaAnaliseSubPrec = "-9"
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaTaxaAnaliseSubPrec"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' RETORNA A TAXA PAGA PELO UTENTE DE UMA ANALISE PARA UMA ENTIDADE (FACTUS)

' -----------------------------------------------------------------------------------------------

Public Function IF_RetornaTaxaRubrica(codRubrica As String, Entidade As String, t_utente As String, Utente As String, Optional p_data As String, _
                                      Optional ByRef p_cod_rubr_efr As String, Optional ByRef p_descr_rubr_efr As String) As String
    Dim sSql As String
    Dim iBD_Aberta As Long
    Dim rsAnaFact As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    Dim rsTaxas As New ADODB.recordset
    Dim rsEntidades As New ADODB.recordset
    Dim qtd As Integer
    Dim Flg_UsaPrecoEntidade As Boolean
    
    ' --------------------------------------
    ' PARAMETROS PARA PROCEDIMENTO FACTUS
    ' --------------------------------------
    Dim p_cod_rubr As String
    Dim p_valor_ent As String
    Dim p_valor_adic As String
    Dim p_valor_doe As String
    Dim p_valor_taxa As String
    Dim p_perc_desc As String
    Dim p_valor_total As String
    'Dim p_cod_rubr_efr As String
    Dim p_alt_prec As String
    Dim p_w_msg As String
    
    Dim ResFACTUS As Integer
    
    On Error GoTo TrataErro
        
    ' -------------------------------------------------------
    ' PARA ALGUMAS ENTIDADES O PRECO PARA RECIBO = ENTIDADE
    ' -------------------------------------------------------
    If BL_HandleNull(BL_SelCodigo("SL_EFR", "FLG_USA_PRECO_ENT", "COD_EFR", Entidade, "V"), "0") = "1" Then
        Flg_UsaPrecoEntidade = True
    Else
        Flg_UsaPrecoEntidade = False
    End If
    
    ' -----------------------
    ' ABRE CONEXAO COM FACTUS
    ' -----------------------
    iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
    If iBD_Aberta = 0 Then
        ' NAO EXISTE CONEXAO FACTUS
        IF_RetornaTaxaRubrica = "-2"
    End If
    
    ' -------------------------------------------------------
    ' SE ENVIA ENTIDADE E DATA CHAMA O PROCEDIMENTO DO FACTUS
    ' -------------------------------------------------------
    ResFACTUS = 0
    If gNaoUsaProcFactusTaxas <> mediSim Then
        If p_data <> "" Then
            ResFACTUS = IF_SetDoente(t_utente, Utente)
            ResFACTUS = IF_ObtemPrecoRubr(Entidade, codRubrica, p_data, p_valor_ent, p_valor_adic, p_valor_doe, p_valor_taxa, p_perc_desc, _
                              p_valor_total, p_cod_rubr_efr, p_descr_rubr_efr, p_alt_prec, p_w_msg)
        End If
    End If
    
    
    If ResFACTUS > 0 And (p_valor_doe <> "0" Or p_valor_taxa <> "0") And Flg_UsaPrecoEntidade = False Then
        If p_valor_doe <> "0" Then
            If Mid(p_valor_doe, 1, 1) = "," Or Mid(p_valor_doe, 1, 1) = "." Then
                p_valor_doe = "0" & p_valor_doe
            End If
            IF_RetornaTaxaRubrica = p_valor_doe
        Else
            If Mid(p_valor_taxa, 1, 1) = "," Or Mid(p_valor_taxa, 1, 1) = "." Then
                p_valor_taxa = "0" & p_valor_taxa
            End If
            IF_RetornaTaxaRubrica = p_valor_taxa
        End If
    ElseIf ResFACTUS > 0 And (p_valor_ent <> "0") And Flg_UsaPrecoEntidade = True Then
        If Mid(p_valor_ent, 1, 1) = "," Or Mid(p_valor_ent, 1, 1) = "." Then
            p_valor_ent = "0" & p_valor_ent
        End If
        IF_RetornaTaxaRubrica = p_valor_ent
    
    Else
        ' -------------------------------
        ' VERIFICA QUAL A ENTIDADE ACTIVA
        ' -------------------------------
        sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & Entidade
        rsEntidades.CursorLocation = adUseServer
        rsEntidades.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsEntidades.Open sSql, gConexaoSecundaria
        If rsEntidades.RecordCount <= 0 Then
            ' NAO EXISTE ENTIDADE CODIFICADA
            IF_RetornaTaxaRubrica = "-5"
        Else
            
            ' --------------------------------------------------------
            ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
            ' --------------------------------------------------------
            sSql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
            sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
            sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "
            rsPortarias.CursorLocation = adUseServer
            rsPortarias.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsPortarias.Open sSql, gConexaoSecundaria
            If rsPortarias.RecordCount <= 0 Then
                ' NAO EXISTE PORTARIA ACTIVA
                IF_RetornaTaxaRubrica = "-3"
            Else
                ' --------------------------------------
                ' RETORNA A TAXA PARA A ANALISE EM CAUSA
                ' --------------------------------------
                sSql = "SELECT val_taxa,val_pag_doe, cod_rubr_efr, val_pag_ent,descr_rubr_efr FROM fa_pr_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(codRubrica)
                sSql = sSql & " AND portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria) & " AND user_rem IS NULL "
                rsTaxas.CursorLocation = adUseServer
                rsTaxas.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsTaxas.Open sSql, gConexaoSecundaria
                If rsTaxas.RecordCount <= 0 Then
                    ' NAO EXISTE TAXA CODIFICADA
                    IF_RetornaTaxaRubrica = "-4"
                Else
                    If (BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Or BL_HandleNull(rsTaxas!val_Taxa, "0") <> "0") And Flg_UsaPrecoEntidade = False Then
                        If BL_HandleNull(rsTaxas!val_pag_doe, "0") <> "0" Then
                            If Mid(BL_HandleNull(rsTaxas!val_pag_doe, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_pag_doe, "0"), 1, 1) = "." Then
                                IF_RetornaTaxaRubrica = "0" & BL_HandleNull(rsTaxas!val_pag_doe, "0")
                            Else
                                IF_RetornaTaxaRubrica = BL_HandleNull(rsTaxas!val_pag_doe, "0")
                            End If
                        Else
                            If Mid(BL_HandleNull(rsTaxas!val_Taxa, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_Taxa, "0"), 1, 1) = "." Then
                                IF_RetornaTaxaRubrica = "0" & BL_HandleNull(rsTaxas!val_Taxa, "0")
                            Else
                                IF_RetornaTaxaRubrica = BL_HandleNull(rsTaxas!val_Taxa, "0")
                            End If
                        End If
                    ElseIf (BL_HandleNull(rsTaxas!val_pag_ent, "0") <> "0") And Flg_UsaPrecoEntidade = True Then
                        If Mid(BL_HandleNull(rsTaxas!val_pag_ent, "0"), 1, 1) = "," Or Mid(BL_HandleNull(rsTaxas!val_pag_ent, "0"), 1, 1) = "." Then
                            p_valor_ent = "0" & BL_HandleNull(rsTaxas!val_pag_ent, "0")
                        Else
                            IF_RetornaTaxaRubrica = BL_HandleNull(rsTaxas!val_pag_ent, "0")
                        End If
                    Else
                        IF_RetornaTaxaRubrica = "0"
                    End If
                    
                    If Flg_UsaPrecoEntidade = False Then
                        'VERIFICA SE DEVE FAZER DESCONTO DE 50% PARA >65 ANOS
                        If IF_VerificaRegraArs(t_utente, Utente, Entidade, p_data) = True Then
                            IF_RetornaTaxaRubrica = CStr(BL_HandleNull(IF_RetornaTaxaRubrica, 0) / 2)
                        End If
                    End If
                    p_cod_rubr_efr = CStr(BL_HandleNull(rsTaxas!cod_rubr_efr, 0))
                    p_descr_rubr_efr = CStr(BL_HandleNull(rsTaxas!descr_rubr_efr, ""))
                End If
                rsTaxas.Close
                Set rsTaxas = Nothing
            End If
            rsPortarias.Close
            Set rsPortarias = Nothing
        End If
        rsEntidades.Close
        Set rsEntidades = Nothing
    End If
    rsAnaFact.Close
    Set rsAnaFact = Nothing
    If IF_RetornaTaxaRubrica > 0 And qtd > 0 Then
        IF_RetornaTaxaRubrica = qtd * Replace(IF_RetornaTaxaRubrica, ".", ",")
        IF_RetornaTaxaRubrica = Replace(IF_RetornaTaxaRubrica, ",", ".")
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaTaxaRubrica"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' RETORNA TAXA PAGA ENTIDADE DE UMA ANALISE PARA UMA ENTIDADE(FACTUS)

' -----------------------------------------------------------------------------------------------

Public Function IF_RetornaTaxaAnaliseEntidade(ByVal codAna As String, ByVal Entidade As String, ByVal p_data As String, _
                                              ByRef cod_rubr_efr As String, ByVal t_utente As String, Utente As String, _
                                              ByRef preco_unitario As Double, ByVal qtd As Integer, ByVal nr_c As String, _
                                              ByVal controlaIsentos As Boolean, ByRef descr_rubr_efr As String, _
                                              ByVal cod_hemodialise As Integer) As String
    Dim sSql As String
    Dim iBD_Aberta As Long
    Dim rsAnaFact As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    Dim rsTaxas As New ADODB.recordset
    Dim rsEntidades As New ADODB.recordset
    Dim flgPercentagem As Integer
    ' --------------------------------------
    ' PARAMETROS PARA PROCEDIMENTO FACTUS
    ' --------------------------------------
    Dim p_cod_rubr As String
    Dim p_valor_ent As String
    Dim p_valor_adic As String
    Dim p_valor_doe As String
    Dim p_valor_taxa As String
    Dim p_perc_desc As String
    Dim p_valor_total As String
    Dim p_cod_rubr_efr As String
    Dim p_nr_c As String
    Dim p_descr_rubr_efr As String
    Dim p_alt_prec As String
    Dim p_w_msg As String
    Dim subprecario As String
    Dim ResFACTUS As Integer
    
    On Error GoTo TrataErro
    ' -----------------------------------------------------------------------------------------------
    ' PROCURA A RUBRICA ASSOCIADA A ANALISE
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE cod_efr = " & Entidade & " AND  cod_ana = " & BL_TrataStringParaBD(codAna)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaFact.Open sSql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        sSql = "SELECT cod_ana_gh, flg_conta_membros FROM sl_ana_facturacao WHERE cod_efr IS NULL AND cod_ana = " & BL_TrataStringParaBD(codAna)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaFact.Open sSql, gConexao
        If rsAnaFact.RecordCount <= 0 Then
            ' ANALISE NAO ESTA MAPEADA PARA FACTUS
            IF_RetornaTaxaAnaliseEntidade = "-1"
            rsAnaFact.Close
            Set rsAnaFact = Nothing
            Exit Function
        End If
    End If
        
    ' ------------------------------------------------------------------------------------------
    ' ABRE CONEXAO COM FACTUS
    ' ------------------------------------------------------------------------------------------
    iBD_Aberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iBD_Aberta = 0 Then
        ' NAO EXISTE CONEXAO FACTUS
        IF_RetornaTaxaAnaliseEntidade = "-2"
    End If

    subprecario = IF_VerificaUsaSubPrecario(Entidade, p_data)
    If subprecario <> "" Then
        IF_RetornaTaxaAnaliseEntidade = IF_RetornaTaxaAnaliseSubPrec(False, codAna, subprecario, Entidade, t_utente, Utente, p_data, p_cod_rubr_efr, preco_unitario, qtd, nr_c, controlaIsentos, p_descr_rubr_efr)
        cod_rubr_efr = p_cod_rubr_efr
        descr_rubr_efr = p_descr_rubr_efr
        Exit Function
    End If
    
    ' -------------------------------------------------------
    ' SE ENVIA ENTIDADE E DATA CHAMA O PROCEDIMENTO DO FACTUS
    ' -------------------------------------------------------
    If gNaoUsaProcFactusTaxas <> mediSim Then
        If p_data <> "" Then
            If gTipoInstituicao = "PRIVADA" Then
                ResFACTUS = IF_SetDoente(t_utente, Utente)
                ResFACTUS = IF_ObtemPrecoRubr(Entidade, BL_HandleNull(rsAnaFact!cod_ana_gh, ""), p_data, p_valor_ent, p_valor_adic, p_valor_doe, p_valor_taxa, p_perc_desc, _
                                  p_valor_total, p_cod_rubr_efr, p_descr_rubr_efr, p_alt_prec, p_w_msg)
            End If
        End If
    End If
    
    If ResFACTUS > 0 Then
        If Mid(p_valor_ent, 1, 1) = "," Or Mid(p_valor_ent, 1, 1) = "." Or p_valor_ent = "" Then
            p_valor_ent = "0" & p_valor_ent
        End If
        If controlaIsentos = False Then
            IF_RetornaTaxaAnaliseEntidade = p_valor_ent
        ElseIf controlaIsentos = True Then
            IF_RetornaTaxaAnaliseEntidade = CDbl(Replace(BL_HandleNull(p_valor_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(p_valor_taxa, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(p_valor_doe, 0), ".", ","))
        End If
        cod_rubr_efr = p_cod_rubr_efr
        descr_rubr_efr = p_descr_rubr_efr
        'soliveira cantanhede
        If Entidade = 2 And gLAB = "ARC" Then
            nr_c = Replace(p_valor_ent, ".", ",") / 0.439
            nr_c = CInt(nr_c)
        Else
            nr_c = ""
        End If
    Else
    
        ' ------------------------------------------------------------------------------------------
        ' VERIFICA QUAL A ENTIDADE ACTIVA
        ' ------------------------------------------------------------------------------------------
        sSql = "SELECT * FROM sl_efr WHERE cod_efr = " & Entidade
        rsEntidades.CursorLocation = adUseServer
        rsEntidades.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsEntidades.Open sSql, gConexao
        If rsEntidades.RecordCount <= 0 Then
            ' NAO EXISTE ENTIDADE CODIFICADA
            IF_RetornaTaxaAnaliseEntidade = "-5"
        Else
        
            If gTipoInstituicao = "PRIVADA" Then
                'UALIA-370
                flgPercentagem = BL_HandleNull(rsEntidades!flg_perc_facturar, 0)
            End If
            
            ' ------------------------------------------------------------------------------------------
            ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
            ' ------------------------------------------------------------------------------------------
            sSql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
            sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
            sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "
            rsPortarias.CursorLocation = adUseServer
            rsPortarias.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsPortarias.Open sSql, gConexaoSecundaria
            If rsPortarias.RecordCount <= 0 Then
                ' NAO EXISTE PORTARIA ACTIVA
                IF_RetornaTaxaAnaliseEntidade = "-3"
            Else
                ' ------------------------------------------------------------------------------------------
                ' RETORNA A TAXA PARA A ANALISE EM CAUSA
                ' ------------------------------------------------------------------------------------------
                sSql = "SELECT val_pag_ent, val_pag_doe, val_taxa,cod_rubr_efr, descr_rubr_efr FROM fa_pr_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(BL_HandleNull(rsAnaFact!cod_ana_gh, ""))
                sSql = sSql & " AND portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria) & " AND user_rem IS NULL "
                rsTaxas.CursorLocation = adUseServer
                rsTaxas.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsTaxas.Open sSql, gConexaoSecundaria
                If rsTaxas.RecordCount <= 0 Then
                    ' NAO EXISTE TAXA CODIFICADA
                    IF_RetornaTaxaAnaliseEntidade = "-4"
                Else
                    If controlaIsentos = False Or (controlaIsentos = True And cod_hemodialise > mediComboValorNull) Or flgPercentagem = mediSim Then
                        ' alterado a pedido do bmonteiro para ljmanso
                        IF_RetornaTaxaAnaliseEntidade = CStr(BL_HandleNull(rsTaxas!val_pag_ent, 0))
                    Else
                        If BL_HandleNull(rsTaxas!val_Taxa, "0") = "0" Then
                            IF_RetornaTaxaAnaliseEntidade = CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_doe, 0), ".", ","))
                        ElseIf BL_HandleNull(rsTaxas!val_pag_doe, "0") = "0" Then
                            IF_RetornaTaxaAnaliseEntidade = CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(rsTaxas!val_Taxa, 0), ".", ","))
                        Else
                            IF_RetornaTaxaAnaliseEntidade = CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(rsTaxas!val_Taxa, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(p_valor_doe, 0), ".", ","))
                        End If
                    End If
                    If CDbl(IF_RetornaTaxaAnaliseEntidade) < 0 Then
                        IF_RetornaTaxaAnaliseEntidade = 0
                    End If
                    cod_rubr_efr = CStr(BL_HandleNull(rsTaxas!cod_rubr_efr, ""))
                    descr_rubr_efr = CStr(BL_HandleNull(rsTaxas!descr_rubr_efr, ""))
                End If
                rsTaxas.Close
                Set rsTaxas = Nothing
            End If
            rsPortarias.Close
            Set rsPortarias = Nothing
        End If
        rsEntidades.Close
        Set rsEntidades = Nothing
        preco_unitario = Replace(IF_RetornaTaxaAnaliseEntidade, ".", ",")
        Exit Function
    End If
    
    If IF_RetornaTaxaAnaliseEntidade > 0 And qtd > 0 Then
        preco_unitario = Replace(IF_RetornaTaxaAnaliseEntidade, ".", ",")
        IF_RetornaTaxaAnaliseEntidade = qtd * Replace(IF_RetornaTaxaAnaliseEntidade, ".", ",")
        IF_RetornaTaxaAnaliseEntidade = Replace(IF_RetornaTaxaAnaliseEntidade, ",", ".")
    Else
        preco_unitario = "0"
    End If
    
    rsAnaFact.Close
    Set rsAnaFact = Nothing
Exit Function
TrataErro:
    IF_RetornaTaxaAnaliseEntidade = "0"
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaTaxaAnaliseEntidade"
    Exit Function
    Resume Next
End Function



' -----------------------------------------------------------------------------------------------

' INSERE LINHA NO FACTUS POR REQUISICAO(FA_MOVI_RESP) ASSIM COMO NO SISLAB.

' -----------------------------------------------------------------------------------------------
Public Sub IF_Factura_RequisicaoPrivado(ByVal n_ord As Integer, ByVal t_episodio As String, ByVal episodio As String, _
                                        ByVal n_benef_doe As String, ByVal cod_efr As String, ByVal user_cri As String, ByVal cod_dom As Integer, _
                                        ByVal qtd_dom As Long, ByVal t_doente As String, ByVal doente As String, ByVal cod_empresa As String)
    Dim sSql As String
    Dim rv As Integer
    Dim rsResp As New ADODB.recordset
    Dim cod_Dom2 As String
    On Error GoTo TrataErro
    
    If cod_dom = -1 Or cod_dom = 0 Then
        cod_Dom2 = "NULL"
    Else
        cod_Dom2 = CStr(cod_dom)
    End If
    
    If cod_dom > 0 And qtd_dom = 0 Then
        qtd_dom = 1
    End If
    ' -----------------------------------------------------------------------------------------------
    ' VERIFICA SE JA EXISTE REGISTO PARA ESTA EFR E TRATA-SE DE UMA REJEICAO
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT * FROM fa_movi_resp WHERE episodio = " & BL_TrataStringParaBD(episodio) & " and COD_EFR = " & cod_efr
    'soliveira ljjno 16.12.2008
    'sSql = sSql & " AND n_ord =  " & N_Ord
    Set rsResp = New ADODB.recordset
    rsResp.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsResp.Open sSql, gConexaoSecundaria, adOpenStatic
    If rsResp.RecordCount <> 0 Then
        sSql = "UPDATE sl_movi_resp SET "
        sSql = sSql & " n_benef_doe = " & BL_TrataStringParaBD(n_benef_doe) & ", "
        sSql = sSql & " cod_efr = " & cod_efr & ", "
        sSql = sSql & " cod_dom = " & cod_Dom2 & ", "
        sSql = sSql & " qtd_dom = " & qtd_dom & ", "
        sSql = sSql & " doente =" & BL_TrataStringParaBD(doente) & ", "
        sSql = sSql & " t_doente =" & BL_TrataStringParaBD(t_doente) & " "
        sSql = sSql & " WHERE episodio =" & BL_TrataStringParaBD(episodio)
        sSql = sSql & " AND t_episodio =" & BL_TrataStringParaBD(t_episodio)
        sSql = sSql & " AND EMPRESA_ID =" & BL_TrataStringParaBD(cod_empresa)
        sSql = sSql & " AND cod_efr =" & cod_efr
        'sSql = sSql & " AND n_ord = " & N_Ord
        BG_ExecutaQuery_ADO sSql
        
        sSql = "UPDATE fa_movi_resp SET "
        sSql = sSql & " n_benef_doe = " & BL_TrataStringParaBD(n_benef_doe) & ", "
        sSql = sSql & " cod_efr = " & cod_efr & ", "
        sSql = sSql & " cod_dom = " & cod_Dom2 & ", "
        sSql = sSql & " qtd_dom = " & qtd_dom & ", "
        sSql = sSql & " doente =" & BL_TrataStringParaBD(doente) & ", "
        sSql = sSql & " t_doente =" & BL_TrataStringParaBD(t_doente) & " "
        sSql = sSql & " WHERE episodio =" & BL_TrataStringParaBD(episodio)
        sSql = sSql & " AND t_episodio =" & BL_TrataStringParaBD(t_episodio)
        sSql = sSql & " AND EMPRESA_ID =" & BL_TrataStringParaBD(cod_empresa)
        sSql = sSql & " AND cod_efr =" & cod_efr
        'sSql = sSql & " AND n_ord = " & N_Ord
        rv = BL_ExecutaQuery_Secundaria(sSql)
        If rv = -1 Then
            BL_LogFile_BD "Integracao_facturacao", "IF_Factura_requisicaoPrivado", "", "", "REQ: " & episodio
            Exit Sub
        End If
        
        'soliveira correccao lacto
        sSql = "UPDATE sl_requis SET mot_fact_rej = null, dt_fact_rej = null, hr_fact_rej = null, flg_facturado = " & gEstadoFactRequisFacturado
        sSql = sSql & " WHERE n_req = " & episodio
        BG_ExecutaQuery_ADO (sSql)
        
        sSql = "UPDATE sl_requis SET user_confirm = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", dt_confirm = " & BL_TrataDataParaBD(Bg_DaData_ADO)
        sSql = sSql & ", hr_confirm = " & BL_TrataStringParaBD(Bg_DaHora_ADO)
        sSql = sSql & " WHERE n_req = " & episodio & " AND user_confirm IS NULL "
        BG_ExecutaQuery_ADO (sSql)
        Exit Sub
    
    'soliveira ljjno 16.12.2008
    'se a EFR n�o existe na fa_movi_resp entao faz o insert
    Else
        ' -----------------------------------------------------------------------------------------------
        ' INSERE NO SISLAB
        ' -----------------------------------------------------------------------------------------------
        sSql = "INSERT INTO sl_movi_resp (n_ord,t_episodio, episodio, n_benef_doe, cod_efr,  user_cri, "
        sSql = sSql & " dt_cri,   cod_dom, qtd_dom, t_doente,empresa_id, doente) VALUES ( "
        sSql = sSql & n_ord & ", "
        sSql = sSql & BL_TrataStringParaBD(t_episodio) & ", "
        sSql = sSql & BL_TrataStringParaBD(episodio) & ", "
        sSql = sSql & BL_TrataStringParaBD(n_benef_doe) & ", "
        sSql = sSql & cod_efr & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        If gSGBD = gOracle Then
            sSql = sSql & "SYSDATE, "
        ElseIf gSGBD = gSqlServer Then
            sSql = sSql & "GETDATE(), "
        End If
        sSql = sSql & cod_Dom2 & ", "
        sSql = sSql & qtd_dom & ", "
        sSql = sSql & BL_TrataStringParaBD(t_doente) & ", "
        sSql = sSql & BL_TrataStringParaBD(cod_empresa) & ", "
        sSql = sSql & BL_TrataStringParaBD(doente) & ") "
        BG_ExecutaQuery_ADO sSql
        
        IF_SetContext cod_empresa

        ' -----------------------------------------------------------------------------------------------
        ' INSERE NO FACTUS
        ' -----------------------------------------------------------------------------------------------
        sSql = "INSERT INTO fa_movi_resp (n_ord,t_episodio, episodio, n_benef_doe, cod_efr,  user_cri, "
        sSql = sSql & " dt_cri,   cod_dom, qtd_dom, t_doente, doente,empresa_id) VALUES ( "
        sSql = sSql & n_ord & ", "
        sSql = sSql & BL_TrataStringParaBD(t_episodio) & ", "
        sSql = sSql & BL_TrataStringParaBD(episodio) & ", "
        sSql = sSql & BL_TrataStringParaBD(n_benef_doe) & ", "
        sSql = sSql & cod_efr & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        If gSGBD_SECUNDARIA = gOracle Then
            sSql = sSql & "SYSDATE, "
        ElseIf gSGBD_SECUNDARIA = gSqlServer Then
            sSql = sSql & "GETDATE(), "
        End If
        sSql = sSql & cod_Dom2 & ", "
        sSql = sSql & qtd_dom & ", "
        sSql = sSql & BL_TrataStringParaBD(t_doente) & ", "
        sSql = sSql & BL_TrataStringParaBD(doente) & ", "
        sSql = sSql & BL_TrataStringParaBD(cod_empresa) & ") "
        rv = BL_ExecutaQuery_Secundaria(sSql)
        If rv = -1 Then
            BL_LogFile_BD "Integracao_facturacao", "IF_Factura_requisicaoPrivado", "", "", "REQ: " & episodio
            Exit Sub
        Else
            sSql = "UPDATE fa_movi_resp SET episodio  = episodio WHERE t_episodio = 'SISLAB' and episodio = " & BL_TrataStringParaBD(episodio)
            rv = BL_ExecutaQuery_Secundaria(sSql)
            If rv = -1 Then
                BL_LogFile_BD "Integracao_facturacao", "IF_Factura_requisicaoPrivado", "", "", "REQ: " & episodio
                Exit Sub
            End If
        End If
        
    End If
    
    ' -----------------------------------------------------------------------------------------------
    ' COLOCA A REQUISICAO COMO TENDO SIDO FACTURADA
    ' -----------------------------------------------------------------------------------------------
        sSql = "UPDATE sl_requis SET mot_fact_rej = null, dt_fact_rej = null, hr_fact_rej = null, flg_facturado = " & gEstadoFactRequisFacturado
        sSql = sSql & ", user_confirm = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", dt_confirm = " & BL_TrataDataParaBD(Bg_DaData_ADO)
        sSql = sSql & ", hr_confirm = " & BL_TrataStringParaBD(Bg_DaHora_ADO)
        sSql = sSql & " WHERE n_req = " & episodio
    BG_ExecutaQuery_ADO sSql
    
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_requisicaoPrivado"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' FACTURACAO DE UMA ANALISE ( INSERT NA FA_MOVI_FACT ) PRIVADO!!

' -----------------------------------------------------------------------------------------------
Public Sub IF_Factura_AnalisePrivado(cod_prog As Long, n_seq_prog As Long, n_ord As Long, t_episodio As String, _
                                     episodio As String, t_doente As String, doente As String, dt_ini_real As String, _
                                     cod_rubr As String, nr_req_ars As String, _
                                     flg_pagou_taxa As String, qtd As Integer, cod_serv_exec As String, flg_estado As Integer, _
                                     chave_prog As String, user_cri As String, dt_cri As String, cod_isen_doe As Integer, _
                                     cor_p1 As String, flg_tip_req As String, n_ord_ins As Integer, _
                                     flg_estado_doe As Integer, serie_fac_doe As String, n_Fac_doe As Long, _
                                     cod_rubr_efr As String, val_pag_ent As String, desconto_utente As String, cod_empresa As String, _
                                     controlaIsentos As Boolean, flg_percentagem As Boolean, flg_insere_cod_rubr_efr As Boolean)
    Dim rs_aux As New ADODB.recordset
    Dim sSql As String
    Dim rv As Integer
    Dim iCodTipRubr As String
    Dim cod_grupo As String
    Dim cod_tip_rubr As String
    Dim iva As Integer
    Dim str_flg_estado_doe As String
    Dim str_n_fac_doe As String
    
    Dim val_pr_unit As String
    Dim perc_pag_efr As String
    Dim val_total As String
    Dim perc_pag_doe As String
    Dim val_pr_u_doe As String
    Dim val_pag_doe As String
    
    
    If flg_percentagem = True Then
        If controlaIsentos = True And cod_isen_doe = gTipoIsencaoIsento Then
            perc_pag_efr = "100"
            perc_pag_doe = "0"
            val_pr_u_doe = "0"
            val_pag_doe = "0"
            val_pr_unit = Replace(val_pag_ent, ",", ".")
            val_total = Replace(val_pag_ent, ",", ".")
        Else
            perc_pag_efr = Replace(100 - CDbl(Replace(desconto_utente, ".", ",")), ".", ",")
            perc_pag_doe = Replace(CStr(CDbl(Replace(desconto_utente, ".", ","))), ".", ",")
            val_pr_u_doe = Replace(CStr(BG_newRound((Replace(val_pag_ent, ".", ",") * CDbl(perc_pag_doe) / 100), 2)), ",", ".")
            val_pag_doe = Replace(CStr(BG_newRound((Replace(val_pag_ent, ".", ",") * CDbl(perc_pag_doe) / 100 * qtd), 2)), ",", ".")
            val_pr_unit = Replace(CStr(BG_newRound(Replace(val_pag_ent, ".", ",") - Replace(val_pr_u_doe, ".", ","), 2)), ",", ".")
            val_total = Replace(CStr(BG_newRound((Replace(val_pag_ent, ".", ",") * qtd) - Replace(val_pag_doe, ".", ","), 2)), ",", ".")
        End If
    Else
        If controlaIsentos = True And cod_isen_doe = gTipoIsencaoIsento Then
            perc_pag_efr = "100"
            perc_pag_doe = "0"
            val_pr_u_doe = "0"
            val_pag_doe = "0"
            val_pr_unit = Replace(val_pag_ent, ",", ".")
            val_total = Replace(val_pag_ent * qtd, ",", ".")
        Else
            val_pr_unit = "NULL"
            perc_pag_efr = "NULL"
            val_total = "NULL"
            perc_pag_doe = "NULL"
            val_pr_u_doe = "NULL"
            val_pag_doe = "NULL"
        End If
    End If
    
    
    
    ' FLG_ESTADO_DOE
    If flg_estado_doe = -1 Then
        str_flg_estado_doe = "null"
    Else
        str_flg_estado_doe = CStr(flg_estado_doe)
    End If
    
    'N_FAC_DOE
    If n_Fac_doe = -1 Then
        str_n_fac_doe = "null"
    Else
        str_n_fac_doe = CStr(n_Fac_doe)
    End If
    
    
    On Error GoTo TrataErro
    
    
    
    '------------------------------------------------------------------------------------------------
    ' VAI AO FACTUS BUSCAR O COD_GRUPO
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT cod_grupo FROM fa_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(cod_rubr) & ""
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    If rs_aux.RecordCount <> 0 Then
        cod_grupo = BL_HandleNull(rs_aux!cod_grupo, "0")
    Else
        cod_grupo = 0
    End If
    rs_aux.Close
    Set rs_aux = Nothing
    
    '-----------------------------------------------------------------------------------------------
    ' VAI AO FACTUS BUSCAR O COD_TIP_RUBR
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT cod_tip_rubr FROM fa_grup_rubr WHERE cod_grupo = " & BL_TrataStringParaBD(cod_grupo) & ""
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    
    If rs_aux.RecordCount <> 0 Then
        cod_tip_rubr = BL_HandleNull(rs_aux!cod_tip_rubr, "0")
    Else
        cod_tip_rubr = 0
    End If
    
    'VERIFICA SE INSERE OU NAO O CODIGO DA RUBRICA DA ENTIDADE
    If flg_insere_cod_rubr_efr = False Then
        cod_rubr_efr = "NULL"
    Else
        cod_rubr_efr = BL_TrataStringParaBD(cod_rubr_efr)
    End If
    
    iva = IF_RetornaIVA(cod_empresa, cod_grupo)
    If iva > 0 And flg_percentagem = True Then
        val_pr_u_doe = Replace(BG_newRound(((Replace(val_pr_u_doe, ".", ",") * 23) / 100) + Replace(val_pr_u_doe, ".", ","), 2), ",", ".")
        val_pag_doe = Replace(BG_newRound(((Replace(val_pag_doe, ".", ",") * 23) / 100) + Replace(val_pag_doe, ".", ","), 2), ",", ".")
        val_pr_unit = Replace(BG_newRound(((Replace(val_pr_unit, ".", ",") * 23) / 100) + Replace(val_pr_unit, ".", ","), 2), ",", ".")
        val_total = Replace(BG_newRound(((Replace(val_total, ".", ",") * 23) / 100) + Replace(val_total, ".", ","), 2), ",", ".")
    End If
    
    '------------------------------------------------------------------------------------------------
    ' INSERE NO SISLAB
    ' -----------------------------------------------------------------------------------------------
    sSql = "INSERT INTO sl_movi_fact ( cod_prog, n_seq_prog, n_ord, t_episodio, episodio, t_doente, doente, "
    sSql = sSql & " dt_ini_real,  cod_rubr, cod_grupo, cod_tip_rubr, nr_req_ars, flg_pagou_taxa, qtd, cod_serv_exec, "
    sSql = sSql & " flg_estado, chave_prog, user_cri, dt_cri, cod_isen_doe, flg_tip_req, n_ord_ins, "
    sSql = sSql & " flg_estado_doe, serie_fac_doe, n_fac_doe, cod_rubr_efr "
    sSql = sSql & " , perc_pag_efr, perc_pag_doe, val_pr_unit, val_total, val_pr_u_doe, val_pag_doe) VALUES( "
    sSql = sSql & cod_prog & ", "
    sSql = sSql & n_seq_prog & ", "
    sSql = sSql & n_ord & ", "
    sSql = sSql & BL_TrataStringParaBD(t_episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(t_doente) & ", "
    sSql = sSql & BL_TrataStringParaBD(doente) & ", "
    sSql = sSql & BL_TrataDataParaBD(dt_ini_real) & ", "
    sSql = sSql & cod_rubr & ", "
    sSql = sSql & cod_grupo & ", "
    sSql = sSql & cod_tip_rubr & ", "
    sSql = sSql & BL_TrataStringParaBD(nr_req_ars) & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_pagou_taxa) & ", "
    sSql = sSql & qtd & ", "
    sSql = sSql & cod_serv_exec & ", "
    sSql = sSql & flg_estado & ", "
    sSql = sSql & BL_TrataStringParaBD(chave_prog) & ", "
    sSql = sSql & BL_TrataStringParaBD(user_cri) & ", "
    If gSGBD = gOracle Then
        sSql = sSql & "SYSDATE, "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & "GETDATE(), "
    End If
    sSql = sSql & cod_isen_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_tip_req) & ", "
    sSql = sSql & n_ord_ins & ", "
    sSql = sSql & str_flg_estado_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(serie_fac_doe) & ", "
    sSql = sSql & str_n_fac_doe & ", "
    sSql = sSql & cod_rubr_efr & ", "
    sSql = sSql & Replace(perc_pag_efr, ",", ".") & ", "
    sSql = sSql & Replace(perc_pag_doe, ",", ".") & ", "
    sSql = sSql & val_pr_unit & ", "
    sSql = sSql & val_total & ", "
    sSql = sSql & val_pr_u_doe & ", "
    sSql = sSql & val_pag_doe & ") "
    BG_ExecutaQuery_ADO (sSql)
    
    
    ' -----------------------------------------------------------------------------------------------
    ' INSERE NO FACTUS
    ' -----------------------------------------------------------------------------------------------
    IF_SetContext cod_empresa
    
    sSql = "INSERT INTO fa_movi_fact ( cod_prog, n_seq_prog, n_ord, t_episodio, episodio, t_doente, doente, "
    sSql = sSql & " dt_ini_real,  cod_rubr, cod_grupo, cod_tip_rubr, nr_req_ars, flg_pagou_taxa, qtd, cod_serv_exec, "
    sSql = sSql & " flg_estado, chave_prog, user_cri, dt_cri, cod_isen_doe,empresa_id,flg_tip_req,n_ord_ins, "
    sSql = sSql & " flg_estado_doe, serie_fac_doe, n_fac_doe, cod_rubr_efr"
    sSql = sSql & ", perc_pag_efr, perc_pag_doe, val_pr_unit, val_total, val_pr_u_doe, val_pag_doe) VALUES( "
    sSql = sSql & cod_prog & ", "
    sSql = sSql & n_seq_prog & " , "
    sSql = sSql & n_ord & ", "
    sSql = sSql & BL_TrataStringParaBD(t_episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(t_doente) & ", "
    sSql = sSql & BL_TrataStringParaBD(doente) & ", "
    sSql = sSql & BL_TrataDataParaBD(dt_ini_real) & ", "
    sSql = sSql & cod_rubr & ", "
    sSql = sSql & cod_grupo & ", "
    sSql = sSql & cod_tip_rubr & ", "
    sSql = sSql & BL_TrataStringParaBD(nr_req_ars) & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_pagou_taxa) & ", "
    sSql = sSql & qtd & ", "
    sSql = sSql & cod_serv_exec & ", "
    sSql = sSql & flg_estado & ", "
    sSql = sSql & BL_TrataStringParaBD(Mid(chave_prog, 1, 15)) & ", "
    sSql = sSql & BL_TrataStringParaBD(user_cri) & ", "
    If gSGBD_SECUNDARIA = gOracle Then
        sSql = sSql & "SYSDATE, "
    ElseIf gSGBD_SECUNDARIA = gSqlServer Then
        sSql = sSql & "GETDATE(), "
    End If
    sSql = sSql & cod_isen_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_empresa) & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_tip_req) & ", "
    sSql = sSql & n_ord_ins & ", "
    sSql = sSql & str_flg_estado_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(serie_fac_doe) & ", "
    sSql = sSql & str_n_fac_doe & ", "
    sSql = sSql & cod_rubr_efr & ", "
    sSql = sSql & Replace(perc_pag_efr, ",", ".") & ", "
    sSql = sSql & Replace(perc_pag_doe, ",", ".") & ", "
    sSql = sSql & val_pr_unit & ", "
    sSql = sSql & val_total & ", "
    sSql = sSql & val_pr_u_doe & ", "
    sSql = sSql & val_pag_doe & ") "
    
    rv = BL_ExecutaQuery_Secundaria(sSql)
    If rv = -1 Then
        BL_LogFile_BD "Integracao_facturacao", "IF_Factura_AnalisePrivado", "", "", "REQ: " & episodio & " RUBR: " & cod_rubr
        Exit Sub
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_AnalisePrivado"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' CHAMA PACKAGE DO FACTUS PARA DEVOLVER PRE�OS DAS RUBRICAS

' -----------------------------------------------------------------------------------------------
Function IF_ObtemPrecoRubr(p_efr As String, p_cod_rubr As String, p_data As String, ByRef p_valor_ent As String, _
                           ByRef p_valor_adic As String, ByRef p_valor_doe As String, ByRef p_valor_taxa As String, _
                           ByRef p_perc_desc As String, ByRef p_valor_total As String, ByRef p_cod_rubr_efr As String, _
                           ByRef p_descr_rubr_efr As String, ByRef p_alt_prec As String, ByRef p_w_msg As String) As Integer

    'comando
    Dim CmdPreco As New ADODB.Command


    'variaveis de input
    Dim PmtP_RetValue As ADODB.Parameter
    Dim PmtP_EFR As ADODB.Parameter
    Dim PmtP_cod_rubr As ADODB.Parameter
    Dim PmtP_data As ADODB.Parameter
    Dim PmtP_empresa_id As ADODB.Parameter
    Dim PmtP_inclui_prec_part As ADODB.Parameter



    'variaveis de in/out
    Dim PmtP_valor_ent As ADODB.Parameter
    Dim PmtP_valor_adic As ADODB.Parameter
    Dim PmtP_valor_doe As ADODB.Parameter
    Dim PmtP_valor_taxa As ADODB.Parameter
    Dim PmtP_perc_desc As ADODB.Parameter
    Dim PmtP_valor_total As ADODB.Parameter
    Dim PmtP_cod_rubr_efr As ADODB.Parameter
    Dim PmtP_descr_rubr_efr As ADODB.Parameter
    Dim PmtP_alt_prec As ADODB.Parameter
    Dim PmtP_w_msg As ADODB.Parameter


    On Error GoTo TrataErro

    With CmdPreco
        .CommandText = "PCK_PRECARIOS_SISLAB.OBTEM_PRECO_RUBR_EMPR"
        .CommandType = adCmdStoredProc
        .ActiveConnection = gConexaoSecundaria
    End With

    ' criar os parametros - INTPUT
    Set PmtP_RetValue = CmdPreco.CreateParameter("RETURN_VALUE", adVarChar, adParamReturnValue, 1)
    Set PmtP_EFR = CmdPreco.CreateParameter("p_efr", adVarChar, adParamInput, 7)
    Set PmtP_cod_rubr = CmdPreco.CreateParameter("p_cod_rubr", adVarChar, adParamInput, 50)
    Set PmtP_data = CmdPreco.CreateParameter("p_data", adVarChar, adParamInput, 10)
    Set PmtP_empresa_id = CmdPreco.CreateParameter("p_empresa_id", adVarChar, adParamInput, 20)
    Set PmtP_inclui_prec_part = CmdPreco.CreateParameter("p_inclui_prec_part", adVarChar, adParamInput, 20)
    
    ' criar os parametros - INTPUT/OUTPUT
    Set PmtP_valor_ent = CmdPreco.CreateParameter("p_valor_ent", adVarChar, adParamInputOutput, 10)
    Set PmtP_valor_adic = CmdPreco.CreateParameter("p_valor_adic", adVarChar, adParamInputOutput, 10)
    Set PmtP_valor_doe = CmdPreco.CreateParameter("p_valor_doe", adVarChar, adParamInputOutput, 10)
    Set PmtP_valor_taxa = CmdPreco.CreateParameter("p_valor_taxa", adVarChar, adParamInputOutput, 10)
    Set PmtP_perc_desc = CmdPreco.CreateParameter("p_perc_desc", adVarChar, adParamInputOutput, 10)
    Set PmtP_valor_total = CmdPreco.CreateParameter("p_valor_total", adVarChar, adParamInputOutput, 10)
    Set PmtP_cod_rubr_efr = CmdPreco.CreateParameter("p_cod_rubr_efr", adVarChar, adParamInputOutput, 30)
    Set PmtP_descr_rubr_efr = CmdPreco.CreateParameter("p_descr_rubr_efr", adVarChar, adParamInputOutput, 200)
    Set PmtP_alt_prec = CmdPreco.CreateParameter("p_alt_prec", adVarChar, adParamInputOutput, 1)
    Set PmtP_w_msg = CmdPreco.CreateParameter("w_msg", adVarChar, adParamInputOutput, 2000)

    'acrescentar os objectos parameter ao comando
    CmdPreco.Parameters.Append PmtP_RetValue
    CmdPreco.Parameters.Append PmtP_EFR
    CmdPreco.Parameters.Append PmtP_cod_rubr
    CmdPreco.Parameters.Append PmtP_data
    CmdPreco.Parameters.Append PmtP_empresa_id
    CmdPreco.Parameters.Append PmtP_inclui_prec_part
    CmdPreco.Parameters.Append PmtP_valor_ent
    CmdPreco.Parameters.Append PmtP_valor_adic
    CmdPreco.Parameters.Append PmtP_valor_doe
    CmdPreco.Parameters.Append PmtP_valor_taxa
    CmdPreco.Parameters.Append PmtP_perc_desc
    CmdPreco.Parameters.Append PmtP_valor_total
    CmdPreco.Parameters.Append PmtP_cod_rubr_efr
    CmdPreco.Parameters.Append PmtP_descr_rubr_efr
    CmdPreco.Parameters.Append PmtP_alt_prec
    CmdPreco.Parameters.Append PmtP_w_msg


    'atribuir os valores de entrada a um recordset
    CmdPreco.Parameters("P_EFR") = p_efr
    CmdPreco.Parameters("P_COD_RUBR") = p_cod_rubr
    CmdPreco.Parameters("P_DATA") = p_data
    CmdPreco.Parameters("P_EMPRESA_ID") = Null
    CmdPreco.Parameters("P_INCLUI_PREC_PART") = Null
    CmdPreco.Parameters("P_VALOR_ENT") = Null
    CmdPreco.Parameters("P_VALOR_ADIC") = Null
    CmdPreco.Parameters("P_VALOR_DOE") = Null
    CmdPreco.Parameters("P_VALOR_TAXA") = Null
    CmdPreco.Parameters("P_PERC_DESC") = Null
    CmdPreco.Parameters("P_VALOR_TOTAL") = Null
    CmdPreco.Parameters("P_COD_RUBR_EFR") = Null
    CmdPreco.Parameters("P_DESCR_RUBR_EFR") = Null
    CmdPreco.Parameters("P_ALT_PREC") = Null
    CmdPreco.Parameters("W_MSG") = Null

    'executa o comando
    CmdPreco.Execute

    'Atribui valores de resultado
    p_efr = Trim(CmdPreco.Parameters.item("P_EFR").value)
    p_cod_rubr = Trim(CmdPreco.Parameters.item("P_COD_RUBR").value)
    p_data = Trim(CmdPreco.Parameters.item("P_DATA").value)
    p_valor_ent = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_VALOR_ENT").value), "")
    p_valor_adic = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_VALOR_ADIC").value), "")
    p_valor_doe = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_VALOR_DOE").value), "")
    p_valor_taxa = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_VALOR_TAXA").value), "")
    p_perc_desc = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_PERC_DESC").value), "")
    p_valor_total = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_VALOR_TOTAL").value), "")
    p_cod_rubr_efr = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_COD_RUBR_EFR").value), "")
    p_descr_rubr_efr = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_DESCR_RUBR_EFR").value), "")
    p_alt_prec = BL_HandleNull(Trim(CmdPreco.Parameters.item("P_ALT_PREC").value), "")
    p_w_msg = BL_HandleNull(Trim(CmdPreco.Parameters.item("W_MSG").value), "")
    
    If p_valor_taxa <> "" Then
        IF_ObtemPrecoRubr = 1
    Else
        IF_ObtemPrecoRubr = 0
    End If

Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_ObtemPrecoRubr"
    IF_ObtemPrecoRubr = -1
    Exit Function
    Resume Next
End Function


' ---------------------------------------------------------------------------------------------------------

' ENTRA CODIGO DA RUBRICA PARA ENTIDADE(!) E DEVOLVE LISTA DE RUBRICAS(SISLAB) ASSOCIADAS A RUBRICA ENVIADA
' REGISTAR ANALISES LENDO CODIGOS DE BARRAS DOS P1's
' DEVOLVE: (123,111,222,333,...,999)

' ---------------------------------------------------------------------------------------------------------
Public Function IF_ObtemRubrParaEFR(codEfr As String, codRubrEfr As String) As String
    Dim sSql As String
    Dim rsPortarias As New ADODB.recordset
    Dim rsEntidades As New ADODB.recordset
    Dim rsTaxas As New ADODB.recordset
    Dim iBD_Aberta As Long
    Dim temp As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    ' -----------------------
    ' ABRE CONEXAO COM FACTUS
    ' -----------------------
    If UCase(HIS.nome) <> "SONHO" Then
        iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
        If iBD_Aberta = 0 Then
           Exit Function
        End If
    End If
    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & codEfr
    rsEntidades.CursorLocation = adUseServer
    rsEntidades.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    If UCase(HIS.nome) <> "SONHO" Then
        rsEntidades.Open sSql, gConexaoSecundaria
    Else
        rsEntidades.Open sSql, gConexao
    End If
    If rsEntidades.RecordCount > 0 Then
        
        ' --------------------------------------------------------
        ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
        ' --------------------------------------------------------
        sSql = "SELECT portaria FROM fa_portarias WHERE cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0")
        sSql = sSql & " AND ((dt_ini <= trunc(sysdate) AND dt_fim is null) OR "
        sSql = sSql & " (dt_ini <= trunc(sysdate) AND dt_fim >= trunc(sysdate))) AND user_rem is null "
        rsPortarias.CursorLocation = adUseServer
        rsPortarias.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        If UCase(HIS.nome) <> "SONHO" Then
            rsPortarias.Open sSql, gConexaoSecundaria
        Else
            rsPortarias.Open sSql, gConexao
        End If
        If rsPortarias.RecordCount > 0 Then
        
            ' --------------------------------------
            ' RETORNA A TAXA PARA A ANALISE EM CAUSA
            ' --------------------------------------
            sSql = "SELECT cod_rubr FROM fa_pr_rubr WHERE " 'cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0")
            sSql = sSql & " portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria)
            sSql = sSql & " AND replace(cod_rubr_efr,'.','') = " & BL_TrataStringParaBD(Replace(codRubrEfr, ".", "")) & " AND user_rem IS NULL "
            
            rsTaxas.CursorLocation = adUseServer
            rsTaxas.CursorType = adOpenStatic
            If UCase(HIS.nome) <> "SONHO" Then
                rsTaxas.Open sSql, gConexaoSecundaria
            Else
                rsTaxas.Open sSql, gConexao
            End If
            If rsTaxas.RecordCount > 0 Then
                temp = "("
                i = 1
                While Not rsTaxas.EOF
                    temp = temp & BL_TrataStringParaBD(BL_HandleNull(rsTaxas!cod_rubr, "0"))
                    If i < rsTaxas.RecordCount Then
                        temp = temp & ", "
                    Else
                        temp = temp & ")"
                    End If
                    i = i + 1
                    rsTaxas.MoveNext
                Wend
            End If
            rsTaxas.Close
            Set rsTaxas = Nothing
        End If
        rsPortarias.Close
        Set rsPortarias = Nothing
    End If
    rsEntidades.Close
    Set rsEntidades = Nothing

    IF_ObtemRubrParaEFR = temp
    If UCase(HIS.nome) <> "SONHO" Then
        gConexaoSecundaria.Close
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_ObtemRubrParaEFR"
    Exit Function
    Resume Next
End Function



' -----------------------------------------------------------------------------------------------

' CHAMA PACKAGE DO FACTUS PARA DEVOLVER PRE�OS DAS RUBRICAS

' -----------------------------------------------------------------------------------------------
Function IF_SetDoente(pT_Doente As String, p_Doente As String) As Integer

    'comando
    Dim CmdSetDoente As New ADODB.Command


    Dim PmtP_RetValue As ADODB.Parameter
    Dim PmtPT_Doente As ADODB.Parameter
    Dim PmtP_Doente As ADODB.Parameter

    On Error GoTo TrataErro

    With CmdSetDoente
        .CommandText = "PCK_PRECARIOS_SISLAB.SET_DOENTE"
        .CommandType = adCmdStoredProc
        .ActiveConnection = gConexaoSecundaria
    End With

    ' criar os parametros - INTPUT
    Set PmtP_RetValue = CmdSetDoente.CreateParameter("RETURN_VALUE", adVarChar, adParamReturnValue, 1)
    Set PmtPT_Doente = CmdSetDoente.CreateParameter("pT_Doente", adVarChar, adParamInput, 7)
    Set PmtP_Doente = CmdSetDoente.CreateParameter("p_Doente", adVarChar, adParamInput, 50)

    'acrescentar os objectos parameter ao comando
    CmdSetDoente.Parameters.Append PmtP_RetValue
    CmdSetDoente.Parameters.Append PmtPT_Doente
    CmdSetDoente.Parameters.Append PmtP_Doente


    'atribuir os valores de entrada a um recordset
    CmdSetDoente.Parameters("PT_DOENTE") = pT_Doente
    CmdSetDoente.Parameters("P_DOENTE") = p_Doente

    'executa o comando
    CmdSetDoente.Execute

    
    IF_SetDoente = 1

Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_SetDoente"
    IF_SetDoente = -1
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' RETORNA QUANTIDADE DE MEMBROS A FACTURAR PARA UMA ANALISE

' -----------------------------------------------------------------------------------------------

Public Function IF_ContaMembros(codAna As String, codEfr As String) As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
        sSql = "SELECT * FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr = " & codEfr
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            IF_ContaMembros = BL_HandleNull(rsAna!flg_conta_membros, 0)
        Else
            rsAna.Close
            sSql = "SELECT * FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr IS NULL "
            sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                IF_ContaMembros = BL_HandleNull(rsAna!flg_conta_membros, 0)
            End If
        End If
        rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_ContaMembros"
    IF_ContaMembros = 0
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE DETERMINADA ANALISE SO FACTURA SE OUTRA NAO ESTIVER MARCADA

' -----------------------------------------------------------------------------------------------
Public Function IF_Verifica_Regra_PRIVADO(codAna As String, Entidade As String) As String
    Dim sSql As String
    Dim rsAnaFact As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT flg_regra, cod_ana_regra FROM sl_ana_facturacao WHERE cod_efr = " & Entidade & " AND  cod_ana = " & BL_TrataStringParaBD(codAna)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaFact.Open sSql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        sSql = "SELECT flg_regra, cod_ana_regra FROM sl_ana_facturacao WHERE cod_Efr IS NULL AND  cod_ana = " & BL_TrataStringParaBD(codAna)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaFact.Open sSql, gConexao
        If rsAnaFact.RecordCount <= 0 Then
            IF_Verifica_Regra_PRIVADO = ""
            rsAnaFact.Close
            Set rsAnaFact = Nothing
        Else
            If BL_HandleNull(rsAnaFact!Flg_Regra, 0) = 1 Then
                IF_Verifica_Regra_PRIVADO = BL_HandleNull(rsAnaFact!Cod_Ana_Regra, "")
            Else
                IF_Verifica_Regra_PRIVADO = ""
            End If
            rsAnaFact.Close
            Set rsAnaFact = Nothing
        End If
    Else
        If BL_HandleNull(rsAnaFact!Flg_Regra, 0) = 1 Then
            IF_Verifica_Regra_PRIVADO = BL_HandleNull(rsAnaFact!Cod_Ana_Regra, "")
        Else
            IF_Verifica_Regra_PRIVADO = ""
        End If
        rsAnaFact.Close
        Set rsAnaFact = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Verifica_Regra_PRIVADO"
    IF_Verifica_Regra_PRIVADO = ""
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE � PARA FACTURAR OUTRA ANALISE PARA ALEM DA MARCADA

' -----------------------------------------------------------------------------------------------
Public Function IF_VerificaFacturarNovaAnalise(codAna As String, Entidade As String) As String
    Dim sSql As String
    Dim rsAnaFact As New ADODB.recordset
    On Error GoTo TrataErro
    IF_VerificaFacturarNovaAnalise = ""
    sSql = "SELECT flg_ana_facturar, cod_ana_facturar FROM sl_ana_facturacao WHERE cod_efr = " & Entidade & " AND  cod_ana = " & BL_TrataStringParaBD(codAna)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaFact.Open sSql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        sSql = "SELECT flg_ana_facturar, cod_ana_facturar FROM sl_ana_facturacao WHERE cod_efr IS NULL AND cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr is null "
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaFact.Open sSql, gConexao
        If rsAnaFact.RecordCount <= 0 Then
            rsAnaFact.Close
            Set rsAnaFact = Nothing
            Exit Function
        Else
            If BL_HandleNull(rsAnaFact!flg_ana_facturar, 0) = 1 Then
                IF_VerificaFacturarNovaAnalise = BL_HandleNull(rsAnaFact!cod_ana_facturar, "")
            End If
            rsAnaFact.Close
            Set rsAnaFact = Nothing
        End If
    Else
        If BL_HandleNull(rsAnaFact!flg_ana_facturar, 0) = 1 Then
            IF_VerificaFacturarNovaAnalise = BL_HandleNull(rsAnaFact!cod_ana_facturar, "")
        End If
        rsAnaFact.Close
        Set rsAnaFact = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_VerificaFacturarNovaAnalise"
    IF_VerificaFacturarNovaAnalise = ""
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' CONFIRMA SE REQUISICAO JA ESTA NO FACTUS

' -----------------------------------------------------------------------------------------------
Public Function IF_ConfirmaFacturado(n_req As String) As Boolean
    Dim sSql As String
    Dim rsFACTUS As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT count(*) conta FROM fa_movi_resp WHERE episodio = " & BL_TrataStringParaBD(n_req)
    rsFACTUS.CursorLocation = adUseServer
    rsFACTUS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFACTUS.Open sSql, gConexaoSecundaria
    If rsFACTUS.RecordCount > 0 Then
        If BL_HandleNull(rsFACTUS!conta, 0) > 0 Then
            IF_ConfirmaFacturado = True
            Exit Function
        Else
            sSql = "DELETE FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(n_req)
            BL_ExecutaQuery_Secundaria (sSql)
            
            sSql = "DELETE FROM sl_movi_fact WHERE episodio = " & BL_TrataStringParaBD(n_req)
            BG_ExecutaQuery_ADO (sSql)
            
            sSql = "DELETE FROM sl_movi_resp WHERE episodio = " & BL_TrataStringParaBD(n_req)
            BG_ExecutaQuery_ADO (sSql)
            
            sSql = "UPDATE sl_requis SET flg_facturado = 0 WHERE n_req = " & BL_TrataStringParaBD(n_req)
            BG_ExecutaQuery_ADO (sSql)
            
            sSql = "UPDATE sl_recibos_det SET flg_facturado = 0 WHERE n_req = " & BL_TrataStringParaBD(n_req)
            BG_ExecutaQuery_ADO (sSql)
            
            IF_ConfirmaFacturado = False
        End If
    Else
        IF_ConfirmaFacturado = False
    End If
    rsFACTUS.Close
    Set rsFACTUS = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_ConfirmaFacturado"
    IF_ConfirmaFacturado = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE COMPARTICIPA DOMICILIO

' -----------------------------------------------------------------------------------------------
Public Function IF_EFRComparticipaDomicilio(cod_efr As String) As Boolean
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    IF_EFRComparticipaDomicilio = True
    sSql = "SELECT FLG_COMPART_DOM FROM sl_efr where cod_efr = " & BL_TrataStringParaBD(cod_efr)
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRec.Open sSql, gConexao
    If rsRec.RecordCount > 0 Then
        If BL_HandleNull(rsRec!flg_compart_dom, 0) = 1 Then
            IF_EFRComparticipaDomicilio = True
        Else
            IF_EFRComparticipaDomicilio = False
        End If
    End If
    rsRec.Close
    Set rsRec = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_EFRComparticipaDomicilio"
    IF_EFRComparticipaDomicilio = False
    Exit Function
    Resume Next
End Function



Public Function IF_RetornaCodAnaGH(cod_ana_sislab As String, cod_efr As String) As String
    Dim sSql As String
    Dim rsAnalises As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT  x3.cod_ana_gh "
    sSql = sSql & " FROM  sl_ana_facturacao x3 "
    sSql = sSql & " WHERE x3.cod_ana  = " & BL_TrataStringParaBD(cod_ana_sislab) & " AND x3.cod_efr = " & cod_efr
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnalises.Open sSql, gConexao
    
    If rsAnalises.RecordCount = 0 Then
        rsAnalises.Close
        sSql = "SELECT  x3.cod_ana_gh "
        sSql = sSql & " FROM  sl_ana_facturacao x3 "
        sSql = sSql & " WHERE x3.cod_efr IS NULL AND x3.cod_ana  = " & BL_TrataStringParaBD(cod_ana_sislab)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnalises.CursorLocation = adUseServer
        rsAnalises.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnalises.Open sSql, gConexao
        If rsAnalises.RecordCount >= 1 Then
            IF_RetornaCodAnaGH = BL_HandleNull(rsAnalises!cod_ana_gh, "")
        Else
            IF_RetornaCodAnaGH = ""
        End If
    Else
        IF_RetornaCodAnaGH = BL_HandleNull(rsAnalises!cod_ana_gh, "")
    End If
    rsAnalises.Close
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaCodAnaGH"
    IF_RetornaCodAnaGH = ""
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE UTENTE TEM DIREITO A DESCONTO POR TER MAIS DE 65 ANOS

' -----------------------------------------------------------------------------------------------
Public Function IF_VerificaRegraArs(t_utente As String, Utente As String, cod_efr As String, DataReq As String) As Boolean
    Dim sSql As String
    Dim RsARS As New ADODB.recordset
    Dim idade As String
    On Error GoTo TrataErro
    sSql = "SELECT flg_65anos from  sl_efr where cod_efr = " & BL_TrataStringParaBD(cod_efr)
    RsARS.CursorLocation = adUseServer
    RsARS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsARS.Open sSql, gConexao
    If RsARS.RecordCount >= 1 Then
        If BL_HandleNull(RsARS!flg_65anos, 0) = 0 Then
            IF_VerificaRegraArs = False
        Else
            RsARS.Close
            sSql = "SELECT dt_nasc_ute "
            sSql = sSql & " FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(t_utente) & " AND utente =" & BL_TrataStringParaBD(Utente)
            RsARS.CursorLocation = adUseServer
            RsARS.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            RsARS.Open sSql, gConexao
            If RsARS.RecordCount >= 1 Then
                idade = BG_CalculaIdade(BL_HandleNull(RsARS!dt_nasc_ute, Bg_DaData_ADO), CDate(DataReq))
                If CLng(Mid(idade, 1, InStr(1, idade, " "))) >= 65 Then
                    IF_VerificaRegraArs = True
                Else
                    IF_VerificaRegraArs = False
                End If
            Else
                IF_VerificaRegraArs = False
            End If
        End If
    Else
        IF_VerificaRegraArs = False
    End If
    Set RsARS = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_VerificaRegraArs"
    IF_VerificaRegraArs = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' FACTURACAO DE UMA ANALISE ( INSERT NA FA_MOVI_FACT ) PRIVADO!!

' -----------------------------------------------------------------------------------------------
Public Sub IF_Factura_AnaliseExtra(cod_prog As Long, n_seq_prog As Long, n_ord As Long, t_episodio As String, _
                                     episodio As String, t_doente As String, doente As String, dt_ini_real As String, _
                                     cod_rubr As String, nr_req_ars As String, _
                                     flg_pagou_taxa As String, qtd As Integer, cod_serv_exec As String, flg_estado As Integer, _
                                     chave_prog As String, user_cri As String, dt_cri As String, cod_isen_doe As Integer, _
                                     cor_p1 As String, flg_tip_req As String, n_ord_ins As Integer, _
                                     flg_estado_doe As Integer, serie_fac_doe As String, n_Fac_doe As Long, _
                                     cod_rubr_efr As String, valorRecibo As String, cod_empresa As String)
    Dim sSql As String
    Dim rv As Integer
    Dim iCodTipRubr As String
    Dim cod_grupo As String
    Dim cod_tip_rubr As String
    
    Dim str_flg_estado_doe As String
    Dim str_n_fac_doe As String
    
    Dim val_pr_unit As String
    Dim val_total As String
    On Error GoTo TrataErro
    
    If valorRecibo <> "" Then
        val_pr_unit = Replace("-" & valorRecibo, ",", ".")
        val_total = Replace("-" & valorRecibo, ",", ".")
    Else
        val_pr_unit = "NULL"
        val_total = "NULL"
    End If
    
    ' FLG_ESTADO_DOE
    If flg_estado_doe = -1 Then
        str_flg_estado_doe = "null"
    Else
        str_flg_estado_doe = CStr(flg_estado_doe)
    End If
    
    'N_FAC_DOE
    If n_Fac_doe = -1 Then
        str_n_fac_doe = "null"
    Else
        str_n_fac_doe = CStr(n_Fac_doe)
    End If
    
    Dim rs_aux As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    
    
    '------------------------------------------------------------------------------------------------
    ' VAI AO FACTUS BUSCAR O COD_GRUPO
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT cod_grupo FROM fa_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(cod_rubr) & ""
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    If rs_aux.RecordCount <> 0 Then
        cod_grupo = BL_HandleNull(rs_aux!cod_grupo, "0")
    Else
        cod_grupo = 0
    End If
    rs_aux.Close
    Set rs_aux = Nothing
    
    '-----------------------------------------------------------------------------------------------
    ' VAI AO FACTUS BUSCAR O COD_TIP_RUBR
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT cod_tip_rubr FROM fa_grup_rubr WHERE cod_grupo = " & BL_TrataStringParaBD(cod_grupo) & ""
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    
    If rs_aux.RecordCount <> 0 Then
        cod_tip_rubr = BL_HandleNull(rs_aux!cod_tip_rubr, "0")
    Else
        cod_tip_rubr = 0
    End If
    
    
    '------------------------------------------------------------------------------------------------
    ' INSERE NO SISLAB
    ' -----------------------------------------------------------------------------------------------
    sSql = "INSERT INTO sl_movi_fact ( cod_prog, n_seq_prog, n_ord, t_episodio, episodio, t_doente, doente, "
    sSql = sSql & " dt_ini_real,  cod_rubr, cod_grupo, cod_tip_rubr, nr_req_ars, flg_pagou_taxa, qtd, cod_serv_exec, "
    sSql = sSql & " flg_estado, chave_prog, user_cri, dt_cri, cod_isen_doe, flg_tip_req, n_ord_ins, "
    sSql = sSql & " flg_estado_doe, serie_fac_doe, n_fac_doe, cod_rubr_efr "
    sSql = sSql & " , perc_pag_efr, perc_pag_doe, val_pr_unit, val_total, val_pr_u_doe, val_pag_doe) VALUES( "
    sSql = sSql & cod_prog & ", "
    sSql = sSql & n_seq_prog & ", "
    sSql = sSql & n_ord & ", "
    sSql = sSql & BL_TrataStringParaBD(t_episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(t_doente) & ", "
    sSql = sSql & BL_TrataStringParaBD(doente) & ", "
    sSql = sSql & BL_TrataDataParaBD(dt_ini_real) & ", "
    sSql = sSql & cod_rubr & ", "
    sSql = sSql & cod_grupo & ", "
    sSql = sSql & cod_tip_rubr & ", "
    sSql = sSql & BL_TrataStringParaBD(nr_req_ars) & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_pagou_taxa) & ", "
    sSql = sSql & qtd & ", "
    sSql = sSql & cod_serv_exec & ", "
    sSql = sSql & flg_estado & ", "
    sSql = sSql & BL_TrataStringParaBD(chave_prog) & ", "
    sSql = sSql & BL_TrataStringParaBD(user_cri) & ", "
    If gSGBD = gOracle Then
        sSql = sSql & "SYSDATE, "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & "GETDATE(), "
    End If
    sSql = sSql & cod_isen_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_tip_req) & ", "
    sSql = sSql & n_ord_ins & ", "
    sSql = sSql & str_flg_estado_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(serie_fac_doe) & ", "
    sSql = sSql & str_n_fac_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_rubr_efr) & ", "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & val_pr_unit & ", "
    sSql = sSql & val_total & ", "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL) "
    BG_ExecutaQuery_ADO (sSql)
    
    IF_SetContext cod_empresa

    ' -----------------------------------------------------------------------------------------------
    ' INSERE NO FACTUS
    ' -----------------------------------------------------------------------------------------------
    sSql = "INSERT INTO fa_movi_fact ( cod_prog, n_seq_prog, n_ord, t_episodio, episodio, t_doente, doente, "
    sSql = sSql & " dt_ini_real,  cod_rubr, cod_grupo, cod_tip_rubr, nr_req_ars, flg_pagou_taxa, qtd, cod_serv_exec, "
    sSql = sSql & " flg_estado, chave_prog, user_cri, dt_cri, cod_isen_doe,empresa_id,flg_tip_req,n_ord_ins, "
    sSql = sSql & " flg_estado_doe, serie_fac_doe, n_fac_doe, cod_rubr_efr"
    sSql = sSql & ", perc_pag_efr, perc_pag_doe, val_pr_unit, val_total, val_pr_u_doe, val_pag_doe) VALUES( "
    sSql = sSql & cod_prog & ", "
    sSql = sSql & "SEQ_MOVI_FACT.NEXTVAL , "
    sSql = sSql & n_ord & ", "
    sSql = sSql & BL_TrataStringParaBD(t_episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(episodio) & ", "
    sSql = sSql & BL_TrataStringParaBD(t_doente) & ", "
    sSql = sSql & BL_TrataStringParaBD(doente) & ", "
    sSql = sSql & BL_TrataDataParaBD(dt_ini_real) & ", "
    sSql = sSql & cod_rubr & ", "
    sSql = sSql & cod_grupo & ", "
    sSql = sSql & cod_tip_rubr & ", "
    sSql = sSql & BL_TrataStringParaBD(nr_req_ars) & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_pagou_taxa) & ", "
    sSql = sSql & qtd & ", "
    sSql = sSql & cod_serv_exec & ", "
    sSql = sSql & flg_estado & ", "
    sSql = sSql & BL_TrataStringParaBD(Mid(chave_prog, 1, 15)) & ", "
    sSql = sSql & BL_TrataStringParaBD(user_cri) & ", "
    If gSGBD_SECUNDARIA = gOracle Then
        sSql = sSql & "SYSDATE, "
    ElseIf gSGBD_SECUNDARIA = gSqlServer Then
        sSql = sSql & "GETDATE(), "
    End If
    sSql = sSql & cod_isen_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_empresa) & ", "
    sSql = sSql & BL_TrataStringParaBD(flg_tip_req) & ", "
    sSql = sSql & n_ord_ins & ", "
    sSql = sSql & str_flg_estado_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(serie_fac_doe) & ", "
    sSql = sSql & str_n_fac_doe & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_rubr_efr) & ", "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & val_pr_unit & ", "
    sSql = sSql & val_total & ", "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL) "
    
    rv = BL_ExecutaQuery_Secundaria(sSql)
    If rv = -1 Then
        GoTo TrataErro
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_Factura_AnaliseExtra"
    Exit Sub
    Resume Next
End Sub




' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE TEM POR DEFEITO DOMICILIO

' -----------------------------------------------------------------------------------------------
Public Function IF_TemDomicilioDefeito(cod_efr As String) As Boolean
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    On Error GoTo TrataErro
    IF_TemDomicilioDefeito = True
    sSql = "SELECT FLG_DOM_DEFEITO FROM sl_efr where cod_efr = " & BL_TrataStringParaBD(cod_efr)
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRec.Open sSql, gConexao
    If rsRec.RecordCount > 0 Then
        If BL_HandleNull(rsRec!flg_dom_defeito, 0) = 1 Then
            IF_TemDomicilioDefeito = True
        Else
            IF_TemDomicilioDefeito = False
        End If
    End If
    rsRec.Close
    Set rsRec = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_TemDomicilioDefeito"
    IF_TemDomicilioDefeito = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE TEM POR DEFEITO DOMICILIO

' -----------------------------------------------------------------------------------------------
Public Function IF_EfrFacturaPercent(cod_efr As String) As Boolean
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    On Error GoTo TrataErro
    IF_EfrFacturaPercent = True
    sSql = "SELECT flg_perc_facturar FROM sl_efr where cod_efr = " & BL_TrataStringParaBD(cod_efr)
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRec.Open sSql, gConexao
    If rsRec.RecordCount > 0 Then
        If BL_HandleNull(rsRec!flg_perc_facturar, 0) = 1 Then
            IF_EfrFacturaPercent = True
        Else
            IF_EfrFacturaPercent = False
        End If
    End If
    rsRec.Close
    Set rsRec = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_EfrFacturaPercent"
    IF_EfrFacturaPercent = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE TEM POR DEFEITO DOMICILIO

' -----------------------------------------------------------------------------------------------
Public Function IF_RetornaPercentPagEFR(cod_efr As String, cod_rubrica As String, data As String) As Double
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    On Error GoTo TrataErro
    IF_RetornaPercentPagEFR = 100
    
    If gSGBD = gOracle Then
'        sSql = "SELECT perc_pag_ent FROM fa_pr_rubr where portaria = " & IF_RetornaPortariaActiva(cod_efr) & " and cod_rubr = " & cod_rubrica & " AND user_rem IS NULL "
'        rsRec.CursorLocation = adUseServer
'        rsRec.CursorType = adOpenStatic
'        If gModoDebug = mediSim Then BG_LogFile_erros sSql
'        rsRec.Open sSql, gConexaoSecundaria
'        If rsRec.RecordCount = 1 Then
'            IF_RetornaPercentPagEFR = 100 - CDbl(BL_HandleNull(rsRec!perc_pag_ent, 0))
'        End If
'        rsRec.Close
'        Set rsRec = Nothing
    ElseIf gSGBD = gSqlServer Then
        sSql = "SELECT perc_facturar FROM sl_ana_facturacao where cod_efr = " & cod_efr & " and cod_ana_gh = " & BL_TrataStringParaBD(cod_rubrica)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(data, Bg_DaData_ADO)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsRec.CursorLocation = adUseServer
        rsRec.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsRec.Open sSql, gConexao
        If rsRec.RecordCount = 1 Then
            IF_RetornaPercentPagEFR = 100 - CDbl(BL_HandleNull(rsRec!perc_facturar, 0))
        ElseIf rsRec.RecordCount = 0 Then
            rsRec.Close
            sSql = "SELECT perc_facturar FROM sl_ana_facturacao where cod_efr is null and cod_ana_gh = " & BL_TrataStringParaBD(cod_rubrica)
            sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(data, Bg_DaData_ADO)) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsRec.CursorLocation = adUseServer
            rsRec.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsRec.Open sSql, gConexao
            If rsRec.RecordCount = 1 Then
                IF_RetornaPercentPagEFR = 100 - CDbl(BL_HandleNull(rsRec!perc_facturar, 0))
            End If
        End If
        rsRec.Close
        Set rsRec = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaPercentPagEFR"
    IF_RetornaPercentPagEFR = 100
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE CONTROLA ISENTOS (NOS DOENTES ISENTOS ENVIA PRECOS PARA FACTUS (SAMS))

' -----------------------------------------------------------------------------------------------
Public Function IF_EfrControlaIsentos(cod_efr As String) As Boolean
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    On Error GoTo TrataErro
    IF_EfrControlaIsentos = False
    sSql = "SELECT FLG_CONTROLA_ISENCAO FROM sl_efr where cod_efr = " & BL_TrataStringParaBD(cod_efr)
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRec.Open sSql, gConexao
    If rsRec.RecordCount > 0 Then
        If BL_HandleNull(rsRec!flg_controla_isencao, 0) = 1 Then
            IF_EfrControlaIsentos = True
        Else
            IF_EfrControlaIsentos = False
        End If
    End If
    rsRec.Close
    Set rsRec = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_EfrControlaIsentos"
    IF_EfrControlaIsentos = False
    Exit Function
    Resume Next
End Function

Public Function IF_RetornaPortariaActiva(cod_efr As String) As String
    Dim sSql As String
    Dim rsEntidades As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & cod_efr
    rsEntidades.CursorLocation = adUseServer
    rsEntidades.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEntidades.Open sSql, gConexaoSecundaria
    If rsEntidades.RecordCount <= 0 Then
        ' NAO EXISTE ENTIDADE CODIFICADA
        IF_RetornaPortariaActiva = ""
    Else
        
        ' --------------------------------------------------------
        ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
        ' --------------------------------------------------------
        sSql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(Bg_DaData_ADO) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(Bg_DaData_ADO)
        sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(Bg_DaData_ADO) & "))"
        sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "
        rsPortarias.CursorLocation = adUseServer
        rsPortarias.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPortarias.Open sSql, gConexaoSecundaria
        If rsPortarias.RecordCount <= 0 Then
            IF_RetornaPortariaActiva = ""
        Else
            IF_RetornaPortariaActiva = BL_HandleNull(rsPortarias!Portaria, "")
        End If
    End If

Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaPortariaActiva"
    IF_RetornaPortariaActiva = ""
    Exit Function
    Resume Next
End Function

Public Function IF_RetornaPortariaData(cod_efr As String, data As String) As String
    Dim sSql As String
    Dim rsEntidades As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    On Error GoTo TrataErro
    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & cod_efr
    rsEntidades.CursorLocation = adUseServer
    rsEntidades.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEntidades.Open sSql, gConexaoSecundaria
    If rsEntidades.RecordCount <= 0 Then
        ' NAO EXISTE ENTIDADE CODIFICADA
        IF_RetornaPortariaData = ""
    Else
        
        ' --------------------------------------------------------
        ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
        ' --------------------------------------------------------
        sSql = "SELECT portaria,dt_ini FROM fa_portarias WHERE  dt_fim is null AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "
        rsPortarias.CursorLocation = adUseServer
        rsPortarias.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPortarias.Open sSql, gConexaoSecundaria
        If rsPortarias.RecordCount <= 0 Then
            IF_RetornaPortariaData = ""
        Else
            IF_RetornaPortariaData = BL_HandleNull(rsPortarias!Portaria, "")
        End If
    End If

Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaPortariaData"
    IF_RetornaPortariaData = ""
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE INIBE FACTURACAO DE NAO URBANOS

' -----------------------------------------------------------------------------------------------
Public Function IF_InibeNaoUrbano(cod_efr As String) As Boolean
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    On Error GoTo TrataErro
    IF_InibeNaoUrbano = False
    sSql = "SELECT flg_nao_urbano FROM sl_efr where cod_efr = " & BL_TrataStringParaBD(cod_efr)
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRec.Open sSql, gConexao
    If rsRec.RecordCount > 0 Then
        If BL_HandleNull(rsRec!flg_nao_urbano, 1) = 0 Then
            IF_InibeNaoUrbano = True
        Else
            IF_InibeNaoUrbano = False
        End If
    End If
    rsRec.Close
    Set rsRec = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_InibeNaoUrbano"
    IF_InibeNaoUrbano = False
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' RETORNA TAXA PAGA ENTIDADE DE UMA RUBRICA PARA UMA ENTIDADE(FACTUS)

' -----------------------------------------------------------------------------------------------

Public Function IF_RetornaTaxaRubricaEntidade(codRubrica As String, Entidade As String, p_data As String, t_utente As String, _
                                              Utente As String, ByRef nr_c As String, ByRef cod_rubr_efr As String) As String
    Dim sSql As String
    Dim iBD_Aberta As Long
    Dim rsPortarias As New ADODB.recordset
    Dim rsTaxas As New ADODB.recordset
    Dim rsEntidades As New ADODB.recordset
    
    ' --------------------------------------
    ' PARAMETROS PARA PROCEDIMENTO FACTUS
    ' --------------------------------------
    Dim p_cod_rubr As String
    Dim p_valor_ent As String
    Dim p_valor_adic As String
    Dim p_valor_doe As String
    Dim p_valor_taxa As String
    Dim p_perc_desc As String
    Dim p_valor_total As String
    Dim p_cod_rubr_efr As String
    Dim p_nr_c As String
    Dim p_descr_rubr_efr As String
    Dim p_alt_prec As String
    Dim p_w_msg As String
    
    Dim ResFACTUS As Integer
    
    On Error GoTo TrataErro
    ' ------------------------------------------------------------------------------------------
    ' ABRE CONEXAO COM FACTUS
    ' ------------------------------------------------------------------------------------------
    iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
    If iBD_Aberta = 0 Then
        ' NAO EXISTE CONEXAO FACTUS
        IF_RetornaTaxaRubricaEntidade = "-2"
    End If

    
        ' ------------------------------------------------------------------------------------------
        ' VERIFICA QUAL A ENTIDADE ACTIVA
        ' ------------------------------------------------------------------------------------------
        sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & Entidade
        rsEntidades.CursorLocation = adUseServer
        rsEntidades.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsEntidades.Open sSql, gConexaoSecundaria
        If rsEntidades.RecordCount <= 0 Then
            ' NAO EXISTE ENTIDADE CODIFICADA
            IF_RetornaTaxaRubricaEntidade = "-5"
        Else
            
            
            ' ------------------------------------------------------------------------------------------
            ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
            ' ------------------------------------------------------------------------------------------
            sSql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
            sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
            sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "
            rsPortarias.CursorLocation = adUseServer
            rsPortarias.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsPortarias.Open sSql, gConexaoSecundaria
            If rsPortarias.RecordCount <= 0 Then
                ' NAO EXISTE PORTARIA ACTIVA
                IF_RetornaTaxaRubricaEntidade = "-3"
            Else
                ' ------------------------------------------------------------------------------------------
                ' RETORNA A TAXA PARA A ANALISE EM CAUSA
                ' ------------------------------------------------------------------------------------------
                sSql = "SELECT val_pag_ent,nr_c, cod_rubr_efr FROM fa_pr_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(BL_HandleNull(codRubrica, ""))
                sSql = sSql & " AND portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria) & " AND user_rem IS NULL "
                rsTaxas.CursorLocation = adUseServer
                rsTaxas.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsTaxas.Open sSql, gConexaoSecundaria
                If rsTaxas.RecordCount <= 0 Then
                    ' NAO EXISTE TAXA CODIFICADA
                    IF_RetornaTaxaRubricaEntidade = "-4"
                    nr_c = ""
                Else
                    IF_RetornaTaxaRubricaEntidade = CStr(BL_HandleNull(rsTaxas!val_pag_ent, 0))
                    nr_c = CStr(BL_HandleNull(rsTaxas!nr_c, ""))
                    cod_rubr_efr = CStr(BL_HandleNull(rsTaxas!cod_rubr_efr, ""))
                End If
                rsTaxas.Close
                Set rsTaxas = Nothing
            End If
            rsPortarias.Close
            Set rsPortarias = Nothing
        End If
        rsEntidades.Close
        Set rsEntidades = Nothing
        Exit Function
    
    IF_RetornaTaxaRubricaEntidade = Replace(IF_RetornaTaxaRubricaEntidade, ".", ",")
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaTaxaRubricaEntidade"
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' RETORNA DATA A FACTURAR.

' -----------------------------------------------------------------------------------------------
Public Function IF_RetornaDataPrecario(cod_efr As String, data As String) As String
    Dim sSql As String
    Dim rsEntidades As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    On Error GoTo TrataErro


    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & cod_efr
    rsEntidades.CursorLocation = adUseServer
    rsEntidades.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEntidades.Open sSql, gConexaoSecundaria
    If rsEntidades.RecordCount <= 0 Then
        ' NAO EXISTE ENTIDADE CODIFICADA
        IF_RetornaDataPrecario = data
    Else
        
        ' --------------------------------------------------------
        ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
        ' --------------------------------------------------------
        sSql = "SELECT portaria, dt_ini FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(data)
        sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(data) & "))"
        sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "
        rsPortarias.CursorLocation = adUseServer
        rsPortarias.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPortarias.Open sSql, gConexaoSecundaria
        If rsPortarias.RecordCount > 0 Then
            If CDate(data) < CDate(BL_HandleNull(rsPortarias!dt_ini, "")) Then
                IF_RetornaDataPrecario = BL_HandleNull(rsPortarias!dt_ini, "")
            Else
                IF_RetornaDataPrecario = data
            End If
        Else
                IF_RetornaDataPrecario = data
        End If
    End If
    rsPortarias.Close
    rsEntidades.Close
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaDataPrecario"
    IF_RetornaDataPrecario = data
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' RETORNA VALOR DO C

' -----------------------------------------------------------------------------------------------
Public Function IF_RetornaValorC(Portaria As String) As String
    Dim sSql As String
    Dim rsValorC As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT valor_c  FROM fa_pr_grup_rubr  where portaria = " & Portaria & " AND cod_grupo in (" & gGrupoRubricaFactus & ")"
    rsValorC.CursorLocation = adUseServer
    rsValorC.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsValorC.Open sSql, gConexaoSecundaria
    If rsValorC.RecordCount <= 0 Then
        IF_RetornaValorC = "0"
    Else
        IF_RetornaValorC = BL_HandleNull(rsValorC!valor_c, "0")
    End If
    rsValorC.Close
    Set rsValorC = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaValorC"
    IF_RetornaValorC = "0"
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' VERIFICA SE REQUISI��O J� EST� FACTURADA, NAO FACTURADA, ETC.

' -----------------------------------------------------------------------------------------------
Public Function IF_VerificaEstadoFactReq(n_req As String) As Integer
    ' -1 - ERRO
    ' 0 - N�o facturado
    ' 1 - facturado mas SL_REQUIS incoerente
    ' 2 - N�o facturado mas SL_REQUIS incoerente
    ' 3 - Facturado.
    ' 4  - com linha na fa_movi_resp mas nao na fa_movi_Fact
    ' 5 - rejeitada FACTUS mas nao no SISLAB
    ' 6 - rejeitada no FACTUS e SISLAB
    
    Dim sSql As String
    Dim rsSISLAB As New ADODB.recordset
    Dim rsFACTUS As New ADODB.recordset
    On Error GoTo TrataErro
    IF_VerificaEstadoFactReq = 0
    
    ' VERIFICA SE EXISTE NA TABELA FA_MOVI_RESP
    sSql = "SELECT *  FROM fa_movi_Resp WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(n_req)
    rsFACTUS.CursorLocation = adUseServer
    rsFACTUS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFACTUS.Open sSql, gConexaoSecundaria
    If rsFACTUS.RecordCount > 0 Then
        IF_VerificaEstadoFactReq = 1
        rsFACTUS.Close
        
        'VERIFICA SE EXISTEM LINHAS NA FA_MOVI_FACT
        sSql = "SELECT *  FROM fa_movi_fact WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(n_req)
        rsFACTUS.CursorLocation = adUseServer
        rsFACTUS.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsFACTUS.Open sSql, gConexaoSecundaria
        If rsFACTUS.RecordCount <= 0 Then
            rsFACTUS.Close
            
            'VERIFICA SE ESTA NA TABELA DE DADOS REJEITADOS
            sSql = "SELECT * FROM Fa_movi_fact_rej WHERE cod_prog = " & gCodProg & " AND n_seq_prog IN ( "
            sSql = sSql & " SELECT n_seq_prog FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(n_req) & ")"
            rsFACTUS.CursorLocation = adUseServer
            rsFACTUS.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsFACTUS.Open sSql, gConexaoSecundaria
            If rsFACTUS.RecordCount <= 0 Then
                ' INCOERENCIA
                IF_VerificaEstadoFactReq = 4
            Else
                'REJEITADO
                IF_VerificaEstadoFactReq = 5
                
            End If
        End If
    End If
    rsFACTUS.Close


    ' SISLAB
    sSql = "SELECT flg_facturado FROM sl_requis WHERE n_req = " & BL_TrataStringParaBD(n_req)
    rsSISLAB.CursorLocation = adUseServer
    rsSISLAB.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSISLAB.Open sSql, gConexao
    If rsSISLAB.RecordCount > 0 Then
    
        'FACTURADA
        If BL_HandleNull(rsSISLAB!Flg_Facturado, 0) = 1 Then
            If IF_VerificaEstadoFactReq = 1 Then
                IF_VerificaEstadoFactReq = 3
            ElseIf IF_VerificaEstadoFactReq = 0 Then
                IF_VerificaEstadoFactReq = 2
            End If
            
        'NAO FACTURADA
        ElseIf BL_HandleNull(rsSISLAB!Flg_Facturado, 0) = 0 Then
            If IF_VerificaEstadoFactReq = 1 Then
                IF_VerificaEstadoFactReq = 1
            ElseIf IF_VerificaEstadoFactReq = 0 Then
                IF_VerificaEstadoFactReq = 0
            End If
            
        'REJEITADA
        ElseIf BL_HandleNull(rsSISLAB!Flg_Facturado, 0) = 2 Then
            If IF_VerificaEstadoFactReq = 5 Or IF_VerificaEstadoFactReq = 4 Then
                IF_VerificaEstadoFactReq = 6
            Else
                IF_VerificaEstadoFactReq = 7
            End If
        End If
    End If
    rsSISLAB.Close
    
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_VerificaEstadoFactReq", True
    IF_VerificaEstadoFactReq = -1
    Exit Function
    Resume Next
End Function


Public Function IF_RetornaDataReqPortaria(Portaria As String, data As String) As String
    Dim sSql As String
    Dim rsEntidades As New ADODB.recordset
    On Error GoTo TrataErro
    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    sSql = "SELECT dt_ini, dt_fim FROM fa_portarias WHERE portaria = " & Portaria
    rsEntidades.CursorLocation = adUseServer
    rsEntidades.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEntidades.Open sSql, gConexaoSecundaria
    If rsEntidades.RecordCount <= 0 Then
        IF_RetornaDataReqPortaria = data
    Else
        If CDate(BL_HandleNull(rsEntidades!dt_ini, Bg_DaData_ADO)) <= CDate(data) And BL_HandleNull(rsEntidades!dt_fim, "") = "" Then
            IF_RetornaDataReqPortaria = data
        ElseIf CDate(BL_HandleNull(rsEntidades!dt_ini, Bg_DaData_ADO)) <= CDate(data) And CDate(BL_HandleNull(rsEntidades!dt_fim, Bg_DaData_ADO)) > CDate(data) And (BL_HandleNull(rsEntidades!dt_fim, "")) <> "" Then
            IF_RetornaDataReqPortaria = data
        ElseIf BL_HandleNull(rsEntidades!dt_fim, "") <> "" Then
            IF_RetornaDataReqPortaria = rsEntidades!dt_fim
        ElseIf CDate(BL_HandleNull(rsEntidades!dt_ini, Bg_DaData_ADO)) > CDate(data) And CDate(BL_HandleNull(rsEntidades!dt_fim, Bg_DaData_ADO)) > CDate(data) Then
            IF_RetornaDataReqPortaria = rsEntidades!dt_ini
        Else
            IF_RetornaDataReqPortaria = data
        End If
    End If
    rsEntidades.Close
    Set rsEntidades = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao calcular data Portaria:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaDataReqPortaria", True
    IF_RetornaDataReqPortaria = data
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' GERA HASH E GUARDA DO LADO DO SISLAB STRING PARA IMPRIMIR NO RECIBO

' -----------------------------------------------------------------------------------------------
Public Function IF_GeraCertificado(dt_emissao As String, cod_empresa As String, serie_rec As String, n_rec As String, _
                                   valor As String, ByRef idCert As Long) As Boolean
    On Error GoTo TrataErro
    Dim CmdCertificacao As New ADODB.Command
    Dim PmtID As ADODB.Parameter
    Dim PmtTipoDoc As ADODB.Parameter
    Dim PmtDtEmissao As ADODB.Parameter
    Dim PmtCodEmpresa As ADODB.Parameter
    Dim PmtSerieRec As ADODB.Parameter
    Dim PmtNRec As ADODB.Parameter
    Dim PmtValor As ADODB.Parameter
    Dim resultado As Long
    
    On Error GoTo TrataErro
    
    If IF_VerificaCertificacao(dt_emissao, cod_empresa) = False Then
        IF_GeraCertificado = True
        Exit Function
    End If
    gSQLError = 0
    gSQLISAM = 0
    
    If gSGBD = gOracle Then
        With CmdCertificacao
            .ActiveConnection = gConexao
            .CommandText = "PCK_CERTIFICACAO.GERA_CERTIFICADO"
            .CommandType = adCmdStoredProc
        End With
    Else
        With CmdCertificacao
            .ActiveConnection = gConexaoSecundaria
            .CommandText = "PCK_CERTIFICACAO2.GERA_CERTIFICADO"
            .CommandType = adCmdStoredProc
        End With
    End If
    
    Set PmtID = CmdCertificacao.CreateParameter("P_ID", adNumeric, adParamOutput, 0)
    PmtID.Precision = 12
    Set PmtTipoDoc = CmdCertificacao.CreateParameter("V_TIPO_DOC", adVarChar, adParamInput, 30)
    Set PmtDtEmissao = CmdCertificacao.CreateParameter("V_DT_EMISSAO", adDate, adParamInput, 0)
    Set PmtCodEmpresa = CmdCertificacao.CreateParameter("V_COD_EMPRESA", adVarChar, adParamInput, 30)
    Set PmtSerieRec = CmdCertificacao.CreateParameter("V_SERIE_REC", adVarChar, adParamInput, 30)
    Set PmtNRec = CmdCertificacao.CreateParameter("V_N_REC", adNumeric, adParamInput, 0)
    Set PmtValor = CmdCertificacao.CreateParameter("V_VALOR", adDouble, adParamInput, 0)
    
    CmdCertificacao.Parameters.Append PmtTipoDoc
    CmdCertificacao.Parameters.Append PmtDtEmissao
    CmdCertificacao.Parameters.Append PmtCodEmpresa
    CmdCertificacao.Parameters.Append PmtSerieRec
    CmdCertificacao.Parameters.Append PmtNRec
    CmdCertificacao.Parameters.Append PmtValor
    CmdCertificacao.Parameters.Append PmtID
    
    CmdCertificacao.Parameters("V_TIPO_DOC") = "SISLAB_FACREC"
    CmdCertificacao.Parameters("V_DT_EMISSAO") = dt_emissao
    CmdCertificacao.Parameters("V_COD_EMPRESA") = cod_empresa
    CmdCertificacao.Parameters("V_SERIE_REC") = serie_rec
    CmdCertificacao.Parameters("V_N_REC") = n_rec
    CmdCertificacao.Parameters("V_VALOR") = CDbl(Replace(valor, ".", ","))
    CmdCertificacao.Execute
    
    resultado = Trim(CmdCertificacao.Parameters.item("P_ID").value)
    idCert = resultado
    If resultado > 0 Then
        IF_GeraCertificado = True
    Else
        IF_GeraCertificado = False
    End If
    
    Set CmdCertificacao = Nothing
    Set PmtDtEmissao = Nothing
    Set PmtCodEmpresa = Nothing
    Set PmtSerieRec = Nothing
    Set PmtNRec = Nothing
    Set PmtValor = Nothing
    Set PmtID = Nothing


Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao gerar certificado:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_GeraCertificado", True
    IF_GeraCertificado = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' insere os dados do certificado do lado do sislab

' -----------------------------------------------------------------------------------------------
Public Function IF_DevolveCertificado(ByVal idCert As Long) As Boolean
    On Error GoTo TrataErro
    Dim CmdCertificacao As New ADODB.Command
    Dim PmtID As ADODB.Parameter
    Dim PmtRET As ADODB.Parameter
    Dim resultado As Long
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    If gSGBD = gOracle Then
        With CmdCertificacao
            .ActiveConnection = gConexao
            .CommandText = "PCK_CERTIFICACAO.DEVOLVE_CERTIFICADO"
            .CommandType = adCmdStoredProc
        End With
    Else
        With CmdCertificacao
            .ActiveConnection = gConexaoSecundaria
            .CommandText = "PCK_CERTIFICACAO2.DEVOLVE_CERTIFICADO"
            .CommandType = adCmdStoredProc
        End With
    End If
    
    Set PmtRET = CmdCertificacao.CreateParameter("RET", adNumeric, adParamOutput, 0)
    PmtRET.Precision = 12
    Set PmtID = CmdCertificacao.CreateParameter("V_ID", adNumeric, adParamInput, 0)
    
    CmdCertificacao.Parameters.Append PmtID
    CmdCertificacao.Parameters.Append PmtRET
    
    CmdCertificacao.Parameters("V_ID") = idCert
    CmdCertificacao.Execute
    
    resultado = Trim(CmdCertificacao.Parameters.item("RET").value)
    If resultado = 1 Then
        IF_DevolveCertificado = True
    Else
        IF_DevolveCertificado = False
    End If
    
    Set CmdCertificacao = Nothing
    Set PmtRET = Nothing
    Set PmtID = Nothing


Exit Function
TrataErro:
    BG_LogFile_Erros "ERro ao devolver certificado:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_devolveCertificado", True
    IF_DevolveCertificado = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE CLIENTE TEM PARAMETRIZADO PARA CERTIFICAR DOCUMENTOS

' -----------------------------------------------------------------------------------------------
Private Function IF_VerificaCertificacao(data As String, cod_empresa As String) As Boolean
    On Error GoTo TrataErro
    Dim CmdCertificacao As New ADODB.Command
    Dim PmtResultado As ADODB.Parameter
    Dim PmtData As ADODB.Parameter
    Dim PmtEmpresa As ADODB.Parameter
    Dim resultado As String
    Dim idBD_Aberta As Integer
    Dim iBD_Aberta As Integer
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    If gSGBD = gSqlServer Or gSGBD = gPostGres Then
        ' -----------------------
        ' ABRE CONEXAO COM FACTUS
        ' -----------------------
        iBD_Aberta = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
        If iBD_Aberta = 0 Then
            IF_VerificaCertificacao = False
            Exit Function
        End If
    End If
    
    With CmdCertificacao
        If gSGBD = gOracle Then
            .ActiveConnection = gConexao
            .CommandText = "PCK_CERTIFICACAO.VERIFICACERTIFICACAO"
        ElseIf gSGBD = gSqlServer Or gSGBD = gPostGres Then
            .ActiveConnection = gConnHIS
            .CommandText = "PCK_CERTIFICACAO2.VERIFICACERTIFICACAO"
        End If
        
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtResultado = CmdCertificacao.CreateParameter("R_RESULTADO", adVarChar, adParamOutput, 5)
    PmtResultado.Precision = 12
    Set PmtData = CmdCertificacao.CreateParameter("V_DT_EMISSAO", adDate, adParamInput, 0)
    Set PmtEmpresa = CmdCertificacao.CreateParameter("V_COD_EMPRESA", adVarChar, adParamInput, 30)
    CmdCertificacao.Parameters.Append PmtData
    CmdCertificacao.Parameters.Append PmtEmpresa
    CmdCertificacao.Parameters.Append PmtResultado
    
    CmdCertificacao.Parameters("V_DT_EMISSAO") = data
    CmdCertificacao.Parameters("V_COD_EMPRESA") = cod_empresa
    CmdCertificacao.Execute
    
    resultado = Trim(CmdCertificacao.Parameters.item("R_RESULTADO").value)
    If resultado = "S" Then
        IF_VerificaCertificacao = True
    Else
        IF_VerificaCertificacao = False
    End If
    
    Set CmdCertificacao = Nothing
    Set PmtData = Nothing
    Set PmtEmpresa = Nothing
    Set PmtResultado = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao verificar certificacao:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_VerificaCertificacao", False
    IF_VerificaCertificacao = False
    Exit Function
    Resume Next
End Function



' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE USA SUBPRECARIO

' -----------------------------------------------------------------------------------------------
Public Function IF_VerificaUsaSubPrecario(cod_efr As String, data As String) As String
    Dim sSql As String
    Dim rsSubP As New ADODB.recordset
    Dim iBD_Aberta As Integer
    On Error GoTo TrataErro
    
    
    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    sSql = "SELECT * fROM FA_SUB_PREC WHERE COD_EFR =" & cod_efr
    sSql = sSql & " AND DT_INI <= " & BL_TrataDataParaBD(data) & " and (dt_fim is null or dt_fim > "
    sSql = sSql & BL_TrataDataParaBD(data) & ") and user_rem is null AND cod_grup_rubr in (" & gGrupoRubricaFactus & ")"
    rsSubP.CursorLocation = adUseServer
    rsSubP.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSubP.Open sSql, gConexaoSecundaria
    If rsSubP.RecordCount <= 0 Then
        IF_VerificaUsaSubPrecario = ""
    Else
        IF_VerificaUsaSubPrecario = BL_HandleNull(rsSubP!cod_sub_prec, "")
    End If
    rsSubP.Close
    Set rsSubP = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao verificar SubPrecario:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_VerificaUsaSubPrecario", False
    IF_VerificaUsaSubPrecario = ""
    Exit Function
    Resume Next
End Function



' -----------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE USA SUBPRECARIO

' -----------------------------------------------------------------------------------------------
Public Function IF_RetornaEntidadeRef(ByVal cod_sub_prec As String, ByVal cod_efr_origem As String, _
                                       ByRef perc_desc As Double, ByRef perc_pag_doe As Double) As String
    Dim sSql As String
    Dim rsSubP As New ADODB.recordset
    On Error GoTo TrataErro
    ' -------------------------------
    ' VERIFICA QUAL A ENTIDADE ACTIVA
    ' -------------------------------
    sSql = "SELECT * fROM FA_SUB_PREC WHERE cod_sub_prec = " & cod_sub_prec & " AND COD_EFR =" & cod_efr_origem
    rsSubP.CursorLocation = adUseServer
    rsSubP.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSubP.Open sSql, gConexaoSecundaria
    If rsSubP.RecordCount <= 0 Then
        IF_RetornaEntidadeRef = ""
    Else
        perc_desc = BL_HandleNull(rsSubP!perc_desc, 0)
        perc_pag_doe = BL_HandleNull(rsSubP!perc_pag_doe, 100)
        IF_RetornaEntidadeRef = BL_HandleNull(rsSubP!tab_utilizada, "")
    End If
    rsSubP.Close
    Set rsSubP = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao verificar SubPrecario:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaEntidadeRef", False
    IF_RetornaEntidadeRef = ""
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' DADO O VALOR VERIFICA QUAL O ERRO

' -----------------------------------------------------------------------------------------------
Public Function IF_DevolveErroFACTUS(ByRef taxa As String) As String
    Select Case taxa
        Case "-1"
            IF_DevolveErroFACTUS = " An�lise n�o est� mapeada para o FACTUS!"
            taxa = 0
        Case "-2"
            IF_DevolveErroFACTUS = "Impossivel abrir conex�o com o FACTUS!"
            taxa = 0
        Case "-3"
            IF_DevolveErroFACTUS = "Impossivel seleccionar portaria activa!"
            taxa = 0
        Case "-4"
            IF_DevolveErroFACTUS = "An�lise n�o comparticipada. Pre�o PARTICULAR!"
            taxa = 0
        Case "-5"
            IF_DevolveErroFACTUS = "N�o existe tabela codificada para a entidade em causa!"
            taxa = 0
        Case "-9"
            IF_DevolveErroFACTUS = "Erro ao calcular o pre�o para Sub-pre��rio!"
            taxa = 0
        Case Else
            IF_DevolveErroFACTUS = ""
    End Select
End Function

' -----------------------------------------------------------------------------------------------

' ENVIA PARA FACTUS AUTOMATICAMENTE

' -----------------------------------------------------------------------------------------------
Public Function IF_EnviaReqFACTUS(n_req As String) As Integer
    Dim iReq As Integer
    Dim iEfr As Integer
    Dim iCred As Integer
    Dim iAna As Integer
    Dim flg_pagouTaxa As String
    Dim cod_isen_doe As Integer
    Dim flg_estado_doe As Integer
    Dim serie_fac_doe As String
    Dim n_Fac_doe As Long
    Dim flg_tip_req As String
    Dim codDomicilio As Integer
    Dim flg_insere_cod_rubr_efr As Boolean
    Dim sSql As String
    On Error GoTo TrataErro
    IF_EnviaReqFACTUS = -1
    If IF_PreencheDadosReq(n_req) = False Then
        IF_EnviaReqFACTUS = 1
        Exit Function
    End If
    For iReq = 1 To totalReq
        For iEfr = 1 To estrutRequis(iReq).totalEntidades
            If estrutRequis(iReq).entidades(iEfr).cod_efr <> gEfrParticular Then
                For iCred = 1 To estrutRequis(iReq).entidades(iEfr).totalCred
                    ' ------------------------------------------------------------------------------------
                    ' ISENCAO
                    ' -------------------------------------------------------------------------------------
                    If estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoIsento Or estrutRequis(iReq).hemodialise <> -1 Then
                        flg_pagouTaxa = "N"
                        cod_isen_doe = "1"
                    ElseIf estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoBorla Or estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoNaoIsento Then
                        flg_pagouTaxa = "S"
                        cod_isen_doe = "0"
                    End If
                    
                    
                        
                        
                    ' ------------------------------------------------------------------------------------
                    ' FACTURA ANALISES
                    ' -------------------------------------------------------------------------------------
                    For iAna = 1 To estrutRequis(iReq).entidades(iEfr).cRed(iCred).totalAna
                        If (estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Facturada = gEstadoFactRequisNaoFacturado Or estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Facturada = gEstadoFactRequisRejeitado) _
                            And estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Retirada = False Then
                                        
                            ' ------------------------------------------------------------------------------------------------------------------
                            'APENAS ENVIA REQUISICOES QUE NAO SEJA OBRIGATORIO RECIBO, OU ENTAO QUE SEJA OBRIGATORIO E TENHA NUMERO RECIBO
                            ' ------------------------------------------------------------------------------------------------------------------
                            If estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_ObrigaRecibo <> True Or _
                                    (estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec <> "0" And estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec <> "") _
                                    Or estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).estadoRec = gEstadoReciboNulo Then
                                    
                                ' ------------------------------------------------------------------------------------
                                ' DADOS DOS RECIBOS
                                ' -------------------------------------------------------------------------------------
                                If estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec <> "0" And estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec <> "" Then
                                    flg_estado_doe = 3
                                    n_Fac_doe = estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec
                                    serie_fac_doe = Right(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).serieRec, 9)
                                Else
                                    flg_estado_doe = -1
                                    n_Fac_doe = -1
                                    serie_fac_doe = ""
                                End If
                                
                                ' ------------------------------------------------------------------------------------
                                ' ISENCAO
                                ' -------------------------------------------------------------------------------------
                                If estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoIsento Or estrutRequis(iReq).hemodialise <> -1 Then
                                    flg_pagouTaxa = "N"
                                    cod_isen_doe = "1"
                                ElseIf estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoBorla Or estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoNaoIsento Then
                                    flg_pagouTaxa = "S"
                                    cod_isen_doe = "0"
                                End If
                                
                                ' ------------------------------------------------------------------------------------
                                ' FLG_TIP_REQ - TIPO DE DOMICILIO(N,D,I,C) CONCATENADO COM TIPO DE P1(B,V)
                                ' HA ENTIDADES EM QUE DOMICILIO NAO E COMPARTICIPADO PELO QUE NAO PODE IR PARA FACTUS
                                ' -------------------------------------------------------------------------------------
                                If estrutRequis(iReq).entidades(iEfr).flg_compart_dom = "1" Then
                                    ' VERIFICA SE PARA EFR CAUSA SE ENVIA SEMPRE DOMICILIO
                                    If estrutRequis(iReq).entidades(iEfr).flg_dom_defeito <> "1" Then
                                        If estrutRequis(iReq).codUrbano <> 0 Then
                                            'Verifica se EFR faz separa��o de lotes por domicilio
                                            If estrutRequis(iReq).entidades(iEfr).flg_separa_dom = "1" Then
                                                flg_tip_req = "D" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                            ' VERIFICA SE ESTAO SOB REGRA DOS 65 ANOS
                                            ElseIf estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoBorla Or estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoNaoIsento And _
                                                   IF_VerificaRegraArsData(CInt(estrutRequis(iReq).entidades(iEfr).flg_65anos), estrutRequis(iReq).dt_nasc_ute, estrutRequis(iReq).dt_chega) = True Then
                                                flg_tip_req = "C" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                            Else
                                                flg_tip_req = "N" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                            End If
                                            codDomicilio = 0
                                            codDomicilio = estrutRequis(iReq).entidades(iEfr).codDom
                                            
                                            ' VERIFICA SE ENTIDADE PODE FACTURAR NAO URBANOS
                                            If estrutRequis(iReq).entidades(iEfr).flg_compart_dom = "0" Then
                                                codDomicilio = lCodUrbano
                                            End If
                                            If codDomicilio = lCodUrbano Then
                                                estrutRequis(iReq).km = 1
                                            ElseIf codDomicilio = lCodUrbanoBiDiario Then
                                                estrutRequis(iReq).km = 2
                                            End If
                                        ElseIf estrutRequis(iReq).convencao = lCodInternacional Then
                                            flg_tip_req = "I" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                            codDomicilio = 0
                                            estrutRequis(iReq).km = 0
                                        ' VERIFICA SE ESTAO SOB REGRA DOS 65 ANOS
                                        ElseIf estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoBorla Or estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoNaoIsento And _
                                               IF_VerificaRegraArsData(CInt(estrutRequis(iReq).entidades(iEfr).flg_65anos), estrutRequis(iReq).dt_nasc_ute, estrutRequis(iReq).dt_chega) = True Then
                                            flg_tip_req = "C" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                            
                                        ElseIf estrutRequis(iReq).convencao = lCodNormal Then
                                            flg_tip_req = "N" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                            codDomicilio = 0
                                            estrutRequis(iReq).km = 0
                                        Else
                                            codDomicilio = 0
                                            estrutRequis(iReq).km = 0
                                        End If
                                    Else
                                        flg_tip_req = "D" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                        codDomicilio = lCodUrbano
                                        estrutRequis(iReq).km = 1
                                    End If
                                Else
                                    flg_tip_req = "N" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor
                                    codDomicilio = 0
                                    estrutRequis(iReq).km = 0
                                    
                                End If
                                
                                ' ------------------------------------------------------------------------------------
                                ' FIM SEMANA - SE NAO ESTIVER COMO FDS MAS DIA FOR DE SEMANA ENVIA ULTIMO DOMINGO
                                ' MARTELADA E DAS GRANDES....
                                ' -------------------------------------------------------------------------------------
                                If estrutRequis(iReq).fimSemana = 1 Then
                                  If Weekday(estrutRequis(iReq).dt_chega) <> 1 Then
                                    Dim k As Integer
                                    For k = 1 To 7
                                        If Weekday(CDate(estrutRequis(iReq).dt_chega) - CDate(k)) = 1 Then
                                            estrutRequis(iReq).dt_chega = CDate(CDate(estrutRequis(iReq).dt_chega) - CDate(k))
                                            Exit For
                                        End If
                                    Next
                                  End If
                                End If
                                
                                
                                'soliveira correccao lacto
                                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).ordem = iAna
                                
                                
                                If estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).CodAnaGH <> "" Then
                                
                                    ' VERIFICA SE ENVIA PARA FACTUS PERCENTAGEM DESCONTO DO UTENTE
                                    If estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescEnvio = "" Then
                                        estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescEnvio = "0"
                                    End If
                                    
                                    'VERIFICA SE INSERE OU NAO O CODIGO DA RUBRICA DA ENTIDADE
                                    If estrutRequis(iReq).entidades(iEfr).flg_insere_cod_rubr_efr = "1" Then
                                        flg_insere_cod_rubr_efr = True
                                    Else
                                        flg_insere_cod_rubr_efr = False
                                    End If
                                    
                                    ' ------------------------------------------------------------------------------------------------------------------
                                    ' N�O ENVIA AN�LISES DE FIM DE SEMANA EM QUE ENTIDADES TEM VALOR FIXO
                                    ' ------------------------------------------------------------------------------------------------------------------
                                    If estrutRequis(iReq).fimSemana = 0 Or (estrutRequis(iReq).fimSemana = 1 And estrutRequis(iReq).entidades(iEfr).flg_FdsValFixo = 0) Then
                                        IF_Factura_AnalisePrivado gCodProg, -1, estrutRequis(iReq).entidades(iEfr).n_ord, "SISLAB", estrutRequis(iReq).NReq, estrutRequis(iReq).TipoUtente, estrutRequis(iReq).Utente, estrutRequis(iReq).dt_chega, _
                                                                 estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).CodAnaGH, estrutRequis(iReq).entidades(iEfr).cRed(iCred).N_REQ_ARS, flg_pagouTaxa, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Qtd_Ana, _
                                                                 CStr(gCodServExec), 1, estrutRequis(iReq).entidades(iEfr).cRed(iCred).N_REQ_ARS, CStr(gCodUtilizador), _
                                                                 Bg_DaData_ADO, cod_isen_doe, estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor, flg_tip_req, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).ordem, _
                                                                 flg_estado_doe, serie_fac_doe, n_Fac_doe, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).cod_rubr_efr, CStr(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PrecoUnitario), _
                                                                 CDbl(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescEnvio), estrutRequis(iReq).entidades(iEfr).cod_empresa, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_controlaIsencao, _
                                                                 estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_percentagem, flg_insere_cod_rubr_efr
                                        
                                        estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Facturada = gEstadoFactRequisFacturado
                                        
                                    ElseIf estrutRequis(iReq).fimSemana = 1 And estrutRequis(iReq).entidades(iEfr).flg_FdsValFixo = 1 Then
                                        estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Facturada = gEstadoFactRequisNaoFacturado
                                        estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Retirada = True
                                    End If
                                End If
                                
                                sSql = "UPDATE sl_recibos_det SET "
                                sSql = sSql & " p1 = " & estrutRequis(iReq).entidades(iEfr).cRed(iCred).n_credencial
                                sSql = sSql & ", cor_p1 = " & BL_TrataStringParaBD(estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor)
                                sSql = sSql & ", flg_facturado =  " & estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Facturada
                                sSql = sSql & ", flg_retirado = " & IIf(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Retirada = True, 1, 0)
                                sSql = sSql & ", ordem_marcacao = " & iAna
                                sSql = sSql & " WHERE n_req = " & BL_TrataStringParaBD(estrutRequis(iReq).NReq) & " AND cod_facturavel = " & BL_TrataStringParaBD(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).codAna)
                                BG_ExecutaQuery_ADO sSql
                                
                                If gSQLError <> 0 Then
                                    IF_EnviaReqFACTUS = 2
                                    Exit Function
                                End If
                            End If
                        End If
                    Next
                    
                    ' ------------------------------------------------------------------------------------
                    ' SE FOR UMA NOVA ENTIDADE, ENTAO ENVIA CABE�ALHO PARA TABELA FA_MOVI_RESP
                    ' -------------------------------------------------------------------------------------
                    IF_Factura_RequisicaoPrivado CInt(estrutRequis(iReq).entidades(iEfr).n_ord), "SISLAB", estrutRequis(iReq).NReq, estrutRequis(iReq).entidades(iEfr).n_benef, estrutRequis(iReq).entidades(iEfr).cod_efr, _
                                                CStr(gCodUtilizador), CInt(BL_HandleNull(estrutRequis(iReq).entidades(iEfr).codDom, -1)), estrutRequis(iReq).km, estrutRequis(iReq).TipoUtente, _
                                                estrutRequis(iReq).Utente, estrutRequis(iReq).entidades(iEfr).cod_empresa
                    estrutRequis(iReq).estadoFact = gEstadoFactRequisFacturado
                Next
            End If
        Next
    Next
    
    
    IF_EnviaReqFACTUS = 0


Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao enviar requisi��o para FACTUS:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_EnviaReqFACTUS", False
    IF_EnviaReqFACTUS = -1
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------

' PREENCHE TODOS OS DADOS DA REQUISICAO E DO UTENTE EM CAUSA

' ---------------------------------------------------------------------------

Private Function IF_PreencheDadosReq(n_req As String) As Boolean
    
    On Error GoTo TrataErro
    
    Dim sSql As String
    Dim rsReq As ADODB.recordset
    Dim i As Integer
    Dim flg_criterio As Boolean
    Dim flg_existe As Boolean
    ' ---------------------------------------------------------------------------
    ' LIMPA ESTRUTURA DAS REQUISICOES A FACTURAR
    ' ---------------------------------------------------------------------------
    ReDim estrutRequis(0)
    totalReq = 0
    
    flg_criterio = False
    sSql = "SELECT DISTINCT x1.n_req,x1.n_req_assoc, x1.n_epis, x1.seq_utente, x1.cod_efr, x1.n_benef, x1.cod_med, x1.estado_req,  x1.cod_urbano,  x1.km,  x1.flg_facturado, "
    sSql = sSql & " x1.estado_req, x1.dt_chega, x1.user_cri, x1.hr_chega, x2.utente, x2.t_utente, x2.n_cartao_ute, x2.nome_ute, x1.cod_isencao, "
    sSql = sSql & " x1.convencao, x1.hemodialise, x1.fim_semana, x2.dt_nasc_ute, x1.cod_sala, x1.seq_utente_fact,x3.descr_efr, x3.flg_perc_facturar, "
    sSql = sSql & " x3.flg_controla_isencao, x3.flg_obriga_recibo, x3.deducao_ute, x3.FLG_ACTO_MED_EFR, x3.cod_empresa,x3.flg_compart_dom, "
    sSql = sSql & " x3.flg_nao_urbano, x3.flg_65anos, x2.dt_nasc_ute, x3.flg_separa_dom, x3.flg_dom_defeito, x3.flg_fds_fixo, x3.flg_insere_cod_rubr_efr, x2.sexo_ute "
    sSql = sSql & " FROM sl_requis x1, sl_identif x2, sl_efr x3 WHERE x1.seq_utente = x2.seq_utente  "
    sSql = sSql & " AND x1.cod_efr = x3.cod_efr  "
    sSql = sSql & " AND x1.n_req = " & n_req
    
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao
        
    If (rsReq.RecordCount <= 0) Then
        IF_PreencheDadosReq = False
        rsReq.Close
        Set rsReq = Nothing
        Exit Function
    Else
        While Not rsReq.EOF
            flg_existe = False
            For i = 1 To totalReq
                If estrutRequis(i).NReq = BL_HandleNull(rsReq!n_req, "") Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                totalReq = totalReq + 1
                ReDim Preserve estrutRequis(totalReq)
                i = totalReq
                ' ---------------------------------------------------------------------------
                ' PREENCHE ESTRUTURA COM DADOS DA REQUISICAO
                ' ---------------------------------------------------------------------------
                estrutRequis(totalReq).NReq = BL_HandleNull(rsReq!n_req, "")
                estrutRequis(totalReq).dt_chega = BL_HandleNull(rsReq!dt_chega, "")
                estrutRequis(totalReq).user_cri = BL_HandleNull(rsReq!user_cri, "")
                estrutRequis(totalReq).hr_chega = BL_HandleNull(rsReq!hr_chega, "")
                estrutRequis(totalReq).n_benef = BL_HandleNull(rsReq!n_benef, "")
                estrutRequis(totalReq).codIsencao = BL_HandleNull(rsReq!cod_isencao, "")
                estrutRequis(totalReq).estadoFact = BL_HandleNull(rsReq!Flg_Facturado, 0)
                estrutRequis(totalReq).codUrbano = BL_HandleNull(rsReq!cod_urbano, 0)
                estrutRequis(totalReq).km = BL_HandleNull(rsReq!km, 0)
                estrutRequis(totalReq).codEfr = BL_HandleNull(rsReq!cod_efr, "")
                estrutRequis(totalReq).codMed = BL_HandleNull(rsReq!cod_med, "")
                estrutRequis(totalReq).SeqUtente = BL_HandleNull(rsReq!seq_utente, "")
                estrutRequis(totalReq).TipoUtente = BL_HandleNull(rsReq!t_utente, "")
                estrutRequis(totalReq).Utente = BL_HandleNull(rsReq!Utente, "")
                estrutRequis(totalReq).NomeUtente = BL_HandleNull(rsReq!nome_ute, "")
                estrutRequis(totalReq).dt_nasc_ute = BL_HandleNull(rsReq!dt_nasc_ute, "")
                estrutRequis(totalReq).Sexo = BL_HandleNull(rsReq!sexo_ute, "")
                estrutRequis(totalReq).numCartaoUtente = BL_HandleNull(rsReq!n_cartao_ute, "")
                estrutRequis(totalReq).estadoFact = BL_HandleNull(rsReq!Flg_Facturado, gEstadoFactRequisNaoFacturado)
                If estrutRequis(totalReq).estadoFact = gEstadoFactRequisFacturado Then
                    If IF_ConfirmaFacturado(estrutRequis(totalReq).NReq) = False Then
                        estrutRequis(totalReq).estadoFact = gEstadoFactRequisNaoFacturado
                    End If
                End If
                estrutRequis(totalReq).dt_chega = BL_HandleNull(rsReq!dt_chega, Bg_DaData_ADO)
                estrutRequis(totalReq).convencao = BL_HandleNull(rsReq!convencao, -1)
                estrutRequis(totalReq).hemodialise = BL_HandleNull(rsReq!hemodialise, -1)
                estrutRequis(totalReq).fimSemana = BL_HandleNull(rsReq!fim_semana, 0)
                estrutRequis(totalReq).cod_sala = BL_HandleNull(rsReq!cod_sala, "")
                estrutRequis(totalReq).DescrEFR = BL_HandleNull(rsReq!descr_efr, "")
            End If
             IF_PreencheDadosReq = IF_PreencheEfrReq(totalReq)
                
            rsReq.MoveNext
        Wend
    End If
    rsReq.Close
    Set rsReq = Nothing
    IF_PreencheDadosReq = True
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Carregar estrutura das requisi��es:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_PreencheDadosReq", False
    IF_PreencheDadosReq = False
    Exit Function
    Resume Next
End Function


Private Function IF_PreencheEfrReq(indice As Integer) As Boolean
    Dim sSql As String
    Dim i As Integer
    Dim RsEFR As New ADODB.recordset
    Dim flg_existe As Boolean
    On Error GoTo TrataErro
    estrutRequis(indice).totalEntidades = 0
    ReDim Preserve estrutRequis(indice).entidades(estrutRequis(indice).totalEntidades)

    sSql = "SELECT  x1.cod_efr, x1.descr_efr,x1.flg_perc_facturar, x1.flg_controla_isencao,x1.flg_obriga_recibo,x1.deducao_ute, x1.flg_separa_dom, "
    sSql = sSql & "x1.FLG_ACTO_MED_EFR, x1.cod_empresa,x1.flg_compart_dom,x1.flg_dom_defeito, x1.flg_nao_urbano, x1.flg_65anos, x1.flg_insere_cod_rubr_efr, x1.flg_fds_fixo"
    sSql = sSql & " FROM sl_efr x1, sl_recibos x2 WHERE x1.cod_efr = x2.cod_efr AND x2.n_req = " & estrutRequis(indice).NReq
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
        
    If (RsEFR.RecordCount <= 0) Then
        IF_PreencheEfrReq = False
        RsEFR.Close
        Set RsEFR = Nothing
        Exit Function
    Else
        While Not RsEFR.EOF
            flg_existe = False
            For i = 1 To estrutRequis(indice).totalEntidades
                If estrutRequis(indice).entidades(i).cod_efr = BL_HandleNull(RsEFR!cod_efr, "") Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                estrutRequis(indice).totalEntidades = estrutRequis(indice).totalEntidades + 1
                ReDim Preserve estrutRequis(indice).entidades(estrutRequis(indice).totalEntidades)
                i = estrutRequis(indice).totalEntidades
                
                estrutRequis(indice).entidades(i).cod_efr = BL_HandleNull(RsEFR!cod_efr, "")
                estrutRequis(indice).entidades(i).descr_efr = BL_HandleNull(RsEFR!descr_efr, "")
                
                estrutRequis(indice).entidades(i).flg_perc_facturar = BL_HandleNull(RsEFR!flg_perc_facturar, "0")
                estrutRequis(indice).entidades(i).flg_controla_isencao = BL_HandleNull(RsEFR!flg_controla_isencao, "0")
                estrutRequis(indice).entidades(i).flg_obriga_recibo = BL_HandleNull(RsEFR!flg_obriga_recibo, "0")
                estrutRequis(indice).entidades(i).deducao_ute = BL_HandleNull(RsEFR!deducao_ute, "")
                estrutRequis(indice).entidades(i).flg_acto_med = BL_HandleNull(RsEFR!flg_acto_med_efr, "0")
                estrutRequis(indice).entidades(i).cod_empresa = BL_HandleNull(RsEFR!cod_empresa, "")
                estrutRequis(indice).entidades(i).flg_compart_dom = BL_HandleNull(RsEFR!flg_compart_dom, "0")
                estrutRequis(indice).entidades(i).flg_dom_defeito = BL_HandleNull(RsEFR!flg_dom_defeito, "0")
                estrutRequis(indice).entidades(i).flg_nao_urbano = BL_HandleNull(RsEFR!flg_nao_urbano, "0")
                estrutRequis(indice).entidades(i).flg_65anos = BL_HandleNull(RsEFR!flg_65anos, "0")
                estrutRequis(indice).entidades(i).codDom = BL_HandleNull(BL_SelCodigo("SL_TBF_T_URBANO", "DOMICILIO", "COD_T_URBANO", estrutRequis(indice).codUrbano), -1)
                estrutRequis(indice).entidades(i).flg_separa_dom = BL_HandleNull(RsEFR!flg_separa_dom, "0")
                estrutRequis(indice).entidades(i).flg_insere_cod_rubr_efr = BL_HandleNull(RsEFR!flg_insere_cod_rubr_efr, "0")
                estrutRequis(indice).entidades(i).flg_FdsValFixo = BL_HandleNull(RsEFR!flg_fds_fixo, "0")
                estrutRequis(indice).entidades(i).n_ord = IF_RetornaNOrd(estrutRequis(indice).NReq, estrutRequis(indice).entidades(i).cod_efr)
                If estrutRequis(indice).hemodialise <> mediComboValorNull Then
                    estrutRequis(indice).entidades(i).flg_controla_isencao = 1
                End If
                
                IF_PreencheEfrReq = IF_PreencheAnaReq(indice, i)
                
            End If
            RsEFR.MoveNext
        Wend
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Carregar estrutura das entidades:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_PreencheEfrReq", False
    IF_PreencheEfrReq = False
    Exit Function
    Resume Next
End Function

Private Function IF_PreencheAnaReq(iReq As Integer, iEfr As Integer)
    Dim mens As String
    Dim TotalFactEnt As Double
    Dim taxa As String
    Dim sSql As String
    Dim i As Integer
    Dim rsAnalises As New ADODB.recordset
    Dim flg_existe As Boolean
    Dim iCred As Integer
    Dim iAna As Integer
    Dim v_cod_ana As String
    Dim v_descr_ana As String
    Dim v_cod_ana_gh As String
    Dim v_descr_ana_gh As String
    Dim v_membro As Integer
    Dim v_peso As Double
    Dim v_qtd As Integer
    
    On Error GoTo TrataErro
    estrutRequis(iReq).entidades(iEfr).totalCred = 0
    ReDim estrutRequis(iReq).entidades(iEfr).cRed(0)
    
    sSql = "SELECT   x1.cod_facturavel, x1.p1,x1.flg_facturado, x1.ordem_marcacao,x1.flg_retirado, "
    sSql = sSql & " x1.flg_adicionada, x1.ordem_marcacao, x1.isencao,x1.cor_p1, x1.cod_efr, X1.quantidade, "
    sSql = sSql & " x1.cod_medico, x1.isencao, "
    sSql = sSql & " x1.n_benef, x1.fds,  x1.n_rec, x1.serie_rec, "
    sSql = sSql & " x1.flg_recibo_manual "
    sSql = sSql & " FROM SL_RECIBOS_DET x1  WHERE  x1.n_req = " & estrutRequis(iReq).NReq
    sSql = sSql & " AND x1.cod_efr = " & estrutRequis(iReq).entidades(iEfr).cod_efr
    If gSGBD = gOracle Then
        sSql = sSql & " ORDER BY cod_efr ASC, p1 ASC, x1.ordem_marcacao ASC "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " ORDER BY x1.cod_efr ASC, p1 ASC, x1.ordem_marcacao ASC "
    End If
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnalises.Open sSql, gConexao
    If rsAnalises.RecordCount > 0 Then
        While Not rsAnalises.EOF
            iCred = -1
            iAna = -1
            flg_existe = False
            For i = 1 To estrutRequis(iReq).entidades(iEfr).totalCred
                If estrutRequis(iReq).entidades(iEfr).cRed(i).n_credencial = BL_HandleNull(rsAnalises!p1, "0") Then
                    iCred = i
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                'PREENCHE ESTRUTURA DE CREDENCIAIS
                estrutRequis(iReq).entidades(iEfr).totalCred = estrutRequis(iReq).entidades(iEfr).totalCred + 1
                ReDim Preserve estrutRequis(iReq).entidades(iEfr).cRed(estrutRequis(iReq).entidades(iEfr).totalCred)
                iCred = estrutRequis(iReq).entidades(iEfr).totalCred
                
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = BL_HandleNull(rsAnalises!isencao, -1)
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_medico = BL_HandleNull(rsAnalises!cod_medico, "")
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).cor = BL_HandleNull(rsAnalises!cor_p1, "B")
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).n_credencial = BL_HandleNull(rsAnalises!p1, "-1")
                If Len(estrutRequis(iReq).entidades(iEfr).cRed(iCred).n_credencial) > 7 Then
                    estrutRequis(iReq).entidades(iEfr).cRed(iCred).N_REQ_ARS = estrutRequis(iReq).entidades(iEfr).cRed(iCred).n_credencial
                Else
                    estrutRequis(iReq).entidades(iEfr).cRed(iCred).N_REQ_ARS = estrutRequis(iReq).NReq & "_" & estrutRequis(iReq).entidades(iEfr).cRed(iCred).n_credencial
                End If
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).totalAna = 0
                ReDim estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(0)
            End If
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).totalAna = estrutRequis(iReq).entidades(iEfr).cRed(iCred).totalAna + 1
            iAna = estrutRequis(iReq).entidades(iEfr).cRed(iCred).totalAna
            ReDim Preserve estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna)
            
            
            'PREENCHE ESTRUTURA DE ANALISES
            v_cod_ana = ""
            v_cod_ana_gh = ""
            v_descr_ana = ""
            v_descr_ana_gh = ""
            v_peso = 0
            v_membro = 0
            v_qtd = 0
            IF_RetornaMapeamentos BL_HandleNull(rsAnalises!cod_facturavel, "0"), estrutRequis(iReq).entidades(iEfr).cod_efr, _
                                  v_cod_ana, v_descr_ana, v_cod_ana_gh, v_descr_ana_gh, v_peso, v_membro, v_qtd
                                  
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).CodFacturavel = BL_HandleNull(rsAnalises!cod_facturavel, "0")
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).codAna = v_cod_ana
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).descrAna = v_descr_ana
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).CodAnaGH = v_cod_ana_gh
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).descrAnaGH = v_descr_ana_gh
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Facturada = BL_HandleNull(rsAnalises!Flg_Facturado, gEstadoFactRequisNaoFacturado)
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_Retirada = BL_HandleNull(rsAnalises!flg_retirado, "0")
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Flg_adicionada = BL_HandleNull(rsAnalises!Flg_adicionada, "0")
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec = BL_HandleNull(rsAnalises!n_rec, "0")
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).serieRec = BL_HandleNull(rsAnalises!serie_rec, "0")
            'estrutRequis(iReq).entidades(iEfr).cred(iCred).analises(iAna).estadoRec = BL_HandleNull(rsAnalises!estado_rec, "")
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).ordem = BL_HandleNull(rsAnalises!ordem_marcacao, "0")
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Peso = BL_HandleNull(v_peso, 0)
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).tratado = False
            If v_qtd > 0 Then
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Qtd_Ana = v_qtd
            Else
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Qtd_Ana = BL_HandleNull(rsAnalises!quantidade, 1)
            End If
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).fds = BL_HandleNull(rsAnalises!fds, "")
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).data = estrutRequis(iReq).dt_chega
            
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_percentagem = estrutRequis(iReq).entidades(iEfr).flg_perc_facturar
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_controlaIsencao = estrutRequis(iReq).entidades(iEfr).flg_controla_isencao
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_recibo_manual = BL_HandleNull(rsAnalises!flg_recibo_manual, "0")
            
            If estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_percentagem = False Then
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescEnvio = ""
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescCodif = ""
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescUtente = ""
            Else
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescEnvio = IF_RetornaDescontoEnvio(estrutRequis(iReq).NReq, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec, estrutRequis(iReq).entidades(iEfr).cod_efr)
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescCodif = IF_RetornaPercentPagEFR(estrutRequis(iReq).entidades(iEfr).cod_efr, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).CodAnaGH, estrutRequis(iReq).dt_chega)
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescUtente = IF_RetornaDescontoUtente(estrutRequis(iReq).NReq, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nRec, estrutRequis(iReq).entidades(iEfr).cod_efr)
            End If
            
            ' APENAS ENVIA REQUISICOES SE TIVER RECIBO EMITIDO (PARA ENTIDADES COM ESTA FLAG)
            If estrutRequis(iReq).entidades(iEfr).flg_obriga_recibo = 1 Then
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_ObrigaRecibo = True
            Else
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_ObrigaRecibo = False
            End If
            
            'PREENCHE CODIGO DA RUBRICA PARA DEDUZIR A ENTIDADE PORQUE UTENTE JA PAGOU
            ' LHL - COMP. SEGUROS ACOREANA
            estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).deducaoUtente = estrutRequis(iReq).entidades(iEfr).deducao_ute
            
            'PREENCHE TAXA UTENTE
            If estrutRequis(iReq).entidades(iEfr).cRed(iCred).cod_isencao = gTipoIsencaoIsento Then
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).precoUnitarioUte = 0
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).precoUte = 0
            Else
                taxa = IF_RetornaTaxaAnalise(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).codAna, estrutRequis(iReq).entidades(iEfr).cod_efr, _
                        estrutRequis(iReq).TipoUtente, estrutRequis(iReq).Utente, estrutRequis(iReq).dt_chega)
                If taxa <= 0 Then
                    taxa = 0
                End If
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).precoUnitarioUte = Replace(taxa, ".", ",")
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).precoUte = estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).precoUnitarioUte * estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Qtd_Ana
            End If
            
            taxa = 0
            taxa = IF_RetornaTaxaAnaliseEntidade(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).codAna, estrutRequis(iReq).entidades(iEfr).cod_efr, BL_HandleNull(estrutRequis(iReq).dt_chega, ""), _
                                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).cod_rubr_efr, estrutRequis(iReq).TipoUtente, estrutRequis(iReq).Utente, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PrecoUnitario, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).Qtd_Ana, _
                                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).nr_c, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_controlaIsencao, estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).descr_rubr_efr, estrutRequis(iReq).hemodialise)
            Select Case taxa
                Case -1
                    mens = " An�lise n�o est� mapeada para o FACTUS!"
                Case -2
                    mens = "Impossivel abrir conex�o com o FACTUS!"
                Case -3
                    mens = "Impossivel seleccionar portaria activa para a entidade em causa!"
                Case -4
                    mens = "N�o existe taxa codificada para a an�lise em causa!"
                Case Else
                    mens = ""
            End Select
            If mens <> "" Then
                If gTipoInstituicao = "PRIVADA" Then
                    BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
                End If
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).preco = 0
            Else
                taxa = Replace(taxa, ".", ",")
                If Mid(taxa, 1, 1) = "," Then
                    taxa = "0" & taxa
                End If
                estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).preco = taxa
                TotalFactEnt = TotalFactEnt + taxa
            End If
            
            ' ---------------------------------------------------------------------------
            ' VERIFICA SE ENVIA PARA FACTUS PERCENTAGEM DESCONTO DO UTENTE
            ' ---------------------------------------------------------------------------
            If estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).flg_percentagem = True Then
                If CDbl(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescCodif) < 100 Then
                    estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescEnvio = CDbl(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescCodif)
                Else
                    estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescEnvio = CDbl(estrutRequis(iReq).entidades(iEfr).cRed(iCred).analises(iAna).PercDescUtente)
                End If
            End If
            
            rsAnalises.MoveNext
        Wend
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao Carregar estrutura das Analises:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_PreencheAnaReq", False
    IF_PreencheAnaReq = False
    Exit Function
    Resume Next
End Function


Private Sub IF_RetornaMapeamentos(ByVal cod_facturavel As String, ByVal cod_efr As String, ByRef v_cod_ana As String, _
                                  ByRef v_descr_ana As String, ByRef v_cod_ana_gh As String, ByRef v_descr_ana_gh As String, _
                                  ByRef v_peso As Double, ByRef v_membros As Integer, ByRef v_qtd As Integer)
    Dim sSql As String
    Dim rsAnalises2 As New ADODB.recordset
    On Error GoTo TrataErro
    v_cod_ana = ""
    v_cod_ana_gh = ""
    v_descr_ana = ""
    v_descr_ana_gh = ""
    v_peso = 0
    v_membros = 0
    v_qtd = 0
    
    If Mid(BL_HandleNull(cod_facturavel, "0"), 1, 1) = "P" Then
        If gSGBD = gOracle Then
            sSql = "SELECT x2.cod_perfis cod_ana, x2.descr_perfis descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso peso_estatistico, x3.flg_conta_membros, x3.qtd "
            sSql = sSql & " FROM   sl_perfis x2, sl_ana_facturacao x3 "
            sSql = sSql & " WHERE x2.cod_perfis = x3.cod_ana(+) AND x2.cod_perfis = " & BL_TrataStringParaBD(cod_facturavel) & " AND x3.cod_efr = " & cod_efr
            sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        ElseIf gSGBD = gSqlServer Then
            sSql = "SELECT x2.cod_perfis cod_ana, x2.descr_perfis descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso peso_estatistico, x3.flg_conta_membros, x3.qtd "
            sSql = sSql & " FROM   sl_perfis x2 LEFT OUTER JOIN sl_ana_facturacao x3 ON x2.cod_perfis = x3.cod_ana"
            sSql = sSql & " WHERE x2.cod_perfis = " & BL_TrataStringParaBD(cod_facturavel) & " AND x3.cod_efr = " & cod_efr
            sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        End If
    Else
        If gSGBD = gOracle Then
            sSql = "SELECT x2.cod_ana cod_ana,  x2.descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso_estatistico, x3.flg_conta_membros, x3.qtd "
            sSql = sSql & " FROM   slv_analises_factus x2, sl_ana_facturacao x3 "
            sSql = sSql & " WHERE x2.cod_ana = x3.cod_ana(+) AND x2.cod_ana = " & BL_TrataStringParaBD(cod_facturavel)
            sSql = sSql & "  AND x3.cod_efr = " & cod_efr
            sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
        ElseIf gSGBD = gSqlServer Then
            sSql = "SELECT x2.cod_ana cod_ana,  x2.descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso_estatistico, x3.flg_conta_membros, x3.qtd "
            sSql = sSql & " FROM   slv_analises_factus x2 LEFT OUTER JOIN sl_ana_facturacao x3 ON x2.cod_ana = x3.cod_ana"
            sSql = sSql & " WHERE x2.cod_ana = " & BL_TrataStringParaBD(cod_facturavel)
            sSql = sSql & "  AND x3.cod_efr = " & cod_efr
            sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
        End If
    End If
    
    rsAnalises2.CursorLocation = adUseServer
    rsAnalises2.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnalises2.Open sSql, gConexao
    If rsAnalises2.RecordCount = 0 Then
        rsAnalises2.Close
        If Mid(cod_facturavel, 1, 1) = "P" Then
            If gSGBD = gOracle Then
                sSql = "SELECT x2.cod_perfis cod_ana, x2.descr_perfis descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso peso_estatistico, x3.flg_conta_membros, x3.qtd  "
                sSql = sSql & " FROM   sl_perfis x2, sl_ana_facturacao x3 "
                sSql = sSql & " WHERE x2.cod_perfis = x3.cod_ana(+) AND x2.cod_perfis = " & BL_TrataStringParaBD(cod_facturavel) & " AND  cod_efr is null "
                sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
                sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            ElseIf gSGBD = gSqlServer Then
                sSql = "SELECT x2.cod_perfis cod_ana, x2.descr_perfis descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso peso_estatistico, x3.flg_conta_membros,x3.qtd  "
                sSql = sSql & " FROM   sl_perfis x2 LEFT OUTER JOIN sl_ana_facturacao x3 ON x2.cod_perfis = x3.cod_ana"
                sSql = sSql & " WHERE x2.cod_perfis = " & BL_TrataStringParaBD(cod_facturavel) & " AND  cod_efr is null "
                sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
                sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            End If
        Else
            If gSGBD = gOracle Then
                sSql = "SELECT x2.cod_ana cod_ana,  x2.descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso_estatistico, x3.flg_conta_membros, x3.qtd "
                sSql = sSql & " FROM   slv_analises_factus x2, sl_ana_facturacao x3 "
                sSql = sSql & " WHERE x2.cod_ana = x3.cod_ana(+) AND x2.cod_ana = " & BL_TrataStringParaBD(cod_facturavel) & " AND  cod_efr is null "
                sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
                sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
            ElseIf gSGBD = gSqlServer Then
                sSql = "SELECT x2.cod_ana cod_ana,  x2.descr_ana, x3.cod_ana_gh,x3.descr_ana_facturacao, x2.peso_estatistico, x3.flg_conta_membros,x3.qtd  "
                sSql = sSql & " FROM   slv_analises_factus x2 LEFT OUTER JOIN sl_ana_facturacao x3 ON x2.cod_ana = x3.cod_ana"
                sSql = sSql & " WHERE x2.cod_ana = " & BL_TrataStringParaBD(cod_facturavel) & " AND  cod_efr is null "
                sSql = sSql & " AND cod_portaria IN (Select x4.cod_portaria FROM sl_portarias x4 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
                sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
            End If
        End If
        rsAnalises2.CursorLocation = adUseServer
        rsAnalises2.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnalises2.Open sSql, gConexao
        If rsAnalises2.RecordCount >= 1 Then
            v_cod_ana = BL_HandleNull(rsAnalises2!cod_ana, "")
            v_cod_ana_gh = BL_HandleNull(rsAnalises2!cod_ana_gh, "")
            v_descr_ana = BL_HandleNull(rsAnalises2!descr_ana, "")
            v_descr_ana_gh = BL_HandleNull(rsAnalises2!descr_ana_facturacao, "")
            v_peso = BL_HandleNull(rsAnalises2!peso_estatistico, 0)
            v_membros = BL_HandleNull(rsAnalises2!flg_conta_membros, 0)
            v_qtd = BL_HandleNull(rsAnalises2!qtd, 0)
        End If
    Else
        v_cod_ana = BL_HandleNull(rsAnalises2!cod_ana, "")
        v_cod_ana_gh = BL_HandleNull(rsAnalises2!cod_ana_gh, "")
        v_descr_ana = BL_HandleNull(rsAnalises2!descr_ana, "")
        v_descr_ana_gh = BL_HandleNull(rsAnalises2!descr_ana_facturacao, "")
        v_peso = BL_HandleNull(rsAnalises2!peso_estatistico, 0)
        v_membros = BL_HandleNull(rsAnalises2!flg_conta_membros, 0)
        v_qtd = BL_HandleNull(rsAnalises2!qtd, 0)
    End If
    rsAnalises2.Close
    Set rsAnalises2 = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Carregar mapeamentos de analises:" & Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaMapeamentos", False
    Exit Sub
    Resume Next
End Sub
Private Function IF_RetornaDescontoEnvio(n_req As String, n_rec As String, cod_efr As String) As String
    Dim sSql As String
    Dim rsDesc As New ADODB.recordset
    On Error GoTo TrataErro
    
    IF_RetornaDescontoEnvio = ""
    sSql = " SELECT desconto_envio FROM sl_recibos where n_req = " & BL_TrataStringParaBD(n_req) & " AND cod_efr = " & cod_efr
    If n_rec <> "0" Then
        sSql = sSql & " AND n_rec = " & n_rec
    End If
    
    rsDesc.CursorLocation = adUseServer
    rsDesc.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDesc.Open sSql, gConexao
    If rsDesc.RecordCount > 0 Then
        IF_RetornaDescontoEnvio = BL_HandleNull(rsDesc!desconto_envio, "0")
    End If
    rsDesc.Close
    Set rsDesc = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "IF", "IF_RetornaDescontoEnvio"
    IF_RetornaDescontoEnvio = ""
    Exit Function
    Resume Next
End Function


Private Function IF_RetornaDescontoUtente(n_req As String, n_rec As String, cod_efr As String) As String
    Dim sSql As String
    Dim rsDesc As New ADODB.recordset
    IF_RetornaDescontoUtente = "0"
    sSql = " SELECT desconto, estado  FROM sl_recibos where n_req = " & BL_TrataStringParaBD(n_req) & " AND cod_efr = " & cod_efr
    If n_rec <> "0" Then
        sSql = sSql & " AND n_rec = " & n_rec
    End If
    
    rsDesc.CursorLocation = adUseServer
    rsDesc.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDesc.Open sSql, gConexao
    If rsDesc.RecordCount > 0 Then
        If BL_HandleNull(rsDesc!estado, "") = "NL" Then
            IF_RetornaDescontoUtente = 0
        Else
            IF_RetornaDescontoUtente = 100 - BL_HandleNull(rsDesc!Desconto, "100")
        End If
    ElseIf rsDesc.RecordCount = 0 Then
        rsDesc.Close
        sSql = " SELECT desconto,estado FROM sl_recibos where n_req = " & BL_TrataStringParaBD(n_req) & " AND cod_efr = " & cod_efr
        sSql = sSql & " AND n_rec = " & n_rec
        rsDesc.CursorLocation = adUseServer
        rsDesc.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsDesc.Open sSql, gConexao
        If rsDesc.RecordCount > 0 Then
            If BL_HandleNull(rsDesc!estado, "") = "NL" Then
                IF_RetornaDescontoUtente = 0
            Else
                IF_RetornaDescontoUtente = 100 - BL_HandleNull(rsDesc!Desconto, "100")
            End If
        End If
    End If
    rsDesc.Close
    Set rsDesc = Nothing
End Function



Private Function IF_RetornaNOrd(n_req As String, cod_efr As String) As Integer
    Dim sSql As String
    Dim rsResp As New ADODB.recordset
    Dim iBD_Aberta As Integer
    
    iBD_Aberta = BL_Abre_Conexao_Secundaria(gOracle)

    If iBD_Aberta = 0 Then
        Exit Function
    End If
    sSql = "select n_ord from fa_movi_resp where t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(n_req) & " and cod_efr = " & cod_efr
    Set rsResp = New ADODB.recordset
    rsResp.CursorLocation = adUseServer
    rsResp.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsResp.Open sSql, gConexaoSecundaria
    If rsResp.RecordCount > 0 Then
        IF_RetornaNOrd = BL_HandleNull(rsResp!n_ord, 1)
    Else
        sSql = "select nvl(max(n_ord),0) conta_n_ord from fa_movi_resp where t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(n_req)
        Set rsResp = New ADODB.recordset
        rsResp.CursorLocation = adUseServer
        rsResp.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsResp.Open sSql, gConexaoSecundaria
        If rsResp.RecordCount > 0 Then
            IF_RetornaNOrd = rsResp!conta_n_ord + 1
        End If
    End If
    rsResp.Close
    Set rsResp = Nothing
    BL_Fecha_Conexao_Secundaria
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE UTENTE TEM DIREITO A DESCONTO POR TER MAIS DE 65 ANOS

' -----------------------------------------------------------------------------------------------
Public Function IF_VerificaRegraArsData(flg_65anos As Integer, dt_nasc As String, dt_chega As String) As Boolean
    Dim idade As String
    On Error GoTo TrataErro
    
    If BL_HandleNull(flg_65anos, 0) = 0 Then
        IF_VerificaRegraArsData = False
    Else
        idade = BG_CalculaIdade(BL_HandleNull(dt_nasc, Bg_DaData_ADO), CDate(dt_chega))
        If CLng(Mid(idade, 1, InStr(1, idade, " "))) >= 65 Then
            IF_VerificaRegraArsData = True
        Else
            IF_VerificaRegraArsData = False
        End If
    End If

Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "IF", "IF_VerificaRegraArs"
    IF_VerificaRegraArsData = False
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' RETORNA TAXA PAGA ENTIDADE DE UMA ANALISE PARA UMA ENTIDADE(FACTUS)

' -----------------------------------------------------------------------------------------------

Public Function IF_RetornaTaxaAnaliseEntidadeSub(ByVal codAna As String, ByVal Entidade As String, ByVal p_data As String, _
                                              ByRef cod_rubr_efr As String, ByVal t_utente As String, Utente As String, _
                                              ByRef preco_unitario As Double, ByVal qtd As Integer, ByVal nr_c As String, _
                                              ByVal controlaIsentos As Boolean, ByRef descr_rubr_efr As String) As String
    Dim sSql As String
    Dim iBD_Aberta As Long
    Dim rsAnaFact As New ADODB.recordset
    Dim rsPortarias As New ADODB.recordset
    Dim rsTaxas As New ADODB.recordset
    Dim rsEntidades As New ADODB.recordset
    
    ' --------------------------------------
    ' PARAMETROS PARA PROCEDIMENTO FACTUS
    ' --------------------------------------
    Dim p_cod_rubr As String
    Dim p_valor_ent As String
    Dim p_valor_adic As String
    Dim p_valor_doe As String
    Dim p_valor_taxa As String
    Dim p_perc_desc As String
    Dim p_valor_total As String
    Dim p_cod_rubr_efr As String
    Dim p_nr_c As String
    Dim p_descr_rubr_efr As String
    Dim p_alt_prec As String
    Dim p_w_msg As String
    Dim subprecario As String
    Dim ResFACTUS As Integer
    
    On Error GoTo TrataErro
    ' -----------------------------------------------------------------------------------------------
    ' PROCURA A RUBRICA ASSOCIADA A ANALISE
    ' -----------------------------------------------------------------------------------------------
    sSql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE cod_efr = " & Entidade & " AND  cod_ana = " & BL_TrataStringParaBD(codAna)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaFact.Open sSql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        sSql = "SELECT cod_ana_gh, flg_conta_membros FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(codAna)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(p_data, Bg_DaData_ADO)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaFact.Open sSql, gConexao
        If rsAnaFact.RecordCount <= 0 Then
            ' ANALISE NAO ESTA MAPEADA PARA FACTUS
            IF_RetornaTaxaAnaliseEntidadeSub = "-1"
            rsAnaFact.Close
            Set rsAnaFact = Nothing
            Exit Function
        End If
    End If
        
    ' ------------------------------------------------------------------------------------------
    ' ABRE CONEXAO COM FACTUS
    ' ------------------------------------------------------------------------------------------
    iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
    If iBD_Aberta = 0 Then
        ' NAO EXISTE CONEXAO FACTUS
        IF_RetornaTaxaAnaliseEntidadeSub = "-2"
    End If

    
    If ResFACTUS > 0 Then
        If Mid(p_valor_ent, 1, 1) = "," Or Mid(p_valor_ent, 1, 1) = "." Or p_valor_ent = "" Then
            p_valor_ent = "0" & p_valor_ent
        End If
        If controlaIsentos = False Then
            IF_RetornaTaxaAnaliseEntidadeSub = p_valor_ent - p_valor_doe
        ElseIf controlaIsentos = True Then
            IF_RetornaTaxaAnaliseEntidadeSub = CDbl(Replace(BL_HandleNull(p_valor_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(p_valor_taxa, 0), ".", ","))
        End If
        cod_rubr_efr = p_cod_rubr_efr
        descr_rubr_efr = p_descr_rubr_efr
        'soliveira cantanhede
        If Entidade = 2 And gLAB = "ARC" Then
            nr_c = Replace(p_valor_ent, ".", ",") / 0.439
            nr_c = CInt(nr_c)
        Else
            nr_c = ""
        End If
    Else
    
        ' ------------------------------------------------------------------------------------------
        ' VERIFICA QUAL A ENTIDADE ACTIVA
        ' ------------------------------------------------------------------------------------------
        sSql = "SELECT tab_utilizada FROM fa_efr WHERE cod_efr = " & Entidade
        rsEntidades.CursorLocation = adUseServer
        rsEntidades.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsEntidades.Open sSql, gConexaoSecundaria
        If rsEntidades.RecordCount <= 0 Then
            ' NAO EXISTE ENTIDADE CODIFICADA
            IF_RetornaTaxaAnaliseEntidadeSub = "-5"
        Else
            
            
            ' ------------------------------------------------------------------------------------------
            ' VERIFICA QUAL A PORTARIA ACTIVA PARA A ENTIDADE EM CAUSA
            ' ------------------------------------------------------------------------------------------
            sSql = "SELECT portaria FROM fa_portarias WHERE ((dt_ini <= " & BL_TrataStringParaBD(p_data) & " AND dt_fim is null) OR (dt_ini <= " & BL_TrataDataParaBD(p_data)
            sSql = sSql & " AND dt_fim >=" & BL_TrataStringParaBD(p_data) & "))"
            sSql = sSql & " AND cod_efr = " & BL_HandleNull(rsEntidades!tab_utilizada, "0") & " AND user_rem is null "
            rsPortarias.CursorLocation = adUseServer
            rsPortarias.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsPortarias.Open sSql, gConexaoSecundaria
            If rsPortarias.RecordCount <= 0 Then
                ' NAO EXISTE PORTARIA ACTIVA
                IF_RetornaTaxaAnaliseEntidadeSub = "-3"
            Else
                ' ------------------------------------------------------------------------------------------
                ' RETORNA A TAXA PARA A ANALISE EM CAUSA
                ' ------------------------------------------------------------------------------------------
                sSql = "SELECT val_pag_ent, val_pag_doe, val_taxa,cod_rubr_efr, descr_rubr_efr FROM fa_pr_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(BL_HandleNull(rsAnaFact!cod_ana_gh, ""))
                sSql = sSql & " AND portaria = " & BL_TrataStringParaBD(rsPortarias!Portaria) & " AND user_rem IS NULL "
                rsTaxas.CursorLocation = adUseServer
                rsTaxas.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsTaxas.Open sSql, gConexaoSecundaria
                If rsTaxas.RecordCount <= 0 Then
                    ' NAO EXISTE TAXA CODIFICADA
                    IF_RetornaTaxaAnaliseEntidadeSub = "-4"
                Else
                    If controlaIsentos = False Then
                        ' alterado a pedido do bmonteiro para ljmanso
                        IF_RetornaTaxaAnaliseEntidadeSub = CStr(BL_HandleNull(rsTaxas!val_pag_ent, 0) - BL_HandleNull(rsTaxas!val_pag_doe, 0))
                    Else
                        If BL_HandleNull(rsTaxas!val_Taxa, "0") = "0" Then
                            IF_RetornaTaxaAnaliseEntidadeSub = CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_doe, 0), ".", ","))
                        ElseIf BL_HandleNull(rsTaxas!val_pag_doe, "0") = "0" Then
                            IF_RetornaTaxaAnaliseEntidadeSub = CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(rsTaxas!val_Taxa, 0), ".", ","))
                        Else
                            IF_RetornaTaxaAnaliseEntidadeSub = CDbl(Replace(BL_HandleNull(rsTaxas!val_pag_ent, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(rsTaxas!val_Taxa, 0), ".", ",")) + CDbl(Replace(BL_HandleNull(p_valor_doe, 0), ".", ","))
                        End If
                    End If
                    If CDbl(IF_RetornaTaxaAnaliseEntidadeSub) < 0 Then
                        IF_RetornaTaxaAnaliseEntidadeSub = 0
                    End If
                    cod_rubr_efr = CStr(BL_HandleNull(rsTaxas!cod_rubr_efr, ""))
                    descr_rubr_efr = CStr(BL_HandleNull(rsTaxas!descr_rubr_efr, ""))
                End If
                rsTaxas.Close
                Set rsTaxas = Nothing
            End If
            rsPortarias.Close
            Set rsPortarias = Nothing
        End If
        rsEntidades.Close
        Set rsEntidades = Nothing
        preco_unitario = Replace(IF_RetornaTaxaAnaliseEntidadeSub, ".", ",")
        Exit Function
    End If
    
    If IF_RetornaTaxaAnaliseEntidadeSub > 0 And qtd > 0 Then
        preco_unitario = Replace(IF_RetornaTaxaAnaliseEntidadeSub, ".", ",")
        IF_RetornaTaxaAnaliseEntidadeSub = qtd * Replace(IF_RetornaTaxaAnaliseEntidadeSub, ".", ",")
        IF_RetornaTaxaAnaliseEntidadeSub = Replace(IF_RetornaTaxaAnaliseEntidadeSub, ",", ".")
    Else
        preco_unitario = "0"
    End If
    
    rsAnaFact.Close
    Set rsAnaFact = Nothing
Exit Function
TrataErro:
    IF_RetornaTaxaAnaliseEntidadeSub = "0"
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaTaxaAnaliseEntidadeSub"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' INSERE LINHA NA TABELA SL_REQ_ARS

' -----------------------------------------------------------------------------------------------
Public Sub IF_InsereReqARS(n_req As String, nr_req_ars As String, natureza As String, cod_dom As String, _
                           qtd_dom As String, cod_post_dom As String, localidade_dom As String, _
                           dt_ini_sess_fisio As String, dt_fim_sess_fisio As String, t_utente As String, _
                           Utente As String, convencao As Integer, flg_tip_req As String)
    Dim sSql As String
    Dim iReg As Integer
    Dim grupo_req As String
    Dim rsGH As New ADODB.recordset
    On Error GoTo TrataErro
    If cod_dom = "0" Then
        cod_dom = "null"
        qtd_dom = "null"
        cod_post_dom = ""
        localidade_dom = ""
    End If
    sSql = "INSERT INTO sl_req_ars(n_req, nr_req_ars, natureza, cod_dom, qtd_dom,cod_post_dom, localidade_dom, "
    sSql = sSql & " dt_ini_sess_fisio,dt_fim_sess_fisio , dt_cri, hr_cri, user_cri,t_utente,utente) VALUES("
    sSql = sSql & n_req & ","
    sSql = sSql & BL_TrataStringParaBD(nr_req_ars) & ","
    sSql = sSql & BL_TrataStringParaBD(natureza) & ","
    sSql = sSql & cod_dom & ","
    sSql = sSql & qtd_dom & ","
    sSql = sSql & BL_TrataStringParaBD(cod_post_dom) & ","
    sSql = sSql & BL_TrataStringParaBD(localidade_dom) & ","
    sSql = sSql & BL_TrataDataParaBD(dt_ini_sess_fisio) & ","
    sSql = sSql & BL_TrataDataParaBD(dt_fim_sess_fisio) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(t_utente)) & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(Utente)) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    
    If gTipoInstituicao <> gTipoInstituicaoHospitalar Then
    
        sSql = "SELECT grupo_req FROM fa_tbf_tip_req WHERE flg_tip_req = " & BL_TrataStringParaBD(flg_tip_req)
        rsGH.CursorLocation = adUseServer
        rsGH.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsGH.Open sSql, gConexao
        If rsGH.RecordCount >= 1 Then
            grupo_req = BL_HandleNull(rsGH!grupo_req, "")
        End If
        rsGH.Close
        
        sSql = "INSERT INTO sd_req_ars( num_doc, natureza, convencao, cod_dom, km_dom, cod_post_dom, localidade_dom, "
        sSql = sSql & " dt_ini_sess_fisio,dt_fim_sess_fisio, grupo_req, t_doente, doente) VALUES("
        sSql = sSql & BL_TrataStringParaBD(nr_req_ars) & ","
        sSql = sSql & BL_TrataStringParaBD(natureza) & ","
        sSql = sSql & BL_TrataStringParaBD(CStr(convencao)) & ","
        sSql = sSql & cod_dom & ","
        sSql = sSql & qtd_dom & ","
        sSql = sSql & BL_TrataStringParaBD(cod_post_dom) & ","
        sSql = sSql & BL_TrataStringParaBD(localidade_dom) & ","
        sSql = sSql & BL_TrataDataParaBD(dt_ini_sess_fisio) & ","
        sSql = sSql & BL_TrataDataParaBD(dt_fim_sess_fisio) & ","
        sSql = sSql & BL_TrataStringParaBD(grupo_req) & ","
        sSql = sSql & BL_TrataStringParaBD(CStr(t_utente)) & ","
        sSql = sSql & BL_TrataStringParaBD(CStr(Utente)) & ")"
        BG_ExecutaQuery_ADO sSql
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_InsereReqARS"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' APAGA LINHA SL_REQ_ARS INSERE LINHA NA TABELA SL_REQ_ARS_HIST

' -----------------------------------------------------------------------------------------------
Public Sub IF_ApagaReqARS(n_req As String, nr_req_ars As String)
    Dim sSql As String
    Dim RsARS As New ADODB.recordset
    Dim iReg As Integer
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_req_ars WHERE n_req = " & n_req
    If nr_req_ars <> "" Then
        sSql = sSql & " AND nr_req_ars = " & BL_TrataStringParaBD(nr_req_ars)
    End If
    RsARS.CursorLocation = adUseServer
    RsARS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsARS.Open sSql, gConexao
    If RsARS.RecordCount >= 1 Then
        sSql = "INSERT INTO sl_req_ars_hist(n_req, nr_req_ars, natureza, cod_dom, qtd_dom,cod_post_dom, localidade_dom, "
        sSql = sSql & " dt_ini_sess_fisio,dt_fim_sess_fisio , dt_cri, hr_cri, user_cri ) VALUES("
        sSql = sSql & n_req & ","
        sSql = sSql & BL_TrataStringParaBD(RsARS!nr_req_ars) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsARS!natureza, "A")) & ","
        sSql = sSql & BL_HandleNull(RsARS!cod_dom, 0) & ","
        sSql = sSql & BL_HandleNull(RsARS!qtd_dom, 0) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsARS!cod_post_dom, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsARS!localidade_dom, "")) & ","
        sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(RsARS!dt_ini_sess_fisio, "")) & ","
        sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(RsARS!dt_fim_sess_fisio, "")) & ","
        sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(RsARS!dt_cri, "-1")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsARS!hr_cri, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsARS!user_cri, "")) & ")"
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
        
        sSql = "DELETE FROM sl_req_ars WHERE n_req = " & n_req
        If nr_req_ars <> "" Then
            sSql = sSql & " AND nr_req_ars = " & BL_TrataStringParaBD(nr_req_ars)
        End If
        BG_ExecutaQuery_ADO sSql
        sSql = "DELETE FROM sd_req_ars WHERE num_doc = " & BL_TrataStringParaBD(RsARS!nr_req_ars)
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
    End If
    RsARS.Close
    Set RsARS = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description & " " & sSql, "Integracao_Facturacao", "IF_InsereReqARS"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' QUERY PARA RETORNAR MAPEAMENTOS DE ANALISES

' -----------------------------------------------------------------------------------------------

Public Function IF_RetornaMapeamentoAna(ByVal cod_ana As String, ByVal cod_efr As String, ByRef cod_ana_gh As String, ByRef flg_conta_membros As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    
    
    'TENTA ENCONTRAR A AN�LISE POR ENTIDADE
    sSql = "SELECT cod_ana_gh, flg_regra, cod_ana_regra, factura_membros, centro_custo,qtd, flg_conta_membros "
    sSql = sSql & " FROM sl_ana_facturacao WHERE cod_ana =" & BL_TrataStringParaBD(cod_ana)
    sSql = sSql & " AND COD_EFR = " & BL_HandleNull(cod_efr, mediComboValorNull)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    RsFact.CursorLocation = adUseServer
    RsFact.CursorType = adOpenStatic
    RsFact.Open sSql, gConexao
    
    
    'SE NAO ENCONTRAR TENTA GENERICA
    If RsFact.RecordCount = 0 Then
        RsFact.Close
        sSql = "SELECT cod_ana_gh, flg_regra, cod_ana_regra, factura_membros, centro_custo,qtd, flg_conta_membros "
        sSql = sSql & " FROM sl_ana_facturacao WHERE cod_ana =" & BL_TrataStringParaBD(cod_ana)
        sSql = sSql & " AND COD_EFR IS NULL "
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        RsFact.Open sSql, gConexao
    End If
    If RsFact.RecordCount = 0 Then
        IF_RetornaMapeamentoAna = False
    Else
        cod_ana_gh = BL_HandleNull(RsFact!cod_ana_gh, "")
        flg_conta_membros = BL_HandleNull(RsFact!flg_conta_membros, "0")
        IF_RetornaMapeamentoAna = True
    End If
Exit Function
TrataErro:
    IF_RetornaMapeamentoAna = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaMapeamentoAna"
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' DADA UMA REQUISICAO DA ARS VERIFICA REGRAS

' -----------------------------------------------------------------------------------------------

Public Function IF_ValidaReqArs(nr_req_ars As String, mostraAviso As Boolean) As Boolean
    
    'verifica tamanho do numero da credencial
    If Len(nr_req_ars) <> 19 Then
        'Glintt-HS-3080 PARA CREDENCIAIS ENTRE 10 E 18 CARACTERES DEIXA ENVIAR SE UTILIZADOR PRETENDER
        If Len(nr_req_ars) >= 10 And Len(nr_req_ars) <= 18 And mostraAviso = True Then
            gMsgMsg = "Requisi��o ARS/ADSE tem apenas " & Len(nr_req_ars) & " caracteres. Pretende enviar?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp <> vbYes Then
                IF_ValidaReqArs = False
                Exit Function
            Else
                IF_ValidaReqArs = True
                Exit Function
            End If
        Else
            IF_ValidaReqArs = False
            Exit Function
        End If
    End If
    
    'verifica Check digit � diferente de um n�mero ou de �X�
    If Mid(nr_req_ars, 19, 1) <> IF_CalculaCheckDigit(Mid(nr_req_ars, 1, 18)) Then
        ' Glintt-HS-3080 SE FALHAR CHECKDIGIT ALERTA UTILIZADOR MAS PERMITE ENVIAR
        If mostraAviso = True Then
            gMsgMsg = "Requisi��o com check digit errado. Pretende enviar?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp <> vbYes Then
                IF_ValidaReqArs = False
                Exit Function
            Else
                IF_ValidaReqArs = True
                Exit Function
            End If
        Else
            IF_ValidaReqArs = False
            Exit Function
        End If
    End If
    
    'verifica se primeiros 18 caracteres sao numericos
    If Not IsNumeric(Mid(nr_req_ars, 1, 18)) Then
        IF_ValidaReqArs = False
        Exit Function
    End If
    
    'verifica 1� d�gito da credencial n�o est� entre entre 1 e 5
    If Mid(nr_req_ars, 1, 1) < 1 Or Mid(nr_req_ars, 1, 1) > 5 Then
        IF_ValidaReqArs = False
        Exit Function
    End If
    
    'verifica D�gitos nas posi��es 2 e 3 diferentes de �04� (c�digo para pedidos de MCDT)
'    If Mid(nr_req_ars, 2, 2) <> "04" Then
'        IF_ValidaReqArs = False
'        Exit Function
'    End If
    

    IF_ValidaReqArs = True
Exit Function
TrataErro:
    IF_ValidaReqArs = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_ValidaReqArs"
    Exit Function
    Resume Next
End Function

Private Function IF_CalculaCheckDigit(str As String) As String

    Dim c As Integer
   
    c = (12 - IF_CheckSum(str)) Mod 11
    IF_CalculaCheckDigit = IIf(c = 10, "X", c)
    
End Function

Private Function IF_CheckSum(ByVal str As String) As Integer

    Dim check As Long
    Dim i As Integer
    Dim c As String
    
    check = 0
    For i = 1 To Len(str)
        c = CInt(Mid(str, i, 1))
            
        check = 2 * (check + c)
        
    Next i
    IF_CheckSum = check Mod 11

End Function

Public Sub IF_SetContext(ByVal wEmpresaID As String)
    Dim ADOCmd As ADODB.Command
    
    Set ADOCmd = New ADODB.Command
    With ADOCmd
        .CommandText = "CONTEXT_PACKAGE.SET_CONTEXT"
        .CommandType = adCmdStoredProc
        .ActiveConnection = gConexaoSecundaria
        .Parameters.Append .CreateParameter("pEMPRESA_ID", adVarChar, adParamInput, 8, wEmpresaID)
        .Execute
    End With

    Set ADOCmd = Nothing
End Sub

Public Function IF_RetornaNSeqProg() As Long
    Dim rs_aux As New ADODB.recordset
    Dim sSql As String
    IF_RetornaNSeqProg = mediComboValorNull
    sSql = "SELECT SEQ_MOVI_FACT.NEXTVAL N_SEQ_PROG FROM DUAL"
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    If rs_aux.RecordCount = 1 Then
        IF_RetornaNSeqProg = BL_HandleNull(rs_aux!n_seq_prog, mediComboValorNull)
    Else
        IF_RetornaNSeqProg = mediComboValorNull
        Exit Function
    End If
    rs_aux.Close
    Set rs_aux = Nothing
End Function


Public Function IF_InsereAnaliseADSE(n_seq_prog As Long, p1 As String, cod_medico As String, cod_proven As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    If Len(p1) < 15 Then
        p1 = ""
    End If
    sSql = "INSERT INTO sl_movi_Fact_adse(n_Seq_prog, p1,cod_medico, cod_proven, user_cri, dt_cri) VALUES("
    sSql = sSql & n_seq_prog & ", "
    sSql = sSql & BL_TrataStringParaBD(p1) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_medico) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_proven) & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & "sysdate) "
    
    BG_ExecutaQuery_ADO sSql
    IF_InsereAnaliseADSE = True
Exit Function
TrataErro:
    IF_InsereAnaliseADSE = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_InsereAnaliseADSE"
    Exit Function
    Resume Next
End Function

Public Function IF_RetornaIVA(empresa_id As String, cod_grupo As String) As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rs_aux As New ADODB.recordset
    
    sSql = "select perc_iva from fa_grup_rubr_iva, fa_iva where fa_grup_rubr_iva.cod_iva = fa_iva.cod_iva "
    sSql = sSql & " AND empresa_id = " & BL_TrataStringParaBD(empresa_id) & " AND cod_grupo = " & cod_grupo
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs_aux.Open sSql, gConexaoSecundaria, adOpenStatic
    If rs_aux.RecordCount >= 1 Then
        IF_RetornaIVA = BL_HandleNull(rs_aux!perc_iva, 0)
    End If
    rs_aux.Close
Exit Function
TrataErro:
    IF_RetornaIVA = 0
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaIVA"
    Exit Function
    Resume Next
End Function

Public Function IF_InsereRespDadosFact(cod_efr As String) As Boolean
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    On Error GoTo TrataErro
    IF_InsereRespDadosFact = False
    
    sSql = "SELECT * FROM sl_efr_paramet WHERE cod_efr = " & cod_efr
    Set RsEFR = New ADODB.recordset
    RsEFR.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao, adOpenStatic
    If RsEFR.RecordCount >= 1 Then
        If BL_HandleNull(RsEFR!flg_ins_dados_fact, mediComboValorNull) = mediSim Then
            IF_InsereRespDadosFact = True
        End If
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    IF_InsereRespDadosFact = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_InsereRespDadosFact"
    Exit Function
    Resume Next
End Function

Public Function IF_AnaliseMapeada(Entidade As String, codAna As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAnaFact As New ADODB.recordset
    
    If Entidade = "" Or codAna = "" Then Exit Function
    sSql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE cod_efr = " & Entidade & " AND  cod_ana = " & BL_TrataStringParaBD(codAna)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnaFact.CursorLocation = adUseServer
    rsAnaFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaFact.Open sSql, gConexao
    If rsAnaFact.RecordCount <= 0 Then
        rsAnaFact.Close
        sSql = "SELECT cod_ana_gh, flg_conta_membros FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(codAna)
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnaFact.CursorLocation = adUseServer
        rsAnaFact.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaFact.Open sSql, gConexao
        If rsAnaFact.RecordCount > 0 Then
            IF_AnaliseMapeada = True
        End If
    Else
        IF_AnaliseMapeada = True
    End If
    rsAnaFact.Close
    Set rsAnaFact = Nothing
Exit Function
TrataErro:
    IF_AnaliseMapeada = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_AnaliseMapeada"
    Exit Function
    Resume Next
End Function

Public Function IR_RetornaDataInternamento(n_epis As String) As String
    Dim sSql As String
    Dim iBD_Aberta As Integer
    Dim rsInt As New ADODB.recordset
    
    iBD_Aberta = BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle)
    If iBD_Aberta = 0 Then
       Exit Function
    End If
    
    If UCase(HIS.nome) = UCase("SONHO") Then
        sSql = "SELECT dta_internamento dt_int FROM sgd.int_admissoes WHERE int_episodio = " & BL_TrataStringParaBD(n_epis)
    ElseIf UCase(HIS.nome) = UCase("GH") Then
        sSql = "SELECT  dt_int From sd_ints WHERE n_int = " & BL_TrataStringParaBD(n_epis)
    End If
    
    rsInt.CursorLocation = adUseServer
    rsInt.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsInt.Open sSql, gConexaoSecundaria
    If rsInt.RecordCount = 1 Then
        IR_RetornaDataInternamento = BL_HandleNull(rsInt!dt_int, "")
    End If
    rsInt.Close
    Set rsInt = Nothing
    
Exit Function
TrataErro:
    IR_RetornaDataInternamento = ""
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IR_RetornaDataInternamento"
    Exit Function
    Resume Next
End Function

Private Function IF_RetornaNumMarcadores(cod_ana As String, n_req As String) As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsMarca As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_realiza WHERE n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_ana)
    sSql = sSql & " AND cod_ana_s IN (SELECT x2.cod_ana_s FROM sl_ana_s WHERE flg_marcador = 1 )"
    rsMarca.CursorLocation = adUseServer
    rsMarca.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMarca.Open sSql, gConexaoSecundaria
    If rsMarca.RecordCount >= 0 Then
        IF_RetornaNumMarcadores = rsMarca.RecordCount
    End If
    rsMarca.Close
    Set rsMarca = Nothing
Exit Function
TrataErro:
    IF_RetornaNumMarcadores = 0
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Integracao_Facturacao", "IF_RetornaNumMarcadores"
    Exit Function
    Resume Next
End Function

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormDeclaracaoPres 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormDeclaracaoPres"
   ClientHeight    =   6885
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11145
   Icon            =   "FormDeclaracaoPres.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6885
   ScaleWidth      =   11145
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcDescrPostal 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   3360
      Locked          =   -1  'True
      TabIndex        =   32
      Top             =   7560
      Width           =   975
   End
   Begin VB.TextBox EcCodPostal 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   31
      Top             =   7560
      Width           =   975
   End
   Begin VB.TextBox EcMorada 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      Locked          =   -1  'True
      TabIndex        =   30
      Top             =   7680
      Width           =   975
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   10935
      Begin VB.CommandButton BtPesquisaUtente 
         Height          =   315
         Left            =   4920
         Picture         =   "FormDeclaracaoPres.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcCartaoUte 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   6840
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   600
         Width           =   1815
      End
      Begin VB.ComboBox EcDescrTipoUtente 
         Height          =   315
         Left            =   840
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcProcHosp2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   8640
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   240
         Width           =   2055
      End
      Begin VB.TextBox EcDataNasc 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   9720
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   840
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   600
         Width           =   4095
      End
      Begin VB.TextBox EcProcHosp1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   6840
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1800
         TabIndex        =   1
         Top             =   240
         Width           =   3135
      End
      Begin VB.Label LbCartaoUte 
         AutoSize        =   -1  'True
         Caption         =   "&Cart�o Utente"
         Height          =   195
         Left            =   5760
         TabIndex        =   12
         Top             =   600
         Width           =   990
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data &Nasc."
         Height          =   195
         Left            =   8760
         TabIndex        =   11
         ToolTipText     =   "Data Nascimento"
         Top             =   600
         Width           =   810
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "&Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   600
         Width           =   420
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Proc. &Hosp."
         Height          =   195
         Left            =   5760
         TabIndex        =   9
         ToolTipText     =   "Processos Hospitalares"
         Top             =   240
         Width           =   840
      End
      Begin VB.Label LbUtente 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   480
      End
   End
   Begin VB.CommandButton BtImprimir 
      Height          =   495
      Left            =   10320
      Picture         =   "FormDeclaracaoPres.frx":0396
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "Imprimir"
      Top             =   6240
      Width           =   735
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   120
      TabIndex        =   24
      Top             =   1440
      Width           =   10335
      Begin VB.ComboBox EcTamanho 
         Height          =   315
         Left            =   3240
         TabIndex        =   26
         Top             =   180
         Width           =   855
      End
      Begin VB.ComboBox EcFonte 
         Height          =   315
         ItemData        =   "FormDeclaracaoPres.frx":1060
         Left            =   120
         List            =   "FormDeclaracaoPres.frx":1062
         Sorted          =   -1  'True
         TabIndex        =   25
         Top             =   180
         Width           =   3015
      End
      Begin MSComctlLib.Toolbar ToolbarFormatacao 
         Height          =   390
         Left            =   4200
         TabIndex        =   27
         Top             =   120
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         ImageList       =   "ImageListFormat"
         DisabledImageList=   "ImageListFormat"
         HotImageList    =   "ImageListFormat"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   8
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "bold"
               ImageIndex      =   4
               Style           =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "italic"
               ImageIndex      =   5
               Style           =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "underline"
               ImageIndex      =   6
               Style           =   1
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "left"
               ImageIndex      =   2
               Style           =   2
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "center"
               ImageIndex      =   1
               Style           =   2
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "right"
               ImageIndex      =   3
               Style           =   2
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
         EndProperty
      End
   End
   Begin VB.TextBox EcHrAct 
      Height          =   285
      Left            =   5040
      Locked          =   -1  'True
      TabIndex        =   23
      Top             =   8160
      Width           =   735
   End
   Begin VB.TextBox EcHrCri 
      Height          =   285
      Left            =   5880
      Locked          =   -1  'True
      TabIndex        =   22
      Top             =   8160
      Width           =   735
   End
   Begin VB.TextBox EcDtAct 
      Height          =   285
      Left            =   4320
      Locked          =   -1  'True
      TabIndex        =   21
      Top             =   8040
      Width           =   735
   End
   Begin VB.TextBox EcSeqUtente 
      Height          =   375
      Left            =   7320
      Locked          =   -1  'True
      TabIndex        =   20
      Top             =   8160
      Width           =   255
   End
   Begin VB.TextBox EcDtCri 
      Height          =   285
      Left            =   3600
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   8040
      Width           =   735
   End
   Begin VB.TextBox EcUserAct 
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   18
      Top             =   8160
      Width           =   735
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   960
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   8040
      Width           =   735
   End
   Begin VB.TextBox EcSeqDeclaracao 
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcSeqRelat 
      Height          =   375
      Left            =   480
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   8160
      Width           =   255
   End
   Begin VB.CommandButton BtImportarRelat 
      BackColor       =   &H00E0E0E0&
      Height          =   375
      Left            =   10680
      Picture         =   "FormDeclaracaoPres.frx":1064
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Importar Relat�rio"
      Top             =   1560
      Width           =   375
   End
   Begin RichTextLib.RichTextBox EcRelatorio 
      Height          =   4095
      Left            =   120
      TabIndex        =   13
      Top             =   2040
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   7223
      _Version        =   393217
      Enabled         =   -1  'True
      Appearance      =   0
      TextRTF         =   $"FormDeclaracaoPres.frx":17CE
   End
   Begin MSComctlLib.ImageList ImageListFormat 
      Left            =   0
      Top             =   7560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDeclaracaoPres.frx":1859
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDeclaracaoPres.frx":80BB
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDeclaracaoPres.frx":E91D
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDeclaracaoPres.frx":1517F
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDeclaracaoPres.frx":1B9E1
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDeclaracaoPres.frx":22243
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormDeclaracaoPres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim Flg_PesqUtente As Boolean

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind As Integer
Dim Max  As Integer

'NELSONPSILVA Glintt-HS-18011 09.02.2018
Dim tabela_aux As String
'

Dim Objecto As Control
Sub FuncaoInserir()
    Dim iRes As Integer
    If EcSeqUtente = "" Then
        Exit Sub
    End If
    
    Me.SetFocus
    BL_InicioProcessamento Me, "A inserir registo."
    EcUserCri = gCodUtilizador
    EcDtCri = Bg_DaData_ADO
    EcHrCri = Bg_DaHora_ADO
    
    BD_Insert
    BL_FimProcessamento Me

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    EcSeqDeclaracao = BG_DaMAX(NomeTabela, ChaveBD) + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    EcSeqDeclaracao.Locked = True
End Sub
Sub FuncaoModificar()
    Dim iRes As Integer
    If EcSeqUtente = "" Or EcSeqDeclaracao = "" Then
        Exit Sub
    End If
    
    BL_InicioProcessamento Me, "A modificar registo."
    EcDtAct = Bg_DaData_ADO
    EcUserAct = gCodUtilizador
    EcHrAct = Bg_DaHora_ADO
    BD_Update
    BL_FimProcessamento Me

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

        
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY " & ChaveBD & " DESC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    
    rs.Open CriterioTabela, gConexao
    
    If rs.EOF Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
        BL_Toolbar_BotaoEstado "Imprimir", "Activo"
        
        BL_FimProcessamento Me
    End If
    
    Set gFormActivo = Me

End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.EOF Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        PreencheUtente
        EcSeqDeclaracao.Enabled = False
        'BL_ColocaTextoCombo "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente
    End If

End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
        BL_Toolbar_BotaoEstado "Inserir", "Activo"
        BL_Toolbar_BotaoEstado "Procurar", "Activo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
        BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub
Private Sub BtImportarRelat_Click()
    Dim sSql As String
    Dim rsRelat As New ADODB.recordset
    EcSeqRelat = ""
    If EcSeqUtente = "" Then
        Exit Sub
    End If
    FormPesquisaRapida.InitPesquisaRapida "sl_cod_relatorio", _
                        "descr_relatorio", "seq_relatorio", _
                         EcSeqRelat
    sSql = "SELECT * FROM sl_cod_relatorio where seq_relatorio =" & BL_HandleNull(EcSeqRelat, -1)
    rsRelat.CursorLocation = adUseServer
    rsRelat.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRelat.Open sSql, gConexao
    If rsRelat.RecordCount = 1 Then
        
        EcRelatorio.TextRTF = BL_HandleNull(rsRelat!relatorio, "")
        EcRelatorio.TextRTF = Replace(EcRelatorio.TextRTF, "@NOMEUTENTE", EcNome)
        EcRelatorio.TextRTF = Replace(EcRelatorio.TextRTF, "@CARTAOUTENTE", EcCartaoUte)
        EcRelatorio.TextRTF = Replace(EcRelatorio.TextRTF, "@DATANASC", EcDataNasc)
        EcRelatorio.TextRTF = Replace(EcRelatorio.TextRTF, "@UTENTE", EcDescrTipoUtente & "/" & EcUtente)
        EcRelatorio.TextRTF = Replace(EcRelatorio.TextRTF, "@DATAACTUAL", Bg_DaData_ADO)
        EcRelatorio.TextRTF = Replace(EcRelatorio.TextRTF, "@NOMEUTIL", gNomeUtilizador)
        
    Else
        EcRelatorio.TextRTF = ""
    End If
    rsRelat.Close
    Set rsRelat = Nothing
End Sub


Sub FuncaoRemover()

End Sub

Sub BD_Delete()

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    EcRelatorio = ""
    EcRelatorio.Enabled = False
    EcDescrTipoUtente.ListIndex = mediComboValorNull
    EcUtente = ""
    EcCartaoUte = ""
    EcDataNasc = ""
    EcProcHosp1 = ""
    EcProcHosp2 = ""
    EcNome = ""
    EcSeqDeclaracao.Enabled = True
    EcSeqDeclaracao.Locked = False
    
End Sub

Sub FuncaoImprimir()
    
    Dim sql As String
    Dim continua As Boolean
    Dim Ass As String
    Dim Titul As String
    Dim codPostal As String
    Dim msg As String
    Dim NotaPS As String
    Dim RegId As ADODB.recordset
    
    If EcSeqDeclaracao.Locked = False Then
        Exit Sub
    End If
        
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Declara��o") = True Then Exit Sub
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("Declara��o", "Declara��o", crptToPrinter)
    Else
        continua = BL_IniciaReport("Declara��o", "Declara��o", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT sl_identif.*, sl_declaracao.relatorio, sl_declaracao.dt_act, sl_declaracao.hr_act, sl_declaracao.dt_cri, sl_declaracao.hr_cri"
    Report.SQLQuery = Report.SQLQuery & " FROM sl_identif, sl_declaracao WHERE sl_identif.seq_utente = sl_declaracao.seq_utente AND seq_declaracao = " & EcSeqDeclaracao
       
    Call BL_ExecutaReport
    Report.PageShow Report.ReportLatestPage
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    Max = -1
    ReDim FOPropertiesTemp(0)
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
        
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me
    
    Flg_PesqUtente = False

End Sub

Sub EventoActivate()
    
    On Error GoTo Trata_Erro
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    
    If gDUtente.seq_utente <> "" And Flg_PesqUtente = True Then
        FuncaoLimpar
        EcSeqUtente = CLng(gDUtente.seq_utente)
        PreencheUtente
        Flg_PesqUtente = False
    End If
    
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

    Exit Sub
Trata_Erro:
    BG_LogFile_Erros "FormDeclaracaoPres: EventoActivate - " & Err.Description
    Resume Next
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormDeclaracaoPres = Nothing
    
End Sub

Sub DefTipoCampos()
    EcRelatorio = ""
    EcRelatorio.Enabled = False
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDtCri, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDtAct, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheValoresDefeito()
    Dim i As Integer
     For i = 0 To Screen.FontCount - 1
        EcFonte.AddItem Screen.Fonts(i)
    Next
    
    For i = 8 To 12
        EcTamanho.AddItem i
    Next i
    
    For i = 14 To 28 Step 2
        EcTamanho.AddItem i
    Next i
    
    EcTamanho.AddItem 36
    EcTamanho.AddItem 48
    EcTamanho.AddItem 72
    EcFonte = "Times New Roman"
    EcTamanho = "10"
    
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", EcDescrTipoUtente

End Sub




Private Sub BtImprimir_Click()
    If EcSeqDeclaracao <> "" And EcSeqDeclaracao.Locked = True Then
        FuncaoModificar
    Else
        FuncaoInserir
    End If
    gImprimirDestino = 1
    FuncaoImprimir
End Sub

Private Sub BtPesquisaUtente_Click()
    
    LimpaCampos
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
        
    FormPesquisaUtentes.Show
    Flg_PesqUtente = True


End Sub

Private Sub EcDataNasc_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataNasc)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub


Private Sub EcDescrTipoUtente_Validate(Cancel As Boolean)
    EcUtente_Validate False
End Sub

Private Sub EcNome_LostFocus()

    EcNome.Text = StrConv(EcNome.Text, vbProperCase)
    
End Sub



Private Sub Ecrelatorio_GotFocus()
    Dim Control As Control
    
    Set CampoActivo = Me.ActiveControl
    
    On Error Resume Next
    
    For Each Control In Controls
        Control.TabStop = False
    Next Control
    
    Set Objecto = Me.ActiveControl

End Sub

Private Sub EcUtente_LostFocus()

    EcUtente.Text = UCase(EcUtente.Text)
    
End Sub

Private Sub EcUtente_Validate(Cancel As Boolean)
    Dim sSql As String
    Dim rsUte As New ADODB.recordset
    If EcUtente <> "" And EcDescrTipoUtente.ListIndex <> mediComboValorNull Then
        sSql = "SELECT n_cartao_ute, nome_ute, dt_nasc_ute, n_proc_1, n_proc_2, utente,t_utente, seq_utente," & tabela_aux & ".cod_postal_ute, sl_cod_postal.descr_postal, "
        sSql = sSql & " descr_mor_ute FROM " & tabela_aux & " left outer join sl_cod_postal on " & tabela_aux & ".cod_postal_ute = sl_cod_postal.cod_postal where "
         sSql = sSql & " t_utente = " & BL_TrataStringParaBD(EcDescrTipoUtente) & " AND utente = " & BL_TrataStringParaBD(EcUtente)
        PreencheUTenteBD sSql
    End If
End Sub

Private Sub Form_Activate()
    
    EventoActivate
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub Inicializacoes()
    Me.caption = "Emiss�o de Declara��es"
    Me.left = 50
    Me.top = 50
    Me.Width = 11235
    Me.Height = 7305
    
    NomeTabela = "sl_declaracao"
    Set CampoDeFocus = EcDescrTipoUtente
    
    NumCampos = 13
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_Declaracao"
    CamposBD(1) = "seq_utente"
    CamposBD(2) = "relatorio"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "hr_Cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "hr_act"
    CamposBD(9) = "nome_ute"
    CamposBD(10) = "descr_mor"
    CamposBD(11) = "cod_postal"
    CamposBD(12) = "descr_postal"
    
    ' Campos do Ecr�
    
    Set CamposEc(0) = EcSeqDeclaracao
    Set CamposEc(1) = EcSeqUtente
    Set CamposEc(2) = EcRelatorio
    Set CamposEc(3) = EcUserCri
    Set CamposEc(4) = EcDtCri
    Set CamposEc(5) = EcHrCri
    Set CamposEc(6) = EcUserAct
    Set CamposEc(7) = EcDtAct
    Set CamposEc(8) = EcHrAct
    Set CamposEc(9) = EcNome
    Set CamposEc(10) = EcMorada
    Set CamposEc(11) = EcCodPostal
    Set CamposEc(12) = EcDescrPostal
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_Declaracao"
    Set ChaveEc = EcSeqDeclaracao
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
     If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
     Else
        tabela_aux = "sl_identif"
     End If
        
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub



Private Sub ToolbarFormatacao_ButtonClick(ByVal Button As MSComctlLib.Button)
        On Error GoTo TrataErro

    If Me.ActiveControl.Name <> "EcRelatorio" Then
        Exit Sub
    Else
        Set Objecto = Me.ActiveControl
    End If
    
    Select Case Button.Key
        Case "bold"
            If Button.value = tbrUnpressed Then
                If Not IsNull(Objecto.SelBold) Then Objecto.SelBold = False
            Else
                Objecto.SelBold = True
            End If
        Case "italic"
            If Button.value = tbrUnpressed Then
                If Not IsNull(Objecto.SelItalic) Then Objecto.SelItalic = False
            Else
                Objecto.SelItalic = True
            End If
        Case "underline"
            If Button.value = tbrUnpressed Then
                If Not IsNull(Objecto.SelUnderline) Then Objecto.SelUnderline = False
            Else
                Objecto.SelUnderline = True
            End If
        Case "left"
            If Button.value <> tbrUnpressed Then
                Objecto.SelAlignment = rtfLeft
            End If
        Case "center"
            If Button.value <> tbrUnpressed Then
                Objecto.SelAlignment = rtfCenter
            End If
        Case "right"
            If Button.value <> tbrUnpressed Then
                Objecto.SelAlignment = rtfRight
            End If
        Case Else
            'Nada
    End Select
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub





Sub ecrelatorio_SelChange()
    
    
    
    If Not IsNull(EcRelatorio.SelFontName) Then
        EcFonte.Text = EcRelatorio.SelFontName
    End If
    
    If Not IsNull(EcRelatorio.SelFontSize) Then
        EcTamanho.Text = Int(EcRelatorio.SelFontSize)
    End If
    
    If EcRelatorio.SelBold Then
        ToolbarFormatacao.Buttons.item("bold").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("bold").value = tbrUnpressed
    End If
    
    If EcRelatorio.SelItalic Then
        ToolbarFormatacao.Buttons.item("italic").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("italic").value = tbrUnpressed
    End If
    
    If EcRelatorio.SelUnderline Then
        ToolbarFormatacao.Buttons.item("underline").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("underline").value = tbrUnpressed
    End If
    
    If IsNull(EcRelatorio.SelAlignment) Then
        ToolbarFormatacao.Buttons.item("left").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("center").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("right").value = tbrUnpressed
    ElseIf EcRelatorio.SelAlignment = rtfLeft Then
        ToolbarFormatacao.Buttons.item("left").value = tbrPressed
    ElseIf EcRelatorio.SelAlignment = rtfCenter Then
        ToolbarFormatacao.Buttons.item("center").value = tbrPressed
    ElseIf EcRelatorio.SelAlignment = rtfRight Then
        ToolbarFormatacao.Buttons.item("right").value = tbrPressed
    End If
End Sub
Private Sub EcFonte_Change()
    On Error Resume Next
        
    Objecto.SelFontName = EcFonte.Text
End Sub

Private Sub EcFonte_Click()
    On Error Resume Next
        
    Objecto.SelFontName = EcFonte.Text
    EcRelatorio.SetFocus
End Sub



Private Sub EcTamanho_Change()
      On Error Resume Next
    
    
    Objecto.SelFontSize = EcTamanho.Text
End Sub

Private Sub EcTamanho_Click()
      On Error Resume Next
    
    
    Objecto.SelFontSize = EcTamanho.Text
    EcRelatorio.SetFocus
End Sub
Private Sub PreencheUtente()
    Dim sSql As String
    If EcSeqUtente <> "" Then
        sSql = "SELECT n_cartao_ute, nome_ute, dt_nasc_ute, n_proc_1, n_proc_2, utente,t_utente, seq_utente, " & tabela_aux & ".cod_postal_ute, sl_cod_postal.descr_postal, descr_mor_ute FROM " & tabela_aux & " left outer join sl_cod_postal on " & tabela_aux & ".cod_postal_ute = sl_cod_postal.cod_postal where seQ_utente = " & EcSeqUtente
        PreencheUTenteBD sSql
    End If
End Sub

Private Sub PreencheUTenteBD(sSql As String)
    Dim rsUte As New ADODB.recordset
    rsUte.CursorType = adOpenStatic
    rsUte.CursorLocation = adUseServer
    rsUte.LockType = adLockReadOnly
    rsUte.Open sSql, gConexao
    EcRelatorio.Enabled = False
    If rsUte.RecordCount = 1 Then
        EcCartaoUte = BL_HandleNull(rsUte!n_cartao_ute, "")
        EcNome = BL_HandleNull(rsUte!nome_ute, "")
        EcDataNasc = BL_HandleNull(rsUte!dt_nasc_ute, "")
        EcProcHosp1 = BL_HandleNull(rsUte!n_proc_1, "")
        EcProcHosp2 = BL_HandleNull(rsUte!n_proc_2, "")
        EcUtente = BL_HandleNull(rsUte!Utente, "")
        EcDescrTipoUtente = BL_HandleNull(rsUte!t_utente, "")
        EcSeqUtente = BL_HandleNull(rsUte!seq_utente, "")
        EcMorada = BL_HandleNull(rsUte!descr_mor_ute, "")
        EcCodPostal = BL_HandleNull(rsUte!cod_postal_ute, "")
        EcDescrPostal = BL_HandleNull(rsUte!descr_postal, "")
        EcRelatorio.Enabled = True
    Else
        EcCartaoUte = ""
        EcNome = ""
        EcDataNasc = ""
        EcProcHosp1 = ""
        EcProcHosp2 = ""
        EcUtente = ""
        EcDescrTipoUtente.ListIndex = mediComboValorNull
        EcRelatorio.Enabled = False
    End If
    rsUte.Close
    Set rsUte = Nothing

End Sub


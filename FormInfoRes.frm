VERSION 5.00
Begin VB.Form FormInfoRes 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Informa��o de Resultados"
   ClientHeight    =   3645
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6870
   Icon            =   "FormInfoRes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3645
   ScaleWidth      =   6870
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcInfoRes 
      Height          =   3500
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   6615
   End
End
Attribute VB_Name = "FormInfoRes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Inicializacoes()
    Me.Left = 8405
    Me.Top = 2200
    EcInfoRes.Font = "Courier new"
    EcInfoRes.text = ""
    gFormInfoRes_Open = True
End Sub

Private Sub Form_Load()
    Inicializacoes
    BG_StackJanelas_Push Me
    Set gFormActivo = Me
End Sub

Public Sub CarregaInfoRes(hWndLeft As Long, hWndTop As Long, _
                            CodMicro As String, CodGrAntib As String, _
                            SeqRealiza As Long, DescrMicro As String, _
                            FlgImp As String, FlgTSQ As String, TipoRes As String, ColRes As String, aForm As String)

    Dim LstGrupo As String
    Dim val As Variant
    Dim RsRes As ADODB.recordset
    Set RsRes = New ADODB.recordset
    LstGrupo = ""
    Me.Left = hWndLeft
    Me.Top = hWndTop
    
    If TipoRes = gT_Microrganismo And ((ColRes = 3 And aForm = "FormResMicro") Or (ColRes = 15 And aForm = "FormResultadosNovo")) Then
        Me.Height = 6500
        EcInfoRes.Height = 6000
        EcInfoRes.text = DescrMicro & ":" & Space(30 - Len(IIf(FlgImp = "Sim", "{Imprimir}", "{N�o Imprimir}"))) & _
                        IIf(FlgImp = "Sim", "{Imprimir}", "{N�o Imprimir}") & Chr(13) & vbLf & Chr(13) & vbLf
        
        If (Mid(FlgTSQ, 1, 1) = "S") Then
            For Each val In Split(BL_HandleNull(CodGrAntib, ""), ";")
                LstGrupo = LstGrupo & "'" & val & "'" & ","
            Next
            
            If (LstGrupo <> "") Then
                LstGrupo = "(" & Mid(LstGrupo, 1, InStrRev(LstGrupo, ",") - 1) & ")"
                sql = " SELECT DISTINCT sl_antibio.descr_antibio, res_sensib, flg_imp, cod_gr_antibio, ordem" & _
                      " FROM sl_res_tsq, sl_rel_grantib, sl_antibio" & _
                      " WHERE seq_realiza = " & SeqRealiza & _
                      " AND n_res = 1 " & _
                      " AND sl_antibio.cod_antibio = sl_res_tsq.cod_antib" & _
                      " AND cod_micro = " & BL_TrataStringParaBD(CodMicro) & _
                      " AND sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio" & _
                      " AND cod_gr_antibio IN " & LstGrupo & _
                      " ORDER BY cod_gr_antibio, flg_imp desc, ordem "
            Else
    
                sql = " SELECT DISTINCT sl_antibio.descr_antibio, res_sensib, flg_imp" & _
                      " FROM sl_res_tsq, sl_antibio" & _
                      " WHERE seq_realiza = " & SeqRealiza & _
                      " AND n_res = 1 " & _
                      " AND sl_antibio.cod_antibio = sl_res_tsq.cod_antib" & _
                      " AND cod_micro = " & BL_TrataStringParaBD(CodMicro) & _
                      " ORDER BY flg_imp , sl_antibio.descr_antibio"
            End If
            RsRes.Open sql, gConexao, adOpenStatic, adLockReadOnly
            
            While (Not RsRes.EOF)
                EcInfoRes.text = EcInfoRes.text & vbTab & RsRes!descr_antibio & Space(30 - Len(RsRes!descr_antibio)) & _
                                 RsRes!res_sensib & Space(2) & IIf(RsRes!flg_imp = "S", "{Imprimir}", "{N�o Imprimir}") & Chr(13) & vbLf
                RsRes.MoveNext
            Wend
            RsRes.Close
            Set RsRes = Nothing
        End If
    ElseIf TipoRes = gT_Microrganismo And (ColRes = 12 And aForm = "FormResultadosNovo") Then
            sql = "SELECT x3.descr_prova, x2.descr_frase FROM sl_res_prova x1, sl_dicionario x2, sl_prova x3  WHERE seq_Realiza = " & SeqRealiza
            sql = sql & " AND x1.cod_prova = x3.cod_prova AND x2.cod_frase = x1.res_prova"
            RsRes.Open sql, gConexao, adOpenStatic, adLockReadOnly
            EcInfoRes.text = ""
            While (Not RsRes.EOF)
                EcInfoRes.text = EcInfoRes.text & RsRes!descr_prova & Space(30 - Len(RsRes!descr_frase)) & RsRes!descr_frase & vbCrLf
                RsRes.MoveNext
            Wend
            RsRes.Close
            Set RsRes = Nothing
            
    ElseIf TipoRes = gT_Frase And ((ColRes = 3 And aForm = "FormResMicro") Or (ColRes = 14 And aForm = "FormResultadosNovo")) Then
        sql = "select descr_frase from sl_res_frase f , sl_dicionario d " & _
                "where f.cod_frase = d.cod_frase and seq_realiza =" & SeqRealiza & " and n_res = 1 order by ord_frase "
        RsRes.Open sql, gConexao, adOpenStatic, adLockReadOnly
        EcInfoRes.text = ""
        While Not RsRes.EOF
            EcInfoRes.text = EcInfoRes.text & RsRes!descr_frase & vbCrLf
            RsRes.MoveNext
        Wend
        RsRes.Close
        Set RsRes = Nothing
    Else
    
        Me.Height = 4000
        EcInfoRes.Height = 3500
        EcInfoRes.text = DescrMicro
    End If
        
End Sub


Private Sub Form_Unload(Cancel As Integer)
    gFormInfoRes_Open = False
    BG_StackJanelas_Pop
    If gF_RESULT_NOVO = mediSim Then
        Set gFormActivo = FormResultadosNovo
    ElseIf gF_RESMICRO = mediSim Then
        Set gFormActivo = FormResMicro
    End If
    EcInfoRes.text = ""
End Sub


Public Sub CarregaInfoConsulta(hWndLeft As Long, hWndTop As Long, hWndWidth As Long, InfoText As String)
    Me.Left = hWndLeft
    Me.Top = hWndTop
    Me.Width = hWndWidth
    EcInfoRes.Width = hWndWidth - 200
    EcInfoRes.text = InfoText
End Sub

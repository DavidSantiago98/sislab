VERSION 5.00
Begin VB.Form FormListagemFrasesPadrao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormListagemFrasesPadrao"
   ClientHeight    =   1980
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   5595
   Icon            =   "FormListagemFrasesPadrao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1980
   ScaleWidth      =   5595
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1935
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      Begin VB.TextBox EcCodGrupo 
         Height          =   285
         Left            =   3480
         TabIndex        =   4
         Top             =   600
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.OptionButton OptTodosGrupos 
         Caption         =   "Todos os Grupos"
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   600
         Width           =   1575
      End
      Begin VB.OptionButton OptPorGrupo 
         Caption         =   "Por Grupo :"
         Height          =   255
         Left            =   480
         TabIndex        =   2
         Top             =   1080
         Width           =   1215
      End
      Begin VB.ComboBox CbGrupo 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1080
         Width           =   3135
      End
      Begin VB.Label Label1 
         Caption         =   "EcCodGrupo"
         Height          =   255
         Left            =   2520
         TabIndex        =   5
         Top             =   600
         Visible         =   0   'False
         Width           =   975
      End
   End
End
Attribute VB_Name = "FormListagemFrasesPadrao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2002
' T�cnico Paulo Costa

'Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset


Dim TemValoresRef As Boolean

Private Sub CbGrupo_Click()

    BL_ColocaComboTexto "sl_gr_ana", "seq_gr_ana", "cod_gr_ana", EcCodGrupo, CbGrupo
    
End Sub

Private Sub CbGrupo_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then
        CbGrupo.ListIndex = -1
        EcCodGrupo = ""
    End If
    
End Sub

Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()

    EventoLoad
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    'DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    Call BL_FechaPreview("Listagem Frases Padr�o")
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    'Unload  da Form
    MDIFormInicio.Tag = ""
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormListarAnalises = Nothing
     
End Sub

Sub Inicializacoes()
        
    Dim p As ADODB.Parameter
    
    Me.left = 540
    Me.top = 450
    Me.Width = 5625
    Me.Height = 2370 ' Normal
    'Me.Height = 2330 ' Campos Extras
    
    OptTodosGrupos.value = True
    CbGrupo.Enabled = False
        
    'ESCAL�ES E ZONAS DE REFER�NCIA
    Me.caption = "Listagem de Frases Padr�o"


    
    
End Sub

Sub PreencheValoresDefeito()

    ' Grupo.
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo, _
                           mediAscComboDesignacao
End Sub

Sub FuncaoLimpar()

    OptTodosGrupos.value = True
    CbGrupo.ListIndex = mediComboValorNull
    
End Sub

Sub FuncaoImprimir()
    Dim sSql As String
    Dim rsFrase As New ADODB.recordset
    Dim continua As Boolean
    
    If BL_PreviewAberto("Listagem Frases Padr�o") = True Then Exit Sub
    
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListagemFrasesPadrao", "Listagem Frases Padr�o", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListagemFrasesPadrao", "Listagem Frases Padr�o", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    sSql = "DELETE FROM SL_CR_FRASES_PADRAO WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    sSql = "SELECT af.cod_perfil, af.cod_ana_S, af.cod_frase, af.ordem, p.descr_perfis, s.descr_ana_s, d.descr_frase FROM sl_ana_Frase af, sl_perfis p, sl_ana_s s, sl_dicionario d "
    sSql = sSql & " WHERE af.cod_perfil is not null and cod_perfil <> '0' AND af.cod_perfil = p.cod_perfis AND af.cod_ana_S = s.cod_ana_s AND af.cod_frase = d.cod_frase "
    If EcCodGrupo <> "" Then
        sSql = sSql & " AND p.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo)
    End If
    sSql = sSql & " UNION SELECT af.cod_perfil, af.cod_ana_S, af.cod_frase, af.ordem,'' descr_perfis, s.descr_ana_s, d.descr_frase FROM sl_ana_Frase af, sl_ana_s s, sl_dicionario d "
    sSql = sSql & " WHERE cod_perfil is null AND af.cod_ana_S = s.cod_ana_s AND af.cod_frase = d.cod_frase "
    If EcCodGrupo <> "" Then
        sSql = sSql & " AND s.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo)
    End If
    
    rsFrase.CursorLocation = adUseServer
    rsFrase.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFrase.Open sSql, gConexao
    If rsFrase.RecordCount >= 1 Then
        While Not rsFrase.EOF
            sSql = "INSERT INTO SL_CR_FRASES_PADRAO(nome_computador, num_sessao, cod_perfil, descr_perfil, cod_ana_s, descr_ana_s, cod_frase, descr_frase"
            sSql = sSql & ",ordem) VALUES( " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsFrase!Cod_Perfil, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsFrase!descr_perfis, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsFrase!cod_ana_s, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsFrase!descr_ana_s, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsFrase!cod_frase, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsFrase!descr_frase, "")) & ","
            sSql = sSql & BL_HandleNull(rsFrase!ordem, 0) & ")"
            BG_ExecutaQuery_ADO sSql
            rsFrase.MoveNext
        Wend
        'Imprime o relat�rio no Crystal Reports
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        
        
        Report.SQLQuery = " SELECT cod_perfil, descr_perfil, cod_ana_s, descr_ana_s, cod_frase, descr_frase, ordem FROM sl_cr_frases_padrao "
        Report.SQLQuery = Report.SQLQuery & " WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
        
        If EcCodGrupo = "" Then
            Report.formulas(0) = "Grupo='Todos'"
        Else
            Report.formulas(0) = "Grupo=" & BL_TrataStringParaBD(CbGrupo)
        End If
        
        Call BL_ExecutaReport
        Report.PageShow Report.ReportLatestPage
    End If
    
End Sub

Private Sub OptPorGrupo_Click()
    
    CbGrupo.Enabled = True

End Sub

Private Sub OptTodosGrupos_Click()
    
    CbGrupo.ListIndex = mediComboValorNull
    CbGrupo.Enabled = False

End Sub




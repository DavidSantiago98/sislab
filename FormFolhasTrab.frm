VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormFolhasTrab 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFolhasTrab"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9570
   Icon            =   "FormFolhasTrab.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   9570
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameReimprime 
      Caption         =   "Reimpressao"
      Height          =   4095
      Left            =   0
      TabIndex        =   36
      Top             =   4560
      Width           =   9495
      Begin VB.CheckBox CkSelTodas 
         Height          =   195
         Left            =   1620
         TabIndex        =   41
         Top             =   480
         Width           =   255
      End
      Begin VB.CommandButton Btimprime 
         Height          =   615
         Left            =   7560
         Picture         =   "FormFolhasTrab.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Imprimir folhas seleccionadas"
         Top             =   3000
         Width           =   735
      End
      Begin VB.CommandButton BtSair 
         Height          =   495
         Left            =   6480
         Picture         =   "FormFolhasTrab.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   39
         ToolTipText     =   "Voltar"
         Top             =   3120
         Width           =   735
      End
      Begin VB.ListBox EcListaFolhasTrab 
         Height          =   2310
         Left            =   1560
         Style           =   1  'Checkbox
         TabIndex        =   38
         Top             =   720
         Width           =   7455
      End
      Begin MSComCtl2.DTPicker EcData 
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145489921
         CurrentDate     =   39876
      End
   End
   Begin VB.CommandButton BtReimprimir 
      Height          =   615
      Left            =   8640
      Picture         =   "FormFolhasTrab.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   240
      Width           =   735
   End
   Begin VB.TextBox EcSeqFolhaTrab 
      Height          =   285
      Left            =   5160
      TabIndex        =   34
      Top             =   4680
      Width           =   1335
   End
   Begin VB.CheckBox OptNovasAnas 
      Caption         =   "Apenas an�lises novas"
      Height          =   195
      Left            =   6000
      TabIndex        =   33
      Top             =   840
      Width           =   2295
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   4080
      TabIndex        =   31
      Top             =   120
      Width           =   1095
   End
   Begin VB.CheckBox CkTodos 
      Caption         =   "Imprime Todos Grupos Trabalho"
      Height          =   255
      Left            =   2880
      TabIndex        =   30
      Top             =   840
      Width           =   2655
   End
   Begin VB.TextBox EcNumFolhaTrab 
      Height          =   285
      Left            =   1800
      TabIndex        =   28
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   1560
      TabIndex        =   27
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CheckBox CkImprimeEtiquetas 
      Caption         =   "Imprime Etiquetas para tubos"
      Height          =   255
      Left            =   360
      TabIndex        =   26
      Top             =   840
      Width           =   2655
   End
   Begin VB.TextBox EcDescrGrTrab 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2880
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   480
      Width           =   4935
   End
   Begin MSComDlg.CommonDialog CmdDlgPrint 
      Left            =   3600
      Top             =   4560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Crystal.CrystalReport FT 
      Left            =   3120
      Top             =   4560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin VB.TextBox EcCodTUte 
      Height          =   285
      Left            =   1560
      TabIndex        =   23
      Top             =   4560
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton BtPesquisaRapidaGrTrab 
      Height          =   375
      Left            =   7830
      Picture         =   "FormFolhasTrab.frx":266A
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Pesquisa R�pida de Grupos de Trabalho"
      Top             =   480
      Width           =   435
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   2775
      Left            =   120
      TabIndex        =   17
      Top             =   1200
      Width           =   9375
      Begin VB.Frame Frame4 
         Height          =   975
         Left            =   0
         TabIndex        =   21
         Top             =   1680
         Width           =   9375
         Begin VB.CommandButton BtPesquisaProveniencia 
            Height          =   375
            Left            =   5070
            Picture         =   "FormFolhasTrab.frx":2BF4
            Style           =   1  'Graphical
            TabIndex        =   13
            ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox EcUtente 
            Height          =   285
            Left            =   6840
            TabIndex        =   15
            Top             =   360
            Width           =   1335
         End
         Begin VB.CommandButton BtPesquisaUtente 
            Height          =   375
            Left            =   8190
            Picture         =   "FormFolhasTrab.frx":317E
            Style           =   1  'Graphical
            TabIndex        =   16
            ToolTipText     =   "Pesquisa R�pida de Utentes"
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox EcCodProveniencia 
            Height          =   285
            Left            =   1200
            TabIndex        =   11
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox EcDescrProveniencia 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   360
            Width           =   3135
         End
         Begin VB.Label Label8 
            Caption         =   "&Tipo de Utentes"
            Height          =   255
            Left            =   5640
            TabIndex        =   14
            Top             =   360
            Width           =   1215
         End
         Begin VB.Label Label9 
            Caption         =   "&Proveni�ncia"
            Height          =   255
            Left            =   120
            TabIndex        =   10
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.Frame Frame2 
         Height          =   1335
         Left            =   0
         TabIndex        =   18
         Top             =   360
         Width           =   9375
         Begin VB.ComboBox CbLocal 
            Height          =   315
            Left            =   4920
            Style           =   2  'Dropdown List
            TabIndex        =   24
            Top             =   840
            Width           =   3615
         End
         Begin VB.ComboBox CbSituacao 
            Height          =   315
            Left            =   4920
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   360
            Width           =   1335
         End
         Begin VB.ComboBox CbUrgencia 
            Height          =   315
            Left            =   7200
            Style           =   2  'Dropdown List
            TabIndex        =   9
            Top             =   360
            Width           =   1335
         End
         Begin VB.TextBox EcDtFim 
            Height          =   285
            Left            =   2160
            TabIndex        =   5
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox EcDtIni 
            Height          =   285
            Left            =   480
            TabIndex        =   4
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label LbLocal 
            Caption         =   "Local"
            Height          =   255
            Left            =   4080
            TabIndex        =   25
            Top             =   840
            Width           =   735
         End
         Begin VB.Line Line2 
            BorderColor     =   &H80000010&
            X1              =   3840
            X2              =   3840
            Y1              =   120
            Y2              =   1320
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000005&
            BorderWidth     =   2
            X1              =   3840
            X2              =   3840
            Y1              =   120
            Y2              =   840
         End
         Begin VB.Label Label6 
            Caption         =   "&Situa��o"
            Height          =   255
            Left            =   4080
            TabIndex        =   6
            Top             =   390
            Width           =   735
         End
         Begin VB.Label Label7 
            Caption         =   "Prio&ridade"
            Height          =   255
            Left            =   6480
            TabIndex        =   8
            Top             =   390
            Width           =   735
         End
         Begin VB.Label Label3 
            Caption         =   "De"
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   390
            Width           =   255
         End
         Begin VB.Label Label2 
            Caption         =   "a"
            Height          =   255
            Index           =   0
            Left            =   1800
            TabIndex        =   19
            Top             =   390
            Width           =   255
         End
      End
   End
   Begin VB.TextBox EcCodGrTrab 
      Height          =   285
      Left            =   1800
      TabIndex        =   1
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   1
      Left            =   3120
      TabIndex        =   32
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "N� Folha Trabalho:"
      Height          =   255
      Left            =   240
      TabIndex        =   29
      Top             =   150
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "cod_t_utente"
      Height          =   255
      Left            =   480
      TabIndex        =   22
      Top             =   4560
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label LbTipo 
      Caption         =   "&Grupo de Trabalho :"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   1575
   End
End
Attribute VB_Name = "FormFolhasTrab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Actualiza��o : 25/01/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public TipoFolha As String 'FolhaTrabalhoAna' ou 'FolhaTrabalhoTubo'

Public rs As ADODB.recordset

Private Type PropriedadeFT
    N_Ft As Long
    descr_gr_trab As String
    Descr_Ana_Aviso As String
    Quebra_Req As Integer
End Type
    
Private Type RegistoFT
    N_Ft As Long
    n_req As Long
    descr_t_sit As String
    t_urg As String
    descr_proven As String
    nome_ute As String
    sexo_ute As String
    idade As String
    t_utente As String
    abrev(1 To 13) As String * 10
    codAnalise(1 To 13) As String * 8
    ResAnt(1 To 13) As String
    ObsAna(1 To 13) As String
    Aviso As String * 1
    Diagn_P As String
    Diagn_S As String
    Tera_Med As String
    DtChega As String
    DtPretendida As String
    DtEntrega As String
    DtNascUte As String
    inf_complementar As String
    Utente As String
    episodio As String
End Type

Private Type RegVertical
    N_Ft As Long
    abrev(1 To 13) As String * 10
    codAnalise(1 To 13) As String * 8
    SeqUtente(1 To 13) As String
End Type

Private Type DefConfigLivre
    flg_situacao As String * 1
    flg_urgencia As String * 1
    flg_nome As String * 1
    flg_idade As String * 1
    flg_sexo As String * 1
    flg_inf_cli As String * 1
    flg_antibioticos As String
    Num_Ult_Res As Integer
    TextoLivre As String
    flg_membros As Integer
End Type

Private CabFt As PropriedadeFT
Private RegFt As RegistoFT
Private FtVertical As RegVertical
Private ConfigLivre As DefConfigLivre

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

'Estruturas utilizadas para gerar as etiquetas
Private Type Tipo_Tubo
    descR_tubo As String
    cod_tubo As String
    n_req As String
End Type

Dim Tubos() As Tipo_Tubo

Private Type Reimpressao
    seq_impr As String
    cod_gr_trab As String
    descr_gr_trab As String
End Type

Dim EstrutReImpr() As Reimpressao
Dim totalImpr As Long



Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub Btimprime_Click()
    Dim i As Integer
    FrameReimprime.Visible = False
    
    For i = 0 To EcListaFolhasTrab.ListCount - 1
        If EcListaFolhasTrab.Selected(i) = True Then
            EcNumFolhaTrab = EstrutReImpr(i + 1).seq_impr
            EcCodGrTrab = EstrutReImpr(i + 1).cod_gr_trab
            Imprime_FolhaTrabalho True
        End If
    Next
End Sub

Private Sub BtPesquisaProveniencia_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub BtPesquisaRapidaGrTrab_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    If TipoFolha = "FolhaTrabalhoTubo" Then
        ChavesPesq(1) = "cod_gr_ana"
        CamposEcran(1) = "cod_gr_ana"
        Tamanhos(1) = 2000
        ChavesPesq(2) = "descr_gr_ana"
        CamposEcran(2) = "descr_gr_ana"
        Tamanhos(2) = 3000
    Else
        ChavesPesq(1) = "cod_gr_trab"
        CamposEcran(1) = "cod_gr_trab"
        Tamanhos(1) = 2000
        ChavesPesq(2) = "descr_gr_trab"
        CamposEcran(2) = "descr_gr_trab"
        Tamanhos(2) = 3000
    End If
    
    
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    If TipoFolha = "FolhaTrabalhoTubo" Then
        ClausulaFrom = "sl_gr_ana"
        CampoPesquisa1 = "descr_gr_ana"
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " order by descr_gr_ana ", " Pesquisar Grupos de An�lises")
        mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    Else
        ClausulaFrom = "sl_gr_trab"
        CampoPesquisa1 = "descr_gr_trab"
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " order by descr_gr_trab ", " Pesquisar Grupos de Trabalho")
        mensagem = "N�o foi encontrada nenhum Grupo de Trabalho."
    End If
    
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrTrab.Text = resultados(1)
            EcDescrGrTrab.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaUtente_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_t_utente"
    CamposEcran(1) = "cod_t_utente"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_t_utente"
    CamposEcran(2) = "descr_t_utente"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_t_utente"
    CampoPesquisa1 = "descr_t_utente"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tipos de Utente")
    
    mensagem = "N�o foi encontrada nenhum Tipo de Utente."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTUte.Text = resultados(1)
            EcUtente.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtReimprimir_Click()
    FrameReimprime.Visible = True
    FrameReimprime.top = 0
    FrameReimprime.left = 0
    EcListaFolhasTrab.Clear

End Sub

Private Sub BtSair_Click()
    FrameReimprime.Visible = False
End Sub

Private Sub CbSituacao_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbSituacao.ListIndex = -1
    
End Sub

Private Sub CbLocal_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbLocal.ListIndex = -1
    
End Sub
Private Sub CbUrgencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub CkSelTodas_Click()
    Dim i As Long
    If CkSelTodas.value = vbChecked Then
        For i = 0 To EcListaFolhasTrab.ListCount - 1
            EcListaFolhasTrab.Selected(i) = True
        Next
    Else
        For i = 0 To EcListaFolhasTrab.ListCount - 1
            EcListaFolhasTrab.Selected(i) = False
        Next
    End If
End Sub

Private Sub EcCodGrTrab_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodGrTrab_Validate(Cancel As Boolean)
        
    Dim RsDescrGrTrab As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrTrab.Text) <> "" Then
        Set RsDescrGrTrab = New ADODB.recordset
        With RsDescrGrTrab
            If TipoFolha = "FolhaTrabalhoAna" Then
                .Source = "SELECT descr_gr_trab FROM sl_gr_trab WHERE cod_gr_trab= " & BL_TrataStringParaBD(EcCodGrTrab.Text)
            ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
                .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcCodGrTrab.Text)
            End If
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrTrab.RecordCount <> 0 Then
            If TipoFolha = "FolhaTrabalhoAna" Then
                EcDescrGrTrab.Text = "" & RsDescrGrTrab!descr_gr_trab
            ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
                EcDescrGrTrab.Text = "" & RsDescrGrTrab!descr_gr_ana
            End If
        Else
            Cancel = True
            EcDescrGrTrab.Text = ""
            If TipoFolha = "FolhaTrabalhoAna" Then
                BG_Mensagem mediMsgBox, "O Grupo de Trabalho indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            ElseIf TipoFolha = " FolhaTrabalhoTubo=" Then
                BG_Mensagem mediMsgBox, "O Grupo de An�lises indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            End If
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrTrab.Close
        Set RsDescrGrTrab = Nothing
    Else
        EcDescrGrTrab.Text = ""
    End If
    
End Sub

Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.Text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.Text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Private Sub EcData_Change()
    Dim sSql As String
    Dim rsFolhasTrab As New ADODB.recordset
    totalImpr = 0
    ReDim EstrutReImpr(0)
    
    sSql = "select seq_impr,cod_gr_trab,descr_gr_trab from sl_impr_folhas_trab, sl_gr_trab where cod_grupo = cod_gr_trab"
    sSql = sSql & " AND dt_imp = " & BL_TrataDataParaBD(EcData.value)
    EcListaFolhasTrab.Clear
    rsFolhasTrab.CursorLocation = adUseClient
    rsFolhasTrab.CursorType = adOpenForwardOnly
    rsFolhasTrab.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFolhasTrab.Open sSql, gConexao
    If rsFolhasTrab.RecordCount > 0 Then
        While Not rsFolhasTrab.EOF
            totalImpr = totalImpr + 1
            ReDim Preserve EstrutReImpr(totalImpr)
            EstrutReImpr(totalImpr).cod_gr_trab = BL_HandleNull(rsFolhasTrab!cod_gr_trab, "")
            EstrutReImpr(totalImpr).descr_gr_trab = BL_HandleNull(rsFolhasTrab!descr_gr_trab, "")
            EstrutReImpr(totalImpr).seq_impr = BL_HandleNull(rsFolhasTrab!seq_impr, -1)
            
            EcListaFolhasTrab.AddItem BL_HandleNull(rsFolhasTrab!cod_gr_trab, "") & BL_HandleNull(rsFolhasTrab!descr_gr_trab, "")
            EcListaFolhasTrab.ItemData(EcListaFolhasTrab.NewIndex) = BL_HandleNull(rsFolhasTrab!seq_impr, -1)
            
            rsFolhasTrab.MoveNext
        Wend
    End If
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub





Private Sub EcUtente_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcUtente_Validate(Cancel As Boolean)
    
    Dim RsTipoUte As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcUtente.Text) <> "" Then
        Set RsTipoUte = New ADODB.recordset
        
        With RsTipoUte
            .Source = "SELECT cod_t_utente FROM sl_t_utente WHERE descr_t_utente = " & BL_TrataStringParaBD(EcUtente.Text)
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsTipoUte.RecordCount > 0 Then
            EcCodTUte.Text = RsTipoUte!cod_t_utente
        Else
            Cancel = True
            BG_Mensagem mediMsgBox, "O Tipo de Utente indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsTipoUte.Close
        Set RsTipoUte = Nothing
    End If
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()

    EventoActivate
    
End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
        
End Sub

Sub Inicializacoes()

    If TipoFolha = "FolhaTrabalhoAna" Then
        Me.caption = " Impress�o de Folhas de Trabalho "
        LbTipo.caption = "Grupo Trabalho"
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        Me.caption = " Impress�o de Folhas de Trabalho por Tubo"
        LbTipo.caption = "Grupo An�lises"
    End If
    
    Me.left = 540
    Me.top = 450
    Me.Width = 9660
    Me.Height = 4290 ' Normal
    'Me.Height = 8330 ' Campos Extras
    EcPrinterEtiq.Text = BL_SelImpressora("Etiqueta.rpt")
    ReDim Tubos(0)
    Set CampoDeFocus = EcCodGrTrab
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormFolhasTrab = Nothing
    
    Call BL_FechaPreview("Folha Trabalho")
    
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbLocal.ListIndex = mediComboValorNull
'    EcNumFolhaTrab.Text = ""
    EcCodGrTrab.Text = ""
    EcDescrGrTrab.Text = ""
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    EcCodTUte.Text = ""
    EcUtente.Text = ""
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    ReDim Tubos(0)
    CkImprimeEtiquetas.value = vbUnchecked
    EcNumFolhaTrab = ""
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodGrTrab.Tag = "200"
    EcCodProveniencia.Tag = "200"
    EcUtente.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodGrTrab.MaxLength = 10
    EcCodProveniencia.MaxLength = 5
    EcUtente.MaxLength = 5
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "gr_empr_inst", "empresa_id", "nome_empr", CbLocal
    Dim i As Long
        
    If gLAB = "CHNE" Then
        For i = 0 To CbLocal.ListCount - 1
            If CbLocal.ItemData(i) = gCodLocal Then
                CbLocal.ListIndex = i
            End If
        Next
    End If

                
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    EcNumFolhaTrab.Tag = adNumeric
    If gTipoInstituicao = "PRIVADA" Then
        OptNovasAnas.value = vbChecked
    End If
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub FuncaoProcurar()
   
End Sub

Sub FuncaoImprimir()
    Dim sSql As String
    Dim rsFolhasTrab As New ADODB.recordset
    If EcNumReq <> "" Then
        If gSGBD = gSqlServer Then
            sSql = "SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_ana_s from sl_marcacoes where n_req = " & BL_TrataStringParaBD(EcNumReq) & ")"
            sSql = sSql & " UNION SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_ana_c from sl_marcacoes where n_req = " & BL_TrataStringParaBD(EcNumReq) & ")"
            sSql = sSql & " UNION SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_perfil from sl_marcacoes where n_req = " & BL_TrataStringParaBD(EcNumReq) & ")"
            sSql = sSql & " ORDER BY sl_ana_trab.ordem "
        ElseIf gSGBD = gOracle Then
            sSql = "SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_ana_s from sl_marcacoes where n_req = " & BL_TrataStringParaBD(EcNumReq) & ")"
            sSql = sSql & " UNION SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_ana_c from sl_marcacoes where n_req = " & BL_TrataStringParaBD(EcNumReq) & ")"
            sSql = sSql & " UNION SELECT distinct cod_gr_trab from sl_ana_trab where cod_analise in (select cod_perfil from sl_marcacoes where n_req = " & BL_TrataStringParaBD(EcNumReq) & ")"
            sSql = sSql & " ORDER BY cod_gr_trab "
        End If
        rsFolhasTrab.CursorLocation = adUseClient
        rsFolhasTrab.CursorType = adOpenForwardOnly
        rsFolhasTrab.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsFolhasTrab.Open sSql, gConexao
        If rsFolhasTrab.RecordCount > 0 Then
            While Not rsFolhasTrab.EOF
                If EcCodGrTrab <> "" And EcCodGrTrab = rsFolhasTrab!cod_gr_trab Then
                    EcCodGrTrab = rsFolhasTrab!cod_gr_trab
                    EcCodGrTrab_Validate True
                    Call Imprime_FolhaTrabalho(True)
                    If EcSeqFolhaTrab <> "" Then
                        RegistaImprFolhaTrab EcCodGrTrab, EcSeqFolhaTrab, True
                    End If
                    EcCodGrTrab = ""
                ElseIf EcCodGrTrab = "" Then
                    EcCodGrTrab = rsFolhasTrab!cod_gr_trab
                    EcCodGrTrab_Validate True
                    Call Imprime_FolhaTrabalho(True)
                    EcCodGrTrab = ""
                    If EcSeqFolhaTrab <> "" Then
                        RegistaImprFolhaTrab EcCodGrTrab, EcSeqFolhaTrab, True
                    End If
                End If
                rsFolhasTrab.MoveNext
            Wend
         End If
         'DIVERSOS
        If gImprimeFolhaTrabDiversos = mediSim Then
            EcCodGrTrab = "999"
            EcCodGrTrab_Validate True
            Call Imprime_FolhaTrabalho(True)
            If EcSeqFolhaTrab <> "" Then
                RegistaImprFolhaTrab EcCodGrTrab, EcSeqFolhaTrab, True
            End If
        End If
         rsFolhasTrab.Close
         Set rsFolhasTrab = Nothing
    ElseIf CkTodos.value = vbChecked Then
        sSql = "SELECT * from sl_gr_trab where impressao_automatica = 1 "
        rsFolhasTrab.CursorLocation = adUseClient
        rsFolhasTrab.CursorType = adOpenForwardOnly
        rsFolhasTrab.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsFolhasTrab.Open sSql, gConexao
        If rsFolhasTrab.RecordCount > 0 Then
            While Not rsFolhasTrab.EOF
                EcCodGrTrab = rsFolhasTrab!cod_gr_trab
                EcCodGrTrab_Validate True
                Call Imprime_FolhaTrabalho(True)
                rsFolhasTrab.MoveNext
                If EcSeqFolhaTrab <> "" Then
                    RegistaImprFolhaTrab EcCodGrTrab, EcSeqFolhaTrab, True
                End If
            Wend
         End If
         rsFolhasTrab.Close
         Set rsFolhasTrab = Nothing
'         'DIVERSOS
        If gImprimeFolhaTrabDiversos = mediSim Then
            EcCodGrTrab = "999"
            EcCodGrTrab_Validate True
            Call Imprime_FolhaTrabalho(True)
            If EcSeqFolhaTrab <> "" Then
                RegistaImprFolhaTrab EcCodGrTrab, EcSeqFolhaTrab, True
            End If
        End If
    Else
        Call Imprime_FolhaTrabalho(False)
        If EcCodGrTrab <> "" And EcSeqFolhaTrab <> "" Then
            RegistaImprFolhaTrab EcCodGrTrab, EcSeqFolhaTrab, True
        End If
    End If
    
    
End Sub

Sub ImprimirVerAntes()
    
    Call Imprime_FolhaTrabalho

End Sub

Public Sub Imprime_FolhaTrabalho(Optional OmiteAvisos As Boolean)
    Dim resAntLivre As String
    Dim RegReq As ADODB.recordset
    Dim RegGrupo As ADODB.recordset
    Dim RegAviso As ADODB.recordset
    Dim RegConfigLivre As ADODB.recordset
    Dim RegInfClinica As ADODB.recordset
    Dim RegProd As ADODB.recordset
    
    Dim CmdDescrAnalise As ADODB.Command
    Dim CmdProd As ADODB.Command
    Dim CmdAnaGhostMember As ADODB.Command
    Dim RegCmdGHOSTMEMBER As ADODB.recordset
    
'   Dim CmdSeqRes As ADODB.Command
    
    Dim RegCmd As ADODB.recordset
    
    Dim i As Integer
    Dim j As Integer
    Dim seq As Long
    Dim Cod_Aviso As String
    Dim Quebra_Req As Integer
    Dim Cols_Fixas As Integer
    Dim DescrMembros As Integer
    Dim UltimaFT As Long
    Dim continua As Boolean
    
    Dim resProteinas As String
    
    Dim NAnalises As Integer
    Dim Query  As String
    Dim ComandoSql As String
    
    Dim Repete As Boolean
    
    Dim Compara As Long
    Dim valor As Long
    'NELSONPSILVA 22.01.2018 CHVNG�9005
    Dim total As Long
    Dim NRegistosLinha As Long
    '
    Dim cont As Integer
    Dim Sai As Boolean
    Dim s As String
    Dim Produtos As String
    Dim codAnalise As String
    Dim UltrapassaLimite As Boolean
    Dim sql As String
    Dim disposicao As Integer
    Dim conta As Integer
    Dim seqObsAnaReq As Long
    Dim SeqUtente As Long
    Dim antibioticos As String
    On Error GoTo TrataErro
    
    
    EcSeqFolhaTrab = ""
    
    '1. Verifica se a Form Preview j� est� aberta
    If BL_PreviewAberto("Folha de Trabalho") = True Then Exit Sub
        
    
    '2. Verifica Campos Obrigat�rios
    If EcCodGrTrab.Text = "" Then
        If TipoFolha = "FolhaTrabalhoAna" Then
            Call BG_Mensagem(mediMsgBox, "Indique o Grupo de Trabalho para o qual deseja listar a Folha de Trabalho.", vbOKOnly + vbExclamation, App.ProductName)
        Else
            Call BG_Mensagem(mediMsgBox, "Indique o Grupo de An�lises para o qual deseja listar a Folha de Trabalho por Tubo.", vbOKOnly + vbExclamation, App.ProductName)
        End If
        EcCodGrTrab.SetFocus
        Exit Sub
    End If
    
    If EcDtIni.Text = "" And EcDtFim.Text <> "" And EcNumFolhaTrab = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    
    
    '3. Obt�m os dados da Folha de Trabalho em fun��o do grupo de trabalho seleccionado
    ' SO PARA FOLHAS DE TRABALHO
    If TipoFolha = "FolhaTrabalhoAna" Then
        Set RegGrupo = New ADODB.recordset
        'NELSONPSILVA 22.01.2018 CHVNG�9005 (adUseServer -> adUseClient em toda a fun��o Imprime_FolhaTrabalho)
        RegGrupo.CursorLocation = adUseClient
        RegGrupo.LockType = adLockReadOnly
        RegGrupo.CursorType = adOpenForwardOnly
        RegGrupo.ActiveConnection = gConexao
        RegGrupo.Source = "SELECT Cod_ana_aviso,T_Disposicao,Quebra_Req,Cols_Fixas, flg_descr_membros " & _
                          "FROM sl_gr_trab " & _
                          "WHERE cod_gr_trab ='" & EcCodGrTrab.Text & "'"
        If gModoDebug = mediSim Then BG_LogFile_Erros RegGrupo.Source
        RegGrupo.Open
        If RegGrupo.RecordCount <> 0 Then
            Cod_Aviso = Trim("" & RegGrupo!cod_ana_aviso)
            disposicao = CDbl("0" & RegGrupo!t_disposicao)
            Quebra_Req = CDbl("0" & RegGrupo!Quebra_Req)
            Cols_Fixas = CDbl("0" & RegGrupo!Cols_Fixas)
            DescrMembros = CDbl("0" & RegGrupo!flg_descr_membros)
        End If
        RegGrupo.Close
        Set RegGrupo = Nothing
        
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        Cod_Aviso = ""
        disposicao = gT_Vertical
        Quebra_Req = 0
        Cols_Fixas = 0
    End If
    
    
    '4.C�lculo da express�o SQL
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
    
    'Obt�m a partir das requisi��es as an�lises pertencentes ao Grupo de Trabalho seleccionado
    Set RegReq = New ADODB.recordset
    RegReq.CursorLocation = adUseClient
    RegReq.LockType = adLockReadOnly
    RegReq.CursorType = adOpenForwardOnly
    RegReq.ActiveConnection = gConexao
    
    If TipoFolha = "FolhaTrabalhoAna" Then
        If disposicao <> gT_Livre Then
            'MODELO VERTICAL E HORIZONTAL
            
            Query = "SELECT sl_requis.N_req,sl_requis.T_urg,sl_requis.dt_chega, sl_requis.dt_pretend, sl_requis.dt_conclusao, "
            
            If gTipoInstituicao <> "PRIVADA" Then
                Query = Query & "sl_tbf_t_sit.Descr_t_sit,"
            End If
            
            Query = Query & "sl_Proven.Descr_proven,"
            Query = Query & "sl_identif.seq_utente, sl_identif.Nome_ute,sl_identif.T_utente,sl_identif.Dt_nasc_ute,"
            Query = Query & "sl_tbf_sexo.descr_sexo,"
            Query = Query & "sl_marcacoes.Cod_perfil,sl_marcacoes.Cod_ana_c,sl_marcacoes.Cod_ana_s,sl_marcacoes.Cod_Agrup,sl_marcacoes.Ord_Ana,sl_marcacoes.Ord_Ana_Perf,sl_marcacoes.Ord_Ana_Compl, "
            Query = Query & "sl_requis.n_epis, sl_identif.utente, sl_requis.inf_complementar "
            Query = Query & "FROM sl_identif,sl_requis,sl_marcacoes, sl_req_tubo, sl_ana_s, sl_perfis, sl_ana_c, sl_ana_acrescentadas"
            If EcCodGrTrab <> "999" And TipoFolha = "FolhaTrabalhoAna" Then
                Query = Query & ", sl_ana_trab "
            End If
            If gTipoInstituicao <> "PRIVADA" Then
                Query = Query & ", sl_tbf_t_sit "
            End If
            
            
            Query = Query & ",sl_tbf_sexo,sl_proven "
            Query = Query & " WHERE "
            Query = Query & " sl_identif.Seq_utente=sl_requis.Seq_utente "
            Query = Query & " AND sl_marcacoes.n_Req = sl_ana_Acrescentadas.n_req AND sl_marcacoes.cod_agrup = sl_ana_acrescentadas.cod_agrup AND nvl(sl_ana_Acrescentadas.flg_eliminada,0) = 0"
            If EcCodGrTrab <> "999" And TipoFolha = "FolhaTrabalhoAna" Then
                Query = Query & " AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
            End If
            If gTipoInstituicao <> "PRIVADA" Then
                Query = Query & " AND sl_tbf_t_sit.Cod_t_sit=sl_requis.T_sit "
            End If
            
            Select Case gSGBD
                Case gOracle
                    Query = Query & " AND sl_requis.Cod_proven=sl_proven.Cod_proven(+) AND sl_marcacoes.cod_perfil=sl_perfis.cod_perfis(+) AND sl_marcacoes.cod_ana_c=sl_ana_c.cod_ana_c(+) AND sl_marcacoes.cod_ana_s=sl_ana_s.cod_ana_s(+) AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo(+) "
                Case gInformix
                    Query = Query & " AND sl_requis.Cod_proven=sl_proven.Cod_proven AND sl_marcacoes.cod_perfil=sl_perfis.cod_perfis AND sl_marcacoes.cod_ana_c=sl_ana_c.cod_ana_c AND sl_marcacoes.cod_ana_s=sl_ana_s.cod_ana_s AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo "
                Case gSqlServer
                    Query = Query & " AND sl_requis.Cod_proven*=sl_proven.Cod_proven AND sl_marcacoes.cod_perfil*=sl_perfis.cod_perfis AND sl_marcacoes.cod_ana_c*=sl_ana_c.cod_ana_c AND sl_marcacoes.cod_ana_s*=sl_ana_s.cod_ana_s AND sl_identif.sexo_ute*=sl_tbf_sexo.cod_sexo "
            End Select
            
            Query = Query & " AND sl_requis.N_req=sl_marcacoes.N_req "
            
            If gLAB <> "HPOVOA" Then
                Query = Query & " AND sl_marcacoes.cod_ana_s <> 'S99999'"
            End If
                    If EcNumFolhaTrab <> "" Then
                        Query = Query & " AND (sl_marcacoes.n_folha_trab = " & EcNumFolhaTrab & " )"
                    ElseIf OptNovasAnas.value = vbChecked Then
                        Query = Query & " AND (sl_marcacoes.n_folha_trab is null or sl_marcacoes.n_folha_trab =0)"
                    End If
                    
                    Query = Query & " AND sl_requis.n_req = sl_marcacoes.n_req AND "
                    Select Case gSGBD
                        Case gOracle
                            Query = Query & "sl_requis.Cod_proven=sl_proven.Cod_proven(+) AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo(+) "
                        Case gInformix
                            Query = Query & "sl_requis.Cod_proven=sl_proven.Cod_proven AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo "
                        Case gSqlServer
                            Query = Query & "sl_requis.Cod_proven*=sl_proven.Cod_proven AND sl_identif.sexo_ute*=sl_tbf_sexo.cod_sexo "
                    End Select
        Else
            'MODELO LIVRE
            Query = "SELECT sl_requis.N_req,sl_requis.T_urg,sl_requis.dt_chega, sl_requis.dt_pretend, sl_requis.dt_conclusao, "
            If gTipoInstituicao <> "PRIVADA" Then
                Query = Query & "sl_tbf_t_sit.Descr_t_sit,"
            End If
            Query = Query & "sl_Proven.Descr_proven,"
            Query = Query & "sl_identif.seq_utente,sl_identif.Nome_ute,sl_identif.T_utente,sl_identif.Dt_nasc_ute,"
            Query = Query & "sl_tbf_sexo.descr_sexo,"
            Query = Query & "sl_marcacoes.Cod_perfil,sl_marcacoes.Cod_ana_c,sl_marcacoes.Cod_ana_s,sl_marcacoes.Cod_Agrup,sl_marcacoes.Ord_Ana,sl_marcacoes.Ord_Ana_Perf,sl_marcacoes.Ord_Ana_Compl,"
            Query = Query & "sl_perfis.descr_perfis,"
            Query = Query & "sl_ana_c.descr_ana_c,"
            Query = Query & "sl_ana_s.descr_ana_s, "
            Query = Query & "sl_requis.n_epis, sl_identif.utente, sl_requis.inf_complementar "
            Query = Query & "FROM sl_identif,sl_requis,sl_marcacoes, sl_req_tubo, sl_ana_acrescentadas, "
            If EcCodGrTrab <> "999" And TipoFolha = "FolhaTrabalhoAna" Then
                Query = Query & " sl_ana_trab, "
            End If
            If gTipoInstituicao <> "PRIVADA" Then
                Query = Query & " sl_tbf_t_sit, "
            End If
            Query = Query & IIf(gSGBD = gInformix, "OUTER sl_tbf_sexo,OUTER sl_proven,OUTER sl_perfis,OUTER sl_ana_c,OUTER sl_ana_s ", "sl_tbf_sexo,sl_proven,sl_perfis,sl_ana_c,sl_ana_s ")
            Query = Query & "WHERE "
            Query = Query & " sl_identif.Seq_utente=sl_requis.Seq_utente "
            If gTipoInstituicao <> "PRIVADA" Then
                Query = Query & " AND sl_tbf_t_sit.Cod_t_sit=sl_requis.T_sit "
            End If
            Query = Query & " AND sl_requis.n_req = sl_marcacoes.n_req AND "
            
            If EcCodGrTrab <> "999" And TipoFolha = "FolhaTrabalhoAna" Then
                Query = Query & "  sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab) & " AND "
            End If
            If EcNumFolhaTrab <> "" Then
                Query = Query & "  (sl_marcacoes.n_folha_trab = " & EcNumFolhaTrab & " ) AND "
            ElseIf OptNovasAnas.value = vbChecked Then
                Query = Query & "  (sl_marcacoes.n_folha_trab is null or sl_marcacoes.n_folha_trab =0) AND "
            End If
            
            Select Case gSGBD
                Case gOracle
                    Query = Query & "sl_requis.Cod_proven=sl_proven.Cod_proven(+) AND sl_marcacoes.cod_perfil=sl_perfis.cod_perfis(+) AND sl_marcacoes.cod_ana_c=sl_ana_c.cod_ana_c(+) AND sl_marcacoes.cod_ana_s=sl_ana_s.cod_ana_s(+) AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo(+) "
                Case gInformix
                    Query = Query & "sl_requis.Cod_proven=sl_proven.Cod_proven AND sl_marcacoes.cod_perfil=sl_perfis.cod_perfis AND sl_marcacoes.cod_ana_c=sl_ana_c.cod_ana_c AND sl_marcacoes.cod_ana_s=sl_ana_s.cod_ana_s AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo "
                Case gSqlServer
                    Query = Query & "sl_requis.Cod_proven*=sl_proven.Cod_proven AND sl_marcacoes.cod_perfil*=sl_perfis.cod_perfis AND sl_marcacoes.cod_ana_c*=sl_ana_c.cod_ana_c AND sl_marcacoes.cod_ana_s*=sl_ana_s.cod_ana_s AND sl_identif.sexo_ute*=sl_tbf_sexo.cod_sexo "
            End Select
        End If
        Query = Query & " AND sl_marcacoes.n_Req = sl_ana_Acrescentadas.n_req AND sl_marcacoes.cod_agrup = sl_ana_acrescentadas.cod_agrup AND nvl(sl_ana_Acrescentadas.flg_eliminada,0) = 0"
        Query = Query & " AND sl_req_tubo.dt_chega IS NOT NULL AND sl_req_tubo.dt_eliminacao IS NULL  "
        Query = Query & " AND sl_marcacoes.n_Req = sl_req_tubo.n_req  "
        Query = Query & " AND (   sl_perfis.cod_tubo = sl_req_tubo.cod_tubo "
        Query = Query & " OR sl_ana_c.cod_tubo = sl_req_tubo.cod_tubo "
        Query = Query & " OR sl_ana_s.cod_tubo = sl_req_tubo.cod_tubo) "
    ' FOLHAS DE TRABALHO POR TUBO
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        Query = "SELECT sl_requis.N_req,sl_requis.T_urg,sl_requis.dt_chega, sl_requis.dt_pretend, sl_requis.dt_conclusao, "
        If gTipoInstituicao <> "PRIVADA" Then
            Query = Query & "sl_tbf_t_sit.Descr_t_sit, "
        End If
        Query = Query & "Sl_Proven.Descr_proven,"
        Query = Query & "sl_identif.seq_utente, sl_identif.Nome_ute,sl_identif.T_utente,sl_identif.Dt_nasc_ute,"
        Query = Query & "sl_tbf_sexo.descr_sexo, sl_ana_s.cod_ana_s, sl_tubo_sec.cod_tubo, sl_tubo_sec.abrv_tubo, "
        Query = Query & "sl_requis.n_epis, sl_identif.utente "
        Query = Query & " FROM sl_identif,sl_requis,sl_marcacoes, "
        If EcCodGrTrab <> "999" And TipoFolha = "FolhaTrabalhoAna" Then
            Query = Query & " sl_ana_trab, "
        End If
        Query = Query & "sl_tbf_sexo,sl_proven, sl_tubo_sec, sl_ana_s "
        If gTipoInstituicao <> "PRIVADA" Then
            Query = Query & ", sl_tbf_t_sit"
        End If
    
        Query = Query & " WHERE "
        Query = Query & " sl_identif.Seq_utente=sl_requis.Seq_utente "
        If gTipoInstituicao <> "PRIVADA" Then
            Query = Query & " AND sl_tbf_t_sit.Cod_t_sit=sl_requis.T_sit "
        End If
        If EcCodGrTrab <> "999" And TipoFolha = "FolhaTrabalhoAna" Then
            Query = Query & " AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
        End If
        Query = Query & " AND sl_requis.dt_chega IS NOT NULL "
        Query = Query & " AND sl_requis.N_req=sl_marcacoes.N_req "
        Query = Query & " AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s "
        Query = Query & " AND sl_ana_s.cod_tubo_sec = sl_tubo_sec.cod_tubo "
                
        If EcNumFolhaTrab <> "" Then
            Query = Query & " AND (sl_marcacoes.n_folha_trab = " & EcNumFolhaTrab & " )"
        ElseIf OptNovasAnas.value = vbChecked Then
            Query = Query & " AND (sl_marcacoes.n_folha_trab is null or sl_marcacoes.n_folha_trab =0)"
        End If
        
        Query = Query & " AND sl_requis.n_req = sl_marcacoes.n_req AND "
        Select Case gSGBD
            Case gOracle
                Query = Query & "sl_requis.Cod_proven=sl_proven.Cod_proven(+) AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo(+) "
            Case gInformix
                Query = Query & "sl_requis.Cod_proven=sl_proven.Cod_proven AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo "
            Case gSqlServer
                Query = Query & "sl_requis.Cod_proven*=sl_proven.Cod_proven AND sl_identif.sexo_ute*=sl_tbf_sexo.cod_sexo "
        End Select
    End If
    
                        
    
    If TipoFolha = "FolhaTrabalhoAna" Then
        'S� interessam as requisi��es cujas an�lises sejam as do grupo de trabalho especificado ou do tipo gGHOSTMEMBER_S.
        If EcCodGrTrab <> "999" Then
            Query = Query & " AND (sl_marcacoes.cod_ana_s = sl_ana_trab.Cod_analise "
            Query = Query & " OR sl_marcacoes.cod_perfil = sl_ana_trab.Cod_analise "
            Query = Query & " OR sl_marcacoes.cod_ana_c = sl_ana_trab.Cod_analise ) "
        Else
            Query = Query & " AND cod_agrup not in (select cod_analise from sl_ana_trab) "
        End If

    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        Query = Query & " AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(EcCodGrTrab)
    End If
    
    'Query = Query & " AND sl_marcacoes.cod_ana_s IN (SELECT Cod_analise FROM sl_ana_trab WHERE cod_gr_trab=" & BL_TrataStringParaBD(EcCodGrTrab) & " ) "
    
    'As an�lises marcadas tem que estar livres (sem estar associadas ainda a nenhuma Folha de Trabalho)
    'Query = Query & " AND sl_marcacoes.N_folha_trab=0"
    
    'Local Preenchido?
    If (CbLocal.ListIndex <> -1) Then
        If gMultiLocalAmbito = mediSim And gFolhaTrabLocalReq <> mediSim Then
            Query = Query & " AND sl_ana_acrescentadas.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
        Else
            Query = Query & " AND sl_requis.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
        End If
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        Query = Query & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(CbUrgencia.ItemData(CbUrgencia.ListIndex))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If EcCodProveniencia.Text <> "" Then
        Query = Query & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    
    'Tipo de Situa��o preenchido?
    If (CbSituacao.ListIndex <> -1) And gTipoInstituicao <> "PRIVADA" Then
        Query = Query & " AND sl_requis.T_sit=" & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    
    'Tipo de Utente preenchido? (n�o pertence � tabela)=>Restringe as an�lises pelo Tipo de utente (e n�o pelo C�digo!)
    If EcUtente.Text <> "" Then
        Query = Query & " AND sl_identif.T_utente=" & BL_TrataStringParaBD(RTrim(EcUtente.Text))
    End If
    
    'Datas preenchidas?
    'S� interessam as an�lises filtradas das requisi��es que sejam poss�veis de iniciar (Data e Produtos Chegados!!)
    If TipoFolha = "FolhaTrabalhoAna" Then
        If EcDtIni.Text <> "" And EcDtFim.Text <> "" Then
            Query = Query & " AND sl_req_tubo.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
        ElseIf EcDtIni.Text <> "" And EcDtFim.Text = "" Then
            Query = Query & " AND sl_req_tubo.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(Date)
        End If
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        If EcDtIni.Text <> "" And EcDtFim.Text <> "" Then
            Query = Query & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
        ElseIf EcDtIni.Text <> "" And EcDtFim.Text = "" Then
            Query = Query & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(Date)
        End If
    End If

    
    If EcNumReq <> "" Then
     Query = Query & " AND SL_REQUIS.n_req = " & BL_TrataStringParaBD(EcNumReq)
    End If
    
    Query = Query & " AND sl_requis.estado_Req not in (" & BL_TrataStringParaBD(gEstadoReqCancelada) & "," & BL_TrataStringParaBD(gEstadoReqBloqueada) & " ) "
    'Crit�rio de Ordena��o
    If TipoFolha = "FolhaTrabalhoAna" Then
        If disposicao <> gT_Livre Then
            Query = Query & " ORDER by  sl_requis.n_Req,  "
            If EcCodGrTrab <> "999" Then
                Query = Query & "  "
            End If
            Query = Query & " sl_marcacoes.ord_ana, sl_marcacoes.Cod_Agrup, sl_marcacoes.cod_perfil, sl_marcacoes.ord_ana_perf, sl_marcacoes.cod_ana_c, sl_marcacoes.ord_ana_compl "
        Else
            'Por causa dos anteriores
            Query = Query & " ORDER by sl_requis.N_req, sl_marcacoes.ord_ana, sl_marcacoes.ord_ana_compl, sl_marcacoes.Cod_Agrup, sl_marcacoes.cod_perfil, sl_marcacoes.ord_ana_perf, sl_marcacoes.cod_ana_c, sl_marcacoes.cod_ana_s "
        End If
        
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        Query = Query & " ORDER by  "
        If EcCodGrTrab <> "999" And TipoFolha = "FolhaTrabalhoAna" Then
            Query = Query & " sl_ana_trab.ordem, "
        End If
        Query = Query & " sl_requis.N_req, sl_tubo_Sec.abrv_tubo "
    End If
    RegReq.Source = Query
    If gModoDebug = mediSim Then BG_LogFile_Erros Query
    RegReq.Open
    
    
    
    
    'Verifica se existem registos para imprimir
    total = RegReq.RecordCount
    If total = 0 Then
        RegReq.Close
        Set RegReq = Nothing
        If OmiteAvisos = False Then
            Call BG_Mensagem(mediMsgBox, "N�o foram encontradas nenhumas an�lises para iniciar uma Folha de Trabalho", vbOKOnly + vbExclamation, App.ProductName)
        End If
        Exit Sub
    End If
        
    
    
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
    
    'Define o Comando das descri��es das an�lises
    If TipoFolha = "FolhaTrabalhoAna" Then
        Set CmdDescrAnalise = New ADODB.Command
        CmdDescrAnalise.ActiveConnection = gConexao
        CmdDescrAnalise.CommandType = adCmdText
        CmdDescrAnalise.Prepared = True
        CmdDescrAnalise.CommandText = "SELECT descr_ana,abr_ana FROM slv_analises WHERE cod_ana=?"
        CmdDescrAnalise.Parameters.Append CmdDescrAnalise.CreateParameter("COD_ANA", adVarChar, adParamInput, 8)
    End If
    
    
    
    
    
    '5. Abre a Common Dialog e Inicializa as propriedades b�sicas do Report
    If TipoFolha = "FolhaTrabalhoAna" Then
        Select Case disposicao
            Case gT_Livre
                Set RegConfigLivre = New ADODB.recordset
                RegConfigLivre.CursorLocation = adUseClient
                RegConfigLivre.LockType = adLockReadOnly
                RegConfigLivre.CursorType = adOpenForwardOnly
                RegConfigLivre.ActiveConnection = gConexao
                RegConfigLivre.Source = "SELECT flg_situacao,flg_urgencia,flg_nome,flg_idade,flg_sexo,flg_inf_cli,num_ult_res,texto_livre, flg_antibioticos, flg_membros FROM sl_gr_trab,sl_modelo WHERE sl_gr_trab.Cod_modelo=sl_modelo.Cod_modelo AND sl_gr_trab.Cod_gr_trab=" & BL_TrataStringParaBD(Trim(EcCodGrTrab.Text))
                If gModoDebug = mediSim Then BG_LogFile_Erros RegConfigLivre.Source
                RegConfigLivre.Open
                If RegConfigLivre.RecordCount = 0 Then
                    ConfigLivre.flg_idade = "1"
                    ConfigLivre.flg_inf_cli = "1"
                    ConfigLivre.flg_nome = "1"
                    ConfigLivre.flg_sexo = "1"
                    ConfigLivre.flg_situacao = "1"
                    ConfigLivre.flg_urgencia = "1"
                    ConfigLivre.Num_Ult_Res = 0
                    ConfigLivre.flg_antibioticos = "0"
                    ConfigLivre.flg_membros = 0
                    ConfigLivre.TextoLivre = ""
                Else
                    ConfigLivre.flg_idade = "" & RegConfigLivre!flg_idade
                    ConfigLivre.flg_inf_cli = "" & RegConfigLivre!flg_inf_cli
                    ConfigLivre.flg_nome = "" & RegConfigLivre!flg_nome
                    ConfigLivre.flg_sexo = "" & RegConfigLivre!flg_sexo
                    ConfigLivre.flg_situacao = "" & RegConfigLivre!flg_situacao
                    ConfigLivre.flg_urgencia = "" & RegConfigLivre!flg_urgencia
                    ConfigLivre.flg_antibioticos = "" & BL_HandleNull(RegConfigLivre!flg_antibioticos, "")
                    ConfigLivre.flg_membros = "" & BL_HandleNull(RegConfigLivre!flg_membros, 0)
                    If Trim("" & RegConfigLivre!Num_Ult_Res) = "" Then
                        ConfigLivre.Num_Ult_Res = 0
                    Else
                        ConfigLivre.Num_Ult_Res = "" & RegConfigLivre!Num_Ult_Res
                    End If
                    ConfigLivre.TextoLivre = "" & RegConfigLivre!Texto_Livre
                End If
                RegConfigLivre.Close
                Set RegConfigLivre = Nothing
                
                If gImprimirDestino = 1 Then
                    If ConfigLivre.Num_Ult_Res = 0 Then
                        continua = BL_IniciaReport("FolhasTrabalhoLivre", "Folha de Trabalho", crptToPrinter)
                    Else
                        continua = BL_IniciaReport("FolhasTrabalhoLivreHist", "Folha de Trabalho", crptToPrinter)
                    End If
                Else
                    If ConfigLivre.Num_Ult_Res = 0 Then
                        continua = BL_IniciaReport("FolhasTrabalhoLivre", "Folha de Trabalho", crptToWindow)
                    Else
                        continua = BL_IniciaReport("FolhasTrabalhoLivreHist", "Folha de Trabalho", crptToWindow)
                    End If
                End If
            Case gT_Vertical
                If gImprimirDestino = 1 Then
                    continua = BL_IniciaReport("FolhasTrabalhoVertical", "Folha de Trabalho", crptToPrinter, , False, , , , , , "HORIZONTAL")
                Else
                    continua = BL_IniciaReport("FolhasTrabalhoVertical", "Folha de trabalho", crptToWindow, , False, , , , , , "HORIZONTAL")
                End If
            Case gT_Horizontal
                If gImprimirDestino = 1 Then
                    continua = BL_IniciaReport("FolhasTrabalho", "Folha de Trabalho", crptToPrinter, , , , , , , , "HORIZONTAL")
                Else
                    continua = BL_IniciaReport("FolhasTrabalho", "Folha de Trabalho", crptToWindow, , , , , , , , "HORIZONTAL")
                End If
        End Select
        
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        
        If gImprimirDestino = 1 Then
            continua = BL_IniciaReport("FolhasTrabalhoVertical", "Folha de Trabalho por Tubo", crptToPrinter, , , , , , , , "HORIZONTAL")
        Else
            continua = BL_IniciaReport("FolhasTrabalhoVertical", "Folha de trabalho por Tubo", crptToWindow, , , , , , , , "HORIZONTAL")
        End If
    End If
    
    
    
    If continua = False Then
        If Not RegReq Is Nothing Then
            If RegReq.state <> adStateClosed Then
                RegReq.Close
            End If
            Set RegReq = Nothing
        End If
        Set CmdDescrAnalise = Nothing
'        Set CmdAnaGhostMember = Nothing
        Exit Sub
    End If
        
    
    
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        
    '6. Inicializa os campos das estruturas com tamanho fixo (Para o Trim funcionar)
    
    UltrapassaLimite = False
    
    If disposicao <> gT_Livre Then
        For i = 1 To 13
            RegFt.abrev(i) = ""
            RegFt.codAnalise(i) = ""
            RegFt.ResAnt(i) = ""
            FtVertical.abrev(i) = ""
            FtVertical.codAnalise(i) = ""
            FtVertical.SeqUtente(i) = ""
        Next i
    End If
    CabFt.Descr_Ana_Aviso = ""
    CabFt.descr_gr_trab = ""
    
    
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
    
    '7. Pesquisa e Actualiza o N� da �ltima Folha de Trabalho emitida (na tabela Config)
    'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
    If EcNumFolhaTrab = "" Then
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraNumero("N_FOLHA_TRAB")
            i = i + 1
        Wend
    Else
        seq = EcNumFolhaTrab
    End If
    
    If seq = -1 Then
        'Incrementa sempre mesmo que d� erro!
        If Not RegReq Is Nothing Then
            If RegReq.state <> adStateClosed Then
                RegReq.Close
            End If
            Set RegReq = Nothing
        End If
        Set CmdDescrAnalise = Nothing
'        Set CmdAnaGhostMember = Nothing
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
    UltimaFT = seq
    EcSeqFolhaTrab = seq
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
    
    
    '8. Inicializa dados para a estrutura 'CABFT' com os dados fixos ou seja,
    '   da Tabela 'FTMODELOCAB'
   
    'N� da Folha de Trabalho
    CabFt.N_Ft = UltimaFT
    
    'Descri��o do Grupo de trabalho (Pelo Controlo na Form)
    CabFt.descr_gr_trab = EcDescrGrTrab.Text
    
    'Abreviatura do c�digo da an�lise de aviso (caso exista)
    If Cod_Aviso <> "" Then
        CmdDescrAnalise.Parameters("COD_ANA").value = Cod_Aviso
        Set RegCmd = CmdDescrAnalise.Execute
        If Not RegCmd.EOF Then
            CabFt.Descr_Ana_Aviso = Trim("" & RegCmd!descr_ana)
        Else
            CabFt.Descr_Ana_Aviso = ""
        End If
        RegCmd.Close
        Set RegCmd = Nothing
    End If
    
    'Quebra de p�gina para a Folha de Trabalho do Grupo
    CabFt.Quebra_Req = Quebra_Req
    
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
                     
    '9. Cria as tabelas tempor�rias para a Impress�o das Folhas de Trabalho
    Select Case disposicao
        Case gT_Horizontal
            sql = "DELETE FROM SL_CR_FTHV WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
        Case gT_Vertical
            sql = "DELETE FROM SL_CR_FTHV WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
            sql = "DELETE FROM SL_CR_FTCV WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
        Case gT_Livre
            sql = "DELETE FROM SL_CR_FTCL WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
            sql = "DELETE FROM SL_CR_FTLV WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
    End Select
    
    
    
    '_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
    
    '10. Inicializa as estruturas
    
    If TipoFolha = "FolhaTrabalhoAna" Then
        Select Case disposicao
            Case gT_Livre
                'MODELO LIVRE
                'Obt�m a configura��o da Inser��o para o Modelo Livre atrav�s da Tabela Modelo
                'Em fun��o de cada Grupo de Trabalho!
                
                'Define o Comando para obter os produtos das an�lises para cada requisi��o
                Set CmdProd = New ADODB.Command
                CmdProd.ActiveConnection = gConexao
                CmdProd.CommandType = adCmdText
                s = "SELECT sl_produto.cod_produto,sl_produto.descr_produto,sl_especif.descr_especif FROM sl_produto,sl_req_prod," & IIf(gSGBD = gInformix, "OUTER sl_especif", " sl_especif") & _
                      " WHERE sl_req_prod.n_req=? AND sl_produto.cod_produto=sl_req_prod.cod_prod "
                Select Case gSGBD
                    Case gOracle
                        s = s & "AND sl_produto.cod_especif=sl_especif.cod_especif(+) "
                    Case gInformix
                        s = s & "AND sl_produto.cod_especif=sl_especif.cod_especif "
                    Case gSqlServer
                        s = s & "AND sl_produto.cod_especif*=sl_especif.cod_especif "
                End Select
                CmdProd.CommandText = s
                CmdProd.Prepared = True
                CmdProd.Parameters.Append CmdProd.CreateParameter("N_REQ", adInteger, adParamInput, 7)
                
        Case gT_Vertical
            FtVertical.N_Ft = CabFt.N_Ft
            
            'folha de trabalho com colunas fixas s� para an�lises simples
            If Cols_Fixas = 1 Then
                Dim RsAnaTrab As ADODB.recordset
                Set RsAnaTrab = New ADODB.recordset
                With RsAnaTrab
                    .CursorLocation = adUseClient
                    .LockType = adLockReadOnly
                    .CursorType = adOpenForwardOnly
                    .ActiveConnection = gConexao
                    If DescrMembros = 1 Then
                        .Source = "SELECT cod_ana_s cod_analise, abr_ana_s, sl_ana_trab.ordem FROM sl_ana_trab,sl_ana_s "
                        .Source = .Source & " WHERE cod_analise = cod_ana_s AND cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text)
                        
                        .Source = .Source & " UNION SELECT cod_ana_s cod_analise, abr_ana_s, sl_ana_trab.ordem FROM sl_ana_trab, sl_ana_s, sl_membro "
                        .Source = .Source & " WHERE sl_Ana_trab.cod_analise = sl_membro.cod_ana_c  AND cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text)
                        .Source = .Source & " AND sl_membro.cod_membro = sl_ana_s.cod_ana_s"
                        
                        .Source = .Source & " UNION SELECT cod_ana_s cod_analise, abr_ana_s, sl_ana_trab.ordem FROM sl_ana_trab, sl_ana_s, sl_ana_perfis "
                        .Source = .Source & " WHERE sl_Ana_trab.cod_analise = sl_ana_perfis.cod_perfis  AND cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text)
                        .Source = .Source & " AND sl_ana_perfis.cod_analise= sl_ana_s.cod_ana_s"
                        
                        .Source = .Source & " UNION SELECT cod_ana_s cod_analise, abr_ana_s, sl_ana_trab.ordem FROM sl_ana_trab, sl_ana_s, sl_ana_perfis, sl_membro "
                        .Source = .Source & " WHERE sl_Ana_trab.cod_analise = sl_ana_perfis.cod_perfis  AND cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text)
                        .Source = .Source & " AND sl_ana_perfis.cod_analise= sl_membro.cod_ana_c AND sl_membro.cod_membro = sl_ana_s.cod_ana_s "
                        
                        .Source = .Source & " ORDER BY ordem"
                        
                    
                    Else
                        .Source = "SELECT cod_analise, ABR_ANA abr_ana_s FROM sl_ana_trab,slv_analises " & _
                                " WHERE cod_analise = cod_ana AND cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text) & _
                                " ORDER BY sl_ana_trab.ordem"
                    End If
                    If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                    .Open
                End With
                If RsAnaTrab.RecordCount = 0 Then
                    If OmiteAvisos = False Then
                        BG_Mensagem mediMsgBeep, "N�o existem an�lises codificadas para esse grupo de trabalho"
                    End If
                    Exit Sub
                End If
            
                'Inicializa o Vector
                For j = 1 To 13
                    If RsAnaTrab.EOF = False Then
                        FtVertical.abrev(j) = Trim(BL_HandleNull(RsAnaTrab!abr_ana_s, ""))
                        FtVertical.codAnalise(j) = Trim(RsAnaTrab!cod_analise)
                        FtVertical.SeqUtente(j) = Trim(RegReq!seq_utente)
                        RsAnaTrab.MoveNext
                    Else
                        FtVertical.abrev(j) = ""
                        FtVertical.codAnalise(j) = ""
                        FtVertical.SeqUtente(j) = ""
                    End If
                Next j
            Else
                'Limpa o Vector
                For j = 1 To 13
                    FtVertical.abrev(j) = ""
                    FtVertical.codAnalise(j) = ""
                    FtVertical.SeqUtente(j) = ""
                Next j
            End If
            
            'Percorre o RecordSet para seleccionar as an�lises distintas
            RegReq.MoveFirst
            cont = 1
            For i = 0 To total - 1
                SeqUtente = RegReq!seq_utente
                'Verifica se o Cod_Agrup da an�lise ainda n�o foi inserido na estrutura
                Repete = False
                
                codAnalise = Trim("" & RegReq!cod_ana_s)
                
                'Se a an�lise pertence a uma complexa, verifica se � a complexa a colocar na folha de trabalho
                If Trim("" & RegReq!cod_ana_c) <> "0" And DescrMembros <> 1 Then
                    Set RsAnaTrab = New ADODB.recordset
                    With RsAnaTrab
                        .CursorLocation = adUseClient
                        .LockType = adLockReadOnly
                        .CursorType = adOpenForwardOnly
                        .ActiveConnection = gConexao
                        .Source = "select * from sl_ana_trab where cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text) & _
                                    " and cod_analise = " & BL_TrataStringParaBD(Trim(RegReq!cod_ana_c))
                        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                        .Open
                    End With
                    If Not RsAnaTrab.EOF Then
                        codAnalise = Trim(RegReq!cod_ana_c)
                    End If
                End If
                'Se a an�lise pertence a um perfil, verifica se � o perfil a colocar na folha de trabalho
                If Trim("" & RegReq!Cod_Perfil) <> "0" And DescrMembros <> 1 Then
                    Set RsAnaTrab = New ADODB.recordset
                    With RsAnaTrab
                        .CursorLocation = adUseClient
                        .LockType = adLockReadOnly
                        .CursorType = adOpenForwardOnly
                        .ActiveConnection = gConexao
                        .Source = "select * from sl_ana_trab where cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text) & _
                                    " and cod_analise = " & BL_TrataStringParaBD(Trim(RegReq!Cod_Perfil))
                        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                        .Open
                    End With
                    If Not RsAnaTrab.EOF Then
                        codAnalise = Trim(RegReq!Cod_Perfil)
                    End If
                End If
        
                'Verifica repeti��es
                Repete = False
                For j = 1 To 13
                    If Trim(FtVertical.codAnalise(j)) = codAnalise Then
                        Repete = True
                    End If
                Next j
                If Repete = False Then
                    If Mid(codAnalise, 1, 1) = "S" Then
                       CmdDescrAnalise.CommandText = "SELECT descr_ana_s descr_ana,abr_ana_s abr_ana FROM sl_ana_s WHERE cod_ana_s=?"
                    ElseIf Mid(codAnalise, 1, 1) = "C" Then
                       CmdDescrAnalise.CommandText = "SELECT descr_ana_c descr_ana,abr_ana_c abr_ana FROM sl_ana_c WHERE cod_ana_c=?"
                    ElseIf Mid(codAnalise, 1, 1) = "P" Then
                       CmdDescrAnalise.CommandText = "SELECT descr_perfis descr_ana,abr_ana_p abr_ana FROM sl_perfis WHERE cod_perfis=?"
                    End If
                    CmdDescrAnalise.Parameters("COD_ANA").value = codAnalise
                    Set RegCmd = CmdDescrAnalise.Execute
                    If Not RegCmd.EOF Then
                        FtVertical.abrev(cont) = "" & RegCmd!abr_ana
                        FtVertical.codAnalise(cont) = "" & codAnalise
                        FtVertical.SeqUtente(cont) = "" & SeqUtente
                        cont = cont + 1
                    End If
                    RegCmd.Close
                    Set RegCmd = Nothing
                    'N�o actualiza mais an�lises distintas (M�ximo=13)
                    If cont > 13 Then
                        UltrapassaLimite = True
                        Exit For
                    End If
                End If
                PreenhceEstrutTubos RegReq!n_req, RegReq!cod_ana_s, True
                RegReq.MoveNext
            Next i
                
            ComandoSql = "INSERT INTO " & "SL_CR_FTCV " & _
            " (N_FT,ABREV1,ABREV2,ABREV3,ABREV4,ABREV5,ABREV6,ABREV7,ABREV8,ABREV9,ABREV10,ABREV11,ABREV12,ABREV13," & _
            " COD_ANA1,COD_ANA2,COD_ANA3,COD_ANA4,COD_ANA5,COD_ANA6,COD_ANA7,COD_ANA8,COD_ANA9,COD_ANA10,COD_ANA11,COD_ANA12,COD_ANA13, nome_computador) " & _
            " VALUES (" & FtVertical.N_Ft & ","
            For j = 1 To 13
                ComandoSql = ComandoSql & IIf(Trim(FtVertical.abrev(j)) = "", "Null", BL_TrataStringParaBD(Trim(FtVertical.abrev(j)))) & ","
            Next j
            For j = 1 To 13
                ComandoSql = ComandoSql & IIf(Trim(FtVertical.codAnalise(j)) = "", "Null", BL_TrataStringParaBD(Trim(FtVertical.codAnalise(j)))) & ","
            Next j
            ComandoSql = left(ComandoSql, Len(ComandoSql) - 1) & ", " & BL_TrataStringParaBD(gComputador) & ")"
            BG_ExecutaQuery_ADO (ComandoSql)
        End Select
        
        
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
    
        FtVertical.N_Ft = CabFt.N_Ft
        
        'Limpa o Vector
        For j = 1 To 13
            FtVertical.abrev(j) = ""
            FtVertical.codAnalise(j) = ""
            FtVertical.SeqUtente(j) = ""
        Next j
        
        'Percorre o RecordSet para seleccionar as an�lises distintas
        RegReq.MoveFirst
        cont = 1
        For i = 0 To total - 1
            'Verifica se o Cod_Agrup da an�lise ainda n�o foi inserido na estrutura
            Repete = False
            
            codAnalise = Trim("" & RegReq!cod_tubo)
            Repete = False
            For j = 1 To 13
                If Trim(FtVertical.codAnalise(j)) = codAnalise Then
                    Repete = True
                End If
            Next j
            If Repete = False Then
                FtVertical.abrev(cont) = "" & RegReq!abrv_tubo
                FtVertical.codAnalise(cont) = "" & codAnalise
                FtVertical.SeqUtente(cont) = "" & SeqUtente
                cont = cont + 1
                
                'N�o actualiza mais an�lises distintas (M�ximo=13)
                If cont > 13 Then
                    UltrapassaLimite = True
                    Exit For
                End If
            End If
            PreenhceEstrutTubos RegReq!n_req, RegReq!cod_ana_s, False
            RegReq.MoveNext
        Next i
            
        ComandoSql = "INSERT INTO " & "SL_CR_FTCV" & _
        " (N_FT,ABREV1,ABREV2,ABREV3,ABREV4,ABREV5,ABREV6,ABREV7,ABREV8,ABREV9,ABREV10,ABREV11,ABREV12,ABREV13, nome_computador) " & _
        " VALUES (" & FtVertical.N_Ft & ","
        For j = 1 To 13
            ComandoSql = ComandoSql & IIf(Trim(FtVertical.abrev(j)) = "", "Null", BL_TrataStringParaBD(Trim(FtVertical.abrev(j)))) & ","
        Next j
        ComandoSql = left(ComandoSql, Len(ComandoSql) - 1) & ", " & BL_TrataStringParaBD(gComputador) & ")"
        BG_ExecutaQuery_ADO (ComandoSql)
        
    End If
    
    
    
    '________________________________________________________________________________________________
    'INICIALIZA A ESTRUTURA COMUM PARA OS 3 MODELOS DE FOLHAS DE TRABALHO
    RegReq.MoveFirst
    While total <> 0
        'N� da Folha de trabalho
        RegFt.N_Ft = CabFt.N_Ft
        'N� da requisi��o
        RegFt.n_req = RegReq!n_req
        'Situa��o da requisi��o
        If disposicao <> gT_Livre And gTipoInstituicao <> "PRIVADA" Then
            RegFt.descr_t_sit = "" & left(RegReq!descr_t_sit, 1)
        ElseIf gTipoInstituicao <> "PRIVADA" Then
            RegFt.descr_t_sit = "" & RegReq!descr_t_sit
        End If
        RegFt.episodio = "" & BL_HandleNull(RegReq!n_epis, "")
        RegFt.Utente = "" & BL_HandleNull(RegReq!Utente, "")
        
        'Urg�ncia da requisi��o
        RegFt.t_urg = "" & RegReq!t_urg
        If disposicao = gT_Livre Then
            If UCase(Trim("" & Mid(RegFt.t_urg, 1, 1))) = "U" Then
                RegFt.t_urg = "Urgente"
            ElseIf UCase(Trim("" & Mid(RegFt.t_urg, 1, 1))) = "L" Then
                RegFt.t_urg = "Linha Azul"
            Else
                RegFt.t_urg = "Normal"
            End If
        End If
        'Proveni�ncia da Requisi��o
        RegFt.descr_proven = Trim("" & RegReq!descr_proven)
        'Nome do utente
        RegFt.nome_ute = RegReq!nome_ute
        'Tipo de utente
        RegFt.t_utente = RegReq!t_utente
        'Idade do utente
        If Not BL_HandleNull(RegReq!dt_nasc_ute, "") = "" Then
            RegFt.idade = BG_CalculaIdade(BL_HandleNull(RegReq!dt_nasc_ute, Date), BL_HandleNull(RegReq!dt_chega, Date))
        End If
        RegFt.DtNascUte = BL_HandleNull(RegReq!dt_nasc_ute, Date)
        
        'Sexo do Utente
        RegFt.sexo_ute = Trim("" & RegReq!descr_sexo)
        If disposicao <> gT_Livre And RegFt.sexo_ute <> "" Then
            RegFt.sexo_ute = left(RegFt.sexo_ute, 1)
        End If
        'Datas da Requisi��o
        RegFt.DtChega = BL_HandleNull(RegReq!dt_chega, "")
        RegFt.DtPretendida = BL_HandleNull(RegReq!dt_pretend, "")
        RegFt.DtEntrega = BL_HandleNull(RegReq!dt_conclusao, "")
        If TipoFolha = "FolhaTrabalhoAna" Then
            RegFt.inf_complementar = BL_HandleNull(RegReq!inf_complementar, "")
        Else
            RegFt.inf_complementar = ""
        End If
        'Aviso da requisi��o
        'Verifica (quando o n� da requisi��o muda) se a requisi��o
        'tem outro tipo de an�lises marcadas (podem ser de outros Grupos de Trabalho!)
        If Cod_Aviso = "" Then
            RegFt.Aviso = "N"
        Else
            Set RegAviso = New ADODB.recordset
            RegAviso.CursorLocation = adUseClient
            RegAviso.LockType = adLockReadOnly
            RegAviso.CursorType = adOpenForwardOnly
            RegAviso.ActiveConnection = gConexao
            If DescrMembros <> 1 Then
                RegAviso.Source = "SELECT N_req FROM sl_marcacoes WHERE N_req= " & _
                                   RegFt.n_req & " AND Cod_agrup=" & BL_TrataStringParaBD(Trim(Cod_Aviso)) & _
                                   " UNION " & _
                                   "SELECT N_req FROM sl_realiza WHERE N_req= " & _
                                   RegFt.n_req & " AND Cod_agrup=" & BL_TrataStringParaBD(Trim(Cod_Aviso))
            Else
                RegAviso.Source = "SELECT N_req FROM sl_marcacoes WHERE N_req= " & _
                                   RegFt.n_req & " AND cod_ana_s=" & BL_TrataStringParaBD(Trim(Cod_Aviso)) & _
                                   " UNION " & _
                                   "SELECT N_req FROM sl_realiza WHERE N_req= " & _
                                   RegFt.n_req & " AND cod_ana_s=" & BL_TrataStringParaBD(Trim(Cod_Aviso))
            End If
            If gModoDebug = mediSim Then BG_LogFile_Erros RegAviso.Source
            RegAviso.Open
            If RegAviso.RecordCount = 0 Then
                RegFt.Aviso = "N"
            Else
                RegFt.Aviso = "S"
            End If
            RegAviso.Close
            Set RegAviso = Nothing
        End If
        'Dados para o Modelo Livre
        If disposicao = gT_Livre Then
            
            UltrapassaLimite = False
            
            'Produtos da requisi��o
            CmdProd.Parameters("N_REQ").value = RegReq!n_req
            Set RegProd = CmdProd.Execute
            Produtos = ""
            While Not RegProd.EOF
                Produtos = Produtos & RegProd!descr_produto & " " & RegProd!descr_especif & ", "
                RegProd.MoveNext
            Wend
            If Len(Produtos) <> 0 Then
                Produtos = left(Produtos, Len(Produtos) - 2) & "."
            End If
            RegProd.Close
            Set RegProd = Nothing
            If ConfigLivre.flg_antibioticos = "1" Then
                antibioticos = DevolveAntibioticos
            Else
                antibioticos = ""
            End If
            'Preenche os dados para o Sub-Relat�rio do Modelo Livre
            If ConfigLivre.flg_inf_cli = "1" Then
            
                'Para os Diagn�sticos Principais
                s = "SELECT Descr_Diag FROM sl_diag,sl_diag_pri WHERE sl_diag.Cod_diag=sl_diag_pri.Cod_diag AND sl_diag_pri.seq_utente=" & RegReq!seq_utente
                Set RegInfClinica = New ADODB.recordset
                RegInfClinica.CursorLocation = adUseClient
                RegInfClinica.LockType = adLockReadOnly
                RegInfClinica.CursorType = adOpenForwardOnly
                RegInfClinica.ActiveConnection = gConexao
                RegInfClinica.Source = s
                If gModoDebug = mediSim Then BG_LogFile_Erros s
                RegInfClinica.Open
                RegFt.Diagn_P = ""
                For i = 1 To RegInfClinica.RecordCount
                    RegFt.Diagn_P = RegFt.Diagn_P & Trim(RegInfClinica!Descr_Diag) & ","
                    RegInfClinica.MoveNext
                Next i
                RegFt.Diagn_P = left(RegFt.Diagn_P, Abs(Len(RegFt.Diagn_P) - 1))
                RegInfClinica.Close
                Set RegInfClinica = Nothing
                
                'Para os Diagn�sticos Secund�rios
                s = "SELECT Descr_Diag FROM sl_diag,sl_diag_sec WHERE sl_diag.Cod_diag=sl_diag_sec.Cod_diag AND sl_diag_sec.N_req=" & RegReq!n_req
                Set RegInfClinica = New ADODB.recordset
                RegInfClinica.CursorLocation = adUseClient
                RegInfClinica.LockType = adLockReadOnly
                RegInfClinica.CursorType = adOpenForwardOnly
                RegInfClinica.ActiveConnection = gConexao
                RegInfClinica.Source = s
                If gModoDebug = mediSim Then BG_LogFile_Erros s
                RegInfClinica.Open
                RegFt.Diagn_S = ""
                For i = 1 To RegInfClinica.RecordCount
                    RegFt.Diagn_S = RegFt.Diagn_S & Trim(RegInfClinica!Descr_Diag) & ","
                    RegInfClinica.MoveNext
                Next i
                RegFt.Diagn_S = left(RegFt.Diagn_S, Abs(Len(RegFt.Diagn_S) - 1))
                RegInfClinica.Close
                Set RegInfClinica = Nothing
                
                'Para as Terap�uticas e Medica��o
                s = "SELECT Descr_tera_med FROM sl_tera_med,sl_tm_ute WHERE sl_tera_med.Cod_tera_med =sl_tm_ute.Cod_tera_med AND sl_tm_ute.N_Req=" & RegReq!n_req
                Set RegInfClinica = New ADODB.recordset
                RegInfClinica.CursorLocation = adUseClient
                RegInfClinica.LockType = adLockReadOnly
                RegInfClinica.CursorType = adOpenForwardOnly
                RegInfClinica.ActiveConnection = gConexao
                RegInfClinica.Source = s
                If gModoDebug = mediSim Then BG_LogFile_Erros s
                RegInfClinica.Open
                RegFt.Tera_Med = ""
                For i = 1 To RegInfClinica.RecordCount
                    RegFt.Tera_Med = RegFt.Tera_Med & Trim(RegInfClinica!descr_tera_med) & ","
                    RegInfClinica.MoveNext
                Next i
                RegFt.Tera_Med = left(RegFt.Tera_Med, Abs(Len(RegFt.Tera_Med) - 1))
                RegInfClinica.Close
                Set RegInfClinica = Nothing
            Else
                RegFt.Diagn_P = ""
                RegFt.Diagn_S = ""
                RegFt.Tera_Med = ""
            End If
        
            If gFolhaTrabElectroforese = EcCodGrTrab Or (gLAB = "CHNE" And EcCodGrTrab = "UM1") Then
                resProteinas = DevolveProteinas(CStr(RegFt.n_req))
            End If
            ComandoSql = "INSERT INTO " & "SL_CR_FTCL" & " (N_FT,N_REQ,DESCR_T_SIT,T_URG,DESCR_PROVEN,SEXO_UTE,T_UTENTE,IDADE_UTE,NOME_UTE,TEM_ANA_AVISO,DIAGN_P,DIAGN_S,TERA_MED,TEXTOLIVRE,PRODUTOS,FLAG_PRODUTOS, RES_PROTEINAS, ANTIBIOTICOS, N_EPIS, UTENTE, nome_computador, DT_NASC_UTE, DT_CHEGA, FLG_MEMBROS, DT_PRETEND, DT_VALIDADE)" & _
                        " VALUES (" & RegFt.N_Ft & "," & RegFt.n_req & "," & IIf(ConfigLivre.flg_situacao = "1" And Trim(RegFt.descr_t_sit) <> "", BL_TrataStringParaBD(RegFt.descr_t_sit), "Null") & "," & IIf(ConfigLivre.flg_urgencia = "1" And Trim(RegFt.t_urg) <> "", BL_TrataStringParaBD(RegFt.t_urg), "Null") & "," & IIf(Trim(RegFt.descr_proven) = "", "Null", BL_TrataStringParaBD(RegFt.descr_proven)) & "," & IIf(ConfigLivre.flg_sexo = "1" And Trim(RegFt.sexo_ute) <> "", BL_TrataStringParaBD(RegFt.sexo_ute), "Null") & "," & BL_TrataStringParaBD(RegFt.t_utente) & ",'" & IIf(ConfigLivre.flg_idade = "1" And Trim(RegFt.idade) <> "", RegFt.idade, "") & "'," & IIf(ConfigLivre.flg_nome = "1" And Trim(RegFt.nome_ute) <> "", BL_TrataStringParaBD(RegFt.nome_ute), "Null") & "," & _
                         BL_TrataStringParaBD(RegFt.Aviso) & "," & IIf(Trim(RegFt.Diagn_P) = "", "Null", BL_TrataStringParaBD(RegFt.Diagn_P)) & "," & IIf(Trim(RegFt.Diagn_S) = "", "Null", BL_TrataStringParaBD(RegFt.Diagn_S)) & "," & IIf(Trim(RegFt.Tera_Med) = "", "Null", BL_TrataStringParaBD(RegFt.Tera_Med)) & "," & IIf(Trim(ConfigLivre.TextoLivre) = "", "Null", BL_TrataStringParaBD(ConfigLivre.TextoLivre)) & "," & IIf(Trim(Produtos) = "", "Null", BL_TrataStringParaBD(Produtos)) & "," & IIf(Trim(Produtos) = "", "Null", "'S'") & ", " & BL_TrataStringParaBD(resProteinas) & "," & BL_TrataStringParaBD(antibioticos) & "," & _
                         BL_TrataStringParaBD(RegFt.episodio) & ", " & BL_TrataStringParaBD(RegFt.Utente) & "," & BL_TrataStringParaBD(gComputador) & "," & BL_TrataDataParaBD(RegFt.DtNascUte) & ", " & BL_TrataDataParaBD(RegFt.DtChega) & "," & ConfigLivre.flg_membros & ", " & BL_TrataDataParaBD(RegFt.DtPretendida) & ", " & BL_TrataDataParaBD(RegFt.DtEntrega) & " )"
            BG_ExecutaQuery_ADO (ComandoSql)
                
        End If
        
        'CAMPOS DAS AN�LISES (VECTOR COM 13 POSI��ES)
        
        'Para cada requisi��o (Linha) preenche as 13 posi��es do vector
        Compara = RegFt.n_req
        valor = Compara
        NRegistosLinha = 0
        Sai = False
        cont = 0
        While Sai = False
            'A an�lise ainda pertence � mesma requisi��o?
            If (valor = Compara) And (RegReq.EOF <> True) Then
                'Incrementa o n� de an�lises inseridas na linha actual
                NRegistosLinha = NRegistosLinha + 1
                
                'Atribui a an�lise da requisi��o � Folha de trabalho corrente.
                
                If TipoFolha = "FolhaTrabalhoAna" Then
                    ComandoSql = "UPDATE sl_marcacoes " & _
                                 "SET N_folha_trab=" & UltimaFT & " " & _
                                 "WHERE N_req=" & RegFt.n_req & " AND " & _
                                 "Cod_perfil='" & RegReq!Cod_Perfil & "' AND " & _
                                 "Cod_ana_c='" & RegReq!cod_ana_c & "' AND " & _
                                 "Cod_ana_s='" & RegReq!cod_ana_s & "'"
                ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
                    ComandoSql = "UPDATE sl_marcacoes " & _
                                 "SET N_folha_tubo=" & UltimaFT & " " & _
                                 "WHERE N_req=" & RegFt.n_req & " AND " & _
                                 "Cod_ana_s='" & RegReq!cod_ana_s & "'"
                End If
                If gImprimirDestino = 1 Then
                    BG_ExecutaQuery_ADO (ComandoSql)
                End If
                
                Select Case disposicao
                    Case gT_Vertical
                        
                        If TipoFolha = "FolhaTrabalhoAna" Then
                            'Se for GHOSTMEMBER n�o vai achar apesar de estarem no cabe�alho
                            codAnalise = Trim("" & RegReq!cod_ana_s)
                            'Se a an�lise pertence a uma complexa, verifica se � a complexa a colocar na folha de trabalho
                            If Trim("" & RegReq!cod_ana_c) <> "0" And DescrMembros <> 1 Then
                                Set RsAnaTrab = New ADODB.recordset
                                With RsAnaTrab
                                    .CursorLocation = adUseClient
                                    .LockType = adLockReadOnly
                                    .CursorType = adOpenForwardOnly
                                    .ActiveConnection = gConexao
                                    .Source = "select * from sl_ana_trab where cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text) & _
                                                " and cod_analise = " & BL_TrataStringParaBD(Trim(RegReq!cod_ana_c))
                                    If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                                    .Open
                                End With
                                If Not RsAnaTrab.EOF Then
                                    codAnalise = Trim(RegReq!cod_ana_c)
                                End If
                            End If
                            'Se a an�lise pertence a um perfil, verifica se � o perfil a colocar na folha de trabalho
                            If Trim("" & RegReq!Cod_Perfil) <> "0" And DescrMembros <> 1 Then
                                Set RsAnaTrab = New ADODB.recordset
                                With RsAnaTrab
                                    .CursorLocation = adUseClient
                                    .LockType = adLockReadOnly
                                    .CursorType = adOpenForwardOnly
                                    .ActiveConnection = gConexao
                                    .Source = "select * from sl_ana_trab where cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text) & _
                                                " and cod_analise = " & BL_TrataStringParaBD(Trim(RegReq!Cod_Perfil))
                                    If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                                    .Open
                                End With
                                If Not RsAnaTrab.EOF Then
                                    codAnalise = Trim(RegReq!Cod_Perfil)
                                End If
                            End If
                        ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
                            codAnalise = Trim("" & RegReq!cod_tubo)
                        End If
                        
                        
                        'Preenche apenas as posi��es do vector que tenham a an�lise do cabe�alho
                        For j = 1 To 13
                            If codAnalise = Trim(FtVertical.codAnalise(j)) Then
                                RegFt.abrev(j) = Trim(FtVertical.abrev(j))
                                BL_DevolveObsAnaReq RegFt.n_req, codAnalise, RegFt.ObsAna(j), seqObsAnaReq
                                Exit For
                            End If
                        Next j
                    Case gT_Horizontal
                        'Preenche sequencialmente as posi��es do vector com as an�lises da requisi��o
                        If cont < 13 Then
                            
                            Repete = False
                            
                            codAnalise = Trim("" & RegReq!cod_ana_s)
                            'Verifica se o CodAgrup j� foi inserido na estrutura=>Caso das an�lises Perfil/Complexas
                            If EcCodGrTrab = "999" Then
                                codAnalise = Trim("" & RegReq!cod_agrup)
                            End If
                            
                            If Trim("" & RegReq!cod_ana_c) <> "0" And DescrMembros <> 1 Then
                                Set RsAnaTrab = New ADODB.recordset
                                With RsAnaTrab
                                    .CursorLocation = adUseClient
                                    .LockType = adLockReadOnly
                                    .CursorType = adOpenForwardOnly
                                    .ActiveConnection = gConexao
                                    .Source = "select * from sl_ana_trab where cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text) & _
                                                " and cod_analise = " & BL_TrataStringParaBD(Trim(RegReq!cod_ana_c))
                                    If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                                    .Open
                                End With
                                If Not RsAnaTrab.EOF Then
                                    codAnalise = Trim(RegReq!cod_ana_c)
                                End If
                            End If
                            
                            'Se a an�lise pertence a um perfil, verifica se � o perfil a colocar na folha de trabalho
                            If Trim("" & RegReq!Cod_Perfil) <> "0" And DescrMembros <> 1 Then
                                Set RsAnaTrab = New ADODB.recordset
                                With RsAnaTrab
                                    .CursorLocation = adUseClient
                                    .LockType = adLockReadOnly
                                    .CursorType = adOpenForwardOnly
                                    .ActiveConnection = gConexao
                                    .Source = "select * from sl_ana_trab where cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab.Text) & _
                                                " and cod_analise = " & BL_TrataStringParaBD(Trim(RegReq!Cod_Perfil))
                                    If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                                    .Open
                                End With
                                If Not RsAnaTrab.EOF Then
                                    codAnalise = Trim(RegReq!Cod_Perfil)
                                End If
                            End If
                                
                            Repete = False
                            For j = 1 To 13
                                If codAnalise = Trim(RegFt.codAnalise(j)) Then
                                    Repete = True
                                End If
                            Next j
                            If Repete = False Then
                                'Incrementa o n� de an�lises j� associadas � requisi��o
                                cont = cont + 1
                                CmdDescrAnalise.Parameters("COD_ANA").value = codAnalise
                                Set RegCmd = CmdDescrAnalise.Execute
                                If Not RegCmd.EOF Then
                                    RegFt.abrev(cont) = BL_HandleNull(RegCmd!abr_ana, "")
                                    RegFt.codAnalise(cont) = codAnalise
                                    RegFt.ResAnt(cont) = BL_UltimoResultado(codAnalise, SeqUtente)
                                    BL_DevolveObsAnaReq RegFt.n_req, codAnalise, RegFt.ObsAna(cont), CLng(seqObsAnaReq)
                                End If
                                RegCmd.Close
                                Set RegCmd = Nothing
                            End If

                        Else
                            UltrapassaLimite = True
                        End If
                    Case gT_Livre
                        resAntLivre = ""
                        'os c�digos das an�lises s�o necess�rios para tratar no Crystal!!
                        If RegReq!cod_ana_s <> gGHOSTMEMBER_S Then
                            resAntLivre = Mid(BL_RetornaResultadoAnterior(CStr(RegFt.n_req), RegFt.DtChega, RegReq!Cod_Perfil, RegReq!cod_ana_c, RegReq!cod_ana_s), 1, 4000)
                        End If
                        If "" & RegReq!cod_ana_s = gGHOSTMEMBER_S Then
                            s = ",'" & RegReq!Cod_Perfil & "','" & RegReq!cod_ana_c & "','0'," & IIf(BL_HandleNull(RegReq!descr_perfis, "") = "", "Null", "'" & RegReq!descr_perfis & "'") & "," & IIf(BL_HandleNull(RegReq!Descr_Ana_C, "") = "", "Null", "'" & RegReq!Descr_Ana_C & "'") & "," & IIf(BL_HandleNull(RegReq!descr_ana_s, "") = "", "Null", "'" & RegReq!descr_ana_s & "'")
                        Else
                            s = ",'" & RegReq!Cod_Perfil & "','" & RegReq!cod_ana_c & "','" & RegReq!cod_ana_s & "'," & IIf(BL_HandleNull(RegReq!descr_perfis, "") = "", "Null", "'" & RegReq!descr_perfis & "'") & "," & IIf(BL_HandleNull(RegReq!Descr_Ana_C, "") = "", "Null", "'" & RegReq!Descr_Ana_C & "'") & "," & IIf(BL_HandleNull(RegReq!descr_ana_s, "") = "", "Null", "'" & RegReq!descr_ana_s & "'")
                        End If
                        s = s & "," & BL_TrataStringParaBD(resAntLivre)
                        
                        'An�lises da Folha de Trabalho Livre
                        ComandoSql = "INSERT INTO " & "SL_CR_FTLV (N_FT,N_REQ,COD_PERFIL,COD_ANA_C,COD_ANA_S,DESCR_PERFIL,DESCR_ANA_C,DESCR_ANA_S,RES_ANT, SEQ_UTENTE,ORD_ANA, nome_computador, inf_complementar )" & _
                                    " VALUES (" & RegFt.N_Ft & "," & RegFt.n_req & s & "," & RegReq!seq_utente & "," & BL_HandleNull(RegReq!Ord_Ana_Compl) & "," & BL_TrataStringParaBD(gComputador) & "," & BL_TrataStringParaBD(RegFt.inf_complementar) & ")"
                        BG_ExecutaQuery_ADO (ComandoSql)
                End Select
                
                RegReq.MoveNext
                If RegReq.EOF <> True Then
                    valor = RegReq!n_req
                End If
            Else
                Sai = True
                If disposicao = gT_Horizontal Then
                    For i = 1 To 13
                        RegFt.codAnalise(i) = ""
                    Next
                End If
            End If
        Wend
       
        If disposicao <> gT_Livre Then
            If gFolhaTrabElectroforese = EcCodGrTrab Or (gLAB = "CHNE" And EcCodGrTrab = "UM1") Then
                resProteinas = DevolveProteinas(CStr(RegFt.n_req))
            End If
            
            'Insere na Tabela Ft o Registo da requisi��o com as an�lises
            ComandoSql = "INSERT INTO " & "SL_CR_FTHV  (N_FT,N_REQ,DESCR_T_SIT,T_URG,DESCR_PROVEN,SEXO_UTE,T_UTENTE,IDADE_UTE,NOME_UTE," & _
            "ABREV1,ABREV2,ABREV3,ABREV4,ABREV5,ABREV6,ABREV7,ABREV8,ABREV9,ABREV10,ABREV11,ABREV12,ABREV13, " & _
            "RES_ANT1, RES_ANT2,RES_ANT3,RES_ANT4,RES_ANT5,RES_ANT6,RES_ANT7,RES_ANT8,RES_ANT9,RES_ANT10,RES_ANT11,RES_ANT12,RES_ANT13, " & _
            "OBS_ANA1, OBS_ANA2,OBS_ANA3,OBS_ANA4,OBS_ANA5,OBS_ANA6,OBS_ANA7,OBS_ANA8,OBS_ANA9,OBS_ANA10,OBS_ANA11,OBS_ANA12,OBS_ANA13, " & _
            " TEM_ANA_AVISO,DT_CHEGA, RES_PROTEINAS, N_EPIS, UTENTE, NOME_COMPUTADOR, DT_PRETEND, DT_VALIDADE) " & _
            " VALUES (" & RegFt.N_Ft & ",'" & RegFt.n_req & "','" & RegFt.descr_t_sit & "','" & RegFt.t_urg & "'," & _
            IIf(Trim(RegFt.descr_proven) = "", "Null", "'" & RegFt.descr_proven & "'") & "," & IIf(Trim(RegFt.sexo_ute) = "", "Null", "'" & RegFt.sexo_ute & "'") & _
            ",'" & RegFt.t_utente & "','" & RegFt.idade & "','" & RegFt.nome_ute & "',"
            
            ' ABREVIATURA DA ANALISE
            For j = 1 To 13
                ComandoSql = ComandoSql & IIf(Trim(RegFt.abrev(j)) = "", "Null", "'" & Trim(RegFt.abrev(j)) & "'") & ","
                RegFt.abrev(j) = ""
                'RegFt.codAnalise(J) = ""
            Next j
            
            ' RESULTADOS ANTERIORES
            For j = 1 To 13
                RegFt.ResAnt(j) = BL_UltimoResultado(FtVertical.codAnalise(j), CLng(BL_HandleNull(FtVertical.SeqUtente(j), -1)))
                ComandoSql = ComandoSql & IIf(Trim(RegFt.ResAnt(j)) = "", "Null", "'" & Trim(RegFt.ResAnt(j)) & "'") & ","
                RegFt.ResAnt(j) = ""
                'FtVertical.codAnalise(J) = ""
                FtVertical.SeqUtente(j) = ""
            Next j
            
            ' OBSERVACOES DA ANALISE
            For j = 1 To 13
                ComandoSql = ComandoSql & IIf(Trim(RegFt.ObsAna(j)) = "", "Null", "'" & Trim(RegFt.ObsAna(j)) & "'") & ","
                RegFt.ObsAna(j) = ""
            Next j
            ComandoSql = ComandoSql & "'" & RegFt.Aviso & "'," & BL_TrataDataParaBD(RegFt.DtChega) & ", " & BL_TrataStringParaBD(resProteinas) & _
            ", " & BL_TrataStringParaBD(RegFt.episodio) & ", " & BL_TrataStringParaBD(RegFt.Utente) & ","
            
            ComandoSql = ComandoSql & BL_TrataStringParaBD(gComputador) & ", " & BL_TrataDataParaBD(RegFt.DtPretendida) & ", " & BL_TrataDataParaBD(RegFt.DtEntrega) & " )"
            
            'Executa as inser��es nas tabelas tempor�rias
            BG_ExecutaQuery_ADO (ComandoSql)
        End If
            
        'Cont � sempre diferente de 0!
        total = total - NRegistosLinha
    Wend
    
    RegReq.Close
    Set RegReq = Nothing
    
    '___________________________________________________________________
    
    
    'Atribui o resto dos dados ao Report=>F�rmulas e Queries
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.WindowShowPrintBtn = False
    Report.WindowShowExportBtn = False
    'F�rmulas do Report
    Report.formulas(0) = "NumFolha=" & BL_TrataStringParaBD("" & CabFt.N_Ft)
    Report.formulas(1) = "DescrGrTrab=" & BL_TrataStringParaBD("" & CabFt.descr_gr_trab)
    Report.formulas(2) = "DescrAnaAviso=" & BL_TrataStringParaBD("" & CabFt.Descr_Ana_Aviso)
    If disposicao = gT_Livre Then
        Report.formulas(3) = "QuebraReq=" & BL_TrataStringParaBD("" & CabFt.Quebra_Req)
    Else
        If UltrapassaLimite = True Then
            Report.formulas(3) = "UltrapassaLimite='Disposi��o da folha de trabalho em uso inadequada: N� de an�lises superior � capacidade do quadro!'"
        Else
            Report.formulas(3) = "UltrapassaLimite=''"
        End If
    End If
    
    If TipoFolha = "FolhaTrabalhoAna" Then
        Report.formulas(4) = "GrupoTrabalho='Grupo Trabalho: '"
    ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
        Report.formulas(4) = "GrupoTrabalho='Grupo An�lises: '"
    End If
    If EcCodGrTrab = gFolhaTrabElectroforese Or (gLAB = "CHNE" And EcCodGrTrab = "UM1") Then
        Report.formulas(5) = "Proteinas='1'"
    End If
    
    Report.formulas(6) = "Descrimina=" & BL_TrataStringParaBD(CStr(DescrMembros))
    
    'SQlQuery's do Report
    Select Case disposicao
        'FT.DataFiles(0) = "SISLAB.FTDINAMICO" & gNumeroSessao
        'FT.DataFiles(1) = "SISLAB.FTLIVREANALISES" & gNumeroSessao
        Case gT_Vertical
            Report.SQLQuery = " SELECT FTVERTICAL.ABREV1,FTVERTICAL.ABREV2,FTVERTICAL.ABREV3,FTVERTICAL.ABREV4,FTVERTICAL.ABREV5,FTVERTICAL.ABREV6,FTVERTICAL.ABREV7,FTVERTICAL.ABREV8,FTVERTICAL.ABREV9,FTVERTICAL.ABREV10,FTVERTICAL.ABREV11,FTVERTICAL.ABREV12,FTVERTICAL.ABREV13, " & _
                              " FTMODELO.N_REQ,FTMODELO.DESCR_T_SIT,FTMODELO.T_URG,FTMODELO.DESCR_PROVEN,FTMODELO.SEXO_UTE,FTMODELO.T_UTENTE,FTMODELO.IDADE_UTE,FTMODELO.NOME_UTE,FTMODELO.ABREV1,FTMODELO.ABREV2,FTMODELO.ABREV3,FTMODELO.ABREV4,FTMODELO.ABREV5,FTMODELO.ABREV6,FTMODELO.ABREV7, FTMODELO.ABREV8,FTMODELO.ABREV9,FTMODELO.ABREV10,FTMODELO.ABREV11,FTMODELO.ABREV12,FTMODELO.ABREV13,FTMODELO.TEM_ANA_AVISO, FTMODELO.RES_PROTEINAS " & _
                              " FROM SL_CR_FTCV " & " FTVERTICAL , SL_CR_FTHV " & " FTMODELO WHERE FTVERTICAL.N_FT = FTMODELO.N_FT AND FTVERTICAL.NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador) & " AND FTMODELO.nome_computador = " & BL_TrataStringParaBD(gComputador) & " ORDER BY n_Req"
        Case gT_Horizontal
            Report.SQLQuery = "SELECT N_REQ,DESCR_T_SIT,T_URG,DESCR_PROVEN,SEXO_UTE,T_UTENTE,IDADE_UTE,NOME_UTE,ABREV1,ABREV2,ABREV3,ABREV4,ABREV5,ABREV6,ABREV7,ABREV8,ABREV9,ABREV10,ABREV11,ABREV12,ABREV13,DESCR_GR_TRAB,DESCR_ANA_AVISO, SL_CR_FTHV.RES_PROTEINAS FROM SL_CR_FTHV FTMODELO WHERE  FTMODELO.nome_computador = " & BL_TrataStringParaBD(gComputador) & " ORDER BY n_req "
            
            CmdDlgPrint.Orientation = cdlLandscape '???
        Case gT_Livre
            If ConfigLivre.Num_Ult_Res = 0 Then
                Report.SQLQuery = "SELECT FTDINAMICO.N_REQ,FTDINAMICO.DESCR_T_SIT,FTDINAMICO.T_URG,FTDINAMICO.IDADE_UTE,FTDINAMICO.NOME_UTE," & _
                                  "FTDINAMICO.DIAGN_P,FTDINAMICO.DIAGN_S,FTDINAMICO.TERA_MED,FTDINAMICO.TEXTOLIVRE, FTDINAMICO.PRODUTOS, FTDINAMICO.FLAG_PRODUTOS," & _
                                  "FTLIVREANALISES.DESCR_PERFIL,FTLIVREANALISES.DESCR_ANA_C,FTLIVREANALISES.DESCR_ANA_S, FTDINAMICO.DT_CHEGA" & _
                                  " FROM SL_CR_FTCL" & " FTDINAMICO,SL_CR_FTLV" & " FTLIVREANALISES" & _
                                  " WHERE FTDINAMICO.N_FT=FTLIVREANALISES.N_FT AND FTDINAMICO.N_REQ=FTLIVREANALISES.N_REQ " & _
                                  " AND FTDINAMICO.nome_computador = " & BL_TrataStringParaBD(gComputador) & _
                                  " ORDER BY FTDINAMICO.N_REQ ASC,FTLIVREANALISES.DESCR_PERFIL ASC,FTLIVREANALISES.DESCR_ANA_C ASC,FTLIVREANALISES.DESCR_ANA_S ASC"
            Else
                'O REPORT ACRESCENTA FILTRA OS REGISTOS NO WHERE POR AN�LISE!
                sql = "SELECT " & _
                    "FTDINAMICO.N_REQ, FTDINAMICO.DESCR_T_SIT, FTDINAMICO.T_URG, FTDINAMICO.SEXO_UTE, FTDINAMICO.IDADE_UTE, FTDINAMICO.NOME_UTE, FTDINAMICO.TEM_ANA_AVISO, FTDINAMICO.DIAGN_P, FTDINAMICO.DIAGN_S, FTDINAMICO.TERA_MED, FTDINAMICO.TEXTOLIVRE, FTDINAMICO.PRODUTOS, FTDINAMICO.FLAG_PRODUTOS," & _
                    "FTLIVREANALISES.N_REQ, FTLIVREANALISES.COD_PERFIL, FTLIVREANALISES.COD_ANA_C, FTLIVREANALISES.COD_ANA_S, FTLIVREANALISES.DESCR_PERFIL, FTLIVREANALISES.DESCR_ANA_C, FTLIVREANALISES.DESCR_ANA_S," & _
                    "SLV_RESULT_ANT.N_REQ, SLV_RESULT_ANT.DT_VAL, SLV_RESULT_ANT.FLG_UNID_ACT1, SLV_RESULT_ANT.UNID_1_RES1, SLV_RESULT_ANT.UNID_2_RES1, SLV_RESULT_ANT.FLG_UNID_ACT2, SLV_RESULT_ANT.UNID_1_RES2, SLV_RESULT_ANT.UNID_2_RES2," & _
                    "SLV_RESULT_VAL.SEQ_REALIZA , SLV_RESULT_VAL.N_Res, SLV_RESULT_VAL.ResNum, SLV_RESULT_VAL.RESFRASE, SLV_RESULT_VAL.QUANTMICRO, SLV_RESULT_VAL.DescrMicro, SLV_RESULT_VAL.Sensib, SLV_RESULT_VAL.CMI " & _
                    "FROM " & _
                    "SL_CR_FTCL " & gNumeroSessao & " FTDINAMICO," & _
                    "SL_CR_FTLV " & gNumeroSessao & " FTLIVREANALISES," & _
                    "SLV_RESULT_ANT SLV_RESULT_ANT," & _
                    "SLV_RESULT_VAL SLV_RESULT_VAL " & _
                    "WHERE " & _
                    "FTDINAMICO.N_FT=FTLIVREANALISES.N_FT AND " & _
                    "FTDINAMICO.N_REQ=FTLIVREANALISES.N_REQ "
                    Select Case gSGBD
                        Case gOracle
                            sql = sql & "AND FTLIVREANALISES.SEQ_UTENTE=SLV_RESULT_ANT.SEQ_UTENTE (+) " & _
                                        "AND FTLIVREANALISES.COD_PERFIL=SLV_RESULT_ANT.COD_PERFIL (+) " & _
                                        "AND FTLIVREANALISES.COD_ANA_C=SLV_RESULT_ANT.COD_ANA_C (+) " & _
                                        "AND FTLIVREANALISES.COD_ANA_S=SLV_RESULT_ANT.COD_ANA_S (+) " & _
                                        "AND (" & _
                                        "     SLV_RESULT_ANT.SEQ_REALIZA IS NULL " & _
                                        "     OR " & _
                                        "     (" & _
                                        "      SLV_RESULT_ANT.SEQ_REALIZA IS NOT NULL " & _
                                        "      AND " & _
                                        "      SLV_RESULT_ANT.SEQ_REALIZA IN " & _
                                        "      (SELECT SEQ_REALIZA " & _
                                        "      FROM SLV_RESULT_ANT A " & _
                                        "      WHERE " & _
                                        "      ROWNUM<=" & ConfigLivre.Num_Ult_Res & " AND " & _
                                        "      A.SEQ_UTENTE=FTLIVREANALISES.SEQ_UTENTE AND " & _
                                        "      A.COD_PERFIL=FTLIVREANALISES.COD_PERFIL AND " & _
                                        "      A.COD_ANA_C=FTLIVREANALISES.COD_ANA_C AND " & _
                                        "      A.COD_ANA_S=FTLIVREANALISES.COD_ANA_S) " & _
                                        "     )" & _
                                        "    ) " & _
                                        "AND SLV_RESULT_ANT.SEQ_REALIZA=SLV_RESULT_VAL.SEQ_REALIZA(+) "
                                        
                        Case gInformix
                                    
                        Case gSqlServer
                            sql = sql & "AND FTLIVREANALISES.SEQ_UTENTE*=SLV_RESULT_ANT.SEQ_UTENTE " & _
                                        "AND FTLIVREANALISES.COD_PERFIL*=SLV_RESULT_ANT.COD_PERFIL " & _
                                        "AND FTLIVREANALISES.COD_ANA_C*=SLV_RESULT_ANT.COD_ANA_C " & _
                                        "AND FTLIVREANALISES.COD_ANA_S*=SLV_RESULT_ANT.COD_ANA_S " & _
                                        "AND (" & _
                                        "     SLV_RESULT_ANT.SEQ_REALIZA IS NULL " & _
                                        "     OR " & _
                                        "     (" & _
                                        "      SLV_RESULT_ANT.SEQ_REALIZA IS NOT NULL " & _
                                        "      AND " & _
                                        "      SLV_RESULT_ANT.SEQ_REALIZA IN " & _
                                        "      (SELECT TOP " & ConfigLivre.Num_Ult_Res & " SEQ_REALIZA " & _
                                        "      FROM SLV_RESULT_ANT A " & _
                                        "      WHERE " & _
                                        "      A.SEQ_UTENTE=FTLIVREANALISES.SEQ_UTENTE AND " & _
                                        "      A.COD_PERFIL=FTLIVREANALISES.COD_PERFIL AND " & _
                                        "      A.COD_ANA_C=FTLIVREANALISES.COD_ANA_C AND " & _
                                        "      A.COD_ANA_S=FTLIVREANALISES.COD_ANA_S) " & _
                                        "     )" & _
                                        "    ) " & _
                                        "AND SLV_RESULT_ANT.SEQ_REALIZA*=SLV_RESULT_VAL.SEQ_REALIZA "
                        
                    End Select
                    sql = sql & "ORDER BY " & _
                                "FTDINAMICO.N_REQ ASC," & _
                                "FTLIVREANALISES.COD_PERFIL ASC,FTLIVREANALISES.COD_ANA_C ASC, FTLIVREANALISES.COD_ANA_S ASC," & _
                                "SLV_RESULT_ANT.DT_VAL DESC," & _
                                "SLV_RESULT_VAL.SEQ_REALIZA,SLV_RESULT_VAL.N_RES,SLV_RESULT_VAL.ORDFRASE,SLV_RESULT_VAL.COD_MICRO"
                    Report.SQLQuery = sql
            End If
            
    End Select
    
    'Imprime o Report
    If BL_ExecutaReport = False And gImprimirDestino = 1 Then
        'EM CASO DE ERRO DESFAZ A FOLHA DE TRABALHO PARA SE PODER VOLTAR A IMPRIMIR!!
        'N�O D� PARA AMBIENTE TRANNSACCIONAL POIS O CRYSTAL ABRE OUTRA SESS�O NA BD!!
        If TipoFolha = "FolhaTrabalhoAna" Then
            sql = "UPDATE sl_marcacoes " & _
                  "SET N_folha_trab=0 " & _
                  "WHERE N_folha_trab=" & UltimaFT
        ElseIf TipoFolha = "FolhaTrabalhoTubo" Then
            sql = "UPDATE sl_marcacoes " & _
                  "SET N_folha_trab=0 " & _
                  "WHERE N_folha_tubo=" & UltimaFT
        End If
        Call BG_ExecutaQuery_ADO(sql)
    End If
    
    '****************************************************************
    
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Select Case disposicao
        Case gT_Horizontal
            sql = " Delete From SL_CR_FTHV  WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
        Case gT_Vertical
            sql = " Delete From SL_CR_FTHV  WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
            sql = " Delete From SL_CR_FTCV  WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
        Case gT_Livre
            sql = "DELETE FROM SL_CR_FTCL WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
            sql = "DELETE FROM SL_CR_FTLV WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
            BG_ExecutaQuery_ADO sql
    End Select
    
    ' IMPRIME ETIQUETAS
    '****************************************************************
        
    If gImprimirDestino = 1 And CkImprimeEtiquetas.value = vbChecked Then
               
        For conta = 1 To UBound(Tubos)
            If TipoFolha = "FolhaTrabalhoAna" Then
                ImprimeEtiq Tubos(conta).n_req, Tubos(conta).descR_tubo, True
            Else
                ImprimeEtiq Tubos(conta).n_req, Tubos(conta).descR_tubo, False
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FormFolhasTrab: " & Err.Description & " " & sql
    Exit Sub
    Resume Next
End Sub
    



Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String _
    ) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", RemovePortuguese(StrConv(nome_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function


Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function



Sub ImprimeEtiq(NReq As String, descR_tubo As String, tubo_primario As Boolean)
              
    On Error GoTo ErrorHandler
    
    Dim i As Integer
    Dim Report As CrystalReport
    Dim continua As Boolean
    Dim sql As String
    Dim Sql2 As String
    Dim rs As ADODB.recordset
    Dim RS2 As ADODB.recordset
    Dim n_req As String
    Dim N_req_bar As String
    Dim situacao As String
    Dim episodio As String
    Dim Criterio As String
    Dim idade As String
    Dim EtqCrystal As String
    
    If Trim(NReq) = "" Then
        Exit Sub
    End If
    
    NReq = Trim(NReq)
    N_req_bar = Right("0000000" & NReq, 7)
        
    Criterio = ""
    
    If (Len(descR_tubo) > 0) Then
        Criterio = " AND T.descr_tubo = '" & descR_tubo & "'"
    End If
    
    ' Preenche a tabela secund�ria.
        
    sql = "SELECT DISTINCT " & vbCrLf & _
          "    U.t_utente, U.utente, U.n_proc_1, R.n_req, " & vbCrLf & _
          "    R.t_urg, R.dt_chega, 'ORDEM', U.nome_ute, " & vbCrLf & _
          "    'N_REQ_BAR', " & vbCrLf & _
          "    T.descr_tubo,R.t_sit, R.n_epis, R.req_aux, U.dt_nasc_ute, R.dt_previ, T.cod_etiq, T.inf_complementar  " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_marcacoes M, " & vbCrLf & _
          "    sl_ana_s     A, " & vbCrLf & _
          "    sl_requis    R, " & vbCrLf & _
          "    sl_identif   U, " & vbCrLf & _
          "    sl_gr_ana    G, " & vbCrLf & ""
    
    If tubo_primario = True Then
        sql = sql & "   sl_tubo  T " & vbCrLf & ""
    Else
        sql = sql & "   sl_tubo_sec  T " & vbCrLf & ""
    End If
    
    sql = sql & _
          "WHERE " & vbCrLf & _
          "    M.n_req        = " & NReq & "          AND " & vbCrLf & _
          "    R.n_req        = M.n_req       AND " & vbCrLf & _
          "    A.cod_ana_s    = M.cod_ana_s   AND " & vbCrLf & _
          "    R.seq_utente   = U.seq_utente  AND " & vbCrLf & _
          "    A.gr_ana       = G.cod_gr_ana  AND " & vbCrLf

    If tubo_primario = True Then
        sql = sql & "   A.cod_tubo = T.cod_tubo     " & vbCrLf & ""
    Else
        sql = sql & "   A.cod_tubo_sec = T.cod_tubo     " & vbCrLf & ""
    End If
    sql = sql & Criterio
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    rs.MoveFirst
    While (Not rs.EOF)
            
        ' ---------------------------------
        If BL_HandleNull(rs!dt_nasc_ute, "") <> "" And BL_HandleNull(rs!dt_previ, "") <> "" And BL_TrataDataParaBD(BL_HandleNull(rs!dt_nasc_ute, "")) <> BL_TrataDataParaBD("01/01/1800") Then
            idade = CStr(BG_CalculaIdade(rs!dt_nasc_ute, rs!dt_previ))
        End If
        
        ' Se o epis�dio for igual ao n�mero de requisi��o, n�o sai na etiqueta.
        n_req = Trim(BL_HandleNull(rs!n_req, ""))
        situacao = Trim(BL_HandleNull(rs!t_sit, ""))
        episodio = Trim(BL_HandleNull(rs!n_epis, ""))
        
        If (n_req = episodio) Then
            situacao = ""
            episodio = ""
        End If
        
        ' ---------------------------------
            
        Select Case situacao
        
            Case gT_Urgencia
                situacao = "URG"
            Case gT_Consulta
                situacao = "CON"
            Case gT_Internamento
                situacao = "INT"
            Case gT_Externo
                situacao = "HDI"
            Case gT_LAB
                situacao = "LAB"
            Case gT_RAD
                situacao = "RAD"
            Case gT_Bloco
                situacao = "BLO"
            Case Else
                situacao = ""
        
        End Select
        
        ' ---------------------------------
        If Not LerEtiqInI Then
            MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
            Exit Sub
        End If
        If Not EtiqOpenPrinter(EcPrinterEtiq) Then
            MsgBox "Imposs�vel abrir impressora etiquetas"
            Exit Sub
        End If
        
'            Call EtiqPrint(Tubos(i).GrAna, Tubos(i).AbrAna, _
'                           CbTipoUtente.Text, EcUtente, _
'                           EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2, _
'                           EcNumReq, IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.Text, 1, 3), " "), _
'                           EcDataPrevista, _
'                           Tubos(i).CodProd & IIf(Tubos(i).codTubo <> "", " (", " ") & Tubos(i).codTubo & IIf(Tubos(i).codTubo <> "", ")", ""), _
'                           EcNome, Tubos(i).CodTuboBar & NumReqBar, _
'                           BG_CvPlica(Tubos(i).Designacao_tubo))
        
        Call EtiqPrint("", "", _
                BL_HandleNull(rs!t_utente, ""), BL_HandleNull(rs!Utente, ""), _
                BL_HandleNull(rs!n_proc_1, ""), BL_HandleNull(rs!n_req, ""), _
                BL_HandleNull(rs!t_urg, ""), BL_HandleNull(rs!dt_chega, ""), "", _
                BL_HandleNull(rs!nome_ute, ""), BL_HandleNull(rs!cod_etiq, "") & N_req_bar, BL_HandleNull(rs!descR_tubo, ""), BL_HandleNull(rs!inf_complementar, ""))
        
        EtiqClosePrinter
        rs.MoveNext
        
    Wend
    
    rs.Close
    Set rs = Nothing
    
     
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : ImprimeEtiq (FormSepSoros) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub



Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function

Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function


Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo Erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While aux <> "P1"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function
Erro:
End Function

Sub PreenhceEstrutTubos(n_req As String, cod_ana_s As String, tubo_primario As Boolean)
              
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim conta As Integer
    ' -----------------------------------------------
    ' Preenche a grid.
    
    sql = "SELECT DISTINCT " & vbCrLf & _
          "    T.descr_tubo, " & vbCrLf & _
          "    T.cod_tubo,  " & vbCrLf & _
          "    A.flg_etiq_prod, " & vbCrLf & _
          "    A.flg_etiq_ord " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_marcacoes M, " & vbCrLf & _
          "    sl_ana_s     A, " & vbCrLf
    If tubo_primario = True Then
        sql = sql & "    sl_tubo  T " & vbCrLf
    Else
        sql = sql & "    sl_tubo_sec  T " & vbCrLf
    End If
    sql = sql & "WHERE " & vbCrLf & _
          "    M.n_req        = " & n_req & " AND " & vbCrLf & _
          "    A.cod_ana_s        = " & BL_TrataStringParaBD(cod_ana_s) & " AND " & vbCrLf & _
          "    A.cod_ana_s    = M.cod_ana_s AND " & vbCrLf
    If tubo_primario = True Then
        sql = sql & "    A.cod_tubo = T.cod_tubo "
    Else
        sql = sql & "    A.cod_tubo_sec = T.cod_tubo "
    End If
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    

    While (Not rs.EOF)
        If BL_HandleNull(rs!cod_tubo, "") <> "" Then
            For conta = 1 To UBound(Tubos)
                If Tubos(conta).cod_tubo = rs!cod_tubo And Tubos(conta).n_req = n_req Then
                    rs.Close
                    Set rs = Nothing
                    Exit Sub
                End If
            Next
            ReDim Preserve Tubos(UBound(Tubos) + 1)
            Tubos(UBound(Tubos)).cod_tubo = rs!cod_tubo
            Tubos(UBound(Tubos)).descR_tubo = rs!descR_tubo
            Tubos(UBound(Tubos)).n_req = n_req
        End If
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
     
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : PreencheEstrutTubos (FormFolhasTrab) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
            Resume Next
    End Select
End Sub

Private Function DevolveProteinas(n_req As String) As String
    Dim sSql As String
    Dim RsRes As New ADODB.recordset
    
    If gCodLocal = 2 And gLAB = "CHNE" Then
        sSql = "SELECT sl_res_alfan.result FROM sl_realiza, sl_res_alfan "
        sSql = sSql & " WHERE sl_realiza.seq_realiza = sl_res_alfan.seq_realiza"
        sSql = sSql & " AND sl_realiza.cod_ana_s IN( " & BL_TrataStringParaBD("SM13091") & ", " & BL_TrataStringParaBD("SA1309") & ")"
        sSql = sSql & " AND sl_realiza.n_req = " & n_req
    Else
        sSql = "SELECT sl_res_alfan.result FROM sl_realiza, sl_res_alfan "
        sSql = sSql & " WHERE sl_realiza.seq_realiza = sl_res_alfan.seq_realiza"
        sSql = sSql & " AND sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(gCodAnaProteinas)
        sSql = sSql & " AND sl_realiza.n_req = " & n_req
    End If
    RsRes.CursorType = adOpenStatic
    RsRes.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        DevolveProteinas = BL_HandleNull(RsRes!result, "")
    Else
        DevolveProteinas = ""
    End If
    RsRes.Close
    Set RsRes = Nothing
End Function


Private Function DevolveAntibioticos()
    Dim sSql As String
    Dim rsTSQ As New ADODB.recordset
    Dim aux As String
    Dim contador As Integer
    
    sSql = "SELECT * FROM sl_antibio WHERE flg_imprime_folhatrab = 1 "
    
    rsTSQ.CursorType = adOpenStatic
    rsTSQ.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTSQ.Open sSql, gConexao
    aux = ""
    If rsTSQ.RecordCount > 0 Then
        contador = 0
        While Not rsTSQ.EOF
            aux = aux & BL_HandleNull(rsTSQ!descr_antibio, "") & Space(40 - Len(BL_HandleNull(rsTSQ!descr_antibio, "")))
            contador = contador + 1
            If contador Mod 3 = 0 Then
                aux = aux & vbCrLf
            End If
            rsTSQ.MoveNext
        Wend
    End If
    rsTSQ.Close
    Set rsTSQ = Nothing
    DevolveAntibioticos = aux
End Function




Private Sub RegistaImprFolhaTrab(CodGrupo As String, SeqImpr As Long, flg_gerou As Boolean)
    Dim sSql As String
    If gImprimirDestino <> 1 Then Exit Sub
    If flg_gerou = True Then
        sSql = "INSERT INTO sl_impr_folhas_trab(seq_impr, cod_grupo, user_imp, dt_imp, hr_imp) VALUES("
        sSql = sSql & SeqImpr & ", " & BL_TrataStringParaBD(CodGrupo) & ", " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & " )"
    Else
        sSql = "UPDATE sl_impr_folhas_trab SET "
        sSql = sSql & " user_imp2 = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & " dt_imp2 = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", hr_imp2 = " & BL_TrataStringParaBD(Bg_DaHora_ADO)
        sSql = sSql & " WHERE seq_impr = " & SeqImpr & " AND cod_grupo  = " & BL_TrataStringParaBD(CodGrupo)
    End If
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
End Sub






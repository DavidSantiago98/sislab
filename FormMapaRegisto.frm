VERSION 5.00
Begin VB.Form FormMapaRegisto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMapaRegisto"
   ClientHeight    =   3285
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5505
   Icon            =   "FormMapaRegisto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3285
   ScaleWidth      =   5505
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtPesquisaLocalEntrega 
      Height          =   315
      Left            =   4920
      Picture         =   "FormMapaRegisto.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1920
      Width           =   375
   End
   Begin VB.ListBox EcListaLocalEntrega 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1200
      TabIndex        =   12
      Top             =   1920
      Width           =   3735
   End
   Begin VB.OptionButton Opt1 
      Caption         =   "Data Cria��o"
      Height          =   255
      Index           =   2
      Left            =   3600
      TabIndex        =   11
      Top             =   2760
      Width           =   1455
   End
   Begin VB.OptionButton Opt1 
      Caption         =   "Data Chegada"
      Height          =   255
      Index           =   1
      Left            =   2040
      TabIndex        =   10
      Top             =   2760
      Width           =   1455
   End
   Begin VB.OptionButton Opt1 
      Caption         =   "Data Prevista"
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   9
      Top             =   2760
      Width           =   1455
   End
   Begin VB.CommandButton BtPesquisaPostos 
      Height          =   315
      Left            =   4920
      Picture         =   "FormMapaRegisto.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
      Top             =   1200
      Width           =   375
   End
   Begin VB.ListBox ListaPostos 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1200
      MultiSelect     =   2  'Extended
      TabIndex        =   6
      Top             =   1200
      Width           =   3720
   End
   Begin VB.ComboBox CbLocal 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   720
      Width           =   3735
   End
   Begin VB.TextBox EcDataInicio 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
   Begin VB.TextBox EcDataFim 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3960
      TabIndex        =   1
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Local Entrega"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   14
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Postos"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   8
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "Local"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicio"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Data Fim"
      Height          =   255
      Left            =   3120
      TabIndex        =   2
      Top             =   240
      Width           =   735
   End
End
Attribute VB_Name = "FormMapaRegisto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

'Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Private Type GR
    cod_grupo As String
    conta As Integer
    tamanho As Integer
End Type
Dim grupos() As GR
Dim ContaGR As Integer


Dim rsGrupo As ADODB.recordset
'Variavel que guarda os grupos
Dim STRGrupos As String

'Variavel que guarda os produtos de uma requisi��o
Dim STRProduto As String
'Variavel (STRING) que guarda a contagem dos grupos para uma requisi��o
Dim STRContagrupos As String

Dim STRObsRegistos As String

Dim totalAna As Integer
Dim STRCodSala As String

Dim CmdNome As New ADODB.Command
Dim PmtNome As ADODB.Parameter
Dim RsNome As ADODB.recordset

Dim CmdSit As New ADODB.Command
Dim PmtSit As ADODB.Parameter
Dim RsSit As ADODB.recordset

Dim CmdProven As New ADODB.Command
Dim PmtProven As ADODB.Parameter
Dim RsProven As ADODB.recordset

Private Sub EcDataFim_GotFocus()
    
    If Trim(EcDataFim.Text) = "" Then
        EcDataFim.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataInicio_GotFocus()
    
    If Trim(EcDataInicio.Text) = "" Then
        EcDataInicio.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataInicio_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInicio)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcListaLocalEntrega_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocalEntrega, KeyCode, Shift
End Sub
Private Sub BtPesquisaLocalEntrega_Click()
    PA_PesquisaLocalEntregaMultiSel EcListaLocalEntrega
End Sub
Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    'DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    If Not RsNome Is Nothing Then
        RsNome.Close
        Set RsNome = Nothing
    End If
    If Not RsProven Is Nothing Then
        RsProven.Close
        Set RsProven = Nothing
    End If
    If Not RsSit Is Nothing Then
        RsSit.Close
        Set RsSit = Nothing
    End If
    Set CmdNome = Nothing
    Set CmdProven = Nothing
    Set CmdSit = Nothing

    Set FormMapaRegisto = Nothing
    
    Call BL_FechaPreview("Mapa Registo")
     
End Sub

Sub Inicializacoes()

    Me.caption = " Mapa de Registo Global"
    Me.left = 540
    Me.top = 450
    Me.Width = 5535
    Me.Height = 3720 ' Normal
    'Me.Height = 2330 ' Campos Extras
    
    'Set CampoDeFocus = EcCodigo
    
    'inicializar o vector de grupos
     ReDim grupos(0)
    
    'Inicializa��o do Comando que vai buscar o nome do utente usando o sequencial do utente na requisi��o
    Set CmdNome.ActiveConnection = gConexao
    CmdNome.CommandType = adCmdText
    CmdNome.CommandText = "SELECT nome_ute,utente,t_utente FROM sl_identif WHERE seq_utente=?"
    CmdNome.Prepared = True
    Set PmtNome = CmdNome.CreateParameter("SEQ_UTENTE", adInteger, adParamInput)
    CmdNome.Parameters.Append PmtNome
    
    'Inicializa��o do comando que vai buscar o c�digo da situa��o de uma requisi��o
    Set CmdSit.ActiveConnection = gConexao
    CmdSit.CommandType = adCmdText
    CmdSit.CommandText = "SELECT descr_t_sit FROM sl_tbf_t_sit WHERE cod_t_sit=?"
    CmdSit.Prepared = True
    Set PmtSit = CmdSit.CreateParameter("COD_T_SIT", adInteger, adParamInput)
    CmdSit.Parameters.Append PmtSit
     
    'Inicializa��o do camoando que vai buscar a descri��o da proveni�ncia
    Set CmdProven.ActiveConnection = gConexao
    CmdProven.CommandType = adCmdText
    If gTipoInstituicao = "HOSPITALAR" Then
        CmdProven.CommandText = "SELECT descr_proven FROM sl_proven WHERE cod_proven=?"
        CmdProven.Prepared = True
        Set PmtProven = CmdProven.CreateParameter("COD_PROVEN", adVarChar, adParamInput, 10)
        CmdProven.Parameters.Append PmtProven
    ElseIf gTipoInstituicao = "PRIVADA" Then
        CmdProven.CommandText = "SELECT descr_sala descr_proven FROM sl_cod_salas WHERE cod_sala=?"
        CmdProven.Prepared = True
        Set PmtProven = CmdProven.CreateParameter("COD_SALA", adVarChar, adParamInput, 10)
        CmdProven.Parameters.Append PmtProven
    End If

End Sub

Sub PreencheValoresDefeito()
    
    Dim sql As String
    
    EcDataInicio.Tag = adDate
    EcDataFim.Tag = adDate
    
    EcDataInicio.Text = Bg_DaData_ADO
    EcDataFim.Text = Bg_DaData_ADO
    
    'Concatenar os Grupos de an�lises
    ReDim grupos(1)
    
    STRGrupos = ""
    ContaGR = 0
    ReDim grupos(ContaGR)
    grupos(0).cod_grupo = ""
    grupos(0).conta = 0
    grupos(0).tamanho = 0
    
    sql = "SELECT cod_gr_ana FROM sl_gr_ana"
    Set rsGrupo = New ADODB.recordset
    rsGrupo.CursorLocation = adUseServer
    rsGrupo.CursorType = adOpenStatic
    rsGrupo.Open sql, gConexao
    While Not rsGrupo.EOF
        STRGrupos = STRGrupos + rsGrupo!cod_gr_ana + " "
        ContaGR = ContaGR + 1
        ReDim Preserve grupos(ContaGR)
        grupos(ContaGR).cod_grupo = Trim(rsGrupo!cod_gr_ana)
        grupos(ContaGR).conta = 0
        grupos(ContaGR).tamanho = Len(Trim(rsGrupo!cod_gr_ana))
        rsGrupo.MoveNext
    Wend
    STRGrupos = STRGrupos + "TOT"
    rsGrupo.Close
    Set rsGrupo = Nothing
    BG_PreencheComboBD_ADO "gr_empr_inst", "empresa_id", "nome_empr", CbLocal
    Opt1(0).value = True
End Sub

Sub FuncaoLimpar()
    
    EcDataInicio.SetFocus
    Opt1(0).value = True
    EcListaLocalEntrega.Clear
End Sub

Sub LimpaListaGrupos()
    
    Dim i As Integer
    
    For i = 0 To ContaGR
        grupos(i).conta = 0
    Next i

End Sub

Sub INSERT_TabelaTemporaria()

    Dim sql As String
    Dim sSql As String
    Dim rsURG As New ADODB.recordset
    Dim TipoUtente As String
    Dim Proven As String
    Dim A
    Dim urg As String
    Dim idade As String
    Dim Sexo As String
    
    CmdSit.Parameters(0).value = BL_HandleNull(rs!t_sit, 4)
    Set RsSit = CmdSit.Execute
        
    If BL_HandleNull(rs!descr_proven, "") <> "" Then
        Proven = BL_HandleNull(rs!descr_proven, "")
    Else
        Proven = "NULO"
    End If
    TipoUtente = BL_HandleNull(rs!Utente, "") + "/" + BL_HandleNull(rs!t_utente, "")
    urg = BL_HandleNull(rs!descr_t_urg, "")
    idade = BG_CalculaIdade(BL_HandleNull(rs!dt_nasc_ute, ""), BL_HandleNull(rs!dt_chega, Bg_DaData_ADO))
    If BL_HandleNull(rs!sexo_ute, "") = gT_Masculino Then
        Sexo = "M"
    ElseIf BL_HandleNull(rs!sexo_ute, "") = gT_Feminino Then
        Sexo = "F"
    Else
        Sexo = "I"
    End If
    sql = "INSERT INTO SL_CR_MR (n_req,nome_ute,tipo_ute,proveniencia,situacao,produto,contagem_grupos,empr_id, "
    sql = sql & " T_URG, NOME_COMPUTADOR, NUM_SESSAO, OBS_REGISTO, TOTAL_ANA, COD_SALA,sexo, idade, cod_local_entrega, descr_local_entrega)"
    sql = sql & " VALUES(" & rs!n_req & "," & BL_TrataStringParaBD(rs!nome_ute) & "," & BL_TrataStringParaBD(TipoUtente)
    sql = sql & "," & IIf((Proven = "NULO"), "null", BL_TrataStringParaBD(Proven)) & ","
    sql = sql & BL_TrataStringParaBD(Mid(BL_HandleNull(RsSit!descr_t_sit, 4), 1, 1)) & "," & BL_TrataStringParaBD(STRProduto)
    sql = sql & "," & BL_TrataStringParaBD(STRContagrupos) & ",null,"
    sql = sql & BL_TrataStringParaBD(urg) & ", " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ","
    sql = sql & BL_TrataStringParaBD(STRObsRegistos) & ", " & totalAna & "," & BL_TrataStringParaBD(BL_HandleNull(rs!cod_sala, ""))
    sql = sql & " ," & BL_TrataStringParaBD(Sexo) & "," & BL_TrataStringParaBD(idade)
    sql = sql & " ," & BL_HandleNull(rs!cod_local_entrega, "NULL") & "," & BL_TrataStringParaBD(BL_HandleNull(rs!descr_local_entrega, "")) & ")"
    BG_ExecutaQuery_ADO sql

End Sub


Sub FuncaoImprimir()
    Dim sSql As String
    Dim SQLQuery As String
    Dim sql As String
    Dim rsOBS As New ADODB.recordset
    Dim RsProd As ADODB.recordset
    Dim rsReq As ADODB.recordset
    Dim RsGr As ADODB.recordset
    Dim rsTotal As New ADODB.recordset
    
    Dim continua As Boolean
    Dim i As Integer
    Dim TotalLinha As Integer
    Dim LenCodigo As Integer
    Dim LenConta As Integer
    
    
    '*************************************************************************
    sql = "DELETE FROM SL_CR_MR WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sql
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Mapa Registo") = True Then Exit Sub
    
    '2.Verifica se os campos obrigat�rios est�o nulos
    
    SQLQuery = "SELECT n_req,n_req_assoc, sl_requis.seq_utente,dt_previ, sl_requis.cod_efr,n_benef,sl_requis.cod_sala cod_proven,"
    SQLQuery = SQLQuery & " sl_requis.t_sit, sl_requis.t_urg, sl_cod_salas.cod_sala, sl_identif.nome_ute, sl_identif.t_utente, sl_identif.utente, "
    SQLQuery = SQLQuery & " sl_identif.dt_nasc_ute,sl_identif.sexo_ute, sl_proven.descr_proven, sl_tbf_t_urg.descr_t_urg,sl_requis.dt_chega, "
    SQLQuery = SQLQuery & " sl_cod_local_entrega.cod_local_entrega, sl_cod_local_entrega.descr_local_entrega "
    SQLQuery = SQLQuery & " FROM sl_identif, sl_requis "
    SQLQuery = SQLQuery & " LEFT OUTER JOIN sl_cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala "
    SQLQuery = SQLQuery & " LEFT OUTER JOIN sl_tbf_t_urg ON sl_requis.t_urg = sl_tbf_t_urg.cod_t_urg "
    SQLQuery = SQLQuery & " LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven = sl_proven.cod_proven "
    SQLQuery = SQLQuery & " LEFT OUTER JOIN sl_cod_local_entrega ON sl_requis.cod_local_entrega = sl_cod_local_entrega.cod_local_entrega "
    
    If Opt1(0).value = True Then
        SQLQuery = SQLQuery & " WHERE sl_requis.dt_previ between " & BL_TrataDataParaBD(EcDataInicio.Text) & " AND "
    ElseIf Opt1(1).value = True Then
        SQLQuery = SQLQuery & " WHERE sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio.Text) & " AND "
    ElseIf Opt1(2).value = True Then
        SQLQuery = SQLQuery & " WHERE sl_requis.dt_cri between " & BL_TrataDataParaBD(EcDataInicio.Text) & " AND "
    End If
    SQLQuery = SQLQuery & BL_TrataDataParaBD(EcDataFim.Text) & " AND estado_req<>'C' "
    SQLQuery = SQLQuery & " AND sl_requis.seq_utente = sl_identif.seq_utente "
    
    'SE FOI INDICADO ALGUM LOCAL
    If CbLocal.ListIndex <> -1 Then
        SQLQuery = SQLQuery & " AND sl_requis.cod_local = " & CbLocal.ItemData(CbLocal.ListIndex)
    End If
    
    'SE FOI INDICADO ALGUMA SALA
    If ListaPostos.ListCount > 0 Then
        SQLQuery = SQLQuery & " AND sl_cod_salas.seq_sala IN ("
        For i = 0 To ListaPostos.ListCount - 1
            SQLQuery = SQLQuery & ListaPostos.ItemData(i) & ", "
        Next i
        SQLQuery = Mid(SQLQuery, 1, Len(SQLQuery) - 2) & ") "
    End If
    
    'SE FOI INDICADO ALGUM local de entrega
    If EcListaLocalEntrega.ListCount > 0 Then
        SQLQuery = SQLQuery & " AND sl_cod_local_entrega.cod_local_entrega IN ("
        For i = 0 To EcListaLocalEntrega.ListCount - 1
            SQLQuery = SQLQuery & EcListaLocalEntrega.ItemData(i) & ", "
        Next i
        SQLQuery = Mid(SQLQuery, 1, Len(SQLQuery) - 2) & ") "
    End If
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLQuery
    rs.Open SQLQuery, gConexao
    If rs.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbOKOnly, "ATEN��O"
        FuncaoLimpar
        Exit Sub
    End If
            
    '5. Inicia o processo de impress�o do relat�rio
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("MapaRegisto", "Mapa Registo", crptToPrinter)
    Else
        continua = BL_IniciaReport("MapaRegisto", "Mapa Registo", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    
    While Not rs.EOF
        LimpaListaGrupos
        
        sSql = "SELECT distinct cod_agrup FROM sl_marcacoes WHERE n_req=" & rs!n_req
        Set rsReq = New ADODB.recordset
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsReq.Open sSql, gConexao
        
        While Not rsReq.EOF
            sSql = "SELECT cod_gr_ana FROM slv_analises_apenas WHERE cod_ana=" & BL_TrataStringParaBD(BL_HandleNull(rsReq!cod_agrup, ""))
            Set RsGr = New ADODB.recordset
            RsGr.CursorLocation = adUseServer
            RsGr.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            RsGr.Open sSql, gConexao
            
            'incrementar numero de ocorr�ncias para um determinado grupo
            If RsGr.RecordCount > 0 Then
                i = 1
                For i = 1 To ContaGR - 1
                    If BL_HandleNull(RsGr!cod_gr_ana, "") = grupos(i).cod_grupo Then
                        Exit For
                    End If
                Next
                If i >= ContaGR Then
                    'Grupo novo
                Else
                    grupos(i).conta = grupos(i).conta + 1
                End If
            End If
            RsGr.Close
            Set RsGr = Nothing
            rsReq.MoveNext
        Wend
        rsReq.Close
        Set rsReq = Nothing
        
        'Construir a string de produtos
        sSql = "SELECT cod_prod FROM sl_req_prod WHERE n_req=" & rs!n_req
        Set RsProd = New ADODB.recordset
        RsProd.CursorLocation = adUseServer
        RsProd.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsProd.Open sSql, gConexao
        STRProduto = ""
        While Not RsProd.EOF
            STRProduto = STRProduto + BL_HandleNull(RsProd!cod_prod, "") + ";"
            RsProd.MoveNext
        Wend
        RsProd.Close
        Set RsProd = Nothing
        
        'Construir a string de contagem de grupos e o total por linha
        STRContagrupos = ""
        TotalLinha = 0
        For i = 1 To ContaGR
            STRContagrupos = STRContagrupos + Trim(str(grupos(i).conta))
            STRContagrupos = STRContagrupos + Space((grupos(i).tamanho - Len(Trim(str(grupos(i).conta)))) + 1)
            TotalLinha = TotalLinha + grupos(i).conta
        Next i
        STRContagrupos = STRContagrupos + str(TotalLinha)
        
        ' constroi string com observacoes do registo
        sql = " SELECT  * FROM sl_obs_ana_req WHERE n_Req = " & rs!n_req
        Set rsOBS = New ADODB.recordset
        rsOBS.CursorLocation = adUseServer
        rsOBS.CursorType = adOpenStatic
        rsOBS.Open sql, gConexao
        STRObsRegistos = ""
        If rsOBS.RecordCount > 0 Then
            While Not rsOBS.EOF
                STRObsRegistos = STRObsRegistos & BL_HandleNull(rsOBS!descr_obs, "") & ";"
                rsOBS.MoveNext
            Wend
        End If
        rsOBS.Close
        
        ' NUMERO TOTAL DE ANALISES
        sql = " SELECT  cod_agrup, count(*) conta FROM slv_resultados WHERE n_Req = " & rs!n_req & " group by cod_agrup "
        Set rsTotal = New ADODB.recordset
        rsTotal.CursorLocation = adUseServer
        rsTotal.CursorType = adOpenStatic
        rsTotal.Open sql, gConexao
        totalAna = 0
        If rsTotal.RecordCount > 0 Then
                totalAna = BL_HandleNull(rsTotal.RecordCount, 0)
                rsTotal.MoveNext
        End If
        rsTotal.Close
        
        INSERT_TabelaTemporaria
        rs.MoveNext
    Wend
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
        
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD(EcDataInicio.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD(EcDataFim.Text)
    Report.formulas(2) = "Grupos=" & BL_TrataStringParaBD("" & STRGrupos)
    
    Report.SQLQuery = "SELECT N_REQ, NOME_UTE, PROVENIENCIA, SITUACAO, PRODUTO, CONTAGEM_GRUPOS, TOTAL_ANA, DESCR_SALA" & _
        " FROM SL_CR_MR, SL_COD_SALAS  WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_Sessao = " & gNumeroSessao
    Report.SQLQuery = Report.SQLQuery & " AND sl_cod_salas.cod_sala (+) = sl_cr_mr.cod_sala "
    If gLAB = "HCVP" Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY NOME_UTE ASC "
    ElseIf gLAB = "CL" Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY DESCR_SALA, N_REQ ASC"
    Else
        Report.SQLQuery = Report.SQLQuery & " ORDER BY N_REQ ASC"
    End If
        
        
    Call BL_ExecutaReport
    
        

End Sub

Private Sub BtPesquisaPostos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CWhere = ""
    CampoPesquisa = "descr_sala"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_sala ", _
                                                                           " Postos Colheita")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPostos.ListCount = 0 Then
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(0) = resultados(i)
            Else
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(ListaPostos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub ListaPostos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPostos.ListCount > 0 Then     'Delete
        ListaPostos.RemoveItem (ListaPostos.ListIndex)
    End If
End Sub




VERSION 5.00
Begin VB.Form FormListarRequisSemEpisodio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   1140
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4035
   Icon            =   "FormListarRequisSemEpisodio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1140
   ScaleWidth      =   4035
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3735
      Begin VB.TextBox EcDtIni 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "MM/dd/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcDtFim 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "MM-dd-yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   1
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2250
         TabIndex        =   4
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   735
      End
   End
End
Attribute VB_Name = "FormListarRequisSemEpisodio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 19/02/2004
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object


Public rs As ADODB.Recordset

Sub Preenche_Lista()
    
    Dim Sql As String
    Dim Continua As Boolean
            
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estatistica por Produto") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If

    On Error GoTo TrataErro
    
    Sql = "DELETE FROM sl_cr_requis_sepis where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO Sql

    If gImprimirDestino = 1 Then
        Continua = BL_IniciaReport("RequisSemEpisodio", "Requisi��es sem epis�dio", crptToPrinter)
    Else
        Continua = BL_IniciaReport("RequisSemEpisodio", "Requisi��es sem epis�dio", crptToWindow)
    End If
    If Continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me
    
    Dim Report As CrystalReport
    Set Report = Forms(0).Controls("Report")
    
    Sql = "INSERT INTO sl_cr_requis_sepis(nome_computador, n_req, nome_ute, cod_agrup) " & _
        " SELECT distinct '" & BG_SYS_GetComputerName & "', sl_requis.n_req, nome_ute, cod_agrup  " & _
        " FROM sl_requis, sl_realiza, sl_identif where sl_requis.n_epis is null and sl_requis.n_req = sl_realiza.n_req " & _
        " AND sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDtIni.Text) & " and " & BL_TrataDataParaBD(EcDtFim.Text) & _
        " UNION " & _
        " SELECT distinct '" & BG_SYS_GetComputerName & "', sl_requis.n_req, nome_ute, cod_agrup  " & _
        " FROM sl_requis, sl_marcacoes, sl_identif where sl_requis.n_epis is null and sl_requis.n_req = sl_marcacoes.n_req " & _
        " AND sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDtIni.Text) & " and " & BL_TrataDataParaBD(EcDtFim.Text)

        BG_ExecutaQuery_ADO Sql
    
    Report.SelectionFormula = "{sl_cr_requis_sepis.nome_computador} = '" & BG_SYS_GetComputerName & "'"

    'F�rmulas do Report
    Report.Formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.Formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
'
    'Me.SetFocus
    
    Report.Connect = "DSN=" & gDSN
    
    Call BL_ExecutaReport

    BL_MudaCursorRato mediMP_Activo, Me
    
    
    
    Exit Sub

TrataErro:

End Sub



Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function


Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtIni_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.Caption = "Listagem de Requisi��es Sem Epis�dio"
    Me.Left = 540
    Me.Top = 450
    Me.Width = 4125
    Me.Height = 1620 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtIni
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormListarRequisSemEpisodio = Nothing
    
    Call BL_FechaPreview("Requisi��es Sem Epis�dio")
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""

    
End Sub

Sub DefTipoCampos()



    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10

        
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()


End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Lista
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Lista

End Sub










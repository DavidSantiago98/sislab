VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormControloStock 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   9030
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   17175
   Icon            =   "FormControloStock.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9030
   ScaleWidth      =   17175
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox TextAuxiliar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   5760
      TabIndex        =   19
      Text            =   "TextAuxiliar"
      Top             =   8160
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Frame Frame3 
      Height          =   6015
      Left            =   4560
      TabIndex        =   17
      Top             =   0
      Width           =   12495
      Begin MSComctlLib.ListView LvStock 
         Height          =   5655
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   12255
         _ExtentX        =   21616
         _ExtentY        =   9975
         SortOrder       =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
   End
   Begin VB.CommandButton BtValidarRegisto 
      Height          =   315
      Left            =   2280
      Picture         =   "FormControloStock.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Valida Registo(s)"
      Top             =   5640
      Width           =   2175
   End
   Begin VB.CommandButton BtAcrescenta 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      Picture         =   "FormControloStock.frx":685E
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Acrescenta Registo"
      Top             =   5640
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Height          =   5535
      Left            =   120
      TabIndex        =   5
      Top             =   0
      Width           =   4335
      Begin VB.ComboBox CbLocal 
         Height          =   765
         Left            =   1080
         Style           =   1  'Simple Combo
         TabIndex        =   13
         Top             =   2400
         Width           =   3135
      End
      Begin VB.ComboBox CbDirecao 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1680
         Width           =   3135
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   1320
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   503
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   147718147
         CurrentDate     =   40217
      End
      Begin VB.TextBox EcQuantidade 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   600
         Width           =   3135
      End
      Begin VB.ComboBox CbProduto 
         Height          =   1740
         Left            =   1080
         Style           =   1  'Simple Combo
         TabIndex        =   4
         Top             =   3600
         Width           =   3135
      End
      Begin VB.Frame Frame2 
         Caption         =   "Frame1"
         Height          =   1455
         Left            =   6720
         TabIndex        =   6
         Top             =   6000
         Width           =   3735
      End
      Begin VB.TextBox EcLote 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   3135
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00C0C0C0&
         X1              =   120
         X2              =   4080
         Y1              =   3360
         Y2              =   3360
      End
      Begin VB.Label Label2 
         Caption         =   "Local"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2400
         Width           =   615
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00C0C0C0&
         X1              =   120
         X2              =   4200
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C0C0C0&
         X1              =   120
         X2              =   4200
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label Label6 
         Caption         =   "Direc��o"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1680
         Width           =   735
      End
      Begin VB.Label Label5 
         Caption         =   "Data"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Produto"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   3600
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Quantidade"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Lote"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   375
      Left            =   4320
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   10
      Top             =   8280
      Width           =   495
   End
   Begin Crystal.CrystalReport CrComponentes 
      Left            =   2640
      Top             =   8160
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComctlLib.ImageList ImageListIcons 
      Left            =   1680
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormControloStock.frx":D0B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormControloStock.frx":D20A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormControloStock.frx":D364
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormControloStock.frx":D764
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormControloStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2010.02.04   .
'     .       � 2010 Glintt-HS      .
'      .............................

' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim erroTmp As Integer
Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Private Enum ListViewIcons
    
    cIconCollapsed = 1
    cIconExpanded = 2
    cIconIn = 3
    cIconOut = 4
    
End Enum

Private Enum StockDirection
    
    cIn = 0
    cOut = 1
    
End Enum

Private Type EstruturaProdutos
    
    Index As Long
    Produto As Long
    quantidade As Integer
    limite As Integer
    lote As String
    direcao As Integer
    data As String
    hora As String
    utilizador As Long
    actual As Double
    cod_local As Integer
    observacao As String
    
End Type

Dim eProdutos() As EstruturaProdutos
Dim TotalProdutos As Long

' Keep handle to the list view.
Private lhwndListView As Long

' Keep handle to the text box.
Private lhwndTextBox As Long

' Keep list item index whose subItem is being edited.
Private lListItemIndex As Long

' Keep zero based index of LvStock.ListItems(lListItemIndex).SubItem being edited.
Private lListSubItem As Long

Private Sub BtAcrescenta_Click()
    
    On Error GoTo ErrorHandler
    If (Not CamposObrigatorios) Then: Exit Sub
    Call InsereItemEstrutura(EcLote.Text, -1, BG_DaComboSel(CbLocal), BG_DaComboSel(CbProduto), Empty, EcQuantidade.Text, EcQuantidade.Text, IIf(BG_DaComboSel(CbDirecao) = mediComboValorNull, Empty, BG_DaComboSel(CbDirecao)), Empty, BL_HandleNull(DTPicker1.value, Bg_DaData_ADO), Empty, gCodUtilizador)
    Call AddListViewItem(TotalProdutos, 1, True, False, True)
    Call LimpaCampos(True)
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub CbLocal_Change()
 
    If (CbLocal.ListIndex <> mediComboValorNull) Then: BG_PreencheComboBD_ADO "select codigo, descricao from " & gBD_PREFIXO_TAB & "view_stock_inventario where cod_local = " & BG_DaComboSel(CbLocal), "codigo", "descricao", CbProduto, mediDescComboCodigo
    If (CbLocal.ListIndex = mediComboValorNull) Then: CbProduto_KeyDown vbKeyDelete, False
    
End Sub

Private Sub CbLocal_Click()
    
    If (CbLocal.ListIndex <> mediComboValorNull) Then: BG_PreencheComboBD_ADO "select codigo, descricao from " & gBD_PREFIXO_TAB & "view_stock_inventario where cod_local = " & BG_DaComboSel(CbLocal), "codigo", "descricao", CbProduto, mediDescComboCodigo
    If (CbLocal.ListIndex = mediComboValorNull) Then: CbProduto_KeyDown vbKeyDelete, False
   
End Sub

Private Sub CbProduto_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If (KeyCode = vbKeyDelete) Then: CbProduto.Clear
    BG_LimpaOpcao CbProduto, KeyCode

End Sub

Private Sub CbLocal_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbLocal, KeyCode
    If (KeyCode = vbKeyDelete) Then: CbProduto_KeyDown vbKeyDelete, False
    
End Sub

Private Sub PreencheValoresDefeito()

    'BG_PreencheComboBD_ADO "select codigo, descricao from " & gBD_PREFIXO_TAB & "view_stock_inventario", "codigo", "descricao", CbProduto, mediDescComboCodigo
    BG_PreencheComboBD_ADO gBD_PREFIXO_TAB & "tbf_stock_direcao", "codigo", "descricao", CbDirecao, mediDescComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal, mediDescComboCodigo
    
End Sub

Private Sub BtValidarRegisto_Click()
    BL_Inserir_Modificar
End Sub

Private Sub EcLote_LostFocus()
    EcLote.Text = UCase(EcLote.Text)
End Sub

Private Sub EcQuantidade_LostFocus()

    On Error GoTo ErrorHandler
    If (Not IsNumeric(EcQuantidade.Text)) Then: BG_Mensagem mediMsgBox, "Dever� indicar apenas caracteres num�ricos!", vbInformation, " Aten��o"
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

Public Sub Form_Load()
    EventoLoad
End Sub

Public Sub Form_Activate()
    EventoActivate
End Sub

Public Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Private Sub Inicializacoes()
    Me.caption = " Controlo de Stocks"
    Me.left = 500
    Me.top = 20
    Me.Width = 17265
    Me.Height = 6495 ' Normal
    'Me.Height = 7500 ' Campos Extras
        
    Set CampoDeFocus = EcLote
    TotalProdutos = 0
    ReDim eProdutos(TotalProdutos)
    DefineLvStock
    DTPicker1.value = Bg_DaData_ADO
    DTPicker1.value = Null
    lhwndListView = LvStock.hwnd
    
End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    estado = 0
    BG_StackJanelas_Push Me
    
End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
        
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormControloStock = Nothing
    
End Sub

Private Sub LimpaCampos(Optional apenas_registo As Boolean)
    
    If (Not apenas_registo) Then
        TotalProdutos = 0
        ReDim eProdutos(TotalProdutos)
        LvStock.ListItems.Clear
        BL_Toolbar_BotaoEstado "Inserir", "Activo"
    End If
    CbProduto.ListIndex = mediComboValorNull
    CbDirecao.ListIndex = mediComboValorNull
    CbLocal.ListIndex = mediComboValorNull
    EcLote.Text = Empty
    EcQuantidade.Text = Empty
    DTPicker1.value = Null
    BtAcrescenta.Enabled = True
    BtValidarRegisto.Enabled = True
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
            
End Sub

Private Sub PreencheCampos()
    'nada
End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Public Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        
    Else
        Unload Me
    End If
End Sub

Public Sub FuncaoProcurar()
    
    On Error GoTo ErrorHandler
    estado = 2
    BL_InicioProcessamento Me, " A procurar registos..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    BtAcrescenta.Enabled = False
    BtValidarRegisto.Enabled = False
    ProcuraRegistos
    WriteIntoListView
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    Exit Sub
        
ErrorHandler:
    BL_FimProcessamento Me
    BL_ToolbarEstadoN 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
End Sub

Public Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados?"
    gMsgResp = BG_Mensagem(mediMsgBox, (gMsgMsg), vbYesNo + vbDefaultButton2 + vbQuestion, (gMsgTitulo))
    
    If gMsgResp = vbYes Then
        BD_Insert
    End If
    
End Sub

Private Sub BD_Insert()
    
    On Error GoTo TrataErro
    
    Dim k As Integer
    Dim sql As String
    Dim iRes As Integer
    Dim DescrComp As String
    
    If (LvStock.ListItems.Count = Empty) Then: BG_Mensagem mediMsgBox, "N�o tem items para adicionados!", vbInformation, " Aten��o"
    
    BG_Mensagem mediMsgStatus, ("A inserir registo(s)..."), mediMsgBeep + mediMsgPermanece
    BL_PoeMousePointer mediMP_Espera, Me
  
    gConexao.BeginTrans
    erroTmp = 0
    
    
    If (GravaStockSelecionados) Then: BG_Mensagem mediMsgBox, "Registo(s) Inserido(s)!", vbInformation, " Gravar": LimpaCampos

    If erroTmp = 0 Then
        gConexao.CommitTrans
  
    Else
        gConexao.RollbackTrans
        BG_Mensagem mediMsgBox, ("Erro a inserir registo(s)!"), vbCritical, App.ProductName
    End If
    
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    BL_PoeMousePointer mediMP_Activo, Me
    
    Exit Sub
    
TrataErro:
    gConexao.RollbackTrans
    BG_Mensagem mediMsgBox, Err.Description, vbCritical, App.ProductName
    BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    BL_PoeMousePointer mediMP_Activo, Me
    
End Sub
    
Public Sub FuncaoModificar()
    'nada
End Sub
    
Private Sub BD_Update()
    'nada
End Sub

Public Sub FuncaoRemover()
    'nada
End Sub

Private Sub BD_Delete()
    'nada
End Sub

Public Sub Funcao_DataActual()
    'nada
End Sub

Private Sub InsereItemEstrutura(lote As String, Index As Long, cod_local As Integer, Optional Produto As Long, Optional observacao As String, Optional actual As Double, _
                                Optional quantidade As Integer, Optional direcao As Integer, Optional limite As Integer, _
                                Optional data As String, Optional hora As String, Optional utilizador As Long)

    TotalProdutos = TotalProdutos + 1
    ReDim Preserve eProdutos(TotalProdutos)
    eProdutos(TotalProdutos).lote = lote
    eProdutos(TotalProdutos).Index = Index
    eProdutos(TotalProdutos).cod_local = cod_local
    eProdutos(TotalProdutos).Produto = Produto
    eProdutos(TotalProdutos).quantidade = quantidade
    eProdutos(TotalProdutos).limite = limite
    eProdutos(TotalProdutos).direcao = direcao
    eProdutos(TotalProdutos).data = data
    eProdutos(TotalProdutos).hora = hora
    eProdutos(TotalProdutos).utilizador = utilizador
    eProdutos(TotalProdutos).actual = actual
    eProdutos(TotalProdutos).observacao = observacao
    
End Sub

Private Sub DefineLvStock()
   
    On Error GoTo ErrorHandler
    With LvStock
        .ColumnHeaders.Add(, , "Lote", 1600, lvwColumnLeft).Key = "LOTE"
        .ColumnHeaders.Add(, , "Produto", 2000, lvwColumnLeft).Key = "PRODUTO"
        .ColumnHeaders.Add(, , "Observa��o", 2050, lvwColumnLeft).Key = "OBSERVACAO"
        .ColumnHeaders.Add(, , "Quantidade", 1000, lvwColumnLeft).Key = "QUANTIDADE"
        .ColumnHeaders.Add(, , "Actual", 650, lvwColumnLeft).Key = "ACTUAL"
        .ColumnHeaders.Add(, , "Data", 1450, lvwColumnLeft).Key = "DATA"
        .ColumnHeaders.Add(, , "Utilizador", 1740, lvwColumnLeft).Key = "UTILIZADOR"
        .ColumnHeaders.Add(, , "Local", 1500, lvwColumnLeft).Key = "LOCAL"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .SmallIcons = ImageListIcons
        .FullRowSelect = True
        .Checkboxes = True
        .ForeColor = &H808080
        .MultiSelect = True
    End With
    SetListViewColor LvStock, PictureListColor, &H8000000F, vbWhite
    Exit Sub

ErrorHandler:
    Exit Sub
    
End Sub

Private Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Function GravaStockSelecionados() As Boolean

    Dim sql As String
    Dim item  As Variant
    Dim seq_controlo As Long
    
    On Error GoTo ErrorHandler
    For Each item In LvStock.ListItems
        If (item.Checked) Then
            seq_controlo = DevolveSeqControlo(eProdutos(item.Index).lote, eProdutos(item.Index).cod_local)
            If (seq_controlo = Empty) Then
                If (Not InsereCabecalho("seq_controlo_stock.nextval", eProdutos(item.Index).lote, eProdutos(item.Index).Produto, eProdutos(item.Index).quantidade, eProdutos(item.Index).cod_local, eProdutos(item.Index).observacao)) Then: Exit Function
                If (Not InsereDetalhe("seq_controlo_stock.currval", eProdutos(item.Index).utilizador, eProdutos(item.Index).direcao, eProdutos(item.Index).data, eProdutos(item.Index).quantidade)) Then: Exit Function
            Else
                If (Not InsereDetalhe(CStr(seq_controlo), eProdutos(item.Index).utilizador, eProdutos(item.Index).direcao, eProdutos(item.Index).data, eProdutos(item.Index).quantidade)) Then: Exit Function
            End If
            If (Not ActualizaStock(seq_controlo, eProdutos(item.Index).quantidade, eProdutos(item.Index).Produto, eProdutos(item.Index).direcao, eProdutos(item.Index).cod_local)) Then: Exit Function
            If (Not ActualizaObservacao(eProdutos(item.Index).observacao, eProdutos(item.Index).lote, eProdutos(item.Index).Produto, eProdutos(item.Index).cod_local)) Then: Exit Function
        End If
    Next
    GravaStockSelecionados = True
    Exit Function
    
ErrorHandler:
    gSQLError = -1
    Exit Function
    
End Function

Private Sub LvStock_DblClick()

    Dim bParent As Boolean
    Dim lStructureIndex As Long
    
    On Error GoTo ErrorHandler
    lStructureIndex = GetStructureIndex(LvStock.SelectedItem.Index, bParent)
    If (LvStock.SelectedItem.Index <> Empty) Then
        If (Not bParent And estado = 1) Then: HandleListViewDoubleClick
        If (bParent) Then: HandleListViewTree LvStock.SelectedItem, Empty
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub HandleListViewTree(ByVal �Item� As MSComctlLib.ListItem, �iManualKey� As Variant)
   
    On Error GoTo ErrorHandler
    If (�Item�.SmallIcon = ListViewIcons.cIconCollapsed And BL_HandleNull(�iManualKey�, vbKeyRight) = vbKeyRight) Then: ExpandListViewItem �Item�: Exit Sub
    If (�Item�.SmallIcon = ListViewIcons.cIconExpanded And BL_HandleNull(�iManualKey�, vbKeyLeft) = vbKeyLeft) Then: CollapseListViewItem �Item�: Exit Sub
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub CollapseListViewItem(ByVal �Item� As MSComctlLib.ListItem)
   
    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    
    On Error GoTo ErrorHandler
    Call GetStructureBounds(�Item�.Text, lLowerBound, lUpperBound)
    For lItem = lLowerBound To lUpperBound: Call RemoveListViewItem(�Item�): Next
    �Item�.SmallIcon = ListViewIcons.cIconCollapsed
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub ExpandListViewItem(ByVal �Item� As MSComctlLib.ListItem)

    Dim lItem As Long
    Dim iShift As Integer
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    
    On Error GoTo ErrorHandler
    Call GetStructureBounds(�Item�.Text, lLowerBound, lUpperBound)
    iShift = �Item�.Index + 1
    For lItem = lLowerBound To lUpperBound: Call AddListViewItem(lItem, CLng(iShift), , , True): iShift = iShift + 1: Next
    �Item�.SmallIcon = ListViewIcons.cIconExpanded
    Exit Sub

ErrorHandler:
    Exit Sub

End Sub

Private Sub GetStructureBounds(lote As String, Optional ByRef �lLowerBound� As Long, Optional ByRef �lUpperBound� As Long)

    Dim lItem As Long
    
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eProdutos)
       If (eProdutos(lItem).lote = lote) Then
            If (eProdutos(lItem).Index = Empty) Then: �lLowerBound� = lItem + 1
            �lUpperBound� = lItem
        End If
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

Private Function GetStructureIndex(�lItem� As Long, Optional ByRef �bParent�) As Integer
    
    Dim lItem As Long

    On Error GoTo ErrorHandler
    GetStructureIndex = �lItem�
    GetStructureIndex = CInt(Split(LvStock.ListItems(�lItem�).Key, "KEY_")(1))
    �bParent� = CBool(eProdutos(GetStructureIndex).Index = Empty)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Sub AddListViewItem(�lItem� As Long, �lIndex� As Long, Optional �bParent� As Boolean, Optional �bAutomatic� As Boolean, Optional �bShowDirection� As Boolean)

    Dim produto_info As Collection
    
    On Error GoTo ErrorHandler
    Set produto_info = DevolveInfoProduto(eProdutos(�lItem�).Produto, eProdutos(�lItem�).cod_local)
    With LvStock.ListItems.Add(�lIndex�, "KEY_" & �lItem�, IIf(�bParent�, eProdutos(�lItem�).lote, Empty), , IIf(�bParent�, IIf(�bAutomatic�, ListViewIcons.cIconExpanded, ListViewIcons.cIconCollapsed), Empty))
        .ListSubItems.Add , , IIf(�bParent�, produto_info(1), DevolveDescricaoDirecao(eProdutos(�lItem�).direcao)), IIf(�bShowDirection�, GetDirectionIcon(eProdutos(�lItem�).direcao), Empty)
        .ListSubItems.Add , , eProdutos(�lItem�).observacao
        .ListSubItems.Add , , eProdutos(�lItem�).quantidade
        .ListSubItems.Add , , IIf(Not �bParent�, Empty, eProdutos(�lItem�).actual)
        .ListSubItems.Add , , eProdutos(�lItem�).data & Space(1) & eProdutos(�lItem�).hora
        .ListSubItems.Add , , BL_DevolveNomeUtilizador(CStr(eProdutos(�lItem�).utilizador), "", "")
        .ListSubItems.Add , , IIf(�bParent�, BL_DevolveDescrLocal(CStr(eProdutos(�lItem�).cod_local)), Empty)
    End With
    LvStock.ListItems(�lIndex�).Bold = True
    Call CleanDispensableSubItems(�lIndex�)
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub RemoveListViewItem(ByVal �Item� As MSComctlLib.ListItem)
   
    On Error GoTo ErrorHandler
    LvStock.ListItems.Remove �Item�.Index + 1
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub ProcuraRegistos()

    Dim sql As String
    Dim RsStock As ADODB.recordset
    Dim rsStockDet As ADODB.recordset
    Dim Index As Long
    
    On Error GoTo ErrorHandler
    Set RsStock = New ADODB.recordset
    RsStock.CursorLocation = adUseServer
    RsStock.CursorType = adOpenStatic
    Set rsStockDet = New ADODB.recordset
    rsStockDet.CursorLocation = adUseServer
    rsStockDet.CursorType = adOpenStatic
    sql = ConstroiQuery1
    RsStock.Open sql, gConexao
    While (Not RsStock.EOF)
        Index = Empty
        InsereItemEstrutura BL_HandleNull(RsStock!lote, Empty), Index, BL_HandleNull(RsStock!cod_local, Empty), BL_HandleNull(RsStock!Produto, Empty), BL_HandleNull(RsStock!observacao, Empty), BL_HandleNull(RsStock!actual, Empty)
        sql = ConstroiQuery2(BL_HandleNull(RsStock!seq_controlo, Empty))
        rsStockDet.Open sql, gConexao
        While (Not rsStockDet.EOF)
            Index = Index + 1
            InsereItemEstrutura BL_HandleNull(RsStock!lote, Empty), Index, _
                                BL_HandleNull(RsStock!cod_local, Empty), _
                                BL_HandleNull(RsStock!Produto, Empty), _
                                Empty, _
                                Empty, _
                                BL_HandleNull(rsStockDet!quantidade, Empty), _
                                BL_HandleNull(rsStockDet!direcao, Empty), _
                                Empty, _
                                BL_HandleNull(rsStockDet!dt_cri, Empty), _
                                BL_HandleNull(rsStockDet!hr_cri, Empty), _
                                BL_HandleNull(rsStockDet!user_cri, Empty)
            rsStockDet.MoveNext
        Wend
        If (rsStockDet.state = adStateOpen) Then: rsStockDet.Close
        RsStock.MoveNext
    Wend
    If (RsStock.RecordCount = Empty) Then: BG_Mensagem mediMsgBox, "N�o encontrou registos!", vbInformation, " Procura"
    If (RsStock.state = adStateOpen) Then: RsStock.Close
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

Private Sub CleanDispensableSubItems(�lItem� As Long)
    
    Dim vSubItem As Variant
    
    On Error GoTo ErrorHandler
    For Each vSubItem In LvStock.ListItems(�lItem�).ListSubItems
        If (vSubItem.Text = "0") Then: vSubItem.Text = Empty
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub ExpandListViewAutomatic()
    
    Dim lItem As Long
            
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eProdutos)
        Call AddListViewItem(lItem, LvStock.ListItems.Count + 1, (eProdutos(lItem).Index = Empty), True)
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub LvStock_KeyDown(�KeyCode� As Integer, �Shift� As Integer)
    
    On Error GoTo ErrorHandler
    If (LvStock.ListItems.Count = Empty) Then: Exit Sub
    Select Case �KeyCode�
        Case vbKeyLeft, vbKeyRight: HandleListViewTree LvStock.SelectedItem, �KeyCode�
        Case Else: ' Empty
    End Select
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub WriteIntoListView()

    Dim lItem As Long
    Dim vSubItem As Variant
        
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eProdutos)
        If (eProdutos(lItem).Index = Empty) Then: Call AddListViewItem(lItem, LvStock.ListItems.Count + 1, True, , False)
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

Private Function ConstroiQuery1() As String

    Dim sql As String
    Dim item As Variant
    Dim filtro As Collection
    
    On Error GoTo ErrorHandler
    Set filtro = New Collection
    sql = "select * from " & gBD_PREFIXO_TAB & "controlo_stock"
    If (EcLote.Text <> Empty) Then: filtro.Add ("lote = " & BL_TrataStringParaBD(EcLote.Text))
    If (CbProduto.ListIndex <> mediComboValorNull) Then: filtro.Add ("produto = " & BG_DaComboSel(CbProduto))
    If (CbLocal.ListIndex <> mediComboValorNull) Then: filtro.Add ("cod_local = " & BG_DaComboSel(CbLocal))
    If (filtro.Count > Empty) Then: sql = sql & " where "
    For Each item In filtro
        If (item <> filtro.item(filtro.Count)) Then
            sql = sql & item & " and "
        Else
            sql = sql & item
        End If
    Next
    ConstroiQuery1 = sql
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function ConstroiQuery2(sequencia_controlo As String) As String

    Dim sql As String
    Dim item As Variant
    Dim filtro As Collection
    
    On Error GoTo ErrorHandler
    Set filtro = New Collection
    sql = "select * from " & gBD_PREFIXO_TAB & "controlo_stock_det"
    If (sequencia_controlo <> Empty) Then: filtro.Add ("seq_controlo = " & sequencia_controlo)
    If (Not IsNull(DTPicker1.value)) Then: filtro.Add ("dt_cri = " & BL_TrataDataParaBD(DTPicker1.value))
    If (CbDirecao.ListIndex <> mediComboValorNull) Then: filtro.Add ("direcao = " & BG_DaComboSel(CbDirecao))
    If (filtro.Count > Empty) Then: sql = sql & " where "
    For Each item In filtro
        If (item <> filtro.item(filtro.Count)) Then
            sql = sql & item & " and "
        Else
            sql = sql & item
        End If
    Next
    ConstroiQuery2 = sql
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function DevolveInfoProduto(Produto As Long, cod_local As Integer) As Collection

    Dim sql As String
    Dim rsDescr As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set DevolveInfoProduto = New Collection
    sql = "select * from " & gBD_PREFIXO_TAB & "view_stock_inventario where codigo = " & Produto & " and cod_local = " & cod_local
    Set rsDescr = New ADODB.recordset
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    rsDescr.Open sql, gConexao
    If rsDescr.RecordCount > 0 Then
        DevolveInfoProduto.Add (BL_HandleNull(rsDescr!descricao, Empty))
        DevolveInfoProduto.Add (BL_HandleNull(rsDescr!stock_actual, Empty))
    End If
    If (rsDescr.state = adStateOpen) Then: rsDescr.Close
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function DevolveSeqControlo(lote As String, cod_local As Integer) As Long

    Dim sql As String
    Dim RsStock As ADODB.recordset
    
    On Error GoTo ErrorHandler
    sql = "select seq_controlo from " & gBD_PREFIXO_TAB & "controlo_stock where upper(lote) = " & BL_TrataStringParaBD(UCase(lote)) & _
          " and cod_local = " & cod_local
    Set RsStock = New ADODB.recordset
    RsStock.CursorLocation = adUseServer
    RsStock.CursorType = adOpenStatic
    RsStock.Open sql, gConexao
    If RsStock.RecordCount > 0 Then: DevolveSeqControlo = BL_HandleNull(RsStock!seq_controlo, Empty)
    If (RsStock.state = adStateOpen) Then: RsStock.Close
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function GetDirectionIcon(direction As Integer)
    
    Select Case (direction)
        Case StockDirection.cIn: GetDirectionIcon = 3
        Case StockDirection.cOut: GetDirectionIcon = 4
    End Select

End Function

Private Function InsereCabecalho(sequencia As String, lote As String, Produto As Long, quantidade As Integer, cod_local As Integer, observacao As String) As Boolean
    
    Dim sql As String
    On Error GoTo ErrorHandler
    If (gSGBD = gOracle) Then
        sql = "insert into " & gBD_PREFIXO_TAB & "controlo_stock (seq_controlo, lote, produto, actual, cod_local, observacao) values (" & sequencia & "," & _
              BL_TrataStringParaBD(lote) & "," & Produto & "," & quantidade & "," & cod_local & "," & BL_TrataStringParaBD(observacao) & ")"
    ElseIf (gSGBD = gSqlServer) Then
        sql = "insert into " & gBD_PREFIXO_TAB & "controlo_stock (lote, produto, actual, cod_local, observacao) values (" & _
              BL_TrataStringParaBD(lote) & "," & Produto & "," & quantidade & "," & cod_local & "," & BL_TrataStringParaBD(observacao) & ")"
    End If
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    erroTmp = gSQLError
    If (erroTmp <> Empty) Then: Exit Function
    InsereCabecalho = True
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function InsereDetalhe(sequencia As String, utilizador As Long, direcao As Integer, data As String, quantidade As Integer) As Boolean
  
    Dim sql As String
    On Error GoTo ErrorHandler
    If (gSGBD = gOracle) Then
        sql = "insert into " & gBD_PREFIXO_TAB & "controlo_stock_det (seq_controlo, quantidade, direcao, dt_cri, hr_cri,user_cri) values (" & _
              sequencia & "," & quantidade & "," & direcao & "," & BL_TrataDataParaBD(data) & "," & _
              BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & BL_TrataStringParaBD(CStr(utilizador)) & ")"
    ElseIf (gSGBD = gSqlServer) Then
        sql = "insert into " & gBD_PREFIXO_TAB & "controlo_stock_det (seq_controlo, quantidade, direcao, dt_cri, hr_cri,user_cri, cod_local) values (" & _
              "(select max(seq_controlo) from controlo_stock )" & "," & quantidade & "," & direcao & "," & BL_TrataDataParaBD(data) & "," & _
              BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & BL_TrataStringParaBD(CStr(utilizador)) & ")"
    End If
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    erroTmp = gSQLError
    If (erroTmp <> Empty) Then: Exit Function
    InsereDetalhe = True
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function ActualizaStock(sequencia As Long, quantidade As Integer, Produto As Long, direcao As Integer, cod_local As Integer) As Boolean
  
    Dim sql1 As String
    Dim Sql2 As String
    On Error GoTo ErrorHandler
    If (erroTmp <> Empty) Then: Exit Function
    Select Case (direcao)
        Case StockDirection.cIn:
            sql1 = "update " & gBD_PREFIXO_TAB & "controlo_stock set actual = actual + " & quantidade & " where seq_controlo = " & sequencia & " and cod_local = " & cod_local
            Select Case (gBD_PREFIXO_TAB)
                Case ("SB_"): Sql2 = "update sb_cod_inv_prod_industriais set stock_actual = nvl(stock_actual, 0) + " & quantidade & " where codigo = " & Produto & " and cod_local = " & cod_local
                Case ("SL_"): Sql2 = "update sl_stock_inventario set stock_actual = nvl(stock_actual, 0) + " & quantidade & " where cod_stock = " & Produto & " and cod_local = " & cod_local
            End Select
        Case StockDirection.cOut:
            sql1 = "update " & gBD_PREFIXO_TAB & "controlo_stock set actual = actual - " & quantidade & " where seq_controlo = " & sequencia & " and cod_local = " & cod_local
            Select Case (gBD_PREFIXO_TAB)
                Case ("SB_"): Sql2 = "update sb_cod_inv_prod_industriais set stock_actual = nvl(stock_actual, 0) - " & quantidade & " where codigo = " & Produto & " and cod_local = " & cod_local
                Case ("SL_"): Sql2 = "update sl_stock_inventario set stock_actual = nvl(stock_actual, 0) - " & quantidade & " where cod_stock = " & Produto & " and cod_local = " & cod_local
            End Select
            Call StockMinimoSatisfeito(Produto, quantidade, cod_local)
    End Select
    BG_ExecutaQuery_ADO sql1: BG_Trata_BDErro
    erroTmp = gSQLError
    If (erroTmp <> Empty) Then: Exit Function
    BG_ExecutaQuery_ADO Sql2: BG_Trata_BDErro
    erroTmp = gSQLError
    If (erroTmp <> Empty) Then: Exit Function
    ActualizaStock = True
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Public Function ExecutaSaidaProduto(Produto As Long, lote As String, quantidade As Integer, cod_local As Integer, Optional valida As Boolean, Optional utilizador As Long, Optional data As String) As Double

    Dim seq_controlo  As Long
    On Error GoTo ErrorHandler
    seq_controlo = DevolveSeqControlo(lote, cod_local)
    If (quantidade = Empty) Then: BG_Mensagem mediMsgBox, "O campo unidades n�o foi preenchido!", vbExclamation, " Aten��o": Exit Function
    If (seq_controlo = Empty) Then: BG_Mensagem mediMsgBox, "O lote n�o existe!", vbExclamation, " Aten��o": Exit Function
    If (valida) Then: ExecutaSaidaProduto = StockMinimoSatisfeito(Produto, quantidade, cod_local): Exit Function
    If (seq_controlo <> Empty) Then
        If (Not InsereDetalhe(CStr(seq_controlo), utilizador, StockDirection.cOut, data, quantidade)) Then: ExecutaSaidaProduto = erroTmp: Exit Function
        If (Not ActualizaStock(seq_controlo, quantidade, Produto, StockDirection.cOut, cod_local)) Then: ExecutaSaidaProduto = erroTmp: Exit Function
    End If
    Call StockMinimoSatisfeito(Produto, CDbl(quantidade), cod_local)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function CamposObrigatorios() As Boolean

    If (EcLote.Text = Empty) Then: BG_Mensagem mediMsgBox, "Obrigat�rio preencher o lote!", vbInformation, " Aten��o": Exit Function
    If (EcQuantidade.Text = Empty) Then: BG_Mensagem mediMsgBox, "Obrigat�rio preencher a quantidade!", vbInformation, " Aten��o": Exit Function
    If (CbLocal.ListIndex = mediComboValorNull) Then: BG_Mensagem mediMsgBox, "Obrigat�rio indicar o produto!", vbInformation, " Aten��o": Exit Function
    If (CbProduto.ListIndex = mediComboValorNull) Then: BG_Mensagem mediMsgBox, "Obrigat�rio indicar o produto!", vbInformation, " Aten��o": Exit Function
    CamposObrigatorios = True
    
End Function

Private Function StockMinimoSatisfeito(Produto As Long, quantidade As Integer, cod_local As Integer) As Boolean

    Dim sql As String
    Dim RsStock As ADODB.recordset
    
    On Error GoTo ErrorHandler
    sql = "select stock_actual, stock_minimo, descricao from " & gBD_PREFIXO_TAB & "view_stock_inventario where codigo = " & Produto & _
          " and cod_local = " & cod_local
    Set RsStock = New ADODB.recordset
    RsStock.CursorLocation = adUseServer
    RsStock.CursorType = adOpenStatic
    RsStock.Open sql, gConexao
    If (RsStock.RecordCount = Empty) Then: Exit Function
    If (BL_HandleNull(RsStock!stock_actual, Empty) - quantidade < Empty) Then
        BG_Mensagem mediMsgBox, "O stock actual de " & BL_HandleNull(RsStock!descricao, Empty) & " � negativo!", vbInformation, " Aten��o": Exit Function
    ElseIf (BL_HandleNull(RsStock!stock_actual, Empty) - quantidade < BL_HandleNull(RsStock!stock_minimo, Empty)) Then
        BG_Mensagem mediMsgBox, "O stock actual de " & BL_HandleNull(RsStock!descricao, Empty) & " atingiu o stock m�nimo!", vbExclamation, " Aten��o": Exit Function
    End If
    If (RsStock.state = adStateOpen) Then: RsStock.Close
    StockMinimoSatisfeito = True
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function DevolveDescricaoDirecao(direcao As Integer) As String

    Dim sql As String
    Dim RsDirecao As ADODB.recordset
    
    On Error GoTo ErrorHandler
    sql = "select descricao from " & gBD_PREFIXO_TAB & "tbf_stock_direcao where codigo = " & direcao
    Set RsDirecao = New ADODB.recordset
    RsDirecao.CursorLocation = adUseServer
    RsDirecao.CursorType = adOpenStatic
    RsDirecao.Open sql, gConexao
    If (RsDirecao.RecordCount > Empty) Then: DevolveDescricaoDirecao = BL_HandleNull(RsDirecao!descricao, Empty)
    If (RsDirecao.state = adStateOpen) Then: RsDirecao.Close
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' pferreira 2010.06.07
' Handle list view double click.
Private Sub HandleListViewDoubleClick()
  
    Dim lvhti As LVHITTESTINFO
    Dim rc As RECT
    Dim lListItem As ListItem
      
    On Error GoTo ErrorHandler
    Call GetCursorPos(lvhti.pt)
    Call ScreenToClient(lhwndListView, lvhti.pt)
    If (ListView_SubItemHitTest(lhwndListView, lvhti) = LVI_NOITEM) Then: Exit Sub
    If (lvhti.iSubItem = Empty) Then: Exit Sub
    If (ListView_GetSubItemRect(lhwndListView, lvhti.iItem, lvhti.iSubItem, LVIR_LABEL, rc)) Then
        Call MapWindowPoints(lhwndListView, hwnd, rc, 2)
        lListItemIndex = lvhti.iItem + 1
        lListSubItem = lvhti.iSubItem
        If (Not ValidateColumnToEdit(LvStock.ColumnHeaders(lListSubItem + 1).Key)) Then: Exit Sub
        TextAuxiliar.Text = LvStock.ListItems(lListItemIndex).SubItems(lListSubItem)
        TextAuxiliar.Tag = LvStock.ColumnHeaders(lListSubItem + 1).Key
        LvStock.ListItems(lListItemIndex).SubItems(lListSubItem) = ""
        Call SubClass(lhwndTextBox, AddressOf WndProc)
        EnableTextAuxiliar True, rc
        TextAuxiliar.SetFocus
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HandleListViewDoubleClick' in form FormControloStock (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2010.06.07
' Validate column to edit.
Private Function ValidateColumnToEdit(�sColumnKey� As String) As Boolean

    On Error GoTo ErrorHandler
    Select Case (�sColumnKey�)
        Case "OBSERVACAO": ValidateColumnToEdit = True
        Case Else: ValidateColumnToEdit = False
    End Select
    Exit Function
     
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ValidateColumnToEdit' in form FormControloStock (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' pferreira 2010.06.07
' Handle editable columns.
Private Sub HandleEditableColumns(�sColumnKey� As String, �sValue� As String, �lStructureIndex� As Long)

    On Error GoTo ErrorHandler
    If (gBD_PREFIXO_TAB = "SL_") Then: Exit Sub
    If (�sValue� = Empty) Then: Exit Sub
    Select Case (�sColumnKey�)
        Case "OBSERVACAO": eProdutos(�lStructureIndex�).observacao = �sValue�
    End Select
    Exit Sub
         
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HandleEditableColumns' in form FormControloStock (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2010.06.07
' Keypress event for the object TextAuxiliar.
Private Sub TextAuxiliar_KeyPress(�KeyAscii� As Integer)

    Dim lStructureIndex As Long
    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    Select Case (�KeyAscii�)
        Case vbKeyEscape:
            Call HideTextBox(False):
            Call EnableTextAuxiliar(False, rc)
        Case vbKeyReturn:
            lStructureIndex = GetStructureIndex(LvStock.SelectedItem.Index)
            HandleEditableColumns TextAuxiliar.Tag, TextAuxiliar.Text, lStructureIndex
            Call HideTextBox(True)
            Call EnableTextAuxiliar(False, rc)
    End Select
    Exit Sub

ErrorHandler:
    BG_LogFile_Erros "Error in function 'TextAuxiliar_KeyPress' in form FormControloStock (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2010.06.07
' LostFocus event for the object TextAuxiliar.
Private Sub TextAuxiliar_LostFocus()
    
    Dim rc As RECT
    
    On Error GoTo ErrorHandler
    If (TextAuxiliar.Visible) Then: Call HideTextBox(False): Call EnableTextAuxiliar(False, rc)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'TextAuxiliar_LostFocus' in form FormControloStock (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

' pferreira 2010.06.07
' Enable or disable TextAuxiliar.
Private Sub EnableTextAuxiliar(�bEnable� As Boolean, rc As RECT)

    On Error GoTo ErrorHandler
    If (Not �bEnable�) Then
        TextAuxiliar.Visible = False
        TextAuxiliar.Text = Empty
        LvStock.SetFocus
    Else
        TextAuxiliar.Alignment = AlignmentConstants.vbLeftJustify
        TextAuxiliar.Move ((rc.left + 1) * Screen.TwipsPerPixelX), (rc.top * Screen.TwipsPerPixelY) + (2 * Screen.TwipsPerPixelY), ((rc.Right - rc.left) * Screen.TwipsPerPixelX) - 10, (rc.Bottom - rc.top) * Screen.TwipsPerPixelY - 100
        TextAuxiliar.ForeColor = &H808080
        If (LvStock.SelectedItem.Index Mod 2 <> 0) Then: TextAuxiliar.BackColor = &H8000000F
        If (LvStock.SelectedItem.Index Mod 2 = 0) Then: TextAuxiliar.BackColor = vbWhite
        TextAuxiliar.SelStart = 0
        TextAuxiliar.SelLength = Len(TextAuxiliar)
        TextAuxiliar.Visible = True
    End If
    Exit Sub
        
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EnableTextAuxiliar' in form FormFechoRequisicoes (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub

End Sub

' pferreira 2010.06.07
' Hide auxiliar text box.
Friend Sub HideTextBox(bApplyChanges As Boolean)

    On Error GoTo ErrorHandler
    If (bApplyChanges) Then: LvStock.ListItems(lListItemIndex).SubItems(lListSubItem) = TextAuxiliar.Text
    If (Not bApplyChanges) Then: LvStock.ListItems(lListItemIndex).ListSubItems(lListSubItem).ReportIcon = Empty
    Call UnSubClass(lhwndTextBox): lListItemIndex = Empty
    Exit Sub
     
ErrorHandler:
    BG_LogFile_Erros "Error in function 'HideTextBox' in form FormFechoRequisicoes (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Function ActualizaObservacao(observacao As String, lote As String, Produto As Long, cod_local As Integer) As Boolean

    Dim sql As String
    Dim RsDirecao As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Select Case gBD_PREFIXO_TAB
        Case "SB_":
            BG_ExecutaQuery_ADO "update " & gBD_PREFIXO_TAB & "rec_derivados set num_certificado_infarmed = " & BL_TrataStringParaBD(observacao) & " where cod_produto = " & Produto & " and lote_deriv = " & BL_TrataStringParaBD(lote) & " and cod_local = " & cod_local
            BG_Trata_BDErro
        Case "SL_":
            ' Empty
        Case Else:
            ' Empty
    End Select
    ActualizaObservacao = True
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

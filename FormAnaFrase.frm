VERSION 5.00
Begin VB.Form FormAnaFrase 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAnaFrase"
   ClientHeight    =   5565
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8730
   Icon            =   "FormAnaFrase.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5565
   ScaleWidth      =   8730
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtCima 
      Height          =   495
      Left            =   4500
      Picture         =   "FormAnaFrase.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   " Alterar ordem na Complexa "
      Top             =   3720
      Width           =   495
   End
   Begin VB.CommandButton BtBaixo 
      Height          =   495
      Left            =   4500
      Picture         =   "FormAnaFrase.frx":0316
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   " Alterar ordem na Complexa "
      Top             =   4245
      Width           =   495
   End
   Begin VB.CommandButton PesquisaFrase 
      Height          =   735
      Left            =   120
      Picture         =   "FormAnaFrase.frx":0620
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Pesquisar Frases"
      Top             =   2040
      Width           =   735
   End
   Begin VB.ListBox EcLista2Simples 
      Height          =   3765
      Left            =   5160
      TabIndex        =   8
      Top             =   1680
      Width           =   3345
   End
   Begin VB.ListBox EcListaSimples 
      Height          =   3375
      Left            =   990
      TabIndex        =   5
      Top             =   2040
      Width           =   3345
   End
   Begin VB.Frame Frame1 
      Caption         =   "An�lise"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8445
      Begin VB.CheckBox CkAssociaPerfis 
         Caption         =   "Associar a todos os perfis"
         Height          =   255
         Left            =   6120
         TabIndex        =   17
         Top             =   1080
         Width           =   2295
      End
      Begin VB.ComboBox EcDescricaoPerfil 
         Height          =   315
         Left            =   2160
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   720
         Width           =   5175
      End
      Begin VB.TextBox EcCodPerfil 
         Height          =   285
         Left            =   1080
         TabIndex        =   13
         Top             =   720
         Width           =   1065
      End
      Begin VB.TextBox EcCodigo 
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   330
         Width           =   1065
      End
      Begin VB.ComboBox EcDescricaoCombo 
         Height          =   315
         Left            =   2160
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   330
         Width           =   5175
      End
      Begin VB.Label Label3 
         Caption         =   "Simples"
         Height          =   255
         Left            =   210
         TabIndex        =   9
         Top             =   390
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Perfil"
         Height          =   255
         Left            =   210
         TabIndex        =   10
         Top             =   750
         Width           =   915
      End
   End
   Begin VB.TextBox EcPesquisa 
      Height          =   285
      Left            =   990
      TabIndex        =   4
      Top             =   1680
      Width           =   3345
   End
   Begin VB.CommandButton BtInsere 
      Height          =   495
      Left            =   4440
      Picture         =   "FormAnaFrase.frx":0EEA
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2460
      Width           =   615
   End
   Begin VB.CommandButton BtRetira 
      Height          =   495
      Left            =   4440
      Picture         =   "FormAnaFrase.frx":11F4
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3060
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Digite o texto que pretende pesquisar"
      Height          =   255
      Left            =   1020
      TabIndex        =   12
      Top             =   1380
      Width           =   3315
   End
   Begin VB.Label Label4 
      Caption         =   "Frases da An�lise"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5190
      TabIndex        =   11
      Top             =   1380
      Width           =   2085
   End
End
Attribute VB_Name = "FormAnaFrase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String
Dim CriterioTabelaF As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Public CamposBDparaListBoxF
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim ListaOrigem As Control
Dim ListaDestino As Control
    
Public rs As ADODB.recordset
Public RSListaDestino As ADODB.recordset
Public RSListaOrigem As ADODB.recordset


Private Sub BtBaixo_Click()
    Dim sdummy As String
    Dim ldummy As Long
    
    Set ListaOrigem = EcListaSimples
    Set ListaDestino = EcLista2Simples
    
    If ListaDestino.ListIndex < ListaDestino.ListCount - 1 Then
        sdummy = ListaDestino.List(ListaDestino.ListIndex + 1)
        ldummy = ListaDestino.ItemData(ListaDestino.ListIndex + 1)
        ListaDestino.List(ListaDestino.ListIndex + 1) = ListaDestino.List(ListaDestino.ListIndex)
        ListaDestino.ItemData(ListaDestino.ListIndex + 1) = ListaDestino.ItemData(ListaDestino.ListIndex)
        ListaDestino.List(ListaDestino.ListIndex) = sdummy
        ListaDestino.ItemData(ListaDestino.ListIndex) = ldummy
    
        ListaDestino.ListIndex = ListaDestino.ListIndex + 1
    End If
End Sub

Private Sub BtCima_Click()
    Dim sdummy As String
    Dim ldummy As Long
    
    Set ListaOrigem = EcListaSimples
    Set ListaDestino = EcLista2Simples
    
    If ListaDestino.ListIndex > 0 Then
        sdummy = ListaDestino.List(ListaDestino.ListIndex - 1)
        ldummy = ListaDestino.ItemData(ListaDestino.ListIndex - 1)
        ListaDestino.List(ListaDestino.ListIndex - 1) = ListaDestino.List(ListaDestino.ListIndex)
        ListaDestino.ItemData(ListaDestino.ListIndex - 1) = ListaDestino.ItemData(ListaDestino.ListIndex)
        ListaDestino.List(ListaDestino.ListIndex) = sdummy
        ListaDestino.ItemData(ListaDestino.ListIndex) = ldummy
        
        ListaDestino.ListIndex = ListaDestino.ListIndex - 1
    End If
End Sub

Private Sub BtInsere_Click()

    Dim i As Integer
            
    Set ListaOrigem = EcListaSimples
    Set ListaDestino = EcLista2Simples
       
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i
              
End Sub

Private Sub BtRetira_Click()

    Set ListaDestino = EcLista2Simples
    If ListaDestino.ListIndex <> -1 Then
        ListaDestino.RemoveItem (ListaDestino.ListIndex)
    End If

End Sub

Private Sub EcCodigo_GotFocus()

    Set CampoActivo = EcCodigo
    
End Sub



Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    Set CampoActivo = EcCodigo
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    If Cancel = False Then
        EcCodigo.Text = UCase(EcCodigo.Text)
        If Trim(EcCodigo.Text) = "S" Then
            BG_Mensagem mediMsgBox, "O [S] por si s�, n�o pode ser c�digo da an�lise, pois � identificativo do tipo de an�lise!", vbExclamation, "C�digo da Simples"
            Cancel = True
        ElseIf left(Trim(EcCodigo.Text), 1) <> "S" And Trim(EcCodigo.Text) <> "" Then
            EcCodigo.Text = "S" & EcCodigo.Text
        ElseIf Trim(EcCodigo.Text) <> "" Then
            EcCodigo.Text = left(Trim(EcCodigo.Text), 1) & Mid(EcCodigo.Text, 2, Len(EcCodigo.Text) - 1)
        End If
        If EcCodigo <> "" Then
            BL_ColocaTextoCombo "sl_ana_s", "seq_ana_s", "cod_ana_s", EcCodigo, EcDescricaoCombo
            Call ActualizaAnalises(EcCodigo, EcCodPerfil)
        End If
    End If
    
'    If Cancel = False Then
'        BL_ColocaTextoCombo "sl_perfis", "seq_perfis", "cod_perfis", EcCodPerfil, EcDescricaoPerfil
'        Call ActualizaAnalises(EcCodigo, EcCodPerfil)
'    End If
    
        
End Sub

Private Sub EcCodPerfil_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodPerfil_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
    If Cancel = False Then
        EcCodPerfil.Text = UCase(EcCodPerfil.Text)
        If Trim(EcCodPerfil.Text) = "P" Then
            BG_Mensagem mediMsgBox, "O [P] por si s�, n�o pode ser c�digo da an�lise, pois � identificativo do tipo de an�lise!", vbExclamation, "C�digo do Perfil"
            Cancel = True
        ElseIf left(Trim(EcCodPerfil.Text), 1) <> "P" And Trim(EcCodPerfil.Text) <> "" Then
            EcCodPerfil.Text = "P" & EcCodPerfil.Text
        ElseIf Trim(EcCodPerfil.Text) <> "" Then
            EcCodPerfil.Text = left(Trim(EcCodPerfil.Text), 1) & Mid(EcCodPerfil.Text, 2, Len(EcCodPerfil.Text) - 1)
        End If
        If EcCodPerfil.Text <> "" Then
            BL_ColocaTextoCombo "sl_perfis", "seq_perfis", "cod_perfis", EcCodPerfil, EcDescricaoPerfil
            Call ActualizaAnalises(EcCodigo, EcCodPerfil)
        End If
    End If
    
'    If Cancel = False Then
'        BL_ColocaTextoCombo "sl_perfis", "seq_perfis", "cod_perfis", EcCodPerfil, EcDescricaoPerfil
'        Call ActualizaAnalises(EcCodigo, EcCodPerfil)
'    End If
    
    
    
        
End Sub
Private Sub EcDescricaoCombo_Click()

    Dim EcAux As String
    EcAux = EcCodigo
    BL_ColocaComboTexto "sl_ana_s", "seq_ana_s", "cod_ana_s", EcCodigo, EcDescricaoCombo
    If EcAux <> EcCodigo Then EcLista2Simples.Clear
    
    Call ActualizaAnalises(EcCodigo, EcCodPerfil)
   
End Sub

Private Sub EcDescricaoCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcDescricaoCombo.ListIndex = -1
End Sub


Private Sub EcDescricaoPerfil_Click()
    Dim EcAux As String

    EcAux = EcCodPerfil
    BL_ColocaComboTexto "sl_perfis", "seq_perfis", "cod_perfis", EcCodPerfil, EcDescricaoPerfil
    If EcAux <> EcCodPerfil Then EcLista2Simples.Clear
    
    Call ActualizaAnalises(EcCodigo, EcCodPerfil)

End Sub

Private Sub EcDescricaoPerfil_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then EcDescricaoPerfil.ListIndex = -1

End Sub

Private Sub EcLista2Simples_Click()
    EcLista2Simples.ToolTipText = Trim(EcLista2Simples.List(EcLista2Simples.ListIndex))
End Sub

Private Sub EcListaSimples_Click()
    EcListaSimples.ToolTipText = Trim(EcListaSimples.List(EcListaSimples.ListIndex))
End Sub

Private Sub EcPesquisa_Change()
    BL_RefinaPesquisa EcPesquisa, EcListaSimples, "sl_dicionario", "seq_frase", "descr_frase"
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Codifica��o de frases padr�o para an�lises simples de resultado tipo frase."
    Me.left = 540
    Me.top = 450
    Me.Width = 8820
    Me.Height = 6135 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    NomeTabela = "sl_ana_frase"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 2
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_ana_s"
    CamposBD(1) = "cod_perfil"
        
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcCodPerfil
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo da An�lise Simples"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_ana_s"
    
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBoxF = Array("descr_frase")
    NumEspacos = Array(250)
    
End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    If Not RSListaOrigem Is Nothing Then
        RSListaOrigem.Close
        Set RSListaOrigem = Nothing
    End If
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAnaFrase = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    EcLista2Simples.Clear
    EcDescricaoCombo.ListIndex = mediComboValorNull
    EcDescricaoPerfil.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    EcCodigo.Tag = adVarChar
    EcCodPerfil.Tag = adVarChar
End Sub
Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_ana_s", "seq_ana_s", "descr_ana_s", EcDescricaoCombo
    BG_PreencheComboBD_ADO "sl_perfis where flg_activo = 1", "seq_perfis", "descr_perfis", EcDescricaoPerfil
End Sub
Sub PreencheCampos()
        
    Me.SetFocus
    
    BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    
    If Trim(EcCodigo) <> "" Then
        BL_ColocaTextoCombo "sl_ana_s", "seq_ana_s", "cod_ana_s", EcCodigo, EcDescricaoCombo
    End If
    If Trim(EcCodPerfil) <> "" Then
        BL_ColocaTextoCombo "sl_perfis", "seq_perfis", "cod_perfis", EcCodPerfil, EcDescricaoPerfil
    End If
    
    Set RSListaDestino = New ADODB.recordset
    RSListaDestino.CursorType = adOpenStatic
    RSListaDestino.CursorLocation = adUseServer

    CriterioTabelaF = "SELECT sl_dicionario.cod_frase,seq_frase,descr_frase FROM sl_ana_frase,sl_dicionario WHERE cod_ana_s='" & EcCodigo & "' AND sl_ana_frase.cod_frase=sl_dicionario.cod_frase "
    If EcCodPerfil <> "" Then
        CriterioTabelaF = CriterioTabelaF & " AND sl_ana_frase.cod_perfil = " & BL_TrataStringParaBD(EcCodPerfil)
    Else
        CriterioTabelaF = CriterioTabelaF & " AND (sl_ana_frase.cod_perfil is null or sl_ana_frase.cod_perfil = '' ) "
    End If
    CriterioTabelaF = CriterioTabelaF & " order by ordem "
    RSListaDestino.Open CriterioTabelaF, gConexao

    If RSListaDestino.RecordCount > 0 Then
        estado = 2
        ' Inicio do preenchimento de 'EcLista'
        BL_PreencheListBoxMultipla_ADO EcLista2Simples, RSListaDestino, CamposBDparaListBoxF, NumEspacos, CamposBD, CamposEc, "SELECT", , "seq_frase"
    End If
    RSListaDestino.Close
    Set RSListaDestino = Nothing
    
End Sub


Sub FuncaoLimpar()
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista2Simples.Clear
        
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If Trim(EcCodigo) = "" Then
        CriterioTabela = "SELECT distinct cod_ana_s, cod_perfil FROM sl_ana_frase GROUP BY cod_ana_s, cod_perfil "
    Else
        If EcCodPerfil = "" Then
            CriterioTabela = "SELECT distinct cod_ana_s, cod_perfil FROM sl_ana_frase WHERE cod_ana_s ='" & Trim(EcCodigo) & "'"
        Else
            CriterioTabela = "SELECT distinct cod_ana_s, cod_perfil FROM sl_ana_frase WHERE cod_ana_s ='" & Trim(EcCodigo) & "' and cod_perfil = '" & Trim(EcCodPerfil) & "'"
        End If
    End If
    CriterioTabela = CriterioTabela & " order by cod_perfil desc "
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub


Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    If CkAssociaPerfis.value = 1 Then
        gMsgMsg = "Tem a certeza que quer associar estes dados a todos os perfis ?"
    Else
        gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    End If
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLAux As String
    Dim SQLAux2 As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    Dim rsAux As ADODB.recordset
    Dim RsPerfis As ADODB.recordset
    Dim sql As String
        
    SQLAux2 = "DELETE FROM " & NomeTabela & " WHERE cod_ana_s='" & EcCodigo & "'"
    If CkAssociaPerfis.value <> 1 Then
        If EcCodPerfil <> "" Then
            SQLAux2 = SQLAux2 & " AND cod_perfil = '" & EcCodPerfil & "'"
        Else
            SQLAux2 = SQLAux2 & " AND cod_perfil is null "
        End If
    End If
            
    BG_ExecutaQuery_ADO SQLAux2
    If CkAssociaPerfis.value = 1 Then
        Set RsPerfis = New ADODB.recordset
        RsPerfis.CursorLocation = adUseServer
        RsPerfis.CursorType = adOpenStatic
        sql = "select cod_perfis from sl_ana_perfis where cod_analise = " & BL_TrataStringParaBD(EcCodigo) & _
                " Union " & _
                " select cod_perfis from sl_ana_perfis,sl_membro where sl_ana_perfis.cod_analise = sl_membro.cod_ana_c and cod_membro = " & BL_TrataStringParaBD(EcCodigo)
        RsPerfis.Open sql, gConexao
        While Not RsPerfis.EOF
            Set rsAux = New ADODB.recordset
            For i = 0 To EcLista2Simples.ListCount - 1
                SQLAux = "SELECT cod_frase FROM sl_dicionario WHERE seq_frase=" & EcLista2Simples.ItemData(i)
                rsAux.Open SQLAux, gConexao, adOpenStatic, adLockReadOnly
                SQLQuery = "INSERT INTO " & NomeTabela & " (cod_ana_s,cod_frase,cod_perfil,ordem) VALUES ('" & EcCodigo & "','" & Trim(rsAux!cod_frase) & "','" & RsPerfis!cod_perfis & "'," & i & ")"
                BG_ExecutaQuery_ADO SQLQuery

                rsAux.Close
            Next i
            RsPerfis.MoveNext
        Wend
        RsPerfis.Close
        Set RsPerfis = Nothing
    
    Else
    
        Set rsAux = New ADODB.recordset
        For i = 0 To EcLista2Simples.ListCount - 1
            SQLAux = "SELECT cod_frase FROM sl_dicionario WHERE seq_frase=" & EcLista2Simples.ItemData(i)
            rsAux.Open SQLAux, gConexao, adOpenStatic, adLockReadOnly
            SQLQuery = "INSERT INTO " & NomeTabela & " (cod_ana_s,cod_frase,cod_perfil,ordem) VALUES ('" & EcCodigo & "','" & Trim(rsAux!cod_frase) & "','" & EcCodPerfil & "'," & i & ")"
            BG_ExecutaQuery_ADO SQLQuery
            rsAux.Close
        Next i
    End If

    Set rsAux = Nothing
   
    'Permite que a FuncaoInserir utilize esta funcao
    If rs Is Nothing Then Exit Sub Else: If rs.RecordCount <= 0 Then Exit Sub
    
    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos
    
End Sub


Sub FuncaoInserir()
    Dim iRes As Integer
    Dim SQLQuery As String
    Dim rsInsAux As ADODB.recordset

    gMsgTitulo = "Inserir"
    If CkAssociaPerfis.value = 1 Then
        gMsgMsg = "Tem a certeza que quer associar estes dados a todos os perfis ?"
    Else
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    End If
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        Set rsInsAux = New ADODB.recordset
    
        SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & ChaveBD & "='" & EcCodigo & "'"
        If EcCodPerfil <> "" Then
            SQLQuery = SQLQuery & " AND cod_perfil = '" & EcCodPerfil & "'"
        Else
            SQLQuery = SQLQuery & " AND cod_perfil is null "
        End If
    
        rsInsAux.CursorLocation = adUseServer
        rsInsAux.CursorType = adOpenStatic
        rsInsAux.Open SQLQuery, gConexao
    
        If rsInsAux.RecordCount = (-1) Then
            BG_Mensagem mediMsgBox, "Erro a verificar c�digo!", vbExclamation, "Inserir"
        ElseIf rsInsAux.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
        Else
            BL_InicioProcessamento Me, "A inserir registo."
            iRes = ValidaCamposEc
        
            If iRes = True Then
                BD_Update
            End If
            BL_FimProcessamento Me
        End If
        
        rsInsAux.Close
        Set rsInsAux = Nothing
    End If
End Sub


Private Sub PesquisaFrase_Click()
    'preenche Eclista
    
    If RSListaOrigem Is Nothing Then
        Set RSListaOrigem = New ADODB.recordset
        RSListaOrigem.CursorLocation = adUseServer
        RSListaOrigem.CursorType = adOpenStatic
    
        RSListaOrigem.Open "SELECT * FROM sl_dicionario", gConexao
        BL_PreencheListBoxMultipla_ADO EcListaSimples, RSListaOrigem, CamposBDparaListBoxF, NumEspacos, CamposBD, CamposEc, "SELECT", , "seq_frase"
    End If
    
End Sub
Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    If EcCodPerfil <> "" Then
        SQLQuery = SQLQuery & " AND cod_perfil = '" & EcCodPerfil & "'"
    End If
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
        
    LimpaCampos
    PreencheCampos

End Sub

Sub ActualizaAnalises(codAnaS As String, CodPerfil As String)
    Dim sql As String
    Dim rsAna As ADODB.recordset
    Dim i As Integer
    
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If codAnaS <> "" Then
        sql = "select seq_perfis, sl_perfis.cod_perfis, descr_perfis " & _
                "from sl_ana_perfis, sl_perfis " & _
                "Where sl_ana_perfis.cod_perfis = sl_perfis.cod_perfis " & _
                "and cod_analise = " & BL_TrataStringParaBD(codAnaS)
        If CodPerfil <> "" Then
            sql = sql & " and sl_ana_perfis.cod_perfis = " & BL_TrataStringParaBD(CodPerfil) & " "
        End If
        sql = sql & _
                "Union " & _
                "select seq_perfis, sl_perfis.cod_perfis, descr_perfis " & _
                "From sl_ana_perfis, sl_perfis, sl_membro " & _
                "where sl_ana_perfis.cod_perfis = sl_perfis.cod_perfis and " & _
                "sl_ana_perfis.cod_analise = sl_membro.cod_ana_c and " & _
                "sl_membro.cod_membro = " & BL_TrataStringParaBD(codAnaS)
        If CodPerfil <> "" Then
            sql = sql & " and sl_ana_perfis.cod_perfis = " & BL_TrataStringParaBD(CodPerfil) & " "
        End If
        rsAna.Open sql, gConexao
        If rsAna.RecordCount <= 0 And CodPerfil <> "" Then
            BG_Mensagem mediMsgBox, "An�lise simples n�o pertencente a esse Perfil!", vbInformation, "Rela��o de Perfis e Simples"
            EcCodPerfil.Text = ""
            EcCodPerfil.SetFocus
            Exit Sub
        End If
        
        If EcCodPerfil = "" Then
            EcDescricaoPerfil.Clear
        End If

        i = 0
        Do Until rsAna.EOF
            EcDescricaoPerfil.AddItem RTrim(rsAna!descr_perfis)
            EcDescricaoPerfil.ItemData(i) = CLng(rsAna!seq_perfis)
            rsAna.MoveNext
            i = i + 1
        Loop
        rsAna.Close

    ElseIf EcCodPerfil <> "" Then
    
        sql = "select sl_ana_s.seq_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.descr_ana_s " & _
                "from sl_ana_s, sl_ana_perfis " & _
                "Where sl_ana_perfis.cod_analise = sl_ana_s.cod_ana_s " & _
                "and sl_ana_perfis.cod_perfis = " & BL_TrataStringParaBD(CodPerfil) & _
                " Union " & _
                "select seq_ana_s, cod_ana_s, descr_ana_s " & _
                "from sl_ana_s, sl_membro, sl_ana_perfis " & _
                "Where sl_ana_perfis.cod_analise = sl_membro.Cod_Ana_C " & _
                "and sl_membro.cod_membro = sl_ana_s.cod_ana_s " & _
                "and sl_ana_perfis.cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
                rsAna.Open sql, gConexao

        EcDescricaoCombo.Clear

        i = 0
        Do Until rsAna.EOF
            EcDescricaoCombo.AddItem RTrim(rsAna!descr_ana_s)
            EcDescricaoCombo.ItemData(i) = CLng(rsAna!seq_ana_s)
            rsAna.MoveNext
            i = i + 1
        Loop
        rsAna.Close
    
    Else
        BG_PreencheComboBD_ADO "sl_ana_s", "seq_ana_s", "descr_ana_s", EcDescricaoCombo
        BG_PreencheComboBD_ADO "sl_perfis where flg_activo = 1", "seq_perfis", "descr_perfis", EcDescricaoPerfil
    End If
End Sub

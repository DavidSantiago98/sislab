VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormGesReqCons 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gest�o Requisi��es"
   ClientHeight    =   11085
   ClientLeft      =   60
   ClientTop       =   3120
   ClientWidth     =   11055
   Icon            =   "FormGesReqCons.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   11085
   ScaleWidth      =   11055
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.TextBox EcDescrArquivo 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5640
      Locked          =   -1  'True
      TabIndex        =   242
      TabStop         =   0   'False
      Top             =   14685
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.CommandButton BtPesquisaArquivo 
      Height          =   315
      Left            =   9360
      Picture         =   "FormGesReqCons.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   241
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de M�dicos "
      Top             =   14685
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodArquivo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4920
      TabIndex        =   240
      Top             =   14685
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcTAgenda 
      Enabled         =   0   'False
      Height          =   285
      Left            =   10560
      Locked          =   -1  'True
      TabIndex        =   239
      Top             =   8880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CheckBox CkFlgInvisivel 
      Caption         =   "Marca��o efectivada ou cancelada"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Left            =   6840
      TabIndex        =   234
      Top             =   1560
      Width           =   3975
   End
   Begin VB.TextBox EcInfComplementar 
      Height          =   285
      Left            =   4320
      Locked          =   -1  'True
      TabIndex        =   233
      Top             =   11370
      Width           =   1335
   End
   Begin VB.TextBox EcPrColheita 
      Height          =   285
      Left            =   4200
      TabIndex        =   223
      Top             =   11160
      Width           =   495
   End
   Begin VB.TextBox EcAuxTubo 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   209
      Top             =   10680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcPesqRapTubo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9960
      TabIndex        =   208
      Top             =   9120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcDataChegada 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6840
      Locked          =   -1  'True
      TabIndex        =   201
      Top             =   10560
      Width           =   1095
   End
   Begin VB.CommandButton BtGetSONHO 
      Enabled         =   0   'False
      Height          =   315
      Left            =   5160
      MaskColor       =   &H00C0C0FF&
      Picture         =   "FormGesReqCons.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   105
      ToolTipText     =   "Importar do SONHO"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcRequAX 
      Height          =   285
      Left            =   3840
      TabIndex        =   91
      Top             =   10440
      Width           =   615
   End
   Begin VB.TextBox EcPesqRapArquivo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7200
      TabIndex        =   89
      Top             =   9960
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapMedico2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7200
      TabIndex        =   87
      Top             =   9000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcGrupoAna 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9360
      TabIndex        =   85
      Top             =   9000
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   9600
      TabIndex        =   84
      Top             =   10200
      Width           =   1095
   End
   Begin VB.TextBox EcPrinterResumo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   83
      Top             =   9000
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcImprimirResumo 
      Height          =   285
      Left            =   9600
      TabIndex        =   82
      Top             =   9840
      Width           =   975
   End
   Begin VB.TextBox EcEtqNCopias 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   80
      Top             =   9120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNEsp 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   78
      Top             =   9360
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqTipo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   76
      Top             =   10080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcReqAssociada 
      Appearance      =   0  'Flat
      BackColor       =   &H00E9FEFD&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6840
      TabIndex        =   4
      ToolTipText     =   "Requisi��o Associada"
      Top             =   120
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "Dados do Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   20
      Top             =   480
      Width           =   10815
      Begin VB.ComboBox CbTipoUtente 
         BackColor       =   &H00E9FEFD&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton BtPesquisaUtente 
         Height          =   315
         Left            =   5160
         Picture         =   "FormGesReqCons.frx":08A8
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Pesquisa R�pida de Utentes"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E9FEFD&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6720
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   1095
      End
      Begin VB.TextBox EcProcHosp2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E9FEFD&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9000
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox EcNumCartaoUte 
         Appearance      =   0  'Flat
         BackColor       =   &H00E9FEFD&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9000
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BackColor       =   &H00E9FEFD&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         TabIndex        =   9
         Top             =   240
         Width           =   2655
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BackColor       =   &H00E9FEFD&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox EcProcHosp1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E9FEFD&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7440
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Data de Entrada"
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label LbReqAssoci 
         AutoSize        =   -1  'True
         Caption         =   "Cart�o Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7920
         TabIndex        =   18
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   990
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5760
         TabIndex        =   16
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   795
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Processo Hospitalar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5760
         TabIndex        =   11
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   14
         Top             =   600
         Width           =   405
      End
   End
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   74
      Top             =   8880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNTubos 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   72
      Top             =   9600
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNAdm 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   70
      Top             =   9840
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcTEFR 
      BackColor       =   &H00EDFAED&
      Enabled         =   0   'False
      Height          =   285
      Left            =   6240
      TabIndex        =   68
      Top             =   10200
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcAuxAna 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      MaxLength       =   9
      TabIndex        =   66
      Top             =   10200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSComctlLib.ImageList ListaImagensAna 
      Left            =   8640
      Top             =   10080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":0E32
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":0F8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":10E6
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":1240
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":139A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":14F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":164E
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":17A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":1902
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":1A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":1BB6
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGesReqCons.frx":1D10
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.TextBox EcAuxProd 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   64
      Top             =   10320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcCodIsencao 
      Height          =   285
      Left            =   7200
      TabIndex        =   62
      Top             =   9720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton BtCancelarReq 
      Height          =   315
      Left            =   5160
      Picture         =   "FormGesReqCons.frx":1E6A
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Raz�o cancelamento"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox CodAnaTemp 
      Height          =   285
      Left            =   5160
      TabIndex        =   60
      Top             =   9240
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEpisodio 
      Appearance      =   0  'Flat
      BackColor       =   &H00E9FEFD&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9120
      TabIndex        =   6
      Top             =   120
      Width           =   1575
   End
   Begin VB.TextBox EcNumReq 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00E9FEFD&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   1440
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapProduto 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9360
      TabIndex        =   58
      Top             =   9120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcSeqUtente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3000
      TabIndex        =   57
      Top             =   9240
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Timer TimerRS 
      Interval        =   60000
      Left            =   -120
      Top             =   8880
   End
   Begin VB.ComboBox CbEstadoReqAux 
      Height          =   315
      Left            =   8160
      Style           =   2  'Dropdown List
      TabIndex        =   55
      TabStop         =   0   'False
      Top             =   9480
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox EcPesqRapMedico 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7200
      TabIndex        =   53
      Top             =   9240
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapProven 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7200
      TabIndex        =   51
      Top             =   9480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEstadoReq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3120
      TabIndex        =   49
      Top             =   9000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CbEstadoReq 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   315
      Left            =   2400
      Style           =   1  'Simple Combo
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   120
      Width           =   3255
   End
   Begin VB.TextBox EcTaxaModeradora 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5160
      TabIndex        =   45
      Top             =   9480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPagEnt 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5160
      TabIndex        =   44
      Top             =   9720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPagUte 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3000
      TabIndex        =   43
      Top             =   9480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Frame Frame6 
      Caption         =   "Registo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   41
      Top             =   6120
      Width           =   10815
      Begin VB.TextBox EcDataAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         TabIndex        =   29
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcHoraAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         TabIndex        =   30
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcUtilizadorAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   27
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcHoraImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9600
         TabIndex        =   35
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   6480
         TabIndex        =   37
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcDataImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8640
         TabIndex        =   34
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcHoraImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9600
         TabIndex        =   40
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   6480
         TabIndex        =   32
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcDataImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8640
         TabIndex        =   39
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcHoraCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         TabIndex        =   25
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox EcDataCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         TabIndex        =   24
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   22
         Top             =   240
         Width           =   615
      End
      Begin VB.Label LbNomeUtilImpressao2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   7080
         TabIndex        =   38
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label LbNomeUtilImpressao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   7080
         TabIndex        =   33
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label LbNomeUtilCriacao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   23
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label LbNomeUtilAlteracao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   28
         Top             =   600
         Width           =   2415
      End
      Begin VB.Label Label18 
         Caption         =   "�ltima 2� Via"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5400
         TabIndex        =   36
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label17 
         Caption         =   "Impress�o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5400
         TabIndex        =   31
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label16 
         Caption         =   "Altera��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Cria��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   21
         Top             =   240
         Width           =   735
      End
   End
   Begin TabDlg.SSTab SSTGestReq 
      Height          =   4455
      Left            =   120
      TabIndex        =   106
      Top             =   1560
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   7858
      _Version        =   393216
      Style           =   1
      Tabs            =   6
      Tab             =   5
      TabsPerRow      =   6
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Dados da Requisi��o"
      TabPicture(0)   =   "FormGesReqCons.frx":24F4
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "LaMedico"
      Tab(0).Control(1)=   "LaDataFact"
      Tab(0).Control(2)=   "LaSemanas"
      Tab(0).Control(3)=   "LaFicha"
      Tab(0).Control(4)=   "LaReqaux"
      Tab(0).Control(5)=   "LaObsProveniencia"
      Tab(0).Control(6)=   "LaMedico2"
      Tab(0).Control(7)=   "Label15"
      Tab(0).Control(8)=   "Label6"
      Tab(0).Control(9)=   "Label4"
      Tab(0).Control(10)=   "Label3(0)"
      Tab(0).Control(11)=   "Label9"
      Tab(0).Control(12)=   "Label10"
      Tab(0).Control(13)=   "Label11"
      Tab(0).Control(14)=   "Label1(3)"
      Tab(0).Control(15)=   "Label1(16)"
      Tab(0).Control(16)=   "Label1(13)"
      Tab(0).Control(17)=   "Label1(15)"
      Tab(0).Control(18)=   "Label3(1)"
      Tab(0).Control(19)=   "EcCodMedico"
      Tab(0).Control(20)=   "EcDescrMedico"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "BtPesquisaMedico"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "EcDataFact"
      Tab(0).Control(23)=   "EcFicha"
      Tab(0).Control(24)=   "EcReqAux"
      Tab(0).Control(25)=   "EcObsProveniencia"
      Tab(0).Control(26)=   "EcCodMedico2"
      Tab(0).Control(27)=   "EcDescrMedico2"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "BtPesquisaMedico2"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "EcObsReq"
      Tab(0).Control(30)=   "CbUrgencia"
      Tab(0).Control(31)=   "CbSituacao"
      Tab(0).Control(32)=   "EcDataPrevista"
      Tab(0).Control(33)=   "BtPesquisaProveniencia"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "EcDescrProveniencia"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "EcCodProveniencia"
      Tab(0).Control(36)=   "BtPesquisaEntFin"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "EcDescrEFR"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "EcCodEFR"
      Tab(0).Control(39)=   "CkIsento"
      Tab(0).Control(40)=   "Frame4"
      Tab(0).Control(41)=   "FrEtiq"
      Tab(0).Control(42)=   "Frame1"
      Tab(0).Control(43)=   "CbTipoIsencao"
      Tab(0).Control(44)=   "EcNumBenef"
      Tab(0).Control(45)=   "Frame3"
      Tab(0).Control(46)=   "Frame8"
      Tab(0).Control(47)=   "Frame9"
      Tab(0).Control(48)=   "CbPrioColheita"
      Tab(0).Control(49)=   "EcPrescricao"
      Tab(0).Control(50)=   "CbDestino"
      Tab(0).Control(51)=   "EcCodValencia"
      Tab(0).Control(52)=   "EcDescrValencia"
      Tab(0).Control(52).Enabled=   0   'False
      Tab(0).Control(53)=   "BtPesquisaValencia"
      Tab(0).Control(53).Enabled=   0   'False
      Tab(0).Control(54)=   "EcDtMarcacao"
      Tab(0).ControlCount=   55
      TabCaption(1)   =   "Pr&odutos"
      TabPicture(1)   =   "FormGesReqCons.frx":2510
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "ListaDiagnSec"
      Tab(1).Control(1)=   "FrProdutos"
      Tab(1).Control(2)=   "FrBtProd"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "&An�lises"
      TabPicture(2)   =   "FormGesReqCons.frx":252C
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FrAnalises"
      Tab(2).Control(1)=   "FrameMarcaArs"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Reci&bos"
      TabPicture(3)   =   "FormGesReqCons.frx":2548
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FrTxMod"
      Tab(3).Control(1)=   "FrVPU"
      Tab(3).Control(2)=   "FrVPE"
      Tab(3).Control(3)=   "FrBtRecibos"
      Tab(3).Control(4)=   "FrListaRecibos"
      Tab(3).ControlCount=   5
      TabCaption(4)   =   "Reci&bos"
      TabPicture(4)   =   "FormGesReqCons.frx":2564
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      TabCaption(5)   =   "&Tubos"
      TabPicture(5)   =   "FormGesReqCons.frx":2580
      Tab(5).ControlEnabled=   -1  'True
      Tab(5).Control(0)=   "FrBtTubos"
      Tab(5).Control(0).Enabled=   0   'False
      Tab(5).Control(1)=   "FrTubos"
      Tab(5).Control(1).Enabled=   0   'False
      Tab(5).ControlCount=   2
      Begin VB.TextBox EcDtMarcacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   244
         Top             =   480
         Width           =   1335
      End
      Begin VB.Frame FrameMarcaArs 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   -75000
         TabIndex        =   235
         Top             =   3960
         Width           =   6375
         Begin VB.TextBox EcAnaEfr 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   120
            TabIndex        =   237
            Top             =   120
            Width           =   3135
         End
         Begin VB.TextBox EcCredAct 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   205
            Left            =   4320
            Locked          =   -1  'True
            TabIndex        =   236
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label Label1 
            Caption         =   "Credencial"
            Height          =   255
            Index           =   32
            Left            =   3480
            TabIndex        =   238
            Top             =   120
            Width           =   975
         End
      End
      Begin VB.CommandButton BtPesquisaValencia 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGesReqCons.frx":259C
         Style           =   1  'Graphical
         TabIndex        =   231
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   2760
         Width           =   375
      End
      Begin VB.TextBox EcDescrValencia 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   230
         TabStop         =   0   'False
         Top             =   2760
         Width           =   3735
      End
      Begin VB.TextBox EcCodValencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   229
         Top             =   2760
         Width           =   735
      End
      Begin VB.ComboBox CbDestino 
         Height          =   315
         Left            =   -73680
         Style           =   2  'Dropdown List
         TabIndex        =   227
         Top             =   1920
         Width           =   1335
      End
      Begin VB.TextBox EcPrescricao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73680
         Locked          =   -1  'True
         TabIndex        =   225
         Top             =   960
         Width           =   1335
      End
      Begin VB.ComboBox CbPrioColheita 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70560
         Style           =   2  'Dropdown List
         TabIndex        =   221
         Top             =   1920
         Width           =   1335
      End
      Begin VB.Frame Frame9 
         Height          =   615
         Left            =   -67200
         TabIndex        =   218
         Top             =   930
         Width           =   1455
         Begin VB.CommandButton BtNorma 
            Height          =   435
            Left            =   960
            Picture         =   "FormGesReqCons.frx":2B26
            Style           =   1  'Graphical
            TabIndex        =   219
            ToolTipText     =   "Imprimir  Norma de Colheita"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label58 
            Caption         =   "Norma"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   220
            ToolTipText     =   "Imprimir  Norma de Colheita"
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.Frame Frame8 
         Height          =   615
         Left            =   -65760
         TabIndex        =   215
         Top             =   930
         Width           =   1335
         Begin VB.CommandButton BtAgenda 
            Height          =   435
            Left            =   840
            Picture         =   "FormGesReqCons.frx":37F0
            Style           =   1  'Graphical
            TabIndex        =   216
            ToolTipText     =   "Abrir Agenda"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label58 
            Caption         =   "Agenda"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   217
            ToolTipText     =   "Abrir Agenda"
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.Frame Frame3 
         Height          =   615
         Left            =   -68640
         TabIndex        =   211
         Top             =   930
         Width           =   1455
         Begin VB.CommandButton BtPassaMarcacao 
            Height          =   480
            Left            =   960
            Picture         =   "FormGesReqCons.frx":3902
            Style           =   1  'Graphical
            TabIndex        =   212
            ToolTipText     =   " Criar Requisi��o "
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label56 
            Caption         =   "Criar Req."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   213
            ToolTipText     =   "Criar Requisi��o"
            Top             =   240
            Width           =   855
         End
      End
      Begin VB.Frame FrTubos 
         Height          =   2415
         Left            =   120
         TabIndex        =   206
         Top             =   380
         Width           =   10575
         Begin MSFlexGridLib.MSFlexGrid FGTubos 
            Height          =   2240
            Left            =   30
            TabIndex        =   207
            Top             =   120
            Width           =   10500
            _ExtentX        =   18521
            _ExtentY        =   3942
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrBtTubos 
         Height          =   495
         Left            =   120
         TabIndex        =   203
         Top             =   2780
         Width           =   10575
         Begin VB.CommandButton BtPesquisaTubos 
            Height          =   375
            Left            =   0
            Picture         =   "FormGesReqCons.frx":4006
            Style           =   1  'Graphical
            TabIndex        =   204
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.Label LaTubos 
            Height          =   345
            Left            =   1320
            TabIndex        =   205
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -67500
         TabIndex        =   186
         Top             =   2100
         Width           =   3065
      End
      Begin VB.ComboBox CbTipoIsencao 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -67560
         Style           =   2  'Dropdown List
         TabIndex        =   185
         Top             =   2685
         Width           =   3135
      End
      Begin VB.Frame FrListaRecibos 
         Height          =   2535
         Left            =   -67560
         TabIndex        =   181
         Top             =   360
         Visible         =   0   'False
         Width           =   3255
         Begin VB.ListBox ListaRecibos 
            Height          =   1635
            Left            =   120
            Style           =   1  'Checkbox
            TabIndex        =   184
            Top             =   240
            Width           =   3015
         End
         Begin VB.CommandButton BtOk 
            Caption         =   "BtOk"
            Height          =   375
            Left            =   600
            TabIndex        =   183
            Top             =   2040
            Width           =   975
         End
         Begin VB.CommandButton BtCanc 
            Caption         =   "BtCanc"
            Height          =   375
            Left            =   1680
            TabIndex        =   182
            Top             =   2040
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Height          =   600
         Left            =   -65760
         TabIndex        =   178
         ToolTipText     =   "Copiar Dados da Requisi��o Anterior"
         Top             =   360
         Width           =   1335
         Begin VB.CommandButton BtDadosAnteriores 
            Height          =   480
            Left            =   830
            Picture         =   "FormGesReqCons.frx":4590
            Style           =   1  'Graphical
            TabIndex        =   179
            ToolTipText     =   "Copiar Dados da Requisi��o Anterior"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaCopiarAnterior 
            Caption         =   "Copiar a&nterior."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   180
            ToolTipText     =   "Copiar Dados da Requisi��o Anterior"
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.Frame FrEtiq 
         Height          =   605
         Left            =   -67200
         TabIndex        =   175
         ToolTipText     =   "Imprimir Etiquetas de C�digo de Barras"
         Top             =   360
         Width           =   1455
         Begin VB.CommandButton BtEtiq 
            Height          =   480
            Left            =   960
            Picture         =   "FormGesReqCons.frx":4C7A
            Style           =   1  'Graphical
            TabIndex        =   176
            ToolTipText     =   "Imprimir Etiquetas de C�digo de Barras"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaEtiq 
            Caption         =   "Eti&quetas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   177
            ToolTipText     =   "Imprimir Etiquetas de C�digo de Barras"
            Top             =   240
            Width           =   735
         End
      End
      Begin VB.Frame Frame4 
         Height          =   600
         Left            =   -68640
         TabIndex        =   172
         ToolTipText     =   "Informa��o Cl�nica do Utente"
         Top             =   360
         Width           =   1455
         Begin VB.CommandButton BtInfClinica 
            Enabled         =   0   'False
            Height          =   480
            Left            =   960
            Picture         =   "FormGesReqCons.frx":53E4
            Style           =   1  'Graphical
            TabIndex        =   173
            ToolTipText     =   "Informa��o Cl�nica do Utente"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaInfCli 
            Caption         =   "Inf. C&l�nica"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   174
            ToolTipText     =   "Informa��o Cl�nica do Utente"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.CheckBox CkIsento 
         Caption         =   "&Isento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   -68520
         TabIndex        =   171
         Top             =   2760
         Width           =   855
      End
      Begin VB.TextBox EcCodEFR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   170
         Top             =   3180
         Width           =   735
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   169
         TabStop         =   0   'False
         Top             =   3180
         Width           =   3735
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGesReqCons.frx":5B4E
         Style           =   1  'Graphical
         TabIndex        =   168
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   3180
         Width           =   375
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   167
         Top             =   2280
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   166
         TabStop         =   0   'False
         Top             =   2280
         Width           =   3735
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGesReqCons.frx":60D8
         Style           =   1  'Graphical
         TabIndex        =   165
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   2280
         Width           =   375
      End
      Begin VB.Frame FrBtRecibos 
         Height          =   495
         Left            =   -74880
         TabIndex        =   159
         Top             =   2880
         Width           =   10575
         Begin VB.ComboBox CbTipoAnulacao 
            Height          =   315
            Left            =   3840
            Style           =   2  'Dropdown List
            TabIndex        =   162
            Top             =   120
            Visible         =   0   'False
            Width           =   4815
         End
         Begin VB.CommandButton BtAnula 
            Enabled         =   0   'False
            Height          =   375
            Left            =   3120
            Picture         =   "FormGesReqCons.frx":6662
            Style           =   1  'Graphical
            TabIndex        =   161
            Top             =   120
            Width           =   615
         End
         Begin VB.CommandButton BtReimpr 
            Enabled         =   0   'False
            Height          =   375
            Left            =   960
            Picture         =   "FormGesReqCons.frx":67AC
            Style           =   1  'Graphical
            TabIndex        =   160
            Top             =   120
            Width           =   615
         End
         Begin VB.Label Label14 
            Caption         =   "Anular recibo "
            Height          =   255
            Left            =   2040
            TabIndex        =   164
            Top             =   195
            Width           =   1455
         End
         Begin VB.Label Label13 
            Caption         =   "Reimprimir"
            Height          =   255
            Left            =   120
            TabIndex        =   163
            Top             =   200
            Width           =   855
         End
      End
      Begin VB.Frame FrVPE 
         Caption         =   "Valor pago pela Entidade"
         Height          =   855
         Left            =   -74880
         TabIndex        =   150
         Top             =   2040
         Width           =   10575
         Begin VB.TextBox NumRecEnt 
            Height          =   285
            Left            =   8280
            Locked          =   -1  'True
            TabIndex        =   154
            Top             =   360
            Width           =   2055
         End
         Begin VB.TextBox DataEnt 
            Height          =   285
            Left            =   5760
            Locked          =   -1  'True
            TabIndex        =   153
            Top             =   360
            Width           =   1215
         End
         Begin VB.TextBox ValorEnt 
            Height          =   285
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   152
            Top             =   360
            Width           =   1695
         End
         Begin VB.ComboBox CbPagEnt 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   151
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label LaMoeda3 
            Caption         =   "LaMoeda3"
            Height          =   255
            Left            =   4320
            TabIndex        =   158
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label48 
            Caption         =   "N� Recibo :"
            Height          =   255
            Left            =   7320
            TabIndex        =   157
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label47 
            Caption         =   "Data :"
            Height          =   255
            Left            =   5160
            TabIndex        =   156
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label46 
            Caption         =   "Valor :"
            Height          =   255
            Left            =   2040
            TabIndex        =   155
            Top             =   360
            Width           =   495
         End
      End
      Begin VB.Frame FrVPU 
         Caption         =   "Valor pago pelo Utente"
         Height          =   855
         Left            =   -74880
         TabIndex        =   141
         Top             =   1200
         Width           =   10575
         Begin VB.TextBox NumRecDoe 
            Height          =   285
            Left            =   8280
            Locked          =   -1  'True
            TabIndex        =   145
            Top             =   360
            Width           =   2055
         End
         Begin VB.TextBox DataDoe 
            Height          =   285
            Left            =   5760
            Locked          =   -1  'True
            TabIndex        =   144
            Top             =   360
            Width           =   1215
         End
         Begin VB.TextBox ValorDoe 
            Height          =   285
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   143
            Top             =   360
            Width           =   1695
         End
         Begin VB.ComboBox CbPagUte 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   142
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label LaMoeda2 
            Caption         =   "LaMoeda2"
            Height          =   255
            Left            =   4320
            TabIndex        =   149
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label51 
            Caption         =   "N� Recibo :"
            Height          =   255
            Left            =   7320
            TabIndex        =   148
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label50 
            Caption         =   "Data :"
            Height          =   255
            Left            =   5160
            TabIndex        =   147
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label49 
            Caption         =   "Valor :"
            Height          =   255
            Left            =   2040
            TabIndex        =   146
            Top             =   360
            Width           =   495
         End
      End
      Begin VB.Frame FrAnalises 
         Height          =   3600
         Left            =   -74880
         TabIndex        =   137
         Top             =   380
         Width           =   10575
         Begin VB.CommandButton BtPesquisaAna 
            Height          =   375
            Left            =   10170
            Picture         =   "FormGesReqCons.frx":68F6
            Style           =   1  'Graphical
            TabIndex        =   138
            Top             =   90
            Width           =   375
         End
         Begin MSFlexGridLib.MSFlexGrid FGAna 
            Height          =   3330
            Left            =   30
            TabIndex        =   139
            TabStop         =   0   'False
            Top             =   120
            Width           =   7545
            _ExtentX        =   13309
            _ExtentY        =   5874
            _Version        =   393216
            BackColorBkg    =   -2147483633
            SelectionMode   =   1
            AllowUserResizing=   3
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.TreeView TAna 
            Height          =   3345
            Left            =   7575
            TabIndex        =   140
            ToolTipText     =   "Duplo click para saltar para a an�lise."
            Top             =   120
            Width           =   2955
            _ExtentX        =   5212
            _ExtentY        =   5900
            _Version        =   393217
            Indentation     =   9999800
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   1
         End
      End
      Begin VB.Frame FrBtProd 
         Height          =   495
         Left            =   -74880
         TabIndex        =   133
         Top             =   2780
         Width           =   10575
         Begin VB.CommandButton BtPesquisaEspecif 
            Height          =   375
            Left            =   360
            Picture         =   "FormGesReqCons.frx":6E80
            Style           =   1  'Graphical
            TabIndex        =   135
            ToolTipText     =   "Pesquisa R�pida de Especifica��es de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaProduto 
            Height          =   375
            Left            =   0
            Picture         =   "FormGesReqCons.frx":740A
            Style           =   1  'Graphical
            TabIndex        =   134
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.Label LaProdutos 
            Height          =   345
            Left            =   1320
            TabIndex        =   136
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.Frame FrProdutos 
         Height          =   2415
         Left            =   -74880
         TabIndex        =   131
         Top             =   380
         Width           =   10575
         Begin MSFlexGridLib.MSFlexGrid FgProd 
            Height          =   2240
            Left            =   30
            TabIndex        =   132
            Top             =   120
            Width           =   10500
            _ExtentX        =   18521
            _ExtentY        =   3942
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.TextBox EcDataPrevista 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -70560
         TabIndex        =   130
         ToolTipText     =   "Data Pretendida dos Resultados(Consulta)"
         Top             =   480
         Width           =   1335
      End
      Begin VB.ComboBox CbSituacao 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73680
         Style           =   2  'Dropdown List
         TabIndex        =   129
         Top             =   1440
         Width           =   1335
      End
      Begin VB.ComboBox CbUrgencia 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70560
         Style           =   2  'Dropdown List
         TabIndex        =   128
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox EcObsReq 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         MultiLine       =   -1  'True
         TabIndex        =   127
         Top             =   4080
         Width           =   9255
      End
      Begin VB.ListBox ListaDiagnSec 
         Height          =   2790
         Left            =   -68520
         TabIndex        =   126
         Top             =   4140
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.Frame FrTxMod 
         Caption         =   "Taxa Moderadora"
         Height          =   855
         Left            =   -74880
         TabIndex        =   117
         Top             =   360
         Width           =   10575
         Begin VB.TextBox NumRecTM 
            Height          =   285
            Left            =   8280
            Locked          =   -1  'True
            TabIndex        =   121
            Top             =   360
            Width           =   2055
         End
         Begin VB.TextBox DataTM 
            Height          =   285
            Left            =   5760
            Locked          =   -1  'True
            TabIndex        =   120
            Top             =   360
            Width           =   1215
         End
         Begin VB.TextBox ValorTM 
            Height          =   285
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   119
            Top             =   360
            Width           =   1695
         End
         Begin VB.ComboBox CbTaxaModeradora 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   118
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label LaMoeda1 
            Caption         =   "LaMoeda1"
            Height          =   255
            Left            =   4320
            TabIndex        =   125
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label45 
            Caption         =   "N� Recibo :"
            Height          =   255
            Left            =   7320
            TabIndex        =   124
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label44 
            Caption         =   "Data :"
            Height          =   255
            Left            =   5160
            TabIndex        =   123
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label43 
            Caption         =   "Valor :"
            Height          =   255
            Left            =   2040
            TabIndex        =   122
            Top             =   360
            Width           =   495
         End
      End
      Begin VB.CommandButton BtPesquisaMedico2 
         Height          =   315
         Left            =   -64800
         Picture         =   "FormGesReqCons.frx":7994
         Style           =   1  'Graphical
         TabIndex        =   116
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de M�dicos "
         Top             =   3240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox EcDescrMedico2 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -66900
         Locked          =   -1  'True
         TabIndex        =   115
         TabStop         =   0   'False
         Top             =   3240
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.TextBox EcCodMedico2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -67500
         TabIndex        =   114
         Top             =   3240
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox EcObsProveniencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -67500
         TabIndex        =   113
         Top             =   1680
         Width           =   3075
      End
      Begin VB.TextBox EcReqAux 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   315
         Left            =   -67500
         TabIndex        =   112
         Top             =   3645
         Visible         =   0   'False
         Width           =   1080
      End
      Begin VB.TextBox EcFicha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -65400
         TabIndex        =   111
         Top             =   3645
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox EcDataFact 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   -65520
         TabIndex        =   110
         Top             =   1680
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton BtPesquisaMedico 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGesReqCons.frx":7F1E
         Style           =   1  'Graphical
         TabIndex        =   109
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de M�dicos "
         Top             =   3600
         Width           =   375
      End
      Begin VB.TextBox EcDescrMedico 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   108
         TabStop         =   0   'False
         Top             =   3600
         Width           =   3735
      End
      Begin VB.TextBox EcCodMedico 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   107
         Top             =   3600
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Dt  Colheita"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   -74760
         TabIndex        =   245
         ToolTipText     =   "Data Prevista para Colheita"
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Val�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   -74760
         TabIndex        =   232
         Top             =   2760
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Destino"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   13
         Left            =   -74760
         TabIndex        =   228
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Prescri��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   -74760
         TabIndex        =   226
         Top             =   1020
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Prior Colheita"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   -71760
         TabIndex        =   222
         Top             =   1920
         Width           =   1395
      End
      Begin VB.Label Label11 
         Caption         =   "N�. Benefici�rio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -68700
         TabIndex        =   200
         Top             =   2100
         Width           =   1335
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   199
         Top             =   3180
         Width           =   495
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   198
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Dt  Resultados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   -71760
         TabIndex        =   197
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   196
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label6 
         Caption         =   "Prior. Requis."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -71760
         TabIndex        =   195
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label15 
         Caption         =   "Obs. &Final"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   194
         Top             =   4080
         Width           =   975
      End
      Begin VB.Label LaMedico2 
         Caption         =   "Respons. &Fish"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -68700
         TabIndex        =   193
         Top             =   3240
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.Label LaObsProveniencia 
         Caption         =   "Obs. Proveni�n."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -68700
         TabIndex        =   192
         Top             =   1680
         Width           =   1500
      End
      Begin VB.Label LaReqaux 
         Caption         =   "Nr. &An�lise"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -68700
         TabIndex        =   191
         Top             =   3645
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label LaFicha 
         Caption         =   "Proc. &Externo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66360
         TabIndex        =   190
         Top             =   3645
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label LaSemanas 
         Caption         =   "semanas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66840
         TabIndex        =   189
         Top             =   1680
         Visible         =   0   'False
         Width           =   750
      End
      Begin VB.Label LaDataFact 
         Caption         =   "Dt &fact"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66045
         TabIndex        =   188
         Top             =   1680
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label LaMedico 
         Caption         =   "&M�dico"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   187
         Top             =   3600
         Width           =   1095
      End
   End
   Begin VB.Frame FrameSONHO 
      Height          =   4335
      Left            =   2760
      TabIndex        =   93
      Top             =   10080
      Visible         =   0   'False
      Width           =   5535
      Begin VB.TextBox EcEpisodioSONHO 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   360
         TabIndex        =   100
         Top             =   2880
         Width           =   1500
      End
      Begin VB.OptionButton OpConsSonho 
         Caption         =   "Consulta"
         Height          =   315
         Left            =   480
         TabIndex        =   99
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton OpIntSONHO 
         Caption         =   "Internamento"
         Height          =   255
         Left            =   480
         TabIndex        =   98
         Top             =   1200
         Width           =   1335
      End
      Begin VB.OptionButton OpUrgSONHO 
         Caption         =   "Urg�ncia"
         Height          =   255
         Left            =   480
         TabIndex        =   97
         Top             =   1920
         Width           =   1335
      End
      Begin VB.OptionButton OpExternoSONHO 
         Caption         =   "Hospital Dia"
         Height          =   255
         Left            =   480
         TabIndex        =   96
         Top             =   1560
         Width           =   1335
      End
      Begin VB.CommandButton BtGetSONHO_OK 
         Caption         =   "Importar"
         Height          =   375
         Left            =   1440
         TabIndex        =   95
         Top             =   3600
         Width           =   1455
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   3000
         TabIndex        =   94
         Top             =   3600
         Width           =   1455
      End
      Begin MSComCtl2.MonthView MonthViewAnalisesSONHO 
         Height          =   2310
         Left            =   2520
         TabIndex        =   101
         Top             =   870
         Width           =   2430
         _ExtentX        =   4286
         _ExtentY        =   4075
         _Version        =   393216
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         Appearance      =   0
         ShowToday       =   0   'False
         StartOfWeek     =   175046658
         CurrentDate     =   37596
      End
      Begin VB.Label Label42 
         Caption         =   "An�lises marcadas para"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2520
         TabIndex        =   104
         Top             =   480
         Width           =   2655
      End
      Begin VB.Label Label52 
         Caption         =   "Epis�dio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   103
         Top             =   2520
         Width           =   975
      End
      Begin VB.Label Label53 
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   102
         Top             =   480
         Width           =   1335
      End
   End
   Begin VB.Label LaArquivo 
      Caption         =   "&Arquivo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3840
      TabIndex        =   243
      Top             =   14640
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.Label Label36 
      Caption         =   "EcPrColheita"
      Height          =   255
      Left            =   3240
      TabIndex        =   224
      Top             =   11160
      Width           =   975
   End
   Begin VB.Label Label57 
      AutoSize        =   -1  'True
      Caption         =   "Requisi��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   360
      TabIndex        =   214
      Top             =   120
      Width           =   945
   End
   Begin VB.Label Label54 
      Caption         =   "EcAuxTubo"
      Enabled         =   0   'False
      Height          =   255
      Left            =   120
      TabIndex        =   210
      Top             =   10680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label40 
      Caption         =   "Dt &chegada"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   202
      Top             =   10560
      Width           =   855
   End
   Begin VB.Label Label8 
      Caption         =   "EcRequAX"
      Height          =   255
      Left            =   2880
      TabIndex        =   92
      Top             =   10440
      Width           =   975
   End
   Begin VB.Label Label55 
      Caption         =   "EcPesqRapArquivo"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5760
      TabIndex        =   90
      Top             =   9960
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "EcPesqRapMedico2"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5640
      TabIndex        =   88
      Top             =   9000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label39 
      Caption         =   "EcGrupoAna"
      Enabled         =   0   'False
      Height          =   255
      Left            =   8400
      TabIndex        =   86
      Top             =   9000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label35 
      Caption         =   "EcEtqNCopias"
      Enabled         =   0   'False
      Height          =   255
      Left            =   360
      TabIndex        =   81
      Top             =   9120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label34 
      Caption         =   "EcEtqNEsp"
      Enabled         =   0   'False
      Height          =   255
      Left            =   360
      TabIndex        =   79
      Top             =   9360
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label33 
      Caption         =   "EcEtqTipo"
      Enabled         =   0   'False
      Height          =   255
      Left            =   360
      TabIndex        =   77
      Top             =   10080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "Re&q. assoc."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   3
      ToolTipText     =   "Requisi��o Associada"
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label32 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   360
      TabIndex        =   75
      Top             =   8880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label27 
      Caption         =   "EcEtqNTubos"
      Enabled         =   0   'False
      Height          =   255
      Left            =   360
      TabIndex        =   73
      Top             =   9600
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label26 
      Caption         =   "EcEtqNAdm"
      Enabled         =   0   'False
      Height          =   255
      Left            =   360
      TabIndex        =   71
      Top             =   9840
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label12 
      Caption         =   "EcTEFR"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5520
      TabIndex        =   69
      Top             =   10200
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label25 
      Caption         =   "EcAuxAna"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2880
      TabIndex        =   67
      Top             =   10200
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label24 
      Caption         =   "EcAuxProd"
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      TabIndex        =   65
      Top             =   10320
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label21 
      Caption         =   "EcCodIsencao"
      Height          =   255
      Left            =   6000
      TabIndex        =   63
      Top             =   9720
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "CodAnaTemp"
      Height          =   255
      Left            =   4080
      TabIndex        =   61
      Top             =   9240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label20 
      Caption         =   "EcPesqRapProduto"
      Enabled         =   0   'False
      Height          =   255
      Left            =   7920
      TabIndex        =   59
      Top             =   9240
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label19 
      Caption         =   "EcSeqUtente"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2040
      TabIndex        =   56
      Top             =   9240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label23 
      Caption         =   "EcPesqRapMedico"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5640
      TabIndex        =   54
      Top             =   9240
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label22 
      Caption         =   "EcPesqRapProven"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5760
      TabIndex        =   52
      Top             =   9480
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label31 
      Caption         =   "EcEstadoReq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2040
      TabIndex        =   50
      Top             =   9000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label28 
      Caption         =   "EcTaxaModeradora"
      Enabled         =   0   'False
      Height          =   255
      Left            =   3600
      TabIndex        =   48
      Top             =   9480
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label29 
      Caption         =   "EcPagEnt"
      Enabled         =   0   'False
      Height          =   255
      Left            =   4200
      TabIndex        =   47
      Top             =   9840
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label30 
      Caption         =   "EcPagUte"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2160
      TabIndex        =   46
      Top             =   9480
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "EcUrgencia"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2040
      TabIndex        =   42
      Top             =   9720
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LbAdmin 
      AutoSize        =   -1  'True
      Caption         =   "&Epis�dio"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   8400
      TabIndex        =   5
      Top             =   120
      Width           =   600
   End
End
Attribute VB_Name = "FormGesReqCons"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/03/2003
' T�cnico : Paulo Costa

' Vari�veis Globais para este Form.

Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Public CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim Formato1 As String
Dim Formato2 As String

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

Public rs As ADODB.recordset
Public MarcaLocal As Variant

'Controlo dos produtos
Public RegistosP As New Collection
Dim LastColP As Integer
Dim LastRowP As Integer
Dim ExecutaCodigoP As Boolean

'Controlo dos tubos
Public RegistosT As New Collection
Dim LastColT As Integer
Dim LastRowT As Integer
Dim ExecutaCodigoT As Boolean

'Flag que indica se a entidade em causa � ARS
Dim FlgARS As Boolean

'Flag que indica se a entidade em causa � ADSE
Dim FlgADSE As Boolean

'Controlo de analises
Public RegistosA As New Collection
Dim LastColA As Integer
Dim LastRowA As Integer
Dim ExecutaCodigoA As Boolean

'acumula o numero do P1 para quando este for superior a 7 incrementar 1 ao P1
Dim ContP1 As Integer

'Lista que vai conter os dados necess�rios � marca��o das an�lises
Private MaReq() As MarcacaoAnalises

'Comando utilizados obter a ordem correcta das an�lises
Dim CmdOrdAna As New ADODB.Command

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdAnaReq As New ADODB.Command

'Estrutura usada para calcular o valor do recibo da requisi��o
Private Recibo() As Estrutura_Recibo

'Total de elementos na lista de marca��es de recibos
Dim TotalListaRecibos As Integer

'Flag usada para controlar a coloca��o da data actual no lostfocus da EcDataPrevista
'quando se faz o limpacampos
Dim Flg_LimpaCampos As Boolean

'flag que indica se se deve preencher a data de chegada de todos os produtos na altura do Insert ou UpDate
Dim Flg_PreencherDtChega As Boolean

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

'valor do indice nas combos de Pagamento (TM,Ent,Ute) quando se executa a fun��o procurar
Dim CbTMIndex As Integer
Dim CbEntIndex As Integer
Dim CbUteIndex As Integer

Dim gRec As Long

'Flag que evita os beep dos enters nas textbox
Dim Enter As Boolean

'Constante com cor azul
Const azul = &H8000000D

Dim Flg_PergProd As Boolean
Dim Flg_DtChProd As Boolean
Dim Resp_PergProd As Boolean
Dim Resp_PergDtChProd As Boolean

Dim Flg_PergTubo As Boolean
Dim Flg_DtChTubo As Boolean
Dim Resp_PergTubo As Boolean
Dim Resp_PergDtChTubo As Boolean

Dim FuncaoListaRecibos As String

'contem a data de chegada da verificacao dos produtos para utilizacao na marcacao de analises
Dim gColocaDataChegada As String

'Flag que indica a entrada no form de pesquisa de utentes
Dim Flg_PesqUtente As Boolean

'Estruturas utilizadas para gerar as etiquetas
Private Type Tipo_Tubo
    Especial As Boolean
    Cheio As Boolean
    abrAna As String
    GrAna As String
    CodProd As String
    codTuboBar As String
    CodTubo As String
    QtMax As Double
    QtOcup As Double
    Designacao_tubo As String
    inf_complementar As String
    num_copias As Integer
End Type
Dim Tubos() As Tipo_Tubo

'Variaveis utilizadas na marca��o de an�lises GhostMember na pr�pria requisi�ao
Dim GhostPerfil As String
Dim GhostComplexa As String

Dim Flg_DadosAnteriores As Boolean

' ---------------------------------------------------

' 02

Dim ESTADO_RECIBO As String
Const REC_INDEFINIDO = "0"
Const REC_NAO_EMITIDO = "1"
Const REC_EMITIDO_NAO_IMPRESSO = "2"
Const REC_EMITIDO_IMPRESSO = "3"
Const REC_ANULADO = "4"

' Recibo activo para a requisi��o actual.
Dim RECIBO_ACTIVO As String
Dim SERIE_RECIBO_ACTIVO As String

' Indica se a estrutura do recibo j� foi gerada.
Dim flgGridRecibo As Boolean

' True : A actualizar.
Dim ActualizaEpisodio As Boolean

Dim Flg_Executa_BtPassaMarcacao As Boolean

'NELSONPSILVA Glintt-HS-18011 04.04.2018 - Acesso contextualizado quando chamado pelo form anterior (Log RGPD)
Public ExternalAccess As Boolean
'

' ---------------------------------------------------

Sub Elimina_Mareq(codAgrup As String)

    Dim i As Integer

    On Error GoTo Trata_Erro

    TAna.Nodes.Remove Trim(UCase(codAgrup))

    i = 1
    While i <= UBound(MaReq)
        If Trim(UCase(codAgrup)) = Trim(UCase(MaReq(i).cod_agrup)) Then
            Actualiza_Estrutura_MAReq i
        Else
            i = i + 1
        End If
    Wend
        
    If UBound(MaReq) = 0 Then ReDim MaReq(1)

    Exit Sub

Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "Elimina_Mareq: " & Err.Description
    End If
    
    Resume Next
End Sub

Sub Elimina_RegistoA(codAgrup As String)

    Dim i As Long

    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            RegistosA.Remove i
            FGAna.RemoveItem i
        End If
    Next i

End Sub

Function Insere_Nova_Analise(indice As Integer, codAgrup As String) As Boolean

    Dim descricao As String
    Dim Marcar() As String
    Dim reqComAnalise As String
    Dim DataReq As String

    Insere_Nova_Analise = False

    descricao = SELECT_Descr_Ana(codAgrup)
    If descricao = "" Then
        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
        EcAuxAna.Text = ""
    Else
        gColocaDataChegada = ""
        
        If (Verifica_Ana_Prod(codAgrup, Marcar)) Then
            If Verifica_Ana_Tubo(codAgrup, Marcar) Then
            End If
            
            If Trim(RegistosA(indice).codAna) <> "" Then
                Elimina_Mareq RegistosA(indice).codAna
                RegistosA(indice).codAna = ""
            End If
        
            ' --------------------------------------------------------------------------------
            
            If (gProcAnPendentesAoRegistar = mediSim) Then
            
                ' Indica a requisi��o mais recente com esta an�lise pendente, para este utente.
                            
                reqComAnalise = ""
                reqComAnalise = REQUISICAO_Get_Com_Analise_Pendente_Consultas(UCase(EcAuxAna.Text), _
                                                                              EcSeqUtente.Text, _
                                                                              DataReq)
        
                If (Len(reqComAnalise)) Then
                    MsgBox "A an�lise " & UCase(EcAuxAna.Text) & " est� pendente na requisi��o " & reqComAnalise & "        " & vbCrLf & _
                           "datada de " & DataReq & ".", vbInformation, " Pesquisa de An�lises Pendentes "
                End If
            
            End If
            ' --------------------------------------------------------------------------------
            
            Select Case Mid(Trim(UCase(codAgrup)), 1, 1)
                Case "P"
                    SELECT_Dados_Perfil Marcar, indice, Trim(UCase(codAgrup))
                Case "C"
                    SELECT_Dados_Complexa Marcar, indice, Trim(UCase(codAgrup))
                Case "S"
                    If SELECT_Dados_Simples(Marcar, indice, Trim(UCase(codAgrup))) = False Then
                        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
                        EcAuxAna.Text = ""
                        Exit Function
                    End If
                Case Else
            End Select
    
            Insere_Nova_Analise = True
        End If
    End If
    
End Function

Sub InsereAutoProd(CodProd As String, DtChega As String)

    Dim LastLastRP As Integer
    Dim i As Integer
    
    EcAuxProd.Text = CodProd
    
    LastLastRP = LastRowP
    LastRowP = FgProd.rows - 1
    
    'Verificar se o produto j� est� na lista
    For i = 1 To LastRowP
        If Trim(FgProd.TextMatrix(i, 0)) = Trim(CodProd) Then
            Exit Sub
        End If
    Next
    
    If SELECT_Descr_Prod = True Then
        ExecutaCodigoP = False
        
        If Trim(DtChega) <> "" Then
            FgProd.TextMatrix(LastRowP, 4) = Trim(DtChega)
            RegistosP(LastRowP).DtPrev = Trim(DtChega)
            FgProd.TextMatrix(LastRowP, 5) = Trim(DtChega)
            RegistosP(LastRowP).DtChega = Trim(DtChega)
            gColocaDataChegada = Trim(DtChega)
            If (EcEstadoReq) = gEstadoReqSemAnalises Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto Then
                CbEstadoReqAux.ListIndex = 2
                EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
                CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
            End If
        Else
            FgProd.TextMatrix(LastRowP, 4) = EcDataPrevista
            RegistosP(LastRowP).DtPrev = EcDataPrevista
        End If

        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
        FgProd.TextMatrix(LastRowP, 6) = RegistosP(LastRowP).EstadoProd
                        
        FgProd.TextMatrix(LastRowP, 0) = UCase(CodProd)
        RegistosP(LastRowP).CodProd = UCase(CodProd)
        
        'Cria linha vazia
        FgProd.AddItem ""
        CriaNovaClasse RegistosP, FgProd.rows - 1, "PROD"
        
        ExecutaCodigoP = True
        
        FgProd.row = FgProd.rows - 1
        FgProd.Col = 0
        LastColP = FgProd.Col
        LastRowP = FgProd.row
    Else
        LastRowP = LastLastRP
    End If
    
    EcAuxProd.Text = ""
End Sub

Sub InsereAutoTubo(CodTubo As String, DtChega As String, HrChega As String)

    Dim LastLastRP As Integer
    Dim i As Integer
    
    EcAuxTubo.Text = CodTubo
    
    LastLastRP = LastRowP
    LastRowT = FGTubos.rows - 1
    
    'Verificar se o tubo j� est� na lista
    For i = 1 To LastRowT
        If Trim(FGTubos.TextMatrix(i, 0)) = Trim(CodTubo) Then
            Exit Sub
        End If
    Next
    
    If SELECT_Descr_Tubo = True Then
        ExecutaCodigoT = False
        
        If Trim(DtChega) <> "" Then
            FGTubos.TextMatrix(LastRowT, 2) = Trim(DtChega)
            RegistosT(LastRowT).DtPrev = Trim(DtChega)
            FGTubos.TextMatrix(LastRowT, 3) = Trim(DtChega)
            RegistosT(LastRowT).DtChega = Trim(DtChega)
            FGTubos.TextMatrix(LastRowT, 4) = Trim(HrChega)
            RegistosT(LastRowT).HrChega = Trim(HrChega)
            gColocaDataChegada = Trim(DtChega)
            If (EcEstadoReq) = gEstadoReqSemAnalises Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto Then
                CbEstadoReqAux.ListIndex = 2
                EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
                CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
            End If
        Else
            FGTubos.TextMatrix(LastRowT, 2) = EcDataPrevista
            RegistosT(LastRowT).DtPrev = EcDataPrevista
        End If

        RegistosT(LastRowT).EstadoTubo = Coloca_Estado(RegistosT(LastRowT).DtPrev, RegistosT(LastRowT).DtChega)
        FGTubos.TextMatrix(LastRowT, 5) = RegistosT(LastRowT).EstadoTubo
                        
        FGTubos.TextMatrix(LastRowT, 0) = UCase(CodTubo)
        RegistosT(LastRowT).CodTubo = UCase(CodTubo)
        
        'Cria linha vazia
        FGTubos.AddItem ""
        CriaNovaClasse RegistosT, FGTubos.rows - 1, "TUBO"
        
        ExecutaCodigoT = True
        
        FGTubos.row = FGTubos.rows - 1
        FGTubos.Col = 0
        LastColT = FGTubos.Col
        LastRowT = FGTubos.row
    Else
        LastRowT = LastLastRP
    End If
    
    EcAuxTubo.Text = ""
End Sub


Sub InsereGhostMember(codAnaS As String)

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim Marcar(1) As String
    Dim indice  As Long
    Dim i As Long
    Dim OrdAna As Integer
    Dim CodPerfil As String
    Dim DescrPerfil As String
    Dim OrdPerf As Integer
    Dim CodAnaC As String
    Dim DescrAnaC As String

codAnaS = Trim(codAnaS)

'For�ar a marca��o
Marcar(1) = codAnaS & ";"

indice = -1
For i = 1 To UBound(MaReq)
    If Trim(GhostPerfil) = Trim(MaReq(i).Cod_Perfil) And _
       Trim(GhostComplexa) = Trim(MaReq(i).cod_ana_c) Then
       indice = i
       Exit For
    End If
Next i

If indice = -1 Then
    BG_Mensagem mediMsgBox, "Erro a inserir an�lise!", vbCritical + vbOKOnly, App.ProductName
    Exit Sub
End If

gColocaDataChegada = MaReq(indice).dt_chega

'Dados da Complexa
sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseClient
    .CursorType = adOpenStatic
    .Open , gConexao
End With

If RsCompl.RecordCount > 0 Then
    descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
Else
    BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
End If
RsCompl.Close
Set RsCompl = Nothing

OrdAna = MaReq(indice).Ord_Ana
CodPerfil = MaReq(indice).Cod_Perfil
DescrPerfil = MaReq(indice).Descr_Perfil
OrdPerf = MaReq(indice).Ord_Ana_Perf
CodAnaC = MaReq(indice).cod_ana_c
DescrAnaC = MaReq(indice).Descr_Ana_C

'Dados do Membro da complexa (CodAnaS)
sql = "SELECT ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa) & _
        " AND cod_membro = " & BL_TrataStringParaBD(codAnaS)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseClient
    .CursorType = adOpenStatic
    .Open , gConexao
End With

If Not RsCompl.EOF Then
    If GhostPerfil <> "0" Then
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, CodPerfil, DescrPerfil, OrdPerf, CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    Else
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, , , , CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    End If
End If

RsCompl.Close
Set RsCompl = Nothing

End Sub

Function Pesquisa_RegistosA(codAgrup As String) As Long

    Dim i As Long
    Dim lRet As Long
    
    lRet = -1
    
    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            lRet = i
            Exit For
        End If
    Next i
    
    Pesquisa_RegistosA = lRet
    
End Function

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes
    
    Max = -1
    ReDim FOPropertiesTemp(0)
    
    DefTipoCampos
    
    EcUtente.Text = ""
    DoEvents
    
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
        
    gF_REQCONS = 1
    Flg_PesqUtente = False
    
    estado = 1
    BG_StackJanelas_Push Me
        
    SSTGestReq.Tab = 0
    DoEvents
    Flg_DadosAnteriores = False
        
    ' _02
    ESTADO_RECIBO = 0
    RECIBO_ACTIVO = ""
        
    ' Retira o tab de recibos.
    If (StrComp(gSKIN, cANASIBAS) = 0) Or _
       (gSISTEMA_FACTURACAO = "") Then
       
        SSTGestReq.TabVisible(4) = False
        SSTGestReq.TabEnabled(4) = False
    End If
               
    ' Bot�o de importa��o do SONHO.
    If (Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "BT_IMP_SONHO")) = "1") Then
        BtGetSONHO.Enabled = True
        BtGetSONHO.Visible = True
        DoEvents
    End If
               
End Sub

Sub Inicializacoes()
    
    Me.caption = " Marca��o de Colheitas"
    Me.left = 500
    Me.top = 20
    Me.Width = 11125
    Me.Height = 8085 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_requis_consultas"
    Set CampoDeFocus = EcObsProveniencia
    
    NumCampos = 43
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "n_req_assoc"
    CamposBD(2) = "seq_utente"
    CamposBD(3) = "n_epis"
    CamposBD(4) = "dt_previ"
    CamposBD(5) = "dt_chega"
    CamposBD(6) = "dt_imp"
    CamposBD(7) = "t_sit"
    CamposBD(8) = "t_urg"
    CamposBD(9) = "cod_efr"
    CamposBD(10) = "n_benef"
    CamposBD(11) = "cod_proven"
    CamposBD(12) = "cod_med"
    CamposBD(13) = "obs_proven"
    CamposBD(14) = "pag_tax"
    CamposBD(15) = "pag_ent"
    CamposBD(16) = "pag_ute"
    CamposBD(17) = "obs_req"
    CamposBD(18) = "hr_imp"
    CamposBD(19) = "user_imp"
    CamposBD(20) = "dt_imp2"
    CamposBD(21) = "hr_imp2"
    CamposBD(22) = "user_imp2"
    CamposBD(23) = "estado_req"
    CamposBD(24) = "user_cri"
    CamposBD(25) = "hr_cri"
    CamposBD(26) = "dt_cri"
    CamposBD(27) = "user_act"
    CamposBD(28) = "hr_act"
    CamposBD(29) = "dt_act"
    CamposBD(30) = "cod_isencao"
    CamposBD(31) = "gr_ana"
    CamposBD(32) = "req_aux"
    CamposBD(33) = "ficha"
    CamposBD(34) = "arquivo"
    CamposBD(35) = "cod_med2"
    CamposBD(36) = "dt_fact"
    CamposBD(37) = "n_prescricao"
    CamposBD(38) = "cod_destino"
    CamposBD(39) = "cod_valencia"
    CamposBD(40) = "inf_complementar"
    CamposBD(41) = "FLG_invisivel"
    CamposBD(42) = "dt_marcacao"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcReqAssociada
    Set CamposEc(2) = EcSeqUtente
    Set CamposEc(3) = EcEpisodio
    Set CamposEc(4) = EcDataPrevista
    Set CamposEc(5) = EcDataChegada
    Set CamposEc(6) = EcDataImpressao
    Set CamposEc(7) = CbSituacao
    Set CamposEc(8) = CbUrgencia
    Set CamposEc(9) = EcCodEFR
    Set CamposEc(10) = EcNumBenef
    Set CamposEc(11) = EcCodProveniencia
    Set CamposEc(12) = EcCodMedico
    Set CamposEc(13) = EcObsProveniencia
    Set CamposEc(14) = EcTaxaModeradora
    Set CamposEc(15) = EcPagEnt
    Set CamposEc(16) = EcPagUte
    Set CamposEc(17) = EcObsReq
    Set CamposEc(18) = EcHoraImpressao
    Set CamposEc(19) = EcUtilizadorImpressao
    Set CamposEc(20) = EcDataImpressao2
    Set CamposEc(21) = EcHoraImpressao2
    Set CamposEc(22) = EcUtilizadorImpressao2
    Set CamposEc(23) = EcEstadoReq
    Set CamposEc(24) = EcUtilizadorCriacao
    Set CamposEc(25) = EcHoraCriacao
    Set CamposEc(26) = EcDataCriacao
    Set CamposEc(27) = EcUtilizadorAlteracao
    Set CamposEc(28) = EcHoraAlteracao
    Set CamposEc(29) = EcDataAlteracao
    Set CamposEc(30) = EcCodIsencao
    Set CamposEc(31) = EcGrupoAna
    Set CamposEc(32) = EcReqAux
    Set CamposEc(33) = EcFicha
    Set CamposEc(34) = EcCodArquivo
    Set CamposEc(35) = EcCodMedico2
    Set CamposEc(36) = EcDataFact
    Set CamposEc(37) = EcPrescricao
    Set CamposEc(38) = CbDestino
    Set CamposEc(39) = EcCodValencia
    Set CamposEc(40) = EcInfComplementar
    Set CamposEc(41) = CkFlgInvisivel
    Set CamposEc(42) = EcDtMarcacao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(2) = "Utente"
    TextoCamposObrigatorios(4) = "Data activa��o da requisi��o"
    TextoCamposObrigatorios(7) = "Tipo de situa��o"
    TextoCamposObrigatorios(8) = "Tipo de urg�ncia"
    TextoCamposObrigatorios(9) = "Entidade financeira"
    
    'TextoCamposObrigatorios(11) = "Proveni�ncia"
    
    If gMultiReport = 1 Then
        TextoCamposObrigatorios(32) = "N�mero da An�lise"
    End If

    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_req"
    Set ChaveEc = EcNumReq
                
    'inicializa��es das estruturas de mem�ria (an�lises e recibos)
    Inicializa_Estrutura_Analises
    
    'O TAB de an�lises est� desactivado at� se introduzir uma entidade financeira
    ' e o de produtos at� introduzir uma data prevista
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabVisible(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabVisible(4) = False
    SSTGestReq.TabEnabled(5) = False
           
    FlgARS = False
    FlgADSE = False
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = False
    
    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    
    'Colocar o bot�o para preencher os dados da requisi��o com os dados da requisi��o anterior inactivo ou activo
    If gRequisicaoActiva = 0 Then
        BtDadosAnteriores.Enabled = False
        LaCopiarAnterior.Enabled = False
    Else
        BtDadosAnteriores.Enabled = True
        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
        LaCopiarAnterior.Enabled = True
    End If
    
    'Inicializa comando para ir buscar as an�lises da requisi��o e os seus dados
    Set CmdAnaReq.ActiveConnection = gConexao
    CmdAnaReq.CommandType = adCmdText
    
    CmdAnaReq.CommandText = "select  m.n_folha_trab, m.n_req, cr.n_req as req_ARS, cr.p1, m.dt_chega, m.cod_agrup, m.ord_ana_perf, m.ord_ana_compl, m.ord_ana, "
    CmdAnaReq.CommandText = CmdAnaReq.CommandText & " m.cod_perfil, descr_perfis, m.cod_ana_c, descr_ana_c, m.cod_ana_s, descr_ana_s,"
    CmdAnaReq.CommandText = CmdAnaReq.CommandText & " m.flg_apar_trans, '-1' as flg_estado, ord_marca from sl_marcacoes_consultas m "
    CmdAnaReq.CommandText = CmdAnaReq.CommandText & " LEFT OUTER JOIN sl_perfis p ON   m.cod_perfil = p.cod_perfis  LEFT OUTER JOIN sl_ana_c c "
    CmdAnaReq.CommandText = CmdAnaReq.CommandText & " ON m.cod_ana_c = c.cod_ana_c  LEFT OUTER JOIN  sl_ana_s s ON m.cod_ana_s = s.cod_ana_S "
    CmdAnaReq.CommandText = CmdAnaReq.CommandText & " LEFT OUTER JOIN sl_credenciais cr  ON m.n_Req = cr.n_req_orig AND m.cod_agrup = cr.cod_ana WHERE m.n_req =  ? "
    CmdAnaReq.CommandText = CmdAnaReq.CommandText & " order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    CmdAnaReq.Prepared = True
    CmdAnaReq.Parameters.Append CmdAnaReq.CreateParameter("N_REQ_M", adInteger, adParamInput, 7)
'    CmdAnaReq.Parameters.Append CmdAnaReq.CreateParameter("N_REQ_R", adDouble, adParamInput, 7)
'    CmdAnaReq.Parameters.Append CmdAnaReq.CreateParameter("N_REQ_H", adDouble, adParamInput, 7)
        
        
    'Inicializa comando para ir buscar a ordem e o grupo das an�lises
    Set CmdOrdAna.ActiveConnection = gConexao
    CmdOrdAna.CommandType = adCmdText
    CmdOrdAna.CommandText = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis = ? "
    CmdOrdAna.Prepared = True

    CmdOrdAna.Parameters.Append CmdAnaReq.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    CmdOrdAna.Parameters.Append CmdAnaReq.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 10)
    CmdOrdAna.Parameters.Append CmdAnaReq.CreateParameter("COD_PERFIS", adVarChar, adParamInput, 10)
        
    'Inicializa��o das variaveis que guardam os indices nas combos de pagamentos
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1
    
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    EcPrinterResumo = BL_SelImpressora("ResumoRequisicoes.rpt")
    
'    gEpisodioActivo = ""
'    gSituacaoActiva = mediComboValorNull
    
    Flg_Executa_BtPassaMarcacao = False
    
End Sub

Function FuncaoProcurar_PreencheProd(NumReq As Long)
    On Error GoTo TrataErro
    
    ' Selecciona todos os produtos da requisi��o e preenche a lista

    Dim i As Long
    Dim rsDescr As ADODB.recordset
    Dim RsProd As ADODB.recordset
    Dim CmdDescrP As New ADODB.Command
    Dim PmtCodProd As ADODB.Parameter
    Dim CmdDescrE As New ADODB.Command
    Dim PmtCodEsp As ADODB.Parameter
      
    Set RsProd = New ADODB.recordset
    With RsProd
        'RGONCALVES 15.12.2014 HDES-3746
        '.Source = "SELECT cod_prod, cod_especif,dt_previ, dt_chega, volume, obs_especif FROM sl_req_prod " & _
                   "WHERE n_req =" & NumReq
        .Source = "SELECT cod_prod, cod_especif,dt_previ, dt_chega FROM sl_req_prod_consultas " & _
                   "WHERE n_req =" & NumReq
        '
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With

    'Inicializa o comando ADO para selec��o da descri��o do produto
    Set CmdDescrP.ActiveConnection = gConexao
    CmdDescrP.CommandType = adCmdText
    CmdDescrP.CommandText = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                            "cod_produto =?"
    CmdDescrP.Prepared = True
    Set PmtCodProd = CmdDescrE.CreateParameter("COD_PRODUTO", adVarChar, adParamInput, 5)
    CmdDescrP.Parameters.Append PmtCodProd
    
    'Inicializa o comando ADO para selec��o da descri��o da especifica��o
     Set CmdDescrE.ActiveConnection = gConexao
     CmdDescrE.CommandType = adCmdText
     CmdDescrE.CommandText = "SELECT descr_especif FROM sl_especif WHERE " & _
                              "cod_especif =?"
     CmdDescrE.Prepared = True
     Set PmtCodEsp = CmdDescrE.CreateParameter("COD_ESPECIF", adVarChar, adParamInput, 5)
     CmdDescrE.Parameters.Append PmtCodEsp
     
     i = 1
     LimpaFgProd
     While Not RsProd.EOF
        RegistosP(i).CodProd = RsProd!cod_prod
        'RGONCALVES 15.12.2014 HDES-3746
        'RegistosP(i).volume = BL_HandleNull(RsProd!volume, "")
        '
        CmdDescrP.Parameters(0).value = Trim(UCase(RsProd!cod_prod))
        Set rsDescr = CmdDescrP.Execute
        RegistosP(i).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(i).EspecifObrig = rsDescr!especif_obrig
        'RGONCALVES 15.12.2014 HDES-3746
        'RegistosP(i).ObsEspecif = BL_HandleNull(RsProd!obs_especif, "")
        '
        rsDescr.Close

        RegistosP(i).CodEspecif = BL_HandleNull(RsProd!cod_Especif, "")
        'descri��o da especifica��o
        If Trim(RegistosP(i).CodEspecif) <> "" Then
            CmdDescrE.Parameters(0) = UCase(RsProd!cod_Especif)
            Set rsDescr = CmdDescrE.Execute
            RegistosP(i).DescrEspecif = BL_HandleNull(rsDescr!descr_especif, "")
            rsDescr.Close
        End If
        RegistosP(i).DtPrev = BL_HandleNull(RsProd!dt_previ, "")
        RegistosP(i).DtChega = BL_HandleNull(RsProd!dt_chega, "")
        
        RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
        
        FgProd.TextMatrix(i, 0) = RegistosP(i).CodProd
        FgProd.TextMatrix(i, 1) = RegistosP(i).DescrProd
        FgProd.TextMatrix(i, 2) = RegistosP(i).CodEspecif
        FgProd.TextMatrix(i, 3) = RegistosP(i).DescrEspecif
        'RGONCALVES 15.12.2014 HDES-3746
'        FgProd.TextMatrix(i, 4) = RegistosP(i).volume
'        FgProd.TextMatrix(i, 5) = RegistosP(i).DtPrev
'        FgProd.TextMatrix(i, 6) = RegistosP(i).DtChega
'        FgProd.TextMatrix(i, 7) = RegistosP(i).EstadoProd
        FgProd.TextMatrix(i, 4) = RegistosP(i).DtPrev
        FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
        FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
        '
        
        'Se h� produtos que j� chegaram ent�o estado n�o deve estar "� espera de prod."
        If ((EcEstadoReq) = gEstadoReqSemAnalises Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto) And Trim(FgProd.TextMatrix(i, 5)) <> "" Then
            CbEstadoReqAux.ListIndex = 2
            EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
            CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
        End If
        
        RsProd.MoveNext
        i = i + 1
        CriaNovaClasse RegistosP, i, "PROD"
        FgProd.AddItem "", i
        FgProd.row = i
     Wend
            
     Set CmdDescrE = Nothing
     Set CmdDescrP = Nothing
     Set rsDescr = Nothing
     Set RsProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar_PreencheProd: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar_PreencheProd"
    Exit Function
    Resume Next
End Function
'Function FuncaoProcurar_PreencheProd(NumReq As Long)
'
'    ' Selecciona todos os produtos da requisi��o e preenche a lista
'
'    Dim i As Long
'    Dim rsDescr As ADODB.recordset
'    Dim RsProd As ADODB.recordset
'    Dim CmdDescrP As New ADODB.Command
'    Dim PmtCodProd As ADODB.Parameter
'    Dim CmdDescrE As New ADODB.Command
'    Dim PmtCodEsp As ADODB.Parameter
'
'    Set RsProd = New ADODB.recordset
'    With RsProd
'        .Source = "SELECT cod_prod, cod_especif,dt_previ, dt_chega FROM sl_req_prod_consultas " & _
'                   "WHERE n_req =" & BL_TrataStringParaBD(CStr(NumReq))
'
'        .CursorLocation = adUseClient
'        .CursorType = adOpenStatic
'        .LockType = adLockReadOnly
'        .ActiveConnection = gConexao
'        .Open
'    End With
'
'    'Inicializa o comando ADO para selec��o da descri��o do produto
'    Set CmdDescrP.ActiveConnection = gConexao
'    CmdDescrP.CommandType = adCmdText
'    CmdDescrP.CommandText = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
'                            "cod_produto =?"
'    CmdDescrP.Prepared = True
'    Set PmtCodProd = CmdDescrE.CreateParameter("COD_PRODUTO", adVarChar, adParamInput, 5)
'    CmdDescrP.Parameters.Append PmtCodProd
'
'    'Inicializa o comando ADO para selec��o da descri��o da especifica��o
'     Set CmdDescrE.ActiveConnection = gConexao
'     CmdDescrE.CommandType = adCmdText
'     CmdDescrE.CommandText = "SELECT descr_especif FROM sl_especif WHERE " & _
'                              "cod_especif =?"
'     CmdDescrE.Prepared = True
'     Set PmtCodEsp = CmdDescrE.CreateParameter("COD_ESPECIF", adVarChar, adParamInput, 5)
'     CmdDescrE.Parameters.Append PmtCodEsp
'
'     i = 1
'     LimpaFgProd
'     While Not RsProd.EOF
'        RegistosP(i).CodProd = RsProd!cod_prod
'        CmdDescrP.Parameters(0).Value = Trim(UCase(RsProd!cod_prod))
'        Set rsDescr = CmdDescrP.Execute
'        RegistosP(i).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
'        RegistosP(i).EspecifObrig = rsDescr!especif_obrig
'        rsDescr.Close
'
'        RegistosP(i).CodEspecif = BL_HandleNull(RsProd!cod_Especif, "")
'        'descri��o da especifica��o
'        If Trim(RegistosP(i).CodEspecif) <> "" Then
'            CmdDescrE.Parameters(0) = UCase(RsProd!cod_Especif)
'            Set rsDescr = CmdDescrE.Execute
'            RegistosP(i).DescrEspecif = BL_HandleNull(rsDescr!descr_especif, "")
'            rsDescr.Close
'        End If
'        RegistosP(i).DtPrev = BL_HandleNull(RsProd!dt_previ, "")
'        RegistosP(i).DtChega = BL_HandleNull(RsProd!dt_chega, "")
'
'        RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
'
'        FgProd.TextMatrix(i, 0) = RegistosP(i).CodProd
'        FgProd.TextMatrix(i, 1) = RegistosP(i).DescrProd
'        FgProd.TextMatrix(i, 2) = RegistosP(i).CodEspecif
'        FgProd.TextMatrix(i, 3) = RegistosP(i).DescrEspecif
'        FgProd.TextMatrix(i, 4) = RegistosP(i).DtPrev
'        FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
'        FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
'
'        'Se h� produtos que j� chegaram ent�o estado n�o deve estar "� espera de prod."
'        If ((EcEstadoReq) = gEstadoReqSemAnalises Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto) And Trim(FgProd.TextMatrix(i, 5)) <> "" Then
'            CbEstadoReqAux.ListIndex = 2
'            EcEstadoReq.text = Mid(CbEstadoReqAux.text, 1, 1)
'            CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
'        End If
'
'        RsProd.MoveNext
'        i = i + 1
'        CriaNovaClasse RegistosP, i, "PROD"
'        FgProd.AddItem "", i
'        FgProd.row = i
'     Wend
'
'     Set CmdDescrE = Nothing
'     Set CmdDescrP = Nothing
'     Set rsDescr = Nothing
'     Set RsProd = Nothing
'
'End Function
'
Function FuncaoProcurar_PreencheTubo(NumReq As Long)
    
    ' Selecciona todos os tubos da requisi��o e preenche a lista

    Dim i As Long
    Dim rsDescr As ADODB.recordset
    Dim rsTubo As ADODB.recordset
    Dim CmdDescrT As New ADODB.Command
    Dim sSql As String
    
    Set rsTubo = New ADODB.recordset
    With rsTubo
        .Source = "SELECT cod_tubo, dt_previ, dt_chega, hr_chega FROM sl_req_tubo_consultas " & _
                   "WHERE n_req =" & NumReq
                   
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        .Open
    End With

     
     i = 1
     LimpaFgTubos
     While Not rsTubo.EOF
        RegistosT(i).CodTubo = rsTubo!cod_tubo
        sSql = "SELECT descr_tubo FROM sl_tubo WHERE cod_tubo =" & BL_TrataStringParaBD(Trim(UCase(rsTubo!cod_tubo)))
        Set rsDescr = New ADODB.recordset
        rsDescr.CursorLocation = adUseClient
        rsDescr.CursorType = adOpenStatic
        rsDescr.LockType = adLockReadOnly
        rsDescr.ActiveConnection = gConexao
        rsDescr.Source = sSql
        rsDescr.Open
        
        RegistosT(i).descrTubo = BL_HandleNull(rsDescr!descR_tubo, "")
        rsDescr.Close
        Set rsDescr = Nothing
        
        RegistosT(i).DtPrev = BL_HandleNull(rsTubo!dt_previ, "")
        RegistosT(i).DtChega = BL_HandleNull(rsTubo!dt_chega, "")
        RegistosT(i).HrChega = BL_HandleNull(rsTubo!hr_chega, "")
        
        RegistosT(i).EstadoTubo = Coloca_Estado(RegistosT(i).DtPrev, RegistosT(i).DtChega)
        
        FGTubos.TextMatrix(i, 0) = RegistosT(i).CodTubo
        FGTubos.TextMatrix(i, 1) = RegistosT(i).descrTubo
        FGTubos.TextMatrix(i, 2) = RegistosT(i).DtPrev
        FGTubos.TextMatrix(i, 3) = RegistosT(i).DtChega
        FGTubos.TextMatrix(i, 4) = RegistosT(i).HrChega
        FGTubos.TextMatrix(i, 5) = RegistosT(i).EstadoTubo
        
        'Se h� produtos que j� chegaram ent�o estado n�o deve estar "� espera de prod."
        If ((EcEstadoReq) = gEstadoReqSemAnalises Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto) And Trim(FGTubos.TextMatrix(i, 3)) <> "" Then
            CbEstadoReqAux.ListIndex = 2
            EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
            CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
        End If
        
        rsTubo.MoveNext
        i = i + 1
        CriaNovaClasse RegistosT, i, "TUBO"
        FGTubos.AddItem "", i
        FGTubos.row = i
     Wend
            
     Set CmdDescrT = Nothing
     Set rsDescr = Nothing
     Set rsTubo = Nothing

End Function


Sub FuncaoProcurar_PreencheAna(NumReq As Long)

    Dim i As Long
    Dim k As Long
    Dim MudaIcon As Integer
    Dim LastIntermedio As String
    Dim LastPai As String
    Dim Pai As String
    Dim Ja_Existe As Boolean
    Dim rsAna As ADODB.recordset
    Dim EstadoFinal As Integer
    Dim EArs As Boolean
    Dim ImgPai As Integer
    Dim ImgPaiSel As Integer
    On Error GoTo TrataErro
    CmdAnaReq.Parameters.item(0).value = NumReq
'    CmdAnaReq.Parameters.Item(1).Value = NumReq
'    CmdAnaReq.Parameters.Item(2).Value = NumReq
    
    Set rsAna = CmdAnaReq.Execute

i = 1
LimpaFGAna
ExecutaCodigoA = False
ContP1 = 0
FGAna.Cols = 2
FGAna.Col = 0
FGAna.ColWidth(1) = 6000
LastIntermedio = ""
LastPai = ""
Pai = ""
MudaIcon = 0
ImgPai = 5
ImgPaiSel = 6

EArs = EFR_Verifica_ARS(EcCodEFR)

While Not rsAna.EOF
        'Debug.Print RsAna!Cod_Ana_C & "-" & RsAna!cod_ana_s
        Preenche_MAReq BL_HandleNull(rsAna!Cod_Perfil, ""), _
                        BL_HandleNull(rsAna!descr_perfis, ""), _
                        BL_HandleNull(rsAna!cod_ana_c, ""), _
                        BL_HandleNull(rsAna!Descr_Ana_C, ""), _
                        BL_HandleNull(rsAna!cod_ana_s, ""), _
                        BL_HandleNull(rsAna!descr_ana_s, ""), _
                        BL_HandleNull(rsAna!Ord_Ana, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Compl, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Perf, 0), _
                        BL_HandleNull(rsAna!cod_agrup, ""), _
                        BL_HandleNull(rsAna!N_Folha_Trab, 0), _
                        BL_HandleNull(rsAna!dt_chega, ""), _
                        BL_HandleNull(rsAna!flg_apar_trans, "0"), _
                        BL_HandleNull(rsAna!flg_estado, "-1"), _
                        BL_HandleNull(rsAna!Ord_Marca, "0")
            
        Ja_Existe = False
        For k = 1 To RegistosA.Count
            If Trim(RegistosA(k).codAna) = Trim(MaReq(i).cod_agrup) Then
                Ja_Existe = True
                Exit For
            End If
        Next k

        If EArs And FGAna.Cols = 2 Then
            FGAna.ColWidth(1) = 4500
            FGAna.Cols = 4
            FGAna.ColWidth(2) = 1200
            FGAna.Col = 2
            FGAna.TextMatrix(0, 2) = "Req. ARS"
            FGAna.ColWidth(3) = 400
            FGAna.Col = 3
            FGAna.TextMatrix(0, 3) = "P1"
            FGAna.Col = 0
        End If

        If Flg_DadosAnteriores = True Then
            'For�a o estado da an�lise para sem resultado quando se copia uma requisi��o
            MaReq(i).flg_estado = "-1"
        End If
        
        If MaReq(i).flg_estado = "-1" Then
            MudaIcon = 0
        Else
            MudaIcon = 6
        End If

        If Not Ja_Existe Then
            RegistosA(FGAna.rows - 1).codAna = MaReq(i).cod_agrup
            If Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).Cod_Perfil) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Perfil
                ImgPai = 1 + MudaIcon
                ImgPaiSel = 2 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_c) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Ana_C
                ImgPai = 3 + MudaIcon
                ImgPaiSel = 4 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_s) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).descr_ana_s
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            Else
                RegistosA(FGAna.rows - 1).descrAna = ""
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            End If
            
            RegistosA(FGAna.rows - 1).NReqARS = BL_HandleNull(rsAna!req_ARS, "A gerar")
            If BL_HandleNull(rsAna!p1, "-1") <> "-1" Then
                ContP1 = BL_HandleNull(rsAna!p1, "-1")
            Else
                ContP1 = ContP1 + 1
            End If
            RegistosA(FGAna.rows - 1).p1 = ContP1
            FGAna.TextMatrix(FGAna.rows - 1, 0) = RegistosA(FGAna.rows - 1).codAna
            FGAna.TextMatrix(FGAna.rows - 1, 1) = RegistosA(FGAna.rows - 1).descrAna
            RegistosA(FGAna.rows - 1).DtChega = BL_HandleNull(rsAna!dt_chega, "")
            
            If MaReq(i).flg_estado <> "-1" And Trim(FGAna.TextMatrix(FGAna.rows - 1, 0)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbBlack
            End If
            
            If FGAna.Cols = 4 Then
                FGAna.TextMatrix(FGAna.rows - 1, 2) = RegistosA(FGAna.rows - 1).NReqARS
                FGAna.TextMatrix(FGAna.rows - 1, 3) = RegistosA(FGAna.rows - 1).p1
            End If
            
            CriaNovaClasse RegistosA, 0, "ANA"
            FGAna.AddItem ""
            FGAna.row = FGAna.rows - 1
        
        End If
        
        Pai = Trim(MaReq(i).cod_agrup)
        
        If Pai <> LastPai Then
            'Pai = codigo de agupamento (seja Perfil, Complexa ou Simples)
            BL_AcrescentaNodoPai TAna, IIf((RegistosA(FGAna.rows - 2).descrAna = ""), MaReq(i).cod_agrup, RegistosA(FGAna.rows - 2).descrAna), MaReq(i).cod_agrup, ImgPai, ImgPaiSel
            LastPai = Pai
        End If
        
        
        If Trim(MaReq(i).cod_ana_c) <> "0" And Trim(MaReq(i).cod_ana_c) <> "" And Trim(Pai) <> Trim(MaReq(i).cod_ana_c) Then
            'Se existir complexa e for diferente do Pai insere-a como intermedia entre o Pai e a simples
            If LastIntermedio = MaReq(i).cod_ana_c Then
                'j� foi inserida uma vez -> insere-se directamente a simples
                If MaReq(i).cod_ana_s = "S99999" Then
                    BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                Else
                    BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                End If
            Else
                '� a primeira vez -> insere a complexa e a simples por debaixo da mesma
                LastIntermedio = MaReq(i).cod_ana_c
                BL_AcrescentaNodoFilho TAna, MaReq(i).Descr_Ana_C, Pai, Pai & MaReq(i).cod_ana_c, 3 + MudaIcon, 4 + MudaIcon
                
                'inserir a simples
                If MaReq(i).cod_ana_s = "S99999" Then
                    BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                Else
                    BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                End If
            End If
        Else
            'N�o existe complexa ou � o pai
            If Trim(Pai) <> Trim(MaReq(i).cod_ana_s) Then
                'Se o pai nao for a propria simples insere-a de baixo do pai
                If MaReq(i).cod_ana_s = "S99999" Then
                    BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai, Pai & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                Else
                    BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai, Pai & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                End If
                
            End If
        End If
        
        If MudaIcon = 6 And TAna.Nodes.item(MaReq(i).cod_agrup).Image < 6 Then
            TAna.Nodes.item(MaReq(i).cod_agrup).Image = TAna.Nodes.item(MaReq(i).cod_agrup).Image + MudaIcon
            TAna.Nodes.item(MaReq(i).cod_agrup).SelectedImage = TAna.Nodes.item(MaReq(i).cod_agrup).SelectedImage + MudaIcon
        End If
        
        i = i + 1
        rsAna.MoveNext
        
        'Se encontrou analises o estado n�o deve estar "Sem Analises"
        If Trim(EcEstadoReq) = "" Then
            CbEstadoReqAux.ListIndex = 1
            EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
            CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
        End If
Wend

For i = 1 To FGAna.rows - 2
    EstadoFinal = -1
    For k = 1 To UBound(MaReq)
        If Trim(RegistosA(i).codAna) = Trim(MaReq(k).cod_agrup) Then
            If MaReq(k).flg_estado <> "-1" Then
                EstadoFinal = 0
                Exit For
            End If
        End If
    Next k
    RegistosA(i).estado = EstadoFinal
Next i
RegistosA(FGAna.rows - 1).estado = "-1"

rsAna.Close
Set rsAna = Nothing

ExecutaCodigoA = True

FGAna.Col = 0
FGAna.row = 1
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_marcacao", EcDtMarcacao, mediTipoData
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_previ", EcDataPrevista, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_chega", EcDataChegada, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp", EcDataImpressao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp", EcHoraImpressao, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp2", EcDataImpressao2, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp2", EcHoraImpressao2, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcHoraImpressao2, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcHoraImpressao2, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fact", EcDataFact, mediTipoData
    
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp", EcUtilizadorImpressao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp2", EcUtilizadorImpressao2, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_cri", EcUtilizadorCriacao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_act", EcUtilizadorAlteracao, adVarChar
    
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    ExecutaCodigoP = False
    ExecutaCodigoT = False
    ExecutaCodigoA = False
    
    With FGAna
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 1000
        .Col = 0
        .TextMatrix(0, 0) = "C�digo"
        .ColWidth(1) = 6000
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    LastColA = 0
    LastRowA = 1
    CriaNovaClasse RegistosA, 1, "ANA", True

    With FgProd
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .TextMatrix(0, 0) = "Produto"
        .ColWidth(1) = 2900
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 900
        .Col = 2
        .TextMatrix(0, 2) = "Especifica."
        .ColWidth(3) = 2900
        .Col = 3
        .TextMatrix(0, 3) = "Descri��o"
        .ColWidth(4) = 950
        .Col = 4
        .TextMatrix(0, 4) = "Previsto"
        .ColWidth(5) = 950
        .Col = 5
        .TextMatrix(0, 5) = "Chegada"
        .ColWidth(6) = 600
        .Col = 6
        .TextMatrix(0, 6) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    LastColP = 0
    LastRowP = 1
    CriaNovaClasse RegistosP, 1, "PROD", True
    
    With FGTubos
        .rows = 2
        .FixedRows = 1
        .Cols = 6
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .TextMatrix(0, 0) = "Tubo"
        .ColWidth(1) = 5700
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 950
        .Col = 2
        .TextMatrix(0, 2) = "Previsto"
        .ColWidth(3) = 950
        .Col = 3
        .TextMatrix(0, 3) = "Chegada"
        .ColWidth(4) = 950
        .Col = 4
        .TextMatrix(0, 4) = "Hr.Chegada"
        .ColWidth(5) = 600
        .Col = 5
        .TextMatrix(0, 5) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    LastColT = 0
    LastRowT = 1
    CriaNovaClasse RegistosT, 1, "TUBO", True

    
    
    Set TAna.ImageList = ListaImagensAna
    TAna.Indentation = 0
    
    Flg_PergProd = False
    Resp_PergProd = False
    Flg_DtChProd = False
    Resp_PergDtChProd = False
    
    Flg_PergTubo = False
    Resp_PergTubo = False
    Flg_DtChTubo = False
    Resp_PergDtChTubo = False
    
    LaMoeda1 = gMoedaActiva
    LaMoeda2 = gMoedaActiva
    LaMoeda3 = gMoedaActiva
    
    ExecutaCodigoA = False
    ExecutaCodigoP = False
    
    BtEtiq.Enabled = False
    BtNorma.Enabled = False
    LaEtiq.Enabled = False
    LaInfCli.Enabled = False
    EcPrescricao.Locked = False
    
    If gUsaMarcacaoCodAnaEFR = mediSim Then
        FrameMarcaArs.Visible = True
    Else
        FrameMarcaArs.Visible = False
    End If

End Sub

Sub PreencheListaRecibos()

    Dim RecEnt As String
    Dim RecTax As String
    Dim RecUte As String
    
    RecEnt = ""
    RecTax = ""
    RecUte = ""
    If CbPagEnt.ListIndex = 0 Then
        RecEnt = NumRecEnt
    End If
    If CbPagUte.ListIndex = 0 Then
        RecUte = NumRecDoe
    End If
    If CbTaxaModeradora.ListIndex = 0 Then
        RecTax = NumRecTM
    End If
    
    If RecTax <> "" Then
        ListaRecibos.AddItem RecTax
    End If
    
    If RecUte <> "" And Trim(RecUte) <> Trim(RecTax) Then
        ListaRecibos.AddItem RecUte
    End If
    
    If RecEnt <> "" And Trim(RecEnt) <> Trim(RecTax) And Trim(RecEnt) <> Trim(RecUte) Then
        ListaRecibos.AddItem RecEnt
    End If

End Sub

Sub PreencheDadosTubo(indice As Long, _
                      DescrGrAna As String, _
                      abrAna As String, _
                      CodProd As String, _
                      CodTubo As String, _
                      Especial As Boolean, _
                      Cheio As Boolean, _
                      codTuboBar As String, _
                      QtMax As Double, _
                      QtOcup As Double, _
                      Designacao As String, inf_complementar As String, num_copias As Integer)
    
    Tubos(indice).CodProd = CodProd
    Tubos(indice).CodTubo = CodTubo
    Tubos(indice).GrAna = Trim(DescrGrAna)
    
    ' Espec�fico HUCs.
    ' Impress�o de etiquetas.
    If gLAB = cHUCs Then
        'OrdAna = 1
    End If
    
    'If OrdAna <> 0 Then
        Tubos(indice).abrAna = abrAna
    'Else
    '    Tubos(Indice).AbrAna = ""
    'End If
    
    Tubos(indice).Especial = Especial
    Tubos(indice).Cheio = Cheio
    Tubos(indice).codTuboBar = codTuboBar
    Tubos(indice).QtMax = QtMax
    Tubos(indice).QtOcup = QtOcup
    Tubos(indice).Designacao_tubo = Designacao
    Tubos(indice).inf_complementar = inf_complementar
    Tubos(indice).num_copias = num_copias
    
End Sub

Sub PreencheValoresDefeito()
    
    ' PFerreira 08.03.2007
    BG_PreencheComboBD_ADO "sl_cod_prioridades", "cod_prio", "nome_prio", CbPrioColheita, mediAscComboCodigo
    
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    
    BG_PreencheComboBD_ADO "sl_isencao", "seq_isencao", "descr_isencao", CbTipoIsencao
    BG_PreencheComboBD_ADO "sl_t_anul", "seq_t_anul", "descr_t_anul", CbTipoAnulacao, mediAscComboDesignacao
    
    'Preenche Combo Tipo Urgencia
    BG_PreencheComboBD_ADO "sl_tbf_t_destino", "cod_t_dest", "descr_t_dest", CbDestino, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, mediAscComboCodigo
    
    'Preenche Combo Taxa Moderadora
    CbTaxaModeradora.AddItem "Pago"
    CbTaxaModeradora.AddItem "N�o Pago"
    
    'Preenche Combo valor pago pela entidade
    CbPagEnt.AddItem "Pago"
    CbPagEnt.AddItem "N�o Pago"
    
    'Preenche Combo valor pago pelo utente
    CbPagUte.AddItem "Pago"
    CbPagUte.AddItem "N�o Pago"
    
    'Preenche combo estado da requisi��o
    CbEstadoReq.AddItem "Sem an�lises"
    CbEstadoReq.AddItem "Espera de produtos"
    CbEstadoReq.AddItem "Espera de resultados"
    CbEstadoReq.AddItem "Espera de valida��o m�dica"
    CbEstadoReq.AddItem "Cancelada"
    CbEstadoReq.AddItem "Valida��o m�dica completa"
    CbEstadoReq.AddItem "N�o passar a hist�rico"
    CbEstadoReq.AddItem "Todas impressas"
    CbEstadoReq.AddItem "Hist�rico"
    CbEstadoReq.AddItem "Resultados parciais"
    CbEstadoReq.AddItem "Valida��o m�dica parcial"
    CbEstadoReq.AddItem "Impress�o parcial"
    CbEstadoReq.AddItem "Bloqueada"
    
    'Preenche combo estado da requisi��o auxiliar
    CbEstadoReqAux.AddItem gEstadoReqSemAnalises
    CbEstadoReqAux.AddItem gEstadoReqEsperaProduto
    CbEstadoReqAux.AddItem gEstadoReqEsperaResultados
    CbEstadoReqAux.AddItem gEstadoReqEsperaValidacao
    CbEstadoReqAux.AddItem gEstadoReqCancelada
    CbEstadoReqAux.AddItem gEstadoReqValicacaoMedicaCompleta
    CbEstadoReqAux.AddItem gEstadoReqNaoPassarHistorico
    CbEstadoReqAux.AddItem gEstadoReqTodasImpressas
    CbEstadoReqAux.AddItem gEstadoReqHistorico
    CbEstadoReqAux.AddItem gEstadoReqResultadosParciais
    CbEstadoReqAux.AddItem gEstadoReqValidacaoMedicaParcial
    CbEstadoReqAux.AddItem gEstadoReqImpressaoParcial
    CbEstadoReqAux.AddItem gEstadoReqBloqueada
    CkFlgInvisivel.value = vbGrayed
End Sub

Sub EventoActivate()
    
    Dim i As Integer
    
    If FormGesReqCons.Enabled = False Then
        FormGesReqCons.Enabled = True
    End If
    
    Set gFormActivo = Me
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
    BG_StackJanelas_Actualiza Me
    
    BL_ToolbarEstadoN estado
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    If estado = 2 Then
       BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        Err.Clear
        
        If rs!estado_req = "C" Then
            
            Select Case Err.Number
                Case 0
                    ' OK
                Case 3021
                    ' BOF ou EOF � verdadeiro ou o registo actual foi eliminado; a opera��o pedida necessita de um registo actual.
                    rs.Requery
                Case Else
                
            End Select
            
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            BtCancelarReq.Visible = False
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
        End If
    End If
       
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If gDUtente.seq_utente <> "" And (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        'j� tem utente
        EcSeqUtente = gDUtente.seq_utente
        If EcSeqUtente = -1 And gF_IDENTIF = 1 Then
            FormIdentificaUtente.Flg_PesqUtente = True
            Unload Me
            Exit Sub
        End If
        PreencheDadosUtente
        Flg_PesqUtente = False
    End If
    
    ' PFerreira 10.04.2007
    ' Caso o ecra de identificacao de utentes for o ecra anterior preenche a prioridade de colheita
    If (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        If (Trim(EcDataNasc) <> "" And IsChild) Then
            CbPrioColheita.ListIndex = 3
        Else
            CbPrioColheita.ListIndex = 1
        End If
    End If
    
    If gF_IDENTIF = 1 And SSTGestReq.Tab = 0 Then
        If Trim(FormIdentificaUtente.EcCodIsencao.Text) <> "" Then
            CkIsento.value = vbChecked
            For i = 0 To CbTipoIsencao.ListCount - 1
                If CbTipoIsencao.ItemData(i) = CLng(Trim(FormIdentificaUtente.EcCodIsencao.Text)) Then
                    CbTipoIsencao.ListIndex = i
                    SSTGestReq.TabEnabled(3) = False
                    SSTGestReq.TabVisible(3) = False
                    SSTGestReq.TabEnabled(4) = False
                    SSTGestReq.TabVisible(4) = False
                    Exit For
                End If
            Next i
        Else
            If gRecibos = "1" Then
                SSTGestReq.TabEnabled(3) = True
                SSTGestReq.TabVisible(3) = False
                SSTGestReq.TabEnabled(4) = True
                SSTGestReq.TabVisible(4) = True
            Else
                SSTGestReq.TabEnabled(3) = False
                SSTGestReq.TabEnabled(4) = False
                SSTGestReq.TabVisible(4) = False
            End If
        End If
    
        EcObsProveniencia.SetFocus
        
    End If
    
'    If (gPassaParams.Param(0) <> "") Then
'        EcUtente.Text = gPassaParams.Param(0)
'    End If
    
    If gMultiReport = 1 Then
        EcReqAux.Visible = True
        LaReqaux.Visible = True
        EcFicha.Visible = True
        LaFicha.Visible = True
        SSTGestReq.TabVisible(5) = False
        SSTGestReq.TabVisible(4) = False
        BtEtiq.ToolTipText = "Imprimir Etiquetas de M�dicos"
        FrEtiq.ToolTipText = "Imprimir Etiquetas de M�dicos"
        LaEtiq.ToolTipText = "Imprimir Etiquetas de M�dicos"
        LaObsProveniencia.caption = "&Gesta��o"
        EcObsProveniencia.Width = 600
        EcObsProveniencia.Alignment = vbCenter
        LaSemanas.Visible = True
        LaDataFact.Visible = True
        EcDataFact.Visible = True
        CkIsento.Enabled = False
        CbTipoIsencao.Enabled = False
        Select Case gLAB
            Case "CITO"
                LaMedico.caption = "Resp.Cari�tipo"
                CkIsento.Visible = False
                CbTipoIsencao.Visible = False
                EcCodArquivo.Visible = True
                BtPesquisaArquivo.Visible = True
                EcDescrArquivo.Visible = True
                LaArquivo.Visible = True
                EcCodMedico2.Visible = True
                EcDescrMedico2.Visible = True
                LaMedico2.Visible = True
                BtPesquisaMedico2.Visible = True
                BtPesquisaMedico2.Enabled = True
            Case "GM"
                LaReqaux.caption = "C�digo Caso"
                LaMedico.caption = "Respons�vel"
        End Select
    End If
    
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
End Sub

Sub PreencheDadosUtente()
    
    Dim Campos(7) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim i As Integer
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "n_proc_1"
    Campos(4) = "n_proc_2"
    Campos(5) = "n_cartao_ute"
    Campos(6) = "dt_nasc_ute"

    iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.Text = ""
        Exit Sub
    End If
    
    EcUtente.Text = retorno(0)
    CbTipoUtente.Text = retorno(1)
    EcNome.Text = retorno(2)
    EcProcHosp1.Text = retorno(3)
    EcProcHosp2.Text = retorno(4)
    EcNumCartaoUte.Text = retorno(5)
    EcDataNasc.Text = retorno(6)
    EcEpisodio.Text = gEpisodioActivo
    For i = 0 To CbSituacao.ListCount - 1
        If CbSituacao.ItemData(i) = gSituacaoActiva Then
            CbSituacao.ListIndex = i
        End If
    Next
    If gLAB = "HFM" And EcDataPrevista.Text = "" Then
        If Weekday(Bg_DaData_ADO) = 6 Then
            EcDataPrevista.Text = Bg_DaData_ADO + 3
        Else
            EcDataPrevista.Text = Bg_DaData_ADO + 1
        End If
        EcDataPrevista_Validate False
        EcDataPrevista_GotFocus
        BG_MostraComboSel 0, CbUrgencia
        EcCodProveniencia.Text = 8
        EcCodProveniencia_Validate False
        
    End If
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim And Me.ExternalAccess = True Then
    Dim contextJson As String
    Dim responseJson As String
    Dim requestJson As String
        contextJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & Chr(34) & "," & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & "}"
        
        responseJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & ", " & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & _
                ", " & Chr(34) & "EcNome" & Chr(34) & ":" & Chr(34) & retorno(2) & Chr(34) & ", " & Chr(34) & "EcProcHosp1" & Chr(34) & ":" & Chr(34) & retorno(3) & Chr(34) & _
                ", " & Chr(34) & "EcProcHosp2" & Chr(34) & ":" & Chr(34) & retorno(4) & Chr(34) & ", " & Chr(34) & "EcNumCartaoUte" & Chr(34) & ":" & Chr(34) & retorno(5) & Chr(34) & _
                ", " & Chr(34) & "EcDataNasc" & Chr(34) & ":" & Chr(34) & retorno(6) & Chr(34) & ", " & Chr(34) & "EcEpisodio" & Chr(34) & ":" & Chr(34) & gEpisodioActivo & Chr(34) & "}"
        
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, contextJson, contextJson, IIf(responseJson = vbNullString, "{}", responseJson)
    End If
    
    BtPassaMarcacao.Enabled = True
    BtInfClinica.Enabled = True
    LaInfCli.Enabled = True
    
End Sub

Sub PreencheEFRUtente()
    
    On Error GoTo ErrorHandler
    
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim SQLEFR As String
    Dim tabelaEFR As ADODB.recordset
    Dim SelEntUte As Boolean
    
'    If (Trim(EcCodEFR.Text) = "") Then
    
        SelEntUte = False
        If Trim(EcSeqUtente) <> "" Then
            If rs Is Nothing Then
                SelEntUte = True
            ElseIf rs.state = adStateClosed Then
                SelEntUte = True
            End If
        End If
        
        If SelEntUte = True Then
            Campos(0) = "cod_efr_ute"
            Campos(1) = "n_benef_ute"
            iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
            
            If iret = -1 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar E.F.R. do utente!", vbCritical + vbOKOnly, App.ProductName
                Exit Sub
            End If
            
            EcCodEFR.Text = retorno(0)
            EcNumBenef.Text = retorno(1)
        
            Set tabelaEFR = New ADODB.recordset
            tabelaEFR.CursorType = adOpenStatic
            tabelaEFR.CursorLocation = adUseClient
            
            SQLEFR = "SELECT " & _
                     "      descr_efr " & _
                     "FROM " & _
                     "      sl_efr " & _
                     "WHERE " & _
                     "      cod_efr = " & Trim(EcCodEFR.Text)
            
            tabelaEFR.Open SQLEFR, gConexao
            If tabelaEFR.RecordCount > 0 Then
                EcDescrEFR.Text = tabelaEFR!descr_efr
            End If
        
            tabelaEFR.Close
            Set tabelaEFR = Nothing
        End If
        ValidarARS EcCodEFR
    
'    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : PreencheEFRUtente (FormGesReqCons) -> " & Err.Description
            Exit Sub
    End Select
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    If Trim(EcPrinterEtiq) <> "" Then
        gImpZebra = EcPrinterEtiq
    End If
    
    ' Descarregar todos os objectos
    Set RegistosP = Nothing
    
    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then
            rs.Close
        End If
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Isencao")
    Call BL_FechaPreview("Recibo Requisi��o")
    
    
    
    Call BG_RollbackTransaction
    Call Apaga_DS_TM
        
    If gF_IDENTIF = 1 Then
        FormIdentificaUtente.Enabled = True
        Set gFormActivo = FormIdentificaUtente
        
    ElseIf gF_REQPENDELECT = 1 Then
        FormReqPedElectr.Enabled = True
        Set gFormActivo = FormReqPedElectr
    Else
        Set gFormActivo = MDIFormInicio
    End If
    
    gF_REQCONS = 0
    Set FormGesReqCons = Nothing
    
    gEpisodioActivo = ""
    gSituacaoActiva = mediComboValorNull

End Sub

Sub SELECT_Dados_Perfil(Marcar() As String, indice As Integer, CodPerfil As String)

    Dim RsPerf As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim JaExiste As Boolean

CodPerfil = UCase(Trim(CodPerfil))

'Activo ?
sql = "SELECT descr_perfis, flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
Set RsPerf = New ADODB.recordset
With RsPerf
    .Source = sql
    .CursorLocation = adUseClient
    .CursorType = adOpenStatic
    .Open , gConexao
End With

Activo = "-1"
If RsPerf.RecordCount > 0 Then
    Activo = BL_HandleNull(RsPerf!flg_activo, "0")
    descricao = BL_HandleNull(RsPerf!descr_perfis, " ")
Else
    BG_LogFile_Erros Me.Name & " - SELECT_Dados_Perfil: Erro a seleccionar dados do perfil!"
End If
RsPerf.Close
Set RsPerf = Nothing

'Membros do perfil
sql = "SELECT cod_analise, ordem FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
Set RsPerf = New ADODB.recordset
With RsPerf
    .Source = sql
    .CursorLocation = adUseClient
    .CursorType = adOpenStatic
    .Open , gConexao
End With

If RsPerf.RecordCount <> 0 Then
    If Activo = "1" Then
        BL_AcrescentaNodoPai TAna, descricao, CodPerfil, 1, 2
        SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, CodPerfil, descricao, indice, "C99999", " ", -1
    End If
Else
    BG_Mensagem mediMsgBox, "Perfil '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
End If

FGAna.CellBackColor = vbWhite
FGAna.CellForeColor = vbBlack
While Not RsPerf.EOF
    JaExiste = Verifica_Ana_ja_Existe(indice, BL_HandleNull(RsPerf!cod_analise, " "))

    If Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "S" Then
        If Activo = "1" Then
            SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0)
        ElseIf Activo = "0" And JaExiste = False Then
            SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " ")
        End If
    ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "C" Then
        If Activo = "1" Then
            SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0)
        ElseIf Activo = "0" And JaExiste = False Then
            SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " ")
        End If
    ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "P" Then
        If Activo = "1" Then
            SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " ")
        ElseIf Activo = "0" And JaExiste = False Then
            SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " ")
        End If
    End If
    RsPerf.MoveNext
    
    If Not RsPerf.EOF And Activo = "0" And JaExiste = False Then
        ExecutaCodigoA = False
        indice = indice + 1
        FGAna.AddItem "", indice
        Insere_aMeio_RegistosA FGAna.row + 1
        FGAna.row = FGAna.row + 1
        FGAna.Col = 0
        ExecutaCodigoA = True
    End If
Wend

RsPerf.Close
Set RsPerf = Nothing

End Sub

Sub SELECT_Dados_Complexa(Marcar() As String, indice As Integer, CodComplexa As String, Optional Perfil As Variant, Optional DescrPerfil As Variant, Optional OrdemP As Variant)

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String

    'Activo ?
    sql = "SELECT descr_ana_c, flg_marc_memb " & _
          "FROM sl_ana_c " & _
          "WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    Set RsCompl = New ADODB.recordset

    With RsCompl
        .Source = sql
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .Open , gConexao
    End With
    
    Activo = "-1"
    If RsCompl.RecordCount > 0 Then
        Activo = BL_HandleNull(RsCompl!flg_marc_memb, "0")
        descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
    End If
    RsCompl.Close
    Set RsCompl = Nothing

    If Activo = "1" Then
        'soliveira teste
        If Not IsMissing(Perfil) Then
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1
        End If
        
        'Membros da complexa
        sql = "SELECT cod_membro, ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND t_membro = 'A' ORDER BY ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .Open , gConexao
        End With
        
        If RsCompl.RecordCount = 0 Then
            BG_Mensagem mediMsgBox, "Complexa '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
        Else
            If Not IsMissing(Perfil) Then
                BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            Else
                BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            End If
        End If
        
        While Not RsCompl.EOF
            
            If Not IsMissing(Perfil) Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
            Else
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
            End If
            RsCompl.MoveNext
        Wend
    Else
        If Not IsMissing(Perfil) Then
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1
        Else
            BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, , , , CodComplexa, descricao, -1
        End If
    
        'Membros da complexa
        sql = "SELECT cod_membro, sl_membro.ordem FROM sl_membro,sl_ana_s " & _
            " WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND cod_ana_s = cod_membro " & _
            " AND flg_marc_auto = '1' " & _
            " AND t_membro = 'A' ORDER BY sl_membro.ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .Open , gConexao
        End With
    
        If Not RsCompl.EOF Then
            While Not RsCompl.EOF
                
                If Not IsMissing(Perfil) Then
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                Else
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                End If
                RsCompl.MoveNext
            Wend
        Else
            If IsMissing(Perfil) Then
                GhostPerfil = "0"
                GhostComplexa = CodComplexa
                Lista_GhostMembers GhostComplexa, descricao
            End If
        End If
        
    End If
    RsCompl.Close
    Set RsCompl = Nothing

End Sub

Function SELECT_Dados_Simples(Marcar() As String, indice As Integer, CodSimples As String, Optional Perfil As Variant, Optional DescrPerfil As Variant, Optional OrdemP As Variant, Optional complexa As Variant, Optional DescrComplexa As Variant, Optional OrdemC As Variant)

    Dim RsSimpl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim codAgrup As String
    Dim descrAgrup As String
    Dim i As Integer
    Dim NaoMarcar As Boolean
    
    SELECT_Dados_Simples = True

If CodSimples = gGHOSTMEMBER_S Then
    If Not IsMissing(complexa) Then
        If Not IsMissing(Perfil) Then
            codAgrup = Perfil
            descrAgrup = DescrPerfil
            Preenche_MAReq Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, " ", CLng(indice), OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", CInt(indice)
            BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Perfil & complexa, complexa & gGHOSTMEMBER_S, 5, 6
        Else
            codAgrup = complexa
            descrAgrup = DescrComplexa
        
            Preenche_MAReq "0", " ", complexa, DescrComplexa, CodSimples, " ", CLng(indice), OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", CInt(indice)
            BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", complexa, complexa & gGHOSTMEMBER_S, 5, 6
        End If
    End If
Else
    NaoMarcar = True
    For i = 1 To UBound(Marcar)
        If InStr(1, Marcar(i), CodSimples & ";") <> 0 Then
            NaoMarcar = False
            Exit For
        End If
    Next i

    If NaoMarcar = True Then Exit Function

    sql = "SELECT descr_ana_s, flg_invisivel FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(CodSimples)
    Set RsSimpl = New ADODB.recordset
    With RsSimpl
        .Source = sql
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .Open , gConexao
    End With
    
    If RsSimpl.RecordCount > 0 Then
        descricao = BL_HandleNull(RsSimpl!descr_ana_s, " ")
        'N�o marcar a an�lise se flg_invisivel = 1
        If RsSimpl!flg_invisivel = 1 Then
            RsSimpl.Close
            Set RsSimpl = Nothing
            SELECT_Dados_Simples = False
            Exit Function
        End If
    End If
    RsSimpl.Close
    Set RsSimpl = Nothing
    
    If Not IsMissing(complexa) Then
         If Not IsMissing(Perfil) Then
            codAgrup = Perfil
            descrAgrup = DescrPerfil
            Preenche_MAReq Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, descricao, CLng(indice), OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", CInt(indice)
            BL_AcrescentaNodoFilho TAna, descricao, Perfil & complexa, complexa & CodSimples, 5, 6
         Else
            codAgrup = complexa
            descrAgrup = DescrComplexa
         
            Preenche_MAReq "0", " ", complexa, DescrComplexa, CodSimples, descricao, CLng(indice), OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", CInt(indice)
            BL_AcrescentaNodoFilho TAna, descricao, complexa, complexa & CodSimples, 5, 6
         End If
    Else
         If Not IsMissing(Perfil) Then
            codAgrup = Perfil
            descrAgrup = DescrPerfil
            
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodSimples, 5, 6
            Preenche_MAReq Perfil, DescrPerfil, "0", " ", CodSimples, descricao, CLng(indice), 0, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", CInt(indice)
         Else
            codAgrup = CodSimples
            descrAgrup = descricao
         
            BL_AcrescentaNodoPai TAna, descricao, CodSimples, 5, 6
            Preenche_MAReq "0", " ", "0", " ", CodSimples, descricao, CLng(indice), 0, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", CInt(indice)
         End If
    End If
    
End If

If Verifica_Ana_ja_Existe(0, codAgrup) = False Then
    If Trim(RegistosA(indice).codAna) = "" Then
        RegistosA(indice).codAna = codAgrup
        RegistosA(indice).descrAna = descrAgrup
        RegistosA(indice).estado = "-1"
        
        If indice <> 1 Then
            If Trim(RegistosA(indice - 1).NReqARS) <> "" Then
                RegistosA(indice).NReqARS = RegistosA(indice - 1).NReqARS
            Else
                If Trim(EcNumReq) <> "" Then
                    RegistosA(indice).NReqARS = EcNumReq
                Else
                    RegistosA(indice).NReqARS = "A gerar"
                End If
            End If
        Else
            If Trim(EcNumReq) <> "" Then
                RegistosA(indice).NReqARS = EcNumReq
            Else
                RegistosA(indice).NReqARS = "A gerar"
            End If
        End If
        
        If Trim(RegistosA(indice).p1) = "" Then
            RegistosA(indice).p1 = ContP1
            ContP1 = ContP1 + 1
        End If
        FGAna.TextMatrix(indice, 0) = codAgrup
        FGAna.TextMatrix(indice, 1) = descrAgrup
        If FGAna.Cols = 4 Then
            FGAna.TextMatrix(indice, 2) = RegistosA(indice).NReqARS
            FGAna.TextMatrix(indice, 3) = RegistosA(indice).p1
        End If
    End If
End If

End Function

Function SELECT_Descr_Ana(codAna As String) As String

    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    Dim Flg_SemTipo As Boolean
    
    codAna = UCase(codAna)
    If InStr(1, "SCP", Mid(codAna, 1, 1)) = 0 Then
        Flg_SemTipo = True
    End If
    
    With rsDescr
        If Flg_SemTipo = True Then
            If gLAB = "CHVNG" Then
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='S" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                                "cod_ana_c='C" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE " & _
                                "cod_perfis='P" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL)) "
            Else
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='S" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                            " UNION " & _
                                "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                                "cod_ana_c='C" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                            " UNION " & _
                                "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE " & _
                                "cod_perfis='P" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")) "
            End If
                            
        Else
            If gLAB = "CHVNG" Then
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                                "cod_ana_c='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL)) " & _
                            " UNION " & _
                                "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE " & _
                                "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )"
            
            Else
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                            " UNION " & _
                                "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                                "cod_ana_c='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (Select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")) " & _
                            " UNION " & _
                                "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE " & _
                                "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))"
            End If

                        
        End If
        .ActiveConnection = gConexao
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        SELECT_Descr_Ana = ""
    Else
        If BL_HandleNull(rsDescr!inibe_marcacao, "0") = "1" Then
            SELECT_Descr_Ana = ""
        Else
            SELECT_Descr_Ana = BL_HandleNull(rsDescr!descricao, "")
            If Flg_SemTipo = True Then
                codAna = rsDescr!tipo & codAna & ""
            End If
        End If
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing

End Function

Sub ValidarARS(codEfr As String)
    
    ExecutaCodigoA = False
    
    If EFR_Verifica_ARS(codEfr) = True Then
    
        FlgARS = True
        
        If FGAna.Cols = 2 Then
            FGAna.Cols = 4
            FGAna.ColWidth(1) = 4500
            FGAna.ColWidth(2) = 1200
            FGAna.Col = 2
            FGAna.TextMatrix(0, 2) = "Req. ARS"
            FGAna.ColWidth(3) = 400
            FGAna.Col = 3
            FGAna.TextMatrix(0, 3) = "P1"
            FGAna.Col = 0
            
            If FGAna.TextMatrix(1, 0) <> "" Then
                For ContP1 = 1 To FGAna.rows - 2
                    If Trim(EcNumReq) <> "" Then
                        RegistosA(ContP1).NReqARS = EcNumReq
                    Else
                        RegistosA(ContP1).NReqARS = "A gerar"
                    End If
                    RegistosA(ContP1).p1 = ContP1
                    FGAna.TextMatrix(ContP1, 2) = RegistosA(ContP1).NReqARS
                    FGAna.TextMatrix(ContP1, 3) = ContP1
                Next ContP1
            Else
                ContP1 = 1
            End If
            
        Else

            If FGAna.TextMatrix(1, 0) <> "" Then
                For ContP1 = 1 To FGAna.rows - 2
                    If Trim(EcNumReq) <> "" Then
                        RegistosA(ContP1).NReqARS = EcNumReq
                    Else
                        RegistosA(ContP1).NReqARS = "A gerar"
                    End If
                    RegistosA(ContP1).p1 = ContP1
                    FGAna.TextMatrix(ContP1, 2) = RegistosA(ContP1).NReqARS
                    FGAna.TextMatrix(ContP1, 3) = ContP1
                Next ContP1
            Else
                ContP1 = 1
            End If
            
        End If
    
    Else
        FlgARS = False
        
        If FGAna.Cols <> 2 Then
            FGAna.Cols = 2
            
            If FGAna.TextMatrix(1, 0) <> "" Then
                For ContP1 = 1 To FGAna.rows - 2
                    RegistosA(ContP1).NReqARS = ""
                    RegistosA(ContP1).p1 = ""
                Next ContP1

                ContP1 = 1
            End If
        
        End If
    End If
    
    ExecutaCodigoA = True
    
End Sub

Function Verifica_Ana_Prod(codAgrup As String, ByRef Marcar() As String) As Boolean
    Dim SQLAux As String
    Dim RsAnaProd As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim k As Integer
    Dim CodProduto() As String
    Dim Resp As Integer
    Dim ListaProd() As String
    Dim JaTemProduto() As String
    Dim TotJaTemProduto As Integer

    gColocaDataChegada = ""
    codAgrup = UCase(Trim(codAgrup))

'----------- PROCURAR NOS PRODUTOS JA MARCADOS, OS PRODUTOS DA ANALISE

sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a LEFT OUTER JOIN sl_produto p ON a.cod_produto = p.cod_produto " & _
    " Where Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a LEFT OUTER JOIN sl_produto p ON a.cod_produto = p.cod_produto " & _
    " WHERE  cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
    " AND t_membro = 'A' )" & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a LEFT OUTER JOIN sl_produto p ON a.cod_produto = p.cod_produto " & _
    " WHERE  cod_ana_s IN " & _
    "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
    " AND cod_analise LIKE 'S%' ) Union " & _
    " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
    " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' ))"


SQLAux = " select cod_perfis cod_ana_s, descr_perfis descr_ana_s, sl_perfis.cod_produto,descr_produto from sl_perfis, sl_produto where " & _
          " sl_perfis.cod_produto = sl_produto.cod_produto and cod_perfis = " & BL_TrataStringParaBD(codAgrup)

Set RsAnaProd = New ADODB.recordset
With RsAnaProd
    .CursorLocation = adUseClient
    .CursorType = adOpenStatic
    .Source = SQLAux
    .ActiveConnection = gConexao
    .Open
End With
If RsAnaProd.RecordCount <= 0 Then
    RsAnaProd.Close
    With RsAnaProd
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .Source = sql
        .ActiveConnection = gConexao
        .Open
    End With
End If

Verifica_Ana_Prod = False

ReDim JaTemProduto(0)
TotJaTemProduto = 0
ReDim ListaProd(0)
ReDim Marcar(0)
k = 0
While Not RsAnaProd.EOF
    If Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) <> "" Then
        For i = 1 To RegistosP.Count - 1
            If Trim(RegistosP(i).CodProd) = Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) Then
                Verifica_Ana_Prod = True
                gColocaDataChegada = RegistosP(i).DtChega
                Exit For
            Else
                Verifica_Ana_Prod = False
            End If
        Next i
        
        If Verifica_Ana_Prod = False Then
            k = k + 1
            ReDim Preserve ListaProd(k)
            ListaProd(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";" & BL_HandleNull(RsAnaProd!cod_produto, "") & ";" & BL_HandleNull(RsAnaProd!descr_produto, "") & Space(20 - Len(Trim(BL_HandleNull(RsAnaProd!descr_produto, "")))) & " -> " & Trim(BL_HandleNull(RsAnaProd!descr_ana_s, ""))
        Else
            TotJaTemProduto = TotJaTemProduto + 1
            ReDim Preserve JaTemProduto(TotJaTemProduto)
            JaTemProduto(TotJaTemProduto) = BL_HandleNull(RsAnaProd!cod_ana_s, "")
        End If
    End If
    
    RsAnaProd.MoveNext
Wend
    
'----------- ALGUM NAO ESTA NA LISTA

If k <> 0 Then
    
    Verifica_Ana_Prod = False
    
    'Pergunta 1: Quer marcar o produto da analise ?
        
    If Flg_PergProd = True Then
        'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
        'que se deve adoptar a ultima resposta � pergunta
            
        If Resp_PergProd = True Then
            'respondeu na ultima vez que sim � pergunta 1
            Verifica_Ana_Prod = True
                
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), Bg_DaData_ADO
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                k = 0
                ReDim Marcar(0)
                For i = 1 To UBound(ListaProd)
                    CodProduto = Split(ListaProd(i), ";")
                    k = k + 1
                    ReDim Preserve Marcar(k)
                    Marcar(k) = CodProduto(0) & ";" & CodProduto(1)
                Next i
                
                FormGesReqCons.Enabled = False

                If FormPergProdCons.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), Bg_DaData_ADO
                    Next i
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
                
            End If
        Else
            'respondeu na ultima vez que n�o � pergunta 1
            Verifica_Ana_Prod = True
        End If
    Else
        'O utilizador ainda n�o escolheu que a pergunta n�o
        'deve ser efectuada de novo, logo questiona-se a pergunta 1
        
        FormGesReqCons.Enabled = False
            
        If FormPergProdCons.PerguntaProduto(ConfirmarDefeito, Flg_PergProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
       
            'respondeu sim � pergunta 1
            Resp_PergProd = True
            Verifica_Ana_Prod = True
            
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), Bg_DaData_ADO
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                If FormPergProdCons.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), Bg_DaData_ADO
                    Next i
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
            End If
        Else
            'respondeu n�o � pergunta 1
            Resp_PergProd = False
        End If
        
    End If

    ' retirar do array de an�lises a marcar o codigo do produto (fica "S1;S2;S3;S4;")
    For i = 1 To UBound(Marcar)
        CodProduto = Split(Marcar(i))
        Marcar(i) = CodProduto(0) & ";"
    Next i
     
    '----------- PERMITIR MARCAR AS ANALISES QUE NAO TEM PRODUTO ASSOCIADO
    
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = UBound(Marcar)
    While Not RsAnaProd.EOF
        If BL_HandleNull(RsAnaProd!cod_produto, "") = "" Then
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
            If gColocaDataChegada = "" Then gColocaDataChegada = Bg_DaData_ADO
            Verifica_Ana_Prod = True
        End If
        RsAnaProd.MoveNext
    Wend
    
    '----------- MARCAR AS ANALISES QUE J� TINHAM O PRODUTO REGISTADO
    
    k = UBound(Marcar)
    If TotJaTemProduto <> 0 Then
        For i = 1 To TotJaTemProduto
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = JaTemProduto(i) & ";"
        Next i
    End If
    
Else
    Verifica_Ana_Prod = True
    
    'Marca todas
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = 0
    ReDim Marcar(0)
    If Not RsAnaProd.EOF Then
        If gColocaDataChegada = "" And EcDataChegada.Text <> "" Then    'sdo em vez de: If gColocaDataChegada = "" Then gColocaDataChegada = BG_DaData_ADO
            gColocaDataChegada = EcDataChegada.Text                     'sdo
        ElseIf EcDataChegada.Text = "" Then                             'sdo
            gColocaDataChegada = ""                                     'sdo
        End If                                                          'sdo
    End If
    While Not RsAnaProd.EOF
        k = k + 1
        ReDim Preserve Marcar(k)
        Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
        RsAnaProd.MoveNext
    Wend

End If

RsAnaProd.Close
Set RsAnaProd = Nothing

End Function

Function Verifica_Ana_Tubo(codAgrup As String, ByRef Marcar() As String) As Boolean

    Dim RsAnaTubo As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim k As Integer
    Dim CodTubo() As String
    Dim Resp As Integer
    Dim ListaTubos() As String
    Dim JaTemTubo() As String
    Dim TotJaTemTubo As Integer
    Dim SQLAux As String
    gColocaDataChegada = ""
    codAgrup = UCase(Trim(codAgrup))

'----------- PROCURAR NOS TUBOS JA MARCADOS, OS TUBOS DA ANALISE

sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_tubo, t.descr_tubo FROM sl_ana_s a LEFT OUTER JOIN sl_tubo t ON a.cod_tubo = t.cod_tubo " & _
    " Where Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_tubo, t.descr_tubo FROM sl_ana_s a LEFT OUTER JOIN sl_tubo t ON a.cod_tubo = t.cod_tubo " & _
    " WHERE cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
    " AND t_membro = 'A' )" & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_tubo, t.descr_tubo FROM sl_ana_s a LEFT OUTER JOIN sl_tubo t ON a.cod_tubo = t.cod_tubo " & _
    " WHERE cod_ana_s IN " & _
    "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
    " AND cod_analise LIKE 'S%' ) Union " & _
    " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
    " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' ))"

SQLAux = "SELECT cod_perfis cod_ana_s, descr_perfis descr_ana_s, p.cod_tubo, descr_tubo " & _
          " FROM sl_perfis p, sl_tubo t WHERE p.cod_tubo = t.cod_tubo AND cod_perfis = " & BL_TrataStringParaBD(codAgrup)

Set RsAnaTubo = New ADODB.recordset
With RsAnaTubo
    .CursorLocation = adUseClient
    .CursorType = adOpenStatic
    .Source = SQLAux
    .ActiveConnection = gConexao
    .Open
End With
If RsAnaTubo.RecordCount <= 0 Then
    RsAnaTubo.Close
    With RsAnaTubo
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .Source = sql
        .ActiveConnection = gConexao
        .Open
    End With
End If

Verifica_Ana_Tubo = False

ReDim JaTemTubo(0)
TotJaTemTubo = 0
ReDim ListaTubos(0)
ReDim Marcar(0)
k = 0
While Not RsAnaTubo.EOF
    If Trim(BL_HandleNull(RsAnaTubo!cod_tubo, "")) <> "" Then
        For i = 1 To RegistosT.Count - 1
            If Trim(RegistosT(i).CodTubo) = Trim(BL_HandleNull(RsAnaTubo!cod_tubo, "")) Then
                Verifica_Ana_Tubo = True
                gColocaDataChegada = RegistosT(i).DtChega
                Exit For
            Else
                Verifica_Ana_Tubo = False
            End If
        Next i
        
        If Verifica_Ana_Tubo = False Then
            k = k + 1
            ReDim Preserve ListaTubos(k)
            ListaTubos(k) = BL_HandleNull(RsAnaTubo!cod_ana_s, "") & ";" & BL_HandleNull(RsAnaTubo!cod_tubo, "") & ";" & BL_HandleNull(RsAnaTubo!descR_tubo, "") & Space(40 - Len(Trim(BL_HandleNull(RsAnaTubo!descR_tubo, "")))) & " -> " & Trim(BL_HandleNull(RsAnaTubo!descr_ana_s, ""))
        Else
            TotJaTemTubo = TotJaTemTubo + 1
            ReDim Preserve JaTemTubo(TotJaTemTubo)
            JaTemTubo(TotJaTemTubo) = BL_HandleNull(RsAnaTubo!cod_ana_s, "")
        End If
    End If
    
    RsAnaTubo.MoveNext
Wend
    
'----------- ALGUM NAO ESTA NA LISTA

If k <> 0 Then
    
    Verifica_Ana_Tubo = False
    
    'Pergunta 1: Quer marcar o Tubo da analise ?
        
    If Flg_PergTubo = True Then
        'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
        'que se deve adoptar a ultima resposta � pergunta
            
        If Resp_PergTubo = True Then
            'respondeu na ultima vez que sim � pergunta 1
            Verifica_Ana_Tubo = True
                
            'Pergunta 2: Quer inserir a data de hoje na chegada do Tubo ?
            
            If Flg_DtChTubo = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(ListaTubos)
                        CodTubo = Split(ListaTubos(i), ";")
                        InsereAutoTubo CodTubo(1), Bg_DaData_ADO, Bg_DaHora_ADO
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(ListaTubos)
                        CodTubo = Split(ListaTubos(i), ";")
                        InsereAutoTubo CodTubo(1), "", ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                k = 0
                ReDim Marcar(0)
                For i = 1 To UBound(ListaTubos)
                    CodTubo = Split(ListaTubos(i), ";")
                    k = k + 1
                    ReDim Preserve Marcar(k)
                    Marcar(k) = CodTubo(0) & ";" & CodTubo(1)
                Next i
                
                FormGesReqCons.Enabled = False

'                If FormPergtubo.Perguntatubo(ConfirmarDtChega, Flg_DtChTubo, SELECT_Descr_Ana(CodAgrup), ListaTubos, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodTubo = Split(Marcar(i), ";")
                        InsereAutoTubo CodTubo(1), Bg_DaData_ADO, Bg_DaHora_ADO
                    Next i
'                Else
'                    'respondeu n�o � pergunta 2
'                    For i = 1 To UBound(Marcar)
'                        CodTubo = Split(Marcar(i), ";")
'                        InsereAutoTubo CodTubo(1), ""
'                    Next i
'                    Flg_PreencherDtChega = False
'                End If
                
            End If
        Else
            'respondeu na ultima vez que n�o � pergunta 1
            Verifica_Ana_Tubo = True
        End If
    Else
        'O utilizador ainda n�o escolheu que a pergunta n�o
        'deve ser efectuada de novo, logo questiona-se a pergunta 1
        
        FormGesReqCons.Enabled = False
            
        If FormPergTubo.PerguntaTubo(ConfirmarDefeito, Flg_PergTubo, SELECT_Descr_Ana(codAgrup), ListaTubos, Marcar) = 0 Then
            'respondeu sim � pergunta 1
            Resp_PergTubo = True
            Verifica_Ana_Tubo = True
            
            'Pergunta 2: Quer inserir a data de hoje na chegada do tubo ?
            
            If Flg_DtChTubo = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodTubo = Split(Marcar(i), ";")
                        InsereAutoTubo CodTubo(1), Bg_DaData_ADO, Bg_DaHora_ADO
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodTubo = Split(Marcar(i), ";")
                        InsereAutoTubo CodTubo(1), "", ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                If FormPergTubo.PerguntaTubo(ConfirmarDtChega, Flg_DtChTubo, SELECT_Descr_Ana(codAgrup), ListaTubos, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodTubo = Split(Marcar(i), ";")
                        InsereAutoTubo CodTubo(1), Bg_DaData_ADO, Bg_DaHora_ADO
                    Next i
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodTubo = Split(Marcar(i), ";")
                        InsereAutoTubo CodTubo(1), "", ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
            End If
        Else
            'respondeu n�o � pergunta 1
            Resp_PergTubo = False
        End If
        
    End If

    ' retirar do array de an�lises a marcar o codigo do tubo (fica "S1;S2;S3;S4;")
    For i = 1 To UBound(Marcar)
        CodTubo = Split(Marcar(i))
        Marcar(i) = CodTubo(0) & ";"
    Next i
     
    '----------- PERMITIR MARCAR AS ANALISES QUE NAO TEM TUBO ASSOCIADO
    
    If RsAnaTubo.RecordCount <> 0 Then RsAnaTubo.MoveFirst
    k = UBound(Marcar)
    While Not RsAnaTubo.EOF
        If BL_HandleNull(RsAnaTubo!cod_tubo, "") = "" Then
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = BL_HandleNull(RsAnaTubo!cod_ana_s, "") & ";"
            If gColocaDataChegada = "" Then gColocaDataChegada = Bg_DaData_ADO
            Verifica_Ana_Tubo = True
        End If
        RsAnaTubo.MoveNext
    Wend
    
    '----------- MARCAR AS ANALISES QUE J� TINHAM O TUBO REGISTADO
    
    k = UBound(Marcar)
    If TotJaTemTubo <> 0 Then
        For i = 1 To TotJaTemTubo
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = JaTemTubo(i) & ";"
        Next i
    End If
    
Else
    Verifica_Ana_Tubo = True
    
    'Marca todas
    If RsAnaTubo.RecordCount <> 0 Then RsAnaTubo.MoveFirst
    k = 0
    ReDim Marcar(0)
    If Not RsAnaTubo.EOF Then
        If gColocaDataChegada = "" And EcDataChegada.Text <> "" Then    'sdo em vez de: If gColocaDataChegada = "" Then gColocaDataChegada = BG_DaData_ADO
            gColocaDataChegada = EcDataChegada.Text                     'sdo
        ElseIf EcDataChegada.Text = "" Then                             'sdo
            gColocaDataChegada = ""                                     'sdo
        End If                                                          'sdo
    End If
    While Not RsAnaTubo.EOF
        k = k + 1
        ReDim Preserve Marcar(k)
        Marcar(k) = BL_HandleNull(RsAnaTubo!cod_ana_s, "") & ";"
        RsAnaTubo.MoveNext
    Wend

End If

RsAnaTubo.Close
Set RsAnaTubo = Nothing

End Function

Private Sub BtAgenda_Click()

    If gModuloAgendaV2 = mediSim Then
        FormGesReqCons.Enabled = False
        Set gFormActivo = FormAgendaV2
        
        FormAgendaV2.Show
        If EcNumReq.Text <> "" Then
            FormAgendaV2.InicializaRequisicao EcNumReq.Text, "C"
        End If
    Else
        ' Guardar as propriedades dos campos do form
        For Ind = 0 To Max
            ReDim Preserve FOPropertiesTemp(Ind)
            FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
            FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
            FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
            FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
            FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
            FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
        Next Ind
                    
        FormGesReqCons.Enabled = False
        Set gFormActivo = FormAgenda
        FormAgenda.Show
        If (EcDtMarcacao.Text = Empty) Then
            Call FormAgenda.PreencheAgenda(Bg_DaData_ADO, "1")
        Else
            Call FormAgenda.PreencheAgenda(EcDtMarcacao.Text, "1")
        End If
    End If
End Sub


Private Sub BtAnula_Click()
    
    FrListaRecibos.caption = "Anular Recibos"
    BtCanc.caption = "&Cancelar"
    BtOk.caption = "&Anular"
    FrListaRecibos.Visible = True
    CbTipoAnulacao.Visible = True
    CbTipoAnulacao.ListIndex = -1
    
    Call PreencheListaRecibos
    FuncaoListaRecibos = "ANULAR"

End Sub

Private Sub BtCanc_Click()
    
    ListaRecibos.Clear
    FrListaRecibos.Visible = False
    CbTipoAnulacao.Visible = False
    CbTipoAnulacao.ListIndex = -1
    FuncaoListaRecibos = ""

End Sub

Private Sub BtCancelarReq_Click()

    Max = UBound(gFieldObjectProperties)
    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    
    MarcaLocal = rs.Bookmark
    FormGesReqCons.Enabled = False
    FormCancelarRequisicoes.Show
    
End Sub

Private Sub BtDadosAnteriores_Click()

    Dim EArs As Boolean
    Dim i As Integer

    ' N�o deixa passar por cima de uma req. existente.
    If (Len(Trim(EcNumReq.Text)) > 0) Then
        MsgBox "Opera��o n�o permitida!" & vbCrLf & _
               "Deve limpar os dados do ecran.            ", vbExclamation, "Copiar anterior"
        Exit Sub
    End If
    
    If Not (rs Is Nothing) Then
        If rs.state = adStateOpen Then
            If rs.EOF = True And rs.BOF = True Then
                'ficou com um registo vazio ??
                rs.Close
            Else
                BG_Mensagem mediMsgStatus, "Tem que limpar os dados do ecran primeiro!", , "Copiar anterior"
                Exit Sub
            End If
        End If
    End If

Dim TipoUtente As Long
Dim NumUtente As String

TipoUtente = CbTipoUtente.ListIndex
NumUtente = EcUtente.Text

LimpaCampos

Flg_DadosAnteriores = True

EcNumReq = gRequisicaoActiva

Call FuncaoProcurar

rs.Close
Set rs = Nothing

EcDataPrevista.Enabled = True
EcDataChegada.Enabled = True
BtCancelarReq.Visible = False
EcNumReq.Enabled = True
EcNumReq.Locked = False
EcEpisodio.Enabled = True
CbTipoUtente.Enabled = True
EcUtente.Enabled = True
EcDataPrevista.Enabled = True
gFlg_JaExisteReq = False
Flg_LimpaCampos = True
ListaRecibos.Clear
FrListaRecibos.Visible = False
CbTipoAnulacao.Visible = False
CbTipoAnulacao.ListIndex = -1
BtAnula.Enabled = False
BtReimpr.Enabled = False

FuncaoListaRecibos = ""
EcNumReq = ""
CbEstadoReq.ListIndex = -1
CbEstadoReqAux.ListIndex = -1
EcEstadoReq = ""
If gCopiarAnteriorUtente = 1 Then                 'sdo
    EcCodArquivo.Text = ""
    EcDescrArquivo.Text = ""
    EcReqAux.Text = ""
    EcFicha.Text = ""
    FGAna.Clear
    FGAna.rows = 2
    FgProd.Clear
    FgProd.rows = 2
    FGTubos.Clear
    FGTubos.rows = 2
    EcDataFact.Text = ""
    EcObsProveniencia.Text = ""
    EcDataChegada.Text = ""
    EcCodMedico.Text = ""
    EcDescrMedico.Text = ""
    EcCodMedico2.Text = ""
    EcDescrMedico2.Text = ""
    ReDim MaReq(0)
    TAna.Nodes.Clear
Else
EcDataPrevista = ""
EcReqAssociada = ""
EcDataPrevista = Bg_DaData_ADO                     '**********************
CbTipoUtente.ListIndex = TipoUtente                '**********************
EcUtente.Text = NumUtente
EcUtente_Validate False
End If
EcDataAlteracao = ""
EcDataCriacao = ""
EcDataImpressao = ""
EcDataImpressao2 = ""
EcUtilizadorAlteracao = ""
EcUtilizadorCriacao = ""
EcUtilizadorImpressao = ""
EcUtilizadorImpressao2 = ""
LbNomeUtilAlteracao.caption = ""
LbNomeUtilCriacao.caption = ""
LbNomeUtilImpressao.caption = ""
LbNomeUtilImpressao2.caption = ""
LbNomeUtilAlteracao.ToolTipText = ""
LbNomeUtilCriacao.ToolTipText = ""
LbNomeUtilImpressao.ToolTipText = ""
LbNomeUtilImpressao2.ToolTipText = ""
EcHoraAlteracao = ""
EcHoraCriacao = ""
EcHoraImpressao = ""
EcHoraImpressao2 = ""
CbTaxaModeradora.ListIndex = mediComboValorNull
CbPagEnt.ListIndex = mediComboValorNull
CbPagUte.ListIndex = mediComboValorNull
ValorTM.Text = ""
ValorEnt.Text = ""
ValorDoe.Text = ""
DataTM.Text = ""
DataEnt.Text = ""
DataDoe.Text = ""
NumRecTM.Text = ""
NumRecEnt.Text = ""
NumRecDoe.Text = ""

If CkIsento.value <> vbChecked Then
    If gRecibos = "1" Then
        SSTGestReq.TabEnabled(3) = True
        SSTGestReq.TabVisible(3) = False
        SSTGestReq.TabEnabled(4) = True
    Else
        SSTGestReq.TabEnabled(3) = False
        SSTGestReq.TabVisible(3) = False
        SSTGestReq.TabEnabled(4) = False
    End If
End If

'EcDataPrevista = Bg_DaData_ADO     '******************
ContP1 = 1
CbTMIndex = -1
CbUteIndex = -1
CbEntIndex = -1

EArs = EFR_Verifica_ARS(EcCodEFR)

If EArs = True Then
    For i = 1 To RegistosA.Count
        If RegistosA(i).codAna <> "" Then
            RegistosA(i).NReqARS = "A gerar"
            RegistosA(i).p1 = ContP1
            FGAna.TextMatrix(i, 2) = RegistosA(i).NReqARS
            FGAna.TextMatrix(i, 3) = RegistosA(i).p1
            ContP1 = ContP1 + 1
        End If
    Next i
End If

For i = LBound(MaReq) To UBound(MaReq)
    With MaReq(i)
        .Flg_Apar_Transf = "0"
        .N_Folha_Trab = 0
    End With
Next

'CbTipoUtente.ListIndex = TipoUtente                '**********************
'EcUtente.Text = NumUtente
'EcUtente_Validate False

If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NaoCopiarEFR") = "1" Then
    EcCodEFR.Text = ""
    EcCodEFR_Validate False
End If

estado = 1
BL_ToolbarEstadoN estado

Flg_DadosAnteriores = False

EcEpisodio.Text = ""

End Sub

Private Sub BtEtiq_Click()
    If gMultiReport = 1 Then
        FormGesReqCons.Enabled = False
        FormMedicosReq.Show
    Else
        Call ImprimeEtiq
    End If
End Sub

Private Sub BtGetSONHO_Click()
    
    Me.FuncaoLimpar
    EcEpisodioSONHO.Text = ""
    
'    Me.OpConsSonho.Value = False
'    Me.OpExternoSONHO.Value = False
'    Me.OpIntSONHO.Value = False
'    Me.OpUrgSONHO.Value = False
    
    Me.MonthViewAnalisesSONHO.value = Date
    
    FrameSONHO.Enabled = True
    FrameSONHO.top = 1450
    FrameSONHO.left = 2880
    FrameSONHO.Visible = True
    EcEpisodioSONHO.SetFocus
    DoEvents

End Sub

Private Sub BtGetSONHO_OK_Click()

    Dim rv As Integer
    Dim situacao As String
    
    EcEpisodioSONHO.Text = Trim(EcEpisodioSONHO.Text)

    If (Len(EcEpisodioSONHO.Text) > 0) Then
        
        FrameSONHO.Visible = False
            
        situacao = ""
        
        If (Me.OpExternoSONHO) Then
            situacao = gT_Externo
        End If
        
        If (Me.OpIntSONHO) Then
            situacao = gT_Internamento
        End If
        
        If (Me.OpConsSonho) Then
            situacao = gT_Consulta
        End If
        
        If (Me.OpUrgSONHO) Then
            situacao = gT_Urgencia
        End If
        
        rv = SONHO_Constroi_Requisicao(Me, _
                                       "", _
                                       situacao, _
                                       EcEpisodioSONHO.Text, _
                                       Format(MonthViewAnalisesSONHO.value, gFormatoData))
    
    End If

End Sub

Private Sub BtInfClinica_Click()

    'for�ar a fazer o procurar da inf. clinica para a requisi��o em causa
    'caso j� exista uma
    
    BL_PreencheDadosUtente EcSeqUtente
    FormGesReqCons.Enabled = False
    FormInformacaoClinica.Show
    If EcNumReq.Text <> "" Or EcSeqUtente.Text <> "" Then
        FormInformacaoClinica.FuncaoProcurar
    End If

End Sub

Private Sub BtNorma_Click()
    Dim continua As Boolean
    'Printer Common Dialog
    gImprimirDestino = 0
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("NormaColheita", "Listagem de Caixa - Anulados", crptToPrinter)
    Else
        continua = BL_IniciaReport("NormaColheita", "Listagem de Caixa - Anulados", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.WindowState = crptMaximized
    Report.SQLQuery = "SELECT SL_REQUIS_CONSULTAS.N_REQ, SL_MARCACOES_CONSULTAS.COD_AGRUP, SLV_ANALISES.descr_ana, SL_TUBO.cod_tubo , SL_TUBO.norma_colheita "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_requis_consultas , sl_marcacoes_consultas, slv_analises, sl_tubo"
    Report.SQLQuery = Report.SQLQuery & " WHERE sl_requis_consultas.n_req = sl_marcacoes_consultas.n_req "
    Report.SQLQuery = Report.SQLQuery & " AND sl_marcacoes_consultas.cod_agrup = slv_analises.cod_ana "
    Report.SQLQuery = Report.SQLQuery & " AND sl_tubo.cod_tubo = slv_analises.cod_tubo "
    Report.SQLQuery = Report.SQLQuery & " AND sl_requis_consultas.n_req = " & EcNumReq & "  ORDER by sl_tubo.cod_tubo asc"
    'F�rmulas do Report
    Report.formulas(0) = "NumReq=" & BL_TrataStringParaBD("" & EcNumReq)
    Report.formulas(1) = "NomeUtente=" & BL_TrataStringParaBD("" & EcNome)
    Report.formulas(2) = "DtNascUtente=" & BL_TrataStringParaBD("" & EcDataNasc)
    Report.formulas(3) = "DtNascUtente=" & BL_TrataStringParaBD("" & EcDataNasc)
    Report.formulas(4) = "DtChegada=" & BL_TrataStringParaBD("" & EcDataChegada)
    Report.formulas(6) = "Processo=" & BL_TrataStringParaBD("" & EcUtente)
    Report.formulas(7) = "DtPrevista=" & BL_TrataStringParaBD("" & EcDataPrevista)
    Report.formulas(8) = "NSC=" & BL_TrataStringParaBD("" & EcProcHosp1)
    Report.formulas(9) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao)
    Report.formulas(10) = "Servico=" & BL_TrataStringParaBD("" & EcDescrProveniencia)
    Report.formulas(11) = "SeqUtente=" & BL_TrataStringParaBD("" & EcSeqUtente)
    
    
    Call BL_ExecutaReport

End Sub

Private Sub BtOk_Click()

Dim Recibos() As Variant
Dim TotRec As Integer
Dim i As Integer

TotRec = 0
For i = 0 To ListaRecibos.ListCount - 1
    If ListaRecibos.Selected(i) = True Then
        TotRec = TotRec + 1
        ReDim Preserve Recibos(TotRec)
        Recibos(TotRec) = ListaRecibos.List(i)
    End If
Next i

If FuncaoListaRecibos = "REIMPRIME" Then
ElseIf FuncaoListaRecibos = "ANULAR" Then
End If

If TotRec <> 0 Then
    ListaRecibos.Clear
    FrListaRecibos.Visible = False
    CbTipoAnulacao.Visible = False
    CbTipoAnulacao.ListIndex = -1
End If

End Sub

Private Sub BtPassaMarcacao_Click()

    On Error GoTo ErrorHandler
    Dim cod_destino As Integer
    Dim rv As Boolean
    Dim isento As Boolean
    Dim i As Integer
    Dim aux As String
    Dim data_chegada As String
    Dim cod_prioridade As Integer
    
    ' Controlar melhor.
    If (Len(Trim(EcUtente.Text)) = 0) Or _
       (Len(Trim(EcNumReq.Text)) = 0) Then
        MsgBox "Deve selecionar uma Marca��o.    ", vbExclamation, Me.caption
        Exit Sub
    End If
    
    If CkFlgInvisivel.value = vbChecked Then
        MsgBox "Marca��o cancelada ou j� efetivada", vbExclamation, Me.caption
        Exit Sub
    End If
    
    If EcDtMarcacao.Text = "" Then
        MsgBox "Para efetivar ter� que indicar data prevista da colheita.", vbExclamation, Me.caption
        Exit Sub
    End If
    FormGesReqCons.Enabled = False
    gFormGesReqCons_Aberto = True
    
    FormGestaoRequisicao.Show
    FormGestaoRequisicao.reqMarca = EcNumReq
    If gLAB = "CHVNG" Then
        data_chegada = ""
    Else
        data_chegada = Bg_DaData_ADO
    End If
    ' Simula a introdu��o dos dados da requisi��o actual em FormGestaoRequisicao.
    
    ' Coloca o Utente.
    rv = SIM_Coloca_Utente(FormGestaoRequisicao, _
                           FormGesReqCons.CbTipoUtente.List(FormGesReqCons.CbTipoUtente.ListIndex), _
                           EcUtente.Text)
    
    If rv <> True Then
        Exit Sub
    End If
    
    If (CkIsento.value = 1) Then
        isento = True
    Else
        isento = False
    End If
    If CbDestino.ListIndex = mediComboValorNull Then
        cod_destino = mediComboValorNull
    Else
        cod_destino = CbDestino.ItemData(CbDestino.ListIndex)
    End If
    If CbPrioColheita.ListIndex = mediComboValorNull Then
        cod_prioridade = mediComboValorNull
    Else
        cod_prioridade = CbPrioColheita.ItemData(CbPrioColheita.ListIndex)
    End If
    
    ' Coloca os outros dados da requisi��o.
     rv = SIM_Coloca_Dados_Requisicao(FormGestaoRequisicao, _
                                      EcDtMarcacao.Text, _
                                     FormGesReqCons.CbSituacao.List(FormGesReqCons.CbSituacao.ListIndex), _
                                     FormGesReqCons.CbUrgencia.List(FormGesReqCons.CbUrgencia.ListIndex), _
                                     EcCodProveniencia.Text, _
                                     EcCodEFR.Text, _
                                     EcCodMedico.Text, _
                                     isento, _
                                     FormGesReqCons.CbTipoIsencao.List(FormGesReqCons.CbTipoIsencao.ListIndex), _
                                     EcObsReq.Text, _
                                     EcObsProveniencia.Text, _
                                     EcNumBenef.Text, _
                                     BL_HandleNull(EcReqAssociada.Text, EcNumReq.Text), EcEpisodio.Text, EcUtilizadorCriacao, EcDataCriacao, EcHoraCriacao, _
                                     data_chegada, EcPrescricao, cod_destino, EcCodValencia, EcInfComplementar, cod_prioridade)
    If rv <> True Then
        Exit Sub
    End If
    
    
    FormGestaoRequisicao.SSTGestReq.TabEnabled(1) = True
    FormGestaoRequisicao.SSTGestReq.TabEnabled(5) = True
    
    ' Insere as analises.
    
    FormGestaoRequisicao.SSTGestReq.TabEnabled(2) = True
    FormGestaoRequisicao.SSTGestReq.Tab = 2
    
    For i = 1 To FormGesReqCons.FGAna.rows - 1
        aux = FormGesReqCons.FGAna.TextMatrix(i, 0)
        aux = Trim(aux)
        If (Len(aux) > 0) Then
            rv = SIM_Coloca_Analise(FormGestaoRequisicao, aux)
            If rv <> True Then
                Exit Sub
            End If
        End If
    Next
    
    
    
    
    ' Coloca a data de hoje por defeito na data de chegada.
    'FormGestaoRequisicao.EcDataChegada.Text = Format(Date, gFormatoData)
    
    FormGestaoRequisicao.SSTGestReq.Tab = 0
    ' ------------------------------------------------------
    If gUsaFilaEspera = mediSim Then
        InsertFilaEspera
    End If

    ' Limpar o form ?
    ' ------------------------------------------------------
    
    '*******************FN
    FormGestaoRequisicao.FuncaoInserir (True)
    Flg_Executa_BtPassaMarcacao = False

    '************************
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("BtPassaMarcacao_Click (FormGesReqCons) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub

Private Sub BtPesquisaAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Button = 1 Then
        If RegistosA(FGAna.row).estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = 0 Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            'MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAREQ
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAMARC
        End If
    End If

End Sub

Private Sub BtPesquisaArquivo_Click()
    FormPesquisaRapida.InitPesquisaRapida "sl_arquivos", _
                        "cod_arquivo", "seq_arquivo", _
                        EcPesqRapArquivo
End Sub

Private Sub BtPesquisaEntFin_Click()
    
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub BtPesquisaEspecif_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "sl_especif.descr_especif"
    CamposEcran(1) = "sl_especif.descr_especif"
    Tamanhos(1) = 3000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "sl_prod_esp.cod_especif"
    CamposEcran(2) = "sl_prod_esp.cod_especif"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = " sl_prod_esp,sl_especif "
    CWhere = " sl_prod_esp.cod_produto='" & UCase(FgProd.TextMatrix(FgProd.row, 0)) & "' AND sl_prod_esp.cod_especif=sl_especif.cod_especif"
    CampoPesquisa = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Especifica��es")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FgProd.TextMatrix(FgProd.row, 3) = resultados(1)
            FgProd.TextMatrix(FgProd.row, 2) = resultados(2)
            FgProd.row = 4
        End If
    Else
        Set CamposRetorno = Nothing
        Set CamposRetorno = New ClassPesqResultados
    
        PesqRapida = False

        ChavesPesq(1) = "descr_especif"
        CamposEcran(1) = "descr_especif"
        Tamanhos(1) = 3000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_especif"
        CamposEcran(2) = "cod_especif"
        Tamanhos(2) = 1000
        Headers(2) = "C�digo"
        
        CamposRetorno.InicializaResultados 2
        
        CFrom = " sl_especif "
        CWhere = ""
        CampoPesquisa = "descr_especif"
        
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Especifica��es")
        
        If PesqRapida = True Then
            FormPesqRapidaAvancada.Show vbModal
            CamposRetorno.RetornaResultados resultados, CancelouPesquisa
            If Not CancelouPesquisa Then
                FgProd.TextMatrix(FgProd.row, 3) = resultados(1)
                FgProd.TextMatrix(FgProd.row, 2) = resultados(2)
                RegistosP(LastRowP).CodEspecif = UCase(resultados(2))
                FgProd.Col = 4
            End If
        Else
            BG_Mensagem mediMsgBox, "N�o existem especifica��es codificadas!", vbExclamation, "Especifica��es"
            FgProd.Col = 2
        End If
    End If
    FgProd.SetFocus
    
End Sub

Private Sub BtPesquisaMedico_Click()

'    Select Case gLAB
'        Case gLAB = "CITO", "GM"
'            FormPesquisaRapida.InitPesquisaRapida "sl_responsaveis", _
'                     "titulo", "codigo", _
'                     EcPesqRapMedico
'        Case Else
'            FormPesquisaRapida.InitPesquisaRapida "sl_medicos", _
'                        "nome_med", "cod_med", _
'                        EcPesqRapMedico
'    End Select

'ESOUSA 12.10.2016 - ULSNE-1324
  Dim ChavesPesq(1 To 3) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 3) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 3) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 3) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 3) As Variant

On Error GoTo TrataErro
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    
    ChavesPesq(3) = "ext_telef"
    CamposEcran(3) = "ext_telef"
    Tamanhos(3) = 0
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    Headers(3) = "Extens�o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 3

    'Query
    ClausulaFrom = "sl_medicos"
    CampoPesquisa1 = "nome_med"
    ClausulaWhere = " (flg_invisivel is null or flg_invisivel = 0) "
    
    Select Case gLAB
        Case gLAB = "CITO", "GM"
            FormPesquisaRapida.InitPesquisaRapida "sl_responsaveis", _
                     "titulo", "codigo", _
                     EcPesqRapMedico
        Case Else
            
            PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar M�dicos")
    
            mensagem = "N�o foi encontrado nenhum M�dico."
    
            If PesqRapida = True Then
                    FormPesqRapidaAvancada.Show vbModal
                    CamposRetorno.RetornaResultados resultados, CancelouPesquisa
                If Not CancelouPesquisa Then
                    EcCodMedico.Text = resultados(1)
                    EcDescrMedico.Text = resultados(2)
                    EcObsProveniencia.Text = resultados(3)
                End If
            Else
                BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
            End If

            Exit Sub
                        
    End Select
    
TrataErro:
    BG_LogFile_Erros "BtPesquisaMedico_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaMedico_Click"
    Exit Sub
    Resume Next
'

End Sub

Private Sub BtPesquisaMedico2_Click()
    FormPesquisaRapida.InitPesquisaRapida "sl_responsaveis", _
                        "titulo", "codigo", _
                        EcPesqRapMedico2
End Sub
Private Sub BtPesquisaProduto_click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
                        "descr_produto", "seq_produto", _
                         EcPesqRapProduto
End Sub

Private Sub BtPesquisaTubos_click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_tubo", _
                        "descr_tubo", "seq_tubo", _
                         EcPesqRapTubo
End Sub

Private Sub BtPesquisaProveniencia_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    ClausulaWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) "

        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub


Private Sub BtPesquisaUtente_Click()
    MsgBox ("Op��o n�o disponivel")
    'Flg_PesqUtente = True
    'FormPesquisaUtentes.Show
    'FormGesReqCons.Enabled = False

End Sub

Sub Funcao_CopiaUte()
    
    If Trim(gDUtente.seq_utente) <> "" Then
        EcSeqUtente = CLng(gDUtente.seq_utente)
        PreencheDadosUtente
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

End Sub

Sub Actualiza_Estrutura_MAReq(inicio As Integer)

    Dim j As Long

    If inicio <> UBound(MaReq) Then
        For j = inicio To UBound(MaReq) - 1
            MaReq(j).Cod_Perfil = MaReq(j + 1).Cod_Perfil
            MaReq(j).Descr_Perfil = MaReq(j + 1).Descr_Perfil
            MaReq(j).cod_ana_c = MaReq(j + 1).cod_ana_c
            MaReq(j).Descr_Ana_C = MaReq(j + 1).Descr_Ana_C
            MaReq(j).cod_ana_s = MaReq(j + 1).cod_ana_s
            MaReq(j).descr_ana_s = MaReq(j + 1).descr_ana_s
            MaReq(j).Ord_Ana = MaReq(j + 1).Ord_Ana
            MaReq(j).Ord_Ana_Compl = MaReq(j + 1).Ord_Ana_Compl
            MaReq(j).Ord_Ana_Perf = MaReq(j + 1).Ord_Ana_Perf
            MaReq(j).cod_agrup = MaReq(j + 1).cod_agrup
            MaReq(j).N_Folha_Trab = MaReq(j + 1).N_Folha_Trab
            MaReq(j).dt_chega = MaReq(j + 1).dt_chega
            MaReq(j).Flg_Apar_Transf = MaReq(j + 1).Flg_Apar_Transf
            MaReq(j).flg_estado = MaReq(j + 1).flg_estado
        Next
    End If
    
    ReDim Preserve MaReq(UBound(MaReq) - 1)

End Sub

Sub Insere_aMeio_RegistosA(inicio As Integer)

    Dim j As Long

    CriaNovaClasse RegistosA, -1, "ANA"
    
    If inicio <> RegistosA.Count Then
        For j = RegistosA.Count To inicio + 1 Step -1
            RegistosA(j).codAna = RegistosA(j - 1).codAna
            RegistosA(j).descrAna = RegistosA(j - 1).descrAna
            RegistosA(j).NReqARS = RegistosA(j - 1).NReqARS
            RegistosA(j).p1 = RegistosA(j - 1).p1
            RegistosA(j).estado = RegistosA(j - 1).estado
            RegistosA(j).DtChega = RegistosA(j - 1).DtChega
        Next
    End If

    RegistosA(inicio).codAna = ""
    RegistosA(inicio).descrAna = ""
    RegistosA(inicio).NReqARS = ""
    RegistosA(inicio).p1 = ""
    RegistosA(inicio).estado = ""
    RegistosA(inicio).DtChega = ""
    
End Sub

Function Calcula_Valor(valor As Double, TaxaConv) As Double
    
    Calcula_Valor = Round(valor / TaxaConv)

End Function

Private Sub BtPesquisaValencia_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_valencia"
    CamposEcran(1) = "cod_valencia"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_valencia"
    CamposEcran(2) = "descr_valencia"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_valencia"
    CampoPesquisa1 = "descr_valencia"
    ClausulaWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) "
    If gCodLocal <> -1 Then
        ClausulaWhere = ClausulaWhere & " AND  cod_valencia IN (SELECT x.cod_valencia FROM sl_ass_valencia_locais x WHERE x.cod_local = " & CStr(gCodLocal) & ") "
    End If
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Val�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Val�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodValencia.Text = resultados(1)
            EcDescrValencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub



Private Sub BtReimpr_Click()
    
    FrListaRecibos.caption = "Reimprimir Recibos"
    BtCanc.caption = "&Cancelar"
    BtOk.caption = "&Reimprimir"
    FrListaRecibos.Visible = True
    
    Call PreencheListaRecibos
    FuncaoListaRecibos = "REIMPRIME"

End Sub

Private Sub CbPagEnt_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbPagEnt, KeyCode

End Sub

Private Sub CbPagUte_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbPagUte, KeyCode

End Sub

' PFerreira 08.03.2007
Private Sub CbPrioColheita_Click()
    On Error GoTo TrataErro
    If (CbPrioColheita.ListIndex >= 0) Then: EcPrColheita.Text = CbPrioColheita.ItemData(CbPrioColheita.ListIndex)
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbPrioColheita_Click: " & Err.Number & " - " & Err.Description, Me.Name, "CbPrioColheita_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub CbSituacao_Change()

    MsgBox "Change !", vbExclamation, "Debug"

End Sub

Private Sub CbSituacao_Scroll()

    MsgBox "Change !", vbExclamation, "Debug"

End Sub

Private Sub CbSituacao_Click()
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
'    'verifica se a situa��o e a proveniencia est�o preenchidas, se sim, verifica se esta � poss�vel
'    If CbSituacao.ListIndex <> -1 And EcCodProveniencia.Text <> "" Then
'        Set Tabela = New ADODB.Recordset
'        Sql = "SELECT t_sit FROM sl_proven WHERE cod_proven ='" & Trim(EcCodProveniencia.Text) & "'"
'        Tabela.CursorType = adOpenStatic
'        Tabela.CursorLocation = adUseClient
'        Tabela.Open Sql, gConexao
'        If Tabela.RecordCount > 0 Then
'            If CbSituacao.ListIndex <> Tabela!t_sit Then
'                BG_Mensagem mediMsgBox, "A proveni�nia n�o � poss�vel para a situa��o seleccionada", vbInformation, "Aten��o"
'            End If
'        End If
'        Tabela.Close
'        Set Tabela = Nothing
'    End If

End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbSituacao, KeyCode

End Sub

Private Sub CbTaxaModeradora_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTaxaModeradora, KeyCode

End Sub

Private Sub CbTipoAnulacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoAnulacao, KeyCode

End Sub

Private Sub CbTipoIsencao_Click()
    
    BL_ColocaComboTexto "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao

End Sub

Private Sub CbTipoIsencao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoIsencao, KeyCode

End Sub

Private Sub CbTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoUtente, KeyCode

End Sub

Public Sub CbTipoUtente_Validate(Cancel As Boolean)
    
    Call EcUtente_Validate(Cancel)
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUrgencia, KeyCode

End Sub

Private Sub CkIsento_Click()

    If CkIsento.value = vbUnchecked Then
        CbTipoIsencao.ListIndex = mediComboValorNull
        EcCodIsencao = ""
        CbTipoIsencao.Enabled = False
        If gRecibos = "1" Then
            ReDim Recibo(TotalListaRecibos)
            TotalListaRecibos = 0
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = True
        Else
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = True
        End If
    ElseIf CkIsento.value = vbChecked Then
        SSTGestReq.TabEnabled(3) = False
        SSTGestReq.TabVisible(3) = False
        SSTGestReq.TabEnabled(4) = False
        CbTipoIsencao.Enabled = True
        CbTipoIsencao.SetFocus
        If gF_IDENTIF = 1 Then
            If Trim(FormIdentificaUtente.EcDescrIsencao.Text) <> "" Then
                CbTipoIsencao.Text = FormIdentificaUtente.EcDescrIsencao.Text
            End If
        End If
        BG_MostraComboSel gCodIsencaoDefeito, CbTipoIsencao
    End If

End Sub

Private Sub cmdOK_Click()
    If gLAB = "HFM" Then
        Flg_Executa_BtPassaMarcacao = True
    Else
        Flg_Executa_BtPassaMarcacao = False
    End If
    FuncaoProcurar

End Sub

Private Sub Command3_Click()

    FrameSONHO.Visible = False
    FrameSONHO.Enabled = False

End Sub




Private Sub EcAnaEfr_GotFocus()
        cmdOK.Default = False
End Sub

Private Sub EcAnaEfr_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If Len(EcAnaEfr) >= 13 Then
            EcCredAct = EcAnaEfr
        ElseIf (Len(EcAnaEfr)) >= 1 Then
            FGAna.row = FGAna.rows - 1
            LastRowA = FGAna.row
            MarcacaoCodigoBarrasP1 EcAnaEfr
        End If
        EcAnaEfr = ""
        EcAnaEfr.SetFocus
    End If
End Sub

Private Sub EcCodArquivo_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodArquivo.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_arquivo FROM sl_arquivos WHERE cod_arquivo='" & Trim(EcCodArquivo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodArquivo.Text = ""
            EcDescrArquivo.Text = ""
        Else
            EcDescrArquivo.Text = Tabela!descr_arquivo
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrArquivo.Text = ""
    End If

End Sub

Private Sub EcAuxAna_Change()
    
    'If Len(EcAuxAna) = 5 And ExecutaCodigoA = True Then Call EcAuxAna_KeyDown(13, 0)

End Sub

Private Sub EcAuxAna_GotFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If EcAuxAna.SelLength = 0 Then
        Sendkeys ("{END}")
    End If

End Sub

Private Sub EcAuxAna_LostFocus()

    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    EcAuxAna.Visible = False
    If FGAna.Enabled = True Then FGAna.SetFocus
    
End Sub

Private Sub EcAuxAna_KeyPress(KeyAscii As Integer)
    
    If Enter = True Then KeyAscii = 0

End Sub

Public Sub EcAuxAna_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim i As Integer
    Dim aux As String
    Dim an_aux As String
    Dim rv As Integer
            
    ' For�a o formato "0".
    aux = Trim(EcAuxAna.Text)
    If (IsNumeric(aux)) Then
        If (CLng(aux) = 0) Then
            EcAuxAna.Text = "0"
        End If
    End If
    
    Enter = False
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxAna.Text) = "") Then
        Enter = True
        EcAuxAna.Visible = False
        FGAna.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        
        ' -------------------------------------------------------
        
        If (gMapeiaSONHO) Then
        
            ' Mapeia do SONHO para o SISLAB.
        
            rv = SONHO_Mapeia_Analise_para_SISLAB(UCase(Trim(EcAuxAna.Text)), an_aux)
            EcAuxAna.Text = an_aux
            DoEvents
            
        End If
            
        ' -------------------------------------------------------
        
        Select Case LastColA
            Case 0 ' Codigo da An�lise
            
                If (Verifica_Ana_ja_Existe(LastRowA, EcAuxAna) = False) Then
                
                    If (Trim(UCase(EcAuxAna)) <> Trim(UCase(RegistosA(LastRowA).codAna))) Then
                        
                        If (Insere_Nova_Analise(LastRowA, EcAuxAna) = True) Then
                            
                            'o estado deixa de estar "Sem Analises"
                            If Trim(EcEstadoReq) = "" Then
                                CbEstadoReqAux.ListIndex = 1
                                EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
                                CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
                            End If

                            If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                                'Cria linha vazia
                                FGAna.AddItem ""
                                CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                            End If
                        End If
                    End If
                Else
                    Beep
                    BG_Mensagem mediMsgStatus, "An�lise j� indicada!"
                End If
                FGAna.Col = 0
            
            Case 2 ' Req. ARS
                
                If FGAna.TextMatrix(LastRowA, 2) <> EcAuxAna Then
                
                    If LastRowA <> 1 Then
                        If FGAna.TextMatrix(LastRowA - 1, 2) <> EcAuxAna Then
                            ContP1 = 1
                        Else
                            ContP1 = FGAna.TextMatrix(LastRowA - 1, 3) + 1
                        End If
                    Else
                        ContP1 = 1
                    End If
                    RegistosA(LastRowA).NReqARS = EcAuxAna
                    RegistosA(LastRowA).p1 = ContP1
                    FGAna.TextMatrix(LastRowA, 2) = EcAuxAna
                    FGAna.TextMatrix(LastRowA, 3) = ContP1
                    ContP1 = ContP1 + 1
                    
                    If LastRowA < FGAna.rows - 2 And FGAna.TextMatrix(LastRowA - 1, 2) <> EcAuxAna Then
                        If BG_Mensagem(mediMsgBox, "Deseja actualizar at� ao fim da lista ?", vbYesNo + vbQuestion, "N�mero de requisi��o da ARS") = vbYes Then
                            
                            For i = LastRowA + 1 To FGAna.rows - 2
                                RegistosA(i).NReqARS = EcAuxAna
                                RegistosA(i).p1 = ContP1
                                FGAna.TextMatrix(i, 2) = EcAuxAna
                                FGAna.TextMatrix(i, 3) = ContP1
                                ContP1 = ContP1 + 1
                            Next i
                        
                        End If
                    End If
                
                End If
                
                If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                    'Cria linha vazia
                    FGAna.AddItem ""
                    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                End If
                FGAna.Col = 2
        End Select
        
        EcAuxAna.Visible = False
        If FGAna.row <= FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
            FGAna.row = FGAna.row + 1
        End If
        
        FormGesReqCons.Enabled = True
    
        FGAna.SetFocus
    
    End If
    
End Sub

Private Sub EcAuxProd_GotFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If LastColP = 4 Then
        EcAuxProd.Tag = adDate
    Else
        EcAuxProd.Tag = ""
    End If
    
    If EcAuxProd.SelLength = 0 Then
        Sendkeys ("{END}")
    End If

End Sub
Private Sub EcAuxTubo_GotFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If LastColT = 2 Then
        EcAuxTubo.Tag = adDate
    Else
        EcAuxTubo.Tag = ""
    End If
    
    If EcAuxTubo.SelLength = 0 Then
        Sendkeys ("{END}")
    End If

End Sub

Public Sub EcAuxProd_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Enter = False
    
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxProd.Text) = "") Then
        Enter = True
        EcAuxProd.Visible = False
        If FgProd.Enabled = True Then FgProd.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        Select Case LastColP
            Case 0 ' Codigo do produto
                If Verifica_Prod_ja_Existe(LastRowP, EcAuxProd.Text) = False Then
                    If SELECT_Descr_Prod = True Then
                        If Trim(EcDataPrevista.Text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 4) = Trim(EcDataPrevista.Text)
                            RegistosP(LastRowP).DtPrev = Trim(EcDataPrevista.Text)
                        End If
                        If Trim(EcDataChegada.Text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 5) = Trim(EcDataChegada.Text)
                            RegistosP(LastRowP).DtChega = Trim(EcDataChegada.Text)
                        End If

                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 6) = RegistosP(LastRowP).EstadoProd
                        
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.Text)
                        RegistosP(LastRowP).CodProd = UCase(EcAuxProd.Text)
                        EcAuxProd.Visible = False
                        If FgProd.row = FgProd.rows - 1 Then
                            'Cria linha vazia
                            FgProd.AddItem ""
                            CriaNovaClasse RegistosP, FgProd.row + 1, "PROD"
                            FgProd.row = FgProd.row + 1
                            FgProd.Col = 0
                        Else
                            FgProd.Col = 2
                        End If
                    End If
                Else
                    Beep
                    BG_Mensagem mediMsgStatus, "Produto j� indicado!"
                End If
            Case 2 ' Codigo da especificacao
                If SELECT_Especif = True Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.Text)
                    RegistosP(LastRowP).CodEspecif = UCase(EcAuxProd.Text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 4
                End If
            Case 4 ' Data Prevista
                If Trim(EcAuxProd.Text) <> "" Then
                    If BG_ValidaTipoCampo_ADO(Me, EcAuxProd) = True Then
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.Text)
                        RegistosP(LastRowP).DtPrev = UCase(EcAuxProd.Text)
                        EcAuxProd.Visible = False
                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 6) = RegistosP(LastRowP).EstadoProd
                        FgProd.Col = 0
                    Else
                        Beep
                        BG_Mensagem mediMsgStatus, "Data inv�lida!"
                    End If
                    
                End If
        End Select
        If FgProd.Enabled = True Then FgProd.SetFocus
    End If
    
End Sub
Public Sub EcAuxTubo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Enter = False
    
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxTubo.Text) = "") Then
        Enter = True
        EcAuxTubo.Visible = False
        If FGTubos.Enabled = True Then FGTubos.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        Select Case LastColT
            Case 0 ' Codigo do tubo
                If Verifica_Tubo_ja_Existe(LastRowT, EcAuxTubo.Text) = False Then
                    If SELECT_Descr_Tubo = True Then
                        If Trim(EcDataPrevista.Text) <> "" Then
                            FGTubos.TextMatrix(LastRowT, 2) = Trim(EcDataPrevista.Text)
                            RegistosT(LastRowT).DtPrev = Trim(EcDataPrevista.Text)
                        End If
                        If Trim(EcDataChegada.Text) <> "" Then
                            FGTubos.TextMatrix(LastRowT, 3) = Trim(EcDataChegada.Text)
                            RegistosT(LastRowT).DtChega = Trim(EcDataChegada.Text)
                            FGTubos.TextMatrix(LastRowT, 4) = Trim(Format(Bg_DaHora_ADO, "HH:mm"))
                            RegistosT(LastRowT).HrChega = FGTubos.TextMatrix(LastRowT, 4)
                        End If

                        RegistosT(LastRowT).EstadoTubo = Coloca_Estado(RegistosT(LastRowT).DtPrev, RegistosT(LastRowT).DtChega)
                        FGTubos.TextMatrix(LastRowT, 5) = RegistosT(LastRowT).EstadoTubo
                        
                        FGTubos.TextMatrix(LastRowT, LastColT) = UCase(EcAuxTubo.Text)
                        RegistosT(LastRowT).CodTubo = UCase(EcAuxTubo.Text)
                        EcAuxTubo.Visible = False
                        If FGTubos.row = FGTubos.rows - 1 Then
                            'Cria linha vazia
                            FGTubos.AddItem ""
                            CriaNovaClasse RegistosT, FGTubos.row + 1, "TUBO"
                            FGTubos.row = FGTubos.row + 1
                            FGTubos.Col = 0
                        Else
                            FGTubos.Col = 2
                        End If
                    End If
                Else
                    Beep
                    BG_Mensagem mediMsgStatus, "Tubo j� indicado!"
                End If
            Case 2 ' Data Prevista
                If Trim(EcAuxTubo.Text) <> "" Then
                    If BG_ValidaTipoCampo_ADO(Me, EcAuxTubo) = True Then
                        FGTubos.TextMatrix(LastRowT, LastColT) = UCase(EcAuxTubo.Text)
                        RegistosT(LastRowT).DtPrev = UCase(EcAuxTubo.Text)
                        EcAuxTubo.Visible = False
                        RegistosT(LastRowT).EstadoTubo = Coloca_Estado(RegistosT(LastRowT).DtPrev, RegistosT(LastRowT).DtChega)
                        FGTubos.TextMatrix(LastRowT, 6) = RegistosT(LastRowT).EstadoTubo
                        FGTubos.Col = 0
                    Else
                        Beep
                        BG_Mensagem mediMsgStatus, "Data inv�lida!"
                    End If
                    
                End If
        End Select
        If FGTubos.Enabled = True Then FGTubos.SetFocus
    End If
    
End Sub

Private Sub EcAuxProd_KeyPress(KeyAscii As Integer)
    
    If Enter = True Then KeyAscii = 0

End Sub
Private Sub EcAuxTubo_KeyPress(KeyAscii As Integer)
    
    If Enter = True Then KeyAscii = 0

End Sub
Private Sub EcAuxProd_LostFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    
    EcAuxProd.Visible = False
    FgProd.SetFocus

End Sub
Private Sub EcAuxTubo_LostFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    
    EcAuxTubo.Visible = False
    FGTubos.SetFocus

End Sub
Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub

Public Sub EcCodMedico_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodMedico.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodMedico.Text = ""
            EcDescrMedico.Text = ""
        Else
            EcDescrMedico.Text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrMedico.Text = ""
    End If
    
End Sub

Private Sub EcCodMedico2_Validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodMedico2.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico2.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodMedico2.Text = ""
            EcDescrMedico2.Text = ""
        Else
            EcDescrMedico2.Text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrMedico2.Text = ""
    End If
End Sub

Public Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    If EcCodProveniencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven, t_sit FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.Text = ""
            EcDescrProveniencia.Text = ""
        Else
            EcDescrProveniencia.Text = Tabela!descr_proven
        
            If Not (IsNull(Tabela!t_sit)) Then
                For i = 0 To CbSituacao.ListCount - 1
                    If CbSituacao.ItemData(i) = Tabela!t_sit Then
                        CbSituacao.ListIndex = i
                    End If
                Next
            End If
        
        End If
        
        
        'verifica se a situa��o est� preenchida, se sim, verifica se esta � poss�vel
'        If CbSituacao.ListIndex <> -1 Then
'            Set Tabela = New ADODB.Recordset
'            Sql = "SELECT t_sit FROM sl_proven WHERE cod_proven ='" & Trim(EcCodProveniencia.Text) & "'"
'            Tabela.CursorType = adOpenStatic
'            Tabela.CursorLocation = adUseClient
'            Tabela.Open Sql, gConexao
'            If Tabela.RecordCount > 0 Then
'                If CbSituacao.ListIndex <> Tabela!t_sit Then
'                    BG_Mensagem mediMsgBox, "A proveni�nia n�o � poss�vel para a situa��o seleccionada", vbInformation, "Aten��o"
'                End If
'            End If
'        End If
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Public Sub EcCodValencia_Validate(Cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodValencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_valencia, t_sit, t_urg FROM sl_valencia WHERE cod_valencia='" & Trim(EcCodValencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodValencia.Text = ""
            EcDescrValencia.Text = ""
        Else
            EcDescrValencia.Text = Tabela!descr_valencia
        
        
        End If
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrValencia.Text = ""
    End If
    
End Sub


Private Sub EcDataAlteracao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataAlteracao)
    
End Sub

Private Sub EcDataChegada_GotFocus()

    If Trim(EcDataChegada.Text) = "" Then
        EcDataChegada.Text = EcDataPrevista.Text
    End If
    EcDataChegada.SelStart = 0
    EcDataChegada.SelLength = Len(EcDataChegada)
    
End Sub

Private Sub EcDataChegada_Validate(Cancel As Boolean)
    
    If EcDataChegada.Text <> "" Then
        Cancel = Not ValidaDataChegada
    End If
    
End Sub

Private Sub EcDataCriacao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataCriacao)
    
End Sub

Private Sub EcDataImpressao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao)
    
End Sub

Private Sub EcDataImpressao2_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao2)
    
End Sub
Private Sub EcDataFact_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataFact)
    
End Sub

Public Sub EcDataPrevista_GotFocus()
    
    'Para evitar que a nova entidade seja reposta pela codificada
    ' no utente
    If gPreencheEFRUtente = 1 Then
        If (Trim(EcCodEFR.Text) = "") Then
            Call PreencheEFRUtente
        End If
    ElseIf gPreencheEFREpisodioGH = 1 Then
        If (Trim(EcCodEFR.Text) = "") And EcEpisodio.Text <> "" And CbSituacao.ListIndex <> mediComboValorNull Then
            Call PreencheEFREpisodioGH(EcEpisodio.Text, CbSituacao.ListIndex)
        End If
    End If

    Call ValidaDataPrevista

    EcDataPrevista.SelStart = 0
    EcDataPrevista.SelLength = Len(EcDataPrevista)
    
End Sub

Sub Preenche_Data_Prev_Prod()
    
    Dim i As Integer
    
    ExecutaCodigoP = False
    For i = 1 To RegistosP.Count - 1
        If RegistosP(i).CodProd <> "" Then
            If Trim(RegistosP(i).DtPrev) = "" Then
                RegistosP(i).DtPrev = EcDataPrevista.Text
                FgProd.TextMatrix(i, 4) = EcDataPrevista.Text
                RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
                FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
            End If
        If RegistosP(i).EstadoProd = "" Then
            RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
            FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
        End If
        End If
    Next i
    
    ExecutaCodigoP = True

End Sub

Function ValidaDataChegada() As Boolean

    ValidaDataChegada = False
    If EcDataChegada.Text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataChegada) Then
'            If DateValue(EcDataChegada.Text) > DateValue(Bg_DaData_ADO) Then
'                BG_Mensagem mediMsgBox, "A data de Chegada tem que ser inferior ou igual � data actual ", vbInformation, "Aten��o"
'                SendKeys ("{HOME}+{END}")
'                If EcDataChegada.Enabled = True Then
'                    EcDataChegada.SetFocus
'                End If
'            Else
'                ValidaDataChegada = True
'            End If
            ValidaDataChegada = True
        End If
    End If
    
End Function

Function ValidaDataPrevista() As Boolean

    ValidaDataPrevista = False
    If EcDataPrevista.Text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataPrevista) And EcDataPrevista.Enabled = True Then
'            If DateValue(EcDataPrevista.Text) < DateValue(Bg_DaData_ADO) Then
'                BG_Mensagem mediMsgBox, "A data prevista tem que ser superior ou igual � data actual ", vbInformation, "Aten��o"
'                If EcDataPrevista.Enabled = True Then
'                    EcDataPrevista.Text = ""
'                    EcDataPrevista.SetFocus
'                End If
'            Else
'                ValidaDataPrevista = True
'            End If
            ValidaDataPrevista = True
        End If
    ElseIf EcDataPrevista.Text = "" Then
        If Flg_LimpaCampos = False Then
            If EcDtMarcacao.Text <> "" Then
                EcDataPrevista.Text = Format(EcDtMarcacao.Text, gFormatoData)
            Else
                EcDataPrevista.Text = Format(Bg_DaData_ADO, gFormatoData)
            End If
            ValidaDataPrevista = True
        End If
    End If

End Function

Public Sub EcDataPrevista_Validate(Cancel As Boolean)
    
    If Trim(EcDataPrevista.Text) <> "" Then
        Cancel = Not ValidaDataPrevista
    End If
    If Cancel = True Then Exit Sub
    
    If EcDataPrevista.Enabled = True And Trim(EcSeqUtente.Text) <> "" Then
        Preenche_Data_Prev_Prod
        SSTGestReq.TabEnabled(1) = True
        SSTGestReq.TabEnabled(2) = True
        SSTGestReq.TabEnabled(5) = True
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FgProd.row = LastRowP
        FgProd.Col = LastColP
        ExecutaCodigoP = True
        FrTubos.Enabled = True
        FrBtTubos.Enabled = True
        FGTubos.row = LastRowT
        FGTubos.Col = LastColT
        ExecutaCodigoT = True
    End If
    
End Sub

Private Sub EcDescrProveniencia_Change()
    Dim Tabela As ADODB.recordset
    Dim sql As String
    'verifica se a situa��o para a proveni�ncia escolhida � urg�ncia, se sim preenche campos autom�ticamente
    If EcCodProveniencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT t_sit FROM sl_proven WHERE cod_proven ='" & Trim(EcCodProveniencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            If BL_HandleNull(Tabela!t_sit, mediComboValorNull) = 2 Then
                Call EcDataPrevista_GotFocus
                Call EcDataPrevista_Validate(True)
                If EcDataChegada.Text = "" Then
                    EcDataChegada.Text = Bg_DaData_ADO
                End If
                If CbUrgencia.ListIndex = mediComboValorNull Then
                    CbUrgencia.ListIndex = 1
                End If
            End If

            
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
End Sub

Private Sub EcDtMarcacao_Click()
    If EcDtMarcacao.Text = "" Then
        EcDtMarcacao.Text = Bg_DaData_ADO
        EcDtMarcacao.SelStart = 0
        EcDtMarcacao.SelLength = Len(EcDtMarcacao)
    End If

End Sub

Private Sub EcDtMarcacao_Validate(Cancel As Boolean)

    If EcDtMarcacao.Text <> "" And EcDataPrevista.Text = "" Then
        EcDataPrevista.SetFocus
        EcDataPrevista.Text = EcDtMarcacao.Text
        EcDataPrevista_Validate False
    End If
    If EcDtMarcacao.Text <> "" Then
    
        If BG_ValidaTipoCampo_ADO(Me, EcDtMarcacao) Then
        Else
        Cancel = True
        End If
    End If
End Sub

Private Sub EcNumBenef_GotFocus()
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodEFR.Text <> "" And Trim(EcNumBenef.Text) = "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseClient
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.Text
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = Trim(BL_HandleNull(Tabela!Formato1, ""))
            Formato2 = Trim(BL_HandleNull(Tabela!Formato2, ""))
            EcNumBenef.Text = Trim(BL_HandleNull(Tabela!sigla, ""))
            Sendkeys ("{END}")
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub

Private Sub EcNumBenef_Validate(Cancel As Boolean)
    
    Dim Formato As Boolean
    
    Formato = Verifica_Formato(UCase(EcNumBenef.Text), Formato1, Formato2)
    If Formato = False And EcNumBenef.Text <> "" Then
        Cancel = True
        Sendkeys ("{END}")
    End If
    
End Sub

Private Sub EcNumReq_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True

End Sub

Private Sub EcNumReq_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        Flg_Executa_BtPassaMarcacao = True
        FormGesReqCons.FuncaoProcurar
    End If

End Sub

Private Sub EcNumReq_LostFocus()
    
    cmdOK.Default = False

End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)

End Sub

Private Sub EcObsProveniencia_LostFocus()

    On Error Resume Next
    Dim aux As String
    aux = BL_Retira_ENTER(EcObsProveniencia.Text)
    EcObsProveniencia.Text = aux
    DoEvents
    DoEvents
    
End Sub

Private Sub EcPesqRapArquivo_Change()
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If EcPesqRapArquivo.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        sql = "SELECT cod_arquivo,descr_arquivo FROM sl_arquivos WHERE seq_arquivo = '" & EcPesqRapArquivo & "'"
        rsCodigo.CursorLocation = adUseClient
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar arquivo!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapArquivo.Text = ""
        Else
            EcCodArquivo.Text = Trim(rsCodigo!cod_arquivo)
            EcDescrArquivo.Text = Trim(rsCodigo!descr_arquivo)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
End Sub

Private Sub EcPesqRapMedico_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If EcPesqRapMedico.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        Select Case gLAB
        Case gLAB = "CITO", "GM"
        
            sql = "SELECT codigo,titulo FROM sl_responsaveis WHERE codigo= '" & EcPesqRapMedico & "'"
            
            rsCodigo.CursorLocation = adUseClient
            rsCodigo.CursorType = adOpenStatic
            
            rsCodigo.Open sql, gConexao
            If rsCodigo.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do responsavel!", vbExclamation, "Pesquisa r�pida"
                EcPesqRapMedico.Text = ""
            Else
                EcCodMedico.Text = Trim(rsCodigo!Codigo)
                EcDescrMedico.Text = Trim(rsCodigo!Titulo)
            End If

        Case Else
            sql = "SELECT cod_med,nome_med FROM sl_medicos WHERE cod_med = '" & EcPesqRapMedico & "'"
            
            rsCodigo.CursorLocation = adUseClient
            rsCodigo.CursorType = adOpenStatic
            
            rsCodigo.Open sql, gConexao
            If rsCodigo.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do m�dico!", vbExclamation, "Pesquisa r�pida"
                EcPesqRapMedico.Text = ""
            Else
                EcCodMedico.Text = Trim(rsCodigo!cod_med)
                EcDescrMedico.Text = Trim(rsCodigo!nome_med)
            End If
        End Select
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcPesqRapMedico2_Change()
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If EcPesqRapMedico2.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        sql = "SELECT codigo,titulo FROM sl_responsaveis WHERE codigo = '" & EcPesqRapMedico2 & "'"
        rsCodigo.CursorLocation = adUseClient
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do respons�vel!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapMedico2.Text = ""
        Else
            EcCodMedico2.Text = Trim(rsCodigo!Codigo)
            EcDescrMedico2.Text = Trim(rsCodigo!Titulo)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
End Sub

Private Sub EcPesqRapProduto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapProduto.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseClient
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_produto FROM sl_produto WHERE seq_produto = " & EcPesqRapProduto, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do produto!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxProd.Text = BL_HandleNull(rsCodigo!cod_produto, "")
            EcAuxProd.left = FgProd.CellLeft + 270
            EcAuxProd.top = FgProd.CellTop + 2060
            EcAuxProd.Width = FgProd.CellWidth + 20
            EcAuxProd.Visible = True
            EcAuxProd.Enabled = True
            EcAuxProd.SetFocus
        End If
    End If

End Sub
Private Sub EcPesqRapTubo_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapTubo.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseClient
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_tubo FROM sl_tubo WHERE seq_tubo = " & EcPesqRapTubo, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do tubo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxTubo.Text = BL_HandleNull(rsCodigo!cod_tubo, "")
            EcAuxTubo.left = FGTubos.CellLeft + 270
            EcAuxTubo.top = FGTubos.CellTop + 2060
            EcAuxTubo.Width = FGTubos.CellWidth + 20
            EcAuxTubo.Visible = True
            EcAuxTubo.Enabled = True
            EcAuxTubo.SetFocus
        End If
    End If

End Sub

Private Sub EcPesqRapProven_Change()
    
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapProven.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseClient
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_proven,descr_proven FROM sl_proven WHERE seq_proven = " & EcPesqRapProven, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da proveni�ncia!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapProven.Text = ""
        Else
            EcCodProveniencia.Text = Trim(rsCodigo!cod_proven)
            EcDescrProveniencia.Text = Trim(rsCodigo!descr_proven)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcReqAssociada_GotFocus()
    If gMultiReport = 1 Then
        Set CampoActivo = Me.ActiveControl
        cmdOK.Default = True
    End If
End Sub

Private Sub EcReqAssociada_LostFocus()
    If gMultiReport = 1 Then
        cmdOK.Default = False
    End If
End Sub
Private Sub EcReqAssociada_Validate(Cancel As Boolean)
        
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcReqAssociada)
    
End Sub

Private Sub EcReqAux_GotFocus()
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True
End Sub

Private Sub EcReqAux_LostFocus()
    cmdOK.Default = False
End Sub

Private Sub EcReqAux_Validate(Cancel As Boolean)
    
    EcReqAux.Text = UCase(EcRequAX.Text)
    
End Sub

Public Sub EcUtente_Validate(Cancel As Boolean)
    
    Dim sql As String
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    
    'Limpa a estrutura de dados do utente
    If CbTipoUtente.ListIndex <> -1 And EcUtente.Text <> "" Then
        'Procurar o sequencial e chamar a fun��o que preenche a identifica��o do utente
        Campos(0) = "seq_utente"
        
        iret = BL_DaDadosUtente(Campos, retorno, , CbTipoUtente.Text, EcUtente.Text)
        
        If iret = -1 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbInformation + vbOKOnly, "Pesquisa ao utente!"
            EcNome.Text = ""
            EcProcHosp1.Text = ""
            EcProcHosp2.Text = ""
            EcNumCartaoUte.Text = ""
            EcDataNasc.Text = ""
        Else
            BtPassaMarcacao.Enabled = True
            BtInfClinica.Enabled = True
            LaInfCli.Enabled = True
            
            EcSeqUtente.Text = retorno(0)
            BL_PreencheDadosUtente EcSeqUtente
            
            PreencheDadosUtente
            
        End If
    Else
        EcNome.Text = ""
        EcProcHosp1.Text = ""
        EcProcHosp2.Text = ""
        EcNumCartaoUte.Text = ""
        EcDataNasc.Text = ""
    End If
    
End Sub

Private Sub EcUtilizadorAlteracao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorAlteracao)
    If Cancel = False Then
        LbNomeUtilAlteracao = BL_SelNomeUtil(EcUtilizadorAlteracao.Text)
    End If
    
End Sub

Private Sub EcUtilizadorCriacao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorCriacao)
    If Cancel = False Then
        LbNomeUtilCriacao = BL_SelNomeUtil(EcUtilizadorCriacao.Text)
    End If
    
End Sub

Private Sub EcUtilizadorImpressao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcUtilizadorImpressao_LostFocus()
    
    LbNomeUtilImpressao = BL_SelNomeUtil(EcUtilizadorImpressao.Text)

End Sub

Private Sub EcUtilizadorImpressao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcUtilizadorImpressao2_Validate(Cancel As Boolean)
    
    LbNomeUtilImpressao2 = BL_SelNomeUtil(EcUtilizadorImpressao2.Text)
    
End Sub

Private Sub Fgana_DblClick()
    
    If RegistosA(FGAna.row).estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" Then
        gRequisicaoActiva = FormGesReqCons.EcNumReq
        gF_REQCONS = 1
        FormGesReqCons.Enabled = False
        FormConsRequis.Show
    End If
End Sub

Private Sub FGAna_GotFocus()
    
    If RegistosA(FGAna.row).estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" Then
        FGAna.CellBackColor = vbRed
        FGAna.CellForeColor = vbWhite
    Else
        FGAna.CellBackColor = azul
        FGAna.CellForeColor = vbWhite
    End If

End Sub

Public Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ana As String
    If (FGAna.Col = 0 Or FGAna.Col = 2) And (KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0)) Then ' Enter = Editar
        If RegistosA(FGAna.row).estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = 0 Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            If LastColA <> 0 And Trim(FGAna.TextMatrix(LastRowA, 0)) = "" Then
                Beep
                BG_Mensagem mediMsgStatus, "Tem que indicar primeiro uma an�lise!"
            Else
                ExecutaCodigoA = False
                EcAuxAna.Text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                ExecutaCodigoA = True
                If KeyCode = 8 Then
                    If Len(EcAuxAna.Text) > 1 Then EcAuxAna.Text = Mid(EcAuxAna.Text, 1, Len(EcAuxAna.Text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxAna.SelStart = 0
                    EcAuxAna.SelLength = Len(EcAuxAna)
                Else
                    EcAuxAna.SelLength = 0
                End If

                EcAuxAna.left = FGAna.CellLeft + 260
                EcAuxAna.top = SSTGestReq.top + FrAnalises.top + FGAna.top + FGAna.CellTop
                EcAuxAna.Width = FGAna.CellWidth + 20
                EcAuxAna.Visible = True
                EcAuxAna.Enabled = True
                EcAuxAna.SetFocus
            End If
        End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga linha
        If RegistosA(FGAna.row).estado <> "-1" And RegistosA(FGAna.row).codAna <> "" Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser eliminada: j� cont�m resultados!"
        ElseIf RegistosA(FGAna.row).codAna <> "" Then
            ana = RegistosA(FGAna.row).codAna
            Elimina_Mareq RegistosA(FGAna.row).codAna
            RegistosA.Remove FGAna.row
            FGAna.RemoveItem FGAna.row
            EliminaTubo ana
            EliminaProduto ana
        End If
        FGAna.SetFocus
    Else
    End If
    
End Sub

Private Sub FGAna_LostFocus()

    If RegistosA(FGAna.row).estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" Then
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbRed
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
    
End Sub

Private Sub Fgana_RowColChange()
    
    Dim tmpCol As Integer
    Dim tmpRow As Integer
        
    If ExecutaCodigoA = True Then
        ExecutaCodigoA = False
        
        tmpCol = FGAna.Col
        tmpRow = FGAna.row
        If FGAna.Col <> LastColA Then FGAna.Col = LastColA
        If FGAna.row <> LastRowA Then FGAna.row = LastRowA
        
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(LastRowA).estado <> "-1" And LastColA = 0 And Trim(FGAna.TextMatrix(LastRowA, 0)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbBlack
            End If
        End If
        
        If FGAna.Col <> tmpCol Then FGAna.Col = tmpCol
        If FGAna.row <> tmpRow Then FGAna.row = tmpRow
        
        ' Controlar as colunas
        If FGAna.Cols = 4 Then
            If FGAna.Col = 3 Then
                FGAna.Col = 0
                If FGAna.row < FGAna.rows - 1 Then FGAna.row = FGAna.row + 1
            ElseIf FGAna.Col = 1 Then
                If LastColA = 2 Then
                    FGAna.Col = 0
                Else
                    FGAna.Col = 2
                End If
            End If
        Else
            If FGAna.Col = 1 Then
                FGAna.Col = 0
                If FGAna.row < FGAna.rows - 1 Then FGAna.row = FGAna.row + 1
            End If
        End If
        
        LastColA = FGAna.Col
        LastRowA = FGAna.row
        
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(FGAna.row).estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" Then
                FGAna.CellBackColor = vbRed
                FGAna.CellForeColor = vbWhite
            Else
                FGAna.CellBackColor = azul
                FGAna.CellForeColor = vbWhite
            End If
        End If
        
        ExecutaCodigoA = True
    End If

End Sub

Private Sub FgProd_GotFocus()
    
    FgProd.CellBackColor = azul
    FgProd.CellForeColor = vbWhite
    
    If FgProd.Col = 0 Then
        BtPesquisaProduto.Enabled = True
        BtPesquisaEspecif.Enabled = False
    ElseIf FgProd.Col = 2 Then
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = True
    Else
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = False
    End If

End Sub
Private Sub FgTubos_GotFocus()
    
    FGTubos.CellBackColor = azul
    FGTubos.CellForeColor = vbWhite
    
    If FGTubos.Col = 0 Then
        BtPesquisaTubos.Enabled = True
    Else
        BtPesquisaTubos.Enabled = False
    End If

End Sub
Public Sub FgProd_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
        If LastColP <> 0 And Trim(FgProd.TextMatrix(LastRowP, 0)) = "" Then
            Beep
            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um produto!"
        Else
            If LastColP = 4 And EcDataPrevista.Enabled = False Then
                Beep
                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este produto!"
            Else
                EcAuxProd.Text = FgProd.TextMatrix(FgProd.row, FgProd.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                If KeyCode = 8 Then
                    EcAuxProd.Text = Mid(EcAuxProd.Text, 1, Len(EcAuxProd.Text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxProd.SelStart = 0
                    EcAuxProd.SelLength = Len(EcAuxProd)
                Else
                    EcAuxProd.SelLength = 0
                End If
                EcAuxProd.left = FgProd.CellLeft + 270
                EcAuxProd.top = FgProd.CellTop + 2060
                EcAuxProd.Width = FgProd.CellWidth + 20
                EcAuxProd.Visible = True
                EcAuxProd.Enabled = True
                EcAuxProd.SetFocus
            End If
        End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
        Select Case FgProd.Col
            Case 0
                If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
                    If FgProd.row < FgProd.rows - 1 Then
                        RegistosP.Remove FgProd.row
                        FgProd.RemoveItem FgProd.row
                    Else
                        FgProd.TextMatrix(FgProd.row, 0) = ""
                        FgProd.TextMatrix(FgProd.row, 1) = ""
                        FgProd.TextMatrix(FgProd.row, 2) = ""
                        FgProd.TextMatrix(FgProd.row, 3) = ""
                        FgProd.TextMatrix(FgProd.row, 4) = ""
                        FgProd.TextMatrix(FgProd.row, 5) = ""
                        FgProd.TextMatrix(FgProd.row, 6) = ""
                        RegistosP(FgProd.row).CodProd = ""
                        RegistosP(FgProd.row).DescrProd = ""
                        RegistosP(FgProd.row).EspecifObrig = ""
                        RegistosP(FgProd.row).CodEspecif = ""
                        RegistosP(FgProd.row).DescrEspecif = ""
                        RegistosP(FgProd.row).DtPrev = ""
                        RegistosP(FgProd.row).DtChega = ""
                        RegistosP(FgProd.row).EstadoProd = ""
                    End If
                End If
            Case 2
                FgProd.TextMatrix(FgProd.row, 2) = ""
                FgProd.TextMatrix(FgProd.row, 3) = ""
                RegistosP(FgProd.row).CodEspecif = ""
                RegistosP(FgProd.row).DescrEspecif = ""
            Case 4
                FgProd.TextMatrix(FgProd.row, 4) = ""
                RegistosP(FgProd.row).DtPrev = ""
            Case 5
                FgProd.TextMatrix(FgProd.row, 5) = ""
                RegistosP(FgProd.row).DtChega = ""
        End Select
        FgProd.SetFocus
    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FgProd.TextMatrix(FgProd.row, 1) & " " & FgProd.TextMatrix(FgProd.row, 3) & " ?", vbYesNo + vbQuestion, "Eliminar produto.") = vbYes Then
                If FgProd.row < FgProd.rows - 1 Then
                    RegistosP.Remove FgProd.row
                    FgProd.RemoveItem FgProd.row
                Else
                    FgProd.TextMatrix(FgProd.row, 0) = ""
                    FgProd.TextMatrix(FgProd.row, 1) = ""
                    FgProd.TextMatrix(FgProd.row, 2) = ""
                    FgProd.TextMatrix(FgProd.row, 3) = ""
                    FgProd.TextMatrix(FgProd.row, 4) = ""
                    FgProd.TextMatrix(FgProd.row, 5) = ""
                    FgProd.TextMatrix(FgProd.row, 6) = ""
                    RegistosP(FgProd.row).CodProd = ""
                    RegistosP(FgProd.row).DescrProd = ""
                    RegistosP(FgProd.row).EspecifObrig = ""
                    RegistosP(FgProd.row).CodEspecif = ""
                    RegistosP(FgProd.row).DescrEspecif = ""
                    RegistosP(FgProd.row).DtPrev = ""
                    RegistosP(FgProd.row).DtChega = ""
                    RegistosP(FgProd.row).EstadoProd = ""
                End If
            End If
        End If
        FgProd.SetFocus
    Else
    End If

End Sub
Public Sub FGTubos_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
        If LastColT <> 0 And Trim(FGTubos.TextMatrix(LastRowT, 0)) = "" Then
            Beep
            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um tubo!"
        Else
            If LastColT = 2 And EcDataPrevista.Enabled = False Then
                Beep
                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este tubo!"
            Else
                EcAuxTubo.Text = FGTubos.TextMatrix(FGTubos.row, FGTubos.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                If KeyCode = 8 Then
                    EcAuxTubo.Text = Mid(EcAuxTubo.Text, 1, Len(EcAuxTubo.Text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxTubo.SelStart = 0
                    EcAuxTubo.SelLength = Len(EcAuxTubo)
                Else
                    EcAuxTubo.SelLength = 0
                End If
                EcAuxTubo.left = FGTubos.CellLeft + 270
                EcAuxTubo.top = FGTubos.CellTop + 2060
                EcAuxTubo.Width = FGTubos.CellWidth + 20
                EcAuxTubo.Visible = True
                EcAuxTubo.Enabled = True
                EcAuxTubo.SetFocus
            End If
        End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
        Select Case FGTubos.Col
            Case 0
                If Trim(FGTubos.TextMatrix(FGTubos.row, 0)) <> "" And Procura_Tubo_ana = False Then
                    If FGTubos.row < FGTubos.rows - 1 Then
                        RegistosT.Remove FGTubos.row
                        FGTubos.RemoveItem FGTubos.row
                    Else
                        FGTubos.TextMatrix(FGTubos.row, 0) = ""
                        FGTubos.TextMatrix(FGTubos.row, 1) = ""
                        FGTubos.TextMatrix(FGTubos.row, 2) = ""
                        FGTubos.TextMatrix(FGTubos.row, 3) = ""
                        FGTubos.TextMatrix(FGTubos.row, 4) = ""
                        FGTubos.TextMatrix(FGTubos.row, 5) = ""
                        RegistosT(FGTubos.row).CodTubo = ""
                        RegistosT(FGTubos.row).descrTubo = ""
                        RegistosT(FGTubos.row).DtPrev = ""
                        RegistosT(FGTubos.row).DtChega = ""
                        RegistosT(FGTubos.row).HrChega = ""
                        RegistosT(FGTubos.row).EstadoTubo = ""
                    End If
                End If
            Case 2
                FGTubos.TextMatrix(FGTubos.row, 2) = ""
                RegistosT(FGTubos.row).DtPrev = ""
            Case 3
                FGTubos.TextMatrix(FGTubos.row, 3) = ""
                RegistosT(FGTubos.row).DtChega = ""
            Case 4
                FGTubos.TextMatrix(FGTubos.row, 4) = ""
                RegistosT(FGTubos.row).HrChega = ""
        End Select
        FGTubos.SetFocus
    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
        If Trim(FGTubos.TextMatrix(FGTubos.row, 0)) <> "" And Procura_Tubo_ana = False Then
            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FGTubos.TextMatrix(FgProd.row, 1) & " ?", vbYesNo + vbQuestion, "Eliminar tubo.") = vbYes Then
                If FGTubos.row < FGTubos.rows - 1 Then
                    RegistosT.Remove FGTubos.row
                    FGTubos.RemoveItem FGTubos.row
                Else
                    FGTubos.TextMatrix(FGTubos.row, 0) = ""
                    FGTubos.TextMatrix(FGTubos.row, 1) = ""
                    FGTubos.TextMatrix(FGTubos.row, 2) = ""
                    FGTubos.TextMatrix(FGTubos.row, 3) = ""
                    FGTubos.TextMatrix(FGTubos.row, 4) = ""
                    FGTubos.TextMatrix(FGTubos.row, 5) = ""
                    RegistosT(FGTubos.row).CodTubo = ""
                    RegistosT(FGTubos.row).descrTubo = ""
                    RegistosT(FGTubos.row).DtPrev = ""
                    RegistosT(FGTubos.row).DtChega = ""
                    RegistosT(FGTubos.row).HrChega = ""
                    RegistosT(FGTubos.row).EstadoTubo = ""
                End If
            End If
        End If
        FGTubos.SetFocus
    Else
    End If

End Sub

Function SELECT_Descr_Prod() As Boolean
    
    'Fun��o que procura a descri��o do produto
    'A fun��o devolve TRUE se encontrou a descri��o caso contr�rio devolve FALSE
    
    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    
    SELECT_Descr_Prod = False
    With rsDescr
        .Source = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                  "cod_produto = " & BL_TrataStringParaBD(UCase(EcAuxProd.Text))
        .ActiveConnection = gConexao
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Produto inexistente!", vbOKOnly + vbInformation, "Produtos"
        EcAuxProd.Text = ""
    Else
        FgProd.TextMatrix(LastRowP, 1) = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).EspecifObrig = rsDescr!especif_obrig
        SELECT_Descr_Prod = True
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing

End Function
Function SELECT_Descr_Tubo() As Boolean
    
    'Fun��o que procura a descri��o do tubo
    'A fun��o devolve TRUE se encontrou a descri��o caso contr�rio devolve FALSE
    
    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    
    SELECT_Descr_Tubo = False
    With rsDescr
        .Source = "SELECT descr_tubo FROM sl_tubo WHERE " & _
                  "cod_tubo = " & BL_TrataStringParaBD(UCase(EcAuxTubo.Text))
        .ActiveConnection = gConexao
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Tubo inexistente!", vbOKOnly + vbInformation, "Tubos"
        EcAuxTubo.Text = ""
    Else
        FGTubos.TextMatrix(LastRowT, 1) = BL_HandleNull(rsDescr!descR_tubo, "")
        RegistosT(LastRowT).descrTubo = BL_HandleNull(rsDescr!descR_tubo, "")
        SELECT_Descr_Tubo = True
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing

End Function

Function SELECT_Especif() As Boolean
    
    Dim RsDescrEspecif As ADODB.recordset
    Dim encontrou As Boolean
    
    SELECT_Especif = False
    encontrou = False
    If Trim(EcAuxProd.Text) <> "" Then
        Set RsDescrEspecif = New ADODB.recordset
        With RsDescrEspecif
            .Source = "SELECT descr_especif FROM sl_especif WHERE " & _
                    "cod_especif=" & UCase(BL_TrataStringParaBD(EcAuxProd.Text))
            .ActiveConnection = gConexao
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .Open
        End With
        If RsDescrEspecif.RecordCount > 0 Then
            FgProd.TextMatrix(LastRowP, 3) = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            RegistosP(LastRowP).DescrEspecif = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            SELECT_Especif = True
        Else
            BG_Mensagem mediMsgBox, "Especifica��o inexistente!", vbOKOnly + vbInformation, "Especifica��es"
        End If
        RsDescrEspecif.Close
        Set RsDescrEspecif = Nothing
    End If

End Function

Private Sub FgProd_LostFocus()

    FgProd.CellBackColor = vbWhite
    FgProd.CellForeColor = vbBlack
    
End Sub

Private Sub FgProd_RowColChange()
    
    Dim tmpCol As Integer
    Dim tmpRow As Integer
    Dim RsEspecif As ADODB.recordset
    
    If ExecutaCodigoP = True Then
        ExecutaCodigoP = False
        
        If Trim(FgProd.TextMatrix(LastRowP, 0)) <> "" And Trim(FgProd.TextMatrix(LastRowP, 2)) = "" Then
    
            'Escolher a especifica��o de defeito do produto
            
            Set RsEspecif = New ADODB.recordset
                
            With RsEspecif
                .Source = "select sl_produto.cod_especif, sl_especif.descr_especif from sl_produto, sl_especif where sl_produto.cod_especif = sl_especif.cod_especif and cod_produto = " & BL_TrataStringParaBD(Trim(FgProd.TextMatrix(LastRowP, 0)))
                .CursorLocation = adUseClient
                .CursorType = adOpenStatic
                .Open , gConexao
            End With
            
            If Not RsEspecif.EOF Then
                FgProd.TextMatrix(LastRowP, 2) = Trim(BL_HandleNull(RsEspecif!cod_Especif, ""))
                FgProd.TextMatrix(LastRowP, 3) = Trim(BL_HandleNull(RsEspecif!descr_especif, ""))
            End If
            
            RsEspecif.Close
        End If
            
        Set RsEspecif = Nothing
        
        tmpCol = FgProd.Col
        tmpRow = FgProd.row
        If FgProd.Col <> LastColP Then FgProd.Col = LastColP
        If FgProd.row <> LastRowP Then FgProd.row = LastRowP
        FgProd.CellBackColor = vbWhite
        FgProd.CellForeColor = vbBlack
        If FgProd.Col <> tmpCol Then FgProd.Col = tmpCol
        If FgProd.row <> tmpRow Then FgProd.row = tmpRow
        
        If FgProd.row <> LastRowP Then
            Preenche_LaProdutos FgProd.row
        End If
        
        ' Controlar as colunas
        If FgProd.Col = 1 Then
            If LastColP >= 2 Then
                FgProd.Col = 0
            Else
                FgProd.Col = 2
            End If
        ElseIf FgProd.Col = 3 Then
            If LastColP >= 4 Then
                FgProd.Col = 2
            Else
                FgProd.Col = 4
            End If
        ElseIf LastColP = 4 And LastRowP = FgProd.row And FgProd.Col <> 5 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        ElseIf FgProd.Col > 4 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        End If
        
        LastColP = FgProd.Col
        LastRowP = FgProd.row
        
        FgProd.CellBackColor = azul
        FgProd.CellForeColor = vbWhite
        
        If LastColP = 0 Then
            BtPesquisaProduto.Enabled = True
            BtPesquisaEspecif.Enabled = False
        ElseIf LastColP = 2 Then
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = True
        Else
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = False
        End If
        ExecutaCodigoP = True
        
    End If

End Sub

Private Sub FGTubos_LostFocus()

    FGTubos.CellBackColor = vbWhite
    FGTubos.CellForeColor = vbBlack
    
End Sub
Private Sub FGTubos_RowColChange()
    
    Dim tmpCol As Integer
    Dim tmpRow As Integer
    
    If ExecutaCodigoT = True Then
        ExecutaCodigoT = False
        
        tmpCol = FGTubos.Col
        tmpRow = FGTubos.row
        If FGTubos.Col <> LastColT Then FGTubos.Col = LastColT
        If FGTubos.row <> LastRowT Then FGTubos.row = LastRowT
        FGTubos.CellBackColor = vbWhite
        FGTubos.CellForeColor = vbBlack
        If FGTubos.Col <> tmpCol Then FGTubos.Col = tmpCol
        If FGTubos.row <> tmpRow Then FGTubos.row = tmpRow
        
        If FGTubos.row <> LastRowT Then
            Preenche_LaTubos FGTubos.row
        End If
        
        ' Controlar as colunas
        If FGTubos.Col = 1 Then
            If LastColT >= 2 Then
                FGTubos.Col = 0
            Else
                FGTubos.Col = 2
            End If
        ElseIf FGTubos.Col = 3 Then
            If LastColT >= 4 Then
                FGTubos.Col = 2
            Else
                FGTubos.Col = 4
            End If
        ElseIf LastColT = 4 And LastRowT = FGTubos.row And FGTubos.Col <> 5 Then
            FGTubos.Col = 0
            If FGTubos.row <> FGTubos.rows - 1 Then FGTubos.row = FGTubos.row + 1
        ElseIf FGTubos.Col > 4 Then
            FGTubos.Col = 0
            If FGTubos.row <> FGTubos.rows - 1 Then FGTubos.row = FGTubos.row + 1
        End If
        
        LastColT = FGTubos.Col
        LastRowT = FGTubos.row
        
        FGTubos.CellBackColor = azul
        FGTubos.CellForeColor = vbWhite
        
        If LastColT = 0 Then
            BtPesquisaTubos.Enabled = True
        Else
            BtPesquisaTubos.Enabled = False
        End If
        ExecutaCodigoT = True
        
    End If

End Sub

Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Me, "")

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoInserir()
    Dim outraMarcacao As String
    
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    
    If gPreencheDatasReq = mediSim Then
        If EcDataPrevista.Text = "" Then
            EcDataPrevista.Text = Bg_DaData_ADO
        End If
        If EcDataChegada.Text = "" Then
            EcDataChegada.Text = Bg_DaData_ADO
        End If
    End If
    
    If Trim(EcEpisodio.Text) = "" And gPreencheEpis = 1 Then
        DevolveEpisodio EcSeqUtente.Text
    End If
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        outraMarcacao = VerificaUtenteMarcacoesPosteriores()
        If outraMarcacao <> "" Then
            gMsgTitulo = "Inserir"
            gMsgMsg = "Utente tem outra marca��o numa data superior: " & outraMarcacao & ". Tem a certeza que quer inserir estes dados ?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbNo Then
                Exit Sub
            End If
        End If
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        EcHoraCriacao = Bg_DaHora_ADO
        EcPagUte.Text = Mid(CbPagUte.Text, 1, 1)
        EcTaxaModeradora.Text = Mid(CbTaxaModeradora.Text, 1, 1)
        EcPagEnt.Text = Mid(CbPagEnt.Text, 1, 1)
        
        'se a data prevista estiver vazia
        If EcDataPrevista.Text = "" Then
            EcDataPrevista.Text = Format(Bg_DaData_ADO, gFormatoData)
        End If
        
        ValidaDataPrevista
'        If DateValue(EcDataPrevista.Text) < DateValue(Bg_DaData_ADO) Then Exit Sub
        
        
        'Se o utente � isento, � obrigat�rio indicar o motivo
        If CkIsento.value = vbChecked And CbTipoIsencao.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Tipo de isen��o obrigat�rio!", vbOKOnly + vbInformation, "Insen��es"
            CbTipoIsencao.SetFocus
            Exit Sub
        End If
        If CkIsento.value = vbUnchecked Or CkIsento.value = vbGrayed Then
            CbTipoIsencao.ListIndex = -1
        End If
        
        If Not ValidaEspecifObrig Then Exit Sub
        
        If ValidaNumBenef = True Then
            iRes = ValidaCamposEc
            If iRes = True Then
            
                Call BD_Insert
                
                If Trim(EcNumReq.Text) <> "" Then
                    'Se n�o houve erro
                    If gRequisicaoActiva = 0 Then
                        BtDadosAnteriores.Enabled = False
                        LaCopiarAnterior.Enabled = False
                    Else
                        BtDadosAnteriores.Enabled = True
                        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
                        LaCopiarAnterior.Enabled = True
                    End If
                    BtEtiq.Enabled = True
                    BtNorma.Enabled = True
                    
                    LaEtiq.Enabled = True
                    ImprimeEtiq
                    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
                    BL_Toolbar_BotaoEstado "Procurar", "Activo"
                    BL_Toolbar_BotaoEstado "Limpar", "Activo"
                    'Mart. ergonomico
                    Call FuncaoProcurar
                End If
            End If
        End If
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim i As Integer
    Dim k As Integer
    Dim req As Long
    Dim ProdJaChegou As Boolean
    Dim rv As Integer
    
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    
    'Determinar o novo n�mero da requisi��o (TENTA 10 VEZES - LOCK!)
    i = 0
    req = -1
    While req = -1 And i <= 10
        If gSerieUnicaMarcacao = 1 Then
            req = BL_GeraNumero("N_REQUIS")
        Else
            req = BL_GeraNumero("N_REQUIS_CONSULTAS")
        End If
        i = i + 1
    Wend
    
    If req = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de requisi��o !", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
    
    EcNumReq.Text = ""
    EcNumReq.Text = req
    
    If (EcEpisodio.Text = "") And _
       (gEpisodioIgualNumReq) Then
        EcEpisodio.Text = Trim(EcNumReq.Text)
    End If
    
    gRequisicaoActiva = CLng(EcNumReq.Text)
    
    BG_BeginTransaction
    
    ' Produtos da requisi��o
    Grava_Prod_Req
    If gSQLError <> 0 Then
        'Erro a gravar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Tubos da requisi��o
    Grava_Tubos_Req
    If gSQLError <> 0 Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Actualizar estado da requisi��o em conformidade com an�lises marcadas, produtos marcados e sua data de chegada
    If CbEstadoReqAux.Text = "" Or CbEstadoReqAux.Text = "I" Or CbEstadoReqAux.Text = "A" Or CbEstadoReqAux.Text = " " Then
        ProdJaChegou = False
        If FGAna.rows >= 1 And Trim(FGAna.TextMatrix(1, 0)) <> "" Then
            If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                For k = 1 To RegistosP.Count - 1
                    If Trim(RegistosP(1).DtChega) <> "" Then
                        ProdJaChegou = True
                    End If
                Next k
                If ProdJaChegou = True Then
                    CbEstadoReqAux.Text = "A"
                Else
                    CbEstadoReqAux.Text = "I"
                End If
            Else
                CbEstadoReqAux.Text = "A"
                EcDataChegada.Text = Bg_DaData_ADO
            End If
        Else
            CbEstadoReqAux.Text = " "
        End If
    End If
    
    CbEstadoReq.ListIndex = CbEstadoReqAux.ListIndex
    EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
    
    ' An�lises da requisi��o
    Call Grava_Ana_Req
    If gSQLError <> 0 Then
        'Erro a gravar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Dados do P1 da ARS (caso a entidade seja ARS)
    Call Grava_Ana_ARS
    If gSQLError <> 0 Then
        'Erro a gravar dados do P1 da ARS
        BG_Mensagem mediMsgBox, "Erro a gravar as requisi��es gARS !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
            
    ' Actualizar o numero de requisi��o associada
    Grava_Req_Assoc
    If gSQLError <> 0 Then
        'Erro a actualizar o numero de requisi��o associada
        BG_Mensagem mediMsgBox, "Erro a actualizar n�mero da requisi��o associada !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Diagn�sticos secund�rios e terapeuticas/medica��o
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar diagn�sticos secund�rios e terapeuticas/medica��o
        BG_Mensagem mediMsgBox, "Erro a actualizar diagn�sticos secund�rios e terap�uticas/medica��o!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Grava na agenda
    Grava_Agenda
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a gravar agenda!", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    ' Gravar os restantes dados da requisi��o
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery

    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        BG_CommitTransaction
        gRequisicaoActiva = CLng(EcNumReq.Text)
        
        'Grava_Recibos
        'Preenche_Recibo_Isencao
    End If
    
    Exit Sub
       
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a inserir requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGesReqCons: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar
    Exit Sub
    Resume Next

End Sub

Sub Grava_Recibos()
    
    Dim sql As String
    Dim i As Integer
    Dim k As Integer
    Dim TotalTaxa As Double
    Dim TotalPagEnt As Double
    Dim TotalPagUte As Double
    Dim LinhasRec() As Estrutura_Recibo
    
    ReDim LinhasRec(TotalListaRecibos)
    For k = 1 To TotalListaRecibos
        LinhasRec(k).cod_rubr = Recibo(k).cod_rubr
        LinhasRec(k).descr_rubr = Recibo(k).descr_rubr
        LinhasRec(k).Perc_Ute = 0
        LinhasRec(k).Valor_Ent = 0
        LinhasRec(k).Valor_Taxa = 0
        LinhasRec(k).Valor_Ute = 0
    Next k
    
    'No caso de nenhuma das combos(TaxaModerador,PagUte,PagEnt) tenha a op��o Pago seleccionada N�o emite recibo
    If CbTaxaModeradora.ListIndex <> 0 And CbPagEnt.ListIndex <> 0 And CbPagUte.ListIndex <> 0 Then Exit Sub
    
    'S� emite o recibos se existirem an�lises
    If TotalListaRecibos > 0 Then
        TotalTaxa = BL_String2Double(ValorTM, 3)
        TotalPagEnt = BL_String2Double(ValorEnt, 3)
        TotalPagUte = BL_String2Double(ValorDoe, 3)
        gSQLError = 0
        
        'Determinar o novo n�mero do recibo (TENTA 10 VEZES - LOCK!)
        i = 0
        gRec = -1
        While gRec = -1 And i <= 10
            gRec = BL_GeraNumero("N_REC")
            i = i + 1
        Wend
        
        If gRec = -1 Then
            
            Exit Sub
        End If
        
        sql = "INSERT INTO sl_recibos(serie_rec,n_rec,n_req,user_emi,dt_emi,hr_emi,t_anul,user_anul," & _
        "dt_anul,dt_fecho,dt_fecho_anul,cod_moeda,val_pag_ent,val_taxa,val_pag_ute, cod_forma_pag) VALUES " & _
        "('" & gSerieRec & "'," & gRec & "," & gRequisicaoActiva & "," & _
        BL_TrataStringParaBD(EcUtilizadorCriacao.Text) & "," & BL_TrataDataParaBD(EcDataCriacao.Text) & "," & _
        BL_TrataStringParaBD(EcHoraCriacao.Text) & ",'0','0',NULL,NULL,NULL,'" & gMoedaActiva & "',"
        
        
        'Valor pago pela entidade
        If CbEntIndex <> CbPagEnt.ListIndex And CbPagEnt.ListIndex = 0 Then
            For k = 1 To TotalListaRecibos
                LinhasRec(k).Valor_Ent = Recibo(k).Valor_Ent
            Next k
            sql = sql & TotalPagEnt & ","
        Else
            sql = sql & "null ,"
        End If
            
        'Taxa moderadora
        If CbTMIndex <> CbTaxaModeradora.ListIndex And CbTaxaModeradora.ListIndex = 0 Then
            For k = 1 To TotalListaRecibos
                LinhasRec(k).Valor_Taxa = Recibo(k).Valor_Taxa
            Next k
            sql = sql & TotalTaxa & ","
        Else
            sql = sql & "null ,"
        End If
        
        'S� o valor pago pelo utente
        If CbUteIndex <> CbPagUte.ListIndex And CbPagUte.ListIndex = 0 Then
            For k = 1 To TotalListaRecibos
                LinhasRec(k).Valor_Ute = Recibo(k).Valor_Ute
            Next k
            sql = sql & TotalPagUte & " ,"
        Else
            sql = sql & "null ,"
        End If
        sql = sql & gModoPagNaoPago & ") "
        gSQLError = 0
        BG_ExecutaQuery_ADO sql 'Cabe�alho do recibo
    
        If gSQLError = 0 Then
            For k = 1 To TotalListaRecibos
                If LinhasRec(k).Valor_Taxa <> 0 Or LinhasRec(k).Valor_Ent <> 0 Or LinhasRec(k).Valor_Ute Then
                    sql = "INSERT INTO sl_lin_rec (serie_rec,n_rec,descr_rubr,val_taxa,val_pag_ent,val_pag_ute,cod_moeda) VALUES " & _
                        "(" & BL_TrataStringParaBD(gSerieRec) & "," & gRec & "," & BL_TrataStringParaBD(LinhasRec(k).descr_rubr) & "," & _
                        LinhasRec(k).Valor_Taxa & "," & LinhasRec(k).Valor_Ent & "," & LinhasRec(k).Valor_Ute & "," & BL_TrataStringParaBD(gMoedaActiva) & ")"
                    gSQLError = 0
                    BG_ExecutaQuery_ADO sql 'Linhas do recibo
                    If gSQLError <> 0 Then
                        Exit Sub
                    End If
                End If
            Next k
        Else
            
            Exit Sub
        End If
        
        If CbTaxaModeradora.ListIndex = 0 And NumRecTM.Text = "" Then
            NumRecTM.Text = gSerieRec & "/" & gRec
        End If
        If CbPagUte.ListIndex = 0 And NumRecDoe.Text = "" Then
            NumRecDoe.Text = gSerieRec & "/" & gRec
        End If
        If CbPagEnt.ListIndex = 0 And NumRecEnt.Text = "" Then
            NumRecEnt.Text = gSerieRec & "/" & gRec
        End If
        
        'Emitir o recibo
        Preenche_Recibo
        
        Verifica_estado_Combos
    End If

End Sub

Sub Grava_Ana_ARS()
    
    Dim i As Integer
    Dim sql As String

    If FlgARS = True Then
        For i = 1 To RegistosA.Count
            If RegistosA(i).NReqARS = "A gerar" And RegistosA(i).codAna <> "" Then
                RegistosA(i).NReqARS = CLng(Trim(EcNumReq.Text))
                FGAna.TextMatrix(i, 2) = RegistosA(i).NReqARS
            End If
        Next
        
        i = RegistosA.Count
        While i > 0
            If RegistosA(i).NReqARS <> "" And RegistosA(i).codAna <> "" Then
                
                sql = "INSERT INTO sl_credenciais " & _
                      "(n_req_orig, " & _
                       "n_req, " & _
                       "cod_ana, " & _
                       "p1 ) " & _
                      "VALUES " & _
                      "(" & CLng(EcNumReq.Text) & "," & _
                        UCase(BL_TrataStringParaBD(RegistosA(i).NReqARS)) & "," & _
                        UCase(BL_TrataStringParaBD(RegistosA(i).codAna)) & "," & _
                        BL_TrataStringParaBD(RegistosA(i).p1) & ")"
                
                BG_ExecutaQuery_ADO sql
                
                If gSQLError <> 0 Then
                    i = 0
                End If
            
            End If
            i = i - 1
        Wend
        
    End If

End Sub

Sub Grava_Ana_Req()
    
    Dim i As Integer
    Dim k As Integer
    Dim maximo As Long
    Dim sql As String
    Dim RsOrd As ADODB.recordset
    
    Set RsOrd = New ADODB.recordset
    
    If UBound(MaReq) = 0 Then
        BG_Mensagem mediMsgBox, "N�o foram registadas an�lises!", vbInformation, "Grava��o de an�lises"
        SSTGestReq.Tab = 2
    End If
    
    'Marca��o de an�lises (sl_marcacoes_consultas)
    For i = 1 To UBound(MaReq)
        MaReq(i).dt_chega = EcDataChegada.Text  'Bruno & sdo
        
        'S� s�o gravadas as an�lise "novas" ou que estavam na tabela de marca��es, as an�lises da tabela de realiza��es n�o s�o marcadas
            '-1 -> an�lise nova ou an�lise da tabela sl_marca�oes
            
        CmdOrdAna.Parameters("COD_ANA_S") = MaReq(i).cod_agrup
        CmdOrdAna.Parameters("COD_ANA_C") = MaReq(i).cod_agrup
        CmdOrdAna.Parameters("COD_PERFIS") = MaReq(i).cod_agrup
        
        
        Set RsOrd = CmdOrdAna.Execute
        
        If Not RsOrd.EOF Then
            If BL_HandleNull(RsOrd!ordem, 0) <> 0 Then
                MaReq(i).Ord_Ana = BL_HandleNull(RsOrd!ordem, 0)
            Else
                maximo = 0
                For k = 1 To UBound(MaReq)
                    If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
                Next k
                maximo = maximo + 1
                'Colocar a an�lise para o fim
                MaReq(i).Ord_Ana = maximo
            End If
            If RsOrd!gr_ana <> "" Then
                If EcGrupoAna.Text = "" Then
                    EcGrupoAna.Text = RsOrd!gr_ana
                Else
                    If InStr(1, EcGrupoAna.Text, RsOrd!gr_ana) <> 0 Then
                    Else
                        EcGrupoAna.Text = EcGrupoAna.Text & ";" & RsOrd!gr_ana
                    End If
                End If
            End If
        Else
            'Colocar a an�lise para o fim
            maximo = 0
            For k = 1 To UBound(MaReq)
                If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
            Next k
            maximo = maximo + 1
            'Colocar a an�lise para o fim
            MaReq(i).Ord_Ana = maximo
        End If
        RsOrd.Close
        
        'MaReq(i).Ord_Ana = i
        
        If MaReq(i).flg_estado = "-1" Then
            Dim RsTrans As ADODB.recordset
            'verifica se a flag "flg_transmanual" na an�lise est� a 1,
            'se sim coloca "flg_apar_trans" a 1 em sl_marcacoes_consultas
            Set RsTrans = New ADODB.recordset
            RsTrans.CursorLocation = adUseClient
            RsTrans.CursorType = adOpenStatic
            sql = "SELECT flg_transmanual FROM sl_ana_s WHERE cod_ana_s=" & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            RsTrans.Open sql, gConexao
            
            If (Not (RsTrans.EOF)) Then
            
                If (RsTrans!flg_transManual <> 1) Then
                    sql = "INSERT INTO sl_marcacoes_consultas(n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,flg_apar_trans,ord_marca"
                    sql = sql & ") VALUES (" & CLng(Trim(EcNumReq.Text)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s)) & "," & MaReq(i).Ord_Ana
                    sql = sql & "," & MaReq(i).Ord_Ana_Compl & "," & MaReq(i).Ord_Ana_Perf & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_agrup)) & "," & MaReq(i).N_Folha_Trab & "," & IIf((MaReq(i).dt_chega = ""), "NULL", BL_TrataDataParaBD(MaReq(i).dt_chega)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Flg_Apar_Transf)) & "," & i & ")"
                    BG_ExecutaQuery_ADO sql
                Else
                    sql = "INSERT INTO sl_marcacoes_consultas(n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,flg_apar_trans,ord_marca"
                    sql = sql & ") VALUES (" & CLng(Trim(EcNumReq.Text)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s)) & "," & MaReq(i).Ord_Ana
                    sql = sql & "," & MaReq(i).Ord_Ana_Compl & "," & MaReq(i).Ord_Ana_Perf & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_agrup)) & "," & MaReq(i).N_Folha_Trab & "," & IIf((MaReq(i).dt_chega = ""), "NULL", BL_TrataDataParaBD(MaReq(i).dt_chega)) & ",'2'," & i & ")"
                    BG_ExecutaQuery_ADO sql
                End If
            Else
                sql = "INSERT INTO sl_marcacoes_consultas(n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,flg_apar_trans,ord_marca"
                sql = sql & ") VALUES (" & CLng(Trim(EcNumReq.Text)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s)) & "," & MaReq(i).Ord_Ana
                sql = sql & "," & MaReq(i).Ord_Ana_Compl & "," & MaReq(i).Ord_Ana_Perf & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_agrup)) & "," & MaReq(i).N_Folha_Trab & "," & IIf((MaReq(i).dt_chega = ""), "NULL", BL_TrataDataParaBD(MaReq(i).dt_chega)) & ",'0'," & i & ")"
                BG_ExecutaQuery_ADO sql
            End If
            
            If gSQLError <> 0 Then
                Exit For
            End If
            
            RsTrans.Close
            Set RsTrans = Nothing
        End If
    Next i
    
    Set RsOrd = Nothing
    
End Sub

Sub Grava_Prod_Req()
    
    Dim sql As String
    Dim i As Integer
    Dim MenorData As String
    Dim DataPrevistaAux As String
    Dim TotalRegistos As Long
    Dim Perguntou As Boolean
     
    Perguntou = False
    If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
        'determinar a menor data da lista de produtos
        MenorData = RegistosP(1).DtPrev
        For i = 1 To RegistosP.Count - 1
            If (RegistosP(i).DtChega) <> "" Then
                If DateValue(RegistosP(i).DtPrev) < DateValue(MenorData) And DateValue(RegistosP(i).DtChega) > DateValue(EcDataPrevista) Then MenorData = RegistosP(i).DtPrev
            End If
        Next i
        If DateValue(MenorData) > DateValue(EcDataPrevista.Text) Then
            gMsgTitulo = "ATEN��O"
            gMsgMsg = "Existe um produto com data inferior � data prevista, deseja alterar a data prevista para " & MenorData & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                DataPrevistaAux = EcDataPrevista.Text
                EcDataPrevista = DateValue(MenorData)
                sql = "UPDATE sl_requis_consultas SET dt_previ=" & BL_TrataDataParaBD(EcDataPrevista.Text) & " WHERE dt_previ=" & BL_TrataDataParaBD(DataPrevistaAux) & " AND n_req=" & EcNumReq.Text
                BG_ExecutaQuery_ADO sql, gConexao
                If gSQLError <> 0 Then Exit Sub
            End If
        End If
        
        TotalRegistos = RegistosP.Count - 1
        For i = 1 To TotalRegistos
            If Trim(RegistosP(i).CodProd) <> "" Then
                If RegistosP(i).DtChega = "" And EcDataChegada.Text <> "" Then
                    'se a data de chegada estiver vazia vai preenche-la
                    If Flg_PreencherDtChega = True Then
                        RegistosP(i).DtChega = DateValue(EcDataChegada.Text)
                        FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
                    ElseIf Perguntou = False Then
                        If BG_Mensagem(mediMsgBox, "Deseja preencher a data de chegada dos produtos com a data '" & EcDataChegada.Text & "' ?", vbQuestion + vbYesNo, "Data Chegada") = vbYes Then
                            RegistosP(i).DtChega = DateValue(EcDataChegada.Text)
                            FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
                            Flg_PreencherDtChega = True
                        End If
                        Perguntou = True
                    End If
                End If
                sql = "INSERT INTO sl_req_prod_consultas(n_req,cod_prod,cod_especif,dt_previ,dt_chega) VALUES(" & EcNumReq.Text & _
                    "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodProd)) & "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodEspecif)) & "," & BL_TrataDataParaBD(RegistosP(i).DtPrev) & "," & BL_TrataDataParaBD(RegistosP(i).DtChega) & ")"
                BG_ExecutaQuery_ADO sql
                
                If gSQLError <> 0 Then
                    i = TotalRegistos + 1
                End If
            End If
        Next i
    
        If gSQLError <> 0 Then Exit Sub
        
        
        'Actualizar o estado da requisi��o se foi introduzida data de chegada (dt_chega)
        If Flg_PreencherDtChega Then
            sql = "UPDATE sl_requis_consultas SET estado_req='A' WHERE n_req=" & EcNumReq.Text
            BG_ExecutaQuery_ADO sql
        End If
    End If

End Sub
Sub Grava_Tubos_Req()
    
    Dim sql As String
    Dim i As Integer
    Dim MenorData As String
    Dim DataPrevistaAux As String
    Dim TotalRegistos As Long
    Dim Perguntou As Boolean
     
    Perguntou = False
    If RegistosT.Count > 0 And Trim(RegistosT(1).CodTubo) <> "" Then
        'determinar a menor data da lista de tubos
        MenorData = RegistosT(1).DtPrev
        For i = 1 To RegistosT.Count - 1
            If (RegistosT(i).DtChega) <> "" Then
                If DateValue(RegistosT(i).DtPrev) < DateValue(MenorData) And DateValue(RegistosT(i).DtChega) > DateValue(EcDataPrevista) Then MenorData = RegistosT(i).DtPrev
            End If
        Next i
        If DateValue(MenorData) > DateValue(EcDataPrevista.Text) Then
            gMsgTitulo = "ATEN��O"
            gMsgMsg = "Existe um tubo com data inferior � data prevista, deseja alterar a data prevista para " & MenorData & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                DataPrevistaAux = EcDataPrevista.Text
                EcDataPrevista = DateValue(MenorData)
                sql = "UPDATE sl_requis SET dt_previ=" & BL_TrataDataParaBD(EcDataPrevista.Text) & " WHERE dt_previ=" & BL_TrataDataParaBD(DataPrevistaAux) & " AND n_req=" & EcNumReq.Text
                BG_ExecutaQuery_ADO sql, gConexao
                If gSQLError <> 0 Then Exit Sub
            End If
        End If
        
        TotalRegistos = RegistosT.Count - 1
        For i = 1 To TotalRegistos
            If Trim(RegistosT(i).CodTubo) <> "" Then
                If RegistosT(i).DtChega = "" And EcDataChegada.Text <> "" Then
                    'se a data de chegada estiver vazia vai preenche-la
                    If Flg_PreencherDtChega = True Then
                        RegistosT(i).DtChega = DateValue(EcDataChegada.Text)
                        FGTubos.TextMatrix(i, 2) = RegistosT(i).DtChega
                    ElseIf Perguntou = False Then
    
                        If BG_Mensagem(mediMsgBox, "Deseja preencher a data de chegada dos tubos com a data '" & EcDataChegada.Text & "' ?", vbQuestion + vbYesNo, "Data Chegada") = vbYes Then
                            RegistosT(i).DtChega = DateValue(EcDataChegada.Text)
                            FGTubos.TextMatrix(i, 2) = RegistosT(i).DtChega
                            Flg_PreencherDtChega = True
                        
                        Perguntou = True
                        End If
                    End If
                End If
                sql = "INSERT INTO sl_req_tubo_consultas(n_req,cod_tubo,dt_previ,dt_chega,hr_chega) VALUES(" & EcNumReq.Text & _
                    "," & UCase(BL_TrataStringParaBD(RegistosT(i).CodTubo)) & "," & BL_TrataDataParaBD(RegistosT(i).DtPrev) & "," & BL_TrataDataParaBD(RegistosT(i).DtChega) & "," & BL_TrataStringParaBD(RegistosT(i).HrChega) & ")"
                BG_ExecutaQuery_ADO sql
                
                If gSQLError <> 0 Then
                    i = TotalRegistos + 1
                End If
            End If
        Next i
    
        If gSQLError <> 0 Then Exit Sub
        
        'Actualizar o estado da requisi��o se foi introduzida data de chegada (dt_chega)
        If Flg_PreencherDtChega Then
            sql = "UPDATE sl_requis SET estado_req='A' WHERE n_req=" & EcNumReq.Text
            BG_ExecutaQuery_ADO sql
        End If
    End If

End Sub

Sub Grava_DS_TM()
    
    Dim sql As String

    'Faz o UPDATE na tabela diagn�sticos secund�rios criados temporariamente com o NumeroSessao
    sql = "UPDATE sl_diag_sec SET n_req=" & CLng(EcNumReq.Text) & " WHERE n_req=" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    If gSQLError = 0 Then
        'Faz o UPDATE na tabela terap�uticas e medica��o criados temporariamente com o NumeroSessao
        sql = "UPDATE sl_tm_ute SET n_req=" & CLng(EcNumReq.Text) & " WHERE n_req=" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
    End If
    
End Sub

Sub Apaga_DS_TM()
    
    Dim sql As String

    'Delete dos diagnosticos secund�rios criados temporariamente com o NumeroSessao
    sql = "DELETE FROM sl_diag_sec " & _
          "WHERE n_req=" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    'BG_LogFile_Erros "FormGesReqCons: apagar diag. secund�rios -> (" & Sql & " ) ErrBD " & gSQLError
    
    'Delete das terap�uticas e medica��es criados temporariamente com o NumeroSessao
    sql = "DELETE FROM sl_tm_ute " & _
          "WHERE n_req=" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    ' BG_LogFile_Erros "FormGesReqCons: apagar terap./medica��o -> (" & Sql & " ) ErrBD " & gSQLError
    
End Sub

Sub Grava_Req_Assoc()

    Dim sql As String
    
    If EcReqAssociada <> "" Then
        'Para evitar associar uma requisi��o a ela pr�pria
        If EcReqAssociada.Text = EcNumReq.Text Then
            BG_Mensagem mediMsgBox, "N�o pode associar uma requisi��o a ela pr�pria !", vbInformation, ""
            EcReqAssociada.Text = ""
            EcReqAssociada.SetFocus
        Else
            sql = "UPDATE sl_requis_consultas SET n_req_assoc=" & CLng(Trim(EcReqAssociada.Text)) & " WHERE n_req=" & CLng(Trim(EcReqAssociada.Text))
            BG_ExecutaQuery_ADO sql
            
            If gSQLError = 0 Then
                sql = "UPDATE sl_requis_consultas SET n_req_assoc=" & CLng(Trim(EcNumReq.Text)) & " WHERE n_req=" & CLng(Trim(EcReqAssociada.Text))
                BG_ExecutaQuery_ADO sql
            End If
        End If
    End If

End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    
    If gPreencheDatasReq = mediSim Then
        If EcDataPrevista.Text = "" Then
            EcDataPrevista.Text = Bg_DaData_ADO
        End If
        If EcDataChegada.Text = "" Then
            EcDataChegada.Text = Bg_DaData_ADO
        End If
    End If
    
    If Trim(EcEpisodio.Text) = "" And gPreencheEpis = 1 Then
        DevolveEpisodio EcSeqUtente.Text
    End If
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        'Impedir que se alterem valores de cria��o / impress�o de registos
        EcDataCriacao = BL_HandleNull(rs!dt_cri, "")
        EcUtilizadorCriacao = BL_HandleNull(rs!user_cri, "")
        EcHoraCriacao = BL_HandleNull(rs!hr_cri, "")
        
        EcDataImpressao = BL_HandleNull(rs!dt_imp, "")
        EcUtilizadorImpressao = BL_HandleNull(rs!user_imp, "")
        EcHoraImpressao = BL_HandleNull(rs!hr_imp, "")
        
        EcDataImpressao2 = BL_HandleNull(rs!dt_imp2, "")
        EcUtilizadorImpressao2 = BL_HandleNull(rs!user_imp2, "")
        EcHoraImpressao2 = BL_HandleNull(rs!hr_imp2, "")
        
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = Bg_DaHora_ADO
        
        'se a data prevista estiver vazia
        If EcDataPrevista.Text = "" Then
            EcDataPrevista.Text = Format(Bg_DaData_ADO, gFormatoData)
        End If
        
        
        If UBound(MaReq) > 0 Then
            If Trim(MaReq(1).cod_ana_s) <> "" Then
                If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                    CbEstadoReqAux.ListIndex = CbEstadoReq.ListIndex
                ElseIf CbEstadoReqAux.Text = "I" Then
                    CbEstadoReqAux.Text = "A"
                End If
            Else
                CbEstadoReqAux.ListIndex = 0
            End If
        Else
            CbEstadoReqAux.ListIndex = 0
        End If
        
        'Se o utente � isento, � obrigat�rio indicar o motivo
        If CkIsento.value = vbChecked And CbTipoIsencao.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Tipo de isen��o obrigat�rio!", vbOKOnly + vbInformation, "Insen��es"
            CbTipoIsencao.SetFocus
            Exit Sub
        End If
        If CkIsento.value = vbUnchecked Or CkIsento.value = vbGrayed Then
            CbTipoIsencao.ListIndex = -1
        End If
        
        If Not ValidaEspecifObrig Then Exit Sub
                
        EcEstadoReq.Text = Mid(CbEstadoReqAux.Text, 1, 1)
        EcPagUte.Text = Mid(CbPagUte.Text, 1, 1)
        EcTaxaModeradora.Text = Mid(CbTaxaModeradora.Text, 1, 1)
        EcPagEnt.Text = Mid(CbPagEnt.Text, 1, 1)
        If ValidaNumBenef = True Then
            iRes = ValidaCamposEc
            If iRes = True Then
            
                BD_Update
                
                If Trim(EcNumReq.Text) <> "" Then
                    'Se n�o houve erro
                    If gRequisicaoActiva = 0 Then
                        BtDadosAnteriores.Enabled = False
                        LaCopiarAnterior.Enabled = False
                    Else
                        BtDadosAnteriores.Enabled = True
                        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
                        LaCopiarAnterior.Enabled = True
                    End If
                End If
            End If
        End If
    End If

End Sub

Function ValidaNumBenef() As Boolean

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim Formato As Boolean
    
    If EcCodEFR.Text <> "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseClient
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.Text
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = BL_HandleNull(Tabela!Formato1, "")
            Formato2 = BL_HandleNull(Tabela!Formato2, "")
            Sendkeys ("{END}")
        End If
        Formato = Verifica_Formato(UCase(EcNumBenef.Text), Formato1, Formato2)
        If Formato = False And EcNumBenef.Text <> "" Then
            Sendkeys ("{END}")
            EcNumBenef.SetFocus
            ValidaNumBenef = False
        Else
            ValidaNumBenef = True
        End If
    End If

End Function

Function Verifica_Formato(NumBenef As String, Formato1 As String, Formato2 As String) As Boolean
    
    If Trim(Formato1) = "" And Trim(Formato2) = "" Then
        Verifica_Formato = True
        Exit Function
    End If
    
    If Trim(Formato1) = "" Then
        Verifica_Formato = False
    End If
    If Len(NumBenef) <> Len(Formato1) Then
        Verifica_Formato = False
    Else
        Verifica_Formato = Percorre_Formato(NumBenef, Formato1)
    End If
    
    'caso o formato 1 n�o esteja correcto vai verificar o formato 2
    If Verifica_Formato = False Then
        If Trim(Formato2) = "" Then
            Verifica_Formato = False
        End If
        If Len(NumBenef) <> Len(Formato2) Then
            Verifica_Formato = False
        Else
            Verifica_Formato = Percorre_Formato(NumBenef, Formato2)
        End If
        If Verifica_Formato = False And EcNumBenef.Text <> "" Then
            gMsgTitulo = "Aten��o"
            gMsgMsg = "N�mero Benefici�rio incorrecto." & Chr(13) & "Deseja continuar ?" & Chr(13) & Chr(13) & "Exemplo de formato(s) correcto(s): " & Chr(13) & "       " & Formato1 & IIf(Trim(Formato1) <> "" And Trim(Formato2) <> "", ", ", "") & Formato2
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbInformation, gMsgTitulo)
            If gMsgResp = vbYes Then
                Verifica_Formato = True
            Else
                Verifica_Formato = False
            End If
        End If
    End If

End Function

Function Percorre_Formato(NumBenef As String, Formato As String) As Boolean
    
    Dim pos As Integer
    Dim comp As Integer
    Dim CharActualF As String
    Dim CharActualN As String
    
    Percorre_Formato = True
    comp = Len(Formato)
    pos = 1
    Do While pos <= comp
        CharActualF = Mid(Formato, pos, 1)
        CharActualN = Mid(NumBenef, pos, 1)
        Select Case CharActualF
            Case "A"
                If CharActualN < "A" Or CharActualN > "Z" Then
                    Percorre_Formato = False
                End If
            Case "9"
                If CharActualN < "0" Or CharActualN > "9" Then
                    Percorre_Formato = False
                End If
            Case " "
                If CharActualN <> " " Then
                    Percorre_Formato = False
                End If
        End Select
        pos = pos + 1
    Loop

End Function

Sub BD_Update()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim k As Integer
    Dim ProdJaChegou As Boolean
    
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    
    BG_BeginTransaction
        
    'actualizar o numero de requisi��o associada
    Grava_Req_Assoc
    If gSQLError <> 0 Then
        'Erro a actualizar o numero de requisi��o associada
        BG_Mensagem mediMsgBox, "Erro a actualizar n�mero da requisi��o associada !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
        
    'actualizar os produtos da requisi��o
    UPDATE_Prod_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar os tubos da requisi��o
    UPDATE_Tubos_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
            
    'Actualizar estado da requisi��o em conformidade com an�lises marcadas, produtos marcados e sua data de chegada
'    If CbEstadoReqAux.Text = "" Or CbEstadoReqAux.Text = "I" Or CbEstadoReqAux.Text = "A" Or CbEstadoReqAux.Text = " " Then
'        ProdJaChegou = False
'        If FGAna.Rows >= 1 And Trim(FGAna.TextMatrix(1, 0)) <> "" Then
'            If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
'                For k = 1 To RegistosP.Count - 1
'                    If Trim(RegistosP(1).DtChega) <> "" Then
'                        ProdJaChegou = True
'                    End If
'                Next k
'                If ProdJaChegou = True Then
'                    CbEstadoReqAux.Text = "A"
'                Else
'                    CbEstadoReqAux.Text = "I"
'                End If
'            Else
'                CbEstadoReqAux.Text = "A"
'                EcDataChegada.Text = Bg_DaData_ADO
'            End If
'        Else
'            CbEstadoReqAux.Text = " "
'        End If
'    End If
    
             
    'actualizar as an�lises da requisi��o
    UPDATE_Ana_Req
    If gSQLError <> 0 Then
        'Erro a actualizar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
                
    'actualizar as an�lises da requisi��o da entidade gARS
    UPDATE_Ana_gARS
    If gSQLError <> 0 Then
        'Erro a actualizar as an�lises da requisi��o da entidade gARS
        BG_Mensagem mediMsgBox, "Erro a actualizar as an�lises gARS da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
                        
    'actualizar as terap�uticas e medica��es da requisi��o
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar as terap�uticas e medica��es da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as terap�uticas e medica��es da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Actualiza agenda
    Actualiza_Agenda
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar agenda!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
   
    'gravar restantes dados da requisicao
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
   
    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        gRequisicaoActiva = CLng(EcNumReq)
        BG_CommitTransaction
        
        EcEstadoReq.Text = BL_MudaEstadoReq(CLng(EcNumReq))
 
        For k = 0 To CbEstadoReqAux.ListCount - 1
            If CbEstadoReqAux.List(k) = EcEstadoReq.Text Then
                CbEstadoReq.ListIndex = k
                Exit Sub
            End If
        Next k
        
        'Preenche_Recibo_Isencao
        'UPDATE_Recibos
    End If
    
    MarcaLocal = rs.Bookmark
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rs.Bookmark = MarcaLocal
    
    CbSituacao.SetFocus
    Exit Sub
    
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a modificar requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGesReqCons: BD_Update -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar

End Sub

Sub UPDATE_Recibos()
    
    Dim sql As String
    
    'Combos pagamento TaxaModeradora ou PagaUte Ou PagEnt foram alteradas
    If (CbTMIndex <> CbTaxaModeradora.ListIndex And CbTaxaModeradora.ListIndex = 0) Or _
       (CbEntIndex <> CbPagEnt.ListIndex And CbPagEnt.ListIndex = 0) Or _
       (CbUteIndex <> CbPagUte.ListIndex And CbPagUte.ListIndex = 0) Then
        'Grava_Recibos
    End If
    
End Sub

Sub UPDATE_Ana_gARS()

    Dim sql As String

    gSQLError = 0
    sql = "DELETE FROM sl_credenciais WHERE n_req_orig=" & CLng(EcNumReq.Text)
    BG_ExecutaQuery_ADO sql
    
    If gSQLError <> 0 Then
        Exit Sub
    End If
    
    Grava_Ana_ARS

End Sub

Sub UPDATE_Prod_Req()
   
   Dim sql As String
    
   gSQLError = 0
   sql = "DELETE FROM sl_req_prod_consultas WHERE n_req=" & Trim(EcNumReq.Text)
   BG_ExecutaQuery_ADO sql
   
   If gSQLError <> 0 Then
        Exit Sub
   End If
   Grava_Prod_Req

End Sub
Sub UPDATE_Tubos_Req()
   
   Dim sql As String
    
   gSQLError = 0
   sql = "DELETE FROM sl_req_tubo_consultas WHERE n_req=" & Trim(EcNumReq.Text)
   BG_ExecutaQuery_ADO sql
   
   If gSQLError <> 0 Then
        Exit Sub
   End If
   Grava_Tubos_Req

End Sub

Sub UPDATE_Ana_Req()
    
    Dim sql As String
    
    gSQLError = 0
    'Apagar todas as requisi��es que n�o t�m dt_chega
    sql = "DELETE FROM sl_marcacoes_consultas WHERE n_req=" & Trim(EcNumReq.Text)
    BG_ExecutaQuery_ADO sql
    
    If gSQLError <> 0 Then
        Exit Sub
    End If

    EcGrupoAna.Text = ""
    
    Call Grava_Ana_Req

End Sub

Sub FuncaoRemover()
    On Error GoTo TrataErro
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer cancelar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    
    If gMsgResp = vbYes Then
        BG_BeginTransaction
        BL_InicioProcessamento Me, "A cancelar registo."
        CkFlgInvisivel.value = vbChecked
        'edgar.parada - CHMA-1412 02.10.2019
        'BD_Update
        BD_Delete
        '
        BL_FimProcessamento Me
        BG_CommitTransaction
    End If
    'edgar.parada - CHMA-1412 02.10.2019
    Call FuncaoProcurar
    '
    Exit Sub
    
TrataErro:
    BG_RollbackTransaction
    BG_Mensagem mediMsgBox, "Erro a remover o registo!", vbError, "Remover"
End Sub

Sub FuncaoProcurar()
          
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    On Error GoTo Trata_Erro
    
    Set rs = New ADODB.recordset
    EcReqAux.Text = UCase(EcReqAux.Text)
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos as requisi��es ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set rs = Nothing
            Exit Sub
        End If
    
        CriterioTabela = CriterioTabela & "  ORDER BY n_req DESC"
    Else
    End If
              
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    'BRUNODSANTOS 10.11.2017 HDES-6196
    'rs.CursorLocation = adUseServer substituido por adUseClient em todo o form
    rs.CursorLocation = adUseClient
    '
    rs.LockType = adLockReadOnly
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        BL_ToolbarEstadoN estado
        
        'inicializa��es da lista de marca��es de an�lises
        Inicializa_Estrutura_Analises
        
        'limpar grids
        LimpaFGAna
        LimpaFgProd
        LimpaFgTubos
        
        Call PreencheCampos
        
        ' Se o Utente n�o est� isento, calcula o recibo e activa o tab.
        If (CkIsento.value = 0) Then
            
            
            
            SSTGestReq.TabEnabled(4) = True
            DoEvents
            DoEvents
        Else
            SSTGestReq.TabEnabled(4) = False
            DoEvents
            DoEvents
        End If
        
        Set CampoDeFocus = EcReqAssociada
        
        BL_FimProcessamento Me
        gRequisicaoActiva = CLng(EcNumReq.Text)
        BtDadosAnteriores.Enabled = True
        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
        LaCopiarAnterior.Enabled = True

        Me.BtPesquisaUtente.Enabled = False
        
        If Flg_Executa_BtPassaMarcacao = True And CDate(Bg_DaData_ADO) >= CDate(EcDataPrevista) Then
            BtPassaMarcacao_Click
            Flg_Executa_BtPassaMarcacao = False
        End If
    
    End If
    
    Exit Sub
    
Trata_Erro:
    
    BG_LogFile_Erros Me.Name & ": FuncaoProcurar (" & Err.Number & "-" & Err.Description & ")"
    Err.Clear
    BL_FimProcessamento Me
    BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
    FuncaoLimpar
    Resume Next

End Sub

Sub Funcao_CopiaReq()
    
    If gRequisicaoActiva <> 0 Then
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If

End Sub

Sub Verifica_estado_Combos()
    
    Dim RsEFR As ADODB.recordset
    Dim sql As String

    BtAnula.Enabled = False
    BtReimpr.Enabled = False
    
    'O ListIndex=0 corresponde � op��o pago(n�o � permitido alterar)
    CbTMIndex = CbTaxaModeradora.ListIndex
    If CbTaxaModeradora.ListIndex = 0 Then
        CbTaxaModeradora.Enabled = False
        BtAnula.Enabled = True
        BtReimpr.Enabled = True
    End If
    CbEntIndex = CbPagEnt.ListIndex
    If CbPagEnt.ListIndex = 0 Then
        CbPagEnt.Enabled = False
        BtAnula.Enabled = True
        BtReimpr.Enabled = True
    End If
    CbUteIndex = CbPagUte.ListIndex
    If CbPagUte.ListIndex = 0 Then
        CbPagUte.Enabled = False
        BtAnula.Enabled = True
        BtReimpr.Enabled = True
    End If
    
    If Trim(EcTEFR) = "" And Trim(EcCodEFR.Text) <> "" Then
        sql = "SELECT t_efr FROM sl_efr WHERE cod_efr=" & CLng(Trim(EcCodEFR.Text))
        Set RsEFR = New ADODB.recordset
        RsEFR.CursorLocation = adUseClient
        RsEFR.CursorType = adOpenStatic
        RsEFR.Open sql, gConexao
        If RsEFR.RecordCount > 0 Then
            EcTEFR.Text = Trim(BL_HandleNull(RsEFR!t_efr, ""))
        End If
        RsEFR.Close
        Set RsEFR = Nothing
    End If
    
    If (CbPagUte.ListIndex = 0 Or CbPagEnt.ListIndex = 0 Or CbTaxaModeradora.ListIndex = 0) And _
        Trim(EcTEFR.Text) = gEFRIndependente Then
        EcCodEFR.Locked = True
        EcNumBenef.Locked = True
        BtPesquisaEntFin.Enabled = False
        EcCodEFR.ForeColor = vbGrayText
        EcNumBenef.ForeColor = vbGrayText
        CkIsento.Enabled = False
        CbTipoIsencao.Enabled = False
        EcCodEFR.ToolTipText = "N�o � possivel alterar: a req. tem recibos duma entidade independente. Anule os recibos para poder alterar."
        EcNumBenef.ToolTipText = "N�o � possivel alterar: a req. tem recibos duma entidade independente. Anule os recibos para poder alterar."
    Else
        EcCodEFR.Locked = False
        EcNumBenef.Locked = False
        CkIsento.Enabled = True
        CbTipoIsencao.Enabled = True
        BtPesquisaEntFin.Enabled = True
        EcCodEFR.ForeColor = vbBlack
        EcNumBenef.ForeColor = vbBlack
        EcCodEFR.ToolTipText = ""
        EcNumBenef.ToolTipText = ""
        BtPesquisaEntFin.ToolTipText = ""
    End If
    
End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        EcUtente.Text = ""
        DoEvents
        
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub LimpaCampos()
    
    'Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    
    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    
    CbPrioColheita.ListIndex = mediComboValorNull
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbEstadoReq.ListIndex = mediComboValorNull
    CbEstadoReqAux.ListIndex = mediComboValorNull
    EcProcHosp1.Text = ""
    EcProcHosp2.Text = ""
    EcNumCartaoUte.Text = ""
    EcDataNasc.Text = ""
    EcNome.Text = ""
    EcUtente.Text = ""
    EcPesqRapMedico.Text = ""
    EcDescrMedico.Text = ""
    EcDescrMedico2.Text = ""
    EcDescrArquivo.Text = ""
    EcDescrEFR.Text = ""
    EcPesqRapProven.Text = ""
    EcPesqRapProduto.Text = ""
    EcPesqRapTubo.Text = ""
    EcDescrProveniencia.Text = ""
    EcDataNasc.Text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrTubos.Enabled = False
    FrBtTubos.Enabled = False
    SSTGestReq.TabEnabled(5) = False
    EcCodEFR.Locked = False
    EcNumBenef.Locked = False
    BtPesquisaEntFin.Enabled = True
    EcCodEFR.ToolTipText = ""
    EcNumBenef.ToolTipText = ""
    BtPesquisaEntFin.ToolTipText = ""
    EcCodEFR.ForeColor = vbBlack
    EcNumBenef.ForeColor = vbBlack
    CkIsento.Enabled = True
    CbTipoIsencao.Enabled = True
    gEpisodioActivo = ""
    gSituacaoActiva = mediComboValorNull
    EcUtente.Text = ""
    EcDescrValencia = ""
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    
    'Fun��o que limpa os tubos da requisi��o
    LaTubos.caption = ""
    
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
     
    'Limpar treeview
    TAna.Nodes.Clear
         
    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.Locked = False
    
    EcEpisodio.Enabled = True
    CbTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcDataPrevista.Enabled = True
    EcDataChegada.Enabled = True
    
    Me.BtPassaMarcacao.Enabled = False
    BtInfClinica.Enabled = False
    LaInfCli.Enabled = False
    Set CampoDeFocus = EcNumReq
    
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    LimpaFgProd
    FGAna.Cols = 2
    LimpaFGAna
    
    SSTGestReq.Tab = 0
    DoEvents
    
    'Limpa Recibos
    CbTaxaModeradora.ListIndex = mediComboValorNull
    CbPagEnt.ListIndex = mediComboValorNull
    CbPagUte.ListIndex = mediComboValorNull
    ValorTM.Text = ""
    ValorEnt.Text = ""
    ValorDoe.Text = ""
    DataTM.Text = ""
    DataEnt.Text = ""
    DataDoe.Text = ""
    NumRecTM.Text = ""
    NumRecEnt.Text = ""
    NumRecDoe.Text = ""
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabVisible(3) = False
    SSTGestReq.TabEnabled(4) = False
    ContP1 = 1
    
    'Colocar as frames de pagamentos enabled
    CbTaxaModeradora.Enabled = True
    CbPagEnt.Enabled = True
    CbPagUte.Enabled = True
    'Inicializa��o das variaveis que guardam os indices nas combos de pagamentos
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1
    
    CkIsento.value = vbGrayed
    CbTipoIsencao.ListIndex = mediComboValorNull
    
    ListaRecibos.Clear
    FrListaRecibos.Visible = False
    CbTipoAnulacao.Visible = False
    CbTipoAnulacao.ListIndex = -1
    BtAnula.Enabled = False
    BtReimpr.Enabled = False
    FuncaoListaRecibos = ""
    
    Call Apaga_DS_TM
    
    BtEtiq.Enabled = False
    BtNorma.Enabled = False
    LaEtiq.Enabled = False
    
    ' Pesquisa R�pida de Utente.
    Me.BtPesquisaUtente.Enabled = True
    
    Flg_DadosAnteriores = False

    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Flg_Executa_BtPassaMarcacao = False
    EcPrescricao.Locked = False
    CkFlgInvisivel.value = vbGrayed
    CkFlgInvisivel.Visible = False

End Sub

Sub FuncaoAnterior()
    
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BG_MensagemAnterior
    Else
        SSTGestReq.TabEnabled(4) = False
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = True
        Call PreencheCampos
        
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = "C" Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(5) = True
            If gRecibos = "1" Then
                SSTGestReq.TabEnabled(3) = True
                SSTGestReq.TabVisible(3) = False
                SSTGestReq.TabEnabled(4) = True
            Else
                SSTGestReq.TabEnabled(3) = False
                SSTGestReq.TabVisible(3) = False
                SSTGestReq.TabEnabled(4) = False
            End If
            BtCancelarReq.Visible = False
        
            ' Se o Utente n�o est� isento, calcula o recibo e activa o tab.
            If (CkIsento.value = 0) Then
                
                SSTGestReq.TabEnabled(4) = True
                DoEvents
                DoEvents
            Else
                SSTGestReq.TabEnabled(4) = False
                DoEvents
                DoEvents
            End If
        End If
    End If

End Sub

Sub FuncaoSeguinte()

    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BG_MensagemSeguinte
    Else
        SSTGestReq.TabEnabled(4) = False
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = True
        Call PreencheCampos
        
        ' Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados.
        If rs!estado_req = "C" Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(5) = True
            If gRecibos = "1" Then
                SSTGestReq.TabEnabled(3) = True
                SSTGestReq.TabVisible(3) = False
                SSTGestReq.TabEnabled(4) = True
            Else
                SSTGestReq.TabEnabled(3) = False
                SSTGestReq.TabVisible(3) = False
                SSTGestReq.TabEnabled(4) = False
            End If
            BtCancelarReq.Visible = False
            
            ' Se o Utente n�o est� isento, calcula o recibo e activa o tab.
            If (CkIsento.value = 0) Then
                
                SSTGestReq.TabEnabled(4) = True
                DoEvents
                DoEvents
            Else
                SSTGestReq.TabEnabled(4) = False
                DoEvents
                DoEvents
            End If
        End If
    End If

End Sub

Sub PreencheCampos()
    Dim i As Integer
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BtCancelarReq.Visible = True
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        EcPrescricao.Locked = False
        
        gEpisodioActivo = EcEpisodio.Text

        gSituacaoActiva = CbSituacao.ItemData(CbSituacao.ListIndex)
        
        If Trim(EcCodIsencao) <> "" Then
            CkIsento.value = Checked
            CbTipoIsencao.Enabled = True
        Else
            CkIsento.value = Unchecked
            CbTipoIsencao.Enabled = False
        End If
        BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao
        If CkFlgInvisivel.value = vbChecked Then
            CkFlgInvisivel.Visible = True
        Else
            CkFlgInvisivel.Visible = False
        End If
        LbNomeUtilCriacao.caption = BL_SelNomeUtil(EcUtilizadorCriacao.Text)
        LbNomeUtilCriacao.ToolTipText = LbNomeUtilCriacao.caption
        LbNomeUtilAlteracao.caption = BL_SelNomeUtil(EcUtilizadorAlteracao.Text)
        LbNomeUtilAlteracao.ToolTipText = LbNomeUtilAlteracao.caption
        LbNomeUtilImpressao.caption = BL_SelNomeUtil(EcUtilizadorImpressao.Text)
        LbNomeUtilImpressao.ToolTipText = LbNomeUtilImpressao.caption
        LbNomeUtilImpressao2.caption = BL_SelNomeUtil(EcUtilizadorImpressao2.Text)
        LbNomeUtilImpressao2.ToolTipText = LbNomeUtilImpressao2.caption

        BL_PreencheDadosUtente EcSeqUtente.Text
        PreencheDescEntFinanceira
        EcCodProveniencia_Validate False
        EcCodValencia_Validate False
        PreencheDescMedico
        PreencheDescMedico2
        PreencheDescArquivo
        PreencheUrgencia
        PreencheTaxaModeradora
        PreenchePagamentoEntidade
        PreenchePagamentoUtente
        PreencheEstadoRequisicao
        PreencheDadosUtente
        ' PFerreira 15.03.2007
        If (Trim(EcPrColheita <> "")) Then
            BL_ColocaTextoCombo "sl_cod_prioridades", "seq_prio", "cod_prio", EcPrColheita, CbPrioColheita
        ElseIf (Trim(EcDataNasc) <> "" And IsChild) Then
            CbPrioColheita.ListIndex = 3
        Else
            CbPrioColheita.ListIndex = 1
        End If
                
        'Verificar o estado das combos(Taxa,Ent,Ute)
        Verifica_estado_Combos
        'colocar o bot�o de cancelar requisi��es activo
        gRequisicaoActiva = CLng(EcNumReq.Text)
        gFlg_JaExisteReq = True
        
        ValidarARS EcCodEFR
        
        'colocar cabe�alho  e data prevista enabled a false
'        EcNumReq.Enabled = False
        EcNumReq.Locked = True
        
        EcEpisodio.Enabled = False
        CbTipoUtente.Enabled = False
        EcUtente.Enabled = False
        EcDataPrevista.Enabled = True
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FrTubos.Enabled = True
        FrBtTubos.Enabled = True
        If Trim(EcDataChegada.Text) <> "" Then
            EcDataChegada.Enabled = False
        End If
        FuncaoProcurar_PreencheProd CLng(Trim(EcNumReq.Text))
        FuncaoProcurar_PreencheAna CLng(Trim(EcNumReq.Text))
        FuncaoProcurar_PreencheTubo CLng(Trim(EcNumReq.Text))
        'CbSituacao.SetFocus
        EcNumReq.SetFocus
        
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = "C" Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            BtEtiq.Enabled = False
            BtNorma.Enabled = False
            LaEtiq.Enabled = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(5) = True
            If CkIsento.value = vbUnchecked Or CkIsento.value = vbGrayed Then
                If gRecibos = "1" Then
                    SSTGestReq.TabEnabled(3) = True
                    SSTGestReq.TabVisible(3) = False
                    SSTGestReq.TabEnabled(4) = True
                Else
                    SSTGestReq.TabEnabled(3) = False
                    SSTGestReq.TabVisible(3) = False
                    SSTGestReq.TabVisible(3) = False
                    SSTGestReq.TabEnabled(4) = False
                End If
            Else
                SSTGestReq.TabEnabled(3) = False
                SSTGestReq.TabEnabled(4) = False
            End If
            
            BtCancelarReq.Visible = False
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
            
            BtEtiq.Enabled = True
            BtNorma.Enabled = True
            LaEtiq.Enabled = True
        EcPrescricao.Locked = False
        End If
                
    End If
End Sub

Sub PreencheEstadoRequisicao()
    
    Select Case (BL_HandleNull(rs!estado_req, ""))
        Case gEstadoReqSemAnalises
            CbEstadoReq.ListIndex = 0
            CbEstadoReqAux.ListIndex = 0
        Case gEstadoReqEsperaProduto
            CbEstadoReq.ListIndex = 1
            CbEstadoReqAux.ListIndex = 1
        Case gEstadoReqEsperaResultados
            CbEstadoReq.ListIndex = 2
            CbEstadoReqAux.ListIndex = 2
        Case gEstadoReqEsperaValidacao
            CbEstadoReq.ListIndex = 3
            CbEstadoReqAux.ListIndex = 3
        Case gEstadoReqCancelada
            CbEstadoReq.ListIndex = 4
            CbEstadoReqAux.ListIndex = 4
        Case gEstadoReqValicacaoMedicaCompleta
            CbEstadoReq.ListIndex = 5
            CbEstadoReqAux.ListIndex = 5
        Case gEstadoReqNaoPassarHistorico
            CbEstadoReq.ListIndex = 6
            CbEstadoReqAux.ListIndex = 6
        Case gEstadoReqTodasImpressas
            CbEstadoReq.ListIndex = 7
            CbEstadoReqAux.ListIndex = 7
        Case gEstadoReqHistorico
            CbEstadoReq.ListIndex = 8
            CbEstadoReqAux.ListIndex = 8
        Case gEstadoReqResultadosParciais
            CbEstadoReq.ListIndex = 9
            CbEstadoReqAux.ListIndex = 9
        Case gEstadoReqValidacaoMedicaParcial
            CbEstadoReq.ListIndex = 10
            CbEstadoReqAux.ListIndex = 10
        Case gEstadoReqImpressaoParcial
            CbEstadoReq.ListIndex = 11
            CbEstadoReqAux.ListIndex = 11
        Case gEstadoReqBloqueada
            CbEstadoReq.ListIndex = 12
            CbEstadoReqAux.ListIndex = 12
        Case Else
            CbEstadoReq.ListIndex = mediComboValorNull
            CbEstadoReqAux.ListIndex = mediComboValorNull
    End Select
    
End Sub

Sub PreenchePagamentoUtente()
    
    Dim rsUte As ADODB.recordset
    Dim aux As String
    
    If rs!pag_ute = "P" Then
        CbPagUte.ListIndex = 0
        
        Set rsUte = New ADODB.recordset
        
        With rsUte
            .Source = "SELECT " & _
                      "     serie_rec, " & _
                      "     n_rec, " & _
                      "     dt_emi, " & _
                      "     val_pag_ute, " & _
                      "     cod_moeda " & _
                      "FROM " & _
                      "     sl_recibos " & _
                      "WHERE " & _
                      "     n_req       = " & EcNumReq & " AND " & _
                      "     val_pag_ute IS NOT NULL AND " & _
                      "     dt_anul     IS NULL"
                      
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .Open , gConexao
        End With
        
        If Not rsUte.EOF Then
            ValorDoe = BL_HandleNull(rsUte!Val_Pag_Ute, 0)
            NumRecDoe = BL_HandleNull(rsUte!serie_rec, "") & "/" & BL_HandleNull(rsUte!n_rec, "")
            DataDoe = BL_HandleNull(rsUte!dt_emi, "")
            
            If BL_HandleNull(rsUte!cod_moeda, "") <> gMoedaActiva Then
                If BL_HandleNull(rsUte!cod_moeda, "") = "PTE" Then
                    aux = BL_String2Double(ValorDoe) / gConvEur
                    ValorDoe = BL_String2Double(aux, 3)
                ElseIf BL_HandleNull(rsUte!cod_moeda, "") = "EUR" Then
                    aux = BL_String2Double(ValorDoe) * gConvEur
                    ValorDoe = BL_String2Double(aux, 3)
                End If
            End If
        Else
            CbPagEnt.ListIndex = -1
            CbPagEnt.Enabled = True
            NumRecDoe = ""
            DataDoe = ""
        End If
            
        rsUte.Close
        Set rsUte = Nothing
                
    ElseIf rs!pag_ute = "N" Then
        CbPagUte.ListIndex = 1
    ElseIf rs!pag_ute = "R" Then
        CbPagUte.ListIndex = 2
    End If

End Sub

Sub PreenchePagamentoEntidade()
    
    Dim RsEnt As ADODB.recordset
    Dim aux As String
    
    If rs!pag_ent = "P" Then
        CbPagEnt.ListIndex = 0
            
        Set RsEnt = New ADODB.recordset
        
        With RsEnt
            .Source = "SELECT " & _
                      "     serie_rec, " & _
                      "     n_rec, " & _
                      "     dt_emi, " & _
                      "     val_pag_ent, " & _
                      "     cod_moeda " & _
                      "FROM " & _
                      "     sl_recibos " & _
                      "WHERE " & _
                      "     n_req       = " & EcNumReq & " AND " & _
                      "     val_pag_ent is NOT NULL AND " & _
                      "     dt_anul     IS NULL"
            
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .Open , gConexao
        End With
        
        If Not RsEnt.EOF Then
            ValorEnt = BL_HandleNull(RsEnt!val_pag_ent, 0)
            NumRecEnt = BL_HandleNull(RsEnt!serie_rec, "") & "/" & BL_HandleNull(RsEnt!n_rec, "")
            DataEnt = BL_HandleNull(RsEnt!dt_emi, "")
            
            If BL_HandleNull(RsEnt!cod_moeda, "") <> gMoedaActiva Then
                If BL_HandleNull(RsEnt!cod_moeda, "") = "PTE" Then
                    aux = BL_String2Double(ValorEnt) / gConvEur
                    ValorEnt = BL_String2Double(aux, 3)
                ElseIf BL_HandleNull(RsEnt!cod_moeda, "") = "EUR" Then
                    aux = BL_String2Double(ValorEnt) * gConvEur
                    ValorEnt = BL_String2Double(aux, 3)
                End If
            End If
        Else
            CbPagEnt.ListIndex = -1
            CbPagEnt.Enabled = True
            NumRecEnt = ""
            DataEnt = ""
        End If
            
        RsEnt.Close
        Set RsEnt = Nothing
    ElseIf rs!pag_ent = "N" Then
        CbPagEnt.ListIndex = 1
    ElseIf rs!pag_ent = "R" Then
        CbPagEnt.ListIndex = 2
    End If
End Sub

Sub PreencheTaxaModeradora()
    
    Dim RsTx As ADODB.recordset
    Dim aux As String
    
    If rs!pag_tax = "P" Then
        CbTaxaModeradora.ListIndex = 0
        
        Set RsTx = New ADODB.recordset
        
        With RsTx
            .Source = "SELECT " & _
                      "     serie_rec, n_rec, dt_emi, val_taxa, cod_moeda " & _
                      "FROM " & _
                      "     sl_recibos " & _
                      "WHERE " & _
                      "     n_req    = " & EcNumReq & " AND " & _
                      "     val_taxa IS NOT NULL AND " & _
                      "     dt_anul  IS NULL"
                      
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .Open , gConexao
        End With
        
        If Not RsTx.EOF Then
            ValorTM = BL_HandleNull(RsTx!val_Taxa, 0)
            NumRecTM = BL_HandleNull(RsTx!serie_rec, "") & "/" & BL_HandleNull(RsTx!n_rec, "")
            DataTM = BL_HandleNull(RsTx!dt_emi, "")
            
            If BL_HandleNull(RsTx!cod_moeda, "") <> gMoedaActiva Then
                If BL_HandleNull(RsTx!cod_moeda, "") = "PTE" Then
                    aux = BL_String2Double(ValorTM) / gConvEur
                    ValorTM = BL_String2Double(aux, 3)
                ElseIf BL_HandleNull(RsTx!cod_moeda, "") = "EUR" Then
                    aux = BL_String2Double(ValorTM) * gConvEur
                    ValorTM = BL_String2Double(aux, 3)
                End If
            End If
        Else
            CbTaxaModeradora.ListIndex = -1
            CbTaxaModeradora.Enabled = True
            NumRecTM = ""
            DataTM = ""
        End If
                    
        RsTx.Close
        Set RsTx = Nothing
        
    ElseIf rs!pag_tax = "N" Then
        CbTaxaModeradora.ListIndex = 1
    End If

End Sub

Sub PreencheUrgencia()
    
    If rs!t_urg = "N" Then
        CbUrgencia.ListIndex = 0
    ElseIf rs!t_urg = "U" Then
        CbUrgencia.ListIndex = 1
    End If

End Sub

Sub PreencheDescEntFinanceira()
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodEFR.Text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_efr FROM sl_efr WHERE cod_efr=" & Trim(EcCodEFR.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade inexistente!", vbOKOnly + vbInformation, "Entidades"
            EcCodEFR.Text = ""
            EcDescrEFR.Text = ""
            EcCodEFR.SetFocus
        Else
            EcDescrEFR.Text = Tabela!descr_efr
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
    
End Sub

Sub PreencheDescMedico()
        
    Dim Tabela As ADODB.recordset
    Dim sql As String

    If EcCodMedico.Text <> "" Then
        Set Tabela = New ADODB.recordset
        Select Case gLAB
        Case gLAB = "CITO", "GM"
            sql = "SELECT titulo FROM sl_responsaveis WHERE codigo='" & Trim(EcCodMedico.Text) & "'"
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseClient
            Tabela.Open sql, gConexao
        
            If Tabela.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "Respons�vel inexistente!", vbOKOnly + vbInformation, "M�dicos"
                EcCodMedico.Text = ""
                EcDescrMedico.Text = ""
                EcCodMedico.SetFocus
            Else
                EcDescrMedico.Text = Tabela!Titulo
            End If
        Case Else
            sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.Text) & "'"
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseClient
            Tabela.Open sql, gConexao
        
            If Tabela.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "M�dico inexistente!", vbOKOnly + vbInformation, "M�dicos"
                EcCodMedico.Text = ""
                EcDescrMedico.Text = ""
                EcCodMedico.SetFocus
            Else
                EcDescrMedico.Text = Tabela!nome_med
            End If
        End Select
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub

Sub PreencheDescMedico2()
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodMedico2.Text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT titulo FROM sl_responsaveis WHERE codigo ='" & Trim(EcCodMedico2.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Respons�vel inexistente!", vbOKOnly + vbInformation, "Respons�veis"
            EcCodMedico2.Text = ""
            EcDescrMedico2.Text = ""
            EcCodMedico2.SetFocus
        Else
            EcDescrMedico2.Text = Tabela!Titulo
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub

Sub PreencheDescArquivo()
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodArquivo.Text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_arquivo FROM sl_arquivos WHERE cod_arquivo='" & Trim(EcCodArquivo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Arquivo inexistente!", vbOKOnly + vbInformation, "Arquivos"
            EcCodArquivo.Text = ""
            EcDescrArquivo.Text = ""
            EcCodArquivo.SetFocus
        Else
            EcDescrArquivo.Text = Tabela!descr_arquivo
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub


Sub Preenche_MAReq(Cod_Perfil As Variant, Descr_Perfil As Variant, cod_ana_c As Variant, Descr_Ana_C As Variant, _
                    cod_ana_s As String, descr_ana_s As String, _
                    Ord_Ana As Long, Ord_Ana_Compl As Variant, Ord_Ana_Perf As Variant, _
                    cod_agrup As String, N_Folha_Trab As Integer, dt_chega As String, Flg_Apar_Transf As String, flg_estado As String, Ord_Marca As String)

    ReDim Preserve MaReq(UBound(MaReq) + 1)
    MaReq(UBound(MaReq)).Cod_Perfil = Cod_Perfil
    MaReq(UBound(MaReq)).Descr_Perfil = Descr_Perfil
    MaReq(UBound(MaReq)).cod_ana_c = cod_ana_c
    MaReq(UBound(MaReq)).Descr_Ana_C = Descr_Ana_C
    MaReq(UBound(MaReq)).cod_ana_s = cod_ana_s
    MaReq(UBound(MaReq)).descr_ana_s = descr_ana_s
    MaReq(UBound(MaReq)).Ord_Ana = Ord_Ana
    MaReq(UBound(MaReq)).Ord_Ana_Compl = Ord_Ana_Compl
    MaReq(UBound(MaReq)).Ord_Ana_Perf = Ord_Ana_Perf
    MaReq(UBound(MaReq)).cod_agrup = cod_agrup
    MaReq(UBound(MaReq)).N_Folha_Trab = N_Folha_Trab
    MaReq(UBound(MaReq)).dt_chega = dt_chega
    MaReq(UBound(MaReq)).Flg_Apar_Transf = Flg_Apar_Transf
    MaReq(UBound(MaReq)).flg_estado = flg_estado
    MaReq(UBound(MaReq)).Ord_Marca = Ord_Marca
    
End Sub

Private Sub SSTGestReq_Click(PreviousTab As Integer)
    
    Dim i
    Dim j As Integer
    DoEvents
    
    If SSTGestReq.Tab = 2 Then
        BtPesquisaAna.Enabled = True
        FGAna.SetFocus
    
    ElseIf SSTGestReq.Tab = 0 Then
        EcAuxAna.Visible = False
        EcAuxProd.Visible = False
        If EcDataPrevista.Enabled = True And EcDataPrevista.Visible = True Then
            If gMultiReport <> 1 Then
                EcDataPrevista.SetFocus
            End If
        ElseIf EcDataChegada.Enabled = True And EcDataChegada.Visible = True Then
            CbSituacao.SetFocus             'sdo
        End If
    
    ElseIf SSTGestReq.Tab = 1 Then
        If FgProd.Enabled = True Then FgProd.SetFocus
    
    ElseIf SSTGestReq.Tab = 3 Then
        
    
    ElseIf SSTGestReq.Tab = 4 Then
        ' Tab de recibos.
        
    End If
    
End Sub

Private Sub TAna_DblClick()
    
    Dim i As Long
    Dim s As String
    
    If TAna.Nodes.Count <> 0 Then
        If InStr(1, TAna.Nodes(TAna.SelectedItem.Index).Key, gGHOSTMEMBER_S) <> 0 Then
            'Inserir Ghostmembers
            
            'Retira nome da complexa
            s = Mid(TAna.Nodes(TAna.SelectedItem.Index).Key, 1, InStr(1, TAna.Nodes(TAna.SelectedItem.Index).Key, gGHOSTMEMBER_S) - 1)
            GhostComplexa = s
            
            If Mid(TAna.SelectedItem.parent.Key, 1, 1) = "P" Then
                'A complexa est� dentro de um perfil
                GhostPerfil = Mid(TAna.SelectedItem.parent.Key, 1, InStr(1, TAna.SelectedItem.parent.Key, "C") - 1)
            Else
                GhostPerfil = "0"
                'A complexa � o pai
            End If
            
            Call BL_SelGhostMember(s)
            BL_MudaPosicaoRato 100, 300
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_POPUP
        Else
            i = Pesquisa_RegistosA(TAna.Nodes(TAna.SelectedItem.Index).Key)
        
            If i <> -1 Then
                FGAna.topRow = i
                FGAna.row = i
                FGAna.SetFocus
            End If
        End If
    End If

End Sub

Sub Funcao_DataActual()

    If Me.ActiveControl.Name = "EcDataCriacao" Then
        BL_PreencheData EcDataCriacao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataAlteracao" Then
        BL_PreencheData EcDataAlteracao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao" Then
        BL_PreencheData EcDataImpressao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao2" Then
        BL_PreencheData EcDataImpressao2, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataPrevista" Then
        BL_PreencheData EcDataPrevista, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataChegada" Then
        BL_PreencheData EcDataChegada, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtMarcacao" Then
        BL_PreencheData EcDtMarcacao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcAuxProd" Then
        If EcAuxProd.Tag = adDate Then
            BL_PreencheData EcAuxProd, Me.ActiveControl
        End If
    End If
    
End Sub

Function Coloca_Estado(DtPrev, DtChega) As String
    
    If Not IsDate(DtPrev) Then
        Coloca_Estado = ""
        Exit Function
    End If
    
    If (DateValue(DtPrev) < DateValue(Bg_DaData_ADO)) And _
       (DtChega = "") Then
        Coloca_Estado = "Falta"
    ElseIf (DateValue(DtPrev) >= DateValue(Bg_DaData_ADO)) And _
           (DtChega = "") Then
        Coloca_Estado = "Espera"
    ElseIf (DtChega <> "") Then
        Coloca_Estado = "Entrou"
    Else
        Coloca_Estado = ""
        Exit Function
    End If

End Function

Sub Preenche_LaProdutos(indice As Integer)
    
    Dim TamDescrProd As Integer
    Dim TamDescrEspecif As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String
    
    LaProdutos.caption = ""
    If RegistosP(indice).CodProd <> "" Then
        Str1 = "Produto vai chegar a: "
        Str2 = "Produto devia ter chegado a: "
        Str3 = "Produto chegou a: "
        If RegistosP(indice).EstadoProd = "Entrou" Then
            LaProdutos.caption = LaProdutos.caption & Str3 & RegistosP(indice).DtChega
        ElseIf RegistosP(indice).EstadoProd = "Falta" Then
            LaProdutos.caption = LaProdutos.caption & Str2 & RegistosP(indice).DtPrev
        ElseIf EcDataPrevista.Text <> "" Then
            LaProdutos.caption = LaProdutos.caption & Str1 & RegistosP(indice).DtPrev
        End If
        TamDescrProd = Len(Trim(UCase(RegistosP(indice).DescrProd)))
        TamDescrEspecif = Len(Trim(UCase(RegistosP(indice).DescrEspecif)))
        For i = 1 To 80 - (TamDescrProd + TamDescrEspecif)
            LaProdutos.caption = LaProdutos.caption & Chr(32)
        Next i
        LaProdutos.caption = LaProdutos.caption & Trim(UCase(RegistosP(indice).DescrProd)) & " " & Trim(UCase(RegistosP(indice).DescrEspecif))
    End If

End Sub
Sub Preenche_LaTubos(indice As Integer)
    
    Dim TamDescrTubo As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String
    
    LaTubos.caption = ""
    If RegistosT(indice).CodTubo <> "" Then
        Str1 = "Tubo vai chegar a: "
        Str2 = "Tubo devia ter chegado a: "
        Str3 = "Tubo chegou a: "
        If RegistosT(indice).EstadoTubo = "Entrou" Then
            LaTubos.caption = LaTubos.caption & Str3 & RegistosT(indice).DtChega
        ElseIf RegistosT(indice).EstadoTubo = "Falta" Then
            LaTubos.caption = LaTubos.caption & Str2 & RegistosT(indice).DtPrev
        ElseIf EcDataPrevista.Text <> "" Then
            LaTubos.caption = LaProdutos.caption & Str1 & RegistosT(indice).DtPrev
        End If
        TamDescrTubo = Len(Trim(UCase(RegistosT(indice).descrTubo)))
        
        For i = 1 To 80 - (TamDescrTubo)
            LaTubos.caption = LaTubos.caption & Chr(32)
        Next i
        LaTubos.caption = LaTubos.caption & Trim(UCase(RegistosT(indice).descrTubo)) & " )"
    End If

End Sub

Sub Inicializa_Estrutura_Analises()
    
    'inicializa��es da lista de marca��es de an�lises
    ReDim MaReq(0 To 0)
    MaReq(0).Cod_Perfil = ""
    MaReq(0).Descr_Perfil = ""
    MaReq(0).cod_ana_c = ""
    MaReq(0).Descr_Ana_C = ""
    MaReq(0).cod_ana_s = ""
    MaReq(0).descr_ana_s = ""
    MaReq(0).Ord_Ana = 0
    MaReq(0).Ord_Ana_Compl = 0
    MaReq(0).Ord_Ana_Perf = 0
    MaReq(0).cod_agrup = ""
    MaReq(0).N_Folha_Trab = 0
    MaReq(0).dt_chega = ""
    MaReq(0).Flg_Apar_Transf = ""
    MaReq(0).flg_estado = ""

End Sub


Sub Cria_TmpRec()
    
    Dim TmpRec(8) As DefTable
    
    TmpRec(1).NomeCampo = "serie_rec"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 10
    
    TmpRec(2).NomeCampo = "n_rec"
    TmpRec(2).tipo = "INTEGER"
    TmpRec(2).tamanho = 9
    
    TmpRec(3).NomeCampo = "descr_rubr"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 80
    
    TmpRec(4).NomeCampo = "val_taxa"
    TmpRec(4).tipo = "DECIMAL"
    TmpRec(4).PInteira = 11
    TmpRec(4).PDecimal = 6
    
    TmpRec(5).NomeCampo = "Val_pag_ute"
    TmpRec(5).tipo = "DECIMAL"
    TmpRec(5).PInteira = 11
    TmpRec(5).PDecimal = 6
    
    TmpRec(6).NomeCampo = "Val_pag_ent"
    TmpRec(6).tipo = "DECIMAL"
    TmpRec(6).PInteira = 11
    TmpRec(6).PDecimal = 6
    
    TmpRec(7).NomeCampo = "empr_id"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 8
    
    TmpRec(8).NomeCampo = "via"
    TmpRec(8).tipo = "STRING"
    TmpRec(8).tamanho = 30
    
    Call BL_CriaTabela("sl_cr_rec" & gNumeroSessao, TmpRec)
    
End Sub

Sub Cria_TmpEtq()

    Dim TmpEtq(1 To 15) As DefTable
    
    TmpEtq(1).NomeCampo = "t_utente"
    TmpEtq(1).tipo = "STRING"
    TmpEtq(1).tamanho = 5
    
    TmpEtq(2).NomeCampo = "utente"
    TmpEtq(2).tipo = "STRING"
    TmpEtq(2).tamanho = 20
    
    TmpEtq(3).NomeCampo = "n_proc"
    TmpEtq(3).tipo = "STRING"
    TmpEtq(3).tamanho = 100
    
    TmpEtq(4).NomeCampo = "n_req"
    TmpEtq(4).tipo = "STRING"
    TmpEtq(4).tamanho = 9
    
    TmpEtq(5).NomeCampo = "t_urg"
    TmpEtq(5).tipo = "STRING"
    TmpEtq(5).tamanho = 3
    
    TmpEtq(6).NomeCampo = "dt_req"
    TmpEtq(6).tipo = "DATE"
   
    TmpEtq(7).NomeCampo = "ordem"
    TmpEtq(7).tipo = "STRING"
    TmpEtq(7).tamanho = 3
    
    TmpEtq(8).NomeCampo = "produto"
    TmpEtq(8).tipo = "STRING"
    TmpEtq(8).tamanho = 25
    
    TmpEtq(9).NomeCampo = "nome_ute"
    TmpEtq(9).tipo = "STRING"
    TmpEtq(9).tamanho = 80
    
    TmpEtq(10).NomeCampo = "n_req_bar"
    TmpEtq(10).tipo = "STRING"
    TmpEtq(10).tamanho = 20

    TmpEtq(11).NomeCampo = "gr_ana"
    TmpEtq(11).tipo = "STRING"
    TmpEtq(11).tamanho = 40

    TmpEtq(12).NomeCampo = "abr_ana"
    TmpEtq(12).tipo = "STRING"
    TmpEtq(12).tamanho = 10
    
    TmpEtq(13).NomeCampo = "descr_tubo"
    TmpEtq(13).tipo = "STRING"
    TmpEtq(13).tamanho = 40

    TmpEtq(14).NomeCampo = "situacao"
    TmpEtq(14).tipo = "STRING"
    TmpEtq(14).tamanho = 7
    
    TmpEtq(15).NomeCampo = "episodio"
    TmpEtq(15).tipo = "STRING"
    TmpEtq(15).tamanho = 15
    
    Call BL_CriaTabela("sl_cr_etiq" & gNumeroSessao, TmpEtq)
    
End Sub

Sub Preenche_Recibo()

    Dim sql As String
    Dim i As Integer
    Dim PagEnt As Double
    Dim PagUte As Double
    Dim PagTax As Double
    Dim utilizador As String
    Dim continua As Boolean
    
    'No caso de nenhuma das combos(TaxaModerador,PagUte,PagEnt) tenha a op��o Pago seleccionada N�o emite recibo
    If CbTaxaModeradora.ListIndex <> 0 And CbPagEnt.ListIndex <> 0 And CbPagUte.ListIndex <> 0 Then Exit Sub
    
    If BG_Mensagem(mediMsgBox, "Imprimir recibo ?", vbQuestion + vbYesNo, "Recibo") = vbNo Then Exit Sub
    
    If BL_PreviewAberto("Recibo Requisi��o") = True Then Exit Sub
    
    continua = BL_IniciaReport("ReciboReq_Simples", "Recibo Requisi��o", crptToPrinter, False)
    If continua = False Then Exit Sub
    
    Call Cria_TmpRec
    
    'Inserir Dados da estrutura de recibos na tabela tempor�ria
    For i = 1 To TotalListaRecibos
        gSQLError = 0
        PagEnt = 0
        PagUte = 0
        PagTax = 0
        
        If CbEntIndex <> CbPagEnt.ListIndex And CbPagEnt.ListIndex = 0 Then
            PagEnt = Recibo(i).Valor_Ent
        End If
        If CbTMIndex <> CbTaxaModeradora.ListIndex And CbTaxaModeradora.ListIndex = 0 Then
            PagTax = Recibo(i).Valor_Taxa
        End If
        If CbUteIndex <> CbPagUte.ListIndex And CbPagUte.ListIndex = 0 Then
            PagUte = Recibo(i).Valor_Ute
        End If
        
        If PagEnt <> 0 Or PagUte <> 0 Or PagTax <> 0 Then
            sql = "INSERT INTO sl_cr_rec" & gNumeroSessao & " (serie_rec, n_rec, descr_rubr,val_taxa,val_pag_ute,val_pag_ent,via) VALUES ('" & gSerieRec & "','" & gRec & "','" & Recibo(i).descr_rubr & "'," & PagTax & "," & PagUte & "," & PagEnt & ", null)"
            BG_ExecutaQuery_ADO sql
        
            If gSQLError <> 0 Then
                BG_Mensagem mediMsgBox, "Erro a gerar recibo!", vbExclamation + vbOKOnly, "Recibo"
                
                Exit Sub
            End If
        End If
    Next i
    
    utilizador = BL_SelNomeUtil(EcUtilizadorCriacao.Text)
    
    'Imprime o relat�rio no Crystal Reports
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT " & _
                      "     sl_modelo_rec.via,         sl_modelo_rec.descr_rubr, " & _
                      "     sl_modelo_rec.val_taxa,    sl_modelo_rec.val_pag_ute, " & _
                      "     sl_modelo_rec.val_pag_ent, sl_recibos.serie_rec, " & _
                      "     sl_recibos.n_rec,          sl_recibos.dt_emi, " & _
                      "     sl_recibos.hr_emi,         sl_requis_consultas.n_req, " & _
                      "     sl_requis_consultas.n_epis,          sl_requis_consultas.cod_efr, " & _
                      "     sl_requis_consultas.n_benef,         sl_idutilizador.utilizador, " & _
                      "     sl_efr.descr_efr,          sl_identif.nome_ute, " & _
                      "     sl_identif.descr_mor_ute,  sl_identif.cod_postal_ute " & _
                      "FROM " & _
                      "     sl_cr_rec" & gNumeroSessao & ", sl_modelo_rec, " & _
                      "     sl_recibos,      sl_requis_consultas, " & _
                      "     sl_idutilizador, sl_efr, " & _
                      "     sl_identif,      gr_empr_inst " & _
                      "WHERE " & _
                      "     sl_recibos.serie_rec = sl_modelo_rec.serie_rec        AND " & _
                      "     sl_recibos.n_rec     = sl_modelo_rec.n_rec            AND " & _
                      "     sl_recibos.n_req     = sl_requis_consultas.n_req                AND " & _
                      "     sl_recibos.user_emi  = sl_idutilizador.cod_utilizador AND " & _
                      "     sl_requis_consultas.cod_efr    = sl_efr.cod_efr                 AND " & _
                      "     sl_requis_consultas.seq_utente = sl_identif.seq_utente          AND " & _
                      "     gr_empr_inst.estado  = 'S'"

    Call BL_ExecutaReport
    
    gSQLError = 0
    BG_ExecutaQuery_ADO "UPDATE sl_cr_rec" & gNumeroSessao & " SET VIA = '(Duplicado)'"
    If gSQLError = 0 Then
        
        If BL_PreviewAberto("Recibo Requisi��o (Duplicado)") = True Then Exit Sub

        continua = BL_IniciaReport("ReciboReq_Simples", "Recibo Requisi��o (Duplicado)", crptToPrinter, False, , , , False)
        If continua = False Then Exit Sub
        
        Set Report = forms(0).Controls("Report")
        
        Report.SQLQuery = "SELECT " & _
                          "     sl_modelo_rec.via,         sl_modelo_rec.descr_rubr, " & _
                          "     sl_modelo_rec.val_taxa,    sl_modelo_rec.val_pag_ute, " & _
                          "     sl_modelo_rec.val_pag_ent, sl_recibos.serie_rec, " & _
                          "     sl_recibos.n_rec,          sl_recibos.dt_emi, " & _
                          "     sl_recibos.hr_emi,         sl_requis_consultas.n_req, " & _
                          "     sl_requis_consultas.n_epis,          sl_requis_consultas.cod_efr, " & _
                          "     sl_requis_consultas.n_benef,         sl_idutilizador.utilizador, " & _
                          "     sl_efr.descr_efr,          sl_identif.nome_ute, " & _
                          "     sl_identif.descr_mor_ute,  sl_identif.cod_postal_ute " & _
                          "FROM " & _
                          "     sl_cr_rec" & gNumeroSessao & " sl_modelo_rec, " & _
                          "     sl_recibos,      sl_requis_consultas, " & _
                          "     sl_idutilizador, sl_efr, " & _
                          "     sl_identif,      gr_empr_inst " & _
                          "WHERE " & _
                          "     sl_recibos.serie_rec = sl_modelo_rec.serie_rec        AND " & _
                          "     sl_recibos.n_rec     = sl_modelo_rec.n_rec            AND " & _
                          "     sl_recibos.n_req     = sl_requis_consultas.n_req                AND " & _
                          "     sl_recibos.user_emi  = sl_idutilizador.cod_utilizador AND " & _
                          "     sl_requis_consultas.cod_efr    = sl_efr.cod_efr                 AND " & _
                          "     sl_requis_consultas.seq_utente = sl_identif.seq_utente          AND " & _
                          "     gr_empr_inst.estado  = 's'"
    
        Call BL_ExecutaReport
    End If
    
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Call BL_RemoveTabela("sl_cr_rec" & gNumeroSessao)
         
End Sub

Sub ImprimeEtiq()
    Dim i As Integer
    Dim k As Integer
    Dim j As Integer
    Dim sql As String
    Dim CmdDadosTubo As ADODB.Command
    Dim RsDadosTubo As ADODB.recordset
    Dim CmdDadosGhostM As ADODB.Command
    Dim RsDadosGhostM As ADODB.recordset
    Dim totalTubos As Long
    Dim CodTuboBarLoc As String
    Dim encontrou As Boolean
    Dim CalculaCapaci As Double
    Dim ordem As Integer
    Dim GhostsJaTratados() As String
    Dim TotalGhostsJaTratados As Integer
    Dim Especiais As Long
    Dim EtqAdministrativas As Long
    Dim NumReqBar As String
    Dim continua As Boolean
    Dim EtqCrystal As String
    Dim EtqPreview As String
    Dim rsReq As ADODB.recordset
    Dim TotalP1s As Integer
    Dim NrReqARS As String

    
    Dim episodio As String
    Dim situacao As String
    Dim rv As Integer
    
    Dim idade As String
    Dim CodProd_aux As String
    
    rv = REQUISICAO_Situacao_Episodio(Trim(Me.EcNumReq), _
                                      situacao, _
                                      episodio)
    Select Case situacao
        Case gT_Urgencia
            situacao = "URG"
        Case gT_Consulta
            situacao = "CON"
        Case gT_Internamento
            situacao = "INT"
        Case gT_Externo
            situacao = "HDI"
        Case gT_LAB
            situacao = "LAB"
        Case gT_RAD
            situacao = "RAD"
        Case gT_Bloco
            situacao = "BLO"
        Case Else
            situacao = ""
    End Select
    
    'Dados das an�lises simples (tubo e produto)
    Set CmdDadosTubo = New ADODB.Command
    Set CmdDadosTubo.ActiveConnection = gConexao
    CmdDadosTubo.CommandType = adCmdText

    If gSGBD = gOracle Then
        CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, tubo.INF_COMPLEMENTAR " & _
                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO (+) " & _
                  " AND   ANA.COD_ANA_S = ?"
    ElseIf gSGBD = gInformix Then
        CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, tubo.INF_COMPLEMENTAR " & _
                  " FROM  SL_ANA_S ANA, OUTER SL_GR_ANA GRUPO_ANA, OUTER SL_TUBO TUBO " & _
                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA " & _
                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                  " AND   ANA.COD_ANA_S = ?"
    ElseIf gSGBD = gSqlServer Then
        CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, tubo.INF_COMPLEMENTAR " & _
                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                  " WHERE ANA.GR_ANA *= GRUPO_ANA.COD_GR_ANA " & _
                  " AND   ANA.COD_TUBO *= TUBO.COD_TUBO  " & _
                  " AND   ANA.COD_ANA_S = ?"
    End If
    CmdDadosTubo.Prepared = True
    CmdDadosTubo.Parameters.Append CmdDadosTubo.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)

    'Simples GhostMembers
    Set CmdDadosGhostM = New ADODB.Command
    Set CmdDadosGhostM.ActiveConnection = gConexao
    CmdDadosGhostM.CommandType = adCmdText
    CmdDadosGhostM.CommandText = "SELECT COD_MEMBRO, ORDEM  FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A' ORDER BY ORDEM"
    CmdDadosGhostM.Prepared = True
    CmdDadosGhostM.Parameters.Append CmdDadosTubo.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)

    TotalGhostsJaTratados = 0
    ReDim GhostsJaTratados(0)
    
    Especiais = 0
    
    totalTubos = 0
    ReDim Tubos(0)

    i = 1
    'Verificar quais as etiquetas a criar
    While i <= UBound(MaReq)
        If MaReq(i).Cod_Perfil <> "0" Then
                ' PERFIS
                CmdDadosTubo.CommandText = "select ana.cod_produto, ana.abr_ana_p abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                            " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                            " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, '' flg_etiq_abr, inf_complementar,num_copias " & _
                                            " from sl_perfis ana LEFT OUTER JOIN  sl_gr_ana grupo_ana  ON ana.gr_ana = grupo_ana.cod_gr_ana , sl_tubo tubo" & _
                                            " where ana.cod_tubo = tubo.cod_tubo and ana.cod_perfis = ?"
                CmdDadosTubo.Parameters(0) = MaReq(i).Cod_Perfil
                Set RsDadosTubo = CmdDadosTubo.Execute
                
                'COMPLEXAS
                If RsDadosTubo.EOF = True Then
                    CmdDadosTubo.CommandText = "select ana.cod_produto, ana.abr_ana_c abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                                " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                                " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, '' flg_etiq_abr, inf_complementar, num_copias " & _
                                                " from sl_ana_c ana LEFT OUTER JOIN  sl_gr_ana grupo_ana ON ana.gr_ana = grupo_ana.cod_gr_ana, sl_tubo tubo " & _
                                                " where ana.cod_tubo = tubo.cod_tubo and ana.cod_ana_c = ?"
                    CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsDadosTubo = CmdDadosTubo.Execute
                End If
                
                'SIMPLES
                If RsDadosTubo.EOF = True Then
                    CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, num_copias " & _
                                  " FROM  SL_ANA_S ANA LEFT OUTER JOIN SL_GR_ANA GRUPO_ANA ON ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA, SL_TUBO TUBO " & _
                                  " WHERE ANA.COD_TUBO = TUBO.COD_TUBO " & _
                                  " AND   ANA.COD_ANA_S = ?"

                    CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_s
                    Set RsDadosTubo = CmdDadosTubo.Execute
                End If
                

        ElseIf MaReq(i).cod_ana_c <> "0" Then
                'COMPLEXAS
                CmdDadosTubo.CommandText = "select ana.cod_produto, ana.abr_ana_c abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                            " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                            " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, '' flg_etiq_abr, inf_complementar, num_copias " & _
                                            " from sl_ana_c ana, sl_tubo tubo, sl_gr_ana grupo_ana " & _
                                            " where ana.gr_ana = grupo_ana.cod_gr_ana(+) and ana.cod_tubo = tubo.cod_tubo and ana.cod_ana_c = ?"
                CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_c
                Set RsDadosTubo = CmdDadosTubo.Execute
                
                'SIMPLES
                If RsDadosTubo.EOF = True Then
                    CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, num_copias " & _
                                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                                  " AND   ANA.COD_ANA_S = ?"

                    CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_s
                    Set RsDadosTubo = CmdDadosTubo.Execute
                End If
        
        ElseIf MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
            
'            'Verificar se j� n�o foram geradas etiquetas
'            ' para a complexa deste Ghostmember
'            For k = 1 To UBound(GhostsJaTratados)
'                If GhostsJaTratados(k) = MaReq(i).Cod_Ana_C Then
'                    'J� tratou passa � an�lise seguinte da MAREQ
'                    'ATEN��O Goto
'                    GoTo Fim
'                End If
'            Next k
'
'            'Regista a complexa do ghostmember para n�o a tratar mais
'            TotalGhostsJaTratados = TotalGhostsJaTratados + 1
'            ReDim Preserve GhostsJaTratados(TotalGhostsJaTratados)
'            GhostsJaTratados(TotalGhostsJaTratados) = MaReq(i).Cod_Ana_C
'
'            'Ciclo para todas as an�lises da gGHOSTMEMBER_S
'            CmdDadosGhostM.Parameters(0) = MaReq(i).Cod_Ana_C
'            Set RsDadosGhostM = CmdDadosGhostM.Execute
'
'            If RsDadosGhostM.EOF Then
'                RsDadosGhostM.Close
'                Set RsDadosGhostM = Nothing
'                'ATEN��O Goto
'                'Passa imediatamente para a an�lise seguinte na estrutura MAREQ
'                GoTo Fim
'            End If
'
'            While Not RsDadosGhostM.EOF
'                CmdDadosTubo.Parameters(0) = BL_HandleNull(RsDadosGhostM!COD_MEMBRO, "0")
'                Set RsDadosTubo = CmdDadosTubo.Execute
'
'                'ATEN��O Goto
'                'Trata a an�lise simples pertencente � complexa do GhostMember
'                GoTo TrataGhostMember
'
'SeguinteGhostMember:
'
'                RsDadosGhostM.MoveNext
'            Wend
'
'            RsDadosGhostM.Close
'            Set RsDadosGhostM = Nothing
'
'            'ATEN��O Goto
'            'Passa imediatamente para a an�lise seguinte na estrutura MAREQ
            
            'soliveira teste: para ghost members n�o interessa imprimir uma etiqueta para cada membro
            '                 passar a ver tubo associado � complexa : fazer (comentei tudo, expecto linha seguinte
            GoTo fim
        Else
        
        'Bruno 15-02-2007
            If gSGBD = gOracle Then
                CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                      " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, NUM_COPIAS " & _
                                      " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                      " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                      " AND   ANA.COD_TUBO = TUBO.COD_TUBO (+) " & _
                                      " AND   ANA.COD_ANA_S = ?"
            ElseIf gSGBD = gSqlServer Then
                CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                      " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, NUM_COPIAS " & _
                                      " FROM  SL_ANA_S ANA RIGHT OUTER JOIN SL_GR_ANA GRUPO_ANA ON ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA, " & _
                                      " SL_ANA_S ANA2 RIGHT OUTER JOIN SL_TUBO tubo ON ANA2.cod_tubo = TUBO.COD_TUBO " & _
                                      " WHERE  ANA2.COD_ANA_S = ANA.COD_ANA_S " & _
                                      " AND   ANA.COD_ANA_S = ?"
            End If
            CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_s
            Set RsDadosTubo = CmdDadosTubo.Execute
        End If

TrataGhostMember:
        
        If Not RsDadosTubo.EOF Then
            
'            If BL_HandleNull(RsDadosTubo!cod_etiq, "0") <> "0" Then
             If BL_HandleNull(RsDadosTubo!cod_tubo, "0") <> "0" Then
                'Etiquetas especiais:
                ' Incluir c�digo do tubo da an�lise na requisi��o
                
                'Procura um tubo j� registado com o mesmo codigo
                ' para verificar se necessita de um novo tubo
                encontrou = False
                For k = 1 To totalTubos
                    If (Tubos(k).CodTubo = BL_HandleNull(RsDadosTubo!cod_tubo, "") Or _
                       BL_HandleNull(RsDadosTubo!cod_tubo, "") = "") Then
                            
                            'imprimir as abreviaturas das analises do tubo
                            If BL_HandleNull(RsDadosTubo!flg_etiq_abr, "") = 1 Then
                                Tubos(k).abrAna = Tubos(k).abrAna & "," & BL_HandleNull(RsDadosTubo!abr_ana_s, "...")
                            End If
                            
                            encontrou = True
                            Exit For
                    End If
                Next k

                If encontrou = False Then
                    'N�o encontrou
                    totalTubos = totalTubos + 1
                    ReDim Preserve Tubos(totalTubos)
                    CodTuboBarLoc = ""
                    CodTuboBarLoc = BL_HandleNull(RsDadosTubo!cod_etiq, "")
                    
                    Call PreencheDadosTubo(totalTubos, _
                                           BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
                                           IIf(RsDadosTubo!flg_etiq_abr = 1, BL_HandleNull(RsDadosTubo!abr_ana_s, ""), ""), _
                                           BL_HandleNull(RsDadosTubo!cod_produto, ""), _
                                           BL_HandleNull(RsDadosTubo!cod_tubo, ""), _
                                           True, _
                                           True, _
                                           CodTuboBarLoc, _
                                           -1, _
                                           -1, _
                                           BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""), BL_HandleNull(RsDadosTubo!inf_complementar, ""), BL_HandleNull(RsDadosTubo!num_copias, 1))
                                           
                    Especiais = Especiais + 1
                End If
            End If
                
'            Else
'                'Etiquetas normais
'                'Procura um tubo j� registado com o mesmo produto
'                ' para verificar se necessita de um novo tubo
'                Encontrou = False
'
'                For k = 1 To TotalTubos
'                    '(Tubos(k).CodProd = BL_HandleNull(RsDadosTubo!cod_produto, "") And
'                    If (Tubos(k).CodTubo = BL_HandleNull(RsDadosTubo!Cod_Tubo, "") And _
'                        Tubos(k).Especial = False And _
'                        Tubos(k).Cheio = False) Or _
'                       (BL_HandleNull(RsDadosTubo!Cod_Tubo, "") = "") Then
'
'                            Encontrou = True
'                            Exit For
'                    End If
'                Next k
'
'                If Encontrou = False Then
'                    'N�o encontrou
'                    TotalTubos = TotalTubos + 1
'                    ReDim Preserve Tubos(TotalTubos)
'
'                    If (BL_HandleNull(RsDadosTubo!flg_etiq_prod, "0") = "0") Then
'                        Call PreencheDadosTubo(TotalTubos, _
'                                               BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
'                                               BL_HandleNull(RsDadosTubo!abr_ana_s, ""), _
'                                               "", _
'                                               BL_HandleNull(RsDadosTubo!Cod_Tubo, ""), _
'                                               False, _
'                                               False, _
'                                               "0", _
'                                               BL_HandleNull(RsDadosTubo!capaci_util, 0), _
'                                               BL_HandleNull(RsDadosTubo!qt_min, 0), _
'                                               BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""))
'
'                    Else
'                        Call PreencheDadosTubo(TotalTubos, _
'                                               BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
'                                               BL_HandleNull(RsDadosTubo!abr_ana_s, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Produto, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Tubo, ""), _
'                                               False, _
'                                               False, _
'                                               "0", _
'                                               BL_HandleNull(RsDadosTubo!capaci_util, 0), _
'                                               BL_HandleNull(RsDadosTubo!qt_min, 0), _
'                                               BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""))
'                    End If
'
'                Else
'                    'Encontrou
'                    CalculaCapaci = BL_HandleNull(RsDadosTubo!qt_min, 0) + (BL_HandleNull(RsDadosTubo!qt_min, 0) * 0.3)
'                    If (CalculaCapaci + Tubos(k).QtOcup > Tubos(k).QtMax) Then
'                        'Ultrapassou a capacidade m�xima do tubo
'                        Tubos(k).Cheio = True
'                        TotalTubos = TotalTubos + 1
'                        ReDim Preserve Tubos(TotalTubos)
'                        Call PreencheDadosTubo(TotalTubos, _
'                                               BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
'                                               BL_HandleNull(RsDadosTubo!abr_ana_s, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Produto, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Tubo, ""), _
'                                               False, _
'                                               False, _
'                                               "0", _
'                                               BL_HandleNull(RsDadosTubo!capaci_util, 0), _
'                                               CalculaCapaci, _
'                                               BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""))
'
'                    Else
'                        Tubos(TotalTubos).QtOcup = Tubos(TotalTubos).QtOcup + CalculaCapaci
'                        If Tubos(k).QtOcup = Tubos(k).QtMax And Tubos(k).QtMax <> 0 Then
'                            Tubos(TotalTubos).Cheio = True
'                        End If
'                    End If
'                'Fim de verifica��o da pesquisa de tubo
'                End If
'            'Fim de verifica��o de etiquetas especiais ou n�o
'            End If

        'Fim de verifica��o de RsDadosTubo.EOF
        End If
        RsDadosTubo.Close
        Set RsDadosTubo = Nothing
        
        'ATEN��O Goto
        'Passa para a an�lise seguinte do ghostmember
        'soliveira teste : comentei
'        If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then GoTo SeguinteGhostMember

fim:
        'an�lise seguinte
        i = i + 1
    Wend

'DEBUG :
'    For i = 1 To TotalTubos
'        SQL = "INDICE " & i & _
'            ", ANA " & Tubos(i).AbrAna & _
'            ", GRANA " & Tubos(i).GrAna & _
'            ", TUBO " & Tubos(i).CodTubo & _
'            ", PROD " & Tubos(i).CodProd & _
'            ", ESPECIAL " & Tubos(i).Especial & _
'            ", PRODBAR " & Tubos(i).codtubobar & _
'            ", ORDANA " & Tubos(i).OrdAna & _
'            ", QTMAX " & Tubos(i).QtMax & _
'            ", QTOCUP " & Tubos(i).QtOcup & _
'            ", CHEIO " & Tubos(i).Cheio
'        MsgBox SQL
'    Next i

    If totalTubos <> 0 Then
        EcEtqNTubos = totalTubos
        EcEtqNEsp = Especiais
        
        'Aten��o - O FormNEtiq retorna nos campos deste form :
        ' EcEtqNAdm   = n� etiquetas administrativas
        ' EcEtqNCopias  = n� copias de etiquetas tubos
        ' EcEtqTipo   = tipo de impress�o
        '   0 - imprimir etq. para tubos e administrativas
        '   1 - imprimir etq. administrativas
        '   2 - imprimir etq. para tubos
        '  -1 - quando se cancela a impress�o
        '
        ' EcEtqNEsp envia para o form o n� etiquetas especiais (com o c�digo do produto ou ordem de an�lises)
        ' EcEtqNTubos envia para o form o n� etiquetas para tubos
        
'        If EFR_Verifica_ARS(EcCodEFR.text) = True Then
'            ReDim p1(0)
'            Dim ExisteP1 As Boolean
'            ExisteP1 = False
'            For i = 1 To RegistosA.Count - 1
'
'                For J = 1 To UBound(p1)
'                    If RegistosA(i).NReqARS = p1(J).NrP1 Then
'                        ExisteP1 = True
'                    End If
'                Next J
'                If ExisteP1 = False Then
'                    ReDim Preserve p1(UBound(p1) + 1)
'                    p1(UBound(p1)).NrP1 = RegistosA(i).NReqARS
'                End If
'                ExisteP1 = False
'            Next i
'
'            TotalP1s = UBound(p1)
'            gNumEtiqAdminDefeito = TotalP1s + 1
'        Else
            TotalP1s = 0
            gNumEtiqAdminDefeito = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NumEtiqAdminDefeito"))
'        End If
        If gSerieUnicaMarcacao = mediSim Then
            FormNEtiq.Show vbModal
        Else
            FormNEtiqConsultas.Show vbModal
        End If
        FormGesReqCons.SetFocus
                
        If Trim(EcEtqNAdm) = "" Then EcEtqNAdm = "0"
        If Trim(EcEtqNCopias) = "" Then EcEtqNCopias = "0"
        If Trim(EcEtqNEsp) = "" Then EcEtqNEsp = "0"
        If Trim(EcEtqNTubos) = "" Then EcEtqNTubos = "0"
        
        If EcEtqTipo <> "-1" Then
        
            EtqAdministrativas = EcEtqNAdm
        
            If totalTubos <> 0 And EtqAdministrativas <> 0 Then
            'FeriasFernando
                If gLAB = "HOVAR" Then
                    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "ETIQ_CRYSTAL")
                Else
                    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
                
                If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                End If
                
                If EtqCrystal = "1" Then
                    If BL_PreviewAberto("Etiquetas") = True Then Exit Sub
            
                    continua = BL_IniciaReport("Etiqueta", "Etiquetas", crptToPrinter, False)
                    If continua = False Then Exit Sub
            
                    Call Cria_TmpEtq
                Else
'                    If Not LerEtiqIni Then
'                        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
'                        Exit Sub
'                    End If
'                    If Not EtiqOpenPrinter(EcPrinterEtiq) Then
'                        MsgBox "Imposs�vel abrir impressora etiquetas"
'                        Exit Sub
'                    End If
                End If
                
                NumReqBar = ""
                For j = 1 To (7 - Len(EcNumReq))
                    NumReqBar = NumReqBar & "0"
                Next j
                NumReqBar = NumReqBar & EcNumReq
                
                If EcEtqTipo <> "1" Then
                    'Tubos
                    'N� de c�pias das etiquetas
                    For j = 1 To EcEtqNCopias
                        For i = 1 To totalTubos
                            If EtqCrystal = "1" Then
                        'Bruno 28-01-2003
                                If (Tubos(i).Especial = True And Tubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else
                                    CodProd_aux = Tubos(i).CodTubo
                                    Tubos(i).CodTubo = ""
                                
                                    sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                        " ( gr_ana, abr_ana, t_utente, utente, n_proc, n_req, t_urg, dt_req, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                        " VALUES (" & BL_TrataStringParaBD(Tubos(i).GrAna) & "," & BL_TrataStringParaBD(Tubos(i).abrAna) & "," & BL_TrataStringParaBD(CbTipoUtente.Text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                        ",'" & EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2 & _
                                        "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.Text, 1, 3), " ") & _
                                        "'," & BL_TrataDataParaBD(EcDataPrevista) & "," & _
                                        "'" & Tubos(i).CodProd & IIf(Tubos(i).CodTubo <> "", " (", " ") & Tubos(i).CodTubo & IIf(Tubos(i).CodTubo <> "", ")'", "'") & ", " & _
                                        BL_TrataStringParaBD(EcNome) & ",'" & Tubos(i).codTuboBar & NumReqBar & "', '" & BG_CvPlica(Tubos(i).Designacao_tubo) & "', '" & situacao & "', '" & episodio & "', '" & idade & "')"
                                    
                                    BG_ExecutaQuery_ADO sql
                            
                                    If gSQLError <> 0 Then
                                        BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                        Set CmdDadosTubo = Nothing
                                        Set CmdDadosGhostM = Nothing
                                        Exit Sub
                                    End If
                                End If
                            Else
                                If (Tubos(i).Especial = True And Tubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else
                                    
                                    If Not LerEtiqInI Then
                                        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                        Exit Sub
                                    End If
                                    If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                        MsgBox "Imposs�vel abrir impressora etiquetas"
                                        Exit Sub
                                    End If
                                    
                                    Call EtiqPrint(Tubos(i).GrAna, Tubos(i).abrAna, _
                                                   CbTipoUtente.Text, EcUtente, _
                                                   EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2, _
                                                   EcNumReq, IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.Text, 1, 3), " "), _
                                                   EcDataPrevista, _
                                                   Tubos(i).CodProd & IIf(Tubos(i).CodTubo <> "", " (", " ") & Tubos(i).CodTubo & IIf(Tubos(i).CodTubo <> "", ")", ""), _
                                                   EcNome, Tubos(i).codTuboBar & NumReqBar, _
                                                   BG_CvPlica(Tubos(i).Designacao_tubo), Tubos(i).inf_complementar, Tubos(i).num_copias)
                                    
                                    EtiqClosePrinter
                                    
                                    BL_RegistaImprEtiq EcNumReq, Tubos(i).CodTubo
                                End If
                            End If
                        Next i
                    Next j
                End If
                
                
                If EcEtqTipo <> "2" Then
                    'Administrativas
                    For k = 1 To EtqAdministrativas
                        If EtqCrystal = "1" Then
                        
                            sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                " ( t_utente, utente, n_proc, n_req, t_urg, dt_req, ordem, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                " VALUES (" & BL_TrataStringParaBD(CbTipoUtente.Text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                ",'" & EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2 & _
                                "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.Text, 1, 3), " ") & _
                                "'," & BL_TrataDataParaBD(EcDataPrevista) & ",null,null, " & _
                                BL_TrataStringParaBD(EcNome) & ",'" & NumReqBar & "', 'Administrativa', '" & situacao & "', '" & episodio & "', '" & idade & "')"
                            BG_ExecutaQuery_ADO sql
                    
                            If gSQLError <> 0 Then
                                BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                Set CmdDadosTubo = Nothing
                                Set CmdDadosGhostM = Nothing
                                Exit Sub
                            End If
                        Else
                            ' --------
                            'Tubos(k).GrAna = "Administrat."
                            
                            If Not LerEtiqInI Then
                                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                Exit Sub
                            End If
                            If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                MsgBox "Imposs�vel abrir impressora etiquetas"
                                Exit Sub
                            End If
                            
'                            Call EtiqPrint(Tubos(k).GrAna, _
'                                           "", _
'                                           CbTipoUtente.Text, _
'                                           EcUtente, _
'                                           EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2, _
'                                           EcNumReq, _
'                                           IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.Text, 1, 3), " "), _
'                                           EcDataPrevista, _
'                                           Tubos(k).CodProd & IIf(Tubos(k).codTubo <> "", " (", " ") & Tubos(k).codTubo & IIf(Tubos(k).codTubo <> "", ")", ""), _
'                                           EcNome, _
'                                           Tubos(k).CodTuboBar & NumReqBar, _
'                                           "Administrativa")
                                                       
                            
                            NrReqARS = ""
                            
                            Call EtiqPrint("", _
                                           "", _
                                           CbTipoUtente.Text, _
                                           EcUtente, _
                                           EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2, _
                                           EcNumReq, _
                                           IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.Text, 1, 3), " "), _
                                           EcDataPrevista, _
                                           "", EcNome, "", "Administrativa", "", "1", NrReqARS)
                                           
                            EtiqClosePrinter
                            ' --------
                        End If
                    Next k
                End If
                
                'Verifica se o utente utente em causa tem a requisi��o em causa
'                Sql = "SELECT n_req FROM sl_requis WHERE n_req=" & EcNumReq.Text & " AND seq_utente=" & EcSeqUtente.Text
'                Set RsReq = New ADODB.Recordset
'                RsReq.CursorLocation = adUseClient
'                RsReq.CursorType = adOpenStatic
'                RsReq.Open Sql, gConexao
'                If RsReq.RecordCount <= 0 Then
'                    End
'                End If
'                RsReq.Close
'                Set RsReq = Nothing
                
                
                If EtqCrystal = "1" Then
                    'Imprime o relat�rio no Crystal Reports
            
                    Dim Report As CrystalReport
                    Set Report = forms(0).Controls("Report")
            
                    Report.SQLQuery = "SELECT " & _
                                      "     T_UTENTE, " & _
                                      "     UTENTE, " & _
                                      "     N_PROC, " & _
                                      "     N_REQ, " & _
                                      "     T_URG, " & _
                                      "     DT_REQ, " & _
                                      "     ORDEM, " & _
                                      "     PRODUTO, " & _
                                      "     NOME_UTE, " & _
                                      "     N_REQ_BAR " & _
                                      "FROM " & _
                                      "     SL_CR_ETIQ" & gNumeroSessao & " SL_CR_ETIQ"
                                        
                    For i = 0 To Printers.Count - 1
                        If Printers(i).DeviceName = EcPrinterEtiq Then
                            Report.PrinterName = Printers(i).DeviceName
                            Report.PrinterDriver = Printers(i).DriverName
                            Report.PrinterPort = Printers(i).Port
                        End If
                    Next i
                    
                    If Trim(gEtiqPreview) Then
                        Report.Destination = crptToWindow
                        Report.WindowState = crptMaximized
                        Report.PageShow Report.ReportStartPage
                    Else
                        Report.Destination = crptToPrinter
                    End If
                    
                    Call BL_ExecutaReport
                    
                    If Trim(gEtiqPreview) Then
                        Report.PageZoom (200)
                    End If
                    
                    'Elimina as Tabelas criadas para a impress�o dos relat�rios
                    Call BL_RemoveTabela("sl_cr_etiq" & gNumeroSessao)
                Else
                    'EtiqClosePrinter
                End If
            End If
            
            If Me.EcImprimirResumo = "1" Then ImprimeResumo
        End If
    End If
    
    Set CmdDadosTubo = Nothing
    Set CmdDadosGhostM = Nothing
     
End Sub

Sub ImprimeResumo()

    Dim Mg As Long
    Dim i As Long
    Dim LineHeight As Long
    Dim idade As String
    
    For i = 0 To Printers.Count - 1
        If Printers(i).DeviceName = EcPrinterResumo Then
            Set Printer = Printers(i)
            Exit For
        End If
    Next
    
    If EcDataNasc <> "" Then
        idade = CStr(BG_CalculaIdade(CDate(EcDataNasc)))
    End If
    Mg = 1000
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    Printer.CurrentY = Mg
    Printer.CurrentX = Mg
    Printer.Print "Requisi��o N�: ";
    Printer.FontBold = True
    Printer.Print EcNumReq;
    Printer.FontBold = False
    Printer.CurrentX = Printer.Width / 3 * 2
    Printer.Print "Data: ";
    Printer.FontBold = True
    Printer.Print EcDataPrevista
    Printer.FontBold = False
    LineHeight = Printer.CurrentY - Mg
    Printer.Print
    Printer.Line (Mg, Printer.CurrentY)-(Printer.Width - Mg, Printer.CurrentY)
    Printer.Print
    Printer.CurrentX = Mg
    Printer.Print "Utente N� ";
    Printer.FontBold = True
    Printer.Print CbTipoUtente.Text & "/" & EcUtente
    Printer.FontBold = False
    Printer.CurrentX = Mg
    Printer.Print "Nome: ";
    Printer.FontBold = True
    Printer.Print EcNome
    Printer.FontBold = False
    Printer.CurrentX = Mg
    Printer.Print "Data Nasc: ";
    Printer.FontBold = True
    Printer.Print EcDataNasc;
    Printer.FontBold = False
    Printer.CurrentX = Printer.Width / 2
    Printer.Print "Idade: ";
    Printer.FontBold = True
    Printer.Print idade
    Printer.FontBold = False
    
    Printer.CurrentX = Mg
    Printer.Print "Proveni�ncia: ";
    Printer.FontBold = True
    Printer.Print EcDescrProveniencia;
    Printer.FontBold = False
    Printer.CurrentX = Printer.Width / 2
    Printer.Print "Obs.Proveniencia: ";
    Printer.FontBold = True
    Printer.Print EcObsProveniencia
    Printer.FontBold = False
    
    Printer.Print
    Printer.Line (Mg, Printer.CurrentY)-(Printer.Width - Mg, Printer.CurrentY)
    Printer.Print
    Printer.CurrentX = Mg
    Printer.Print "Produtos:"
    Printer.FontBold = True
    If FgProd.rows > 2 Then
        For i = 1 To FgProd.rows - 2
            Printer.CurrentX = 2 * Mg
            Printer.Print FgProd.TextMatrix(i, 0);
            Printer.CurrentX = Printer.Width / 4
            Printer.Print FgProd.TextMatrix(i, 1)
        Next
    Else
        Printer.CurrentX = 2 * Mg
        Printer.Print "(nenhum)"
    End If
    Printer.FontBold = False
    Printer.Print
    Printer.Line (Mg, Printer.CurrentY)-(Printer.Width - Mg, Printer.CurrentY)
    Printer.Print
    Printer.CurrentX = Mg
    Printer.Print "An�lises:"
    Printer.FontBold = True
    If FGAna.rows > 2 Then
        For i = 1 To FGAna.rows - 2
            Printer.CurrentX = 2 * Mg
            Printer.Print FGAna.TextMatrix(i, 0);
            Printer.CurrentX = Printer.Width / 4
            Printer.Print FGAna.TextMatrix(i, 1)
            Printer.CurrentY = Printer.CurrentY + LineHeight / 2
        Next
    Else
        Printer.CurrentX = 2 * Mg
        Printer.Print "(nenhuma)"
    End If
    Printer.FontBold = False
    Printer.EndDoc

End Sub

Sub Preenche_Recibo_Isencao()

    Dim sql As String
    Dim utilizador As String
    Dim continua As Boolean
            
    If CkIsento.value = vbChecked And gRecibos = "1" Then
                
        If BG_Mensagem(mediMsgBox, "Imprimir declara��o de isen��o ?", vbQuestion + vbYesNo, "Isen��o") = vbNo Then Exit Sub

        '1.Verifica se a Form Preview j� estava aberta!!
        If BL_PreviewAberto("Isencao") = True Then Exit Sub
        
        'Printer Common Dialog
        continua = BL_IniciaReport("Isencao", "Isencao", crptToPrinter, False)
        If continua = False Then Exit Sub
        
        Call Cria_TmpRec_Isencao
        
        utilizador = BL_SelNomeUtil(EcUtilizadorCriacao.Text)
        sql = "INSERT INTO sl_cr_isen" & gNumeroSessao & " (nome_ute,tipo_ute,num_ute,situacao,tipo_isencao,nome_usr ) VALUES('" & EcNome.Text & "','" & CbTipoUtente.Text & "','" & EcUtente.Text & "','" & CbSituacao.Text & "','" & CbTipoIsencao.Text & "','" & utilizador & "')"
        BG_ExecutaQuery_ADO sql
            
        If gSQLError <> 0 Then
            BG_Mensagem mediMsgBox, "Erro a criar recibo de isen��o!", vbExclamation, "Isen��o"
            Exit Sub
        Else
            
        End If
        
        'Imprime o relat�rio no Crystal Reports
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        
        Report.SQLQuery = "SELECT SL_CR_ISEN.NOME_UTE, SL_CR_ISEN.TIPO_UTE, " & _
            " SL_CR_ISEN.NUM_UTE, SL_CR_ISEN.SITUACAO, SL_CR_ISEN.TIPO_ISENCAO, " & _
            " SL_CR_ISEN.NOME_USR, GR_EMPR_INST.NOME_EMPR, GR_EMPR_INST.TELEFONES, " & _
            " GR_EMPR_INST.N_FAX, GR_EMPR_INST.LOCALIDADE, GR_EMPR_INST.COD_POSTAL, " & _
            " GR_EMPR_INST.N_SEQ_POSTAL, GR_EMPR_INST.MORADA " & _
            " From sl_cr_isen" & gNumeroSessao & " SL_CR_ISEN, GR_EMPR_INST " & _
            " WHERE GR_EMPR_INST.ESTADO='S'"
       
        Report.Destination = crptToPrinter
        Call BL_ExecutaReport
        
        'Elimina as Tabelas criadas para a impress�o dos relat�rios
        
        Call BL_RemoveTabela("sl_cr_isen" & gNumeroSessao)
        
    End If
    
End Sub

Sub Cria_TmpRec_Isencao()
    
    Dim TmpRec(7) As DefTable
    
    TmpRec(1).NomeCampo = "nome_ute"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 80
    
    TmpRec(2).NomeCampo = "tipo_ute"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 5
    
    TmpRec(3).NomeCampo = "num_ute"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 20
    
    TmpRec(4).NomeCampo = "situacao"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 15
    
    TmpRec(5).NomeCampo = "tipo_isencao"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 30
    
    TmpRec(6).NomeCampo = "nome_usr"
    TmpRec(6).tipo = "STRING"
    TmpRec(6).tamanho = 80
    
    TmpRec(7).NomeCampo = "empr_id"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 8
    
    Call BL_CriaTabela("sl_cr_isen" & gNumeroSessao, TmpRec)
    
End Sub

Private Sub CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)

    ' Tipo = ANA ou PROD
    ' EmCascata = True : Cria todos elementos at� o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRegProd As New ClRegProdutos
    Dim AuxRegAna As New ClRegAnalises
    Dim AuxRegTubo As New ClRegTubos
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "PROD" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegProd
            Next i
        Else
            Coleccao.Add AuxRegProd
        End If
    ElseIf tipo = "TUBO" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegTubo
            Next i
        Else
            Coleccao.Add AuxRegTubo
        End If
    Else
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegAna
            Next i
        Else
            Coleccao.Add AuxRegAna
        End If
    End If
    
    Set AuxRegAna = Nothing
    Set AuxRegProd = Nothing
    Set AuxRegTubo = Nothing
    
End Sub

Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)

    ' Index = -1 : Apaga todos elementos
    
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        Coleccao.Remove Index
    End If

End Sub

Function Verifica_Prod_ja_Existe(indice As Integer, Produto As String) As Boolean
    
    Dim i As Integer
    Dim k As Integer
    Dim TotalRegistos As Integer
    
    Verifica_Prod_ja_Existe = False
    TotalRegistos = RegistosP.Count - 1
    If indice >= 0 Then
        i = 1
        While i <= TotalRegistos And Verifica_Prod_ja_Existe = False
            If UCase(Trim(Produto)) = UCase(Trim(RegistosP(i).CodProd)) And indice <> i Then
                Verifica_Prod_ja_Existe = True
            End If
            i = i + 1
        Wend
    End If

End Function
Function Verifica_Tubo_ja_Existe(indice As Integer, tubo As String) As Boolean
    
    Dim i As Integer
    Dim k As Integer
    Dim TotalRegistos As Integer
    
    Verifica_Tubo_ja_Existe = False
    TotalRegistos = RegistosT.Count - 1
    If indice >= 0 Then
        i = 1
        While i <= TotalRegistos And Verifica_Tubo_ja_Existe = False
            If UCase(Trim(tubo)) = UCase(Trim(RegistosT(i).CodTubo)) And indice <> i Then
                Verifica_Tubo_ja_Existe = True
            End If
            i = i + 1
        Wend
    End If

End Function

Function Verifica_Ana_ja_Existe(indice As Integer, codAgrup As String) As Boolean
        
    Dim i As Integer
    Dim TotalRegistos As Integer
    Dim CodAgrupAux As String
    Dim codAna As String
    
    codAna = codAgrup
    SELECT_Descr_Ana codAna
    

        
    Verifica_Ana_ja_Existe = False
    TotalRegistos = RegistosA.Count - 1
   
   If InStr(1, "SCP", UCase(Mid(codAgrup, 1, 1))) > 0 Then
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    Else
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    End If

    codAgrup = CodAgrupAux & codAgrup

End Function

Sub LimpaFgProd()
    
    Dim j As Long
    
    ExecutaCodigoP = False
    
    j = FgProd.rows - 1
    While j > 0
        If j > 1 Then
            FgProd.RemoveItem j
        Else
            FgProd.TextMatrix(j, 0) = ""
            FgProd.TextMatrix(j, 1) = ""
            FgProd.TextMatrix(j, 2) = ""
            FgProd.TextMatrix(j, 3) = ""
            FgProd.TextMatrix(j, 4) = ""
            FgProd.TextMatrix(j, 5) = ""
            FgProd.TextMatrix(j, 6) = ""
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoP = True
    LastColP = 0
    LastRowP = 1
        
    CriaNovaClasse RegistosP, 1, "PROD", True

End Sub
Sub LimpaFgTubos()
    
    Dim j As Long
    
    ExecutaCodigoT = False
    
    j = FGTubos.rows - 1
    While j > 0
        If j > 1 Then
            FGTubos.RemoveItem j
        Else
            FGTubos.TextMatrix(j, 0) = ""
            FGTubos.TextMatrix(j, 1) = ""
            FGTubos.TextMatrix(j, 2) = ""
            FGTubos.TextMatrix(j, 3) = ""
            FGTubos.TextMatrix(j, 4) = ""
            FGTubos.TextMatrix(j, 5) = ""
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoT = True
    LastColT = 0
    LastRowT = 1
        
    CriaNovaClasse RegistosT, 1, "TUBO", True

End Sub

Sub LimpaFGAna()
    
    Dim j As Long
    
    ExecutaCodigoA = False
    
    j = FGAna.rows - 1
    While j > 0
        If j > 1 Then
            FGAna.RemoveItem j
        Else
            FGAna.TextMatrix(j, 0) = ""
            FGAna.TextMatrix(j, 1) = ""
            If FGAna.Cols = 4 Then
                FGAna.TextMatrix(j, 2) = ""
                FGAna.TextMatrix(j, 3) = ""
            End If
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoA = True
    LastColA = 0
    LastRowA = 1
        
    CriaNovaClasse RegistosA, 1, "ANA", True

End Sub

Function Procura_Prod_ana() As Boolean

    'Verifica se existem an�lises com o produto que se pretende eliminar
    'No caso de existirem n�o deixa apagar o produto sem que se tenha apagado a an�lise

    Dim RsProd As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset
    
    Procura_Prod_ana = False
    
    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    CmdPesqGhost.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsPesqGhost = CmdPesqGhost.Execute
                    While Not RsPesqGhost.EOF
                        lista = lista & BL_TrataStringParaBD(BL_HandleNull(RsPesqGhost!cod_membro, "-1")) & ","
                        RsPesqGhost.MoveNext
                    Wend
                    RsPesqGhost.Close
                    Set RsPesqGhost = Nothing
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana_s from sl_ana_s WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_ana_s IN ( " & lista & ")"
            
            Set RsProd = New ADODB.recordset
            RsProd.CursorLocation = adUseClient
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_ana_s, "") & ","
                RsProd.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Prod_ana = True
                
                If RsProd.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                End If
            End If
            
            RsProd.Close
            Set RsProd = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
    
End Function
Function Procura_Tubo_ana() As Boolean

    'Verifica se existem an�lises com o tubo que se pretende eliminar
    'No caso de existirem n�o deixa apagar o tubo sem que se tenha apagado a an�lise

    Dim rsTubo As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset
    
    Procura_Tubo_ana = False
    
    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    CmdPesqGhost.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsPesqGhost = CmdPesqGhost.Execute
                    While Not RsPesqGhost.EOF
                        lista = lista & BL_TrataStringParaBD(BL_HandleNull(RsPesqGhost!cod_membro, "-1")) & ","
                        RsPesqGhost.MoveNext
                    Wend
                    RsPesqGhost.Close
                    Set RsPesqGhost = Nothing
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana_s from sl_ana_s WHERE cod_tubo = " & BL_TrataStringParaBD(FGTubos.TextMatrix(FGTubos.row, 0)) & _
                    " AND cod_ana_s IN ( " & lista & ")"
            
            Set rsTubo = New ADODB.recordset
            rsTubo.CursorLocation = adUseClient
            rsTubo.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not rsTubo.EOF
                lista = lista & BL_HandleNull(rsTubo!descr_ana_s, "") & ","
                rsTubo.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Tubo_ana = True
                
                If rsTubo.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                End If
            End If
            
            rsTubo.Close
            Set rsTubo = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
    
End Function

Function ValidaEspecifObrig() As Boolean

    Dim i As Integer
    Dim n As Integer
    
    n = RegistosP.Count
    If n > 0 Then
        n = n - 1
        For i = 1 To n
            If RegistosP(i).CodProd <> "" Then
                If RegistosP(i).EspecifObrig = "S" Then
                    If RegistosP(i).CodEspecif = "" Then
                        BG_Mensagem mediMsgBox, "O produto " & RegistosP(i).DescrProd & " exige especifica��o !", vbInformation, ""
                        SSTGestReq.Tab = 1
                        Exit Function
                    End If
                End If
            End If
        Next
    End If
    ValidaEspecifObrig = True
    
End Function

Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq_cons.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq_cons.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While aux <> "P1"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function
erro:
If Err.Number = 55 Then
    Close 1
    Open s For Input As 1
    Resume Next
End If
End Function


Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function

Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
' XXXX
MyDocInfo.pDocName = "EtiquetaConsultas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function

Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String, _
    ByVal quantidade As String, _
    Optional ByVal N_REQ_ARS As String _
    ) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", RemovePortuguese(StrConv(nome_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", N_REQ_ARS)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)
    sWrittenData = Replace(sWrittenData, "{QUANTIDADE}", BL_HandleNull(quantidade, "1"))

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function

Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function

Private Sub TAna_KeyDown(KeyCode As Integer, Shift As Integer)

    Dim Flg_EncontrouMAReq As Boolean
    Dim Flg_EncontrouRegistosA As Boolean
    Dim i As Integer

    If KeyCode = 46 And InStr(1, TAna.Nodes(TAna.SelectedItem.Index).Key, "S") <> 0 And (InStr(1, TAna.Nodes(TAna.SelectedItem.Index).Key, "C") <> 0 Or InStr(1, TAna.Nodes(TAna.SelectedItem.Index).Key, "P") <> 0) Then   'Delete de um nodo filho (analise simples) de uma complexa ou perfil
        'Apagar analise das estruturas
        Flg_EncontrouMAReq = False
        i = 1
        While Not Flg_EncontrouMAReq
            If MaReq(i).descr_ana_s = TAna.Nodes.item(TAna.SelectedItem.Index).Text Then
                Flg_EncontrouMAReq = True
                Actualiza_Estrutura_MAReq i
            End If
            i = i + 1
        Wend
        If Flg_EncontrouMAReq Then
            TAna.Nodes.Remove (TAna.SelectedItem.Index)
        End If
    End If
    
End Sub

' 02


' Copia uma grid.

Private Function Copia_MSF_Grid(gr1 As MSFlexGrid, _
                               gr2 As MSFlexGrid)
    
    On Error Resume Next
    
    Dim i As Integer
    Dim j As Integer

    gr2.Clear
    
    gr2.Cols = gr1.Cols
    gr2.rows = gr1.rows
    
    ' Copia formata��o.
    gr2.BackColor = gr1.BackColor
    gr2.ForeColor = gr1.ForeColor
    gr2.CellFontName = gr1.CellFontName
    gr2.CellFontSize = gr1.CellFontSize
    gr2.BackColor = gr1.BackColor
'    gr2.GridColor = gr1.GridColor
'    gr2.GridColorFixed = gr1.GridColorFixed
    
    For i = 0 To gr2.Cols - 1
        gr2.ColWidth(i) = gr1.ColWidth(i)
        gr2.ColAlignment(i) = gr1.ColAlignment(i)
    Next
    
    For i = 0 To gr2.rows - 1
        gr2.RowHeight(i) = gr1.RowHeight(i)
    Next
    
    ' ---------------------
    
    ' Copia valores.
    For j = 0 To gr2.rows - 1
        For i = 0 To gr2.Cols - 1
            gr2.TextMatrix(j, i) = gr1.TextMatrix(j, i)
        Next
    Next
    
    ' ---------------------
    
    Copia_MSF_Grid = 1

End Function

' Subterf�gio para introduzir valores (num�ricos) na grid.

Private Sub Altera_Grid(GR As MSFlexGrid, _
                       linha As Integer, _
                       coluna As Integer, _
                       msg As String, _
                       title As String)
    
    On Error GoTo ErrorHandler
    
    Dim rv As String
    Dim aux1 As String
    Dim aux2 As Double
    
    rv = InputBox(msg, title)
    
    If (IsNumeric(rv)) Then
    
        aux1 = Trim(rv)
        rv = BG_CvDecimalParaCalculo(aux1)
        
        aux2 = CDbl(rv)
        rv = Trim(CStr(aux2))
        GR.TextMatrix(linha, coluna) = Format(rv, "0.00")
    Else
        If (Len(rv) > 0) Then
            MsgBox "Deve introduzir um valor num�rico !         ", vbExclamation, "Erro"
        End If
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("Emitir_Recibo_02 (FormGesReqCons) " & Err.Number & " - " & Err.Description)
            
            Exit Sub
    End Select
End Sub

Sub DevolveEpisodio(SeqUtente As String)

    'Preenche Situacao e episodio caso o episodio esteja vazio

    Dim sql As String
    Dim RsEpis As ADODB.recordset
    
    On Error GoTo TrataErro
    
    If Trim(SeqUtente) <> "" Then
        sql = "SELECT n_epis, t_sit FROM sl_identif WHERE seq_utente=" & SeqUtente
        Set RsEpis = New ADODB.recordset
        RsEpis.CursorLocation = adUseClient
        RsEpis.CursorType = adOpenStatic
        RsEpis.Open sql, gConexao
        If RsEpis.RecordCount > 0 Then
            EcEpisodio.Text = BL_HandleNull(RsEpis!n_epis, "")
            BG_MostraComboSel BL_HandleNull(RsEpis!t_sit, "-1"), CbSituacao
        End If
        RsEpis.Close
        Set RsEpis = Nothing
    End If
    
    Exit Sub
    
TrataErro:
    
    BG_LogFile_Erros "Erro ao procurar episodio (DevolveEpisodio) - " & Err.Description
    
End Sub

Sub Lista_GhostMembers(CodComplexa As String, complexa As String)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_membro"
    CamposEcran(1) = "cod_membro"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_membro, sl_ana_s "
    CampoPesquisa = "descr_ana_s"
    CWhere = " sl_membro.cod_membro = sl_ana_s.cod_ana_s and cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                        ChavesPesq, _
                                                                        CamposEcran, _
                                                                        CamposRetorno, _
                                                                        Tamanhos, _
                                                                        Headers, _
                                                                        CWhere, _
                                                                        CFrom, _
                                                                        "", _
                                                                        CampoPesquisa, _
                                                                        " ORDER BY sl_membro.ordem ", _
                                                                        " Membros da an�lise " & complexa)
    
    If PesqRapida = True Then

        FormPesqRapidaAvancadaMultiSel.Show vbModal
        
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            For i = 1 To TotalElementosSel
                InsereGhostMember CStr(resultados(i))
            Next i
            FormGesReqCons.FGAna.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem membros", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGesReqCons" Then
            FormGesReqCons.FGAna.SetFocus
        End If
    End If

End Sub
'Sub BD_Delete()
'    Dim sql As String
'    Dim condicao As String
'    Dim SQLQuery As String
'    Dim MarcaLocal As Variant
'
'
'    sql = "insert into sl_requis_eliminadas(n_req,user_elim,dt_elim,hr_elim) values (" & _
'                EcNumReq.Text & ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
'    BG_ExecutaQuery_ADO sql
'    BG_Trata_BDErro
'
'    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
'    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
'    BG_ExecutaQuery_ADO SQLQuery
'
'    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
'    MarcaLocal = rs.Bookmark
'    rs.Requery
'
'    If rs.BOF And rs.EOF Then
'        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
'        FuncaoEstadoAnterior
'        Exit Sub
'    End If
'
'    If rs.EOF Then rs.MovePrevious
'
'    LimpaCampos
'    PreencheCampos
'
'End Sub


Sub PreencheEFREpisodioGH(Optional episodio As String, Optional situacao As String)
    
    Dim RsCodEFR As ADODB.recordset
    Dim rsNumBenef As ADODB.recordset
    Dim HisAberto As Integer
    
    On Error GoTo ErrorHandler
    
                    
    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
                    
    If HisAberto = 1 Then
    
        If episodio <> "" And situacao <> "-1" And CbTipoUtente.ListIndex <> -1 And EcUtente <> "" Then
            ' Mapeia a situa��o do SISLAB para a GH.
            Select Case situacao
                Case gT_Urgencia
                    situacao = "Urgencias"
                Case gT_Consulta
                    situacao = "Consultas"
                Case gT_Internamento
                    situacao = "Internamentos"
                Case gT_Ficha_ID
                    situacao = "Ficha-ID"
                Case gT_Ambulatorio
                    situacao = "Ambulatorio"
                Case gT_Prescricoes
                    situacao = "Prescricoes"
                
                Case gT_Acidente
                    situacao = "Acidente"
                Case gT_Ambulatorio
                    situacao = "Ambulatorio"
                Case gT_Cons_Inter
                    situacao = "Cons-Inter"
                Case gT_Cons_Telef
                    situacao = "Cons-Telef"
                Case gT_Consumos
                    situacao = "Consumos"
                Case gT_Credenciais
                    situacao = "Credenciais"
                Case gT_Diagnosticos
                    situacao = "Diagnosticos"
                Case gT_Exame
                    situacao = "Exame"
                Case gT_Fisio
                    situacao = "Fisio"
                Case gT_HDI
                    situacao = "Hosp-Dia"
                Case gT_Intervencao
                    situacao = "Intervencao"
                Case gT_MCDT
                    situacao = "Mcdt"
                Case gT_Plano_Oper
                    situacao = "Plano-Oper"
                Case gT_Pre_Intern
                    situacao = "Pre-Intern"
                Case gT_Prog_Cirugico
                    situacao = "Prog-Cirurgico"
                Case gT_Protoc
                    situacao = "Protoc"
                Case gT_Referenciacao
                    situacao = "Referenciacao"
                Case gT_Reg_Oper
                    situacao = "Reg-Oper"
                Case gT_Tratamentos
                    situacao = "Tratamentos"
                Case Else
                    situacao = ""
            End Select
            If situacao <> "" And CbTipoUtente.ListIndex <> -1 And EcUtente <> "" Then
                If gHIS_Import = 1 And UCase(HIS.uID) = UCase("GH") Then
                    'Obter Codigo da EFR da GH
                    Set RsCodEFR = New ADODB.recordset
                    With RsCodEFR
                        .Source = "SELECT obter_cod_resp('" & CbTipoUtente.Text & "','" & EcUtente.Text & "','" & situacao & "','" & episodio & "') CodEFR FROM dual"
                        .CursorType = adOpenStatic
                        .CursorLocation = adUseClient
                        .Open , gConnHIS
                    End With
                    If RsCodEFR.RecordCount > 0 Then
                        EcCodEFR = BL_HandleNull(RsCodEFR!codEfr, 0)
                        EcCodEFR_Validate (True)
                    Else
                        EcCodEFR = ""
                    End If
                    
                    'Obter n�mero do cart�o de beneficiario da GH
                    Set rsNumBenef = New ADODB.recordset
                    With rsNumBenef
                        .Source = "SELECT obter_cartao('" & CbTipoUtente.Text & "','" & EcUtente.Text & "','" & situacao & "','" & episodio & "') NumBenef FROM dual"
                        .CursorType = adOpenStatic
                        .CursorLocation = adUseClient
                        .Open , gConnHIS
                    End With
                    If rsNumBenef.RecordCount > 0 Then
                        EcNumBenef = BL_HandleNull(rsNumBenef!NumBenef, 0)
                    Else
                        EcNumBenef = ""
                    End If
                Else
                    EcCodEFR = ""
                    EcNumBenef = ""
                End If
            End If
    
        End If
        BL_Fecha_conexao_HIS
    End If


Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : PreencheEFRUtente (FormGestaoRequisicao) -> " & Err.Description
            Exit Sub
    End Select
End Sub

' pferreira 2008.10.09
Sub Grava_Agenda()
    Dim sql As String
    If gModuloAgendaV2 <> mediSim Then
        sql = "insert into sl_agenda(n_req,cod_t_agenda,dt_marcacao,flg_activo,cod_local, tipo_req) VALUES (" & _
                EcNumReq.Text & "," & BL_HandleNull(EcTAgenda, 0) & "," & BL_HandleNull(BL_TrataDataParaBD(EcDtMarcacao.Text), "") & "," & "1" & "," & gCodLocal & ",'C')"
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
    End If
    
End Sub

' pferreira 2008.10.09
Sub Actualiza_Agenda()
    Dim sql As String
    Dim iReg As Integer

    sql = "update sl_agenda set dt_marcacao = " & BL_TrataDataParaBD(EcDataPrevista.Text) & _
          "where n_req = " & EcNumReq.Text & " and tipo_req = 'C' AND cod_local = " & gCodLocal
    iReg = BG_ExecutaQuery_ADO(sql)
    If iReg = 0 Then
        Grava_Agenda
    End If
    BG_Trata_BDErro

End Sub

' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Sub EliminaTubo(cod_ana As String)
    Dim i  As Integer
    Dim sSql As String
    Dim sSqlP As String
    Dim sSqlC As String
    Dim rsAna As New ADODB.recordset
    Dim rsTubo As New ADODB.recordset
    Dim CodTubo As String
    Dim flg_PodeEliminar As Boolean
    Dim flg_existeTubo As Boolean
    
    On Error GoTo TrataErro
    
    
    ' SE FOR COMPLEXA CHAMA A MESMA FUNCAO PARA CADA SIMPLES
    If Mid(cod_ana, 1, 1) = "C" Then
        sSqlC = "SELECT cod_tubo FROM sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlC
        rsAna.Open sSqlC, gConexao
        If rsAna.RecordCount > 0 Then
            'Encontrou o c�digo do tubo da complexa
            CodTubo = BL_HandleNull(rsAna!cod_tubo, "")
            GoTo continua
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaTubo rsAna!cod_membro
                    rsAna.MoveNext
                Wend
            End If
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
    
    ' SE FOR PERFIL CHAMA A MESMA FUNCAO PARA CADA MEMBRO
    If Mid(cod_ana, 1, 1) = "P" Then
        sSqlP = "SELECT cod_tubo FROM sl_perfis where cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlP
        rsAna.Open sSqlP, gConexao
        If rsAna.RecordCount > 0 Then
            'Encontrou o c�digo do tubo do perfil
            CodTubo = BL_HandleNull(rsAna!cod_tubo, "")
            GoTo continua
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaTubo rsAna!cod_analise
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If
    
    
    If Mid(cod_ana, 1, 1) = "S" Then
        ' ENCONTRA O CODIGO DO TUBO DA ANALISE EM CAUSA
        CodTubo = ""
        sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            CodTubo = BL_HandleNull(rsAna!cod_tubo, "")
continua:
            ' VERIFICA SE EXISTE TUBO NA FLEX GRID
            flg_existeTubo = False
            For i = 1 To RegistosT.Count - 1
                If RegistosT(i).CodTubo = CodTubo Then
                    flg_existeTubo = True
                End If
            Next
            If flg_existeTubo = False Then
                rsAna.Close
                Set rsAna = Nothing
                Exit Sub
            End If
            ' ---------------------------------------
        End If
        rsAna.Close
        Set rsAna = Nothing
        
        ' SE NAO TEM TUBO CODIFICADO...SAI
        If CodTubo = "" Then
            Exit Sub
        End If
        

        flg_PodeEliminar = VerificaPodeEliminarTubo(CodTubo)
        
        
        ' NAO ENCONTROU MAIS NENHUMA ANALISE COM ESSE TUBO LOGO PODE ELIMINAR
        If flg_PodeEliminar = True Then
            
            ' VERIFICA SE JA FOI DADO ENTRADA AO TUBO
            If EcNumReq <> "" Then
                sSql = "SELECT * FROM sl_req_tubo WHERE n_req =" & EcNumReq
                sSql = sSql & " AND cod_tubo = '" & CodTubo & "'"
                rsTubo.CursorLocation = adUseClient
                rsTubo.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsTubo.Open sSql, gConexao
                If rsTubo.RecordCount > 0 Then
                    If BL_HandleNull(rsTubo!dt_chega, "") <> "" Then
                        ' SE JA DEU ENTRADA NAO ELIMINA
                        rsTubo.Close
                        Set rsTubo = Nothing
                        Exit Sub
                    End If
                End If
            End If
            ' -----------------------------------------
            
            
            ' ELIMINA DA FLEX GRID E DA ESTRUTURA
            For i = 1 To RegistosT.Count - 1
                If RegistosT(i).CodTubo = CodTubo Then
                    FGTubos.row = 1
                    If i < FGTubos.rows - 1 Then
                        RegistosT.Remove i
                        FGTubos.RemoveItem i
                    Else
                        FGTubos.TextMatrix(i, 0) = ""
                        FGTubos.TextMatrix(i, 1) = ""
                        FGTubos.TextMatrix(i, 2) = ""
                        FGTubos.TextMatrix(i, 3) = ""
                        FGTubos.TextMatrix(i, 4) = ""
                        FGTubos.TextMatrix(i, 5) = ""
                        FGTubos.TextMatrix(i, 6) = ""
                        FGTubos.TextMatrix(i, 7) = ""
                        RegistosT(i).CodTubo = ""
                        RegistosT(i).descrTubo = ""
                        RegistosT(i).Garrafa = ""
                        RegistosT(i).DtPrev = ""
                        RegistosT(i).DtChega = ""
                        RegistosT(i).HrChega = ""
                        RegistosT(i).UserChega = ""
                        RegistosT(i).EstadoTubo = ""
                        
                    End If
                    Exit For
                End If
            Next
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoReqcons -> Elimina��o de Tubos: " & Err.Description
    Exit Sub
    Resume Next
End Sub



' FGONCALVES
' 30 JUN 2006 - CHVNG


Private Function VerificaPodeEliminarTubo(CodTubo As String) As Boolean
    Dim i As Integer
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    VerificaPodeEliminarTubo = True
    
    For i = 1 To RegistosA.Count - 1
        
        If Mid(RegistosA(i).codAna, 1, 1) = "C" Then
            flg_Elimina = VerificaPodeEliminarTubo_C(CodTubo, RegistosA(i).codAna)
        ElseIf Mid(RegistosA(i).codAna, 1, 1) = "P" Then
            flg_Elimina = VerificaPodeEliminarTubo_P(CodTubo, RegistosA(i).codAna)
        Else
            flg_Elimina = VerificaPodeEliminarTubo_S(CodTubo, RegistosA(i).codAna)
        End If
        
        If flg_Elimina = False Then
            VerificaPodeEliminarTubo = flg_Elimina
            Exit Function
        End If
    Next
    VerificaPodeEliminarTubo = True
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo"
    Exit Function
    Resume Next
    VerificaPodeEliminarTubo = False
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarTubo_C(CodTubo As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    flg_Elimina = False
    
    sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(analise)
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            flg_Elimina = VerificaPodeEliminarTubo_S(CodTubo, rsAna!cod_membro)
            If flg_Elimina = False Then
                VerificaPodeEliminarTubo_C = flg_Elimina
                rsAna.Close
                Set rsAna = Nothing
                Exit Function
            End If
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarTubo_C = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo_C: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo_C"
    VerificaPodeEliminarTubo_C = False
    Exit Function
    Resume Next
    
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarTubo_P(CodTubo As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    flg_Elimina = False
    
    sSql = "SELECT  cod_tubo from sl_perfis where cod_perfis = " & BL_TrataStringParaBD(analise)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If Not rsAna.EOF Then
        If BL_HandleNull(rsAna!cod_tubo, "") <> "" Then
            If rsAna!cod_tubo <> CodTubo Then
                flg_Elimina = True
            End If
        Else
            Set rsAna = New ADODB.recordset
            sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    If Mid(analise, 1, 1) = "C" Then
                        flg_Elimina = VerificaPodeEliminarTubo_C(CodTubo, rsAna!cod_analise)
                    ElseIf Mid(analise, 1, 1) = "S" Then
                        flg_Elimina = VerificaPodeEliminarTubo_S(CodTubo, rsAna!cod_analise)
                    End If
                    
                    If flg_Elimina = False Then
                        VerificaPodeEliminarTubo_P = flg_Elimina
                        rsAna.Close
                        Set rsAna = Nothing
                        Exit Function
                    End If
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If
        

    
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarTubo_P = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo_P: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo_P"
    VerificaPodeEliminarTubo_P = False
    Exit Function
    Resume Next
    
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarTubo_S(CodTubo As String, analise As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    
    sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(analise)
    sSql = sSql & " AND cod_tubo = " & BL_TrataStringParaBD(CodTubo)
    
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaPodeEliminarTubo_S = False
    Else
        VerificaPodeEliminarTubo_S = True
    End If
    
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo_S: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo_S"
    VerificaPodeEliminarTubo_S = False
    Exit Function
    Resume Next
End Function





' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Sub EliminaProduto(cod_ana As String)
    Dim i  As Integer
    Dim sSql As String
    Dim sSqlP As String
    Dim sSqlC As String
    Dim rsAna As New ADODB.recordset
    Dim RsProd As New ADODB.recordset
    Dim CodProduto As String
    Dim flg_PodeEliminar As Boolean
    Dim flg_existeProd As Boolean
    
    On Error GoTo TrataErro
    
    
    ' SE FOR COMPLEXA CHAMA A MESMA FUNCAO PARA CADA SIMPLES
    If Mid(cod_ana, 1, 1) = "C" Then
        sSqlC = "SELECT cod_produto FROM sl_ana_C WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlC
        rsAna.Open sSqlC, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_membro
                    rsAna.MoveNext
                Wend
            End If
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
    
    ' SE FOR PERFIL CHAMA A MESMA FUNCAO PARA CADA MEMBRO
    If Mid(cod_ana, 1, 1) = "P" Then
        sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        sSqlP = "SELECT cod_produto FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlP
        rsAna.Open sSqlP, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_analise
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If
    
    ' ENCONTRA O CODIGO DO PRODUTO DA ANALISE EM CAUSA
    If Mid(cod_ana, 1, 1) = "S" Then
        CodProduto = ""
        sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
continuap:
            ' VERIFICA SE EXISTE PRODUTO NA FLEX GRID
            flg_existeProd = False
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    flg_existeProd = True
                    Exit For
                End If
            Next
            If flg_existeProd = False Then
                rsAna.Close
                Set rsAna = Nothing
                Exit Sub
            End If
            ' ---------------------------------------
        End If
        rsAna.Close
        Set rsAna = Nothing
        
        ' SE NAO TEM PRODUTO CODIFICADO...SAI
        If CodProduto = "" Then
            Exit Sub
        End If
        
        flg_PodeEliminar = VerificaPodeEliminarProduto(CodProduto)
        
        
        ' NAO ENCONTROU MAIS NENHUMA ANALISE COM ESSE TUBO LOGO PODE ELIMINAR
        If flg_PodeEliminar = True Then
            
            ' VERIFICA SE JA FOI DADO ENTRADA DO PRODUTO
            If EcNumReq <> "" Then
                sSql = "SELECT * FROM sl_req_prod WHERE n_req =" & EcNumReq
                sSql = sSql & " AND cod_prod = " & BL_TrataStringParaBD(CodProduto)
                RsProd.CursorLocation = adUseClient
                RsProd.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsProd.Open sSql, gConexao
                If RsProd.RecordCount > 0 Then
                    If BL_HandleNull(RsProd!dt_chega, "") <> "" Then
                        ' SE JA DEU ENTRADA NAO ELIMINA
                        RsProd.Close
                        Set RsProd = Nothing
                        Exit Sub
                    End If
                End If
                RsProd.Close
                Set RsProd = Nothing
                ' -----------------------------------------
            End If
            
            ' ELIMINA DA FLEX GRID E DA ESTRUTURA
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    FgProd.row = 1
                    If i < FgProd.rows - 1 Then
                        RegistosP.Remove i
                        FgProd.RemoveItem i
                    Else
                        FgProd.TextMatrix(i, 0) = ""
                        FgProd.TextMatrix(i, 1) = ""
                        FgProd.TextMatrix(i, 2) = ""
                        FgProd.TextMatrix(i, 3) = ""
                        FgProd.TextMatrix(i, 4) = ""
                        FgProd.TextMatrix(i, 5) = ""
                        FgProd.TextMatrix(i, 6) = ""
                        FgProd.TextMatrix(i, 7) = ""
                        RegistosP(i).CodProd = ""
                        RegistosP(i).DescrProd = ""
                        RegistosP(i).EspecifObrig = ""
                        RegistosP(i).CodEspecif = ""
                        RegistosP(i).DescrEspecif = ""
                        RegistosP(i).DtPrev = ""
                        RegistosP(i).DtChega = ""
                        RegistosP(i).EstadoProd = ""
                        RegistosP(i).Volume = ""
                    End If
                    Exit For
                End If
            Next
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoReqCons -> Elimina��o de Produtos: " & Err.Description
    Exit Sub
    Resume Next
End Sub


Private Function VerificaPodeEliminarProduto(CodProduto As String) As Boolean
    Dim i As Integer
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    
    VerificaPodeEliminarProduto = True
    
    For i = 1 To RegistosA.Count - 1
        
        If Mid(RegistosA(i).codAna, 1, 1) = "C" Then
            flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, RegistosA(i).codAna)
        ElseIf Mid(RegistosA(i).codAna, 1, 1) = "P" Then
            flg_Elimina = VerificaPodeEliminarProduto_P(CodProduto, RegistosA(i).codAna)
        Else
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, RegistosA(i).codAna)
        End If
        
        If flg_Elimina = False Then
            VerificaPodeEliminarProduto = flg_Elimina
            Exit Function
        End If
    Next
    VerificaPodeEliminarProduto = True
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto"
    VerificaPodeEliminarProduto = False
    Exit Function
    Resume Next
End Function





' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_C(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
     On Error GoTo TrataErro
   
    flg_Elimina = False
    
    sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(analise)
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, rsAna!cod_membro)
            If flg_Elimina = False Then
                VerificaPodeEliminarProduto_C = flg_Elimina
                rsAna.Close
                Set rsAna = Nothing
                Exit Function
            End If
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarProduto_C = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_C: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_C"
    VerificaPodeEliminarProduto_C = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_P(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    
    flg_Elimina = False
    
    sSql = "SELECT cod_produto from sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If Not rsAna.EOF Then
        If BL_HandleNull(rsAna!cod_produto, "") <> "" Then
            If rsAna!cod_produto <> CodProduto Then
                flg_Elimina = True
            End If
        Else
            sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    If Mid(analise, 1, 1) = "C" Then
                        flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, rsAna!cod_analise)
                    ElseIf Mid(analise, 1, 1) = "S" Then
                        'flg_Elimina = VerificaPodeEliminarTubo_S(CodProduto, RsAna!cod_analise)
                        flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, rsAna!cod_analise)
                    End If
                    
                    If flg_Elimina = False Then
                        VerificaPodeEliminarProduto_P = flg_Elimina
                        rsAna.Close
                        Set rsAna = Nothing
                        Exit Function
                    End If
                    rsAna.MoveNext
                Wend
            End If
        End If
        
    End If
    
        
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarProduto_P = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_P: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_P"
    VerificaPodeEliminarProduto_P = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_S(CodProduto As String, analise As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    
    sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(analise)
    sSql = sSql & " AND cod_produto = " & BL_TrataStringParaBD(CodProduto)
    
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaPodeEliminarProduto_S = False
    Else
        VerificaPodeEliminarProduto_S = True
    End If
    
    rsAna.Close
    Set rsAna = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_S: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_S"
    VerificaPodeEliminarProduto_S = False
    Exit Function
    Resume Next
End Function



''''''''''''''''''''''''''''''''''''''''''''
' Insere requisicao em fila de espera ''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub InsertFilaEspera()
    
    Dim sql As String
    Dim morada As String
    Dim Sexo As String
    Dim efr As String
    Dim RsStack As ADODB.recordset
    Set RsStack = New ADODB.recordset
        
    On Error GoTo Trata_Erro
    ' Procura dados sobre o utente
    sql = "SELECT descr_mor_ute, sexo_ute, cod_efr_ute, descr_efr FROM (SELECT * FROM sl_identif " & _
             "WHERE seq_utente = " & EcSeqUtente & ") a, sl_efr WHERE cod_efr = a.cod_efr_ute"
    If gModoDebug = mediSim Then BG_LogFile_Erros CStr(sql)
    RsStack.Open sql, gConexao, adOpenStatic, adLockOptimistic
    
    If (RsStack.RecordCount > 0) Then
        morada = BL_TrataStringParaBD(BL_HandleNull(RsStack!descr_mor_ute, ""))
        Sexo = BL_SelCodigo("SL_TBF_SEXO", "DESCR_SEXO", "COD_SEXO", BL_HandleNull(RsStack!sexo_ute, 0))
        efr = BL_TrataStringParaBD(BL_HandleNull(RsStack!descr_efr, ""))
    Else
        morada = "''"
        Sexo = "''"
        efr = "''"
    End If
    
    ' Apaga a requisicao existentes com a requisicao corrente
    BG_ExecutaQuery_ADO "DELETE FROM sl_fila_espera WHERE n_req = " & EcNumReq
    BG_Trata_BDErro
    
    ' Insere nova requisicao em fila de espera
    If (RsStack.state = adStateOpen) Then: RsStack.Close
    sql = "INSERT INTO sl_fila_espera (n_req,nome_utente,seq_utente," & _
        "cod_local,cod_sala,prioridade,dt_cri,hr_cri,user_cri,dt_nasc,idade,sexo,entidade,morada," & _
        "medico,flg_proc, user_registo) VALUES (" & EcNumReq & "," & BL_TrataStringParaBD(EcNome) & "," & _
        EcSeqUtente & "," & gCodLocal & ",0," & BL_HandleNull(EcPrColheita, 2) & _
        "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & _
        "," & EcUtilizadorCriacao & "," & BL_TrataDataParaBD(EcDataNasc.Text) & "," & _
        BL_TrataStringParaBD(BG_CalculaIdade(CDate(EcDataNasc.Text))) & "," & _
        BL_TrataStringParaBD(Sexo) & "," & BL_TrataStringParaBD(efr) & "," & BL_TrataStringParaBD(morada) & "," & BL_TrataStringParaBD(EcDescrMedico.Text) & ",0," & _
        BL_TrataStringParaBD(BL_HandleNull(EcUtilizadorCriacao, gCodUtilizador)) & ")"

    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsStack.Open sql, gConexao, adOpenStatic, adLockOptimistic

    ' Fecha conexao
    If (RsStack.state = adStateOpen) Then: RsStack.Close
    Exit Sub
    
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro ao inserir em fila de espera!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoReqcons: InsertFilaEspera -> " & Err.Description
    BG_RollbackTransaction
    
    ' Fecha conexao
    If (RsStack.state = adStateOpen) Then: RsStack.Close
    Exit Sub
    Resume Next
End Sub


' PFerreira 15.03.2007
Private Function IsChild() As Boolean
    On Error GoTo TrataErro
    If EcDataNasc <> "" Then
        If InStr(1, Trim(Replace(Replace(BG_CalculaIdade(CDate(EcDataNasc)), "anos", ""), "ano", "")), "meses") = 0 Then
            If Trim(Replace(Replace(Replace(BG_CalculaIdade(CDate(EcDataNasc)), "anos", ""), "dia", ""), "ano", "")) <= 13 Then
                IsChild = True
            Else
                IsChild = False
            End If
        Else
            IsChild = True
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "IsChild: " & Err.Number & " - " & Err.Description, Me.Name, "IsChild"
    IsChild = False
    Exit Function
    Resume Next
End Function


Private Function VerificaUtenteMarcacoesPosteriores() As String
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    On Error GoTo TrataErro
    
    VerificaUtenteMarcacoesPosteriores = ""
    sSql = "SELECT * FROM sl_requis_consultas WHERE seq_utente = " & EcSeqUtente
    sSql = sSql & " AND dt_previ > " & BL_TrataDataParaBD(BL_HandleNull(EcDataPrevista, Bg_DaData_ADO))
    sSql = sSql & " ORDER by dt_previ "
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao, adOpenStatic, adLockOptimistic
    If rsReq.RecordCount > 0 Then
        VerificaUtenteMarcacoesPosteriores = BL_HandleNull(rsReq!n_req, "") & " (" & BL_HandleNull(rsReq!dt_previ, "") & ")"
    End If
    rsReq.Close
    Set rsReq = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaUtenteMarcacoesPosteriores: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaUtenteMarcacoesPosteriores"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE ANALISE FOI MARCACDA PELO CODIGO DE BARRAS DO P1

' ------------------------------------------------------------------------------------------------
Private Function MarcacaoCodigoBarrasP1(codAna As String) As String
    On Error GoTo TrataErro
    Dim codRubrEfr As String
    Dim rsAnaFact As New ADODB.recordset
    Dim sSql As String
    Dim temp  As String
    Dim i As Integer
    Dim MultAna As String
    If codAna <> "" Then
        codRubrEfr = IF_ObtemRubrParaEFR(EcCodEFR, codAna)
        If codRubrEfr <> "" Then
            sSql = "SELECT x1.cod_ana FROM sl_ana_facturacao x1, slv_analises_factus x2 WHERE replace(x1.cod_ana_gh,'A','') IN " & codRubrEfr
            sSql = sSql & " AND x1.cod_ana = x2.cod_ana and (x2.flg_invisivel = 0 or x2.flg_invisivel is null)"
            sSql = sSql & " AND x2.cod_ana IN (SELECT cod_ana FROM Sl_ana_locais WHERE cod_local = " & gCodLocal & ")"
            sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataPrevista.Text, Bg_DaData_ADO)) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAnaFact.CursorLocation = adUseClient
            rsAnaFact.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnaFact.Open sSql, gConexao
            If rsAnaFact.RecordCount = 0 Then
                MarcacaoCodigoBarrasP1 = ""
                rsAnaFact.Close
                BG_Mensagem mediMsgBox, "Mapeamento inexistente!", vbOKOnly + vbInformation, "An�lises"
                Set rsAnaFact = Nothing
                Exit Function
            ElseIf rsAnaFact.RecordCount >= 1 Then
                If rsAnaFact.RecordCount > 1 Then
                    'MsgBox codRubrEfr
                    MultAna = "("
                    While Not rsAnaFact.EOF
                        MultAna = MultAna & BL_TrataStringParaBD(rsAnaFact!cod_ana) & ","
                        rsAnaFact.MoveNext
                    Wend
                    MultAna = Mid(MultAna, 1, Len(MultAna) - 1) & ")"
                    AdicionaAnaliseMulti MultAna, BL_HandleNull(EcCredAct, "1")
                Else
                    MultAna = ""
                rsAnaFact.MoveFirst
                MarcacaoCodigoBarrasP1 = BL_HandleNull(rsAnaFact!cod_ana, "")
                AdicionaAnalise MarcacaoCodigoBarrasP1, BL_HandleNull(EcCredAct, "1")
                End If
            End If
            rsAnaFact.Close
            Set rsAnaFact = Nothing
        
        Else
            BG_Mensagem mediMsgBox, "R�brica inexistente!", vbOKOnly + vbInformation, "An�lises"
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "MarcacaoCodigoBarrasP1: " & Err.Number & " - " & Err.Description, Me.Name, "MarcacaoCodigoBarrasP1"
    MarcacaoCodigoBarrasP1 = False
    Exit Function
    Resume Next
End Function


Private Sub AdicionaAnalise(ByVal analise As String, credencial As String)
    On Error GoTo TrataErro
    Dim cod_Agrup_aux As String
    Dim d As String
    If Verifica_Ana_ja_Existe(LastRowA, analise) = False Then
    If Trim(UCase(analise)) <> Trim(UCase(RegistosA(LastRowA).codAna)) Then
        If Insere_Nova_Analise(LastRowA, analise) = True Then
            cod_Agrup_aux = analise
            d = SELECT_Descr_Ana(cod_Agrup_aux)
            'o estado deixa de estar "Sem Analises"
            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
                EcEstadoReq = gEstadoReqEsperaProduto
            End If

            If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                'Cria linha vazia
                FGAna.AddItem ""
                CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
            End If
         
        End If
    End If
Else
    Beep
    
    BG_Mensagem mediMsgStatus, "An�lise j� indicada!"
End If
FGAna.Col = 0
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaAnalise: " & Err.Description, Me.Name, "AdicionaAnalise", False
    Exit Sub
    Resume Next
End Sub

Private Sub AdicionaAnaliseMulti(multiAna As String, credencial As String)

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "slv_analises_apenas "
    CWhere = " cod_ana in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") AND (flg_invisivel is null or flg_invisivel = 0) AND (inibe_marcacao is null or inibe_marcacao = 0) AND cod_ana IN " & multiAna
    CampoPesquisa = "descr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            FGAna.row = FGAna.rows - 1
            LastRowA = FGAna.row
            AdicionaAnalise BL_SelCodigo("SLV_ANALISES_APENAS", "COD_ANA", "SEQ_ANA", resultados(i)), credencial
        Next i
        

    End If
End Sub
'edgar.parada - CHMA-1412 02.10.2018
Function validaCancelamento(n_req As String) As Boolean
Dim rs As New ADODB.recordset
Dim sql As String

On Error GoTo TrataErro

    sql = "SELECT * FROM sl_requis_consultas WHERE n_req = " & BL_TrataStringParaBD(n_req) & " AND flg_invisivel = 1"

    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao

    If rs.RecordCount > 0 Then
        validaCancelamento = False 'N�o pode cancelar
    Else
        validaCancelamento = True  'Pode cancelar
    End If

Exit Function
TrataErro:
    validaCancelamento = False
    BG_LogFile_Erros "validaCancelamento: " & Err.Number & " - " & Err.Description, Me.Name, "validaCancelamento"
    Exit Function
    Resume Next
End Function
'
'edgar.parada - CHMA-1412 02.10.2018
Sub BD_Delete()
    Dim sql As String
    Dim erro As Integer
    On Error GoTo TrataErro
    
    If validaCancelamento(EcNumReq.Text) Then
       sql = "UPDATE sl_requis_consultas SET estado_req = " & BL_TrataStringParaBD(gEstadoReqCancelada) & ", flg_invisivel=1" & " WHERE n_req = " & BL_TrataStringParaBD(EcNumReq.Text)
       BG_ExecutaQuery_ADO (sql)
       
       erro = BG_Trata_BDErro
       
       If erro = 0 Then
          EcEstadoReq = gEstadoReqCancelada
       End If
    Else
      BG_Mensagem mediMsgBox, "Colheita j� foi activada! N�o pode ser cancelada!", vbInformation, App.ProductName
      Exit Sub
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BD_Delete: " & Err.Number & " - " & Err.Description, Me.Name, "BD_Delete"
    Exit Sub
    Resume Next
End Sub
'

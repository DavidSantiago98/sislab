VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormGestaoRequisicao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "EcPrColheita"
   ClientHeight    =   9330
   ClientLeft      =   2775
   ClientTop       =   3120
   ClientWidth     =   13185
   Icon            =   "FormGestaoRequisicao.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   9330
   ScaleWidth      =   13185
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.TextBox EcNumColheita 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   315
      Left            =   3960
      Locked          =   -1  'True
      TabIndex        =   303
      Top             =   600
      Width           =   1215
   End
   Begin VB.TextBox EcPrescricao 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   298
      Top             =   600
      Width           =   975
   End
   Begin VB.TextBox EcTAgenda 
      Enabled         =   0   'False
      Height          =   285
      Left            =   12720
      Locked          =   -1  'True
      TabIndex        =   297
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodUSaude 
      Height          =   375
      Left            =   11640
      TabIndex        =   289
      Top             =   11640
      Width           =   615
   End
   Begin VB.TextBox EcFlgDiagSec 
      Height          =   285
      Left            =   4800
      TabIndex        =   287
      Top             =   13320
      Width           =   375
   End
   Begin VB.TextBox EcFlgAnexo 
      Height          =   375
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   275
      Top             =   12960
      Width           =   375
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   9360
      Locked          =   -1  'True
      TabIndex        =   265
      Top             =   12240
      Width           =   375
   End
   Begin VB.TextBox EcDescrEstado 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   245
      Top             =   120
      Width           =   4455
   End
   Begin VB.TextBox EcAbrevUte 
      Height          =   285
      Left            =   3360
      TabIndex        =   239
      Top             =   11520
      Width           =   1335
   End
   Begin VB.TextBox EcSexoUte 
      Height          =   285
      Left            =   9360
      TabIndex        =   237
      Top             =   11520
      Width           =   495
   End
   Begin VB.TextBox EcCodArquivo 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      TabIndex        =   225
      Top             =   14640
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcDataFact 
      Alignment       =   2  'Center
      Height          =   315
      Left            =   0
      TabIndex        =   224
      Top             =   18960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame FrameGarrafa 
      Caption         =   "Garrafa de Hemocultura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1545
      Left            =   9720
      TabIndex        =   211
      Top             =   5520
      Width           =   3255
      Begin VB.TextBox EcGarrafa 
         Height          =   285
         Left            =   120
         TabIndex        =   214
         Top             =   840
         Width           =   2055
      End
      Begin VB.CommandButton BtGarrafaOK 
         Height          =   255
         Left            =   2280
         Picture         =   "FormGestaoRequisicao.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   213
         Top             =   840
         Width           =   615
      End
      Begin VB.CommandButton BtGarrafaVoltar 
         Height          =   255
         Left            =   2280
         Picture         =   "FormGestaoRequisicao.frx":044E
         Style           =   1  'Graphical
         TabIndex        =   212
         Top             =   1200
         Width           =   615
      End
      Begin VB.Label LbGarrafa 
         Height          =   975
         Left            =   240
         TabIndex        =   215
         Top             =   360
         Width           =   2415
      End
   End
   Begin VB.TextBox EcReqGrupoEspecial 
      Height          =   285
      Left            =   7320
      TabIndex        =   201
      Top             =   12480
      Width           =   975
   End
   Begin VB.TextBox EcTipoUrgencia 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   4320
      TabIndex        =   199
      Top             =   12600
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcPrColheita 
      Height          =   285
      Left            =   9360
      TabIndex        =   194
      Top             =   11880
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcinfComplementar 
      Height          =   285
      Left            =   6840
      TabIndex        =   192
      Top             =   12120
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapTubo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   10320
      TabIndex        =   173
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcAuxTubo 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   171
      Top             =   12240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcCodMotivoRejFact 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   1200
      TabIndex        =   168
      Top             =   12480
      Width           =   615
   End
   Begin VB.TextBox EcDataTrataRejFact 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   2400
      TabIndex        =   166
      Top             =   12480
      Width           =   975
   End
   Begin VB.TextBox EcHoraTrataRejFact 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   3360
      TabIndex        =   165
      Top             =   12480
      Width           =   735
   End
   Begin VB.TextBox EcUtilTrataRejFact 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   1800
      TabIndex        =   164
      Top             =   12480
      Width           =   615
   End
   Begin VB.TextBox EcMotivoRejFact 
      Alignment       =   2  'Center
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   2400
      TabIndex        =   163
      Top             =   120
      Visible         =   0   'False
      Width           =   3255
   End
   Begin VB.TextBox EcHoraChegada 
      Height          =   285
      Left            =   4920
      Locked          =   -1  'True
      TabIndex        =   162
      Top             =   12120
      Width           =   975
   End
   Begin VB.Frame Frame6 
      Caption         =   "Registo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2175
      Left            =   120
      TabIndex        =   39
      Top             =   7080
      Width           =   12975
      Begin VB.TextBox EcDtSMS 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10920
         Locked          =   -1  'True
         TabIndex        =   284
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcHrSMS 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11880
         Locked          =   -1  'True
         TabIndex        =   283
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcUtilSMS 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8040
         Locked          =   -1  'True
         TabIndex        =   282
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcUtilEmail 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   279
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox EcHrEmail 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   278
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox EcDtEmail 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   277
         Top             =   1680
         Width           =   975
      End
      Begin VB.TextBox EcDtConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   207
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox EcHrConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   206
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox EcUtilConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   205
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox EcLocal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8040
         TabIndex        =   183
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox EcUtilizadorFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   180
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcHoraFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5520
         TabIndex        =   179
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox EcDataFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         TabIndex        =   178
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcDataAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         TabIndex        =   27
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcHoraAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5520
         TabIndex        =   28
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcUtilizadorAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   25
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcHoraImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11880
         TabIndex        =   33
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8040
         TabIndex        =   35
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcDataImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10920
         TabIndex        =   32
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcHoraImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11880
         TabIndex        =   38
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8040
         TabIndex        =   30
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcDataImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10920
         TabIndex        =   37
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcHoraCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5520
         TabIndex        =   23
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox EcDataCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         TabIndex        =   22
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   20
         Top             =   240
         Width           =   615
      End
      Begin VB.Label EcEresultsEstado 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   10920
         TabIndex        =   293
         Top             =   1680
         Width           =   1935
      End
      Begin VB.Label EcEresultsData 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8040
         TabIndex        =   292
         Top             =   1680
         Width           =   2895
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         Caption         =   "eResults"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   7080
         TabIndex        =   291
         Top             =   1680
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "SMS"
         Height          =   255
         Index           =   40
         Left            =   7080
         TabIndex        =   286
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label EcUtilNomeSMS 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8640
         TabIndex        =   285
         Top             =   960
         Width           =   2295
      End
      Begin VB.Label EcUtilNomeEmail 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   281
         Top             =   1680
         Width           =   2895
      End
      Begin VB.Label Label1 
         Caption         =   "Email"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   38
         Left            =   120
         TabIndex        =   280
         Top             =   1680
         Width           =   975
      End
      Begin VB.Label LbAssinatura 
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   7080
         TabIndex        =   271
         Top             =   1680
         Width           =   3855
      End
      Begin VB.Label Label1 
         Caption         =   "Confirma��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   209
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label EcUtilNomeConf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   208
         Top             =   1320
         Width           =   2895
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         Caption         =   "Local Requis."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   7080
         TabIndex        =   185
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label LbNomeLocal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8640
         TabIndex        =   184
         Top             =   1320
         Width           =   4215
      End
      Begin VB.Label Label1 
         Caption         =   "Fecho"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   182
         Top             =   960
         Width           =   855
      End
      Begin VB.Label LbNomeUtilFecho 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   181
         Top             =   960
         Width           =   2895
      End
      Begin VB.Label LbNomeUtilImpressao2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8640
         TabIndex        =   36
         Top             =   600
         Width           =   2295
      End
      Begin VB.Label LbNomeUtilImpressao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8640
         TabIndex        =   31
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label LbNomeUtilCriacao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   21
         Top             =   240
         Width           =   2895
      End
      Begin VB.Label LbNomeUtilAlteracao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   26
         Top             =   600
         Width           =   2895
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "�ltima 2� Via"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   11
         Left            =   7080
         TabIndex        =   34
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Impress�o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   10
         Left            =   7080
         TabIndex        =   29
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Altera��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   24
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Cria��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.CommandButton BtGetSONHO 
      Enabled         =   0   'False
      Height          =   315
      Left            =   5160
      MaskColor       =   &H00C0C0FF&
      Picture         =   "FormGestaoRequisicao.frx":09D8
      Style           =   1  'Graphical
      TabIndex        =   146
      ToolTipText     =   "Importar do SONHO"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcRequAX 
      BackColor       =   &H00E9FEFD&
      Height          =   285
      Left            =   4200
      TabIndex        =   132
      Top             =   12120
      Width           =   615
   End
   Begin VB.TextBox EcPesqRapArquivo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7560
      TabIndex        =   125
      Top             =   11520
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapMedico2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7440
      TabIndex        =   123
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcGrupoAna 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9600
      TabIndex        =   117
      Top             =   16000
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   9960
      TabIndex        =   116
      Top             =   11760
      Width           =   1095
   End
   Begin VB.TextBox EcPrinterResumo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4680
      TabIndex        =   82
      Top             =   16000
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcImprimirResumo 
      Height          =   285
      Left            =   9840
      TabIndex        =   81
      Top             =   17000
      Width           =   975
   End
   Begin VB.TextBox EcEtqNCopias 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   79
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNEsp 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   77
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqTipo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   75
      Top             =   11640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcReqAssociada 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8520
      TabIndex        =   4
      ToolTipText     =   "Requisi��o Associada"
      Top             =   120
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Dados do Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   18
      Top             =   960
      Width           =   12975
      Begin VB.PictureBox PicFem 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   6720
         Picture         =   "FormGestaoRequisicao.frx":0CEA
         ScaleHeight     =   375
         ScaleWidth      =   375
         TabIndex        =   236
         ToolTipText     =   "Feminino"
         Top             =   550
         Width           =   375
      End
      Begin VB.PictureBox PicMasc 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   6720
         Picture         =   "FormGestaoRequisicao.frx":1454
         ScaleHeight     =   375
         ScaleWidth      =   375
         TabIndex        =   235
         ToolTipText     =   "Masculino"
         Top             =   550
         Width           =   375
      End
      Begin VB.ComboBox CbTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton BtPesquisaUtente 
         Height          =   315
         Left            =   6720
         Picture         =   "FormGestaoRequisicao.frx":1BBE
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Pesquisa R�pida de Utentes"
         Top             =   240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8400
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   1095
      End
      Begin VB.TextBox EcNumCartaoUte 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11280
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         TabIndex        =   7
         Top             =   240
         Width           =   4215
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   600
         Width           =   5415
      End
      Begin VB.TextBox EcProcHosp1 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8400
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Data de Entrada"
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcProcHosp2 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11280
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Proc Hospit."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   10440
         TabIndex        =   276
         Top             =   240
         Width           =   870
      End
      Begin VB.Label LbReqAssoci 
         AutoSize        =   -1  'True
         Caption         =   "Cart�o Ute."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   10440
         TabIndex        =   16
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   810
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7440
         TabIndex        =   14
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   795
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Proc Hospit."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7440
         TabIndex        =   9
         Top             =   240
         Width           =   870
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   12
         Top             =   600
         Width           =   405
      End
   End
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   73
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNTubos 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   71
      Top             =   17000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNAdm 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   69
      Top             =   17000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcTEFR 
      BackColor       =   &H00EDFAED&
      Enabled         =   0   'False
      Height          =   285
      Left            =   6600
      TabIndex        =   67
      Top             =   11760
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcAuxAna 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      MaxLength       =   19
      TabIndex        =   65
      Top             =   11760
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSComctlLib.ImageList ListaImagensAna 
      Left            =   9720
      Top             =   12360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":1F48
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":20A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":21FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":2356
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":24B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":260A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":2764
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":28BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":2A18
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":2B72
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":2CCC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicao.frx":2E26
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.TextBox EcAuxProd 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   63
      Top             =   11880
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcCodIsencao 
      Height          =   285
      Left            =   7440
      TabIndex        =   61
      Top             =   17000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton BtCancelarReq 
      Height          =   315
      Left            =   6840
      Picture         =   "FormGestaoRequisicao.frx":2F80
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Raz�o cancelamento"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox CodAnaTemp 
      Height          =   285
      Left            =   5400
      TabIndex        =   59
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcNumReq 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapProduto 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9600
      TabIndex        =   57
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcSeqUtente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3240
      TabIndex        =   56
      Top             =   16000
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Timer TimerRS 
      Interval        =   60000
      Left            =   120
      Top             =   16000
   End
   Begin VB.ComboBox CbEstadoReqAux 
      Height          =   315
      Left            =   8400
      Style           =   2  'Dropdown List
      TabIndex        =   54
      TabStop         =   0   'False
      Top             =   16000
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox EcPesqRapMedico 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7440
      TabIndex        =   52
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapProven 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7440
      TabIndex        =   50
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEstadoReq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3360
      TabIndex        =   48
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcTaxaModeradora 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5400
      TabIndex        =   44
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPagEnt 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5400
      TabIndex        =   43
      Top             =   17000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPagUte 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3240
      TabIndex        =   42
      Top             =   16000
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcUrgencia 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3240
      TabIndex        =   40
      Top             =   17000
      Visible         =   0   'False
      Width           =   375
   End
   Begin TabDlg.SSTab SSTGestReq 
      Height          =   5055
      Left            =   120
      TabIndex        =   167
      Top             =   2040
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   8916
      _Version        =   393216
      Style           =   1
      Tabs            =   8
      Tab             =   7
      TabsPerRow      =   8
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Dados da Requisi��o"
      TabPicture(0)   =   "FormGestaoRequisicao.frx":360A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Labe41(4)"
      Tab(0).Control(1)=   "Label1(4)"
      Tab(0).Control(2)=   "Label1(5)"
      Tab(0).Control(3)=   "Label40(0)"
      Tab(0).Control(4)=   "Label1(3)"
      Tab(0).Control(5)=   "Label1(2)"
      Tab(0).Control(6)=   "Label1(1)"
      Tab(0).Control(7)=   "Label1(6)"
      Tab(0).Control(8)=   "Labe41(5)"
      Tab(0).Control(9)=   "LaArquivo"
      Tab(0).Control(10)=   "LaSemanas"
      Tab(0).Control(11)=   "LaDataFact"
      Tab(0).Control(12)=   "LaMedico"
      Tab(0).Control(13)=   "Label1(100)"
      Tab(0).Control(14)=   "Label1(7)"
      Tab(0).Control(15)=   "LbPrioColheita"
      Tab(0).Control(16)=   "Label1(13)"
      Tab(0).Control(17)=   "Label1(14)"
      Tab(0).Control(18)=   "Label1(15)"
      Tab(0).Control(19)=   "LbAdmin"
      Tab(0).Control(20)=   "EcDescrMedico"
      Tab(0).Control(21)=   "CkIsento"
      Tab(0).Control(22)=   "EcNumBenef"
      Tab(0).Control(23)=   "CbTipoIsencao"
      Tab(0).Control(24)=   "Frame1"
      Tab(0).Control(25)=   "FrEtiq"
      Tab(0).Control(26)=   "Frame4"
      Tab(0).Control(27)=   "EcCodEFR"
      Tab(0).Control(28)=   "BtPesquisaEntFin"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "EcCodProveniencia"
      Tab(0).Control(30)=   "BtPesquisaProveniencia"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "EcDataChegada"
      Tab(0).Control(32)=   "EcDataPrevista"
      Tab(0).Control(33)=   "CbSituacao"
      Tab(0).Control(34)=   "CbUrgencia"
      Tab(0).Control(35)=   "EcObsReq"
      Tab(0).Control(36)=   "EcObsProveniencia"
      Tab(0).Control(37)=   "EcCodMedico"
      Tab(0).Control(38)=   "BtRegistaMedico"
      Tab(0).Control(39)=   "BtPesquisaMedico"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "EcDtEntrega"
      Tab(0).Control(41)=   "CbPrioColheita"
      Tab(0).Control(42)=   "CbDestino"
      Tab(0).Control(43)=   "BtPesquisaSala"
      Tab(0).Control(43).Enabled=   0   'False
      Tab(0).Control(44)=   "EcCodSala"
      Tab(0).Control(45)=   "BtPesquisaValencia"
      Tab(0).Control(45).Enabled=   0   'False
      Tab(0).Control(46)=   "EcDescrValencia"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).Control(47)=   "EcCodValencia"
      Tab(0).Control(48)=   "Frame10"
      Tab(0).Control(49)=   "Frame5"
      Tab(0).Control(50)=   "CkReqHipocoagulados"
      Tab(0).Control(51)=   "Frame8(0)"
      Tab(0).Control(52)=   "Frame7(0)"
      Tab(0).Control(53)=   "EcDescrEFR"
      Tab(0).Control(53).Enabled=   0   'False
      Tab(0).Control(54)=   "EcNomeMedico"
      Tab(0).Control(54).Enabled=   0   'False
      Tab(0).Control(55)=   "EcDescrProveniencia"
      Tab(0).Control(55).Enabled=   0   'False
      Tab(0).Control(56)=   "EcDescrSala"
      Tab(0).Control(56).Enabled=   0   'False
      Tab(0).Control(57)=   "FrameDomicilio"
      Tab(0).Control(58)=   "CkAvisarSMS"
      Tab(0).Control(59)=   "Frame8(1)"
      Tab(0).Control(60)=   "EcEpisodio"
      Tab(0).Control(61)=   "EcEpisodioAux"
      Tab(0).ControlCount=   62
      TabCaption(1)   =   "Pr&odutos"
      TabPicture(1)   =   "FormGestaoRequisicao.frx":3626
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FrBtProd"
      Tab(1).Control(1)=   "FrProdutos"
      Tab(1).Control(2)=   "ListaDiagnSec"
      Tab(1).Control(3)=   "FrameObsEspecif"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "&An�lises"
      TabPicture(2)   =   "FormGestaoRequisicao.frx":3642
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FrAnalises"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Reci&bos"
      TabPicture(3)   =   "FormGestaoRequisicao.frx":365E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "Reci&bos"
      TabPicture(4)   =   "FormGestaoRequisicao.frx":367A
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      TabCaption(5)   =   "&Tubos"
      TabPicture(5)   =   "FormGestaoRequisicao.frx":3696
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "FrTubos"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Colheita"
      TabPicture(6)   =   "FormGestaoRequisicao.frx":36B2
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Label7"
      Tab(6).Control(1)=   "Label20"
      Tab(6).Control(2)=   "Label22"
      Tab(6).Control(3)=   "LabelColheita"
      Tab(6).Control(4)=   "EcDtColheita"
      Tab(6).Control(5)=   "EcHrColheita"
      Tab(6).Control(6)=   "EcUserColheita"
      Tab(6).Control(7)=   "EcCodUserColheita"
      Tab(6).Control(8)=   "BtPesquisaColheita"
      Tab(6).Control(8).Enabled=   0   'False
      Tab(6).Control(9)=   "EcDescrUserColheita"
      Tab(6).Control(9).Enabled=   0   'False
      Tab(6).ControlCount=   10
      TabCaption(7)   =   "Pedidos Adicionais"
      TabPicture(7)   =   "FormGestaoRequisicao.frx":36CE
      Tab(7).ControlEnabled=   -1  'True
      Tab(7).Control(0)=   "FrPedAdicional"
      Tab(7).Control(0).Enabled=   0   'False
      Tab(7).ControlCount=   1
      Begin VB.Frame FrPedAdicional 
         Height          =   4320
         Left            =   120
         TabIndex        =   305
         Top             =   360
         Width           =   12735
         Begin MSFlexGridLib.MSFlexGrid FgPedAdiciona 
            Height          =   3195
            Left            =   120
            TabIndex        =   306
            Top             =   240
            Width           =   12420
            _ExtentX        =   21908
            _ExtentY        =   5636
            _Version        =   393216
            BackColorBkg    =   -2147483633
            GridColor       =   14737632
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label LbTotalAnaPed 
            Caption         =   "L"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   8040
            TabIndex        =   308
            Top             =   3960
            Width           =   495
         End
         Begin VB.Label Label9 
            Caption         =   "Total An�lises:"
            Height          =   255
            Left            =   6840
            TabIndex        =   307
            Top             =   3960
            Width           =   1095
         End
      End
      Begin VB.TextBox EcEpisodioAux 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E3FFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -70560
         TabIndex        =   300
         Top             =   840
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox EcEpisodio 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -70560
         TabIndex        =   301
         Top             =   840
         Width           =   1335
      End
      Begin VB.Frame Frame8 
         Height          =   615
         Index           =   1
         Left            =   -63600
         TabIndex        =   294
         Top             =   960
         Width           =   1455
         Begin VB.CommandButton BtAgenda 
            Height          =   435
            Left            =   930
            Picture         =   "FormGestaoRequisicao.frx":36EA
            Style           =   1  'Graphical
            TabIndex        =   295
            ToolTipText     =   "Abrir Agenda"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label58 
            Caption         =   "Agenda"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   296
            ToolTipText     =   "Abrir Agenda"
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.CheckBox CkAvisarSMS 
         Caption         =   "Enviar SMS quando resultados completos"
         Height          =   255
         Left            =   -66600
         TabIndex        =   290
         Top             =   100
         Width           =   4575
      End
      Begin VB.Frame FrameDomicilio 
         Caption         =   "Domicilio"
         Height          =   2820
         Left            =   -67920
         TabIndex        =   246
         Top             =   1620
         Visible         =   0   'False
         Width           =   5775
         Begin VB.TextBox EcDescrPostal 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Height          =   315
            Left            =   2160
            TabIndex        =   264
            Top             =   1200
            Width           =   1695
         End
         Begin VB.CommandButton BtPesqCodPostal 
            Height          =   315
            Left            =   3840
            Picture         =   "FormGestaoRequisicao.frx":37FC
            Style           =   1  'Graphical
            TabIndex        =   263
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
            Top             =   1200
            Width           =   375
         End
         Begin VB.TextBox EcDescrPais 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   260
            TabStop         =   0   'False
            Top             =   1620
            Width           =   2175
         End
         Begin VB.TextBox EcRuaPostal 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   259
            Top             =   1200
            Width           =   490
         End
         Begin VB.TextBox EcCodPostal 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   258
            Top             =   1200
            Width           =   615
         End
         Begin VB.CommandButton BtPesqPais 
            Height          =   315
            Left            =   3840
            Picture         =   "FormGestaoRequisicao.frx":3B86
            Style           =   1  'Graphical
            TabIndex        =   257
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Pa�ses"
            Top             =   1620
            Width           =   375
         End
         Begin VB.TextBox EcCodPais 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1080
            TabIndex        =   256
            Top             =   1620
            Width           =   615
         End
         Begin VB.ComboBox CbConvencao 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1080
            Style           =   2  'Dropdown List
            TabIndex        =   254
            Top             =   840
            Width           =   1575
         End
         Begin VB.TextBox EcKm 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3240
            TabIndex        =   248
            Top             =   480
            Width           =   495
         End
         Begin VB.ComboBox CbCodUrbano 
            Height          =   315
            Left            =   1080
            Style           =   2  'Dropdown List
            TabIndex        =   247
            Top             =   480
            Width           =   1575
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Cod.Pos&tal"
            Height          =   195
            Index           =   19
            Left            =   120
            TabIndex        =   262
            Top             =   1260
            Width           =   765
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Pa�s"
            Height          =   195
            Index           =   36
            Left            =   120
            TabIndex        =   261
            Top             =   1620
            Width           =   330
         End
         Begin VB.Label Label1 
            Caption         =   "Conven��o"
            Height          =   255
            Index           =   26
            Left            =   120
            TabIndex        =   255
            Top             =   840
            Width           =   855
         End
         Begin VB.Label Label4 
            Caption         =   "Km"
            Height          =   255
            Left            =   2880
            TabIndex        =   250
            Top             =   480
            Width           =   495
         End
         Begin VB.Label Label3 
            Caption         =   "C�d. Urbano"
            Height          =   255
            Left            =   120
            TabIndex        =   249
            Top             =   480
            Width           =   1215
         End
      End
      Begin VB.TextBox EcDescrUserColheita 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   241
         TabStop         =   0   'False
         Top             =   900
         Width           =   3735
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   219
         TabStop         =   0   'False
         Top             =   2220
         Width           =   3735
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   96
         TabStop         =   0   'False
         Top             =   2700
         Width           =   3735
      End
      Begin VB.TextBox EcNomeMedico 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   130
         TabStop         =   0   'False
         Top             =   4260
         Width           =   3735
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   94
         TabStop         =   0   'False
         Top             =   3720
         Width           =   3735
      End
      Begin VB.Frame Frame7 
         Height          =   600
         Index           =   0
         Left            =   -65040
         TabIndex        =   272
         ToolTipText     =   "Anexos Associados ao Utente"
         Top             =   940
         Width           =   1455
         Begin VB.CommandButton BtAnexos 
            Height          =   495
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":3F10
            Style           =   1  'Graphical
            TabIndex        =   274
            ToolTipText     =   "Anexos Associados ao Utente"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Anexos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   273
            ToolTipText     =   "Anexos Associados ao Utente"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame8 
         Height          =   600
         Index           =   0
         Left            =   -63600
         TabIndex        =   251
         ToolTipText     =   "Domicilio"
         Top             =   350
         Width           =   1455
         Begin VB.CommandButton BtDomicilio 
            Enabled         =   0   'False
            Height          =   480
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":467A
            Style           =   1  'Graphical
            TabIndex        =   252
            ToolTipText     =   "Domicilio"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Domicilio"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   80
            TabIndex        =   253
            ToolTipText     =   "Domicilio"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.CheckBox CkReqHipocoagulados 
         Caption         =   "Requisi��o de hipocoagulados"
         Height          =   255
         Left            =   -67860
         TabIndex        =   244
         Top             =   4140
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaColheita 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGestaoRequisicao.frx":4DE4
         Style           =   1  'Graphical
         TabIndex        =   242
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   900
         Width           =   375
      End
      Begin VB.TextBox EcCodUserColheita 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   240
         Top             =   900
         Width           =   735
      End
      Begin VB.Frame Frame5 
         Height          =   600
         Left            =   -66480
         TabIndex        =   231
         ToolTipText     =   "Notas Associadas � Requisi��o"
         Top             =   940
         Width           =   1455
         Begin VB.CommandButton BtNotas 
            Enabled         =   0   'False
            Height          =   480
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":516E
            Style           =   1  'Graphical
            TabIndex        =   233
            ToolTipText     =   "Notas"
            Top             =   110
            Width           =   495
         End
         Begin VB.CommandButton BtNotasVRM 
            Enabled         =   0   'False
            Height          =   480
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":58D8
            Style           =   1  'Graphical
            TabIndex        =   232
            ToolTipText     =   "Notas"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Notas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   120
            TabIndex        =   234
            ToolTipText     =   "Notas"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame10 
         Height          =   600
         Left            =   -67920
         TabIndex        =   228
         ToolTipText     =   "Folha de Resumo"
         Top             =   940
         Width           =   1455
         Begin VB.CommandButton BtResumo 
            Height          =   480
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":6042
            Style           =   1  'Graphical
            TabIndex        =   229
            ToolTipText     =   "Folha de Resumo"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label6 
            Caption         =   "Resumo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   230
            ToolTipText     =   "Folha de Resumo"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.TextBox EcCodValencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   158
         Top             =   3180
         Width           =   735
      End
      Begin VB.TextBox EcDescrValencia 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   222
         TabStop         =   0   'False
         Top             =   3180
         Width           =   3735
      End
      Begin VB.CommandButton BtPesquisaValencia 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGestaoRequisicao.frx":67AC
         Style           =   1  'Graphical
         TabIndex        =   221
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   3180
         Width           =   375
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   157
         Top             =   2220
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGestaoRequisicao.frx":6B36
         Style           =   1  'Graphical
         TabIndex        =   218
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   2220
         Width           =   375
      End
      Begin VB.ComboBox CbDestino 
         Height          =   315
         Left            =   -73680
         Style           =   2  'Dropdown List
         TabIndex        =   154
         Top             =   1380
         Width           =   1335
      End
      Begin VB.ComboBox CbPrioColheita 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70560
         Style           =   2  'Dropdown List
         TabIndex        =   155
         Top             =   1380
         Width           =   1335
      End
      Begin VB.TextBox EcDtEntrega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73680
         Locked          =   -1  'True
         TabIndex        =   156
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox EcUserColheita 
         Height          =   285
         Left            =   -73680
         TabIndex        =   188
         Top             =   900
         Width           =   3615
      End
      Begin VB.TextBox EcHrColheita 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -71280
         TabIndex        =   187
         Top             =   540
         Width           =   1095
      End
      Begin VB.TextBox EcDtColheita 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73680
         TabIndex        =   186
         Top             =   540
         Width           =   975
      End
      Begin VB.Frame FrameObsEspecif 
         Caption         =   "Observa��o do Produto/Especifica��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2295
         Left            =   -68880
         TabIndex        =   175
         Top             =   660
         Width           =   4575
         Begin VB.CommandButton BtOkObsEspecif 
            Caption         =   "OK"
            Height          =   375
            Left            =   1920
            TabIndex        =   177
            Top             =   1800
            Width           =   735
         End
         Begin VB.TextBox EcObsEspecif 
            Height          =   1335
            Left            =   240
            TabIndex        =   176
            Top             =   360
            Width           =   4095
         End
      End
      Begin VB.Frame FrTubos 
         Height          =   3960
         Left            =   -74880
         TabIndex        =   169
         Top             =   435
         Width           =   12735
         Begin MSFlexGridLib.MSFlexGrid FGTubos 
            Height          =   3675
            Left            =   30
            TabIndex        =   170
            Top             =   120
            Width           =   12180
            _ExtentX        =   21484
            _ExtentY        =   6482
            _Version        =   393216
            BackColorBkg    =   -2147483633
            GridColor       =   14737632
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton BtPesquisaMedico 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGestaoRequisicao.frx":6EC0
         Style           =   1  'Graphical
         TabIndex        =   160
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de M�dicos "
         Top             =   4260
         Width           =   375
      End
      Begin VB.CommandButton BtRegistaMedico 
         Height          =   315
         Left            =   -73965
         Picture         =   "FormGestaoRequisicao.frx":724A
         Style           =   1  'Graphical
         TabIndex        =   147
         ToolTipText     =   "M�dicos da Requisi��o "
         Top             =   4260
         Visible         =   0   'False
         Width           =   300
      End
      Begin VB.TextBox EcCodMedico 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   159
         Top             =   4260
         Width           =   735
      End
      Begin VB.TextBox EcObsProveniencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -66780
         TabIndex        =   119
         Top             =   2700
         Width           =   4515
      End
      Begin VB.ListBox ListaDiagnSec 
         Height          =   2790
         Left            =   -68520
         TabIndex        =   108
         Top             =   3840
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.TextBox EcObsReq 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         MultiLine       =   -1  'True
         TabIndex        =   161
         Top             =   4620
         Width           =   11415
      End
      Begin VB.ComboBox CbUrgencia 
         BackColor       =   &H00E3FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70560
         Style           =   2  'Dropdown List
         TabIndex        =   149
         Top             =   1800
         Width           =   1335
      End
      Begin VB.ComboBox CbSituacao 
         BackColor       =   &H00E3FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73680
         Style           =   2  'Dropdown List
         TabIndex        =   153
         Top             =   900
         Width           =   1335
      End
      Begin VB.TextBox EcDataPrevista 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E3FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   148
         Top             =   420
         Width           =   1335
      End
      Begin VB.Frame FrProdutos 
         Height          =   3840
         Left            =   -74880
         TabIndex        =   106
         Top             =   315
         Width           =   12735
         Begin MSFlexGridLib.MSFlexGrid FgProd 
            Height          =   2235
            Left            =   120
            TabIndex        =   107
            Top             =   120
            Width           =   10500
            _ExtentX        =   18521
            _ExtentY        =   3942
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrBtProd 
         Height          =   495
         Left            =   -74880
         TabIndex        =   102
         Top             =   4035
         Width           =   10575
         Begin VB.CommandButton BtObsEspecif 
            Enabled         =   0   'False
            Height          =   375
            Left            =   840
            Picture         =   "FormGestaoRequisicao.frx":77D4
            Style           =   1  'Graphical
            TabIndex        =   174
            ToolTipText     =   "Obs. Especifica��o"
            Top             =   120
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaProduto 
            Height          =   375
            Left            =   0
            Picture         =   "FormGestaoRequisicao.frx":7D5E
            Style           =   1  'Graphical
            TabIndex        =   104
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaEspecif 
            Height          =   375
            Left            =   360
            Picture         =   "FormGestaoRequisicao.frx":82E8
            Style           =   1  'Graphical
            TabIndex        =   103
            ToolTipText     =   "Pesquisa R�pida de Especifica��es de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.Label LaProdutos 
            Height          =   345
            Left            =   1320
            TabIndex        =   105
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.TextBox EcDataChegada 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E3FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -70560
         TabIndex        =   152
         Top             =   420
         Width           =   1335
      End
      Begin VB.Frame FrAnalises 
         Height          =   4560
         Left            =   -74880
         TabIndex        =   98
         Top             =   435
         Width           =   12735
         Begin VB.Frame FrameMarcaArs 
            BorderStyle     =   0  'None
            Height          =   495
            Left            =   120
            TabIndex        =   267
            Top             =   3960
            Width           =   6375
            Begin VB.TextBox EcCredAct 
               Appearance      =   0  'Flat
               BackColor       =   &H00E0E0E0&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   205
               Left            =   4320
               Locked          =   -1  'True
               TabIndex        =   269
               Top             =   120
               Width           =   1815
            End
            Begin VB.TextBox EcAnaEfr 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   120
               TabIndex        =   268
               Top             =   120
               Width           =   3135
            End
            Begin VB.Label Label1 
               Caption         =   "Credencial"
               Height          =   255
               Index           =   32
               Left            =   3480
               TabIndex        =   270
               Top             =   120
               Width           =   975
            End
         End
         Begin VB.Frame FrameObsAna 
            Caption         =   "Observa��o da An�lise"
            Height          =   4575
            Left            =   9960
            TabIndex        =   197
            Top             =   120
            Width           =   3615
            Begin VB.TextBox EcObsAna 
               Appearance      =   0  'Flat
               Height          =   4215
               Left            =   1005
               TabIndex        =   198
               Top             =   240
               Width           =   2895
            End
         End
         Begin VB.CommandButton BtPesquisaAna 
            Height          =   375
            Left            =   12360
            Picture         =   "FormGestaoRequisicao.frx":8872
            Style           =   1  'Graphical
            TabIndex        =   99
            Top             =   90
            Width           =   375
         End
         Begin MSFlexGridLib.MSFlexGrid FGAna 
            Height          =   3930
            Left            =   120
            TabIndex        =   100
            TabStop         =   0   'False
            Top             =   120
            Width           =   9825
            _ExtentX        =   17330
            _ExtentY        =   6932
            _Version        =   393216
            BackColorBkg    =   -2147483633
            SelectionMode   =   1
            AllowUserResizing=   3
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.TreeView TAna 
            Height          =   4305
            Left            =   9975
            TabIndex        =   101
            ToolTipText     =   "Duplo click para saltar para a an�lise."
            Top             =   240
            Width           =   2715
            _ExtentX        =   4789
            _ExtentY        =   7594
            _Version        =   393217
            Indentation     =   9999800
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   0
         End
         Begin VB.Label Label2 
            Caption         =   "Total An�lises:"
            Height          =   255
            Left            =   6720
            TabIndex        =   227
            Top             =   4080
            Width           =   1095
         End
         Begin VB.Label LbTotalAna 
            Caption         =   "L"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   8040
            TabIndex        =   226
            Top             =   4080
            Width           =   375
         End
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGestaoRequisicao.frx":8DFC
         Style           =   1  'Graphical
         TabIndex        =   97
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   2700
         Width           =   375
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E3FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   150
         Top             =   2700
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   -69240
         Picture         =   "FormGestaoRequisicao.frx":9186
         Style           =   1  'Graphical
         TabIndex        =   95
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   3720
         Width           =   375
      End
      Begin VB.TextBox EcCodEFR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E3FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   151
         Top             =   3720
         Width           =   735
      End
      Begin VB.Frame Frame4 
         Height          =   600
         Left            =   -67920
         TabIndex        =   89
         ToolTipText     =   "Informa��o Cl�nica do Utente"
         Top             =   350
         Width           =   1455
         Begin VB.CommandButton BtInfClinica 
            Enabled         =   0   'False
            Height          =   480
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":9510
            Style           =   1  'Graphical
            TabIndex        =   90
            ToolTipText     =   "Informa��o Cl�nica do Utente"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaInfCli 
            Caption         =   "Inf. C&l�nica"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   91
            ToolTipText     =   "Informa��o Cl�nica do Utente"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame FrEtiq 
         Height          =   605
         Left            =   -66480
         TabIndex        =   86
         ToolTipText     =   "Imprimir Etiquetas de C�digo de Barras"
         Top             =   350
         Width           =   1455
         Begin VB.CommandButton BtEtiq 
            Height          =   480
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":9C7A
            Style           =   1  'Graphical
            TabIndex        =   87
            ToolTipText     =   "Imprimir Etiquetas de C�digo de Barras"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaEtiq 
            Caption         =   "Eti&quetas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   88
            ToolTipText     =   "Imprimir Etiquetas de C�digo de Barras"
            Top             =   240
            Width           =   735
         End
      End
      Begin VB.Frame Frame1 
         Height          =   600
         Left            =   -65040
         TabIndex        =   83
         ToolTipText     =   "Copiar Dados da Requisi��o Anterior"
         Top             =   350
         Width           =   1455
         Begin VB.CommandButton BtDadosAnteriores 
            Height          =   480
            Left            =   960
            Picture         =   "FormGestaoRequisicao.frx":A3E4
            Style           =   1  'Graphical
            TabIndex        =   84
            ToolTipText     =   "Copiar Dados da Requisi��o Anterior"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaCopiarAnterior 
            Caption         =   "Copiar a&nterior."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   85
            ToolTipText     =   "Copiar Dados da Requisi��o Anterior"
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.ComboBox CbTipoIsencao 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -66780
         Style           =   2  'Dropdown List
         TabIndex        =   92
         Top             =   3225
         Width           =   1980
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -66780
         TabIndex        =   120
         Top             =   3720
         Width           =   1980
      End
      Begin VB.CheckBox CkIsento 
         Caption         =   "&Isento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   -67860
         TabIndex        =   93
         Top             =   3225
         Width           =   855
      End
      Begin VB.TextBox EcDescrMedico 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -73680
         TabIndex        =   203
         Top             =   4305
         Width           =   4455
      End
      Begin VB.Label LbAdmin 
         AutoSize        =   -1  'True
         Caption         =   "&Epis�dio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   -71520
         TabIndex        =   302
         Top             =   840
         Width           =   600
      End
      Begin VB.Label LabelColheita 
         Caption         =   "Colhido Por"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   243
         Top             =   900
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Val�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   -74760
         TabIndex        =   223
         Top             =   3180
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Sala / Posto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   -74760
         TabIndex        =   220
         Top             =   2220
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Destino"
         Height          =   255
         Index           =   13
         Left            =   -74760
         TabIndex        =   217
         Top             =   1380
         Width           =   855
      End
      Begin VB.Label LbPrioColheita 
         Caption         =   "Prio.Colheita"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -71520
         TabIndex        =   216
         Top             =   1380
         Width           =   915
      End
      Begin VB.Label Label1 
         Caption         =   "Dt Entrega"
         Height          =   255
         Index           =   7
         Left            =   -74760
         TabIndex        =   204
         Top             =   1860
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "�rea"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   100
         Left            =   -69000
         TabIndex        =   200
         Top             =   540
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label Label22 
         Caption         =   "Colhido por"
         Height          =   255
         Left            =   -74760
         TabIndex        =   191
         Top             =   900
         Width           =   975
      End
      Begin VB.Label Label20 
         Caption         =   "Hora Colheita"
         Height          =   255
         Left            =   -72360
         TabIndex        =   190
         Top             =   540
         Width           =   975
      End
      Begin VB.Label Label7 
         Caption         =   "Data Colheita"
         Height          =   255
         Left            =   -74760
         TabIndex        =   189
         Top             =   540
         Width           =   975
      End
      Begin VB.Label LaMedico 
         Caption         =   "&M�dico"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   131
         Top             =   4260
         Width           =   1095
      End
      Begin VB.Label LaDataFact 
         Caption         =   "Dt &fact"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66045
         TabIndex        =   129
         Top             =   2700
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label LaSemanas 
         Caption         =   "semanas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66720
         TabIndex        =   128
         Top             =   2700
         Visible         =   0   'False
         Width           =   750
      End
      Begin VB.Label LaArquivo 
         Caption         =   "&Arquivo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   127
         Top             =   4065
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.Label Labe41 
         Caption         =   "Obs. Proven."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   -67860
         TabIndex        =   121
         Top             =   2700
         Width           =   1500
      End
      Begin VB.Label Label1 
         Caption         =   "Obs. &Final"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   -74760
         TabIndex        =   115
         Top             =   4620
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Prio&ridade"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   -71520
         TabIndex        =   114
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo Epis�dio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   -74760
         TabIndex        =   113
         Top             =   900
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Dt previs&ta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   -74760
         TabIndex        =   112
         Top             =   420
         Width           =   975
      End
      Begin VB.Label Label40 
         Caption         =   "Dt &chegada"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   -71520
         TabIndex        =   111
         Top             =   420
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "&Proveni�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   -74760
         TabIndex        =   110
         Top             =   2700
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "E.&F.R."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   -74760
         TabIndex        =   109
         Top             =   3720
         Width           =   495
      End
      Begin VB.Label Labe41 
         Caption         =   "N�. Benefic."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   -67860
         TabIndex        =   122
         Top             =   3720
         Width           =   1335
      End
   End
   Begin VB.Frame FrameSONHO 
      Height          =   4335
      Left            =   4680
      TabIndex        =   134
      Top             =   16000
      Visible         =   0   'False
      Width           =   5535
      Begin VB.TextBox EcEpisodioSONHO 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   360
         TabIndex        =   141
         Top             =   2880
         Width           =   1500
      End
      Begin VB.OptionButton OpConsSonho 
         Caption         =   "Consulta"
         Height          =   315
         Left            =   480
         TabIndex        =   140
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton OpIntSONHO 
         Caption         =   "Internamento"
         Height          =   255
         Left            =   480
         TabIndex        =   139
         Top             =   1200
         Width           =   1335
      End
      Begin VB.OptionButton OpUrgSONHO 
         Caption         =   "Urg�ncia"
         Height          =   255
         Left            =   480
         TabIndex        =   138
         Top             =   1920
         Width           =   1335
      End
      Begin VB.OptionButton OpExternoSONHO 
         Caption         =   "Hospital Dia"
         Height          =   255
         Left            =   480
         TabIndex        =   137
         Top             =   1560
         Width           =   1335
      End
      Begin VB.CommandButton BtGetSONHO_OK 
         Caption         =   "Importar"
         Height          =   375
         Left            =   1440
         TabIndex        =   136
         Top             =   3600
         Width           =   1455
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   3000
         TabIndex        =   135
         Top             =   3600
         Width           =   1455
      End
      Begin MSComCtl2.MonthView MonthViewAnalisesSONHO 
         Height          =   2310
         Left            =   2520
         TabIndex        =   142
         Top             =   870
         Width           =   2430
         _ExtentX        =   4286
         _ExtentY        =   4075
         _Version        =   393216
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         Appearance      =   0
         ShowToday       =   0   'False
         StartOfWeek     =   171900930
         CurrentDate     =   37596
      End
      Begin VB.Label Label42 
         Caption         =   "An�lises marcadas para"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2520
         TabIndex        =   145
         Top             =   480
         Width           =   2655
      End
      Begin VB.Label Label52 
         Caption         =   "Epis�dio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   144
         Top             =   2520
         Width           =   975
      End
      Begin VB.Label Label53 
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   143
         Top             =   480
         Width           =   1335
      End
   End
   Begin VB.CommandButton BtConfirm 
      Height          =   315
      Left            =   6840
      Picture         =   "FormGestaoRequisicao.frx":AACE
      Style           =   1  'Graphical
      TabIndex        =   210
      ToolTipText     =   "Confirmar Requisi��o"
      Top             =   120
      Width           =   495
   End
   Begin VB.Label LbNumColheita 
      AutoSize        =   -1  'True
      Caption         =   "N�Colheita"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   3000
      TabIndex        =   304
      Top             =   600
      Width           =   750
   End
   Begin VB.Label Label1 
      Caption         =   "Prescri��o"
      Height          =   255
      Index           =   16
      Left            =   240
      TabIndex        =   299
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgDiagSec"
      Enabled         =   0   'False
      Height          =   255
      Index           =   29
      Left            =   3480
      TabIndex        =   288
      Top             =   13320
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodPostalAux"
      Enabled         =   0   'False
      Height          =   255
      Index           =   26
      Left            =   8040
      TabIndex        =   266
      Top             =   12240
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcSexoUte"
      Enabled         =   0   'False
      Height          =   255
      Index           =   23
      Left            =   8640
      TabIndex        =   238
      Top             =   11520
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label41 
      Caption         =   "EcReqGrupoEspecial"
      Height          =   255
      Index           =   25
      Left            =   6000
      TabIndex        =   202
      Top             =   12480
      Width           =   1695
   End
   Begin VB.Label LbSala 
      Caption         =   "Sala"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   196
      Top             =   16000
      Width           =   675
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrColheita"
      Enabled         =   0   'False
      Height          =   255
      Index           =   24
      Left            =   8640
      TabIndex        =   195
      Top             =   11880
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label41 
      Caption         =   "EcinfComplementar"
      Height          =   255
      Index           =   21
      Left            =   6000
      TabIndex        =   193
      Top             =   12120
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxTubo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   22
      Left            =   600
      TabIndex        =   172
      Top             =   12240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcRequAX"
      Height          =   255
      Index           =   18
      Left            =   3240
      TabIndex        =   133
      Top             =   12120
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapArquivo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   7
      Left            =   6120
      TabIndex        =   126
      Top             =   11520
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Labe41 
      Caption         =   "EcPesqRapMedico2"
      Enabled         =   0   'False
      Height          =   255
      Index           =   2
      Left            =   5880
      TabIndex        =   124
      Top             =   16000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcGrupoAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   13
      Left            =   8640
      TabIndex        =   118
      Top             =   16000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNCopias"
      Enabled         =   0   'False
      Height          =   255
      Index           =   0
      Left            =   600
      TabIndex        =   80
      Top             =   16000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNEsp"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   600
      TabIndex        =   78
      Top             =   16000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqTipo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   15
      Left            =   720
      TabIndex        =   76
      Top             =   11640
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "Re&q. assoc."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7560
      TabIndex        =   3
      ToolTipText     =   "Requisi��o Associada"
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   2
      Left            =   600
      TabIndex        =   74
      Top             =   16000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNTubos"
      Enabled         =   0   'False
      Height          =   255
      Index           =   3
      Left            =   600
      TabIndex        =   72
      Top             =   17000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNAdm"
      Enabled         =   0   'False
      Height          =   255
      Index           =   4
      Left            =   600
      TabIndex        =   70
      Top             =   17000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcTEFR"
      Enabled         =   0   'False
      Height          =   255
      Index           =   20
      Left            =   5880
      TabIndex        =   68
      Top             =   11760
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   17
      Left            =   3240
      TabIndex        =   66
      Top             =   11760
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxProd"
      Enabled         =   0   'False
      Height          =   255
      Index           =   5
      Left            =   600
      TabIndex        =   64
      Top             =   11880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodIsencao"
      Height          =   255
      Index           =   19
      Left            =   6240
      TabIndex        =   62
      Top             =   17000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "CodAnaTemp"
      Height          =   255
      Index           =   0
      Left            =   4320
      TabIndex        =   60
      Top             =   16000
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProduto"
      Enabled         =   0   'False
      Height          =   255
      Index           =   14
      Left            =   8160
      TabIndex        =   58
      Top             =   16000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcSeqUtente"
      Enabled         =   0   'False
      Height          =   255
      Index           =   8
      Left            =   2280
      TabIndex        =   55
      Top             =   16000
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapMedico"
      Enabled         =   0   'False
      Height          =   255
      Index           =   12
      Left            =   5880
      TabIndex        =   53
      Top             =   16000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProven"
      Enabled         =   0   'False
      Height          =   255
      Index           =   6
      Left            =   6000
      TabIndex        =   51
      Top             =   16000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcEstadoReq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   2280
      TabIndex        =   49
      Top             =   16000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcTaxaModeradora"
      Enabled         =   0   'False
      Height          =   255
      Index           =   11
      Left            =   3840
      TabIndex        =   47
      Top             =   16000
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcPagEnt"
      Enabled         =   0   'False
      Height          =   255
      Index           =   16
      Left            =   4440
      TabIndex        =   46
      Top             =   17000
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcPagUte"
      Enabled         =   0   'False
      Height          =   255
      Index           =   9
      Left            =   2400
      TabIndex        =   45
      Top             =   16000
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcUrgencia"
      Enabled         =   0   'False
      Height          =   255
      Index           =   10
      Left            =   2280
      TabIndex        =   41
      Top             =   17000
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Requisi��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   900
   End
End
Attribute VB_Name = "FormGestaoRequisicao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/03/2003
' T�cnico : Paulo Costa

' Vari�veis Globais para este Form.

Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim CamposBCK() As String
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Public CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim Formato1 As String
Dim Formato2 As String

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

Public rs As ADODB.recordset
Public MarcaLocal As Variant

'Controlo dos produtos
Public RegistosP As New Collection
Dim LastColP As Integer
Dim LastRowP As Integer
Dim ExecutaCodigoP As Boolean


'Flag que indica se a entidade em causa � ARS
Dim FlgARS As Boolean

'Flag que indica se a entidade em causa � ADSE
Dim FlgADSE As Boolean

'Controlo de analises
Public RegistosA As New Collection
Dim LastColA As Integer
Dim LastRowA As Long
Public ExecutaCodigoA As Boolean

'acumula o numero do P1 para quando este for superior a 7 incrementar 1 ao P1
Dim ContP1 As Integer


'Comando utilizados obter a ordem correcta das an�lises
Dim CmdOrdAna As New ADODB.Command

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdAnaReq As New ADODB.Command
Dim CmdAnaReq_H As New ADODB.Command

'Estrutura usada para calcular o valor do recibo da requisi��o
Private Recibo() As Estrutura_Recibo


'Flag usada para controlar a coloca��o da data actual no lostfocus da EcDataPrevista
'quando se faz o limpacampos
Dim Flg_LimpaCampos As Boolean

'flag que indica se se deve preencher a data de chegada de todos os produtos na altura do Insert ou UpDate
Dim Flg_PreencherDtChega As Boolean

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

'valor do indice nas combos de Pagamento (TM,Ent,Ute) quando se executa a fun��o procurar
Dim CbTMIndex As Integer
Dim CbEntIndex As Integer
Dim CbUteIndex As Integer

Dim gRec As Long

'Flag que evita os beep dos enters nas textbox
Dim Enter As Boolean

'Constante com cor azul
Const azul = &H8000000D

Dim Flg_PergProd As Boolean
Dim Flg_DtChProd As Boolean
Dim Resp_PergProd As Boolean
Dim Resp_PergDtChProd As Boolean

Dim Flg_PergTubo As Boolean
Dim Flg_DtChTubo As Boolean
Dim Resp_PergTubo As Boolean
Dim Resp_PergDtChTubo As Boolean


'contem a data de chegada da verificacao dos produtos para utilizacao na marcacao de analises
Dim gColocaDataChegada As String

'Flag que indica a entrada no form de pesquisa de utentes
Dim Flg_PesqUtente As Boolean

'Estruturas utilizadas para gerar as etiquetas
Private Type Tipo_Tubo
    Especial As Boolean
    Cheio As Boolean
    abrAna As String
    GrAna As String
    CodProd As String
    codTuboBar As String
    CodTubo As String
    QtMax As Double
    QtOcup As Double
    Designacao_tubo As String
    inf_complementar As String
    num_copias As Integer
    Descr_etiq_ana As String
    dt_chega As String
End Type
Dim Tubos() As Tipo_Tubo

'Variaveis utilizadas na marca��o de an�lises GhostMember na pr�pria requisi�ao
Dim GhostPerfil As String
Dim GhostComplexa As String

Dim Flg_DadosAnteriores As Boolean

' ---------------------------------------------------

' 02

Dim ESTADO_RECIBO As String
Const REC_INDEFINIDO = "0"
Const REC_NAO_EMITIDO = "1"
Const REC_EMITIDO_NAO_IMPRESSO = "2"
Const REC_EMITIDO_IMPRESSO = "3"
Const REC_ANULADO = "4"

' Recibo activo para a requisi��o actual.
Dim RECIBO_ACTIVO As String
Dim SERIE_RECIBO_ACTIVO As String

' Indica se a estrutura do recibo j� foi gerada.
Dim flgGridRecibo As Boolean

' True : A actualizar.
Dim ActualizaEpisodio As Boolean

Dim Flg_Gravou As Boolean

Dim episodioGH As String
Dim situacaoGH As String

'Estrutura que guarda as an�lises eliminadas
Private Type Tipo_AnalisesEliminadas
    codAna As String
    NReq As String
End Type
Dim Eliminadas() As Tipo_AnalisesEliminadas
Dim TotalEliminadas As Integer

'Estrutura que guarda as an�lises acrescentadas
Private Type Tipo_AnalisesAcrescentadas
    codAna As String
    NReq As String
End Type
Dim Acrescentadas() As Tipo_AnalisesAcrescentadas
Dim TotalAcrescentadas As Integer


'Lista que vai conter os dados das credenciais da requisi��o
Private p1() As credencial
'Lista que vai conter os dados das credenciais da requisi��o
Private Type credencial
    NrP1 As String
    totalAna As Integer
End Type

Dim TuboColunaActual As Long
Dim TuboLinhaActual As Long

Dim Flg_IncrementaP1 As Boolean
Dim flg_ReqBloqueada As Boolean
Public reqMarca As String

Const lColFgAnaCodAna = 0
Const lColFgAnaDescrAna = 1
Const lColFgAnaCred = 2
Const lColFgAnaOrdem = 3
Const lColFgAnaUnidSaude = 4
Const lColFgAnaMedSaude = 5
Const lColFgAnaPreco = 6
'BRUNODSANTOS GLINTT-HS-15750
Const lColFgAnaConsenteEnvioPDS = 7
'
'NELSONPSILVA CHVNG-7461 29.10.2018
Const lColFgPedAdicionaCodAna = 0
Const lColFgPedAdicionaDescrAna = 1
Const lColFgPedAdicionaMedico = 2
Const lColFgPedAdicionaTecnico = 3
Const lColFgPedAdicionaPrescricao = 4
Const lColFgPedAdicionaDtPedido = 5
Const lColFgPedAdicionaHrPedido = 6
Const lColFgPedAdicionaProven = 7
'
Dim dataAct As String
Const vermelhoClaro = &HBDC8F4

Private Type Tipo_tuboEliminado
    seq_req_tubo As Long
    n_req As String
End Type
Dim tubosEliminados() As Tipo_tuboEliminado
Dim TotalTubosEliminados As Integer

'Controlo de Recibos
Public RegistosR As New Collection
'Controlo de Recibos Manuais
Public RegistosRM As New Collection

Dim lObrigaTerap As Boolean
Dim lObrigaInfoCli As Boolean

Private Type Questoes
    Resposta As String
    cod_questao As String
    descr_questao As String
End Type
Dim EstrutQuestao() As Questoes
Dim totalQuestao As Integer
Const EstadoCheckboxEnvioPDS_Checked = "�"
Const EstadoCheckboxEnvioPDS_Unchecked = "q"

'NELSONPSILVA CHVNG-7461 29.10.2018
Dim TotalAnaReutil As Integer
Dim anaReutil() As PedidoReutil
'NELSONPSILVA Glintt-HS-18011 04.04.2018 - Acesso contextualizado quando chamado pelo form anterior (Log RGPD)
Public ExternalAccess As Boolean
Dim etiqFlag As Boolean
 
'tania.oliveira CHUC-12912 12.11.2019
Dim totalTubos As Long
' ---------------------------------------------------

Sub Elimina_Mareq(codAgrup As String)
    Dim i As Integer

    On Error GoTo Trata_Erro

    TAna.Nodes.Remove Trim(UCase(codAgrup))
    
    i = 1
    While i <= UBound(MaReq)
        If Trim(UCase(codAgrup)) = Trim(UCase(MaReq(i).cod_agrup)) Then
            Actualiza_Estrutura_MAReq i
        Else
            i = i + 1
        End If
    Wend
        
    If UBound(MaReq) = 0 Then ReDim MaReq(1)

    Exit Sub

Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "Elimina_Mareq: " & Err.Description
    End If
    
    Resume Next
End Sub

Sub Elimina_RegistoA(codAgrup As String)

    Dim i As Long

    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            RegistosA.Remove i
            FGAna.RemoveItem i
        End If
    Next i

End Sub

Function Insere_Nova_Analise(indice As Long, codAgrup As String, NrReqARS As String) As Boolean

    Dim descricao As String
    Dim Marcar() As String
    Dim reqComAnalise As String
    Dim DataReq As String
    Dim aux As String
    Insere_Nova_Analise = False
    
    'BRUNODSANTOS Glintt-HS-17949 10.01.2018
    If gVerificaContaFechada = mediSim And CbSituacao.ListIndex <> mediComboValorNull Then
        If BL_VerificaContaFechada(CbTipoUtente.text, EcUtente.text, GH_RetornaDescrEpisodio(CbSituacao.ItemData(CbSituacao.ListIndex)), EcEpisodio.text, "", "", CDate("01-01-1000"), CDate("01-01-1000"), "S") = True Then
             BG_Mensagem mediMsgBox, "N�o � poss�vel adicionar an�lise, pois o epis�dio encontra-se associado a uma conta fechada!", vbInformation, ""
            Exit Function
        End If
    End If
    '
    
    descricao = SELECT_Descr_Ana(codAgrup)
    If descricao = "" Then
        aux = ""
        aux = BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", codAgrup, "V")
        If aux = "" Then
            BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
        Else
            BG_Mensagem mediMsgStatus, "An�lise cancelada!", vbOKOnly + vbInformation, "An�lises"
        End If
        EcAuxAna.text = ""
    Else
        If VerificaRegraSexoMarcar(codAgrup, EcSeqUtente) = False Then
            BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
            EcAuxAna.text = ""
            Insere_Nova_Analise = False
            Exit Function
        End If
        If gPrecosAmbienteHospitalar = mediSim And CbSituacao.ListIndex = mediComboValorNull Then
            BG_Mensagem mediMsgBox, "Deve escolher um tipo de epis�dio para poder marcar an�lises.", vbExclamation, App.ProductName
            Insere_Nova_Analise = False
            Exit Function
        End If
        If Not VerificaCompatibilidadeAnalises(UCase(Trim(codAgrup))) Then
            BG_Mensagem mediMsgBox, "Esta an�lise n�o pode ser marcada em conjunto com an�lises marcadas anteriormente." & vbCrLf & _
                "Deve criar uma nova requisi��o", vbExclamation, App.ProductName
            Insere_Nova_Analise = False
            Exit Function
        End If
       'NELSONPSILVA CHVNG-7461 29.10.2018
       If gAtiva_Reutilizacao_Colheitas = mediSim And EcNumReq.text <> "" Then
            If VerificaColheitaTubos(EcNumReq, EcAuxAna) = False Then
                 BG_Mensagem mediMsgBox, "A requisi��o j� tem tubos com data de colheita! N�o pode inserir an�lises que gere novos tubos!", vbOKOnly + vbInformation, "An�lises"
                 Insere_Nova_Analise = False
                 Exit Function
            End If
       End If
       '
        gColocaDataChegada = ""
        If Verifica_Ana_Prod(codAgrup, Marcar) Then
            If Trim(RegistosA(indice).codAna) <> "" Then
                Elimina_Mareq RegistosA(indice).codAna
                RegistosA(indice).codAna = ""
            End If
        
            ' --------------------------------------------------------------------------------
            
            If (gProcAnPendentesAoRegistar = mediSim) Then
            
                ' Indica a requisi��o mais recente com esta an�lise pendente, para este utente.
                            
                reqComAnalise = ""
                reqComAnalise = REQUISICAO_Get_Com_Analise_Pendente(UCase(EcAuxAna.text), _
                                                                    EcSeqUtente.text, _
                                                                    DataReq)
        
                If (Len(reqComAnalise)) Then
                    MsgBox "A an�lise " & UCase(EcAuxAna.text) & " est� pendente na requisi��o " & reqComAnalise & "        " & vbCrLf & _
                           "datada de " & DataReq & ".", vbInformation, " Pesquisa de An�lises Pendentes "
                End If
            
            End If
            ' --------------------------------------------------------------------------------
            
            Select Case Mid(Trim(UCase(codAgrup)), 1, 1)
                Case "P"
                    SELECT_Dados_Perfil Marcar, indice, Trim(UCase(codAgrup)), 0, NrReqARS, Trim(UCase(codAgrup))
                Case "C"
                    SELECT_Dados_Complexa Marcar, indice, Trim(UCase(codAgrup)), 0, Empty, Empty, Empty, NrReqARS, Trim(UCase(codAgrup))
                Case "S"
                    If SELECT_Dados_Simples(Marcar, indice, Trim(UCase(codAgrup)), 0, Empty, Empty, Empty, Empty, Empty, Empty, NrReqARS, Trim(UCase(codAgrup))) = False Then
                        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
                        EcAuxAna.text = ""
                        Exit Function
                    End If
                Case Else
            End Select

    
            Insere_Nova_Analise = True
        End If
        
    End If
    
End Function

Sub InsereAutoProd(CodProd As String, DtChega As String)

    Dim LastLastRP As Integer
    Dim i As Integer
    
    EcAuxProd.text = CodProd
    
    LastLastRP = LastRowP
    LastRowP = FgProd.rows - 1
    
    'Verificar se o produto j� est� na lista
    For i = 1 To LastRowP
        If Trim(FgProd.TextMatrix(i, 0)) = Trim(CodProd) Then
            Exit Sub
        End If
    Next
    
    If SELECT_Descr_Prod = True Then
        ExecutaCodigoP = False
        
        If Trim(DtChega) <> "" Then
            FgProd.TextMatrix(LastRowP, 5) = Trim(DtChega)
            RegistosP(LastRowP).DtPrev = Trim(DtChega)
            FgProd.TextMatrix(LastRowP, 6) = Trim(DtChega)
            RegistosP(LastRowP).DtChega = Trim(DtChega)
            gColocaDataChegada = Trim(DtChega)
            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto Then
                EcEstadoReq = gEstadoReqEsperaResultados
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
            End If
        Else
            FgProd.TextMatrix(LastRowP, 5) = EcDataPrevista
            RegistosP(LastRowP).DtPrev = EcDataPrevista
        End If

        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        
        FgProd.TextMatrix(LastRowP, 0) = UCase(CodProd)
        RegistosP(LastRowP).CodProd = UCase(CodProd)
        
        'Cria linha vazia
        FgProd.AddItem ""
        CriaNovaClasse RegistosP, FgProd.rows - 1, "PROD"
        
        ExecutaCodigoP = True
        
        FgProd.row = FgProd.rows - 1
        FgProd.Col = 0
        LastColP = FgProd.Col
        LastRowP = FgProd.row
    Else
        LastRowP = LastLastRP
    End If
    
    EcAuxProd.text = ""
End Sub


Sub InsereGhostMember(codAnaS As String, Optional Flg_Facturado As Integer)

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim Marcar(1) As String
    Dim indice  As Long
    Dim i As Long
    Dim OrdAna As Long
    Dim CodPerfil As String
    Dim DescrPerfil As String
    Dim OrdPerf As Integer
    Dim CodAnaC As String
    Dim DescrAnaC As String

codAnaS = Trim(codAnaS)

'For�ar a marca��o
Marcar(1) = codAnaS & ";"

indice = -1
For i = 1 To UBound(MaReq)
    If Trim(GhostPerfil) = Trim(MaReq(i).Cod_Perfil) And _
       Trim(GhostComplexa) = Trim(MaReq(i).cod_ana_c) And Trim(codAnaS) = Trim(MaReq(i).cod_ana_s) Then
       Exit Sub
    End If
Next i

For i = 1 To UBound(MaReq)
    If Trim(GhostPerfil) = Trim(MaReq(i).Cod_Perfil) And _
       Trim(GhostComplexa) = Trim(MaReq(i).cod_ana_c) Then
       indice = i
       Exit For
    End If
Next i

If indice = -1 Then
    BG_Mensagem mediMsgBox, "Erro a inserir an�lise!", vbCritical + vbOKOnly, App.ProductName
    Exit Sub
End If

gColocaDataChegada = MaReq(indice).dt_chega

'Dados da Complexa
sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    .Open , gConexao
End With

If RsCompl.RecordCount > 0 Then
    descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
Else
    BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
End If
RsCompl.Close
Set RsCompl = Nothing

If IsNull(Flg_Facturado) Then
    Flg_Facturado = 0
End If

OrdAna = MaReq(indice).Ord_Ana
CodPerfil = MaReq(indice).Cod_Perfil
DescrPerfil = MaReq(indice).Descr_Perfil
OrdPerf = MaReq(indice).Ord_Ana_Perf
CodAnaC = MaReq(indice).cod_ana_c
DescrAnaC = MaReq(indice).Descr_Ana_C

'Dados do Membro da complexa (CodAnaS)
sql = "SELECT ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa) & _
        " AND cod_membro = " & BL_TrataStringParaBD(codAnaS)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    .Open , gConexao
End With

If Not RsCompl.EOF Then
    If GhostPerfil <> "0" Then
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, Flg_Facturado, CodPerfil, DescrPerfil, OrdPerf, CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    Else
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, Flg_Facturado, , , , CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    End If
End If

RsCompl.Close
Set RsCompl = Nothing

End Sub

Function Pesquisa_RegistosA(codAgrup As String) As Long

    Dim i As Long
    Dim lRet As Long
    
    lRet = -1
    
    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            lRet = i
            Exit For
        End If
    Next i
    
    Pesquisa_RegistosA = lRet
    
End Function


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes
    
    Max = -1
    ReDim FOPropertiesTemp(0)
    
    DefTipoCampos
    
    PreencheValoresDefeito
    EcUtente.text = ""
    DoEvents
    
    BG_ParametrizaPermissoes_ADO Me.Name
        
    gF_REQUIS = 1
    Flg_PesqUtente = False
    
    Estado = 1
    BG_StackJanelas_Push Me
        
    SSTGestReq.Tab = 0
    DoEvents
    Flg_DadosAnteriores = False
        
    ' _02
    ESTADO_RECIBO = 0
    RECIBO_ACTIVO = ""
        
    ' Retira o tab de recibos.
    If (StrComp(gSKIN, cANASIBAS) = 0) Or _
       (gSISTEMA_FACTURACAO = "") Or _
       (gSISTEMA_FACTURACAO = "SONHO") Then
       
        SSTGestReq.TabVisible(4) = False
        SSTGestReq.TabEnabled(4) = False
    End If
    
    
               
    ' Bot�o de importa��o do SONHO.
    If (Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "BT_IMP_SONHO")) = "1") Then
        BtGetSONHO.Enabled = True
        BtGetSONHO.Visible = True
        DoEvents
    End If

    ' Coloca o epis�dio activo.
    If (Len(gEpisodioActivoAux) > 0) Then
        Me.EcEpisodio = gEpisodioActivoAux
        gEpisodioActivoAux = ""
    End If



End Sub

Sub Inicializacoes()
    
    Me.caption = " Gest�o de Requisi��es"
    Me.left = 500
    Me.top = 20
    Me.Width = 13275
    Me.Height = 9765  ' 6780 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_requis"
    Set CampoDeFocus = EcNumReq
    
    NumCampos = 72
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim CamposBCK(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "n_req_assoc"
    CamposBD(2) = "seq_utente"
    CamposBD(3) = "n_epis"
    CamposBD(4) = "dt_previ"
    CamposBD(5) = "dt_chega"
    CamposBD(6) = "dt_imp"
    CamposBD(7) = "t_sit"
    CamposBD(8) = "t_urg"
    CamposBD(9) = "cod_efr"
    CamposBD(10) = "n_benef"
    CamposBD(11) = "cod_proven"
    If gListaMedicos = 1 Then
        CamposBD(12) = "cod_med"
    Else
        CamposBD(12) = "descr_medico"
    End If
    CamposBD(13) = "obs_proven"
    CamposBD(14) = "pag_tax"
    CamposBD(15) = "pag_ent"
    CamposBD(16) = "pag_ute"
    CamposBD(17) = "obs_req"
    CamposBD(18) = "hr_imp"
    CamposBD(19) = "user_imp"
    CamposBD(20) = "dt_imp2"
    CamposBD(21) = "hr_imp2"
    CamposBD(22) = "user_imp2"
    CamposBD(23) = "estado_req"
    CamposBD(24) = "user_cri"
    CamposBD(25) = "hr_cri"
    CamposBD(26) = "dt_cri"
    CamposBD(27) = "user_act"
    CamposBD(28) = "hr_act"
    CamposBD(29) = "dt_act"
    CamposBD(30) = "cod_isencao"
    CamposBD(31) = "gr_ana"
    CamposBD(32) = "arquivo"
    CamposBD(33) = "dt_fact"
    CamposBD(34) = "hr_chega"
    CamposBD(35) = "mot_fact_rej"
    CamposBD(36) = "user_fact_rej"
    CamposBD(37) = "dt_fact_rej"
    CamposBD(38) = "hr_fact_rej"
    CamposBD(39) = "cod_local"
    CamposBD(40) = "user_fecho"
    CamposBD(41) = "dt_fecho"
    CamposBD(42) = "hr_fecho"
    CamposBD(43) = "user_colheita"
    CamposBD(44) = "dt_colheita"
    CamposBD(45) = "hr_colheita"
    CamposBD(46) = "inf_complementar"
    CamposBD(47) = "cod_t_colheita"
    CamposBD(48) = "cod_sala"
    ' pferreira 2007.10.24
    CamposBD(49) = "tipo_urgencia"
    CamposBD(50) = "flg_req_grupo_especial"
    CamposBD(51) = "dt_conclusao"
    CamposBD(52) = "user_confirm"
    CamposBD(53) = "dt_confirm"
    CamposBD(54) = "hr_confirm"
    CamposBD(55) = "cod_destino"
    CamposBD(56) = "COD_VALENCIA"
    CamposBD(57) = "n_prescricao"
    ' pferreira 2011.05.31
    CamposBD(58) = "flg_hipo"
    CamposBD(59) = "cod_urbano"
    CamposBD(60) = "km"
    CamposBD(61) = "convencao"
    CamposBD(62) = "cod_postal"
    CamposBD(63) = "flg_anexo"
    CamposBD(64) = "user_sms"
    CamposBD(65) = "dt_sms"
    CamposBD(66) = "hr_sms"
    CamposBD(67) = "user_email"
    CamposBD(68) = "dt_email"
    CamposBD(69) = "hr_email"
    CamposBD(70) = "flg_diag_sec"
    'RGONCALVES 16.06.2013 ICIL-470
    CamposBD(71) = "flg_aviso_sms"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcReqAssociada
    Set CamposEc(2) = EcSeqUtente
    Set CamposEc(3) = EcEpisodio
    Set CamposEc(4) = EcDataPrevista
    Set CamposEc(5) = EcDataChegada
    Set CamposEc(6) = EcDataImpressao
    Set CamposEc(7) = CbSituacao
    Set CamposEc(8) = CbUrgencia
    Set CamposEc(9) = EcCodEFR
    Set CamposEc(10) = EcNumBenef
    Set CamposEc(11) = EcCodProveniencia
    If gListaMedicos = 1 Then
        Set CamposEc(12) = EcCodMedico
    Else
        Set CamposEc(12) = EcDescrMedico
    End If
    Set CamposEc(13) = EcObsProveniencia
    Set CamposEc(14) = EcTaxaModeradora
    Set CamposEc(15) = EcPagEnt
    Set CamposEc(16) = EcPagUte
    Set CamposEc(17) = EcObsReq
    Set CamposEc(18) = EcHoraImpressao
    Set CamposEc(19) = EcUtilizadorImpressao
    Set CamposEc(20) = EcDataImpressao2
    Set CamposEc(21) = EcHoraImpressao2
    Set CamposEc(22) = EcUtilizadorImpressao2
    Set CamposEc(23) = EcEstadoReq
    Set CamposEc(24) = EcUtilizadorCriacao
    Set CamposEc(25) = EcHoraCriacao
    Set CamposEc(26) = EcDataCriacao
    Set CamposEc(27) = EcUtilizadorAlteracao
    Set CamposEc(28) = EcHoraAlteracao
    Set CamposEc(29) = EcDataAlteracao
    Set CamposEc(30) = EcCodIsencao
    Set CamposEc(31) = EcGrupoAna
    Set CamposEc(32) = EcCodArquivo
    Set CamposEc(33) = EcDataFact
    Set CamposEc(34) = EcHoraChegada
    Set CamposEc(35) = EcCodMotivoRejFact
    Set CamposEc(36) = EcUtilTrataRejFact
    Set CamposEc(37) = EcDataTrataRejFact
    Set CamposEc(38) = EcHoraTrataRejFact
    Set CamposEc(39) = EcLocal
    Set CamposEc(40) = EcUtilizadorFecho
    Set CamposEc(41) = EcDataFecho
    Set CamposEc(42) = EcHoraFecho
    If gUsaUtilizadoresColheitas = mediSim Then
        Set CamposEc(43) = EcCodUserColheita
    Else
        Set CamposEc(43) = EcUserColheita
    End If
    Set CamposEc(44) = EcDtColheita
    Set CamposEc(45) = EcHrColheita
    Set CamposEc(46) = EcinfComplementar
    Set CamposEc(47) = EcPrColheita
    Set CamposEc(48) = EcCodSala
    ' pferreira 2007.10.24
    Set CamposEc(49) = EcTipoUrgencia
    Set CamposEc(50) = EcReqGrupoEspecial
    Set CamposEc(51) = EcDtEntrega
    Set CamposEc(52) = EcUtilConf
    Set CamposEc(53) = EcDtConf
    Set CamposEc(54) = EcHrConf
    Set CamposEc(55) = CbDestino
    Set CamposEc(56) = EcCodValencia
    Set CamposEc(57) = EcPrescricao
    ' pferreira 2011.05.31
    Set CamposEc(58) = CkReqHipocoagulados
    Set CamposEc(59) = CbCodUrbano
    Set CamposEc(60) = EcKm
    Set CamposEc(61) = CbConvencao
    Set CamposEc(62) = EcCodPostalAux
    Set CamposEc(63) = EcFlgAnexo
    Set CamposEc(64) = EcUtilSMS
    Set CamposEc(65) = EcDtSMS
    Set CamposEc(66) = EcHrSMS
    Set CamposEc(67) = EcUtilEmail
    Set CamposEc(68) = EcDtEmail
    Set CamposEc(69) = EcHrEmail
    Set CamposEc(70) = EcFlgDiagSec
    'RGONCALVES 16.06.2013 ICIL-470
    Set CamposEc(71) = CkAvisarSMS
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(2) = "Utente"
    TextoCamposObrigatorios(4) = "Data activa��o da requisi��o"
    TextoCamposObrigatorios(7) = "Tipo de situa��o"
    TextoCamposObrigatorios(8) = "Tipo de urg�ncia"
    'TextoCamposObrigatorios(9) = "Entidade financeira"
    TextoCamposObrigatorios(11) = "Proveni�ncia"
    If gUsaFilaEspera = mediSim Then
        TextoCamposObrigatorios(47) = "Prioridade Colheita"
    End If
        
    
'    If gMarcacoesPrevias = 1 Then
'        TextoCamposObrigatorios(5) = "Data de Chegada"
'    End If
    
    'If gLAB = "HDMC" Or gLAB = "HPOVOA" Or gLAB = "HOVAR" Then
    If gEpisObrigatorio = 1 Then
        TextoCamposObrigatorios(3) = "N�mero de Epis�dio"
        EcEpisodio.locked = False
    End If
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_req"
    Set ChaveEc = EcNumReq
                
    'inicializa��es das estruturas de mem�ria (an�lises e recibos)
    Inicializa_Estrutura_Analises
    
    'O TAB de an�lises est� desactivado at� se introduzir uma entidade financeira
    ' e o de produtos at� introduzir uma data prevista
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabVisible(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    'NELSONPSILVA CHVNG-7461 29.10.2018
    SSTGestReq.TabEnabled(7) = False
    '
    FlgARS = False
    FlgADSE = False
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = False
    
    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    
    'Colocar o bot�o para preencher os dados da requisi��o com os dados da requisi��o anterior inactivo ou activo
    If gRequisicaoActiva = 0 Then
        BtDadosAnteriores.Enabled = False
        LaCopiarAnterior.Enabled = False
    Else
        BtDadosAnteriores.Enabled = True
        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
        LaCopiarAnterior.Enabled = True
    End If
    
    'Inicializa comando para ir buscar as an�lises da requisi��o e os seus dados
    Set CmdAnaReq.ActiveConnection = gConexao
    CmdAnaReq.CommandType = adCmdText
        CmdAnaReq.CommandText = "( Select  m.n_folha_trab, m.n_req, cr.n_req as req_ARS, cr.p1, m.dt_chega, m.cod_agrup, m.ord_ana_perf, m.ord_ana_compl, m.ord_ana, m.cod_perfil, descr_perfis, m.cod_ana_c, descr_ana_c, m.cod_ana_s, descr_ana_s, m.flg_apar_trans, '-1' as flg_estado, m.flg_facturado, m.ord_marca, m.transmit_psm, m.n_envio, cr.cod_u_saude, cr.n_mecan_Ext from sl_marcacoes m LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = m.cod_perfil LEFT OUTER JOIN sl_ana_c c on c.cod_ana_c = m.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = m.cod_ana_s  LEFT OUTER JOIN sl_credenciais cr ON cr.n_req_orig = m.n_req AND cr.cod_ana = m.cod_agrup  where m.n_req =  ? )" & _
                                " Union " & _
                                "( select  r.n_folha_trab, r.n_req, cr.n_req as req_ARS, cr.p1, r.dt_chega, r.cod_agrup, r.ord_ana_perf, r.ord_ana_compl, r.ord_ana, r.cod_perfil, descr_perfis, r.cod_ana_c, descr_ana_c, r.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, r.flg_estado, r.flg_facturado, r.ord_marca, 1 transmit_psm, 1 n_envio, cr.cod_u_saude, cr.n_mecan_Ext from sl_realiza r LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = r.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = r.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = r.cod_ana_s LEFT OUTER JOIN sl_credenciais cr ON cr.n_req_orig = r.n_req and cr.cod_ana = r.cod_agrup where r.n_req =  ? )" & _
                                "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    
    Set CmdAnaReq_H.ActiveConnection = gConexao
    CmdAnaReq_H.CommandType = adCmdText
    If gSGBD = gOracle Then
        CmdAnaReq_H.CommandText = "( select  h.n_folha_trab, h.n_req, cr.n_req as req_ARS, cr.p1, h.dt_chega, h.cod_agrup, h.ord_ana_perf, h.ord_ana_compl, h.ord_ana, h.cod_perfil, descr_perfis, h.cod_ana_c, descr_ana_c, h.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, h.flg_estado, h.flg_facturado, h.ord_marca, 1 transmit_psm, 1 n_envio, cr.cod_u_saude, cr.n_mecan_Ext  from sl_realiza_h h, sl_perfis p, sl_ana_c c, sl_ana_s s, sl_credenciais cr where h.n_req = ? and p.cod_perfis (+) = h.cod_perfil and c.cod_ana_c (+) = h.cod_ana_c and s.cod_ana_s (+) = h.cod_ana_s and ( cr.n_req_orig (+) = h.n_req and cr.cod_ana (+) = h.cod_agrup ) )" & _
                                "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    ElseIf gSGBD = gSqlServer Then
        CmdAnaReq_H.CommandText = "( select  h.n_folha_trab, h.n_req, cr.n_req as req_ARS, cr.p1, h.dt_chega, h.cod_agrup, h.ord_ana_perf, h.ord_ana_compl, h.ord_ana, h.cod_perfil, descr_perfis, h.cod_ana_c, descr_ana_c, h.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, h.flg_estado, h.flg_facturado, h.ord_marca, 1 transmit_psm, 1 n_envio, cr.cod_u_saude, cr.n_mecan_Ext  from  sl_realiza_h h LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = h.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = h.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = h.cod_ana_s LEFT OUTER JOIN sl_credenciais cr ON cr.n_req_orig = h.n_req and cr.cod_ana = h.cod_agrup where h.n_req =  ? )" & _
                                "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    End If
    
    CmdAnaReq.Prepared = True
    CmdAnaReq.Parameters.Append CmdAnaReq.CreateParameter("N_REQ_M", adDouble, adParamInput, 7)
    CmdAnaReq.Parameters.Append CmdAnaReq.CreateParameter("N_REQ_R", adDouble, adParamInput, 7)
    
    CmdAnaReq_H.Prepared = True
    CmdAnaReq_H.Parameters.Append CmdAnaReq_H.CreateParameter("N_REQ_H", adDouble, adParamInput, 7)
        
        
    'Inicializa comando para ir buscar a ordem e o grupo das an�lises
    Set CmdOrdAna.ActiveConnection = gConexao
    CmdOrdAna.CommandType = adCmdText
    CmdOrdAna.CommandText = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis = ? "
    CmdOrdAna.Prepared = True

    CmdOrdAna.Parameters.Append CmdAnaReq.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    CmdOrdAna.Parameters.Append CmdAnaReq.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 10)
    CmdOrdAna.Parameters.Append CmdAnaReq.CreateParameter("COD_PERFIS", adVarChar, adParamInput, 10)
        
    'Inicializa��o das variaveis que guardam os indices nas combos de pagamentos
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1
    
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    EcPrinterResumo = BL_SelImpressora("ResumoRequisicoes.rpt")
    
    'gEpisodioActivo = ""
    
    'gSituacaoActiva = mediComboValorNull
    
    
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    FrameObsEspecif.Visible = False
    FrameObsEspecif.left = 6120
    FrameObsEspecif.top = 360
    
    Flg_Gravou = False
    
    'RGONCALVES 16.06.2013 ICIL-470
    If gEnvioSMSActivo <> mediSim And gEnvioResultadosSMSActivo <> mediSim Then
        CkAvisarSMS.Visible = False
    End If
    '
	'tania.oliveira CHUC-12912 12.11.2019 -> Inicializa��o da estrutura dos tubos
    InicializaEstruturaTubos
    '
End Sub

Function FuncaoProcurar_PreencheProd(NumReq As Long)
    
    ' Selecciona todos os produtos da requisi��o e preenche a lista

    Dim i As Long
    Dim j As Long
    Dim req As String
    Dim rsDescr As ADODB.recordset
    Dim RsProd As ADODB.recordset
    Dim CmdDescrP As New ADODB.Command
    Dim PmtCodProd As ADODB.Parameter
    Dim CmdDescrE As New ADODB.Command
    Dim PmtCodEsp As ADODB.Parameter
      
    Set RsProd = New ADODB.recordset
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Reutilizacao_Colheitas = mediSim Then
       If UBound(reqColh) > 0 Then
            For j = 1 To UBound(reqColh)
                req = req & reqColh(j).n_req & "','"
            Next j
            req = "'" & Mid(req, 1, Len(req) - 2)
        Else
            req = NumReq
        End If
    Else
        req = NumReq
    End If
    '
    With RsProd
        .Source = "SELECT distinct x1.cod_prod, x1.cod_especif,x1.dt_previ, x1.dt_chega, x1.volume, x1.obs_especif, x2.descr_produto, x2.especif_obrig FROM sl_req_prod x1, sl_produto x2" & _
                   " WHERE x1.cod_prod = x2.cod_produto AND n_req in (" & req & ") order by cod_prod"
                   
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With

    'Inicializa o comando ADO para selec��o da descri��o do produto
    Set CmdDescrP.ActiveConnection = gConexao
    CmdDescrP.CommandType = adCmdText
    CmdDescrP.CommandText = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                            "cod_produto =?"
    CmdDescrP.Prepared = True
    Set PmtCodProd = CmdDescrE.CreateParameter("COD_PRODUTO", adVarChar, adParamInput, 5)
    CmdDescrP.Parameters.Append PmtCodProd
    
    'Inicializa o comando ADO para selec��o da descri��o da especifica��o
     Set CmdDescrE.ActiveConnection = gConexao
     CmdDescrE.CommandType = adCmdText
     CmdDescrE.CommandText = "SELECT descr_especif FROM sl_especif WHERE " & _
                              "cod_especif =?"
     CmdDescrE.Prepared = True
     Set PmtCodEsp = CmdDescrE.CreateParameter("COD_ESPECIF", adVarChar, adParamInput, 5)
     CmdDescrE.Parameters.Append PmtCodEsp
     
     i = 1
     LimpaFgProd
     While Not RsProd.EOF
        RegistosP(i).CodProd = RsProd!cod_prod
        RegistosP(i).Volume = BL_HandleNull(RsProd!Volume, "")
        RegistosP(i).DescrProd = BL_HandleNull(RsProd!descr_produto, "")
        RegistosP(i).EspecifObrig = RsProd!especif_obrig
        RegistosP(i).ObsEspecif = BL_HandleNull(RsProd!obs_especif, "")

        RegistosP(i).CodEspecif = BL_HandleNull(RsProd!cod_Especif, "")
        'descri��o da especifica��o
        If Trim(RegistosP(i).CodEspecif) <> "" Then
            CmdDescrE.Parameters(0) = UCase(RsProd!cod_Especif)
            Set rsDescr = CmdDescrE.Execute
            RegistosP(i).DescrEspecif = BL_HandleNull(rsDescr!descr_especif, "")
            rsDescr.Close
        End If
        RegistosP(i).DtPrev = BL_HandleNull(RsProd!dt_previ, "")
        RegistosP(i).DtChega = BL_HandleNull(RsProd!dt_chega, "")
        
        RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
        
        FgProd.TextMatrix(i, 0) = RegistosP(i).CodProd
        FgProd.TextMatrix(i, 1) = RegistosP(i).DescrProd
        FgProd.TextMatrix(i, 2) = RegistosP(i).CodEspecif
        FgProd.TextMatrix(i, 3) = RegistosP(i).DescrEspecif
        FgProd.TextMatrix(i, 4) = RegistosP(i).Volume
        FgProd.TextMatrix(i, 5) = RegistosP(i).DtPrev
        FgProd.TextMatrix(i, 6) = RegistosP(i).DtChega
        FgProd.TextMatrix(i, 7) = RegistosP(i).EstadoProd
        
        'Se h� produtos que j� chegaram ent�o estado n�o deve estar "� espera de prod."
        If (Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto) And Trim(FgProd.TextMatrix(i, 5)) <> "" Then
            EcEstadoReq = gEstadoReqEsperaResultados
            EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        End If
        
        RsProd.MoveNext
        i = i + 1
        CriaNovaClasse RegistosP, i, "PROD"
        FgProd.AddItem "", i
        FgProd.row = i
     Wend
            
     Set CmdDescrE = Nothing
     Set CmdDescrP = Nothing
     Set rsDescr = Nothing
     Set RsProd = Nothing

End Function


Sub FuncaoProcurar_PreencheAna(NumReq As Long)

    Dim i As Long
    Dim k As Long
    Dim z As Long
    Dim j As Long
    Dim MudaIcon As Integer
    Dim LastIntermedio As String
    Dim LastPai As String
    Dim Pai As String
    Dim Ja_Existe As Boolean
    Dim rsAna As New ADODB.recordset
    Dim EstadoFinal As Integer
    Dim EArs As Boolean
    Dim eADSE As Boolean
    Dim ImgPai As Integer
    Dim ImgPaiSel As Integer
    Dim ObsAnaReq As String
    Dim seqObsAnaReq As Long
    Dim sql As String
    Dim flg_estado_aux As String
    
    If EcEstadoReq = gEstadoReqHistorico Then
        sql = Replace(CmdAnaReq_H.CommandText, "?", NumReq)
    Else
        sql = Replace(CmdAnaReq.CommandText, "?", NumReq)
    End If
    With rsAna
        .Source = sql
        .CursorType = adOpenStatic
        .CursorLocation = adUseServer
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open
    End With
    i = 1
    LimpaFGAna
    ExecutaCodigoA = False
    ContP1 = 0
    DefFgAna EFR_Verifica_ARS(EcCodEFR)
    FGAna.Col = lColFgAnaCodAna
    'FGAna.ColWidth(lColFgAnaDescrAna) = 6000
    LastIntermedio = ""
    LastPai = ""
    Pai = ""
    MudaIcon = 0
    ImgPai = 5
    ImgPaiSel = 6

    EArs = EFR_Verifica_ARS(EcCodEFR)
    eADSE = EFR_Verifica_ADSE(EcCodEFR)
    While Not rsAna.EOF
        flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
        If BL_HandleNull(rsAna!flg_estado, "-1") <> "-1" Then
            If VerificaAnaliseJaExisteRealiza(NumReq, BL_HandleNull(rsAna!Cod_Perfil, ""), BL_HandleNull(rsAna!cod_ana_c, ""), BL_HandleNull(rsAna!cod_ana_s, ""), False) = 0 Then
                flg_estado_aux = -1
            Else
                flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
            End If
        Else
            flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
        End If
            'Debug.Print RsAna!Cod_Ana_C & "-" & RsAna!cod_ana_s
            BL_Preenche_MAReq FgTubos, BL_HandleNull(rsAna!Cod_Perfil, ""), _
                            BL_HandleNull(rsAna!descr_perfis, ""), _
                            BL_HandleNull(rsAna!cod_ana_c, ""), _
                            BL_HandleNull(rsAna!Descr_Ana_C, ""), _
                            BL_HandleNull(rsAna!cod_ana_s, ""), _
                            BL_HandleNull(rsAna!descr_ana_s, ""), _
                            BL_HandleNull(rsAna!Ord_Ana, 0), _
                            BL_HandleNull(rsAna!Ord_Ana_Compl, 0), _
                            BL_HandleNull(rsAna!Ord_Ana_Perf, 0), _
                            BL_HandleNull(rsAna!cod_agrup, ""), _
                            BL_HandleNull(rsAna!N_Folha_Trab, 0), _
                            BL_HandleNull(rsAna!dt_chega, ""), _
                            BL_HandleNull(rsAna!flg_apar_trans, "0"), _
                            flg_estado_aux, _
                            BL_HandleNull(rsAna!Flg_Facturado, 0), _
                            BL_HandleNull(rsAna!Ord_Marca, 0), _
                            BL_HandleNull(rsAna!transmit_psm, 0), _
                            BL_HandleNull(rsAna!n_envio, 0), EcDataPrevista
    
            Ja_Existe = False
            For k = 1 To RegistosA.Count
                If Trim(RegistosA(k).codAna) = Trim(MaReq(i).cod_agrup) Then
                    Ja_Existe = True
                    Exit For
                End If
            Next k
    
            If (EArs Or eADSE) And FGAna.Cols = 2 Then
                DefFgAna True
    '            FGAna.ColWidth(lColFgAnaDescrAna) = 3700
    '            FGAna.Cols = 6
    '            FGAna.ColWidth(lColFgAnaCred) = 2000
    '            FGAna.Col = lColFgAnaCred
    '            FGAna.TextMatrix(0, lColFgAnaCred) = "Req. ARS"
    '            FGAna.ColWidth(lColFgAnaOrdem) = 400
    '            FGAna.Col = lColFgAnaOrdem
    '            FGAna.TextMatrix(0, lColFgAnaOrdem) = "P1"
    '            FGAna.ColWidth(lColFgAnaMedSaude) = 1000
    '            FGAna.Col = lColFgAnaMedSaude
    '            FGAna.TextMatrix(0, lColFgAnaMedSaude) = "M�dico"
    '
    '            FGAna.ColWidth(lColFgAnaUnidSaude) = 1000
    '            FGAna.Col = lColFgAnaUnidSaude
    '            FGAna.TextMatrix(0, lColFgAnaUnidSaude) = "U.Sa�de"
    '            FGAna.Col = lColFgAnaCodAna
            End If
    
            If Flg_DadosAnteriores = True Then
                'For�a o estado da an�lise para sem resultado quando se copia uma requisi��o
                MaReq(i).flg_estado = "-1"
            End If
            
            If MaReq(i).flg_estado = "-1" Then
                MudaIcon = 0
            Else
                MudaIcon = 6
            End If
    
            If Not Ja_Existe Then
                LbTotalAna = LbTotalAna + 1
                RegistosA(FGAna.rows - 1).codAna = MaReq(i).cod_agrup
                If Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).Cod_Perfil) Then
                    RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Perfil
                    ImgPai = 1 + MudaIcon
                    ImgPaiSel = 2 + MudaIcon
                ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_c) Then
                    RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Ana_C
                    ImgPai = 3 + MudaIcon
                    ImgPaiSel = 4 + MudaIcon
                ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_s) Then
                    RegistosA(FGAna.rows - 1).descrAna = MaReq(i).descr_ana_s
                    ImgPai = 5 + MudaIcon
                    ImgPaiSel = 6 + MudaIcon
                Else
                    RegistosA(FGAna.rows - 1).descrAna = ""
                    ImgPai = 5 + MudaIcon
                    ImgPaiSel = 6 + MudaIcon
                End If
                
                ObsAnaReq = ""
                seqObsAnaReq = 0
                BL_DevolveObsAnaReq NumReq, MaReq(i).cod_agrup, ObsAnaReq, seqObsAnaReq
                RegistosA(FGAna.rows - 1).ObsAnaReq = ObsAnaReq
                RegistosA(FGAna.rows - 1).seqObsAnaReq = seqObsAnaReq
                
                RegistosA(FGAna.rows - 1).NReqARS = BL_HandleNull(rsAna!req_ARS, "A gerar")
                RegistosA(FGAna.rows - 1).codUnidSaude = BL_HandleNull(rsAna!cod_u_saude, "")
                RegistosA(FGAna.rows - 1).MedUnidSaude = BL_HandleNull(rsAna!n_mecan_ext, "")
                If BL_HandleNull(rsAna!p1, "-1") <> "-1" Then
                    ContP1 = BL_HandleNull(rsAna!p1, "-1")
                Else
                    ContP1 = ContP1 + 1
                End If
                'FeriasFernando
                RegistosA(FGAna.rows - 1).p1 = ContP1
                'RegistosA(FgAna.rows - 1).ReciboP1 = ContP1
                FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodAna) = RegistosA(FGAna.rows - 1).codAna
                'NELSONPSILVA CHVNG-7461 29.10.2018
                If gAtiva_Reutilizacao_Colheitas = mediSim And TotalAnaReutil > 0 Then
                    FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescrAna) = IIf(COLH_Existe_Ana_Reutil(RegistosA(FGAna.rows - 1).codAna) = True, RegistosA(FGAna.rows - 1).descrAna & " *", RegistosA(FGAna.rows - 1).descrAna)
                    'FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescrAna) = BL_Verifica_Analise_Reutil(NumReq, RegistosA(FGAna.rows - 1).codAna, RegistosA(FGAna.rows - 1).descrAna)
                    'FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescrAna) = IIf(MaReq(i).reutil = mediSim, RegistosA(FGAna.rows - 1).descrAna & " *", RegistosA(FGAna.rows - 1).descrAna)
                Else
                    FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescrAna) = RegistosA(FGAna.rows - 1).descrAna
                End If
                RegistosA(FGAna.rows - 1).DtChega = BL_HandleNull(rsAna!dt_chega, "")
                
                If MaReq(i).flg_estado <> "-1" And Trim(FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodAna)) <> "" Then
                    FGAna.CellBackColor = vbWhite
                    FGAna.CellForeColor = vbRed
                Else
                    FGAna.CellBackColor = vbWhite
                    FGAna.CellForeColor = vbBlack
                End If
                
                If FGAna.Cols >= 4 Then
                    FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCred) = RegistosA(FGAna.rows - 1).NReqARS
                    FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaOrdem) = RegistosA(FGAna.rows - 1).p1
                    FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaMedSaude) = RegistosA(FGAna.rows - 1).MedUnidSaude
                    FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaUnidSaude) = RegistosA(FGAna.rows - 1).codUnidSaude
                End If
                
                'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
                If gUsaEnvioPDS = mediSim Then
                    Call ChangeCheckboxState(FGAna.rows - 1, lColFgAnaConsenteEnvioPDS, DevolveEstadoEnvioPDS(rsAna!cod_agrup, EcNumReq.text))
                    'FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaConsenteEnvioPDS) = DevolveEstadoEnvioPDS(rsAna!cod_agrup, EcNumReq.Text)
                End If
                '
                
                CriaNovaClasse RegistosA, 0, "ANA"
                FGAna.AddItem ""
                FGAna.row = FGAna.rows - 1
            
            End If
            
            Pai = Trim(MaReq(i).cod_agrup)
            
            If Pai <> LastPai Then
                'Pai = codigo de agupamento (seja Perfil, Complexa ou Simples)
                BL_AcrescentaNodoPai TAna, IIf((RegistosA(FGAna.rows - 2).descrAna = ""), MaReq(i).cod_agrup, RegistosA(FGAna.rows - 2).descrAna), MaReq(i).cod_agrup, ImgPai, ImgPaiSel
                LastPai = Pai
            End If
            
            
            If Trim(MaReq(i).cod_ana_c) <> "0" And Trim(MaReq(i).cod_ana_c) <> "" And Trim(Pai) <> Trim(MaReq(i).cod_ana_c) Then
                'Se existir complexa e for diferente do Pai insere-a como intermedia entre o Pai e a simples
                If LastIntermedio = MaReq(i).cod_ana_c Then
                    'j� foi inserida uma vez -> insere-se directamente a simples
                    If MaReq(i).cod_ana_s = "S99999" Then
                        BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                    Else
                        BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                    End If
                Else
                    '� a primeira vez -> insere a complexa e a simples por debaixo da mesma
                    LastIntermedio = MaReq(i).cod_ana_c
                    BL_AcrescentaNodoFilho TAna, MaReq(i).Descr_Ana_C, Pai, Pai & MaReq(i).cod_ana_c, 3 + MudaIcon, 4 + MudaIcon
                    
                    'inserir a simples
                    If MaReq(i).cod_ana_s = "S99999" Then
                        BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                    Else
                        BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                    End If
                End If
            Else
                'N�o existe complexa ou � o pai
                If Trim(Pai) <> Trim(MaReq(i).cod_ana_s) Then
                    'Se o pai nao for a propria simples insere-a de baixo do pai
                    If MaReq(i).cod_ana_s = "S99999" Then
                        BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai, Pai & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                    Else
                        BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai, Pai & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                    End If
                    
                End If
            End If
            
            If MudaIcon = 6 And TAna.Nodes.Item(MaReq(i).cod_agrup).Image < 6 Then
                TAna.Nodes.Item(MaReq(i).cod_agrup).Image = TAna.Nodes.Item(MaReq(i).cod_agrup).Image + MudaIcon
                TAna.Nodes.Item(MaReq(i).cod_agrup).SelectedImage = TAna.Nodes.Item(MaReq(i).cod_agrup).SelectedImage + MudaIcon
            End If
            
            i = i + 1
            rsAna.MoveNext
            
            'Se encontrou analises o estado n�o deve estar "Sem Analises"
            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
                EcEstadoReq = gEstadoReqEsperaProduto
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
            End If
    Wend
    
    For i = 1 To FGAna.rows - 2
        EstadoFinal = -1
        For k = 1 To UBound(MaReq)
            If Trim(RegistosA(i).codAna) = Trim(MaReq(k).cod_agrup) Then
                If MaReq(k).flg_estado <> "-1" Then
                    EstadoFinal = 0
                    Exit For
                End If
            End If
        Next k
        RegistosA(i).Estado = EstadoFinal
    Next i
    RegistosA(FGAna.rows - 1).Estado = "-1"
    
    'NELSONPSILVA CHVNG-7461 29.10.2018 adiciona analises da colheita na grelha
    If gAtiva_Reutilizacao_Colheitas = mediSim Then
        AcrescentaAnaReutil
        'RemoveAnaReutil
    End If
    
    rsAna.Close
    Set rsAna = Nothing
    
    ExecutaCodigoA = True
    
    FGAna.Col = lColFgAnaCodAna
    FGAna.row = 1

End Sub

Sub DefTipoCampos()
    dataAct = Bg_DaData_ADO
    
    EcDtColheita.Tag = adDate
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_previ", EcDataPrevista, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_chega", EcDataChegada, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp", EcDataImpressao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp", EcHoraImpressao, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp2", EcDataImpressao2, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp2", EcHoraImpressao2, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fact", EcDataFact, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fact_rej", EcDataTrataRejFact, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fecho", EcDataFecho, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_sms", EcDtSMS, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_email", EcDtEmail, mediTipoData
    
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp", EcUtilizadorImpressao, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp2", EcUtilizadorImpressao2, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "user_cri", EcUtilizadorCriacao, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "user_act", EcUtilizadorAlteracao, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_conclusao", EcDtEntrega, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_confirm", EcDtConf, mediTipoData
    
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    ExecutaCodigoP = False
    DefFgAna False
    FGAna.rows = 2
    If gGestaoColheita <> mediSim Then
        LbNumColheita.Visible = False
        EcNumColheita.Visible = False
    Else
        LbNumColheita.Visible = True
        EcNumColheita.Visible = True
    End If
    
    LastColA = 0
    LastRowA = 1
    CriaNovaClasse RegistosA, 1, "ANA", True

    With FgProd
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Produto"
        .ColWidth(1) = 2700
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 850
        .Col = 2
        .TextMatrix(0, 2) = "Especifica."
        .ColWidth(3) = 2700
        .Col = 3
        .TextMatrix(0, 3) = "Descri��o"
        .Col = 4
        .ColWidth(4) = 750
        .TextMatrix(0, 4) = "Volume"
        .ColWidth(5) = 900
        .Col = 5
        .TextMatrix(0, 5) = "Previsto"
        .ColWidth(6) = 900
        .Col = 6
        .TextMatrix(0, 6) = "Chegada"
        .ColWidth(7) = 600
        .Col = 7
        .TextMatrix(0, 7) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    LastColP = 0
    LastRowP = 1
    CriaNovaClasse RegistosP, 1, "PROD", True
    
    
    TB_DefTipoFGTubos FgTubos
    
    Set TAna.ImageList = ListaImagensAna
    TAna.Indentation = 0
    
    Flg_PergProd = False
    Resp_PergProd = False
    Flg_DtChProd = False
    Resp_PergDtChProd = False
    
    Flg_PergTubo = False
    Resp_PergTubo = False
    Flg_DtChTubo = False
    Resp_PergDtChTubo = False
    
    
    'ExecutaCodigoA = False
    ExecutaCodigoP = False
    
    BtEtiq.Enabled = False
    LaEtiq.Enabled = False
    LaInfCli.Enabled = False
    BtResumo.Enabled = False
    BtAnexos.Enabled = False
    FrameGarrafa.Enabled = False
    FrameGarrafa.Visible = False
    FrameGarrafa.left = 7800
    FrameGarrafa.top = 1920
    SSTGestReq.Enabled = True
    EcObsAna = ""
    FrameObsAna.Visible = False
    BtNotas.Enabled = True
    If gListaMedicos = 1 Then
        EcDescrMedico.Visible = False
        EcCodMedico.Visible = True
        EcNomeMedico.Visible = True
        BtPesquisaMedico.Visible = True
    Else
        EcDescrMedico.Visible = True
        EcCodMedico.Visible = False
        EcNomeMedico.Visible = False
        BtPesquisaMedico.Visible = False
    End If
    If gUsaFilaEspera = mediSim Then
        CbPrioColheita.Visible = True
        LbPrioColheita.Visible = True
    Else
        CbPrioColheita.Visible = False
        LbPrioColheita.Visible = False
    End If
    If gAssinaturaActivo = mediSim Then
        LbAssinatura.Visible = True
    Else
        LbAssinatura.Visible = False
    End If
    If gUsaUtilizadoresColheitas = mediSim Then
        EcUserColheita.Visible = False
        LabelColheita.Visible = True
        EcCodUserColheita.Visible = True
        EcDescrUserColheita.Visible = True
        BtPesquisaColheita.Visible = True
    Else
        EcUserColheita.Visible = True
        LabelColheita.Visible = True
        EcCodUserColheita.Visible = False
        EcDescrUserColheita.Visible = False
        BtPesquisaColheita.Visible = False
    End If
    EcPrescricao.locked = False
    FrameDomicilio.Visible = False
    If gUsaMarcacaoCodAnaEFR = mediSim Then
        FrameMarcaArs.Visible = True
    Else
        FrameMarcaArs.Visible = False
    End If
    CkIsento.value = vbGrayed
    CkReqHipocoagulados.value = vbGrayed
    CriaNovaClasse RegistosRM, 1, "REC_MAN"
    CriaNovaClasse RegistosR, 1, "REC"
    If gLAB = "CHVNG" Or gLAB = "HCVP" Or gInibeDataChegada = mediSim Then
        EcDataChegada.Enabled = False
    Else
        EcDataChegada.Enabled = True
    End If
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    ReDim reqColh(0)
    TotalReqColh = 0
    EcUtilNomeEmail = ""
    EcUtilNomeSMS = ""
    PM_LimpaEstrutura
    PreencheSalaDefeito
    If gLAB = "CHNE" Then
        EcProcHosp1.Enabled = True
    End If
    'RGONCALVES 16.06.2013 ICIL-470
    CkAvisarSMS.value = vbGrayed
    
    'NELSONPSILVA CHVNG-7461 29.10.2018
    With FgPedAdiciona
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "C�digo"
        .ColWidth(1) = 2700
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 2000
        .Col = 2
        
        .TextMatrix(0, 2) = "M�dico"
        .ColWidth(3) = 2000
        .Col = 3
        
        .TextMatrix(0, 3) = "T�cnico"
        .Col = 4
        .ColWidth(4) = 1000
        
        .TextMatrix(0, 4) = "Prescricao"
        .ColWidth(5) = 1100
        .Col = 5
        
        .TextMatrix(0, 5) = "Data"
        .ColWidth(6) = 600
        .Col = 6
        '.MergeCol(lColFgPedAdicionaHrPedido) = True
        .TextMatrix(0, 6) = "Hora"
        .ColWidth(7) = 2000
        .Col = 7
        
        .TextMatrix(0, 7) = "Proveni�ncia"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    FgPedAdiciona.MergeCol(lColFgPedAdicionaMedico) = True
    FgPedAdiciona.MergeCol(lColFgPedAdicionaTecnico) = True
    FgPedAdiciona.MergeCol(lColFgPedAdicionaPrescricao) = True
    FgPedAdiciona.MergeCol(lColFgPedAdicionaDtPedido) = True
    FgPedAdiciona.MergeCol(lColFgPedAdicionaHrPedido) = True
    FgPedAdiciona.MergeCol(lColFgPedAdicionaProven) = True
End Sub

Private Sub DefFgAna(flg_ars As Boolean)
    
    With FGAna
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(lColFgAnaCodAna) = 1000
        .Col = lColFgAnaCodAna
        .ColAlignment(lColFgAnaCodAna) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaCodAna) = "C�digo"
        
        .ColWidth(lColFgAnaDescrAna) = 5900
        .Col = lColFgAnaDescrAna
        .ColAlignment(lColFgAnaDescrAna) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaDescrAna) = "Descri��o"
        
        If flg_ars = True Then
            FGAna.ColWidth(lColFgAnaDescrAna) = 2500
            FGAna.ColWidth(lColFgAnaCred) = 3000
            FGAna.Col = lColFgAnaCred
            FGAna.TextMatrix(0, lColFgAnaCred) = "Req. ARS"
            FGAna.ColWidth(lColFgAnaOrdem) = 500
            FGAna.Col = lColFgAnaOrdem
            FGAna.TextMatrix(0, lColFgAnaOrdem) = "Ord"
            
            FGAna.ColWidth(lColFgAnaMedSaude) = 950
            FGAna.Col = lColFgAnaMedSaude
            FGAna.TextMatrix(0, lColFgAnaMedSaude) = "M�dico"
            
            FGAna.ColWidth(lColFgAnaUnidSaude) = 1000
            FGAna.Col = lColFgAnaUnidSaude
            FGAna.TextMatrix(0, lColFgAnaUnidSaude) = "U.Sa�de"
            
            'BRUNODSANTOS GLINTT-HS-15750 03.12.2018
            If gUsaEnvioPDS = mediSim Then
                .Cols = .Cols + 1
                .ColWidth(lColFgAnaConsenteEnvioPDS) = 850
                .Col = lColFgAnaConsenteEnvioPDS
                .ColAlignment(lColFgAnaConsenteEnvioPDS) = flexAlignLeftCenter
                .TextMatrix(0, lColFgAnaConsenteEnvioPDS) = "Envio PDS"
            End If
            '
            
        ElseIf flg_ars = False Then
            FGAna.ColWidth(lColFgAnaCred) = 0
            FGAna.Col = lColFgAnaCred
            FGAna.TextMatrix(0, lColFgAnaCred) = ""
            FGAna.ColWidth(lColFgAnaOrdem) = 0
            FGAna.Col = lColFgAnaOrdem
            FGAna.TextMatrix(0, lColFgAnaOrdem) = ""
            
            FGAna.ColWidth(lColFgAnaMedSaude) = 0
            FGAna.Col = lColFgAnaMedSaude
            FGAna.TextMatrix(0, lColFgAnaMedSaude) = ""
            
            FGAna.ColWidth(lColFgAnaUnidSaude) = 0
            FGAna.Col = lColFgAnaUnidSaude
            FGAna.TextMatrix(0, lColFgAnaUnidSaude) = ""
        End If
        If gPrecosAmbienteHospitalar = mediSim Then
            FGAna.ColWidth(lColFgAnaPreco) = 600
            FGAna.Col = lColFgAnaPreco
            FGAna.TextMatrix(0, lColFgAnaPreco) = "Taxa"
        
        Else
            FGAna.ColWidth(lColFgAnaPreco) = 0
            FGAna.Col = lColFgAnaPreco
            FGAna.TextMatrix(0, lColFgAnaPreco) = ""
        End If
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

End Sub
Sub PreencheDadosTubo(indice As Long, _
                      DescrGrAna As String, _
                      abrAna As String, _
                      CodProd As String, _
                      CodTubo As String, _
                      Especial As Boolean, _
                      Cheio As Boolean, _
                      codTuboBar As String, _
                      QtMax As Double, _
                      QtOcup As Double, _
                      Designacao As String, inf_complementar As String, _
                      num_copias As Integer, Descr_etiq_ana As String)
    
    Tubos(indice).CodProd = CodProd
    Tubos(indice).CodTubo = CodTubo
    Tubos(indice).GrAna = Trim(DescrGrAna)
    
    ' Espec�fico HUCs.
    ' Impress�o de etiquetas.
    If gLAB = cHUCs Then
        'OrdAna = 1
    End If
    
    'If OrdAna <> 0 Then
        Tubos(indice).abrAna = abrAna
    'Else
    '    Tubos(Indice).AbrAna = ""
    'End If
    
    Tubos(indice).Especial = Especial
    Tubos(indice).Cheio = Cheio
    Tubos(indice).codTuboBar = codTuboBar
    Tubos(indice).QtMax = QtMax
    Tubos(indice).QtOcup = QtOcup
    Tubos(indice).Designacao_tubo = Designacao
    Tubos(indice).inf_complementar = inf_complementar
    Tubos(indice).num_copias = num_copias
    Tubos(indice).Descr_etiq_ana = Descr_etiq_ana
    Tubos(indice).dt_chega = BL_RetornaDtChegaTubo(EcNumReq, CodTubo)
    
End Sub

Sub PreencheValoresDefeito()
    
    EcNumColheita.text = ""
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao, "1"
    
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, mediAscComboCodigo
    
    BG_PreencheComboBD_ADO "sl_isencao", "seq_isencao", "descr_isencao", CbTipoIsencao
    
    ' PFerreira 08.03.2007
    BG_PreencheComboBD_ADO "sl_cod_prioridades", "cod_prio", "nome_prio", CbPrioColheita, mediAscComboCodigo
    
    BG_PreencheComboBD_ADO "sl_tbf_t_destino", "cod_t_dest", "descr_t_dest", CbDestino, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_t_urbano", "cod_t_urbano", "descr_t_urbano", CbCodUrbano
    BG_PreencheComboBD_ADO "sl_tbf_convencao", "cod_convencao", "descr_convencao", CbConvencao
    
    
'    'Preenche Combo Tipo Urgencia
'    CbUrgencia.AddItem "Normal"
'    CbUrgencia.AddItem "Urgente"
'    CbUrgencia.AddItem "L.Azul"

    
    'Preenche combo estado da requisi��o
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)

    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    
    'NELSONPSILVA CHVNG-7461 29.10.2018
    TotalAnaReutil = 0
    ReDim anaReutil(0)
    '
    
    PicMasc.Visible = False
    PicFem.Visible = False
    
    lObrigaTerap = False
    lObrigaInfoCli = False
End Sub


Sub EventoActivate()
    
    Dim i As Integer
    Dim RsPesqUte As ADODB.recordset
    Dim sql As String
    Dim HisAberto As Integer
    DoEvents
    If FormGestaoRequisicao.Enabled = False Then
        FormGestaoRequisicao.Enabled = True
    End If
    
    Set gFormActivo = Me
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
    BG_StackJanelas_Actualiza Me
    
    BL_ToolbarEstadoN Estado
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    If Estado = 2 Then
       BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        Err.Clear
        On Error Resume Next
        If rs!estado_req = gEstadoReqCancelada Then
            
            Select Case Err.Number
                Case 0
                    ' OK
                Case 3021
                    ' BOF ou EOF � verdadeiro ou o registo actual foi eliminado; a opera��o pedida necessita de um registo actual.
                    rs.Requery
                Case Else
                
            End Select
            
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            BtCancelarReq.Visible = False
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
        End If
    End If
       
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    'SOLIVEIRA 09.10.2007
    'Pesquisar utente a partir do comando recebido do exterior (GH)
        'SCMVC-1561 04.03.2019 valida se o ecra etiquetas foi aberto
    '
    If Command <> "" And etiqFlag = False Then
        HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
        If HisAberto = 1 Then
        
            Set RsPesqUte = New ADODB.recordset
            With RsPesqUte
                .Source = "select * from sl_identif where t_utente = " & BL_TrataStringParaBD(UCase(gArgTUtente)) & _
                            " AND utente = " & BL_TrataStringParaBD(gArgNUtente)
                .CursorType = adOpenStatic
                .CursorLocation = adUseServer
                .ActiveConnection = gConnHIS
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open
            End With
            If RsPesqUte.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo do " & HIS.nome & " do utente " & gArgTUtente & "/" & gArgNUtente & "!", vbInformation, "Procurar"
                gDUtente.seq_utente = ""
            Else
                gDUtente.seq_utente = "-1"
                gDUtente.morada = BL_HandleNull(RsPesqUte!descr_mor_ute, "")
                gDUtente.cod_postal = BL_HandleNull(RsPesqUte!cod_postal_ute, "")
                gDUtente.nome_ute = RsPesqUte!nome_ute
                gDUtente.dt_nasc_ute = RsPesqUte!dt_nasc_ute
                gDUtente.sexo_ute = RsPesqUte!sexo_ute
                gDUtente.t_utente = RsPesqUte!t_utente
                gDUtente.Utente = RsPesqUte!Utente
                gDUtente.cod_efr = BL_HandleNull(RsPesqUte!cod_efr_ute, 0)
                gDUtente.cod_genero = RsPesqUte!cod_genero
                gDUtente.telefone = BL_HandleNull(RsPesqUte!telef_ute, "")
                gDUtente.telemovel = BL_HandleNull(RsPesqUte!telemovel, "")
                gDUtente.num_contribuinte = BL_HandleNull(RsPesqUte!n_contrib_ute, "")
                gDUtente.cod_isencao_ute = BL_HandleNull(RsPesqUte!cod_isencao_ute, "")
                Flg_PesqUtente = True
            End If
            RsPesqUte.Close
            Set RsPesqUte = Nothing
        End If
    End If
    
    'soliveira 05-12-2003
    'Para poder ir buscar um doente a partir da pesquisa de utentes da gest�o de requisi��es
    'Inserir o utente se este ainda n�o existir na BD
    '----------------------------------------------------------------------
    If gDUtente.seq_utente <> "" And (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        If gDUtente.seq_utente = "-1" And gF_IDENTIF = 0 Then
'            ' UTENTE HIS
            Set RsPesqUte = New ADODB.recordset
            With RsPesqUte
                .Source = "SELECT * from sl_identif where " & _
                        " t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & _
                        " and utente = " & BL_TrataStringParaBD(gDUtente.Utente)
                .CursorType = adOpenStatic
                .CursorLocation = adUseServer
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                .Open , gConexao
            End With
            If RsPesqUte.RecordCount > 0 Then
                EcSeqUtente = RsPesqUte!seq_utente
                PreencheDadosUtente
                If Flg_PesqUtente = True Then
                    AtualizaDadosUtente
                End If
            Else
                BD_Insert_Utente
            End If
        ElseIf gDUtente.seq_utente = "-1" And gF_IDENTIF = 1 Then
            FormIdentificaUtente.Flg_PesqUtente = True
            Unload Me
            Exit Sub
        Else
            'j� tem utente
            EcSeqUtente = gDUtente.seq_utente
            PreencheDadosUtente
            Flg_PesqUtente = False
        End If
        Flg_PesqUtente = False

    End If
    '-------------------------------------------------------------------
    
    ' PFerreira 10.04.2007
    ' Caso o ecra de identificacao de utentes for o ecra anterior preenche a prioridade de colheita
    If (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        If CbPrioColheita.ListCount > 0 Then
            If (Trim(EcDataNasc) <> "" And IsChild) Then
                CbPrioColheita.ListIndex = 3
            Else
                CbPrioColheita.ListIndex = 1
            End If
        End If
    End If
    
    If gF_IDENTIF = 1 And SSTGestReq.Tab = 0 Then
        If Trim(FormIdentificaUtente.EcCodIsencao.text) <> "" Then
            CkIsento.value = vbChecked
            For i = 0 To CbTipoIsencao.ListCount - 1
                If CbTipoIsencao.ItemData(i) = CLng(Trim(FormIdentificaUtente.EcCodIsencao.text)) Then
                    CbTipoIsencao.ListIndex = i
                    SSTGestReq.TabEnabled(3) = False
                    SSTGestReq.TabVisible(3) = False
                    SSTGestReq.TabEnabled(4) = False
                    Exit For
                End If
            Next i
        Else
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabVisible(4) = False
        End If
    
        If EcEpisodio.Enabled = True Then EcEpisodio.SetFocus
        If CbSituacao.Enabled = False And EcDataPrevista.Enabled = True Then EcDataPrevista.SetFocus
    End If
    If EcNumReq <> "" Then
        PreencheNotas
    End If
    
'    If gPassaParams.Param(0) <> "" Then
'        EcUtente.Text = gPassaParams.Param(0)
'    End If
    
    If gMultiReport = 1 Then
        SSTGestReq.TabEnabled(6) = False
        SSTGestReq.TabVisible(5) = False
        SSTGestReq.TabVisible(4) = False
        BtEtiq.ToolTipText = "Imprimir Etiquetas de M�dicos"
        FrEtiq.ToolTipText = "Imprimir Etiquetas de M�dicos"
        LaEtiq.ToolTipText = "Imprimir Etiquetas de M�dicos"
        Label41(5).caption = "&Gesta��o"
        Label41(5).Width = 600
        Label41(5).Alignment = vbCenter
        LaSemanas.Visible = True
        LaDataFact.Visible = True
        EcDataFact.Visible = True
        CkIsento.Enabled = False
        CbTipoIsencao.Enabled = False
    Else
        SSTGestReq.TabVisible(5) = True
        SSTGestReq.TabEnabled(6) = True
    End If
    
    If gMostraColheita = 1 Then
        SSTGestReq.TabVisible(6) = True
        SSTGestReq.TabEnabled(6) = False
    Else
        SSTGestReq.TabVisible(6) = False
    End If
    'NELSONPSILVA CHVNG-7461 29.10.2018
    If gAtiva_Reutilizacao_Colheitas = mediSim Then
        SSTGestReq.TabVisible(7) = True
        SSTGestReq.TabEnabled(7) = False
    Else
        SSTGestReq.TabVisible(7) = False
    End If
    '
    
    If gF_TRATAREJ = 1 And gPassaParams.id = "TRATA_REJEITADOS" Then
        EcNumReq.text = gPassaParams.Param(0)
        EcMotivoRejFact.Visible = True
        EcMotivoRejFact.text = "REJEITADA - " & gPassaParams.Param(2)
        BG_LimpaPassaParams
        FuncaoProcurar
    End If
    
    ' PFerreira 10.04.2007
    ' Caso o ecra de identificacao de utentes for o ecra anterior preenche a prioridade de colheita
    If (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        If CbPrioColheita.ListCount > 0 Then
            If (Trim(EcDataNasc) <> "" And IsChild) Then
                CbPrioColheita.ListIndex = 3
            Else
                CbPrioColheita.ListIndex = 1
            End If
        End If
    End If
    
    'SOLIVEIRA 09.10.2007
        'SCMVC-1561 04.03.2019 valida se o ecra etiquetas foi aberto
    '
    If Command <> "" And etiqFlag = False Then
        Select Case gArgTEpisodio
            Case "Urgencias"
                CbSituacao.ListIndex = gT_Urgencia
            Case "Consultas"
                CbSituacao.ListIndex = gT_Consulta
            Case "Internamentos"
                CbSituacao.ListIndex = gT_Internamento
            Case "Hosp-Dia"
                CbSituacao.ListIndex = gT_Externo
            Case "Ambulatorio"
                CbSituacao.ListIndex = gT_Externo
            Case "Prescricoes"
                CbSituacao.ListIndex = gT_Prescricoes
            Case "FICHA-ID"
                CbSituacao.ListIndex = gT_Ficha_ID
                
            Case "Acidente"
                CbSituacao.ListIndex = gT_Acidente
            Case "Ambulatorio"
                CbSituacao.ListIndex = gT_Ambulatorio
            Case "Cons-Inter"
                CbSituacao.ListIndex = gT_Cons_Inter
            Case "Cons-Telef"
                CbSituacao.ListIndex = gT_Cons_Telef
            Case "Consumos"
                CbSituacao.ListIndex = gT_Consumos
            Case "Credenciais"
                CbSituacao.ListIndex = gT_Credenciais
            Case "Diagnosticos"
                CbSituacao.ListIndex = gT_Diagnosticos
            Case "Exame"
                CbSituacao.ListIndex = gT_Exame
            Case "Fisio"
                CbSituacao.ListIndex = gT_Fisio
            Case "Hosp-Dia"
                CbSituacao.ListIndex = gT_HDI
            Case "Intervencao"
                CbSituacao.ListIndex = gT_Intervencao
            Case "Mcdt"
                CbSituacao.ListIndex = gT_MCDT
            Case "Plano-Oper"
                CbSituacao.ListIndex = gT_Plano_Oper
            Case "Pre-Intern"
                CbSituacao.ListIndex = gT_Pre_Intern
            Case "Prog-Cirurgico"
                CbSituacao.ListIndex = gT_Prog_Cirugico
            Case "Protoc"
                CbSituacao.ListIndex = gT_Protoc
            Case "Referenciacao"
                CbSituacao.ListIndex = gT_Referenciacao
            Case "Reg-Oper"
                CbSituacao.ListIndex = gT_Reg_Oper
            Case "Tratamentos"
                CbSituacao.ListIndex = gT_Tratamentos
            Case Else
                CbSituacao.ListIndex = -1
        End Select
        EcEpisodio.text = gArgNEpisodio
        If gArgServReq <> "100" Then
            EcCodProveniencia.text = gArgServReq
            EcCodProveniencia_Validate (False)
        Else
            EcCodProveniencia.text = ""
        End If
        EcCodEFR.text = gArgEFR
        EcCodEFR_Validate (False)
        EcNumBenef.text = gArgNBenef
        EcNumBenef_Validate (False)
        If gListaMedicos = mediSim Then
            EcCodMedico.text = gArgCodMed
            EcCodMedico_Validate (False)
        End If
        EcDataPrevista_GotFocus
        EcDataPrevista_Validate (False)
        EcDataChegada_GotFocus
        EcDataChegada_Validate (False)
        
    End If
    
End Sub

Sub PreencheDadosUtente()
    
    Dim Campos(11) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "n_proc_1"
    Campos(4) = "n_proc_2"
    Campos(5) = "n_cartao_ute"
    Campos(6) = "dt_nasc_ute"
    Campos(7) = "sexo_ute"
    Campos(8) = "abrev_ute"
    Campos(9) = "n_prescricao"
    Campos(10) = "cod_isencao_ute"
    
    iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.text = ""
        Exit Sub
    End If
    
    EcUtente.text = retorno(0)
    
    CbTipoUtente.text = retorno(1)
    EcNome.text = retorno(2)
    EcProcHosp1.text = retorno(3)
    EcProcHosp2.text = retorno(4)
    EcNumCartaoUte.text = retorno(5)
    EcDataNasc.text = retorno(6)
    EcSexoUte.text = retorno(7)
    If EcSexoUte.text = gT_Masculino Then
        PicMasc.Visible = True
        PicFem.Visible = False
    End If
    If EcSexoUte.text = gT_Feminino Then
        PicMasc.Visible = False
        PicFem.Visible = True
    End If
    EcAbrevUte.text = retorno(8)
    If retorno(10) = "" And CkIsento.value = vbGrayed Then
        CkIsento.value = vbUnchecked
        EcCodIsencao.text = ""
        CbTipoIsencao.ListIndex = mediComboValorNull
    ElseIf CkIsento.value = vbGrayed Then
        CkIsento.value = vbChecked
        EcCodIsencao.text = retorno(10)
        BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao
    End If
    If (gLAB = "HOSPOR" And EcPrescricao.text = "") Then: EcPrescricao.text = retorno(9)
    
    ' PAULO COSTA 01/07/2003 - Verificar
    If (Trim(gEpisodioActivo) <> "") Then
'Hoje
        EcEpisodio.text = gEpisodioActivo
        BL_SeleccionaValorCombo CStr(gSituacaoActiva), CbSituacao
        CbSituacao.Enabled = False
        EcEpisodio.Enabled = False
    End If
    ' FIM PAULO COSTA 01/07/2003 - Verificar
           
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim And Me.ExternalAccess = True Then
    Dim contextJson As String
    Dim responseJson As String
    Dim requestJson As String
   
        contextJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & Chr(34) & "," & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & "}"
        requestJson = "{" & Chr(34) & "n_req" & Chr(34) & ":" & Chr(34) & EcNumReq.text & Chr(34) & "}"
        responseJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & ", " & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & _
                ", " & Chr(34) & "EcNome" & Chr(34) & ":" & Chr(34) & retorno(2) & Chr(34) & ", " & Chr(34) & "EcProcHosp1" & Chr(34) & ":" & Chr(34) & retorno(3) & Chr(34) & _
                ", " & Chr(34) & "EcProcHosp2" & Chr(34) & ":" & Chr(34) & retorno(4) & Chr(34) & ", " & Chr(34) & "EcNumCartaoUte" & Chr(34) & ":" & Chr(34) & retorno(5) & Chr(34) & _
                ", " & Chr(34) & "EcDataNasc" & Chr(34) & ":" & Chr(34) & retorno(6) & Chr(34) & "}"
        
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & Me.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, contextJson, IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)

    End If
    
    BtInfClinica.Enabled = True
    LaInfCli.Enabled = True
    BtRegistaMedico.Enabled = True
    BtDomicilio.Enabled = True
    If HIS.nome = cFACTURACAO_NOVAHIS And EcEpisodio <> "" And EcUtente <> "" And CbTipoUtente.ListIndex > mediComboValorNull Then
        If CbTipoUtente = BL_SelCodigo("GR_EMPR_INST", "T_UTENTE", "EMPRESA_ID", CStr(gCodLocal), "V") And EcNumReq = "" Then
            IN_RegistaAnalises EcUtente
        End If
    End If
End Sub

Sub PreencheEFRUtente()
    
    On Error GoTo ErrorHandler
    
    Dim Campos(2) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim SQLEFR As String
    Dim tabelaEFR As ADODB.recordset
    Dim SelEntUte As Boolean
    
'    If (Trim(EcCodEFR.Text) = "") Then
    
        SelEntUte = False
        If Trim(EcSeqUtente) <> "" Then
            If rs Is Nothing Then
                SelEntUte = True
            ElseIf rs.state = adStateClosed Then
                SelEntUte = True
            End If
        End If
        
        If SelEntUte = True Then
            Campos(0) = "cod_efr_ute"
            Campos(1) = "n_benef_ute"
            Campos(2) = "n_prescricao"
            iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
            
            If iret = -1 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar E.F.R. do utente!", vbCritical + vbOKOnly, App.ProductName
                Exit Sub
            End If
            
            EcCodEFR.text = retorno(0)
            EcNumBenef.text = retorno(1)
            If EcPrescricao.text = "" Then
                EcPrescricao.text = retorno(2)
            End If
        
            Set tabelaEFR = New ADODB.recordset
            tabelaEFR.CursorType = adOpenStatic
            tabelaEFR.CursorLocation = adUseServer
            
            SQLEFR = "SELECT " & _
                     "      descr_efr " & _
                     "FROM " & _
                     "      sl_efr " & _
                     "WHERE " & _
                     "      cod_efr = " & Trim(EcCodEFR.text)
            
            If gModoDebug = mediSim Then BG_LogFile_Erros SQLEFR
            tabelaEFR.Open SQLEFR, gConexao
            If tabelaEFR.RecordCount > 0 Then
                EcDescrEFR.text = tabelaEFR!descr_efr
            End If
        
            tabelaEFR.Close
            Set tabelaEFR = Nothing
        End If
        ValidarARS EcCodEFR
    
'    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : PreencheEFRUtente (FormGestaoRequisicao) -> " & Err.Description
            Exit Sub
    End Select
End Sub
Sub PreencheEFREpisodioGH(Optional episodio As String, Optional situacao As String)
    
    Dim RsCodEFR As ADODB.recordset
    Dim rsNumBenef As ADODB.recordset
    Dim HisAberto As Integer
    
    On Error GoTo ErrorHandler
    
                    
    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
                    
    If HisAberto = 1 Then
    
        If episodio <> "" And situacao <> "-1" And CbTipoUtente.ListIndex <> -1 And EcUtente <> "" Then
            ' Mapeia a situa��o do SISLAB para a GH.
            Select Case situacao
                Case gT_Urgencia
                    situacao = "Urgencias"
                Case gT_Consulta
                    situacao = "Consultas"
                Case gT_Internamento
                    situacao = "Internamentos"
                Case gT_Ficha_ID
                    situacao = "Ficha-ID"
                Case gT_Ambulatorio
                    situacao = "Ambulatorio"
                Case gT_Prescricoes
                    situacao = "Prescricoes"
                
                Case gT_Acidente
                    situacao = "Acidente"
                Case gT_Ambulatorio
                    situacao = "Ambulatorio"
                Case gT_Cons_Inter
                    situacao = "Cons-Inter"
                Case gT_Cons_Telef
                    situacao = "Cons-Telef"
                Case gT_Consumos
                    situacao = "Consumos"
                Case gT_Credenciais
                    situacao = "Credenciais"
                Case gT_Diagnosticos
                    situacao = "Diagnosticos"
                Case gT_Exame
                    situacao = "Exame"
                Case gT_Fisio
                    situacao = "Fisio"
                Case gT_HDI
                    situacao = "Hosp-Dia"
                Case gT_Intervencao
                    situacao = "Intervencao"
                Case gT_MCDT
                    situacao = "Mcdt"
                Case gT_Plano_Oper
                    situacao = "Plano-Oper"
                Case gT_Pre_Intern
                    situacao = "Pre-Intern"
                Case gT_Prog_Cirugico
                    situacao = "Prog-Cirurgico"
                Case gT_Protoc
                    situacao = "Protoc"
                Case gT_Referenciacao
                    situacao = "Referenciacao"
                Case gT_Reg_Oper
                    situacao = "Reg-Oper"
                Case gT_Tratamentos
                    situacao = "Tratamentos"
                Case Else
                    situacao = ""
            End Select
            If situacao <> "" And CbTipoUtente.ListIndex <> -1 And EcUtente <> "" Then
                If gHIS_Import = 1 And UCase(HIS.uID) = UCase("GH") Then
                    'Obter Codigo da EFR da GH
                    Set RsCodEFR = New ADODB.recordset
                    With RsCodEFR
                        .Source = "SELECT obter_cod_resp('" & CbTipoUtente.text & "','" & EcUtente.text & "','" & situacao & "','" & episodio & "') CodEFR FROM dual"
                        .CursorType = adOpenStatic
                        .CursorLocation = adUseServer
                        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                        .Open , gConnHIS
                    End With
                    If RsCodEFR.RecordCount > 0 Then
                        EcCodEFR = BL_HandleNull(RsCodEFR!codEfr, 0)
                        EcCodEFR_Validate (True)
                    Else
                        EcCodEFR = ""
                    End If
                    
                    'Obter n�mero do cart�o de beneficiario da GH
                    Set rsNumBenef = New ADODB.recordset
                    With rsNumBenef
                        .Source = "SELECT obter_cartao('" & CbTipoUtente.text & "','" & EcUtente.text & "','" & situacao & "','" & episodio & "') NumBenef FROM dual"
                        .CursorType = adOpenStatic
                        .CursorLocation = adUseServer
                        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                        .Open , gConnHIS
                    End With
                    If rsNumBenef.RecordCount > 0 Then
                        EcNumBenef = BL_HandleNull(rsNumBenef!NumBenef, 0)
                    Else
                        EcNumBenef = ""
                    End If
                Else
                    EcCodEFR = ""
                    EcNumBenef = ""
                End If
            End If
    
        End If
        BL_Fecha_conexao_HIS
    End If


Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : PreencheEFRUtente (FormGestaoRequisicao) -> " & Err.Description
            Exit Sub
    End Select
End Sub
Sub EventoUnload()
    
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    
    If Trim(EcPrinterEtiq) <> "" Then
        gImpZebra = EcPrinterEtiq
    End If
    
    ' Descarregar todos os objectos
    Set RegistosP = Nothing
    
    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then
            rs.Close
        End If
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Isencao")
    Call BL_FechaPreview("Recibo Requisi��o")
    
    Set FormGestaoRequisicao = Nothing
    
    Call BG_RollbackTransaction
    Call Apaga_DS_TM
        
'    If gF_IDENTIF = 1 Then
'        FormIdentificaUtente.Enabled = True
'        Set gFormActivo = FormIdentificaUtente
'    Else
'        Set gFormActivo = MDIFormInicio
'    End If
    
    gF_REQUIS = 0
    'Hoje
    gEpisodioActivo = ""
    gSituacaoActiva = mediComboValorNull
    gEpisodioActivoAux = ""
    gSituacaoActivaAux = mediComboValorNull

    If (gFormGesReqCons_Aberto = True) Then
        Flg_PesqUtente = False
        ' Quando este form foi invocado atrav�s de FormGesReqCons.
        gFormGesReqCons_Aberto = False
        Set gFormActivo = FormGesReqCons
        FormGesReqCons.Enabled = True
    ElseIf gF_IDENTIF = 1 Then
        FormIdentificaUtente.Enabled = True
        Set gFormActivo = FormIdentificaUtente
    ElseIf gF_TRATAREJ = 1 And gSISTEMA_FACTURACAO = cFACTURACAO_FACTUS Then
        FormFACTUSTrataRejeitados.Enabled = True
        Set gFormActivo = FormFACTUSTrataRejeitados
        gPassaParams.id = "ABRIU_REQUIS"
    ElseIf gF_FILA_ESPERA = mediSim Then
        FormFilaEspera.Enabled = True
        Set gFormActivo = FormFilaEspera
    ElseIf gF_REQPENDELECT = 1 Then
        FormReqPedElectr.Enabled = True
        Set gFormActivo = FormReqPedElectr
    Else
        Set gFormActivo = MDIFormInicio
    End If
    
'    If gF_TRATAREJ = 1 And gSISTEMA_FACTURACAO = cFACTURACAO_FACTUS Then
'        FormFACTUSTrataRejeitados.Enabled = True
'        Set gFormActivo = FormFACTUSTrataRejeitados
'        gPassaParams.ID = "ABRIU_REQUIS"
'    End If
    
End Sub

Sub SELECT_Dados_Perfil(Marcar() As String, indice As Long, CodPerfil As String, _
                        Flg_Facturado As Integer, NrReqARS As String, Optional CodFacturavel As String)

    Dim RsPerf As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim JaExiste As Boolean
    Dim flg_nao_facturar As Integer

CodPerfil = UCase(Trim(CodPerfil))
'Activo ?
sql = "SELECT descr_perfis, flg_activo,flg_nao_facturar FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
Set RsPerf = New ADODB.recordset
With RsPerf
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    .Open , gConexao
End With

Activo = "-1"
flg_nao_facturar = 0
If RsPerf.RecordCount > 0 Then
    Activo = BL_HandleNull(RsPerf!flg_activo, "0")
    descricao = BL_HandleNull(RsPerf!descr_perfis, " ")
    flg_nao_facturar = BL_HandleNull(RsPerf!flg_nao_facturar, 0)
Else
    BG_LogFile_Erros Me.Name & " - SELECT_Dados_Perfil: Erro a seleccionar dados do perfil!"
End If
RsPerf.Close
Set RsPerf = Nothing

'Membros do perfil
sql = "SELECT cod_analise, ordem FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)

Set RsPerf = New ADODB.recordset
With RsPerf
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    .Open , gConexao
End With

If RsPerf.RecordCount <> 0 Then
    If Activo = "1" Then
        LbTotalAna = LbTotalAna + 1
        BL_AcrescentaNodoPai TAna, descricao, CodPerfil, 1, 2
        'soliveira 03.12.2007 Acrescentar analise "C99999" para descri��o do perfil no ecra de resultados
        BL_Preenche_MAReq FgTubos, CodPerfil, descricao, "C99999", " ", gGHOSTMEMBER_S, " ", indice, -1, -1, CodPerfil, 0, gColocaDataChegada, "0", "-1", Flg_Facturado, CInt(indice), 0, 0, EcDataPrevista
        AdicionaQuestaoAna CodPerfil
    End If
Else
    BG_Mensagem mediMsgBox, "Perfil '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
End If

FGAna.CellBackColor = vbWhite
FGAna.CellForeColor = vbBlack
While Not RsPerf.EOF
    JaExiste = Verifica_Ana_ja_Existe(indice, BL_HandleNull(RsPerf!cod_analise, " "))

    If Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "S" Then
        If Activo = "1" Then
            SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), Flg_Facturado, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), Empty, Empty, Empty, NrReqARS, CodFacturavel
        ElseIf Activo = "0" And JaExiste = False Then
            If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                JaExiste = True
            Else
                PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                If SELECT_Dados_Simples(Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), flg_nao_facturar, Empty, Empty, Empty, Empty, Empty, Empty, NrReqARS, CodFacturavel) = False Then
                      indice = indice - 1
                End If
            End If
        End If
    ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "C" Then
        If Activo = "1" Then
            SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), Flg_Facturado, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), NrReqARS, CodFacturavel
        ElseIf Activo = "0" And JaExiste = False Then
            If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                JaExiste = True
            Else
                PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), flg_nao_facturar, , , , NrReqARS, CodFacturavel
            End If
        End If
        
    ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "P" Then
        If Activo = "1" Then
            SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), Flg_Facturado, NrReqARS, CodFacturavel
        ElseIf Activo = "0" And JaExiste = False Then
            If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                JaExiste = True
            Else
                PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), flg_nao_facturar, NrReqARS, CodFacturavel
            End If
        End If
    End If
    RsPerf.MoveNext
    
    If Not RsPerf.EOF And Activo = "0" And JaExiste = False Then
        ExecutaCodigoA = False
        indice = indice + 1
        FGAna.AddItem "", indice
        Insere_aMeio_RegistosA FGAna.row + 1
        FGAna.row = FGAna.row + 1
        FGAna.Col = 0
        ExecutaCodigoA = True
    End If
Wend

RsPerf.Close
Set RsPerf = Nothing

End Sub

Sub SELECT_Dados_Complexa(Marcar() As String, indice As Long, CodComplexa As String, Flg_Facturado As Integer, _
                         Optional Perfil As Variant, Optional DescrPerfil As Variant, _
                         Optional OrdemP As Variant, Optional NrReqARS As String, Optional CodFacturavel As String)

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String

    'Activo ?
    sql = "SELECT descr_ana_c, flg_marc_memb " & _
          "FROM sl_ana_c " & _
          "WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    Set RsCompl = New ADODB.recordset

    With RsCompl
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With

    Activo = "-1"
    If RsCompl.RecordCount > 0 Then
        Activo = BL_HandleNull(RsCompl!flg_marc_memb, "0")
        descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
    End If
    RsCompl.Close
    Set RsCompl = Nothing

    If IsMissing(Perfil) Then
        AdicionaQuestaoAna CodComplexa
    ElseIf Perfil = "" Or Perfil = "0" Then
        AdicionaQuestaoAna CodComplexa
    End If
    
    If Activo = "1" Then
        'soliveira teste
        If Not IsMissing(Perfil) And Not IsEmpty(Perfil) Then
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, Flg_Facturado, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, NrReqARS, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, Flg_Facturado, , , , CodComplexa, descricao, -1, NrReqARS, CodFacturavel
        End If
        
        'Membros da complexa
        sql = "SELECT cod_membro, ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND t_membro = 'A' ORDER BY ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsCompl.RecordCount = 0 Then
            BG_Mensagem mediMsgBox, "Complexa '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
        Else
            If Not IsMissing(Perfil) And Not IsEmpty(Perfil) Then
                BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            Else
                BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            End If
        End If
        
        While Not RsCompl.EOF
            
            If Not IsMissing(Perfil) And Not IsEmpty(Perfil) Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), Flg_Facturado, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), NrReqARS, CodFacturavel
            Else
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), Flg_Facturado, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), NrReqARS, CodFacturavel
            End If
            RsCompl.MoveNext
        Wend
    Else
        If Not IsMissing(Perfil) And Not IsEmpty(Perfil) Then
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, Flg_Facturado, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, NrReqARS, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, Flg_Facturado, , , , CodComplexa, descricao, -1, NrReqARS, CodFacturavel
        End If
    
        'Membros da complexa
        sql = "SELECT cod_membro, sl_membro.ordem FROM sl_membro,sl_ana_s " & _
            " WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND cod_ana_s = cod_membro " & _
            " AND flg_marc_auto = '1' " & _
            " AND t_membro = 'A' ORDER BY sl_membro.ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
    
        If Not RsCompl.EOF Then
            While Not RsCompl.EOF
                
                If Not IsMissing(Perfil) And Not IsEmpty(Perfil) Then
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), Flg_Facturado, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), NrReqARS, CodFacturavel
                Else
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), Flg_Facturado, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), NrReqARS, CodFacturavel
                End If
                RsCompl.MoveNext
            Wend
        End If
        RsCompl.Close
        
        sql = "SELECT cod_membro, sl_membro.ordem FROM sl_membro,sl_ana_s " & _
            " WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND cod_ana_s = cod_membro " & _
            " AND flg_opcional = '1' " & _
            " AND t_membro = 'A' ORDER BY sl_membro.ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsCompl.RecordCount > 0 And (IsMissing(Perfil) Or IsEmpty(Perfil)) Then
            GhostPerfil = "0"
            GhostComplexa = CodComplexa
            Lista_GhostMembers GhostComplexa, descricao, Flg_Facturado
        End If
        
        
    End If
    RsCompl.Close
    Set RsCompl = Nothing

End Sub

Function SELECT_Dados_Simples(Marcar() As String, indice As Long, CodSimples As String, Flg_Facturado As Integer, _
                              Optional Perfil As Variant, Optional DescrPerfil As Variant, _
                              Optional OrdemP As Variant, Optional complexa As Variant, _
                              Optional DescrComplexa As Variant, Optional OrdemC As Variant, Optional NrReqARS As String, _
                              Optional CodFacturavel As String) As Boolean

    Dim flgAtivo As Integer
    Dim RsSimpl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim codAgrup As String
    Dim descrAgrup As String
    Dim i As Integer
    Dim NaoMarcar As Boolean
    Dim flgAcrescentada As Boolean
    Dim j As Integer
    Dim k As Integer
    Dim iRec As Integer
    SELECT_Dados_Simples = True

If CodSimples = gGHOSTMEMBER_S Then
    If Not IsMissing(complexa) And Not IsEmpty(complexa) Then
        If Not IsMissing(Perfil) And Not IsEmpty(Perfil) Then
            codAgrup = Perfil
            descrAgrup = DescrPerfil
            BL_Preenche_MAReq FgTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", Flg_Facturado, CInt(indice), 0, 0, EcDataPrevista
            BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Perfil & complexa, complexa & gGHOSTMEMBER_S, 5, 6
        Else
            codAgrup = complexa
            descrAgrup = DescrComplexa
        
            BL_Preenche_MAReq FgTubos, "0", " ", complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", Flg_Facturado, CInt(indice), 0, 0, EcDataPrevista
            BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", complexa, complexa & gGHOSTMEMBER_S, 5, 6
        End If
    End If
Else
    'soliveira verificar necessidade 03-07-2006
'    NaoMarcar = True
'    For i = 1 To UBound(Marcar)
'        If InStr(1, Marcar(i), CodSimples & ";") <> 0 Then
'            NaoMarcar = False
'            Exit For
'        End If
'    Next i
'
'    If NaoMarcar = True Then Exit Function

    sql = "SELECT descr_ana_s, flg_invisivel FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(CodSimples)
    Set RsSimpl = New ADODB.recordset
    With RsSimpl
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With
    
    If RsSimpl.RecordCount > 0 Then
        descricao = BL_HandleNull(RsSimpl!descr_ana_s, " ")
        'N�o marcar a an�lise se flg_invisivel = 1
        If RsSimpl!flg_invisivel = 1 Then
            RsSimpl.Close
            Set RsSimpl = Nothing
            SELECT_Dados_Simples = False
            Exit Function
        End If
    End If
    RsSimpl.Close
    Set RsSimpl = Nothing
    
    If Not IsMissing(complexa) And Not IsEmpty(complexa) Then
         If Not IsMissing(Perfil) Then
            codAgrup = Perfil
            descrAgrup = DescrPerfil
           BL_Preenche_MAReq FgTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", Flg_Facturado, CInt(indice), 0, 0, EcDataPrevista
            BL_AcrescentaNodoFilho TAna, descricao, Perfil & complexa, complexa & CodSimples, 5, 6
         Else
            codAgrup = complexa
            descrAgrup = DescrComplexa
         
            BL_Preenche_MAReq FgTubos, "0", " ", complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", Flg_Facturado, CInt(indice), 0, 0, EcDataPrevista
            BL_AcrescentaNodoFilho TAna, descricao, complexa, complexa & CodSimples, 5, 6
         End If
    Else
         If Not IsMissing(Perfil) And Not IsEmpty(Perfil) Then
            codAgrup = Perfil
            descrAgrup = DescrPerfil
            
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodSimples, 5, 6
            BL_Preenche_MAReq FgTubos, Perfil, DescrPerfil, "0", " ", CodSimples, descricao, indice, 0, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", Flg_Facturado, CInt(indice), 0, 0, EcDataPrevista
         Else
            LbTotalAna = LbTotalAna + 1
            codAgrup = CodSimples
            descrAgrup = descricao
         
            BL_AcrescentaNodoPai TAna, descricao, CodSimples, 5, 6
            BL_Preenche_MAReq FgTubos, "0", " ", "0", " ", CodSimples, descricao, indice, 0, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", Flg_Facturado, CInt(indice), 0, 0, EcDataPrevista
         End If
    End If
    
End If

If Verifica_Ana_ja_Existe(0, codAgrup) = False Then
    If Trim(RegistosA(indice).codAna) = "" Then
        If codAgrup = CodSimples Then
            AdicionaQuestaoAna CodSimples
        End If
        
        
        If BL_VerificaInfoClinObrig(codAgrup, "T") = True Then
            lObrigaTerap = True
        End If
        If BL_VerificaInfoClinObrig(codAgrup, "I") = True Then
            lObrigaInfoCli = True
        End If
        
        flgAcrescentada = False
        For j = 1 To TotalAcrescentadas
            If Acrescentadas(j).codAna = codAgrup Then
                flgAcrescentada = True
                Exit For
            End If
        Next j
        If flgAcrescentada = False Then
        TotalAcrescentadas = TotalAcrescentadas + 1
        ReDim Preserve Acrescentadas(TotalAcrescentadas)
        Acrescentadas(TotalAcrescentadas).codAna = codAgrup
        End If
        For j = 1 To TotalEliminadas
            If Eliminadas(j).codAna = codAgrup Then
                For k = j To TotalEliminadas - 1
                    Eliminadas(k).codAna = Eliminadas(k + 1).codAna
                    Eliminadas(k).NReq = Eliminadas(k + 1).NReq
                Next k
                TotalEliminadas = TotalEliminadas - 1
                ReDim Preserve Eliminadas(TotalEliminadas)
                Exit For
            End If
        Next j
        
        RegistosA(indice).codAna = codAgrup
        RegistosA(indice).descrAna = descrAgrup
        RegistosA(indice).Estado = "-1"
        RegistosA(indice).ObsAnaReq = ""
        RegistosA(indice).seqObsAnaReq = 0
        
        If NrReqARS <> "" Then
            RegistosA(indice).NReqARS = NrReqARS
            If indice <> 1 Then
                If RegistosA(indice).NReqARS <> RegistosA(indice - 1).NReqARS Then
                    ContP1 = 1
                End If
            Else
                ContP1 = 1
            End If
        Else
            If indice <> 1 Then
                RegistosA(indice).codUnidSaude = RegistosA(indice - 1).codUnidSaude
                RegistosA(indice).MedUnidSaude = RegistosA(indice - 1).MedUnidSaude
                If Trim(RegistosA(indice - 1).NReqARS) <> "" Then
                    If Flg_IncrementaP1 = True Then
                        If IsNumeric(RegistosA(indice - 1).NReqARS) Then
                            RegistosA(indice).NReqARS = RegistosA(indice - 1).NReqARS + 1
                        End If
                    Else
                        RegistosA(indice).NReqARS = RegistosA(indice - 1).NReqARS
                    End If
                    Flg_IncrementaP1 = False
                Else
                    RegistosA(indice).NReqARS = 1
                End If
            Else
                'soliveira 23.10.2007
                RegistosA(indice).NReqARS = 1
            End If
        End If
        
        If Trim(RegistosA(indice).p1) = "" Then
            RegistosA(indice).p1 = ContP1
            ContP1 = ContP1 + 1
        End If
        
        ' ------------------------------------------------------------------------------------------
        ' PARA ANALISES QUE NAOOOOO SAO MARCADAS DENTRO DE NENHUM PERFIL DE MARCACAO
        ' ------------------------------------------------------------------------------------------
        If gPrecosAmbienteHospitalar = mediSim Then
            If CbSituacao.ListIndex <> mediComboValorNull Then
                If CbSituacao.ItemData(CbSituacao.ListIndex) <> gT_Protoc Then
                    If CodFacturavel <> "" Then
                        If RegistosA(indice).codAna <> BL_HandleNull(UCase(CodFacturavel), "0") And Mid(UCase(CodFacturavel), 1, 1) = "P" Then
                            flgAtivo = BL_HandleNull(BL_SelCodigo("SL_PERFIS", "flg_activo", "cod_perfis", UCase(CodFacturavel), "V"), 0)
                            If flgAtivo = 0 Then
        '                        ' PERFIL DE MARCACAO. COMO TAL VERIFICAR SE JA FOI MARCADO
        '                        If VerificaPerfilMarcacaoJaMarcado(BL_HandleNull(UCase(CodFacturavel), "0")) = False Then
        '                            AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", BL_HandleNull(UCase(CodFacturavel), "0"), False
        '                        End If
                            Else
                                AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(RegistosA(indice).codAna), "0"), 1, "", RegistosA(indice).codAna, False
                            End If
                        Else
                            AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", RegistosA(indice).codAna, False
                        End If
                    End If
                End If
            End If
        End If
        
        
        FGAna.TextMatrix(indice, lColFgAnaCodAna) = codAgrup
        FGAna.TextMatrix(indice, lColFgAnaDescrAna) = descrAgrup
        If FGAna.ColWidth(lColFgAnaCred) > 0 Then
            FGAna.TextMatrix(indice, lColFgAnaCred) = RegistosA(indice).NReqARS
            FGAna.TextMatrix(indice, lColFgAnaOrdem) = RegistosA(indice).p1
            FGAna.TextMatrix(indice, lColFgAnaMedSaude) = RegistosA(indice).codUnidSaude
            FGAna.TextMatrix(indice, lColFgAnaUnidSaude) = RegistosA(indice).MedUnidSaude
        End If
        iRec = DevIndice(codAgrup)
        If iRec > mediComboValorNull Then
            FGAna.TextMatrix(indice, lColFgAnaPreco) = RegistosRM(iRec).ReciboTaxa
        End If
        'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
        If gUsaEnvioPDS = mediSim Then
            Call ChangeCheckboxState(indice, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Unchecked)
        End If
        '
    End If
End If

End Function

Function SELECT_Descr_Ana(codAna As String) As String

    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    Dim Flg_SemTipo As Boolean
    
    codAna = UCase(codAna)
    If InStr(1, "SCP", Mid(codAna, 1, 1)) = 0 Then
        Flg_SemTipo = True
    End If
    
    With rsDescr
        If Flg_SemTipo = True Then
            If gLAB = "CHVNG" Then
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='S" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                                "cod_ana_c='C" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE " & _
                                "cod_perfis='P" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL)) "
            Else
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='S" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                            " UNION " & _
                                "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                                "cod_ana_c='C" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                            " UNION " & _
                                "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE " & _
                                "cod_perfis='P" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")) "
            End If
                            
        Else
            If gLAB = "CHVNG" Then
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                                "cod_ana_c='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL)) " & _
                            " UNION " & _
                                "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE " & _
                                "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )"
            
            Else
                .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE "
                .Source = .Source & "cod_ana_s ='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) "
                If gF_REQCONS < 1 Then
                    .Source = .Source & " AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")"
                End If
                .Source = .Source & ") UNION "
                .Source = .Source & "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE "
                .Source = .Source & "cod_ana_c='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) "
                If gF_REQCONS < 1 Then
                    .Source = .Source & " AND cod_ana_c in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")"
                End If
                .Source = .Source & " )UNION "
                .Source = .Source & "(SELECT descr_perfis as descricao, 'P' Tipo, inibe_marcacao FROM sl_perfis WHERE "
                .Source = .Source & "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) "
                If gF_REQCONS < 1 Then
                    .Source = .Source & " AND cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")"
                End If
                .Source = .Source & ")"
            End If

                        
        End If
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        SELECT_Descr_Ana = ""
    Else
        If BL_HandleNull(rsDescr!inibe_marcacao, "0") = "1" And gF_REQCONS < 1 Then
            SELECT_Descr_Ana = ""
        Else
            SELECT_Descr_Ana = BL_HandleNull(rsDescr!descricao, "")
            If Flg_SemTipo = True Then
                codAna = rsDescr!tipo & codAna & ""
            End If
        End If
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing

End Function

Sub ValidarARS(codEfr As String)
    
    ExecutaCodigoA = False
    
    If EFR_Verifica_ARS(codEfr) = True Or EFR_Verifica_ADSE(codEfr) = True Then
    
        FlgARS = True
        
        DefFgAna True
            
            FGAna.Col = lColFgAnaCodAna
            
            If FGAna.TextMatrix(1, lColFgAnaCodAna) <> "" Then
                For ContP1 = 1 To FGAna.rows - 2
                    If RegistosA(ContP1).NReqARS <= 0 Then
                        RegistosA(ContP1).NReqARS = 1
                        RegistosA(ContP1).p1 = ContP1
                    End If
                    FGAna.TextMatrix(ContP1, lColFgAnaCred) = RegistosA(ContP1).NReqARS
                    FGAna.TextMatrix(ContP1, lColFgAnaOrdem) = ContP1
                    FGAna.TextMatrix(ContP1, lColFgAnaMedSaude) = RegistosA(ContP1).MedUnidSaude
                    FGAna.TextMatrix(ContP1, lColFgAnaUnidSaude) = RegistosA(ContP1).codUnidSaude
                Next ContP1
            Else
                ContP1 = 1
                'soliveira 23.10.2007
                RegistosA(ContP1).NReqARS = 1
                FGAna.TextMatrix(ContP1, lColFgAnaCred) = RegistosA(ContP1).NReqARS
                FGAna.TextMatrix(ContP1, lColFgAnaOrdem) = ContP1
                FGAna.TextMatrix(ContP1, lColFgAnaMedSaude) = RegistosA(ContP1).MedUnidSaude
                FGAna.TextMatrix(ContP1, lColFgAnaUnidSaude) = RegistosA(ContP1).codUnidSaude
            End If
                
    Else
        FlgARS = False
        
        If FGAna.Cols > 2 Then
            DefFgAna False
            
            If FGAna.TextMatrix(1, lColFgAnaCodAna) <> "" Then
                For ContP1 = 1 To FGAna.rows - 2
                    RegistosA(ContP1).NReqARS = ""
                    RegistosA(ContP1).p1 = ""
                Next ContP1

                ContP1 = 1
            End If
        
        End If
    End If
    
    ExecutaCodigoA = True
    
End Sub

Function Verifica_Ana_Prod(codAgrup As String, ByRef Marcar() As String) As Boolean
    On Error GoTo TrataErro

    Dim RsAnaProd As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim k As Integer
    Dim CodProduto() As String
    Dim Resp As Integer
    Dim ListaProd() As String
    Dim JaTemProduto() As String
    Dim TotJaTemProduto As Integer
    Dim SQLAux As String
    Dim RsPerf As New ADODB.recordset
    gColocaDataChegada = ""
    codAgrup = UCase(Trim(codAgrup))
    'PERFIL Activo ?
    
    If Mid(codAgrup, 1, 1) = "P" Then
        sql = "SELECT  flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
        Set RsPerf = New ADODB.recordset
        With RsPerf
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsPerf.RecordCount > 0 Then
            RsPerf.Close
            sql = "SELECT * FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
            With RsPerf
                .Source = sql
                .CursorLocation = adUseServer
                .CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                .Open , gConexao
            End With
            If RsPerf.RecordCount > 0 Then
                While Not RsPerf.EOF
                    Verifica_Ana_Prod BL_HandleNull(RsPerf!cod_analise, ""), Marcar
                    RsPerf.MoveNext
                Wend
            End If
        End If
        RsPerf.Close
        Set RsPerf = Nothing
    
    End If

'----------- PROCURAR NOS PRODUTOS JA MARCADOS, OS PRODUTOS DA ANALISE

If gSGBD = gOracle Then
    sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
        " FROM sl_ana_s a, sl_produto p " & _
        " Where a.cod_produto = p.cod_produto (+) And " & _
        " Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
        " Union " & _
        " SELECT ac.cod_ana_c cod_ana_s, ac.descr_ana_c descr_ana_s, ac.cod_produto, p.descr_produto " & _
        " FROM sl_ana_c ac, sl_produto p " & _
        " Where ac.cod_produto = p.cod_produto (+) And " & _
        " ac.Cod_Ana_c in (select cod_analise from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup) & ")" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
        " FROM sl_ana_s a, sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto (+) AND " & _
        " cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
        " AND t_membro = 'A' )" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
        " FROM sl_ana_s a, sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto (+) AND " & _
        " cod_ana_s IN " & _
        "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
        " AND cod_analise LIKE 'S%' ) Union " & _
        " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
        " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' )) "

ElseIf gSGBD = gInformix Then
    sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a, OUTER sl_produto p" & _
        " Where a.cod_produto = p.cod_produto And Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a, OUTER sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto AND cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
        " AND t_membro = 'A' )" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a, OUTER sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto AND cod_ana_s IN " & _
        "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
        " AND cod_analise LIKE 'S%' ) Union " & _
        " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
        " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' ))"
ElseIf gSGBD = gSqlServer Then
    sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a RIGHT OUTER JOIN sl_produto p " & _
        " ON a.cod_produto = p.cod_produto WHERE Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
        " Union " & _
        "SELECT ac.cod_ana_c cod_ana_s, ac.descr_ana_c descr_ana_s, ac.cod_produto, p.descr_produto FROM sl_ana_c ac RIGHT OUTER JOIN sl_produto p " & _
        " ON ac.cod_produto = p.cod_produto WHERE ac.Cod_Ana_c in (select cod_analise from sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & ")" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a RIGHT OUTER JOIN sl_produto p " & _
        " ON a.cod_produto = p.cod_produto WHERE cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
        " AND t_membro = 'A' )" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a RIGHT OUTER JOIN sl_produto p " & _
        " ON a.cod_produto = p.cod_produto WHERE cod_ana_s IN " & _
        "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
        " AND cod_analise LIKE 'S%' ) Union " & _
        " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
        " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' ))"
End If

SQLAux = " select cod_perfis cod_ana_s, descr_perfis descr_ana_s, sl_perfis.cod_produto,descr_produto from sl_perfis, sl_produto where " & _
          " sl_perfis.cod_produto = sl_produto.cod_produto and cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    
Set RsAnaProd = New ADODB.recordset
With RsAnaProd
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    .Source = SQLAux
    .ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
    .Open
End With


Verifica_Ana_Prod = False

ReDim JaTemProduto(0)
TotJaTemProduto = 0
ReDim ListaProd(0)
ReDim Marcar(0)
k = 0
If RsAnaProd.RecordCount <= 0 Then
    SQLAux = " select cod_ana_c cod_ana_s, descr_ana_c descr_ana_s, sl_ana_c.cod_produto,descr_produto from sl_ana_c, sl_produto where " & _
              " sl_ana_c.cod_produto = sl_produto.cod_produto and cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    Set RsAnaProd = New ADODB.recordset
    With RsAnaProd
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Source = SQLAux
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
        .Open
    End With
    If RsAnaProd.RecordCount <= 0 Then
        Set RsAnaProd = New ADODB.recordset
        With RsAnaProd
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .Source = sql
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open
        End With
    End If
End If

While Not RsAnaProd.EOF
    If Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) <> "" Then
        For i = 1 To RegistosP.Count - 1
            If Trim(RegistosP(i).CodProd) = Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) Then
                Verifica_Ana_Prod = True
                gColocaDataChegada = RegistosP(i).DtChega
                Exit For
            Else
                Verifica_Ana_Prod = False
            End If
        Next i
        
        If Verifica_Ana_Prod = False Then
            k = k + 1
            ReDim Preserve ListaProd(k)
            ListaProd(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";" & BL_HandleNull(RsAnaProd!cod_produto, "") & ";" & BL_HandleNull(RsAnaProd!descr_produto, "") & Space(35 - Len(Trim(BL_HandleNull(RsAnaProd!descr_produto, "")))) & " -> " & Trim(BL_HandleNull(RsAnaProd!descr_ana_s, ""))
        Else
            TotJaTemProduto = TotJaTemProduto + 1
            ReDim Preserve JaTemProduto(TotJaTemProduto)
            JaTemProduto(TotJaTemProduto) = BL_HandleNull(RsAnaProd!cod_ana_s, "")
        End If
    End If
    
    RsAnaProd.MoveNext
Wend
    
'----------- ALGUM NAO ESTA NA LISTA

If k <> 0 Then
    
    Verifica_Ana_Prod = False
    
    'Pergunta 1: Quer marcar o produto da analise ?
        
    If Flg_PergProd = True Then
        'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
        'que se deve adoptar a ultima resposta � pergunta
            
        If Resp_PergProd = True Then
            'respondeu na ultima vez que sim � pergunta 1
            Verifica_Ana_Prod = True
                
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                k = 0
                ReDim Marcar(0)
                For i = 1 To UBound(ListaProd)
                    CodProduto = Split(ListaProd(i), ";")
                    k = k + 1
                    ReDim Preserve Marcar(k)
                    Marcar(k) = CodProduto(0) & ";" & CodProduto(1)
                Next i
                
                FormGestaoRequisicao.Enabled = False

                If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
                
            End If
        Else
            'respondeu na ultima vez que n�o � pergunta 1
            Verifica_Ana_Prod = True
        End If
    Else
        'O utilizador ainda n�o escolheu que a pergunta n�o
        'deve ser efectuada de novo, logo questiona-se a pergunta 1
        
        FormGestaoRequisicao.Enabled = False
            
        If FormPergProd.PerguntaProduto(ConfirmarDefeito, Flg_PergProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
            'respondeu sim � pergunta 1
            Resp_PergProd = True
            Verifica_Ana_Prod = True
            
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                If gRegChegaTubos <> 1 Then
                    If gIgnoraPerguntaProdutoTubo = 1 Then
                        For i = 1 To UBound(Marcar)
                            CodProduto = Split(Marcar(i), ";")
                            'RGONCALVES 15.12.2014
                            'InsereAutoProd CodProduto(1), dataAct
                            InsereAutoProd CodProduto(1), IIf(EcDataChegada.text = "", "", dataAct)
                            '
                        Next i
                        Flg_PreencherDtChega = True
                    Else
                        'O utilizador ainda n�o escolheu que a pergunta n�o
                        'deve ser efectuada de novo, logo questiona-se a pergunta 2
                        If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                            'respondeu sim � pergunta 2
                            Flg_PreencherDtChega = True
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), dataAct
                            Next i
                        Else
                            'respondeu n�o � pergunta 2
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), ""
                            Next i
                            Flg_PreencherDtChega = False
                        End If
                    End If
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
            End If
        Else
            'respondeu n�o � pergunta 1
            Resp_PergProd = False
        End If
        
    End If

    ' retirar do array de an�lises a marcar o codigo do produto (fica "S1;S2;S3;S4;")
    For i = 1 To UBound(Marcar)
        CodProduto = Split(Marcar(i))
        Marcar(i) = CodProduto(0) & ";"
    Next i
     
    '----------- PERMITIR MARCAR AS ANALISES QUE NAO TEM PRODUTO ASSOCIADO
    
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = UBound(Marcar)
    While Not RsAnaProd.EOF
        If BL_HandleNull(RsAnaProd!cod_produto, "") = "" Then
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
            If gColocaDataChegada = "" Then gColocaDataChegada = dataAct
            Verifica_Ana_Prod = True
        End If
        RsAnaProd.MoveNext
    Wend
    
    '----------- MARCAR AS ANALISES QUE J� TINHAM O PRODUTO REGISTADO
    
    k = UBound(Marcar)
    If TotJaTemProduto <> 0 Then
        For i = 1 To TotJaTemProduto
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = JaTemProduto(i) & ";"
        Next i
    End If
    
Else
    Verifica_Ana_Prod = True
    
    'Marca todas
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = 0
    ReDim Marcar(0)
    If Not RsAnaProd.EOF Then
        If gColocaDataChegada = "" And EcDataChegada.text <> "" Then    'sdo em vez de: If gColocaDataChegada = "" Then gColocaDataChegada = dataAct
            gColocaDataChegada = EcDataChegada.text                     'sdo
        ElseIf EcDataChegada.text = "" Then                             'sdo
            gColocaDataChegada = ""                                     'sdo
        End If                                                          'sdo
    End If
    While Not RsAnaProd.EOF
        k = k + 1
        ReDim Preserve Marcar(k)
        Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
        RsAnaProd.MoveNext
    Wend

End If

RsAnaProd.Close
Set RsAnaProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_Prod"
    Verifica_Ana_Prod = False
    Exit Function
    Resume Next
End Function




Private Sub BtActualizaEpisodio_Click()
    
    On Error GoTo ErrorHandler:
    
    Dim processo As String
    Dim nome As String
    Dim dt_nasc As String
    Dim rv As Integer
    
    If (ActualizaEpisodio = False) Then
        
        ' Activa a sele�ao de episodio.
        If (Trim(EcUtente.text) = "") Then
            MsgBox "Deve selecionar um Utente.       ", vbInformation, " Actualizar Epis�dio"
            Exit Sub
        Else
            If (CbSituacao.ListIndex = mediComboValorNull) Then
                MsgBox "Deve indicar uma situa�ao.       ", vbInformation, " Actualizar Epis�dio"
                Exit Sub
            End If
        End If
        
        EcEpisodioAux.text = EcEpisodio.text
        EcEpisodio.Visible = True
        EcEpisodio.Enabled = True

        EcEpisodioAux.Visible = True
        EcEpisodioAux.Enabled = True
        
        EcEpisodioAux.SelStart = Len(EcEpisodioAux.text)
        EcEpisodioAux.SetFocus
        
        ActualizaEpisodio = True
    Else
        
        If (Trim(EcUtente.text) = "") Then
            MsgBox "Deve selecionar um Utente.       ", vbInformation, " Actualizar Epis�dio"
            Exit Sub
        Else
            ActualizaProcessoUtente
            ActualizaEpisodio = False
            EcEpisodioAux.text = ""
            EcEpisodioAux.Visible = False
        End If
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BtActualizaEpisodio_Click (FormGestaoRequisicao) -> " & Err.Description)
            Exit Sub
    End Select
    Exit Sub
    Resume Next
End Sub

Private Sub BtAgenda_Click()

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
                
    FormGestaoRequisicao.Enabled = False
    Set gFormActivo = FormAgenda
    FormAgenda.Show
    If (EcDataPrevista.text = Empty) Then
        Call FormAgenda.PreencheAgenda(Bg_DaData_ADO, "1")
    Else
        Call FormAgenda.PreencheAgenda(EcDataPrevista.text, "1")
    End If
End Sub

Private Sub BtAnexos_Click()
    If EcSeqUtente <> "" And EcNumReq <> "" Then
        FormCommonDialog.Show
        FormCommonDialog.EcReq = EcNumReq
        FormCommonDialog.EcSeqUtente = EcSeqUtente
        FormCommonDialog.FuncaoProcurar
        Set gFormActivo = FormCommonDialog
    End If
End Sub

Private Sub BtCancelarReq_Click()

    Max = UBound(gFieldObjectProperties)
    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    
    MarcaLocal = rs.Bookmark
    FormGestaoRequisicao.Enabled = False
    FormCancelarRequisicoes.Show

End Sub

Private Sub BtConfirm_Click()
    If EcNumReq <> "" And EcUtilConf = "" And EcDtConf = "" And EcHrConf = "" Then
        EcUtilConf = gCodUtilizador
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcDtConf = dataAct
        EcHrConf = Bg_DaHora_ADO
        BD_Update
    End If
End Sub

Private Sub BtDadosAnteriores_Click()

    Dim EArs As Boolean
    Dim i As Integer

    ' N�o deixa passar por cima de uma req. existente.
    If (Len(Trim(EcNumReq.text)) > 0) Then
        MsgBox "Opera��o n�o permitida!" & vbCrLf & _
               "Deve limpar os dados do ecran.            ", vbExclamation, "Copiar anterior"
        Exit Sub
    End If
    
    If Not (rs Is Nothing) Then
        If rs.state = adStateOpen Then
            If rs.EOF = True And rs.BOF = True Then
                'ficou com um registo vazio ??
                rs.Close
            Else
                BG_Mensagem mediMsgStatus, "Tem que limpar os dados do ecran primeiro!", , "Copiar anterior"
                Exit Sub
            End If
        End If
    End If

Dim TipoUtente As Long
Dim NumUtente As String

TipoUtente = CbTipoUtente.ListIndex
NumUtente = EcUtente.text

LimpaCampos2

Flg_DadosAnteriores = True

EcNumReq = gRequisicaoActiva

Call FuncaoProcurar

rs.Close
Set rs = Nothing

EcDataPrevista.Enabled = True
If gLAB = "CHVNG" Or gLAB = "HCVP" Or gInibeDataChegada = mediSim Then
    EcDataChegada.Enabled = False
Else
    EcDataChegada.Enabled = True
End If
BtCancelarReq.Visible = False
'EcNumReq.Enabled = True
EcNumReq.locked = False
EcEpisodio.Enabled = True
CbTipoUtente.Enabled = False
EcUtente.Enabled = False
EcDataPrevista.Enabled = True
gFlg_JaExisteReq = False
Flg_LimpaCampos = True

EcNumReq = ""
EcDescrEstado = ""
EcEstadoReq = gEstadoReqSemAnalises

If gCopiarAnteriorUtente = 1 Then                 'sdo
    
    LimpaFGAna
    FGAna.Clear
    FGAna.rows = 2
    TAna.Nodes.Clear
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
    FgProd.Clear
    FgProd.rows = 2
    FgTubos.Clear
    FgTubos.rows = 2
    LimpaFgProd
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    TB_LimpaFgTubos FgTubos
    EcDataFact.text = ""
    EcObsProveniencia.text = ""
    If gMarcacoesPrevias = 1 Then
        'EcDataChegada.text = dataAct
    Else
        EcDataChegada.text = ""
    End If
'    EcCodMedico.Text = ""
'    EcNomeMedico.Text = ""
    SSTGestReq.Tab = 2

     
    
Else
    For i = 1 To RegistosA.Count
        If RegistosA(i).codAna <> "" Then
            TotalAcrescentadas = TotalAcrescentadas + 1
            ReDim Preserve Acrescentadas(TotalAcrescentadas)
            Acrescentadas(TotalAcrescentadas).codAna = RegistosA(i).codAna
        End If
    Next
    EcDataPrevista = ""
    EcReqAssociada = ""
    EcDataPrevista = dataAct                     '**********************
    CbTipoUtente.ListIndex = TipoUtente                '**********************
    EcUtente.text = NumUtente
    EcUtente_Validate False
End If
EcDataAlteracao = ""
EcDataCriacao = ""
EcDataImpressao = ""
EcDataImpressao2 = ""
EcUtilizadorAlteracao = ""
EcUtilizadorCriacao = ""
EcUtilizadorImpressao = ""
EcUtilizadorImpressao2 = ""
LbNomeUtilAlteracao.caption = ""
LbNomeUtilCriacao.caption = ""
LbNomeUtilImpressao.caption = ""
LbNomeUtilImpressao2.caption = ""
LbNomeUtilAlteracao.ToolTipText = ""
LbNomeUtilCriacao.ToolTipText = ""
LbNomeUtilImpressao.ToolTipText = ""
LbNomeUtilImpressao2.ToolTipText = ""
LbNomeUtilFecho.caption = ""
LbNomeUtilFecho.ToolTipText = ""
LbNomeLocal.caption = ""
LbNomeLocal.ToolTipText = ""
EcHoraAlteracao = ""
EcHoraCriacao = ""
EcHoraImpressao = ""
EcHoraImpressao2 = ""

If CkIsento.value <> vbChecked Then
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabVisible(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabVisible(4) = False
End If

'EcDataPrevista = dataAct     '******************
ContP1 = 1
CbTMIndex = -1
CbUteIndex = -1
CbEntIndex = -1

EArs = EFR_Verifica_ARS(EcCodEFR)

If EArs = True Then
    For i = 1 To RegistosA.Count
        If RegistosA(i).codAna <> "" Then
'            If gLAB = "BIO" Then
'                RegistosA(i).NReqARS = ""
'            Else
                RegistosA(i).NReqARS = "A gerar"
'            End If
            RegistosA(i).p1 = ContP1
            FGAna.TextMatrix(i, lColFgAnaCred) = RegistosA(i).NReqARS
            FGAna.TextMatrix(i, lColFgAnaOrdem) = RegistosA(i).p1
            ContP1 = ContP1 + 1
        End If
    Next i
End If

For i = LBound(MaReq) To UBound(MaReq)
    With MaReq(i)
        .Flg_Apar_Transf = "0"
        .N_Folha_Trab = 0
        .Flg_Facturado = 0
    End With
Next

'CbTipoUtente.ListIndex = TipoUtente                '**********************
'EcUtente.Text = NumUtente
'EcUtente_Validate False

If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NaoCopiarEFR") = "1" Then
    EcCodEFR.text = ""
    EcCodEFR_Validate False
End If

Estado = 1
BL_ToolbarEstadoN Estado

Flg_DadosAnteriores = False

'Hoje
'EcEpisodio.Text = ""

'    If (gEpisodioActivoAux <> "") Then
'        EcEpisodio.Text = gEpisodioActivoAux

'comentei linha seguinte: episodio ficava vazio qdo se copia anterior
         'EcEpisodio.Text = gEpisodioActivo
         
'    Else
'        EcEpisodio.Text = ""
'    End If

End Sub

Private Sub BtEtiq_Click()
    Call ImprimeEtiqNovo
End Sub

Private Sub BtGarrafaVoltar_Click()
    FrameGarrafa.Enabled = False
    FrameGarrafa.Visible = False
    SSTGestReq.Enabled = True
    SSTGestReq.Tab = 2
    FGAna.SetFocus
    
End Sub

Private Sub BtGetSONHO_Click()
    
    Me.FuncaoLimpar
    EcEpisodioSONHO.text = ""
    
'    Me.OpConsSonho.Value = False
'    Me.OpExternoSONHO.Value = False
'    Me.OpIntSONHO.Value = False
'    Me.OpUrgSONHO.Value = False
    
    Me.MonthViewAnalisesSONHO.value = Date
    
    FrameSONHO.Enabled = True
    FrameSONHO.top = 1450
    FrameSONHO.left = 2880
    FrameSONHO.Visible = True
    EcEpisodioSONHO.SetFocus
    DoEvents

End Sub

Private Sub BtGetSONHO_OK_Click()

    Dim rv As Integer
    Dim situacao As String
    
    EcEpisodioSONHO.text = Trim(EcEpisodioSONHO.text)

    If (Len(EcEpisodioSONHO.text) > 0) Then
        
        FrameSONHO.Visible = False
            
        situacao = ""
        
        If (Me.OpExternoSONHO) Then
            situacao = gT_Externo
        End If
        
        If (Me.OpIntSONHO) Then
            situacao = gT_Internamento
        End If
        
        If (Me.OpConsSonho) Then
            situacao = gT_Consulta
        End If
        
        If (Me.OpUrgSONHO) Then
            situacao = gT_Urgencia
        End If
        
        rv = SONHO_Constroi_Requisicao(Me, _
                                       "", _
                                       situacao, _
                                       EcEpisodioSONHO.text, _
                                       Format(MonthViewAnalisesSONHO.value, gFormatoData))
    
        If (rv <> 1) Then
            FrameSONHO.Enabled = True
            FrameSONHO.top = 1450
            FrameSONHO.left = 2880
            FrameSONHO.Visible = True
            EcEpisodioSONHO.SetFocus
            DoEvents
        End If
    
    End If

End Sub

Private Sub BtInfClinica_Click()

    'for�ar a fazer o procurar da inf. clinica para a requisi��o em causa
    'caso j� exista uma
    
    BL_PreencheDadosUtente EcSeqUtente
    FormGestaoRequisicao.Enabled = False
    FormInformacaoClinica.Show
    'If EcNumReq.Text <> "" Or EcSeqUtente.Text <> "" Then
        FormInformacaoClinica.FuncaoProcurar
    'End If
    FormInformacaoClinica.EcInfCompl = EcinfComplementar

End Sub

Private Sub BtObsEspecif_Click()

    EcObsEspecif = RegistosP(LastRowP).ObsEspecif
    FrameObsEspecif.Visible = True
    EcObsEspecif.SetFocus
End Sub

Private Sub BtOkObsEspecif_Click()
    EcObsEspecif = UCase(EcObsEspecif)
    RegistosP(LastRowP).ObsEspecif = Trim(EcObsEspecif)
    FgProd.SetFocus
    EcObsEspecif = ""
    FrameObsEspecif.Visible = False
End Sub

Private Sub BtPesquisaAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    FGAna.Col = lColFgAnaCodAna
    FGAna.row = FGAna.rows - 1
    If Button = 1 Then
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = lColFgAnaCodAna Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            'MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAREQ
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAMARC
        End If
    End If

End Sub

Private Sub BtPesquisaArquivo_Click()
    FormPesquisaRapida.InitPesquisaRapida "sl_arquivos", _
                        "cod_arquivo", "seq_arquivo", _
                        EcPesqRapArquivo
End Sub

Private Sub BtPesquisaColheita_Click()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "nome"
    CamposEcran(1) = "nome"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_utilizador"
    CamposEcran(2) = "cod_utilizador"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_idutilizador"
    CampoPesquisa = "nome"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Utilizadores")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodUserColheita.text = Resultados(2)
            EcDescrUserColheita.text = Resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Utilizadores codificados", vbExclamation, "EFR"
        EcCodUserColheita.SetFocus
    End If

End Sub

Private Sub BtPesquisaEntFin_Click()
    Dim i As Integer
    Dim iRec As Integer
    
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
    ValidarARS EcCodEFR
    If gPrecosAmbienteHospitalar = mediSim Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboNumDoc = "0" Or RegistosRM(i).ReciboNumDoc = "" Then
                AlteraEntidadeReciboManual CLng(i), EcCodEFR, EcDescrEFR, "", -1
            Else
                'BG_Mensagem mediMsgBox, "N�o � possivel alterar a entidade das an�lises associadas ao recibo emitido.", vbInformation, "Altera��o entidade"
            End If
        Next
        For i = 1 To RegistosA.Count - 1
            iRec = DevIndice(RegistosA(i).codAna)
            If iRec = -1 Then
                AdicionaReciboManual 0, EcCodEFR, "0", "0", RegistosA(i).codAna, 1, "", RegistosA(i).codAna, False
            End If
        Next
        ActualizaLinhasRecibos -1
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  BtPesquisaEntFin_Click: " & Err.Description, Me.Name, "BtPesquisaEntFin_Click", False
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaEspecif_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "sl_especif.descr_especif"
    CamposEcran(1) = "sl_especif.descr_especif"
    Tamanhos(1) = 3000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "sl_produto_especif.cod_especif"
    CamposEcran(2) = "sl_produto_especif.cod_especif"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = " sl_produto_especif, sl_especif "
    CWhere = " sl_produto_especif.cod_produto='" & UCase(FgProd.TextMatrix(FgProd.row, 0)) & "' AND sl_produto_especif.cod_especif=sl_especif.cod_especif"
    CampoPesquisa = "sl_especif.descr_especif"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Especifica��es")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FgProd.TextMatrix(FgProd.row, 3) = Resultados(1)
            FgProd.TextMatrix(FgProd.row, 2) = Resultados(2)
            RegistosP(FgProd.row).CodEspecif = Resultados(2)
            RegistosP(FgProd.row).DescrEspecif = Resultados(1)
            'FgProd.Row = 4
        End If
    Else
        Set CamposRetorno = Nothing
        Set CamposRetorno = New ClassPesqResultados
    
        PesqRapida = False

        ChavesPesq(1) = "descr_especif"
        CamposEcran(1) = "descr_especif"
        Tamanhos(1) = 3000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_especif"
        CamposEcran(2) = "cod_especif"
        Tamanhos(2) = 1000
        Headers(2) = "C�digo"
        
        CamposRetorno.InicializaResultados 2
        
        CFrom = " sl_especif "
        CWhere = ""
        CampoPesquisa = "descr_especif"
        
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Especifica��es")
        
        If PesqRapida = True Then
            FormPesqRapidaAvancada.Show vbModal
            CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
            If Not CancelouPesquisa Then
                FgProd.TextMatrix(FgProd.row, 3) = Resultados(1)
                FgProd.TextMatrix(FgProd.row, 2) = Resultados(2)
                RegistosP(LastRowP).CodEspecif = UCase(Resultados(2))
                FgProd.Col = 4
            End If
        Else
            BG_Mensagem mediMsgBox, "N�o existem especifica��es codificadas!", vbExclamation, "Especifica��es"
            FgProd.Col = 2
        End If
    End If
    FgProd.SetFocus
    
End Sub

Private Sub BtPesquisaMedico_Click()
    On Error GoTo TrataErro


    Dim ChavesPesq(1 To 3) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 3) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 3) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 3) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 3) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    
    ChavesPesq(3) = "ext_telef"
    CamposEcran(3) = "ext_telef"
    Tamanhos(3) = 0
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    Headers(3) = "Extens�o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 3

    'Query
    ClausulaFrom = "sl_medicos"
    CampoPesquisa1 = "nome_med"
    ClausulaWhere = " (flg_invisivel is null or flg_invisivel = 0) "
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar M�dicos")
    
    mensagem = "N�o foi encontrado nenhum M�dico."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMedico.text = Resultados(1)
            EcNomeMedico.text = Resultados(2)
            EcObsProveniencia.text = Resultados(3)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaMedico_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaMedico_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtPesquisaMedico2_Click()
    FormPesquisaRapida.InitPesquisaRapida "sl_responsaveis", _
                        "titulo", "codigo", _
                        EcPesqRapMedico2
End Sub
Private Sub BtPesquisaProduto_click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
                        "descr_produto", "seq_produto", _
                         EcPesqRapProduto
End Sub


Private Sub BtPesquisaProveniencia_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    ClausulaWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) AND cod_proven IN (SELECT x.cod_proven FROM sl_ass_proven_locais x WHERE x.cod_local = " & BL_HandleNull(EcLocal, CStr(gCodLocal)) & ") "

        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.text = Resultados(1)
            EcDescrProveniencia.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub BtPesquisaUtente_Click()
    
    gEpisodioActivo = ""
    gEpisodioActivoAux = ""
    gSituacaoActiva = mediComboValorNull
    gReqAssocActiva = ""
    Flg_PesqUtente = True
    FormPesquisaUtentes.Show
    FormGestaoRequisicao.Enabled = False

End Sub

Sub Funcao_CopiaUte()
    
    If Trim(gDUtente.seq_utente) <> "" Then
        EcSeqUtente = CLng(gDUtente.seq_utente)
        PreencheDadosUtente
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

End Sub

Sub Actualiza_Estrutura_MAReq(inicio As Integer)

    Dim j As Long

    If inicio <> UBound(MaReq) Then
        For j = inicio To UBound(MaReq) - 1
            MaReq(j).Cod_Perfil = MaReq(j + 1).Cod_Perfil
            MaReq(j).Descr_Perfil = MaReq(j + 1).Descr_Perfil
            MaReq(j).cod_ana_c = MaReq(j + 1).cod_ana_c
            MaReq(j).Descr_Ana_C = MaReq(j + 1).Descr_Ana_C
            MaReq(j).cod_ana_s = MaReq(j + 1).cod_ana_s
            MaReq(j).descr_ana_s = MaReq(j + 1).descr_ana_s
            MaReq(j).Ord_Ana = MaReq(j + 1).Ord_Ana
            MaReq(j).Ord_Ana_Compl = MaReq(j + 1).Ord_Ana_Compl
            MaReq(j).Ord_Ana_Perf = MaReq(j + 1).Ord_Ana_Perf
            MaReq(j).cod_agrup = MaReq(j + 1).cod_agrup
            MaReq(j).N_Folha_Trab = MaReq(j + 1).N_Folha_Trab
            MaReq(j).dt_chega = MaReq(j + 1).dt_chega
            MaReq(j).Flg_Apar_Transf = MaReq(j + 1).Flg_Apar_Transf
            MaReq(j).flg_estado = MaReq(j + 1).flg_estado
            MaReq(j).Flg_Facturado = MaReq(j + 1).Flg_Facturado
            MaReq(j).Ord_Marca = MaReq(j + 1).Ord_Marca
            MaReq(j).seq_req_tubo = MaReq(j + 1).seq_req_tubo
            MaReq(j).indice_tubo = MaReq(j + 1).indice_tubo
        Next
    End If
    
    ReDim Preserve MaReq(UBound(MaReq) - 1)

End Sub

Sub Insere_aMeio_RegistosA(inicio As Integer)

    Dim j As Long

    CriaNovaClasse RegistosA, -1, "ANA"
    
    If inicio <> RegistosA.Count Then
        For j = RegistosA.Count To inicio + 1 Step -1
            RegistosA(j).codAna = RegistosA(j - 1).codAna
            RegistosA(j).descrAna = RegistosA(j - 1).descrAna
            RegistosA(j).NReqARS = RegistosA(j - 1).NReqARS
            RegistosA(j).p1 = RegistosA(j - 1).p1
            RegistosA(j).codUnidSaude = RegistosA(j - 1).codUnidSaude
            RegistosA(j).MedUnidSaude = RegistosA(j - 1).MedUnidSaude
            RegistosA(j).Estado = RegistosA(j - 1).Estado
            RegistosA(j).DtChega = RegistosA(j - 1).DtChega
            RegistosA(j).ObsAnaReq = RegistosA(j - 1).ObsAnaReq
            RegistosA(j).seqObsAnaReq = RegistosA(j - 1).seqObsAnaReq
        Next
    End If

    RegistosA(inicio).codAna = ""
    RegistosA(inicio).descrAna = ""
    RegistosA(inicio).NReqARS = ""
    RegistosA(inicio).p1 = ""
    RegistosA(inicio).codUnidSaude = ""
    RegistosA(inicio).MedUnidSaude = ""
    RegistosA(inicio).Estado = ""
    RegistosA(inicio).DtChega = ""
    RegistosA(inicio).ObsAnaReq = ""
    RegistosA(inicio).seqObsAnaReq = ""
    
End Sub

Function Calcula_Valor(valor As Double, TaxaConv) As Double
    
    Calcula_Valor = Round(valor / TaxaConv)

End Function

Private Sub BtPesquisaValencia_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_valencia"
    CamposEcran(1) = "cod_valencia"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_valencia"
    CamposEcran(2) = "descr_valencia"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_valencia"
    CampoPesquisa1 = "descr_valencia"
    ClausulaWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) "
    If gCodLocal <> -1 Then
        ClausulaWhere = ClausulaWhere & " AND  cod_valencia IN (SELECT x.cod_valencia FROM sl_ass_valencia_locais x WHERE x.cod_local = " & BL_HandleNull(EcLocal, CStr(gCodLocal)) & ") "
    End If
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Val�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Val�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodValencia.text = Resultados(1)
            EcDescrValencia.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub


Private Sub BtGarrafaOk_Click()
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    If Len(EcGarrafa) <> 0 Then
        FrameGarrafa.Enabled = False
        FrameGarrafa.Visible = False
        SSTGestReq.Enabled = True
        AlteraGarrafa FGAna.TextMatrix(FGAna.row - 1, lColFgAnaCodAna)
        FGAna.SetFocus
    End If
End Sub

Private Sub AlteraGarrafa(codAnalise As String)
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim i As Integer
    
    If Mid(codAnalise, 1, 1) = "P" Then

        sSql = "SELECT cod_tubo FROM sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codAnalise)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            For i = 1 To FgTubos.rows - 1
                If FgTubos.TextMatrix(i, 0) = BL_HandleNull(rsAna!cod_tubo, "") Then
                    FgTubos.TextMatrix(i, 2) = EcGarrafa
                    gEstruturaTubos(i).Garrafa = EcGarrafa
                End If
            Next
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
End Sub





Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbSituacao, KeyCode

End Sub




Private Sub CbTipoIsencao_Click()
    
    BL_ColocaComboTexto "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao

End Sub

Private Sub CbTipoIsencao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoIsencao, KeyCode

End Sub

Private Sub CbTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbTipoUtente, KeyCode

End Sub

Public Sub CbTipoUtente_Validate(cancel As Boolean)
    
    Call EcUtente_Validate(cancel)
    
End Sub

Private Sub CbUrgencia_Click()
'    EcUrgencia.Text = Mid(CbUrgencia.Text, 1, 1)
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUrgencia, KeyCode

End Sub

Private Sub CkIsento_Click()

    If CkIsento.value = vbUnchecked Then
        CbTipoIsencao.ListIndex = mediComboValorNull
        EcCodIsencao = ""
        CbTipoIsencao.Enabled = False
        SSTGestReq.TabEnabled(3) = True
        SSTGestReq.TabVisible(3) = False
        SSTGestReq.TabEnabled(4) = True
    ElseIf CkIsento.value = vbChecked Then
        SSTGestReq.TabEnabled(3) = False
        SSTGestReq.TabVisible(3) = False
        SSTGestReq.TabEnabled(4) = False
        CbTipoIsencao.Enabled = True
        CbTipoIsencao.SetFocus
        If gF_IDENTIF = 1 Then
            If Trim(FormIdentificaUtente.EcDescrIsencao.text) <> "" Then
                CbTipoIsencao.text = FormIdentificaUtente.EcDescrIsencao.text
            End If
        End If
        BG_MostraComboSel gCodIsencaoDefeito, CbTipoIsencao
    End If

End Sub

Private Sub cmdOK_Click()
    
    FuncaoProcurar

End Sub


Private Sub Command3_Click()

    FrameSONHO.Visible = False
    FrameSONHO.Enabled = False

End Sub





Private Sub EcAuxAna_Change()
    
    'If Len(EcAuxAna) = 5 And ExecutaCodigoA = True Then Call EcAuxAna_KeyDown(13, 0)

End Sub

Private Sub EcAuxAna_GotFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If EcAuxAna.SelLength = 0 Then
        Sendkeys ("{END}")
    End If

End Sub

Private Sub EcAuxAna_LostFocus()

    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    EcAuxAna.Visible = False
    If FGAna.Enabled = True Then FGAna.SetFocus
    
End Sub

Private Sub EcAuxAna_KeyPress(KeyAscii As Integer)
    
    If Enter = True Then KeyAscii = 0

End Sub
Public Sub EcAuxAna_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iRec As Integer
    Dim cod_Agrup_aux As String
    Dim d As String
    Dim i As Integer
    Dim aux As String
    Dim an_aux As String
    Dim rv As Integer
    ' For�a o formato "0".
    aux = Trim(EcAuxAna.text)
'    If (IsNumeric(aux)) Then
'        If (CLng(aux) = 0) Then
'            EcAuxAna.text = "0"
'        End If
'    End If
    
    Enter = False
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxAna.text) = "") Then
        Enter = True
        EcAuxAna.Visible = False
        FGAna.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        
        ' -------------------------------------------------------
        
        If (gMapeiaSONHO) Then
        
            ' Mapeia do SONHO para o SISLAB.
        
            rv = SONHO_Mapeia_Analise_para_SISLAB(UCase(Trim(EcAuxAna.text)), an_aux)
            EcAuxAna.text = an_aux
            DoEvents
            
        End If
            
        ' -------------------------------------------------------
        
        Select Case LastColA
            Case 0 ' Codigo da An�lise
                If Verifica_Ana_ja_Existe(LastRowA, EcAuxAna) = False Then
                    If Trim(UCase(EcAuxAna)) <> Trim(UCase(RegistosA(LastRowA).codAna)) Then
                        
                        If Insere_Nova_Analise(LastRowA, EcAuxAna, "") = True Then
                            cod_Agrup_aux = EcAuxAna
                            d = SELECT_Descr_Ana(cod_Agrup_aux)
                            If LastRowA < FGAna.rows Then
                                VerificaAnaliseJaMarcada EcNumReq, cod_Agrup_aux
                            End If
                            
                            'o estado deixa de estar "Sem Analises"
                            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
                                EcEstadoReq = gEstadoReqEsperaProduto
                                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
                            End If

                            If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                                'Cria linha vazia
                                FGAna.AddItem ""
                                CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                            End If
                         
                            If IdentificaGarrafa() = True Then
                                LbGarrafa.caption = "Introduza a identifica��o da garrafa de hemocultura."
                                EcGarrafa = ""
                                FrameGarrafa.Enabled = True
                                FrameGarrafa.Visible = True
                                EcGarrafa.SetFocus
                                SSTGestReq.Enabled = False
                            End If
                        
                        End If
                    End If
                Else
                    Beep
                    
                    BG_Mensagem mediMsgStatus, "An�lise j� indicada!"
                End If
                FGAna.Col = lColFgAnaCodAna
            
            Case 2 ' Req. ARS
                
                If FGAna.TextMatrix(LastRowA, lColFgAnaCred) <> EcAuxAna Then
                
                    If LastRowA <> 1 Then
                        If FGAna.TextMatrix(LastRowA - 1, lColFgAnaCred) <> EcAuxAna Then
                            ContP1 = 1
                        Else
                            ContP1 = FGAna.TextMatrix(LastRowA - 1, lColFgAnaOrdem) + 1
                        End If
                    Else
                        ContP1 = 1
                    End If
                    If Len(EcAuxAna) <> 19 Then
                        If BG_Mensagem(mediMsgBox, "O identificador da credencial n�o tem 19 d�gitos. Deseja Continuar?", vbYesNo + vbQuestion, "N�mero de requisi��o da ARS") <> vbYes Then
                            EcAuxAna.Visible = False
                            FGAna.SetFocus
                            Exit Sub
                        End If
                    End If
                    RegistosA(LastRowA).NReqARS = EcAuxAna
                    RegistosA(LastRowA).p1 = ContP1
                    RegistosA(LastRowA).codUnidSaude = ""
                    RegistosA(LastRowA).MedUnidSaude = ""
                    FGAna.TextMatrix(LastRowA, lColFgAnaCred) = EcAuxAna
                    FGAna.TextMatrix(LastRowA, lColFgAnaOrdem) = ContP1
                    FGAna.TextMatrix(LastRowA, lColFgAnaUnidSaude) = RegistosA(LastRowA).codUnidSaude
                    FGAna.TextMatrix(LastRowA, lColFgAnaMedSaude) = RegistosA(LastRowA).MedUnidSaude
                    If gPrecosAmbienteHospitalar = mediSim Then
                        iRec = DevIndice(RegistosA(LastRowA).codAna)
                        If iRec > mediComboValorNull Then
                            RegistosRM(iRec).ReciboP1 = RegistosA(LastRowA).NReqARS
                        End If
                    End If
                    ContP1 = ContP1 + 1
                   
                    If LastRowA < FGAna.rows - 2 And FGAna.TextMatrix(LastRowA - 1, lColFgAnaCred) <> EcAuxAna Then
                        If BG_Mensagem(mediMsgBox, "Deseja actualizar at� ao fim da lista ?", vbYesNo + vbQuestion, "N�mero de requisi��o da ARS") = vbYes Then
                            
                            For i = LastRowA + 1 To FGAna.rows - 2
                                RegistosA(i).NReqARS = EcAuxAna
                                RegistosA(i).p1 = ContP1
                                RegistosA(i).codUnidSaude = ""
                                RegistosA(i).MedUnidSaude = ""
                                FGAna.TextMatrix(i, lColFgAnaCred) = EcAuxAna
                                FGAna.TextMatrix(i, lColFgAnaOrdem) = ContP1
                                FGAna.TextMatrix(i, lColFgAnaUnidSaude) = RegistosA(i).codUnidSaude
                                FGAna.TextMatrix(i, lColFgAnaMedSaude) = RegistosA(i).MedUnidSaude
                                
                                If gPrecosAmbienteHospitalar = mediSim Then
                                    iRec = DevIndice(RegistosA(i).codAna)
                                    If iRec > mediComboValorNull Then
                                        RegistosRM(iRec).ReciboP1 = RegistosA(i).NReqARS
                                    End If
                                End If
                                ContP1 = ContP1 + 1
                            Next i
                        
                        End If
                    End If
                
                    'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
                    If gUsaEnvioPDS = mediSim Then
                        AtualizaColEnvioPDS
                    End If
                    '
                    
                End If
                
                If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                    'Cria linha vazia
                    FGAna.AddItem ""
                    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                End If
                FGAna.Col = lColFgAnaCred
            Case 3
                If FGAna.TextMatrix(LastRowA, lColFgAnaOrdem) <> EcAuxAna Then
                    RegistosA(LastRowA).p1 = EcAuxAna
                    FGAna.TextMatrix(LastRowA, lColFgAnaOrdem) = EcAuxAna
                    ContP1 = EcAuxAna
                End If
                If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                    'Cria linha vazia
                    FGAna.AddItem ""
                    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                End If
                FGAna.Col = lColFgAnaCred
        End Select
        
        EcAuxAna.Visible = False
        If FGAna.row <= FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
            FGAna.row = FGAna.row + 1
        Else
            Fgana_RowColChange
        End If
        
        If FGAna.Enabled = True And FGAna.Visible = True Then
            FGAna.SetFocus
        Else
            If EcGarrafa.Visible = True Then
                EcGarrafa.SetFocus
            End If
        End If
        
    End If
    
End Sub

Private Sub EcAuxProd_GotFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If LastColP = 5 Then
        EcAuxProd.Tag = adDate
    Else
        EcAuxProd.Tag = ""
    End If
    
    If EcAuxProd.SelLength = 0 Then
        Sendkeys ("{END}")
    End If

End Sub



Public Sub EcAuxProd_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Enter = False
    
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxProd.text) = "") Then
        Enter = True
        EcAuxProd.Visible = False
        If FgProd.Enabled = True Then FgProd.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        Select Case LastColP
            Case 0 ' Codigo do produto
                If Verifica_Prod_ja_Existe(LastRowP, EcAuxProd.text) = False Then
                    If SELECT_Descr_Prod = True Then
                        If Trim(EcDataPrevista.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 5) = Trim(EcDataPrevista.text)
                            RegistosP(LastRowP).DtPrev = Trim(EcDataPrevista.text)
                        End If
                        If Trim(EcDataChegada.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 6) = Trim(EcDataChegada.text)
                            RegistosP(LastRowP).DtChega = Trim(EcDataChegada.text)
                        End If

                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).CodProd = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        If FgProd.row = FgProd.rows - 1 Then
                            'Cria linha vazia
                            FgProd.AddItem ""
                            CriaNovaClasse RegistosP, FgProd.row + 1, "PROD"
                            FgProd.row = FgProd.row + 1
                            FgProd.Col = 0
                        Else
                            FgProd.Col = 2
                        End If
                    End If
                Else
                    Beep
                    BG_Mensagem mediMsgStatus, "Produto j� indicado!"
                End If
            Case 2 ' Codigo da especificacao
                If SELECT_Especif = True Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).CodEspecif = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 5
                End If
            Case 4 ' volume
                If Trim(EcAuxProd.text) <> "" Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).Volume = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 0
                End If
            Case 5 ' Data Prevista
                If Trim(EcAuxProd.text) <> "" Then
                    If BG_ValidaTipoCampo_ADO(Me, EcAuxProd) = True Then
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).DtPrev = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        FgProd.Col = 0
                    Else
                        Beep
                        BG_Mensagem mediMsgStatus, "Data inv�lida!"
                    End If
                    
                End If
        End Select
        If FgProd.Enabled = True Then FgProd.SetFocus
    End If
    
End Sub

Private Sub EcAuxProd_KeyPress(KeyAscii As Integer)
    
    If Enter = True Then KeyAscii = 0

End Sub

Private Sub EcAuxTubo_KeyPress(KeyAscii As Integer)
    
    If Enter = True Then KeyAscii = 0

End Sub

Private Sub EcAuxProd_LostFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    
    EcAuxProd.Visible = False
    FgProd.SetFocus

End Sub

Private Sub EcAuxTubo_LostFocus()
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    
    EcAuxTubo.Visible = False
    FgTubos.SetFocus

End Sub

Public Sub EcCodEFR_Validate(cancel As Boolean)
    Dim i As Integer
    cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
    If gPrecosAmbienteHospitalar = mediSim Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboNumDoc = "0" Or RegistosRM(i).ReciboNumDoc = "" Then
                AlteraEntidadeReciboManual CLng(i), EcCodEFR, EcDescrEFR, "", -1
            Else
                'BG_Mensagem mediMsgBox, "N�o � possivel alterar a entidade das an�lises associadas ao recibo emitido.", vbInformation, "Altera��o entidade"
            End If
        Next
        ActualizaLinhasRecibos -1
    End If
    ValidarARS EcCodEFR
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  EcCodEFR_Validate: " & Err.Description, Me.Name, "EcCodEFR_Validate", False
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodMedico_Validate(cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodMedico.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome_med, ext_telef FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Tabela.Close
            sql = "SELECT cod_med, nome_med, ext_telef FROM sl_medicos WHERE upper(cod_ordem) ='" & Replace(UCase(Trim(EcCodMedico.text)), "M", "") & "'"
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
            If Tabela.RecordCount = 0 Then
            
                cancel = True
                BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
                EcCodMedico.text = ""
                EcNomeMedico.text = ""
            Else
                EcCodMedico.text = Tabela!cod_med
                EcNomeMedico.text = Tabela!nome_med
                EcObsProveniencia = BL_HandleNull(Tabela!ext_telef, "")
            End If
        Else
            EcNomeMedico.text = Tabela!nome_med
            EcObsProveniencia = BL_HandleNull(Tabela!ext_telef, "")
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcNomeMedico.text = ""
        Grava_Medico_Requis
    End If
    
End Sub


Private Sub EcCodPostal_validate(cancel As Boolean)
    If EcCodPostal = "" Then
        EcDescrPostal = ""
        EcRuaPostal = ""
        EcCodPostal = ""
        EcCodPostalAux = ""
    End If
End Sub

Private Sub EcRuaPostal_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset
    
    EcRuaPostal.text = UCase(EcRuaPostal.text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.text) & "-" & Trim(EcRuaPostal.text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            cancel = True
            'BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcRuaPostal = ""
            EcCodPostal = ""
            EcCodPostalAux = ""
        Else
            EcCodPostalAux.text = Postal
            EcDescrPostal.text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcRuaPostal_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcRuaPostal_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqCodPostal_Click()
    On Error GoTo TrataErro

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" And EcCodPostal = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    If EcDescrPostal <> "" Then
        ChavesPesq(1) = "descr_postal"
        CamposEcran(1) = "descr_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_postal"
        CamposEcran(2) = "cod_postal"
        Tamanhos(2) = 2000
        Headers(2) = "C�digo"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        ChavesPesq(1) = "cod_postal"
        CamposEcran(1) = "cod_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Codigo"
        
        ChavesPesq(2) = "descr_postal"
        CamposEcran(2) = "descr_postal"
        Tamanhos(2) = 2000
        Headers(2) = "Descr��o"
    End If
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_postal"
    If EcDescrPostal <> "" And EcCodPostal = "" Then
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        CWhere = "UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    Else
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
        CWhere = CWhere & " AND UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    End If
    CampoPesquisa = "descr_postal"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_postal ", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, Resultados(2), "-") - 1
            If i = -1 Then
                i = InStr(1, Resultados(1), "-") - 1
            End If
            If EcDescrPostal <> "" Then
                If i = -1 Then
                    s1 = Resultados(2)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(2), 1, i)
                    s2 = Mid(Resultados(2), InStr(1, Resultados(2), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(2))
                EcDescrPostal.text = Resultados(1)
                EcDescrPostal.SetFocus
            ElseIf EcDescrPostal = "" Then
                If i = -1 Then
                    s1 = Resultados(1)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(1), 1, i)
                    s2 = Mid(Resultados(1), InStr(1, Resultados(1), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(1))
                EcDescrPostal.text = Resultados(2)
                EcDescrPostal.SetFocus
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqCodPostal_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqCodPostal_Click"
    Exit Sub
    Resume Next
End Sub
Private Sub EcCodpais_Validate(cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodPais.text = UCase(EcCodPais.text)
    If EcCodPais.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_pais FROM sl_pais WHERE cod_pais='" & Trim(EcCodPais.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            cancel = True
            BG_Mensagem mediMsgBox, "Pa�s inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodPais.text = ""
            EcDescrPais.text = ""
        Else
            EcDescrPais.text = BL_HandleNull(Tabela!descr_pais)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPais.text = ""
    End If
End Sub


Private Sub BtPesqPais_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_pais", _
                        "descr_pais", "cod_pais", _
                        EcCodPais
    EcCodpais_Validate False
End Sub

Public Sub EcCodProveniencia_Validate(cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodProveniencia.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven, t_sit, t_urg FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.text = ""
            EcDescrProveniencia.text = ""
        Else
            EcDescrProveniencia.text = Tabela!descr_proven
        
            If Not (IsNull(Tabela!t_sit)) Then
'                CbSituacao.ListIndex = Tabela!t_sit
                BG_MostraComboSel BL_HandleNull(Tabela!t_sit, -1), CbSituacao
            End If
            
            If Not (IsNull(Tabela!t_urg)) Then
'                If Tabela!T_urg = "N" Then
'                    CbUrgencia.ListIndex = 0
'                ElseIf Tabela!T_urg = "U" Then
'                    CbUrgencia.ListIndex = 1
'                ElseIf Tabela!T_urg = "L" Then
'                    CbUrgencia.ListIndex = 2
'                End If
'                CbUrgencia.ListIndex = Tabela!T_urg
                BG_MostraComboSel BL_HandleNull(Tabela!t_urg, -1), CbUrgencia
            End If
        
        End If
        
        
        'verifica se a situa��o est� preenchida, se sim, verifica se esta � poss�vel
'        If CbSituacao.ListIndex <> -1 Then
'            Set Tabela = New ADODB.Recordset
'            Sql = "SELECT t_sit FROM sl_proven WHERE cod_proven ='" & Trim(EcCodProveniencia.Text) & "'"
'            Tabela.CursorType = adOpenStatic
'            Tabela.CursorLocation = adUseServer
'            Tabela.Open Sql, gConexao
'            If Tabela.RecordCount > 0 Then
'                If CbSituacao.ListIndex <> Tabela!t_sit Then
'                    BG_Mensagem mediMsgBox, "A proveni�nia n�o � poss�vel para a situa��o seleccionada", vbInformation, "Aten��o"
'                End If
'            End If
'        End If
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.text = ""
    End If
    
End Sub

Private Sub EcDataAlteracao_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataAlteracao)
    
End Sub

Private Sub EcDataChegada_GotFocus()

    If Trim(EcDataChegada.text) = "" Then
        If gLAB = "CHVNG" Or gLAB = "HCVP" Then
            EcDataChegada.text = dataAct
        Else
            EcDataChegada.text = EcDataPrevista.text
        End If
    End If
    EcDataChegada.SelStart = 0
    EcDataChegada.SelLength = Len(EcDataChegada)
    
End Sub

Private Sub EcDtColheita_GotFocus()

    If Trim(EcDtColheita.text) = "" Then
        EcDtColheita.text = dataAct
    End If
    EcDtColheita.SelStart = 0
    EcDtColheita.SelLength = Len(EcDtColheita)
    
End Sub
Private Sub EcHrColheita_GotFocus()

    If Trim(EcHrColheita.text) = "" Then
        EcHrColheita.text = Bg_DaHora_ADO
    End If
    EcHrColheita.SelStart = 0
    EcHrColheita.SelLength = Len(EcHrColheita)
    
End Sub
Private Sub EcDataChegada_Validate(cancel As Boolean)
    
    If EcDataChegada.text <> "" Then
        cancel = Not ValidaDataChegada
    End If
        
End Sub

Private Sub EcDataCriacao_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataCriacao)
    
End Sub

Private Sub EcDataFact_GotFocus()
    If EcDataFact.text = "" Then
        EcDataFact.text = Format(dataAct, gFormatoData)
    End If
End Sub

Private Sub EcDataImpressao_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao)
    
End Sub

Private Sub EcDataImpressao2_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao2)
    
End Sub
Private Sub EcDataFact_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataFact)
    
End Sub

Public Sub EcDataPrevista_GotFocus()
    Dim cama As String
    Dim cod_valencia As String
    
    'Para evitar que a nova entidade seja reposta pela codificada
    ' no utente
    If gPreencheEFRUtente = 1 Then
        If (Trim(EcCodEFR.text) = "") Then
            Call PreencheEFRUtente
        End If
    ElseIf gPreencheEFREpisodioGH = 1 Then
        If (Trim(EcCodEFR.text) = "") And EcEpisodio.text <> "" And CbSituacao.ListIndex <> mediComboValorNull Then
            Call PreencheEFREpisodioGH(EcEpisodio.text, CbSituacao.ItemData(CbSituacao.ListIndex))
        End If
    End If
    Call ValidaDataPrevista
    
    If gHIS_Import = mediSim And UCase(HIS.nome) = "GH" Then
        If CbSituacao.ListIndex > mediComboValorNull And EcCodProveniencia = "" Then
            EcCodProveniencia = GH_RetornaServicoEpisodio(CbTipoUtente, EcUtente, CbSituacao.ItemData(CbSituacao.ListIndex), EcEpisodio)
            EcCodProveniencia_Validate False
            
            cod_valencia = GH_RetornaValencia(CbTipoUtente, EcUtente, EcEpisodio, cama)
            If cod_valencia <> "" Then
                EcCodValencia = cod_valencia
                EcCodValencia_Validate False
                If EcObsProveniencia = "" Then
                    EcObsProveniencia = "Cama:" & cama
                End If
            End If
            
        End If
    End If
    If gLAB = "ICIL" Then
        If EcUtente <> "" And CbUrgencia.ListIndex = mediComboValorNull Then
            CbUrgencia.ListIndex = 0
        End If
    End If
    ' -----------------------------------------------
    ' PAULO COSTA 07/04/2003 CONSULTA ?
    ' -----------------------------------------------
    
'    Me.CbSituacao.ListIndex = 0
'    Me.CbUrgencia.ListIndex = 0
    
    ' -----------------------------------------------
    ' FIM PAULO COSTA 07/04/2003 CONSULTA ?
    ' -----------------------------------------------
    
    EcDataPrevista.SelStart = 0
    EcDataPrevista.SelLength = Len(EcDataPrevista)
    
End Sub

Sub Preenche_Data_Prev_Prod()
    
    Dim i As Integer
    
    ExecutaCodigoP = False
    For i = 1 To RegistosP.Count - 1
        If RegistosP(i).CodProd <> "" Then
            If Trim(RegistosP(i).DtPrev) = "" Then
                RegistosP(i).DtPrev = EcDataPrevista.text
                FgProd.TextMatrix(i, 4) = EcDataPrevista.text
                RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
                FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
            End If
        If RegistosP(i).EstadoProd = "" Then
            RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
            FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
        End If
        End If
    Next i
    
    ExecutaCodigoP = True

End Sub

Function ValidaDataChegada() As Boolean

    ValidaDataChegada = False
    If EcDataChegada.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataChegada) Then
'            If DateValue(EcDataChegada.Text) > DateValue(dataAct) Then
'                BG_Mensagem mediMsgBox, "A data de Chegada tem que ser inferior ou igual � data actual ", vbInformation, "Aten��o"
'                SendKeys ("{HOME}+{END}")
'                If EcDataChegada.Enabled = True Then
'                    EcDataChegada.SetFocus
'                End If
'            Else
'                ValidaDataChegada = True
'            End If
            ValidaDataChegada = True
        End If
    End If
    
End Function

Function ValidaDataPrevista() As Boolean

    ValidaDataPrevista = False
    If EcDataPrevista.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataPrevista) And EcDataPrevista.Enabled = True Then
'            If DateValue(EcDataPrevista.Text) < DateValue(dataAct) Then
'                BG_Mensagem mediMsgBox, "A data prevista tem que ser superior ou igual � data actual ", vbInformation, "Aten��o"
'                If EcDataPrevista.Enabled = True Then
'                    EcDataPrevista.Text = ""
'                    EcDataPrevista.SetFocus
'                End If
'            Else
'                ValidaDataPrevista = True
'            End If
            ValidaDataPrevista = True
        End If
    ElseIf EcDataPrevista.text = "" Then
        If Flg_LimpaCampos = False Then
            EcDataPrevista.text = Format(dataAct, gFormatoData)
            ValidaDataPrevista = True
        End If
    End If

End Function

Public Sub EcDataPrevista_Validate(cancel As Boolean)
    
    On Error Resume Next
    
    If Trim(EcDataPrevista.text) <> "" Then
        cancel = Not ValidaDataPrevista
    End If
    If cancel = True Then Exit Sub
    
    If EcDataPrevista.Enabled = True And Trim(EcSeqUtente.text) <> "" Then
        Preenche_Data_Prev_Prod
        SSTGestReq.TabEnabled(1) = True
        SSTGestReq.TabEnabled(2) = True
        SSTGestReq.TabEnabled(5) = True
        If gMostraColheita = 1 Then
            SSTGestReq.TabEnabled(6) = True
        End If
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FgProd.row = LastRowP
        FgProd.Col = LastColP
        ExecutaCodigoP = True
        FrTubos.Enabled = True
        
    End If
    
End Sub

Private Sub EcDescrProveniencia_Change()
    Dim Tabela As ADODB.recordset
    Dim sql As String
    'verifica se a situa��o para a proveni�ncia escolhida � urg�ncia, se sim preenche campos autom�ticamente
    If EcCodProveniencia.text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT t_sit FROM sl_proven WHERE cod_proven ='" & Trim(EcCodProveniencia.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            If Tabela!t_sit = 2 Then
                Call EcDataPrevista_GotFocus
                Call EcDataPrevista_Validate(True)
                If EcDataChegada.text = "" And EcDataChegada.Enabled = True Then
                    EcDataChegada.text = dataAct
                End If
                If CbUrgencia.ListIndex = mediComboValorNull Then
                    CbUrgencia.ListIndex = 1
                End If
                If CbSituacao.ListIndex = mediComboValorNull Then
                    CbSituacao.ListIndex = 2
                End If
            End If
            
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
End Sub



Private Sub EcEpisodioSONHO_KeyPress(KeyAscii As Integer)

    If (KeyAscii = 13) Then
        Call BtGetSONHO_OK_Click
    End If

End Sub


Private Sub EcNumBenef_GotFocus()
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodEFR.text <> "" And Trim(EcNumBenef.text) = "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = Trim(BL_HandleNull(Tabela!Formato1, ""))
            Formato2 = Trim(BL_HandleNull(Tabela!Formato2, ""))
            EcNumBenef.text = Trim(BL_HandleNull(Tabela!sigla, ""))
            Sendkeys ("{END}")
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub

Private Sub EcNumBenef_Validate(cancel As Boolean)
    
    Dim Formato As Boolean
    
    Formato = Verifica_Formato(UCase(EcNumBenef.text), Formato1, Formato2)
    If Formato = False And EcNumBenef.text <> "" Then
        cancel = True
        Sendkeys ("{END}")
    End If
    
End Sub

Private Sub EcNumReq_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    If gLAB <> "ICIL" Then
        cmdOK.Default = True
    End If

End Sub

Private Sub EcNumReq_LostFocus()
    
    cmdOK.Default = False

End Sub

Private Sub EcNumReq_Validate(cancel As Boolean)
    If Len(EcNumReq) > 7 Then
        EcNumReq = Right(EcNumReq, 7)
    End If
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)

End Sub

Private Sub EcObsProveniencia_LostFocus()

    On Error Resume Next
    Dim aux As String
    EcObsProveniencia = Replace(EcObsProveniencia, "'", " ")
    aux = BL_Retira_ENTER(EcObsProveniencia.text)
    EcObsProveniencia.text = aux
    DoEvents
    DoEvents
    
End Sub
Private Sub EcPesqRapMedico_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If EcPesqRapMedico.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        sql = "SELECT cod_med,nome_med FROM sl_medicos WHERE cod_med = '" & EcPesqRapMedico & "'"
        
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do m�dico!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapMedico.text = ""
        Else
            EcCodMedico.text = Trim(rsCodigo!cod_med)
            EcNomeMedico.text = Trim(rsCodigo!nome_med)
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcPesqRapProduto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapProduto.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_produto FROM sl_produto WHERE seq_produto = " & EcPesqRapProduto, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do produto!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxProd.text = BL_HandleNull(rsCodigo!cod_produto, "")
            EcAuxProd.left = FgProd.CellLeft + 270
            EcAuxProd.top = FgProd.CellTop + 2060
            EcAuxProd.Width = FgProd.CellWidth + 20
            EcAuxProd.Visible = True
            EcAuxProd.Enabled = True
            EcAuxProd.SetFocus
        End If
    End If

End Sub

Private Sub EcPesqRapTubo_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapTubo.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_tubo FROM sl_tubo WHERE seq_tubo = " & EcPesqRapTubo, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do tubo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxTubo.text = BL_HandleNull(rsCodigo!cod_tubo, "")
            EcAuxTubo.left = FgTubos.CellLeft + 270
            EcAuxTubo.top = FgTubos.CellTop + 2060
            EcAuxTubo.Width = FgTubos.CellWidth + 20
            EcAuxTubo.Visible = True
            EcAuxTubo.Enabled = True
            EcAuxTubo.SetFocus
        End If
    End If

End Sub

Private Sub EcPesqRapProven_Change()
    
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapProven.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_proven,descr_proven FROM sl_proven WHERE seq_proven = " & EcPesqRapProven, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da proveni�ncia!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapProven.text = ""
        Else
            EcCodProveniencia.text = Trim(rsCodigo!cod_proven)
            EcDescrProveniencia.text = Trim(rsCodigo!descr_proven)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

'Function ValidaReqAssoc() As Boolean
'
'    Dim sql As String
'    Dim Tabela As ADODB.recordset
'
'    ValidaReqAssoc = True
'    If gValidarReqAssociada <> mediNao Then
'        If EcReqAssociada.text <> "" Then
'            Set Tabela = New ADODB.recordset
'            sql = "SELECT n_req FROM sl_requis WHERE n_req=" & EcReqAssociada.text
'            Tabela.CursorType = adOpenStatic
'            Tabela.CursorLocation = adUseServer
'            Tabela.Open sql, gConexao
'            If Tabela.RecordCount = 0 Then
'                BG_Mensagem mediMsgBox, "N�o existe requisi��o para associa��o com esse n�mero !", vbInformation, ""
'                ValidaReqAssoc = False
'                EcReqAssociada.text = ""
'                'Para n�o permitir associar requisi��es a ela pr�pria
'            ElseIf Trim(EcReqAssociada.text) = Trim(EcNumReq.text) Then
'                BG_Mensagem mediMsgBox, "N�o pode associar uma requisi��o a ela pr�pria !", vbInformation, ""
'                ValidaReqAssoc = False
'                EcReqAssociada.text = ""
'            End If
'        End If
'    End If
'
'End Function

'Private Sub EcReqAssociada_GotFocus()
'    If gMultiReport = 1 Then
'        Set CampoActivo = Me.ActiveControl
'        cmdOK.Default = True
'    End If
'End Sub

'Private Sub EcReqAssociada_LostFocus()
'    If gMultiReport = 1 Then
'        cmdOK.Default = False
'    End If
'End Sub
'Private Sub EcReqAssociada_Validate(Cancel As Boolean)
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcReqAssociada)
'    If Cancel = False Then
'        Cancel = Not ValidaReqAssoc
'    End If
    
'End Sub

Private Sub EcReqAux_GotFocus()
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True
End Sub

Private Sub EcReqAux_LostFocus()
    cmdOK.Default = False
End Sub




Private Sub EcCodUserColheita_Validate(cancel As Boolean)
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodUserColheita.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador='" & Trim(EcCodUserColheita.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodUserColheita.text = ""
            EcDescrUserColheita.text = ""
        Else
            EcDescrUserColheita.text = Tabela!nome
        End If
       
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrUserColheita.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodUserColheita_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodUserColheita_Validate"
    Exit Sub
    Resume Next

End Sub



Public Sub EcUtente_Validate(cancel As Boolean)
    
    Dim sql As String
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    
    'Limpa a estrutura de dados do utente
    If CbTipoUtente.ListIndex <> -1 And EcUtente.text <> "" Then
        'Procurar o sequencial e chamar a fun��o que preenche a identifica��o do utente
        Campos(0) = "seq_utente"
        
        iret = BL_DaDadosUtente(Campos, retorno, , CbTipoUtente.text, EcUtente.text)
        
        If iret = -1 Then
            cancel = True
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbInformation + vbOKOnly, "Pesquisa ao utente!"
            EcNome.text = ""
            EcProcHosp1.text = ""
            EcProcHosp2.text = ""
            EcNumCartaoUte.text = ""
            EcDataNasc.text = ""
            EcSexoUte.text = ""
        Else
            BtInfClinica.Enabled = True
            LaInfCli.Enabled = True
            BtRegistaMedico.Enabled = True
            
            EcSeqUtente.text = retorno(0)
            BL_PreencheDadosUtente EcSeqUtente
            
            PreencheDadosUtente
            
        End If
    Else
        EcNome.text = ""
        EcProcHosp1.text = ""
        EcProcHosp2.text = ""
        EcNumCartaoUte.text = ""
        EcDataNasc.text = ""
        EcSexoUte.text = ""
    End If
    
End Sub

Private Sub EcUtilizadorAlteracao_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorAlteracao)
    If cancel = False Then
        LbNomeUtilAlteracao = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
    End If
    
End Sub

Private Sub EcUtilizadorCriacao_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorCriacao)
    If cancel = False Then
        LbNomeUtilCriacao = BL_SelNomeUtil(EcUtilizadorCriacao.text)
    End If
    
End Sub



Private Sub EcUtilizadorImpressao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcUtilizadorImpressao_LostFocus()
    
    LbNomeUtilImpressao = BL_SelNomeUtil(EcUtilizadorImpressao.text)

End Sub

Private Sub EcUtilizadorImpressao_Validate(cancel As Boolean)
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcUtilizadorImpressao2_Validate(cancel As Boolean)
    
    LbNomeUtilImpressao2 = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
    
End Sub



Private Sub EcGarrafa_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        BtGarrafaOk_Click
    End If
End Sub

'BRUNODSANTOS GLINTT-HS-15750
Private Sub FgAna_Click()

    If gUsaEnvioPDS <> mediSim Or (FlgARS = False And FlgADSE = False) Then Exit Sub
 
    If (FGAna.rows <= 2) Or (FGAna.TextMatrix(FGAna.RowSel, lColFgAnaCodAna) = "") Or (FGAna.TextMatrix(FGAna.RowSel, lColFgAnaConsenteEnvioPDS) = "") Then
        Exit Sub
    End If

    With FGAna
       If .MouseCol = lColFgAnaConsenteEnvioPDS Then
            If .TextMatrix(.row, lColFgAnaConsenteEnvioPDS) = EstadoCheckboxEnvioPDS_Unchecked Then
                Call ChangeCheckboxState(.RowSel, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Checked)
                Call CheckAnalisesMesmaCredencial(EstadoCheckboxEnvioPDS_Checked)
            ElseIf .TextMatrix(.row, lColFgAnaConsenteEnvioPDS) = EstadoCheckboxEnvioPDS_Checked Then
                Call ChangeCheckboxState(.RowSel, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Unchecked)
                Call CheckAnalisesMesmaCredencial(EstadoCheckboxEnvioPDS_Unchecked)
            End If
        End If
    End With
    
End Sub

Private Sub Fgana_DblClick()
    FgAna_KeyDown vbKeyReturn, 0
    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = lColFgAnaCodAna And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodAna)) <> "" Then
        gRequisicaoActiva = FormGestaoRequisicao.EcNumReq
        gF_REQUIS = 1
        FormGestaoRequisicao.Enabled = False
        FormConsNovo.Show
    End If
End Sub



Private Sub FGAna_GotFocus()
    FGAna.row = RegistosA.Count
    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = lColFgAnaCodAna And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodAna)) <> "" Then
        FGAna.CellBackColor = vbRed
        FGAna.CellForeColor = vbWhite
    Else
        FGAna.CellBackColor = azul
        FGAna.CellForeColor = vbWhite
    End If

End Sub


Public Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ana As String
    Dim rsApaga As ADODB.recordset
    Dim iRec As Long
    If KeyCode = 122 Then               'F11 - Incrementa n� P1
        Flg_IncrementaP1 = True
        Exit Sub
    End If
    
    Select Case KeyCode
        Case 96
            KeyCode = 48
        Case 97
            KeyCode = 49
        Case 98
            KeyCode = 50
        Case 99
            KeyCode = 51
        Case 100
            KeyCode = 52
        Case 101
            KeyCode = 53
        Case 102
            KeyCode = 54
        Case 103
            KeyCode = 55
        Case 104
            KeyCode = 56
        Case 105
            KeyCode = 57
    End Select
    
    If (FGAna.Col = lColFgAnaCodAna Or FGAna.Col = lColFgAnaCred Or FGAna.Col = lColFgAnaOrdem) And (KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0)) Then ' Enter = Editar
        'If gLAB <> "HCVP" And gLAB <> "ICIL" Then
        '    If FGAna.TextMatrix(FGAna.row, FGAna.Col) <> "" Then Exit Sub
        'End If
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = lColFgAnaCodAna Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        
        Else
            If LastColA <> 0 And Trim(FGAna.TextMatrix(LastRowA, lColFgAnaCodAna)) = "" Then
                Beep
                BG_Mensagem mediMsgStatus, "Tem que indicar primeiro uma an�lise!"
            Else
                If EcDataFecho.text <> "" Then
                    Beep
                    BG_Mensagem mediMsgStatus, "N�o pode acrescentar an�lises numa requisi��o fechada!"
                    Exit Sub
                End If
                ExecutaCodigoA = False
                If (FGAna.Col = lColFgAnaCred Or FGAna.Col = lColFgAnaOrdem) And KeyCode <> 13 Then
                    EcAuxAna.text = IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                Else
                    EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                End If
                ExecutaCodigoA = True
                If KeyCode = 8 Then
                    If Len(EcAuxAna.text) > 1 Then EcAuxAna.text = Mid(EcAuxAna.text, 1, Len(EcAuxAna.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxAna.SelStart = 0
                    EcAuxAna.SelLength = Len(EcAuxAna)
                Else
                    EcAuxAna.SelLength = 0
                    EcAuxAna.SelStart = Len(EcAuxAna)
                End If

                EcAuxAna.left = FGAna.left + SSTGestReq.left + FrAnalises.left + FGAna.CellLeft
                EcAuxAna.top = FGAna.top + SSTGestReq.top + FrAnalises.top + FGAna.CellTop
                
                EcAuxAna.Width = FGAna.CellWidth + 20
                EcAuxAna.Visible = True
                EcAuxAna.Enabled = True
                EcAuxAna.SetFocus
            End If
        End If
    ElseIf (FGAna.Col = lColFgAnaUnidSaude) And (KeyCode = 13 And Shift = 0) Then ' Enter = Editar
        PesquisaUnidSaude
        If EcCodUSaude <> "" Then

            AlteraUnidadeSaude EcCodUSaude, FGAna.row
        End If
        
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga linha
        If (FGAna.Col = lColFgAnaCred Or FGAna.Col = lColFgAnaOrdem) Then
            FGAna.TextMatrix(FGAna.row, FGAna.Col) = ""
            If FGAna.Col = lColFgAnaCred Then
                RegistosA(FGAna.row).NReqARS = ""
            ElseIf FGAna.Col = lColFgAnaOrdem Then
                ContP1 = ContP1 - 1
            End If
        ElseIf RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser eliminada: j� cont�m resultados!"
        ElseIf RegistosA(FGAna.row).codAna <> "" Then
            'soliveira CHVNG-Espinho s� deixa apagar a an�lise se a analise pertencer a esse local de marca��o
            Set rsApaga = New ADODB.recordset
            rsApaga.CursorLocation = adUseServer
            rsApaga.CursorType = adOpenStatic
            rsApaga.Source = "select cod_ana from sl_ana_locais where cod_ana = " & BL_TrataStringParaBD(RegistosA(FGAna.row).codAna) & " and cod_local = " & gCodLocal
            rsApaga.ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros rsApaga.Source
            rsApaga.Open
            If rsApaga.RecordCount > 0 Or gLAB = "CHVNG" Then
            TotalEliminadas = TotalEliminadas + 1
            ReDim Preserve Eliminadas(TotalEliminadas)
            Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
            Eliminadas(TotalEliminadas).NReq = EcNumReq.text
            Elimina_Mareq RegistosA(FGAna.row).codAna
            LbTotalAna = LbTotalAna - 1
            If LbTotalAna < 0 Then
                LbTotalAna = 0
            End If
            ana = RegistosA(FGAna.row).codAna
            RegistosA.Remove FGAna.row
            FGAna.RemoveItem FGAna.row
            'FGONCALVES
            EliminaTuboEstrut
            EliminaProduto ana
            ContP1 = ContP1 - 1
            
            If gPrecosAmbienteHospitalar = mediSim Then
                iRec = DevIndice(ana)
                If EliminaReciboManual(iRec, ana, True) = True Then
                    LimpaColeccao RegistosRM, iRec
                End If
            End If
            
            Else
                BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser eliminada: n�o pertence a este local!"
            End If
        End If
        
        FGAna.SetFocus
    ElseIf KeyCode = 118 Then ' F7 - INSERIR OBSERVA��O DA AN�LISE
        If FGAna.TextMatrix(LastRowA, 0) <> "" And LastRowA <> 0 Then
            FrameObsAna.caption = "Observa��o da An�lise: " & FGAna.TextMatrix(LastRowA, lColFgAnaDescrAna)
            FrameObsAna.Visible = True
            FrameObsAna.left = 8280
            FrameObsAna.top = 0
            EcObsAna = RegistosA(LastRowA).ObsAnaReq
            EcObsAna.SetFocus
        End If
    End If
    
End Sub

Private Sub FGAna_LostFocus()

    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = lColFgAnaCodAna And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodAna)) <> "" Then
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbRed
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
    
End Sub

Private Sub FGAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 And FGAna.Col = lColFgAnaCodAna And FGAna.row > 0 Then
        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANA
    End If
End Sub

Private Sub Fgana_RowColChange()
    
    Dim tmpCol As Integer
    Dim tmpRow As Integer
        
    If ExecutaCodigoA = True Then
        ExecutaCodigoA = False
        
        tmpCol = FGAna.Col
        tmpRow = FGAna.row
        If FGAna.Col <> LastColA And FGAna.Cols > LastColA Then FGAna.Col = LastColA
        If LastRowA < FGAna.rows Then
        If FGAna.row <> LastRowA Then FGAna.row = LastRowA
        End If
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(LastRowA).Estado <> "-1" And LastColA = lColFgAnaCodAna And Trim(FGAna.TextMatrix(LastRowA, lColFgAnaCodAna)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbBlack
            End If
        End If
        
        If FGAna.Col <> tmpCol Then FGAna.Col = tmpCol
        If FGAna.row <> tmpRow Then FGAna.row = tmpRow
        
        ' Controlar as colunas
        If FGAna.Cols > 2 Then
            If FGAna.Col = lColFgAnaOrdem Then
            ElseIf FGAna.Col = lColFgAnaDescrAna Then
                If LastColA = lColFgAnaCred Then
                    FGAna.Col = lColFgAnaCodAna
                Else
                    FGAna.Col = lColFgAnaCred
                End If
            End If
        Else
            If FGAna.Col = lColFgAnaDescrAna Then
                FGAna.Col = lColFgAnaCodAna
                If FGAna.row < FGAna.rows - 1 Then FGAna.row = FGAna.row + 1
            End If
        End If
        
        LastColA = FGAna.Col
        LastRowA = FGAna.row
        
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = lColFgAnaCodAna And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodAna)) <> "" Then
                FGAna.CellBackColor = vbRed
                FGAna.CellForeColor = vbWhite
            Else
                FGAna.CellBackColor = azul
                FGAna.CellForeColor = vbWhite
            End If
        End If
        
        ExecutaCodigoA = True
    End If

End Sub

Private Sub FgProd_GotFocus()
    Dim i As Integer
    FgProd.CellBackColor = azul
    FgProd.CellForeColor = vbWhite
    
    If FgProd.Col = 0 Then
        BtPesquisaProduto.Enabled = True
        BtPesquisaEspecif.Enabled = False
    ElseIf FgProd.Col = 2 Then
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = True
        BtObsEspecif.Enabled = True
    Else
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = False
    End If
    
    If gLAB = "CHVNG" Then
        BtPesquisaProduto.Enabled = False
        For i = 1 To FGAna.rows - 1
            If FGAna.TextMatrix(i, lColFgAnaCodAna) = "PCULMCB" Then
                BtPesquisaProduto.Enabled = True
            End If
        Next
    End If

End Sub


Public Sub FgProd_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
        If LastColP <> 0 And Trim(FgProd.TextMatrix(LastRowP, 0)) = "" Then
            Beep
            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um produto!"
        Else
            If LastColP = 6 And EcDataPrevista.Enabled = False Then
                Beep
                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este produto!"
            Else
                If LastColP = 0 And gLAB = "CHVNG" Then
                Else
                EcAuxProd.text = FgProd.TextMatrix(FgProd.row, FgProd.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                If KeyCode = 8 Then
                    EcAuxProd.text = Mid(EcAuxProd.text, 1, Len(EcAuxProd.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxProd.SelStart = 0
                    EcAuxProd.SelLength = Len(EcAuxProd)
                Else
                    EcAuxProd.SelLength = 0
                End If
                EcAuxProd.left = FgProd.CellLeft + 270
                EcAuxProd.top = FgProd.CellTop + 2060
                EcAuxProd.Width = FgProd.CellWidth + 20
                EcAuxProd.Visible = True
                EcAuxProd.Enabled = True
                EcAuxProd.SetFocus
                End If
            End If
        End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
        Select Case FgProd.Col
            Case 0
                If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
                    If FgProd.row < FgProd.rows - 1 Then
                        RegistosP.Remove FgProd.row
                        FgProd.RemoveItem FgProd.row
                    Else
                        FgProd.TextMatrix(FgProd.row, 0) = ""
                        FgProd.TextMatrix(FgProd.row, 1) = ""
                        FgProd.TextMatrix(FgProd.row, 2) = ""
                        FgProd.TextMatrix(FgProd.row, 3) = ""
                        FgProd.TextMatrix(FgProd.row, 4) = ""
                        FgProd.TextMatrix(FgProd.row, 5) = ""
                        FgProd.TextMatrix(FgProd.row, 6) = ""
                        FgProd.TextMatrix(FgProd.row, 7) = ""
                        RegistosP(FgProd.row).CodProd = ""
                        RegistosP(FgProd.row).DescrProd = ""
                        RegistosP(FgProd.row).EspecifObrig = ""
                        RegistosP(FgProd.row).CodEspecif = ""
                        RegistosP(FgProd.row).DescrEspecif = ""
                        RegistosP(FgProd.row).DtPrev = ""
                        RegistosP(FgProd.row).DtChega = ""
                        RegistosP(FgProd.row).EstadoProd = ""
                        RegistosP(FgProd.row).Volume = ""
                    End If
                End If
            Case 2
                FgProd.TextMatrix(FgProd.row, 2) = ""
                FgProd.TextMatrix(FgProd.row, 3) = ""
                RegistosP(FgProd.row).CodEspecif = ""
                RegistosP(FgProd.row).DescrEspecif = ""
            Case 4
                FgProd.TextMatrix(FgProd.row, 4) = ""
                RegistosP(FgProd.row).Volume = ""
            Case 5
                FgProd.TextMatrix(FgProd.row, 5) = ""
                RegistosP(FgProd.row).DtPrev = ""
            Case 6
                FgProd.TextMatrix(FgProd.row, 6) = ""
                RegistosP(FgProd.row).DtChega = ""
        End Select
        FgProd.SetFocus
    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FgProd.TextMatrix(FgProd.row, 1) & " " & FgProd.TextMatrix(FgProd.row, 3) & " ?", vbYesNo + vbQuestion, "Eliminar produto.") = vbYes Then
                If FgProd.row < FgProd.rows - 1 Then
                    RegistosP.Remove FgProd.row
                    FgProd.RemoveItem FgProd.row
                Else
                    FgProd.TextMatrix(FgProd.row, 0) = ""
                    FgProd.TextMatrix(FgProd.row, 1) = ""
                    FgProd.TextMatrix(FgProd.row, 2) = ""
                    FgProd.TextMatrix(FgProd.row, 3) = ""
                    FgProd.TextMatrix(FgProd.row, 4) = ""
                    FgProd.TextMatrix(FgProd.row, 5) = ""
                    FgProd.TextMatrix(FgProd.row, 6) = ""
                    FgProd.TextMatrix(FgProd.row, 7) = ""
                    RegistosP(FgProd.row).CodProd = ""
                    RegistosP(FgProd.row).DescrProd = ""
                    RegistosP(FgProd.row).EspecifObrig = ""
                    RegistosP(FgProd.row).CodEspecif = ""
                    RegistosP(FgProd.row).DescrEspecif = ""
                    RegistosP(FgProd.row).DtPrev = ""
                    RegistosP(FgProd.row).DtChega = ""
                    RegistosP(FgProd.row).EstadoProd = ""
                    RegistosP(FgProd.row).Volume = ""
                    
                End If
            End If
        End If
        FgProd.SetFocus
    Else
    End If

End Sub



Function SELECT_Descr_Prod() As Boolean
    
    'Fun��o que procura a descri��o do produto
    'A fun��o devolve TRUE se encontrou a descri��o caso contr�rio devolve FALSE
    
    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    
    SELECT_Descr_Prod = False
    With rsDescr
        .Source = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                  "cod_produto = " & BL_TrataStringParaBD(UCase(EcAuxProd.text))
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Produto inexistente!", vbOKOnly + vbInformation, "Produtos"
        EcAuxProd.text = ""
    Else
        FgProd.TextMatrix(LastRowP, 1) = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).EspecifObrig = rsDescr!especif_obrig
        SELECT_Descr_Prod = True
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing

End Function

Function SELECT_Especif() As Boolean
    
    Dim RsDescrEspecif As ADODB.recordset
    Dim encontrou As Boolean
    
    SELECT_Especif = False
    encontrou = False
    If Trim(EcAuxProd.text) <> "" Then
        Set RsDescrEspecif = New ADODB.recordset
        With RsDescrEspecif
            .Source = "SELECT descr_especif FROM sl_especif WHERE " & _
                    "cod_especif=" & UCase(BL_TrataStringParaBD(EcAuxProd.text))
            .ActiveConnection = gConexao
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
        If RsDescrEspecif.RecordCount > 0 Then
            FgProd.TextMatrix(LastRowP, 3) = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            RegistosP(LastRowP).DescrEspecif = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            SELECT_Especif = True
        Else
            BG_Mensagem mediMsgBox, "Especifica��o inexistente!", vbOKOnly + vbInformation, "Especifica��es"
        End If
        RsDescrEspecif.Close
        Set RsDescrEspecif = Nothing
    End If

End Function

Private Sub FgProd_LostFocus()

    FgProd.CellBackColor = vbWhite
    FgProd.CellForeColor = vbBlack
    
End Sub

Private Sub FGTubos_LostFocus()

    FgTubos.CellBackColor = vbWhite
    FgTubos.CellForeColor = vbBlack
    
End Sub

Private Sub FgProd_RowColChange()
    
    Dim tmpCol As Integer
    Dim tmpRow As Integer
    Dim RsEspecif As ADODB.recordset
    
    If ExecutaCodigoP = True Then
        ExecutaCodigoP = False
        
        BtObsEspecif.Enabled = False
        
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Trim(FgProd.TextMatrix(FgProd.row, 2)) <> "" Then
            BtObsEspecif.Enabled = True
        End If
        
        If Trim(FgProd.TextMatrix(LastRowP, 0)) <> "" And Trim(FgProd.TextMatrix(LastRowP, 2)) = "" Then
    
            'Escolher a especifica��o de defeito do produto
            
            Set RsEspecif = New ADODB.recordset
                
            With RsEspecif
                .Source = "select sl_produto.cod_especif, sl_especif.descr_especif from sl_produto, sl_especif where sl_produto.cod_especif = sl_especif.cod_especif and cod_produto = " & BL_TrataStringParaBD(Trim(FgProd.TextMatrix(LastRowP, 0)))
                .CursorLocation = adUseServer
                .CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open , gConexao
            End With
            
            If Not RsEspecif.EOF Then
                FgProd.TextMatrix(LastRowP, 2) = Trim(BL_HandleNull(RsEspecif!cod_Especif, ""))
                FgProd.TextMatrix(LastRowP, 3) = Trim(BL_HandleNull(RsEspecif!descr_especif, ""))
            End If
            
            RsEspecif.Close
        End If
            
        Set RsEspecif = Nothing
        
        tmpCol = FgProd.Col
        tmpRow = FgProd.row
        If FgProd.Col <> LastColP Then FgProd.Col = LastColP
        If FgProd.row <> LastRowP Then FgProd.row = LastRowP
        FgProd.CellBackColor = vbWhite
        FgProd.CellForeColor = vbBlack
        If FgProd.Col <> tmpCol Then FgProd.Col = tmpCol
        If FgProd.row <> tmpRow Then FgProd.row = tmpRow
        
        If FgProd.row <> LastRowP Then
            Preenche_LaProdutos FgProd.row
        End If
        
        ' Controlar as colunas
        If FgProd.Col = 1 Then
            If LastColP >= 2 Then
                FgProd.Col = 0
            Else
                FgProd.Col = 2
            End If
        ElseIf FgProd.Col = 3 Then
            If LastColP >= 5 Then
                FgProd.Col = 2
            Else
                FgProd.Col = 5
            End If
        ElseIf LastColP = 5 And LastRowP = FgProd.row And FgProd.Col <> 6 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        ElseIf FgProd.Col > 5 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        End If
        
        LastColP = FgProd.Col
        LastRowP = FgProd.row
        
        FgProd.CellBackColor = azul
        FgProd.CellForeColor = vbWhite
        
        If LastColP = 0 Then
            BtPesquisaProduto.Enabled = True
            BtPesquisaEspecif.Enabled = False
        ElseIf LastColP = 2 Then
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = True
            BtObsEspecif.Enabled = True
        Else
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = False
        End If
        If gLAB = "CHVNG" Then
            BtPesquisaProduto.Enabled = False
        End If
        ExecutaCodigoP = True
        
    End If

End Sub


Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Me, "")

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_QueryUnload(cancel As Integer, UnloadMode As Integer)

    If UnloadMode <> 1 Then
        If Flg_Gravou = False Then
            If BG_Mensagem(mediMsgBox, "Deseja realmente sair sem gravar?", vbQuestion + vbYesNo, "Sislab") = vbNo Then
                'respondeu n�o
                cancel = True
            End If
        End If
    End If
    If Command <> "" Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoInserir(Optional not_msg As Boolean)

    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    Dim rv As Integer
    'EcPrescricao = ""
    If dataAct = "" Then
        dataAct = Bg_DaData_ADO
    End If
    If EcEpisodio = "0" And UCase(HIS.nome) = "SISBIT" Then
        EcEpisodio = "0."
    End If
    If EcEpisodioAux = "0" And UCase(HIS.nome) = "SISBIT" Then
        EcEpisodioAux = "0."
    End If
    
    If gNReqPreImpressa = 1 And EcNumReq.text = "" Then
        BG_Mensagem mediMsgBox, "N�mero de requisi��o obrigat�rio! ", vbInformation, "Aten��o"
        Exit Sub
    ElseIf gNReqPreImpressa = 1 And EcNumReq.text <> "" Then
        'valida n�mero
        If IsNumeric(EcNumReq.text) = False Then
            BG_Mensagem mediMsgBox, "N�mero inv�lido!", vbInformation, "Aten��o"
            Exit Sub
        ElseIf BL_SelCodigo("sl_requis", "n_req", "n_req", EcNumReq.text) = EcNumReq.text Then
            BG_Mensagem mediMsgBox, "N�mero de requisi��o j� existente!", vbInformation, "Aten��o"
            Exit Sub
        End If
    End If
    If gPreencheDatasReq = mediSim Then
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = dataAct
        End If
        If EcDataChegada.text = "" And EcDataChegada.Enabled = True Then
            EcDataChegada.text = dataAct
        End If
    End If
    
    If Trim(EcEpisodio.text) = "" And gPreencheEpis = 1 Then
        DevolveEpisodio EcSeqUtente.text
    End If
    If EcDataPrevista.text <> "" And EcDataPrevista.Enabled = True Then
'        If DateValue(EcDataPrevista.text) < DateValue(dataAct) Then
'            BG_Mensagem mediMsgBox, "A data prevista tem que ser superior ou igual � data actual ", vbInformation, "Aten��o"
'            EcDataPrevista.text = ""
'            EcDataPrevista.SetFocus
'            Exit Sub
'        End If
    End If
    If EcDataChegada.text <> "" And EcDataChegada.Enabled = True Then
        If DateValue(EcDataChegada.text) > DateValue(dataAct) Then
            BG_Mensagem mediMsgBox, "A data de Chegada tem que ser inferior ou igual � data actual ", vbInformation, "Aten��o"
            Sendkeys ("{HOME}+{END}")
            EcDataChegada.text = ""
            EcDataChegada.SetFocus
            Exit Sub
        End If
    End If
    
    If lObrigaInfoCli = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "I") = False Then
            MsgBox "An�lises marcadas exigem informa��o cl�nica.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    If lObrigaTerap = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "T") = False Then
            MsgBox "An�lises marcadas exigem terap�utica/medica��o.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    
    
    If gHIS_Import = mediSim And EcEpisodio <> "" And gValidaEpisodioInserir = mediSim Then
        If IsNumeric(EcEpisodio) Then
            If CLng(EcEpisodio) > 0 Then
                BtActualizaEpisodio_Click
                If (Trim(EcUtente.text) = "") Then
                    MsgBox "Deve selecionar um Utente.       ", vbInformation, " Actualizar Epis�dio"
                    Exit Sub
                Else
                    If ActualizaProcessoUtente = False Then
                        ActualizaEpisodio = False
                        EcEpisodioAux.text = ""
                        EcEpisodioAux.Visible = False
                        BL_FimProcessamento Me
                        Exit Sub
                    End If
                    EcEpisodio.Enabled = False
                    ActualizaEpisodio = False
                    EcEpisodioAux.text = ""
                    EcEpisodioAux.Visible = False
                End If
            End If
        End If
    End If
    
    ' --------------------------------------------------------------
    ' VERIFICA PARAMETRIZACOES POR LOCAL
    ' --------------------------------------------------------------
    
    If EcCodProveniencia <> "" Then
        If BL_VerificaProvenValida(EcCodProveniencia, CStr(gCodLocal)) = False Then
            EcCodProveniencia.text = ""
            EcDescrProveniencia = ""
            EcCodProveniencia.SetFocus
            Exit Sub
        End If
    End If
    
    If EcCodEFR <> "" Then
        If BL_VerificaEfrValida(EcCodEFR, CStr(gCodLocal)) = False Then
            EcCodEFR.text = ""
            EcDescrEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
        End If
    End If
    
    If EcCodMedico <> "" Then
        If BL_VerificaMedValida(EcCodMedico, CStr(gCodLocal)) = False Then
            EcCodMedico.text = ""
            EcDescrMedico = ""
            EcCodMedico.SetFocus
            Exit Sub
        End If
    End If
    If EcCodSala <> "" Then
        If BL_VerificaSalaValida(EcCodSala, CStr(gCodLocal)) = False Then
            EcCodSala.text = ""
            EcDescrSala = ""
            EcCodSala.SetFocus
            Exit Sub
        End If
    End If
    
    If not_msg = True Then
        gMsgResp = vbYes
    Else
        gMsgTitulo = "Inserir"
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    End If
    
    Me.SetFocus

    If gMsgResp = vbYes Then
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If gAtiva_Reutilizacao_Colheitas = mediSim And EcNumReq.text <> "" Then
            If Bl_VerificaEstadoPendente(Trim(EcNumReq.text)) = True Then
                BG_Mensagem mediMsgBox, "Requisi��o pendente de valida��o para reutiliza��o!", vbError, "ERRO"
                Exit Sub
            End If
        End If
        If EcUtilizadorCriacao = "" Then
            EcUtilizadorCriacao = gCodUtilizador
            EcDataCriacao = dataAct
            EcHoraCriacao = Bg_DaHora_ADO
        End If
        If gMostraColheita = mediSim Then
            If EcDtColheita = "" Then EcDtColheita = EcDataCriacao
            If EcHrColheita = "" Then EcHrColheita = EcHoraCriacao
        End If
        
        'se a data prevista estiver vazia
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = Format(dataAct, gFormatoData)
        End If
        
        ValidaDataPrevista
'        If DateValue(EcDataPrevista.Text) < DateValue(dataAct) Then Exit Sub
        
        ' Obriga a especifica��o de produtos.
        If (gObrigaProduto) Then
            If Not (ValidaProdutos) Then
                MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
                Exit Sub
            End If
        End If
        
        If Len(EcGrupoAna.text) = 1 Then
            If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count > 1 Then
                MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            End If
        End If
        
        If EcReqAssociada = "" Then
            EcReqAssociada = EcNumReq
        End If
        
        'If Not ValidaReqAssoc Then Exit Sub
        
        If gLAB = "HSMARTA" Then
            If EcEpisodio.text = "" And EcEpisodioAux.text <> "" And EcEpisodioAux.text <> "24000594" Then
                ValidaEpisodio
            End If
        End If
        
        'Se o utente � isento, � obrigat�rio indicar o motivo
        If CkIsento.value = vbChecked And CbTipoIsencao.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Tipo de isen��o obrigat�rio!", vbOKOnly + vbInformation, "Insen��es"
            CbTipoIsencao.SetFocus
            Exit Sub
        End If
        If CkIsento.value = vbUnchecked Or CkIsento.value = vbGrayed Then
            CbTipoIsencao.ListIndex = -1
        End If
        
        If Not ValidaEspecifObrig Then Exit Sub
        
        If ValidaNumBenef = True Then
            iRes = ValidaCamposEc
            If iRes = True Then
            
                Call BD_Insert
                'FG ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, "Requisi��o: " & EcNumReq & " Inserida"
                
                If (gFormGesReqCons_Aberto = True) Then
                    ' Quando este form foi invocado atrav�s de FormGesReqCons,
                    ' apaga a marca��o correspondente.
                    
                    rv = MARCACAO_Apaga(FormGesReqCons.EcNumReq.text)
                    FormGesReqCons.LimpaCampos
                End If
                
                If Trim(EcNumReq.text) <> "" Then
                    'Se n�o houve erro
                    If gRequisicaoActiva = 0 Then
                        BtDadosAnteriores.Enabled = False
                        LaCopiarAnterior.Enabled = False
                    Else
                        BtDadosAnteriores.Enabled = True
                        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
                        LaCopiarAnterior.Enabled = True
                    End If
                    BtEtiq.Enabled = True
                    LaEtiq.Enabled = True
                    BtAnexos.Enabled = True
                    ImprimeEtiqNovo
                    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
                    BL_Toolbar_BotaoEstado "Procurar", "Activo"
                    BL_Toolbar_BotaoEstado "Limpar", "Activo"
                    BL_Toolbar_BotaoEstado "Remover", "Activo"
                    'Mart. ergonomico
                    Call FuncaoProcurar
                End If
            End If
        End If
    End If

    'SCMVC-1533
    'SOLIVEIRA 09.10.2007
    If Command <> "" And Flg_Gravou Then
         Dim f As Form
        Do Until etiqFlag = False
            DoEvents
            DoEvents
            For Each f In forms
               If f.Name = "FormNEtiqNTubos" Then
                    etiqFlag = True
                    Exit For
                Else
                    etiqFlag = False
                End If
            Next
        Loop
        Call MDIFormInicio.Registo("Sair")
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim i As Integer
    Dim k As Integer
    Dim req As Long
    Dim ProdJaChegou As Boolean
    Dim prioridade_colheita As String
    Dim rv As Integer
    
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    
    ' NAO GERA NUMERO DE REQUISICAO PARA AS CONSULTAS - SE CHAVE ACTIVA!!!
    If (gFormGesReqCons_Aberto = True) And gSerieUnicaMarcacao = mediSim Then
        req = BL_HandleNull(FormGesReqCons.EcNumReq, -1)
        If req = -1 Or req = "0" Then
            req = reqMarca
        End If
    Else
        If gNReqPreImpressa = 1 Then
            req = EcNumReq.text
        Else
            'Determinar o novo n�mero da requisi��o (TENTA 10 VEZES - LOCK!)
            i = 0
            req = -1
            While req = -1 And i <= 10
                req = BL_GeraNumero("N_REQUIS")
                i = i + 1
            Wend
            
            If req = -1 Then
                BG_Mensagem mediMsgBox, "Erro a gerar n�mero de requisi��o !", vbCritical + vbOKOnly, "ERRO"
                Exit Sub
            End If
        End If
    End If
    
    EcNumReq.text = ""
    EcNumReq.text = req
    
    If (EcEpisodio.text = "") And _
       (gEpisodioIgualNumReq) Then
        EcEpisodio.text = Trim(EcNumReq.text)
    End If
    
    gRequisicaoActiva = CLng(EcNumReq.text)
    EcLocal = gCodLocal
    
    BG_BeginTransaction
    
    ' grava colheita
    If EcNumColheita.text = "" Then
        EcNumColheita.text = COLH_InsereNovaColheita(CLng(EcSeqUtente.text))
    End If
    If EcNumColheita.text = "" Then
        BG_Mensagem mediMsgBox, "Erro ao gravar colheita!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    Else
        If COLH_AssociaReqColheita(EcNumReq.text, EcNumColheita.text, CLng(EcSeqUtente.text)) = "" Then
            BG_Mensagem mediMsgBox, "Erro ao associar requisi��o a colheita!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
    End If
    
    ' Produtos da requisi��o
    Grava_Prod_Req
    If gSQLError <> 0 Then
        'Erro a gravar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Tubos da requisi��o
    'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
    TB_GravaTubosReq EcNumReq.text, EcDataChegada.text
    If gSQLError <> 0 Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    SQLQuery = "UPDATE sl_obs_ana_req SET n_req = " & EcNumReq.text & " WHERE seq_utente = " & EcSeqUtente.text & " AND n_req = '-" & gNumeroSessao & "'"
    BG_ExecutaQuery_ADO SQLQuery
    
    'Actualizar estado da requisi��o em conformidade com an�lises marcadas, produtos marcados e sua data de chegada
    If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or EcEstadoReq = gEstadoReqEsperaProduto Or EcEstadoReq = gEstadoReqEsperaResultados Or EcEstadoReq = gEstadoReqSemAnalises Then
        ProdJaChegou = False
        If FGAna.rows >= 1 And Trim(FGAna.TextMatrix(1, lColFgAnaCodAna)) <> "" Then
            If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                For k = 1 To RegistosP.Count - 1
                    If Trim(RegistosP(1).DtChega) <> "" Then
                        ProdJaChegou = True
                    End If
                Next k
                If ProdJaChegou = True Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                Else
                    EcEstadoReq = gEstadoReqEsperaProduto
                End If
            Else
                EcEstadoReq = gEstadoReqEsperaResultados
                EcDataChegada.text = dataAct
            End If
        Else
            EcEstadoReq = gEstadoReqSemAnalises
        End If
    End If
    
    EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
    EcGrupoAna.text = ""
    
    If TotalAcrescentadas <= 0 And UBound(MaReq) > 0 Then
        BG_LogFile_Erros "Erro ao acrescentar an�lises, por favor marque novamente!", Me.Name, "BD_INSERT", True
    End If
    ' An�lises da requisi��o
    If Grava_Ana_Req = False Then
        'Erro a gravar as an�lises da requisi��o
        BG_LogFile_Erros "Erro a gravar as an�lises da requisi��o !", Me.Name, "BD_INSERT", True
        GoTo Trata_Erro
    End If
    If gSQLError <> 0 Then
        'Erro a gravar as an�lises da requisi��o
        BG_LogFile_Erros "Erro a gravar as an�lises da requisi��o !", Me.Name, "BD_INSERT", True
        GoTo Trata_Erro
    End If
    
    ' Dados do P1 da ARS (caso a entidade seja ARS)
    Call Grava_Ana_ARS
    If gSQLError <> 0 Then
        'Erro a gravar dados do P1 da ARS
        BG_LogFile_Erros "Erro a gravar as requisi��es gARS !", Me.Name, "BD_INSERT", True
        GoTo Trata_Erro
    End If
    
    
    ' Diagn�sticos secund�rios e terapeuticas/medica��o
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar diagn�sticos secund�rios e terapeuticas/medica��o
        BG_LogFile_Erros "Erro a actualizar diagn�sticos secund�rios e terap�uticas/medica��o!", Me.Name, "BD_INSERT", True
        GoTo Trata_Erro
    End If
    
    'Grava questoes das analises
    If GravaQuestoesAna(EcNumReq) = False Then
        BG_LogFile_Erros "Erro ao gravar Quest�es das an�lises!", Me.Name, "BD_INSERT", True
        GoTo Trata_Erro
    End If
    
    
    'Grava na agenda
    Grava_Agenda
    If gSQLError <> 0 Then
        BG_LogFile_Erros "Erro a gravar agenda!", Me.Name, "BD_INSERT", True
        GoTo Trata_Erro
    End If
    
    ' RECIBOS DA REQUISICAO
    If gPrecosAmbienteHospitalar = mediSim Then
        If RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False) = False Then
            'Erro a gravar os tubos da requisi��o
            BG_Mensagem mediMsgBox, "Erro a gravar os pre�os da requisi��o !", vbError, "ERRO"
            GoTo Trata_Erro
        End If
    End If
    
    'GRAVA PERFIS MARCADOS
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_LogFile_Erros "Erro ao gravar perfis marcados", Me.Name, "BD_INSERT", True
        GoTo Trata_Erro
    End If
    
    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If

    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)
    
    ' Gravar os restantes dados da requisi��o
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    If gLAB = "ICIL" Then ' DISPARAR TRIGGER
        SQLQuery = " UPDATE SL_MARCACOES SET n_req = n_req WHERE n_req = " & EcNumReq
        BG_ExecutaQuery_ADO SQLQuery
    End If
    
    
    'fila de espera
    If CbPrioColheita.ListIndex > mediComboValorNull Then
        prioridade_colheita = CbPrioColheita.ItemData(CbPrioColheita.ListIndex)
    Else
        prioridade_colheita = ""
    End If
    FE_InsereFilaEspera EcNumReq.text, CLng(EcSeqUtente.text), EcCodSala.text, prioridade_colheita, EcCodProveniencia.text
    

    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        BG_CommitTransaction
        gRequisicaoActiva = CLng(EcNumReq.text)
        Flg_Gravou = True
        
        EnviaFact_ARS
    End If
    
    Exit Sub
       
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a inserir requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicao: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar
    Exit Sub
    Resume Next
End Sub

Function Grava_Ana_Req() As Boolean
    On Error GoTo TrataErro
    Grava_Ana_Req = False
    Dim quantidade As Integer
    Dim i As Integer
    Dim k As Integer
    Dim maximo As Long
    Dim sql As String
    Dim RsOrd As ADODB.recordset
    Dim CodAnaSONHO As String
    Dim CodAnaGH As String
    Dim HisAberto As Integer
    Dim analises As String
    Dim rsApagaANa As ADODB.recordset
    Dim conta2 As Integer
    Dim flg
    Dim CodLocalFact As Long
    Set RsOrd = New ADODB.recordset
    Dim RsFact As New ADODB.recordset
    Dim flg_realiza As Boolean
    Dim l As Integer
    Dim seqReqTubo As String
    
    flg_realiza = False
    CodLocalFact = -1
    
    If RegistaEliminadas = False Then
        BG_Mensagem mediMsgBox, "Erro ao registar an�lises eliminadas!", vbInformation, "Grava��o de an�lises"
        Grava_Ana_Req = False
        Exit Function
    End If
    If RegistaTubosEliminados = False Then
        BG_Mensagem mediMsgBox, "Erro ao registar tubos eliminados!", vbInformation, "Grava��o de an�lises"
        Grava_Ana_Req = False
        Exit Function
    End If
    If RegistaAcrescentadas = False Then
        BG_Mensagem mediMsgBox, "Erro ao adicionar Anal�lises acrescentadas!", vbInformation, "Grava��o de an�lises"
        Grava_Ana_Req = False
        Exit Function
    End If
     
    'BRUNODSANTOS GLINTT-HS-15750 04.01.2018
    If gUsaEnvioPDS = mediSim Then
        Call VerificaAnalisesEviarPDS(EcNumReq.text)
    End If
    '
     
    'FGONCALVES - HMP
    If ((gEnviaFact_GesReq = 1) And _
        (Len(EcEpisodio.text) > 0) And EcEpisodio.text <> "0" And _
        (CbSituacao.ListIndex <> -1) And EFR_Verifica_ARS(EcCodEFR.text) = False And EFR_Verifica_ADSE(EcCodEFR.text) = False And _
        EcDataChegada.text <> "") Then
        
        If UCase(HIS.nome) = UCase("SONHO") Then
            If gHIS_Import = 1 Then
                HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
            Else
                HisAberto = 0
            End If
            'necess�rio quando existe database links
            gConnHIS.BeginTrans
        Else
            HisAberto = 1
        End If
    End If
    
    If UBound(MaReq) = 0 Then
        BG_Mensagem mediMsgBox, "N�o foram registadas an�lises!", vbInformation, "Grava��o de an�lises"
        SSTGestReq.Tab = 2
    End If
    
    If gEnviaLocalSONHO = mediSim Then
        CodLocalFact = BL_HandleNull(BL_SelCodigo("GR_EMPR_INST", "COD_FACT", "COD_EMPR", CStr(BL_HandleNull(EcLocal, -1))), -1)
    End If
    '-------------------------------------------------------------------------------
    ' FACTURACAO DA ANALISE Z..... PARA AS CONSULTAS - HFM
    '-------------------------------------------------------------------------------
    If ((gEnviaFact_GesReq = 1) And (gCodAnaSONHO <> "" And gCodAnaSONHO <> "-1") And _
        (Len(EcEpisodio.text) > 0) And EcEpisodio.text <> "0" And _
        (CbSituacao.ListIndex <> -1) And EFR_Verifica_ARS(EcCodEFR.text) = False And EFR_Verifica_ADSE(EcCodEFR.text) = False And EcDataChegada.text <> "") Then
            If CbSituacao.ItemData(CbSituacao.ListIndex) = gT_Consulta Then
                If IF_Factura_Analise(EcNumReq.text, _
                                            EcCodEFR.text, _
                                            CbSituacao.ItemData(CbSituacao.ListIndex), _
                                            EcEpisodio.text, _
                                            IIf(EcDataChegada.text <> "", EcDataChegada.text, IIf(CDate(dataAct) > CDate(EcDataPrevista.text), dataAct, EcDataPrevista.text)), _
                                            EcUtente, _
                                            EcCodProveniencia.text, _
                                            "0", _
                                            "0", _
                                            "S99999", _
                                            "0", "", CbTipoUtente, EcUtente.text, Empty, CodLocalFact, Empty, 1, EcCodMedico, "", "") = True Then
                End If
            End If
    End If
    '-------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------
        
    'Marca��o de an�lises (sl_marcacoes)
    For i = 1 To UBound(MaReq)
        MaReq(i).dt_chega = EcDataChegada.text  'Bruno & sdo
        
        'S� s�o gravadas as an�lise "novas" ou que estavam na tabela de marca��es, as an�lises da tabela de realiza��es n�o s�o marcadas
            '-1 -> an�lise nova ou an�lise da tabela sl_marca�oes
            
        CmdOrdAna.Parameters("COD_ANA_S") = MaReq(i).cod_agrup
        CmdOrdAna.Parameters("COD_ANA_C") = MaReq(i).cod_agrup
        CmdOrdAna.Parameters("COD_PERFIS") = MaReq(i).cod_agrup
        
        
        Set RsOrd = CmdOrdAna.Execute
        
        If Not RsOrd.EOF Then
            If BL_HandleNull(RsOrd!ordem, 0) <> 0 Then
                MaReq(i).Ord_Ana = BL_HandleNull(RsOrd!ordem, 0)
            Else
                maximo = 0
                For k = 1 To UBound(MaReq)
                    If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
                Next k
                maximo = maximo + 1
                'Colocar a an�lise para o fim
                MaReq(i).Ord_Ana = maximo
            End If
            If RsOrd!gr_ana <> "" Then
                If EcGrupoAna.text = "" Then
                    EcGrupoAna.text = RsOrd!gr_ana
                Else
                    If InStr(1, EcGrupoAna.text, RsOrd!gr_ana) <> 0 Then
                    Else
                        EcGrupoAna.text = EcGrupoAna.text & ";" & RsOrd!gr_ana
                    End If
                End If
            End If
            
            'Envia an�lises para a factura��o
            
            quantidade = RetornaQtdFact(MaReq(i).cod_agrup, EcCodEFR.text)
            If ((gEnviaFact_GesReq = 1) And _
                (Len(EcEpisodio.text) > 0) And EcEpisodio.text <> "0" And _
                (CbSituacao.ListIndex <> -1) And EFR_Verifica_ARS(EcCodEFR.text) = False And EFR_Verifica_ADSE(EcCodEFR.text) = False And EcDataChegada.text <> "") Then
                
'                If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
                    
                    '----------------------------------------------------
                    'soliveira 18.09.2007
                    If MaReq(i).Flg_Facturado = 0 And ANALISE_Verifica_MICRO(MaReq(i).cod_ana_s) = False And ANALISE_Verifica_AnaNaoFacturaGESREQ(MaReq(i).cod_ana_s) = False Then
                        If IF_Factura_Analise(EcNumReq.text, _
                                                    EcCodEFR, _
                                                    CbSituacao.ItemData(CbSituacao.ListIndex), _
                                                    EcEpisodio.text, _
                                                    IIf(EcDataChegada.text <> "", EcDataChegada.text, IIf(CDate(dataAct) > CDate(EcDataPrevista.text), dataAct, EcDataPrevista.text)), _
                                                    EcUtente, _
                                                    EcCodProveniencia.text, _
                                                    MaReq(i).Cod_Perfil, _
                                                    MaReq(i).cod_ana_c, _
                                                    MaReq(i).cod_ana_s, _
                                                    MaReq(i).cod_agrup, "", CbTipoUtente, EcUtente.text, Empty, CodLocalFact, Empty, quantidade, EcCodMedico, "", "") = True Then
                            MaReq(i).Flg_Facturado = 1
                        Else
                            If gAnalise_Ja_Facturada = True Then
                                MaReq(i).Flg_Facturado = 1
                            End If
                        End If
                    End If
                    '----------------------------------------------------
            
            ElseIf gSISTEMA_FACTURACAO = cFACTURACAO_FACTUS And gF_TRATAREJ = 1 Then
                MaReq(i).Flg_Facturado = 0

            End If
                

        Else
            'Colocar a an�lise para o fim
            maximo = 0
            For k = 1 To UBound(MaReq)
                If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
            Next k
            maximo = maximo + 1
            'Colocar a an�lise para o fim
            MaReq(i).Ord_Ana = maximo
        End If
        RsOrd.Close
        
        'MaReq(i).Ord_Ana = i
        
        If MaReq(i).flg_estado = "-1" Then
            If VerificaAnaliseJaExisteRealiza(CLng(Trim(EcNumReq.text)), UCase((MaReq(i).Cod_Perfil)), UCase((MaReq(i).cod_ana_c)), UCase((MaReq(i).cod_ana_s)), True) > 0 Then
                flg_realiza = True
            Else
                Dim RsTrans As ADODB.recordset
                'verifica se a flag "flg_transmanual" na an�lise est� a 1,
                'se sim coloca "flg_apar_trans" a 1 em sl_marcacoes
                Set RsTrans = New ADODB.recordset
                RsTrans.CursorLocation = adUseServer
                RsTrans.CursorType = adOpenStatic
                sql = "SELECT flg_transmanual FROM sl_ana_s WHERE cod_ana_s=" & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                RsTrans.Open sql, gConexao
                
                If (Not (RsTrans.EOF)) Then
                    If (RsTrans!flg_transManual = 1 And MaReq(i).Flg_Apar_Transf = 0) Then
                        MaReq(i).Flg_Apar_Transf = 2
                    End If
                End If
                If MaReq(i).seq_req_tubo > mediComboValorNull Then
                    seqReqTubo = MaReq(i).seq_req_tubo
                Else
                    seqReqTubo = " NULL "
                End If
                
                If VerificaAnaMarcada(CLng(Trim(EcNumReq.text)), MaReq(i).Cod_Perfil, MaReq(i).cod_ana_c, MaReq(i).cod_ana_s) = True Then
                    sql = "UPDATE sl_marcacoes set ord_ana = " & MaReq(i).Ord_Ana
                    sql = sql & ",ord_ana_compl = " & MaReq(i).Ord_Ana_Compl & ", "
                    sql = sql & " ord_ana_perf  =" & MaReq(i).Ord_Ana_Perf & ", "
                    sql = sql & " n_folha_trab = " & MaReq(i).N_Folha_Trab & ","
                    sql = sql & " dt_chega = " & BL_TrataDataParaBD(MaReq(i).dt_chega) & ","
                    sql = sql & " flg_apar_trans = " & UCase(BL_TrataStringParaBD(MaReq(i).Flg_Apar_Transf)) & ", "
                    sql = sql & " ord_marca = " & MaReq(i).Ord_Marca & ","
                    sql = sql & " flg_facturado = " & MaReq(i).Flg_Facturado & ","
                    sql = sql & " transmit_psm = " & MaReq(i).transmit_psm & ","
                    sql = sql & " n_envio = " & MaReq(i).n_envio & ","
                    sql = sql & " seq_req_tubo = " & seqReqTubo
                    sql = sql & " WHERE n_req =" & EcNumReq.text
                    sql = sql & " AND cod_perfil = " & BL_TrataStringParaBD(MaReq(i).Cod_Perfil)
                    sql = sql & " AND cod_ana_c = " & BL_TrataStringParaBD(MaReq(i).cod_ana_c)
                    sql = sql & " AND cod_ana_S = " & BL_TrataStringParaBD(MaReq(i).cod_ana_s)
                Else
                    sql = "INSERT INTO sl_marcacoes(n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,flg_apar_trans,ord_marca, flg_facturado, transmit_psm, n_envio, seq_req_tubo "
                    sql = sql & ") VALUES (" & CLng(Trim(EcNumReq.text)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s)) & "," & MaReq(i).Ord_Ana
                    sql = sql & "," & MaReq(i).Ord_Ana_Compl & "," & MaReq(i).Ord_Ana_Perf & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_agrup)) & "," & MaReq(i).N_Folha_Trab & "," & IIf((MaReq(i).dt_chega = ""), "NULL", BL_TrataDataParaBD(MaReq(i).dt_chega)) & ","
                    sql = sql & UCase(BL_TrataStringParaBD(MaReq(i).Flg_Apar_Transf)) & "," & MaReq(i).Ord_Marca & ", " & MaReq(i).Flg_Facturado & "," & MaReq(i).transmit_psm & "," & MaReq(i).n_envio & "," & seqReqTubo & " )"
                End If
                BG_ExecutaQuery_ADO sql
                
                If gSQLError <> 0 Then
                    GoTo TrataErro
                End If
                
                RsTrans.Close
                Set RsTrans = Nothing
            End If
        'soliveira 08-09-2003
        'para alterar a flag flg_facturado quando as analises j� se encontram na tabela sl_realiza
        'ElseIf MaReq(i).Flg_Facturado = 1 Then 'alterei por causa do tratamento de rejeitados
        Else
            If EcEstadoReq = gEstadoReqHistorico Then
                sql = "UPDATE sl_realiza_h set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & CLng(Trim(EcNumReq.text)) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            Else
                sql = "UPDATE sl_realiza set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & CLng(Trim(EcNumReq.text)) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            End If
            BG_ExecutaQuery_ADO sql
        End If
        
        '----------------
        If InStr(1, analises, BL_TrataStringParaBD(MaReq(i).cod_agrup)) = 0 Then
            analises = analises & IIf(BL_HandleNull(MaReq(i).cod_agrup, "") <> "", BL_TrataStringParaBD(MaReq(i).cod_agrup) & ",", "")
        End If
    Next i
    If analises <> "" Then
        analises = Mid(analises, 1, Len(analises) - 1)
    End If
    
    
    If UCase(HIS.nome) = UCase(cFACTURACAO_NOVAHIS) And EcEpisodio <> "" Then
        If CbTipoUtente = BL_SelCodigo("GR_EMPR_INST", "T_UTENTE", "EMPRESA_ID", CStr(gCodLocal), "V") Then
            IN_AlteraEstadoSISLAB EcUtente, EcNumReq, analises
        End If
    End If
    
    If gEnviaFact_GesReq = 1 And EcDataChegada.text <> "" Then
        If analises <> "" Then
            'Retirar a virgula
    
            If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then

                'Insere (Retorno = 'A') na tabela do sonho as an�lises apagadas do sislab j� facturadas anteriormente
                Set rsApagaANa = New ADODB.recordset
                rsApagaANa.CursorLocation = adUseServer
                rsApagaANa.CursorType = adOpenStatic
                sql = " SELECT cod_modulo,num_episodio,num_processo,dta_episodio,cod_especialidade,cod_analise,quantidade,dta_analise " & _
                      " FROM sl_analises_in " & _
                      " WHERE cod_agrup not in (" & analises & ") and n_req = " & EcNumReq & " AND  cod_analise <> " & BL_TrataStringParaBD(gCodAnaSONHO)
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                rsApagaANa.Open sql, gConexao
                While Not rsApagaANa.EOF
                    If (SONHO_Remove_ANALISES_IN(BL_HandleNull(rsApagaANa!cod_modulo, ""), _
                                   BL_HandleNull(rsApagaANa!num_episodio, ""), _
                                   BL_HandleNull(rsApagaANa!num_processo, ""), _
                                   BL_HandleNull(rsApagaANa!cod_especialidade, ""), _
                                   BL_HandleNull(rsApagaANa!cod_analise, ""), _
                                   BL_HandleNull(rsApagaANa!dta_analise, ""), _
                                   EcNumReq) = 1) Then
                    End If
                    rsApagaANa.MoveNext
                Wend
                rsApagaANa.Close
                Set rsApagaANa = Nothing
            
            ElseIf (UCase(HIS.nome) = UCase("SISBIT")) Then
    
                Set RsFact = New ADODB.recordset
                If analises <> "" Then
                    sql = "SELECT r.t_sit, r.n_epis,f.num_processo,f.cod_especialidade,f.n_req,f.cod_analise, f.dta_analise, "
                    sql = sql & " r.cod_efr, f.cod_ana_sislab, f.cod_perfil, f.cod_ana_c, f.cod_ana_s, f.cod_agrup,f.centro_custo "
                    sql = sql & " FROM sl_analises_in f, sl_requis r WHERE f.cod_agrup not in (" & analises & ") AND  f.n_req = r.n_Req AND r.n_req = " & gRequisicaoActiva
                Else
                    sql = "SELECT r.t_sit, r.n_epis,f.num_processo,f.cod_especialidade,f.n_req,f.cod_analise, f.dta_analise, "
                    sql = sql & " r.cod_efr, f.cod_ana_sislab, f.cod_perfil, f.cod_ana_c, f.cod_ana_s, f.cod_agrup,f.centro_custo "
                    sql = sql & " FROM sl_analises_in f, sl_requis r WHERE  f.n_req = r.n_Req AND r.n_req = " & gRequisicaoActiva
                End If
                RsFact.CursorType = adOpenStatic
                RsFact.CursorLocation = adUseServer
                RsFact.Open sql, gConexao
                If RsFact.RecordCount > 0 Then
                    While Not RsFact.EOF
                        For l = 1 To 1000
                            If (SISBIT_Remove_ANALISES_IN(BL_HandleNull(RsFact!t_sit, ""), _
                                           BL_HandleNull(RsFact!n_epis, ""), _
                                           BL_HandleNull(RsFact!num_processo, ""), _
                                           BL_HandleNull(RsFact!cod_especialidade, ""), _
                                           BL_HandleNull(RsFact!cod_analise, ""), _
                                           BL_HandleNull(RsFact!dta_analise, ""), _
                                           BL_HandleNull(RsFact!n_req, -1), _
                                           BL_HandleNull(RsFact!cod_efr, ""), _
                                           BL_HandleNull(RsFact!cod_ana_sislab, ""), _
                                           BL_HandleNull(RsFact!Cod_Perfil, "0"), _
                                           BL_HandleNull(RsFact!cod_ana_c, "0"), _
                                           BL_HandleNull(RsFact!cod_ana_s, "0"), _
                                           BL_HandleNull(RsFact!cod_agrup, "0"), "", "", "", "", _
                                           BL_HandleNull(RsFact!centro_custo, ""), gCodLocal)) = 1 Then
                                Exit For
                            End If
                        Next l
                        RsFact.MoveNext
                    Wend
                End If
                RsFact.Close
                Set RsFact = Nothing
            ElseIf UCase(HIS.nome) = "GH" Then
            
                Set rsApagaANa = New ADODB.recordset
                rsApagaANa.CursorLocation = adUseServer
                rsApagaANa.CursorType = adOpenStatic
                sql = " SELECT * " & _
                      " FROM sl_dados_fact " & _
                      " WHERE cod_agrup not in (" & analises & ") and n_req = " & EcNumReq & _
                      " AND n_seq not in (select n_seq_anul from sl_dados_fact where n_seq_anul is not null and n_req = " & EcNumReq & ") and n_seq_anul is null"
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                rsApagaANa.Open sql, gConexao
                While Not rsApagaANa.EOF
                    
                    If (GH_Remove_ANALISES_IN(rsApagaANa!t_episodio, _
                                                       BL_HandleNull(rsApagaANa!episodio, "-1"), _
                                                       rsApagaANa!t_doente, _
                                                       rsApagaANa!doente, _
                                                       rsApagaANa!cod_acto, _
                                                       rsApagaANa!n_req, _
                                                       rsApagaANa!dt_ini_acto, BL_HandleNull(rsApagaANa!n_mecan, ""), , rsApagaANa!cod_serv_req, , , , rsApagaANa!n_analise, _
                                                       BL_HandleNull(rsApagaANa!cod_agrup, ""), BL_HandleNull(rsApagaANa!Cod_Perfil, ""), BL_HandleNull(rsApagaANa!cod_ana_c, ""), BL_HandleNull(rsApagaANa!cod_ana_s, ""), _
                                                       BL_HandleNull(rsApagaANa!cod_micro, ""), BL_HandleNull(rsApagaANa!Cod_Gr_Antibio, ""), BL_HandleNull(rsApagaANa!cod_antibio, ""), BL_HandleNull(rsApagaANa!Cod_Prova, ""), _
                                                       rsApagaANa!n_seq) = 1) Then
                    End If
                    rsApagaANa.MoveNext
                Wend
                rsApagaANa.Close
                Set rsApagaANa = Nothing
            End If
            
        Else
            If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
                'Insere (Retorno = 'A') na tabela do sonho as an�lises apagadas do sislab j� facturadas anteriormente
                Set rsApagaANa = New ADODB.recordset
                rsApagaANa.CursorLocation = adUseServer
                rsApagaANa.CursorType = adOpenStatic
                sql = " SELECT cod_modulo,num_episodio,num_processo,dta_episodio,cod_especialidade,cod_analise,quantidade,dta_analise " & _
                      " FROM sl_analises_in " & _
                      " WHERE n_req = " & EcNumReq
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                rsApagaANa.Open sql, gConexao
                While Not rsApagaANa.EOF
                    If (SONHO_Remove_ANALISES_IN(BL_HandleNull(rsApagaANa!cod_modulo, ""), _
                                   BL_HandleNull(rsApagaANa!num_episodio, ""), _
                                   BL_HandleNull(rsApagaANa!num_processo, ""), _
                                   BL_HandleNull(rsApagaANa!cod_especialidade, ""), _
                                   BL_HandleNull(rsApagaANa!cod_analise, ""), _
                                   BL_HandleNull(rsApagaANa!dta_analise, ""), _
                                   EcNumReq) = 1) Then
                    End If
                    rsApagaANa.MoveNext
                Wend
                rsApagaANa.Close
                Set rsApagaANa = Nothing
            ElseIf (UCase(HIS.nome) = UCase("SISBIT")) Then
    
                Set RsFact = New ADODB.recordset
                sql = "SELECT r.t_sit, r.n_epis,f.num_processo,f.cod_especialidade,f.n_req,f.cod_analise, f.dta_analise, "
                sql = sql & " r.cod_efr, f.cod_ana_sislab, f.cod_perfil, f.cod_ana_c, f.cod_ana_s, f.cod_agrup,f.centro_custo "
                sql = sql & " FROM sl_analises_in f, sl_requis r WHERE f.cod_agrup not in (" & analises & ") AND  f.n_req = r.n_Req AND r.n_req = " & gRequisicaoActiva
                
                RsFact.CursorType = adOpenStatic
                RsFact.CursorLocation = adUseServer
                RsFact.Open sql, gConexao
                If RsFact.RecordCount > 0 Then
                    While Not RsFact.EOF
                        For l = 1 To 1000
                            If (SISBIT_Remove_ANALISES_IN(BL_HandleNull(RsFact!t_sit, ""), _
                                           BL_HandleNull(RsFact!n_epis, ""), _
                                           BL_HandleNull(RsFact!num_processo, ""), _
                                           BL_HandleNull(RsFact!cod_especialidade, ""), _
                                           BL_HandleNull(RsFact!cod_analise, ""), _
                                           BL_HandleNull(RsFact!dta_analise, ""), _
                                           BL_HandleNull(RsFact!n_req, -1), _
                                           BL_HandleNull(RsFact!cod_efr, ""), _
                                           BL_HandleNull(RsFact!cod_ana_sislab, ""), _
                                           BL_HandleNull(RsFact!Cod_Perfil, "0"), _
                                           BL_HandleNull(RsFact!cod_ana_c, "0"), _
                                           BL_HandleNull(RsFact!cod_ana_s, "0"), _
                                           BL_HandleNull(RsFact!cod_agrup, "0"), "", "", "", "", _
                                           BL_HandleNull(RsFact!centro_custo, ""), gCodLocal)) = 1 Then
                                Exit For
                            End If
                        Next l
                        RsFact.MoveNext
                    Wend
                End If
                RsFact.Close
                Set RsFact = Nothing
                
            ElseIf UCase(HIS.nome) = "GH" Then
                '---------------------------------------------------------------------
                Set rsApagaANa = New ADODB.recordset
                rsApagaANa.CursorLocation = adUseServer
                rsApagaANa.CursorType = adOpenStatic
                sql = " SELECT * " & _
                      " FROM sl_dados_fact " & _
                      " WHERE n_req = " & EcNumReq & _
                      " AND n_seq not in (select n_seq_anul from sl_dados_fact where n_seq_anul is not null and n_req = " & EcNumReq & ") and n_seq_anul is null"
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                rsApagaANa.Open sql, gConexao
                While Not rsApagaANa.EOF
                    If (GH_Remove_ANALISES_IN(rsApagaANa!t_episodio, _
                                                       rsApagaANa!episodio, _
                                                       rsApagaANa!t_doente, _
                                                       rsApagaANa!doente, _
                                                       rsApagaANa!cod_acto, _
                                                       rsApagaANa!n_req, _
                                                       rsApagaANa!dt_ini_acto, rsApagaANa!n_mecan, , _
                                                       rsApagaANa!cod_serv_req, , , , rsApagaANa!n_analise, _
                                                       BL_HandleNull(rsApagaANa!cod_agrup, ""), BL_HandleNull(rsApagaANa!Cod_Perfil, ""), BL_HandleNull(rsApagaANa!cod_ana_c, ""), BL_HandleNull(rsApagaANa!cod_ana_s, ""), _
                                                       BL_HandleNull(rsApagaANa!cod_micro, ""), BL_HandleNull(rsApagaANa!Cod_Gr_Antibio, ""), BL_HandleNull(rsApagaANa!cod_antibio, ""), BL_HandleNull(rsApagaANa!Cod_Prova, ""), _
                                                       rsApagaANa!n_seq) = 1) Then
                    End If
                    rsApagaANa.MoveNext
                Wend
                rsApagaANa.Close
                Set rsApagaANa = Nothing
                '-------------------------------------------------------------------------
            End If
            ContP1 = 1
        End If
    End If
    
    If flg_realiza = True Then
        BG_Mensagem mediMsgBox, "Foram actualizados resultados durante a altera��o. Por favor confirme se as altera��es pretendidas foram efectuadas.", vbInformation, "Aten��o"
    End If
    gCodAna_aux = ""
    gReq_aux = ""

    Set RsOrd = Nothing
    
    
    'necess�rio quando existe database links (HDM)
    If ((gEnviaFact_GesReq = 1) And _
        (Len(EcEpisodio.text) > 0) And EcEpisodio.text <> "0" And _
        (CbSituacao.ListIndex <> -1)) And UCase(HIS.nome) = UCase("SONHO") And EcDataChegada.text <> "" Then
        
        gConnHIS.CommitTrans
    End If
    BL_Fecha_conexao_HIS
    Grava_Ana_Req = True
Exit Function
TrataErro:
    Grava_Ana_Req = False
    BG_LogFile_Erros "Grava_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Ana_Req"
    Exit Function
    Resume Next
End Function

Sub Grava_Prod_Req()
    
    Dim sql As String
    Dim i As Integer
    Dim MenorData As String
    Dim DataPrevistaAux As String
    Dim TotalRegistos As Long
    Dim Perguntou As Boolean
     
    Perguntou = False
    If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
        'determinar a menor data da lista de produtos
        MenorData = RegistosP(1).DtPrev
        For i = 1 To RegistosP.Count - 1
            If (RegistosP(i).DtChega) <> "" Then
                If DateValue(RegistosP(i).DtPrev) < DateValue(MenorData) And DateValue(RegistosP(i).DtChega) > DateValue(EcDataPrevista) Then MenorData = RegistosP(i).DtPrev
            End If
        Next i
        If DateValue(MenorData) > DateValue(EcDataPrevista.text) Then
            gMsgTitulo = "ATEN��O"
            gMsgMsg = "Existe um produto com data inferior � data prevista, deseja alterar a data prevista para " & MenorData & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                DataPrevistaAux = EcDataPrevista.text
                EcDataPrevista = DateValue(MenorData)
                sql = "UPDATE sl_requis SET dt_previ=" & BL_TrataDataParaBD(EcDataPrevista.text) & " WHERE dt_previ=" & BL_TrataDataParaBD(DataPrevistaAux) & " AND n_req=" & EcNumReq.text
                BG_ExecutaQuery_ADO sql, gConexao
                If gSQLError <> 0 Then Exit Sub
            End If
        End If
        
        TotalRegistos = RegistosP.Count - 1
        For i = 1 To TotalRegistos
            If Trim(RegistosP(i).CodProd) <> "" Then
                If RegistosP(i).DtChega = "" And EcDataChegada.text <> "" Then
                    'se a data de chegada estiver vazia vai preenche-la
                    If Flg_PreencherDtChega = True Then
                        RegistosP(i).DtChega = DateValue(EcDataChegada.text)
                        FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
                    ElseIf Perguntou = False Then
                        Perguntou = True
                    End If
                End If
                sql = "INSERT INTO sl_req_prod(n_req,cod_prod,cod_especif,dt_previ,dt_chega, volume, obs_especif) VALUES(" & EcNumReq.text & _
                    "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodProd)) & "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodEspecif)) & "," & BL_TrataDataParaBD(RegistosP(i).DtPrev) & "," & BL_TrataDataParaBD(RegistosP(i).DtChega) & ", " & BL_TrataStringParaBD(RegistosP(i).Volume) & ", " & BL_TrataStringParaBD(RegistosP(i).ObsEspecif) & ")"
                BG_ExecutaQuery_ADO sql
                
                If gSQLError <> 0 Then
                    i = TotalRegistos + 1
                End If
            End If
        Next i
    
        If gSQLError <> 0 Then Exit Sub
        
        'Actualizar o estado da requisi��o se foi introduzida data de chegada (dt_chega)
        If Flg_PreencherDtChega Then
            sql = "UPDATE sl_requis SET estado_req='A' WHERE n_req=" & EcNumReq.text
            BG_ExecutaQuery_ADO sql
        End If
    End If

End Sub


Sub Grava_DS_TM()

    Dim sql As String

    'Faz o UPDATE na tabela diagn�sticos secund�rios criados temporariamente com o NumeroSessao
    sql = "UPDATE sl_diag_sec SET n_req=" & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    If gSQLError = 0 Then
        'Faz o UPDATE na tabela terap�uticas e medica��o criados temporariamente com o NumeroSessao
        sql = "UPDATE sl_tm_ute SET n_req=" & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
    End If
    
    If gSQLError = 0 Then
        'Faz o UPDATE na tabela de motivos da requisi��o criados temporariamente com o NumeroSessao
        sql = "UPDATE sl_motivo_req SET n_req= " & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
    End If
    
    If gSQLError = 0 Then
        'Faz o UPDATE na tabela de m�dicos da requisi��o criados temporariamente com o NumeroSessao
        sql = "UPDATE sl_medicos_req SET n_req= " & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
    End If
    

    
    
End Sub

Sub Apaga_DS_TM()
    
    Dim sql As String

    'Delete dos diagnosticos secund�rios criados temporariamente com o NumeroSessao
    sql = "DELETE FROM sl_diag_sec " & _
          "WHERE n_req=-" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    'BG_LogFile_Erros "FormGestaoRequisicao: apagar diag. secund�rios -> (" & Sql & " ) ErrBD " & gSQLError
    
    'Delete das terap�uticas e medica��es criados temporariamente com o NumeroSessao
    sql = "DELETE FROM sl_tm_ute " & _
          "WHERE n_req=-" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    'Delete dos motivos da requisi��o criados temporariamente com o NumeroSessao
    sql = "DELETE FROM sl_motivo_req " & _
          "WHERE n_req=-" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    'Delete dos medicos da requisi��o criados temporariamente com o NumeroSessao
    sql = "DELETE FROM sl_medicos_req " & _
          "WHERE n_req=-" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    
        
End Sub


Sub FuncaoModificar()
    
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    
    If EcEpisodio = "0" And UCase(HIS.nome) = "SISBIT" Then
        EcEpisodio = "0."
    End If
    If EcEpisodioAux = "0" And UCase(HIS.nome) = "SISBIT" Then
        EcEpisodioAux = "0."
    End If
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    ' Obriga a especifica��o de produtos.
    If (gObrigaProduto) Then
        If Not (ValidaProdutos) Then
            MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                "A modifica��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
        For i = 1 To FGAna.rows - 2
            If IdentificaGarrafa(FGAna.TextMatrix(i, lColFgAnaCodAna)) = True Then
                If BG_Mensagem(mediMsgBox, "N�o foi identificada a garrafa de hemocultura." & vbCrLf & "Deseja continuar? ", vbYesNo, "GARRAFA HEMOCULTURA") = vbNo Then Exit Sub
            End If
        Next
    End If
    If Len(EcGrupoAna.text) = 1 Then
        If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count - 1 > 1 Then
            MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
    End If
    
    If gPreencheDatasReq = mediSim Then
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = dataAct
        End If
        If EcDataChegada.text = "" And EcDataChegada.Enabled = True Then
            EcDataChegada.text = dataAct
        End If
    End If
    
    If lObrigaInfoCli = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "I") = False Then
            MsgBox "An�lises marcadas exigem informa��o cl�nica.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    If lObrigaTerap = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "T") = False Then
            MsgBox "An�lises marcadas exigem terap�utica/medica��o.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    
    If gHIS_Import = mediSim And EcEpisodio <> "" And gValidaEpisodioInserir = mediSim Then
        If IsNumeric(EcEpisodio) Then
            If CLng(EcEpisodio) > 0 Then
                BtActualizaEpisodio_Click
                If (Trim(EcUtente.text) = "") Then
                    MsgBox "Deve selecionar um Utente.       ", vbInformation, " Actualizar Epis�dio"
                    Exit Sub
                Else
                    If ActualizaProcessoUtente = False Then
                        ActualizaEpisodio = False
                        EcEpisodioAux.text = ""
                        EcEpisodioAux.Visible = False
                        BL_FimProcessamento Me
                        Exit Sub
                    End If
                    EcEpisodio.Enabled = False
                    ActualizaEpisodio = False
                    EcEpisodioAux.text = ""
                    EcEpisodioAux.Visible = False
                End If
            End If
        End If
    End If
    
    ' --------------------------------------------------------------
    ' VERIFICA PARAMETRIZACOES POR LOCAL
    ' --------------------------------------------------------------
    
    If EcCodProveniencia <> "" Then
        If BL_VerificaProvenValida(EcCodProveniencia, BL_HandleNull(EcLocal, CStr(gCodLocal))) = False Then
            EcCodProveniencia.text = ""
            EcDescrProveniencia = ""
            EcCodProveniencia.SetFocus
            Exit Sub
        End If
    End If
    
    If EcCodEFR <> "" Then
        If BL_VerificaEfrValida(EcCodEFR, BL_HandleNull(EcLocal, CStr(gCodLocal))) = False Then
            EcCodEFR.text = ""
            EcDescrEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
        End If
    End If
    
    If EcCodMedico <> "" Then
        If BL_VerificaMedValida(EcCodMedico, BL_HandleNull(EcLocal, CStr(gCodLocal))) = False Then
            EcCodMedico.text = ""
            EcDescrMedico = ""
            EcCodMedico.SetFocus
            Exit Sub
        End If
    End If

    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        'NELSONPSILVA CHVNG-7461 29.10.2018
         If gAtiva_Reutilizacao_Colheitas = mediSim And EcNumReq.text <> "" Then
            If Bl_VerificaEstadoPendente(Trim(EcNumReq.text)) = True Then
                BG_Mensagem mediMsgBox, "Requisi��o pendente de valida��o para reutiliza��o!", vbOKOnly + vbInformation, "Reutiliza��o de Colheita"
             Exit Sub
            End If
        End If
        'Impedir que se alterem valores de cria��o / impress�o de registos
        EcDataCriacao = BL_HandleNull(rs!dt_cri, "")
        EcUtilizadorCriacao = BL_HandleNull(rs!user_cri, "")
        EcHoraCriacao = BL_HandleNull(rs!hr_cri, "")
        
        EcDataImpressao = BL_HandleNull(rs!dt_imp, "")
        EcUtilizadorImpressao = BL_HandleNull(rs!user_imp, "")
        EcHoraImpressao = BL_HandleNull(rs!hr_imp, "")
        
        EcDataImpressao2 = BL_HandleNull(rs!dt_imp2, "")
        EcUtilizadorImpressao2 = BL_HandleNull(rs!user_imp2, "")
        EcHoraImpressao2 = BL_HandleNull(rs!hr_imp2, "")
        
        EcDataAlteracao = dataAct
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = Bg_DaHora_ADO
        
        If gMostraColheita = mediSim Then
            If EcDtColheita = "" Then EcDtColheita = EcDataCriacao
            If EcHrColheita = "" Then EcHrColheita = EcHoraCriacao
        End If
        
        'se a data prevista estiver vazia
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = Format(dataAct, gFormatoData)
        End If
        
        'ValidaReqAssoc
        
        If gLAB = "HSMARTA" Then
            If EcEpisodio.text = "" And EcEpisodioAux.text <> "" And EcEpisodioAux.text <> "24000594" Then
                ValidaEpisodio
            End If
        End If
        
        If UBound(MaReq) > 0 Then
            If Trim(MaReq(1).cod_ana_s) <> "" Then
                If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                    EcEstadoReq = EcEstadoReq
                ElseIf EcEstadoReq = gEstadoReqEsperaProduto Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                End If
            Else
                EcEstadoReq = gEstadoReqSemAnalises
            End If
        Else
            EcEstadoReq = gEstadoReqSemAnalises
        End If
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        
        'Se o utente � isento, � obrigat�rio indicar o motivo
        If CkIsento.value = vbChecked And CbTipoIsencao.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Tipo de isen��o obrigat�rio!", vbOKOnly + vbInformation, "Insen��es"
            CbTipoIsencao.SetFocus
            Exit Sub
        End If
        If CkIsento.value = vbUnchecked Or CkIsento.value = vbGrayed Then
            CbTipoIsencao.ListIndex = -1
        End If
        
        If Not ValidaEspecifObrig Then Exit Sub
        
        If gSISTEMA_FACTURACAO = cFACTURACAO_FACTUS And gF_TRATAREJ = 1 Then
            EcMotivoRejFact.text = ""
            EcCodMotivoRejFact.text = ""
            EcMotivoRejFact.Visible = False
            EcUtilTrataRejFact.text = gCodUtilizador
            EcDataTrataRejFact = dataAct
            EcHoraTrataRejFact = Bg_DaHora_ADO
        End If
                
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        
        If Trim(EcEpisodio.text) = "" And gPreencheEpis = 1 Then
            DevolveEpisodio EcSeqUtente.text
        End If
        
        
        If ValidaNumBenef = True Then
            iRes = ValidaCamposEc
            If iRes = True Then
            
                BD_Update
                
                'EcCodMedico.SetFocus
                RegistaAlteracoesRequis EcNumReq
                'FG ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, "Requisi��o: " & EcNumReq & " Modificada"
                
                If Trim(EcNumReq.text) <> "" Then
                    'Se n�o houve erro
                    If gRequisicaoActiva = 0 Then
                        BtDadosAnteriores.Enabled = False
                        LaCopiarAnterior.Enabled = False
                    Else
                        gRequisicaoActiva = EcNumReq.text
                        BtDadosAnteriores.Enabled = True
                        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
                        LaCopiarAnterior.Enabled = True
                    End If
                    
                    'Tratamento de uma requisi��o rejeitada
                    If gF_TRATAREJ = 1 And gSISTEMA_FACTURACAO = cFACTURACAO_FACTUS Then
                        BG_Mensagem mediMsgBox, "Requisi��o de novo pronta a ser facturada!", vbInformation + vbOKOnly, App.ProductName
                        Unload Me
                    End If
                                        
                End If
            End If
        End If
        
        'SOLIVEIRA 09.10.2007
        If Command <> "" Then
            Call MDIFormInicio.Registo("Sair")
        End If
        
    End If

End Sub

Function ValidaNumBenef() As Boolean

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim Formato As Boolean
    
    If EcCodEFR.text <> "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = BL_HandleNull(Tabela!Formato1, "")
            Formato2 = BL_HandleNull(Tabela!Formato2, "")
            'SendKeys ("{END}")
        End If
        Formato = Verifica_Formato(UCase(EcNumBenef.text), Formato1, Formato2)
        If Formato = False And EcNumBenef.text <> "" Then
            Sendkeys ("{END}")
            EcNumBenef.SetFocus
            ValidaNumBenef = False
        Else
            ValidaNumBenef = True
        End If
    Else
        ValidaNumBenef = True
    End If

End Function

Function Verifica_Formato(NumBenef As String, Formato1 As String, Formato2 As String) As Boolean
    
    If Trim(Formato1) = "" And Trim(Formato2) = "" Then
        Verifica_Formato = True
        Exit Function
    End If
    
    If Trim(Formato1) = "" Then
        Verifica_Formato = False
    End If
    If Len(NumBenef) <> Len(Formato1) Then
        Verifica_Formato = False
    Else
        Verifica_Formato = Percorre_Formato(NumBenef, Formato1)
    End If
    
    'caso o formato 1 n�o esteja correcto vai verificar o formato 2
    If Verifica_Formato = False Then
        If Trim(Formato2) = "" Then
            Verifica_Formato = False
        End If
        If Len(NumBenef) <> Len(Formato2) Then
            Verifica_Formato = False
        Else
            Verifica_Formato = Percorre_Formato(NumBenef, Formato2)
        End If
        If Verifica_Formato = False And EcNumBenef.text <> "" Then
            gMsgTitulo = "Aten��o"
            gMsgMsg = "N�mero Benefici�rio incorrecto." & Chr(13) & "Deseja continuar ?" & Chr(13) & Chr(13) & "Exemplo de formato(s) correcto(s): " & Chr(13) & "       " & Formato1 & IIf(Trim(Formato1) <> "" And Trim(Formato2) <> "", ", ", "") & Formato2
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbInformation, gMsgTitulo)
            If gMsgResp = vbYes Then
                Verifica_Formato = True
            Else
                Verifica_Formato = False
            End If
        End If
    End If

End Function

Function Percorre_Formato(NumBenef As String, Formato As String) As Boolean
    
    Dim pos As Integer
    Dim comp As Integer
    Dim CharActualF As String
    Dim CharActualN As String
    
    Percorre_Formato = True
    comp = Len(Formato)
    pos = 1
    Do While pos <= comp
        CharActualF = Mid(Formato, pos, 1)
        CharActualN = Mid(NumBenef, pos, 1)
        Select Case CharActualF
            Case "A"
                If CharActualN < "A" Or CharActualN > "Z" Then
                    Percorre_Formato = False
                End If
            Case "9"
                If CharActualN < "0" Or CharActualN > "9" Then
                    Percorre_Formato = False
                End If
            Case " "
                If CharActualN <> " " Then
                    Percorre_Formato = False
                End If
        End Select
        pos = pos + 1
    Loop

End Function

Sub BD_Update()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim k As Integer
    Dim ProdJaChegou As Boolean
    Dim iErro As Integer
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    
    BG_BeginTransaction
        
'    'actualizar o numero de requisi��o associada
'    Grava_Req_Assoc
'    If gSQLError <> 0 Then
'        'Erro a actualizar o numero de requisi��o associada
'        BG_Mensagem mediMsgBox, "Erro a actualizar n�mero da requisi��o associada !", vbError, "ERRO"
'        GoTo Trata_Erro
'    End If
        
    'actualizar os produtos da requisi��o
    iErro = 0
    UPDATE_Prod_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar os tubos da requisi��o
    iErro = 1
    UPDATE_Tubos_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
            
    'actualizar as obs das analises
    iErro = 2
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    

    
             
    'actualizar as an�lises da requisi��o
    iErro = 3
    If UPDATE_Ana_Req = False Then
        'Erro a actualizar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
                
    'actualizar as an�lises da requisi��o da entidade gARS
    iErro = 4
    UPDATE_Ana_gARS
    If gSQLError <> 0 Then
        'Erro a actualizar as an�lises da requisi��o da entidade gARS
        BG_Mensagem mediMsgBox, "Erro a actualizar as an�lises gARS da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
                        
    'actualizar as terap�uticas e medica��es da requisi��o
    iErro = 5
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar as terap�uticas e medica��es da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as terap�uticas e medica��es da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Grava questoes das analises
    iErro = 6
    If GravaQuestoesAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar Quest�es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Actualiza agenda
    iErro = 7
    Actualiza_Agenda
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar agenda!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' RECIBOS DA REQUISICAO
    iErro = 8
    If gPrecosAmbienteHospitalar = mediSim Then
        If RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False) = False Then
            'Erro a gravar os tubos da requisi��o
            BG_Mensagem mediMsgBox, "Erro a gravar os pre�os da requisi��o !", vbError, "ERRO"
            GoTo Trata_Erro
        End If
    End If
    
    'GRAVA PERFIS MARCADOS
    iErro = 9
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar perfis marcados", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    iErro = 10
    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If
   
    iErro = 11
    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)
    'gravar restantes dados da requisicao
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    iErro = 12
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
   
    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        Flg_Gravou = True
        gRequisicaoActiva = CLng(EcNumReq)
        BG_CommitTransaction
        
    iErro = 13
        EcEstadoReq.text = BL_MudaEstadoReq(CLng(EcNumReq))
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
    iErro = 14
        
        If (EcEstadoReq.text = (gEstadoReqSemAnalises)) Then
            Me.BtPesquisaUtente.Enabled = True
            Me.EcDataPrevista.Enabled = True
            If gLAB = "CHVNG" Or gLAB = "HCVP" Or gInibeDataChegada = mediSim Then
                EcDataChegada.Enabled = False
            Else
                EcDataChegada.Enabled = True
            End If
        Else
            Me.BtPesquisaUtente.Enabled = False
            Me.EcDataPrevista.Enabled = False
            Me.EcDataChegada.Enabled = False
        End If
        
    iErro = 15
        EnviaFact_ARS
        
    End If
    
    iErro = 16
    MarcaLocal = rs.Bookmark
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rs.Bookmark = MarcaLocal
    
    iErro = 17
    'CbSituacao.SetFocus
    EcNumReq.SetFocus
    Exit Sub
    
Trata_Erro:
    BG_RollbackTransaction
    BG_Mensagem mediMsgBox, iErro & "-Erro a modificar requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicao: BD_Update -> " & Err.Description
    gRequisicaoActiva = 0
    FuncaoLimpar
    Resume Next

End Sub

Sub UPDATE_Prod_Req()
   
   Dim sql As String
    
   gSQLError = 0
   sql = "DELETE FROM sl_req_prod WHERE n_req=" & Trim(EcNumReq.text)
   BG_ExecutaQuery_ADO sql
   
   If gSQLError <> 0 Then
        Exit Sub
   End If
   Grava_Prod_Req

End Sub
Sub UPDATE_Tubos_Req()
   
   'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
   TB_GravaTubosReq EcNumReq, EcDataChegada.text

End Sub
Private Function UPDATE_Ana_Req() As Boolean
    On Error GoTo TrataErro
    
    Dim sql As String
    ActualizaFlgAparTrans
    gSQLError = 0
    'Apagar todas as requisi��es que n�o t�m dt_chega
    'sql = "DELETE FROM sl_marcacoes WHERE n_req=" & Trim(EcNumReq.text)
    'BG_ExecutaQuery_ADO sql
    
    If gSQLError <> 0 Then
        Exit Function
    End If

    EcGrupoAna.text = ""
    
    If Grava_Ana_Req = False Then
        UPDATE_Ana_Req = False
        Exit Function
    End If
    
    UPDATE_Ana_Req = True
    
Exit Function
TrataErro:
    UPDATE_Ana_Req = False
    BG_LogFile_Erros "UPDATE_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Ana_Req"
    Exit Function
    Resume Next
End Function


Sub FuncaoRemover()
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    
    If Trim(EcEstadoReq) <> Trim(gEstadoReqSemAnalises) And EcEstadoReq <> gEstadoReqEsperaProduto And EcEstadoReq <> gEstadoReqEsperaResultados Then
        Call BG_Mensagem(mediMsgBox, "N�o pode cancelar requisi��es com resultados!", vbExclamation + vbOKOnly, App.ProductName)
        Exit Sub
    End If
    
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind

    MarcaLocal = rs.Bookmark
    
    FormGestaoRequisicao.Enabled = False
    
    FormCancelarRequisicoes.Show
    
    SSTGestReq.TabEnabled(1) = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabVisible(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
 
End Sub

Sub FuncaoProcurar()
          
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim sSql As String
    Dim rsProc As New ADODB.recordset
    Dim RsUtente As New ADODB.recordset
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    
    On Error GoTo Trata_Erro
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    
    If EcProcHosp1.text <> "" And EcSeqUtente.text = "" Then
        sSql = "SELECT * FROM " & tabela_aux & " WHERE n_proc_1 = " & BL_TrataStringParaBD(EcProcHosp1.text)
        Set RsUtente = New ADODB.recordset
        RsUtente.CursorLocation = adUseServer
        RsUtente.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsUtente.Open sSql, gConexao
        If RsUtente.RecordCount > 0 Then
            EcSeqUtente.text = BL_HandleNull(RsUtente!seq_utente, mediComboValorNull)
        End If
        RsUtente.Close
        Set RsUtente = Nothing
    End If
    
        
    Set rs = New ADODB.recordset
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos as requisi��es ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set rs = Nothing
            Exit Sub
        End If
    Else
    End If
    CriterioTabela = CriterioTabela & "  ORDER BY dt_chega DESC, n_req DESC"
              
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    rs.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        BL_ToolbarEstadoN Estado
        
        'inicializa��es da lista de marca��es de an�lises
        Inicializa_Estrutura_Analises
        
        'limpar grids
        LimpaFGAna
        LimpaFgProd
        TB_LimpaFgTubos FgTubos

        
        Call PreencheCampos
        
        SSTGestReq.TabEnabled(4) = False
        DoEvents
        DoEvents
        
        Set CampoDeFocus = EcCodProveniencia
        
        BL_FimProcessamento Me
        gRequisicaoActiva = CLng(EcNumReq.text)
        BtDadosAnteriores.Enabled = True
        BtDadosAnteriores.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva
        LaCopiarAnterior.Enabled = True
        'brunodsantos 02.12.2015
        LaCopiarAnterior.ToolTipText = "Copiar dados da requisi��o " & gRequisicaoActiva


        Me.BtPesquisaUtente.Enabled = False
        If EcDataChegada <> "" Then
            Me.EcDataChegada.Enabled = False
        End If
        Me.EcDataPrevista.Enabled = False

    End If
    
    Exit Sub
    
Trata_Erro:
    
    BG_LogFile_Erros Me.Name & ": FuncaoProcurar (" & Err.Number & "-" & Err.Description & ")" & CriterioTabela
    Err.Clear
    BL_FimProcessamento Me
    BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
    FuncaoLimpar
    Resume Next

End Sub

Sub Funcao_CopiaReq()
    
    If gRequisicaoActiva <> 0 Then
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If

End Sub

Sub FuncaoLimpar()
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
'        gMsgTitulo = "Limpar"
'        gMsgMsg = "Tem a certeza que quer LIMPAR o ecr�?"
'        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
'        If gMsgResp = vbYes Then
            LimpaCampos
            EcUtente.text = ""
            DoEvents
            
'            CampoDeFocus.SetFocus
'        End If
    
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub LimpaCampos()
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    
    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    
    EcNumColheita.text = ""
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDescrEstado = ""
    EcProcHosp1.text = ""
    EcProcHosp2.text = ""
    EcNumCartaoUte.text = ""
    EcDataNasc.text = ""
    EcSexoUte.text = ""
    EcNome.text = ""
    EcUtente.text = ""
    EcPesqRapMedico.text = ""
    EcNomeMedico.text = ""
    EcDescrValencia = ""
    EcDescrEFR.text = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcPesqRapTubo.text = ""
    EcDescrProveniencia.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrTubos.Enabled = False
    SSTGestReq.TabEnabled(5) = False
    EcCodEFR.locked = False
    EcPrescricao.locked = False
    EcNumBenef.locked = False
    BtPesquisaEntFin.Enabled = True
    EcCodEFR.ToolTipText = ""
    EcNumBenef.ToolTipText = ""
    BtPesquisaEntFin.ToolTipText = ""
    EcCodEFR.ForeColor = vbBlack
    EcNumBenef.ForeColor = vbBlack
    CkIsento.Enabled = True
    CbTipoIsencao.Enabled = True
    gEpisodioActivo = ""
    gEpisodioActivoAux = ""
    gSituacaoActiva = mediComboValorNull
    gReqAssocActiva = ""
    CbPrioColheita.ListIndex = mediComboValorNull
    EcObsAna = ""
    FrameObsAna.Visible = False
    EcDescrSala = ""
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    

    
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
     
    'Limpar treeview
    TAna.Nodes.Clear
         
    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False
    
    CbSituacao.Enabled = True
    EcEpisodio.Enabled = True
    CbTipoUtente.Enabled = False
    EcUtente.Enabled = False
    EcDataPrevista.Enabled = True
    If gLAB = "CHVNG" Or gLAB = "HCVP" Or gInibeDataChegada = mediSim Then
        EcDataChegada.Enabled = False
    Else
        EcDataChegada.Enabled = True
    End If
    
    BtInfClinica.Enabled = False
    LaInfCli.Enabled = False
    BtRegistaMedico.Enabled = False
    Set CampoDeFocus = EcNumReq
    
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    LimpaFgProd
    TB_LimpaFgTubos FgTubos
    DefFgAna False
    LimpaFGAna
    
    SSTGestReq.Tab = 0
    DoEvents
    
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabVisible(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    'NELSONPSILVA CHVNG-7461 29.10.2018
    SSTGestReq.TabEnabled(7) = False
    '
    
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1
    
    CkIsento.value = vbGrayed
    CbTipoIsencao.ListIndex = mediComboValorNull
    
    Call Apaga_DS_TM
    
    BtEtiq.Enabled = False
    LaEtiq.Enabled = False
    BtAnexos.Enabled = False
    
    ' Pesquisa R�pida de Utente.
    Me.BtPesquisaUtente.Enabled = True
    
    Flg_DadosAnteriores = False
    
    SSTGestReq.Enabled = True
    FrameGarrafa.Enabled = False
    FrameGarrafa.Visible = False
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    
    'NELSONPSILVA CHVNG-7461 29.10.2018
    ReDim analisesReutil(0)
    TotalAnaReutil = 0
    LimpaFgPedAdiciona
    '
    Label1(100).Visible = False
    Label1(100).caption = "�rea"
    
    gCodAna_aux = ""
    gCodPerfil_aux = ""
    gReq_aux = ""
    EcEresultsData.caption = ""
    EcEresultsEstado.caption = ""
    LbAssinatura.caption = ""
    EcUtilNomeConf = ""
    'BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    BtResumo.Enabled = False
    
    
    EcUtilizadorCriacao.locked = False
    EcDataCriacao.locked = False
    EcHoraCriacao.locked = False
    
    EcUtilizadorAlteracao.locked = False
    EcDataAlteracao.locked = False
    EcHoraAlteracao.locked = False
    
    EcUtilizadorFecho.locked = False
    EcDataFecho.locked = False
    EcHoraFecho.locked = False

    EcUtilizadorImpressao.locked = False
    EcDataImpressao.locked = False
    EcHoraImpressao.locked = False
    
    EcUtilizadorImpressao2.locked = False
    EcDataImpressao2.locked = False
    EcHoraImpressao2.locked = False
    
    EcUtilConf.locked = False
    EcDtConf.locked = False
    EcHrConf.locked = False

    EcLocal.locked = False
    
    PicMasc.Visible = False
    PicFem.Visible = False
    EcDescrUserColheita = ""
    FrameDomicilio.Visible = False
    BtDomicilio.Enabled = False
    CkIsento.value = vbGrayed
    CkReqHipocoagulados.value = vbGrayed
    EcDescrPais = ""
    EcDescrPostal = ""
    AnexaDocumento mediComboValorNull
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    EcUtilNomeEmail = ""
    EcUtilNomeSMS = ""
    PM_LimpaEstrutura
    PreencheSalaDefeito
    'RGONCALVES 16.06.2013 ICIL-470
    CkAvisarSMS.value = vbGrayed
    CriaNovaClasse RegistosR, 1, "REC", True
    CriaNovaClasse RegistosRM, 1, "REC_MAN", True
    lObrigaTerap = False
    lObrigaInfoCli = False
End Sub

Sub LimpaCampos2()
    
    Me.SetFocus
    BG_LimpaCampo_Todos CamposEc
    
    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    
    EcNumColheita.text = ""
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDescrEstado = ""
    EcProcHosp1.text = ""
    EcProcHosp2.text = ""
    EcNumCartaoUte.text = ""
    EcDataNasc.text = ""
    EcSexoUte.text = ""
    EcNome.text = ""
    EcUtente.text = ""
    EcPesqRapMedico.text = ""
    EcNomeMedico.text = ""
    EcDescrEFR.text = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcDescrProveniencia.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrTubos.Enabled = False
    SSTGestReq.TabEnabled(5) = False
    EcCodEFR.locked = False
    EcPrescricao.locked = False
    EcNumBenef.locked = False
    BtPesquisaEntFin.Enabled = True
    EcCodEFR.ToolTipText = ""
    EcNumBenef.ToolTipText = ""
    BtPesquisaEntFin.ToolTipText = ""
    EcCodEFR.ForeColor = vbBlack
    EcNumBenef.ForeColor = vbBlack
    CkIsento.Enabled = True
    CbTipoIsencao.Enabled = True
    CbPrioColheita.ListIndex = mediComboValorNull
    EcObsAna = ""
    FrameObsAna.Visible = False
    EcDescrValencia = ""
    EcDescrSala = ""

    'Hoje passa ao limpar
    'gEpisodioActivo = ""
    'gSituacaoActiva = mediComboValorNull
    
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
     
    'Limpar treeview
    TAna.Nodes.Clear
         
    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False
    
    EcEpisodio.Enabled = True
    CbTipoUtente.Enabled = False
    EcUtente.Enabled = False
    EcDataPrevista.Enabled = True
    If gLAB = "CHVNG" Or gLAB = "HCVP" Or gInibeDataChegada = mediSim Then
        EcDataChegada.Enabled = False
    Else
        EcDataChegada.Enabled = True
    End If
    
    BtInfClinica.Enabled = False
    LaInfCli.Enabled = False
    BtRegistaMedico.Enabled = False
    
    Set CampoDeFocus = EcNumReq
    
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    LimpaFgProd
    TB_LimpaFgTubos FgTubos

    DefFgAna False
    LimpaFGAna
    'NELSONPSILVA CHVNG-7461 29.10.2018
    LimpaFgPedAdiciona
    '
    SSTGestReq.Tab = 0
    DoEvents
    
    
    'Inicializa��o das variaveis que guardam os indices nas combos de pagamentos
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1
    
    CkIsento.value = vbGrayed
    CbTipoIsencao.ListIndex = mediComboValorNull
    
    
    Call Apaga_DS_TM
    
    BtEtiq.Enabled = False
    LaEtiq.Enabled = False
    BtAnexos.Enabled = False
    
    ' Pesquisa R�pida de Utente.
    Me.BtPesquisaUtente.Enabled = True
    
    Flg_DadosAnteriores = False
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)

    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    
    'NELSONPSILVA CHVNG-7461 29.10.2018
    TotalAnaReutil = 0
    ReDim anaReutil(0)
    '
    
    EcEresultsData.caption = ""
    EcEresultsEstado.caption = ""
    LbAssinatura.caption = ""
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    
    EcUtilizadorCriacao.locked = False
    EcDataCriacao.locked = False
    EcHoraCriacao.locked = False
    
    EcUtilizadorAlteracao.locked = False
    EcDataAlteracao.locked = False
    EcHoraAlteracao.locked = False
    
    EcUtilizadorFecho.locked = False
    EcDataFecho.locked = False
    EcHoraFecho.locked = False

    EcUtilizadorImpressao.locked = False
    EcDataImpressao.locked = False
    EcHoraImpressao.locked = False
    
    EcUtilizadorImpressao2.locked = False
    EcDataImpressao2.locked = False
    EcHoraImpressao2.locked = False
    
    EcUtilConf.locked = False
    EcDtConf.locked = False
    EcHrConf.locked = False

    EcLocal.locked = False
    
    PicMasc.Visible = False
    PicFem.Visible = False
    EcDescrUserColheita = ""
    FrameDomicilio.Visible = False
    BtDomicilio.Enabled = False
    EcDescrPais = ""
    EcDescrPostal = ""
    AnexaDocumento mediComboValorNull
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    EcUtilNomeEmail = ""
    EcUtilNomeSMS = ""
    PM_LimpaEstrutura
    PreencheSalaDefeito
    'RGONCALVES 16.06.2013 ICIL-470
    CkAvisarSMS.value = vbGrayed
    CriaNovaClasse RegistosR, 1, "REC", True
    CriaNovaClasse RegistosRM, 1, "REC_MAN", True
    lObrigaTerap = False
    lObrigaInfoCli = False
	
	'tania.oliveira CHUC-12912 12.11.2019 -> Inicializa��o da estrutura dos tubos
    InicializaEstruturaTubos
    '
End Sub

Sub FuncaoAnterior()
    
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BG_MensagemAnterior
    Else
        SSTGestReq.TabEnabled(4) = False
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = False
        Call PreencheCampos
        
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
            'NELSONPSILVA CHVNG-7461 29.10.2018
            SSTGestReq.TabEnabled(7) = False
            '
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(6) = True
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            BtCancelarReq.Visible = False
        
            SSTGestReq.TabEnabled(4) = False
            'NELSONPSILVA CHVNG-7461 29.10.2018
            SSTGestReq.TabEnabled(7) = True
            '
            DoEvents
            DoEvents
        End If
    End If

End Sub

Sub FuncaoSeguinte()

    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BG_MensagemSeguinte
    Else
        SSTGestReq.TabEnabled(4) = False
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = False
        Call PreencheCampos
        
        ' Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados.
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
            'NELSONPSILVA CHVNG-7461 29.10.2018
            SSTGestReq.TabEnabled(7) = False
            '
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            BtCancelarReq.Visible = False
            
            SSTGestReq.TabEnabled(4) = False
            'NELSONPSILVA CHVNG-7461 29.10.2018
            SSTGestReq.TabEnabled(7) = True
            '
            DoEvents
            DoEvents
        End If
    End If

End Sub

Sub PreencheCampos()
    Dim i As Integer
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BtNotas.Enabled = True
        BtNotasVRM.Enabled = True
        BtResumo.Enabled = True
        BtCancelarReq.Visible = True
        
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        
        BL_PreencheVectorBackup NumCampos, CamposEc, CamposBCK
        'Hoje
'        gEpisodioActivo = EcEpisodio.Text
'        gSituacaoActiva = CbSituacao.ListIndex

        If Trim(EcCodIsencao) <> "" Then
            CkIsento.value = checked
            CbTipoIsencao.Enabled = True
        Else
            CkIsento.value = Unchecked
            CbTipoIsencao.Enabled = False
        End If
        BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao
        AnexaDocumento BL_HandleNull(EcFlgAnexo, 0)
        LbNomeUtilCriacao.caption = BL_SelNomeUtil(EcUtilizadorCriacao.text)
        LbNomeUtilCriacao.ToolTipText = LbNomeUtilCriacao.caption
        LbNomeUtilAlteracao.caption = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
        LbNomeUtilAlteracao.ToolTipText = LbNomeUtilAlteracao.caption
        LbNomeUtilImpressao.caption = BL_SelNomeUtil(EcUtilizadorImpressao.text)
        LbNomeUtilImpressao.ToolTipText = LbNomeUtilImpressao.caption
        LbNomeUtilImpressao2.caption = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
        LbNomeUtilImpressao2.ToolTipText = LbNomeUtilImpressao2.caption
        LbNomeUtilFecho.caption = BL_SelNomeUtil(EcUtilizadorFecho.text)
        LbNomeUtilFecho.ToolTipText = LbNomeUtilFecho.caption
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcUtilNomeConf.ToolTipText = EcUtilNomeConf.caption
        LbNomeLocal.caption = BL_SelCodigo("gr_empr_inst", "nome_empr", "empresa_id", EcLocal, "V")
        LbNomeLocal.ToolTipText = LbNomeLocal.caption
        EcUtilNomeSMS.caption = BL_SelNomeUtil(EcUtilSMS.text)
        EcUtilNomeEmail.caption = BL_SelNomeUtil(EcUtilEmail.text)
        
        EcUtilizadorCriacao.locked = True
        EcDataCriacao.locked = True
        EcHoraCriacao.locked = True
        
        EcUtilizadorAlteracao.locked = True
        EcDataAlteracao.locked = True
        EcHoraAlteracao.locked = True
        
        EcUtilizadorFecho.locked = True
        EcDataFecho.locked = True
        EcHoraFecho.locked = True

        EcUtilizadorImpressao.locked = True
        EcDataImpressao.locked = True
        EcHoraImpressao.locked = True
        
        EcUtilizadorImpressao2.locked = True
        EcDataImpressao2.locked = True
        EcHoraImpressao2.locked = True
        
        EcUtilConf.locked = True
        EcDtConf.locked = True
        EcHrConf.locked = True
    
        If gLAB <> "HMP" Then
            EcLocal.locked = True
        End If
        EcNumColheita.text = COLH_DevolveNumColheita(EcNumReq.text)
        
        BL_PreencheDadosUtente EcSeqUtente.text
        PM_CarregaPerfisMarcados EcNumReq.text
        PreencheDescEntFinanceira
        EcCodProveniencia_Validate False
        EcCodValencia_Validate False
        EcCodsala_Validate False
        EcCodUserColheita_Validate False
        PreencheDescMedico
        PreencheUrgencia
        PreencheEstadoRequisicao
        PreencheDadosUtente
        PreencheEnvioEResults
        PreencheEstadoAssinatura
        PreencheNotas
        EcCodpais_Validate False
        
        i = InStr(1, EcCodPostalAux, "-")
        If i > 0 Then
            EcCodPostal = Mid(EcCodPostalAux, 1, i - 1)
            EcRuaPostal = Mid(EcCodPostalAux, i + 1, Len(EcCodPostalAux))
        ElseIf i = 0 And EcCodPostalAux.text <> "" Then
            EcCodPostal = Mid(EcCodPostalAux, 1, 4)
            EcRuaPostal = Mid(EcCodPostalAux, 5, 7)
        End If
        EcDescrPostal = BL_SeleccionaDescrPostal(EcCodPostalAux)
                
        'colocar o bot�o de cancelar requisi��es activo
        gRequisicaoActiva = CLng(EcNumReq.text)
        gFlg_JaExisteReq = True
        
        ValidarARS EcCodEFR
        
        'colocar cabe�alho  e data prevista enabled a false
'        EcNumReq.Enabled = False
        EcNumReq.locked = True
        EcPrescricao.locked = True
        If (gLAB = "HDMC" Or gLAB = "HPOVOA") And EcEpisodio.text = "" Then
            EcEpisodio.Enabled = True
        Else
            EcEpisodio.Enabled = False
        End If
        CbSituacao.Enabled = False
        CbTipoUtente.Enabled = False
        EcUtente.Enabled = False
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FrTubos.Enabled = True
        Me.BtPesquisaUtente.Enabled = False
        If Trim(EcDataChegada.text) <> "" Then
            EcDataChegada.Enabled = False
            EcDataPrevista.Enabled = False
        Else
            If gLAB = "CHVNG" Or gLAB = "HCVP" Or gInibeDataChegada = mediSim Then
                EcDataChegada.Enabled = False
            Else
                EcDataChegada.Enabled = True
            End If
            EcDataPrevista.Enabled = True
        End If
         
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If gAtiva_Reutilizacao_Colheitas = mediSim Then
            Dim cod_colheita As String
            cod_colheita = COLH_DevolveNumColheita(CLng(Trim(EcNumReq.text)))
            If cod_colheita <> "" Then
                COLH_EstrutReutil CLng(Trim(EcNumReq.text)), cod_colheita
                anaReutil = analisesReutil
                COLH_DevolveReqColheita CLng(Trim(cod_colheita))
            End If
        End If
        
        FuncaoProcurar_PreencheProd CLng(Trim(EcNumReq.text))
        TB_ProcuraTubos FgTubos, CLng(Trim(EcNumReq.text)), True
        FuncaoProcurar_PreencheAna CLng(Trim(EcNumReq.text))
        If gPrecosAmbienteHospitalar = mediSim Then
            RECIBO_PreencheCabecalho EcNumReq, RegistosR
            RECIBO_PreencheDetManuais Trim(EcNumReq.text), RegistosRM, CbTipoUtente, EcUtente
            ActualizaLinhasRecibos -1
        End If
        'CbSituacao.SetFocus
        EcNumReq.SetFocus
        'NELSONPSILVA CHVNG-7461 29.10.2018
        If gAtiva_Reutilizacao_Colheitas = mediSim Then
            PreencheFgPedAdiciona
        End If
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabVisible(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
            'NELSONPSILVA CHVNG-7461 29.10.2018
            SSTGestReq.TabEnabled(7) = False
            '
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            BtEtiq.Enabled = False
            LaEtiq.Enabled = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(6) = True
            'NELSONPSILVA CHVNG-7461 29.10.2018
            SSTGestReq.TabEnabled(7) = True
            '
            If CkIsento.value = vbUnchecked Or CkIsento.value = vbGrayed Then
                SSTGestReq.TabEnabled(3) = False
                SSTGestReq.TabVisible(3) = False
                SSTGestReq.TabVisible(3) = False
                SSTGestReq.TabEnabled(4) = False
            Else
                SSTGestReq.TabEnabled(3) = False
                SSTGestReq.TabEnabled(4) = False
            End If
            
            BtCancelarReq.Visible = False
            If flg_ReqBloqueada = False Then
                BL_Toolbar_BotaoEstado "Remover", "Activo"
                BL_Toolbar_BotaoEstado "Modificar", "Activo"
            Else
                BL_Toolbar_BotaoEstado "Remover", "InActivo"
                BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            End If
            BtEtiq.Enabled = True
            LaEtiq.Enabled = True
            BtAnexos.Enabled = True
            
            
        End If
        ' PFerreira 15.03.2007
        If gUsaFilaEspera = mediSim Then
            If (Trim(EcPrColheita <> "")) Then
                BL_ColocaTextoCombo "sl_cod_prioridades", "seq_prio", "cod_prio", EcPrColheita, CbPrioColheita
            ElseIf (Trim(EcDataNasc) <> "" And IsChild) Then
                CbPrioColheita.ListIndex = 3
            Else
                CbPrioColheita.ListIndex = 1
            End If
        End If
        
        ' pferreira 2007.10.24
        If EcTipoUrgencia.text <> "" Then
            Label1(100).ForeColor = BL_DevolveCorTipoUrgencia(EcTipoUrgencia.text)
            Label1(100).caption = UCase(Label1(100).caption & vbCrLf & EcTipoUrgencia.text)
            Label1(100).Visible = True
        End If
    End If
    

End Sub

Sub PreencheEstadoRequisicao()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
       EcDescrEstado.BackColor = vermelhoClaro
    Else
       EcDescrEstado.BackColor = 14737632
    End If
   EcDescrEstado = BL_DevolveEstadoReq(Trim(BL_HandleNull(rs!estado_req, "")))
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstadoRequisicao: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEstadoRequisicao"
    Exit Sub
    Resume Next
End Sub

Sub PreencheUrgencia()

End Sub

Sub PreencheDescEntFinanceira()
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodEFR.text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_efr FROM sl_efr WHERE cod_efr=" & Trim(EcCodEFR.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade inexistente!", vbOKOnly + vbInformation, "Entidades"
            EcCodEFR.text = ""
            EcDescrEFR.text = ""
            
            If (gLAB = cHSMARTA) Then
                Me.EcCodEFR = "0"
                Call EcCodEFR_Validate(True)
            End If
            
            EcCodEFR.SetFocus
        Else
            EcDescrEFR.text = Tabela!descr_efr
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
    
End Sub

Sub PreencheDescMedico()
        
    Dim Tabela As ADODB.recordset
    Dim sql As String

    If EcCodMedico.text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "M�dico inexistente!", vbOKOnly + vbInformation, "M�dicos"
            EcCodMedico.text = ""
            EcNomeMedico.text = ""
            EcCodMedico.SetFocus
        Else
            EcNomeMedico.text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If

End Sub


Private Sub BtPesquisaSala_Click()
        PA_PesquisaSala EcCodSala, EcDescrSala, ""
End Sub




Private Sub SSTGestReq_Click(PreviousTab As Integer)
    
    Dim i
    Dim j As Integer
    DoEvents
    
    If SSTGestReq.Tab = 2 Then
        If EcDataFecho.text <> "" Then
            BtPesquisaAna.Enabled = False
        Else
            BtPesquisaAna.Enabled = True
        End If
        FGAna.SetFocus

    ElseIf SSTGestReq.Tab = 0 Then
        EcAuxAna.Visible = False
        EcAuxProd.Visible = False
        If EcDataPrevista.Enabled = True And EcDataPrevista.Visible = True Then
            If gMultiReport <> 1 Then
                EcDataPrevista.SetFocus
            End If
        ElseIf EcDataChegada.Enabled = True And EcDataChegada.Visible = True Then
            'CbSituacao.SetFocus             'sdo
        End If
    
    ElseIf SSTGestReq.Tab = 1 Then
        If FgProd.Enabled = True Then FgProd.SetFocus
    
    End If
    
End Sub

Private Sub TAna_DblClick()
    
    Dim i As Long
    Dim s As String
    
    If TAna.Nodes.Count <> 0 Then
        If InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, gGHOSTMEMBER_S) <> 0 Then
            'Inserir Ghostmembers
            
            'Retira nome da complexa
            s = Mid(TAna.Nodes(TAna.SelectedItem.Index).key, 1, InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, gGHOSTMEMBER_S) - 1)
            GhostComplexa = s
            
            If Mid(TAna.SelectedItem.parent.key, 1, 1) = "P" Then
                'A complexa est� dentro de um perfil
                'GhostPerfil = Mid(TAna.SelectedItem.Parent.Key, 1, InStr(1, TAna.SelectedItem.Parent.Key, "C") - 1) 'soliveira 10.05.2006 ex: perfil = PURIMCB, ficava PURIM
                GhostPerfil = Mid(TAna.SelectedItem.parent.key, 1, InStr(1, TAna.SelectedItem.parent.key, GhostComplexa) - 1)
            Else
                GhostPerfil = "0"
                'A complexa � o pai
            End If
            
            Call BL_SelGhostMember(s)
            BL_MudaPosicaoRato 100, 300
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_POPUP
        Else
            i = Pesquisa_RegistosA(TAna.Nodes(TAna.SelectedItem.Index).key)
        
            If i <> -1 Then
                FGAna.topRow = i
                FGAna.row = i
                FGAna.SetFocus
            End If
        End If
    End If

End Sub

Sub Funcao_DataActual()

    If Me.ActiveControl.Name = "EcDataCriacao" Then
        BL_PreencheData EcDataCriacao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataAlteracao" Then
        BL_PreencheData EcDataAlteracao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao" Then
        BL_PreencheData EcDataImpressao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao2" Then
        BL_PreencheData EcDataImpressao2, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataPrevista" Then
        BL_PreencheData EcDataPrevista, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataChegada" Then
        BL_PreencheData EcDataChegada, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtColheita" Then
        BL_PreencheData EcDtColheita, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcAuxProd" Then
        If EcAuxProd.Tag = adDate Then
            BL_PreencheData EcAuxProd, Me.ActiveControl
        End If
    End If
    
End Sub

Function Coloca_Estado(DtPrev, DtChega, Optional DtElim = "") As String
    
    If DateValue(DtPrev) < DateValue(dataAct) And DtChega = "" Then
        Coloca_Estado = "Falta"
    ElseIf DateValue(DtPrev) >= DateValue(dataAct) And DtChega = "" Then
        Coloca_Estado = "Espera"
    ElseIf DtChega <> "" Then
        Coloca_Estado = "Entrou"
    Else
        Coloca_Estado = ""
    End If

End Function

Sub Preenche_LaProdutos(indice As Integer)
    
    Dim TamDescrProd As Integer
    Dim TamDescrEspecif As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String
    
    LaProdutos.caption = ""
    If RegistosP(indice).CodProd <> "" Then
        Str1 = "Produto vai chegar a: "
        Str2 = "Produto devia ter chegado a: "
        Str3 = "Produto chegou a: "
        If RegistosP(indice).EstadoProd = "Entrou" Then
            LaProdutos.caption = LaProdutos.caption & Str3 & RegistosP(indice).DtChega
        ElseIf RegistosP(indice).EstadoProd = "Falta" Then
            LaProdutos.caption = LaProdutos.caption & Str2 & RegistosP(indice).DtPrev
        ElseIf EcDataPrevista.text <> "" Then
            LaProdutos.caption = LaProdutos.caption & Str1 & RegistosP(indice).DtPrev
        End If
        TamDescrProd = Len(Trim(UCase(RegistosP(indice).DescrProd)))
        TamDescrEspecif = Len(Trim(UCase(RegistosP(indice).DescrEspecif)))
        For i = 1 To 80 - (TamDescrProd + TamDescrEspecif)
            LaProdutos.caption = LaProdutos.caption & Chr(32)
        Next i
        LaProdutos.caption = LaProdutos.caption & Trim(UCase(RegistosP(indice).DescrProd)) & " " & Trim(UCase(RegistosP(indice).DescrEspecif))
    End If

End Sub
Sub Inicializa_Estrutura_Analises()
    
    'inicializa��es da lista de marca��es de an�lises
    ReDim MaReq(0 To 0)
    MaReq(0).Cod_Perfil = ""
    MaReq(0).Descr_Perfil = ""
    MaReq(0).cod_ana_c = ""
    MaReq(0).Descr_Ana_C = ""
    MaReq(0).cod_ana_s = ""
    MaReq(0).descr_ana_s = ""
    MaReq(0).Ord_Ana = 0
    MaReq(0).Ord_Ana_Compl = 0
    MaReq(0).Ord_Ana_Perf = 0
    MaReq(0).cod_agrup = ""
    MaReq(0).N_Folha_Trab = 0
    MaReq(0).dt_chega = ""
    MaReq(0).Flg_Apar_Transf = ""
    MaReq(0).flg_estado = ""
    MaReq(0).Flg_Facturado = 0
    MaReq(0).Ord_Marca = 0
    LbTotalAna = "0"
End Sub

Sub Cria_TmpEtq()

    Dim TmpEtq(1 To 16) As DefTable
    
    TmpEtq(1).NomeCampo = "t_utente"
    TmpEtq(1).tipo = "STRING"
    TmpEtq(1).tamanho = 5
    
    TmpEtq(2).NomeCampo = "utente"
    TmpEtq(2).tipo = "STRING"
    TmpEtq(2).tamanho = 20
    
    TmpEtq(3).NomeCampo = "n_proc"
    TmpEtq(3).tipo = "STRING"
    TmpEtq(3).tamanho = 100
    
    TmpEtq(4).NomeCampo = "n_req"
    TmpEtq(4).tipo = "STRING"
    TmpEtq(4).tamanho = 9
    
    TmpEtq(5).NomeCampo = "t_urg"
    TmpEtq(5).tipo = "STRING"
    TmpEtq(5).tamanho = 3
    
    TmpEtq(6).NomeCampo = "dt_req"
    TmpEtq(6).tipo = "DATE"
   
    TmpEtq(7).NomeCampo = "ordem"
    TmpEtq(7).tipo = "STRING"
    TmpEtq(7).tamanho = 3
    
    TmpEtq(8).NomeCampo = "produto"
    TmpEtq(8).tipo = "STRING"
    TmpEtq(8).tamanho = 25
    
    TmpEtq(9).NomeCampo = "nome_ute"
    TmpEtq(9).tipo = "STRING"
    TmpEtq(9).tamanho = 80
    
    TmpEtq(10).NomeCampo = "n_req_bar"
    TmpEtq(10).tipo = "STRING"
    TmpEtq(10).tamanho = 20

    TmpEtq(11).NomeCampo = "gr_ana"
    TmpEtq(11).tipo = "STRING"
    TmpEtq(11).tamanho = 40

    TmpEtq(12).NomeCampo = "abr_ana"
    TmpEtq(12).tipo = "STRING"
    TmpEtq(12).tamanho = 10
    
    TmpEtq(13).NomeCampo = "descr_tubo"
    TmpEtq(13).tipo = "STRING"
    TmpEtq(13).tamanho = 40

    TmpEtq(14).NomeCampo = "situacao"
    TmpEtq(14).tipo = "STRING"
    TmpEtq(14).tamanho = 7
    
    TmpEtq(15).NomeCampo = "episodio"
    TmpEtq(15).tipo = "STRING"
    TmpEtq(15).tamanho = 15
    
    TmpEtq(16).NomeCampo = "idade"
    TmpEtq(16).tipo = "STRING"
    TmpEtq(16).tamanho = 3
    
    Call BL_CriaTabela("sl_cr_etiq" & gNumeroSessao, TmpEtq)
    
End Sub

Private Sub ImprimeEtiqNovo()
    'Limpar parametros
        BG_LimpaPassaParams
    'Passar parametros necessarios o ao form de impress�o
        gPassaParams.Param(0) = EcNumReq.Text
    'tania.oliveira CHUC-12912 12.11.2019 -> Carregar a Etiqueta Resumo
        PassaParam.NumReq = EcNumReq.Text
        InicializaEstruturaTubos
        EstruturaTubosResumo EcNumReq.Text
    '
    'Abrir form de impress�o
    FormNEtiqNTubos.Show
    etiqFlag = True
    
    FormGestaoRequisicao.SetFocus

End Sub

Sub ImprimeEtiq()

    Dim i As Integer
    Dim k As Integer
    Dim j As Integer
    Dim sql As String
    Dim CmdDadosTubo As ADODB.Command
    Dim RsDadosTubo As ADODB.recordset
    Dim CmdDadosGhostM As ADODB.Command
    Dim RsDadosGhostM As ADODB.recordset
    Dim totalTubos As Long
    Dim CodTuboBarLoc As String
    Dim encontrou As Boolean
    Dim CalculaCapaci As Double
    Dim ordem As Integer
    Dim GhostsJaTratados() As String
    Dim TotalGhostsJaTratados As Integer
    Dim Especiais As Long
    Dim EtqAdministrativas As Long
    Dim NumReqBar As String
    Dim continua As Boolean
    Dim EtqCrystal As String
    Dim EtqPreview As String
    Dim rsReq As ADODB.recordset
    Dim TotalP1s As Integer
    Dim NrReqARS As String
    Dim dataImpressao As String
    
    Dim episodio As String
    Dim situacao As String
    Dim rv As Integer
    
    Dim idade As String
    Dim CodProd_aux As String
    
    Dim CodPerfilAnt As String
    Dim CodAnaCAnt As String
    Dim CodAnaSAnt As String
    
    idade = ""
    
    rv = REQUISICAO_Situacao_Episodio(Trim(Me.EcNumReq), _
                                      situacao, _
                                      episodio)
    
    Select Case situacao
        Case gT_Urgencia
            situacao = "URG"
        Case gT_Consulta
            situacao = "CON"
        Case gT_Internamento
            situacao = "INT"
        Case gT_Externo
            situacao = "HDI"
        Case gT_LAB
            situacao = "LAB"
        Case gT_RAD
            situacao = "RAD"
        Case gT_Bloco
            situacao = "BLO"
        Case Else
            situacao = ""
    End Select
    
    'Dados das an�lises simples (tubo e produto)
    Set CmdDadosTubo = New ADODB.Command
    Set CmdDadosTubo.ActiveConnection = gConexao
    CmdDadosTubo.CommandType = adCmdText

    If gSGBD = gOracle Then
        CmdDadosTubo.CommandText = "SELECT ANA.COD_ANA_S, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, tubo.INF_COMPLEMENTAR " & _
                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO (+) " & _
                  " AND   ANA.COD_ANA_S = ?"
    ElseIf gSGBD = gInformix Then
        CmdDadosTubo.CommandText = "SELECT ANA.COD_ANA_S, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, tubo.INF_COMPLEMENTAR " & _
                  " FROM  SL_ANA_S ANA, OUTER SL_GR_ANA GRUPO_ANA, OUTER SL_TUBO TUBO " & _
                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA " & _
                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                  " AND   ANA.COD_ANA_S = ?"
    ElseIf gSGBD = gSqlServer Then
        CmdDadosTubo.CommandText = "SELECT ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, tubo.INF_COMPLEMENTAR " & _
                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                  " WHERE ANA.GR_ANA *= GRUPO_ANA.COD_GR_ANA " & _
                  " AND   ANA.COD_TUBO *= TUBO.COD_TUBO  " & _
                  " AND   ANA.COD_ANA_S = ?"
    End If
    CmdDadosTubo.Prepared = True
    CmdDadosTubo.Parameters.Append CmdDadosTubo.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)

    'Simples GhostMembers
    Set CmdDadosGhostM = New ADODB.Command
    Set CmdDadosGhostM.ActiveConnection = gConexao
    CmdDadosGhostM.CommandType = adCmdText
    CmdDadosGhostM.CommandText = "SELECT COD_MEMBRO, ORDEM  FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A' ORDER BY ORDEM"
    CmdDadosGhostM.Prepared = True
    CmdDadosGhostM.Parameters.Append CmdDadosTubo.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)

    TotalGhostsJaTratados = 0
    ReDim GhostsJaTratados(0)
    
    Especiais = 0
    
    totalTubos = 0
    ReDim Tubos(0)

    i = 1
    'Verificar quais as etiquetas a criar
    While i <= UBound(MaReq)
        If MaReq(i).Cod_Perfil <> "0" Then
                ' PERFIS
                CmdDadosTubo.CommandText = "select ANA.cod_perfis cod_ana_s ,ana.cod_produto, ana.abr_ana_p abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                            " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                            " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, '' flg_etiq_abr, inf_complementar,num_copias " & _
                                            " from sl_perfis ana, sl_tubo tubo, sl_gr_ana grupo_ana " & _
                                            " where ana.gr_ana = grupo_ana.cod_gr_ana(+) and ana.cod_tubo = tubo.cod_tubo and ana.cod_perfis = ?"
                CmdDadosTubo.Parameters(0) = MaReq(i).Cod_Perfil
                Set RsDadosTubo = CmdDadosTubo.Execute
                
                'COMPLEXAS
                If RsDadosTubo.EOF = True Then
                    CmdDadosTubo.CommandText = "select ana.cod_ana_c cod_ana_s, ana.cod_produto, ana.abr_ana_c abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                                " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                                " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, ana.flg_etiq_abr, inf_complementar, num_copias " & _
                                                " from sl_ana_c ana, sl_tubo tubo, sl_gr_ana grupo_ana " & _
                                                " where ana.gr_ana = grupo_ana.cod_gr_ana(+) and ana.cod_tubo = tubo.cod_tubo and ana.cod_ana_c = ?"
                    CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsDadosTubo = CmdDadosTubo.Execute
                End If
                
                'SIMPLES
                If RsDadosTubo.EOF = True Then
                    CmdDadosTubo.CommandText = "SELECT ana.cod_ana_s,ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, num_copias " & _
                                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                                  " AND   ANA.COD_ANA_S = ?"

                    CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_s
                    Set RsDadosTubo = CmdDadosTubo.Execute
                End If
                

        ElseIf MaReq(i).cod_ana_c <> "0" Then
                'COMPLEXAS
                CmdDadosTubo.CommandText = "select ana.cod_ana_c cod_ana_s, ana.cod_produto, ana.abr_ana_c abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                            " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                            " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, ana.flg_etiq_abr, inf_complementar, num_copias " & _
                                            " from sl_ana_c ana, sl_tubo tubo, sl_gr_ana grupo_ana " & _
                                            " where ana.gr_ana = grupo_ana.cod_gr_ana(+) and ana.cod_tubo = tubo.cod_tubo and ana.cod_ana_c = ?"
                CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_c
                Set RsDadosTubo = CmdDadosTubo.Execute
                
                'SIMPLES
                If RsDadosTubo.EOF = True Then
                    CmdDadosTubo.CommandText = "SELECT ana.cod_ana_s, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, num_copias " & _
                                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                                  " AND   ANA.COD_ANA_S = ?"

                    CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_s
                    Set RsDadosTubo = CmdDadosTubo.Execute
                End If
        
        ElseIf MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
            
'            'Verificar se j� n�o foram geradas etiquetas
'            ' para a complexa deste Ghostmember
'            For k = 1 To UBound(GhostsJaTratados)
'                If GhostsJaTratados(k) = MaReq(i).Cod_Ana_C Then
'                    'J� tratou passa � an�lise seguinte da MAREQ
'                    'ATEN��O Goto
'                    GoTo Fim
'                End If
'            Next k
'
'            'Regista a complexa do ghostmember para n�o a tratar mais
'            TotalGhostsJaTratados = TotalGhostsJaTratados + 1
'            ReDim Preserve GhostsJaTratados(TotalGhostsJaTratados)
'            GhostsJaTratados(TotalGhostsJaTratados) = MaReq(i).Cod_Ana_C
'
'            'Ciclo para todas as an�lises da gGHOSTMEMBER_S
'            CmdDadosGhostM.Parameters(0) = MaReq(i).Cod_Ana_C
'            Set RsDadosGhostM = CmdDadosGhostM.Execute
'
'            If RsDadosGhostM.EOF Then
'                RsDadosGhostM.Close
'                Set RsDadosGhostM = Nothing
'                'ATEN��O Goto
'                'Passa imediatamente para a an�lise seguinte na estrutura MAREQ
'                GoTo Fim
'            End If
'
'            While Not RsDadosGhostM.EOF
'                CmdDadosTubo.Parameters(0) = BL_HandleNull(RsDadosGhostM!COD_MEMBRO, "0")
'                Set RsDadosTubo = CmdDadosTubo.Execute
'
'                'ATEN��O Goto
'                'Trata a an�lise simples pertencente � complexa do GhostMember
'                GoTo TrataGhostMember
'
'SeguinteGhostMember:
'
'                RsDadosGhostM.MoveNext
'            Wend
'
'            RsDadosGhostM.Close
'            Set RsDadosGhostM = Nothing
'
'            'ATEN��O Goto
'            'Passa imediatamente para a an�lise seguinte na estrutura MAREQ
            
            'soliveira teste: para ghost members n�o interessa imprimir uma etiqueta para cada membro
            '                 passar a ver tubo associado � complexa : fazer (comentei tudo, expecto linha seguinte
            GoTo fim
        Else
        
        'Bruno 15-02-2007
            If gSGBD = gOracle Then
                CmdDadosTubo.CommandText = "SELECT  ana.cod_ana_s, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                      " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, NUM_COPIAS " & _
                                      " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                      " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                      " AND   ANA.COD_TUBO = TUBO.COD_TUBO (+) " & _
                                      " AND   ANA.COD_ANA_S = ?"
            ElseIf gSGBD = gSqlServer Then
                CmdDadosTubo.CommandText = "SELECT  ana.cod_ana_s, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                      " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, NUM_COPIAS " & _
                                      " FROM  SL_ANA_S ANA RIGHT OUTER JOIN SL_GR_ANA GRUPO_ANA ON ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA, " & _
                                      " SL_ANA_S ANA2 RIGHT OUTER JOIN SL_TUBO tubo ON ANA2.COD_TUBO= TUBO.COD_TUBO " & _
                                      " WHERE  ANA2.COD_ANA_S = ANA.COD_ANA_S " & _
                                      " AND   ANA.COD_ANA_S = ?"
            End If
            CmdDadosTubo.Parameters(0) = MaReq(i).cod_ana_s
            Set RsDadosTubo = CmdDadosTubo.Execute
        End If

TrataGhostMember:
        
        If Not RsDadosTubo.EOF Then
            
'            If BL_HandleNull(RsDadosTubo!cod_etiq, "0") <> "0" Then
             If BL_HandleNull(RsDadosTubo!cod_tubo, "0") <> "0" Then
                'Etiquetas especiais:
                ' Incluir c�digo do tubo da an�lise na requisi��o
                
                'Procura um tubo j� registado com o mesmo codigo
                ' para verificar se necessita de um novo tubo
                encontrou = False
                For k = 1 To totalTubos
                    If (Tubos(k).CodTubo = BL_HandleNull(RsDadosTubo!cod_tubo, "") Or _
                       BL_HandleNull(RsDadosTubo!cod_tubo, "") = "") Then
                            
                            'imprimir as abreviaturas das analises do tubo
                            If BL_HandleNull(RsDadosTubo!flg_etiq_abr, "") = 1 And InStr(Tubos(k).abrAna, BL_HandleNull(RsDadosTubo!abr_ana_s, "...")) = 0 Then
                                Tubos(k).abrAna = Tubos(k).abrAna & "," & BL_HandleNull(RsDadosTubo!abr_ana_s, "...")
                            End If
                            If InStr(1, Tubos(k).Descr_etiq_ana, BL_RetornaDescrEtiqAna(BL_HandleNull(RsDadosTubo!cod_ana_s, ""))) = 0 Then
                                Tubos(k).Descr_etiq_ana = Tubos(k).Descr_etiq_ana & BL_RetornaDescrEtiqAna(BL_HandleNull(RsDadosTubo!cod_ana_s, ""))
                            End If
                            
                            encontrou = True
                            Exit For
                    End If
                Next k

                If encontrou = False Then
                    'N�o encontrou
                    totalTubos = totalTubos + 1
                    ReDim Preserve Tubos(totalTubos)
                    CodTuboBarLoc = ""
                    CodTuboBarLoc = BL_HandleNull(RsDadosTubo!cod_etiq, "")
                    
                    Call PreencheDadosTubo(totalTubos, _
                                           BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
                                           IIf(RsDadosTubo!flg_etiq_abr = 1, BL_HandleNull(RsDadosTubo!abr_ana_s, ""), ""), _
                                           BL_HandleNull(RsDadosTubo!cod_produto, ""), _
                                           BL_HandleNull(RsDadosTubo!cod_tubo, ""), _
                                           True, _
                                           True, _
                                           CodTuboBarLoc, _
                                           -1, _
                                           -1, _
                                           BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""), BL_HandleNull(RsDadosTubo!inf_complementar, ""), BL_HandleNull(RsDadosTubo!num_copias, 1), _
                                           BL_RetornaDescrEtiqAna(BL_HandleNull(RsDadosTubo!cod_ana_s, "")))
                                           
                    Especiais = Especiais + 1
                End If
            End If
                
'            Else
'                'Etiquetas normais
'                'Procura um tubo j� registado com o mesmo produto
'                ' para verificar se necessita de um novo tubo
'                Encontrou = False
'
'                For k = 1 To TotalTubos
'                    '(Tubos(k).CodProd = BL_HandleNull(RsDadosTubo!cod_produto, "") And
'                    If (Tubos(k).CodTubo = BL_HandleNull(RsDadosTubo!Cod_Tubo, "") And _
'                        Tubos(k).Especial = False And _
'                        Tubos(k).Cheio = False) Or _
'                       (BL_HandleNull(RsDadosTubo!Cod_Tubo, "") = "") Then
'
'                            Encontrou = True
'                            Exit For
'                    End If
'                Next k
'
'                If Encontrou = False Then
'                    'N�o encontrou
'                    TotalTubos = TotalTubos + 1
'                    ReDim Preserve Tubos(TotalTubos)
'
'                    If (BL_HandleNull(RsDadosTubo!flg_etiq_prod, "0") = "0") Then
'                        Call PreencheDadosTubo(TotalTubos, _
'                                               BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
'                                               BL_HandleNull(RsDadosTubo!abr_ana_s, ""), _
'                                               "", _
'                                               BL_HandleNull(RsDadosTubo!Cod_Tubo, ""), _
'                                               False, _
'                                               False, _
'                                               "0", _
'                                               BL_HandleNull(RsDadosTubo!capaci_util, 0), _
'                                               BL_HandleNull(RsDadosTubo!qt_min, 0), _
'                                               BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""))
'
'                    Else
'                        Call PreencheDadosTubo(TotalTubos, _
'                                               BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
'                                               BL_HandleNull(RsDadosTubo!abr_ana_s, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Produto, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Tubo, ""), _
'                                               False, _
'                                               False, _
'                                               "0", _
'                                               BL_HandleNull(RsDadosTubo!capaci_util, 0), _
'                                               BL_HandleNull(RsDadosTubo!qt_min, 0), _
'                                               BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""))
'                    End If
'
'                Else
'                    'Encontrou
'                    CalculaCapaci = BL_HandleNull(RsDadosTubo!qt_min, 0) + (BL_HandleNull(RsDadosTubo!qt_min, 0) * 0.3)
'                    If (CalculaCapaci + Tubos(k).QtOcup > Tubos(k).QtMax) Then
'                        'Ultrapassou a capacidade m�xima do tubo
'                        Tubos(k).Cheio = True
'                        TotalTubos = TotalTubos + 1
'                        ReDim Preserve Tubos(TotalTubos)
'                        Call PreencheDadosTubo(TotalTubos, _
'                                               BL_HandleNull(RsDadosTubo!descr_gr_ana, ""), _
'                                               BL_HandleNull(RsDadosTubo!abr_ana_s, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Produto, ""), _
'                                               BL_HandleNull(RsDadosTubo!Cod_Tubo, ""), _
'                                               False, _
'                                               False, _
'                                               "0", _
'                                               BL_HandleNull(RsDadosTubo!capaci_util, 0), _
'                                               CalculaCapaci, _
'                                               BL_HandleNull(RsDadosTubo!TUBO_DESCR_TUBO, ""))
'
'                    Else
'                        Tubos(TotalTubos).QtOcup = Tubos(TotalTubos).QtOcup + CalculaCapaci
'                        If Tubos(k).QtOcup = Tubos(k).QtMax And Tubos(k).QtMax <> 0 Then
'                            Tubos(TotalTubos).Cheio = True
'                        End If
'                    End If
'                'Fim de verifica��o da pesquisa de tubo
'                End If
'            'Fim de verifica��o de etiquetas especiais ou n�o
'            End If

        'Fim de verifica��o de RsDadosTubo.EOF
        End If
        RsDadosTubo.Close
        Set RsDadosTubo = Nothing
        
        'ATEN��O Goto
        'Passa para a an�lise seguinte do ghostmember
        'soliveira teste : comentei
'        If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then GoTo SeguinteGhostMember

fim:
        
        'an�lise seguinte
        i = i + 1
        
    Wend

'DEBUG :
'    For i = 1 To TotalTubos
'        SQL = "INDICE " & i & _
'            ", ANA " & Tubos(i).AbrAna & _
'            ", GRANA " & Tubos(i).GrAna & _
'            ", TUBO " & Tubos(i).CodTubo & _
'            ", PROD " & Tubos(i).CodProd & _
'            ", ESPECIAL " & Tubos(i).Especial & _
'            ", PRODBAR " & Tubos(i).codtubobar & _
'            ", ORDANA " & Tubos(i).OrdAna & _
'            ", QTMAX " & Tubos(i).QtMax & _
'            ", QTOCUP " & Tubos(i).QtOcup & _
'            ", CHEIO " & Tubos(i).Cheio
'        MsgBox SQL
'    Next i

    If totalTubos >= 0 Then
        EcEtqNTubos = totalTubos
        EcEtqNEsp = Especiais
        
        'Aten��o - O FormNEtiq retorna nos campos deste form :
        ' EcEtqNAdm   = n� etiquetas administrativas
        ' EcEtqNCopias  = n� copias de etiquetas tubos
        ' EcEtqTipo   = tipo de impress�o
        '   0 - imprimir etq. para tubos e administrativas
        '   1 - imprimir etq. administrativas
        '   2 - imprimir etq. para tubos
        '  -1 - quando se cancela a impress�o
        '
        ' EcEtqNEsp envia para o form o n� etiquetas especiais (com o c�digo do produto ou ordem de an�lises)
        ' EcEtqNTubos envia para o form o n� etiquetas para tubos
        
        If EFR_Verifica_ARS(EcCodEFR.text) = True Then
            ReDim p1(0)
            Dim ExisteP1 As Boolean
            ExisteP1 = False
            For i = 1 To RegistosA.Count - 1
                
                For j = 1 To UBound(p1)
                    If RegistosA(i).NReqARS = p1(j).NrP1 Then
                        ExisteP1 = True
                    End If
                Next j
                If ExisteP1 = False Then
                    ReDim Preserve p1(UBound(p1) + 1)
                    p1(UBound(p1)).NrP1 = RegistosA(i).NReqARS
                End If
                ExisteP1 = False
            Next i
            
            TotalP1s = UBound(p1)
            gNumEtiqAdminDefeito = TotalP1s + 1
        Else
            TotalP1s = 0
            gNumEtiqAdminDefeito = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NumEtiqAdminDefeito"))
        End If
        
        FormNEtiq.Show vbModal
        
        FormGestaoRequisicao.SetFocus
                
        If Trim(EcEtqNAdm) = "" Then EcEtqNAdm = "0"
        If Trim(EcEtqNCopias) = "" Then EcEtqNCopias = "0"
        If Trim(EcEtqNEsp) = "" Then EcEtqNEsp = "0"
        If Trim(EcEtqNTubos) = "" Then EcEtqNTubos = "0"
        
        If EcEtqTipo <> "-1" Then
        
            EtqAdministrativas = EcEtqNAdm
        
            If totalTubos <> 0 Or EtqAdministrativas <> 0 Then
            'FeriasFernando
                If gLAB = "HOVAR" Then
                    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "ETIQ_CRYSTAL")
                Else
                    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
                
                If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                End If
                
                If EtqCrystal = "1" Then
                    If BL_PreviewAberto("Etiquetas") = True Then Exit Sub
            
                    continua = BL_IniciaReport("Etiqueta", "Etiquetas", crptToPrinter, False)
                    If continua = False Then Exit Sub
            
                    Call Cria_TmpEtq
                Else

                End If
                
                NumReqBar = ""
                For j = 1 To (7 - Len(EcNumReq))
                    NumReqBar = NumReqBar & "0"
                Next j
                NumReqBar = NumReqBar & EcNumReq
                
                If EcEtqTipo <> "1" Then
                    'Tubos
                    'N� de c�pias das etiquetas
                    For j = 1 To EcEtqNCopias
                        For i = 1 To totalTubos
                            If EtqCrystal = "1" Then
                        'Bruno 28-01-2003
                                If (Tubos(i).Especial = True And Tubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else
                                    CodProd_aux = Tubos(i).CodTubo
                                    Tubos(i).CodTubo = ""
                                    If gLAB = "CHVNG" Then
                                        dataImpressao = BL_HandleNull(Tubos(i).dt_chega, "")
                                    Else
                                        dataImpressao = BL_HandleNull(Tubos(i).dt_chega, BL_HandleNull(EcDataChegada, EcDataPrevista))
                                    End If
                                    sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                        " ( gr_ana, abr_ana, t_utente, utente, n_proc, n_req, t_urg, dt_req, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                        " VALUES (" & BL_TrataStringParaBD(Tubos(i).GrAna) & "," & BL_TrataStringParaBD(Tubos(i).abrAna) & "," & BL_TrataStringParaBD(CbTipoUtente.text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                        ",'" & EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2 & _
                                        "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " ") & _
                                        "'," & BL_TrataDataParaBD(dataImpressao) & "," & _
                                        "'" & Tubos(i).CodProd & IIf(Tubos(i).CodTubo <> "", " (", " ") & Tubos(i).CodTubo & IIf(Tubos(i).CodTubo <> "", ")'", "'") & ", " & _
                                        BL_TrataStringParaBD(BL_AbreviaNome(EcNome, 40)) & ",'" & Tubos(i).codTuboBar & NumReqBar & "', '" & BG_CvPlica(Tubos(i).Designacao_tubo) & "', '" & situacao & "', '" & episodio & "', '" & idade & "')"
                                    
                                    BG_ExecutaQuery_ADO sql
                            
                                    If gSQLError <> 0 Then
                                        BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                        Set CmdDadosTubo = Nothing
                                        Set CmdDadosGhostM = Nothing
                                        Exit Sub
                                    End If
                                End If
                            Else
                                If (Tubos(i).Especial = True And Tubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else
                                    
                                    If Not LerEtiqInI Then
                                        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                        Exit Sub
                                    End If
                                    If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                        MsgBox "Imposs�vel abrir impressora etiquetas"
                                        Exit Sub
                                    End If
                                    If gLAB = "CHVNG" Then
                                        dataImpressao = BL_HandleNull(Tubos(i).dt_chega, "")
                                    Else
                                        dataImpressao = BL_HandleNull(Tubos(i).dt_chega, BL_HandleNull(EcDataChegada, EcDataPrevista))
                                    End If
                                    Call EtiqPrint(Tubos(i).GrAna, Tubos(i).abrAna, _
                                                   CbTipoUtente.text, EcUtente, _
                                                   EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2, _
                                                   EcNumReq, IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                                   dataImpressao, _
                                                   Tubos(i).CodProd & IIf(Tubos(i).CodTubo <> "", " (", " ") & Tubos(i).CodTubo & IIf(Tubos(i).CodTubo <> "", ")", ""), _
                                                   EcNome, EcAbrevUte.text, Tubos(i).codTuboBar & NumReqBar, _
                                                   BG_CvPlica(Tubos(i).Designacao_tubo), Tubos(i).inf_complementar, Tubos(i).num_copias, Tubos(i).Descr_etiq_ana, Tubos(i).num_copias, "", Tubos(i).codTuboBar)
                                    
                                    EtiqClosePrinter
                                    
                                    BL_RegistaImprEtiq EcNumReq, Tubos(i).CodTubo
                                End If
                            End If
                        Next i
                    Next j
                End If
                
                If EcEtqTipo <> "2" Then
                    'Administrativas
                    
                    '******INICIO ETIQUETA RESUMO*************************
                    If gImprimirEtiqResumo = mediSim Then
                    
                        totalTubos = 0
                    
                        NumReqBar = ""
                        For j = 1 To (7 - Len(EcNumReq))
                            NumReqBar = NumReqBar & "0"
                        Next j
                        NumReqBar = NumReqBar & EcNumReq
                    
                        Dim RsEtiqResumo As ADODB.recordset
                        Dim TmpStr As String
                        Dim StrComplexas As String
                        Dim VecAbr() As String
                        Dim TotalVecAbr As Integer
                        Dim flgImprimir As Boolean
                                
                        Set RsEtiqResumo = New ADODB.recordset
                        RsEtiqResumo.CursorLocation = adUseServer
                        RsEtiqResumo.CursorType = adOpenStatic
                        
                        StrComplexas = "" ' Imprime so uma vez a descricao de cada complexa
                        TotalVecAbr = 0
                        For i = 1 To UBound(MaReq())
                            sql = ""
                            If MaReq(i).cod_ana_c <> "0" And InStr(1, StrComplexas, MaReq(i).cod_ana_c) = 0 Then
                                sql = "SELECT abr_ana_c abr_ana FROM sl_ana_c WHERE cod_ana_c='" & MaReq(i).cod_ana_c & "'"
                                StrComplexas = StrComplexas & MaReq(i).cod_ana_c & ";"
                            ElseIf MaReq(i).cod_ana_c = "0" And MaReq(i).cod_ana_s <> "" Then
                                sql = "SELECT abr_ana_s abr_ana FROM sl_ana_s WHERE cod_ana_s='" & MaReq(i).cod_ana_s & "'"
                            End If
                            If sql <> "" Then
                                RsEtiqResumo.Open sql, gConexao
                                If RsEtiqResumo.RecordCount > 0 Then
                                    TmpStr = TmpStr & BL_HandleNull(RsEtiqResumo!abr_ana, "")
                                    TotalVecAbr = TotalVecAbr + 1
                                    ReDim Preserve VecAbr(TotalVecAbr)
                                    VecAbr(TotalVecAbr) = BL_HandleNull(RsEtiqResumo!abr_ana, "")
                                End If
                                RsEtiqResumo.Close
                            End If
                        Next i
                        Set RsEtiqResumo = Nothing
                            
                        If TotalVecAbr < 12 Then
                            TotalVecAbr = 12
                            ReDim Preserve VecAbr(TotalVecAbr)
                        End If
                            
                        If EtqCrystal = "1" Then 'Crystal
                            If BL_PreviewAberto("EtiquetaResumo") = True Then Exit Sub
                                
                            continua = BL_IniciaReport("EtiquetaResumo", "Etiqueta Resumo", crptToPrinter, False, , , , , EcPrinterEtiq)
                            If continua = False Then Exit Sub
                            
                            Dim ReportResumo As CrystalReport
                            Set ReportResumo = forms(0).Controls("Report")
                                
                            For i = 0 To Printers.Count - 1
                                If Printers(i).DeviceName = EcPrinterEtiq Then
                                    ReportResumo.PrinterName = Printers(i).DeviceName
                                    ReportResumo.PrinterDriver = Printers(i).DriverName
                                    ReportResumo.PrinterPort = Printers(i).Port
                                End If
                            Next i
                            
                            flgImprimir = False
                            For i = 1 To TotalVecAbr
                                If i <= 12 Then
                                    ReportResumo.formulas(i - 1) = "Abr" & i & " ='" & VecAbr(i) & "'"
                                    flgImprimir = True
                                Else
                                    ReportResumo.formulas((i Mod 12) - 1) = "Abr" & i Mod 12 & " ='" & VecAbr(i) & "'"
                                    flgImprimir = True
                                End If
                                If i Mod 12 = 0 Then
                                    flgImprimir = False
                                    ReportResumo.formulas(i) = "NumReq='" & NumReqBar & "'"
                                    ReportResumo.Destination = crptToPrinter
                                    ReportResumo.Action = 1
                                    For j = 1 To 12
                                        ReportResumo.formulas(j - 1) = "Abr" & j & " =''"
                                    Next j
                        '            Call BL_ExecutaReport
                                End If
                            Next i
                            If flgImprimir Then
                                ReportResumo.formulas(i) = "NumReq='" & NumReqBar & "'"
                                ReportResumo.Destination = crptToWindow
                                ReportResumo.Action = 1
                            End If
                            
                            ReportResumo.Reset
                        Else
                            'ZPL
                            
                            NumReqBar = ""
                            For j = 1 To (7 - Len(EcNumReq))
                                NumReqBar = NumReqBar & "0"
                            Next j
                            NumReqBar = NumReqBar & EcNumReq
                            
                            TmpStr = ""
                            For i = 1 To TotalVecAbr
                                If i <= 12 Then
                                    TmpStr = TmpStr & VecAbr(i) & ","
                                    flgImprimir = True
                                Else
                                    TmpStr = TmpStr & VecAbr(i) & ","
                                    flgImprimir = True
                                End If
                                If i Mod 12 = 0 Then
                                    If Not LerEtiqIniResumo Then
                                        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                        Exit Sub
                                    End If
                                    If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                        MsgBox "Imposs�vel abrir impressora etiquetas"
                                        Exit Sub
                                    End If
                                    flgImprimir = False
                                    Call EtiqPrintResumo(TmpStr, NumReqBar)
                                    EtiqClosePrinter
                                    TmpStr = ""
                                End If
                            Next i
                            
                            If flgImprimir Then
                                If Not BL_LerEtiqIni("EtiqResumo") Then
                                    MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                    Exit Sub
                                End If
                                If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                    MsgBox "Imposs�vel abrir impressora etiquetas"
                                    Exit Sub
                                End If
                                Call EtiqPrintResumo(TmpStr, EcNumReq.text)
                                EtiqClosePrinter
                            End If
                        End If
                    End If
                    '******FIM ETIQUETA RESUMO*************************
                
                    For k = 1 To EtqAdministrativas
                        If EtqCrystal = "1" Then
                            If gLAB = "CHVNG" Then
                                dataImpressao = BL_HandleNull(EcDataChegada, "")
                            Else
                                dataImpressao = BL_HandleNull(EcDataChegada, EcDataPrevista)
                            End If

                            sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                " ( t_utente, utente, n_proc, n_req, t_urg, dt_req, ordem, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                " VALUES (" & BL_TrataStringParaBD(CbTipoUtente.text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                ",'" & EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2 & _
                                "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " ") & _
                                "'," & BL_TrataDataParaBD(dataImpressao) & ",null,null, " & _
                                BL_TrataStringParaBD(BL_AbreviaNome(EcNome, 40)) & ",'" & NumReqBar & "', 'Administrativa', '" & situacao & "', '" & episodio & "', '" & idade & "')"
                            BG_ExecutaQuery_ADO sql
                    
                            If gSQLError <> 0 Then
                                BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                Set CmdDadosTubo = Nothing
                                Set CmdDadosGhostM = Nothing
                                Exit Sub
                            End If
                        Else
                            ' --------
                            'Tubos(k).GrAna = "Administrat."
                            
                            If Not LerEtiqInI Then
                                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                Exit Sub
                            End If
                            If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                MsgBox "Imposs�vel abrir impressora etiquetas"
                                Exit Sub
                            End If
                            
                                                       
                            
                            If TotalP1s >= k Then
                                NrReqARS = p1(k).NrP1
                            Else
                                NrReqARS = ""
                            End If
                            If gLAB = "CHVNG" Then
                                dataImpressao = BL_HandleNull(EcDataChegada, "")
                            Else
                                dataImpressao = BL_HandleNull(EcDataChegada, EcDataPrevista)
                            End If
                            Call EtiqPrint("", _
                                           "", _
                                           CbTipoUtente.text, _
                                           EcUtente, _
                                           EcProcHosp1 & IIf(Trim(EcProcHosp1) <> "" And Trim(EcProcHosp2) <> "", "/", "") & EcProcHosp2, _
                                           EcNumReq, _
                                           IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                           dataImpressao, _
                                           "", EcNome, EcAbrevUte.text, "", "Administrativa", "", "1", "", 1, NrReqARS, "")
                                           
                            EtiqClosePrinter
                        End If
                    Next k
                End If
                
               
                
                If EtqCrystal = "1" Then
                    'Imprime o relat�rio no Crystal Reports
            
                    Dim Report As CrystalReport
                    Set Report = forms(0).Controls("Report")
            
                    Report.SQLQuery = "SELECT " & _
                                      "     T_UTENTE, " & _
                                      "     UTENTE, " & _
                                      "     N_PROC, " & _
                                      "     N_REQ, " & _
                                      "     T_URG, " & _
                                      "     DT_REQ, " & _
                                      "     ORDEM, " & _
                                      "     PRODUTO, " & _
                                      "     NOME_UTE, " & _
                                      "     N_REQ_BAR " & _
                                      "FROM " & _
                                      "     SL_CR_ETIQ" & gNumeroSessao & " SL_CR_ETIQ"
                                        
                    For i = 0 To Printers.Count - 1
                        If Printers(i).DeviceName = EcPrinterEtiq Then
                            Report.PrinterName = Printers(i).DeviceName
                            Report.PrinterDriver = Printers(i).DriverName
                            Report.PrinterPort = Printers(i).Port
                        End If
                    Next i
                    
                    If Trim(gEtiqPreview) Then
                        Report.Destination = crptToWindow
                        Report.WindowState = crptMaximized
                        Report.PageShow Report.ReportStartPage
                    Else
                        Report.Destination = crptToPrinter
                    End If
                    
                    Call BL_ExecutaReport
                    
                    If Trim(gEtiqPreview) Then
                        Report.PageZoom (200)
                    End If
                    
                    'Elimina as Tabelas criadas para a impress�o dos relat�rios
                    Call BL_RemoveTabela("sl_cr_etiq" & gNumeroSessao)
                Else
                    'EtiqClosePrinter
                End If
            End If
            
            If Me.EcImprimirResumo = "1" Then ImprimeResumo
        End If
    End If
    
    Set CmdDadosTubo = Nothing
    Set CmdDadosGhostM = Nothing
     
End Sub

Sub ImprimeResumo()
    If EcNumReq <> "" Then
        FR_ImprimeResumoCrystal_HOSP EcNumReq
    End If
    'ImprimeResumoFicheiro
End Sub

Private Sub CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)

    ' Tipo = ANA ou PROD
    ' EmCascata = True : Cria todos elementos at� o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRegProd As New ClRegProdutos
    Dim AuxRegAna As New ClRegAnalises
    Dim AuxRegTubo As New ClRegTubos
    Dim AuxRegRecManuais As New ClRegRecibosAna
    Dim AuxRegEnt As New ClRegRecibos
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "PROD" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegProd
            Next i
        Else
            Coleccao.Add AuxRegProd
        End If
    ElseIf tipo = "TUBO" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegTubo
            Next i
        Else
            Coleccao.Add AuxRegTubo
        End If
    ElseIf tipo = "REC_MAN" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegRecManuais
            Next i
        Else
            Coleccao.Add AuxRegRecManuais
        End If
    ElseIf tipo = "REC" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegEnt
            Next i
        Else
            Coleccao.Add AuxRegEnt
        End If
    Else
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegAna
            Next i
        Else
            Coleccao.Add AuxRegAna
        End If
    End If
    
    Set AuxRegAna = Nothing
    Set AuxRegProd = Nothing
    Set AuxRegTubo = Nothing
    
End Sub

Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)

    ' Index = -1 : Apaga todos elementos
    
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        Coleccao.Remove Index
    End If

End Sub

Function Verifica_Prod_ja_Existe(indice As Integer, Produto As String) As Boolean
    
    Dim i As Integer
    Dim k As Integer
    Dim TotalRegistos As Integer
    
    Verifica_Prod_ja_Existe = False
    TotalRegistos = RegistosP.Count - 1
    If indice >= 0 Then
        i = 1
        While i <= TotalRegistos And Verifica_Prod_ja_Existe = False
            If UCase(Trim(Produto)) = UCase(Trim(RegistosP(i).CodProd)) And indice <> i Then
                Verifica_Prod_ja_Existe = True
            End If
            i = i + 1
        Wend
    End If

End Function

Function Verifica_Ana_ja_Existe(indice As Long, codAgrup As String) As Boolean
        
    Dim i As Integer
    Dim TotalRegistos As Integer
    Dim CodAgrupAux As String
    Dim codAna As String
    
    codAna = codAgrup
    SELECT_Descr_Ana codAna
    

        
    Verifica_Ana_ja_Existe = False
    TotalRegistos = RegistosA.Count - 1
   
   If InStr(1, "SCP", UCase(Mid(codAgrup, 1, 1))) > 0 Then
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    Else
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    End If

    codAgrup = CodAgrupAux & codAgrup

End Function

Sub LimpaFgProd()
    
    Dim j As Long
    
    ExecutaCodigoP = False
    
    j = FgProd.rows - 1
    While j > 0
        If j > 1 Then
            FgProd.RemoveItem j
        Else
            FgProd.TextMatrix(j, 0) = ""
            FgProd.TextMatrix(j, 1) = ""
            FgProd.TextMatrix(j, 2) = ""
            FgProd.TextMatrix(j, 3) = ""
            FgProd.TextMatrix(j, 4) = ""
            FgProd.TextMatrix(j, 5) = ""
            FgProd.TextMatrix(j, 6) = ""
            FgProd.TextMatrix(j, 7) = ""
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoP = True
    LastColP = 0
    LastRowP = 1
        
    CriaNovaClasse RegistosP, 1, "PROD", True

End Sub

Sub LimpaFGAna()
    
    Dim j As Long
    
    ExecutaCodigoA = False
    
    j = FGAna.rows - 1
    While j > 0
        If j > 1 Then
            FGAna.RemoveItem j
        Else
            FGAna.TextMatrix(j, lColFgAnaCodAna) = ""
            FGAna.TextMatrix(j, lColFgAnaDescrAna) = ""
            If FGAna.Cols >= 4 Then
                FGAna.TextMatrix(j, lColFgAnaCred) = ""
                FGAna.TextMatrix(j, lColFgAnaOrdem) = ""
                FGAna.TextMatrix(j, lColFgAnaMedSaude) = ""
                FGAna.TextMatrix(j, lColFgAnaUnidSaude) = ""
            End If
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoA = True
    LastColA = 0
    LastRowA = 1
        
    CriaNovaClasse RegistosA, 1, "ANA", True

End Sub
'NELSONPSILVA CHVNG-7461 29.10.2018
Sub LimpaFgPedAdiciona()
    
    Dim j As Long
    
   ' ExecutaCodigoA = False
    
    j = FgPedAdiciona.rows - 1
    While j > 0
        If j > 1 Then
            FgPedAdiciona.RemoveItem j
        Else
            FgPedAdiciona.TextMatrix(j, 0) = ""
            FgPedAdiciona.TextMatrix(j, 1) = ""
            FgPedAdiciona.TextMatrix(j, 2) = ""
            FgPedAdiciona.TextMatrix(j, 3) = ""
            FgPedAdiciona.TextMatrix(j, 4) = ""
            FgPedAdiciona.TextMatrix(j, 5) = ""
            FgPedAdiciona.TextMatrix(j, 6) = ""
            FgPedAdiciona.TextMatrix(j, 7) = ""
        End If
        
        j = j - 1
    Wend

End Sub
'
Function Procura_Prod_ana() As Boolean

    'Verifica se existem an�lises com o produto que se pretende eliminar
    'No caso de existirem n�o deixa apagar o produto sem que se tenha apagado a an�lise

    Dim RsProd As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim ListaP As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset
    
    Procura_Prod_ana = False
    
    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            ListaP = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    CmdPesqGhost.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsPesqGhost = CmdPesqGhost.Execute
                    While Not RsPesqGhost.EOF
                        lista = lista & BL_TrataStringParaBD(BL_HandleNull(RsPesqGhost!cod_membro, "-1")) & ","
                        RsPesqGhost.MoveNext
                    Wend
                    RsPesqGhost.Close
                    Set RsPesqGhost = Nothing
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana_s from sl_ana_s WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_ana_s IN ( " & lista & ")"
            
            Set RsProd = New ADODB.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_ana_s, "") & ","
                RsProd.MoveNext
            Wend
            
            For i = 1 To UBound(MaReq)
                If MaReq(i).Cod_Perfil <> "" Then
                    If InStr(1, ListaP, MaReq(i).Cod_Perfil) = 0 Then
                        ListaP = ListaP & BL_TrataStringParaBD(MaReq(i).Cod_Perfil)
                    End If
                End If
            Next i
            
            sql = "SELECT descr_perfis from sl_perfis WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_perfis IN ( " & ListaP & ")"
            Set RsProd = New ADODB.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly
            
            ListaP = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_perfis, "") & ","
                RsProd.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Prod_ana = True
                
                If RsProd.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                End If
            End If
            
            RsProd.Close
            Set RsProd = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
    
End Function

Function Procura_Tubo_ana() As Boolean

    'Verifica se existem an�lises com o tubo que se pretende eliminar
    'No caso de existirem n�o deixa apagar o tubo sem que se tenha apagado a an�lise

    Dim rsTubo As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset
    
    Procura_Tubo_ana = False
    
    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_c = "C99999" Then
                    lista = lista & BL_TrataStringParaBD(MaReq(i).Cod_Perfil) & ","
                ElseIf MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_c) & ","
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana descr_ana_s from slv_analises WHERE cod_tubo = " & BL_TrataStringParaBD(FgTubos.TextMatrix(FgTubos.row, 0)) & _
                    " AND cod_ana IN ( " & lista & ")"
            
            Set rsTubo = New ADODB.recordset
            rsTubo.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            rsTubo.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not rsTubo.EOF
                lista = lista & BL_HandleNull(rsTubo!descr_ana_s, "") & ","
                rsTubo.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Tubo_ana = True
                
                If rsTubo.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                End If
            End If
            
            rsTubo.Close
            Set rsTubo = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
    
End Function
Function ValidaEspecifObrig() As Boolean

    Dim i As Integer
    Dim n As Integer
    
    n = RegistosP.Count
    If n > 0 Then
        n = n - 1
        For i = 1 To n
            If RegistosP(i).CodProd <> "" Then
                If RegistosP(i).EspecifObrig = "S" Then
                    If RegistosP(i).CodEspecif = "" Then
                        BG_Mensagem mediMsgBox, "O produto " & RegistosP(i).DescrProd & " exige especifica��o !", vbInformation, ""
                        SSTGestReq.Tab = 1
                        Exit Function
                    End If
                End If
            End If
        Next
    End If
    ValidaEspecifObrig = True
    
End Function

Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
Exit Function
erro:
    If Err.Number = 55 Then
        Close 1
        Open s For Input As 1
        Resume Next
    End If
    BG_LogFile_Erros "Erro ao abrir Ficheiro:" & s & " " & Err.Description, Me.Name, "LerEtiqIni", True
    Exit Function
    Resume Next
End Function
Private Function LerEtiqIniResumo() As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\ETIQRESUMO.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\ETIQRESUMO.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqIniResumo = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqIniResumo = True
    Exit Function
erro:
End Function
Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function

Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function

Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal abrev_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String, _
    ByVal quantidade As String, _
    ByVal Descr_etiq_ana As String, _
    ByVal num_copias As Integer, _
    Optional ByVal N_REQ_ARS As String, _
    Optional ByVal cod_etiq As String _
    ) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    Dim Nome1 As String
    Dim Nome2 As String
    Dim nome3 As String
    
    BL_TrataNomes RemovePortuguese(StrConv(nome_ute, vbUpperCase)), Nome1, Nome2, nome3

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", BL_HandleNull(n_proc, Utente))
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_AbreviaNome(RemovePortuguese(StrConv(nome_ute, vbUpperCase)), 40))
    sWrittenData = Replace(sWrittenData, "{NOME1}", Nome1)
    sWrittenData = Replace(sWrittenData, "{NOME2}", Nome2)
    sWrittenData = Replace(sWrittenData, "{NOME3}", nome3)
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", abrev_ute)
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", N_REQ_ARS)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", Descr_etiq_ana)
    sWrittenData = Replace(sWrittenData, "{QUANTIDADE}", BL_HandleNull(quantidade, "1"))
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", num_copias)
    sWrittenData = Replace(sWrittenData, "{COD_ETIQ}", cod_etiq)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", num_copias)
        EtiqEndJob = Replace(EtiqEndJob, "{QUANTIDADE}", BL_HandleNull(quantidade, "1"))
    End If
    EtiqPrint = True

End Function

Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function

Private Sub TAna_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ana As String
    Dim Flg_EncontrouMAReq As Boolean
    Dim Flg_EncontrouRegistosA As Boolean
    Dim i As Integer

    If KeyCode = 46 And InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, "S") <> 0 And (InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, "C") <> 0 Or InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, "P") <> 0) Then   'Delete de um nodo filho (analise simples) de uma complexa ou perfil
        'Apagar analise das estruturas
        Flg_EncontrouMAReq = False
        i = 1
        While Not Flg_EncontrouMAReq
            If MaReq(i).descr_ana_s = TAna.Nodes.Item(TAna.SelectedItem.Index).text Then
                ana = MaReq(i).cod_ana_s
                Actualiza_Estrutura_MAReq i
                Flg_EncontrouMAReq = True
                TotalEliminadas = TotalEliminadas + 1
                ReDim Preserve Eliminadas(TotalEliminadas)
                Eliminadas(TotalEliminadas).codAna = ana
                Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                EliminaTuboEstrut

                EliminaProduto ana
            End If
            i = i + 1
        Wend
        If Flg_EncontrouMAReq Then
            TAna.Nodes.Remove (TAna.SelectedItem.Index)
        End If
    End If
    
End Sub

' Bot�o Anula Recibo

' Copia uma grid.

Private Function Copia_MSF_Grid(gr1 As MSFlexGrid, _
                               gr2 As MSFlexGrid)
    
    On Error Resume Next
    
    Dim i As Integer
    Dim j As Integer

    gr2.Clear
    
    gr2.Cols = gr1.Cols
    gr2.rows = gr1.rows
    
    ' Copia formata��o.
    gr2.BackColor = gr1.BackColor
    gr2.ForeColor = gr1.ForeColor
    gr2.CellFontName = gr1.CellFontName
    gr2.CellFontSize = gr1.CellFontSize
    gr2.BackColor = gr1.BackColor
'    gr2.GridColor = gr1.GridColor
'    gr2.GridColorFixed = gr1.GridColorFixed
    
    For i = 0 To gr2.Cols - 1
        gr2.ColWidth(i) = gr1.ColWidth(i)
        gr2.ColAlignment(i) = gr1.ColAlignment(i)
    Next
    
    For i = 0 To gr2.rows - 1
        gr2.RowHeight(i) = gr1.RowHeight(i)
    Next
    
    ' ---------------------
    
    ' Copia valores.
    For j = 0 To gr2.rows - 1
        For i = 0 To gr2.Cols - 1
            gr2.TextMatrix(j, i) = gr1.TextMatrix(j, i)
        Next
    Next
    
    ' ---------------------
    
    Copia_MSF_Grid = 1

End Function

' Subterf�gio para introduzir valores (num�ricos) na grid.

Private Sub Altera_Grid(GR As MSFlexGrid, _
                       linha As Integer, _
                       coluna As Integer, _
                       msg As String, _
                       title As String)
    
    On Error GoTo ErrorHandler
    
    Dim rv As String
    Dim aux1 As String
    Dim aux2 As Double
    
    rv = InputBox(msg, title)
    
    If (IsNumeric(rv)) Then
    
        aux1 = Trim(rv)
        rv = BG_CvDecimalParaCalculo(aux1)
        
        aux2 = CDbl(rv)
        rv = Trim(CStr(aux2))
        GR.TextMatrix(linha, coluna) = Format(rv, "0.00")
    Else
        If (Len(rv) > 0) Then
            MsgBox "Deve introduzir um valor num�rico !         ", vbExclamation, "Erro"
        End If
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("Emitir_Recibo_02 (FormGestaoRequisicao) " & Err.Number & " - " & Err.Description)
            
            Exit Sub
    End Select
End Sub
Function ValidaProdutos() As Boolean

    On Error GoTo ErrorHandler
    
    Dim ret As Boolean
    ret = True
    
    If (RegistosP.Count > 1) Then
        ret = True
    Else
        If (RegistosA.Count > 1) Then
            ret = False
        Else
            ret = True
        End If
    End If
    
    ValidaProdutos = ret
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Descricao (ValidaProdutos) -> " & Err.Description)
            ValidaProdutos = True
            Exit Function
    End Select
End Function

Function ValidaEpisodio() As Integer
    
    On Error GoTo ErrorHandler:
    
    Dim processo As String
    Dim nome As String
    Dim dt_nasc As String
    Dim rv As Integer
    
    If (ActualizaEpisodio = False) Then
        
        ' Activa a sele�ao de episodio.
        
        If (Trim(EcUtente.text) = "") Then
            MsgBox "Deve selecionar um Utente.       ", vbInformation, " Actualizar Epis�dio"
            ValidaEpisodio = -1
            Exit Function
        Else
            If (CbSituacao.ListIndex = mediComboValorNull) Then
                MsgBox "Deve indicar uma situa�ao.       ", vbInformation, " Actualizar Epis�dio"
                ValidaEpisodio = -1
                Exit Function
            End If
        End If
        
        EcEpisodioAux.text = EcEpisodio.text
        
        EcEpisodio.Visible = True
        EcEpisodio.Enabled = True
        
        EcEpisodioAux.Visible = True
        EcEpisodioAux.Enabled = True
        
        EcEpisodioAux.SelStart = Len(EcEpisodioAux.text)
        EcEpisodioAux.SetFocus
        
        ActualizaEpisodio = True
    Else
        
        If (Trim(EcUtente.text) = "") Then
            MsgBox "Deve selecionar um Utente.       ", vbInformation, " Actualizar Epis�dio"
            ValidaEpisodio = -1
            Exit Function
        Else
            
            ' Validar
        
            processo = EcUtente.text
            nome = ""
            dt_nasc = ""
            
            ' Manda-se o processo para tornar o processo mais fiavel.
            ' (Na pesquisa de Utente, usamos o codigo de sessao).
            rv = SONHO_Get_Processo(CbSituacao.ItemData(CbSituacao.ListIndex), _
                                    EcEpisodioAux.text, _
                                    processo, _
                                    nome, _
                                    dt_nasc)
                              
            If ((rv = 1) And (processo = Trim(EcUtente.text))) Then
                ' O processo coincide.
                EcEpisodio.text = EcEpisodioAux.text
                EcEpisodioAux.Visible = False
                EcEpisodioAux.Enabled = False
                
'                MsgBox vbCrLf & _
'                       "O epis�dio indicado corresponde ao Utente actual.            " & vbCrLf & vbCrLf, vbInformation, " Actualizar Epis�dio"
                ValidaEpisodio = 1
            Else
                If (rv = 1) Then
                    ' O processo nao coincide.
                    MsgBox vbCrLf & _
                        "O epis�dio indicado nao corresponde ao Utente actual." & vbCrLf & " An�lises n�o enviadas para o SONHO!" & vbCrLf, vbExclamation, " Actualizar Epis�dio"
                
                    EcEpisodio.text = ""
                    EcEpisodioAux.text = ""
                    EcEpisodioAux.Visible = False
                    EcEpisodioAux.Enabled = False
                    
                    ValidaEpisodio = -1
                Else
                    MsgBox vbCrLf & _
                        "O epis�dio n�o foi actualizado.             " & vbCrLf & vbCrLf, vbCritical, " Actualizar Epis�dio"
                    EcEpisodioAux.Visible = False
                    EcEpisodioAux.Enabled = False
                    ValidaEpisodio = -1
                End If
            
            End If
            
            ' -----------------------------------------------------
            
            ActualizaEpisodio = False
        
        End If
    End If

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BtActualizaEpisodio_Click (FormGestaoRequisicao) -> " & Err.Description)
            Exit Function
    End Select
End Function

Sub Grava_Medico_Requis()
    
    Dim sql As String
    Dim NReq As Long
    Dim RsMed As ADODB.recordset
    
    
    If Trim(EcNumReq) <> "" Then
        NReq = EcNumReq
    Else
        NReq = "-" & gNumeroSessao
    End If
    
    'Faz o UPDATE na tabela de m�dicos da requisi��o criados temporariamente com o NumeroSessao
    sql = "DELETE from sl_medicos_req WHERE n_req= " & CLng(NReq) & " AND flg_med_requis='1'"
    BG_ExecutaQuery_ADO sql
        
   If EcCodMedico.text <> "" Then
        sql = "INSERT into sl_medicos_req(n_req,cod_med,flg_med_requis,user_cri,dt_cri) values( " & _
                CLng(NReq) & "," & BL_TrataStringParaBD(EcCodMedico.text) & ",'1','" & _
                gCodUtilizador & "'," & BL_TrataDataParaBD(dataAct) & ")"
        BG_ExecutaQuery_ADO sql

    End If
    
End Sub

Sub PesquisaMedico()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 5) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 5) As Long
    Dim Headers(1 To 5) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    Headers(2) = "Nome"
    
    'ChavesPesq(3) = "servico"
    CamposEcran(3) = "servico"
    Tamanhos(3) = 3000
    Headers(3) = "Servico"
    
    'ChavesPesq(4) = "csaude"
    CamposEcran(4) = "csaude"
    Tamanhos(4) = 3000
    Headers(4) = "Centro Sa�de"
    
    'ChavesPesq(5) = "morada"
    CamposEcran(5) = "morada"
    Tamanhos(5) = 3000
    Headers(5) = "Morada"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_medicos"
    CampoPesquisa = "nome_med"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " M�dicos")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMedico.text = Resultados(1)
            EcNomeMedico.text = Resultados(2)
            Grava_Medico_Requis
            BtPesquisaMedico.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem m�dicos codificados", vbExclamation, "ATEN��O"
        EcCodMedico.SetFocus
    End If
End Sub

Sub BD_Insert_Utente()
    
    Dim SQLQuery As String
    Dim RsNUtente As ADODB.recordset
    Dim Query As String
    Dim i As Integer
    Dim seq As Long
    
    'se a data de inscri��o estiver vazia
    If gDUtente.dt_inscr = "" Then
        gDUtente.dt_inscr = Format(dataAct, gFormatoData)
    End If
   
    If gDUtente.dt_nasc_ute = "" Then
        gDUtente.dt_nasc_ute = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If gDUtente.t_utente = "" Then
        gDUtente.t_utente = "LAB"           'obrigar a que seja por defeito criado um utente LAB
    End If

    
    On Error GoTo Trata_Erro
        
    If Trim(gDUtente.Utente) = "" Then
        If BG_Mensagem(mediMsgBox, "Quer gerar um n�mero sequencial para o utente do tipo " & gDUtente.t_utente & "?", vbYesNo + vbDefaultButton2 + vbQuestion, "Gerar Sequencial") = vbNo Then
            Exit Sub
        End If
        
        'Determinar o novo numero para o Tipo Utente (TENTA 10 VEZES - LOCK!)
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraUtente(gDUtente.t_utente)
            i = i + 1
        Wend
        
        If seq = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero do utente!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
        
        EcUtente = seq
    Else
        Set RsNUtente = New ADODB.recordset
        RsNUtente.CursorLocation = adUseServer
        RsNUtente.Open "SELECT nome_ute, dt_nasc_ute FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & " AND utente = " & BL_TrataStringParaBD(gDUtente.Utente), gConexao, adOpenStatic, adLockReadOnly
        
        If RsNUtente.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe um utente do tipo " & gDUtente.t_utente & " com o n�mero " & gDUtente.Utente & "!" & Chr(13) & "Nome : " & BL_HandleNull(RsNUtente!nome_ute) & Chr(13) & "Dt. Nasc : " & BL_HandleNull(RsNUtente!dt_nasc_ute), vbOKOnly + vbExclamation, "Inserir Utente"
            RsNUtente.Close
            Set RsNUtente = Nothing
            Exit Sub
        Else
            RsNUtente.Close
            Set RsNUtente = Nothing
        End If
    End If
        
    'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
    i = 0
    seq = -1
    While seq = -1 And i <= 10
        seq = BL_GeraNumero("SEQ_UTENTE")
        i = i + 1
    Wend
    
    If seq = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
    EcSeqUtente = seq
    
    gSQLError = 0
    gSQLISAM = 0
    
    
    SQLQuery = "INSERT INTO sl_identif (seq_utente,t_utente,utente,n_proc_1,n_proc_2," & _
            " dt_inscr,n_cartao_ute,nome_ute,dt_nasc_ute,sexo_ute,est_civ_ute, " & _
            " descr_mor_ute,cod_postal_ute,telef_ute, cod_efr_ute,user_cri,dt_cri, cod_genero, cod_isencao_ute, telemovel, n_contrib_ute )" & _
            " values (" & EcSeqUtente & "," & BL_TrataStringParaBD(gDUtente.t_utente) & "," & BL_TrataStringParaBD(gDUtente.Utente) & "," & BL_TrataStringParaBD(gDUtente.n_proc_1) & "," & BL_TrataStringParaBD(gDUtente.n_proc_2) & _
            "," & BL_TrataDataParaBD(gDUtente.dt_inscr) & "," & BL_TrataStringParaBD(gDUtente.n_cartao_ute) & "," & BL_TrataStringParaBD(gDUtente.nome_ute) & "," & BL_TrataStringParaBD(gDUtente.dt_nasc_ute) & "," & BL_TrataStringParaBD(gDUtente.sexo_ute) & "," & BL_TrataStringParaBD(gDUtente.est_civil) & _
            "," & BL_TrataStringParaBD(gDUtente.morada) & "," & BL_TrataStringParaBD(gDUtente.cod_postal) & "," & BL_TrataStringParaBD(gDUtente.telefone) & "," & BL_TrataStringParaBD(BL_HandleNull(gDUtente.cod_efr, "0")) & ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(dataAct) & ","
    SQLQuery = SQLQuery & gDUtente.cod_genero & ","
    SQLQuery = SQLQuery & BL_TrataStringParaBD(gDUtente.cod_isencao_ute) & ","
    SQLQuery = SQLQuery & BL_TrataStringParaBD(gDUtente.telemovel) & ","
    SQLQuery = SQLQuery & BL_TrataStringParaBD(gDUtente.num_contribuinte) & ")"
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro
    
    If gSQLError <> 0 Then
        'Erro a inserir utente
        
        GoTo Trata_Erro
    End If
    

    gDUtente.seq_utente = BL_String2Double(EcSeqUtente)
    PreencheDadosUtente
    
    BG_CommitTransaction
    
    Exit Sub
    
Trata_Erro:
    BG_LogFile_Erros "FormIdentificaUtente: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos

End Sub

Private Sub AtualizaDadosUtente()
    Dim SQLQuery As String
    Dim RsNUtente As ADODB.recordset
    Dim Query As String
    Dim i As Integer
    Dim seq As Long
    Dim seq_utente As Long
    
    On Error GoTo Trata_Erro
    'se a data de inscri��o estiver vazia
    If gDUtente.dt_inscr = "" Then
        gDUtente.dt_inscr = Format(dataAct, gFormatoData)
    End If
   
    If gDUtente.dt_nasc_ute = "" Then
        gDUtente.dt_nasc_ute = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If gDUtente.t_utente = "" Then
        gDUtente.t_utente = "LAB"           'obrigar a que seja por defeito criado um utente LAB
    End If

    
        
    If Trim(gDUtente.Utente) <> "" Then
        Set RsNUtente = New ADODB.recordset
        RsNUtente.CursorLocation = adUseServer
        RsNUtente.Open "SELECT seq_utente, nome_ute, dt_nasc_ute FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & " AND utente = " & BL_TrataStringParaBD(gDUtente.Utente), gConexao, adOpenStatic, adLockReadOnly
        
        If RsNUtente.RecordCount <= 0 Then
            RsNUtente.Close
            Set RsNUtente = Nothing
            Exit Sub
        Else
            seq_utente = BL_HandleNull(RsNUtente!seq_utente, "")
            RsNUtente.Close
            Set RsNUtente = Nothing
        End If
    End If
        
    
    EcSeqUtente = seq_utente
    
    gSQLError = 0
    gSQLISAM = 0
    
    SQLQuery = "UPDATE sl_identif SET "
    SQLQuery = SQLQuery & "n_cartao_ute = " & BL_TrataStringParaBD(gDUtente.n_cartao_ute)
    SQLQuery = SQLQuery & ", nome_ute = " & BL_TrataStringParaBD(gDUtente.nome_ute)
    SQLQuery = SQLQuery & ", dt_nasc_ute = " & BL_TrataStringParaBD(gDUtente.dt_nasc_ute)
    SQLQuery = SQLQuery & ", sexo_ute = " & BL_TrataStringParaBD(gDUtente.sexo_ute)
    SQLQuery = SQLQuery & ", est_civ_ute = " & BL_TrataStringParaBD(gDUtente.est_civil)
    SQLQuery = SQLQuery & ", descr_mor_ute = " & BL_TrataStringParaBD(gDUtente.morada)
    SQLQuery = SQLQuery & ", cod_postal_ute = " & BL_TrataStringParaBD(gDUtente.cod_postal)
    SQLQuery = SQLQuery & ", telef_ute = " & BL_TrataStringParaBD(gDUtente.telefone)
    SQLQuery = SQLQuery & ", telemovel = " & BL_TrataStringParaBD(gDUtente.telemovel)
    SQLQuery = SQLQuery & ", n_contrib_ute = " & BL_TrataStringParaBD(gDUtente.num_contribuinte)
    SQLQuery = SQLQuery & ", cod_efr_ute = " & BL_TrataStringParaBD(gDUtente.cod_efr)
    SQLQuery = SQLQuery & " WHERE seq_utente = " & BL_TrataStringParaBD(CStr(seq_utente))
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro
    
    If gSQLError <> 0 Then
        'Erro a inserir utente
        
        GoTo Trata_Erro
    End If
    

    gDUtente.seq_utente = BL_String2Double(EcSeqUtente)
    PreencheDadosUtente
    
    
Exit Sub
Trata_Erro:
    BG_LogFile_Erros "AtualizaDadosUtente:-> " & Err.Description, Me.Name, "AtualizaDadosUtente", True
    Exit Sub
    Resume Next
End Sub
Function PreencheProvenDoc(nDoc As String) As String

    Dim sql As String
    Dim rs As ADODB.recordset
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    On Error GoTo TrataErro
    
    sql = "select c.cod_u_saude " & _
            "from sd_episod_act_med e, sd_cons_marc c " & _
            " where (e.flg_estado is null or e.flg_estado <> 'A') and " & _
            " e.T_Doente = c.T_Doente And e.Doente = c.Doente And e.cod_serv_exec = c.cod_serv " & _
            " and e.dt_act_med = c.dt_cons and e.tipo_origem = 'CON' " & _
            " and c.cod_serv= '3' and e.n_doc = '" & nDoc & "'"
    BG_ExecutaQuery_ADO sql
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        PreencheProvenDoc = BL_HandleNull(rs!cod_u_saude, "")
    Else
        PreencheProvenDoc = ""
    End If
    Exit Function
TrataErro:
    PreencheProvenDoc = ""
End Function



Sub DevolveEpisodio(SeqUtente As String)

    'Preenche Situacao e episodio caso o episodio esteja vazio

    Dim sql As String
    Dim RsEpis As ADODB.recordset
    
    On Error GoTo TrataErro
    
    If Trim(SeqUtente) <> "" Then
        sql = "SELECT n_epis, t_sit FROM sl_identif WHERE seq_utente=" & SeqUtente
        Set RsEpis = New ADODB.recordset
        RsEpis.CursorLocation = adUseServer
        RsEpis.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsEpis.Open sql, gConexao
        If RsEpis.RecordCount > 0 Then
            EcEpisodio.text = BL_HandleNull(RsEpis!n_epis, "")
            BG_MostraComboSel BL_HandleNull(RsEpis!t_sit, "-1"), CbSituacao
        End If
        RsEpis.Close
        Set RsEpis = Nothing
    End If
    
    Exit Sub
    
TrataErro:
    
    BG_LogFile_Erros "Erro ao procurar episodio (DevolveEpisodio) - " & Err.Description
    
End Sub

Function Verifica_Ana_Pertence_Local(codAgrup As String) As Boolean
    
    Dim RsLocal As ADODB.recordset
    
    Set RsLocal = New ADODB.recordset
    RsLocal.CursorLocation = adUseServer
    RsLocal.CursorType = adOpenStatic
    RsLocal.Source = "select * from slv_analises where cod_ana = " & BL_TrataStringParaBD(codAgrup) & " and cod_local = " & gCodLocal
    RsLocal.ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros RsLocal.Source
    RsLocal.Open
    If RsLocal.RecordCount <= 0 Then
        Verifica_Ana_Pertence_Local = False
    Else
        Verifica_Ana_Pertence_Local = True
    End If

End Function



' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Sub EliminaProduto(cod_ana As String)
    Dim i  As Integer
    Dim sSql As String
    Dim sSqlP As String
    Dim rsAna As New ADODB.recordset
    Dim RsProd As New ADODB.recordset
    Dim CodProduto As String
    Dim flg_PodeEliminar As Boolean
    Dim flg_existeProd As Boolean
    
    On Error GoTo TrataErro
    
    
    ' SE FOR COMPLEXA CHAMA A MESMA FUNCAO PARA CADA SIMPLES
    If Mid(cod_ana, 1, 1) = "C" Then
        sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            While Not rsAna.EOF
                EliminaProduto rsAna!cod_membro
                rsAna.MoveNext
            Wend
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
    
    ' SE FOR PERFIL CHAMA A MESMA FUNCAO PARA CADA MEMBRO
    If Mid(cod_ana, 1, 1) = "P" Then
        sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        sSqlP = "SELECT cod_produto FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlP
        rsAna.Open sSqlP, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_analise
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If
    
    ' ENCONTRA O CODIGO DO PRODUTO DA ANALISE EM CAUSA
    If Mid(cod_ana, 1, 1) = "S" Then
        CodProduto = ""
        sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
continuap:
            ' VERIFICA SE EXISTE PRODUTO NA FLEX GRID
            flg_existeProd = False
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    flg_existeProd = True
                    Exit For
                End If
            Next
            If flg_existeProd = False Then
                rsAna.Close
                Set rsAna = Nothing
                Exit Sub
            End If
            ' ---------------------------------------
        End If
        rsAna.Close
        Set rsAna = Nothing
        
        ' SE NAO TEM PRODUTO CODIFICADO...SAI
        If CodProduto = "" Then
            Exit Sub
        End If
        
        flg_PodeEliminar = VerificaPodeEliminarProduto(CodProduto)
        
        
        ' NAO ENCONTROU MAIS NENHUMA ANALISE COM ESSE TUBO LOGO PODE ELIMINAR
        If flg_PodeEliminar = True Then
            
            ' VERIFICA SE JA FOI DADO ENTRADA DO PRODUTO
            If gRegChegaTubos = 1 Then
            If EcNumReq <> "" Then
                sSql = "SELECT * FROM sl_req_prod WHERE n_req =" & EcNumReq
                sSql = sSql & " AND cod_prod = " & BL_TrataStringParaBD(CodProduto)
                RsProd.CursorLocation = adUseServer
                RsProd.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsProd.Open sSql, gConexao
                If RsProd.RecordCount > 0 Then
                    If BL_HandleNull(RsProd!dt_chega, "") <> "" Then
                        ' SE JA DEU ENTRADA NAO ELIMINA
                        RsProd.Close
                        Set RsProd = Nothing
                        Exit Sub
                    End If
                End If
                RsProd.Close
                Set RsProd = Nothing
                ' -----------------------------------------
            End If
            End If
            
            ' ELIMINA DA FLEX GRID E DA ESTRUTURA
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    FgProd.row = 1
                    If i < FgProd.rows - 1 Then
                        RegistosP.Remove i
                        FgProd.RemoveItem i
                    Else
                        FgProd.TextMatrix(i, 0) = ""
                        FgProd.TextMatrix(i, 1) = ""
                        FgProd.TextMatrix(i, 2) = ""
                        FgProd.TextMatrix(i, 3) = ""
                        FgProd.TextMatrix(i, 4) = ""
                        FgProd.TextMatrix(i, 5) = ""
                        FgProd.TextMatrix(i, 6) = ""
                        FgProd.TextMatrix(i, 7) = ""
                        RegistosP(i).CodProd = ""
                        RegistosP(i).DescrProd = ""
                        RegistosP(i).EspecifObrig = ""
                        RegistosP(i).CodEspecif = ""
                        RegistosP(i).DescrEspecif = ""
                        RegistosP(i).DtPrev = ""
                        RegistosP(i).DtChega = ""
                        RegistosP(i).EstadoProd = ""
                        RegistosP(i).Volume = ""
                    End If
                    Exit For
                End If
            Next
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicao -> Elimina��o de Produtos: " & Err.Description
    Exit Sub
    Resume Next
End Sub


Private Function VerificaPodeEliminarProduto(CodProduto As String) As Boolean
    Dim i As Integer
    Dim flg_Elimina As Boolean
    
    VerificaPodeEliminarProduto = True
    
    For i = 1 To RegistosA.Count - 1
        
        If Mid(RegistosA(i).codAna, 1, 1) = "C" Then
            flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, RegistosA(i).codAna)
        ElseIf Mid(RegistosA(i).codAna, 1, 1) = "P" Then
            flg_Elimina = VerificaPodeEliminarProduto_P(CodProduto, RegistosA(i).codAna)
        Else
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, RegistosA(i).codAna)
        End If
        
        If flg_Elimina = False Then
            VerificaPodeEliminarProduto = flg_Elimina
            Exit Function
        End If
    Next
    VerificaPodeEliminarProduto = True
End Function






Private Function VerificaPodeEliminarProduto_C(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
     On Error GoTo TrataErro
   
    flg_Elimina = False
    
    For i = 1 To UBound(MaReq)
        If MaReq(i).cod_ana_c = analise And MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, MaReq(i).cod_ana_s)
            If flg_Elimina = False Then
                VerificaPodeEliminarProduto_C = flg_Elimina
                Exit Function
            End If
        End If
    Next
    VerificaPodeEliminarProduto_C = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_C: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_C"
    VerificaPodeEliminarProduto_C = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_P(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    
    flg_Elimina = False
    
    sSql = "SELECT cod_produto from sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If Not rsAna.EOF Then
        If BL_HandleNull(rsAna!cod_produto, "") <> "" Then
            If rsAna!cod_produto <> CodProduto Then
                flg_Elimina = True
            End If
        Else
            For i = 1 To UBound(MaReq)
                If MaReq(i).Cod_Perfil = analise Then
                    If MaReq(i).cod_ana_c <> "0" And MaReq(i).cod_ana_c <> gGHOSTMEMBER_C And MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                        flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, MaReq(i).cod_ana_c)
                    ElseIf MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
                        flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, MaReq(i).cod_ana_s)
                    End If
                    
                    If flg_Elimina = False Then
                        VerificaPodeEliminarProduto_P = flg_Elimina
                    End If
                End If
            Next
        End If
        
    End If
    
        
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarProduto_P = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_P: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_P"
    VerificaPodeEliminarProduto_P = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_S(CodProduto As String, analise As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    
    sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(analise)
    sSql = sSql & " AND cod_produto = " & BL_TrataStringParaBD(CodProduto)
    
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaPodeEliminarProduto_S = False
    Else
        VerificaPodeEliminarProduto_S = True
    End If
    
    rsAna.Close
    Set rsAna = Nothing
End Function


Sub Lista_GhostMembers(CodComplexa As String, complexa As String, Flg_Facturado As Integer)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_membro"
    CamposEcran(1) = "cod_membro"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_membro, sl_ana_s "
    CampoPesquisa = "descr_ana_s"
    CWhere = " sl_membro.cod_membro = sl_ana_s.cod_ana_s and cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                        ChavesPesq, _
                                                                        CamposEcran, _
                                                                        CamposRetorno, _
                                                                        Tamanhos, _
                                                                        Headers, _
                                                                        CWhere, _
                                                                        CFrom, _
                                                                        "", _
                                                                        CampoPesquisa, _
                                                                        " ORDER BY sl_membro.ordem ", _
                                                                        " Membros da an�lise " & complexa)
    
    If PesqRapida = True Then

        FormPesqRapidaAvancadaMultiSel.Show vbModal
        
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            For i = 1 To TotalElementosSel
                InsereGhostMember CStr(Resultados(i))
'                FormGestaoRequisicao.EcAuxAna.Enabled = True
'                FormGestaoRequisicao.EcAuxAna = Resultados(i)
'                FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
            Next i
            FormGestaoRequisicao.FGAna.SetFocus
        Else
            i = FGAna.row
            Dim ana As String
            ana = RegistosA(i).codAna
            FGAna.row = i
            Elimina_Mareq RegistosA(i).codAna
            ana = RegistosA(i).codAna
            RegistosA.Remove i
            If RegistosA.Count = 0 Then
                CriaNovaClasse RegistosA, 1, "ANA"
            End If
            If FGAna.rows = 2 Then
                FGAna.AddItem ""
            End If
            FGAna.RemoveItem i
            EliminaTuboEstrut
            EliminaProduto ana
        
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem membros", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicao" Then
            FormGestaoRequisicao.FGAna.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Lista_GhostMembers: " & Err.Number & " - " & Err.Description, Me.Name, "Lista_GhostMembers"
    Exit Sub
    Resume Next
End Sub

Private Function IdentificaGarrafa(Optional codAnalise As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim rs1Res As New ADODB.recordset
    Dim Rs2Res As New ADODB.recordset
    Dim i As Integer
    Dim flg_resultado As Boolean
    Dim cod_tubo As String
    On Error GoTo TrataErro
    If IsMissing(codAnalise) = True Or codAnalise = "" Then
        codAnalise = FGAna.TextMatrix(FGAna.row, lColFgAnaCodAna)
    End If
    
    ' S� verifica para perfis
    If Mid(codAnalise, 1, 1) = "P" Then
        sSql = "SELECT flg_garrafa,cod_tubo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAnalise)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            If BL_HandleNull(rsAna!flg_garrafa, 0) = 1 Then
                cod_tubo = BL_HandleNull(rsAna!cod_tubo, "")
                For i = 1 To FgTubos.rows - 1
                    If cod_tubo = FgTubos.TextMatrix(i, 0) Then
                        If FgTubos.TextMatrix(i, 2) = "" Then
                            IdentificaGarrafa = True
                        Else
                            IdentificaGarrafa = False
                        End If
                        Exit Function
                    End If
                Next i
            Else
                IdentificaGarrafa = False
            End If
        Else
            IdentificaGarrafa = False
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "IdentificaGarrafa: " & Err.Number & " - " & Err.Description, Me.Name, "IdentificaGarrafa"
    Exit Function
    Resume Next
End Function

' FGONCALVES - HMILITAR / soliveira - CHVNG / soliveira - HCVP
' 23 SET 2006           / 16 JAN 2007       / 09 JAN 2008
Private Function ActualizaProcessoUtente() As Boolean
    On Error GoTo TrataErro
    Dim SelTotal As Boolean
    Dim SelCsi As Boolean
    Dim SimNao As Integer
    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim WhereAnd As String
    Dim HisAberto As Integer
    Dim RetPesquisaSONHO As Integer
    Dim situacao As String
    Dim NumEpisodio As String
    Dim rs As New ADODB.recordset
    Dim t_episodio As String
    
    ActualizaProcessoUtente = False
    
    If Trim(EcEpisodio.text) = "" And CbSituacao.ListIndex = mediComboValorNull And UCase(HIS.nome) <> "NOVAHIS" Then
        BG_Mensagem mediMsgBox, "Tem de indicar  n�mero de epis�dio e tipo de epis�dio."
        Exit Function
    End If
                    
    BL_InicioProcessamento Me, "A pesquisar " & HIS.nome
                    
    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
                    
    If HisAberto = 1 Then
                                
        ' SE FOR GH
        If UCase(HIS.nome) = UCase("GH") And ((CbSituacao.ListIndex <> mediComboValorNull And EcEpisodioAux.text <> "")) Then
            Dim RsPesquisaGH As ADODB.recordset
            Dim SqlGH As String
                
                
                ' Mapeia a situa��o do SISLAB para a GH.
                Select Case CbSituacao.ItemData(CbSituacao.ListIndex)
                    Case gT_Urgencia
                        t_episodio = "Urgencias"
                    Case gT_Consulta
                        t_episodio = "Consultas"
                    Case gT_Internamento
                        t_episodio = "Internamentos"
                    Case gT_Externo
                        t_episodio = "Consultas"
                    Case "Ficha-ID"
                        t_episodio = "Ficha-ID"
                    Case gT_Prescricoes
                        t_episodio = "Prescricoes"
                    Case gT_LAB
                        t_episodio = "Consultas"
                    Case gT_Ambulatorio
                        t_episodio = "Ambulatorio"
                    
                    Case gT_Acidente
                        t_episodio = "Acidente"
                    Case gT_Ambulatorio
                        t_episodio = "Ambulatorio"
                    Case gT_Cons_Inter
                        t_episodio = "Cons-Inter"
                    Case gT_Cons_Telef
                        t_episodio = "Cons-Telef"
                    Case gT_Consumos
                        t_episodio = "Consumos"
                    Case gT_Credenciais
                        t_episodio = "Credenciais"
                    Case gT_Diagnosticos
                        t_episodio = "Diagnosticos"
                    Case gT_Exame
                        t_episodio = "Exame"
                    Case gT_Fisio
                        t_episodio = "Fisio"
                    Case gT_HDI
                        t_episodio = "Hosp-Dia"
                    Case gT_Intervencao
                        t_episodio = "Intervencao"
                    Case gT_MCDT
                        t_episodio = "Mcdt"
                    Case gT_Plano_Oper
                        t_episodio = "Plano-Oper"
                    Case gT_Pre_Intern
                        t_episodio = "Pre-Intern"
                    Case gT_Prog_Cirugico
                        t_episodio = "Prog-Cirurgico"
                    Case gT_Protoc
                        t_episodio = "Protoc"
                    Case gT_Referenciacao
                        t_episodio = "Referenciacao"
                    Case gT_Reg_Oper
                        t_episodio = "Reg-Oper"
                    Case gT_Tratamentos
                        t_episodio = "Tratamentos"
                    Case Else
                        t_episodio = ""
                End Select
                
                If t_episodio <> "" Then
                    CriterioTabela = "select e.t_doente, e.doente, e.t_episodio, e.episodio " & _
                                                    "from  sd_episodio e " & _
                                                    "where e.episodio = " & BL_TrataStringParaBD(EcEpisodioAux.text) & _
                                                    " and  e.t_episodio = " & BL_TrataStringParaBD(t_episodio)
                Else
                    BG_Mensagem mediMsgBox, vbCrLf & "Erro na selec��o do doente do " & HIS.nome & "." & vbCrLf & vbCrLf, vbExclamation, " " & App.ProductName
                    BL_FimProcessamento Me
                    Exit Function
                End If
        
        ElseIf UCase(HIS.nome) = "SONHO" And ((CbSituacao.ListIndex <> mediComboValorNull And EcEpisodioAux.text <> "")) Then
            
            ' Mapeia a situa��o do SISLAB para o SONHO.
            Select Case CbSituacao.ItemData(CbSituacao.ListIndex)
                Case gT_Urgencia
                    t_episodio = "URG"
                Case gT_Consulta
                    t_episodio = "CON"
                Case gT_Internamento
                    t_episodio = "INT"
                Case gT_Externo
                    t_episodio = "HDI"
                Case gT_LAB
                    t_episodio = "LAB"
                Case gT_Bloco
                    t_episodio = "BLO"
                Case gT_RAD
                    t_episodio = "RAD"
                Case Else
                    t_episodio = ""
            End Select
            
            RetPesquisaSONHO = SONHO_PesquisaUtente("", "", EcEpisodioAux.text, t_episodio)
        
            CriterioTabela = "SELECT " & vbCrLf & _
                   "        * " & vbCrLf & _
                   "FROM " & vbCrLf & _
                   "        sl_identif " & vbCrLf & _
                   "WHERE " & vbCrLf & _
                   "        cod_apl = " & gNumeroSessao
        
            If (Len(EcUtente.text) > 0) And gUtilizaNumSeqSONHO = mediSim Then
                CriterioTabela = CriterioTabela & " AND utente = " & EcUtente.text
            Else
                CriterioTabela = CriterioTabela & " AND nome_ute = " & BL_TrataStringParaBD(EcNome.text)
            End If
        ElseIf UCase(HIS.nome) = "NOVAHIS" Then
            If EcPrescricao.text <> "" Then
                CriterioTabela = "SELECT distinct x1.nhc, x1.n_pedido, x2.epis_pk  FROM buzon_lab x1, sislab_epi x2"
                CriterioTabela = CriterioTabela & " WHERE x1.actividad_det_pk = x2.actividad_det_pk"
                CriterioTabela = CriterioTabela & " AND x1.n_pedido = " & BL_TrataStringParaBD(EcPrescricao.text)
            Else
                EcPrescricao.text = ""
                EcEpisodioAux.text = ""
                EcEpisodio.text = ""
                EcEpisodioAux.Visible = False
                ActualizaProcessoUtente = True
                Exit Function
            End If
        End If
                        
        With rs
            .Source = CriterioTabela
            .CursorType = adOpenForwardOnly
            .CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
            .Open , gConnHIS
        End With
        
        If rs.RecordCount <= 0 Then
'            BL_FimProcessamento Me
            BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum utente do(a) " & HIS.nome & " para a situacao " & t_episodio & " e o epis�dio " & EcEpisodioAux.text & " !", vbInformation, "Procurar"
            ActualizaProcessoUtente = False
            Exit Function
        Else
            If UCase(HIS.nome) = "GH" Then
                If Trim(EcUtente.text) = Trim(rs!doente) Then
                    ' O processo coincide.
                    EcEpisodio.text = EcEpisodioAux.text
                    EcEpisodioAux.Visible = False
                    EcEpisodioAux.Enabled = False
                    
                    MsgBox vbCrLf & _
                           "O epis�dio indicado corresponde ao Utente actual.            " & vbCrLf & vbCrLf, vbInformation, " Actualizar Epis�dio"
                    ActualizaProcessoUtente = True
                Else
                    ' O processo n�o coincide.
                    MsgBox vbCrLf & _
                        "O epis�dio indicado nao corresponde ao Utente actual.            " & vbCrLf & "Epis�dio n�o alterado! " & vbCrLf, vbExclamation, " Actualizar Epis�dio"

                    'EcEpisodio.text = ""
                    EcEpisodioAux.text = ""
                    EcEpisodioAux.Visible = False
                    EcEpisodioAux.Enabled = False
                    ActualizaProcessoUtente = True
                End If
            ElseIf UCase(HIS.nome) = "NOVAHIS" Then
                BG_LogFile_Erros "NOVAHIS. Utente:" & Trim(EcUtente.text) & "-" & Trim(BL_HandleNull(rs!nhc, "")) & ".Epi:" & EcEpisodio.text & "-" & BL_HandleNull(rs!epis_pk, "") & "."
                If Trim(EcUtente.text) = Trim(BL_HandleNull(rs!nhc, "")) And EcEpisodio.text = BL_HandleNull(rs!epis_pk, "") Then
                    ' O processo coincide.
                    
                    ActualizaProcessoUtente = True
                ElseIf Trim(EcUtente.text) <> Trim(BL_HandleNull(rs!nhc, "")) Then
                    ' O processo n�o coincide.
                    MsgBox vbCrLf & _
                        "A prescri��o indicada nao corresponde ao Utente actual.", vbExclamation, " Actualizar Epis�dio"

                    ActualizaProcessoUtente = False
                ElseIf EcEpisodio.text <> BL_HandleNull(rs!epis_pk, "") Then
                    ' O processo n�o coincide.
                    MsgBox vbCrLf & _
                        "A prescri��o indicada nao corresponde ao epis�dio actual.", vbExclamation, " Actualizar Epis�dio"
                    ActualizaProcessoUtente = False
                End If
                
            ElseIf UCase(HIS.nome) = "SONHO" Then
                ActualizaProcessoUtente = True
                '  Actualiza
                ' O processo coincide.
                EcEpisodio.text = EcEpisodioAux.text
                EcEpisodioAux.Visible = False
                EcEpisodioAux.Enabled = False
            
                ' Apaga o registo da tabela tempor�ria do SONHO.
                '-----------------------------------------------
                 Dim rsAux As ADODB.recordset
                 Dim sSql As String
                 Set rsAux = New ADODB.recordset
                 
                 sSql = "DELETE FROM sl_identif " & _
                        "WHERE cod_apl = " & gNumeroSessao
                
                 With rsAux
                     .Source = sSql
                     .CursorType = adOpenForwardOnly
                     .CursorLocation = adUseServer
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                     .Open , gConnHIS
                 End With
                 
                 Set rsAux = Nothing
                 '------------------------------------------------
            End If
        End If
        rs.Close
        
    End If
    BL_Fecha_conexao_HIS
    BL_FimProcessamento Me
Exit Function
TrataErro:
    BG_LogFile_Erros "ActualizaProcessoUtente: " & Err.Number & " - " & Err.Description, Me.Name, "ActualizaProcessoUtente"
    Exit Function
    Resume Next
End Function





Private Function DevolveDataValidade(n_req As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    BL_VerificaConclusaoRequisicao n_req, EcDataPrevista.text
    
    '----------------------------------
    ' DATA VALIDADE ACTUAL
    sql = "SELECT dt_conclusao FROM sl_requis WHERE n_req = " & n_req
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        DevolveDataValidade = BL_HandleNull(rs!dt_conclusao, CDate(1))
    End If
    rs.Close
    Set rs = Nothing
    '----------------------------------
Exit Function
TrataErro:
    BG_LogFile_Erros "DevolveDataValidade: " & Err.Number & " - " & Err.Description, Me.Name, "DevolveDataValidade"
    Exit Function
    Resume Next
End Function

' pferreira 2010.03.08
' TICKET GLINTTHS-263.
Public Function RegistaEliminadas(Optional elimina_todas As Boolean) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim total As Integer
    Dim sSql As String
    RegistaEliminadas = False
    
    If (elimina_todas) Then
        total = FGAna.rows - 2
    Else
        total = TotalEliminadas
    End If
    If EcNumReq <> "" Then
        For i = 1 To total
            If (Not elimina_todas) Then
                sSql = " DELETE FROM sl_marcacoes WHERE n_Req = " & EcNumReq & " AND cod_agrup = " & BL_TrataStringParaBD(Eliminadas(i).codAna)
                BG_ExecutaQuery_ADO sSql
                BL_RegistaAnaEliminadas EcNumReq, Eliminadas(i).codAna, "", "", ""
            Else
                sSql = " DELETE FROM sl_marcacoes WHERE n_Req = " & EcNumReq & " AND cod_agrup = " & BL_TrataStringParaBD(FGAna.TextMatrix(i, lColFgAnaCodAna))
                BG_ExecutaQuery_ADO sSql
                BL_RegistaAnaEliminadas EcNumReq, FGAna.TextMatrix(i, lColFgAnaCodAna), "", "", ""
            End If
        Next i
    End If
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    RegistaEliminadas = True
Exit Function
TrataErro:
    RegistaEliminadas = False
    BG_LogFile_Erros "RegistaEliminadas: " & Err.Number & " - " & Err.Description, Me.Name, "RegistaEliminadas"
    Exit Function
    Resume Next
End Function




Function ProdutoUnico(GrupoAna As String) As Boolean
    Dim RsProdU As ADODB.recordset
    Dim sql As String
    Dim ret As Boolean
    
    On Error GoTo ErrorHandler
    
    ret = True
    
    sql = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)
    
    Set RsProdU = New ADODB.recordset
    RsProdU.CursorLocation = adUseServer
    RsProdU.CursorType = adOpenStatic
    RsProdU.Source = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)
    If gModoDebug = mediSim Then BG_LogFile_Erros RsProdU.Source
    RsProdU.Open sql, gConexao
    If Not RsProdU.EOF Then
        If BL_HandleNull(RsProdU!flg_produnico, "") = "1" Then
            ret = True
        Else
            ret = False
        End If
    End If
    
    ProdutoUnico = ret
    
    RsProdU.Close
    Set RsProdU = Nothing
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : FormGestaoRequisicao (ProdutoUnico) -> " & Err.Description)
            ProdutoUnico = False
            Exit Function
    End Select
End Function



Private Sub CbPrioColheita_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    BG_LimpaOpcao CbPrioColheita, KeyCode
    If (IsChild) Then
        EcPrColheita.text = "4"
    Else
        EcPrColheita.text = "2"
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbPrioColheita_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbPrioColheita_KeyDown"
    Exit Sub
    Resume Next
End Sub

' PFerreira 08.03.2007
Private Sub CbPrioColheita_Click()
    On Error GoTo TrataErro
    If (CbPrioColheita.ListIndex >= 0) Then: EcPrColheita.text = CbPrioColheita.ItemData(CbPrioColheita.ListIndex)
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbPrioColheita_Click: " & Err.Number & " - " & Err.Description, Me.Name, "CbPrioColheita_Click"
    Exit Sub
    Resume Next
End Sub

' PFerreira 15.03.2007
Private Function IsChild() As Boolean
    On Error GoTo TrataErro
    If EcDataNasc <> "" Then
        If Trim(Replace(Replace(BG_CalculaIdade(CDate(EcDataNasc)), "anos", ""), "dia", "")) <= 13 Then
            IsChild = True
        Else
            IsChild = False
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "IsChild: " & Err.Number & " - " & Err.Description, Me.Name, "IsChild"
    Exit Function
    Resume Next
End Function


Sub Grava_ObsAnaReq()

    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
    
    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA ANALISE NA TABELA SL_RECIBOS_DET
    ' -----------------------------------------------------------------------------------------------
    If RegistosA.Count > 0 And Trim(RegistosA(1).codAna) <> "" Then
        For i = 1 To RegistosA.Count
            If RegistosA(i).codAna <> "" Then
                BL_GravaObsAnaReq EcNumReq, RegistosA(i).codAna, RegistosA(i).ObsAnaReq, RegistosA(i).seqObsAnaReq, 0, _
                                  EcSeqUtente, mediComboValorNull
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_ObsAnaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_ObsAnaReq"
    Exit Sub
    Resume Next
End Sub


Private Sub EcObsAna_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Or KeyCode = 13 Then
        RegistosA(LastRowA).ObsAnaReq = EcObsAna
        EcObsAna = ""
        FrameObsAna.Visible = False
        FGAna.SetFocus
    End If
End Sub

Private Sub EcObsAna_LostFocus()
    EcObsAna = ""
    FrameObsAna.Visible = False
    'FGAna.SetFocus
End Sub

Sub AcrescentaAnalise(linha As Integer)
    
    Dim i As Integer

    On Error GoTo Trata_Erro
    
    
    'Cria linha vazia
    FGAna.AddItem "", FGAna.row
    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
    ExecutaCodigoA = True
    Fgana_RowColChange
    
    ReDim Preserve MaReq(UBound(MaReq) + 1)
    If UBound(MaReq) = 0 Then ReDim MaReq(1)
    
'    i = UBound(MaReq)
    LastRowA = linha
    LastColA = 0
    i = RegistosA.Count
    
    While i > linha
            
            RegistosA(i).codAna = RegistosA(i - 1).codAna
            RegistosA(i).descrAna = RegistosA(i - 1).descrAna
            RegistosA(i).Estado = RegistosA(i - 1).Estado
            RegistosA(i).ObsAnaReq = RegistosA(i - 1).ObsAnaReq
            RegistosA(i).seqObsAnaReq = RegistosA(i - 1).seqObsAnaReq
            RegistosA(i).NReqARS = RegistosA(i - 1).NReqARS
            RegistosA(i).codUnidSaude = RegistosA(i - 1).codUnidSaude
            RegistosA(i).MedUnidSaude = RegistosA(i - 1).MedUnidSaude
            FGAna.TextMatrix(i, lColFgAnaCred) = RegistosA(i).NReqARS
            FGAna.TextMatrix(i, lColFgAnaUnidSaude) = RegistosA(i).codUnidSaude
            FGAna.TextMatrix(i, lColFgAnaMedSaude) = RegistosA(i).MedUnidSaude
            If RegistosA(i - 1).NReqARS = RegistosA(linha).NReqARS Then
                RegistosA(i).p1 = RegistosA(i - 1).p1 + 1
                If ContP1 < RegistosA(i).p1 Then
                    ContP1 = RegistosA(i).p1
                End If
            Else
                RegistosA(i).p1 = RegistosA(i - 1).p1
            End If
            FGAna.TextMatrix(i, lColFgAnaOrdem) = RegistosA(i).p1
'            RegistosA(i).ReciboPerfilMarcacao = RegistosA(i - 1).ReciboPerfilMarcacao
'            RegistosA(i).ReciboEntidade = RegistosA(i - 1).ReciboEntidade
'            RegistosA(i).ReciboP1 = RegistosA(i - 1).ReciboP1
'            RegistosA(i).ReciboCorP1 = RegistosA(i - 1).ReciboCorP1
'            RegistosA(i).ReciboTaxa = RegistosA(i - 1).ReciboTaxa
'            RegistosA(i).ReciboNumDoc = RegistosA(i - 1).ReciboNumDoc
'            RegistosA(i).ReciboUserCri = RegistosA(i - 1).ReciboUserCri
'            RegistosA(i).ReciboDtCri = RegistosA(i - 1).ReciboDtCri
'            RegistosA(i).ReciboHrCri = RegistosA(i - 1).ReciboHrCri
'            RegistosA(i).ReciboUserEmi = RegistosA(i - 1).ReciboUserEmi
'            RegistosA(i).ReciboDtEmi = RegistosA(i - 1).ReciboDtEmi
'            RegistosA(i).ReciboHrEmi = RegistosA(i - 1).ReciboHrEmi
'            RegistosA(i).ReciboFlgImpresso = RegistosA(i - 1).ReciboFlgImpresso
'            RegistosA(i).ReciboFlgFacturado = RegistosA(i - 1).ReciboFlgFacturado
'            RegistosA(i).ReciboFlgAdicionado = RegistosA(i - 1).ReciboFlgAdicionado
'            RegistosA(i).ReciboFlgRetirado = RegistosA(i - 1).ReciboFlgRetirado
'            RegistosA(i).ReciboIsencao = RegistosA(i - 1).ReciboIsencao
'            RegistosA(i).ReciboOrdemMarcacao = RegistosA(i - 1).ReciboOrdemMarcacao
        i = i - 1
    Wend
    
    RegistosA(linha).codAna = ""
    RegistosA(linha).descrAna = ""
    RegistosA(linha).Estado = "-1"
    RegistosA(linha).ObsAnaReq = ""
    RegistosA(linha).seqObsAnaReq = 0
    RegistosA(linha).NReqARS = RegistosA(linha - 1).NReqARS
    RegistosA(linha).codUnidSaude = RegistosA(linha - 1).codUnidSaude
    RegistosA(linha).MedUnidSaude = RegistosA(linha - 1).MedUnidSaude
    RegistosA(linha).p1 = RegistosA(linha - 1).p1 + 1
    FGAna.TextMatrix(LastRowA, lColFgAnaCred) = RegistosA(linha).NReqARS
    FGAna.TextMatrix(LastRowA, lColFgAnaOrdem) = RegistosA(linha).p1
    FGAna.TextMatrix(LastRowA, lColFgAnaMedSaude) = RegistosA(linha).MedUnidSaude
    FGAna.TextMatrix(LastRowA, lColFgAnaUnidSaude) = RegistosA(linha).codUnidSaude
    
    
    
    ExecutaCodigoA = True
    FGAna.RowSel = linha
    FGAna.ColSel = 0
    Call Fgana_RowColChange
    FGAna.row = linha
    LastRowA = linha
    LastColA = 0
    
    
    Exit Sub

Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & " AcrescentaAnalise: " & Err.Description
    End If
    
    Resume Next


End Sub


Sub EnviaFact_ARS()
    Dim sql As String
    Dim SqlUpd As String
    Dim RsARS As ADODB.recordset
    Dim nr_req_ars As String
    Dim n_acto As Integer
    Dim quantidade As Integer
    Dim cod_u_saude As String
    Dim n_mecan_ext As String
    On Error GoTo TrataErro
    

    
    If ((gEnviaFact_GesReq = 1) And _
        (Len(EcEpisodio.text) > 0) And EcEpisodio.text <> "0" And _
        (CbSituacao.ListIndex <> -1) And (EFR_Verifica_ARS(EcCodEFR.text) = True Or EFR_Verifica_ADSE(EcCodEFR.text))) Then
    
        sql = "select n_req_orig n_req, c.n_req n_p1, p1 ord_p1,cod_perfil,cod_ana_c,cod_ana_s,cod_ana cod_agrup, flg_facturado, ord_ana_perf,ord_ana_compl, ord_ana, ord_req, cod_u_saude, n_mecan_ext " & _
                " from sl_credenciais c, sl_marcacoes m " & _
                " Where c.n_req_orig = m.n_req And c.cod_ana = m.cod_agrup And n_req_orig = " & EcNumReq.text & _
                " Union " & _
                " select n_req_orig n_req, c.n_req n_p1,p1 ord_p1,cod_perfil,cod_ana_c,cod_ana_s,cod_ana cod_agrup, flg_facturado, ord_ana_perf,ord_ana_compl, ord_ana, ord_req, cod_u_saude, n_mecan_ext " & _
                " from sl_credenciais c, sl_realiza r " & _
                " Where c.n_req_orig = r.n_req And c.cod_ana = r.cod_agrup " & _
                " And n_req_orig = " & EcNumReq.text & _
                " order by n_p1, ord_req ,ord_ana, ord_ana_perf,ord_ana_compl "
        Set RsARS = New ADODB.recordset
        RsARS.CursorType = adOpenStatic
        RsARS.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsARS.Open sql, gConexao
        While Not RsARS.EOF
            quantidade = RetornaQtdFact(RsARS!cod_agrup, EcCodEFR.text)
            
            nr_req_ars = ""
            nr_req_ars = BL_HandleNull(RsARS!n_p1, "1")
            nr_req_ars = Mid(nr_req_ars, 1, 20)
            
            n_acto = BL_HandleNull(RsARS!ord_p1, mediComboValorNull)
            
            cod_u_saude = BL_HandleNull(RsARS!cod_u_saude, "")
            n_mecan_ext = BL_HandleNull(RsARS!n_mecan_ext, "")
            If BL_HandleNull(RsARS!Flg_Facturado, "0") = "0" Then
                If IF_Factura_Analise(EcNumReq.text, _
                                            EcCodEFR, _
                                            CbSituacao.ItemData(CbSituacao.ListIndex), _
                                            EcEpisodio.text, _
                                            IIf(EcDataChegada.text <> "", EcDataChegada.text, IIf(CDate(dataAct) > CDate(EcDataPrevista.text), dataAct, EcDataPrevista.text)), _
                                            EcProcHosp1, _
                                            EcCodProveniencia.text, _
                                            RsARS!Cod_Perfil, _
                                            RsARS!cod_ana_c, _
                                            RsARS!cod_ana_s, _
                                            RsARS!cod_agrup, "", CbTipoUtente, EcUtente.text, nr_req_ars, Empty, n_acto, quantidade, EcCodMedico, cod_u_saude, n_mecan_ext) = True Then
                    

                    SqlUpd = "update sl_marcacoes set flg_facturado = 1 where n_req = " & RsARS!n_req & _
                                " and cod_agrup = " & BL_TrataStringParaBD(RsARS!cod_agrup)
                    BG_ExecutaQuery_ADO SqlUpd
                    BG_Trata_BDErro
                    
                End If
            End If
            RsARS.MoveNext
        Wend
    
    End If
    Exit Sub
TrataErro:
    BG_LogFile_Erros "FormGestaoRequisicoes - EnviaFact_ARS: " & Err.Number & " - " & Err.Description
    Resume Next
End Sub
Sub ActualizaFlgAparTrans()
    Dim i As Integer
    Dim RsTrans As ADODB.recordset
    Dim sql As String
    
    For i = 1 To UBound(MaReq)
        Set RsTrans = New ADODB.recordset
        RsTrans.CursorLocation = adUseServer
        RsTrans.CursorType = adOpenStatic
        sql = "SELECT flg_apar_trans,n_folha_trab FROM sl_marcacoes WHERE n_req = " & EcNumReq.text & " and cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & " And cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & " and cod_ana_s= " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsTrans.Open sql, gConexao
        If Not RsTrans.EOF Then
            MaReq(i).Flg_Apar_Transf = BL_HandleNull(RsTrans!flg_apar_trans, 0)
            MaReq(i).N_Folha_Trab = BL_HandleNull(RsTrans!N_Folha_Trab, 0)
        Else
            MaReq(i).Flg_Apar_Transf = 0
            MaReq(i).N_Folha_Trab = 0
        End If
    Next i
    
End Sub

'################################################################
' FNOGUEIRA No HSJ as an�lises da Gen�tica n�o podem ser marcadas na mesma requisi��o que
'as restantes an�lises
'#################################################################
Function VerificaCompatibilidadeAnalises(codAna As String) As Boolean
    On Error GoTo TrataErro
    
    Dim VecAna() As String
    Dim i As Integer
    Dim j As Integer
    Dim TotalRegistos As Integer
    Dim VerificaAnalisesJaMarcadas As Boolean
    Dim FlgAnalisesConjunto As Boolean
    
    'Verificar se a an�lise a inserir pertence � chave
    FlgAnalisesConjunto = False
    If gConjuntoAnalisesRequisicao <> "-1" And gConjuntoAnalisesRequisicao <> "" And gConjuntoAnalisesRequisicao <> "0" Then
        VecAna = Split(gConjuntoAnalisesRequisicao, ";")
        i = 0
        While i <= UBound(VecAna)
            If codAna = VecAna(i) Then
                FlgAnalisesConjunto = True
            End If
            i = i + 1
        Wend
    Else
        VerificaCompatibilidadeAnalises = True
        Exit Function
    End If
    
    'Verificar se as an�lises marcadas pertencem � chave
    TotalRegistos = RegistosA.Count - 1
    i = 1
    j = 0
    While i <= TotalRegistos
        While j <= UBound(VecAna)
            If VecAna(j) = UCase(Trim(RegistosA(i).codAna)) Then
                VerificaAnalisesJaMarcadas = True
            End If
            j = j + 1
        Wend
        i = i + 1
    Wend
    
    'Verifica se a an�lise a marcar � compativel com as an�lises j� marcadas
    If TotalRegistos = 0 Then '1� Linha
        If FlgAnalisesConjunto Then
            VerificaCompatibilidadeAnalises = True
            EcReqGrupoEspecial.text = "1"
        Else
            VerificaCompatibilidadeAnalises = True
            EcReqGrupoEspecial.text = ""
        End If
    Else
        If FlgAnalisesConjunto And VerificaAnalisesJaMarcadas Then
            VerificaCompatibilidadeAnalises = True
            EcReqGrupoEspecial.text = "1"
        ElseIf FlgAnalisesConjunto = False And VerificaAnalisesJaMarcadas Then
            VerificaCompatibilidadeAnalises = False
        ElseIf FlgAnalisesConjunto And VerificaAnalisesJaMarcadas = False Then
            VerificaCompatibilidadeAnalises = False
        ElseIf FlgAnalisesConjunto = False And VerificaAnalisesJaMarcadas = False Then
            VerificaCompatibilidadeAnalises = True
        End If
    End If
    
    Exit Function
    
TrataErro:


End Function


Private Function RegistaAcrescentadas() As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim t_urg As Integer
    Dim t_sit As Integer
    RegistaAcrescentadas = False
    If CbUrgencia.ListIndex = mediComboValorNull Then
        t_urg = mediComboValorNull
    Else
        t_urg = CbUrgencia.ItemData(CbUrgencia.ListIndex)
    End If
    
    If CbSituacao.ListIndex = mediComboValorNull Then
        t_sit = mediComboValorNull
    Else
        t_sit = CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    
    If EcNumReq <> "" Then
        For i = 1 To TotalAcrescentadas
            If BL_RegistaAnaAcrescentadas(EcSeqUtente.text, EcNumReq, "", "", "", Acrescentadas(i).codAna, dataAct, Bg_DaHora_ADO, t_urg, t_sit) = False Then
                GoTo TrataErro
            End If
        Next i
    End If
    
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    RegistaAcrescentadas = True
Exit Function
TrataErro:
    RegistaAcrescentadas = False
    BG_LogFile_Erros "RegistaAcrescentadas: " & Err.Number & " - " & Err.Description, Me.Name, "RegistaAcrescentadas"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' RETORNA A COLUNA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveColunaActual(X As Single)
    Dim coluna As Integer
    For coluna = 0 To FgTubos.Cols - 1
        If FgTubos.ColPos(coluna) <= X And FgTubos.ColPos(coluna) + FgTubos.ColWidth(coluna) >= X Then
            TuboColunaActual = coluna
            Exit Sub
        End If
    Next
    TuboColunaActual = -1
End Sub


' ----------------------------------------------------------------------

' RETORNA A LINHA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveLinhaActual(Y As Single)
    Dim linha As Integer
        For linha = 0 To FgTubos.rows - 1
            If FgTubos.RowPos(linha) <= Y And FgTubos.RowPos(linha) + FgTubos.RowHeight(linha) >= Y Then
                TuboLinhaActual = linha
                Exit Sub
            End If
        Next
        TuboLinhaActual = -1
End Sub



' -----------------------------------------------------------------

' PREENCHE TOP ANALISES

' -----------------------------------------------------------------
Public Sub PreencheTopAna()
    Dim CamposRetorno As New ClassPesqResultados
    Dim cancelou As Boolean
    Dim total As Integer
    Dim Resultados(100) As Variant
    Dim i As Integer
    On Error GoTo TrataErro
    
    CamposRetorno.InicializaResultados 1
    FormMarcAnaTop.Inicializa CamposRetorno
    FormMarcAnaTop.Show vbModal
    CamposRetorno.RetornaResultados Resultados(), cancelou, total
    For i = 1 To total
        EcAuxAna.Enabled = True
        EcAuxAna = Resultados(i)
        EcAuxAna_KeyDown 13, 0
    Next i
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheTopAna: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheTopAna"
    Exit Sub
    Resume Next
End Sub
Private Function EtiqPrintResumo( _
    ByVal Abr As String, NReq As String) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    Dim VecAna() As String

    VecAna = Split(Abr, ",")
    
    If UBound(VecAna()) < 12 Then
        ReDim Preserve VecAna(12)
    End If

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{ABR1}", Mid(VecAna(0), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR2}", Mid(VecAna(1), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR3}", Mid(VecAna(2), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR4}", Mid(VecAna(3), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR5}", Mid(VecAna(4), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR6}", Mid(VecAna(5), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR7}", Mid(VecAna(6), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR8}", Mid(VecAna(7), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR9}", Mid(VecAna(8), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR10}", Mid(VecAna(9), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR11}", Mid(VecAna(10), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR12}", Mid(VecAna(11), 1, 5))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", NReq)
    
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrintResumo = True
    
End Function


' -----------------------------------------------------------------

' PREENCHE ESTADO DA REQUISI��O NO ERESULTS

' -----------------------------------------------------------------
Private Sub PreencheEnvioEResults()
    Dim Estado As Integer
    Dim sSql As String
    Dim rsER As New ADODB.recordset
    
    On Error GoTo TrataErro
    If geResults <> True Then
        Exit Sub
    End If
    EcEresultsData.caption = ""
    EcEresultsEstado.caption = ""
    
    sSql = "SELECT * FROM sl_eresults_envio WHERE n_req = " & EcNumReq.text & " ORDER BY data desc "
    rsER.CursorType = adOpenStatic
    rsER.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsER.Open sSql, gConexao
    If rsER.RecordCount > 0 Then
    
        If BL_HandleNull(rsER!flg_estado, "") = 0 Then
            EcEresultsData.caption = BL_HandleNull(rsER!data, "")
            EcEresultsEstado.caption = "PDF N�o gerado"
        ElseIf BL_HandleNull(rsER!flg_estado, "") = 1 Then
            EcEresultsData.caption = BL_HandleNull(rsER!data, "")
            EcEresultsEstado.caption = "PDF Gerado"
        Else
            EcEresultsData.caption = BL_HandleNull(rsER!data, "")
            EcEresultsEstado.caption = "Erro ao gerar PDF"
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEnvioEResults: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEnvioEResults"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' PREENCHE ESTADO DA ASSINATURA DA REQUISICAO

' -----------------------------------------------------------------
Private Sub PreencheEstadoAssinatura()
    Dim Estado As Integer
    On Error GoTo TrataErro

    Estado = BL_RetornaEstadoAssinatura(EcNumReq)
'    If estado = -1 Then
'        LbeResults.caption = ""
'    ElseIf estado = 0 Then
'        LbeResults.caption = "Requisi��o n�o assinada."
'    ElseIf estado = 1 Then
'        LbeResults.caption = "Requisi��o assinada."
'    ElseIf estado = 2 Then
'        LbeResults.caption = "Requisi��o assinada parcialmente."
'    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstadoAssinatura: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEstadoAssinatura"
    Exit Sub
    Resume Next
End Sub
Sub UPDATE_Ana_gARS()

    Dim sql As String

    gSQLError = 0
    sql = "DELETE FROM sl_credenciais WHERE n_req_orig=" & CLng(EcNumReq.text)
    BG_ExecutaQuery_ADO sql
    
    If gSQLError <> 0 Then
        Exit Sub
    End If
    
    Grava_Ana_ARS

End Sub

Sub Grava_Ana_ARS()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim sql As String

    If FlgARS = True Then
        i = RegistosA.Count
        While i > 0
            If RegistosA(i).NReqARS <> "" And RegistosA(i).codAna <> "" Then
                
                sql = "INSERT INTO sl_credenciais " & _
                      "(n_req_orig, " & _
                       "n_req, " & _
                       "cod_ana, " & _
                       "p1, cod_u_saude,n_mecan_ext ) " & _
                      "VALUES " & _
                      "(" & CLng(EcNumReq.text) & "," & _
                        UCase(BL_TrataStringParaBD(RegistosA(i).NReqARS)) & "," & _
                        UCase(BL_TrataStringParaBD(RegistosA(i).codAna)) & "," & _
                        BL_TrataStringParaBD(RegistosA(i).p1) & "," & _
                        BL_TrataStringParaBD(RegistosA(i).codUnidSaude) & "," & _
                        BL_TrataStringParaBD(RegistosA(i).MedUnidSaude) & ")"
                
                BG_ExecutaQuery_ADO sql
                
                
                If gSQLError <> 0 Then
                    i = 0
                End If
            
            End If
            i = i - 1
        Wend
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_Ana_ARS: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Ana_ARS"
    Exit Sub
    Resume Next

End Sub




Private Sub RegistaAlteracoesRequis(num_requis As String)
    Dim i As Integer
    Dim seq_alteracao As Integer
    Dim ValorControloNovo As String
    On Error GoTo TrataErro
    seq_alteracao = BL_ContaAltRequis(num_requis) + 1
    For i = 0 To NumCampos - 1
        ValorControloNovo = ""
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf CamposEc(i) Is TextBox Or _
           TypeOf CamposEc(i) Is Label Or _
           TypeOf CamposEc(i) Is MaskEdBox Or _
           TypeOf CamposEc(i) Is RichTextBox Then
           
            ValorControloNovo = CamposEc(i)
        'DataCombo
        ElseIf TypeOf CamposEc(i) Is DataCombo Then
            ValorControloNovo = BG_DataComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'ComboBox
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            ValorControloNovo = BG_DaComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'CheckBox
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            If CamposEc(i).value = 0 Then
                ValorControloNovo = "0"
            ElseIf CamposEc(i).value = 1 Then
                ValorControloNovo = "1"
            Else
                ValorControloNovo = ""
            End If
        Else
        End If
        
        If CamposBCK(i) <> ValorControloNovo Then
            BL_RegistaAltRequis num_requis, CamposBD(i), CamposBCK(i), ValorControloNovo, seq_alteracao
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "RegistaAlteracoesRequis: " & Err.Number & " - " & Err.Description, Me.Name, "RegistaAlteracoesRequis"
    Exit Sub
    Resume Next
End Sub


Private Function VerificaRegraSexoMarcar(codAgrup As String, seq_utente As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAux As New ADODB.recordset
    Dim Sexo As String
    
    If Mid(codAgrup, 1, 1) = "S" Then
        sSql = "SELECT flg_sexo FROM sl_ana_s where cod_ana_S = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "C" Then
        sSql = "SELECT flg_sexo FROM sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "P" Then
        sSql = "SELECT flg_sexo FROM sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    End If
    rsAux.CursorType = adOpenStatic
    rsAux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAux.Open sSql, gConexao
    If rsAux.RecordCount > 0 Then
        If BL_HandleNull(rsAux!flg_sexo, "") <> "" Then
            Sexo = rsAux!flg_sexo
            rsAux.Close
            
            sSql = "SELECT sexo_ute FROM sl_identif where seq_utente = " & seq_utente
            rsAux.CursorType = adOpenStatic
            rsAux.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAux.Open sSql, gConexao
            If rsAux.RecordCount > 0 Then
                If BL_HandleNull(rsAux!sexo_ute, "") = Sexo Then
                    rsAux.Close
                    VerificaRegraSexoMarcar = True
                    Exit Function
                Else
                    rsAux.Close
                    VerificaRegraSexoMarcar = False
                    Exit Function
                End If
            Else
                rsAux.Close
                VerificaRegraSexoMarcar = False
                Exit Function
            End If
            
        Else
            rsAux.Close
            VerificaRegraSexoMarcar = True
            Exit Function
        End If
    
    End If
    Set rsAux = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaRegraSexoMarcar: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRegraSexoMarcar"
    VerificaRegraSexoMarcar = False
    Exit Function
    Resume Next
   
End Function

' PFerreira 04.04.2007
Private Sub CbDestino_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbDestino, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbDestino_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbDestino_KeyDown"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodsala_Validate(cancel As Boolean)
    cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodsala_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodsala_Validate"
    Exit Sub
    Resume Next
End Sub


Public Sub EcCodValencia_Validate(cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodValencia.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_valencia, cod_proven_defeito FROM sl_valencia WHERE cod_valencia='" & Trim(EcCodValencia.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodValencia.text = ""
            EcDescrValencia.text = ""
        Else
            EcDescrValencia.text = Tabela!descr_valencia
            If BL_HandleNull(Tabela!cod_proven_defeito, "") <> "" Then
                EcCodProveniencia.text = BL_HandleNull(Tabela!cod_proven_defeito, "")
                EcCodProveniencia_Validate False
            End If
        End If
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrValencia.text = ""
    End If
End Sub

Private Sub BtResumo_Click()
    ImprimeResumo
End Sub
Private Sub BtNotas_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicao.Enabled = False
    FormNotasReq.Show
    If EcNumReq.text = "" Then
        FormNotasReq.EcNumReq.text = "-" & gNumeroSessao
    Else
        FormNotasReq.EcNumReq.text = EcNumReq.text
    End If
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotas_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotas_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotasVRM_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicao.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcNumReq = EcNumReq
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotasVRM_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotasVRM_Click"
    Exit Sub
    Resume Next
End Sub


' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset
    
    If EcNumReq = "" Then Exit Sub
    sSql = "SELECT count(*) total FROM sl_obs_ana_req WHERE n_req = " & EcNumReq

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True
            
        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotas: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheNotas"
    Exit Sub
    Resume Next
End Sub


Private Function VerificaAnaliseJaExisteRealiza(n_req As Long, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String, flg_apaga As Boolean) As Long
    Dim sSql As String
    Dim rsReal As New ADODB.recordset
    Dim RsRes As New ADODB.recordset
    On Error GoTo TrataErro
    
    VerificaAnaliseJaExisteRealiza = 0
    
    sSql = "SELECT seq_realiza FROM sl_realiza WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " UNION SELECT seq_realiza FROM sl_realiza_h WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    rsReal.CursorLocation = adUseServer
    rsReal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then
        BG_LogFile_Erros sSql
    End If
    rsReal.Open sSql, gConexao
    If rsReal.RecordCount > 0 Then
        VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        sSql = "SELECT seq_Realiza FROM sl_res_alfan where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_frase where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_micro where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_alfan_h where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_frase_h where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_micro_h where seq_realiza = " & rsReal!seq_realiza
        RsRes.CursorLocation = adUseServer
        RsRes.CursorType = adOpenStatic
        If gModoDebug = mediSim Then
            BG_LogFile_Erros sSql
        End If
        RsRes.Open sSql, gConexao
        If RsRes.RecordCount > 0 Or cod_ana_s = "S99999" Then
            VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        Else
            If flg_apaga = True Then
                sSql = "DELETE FROM sl_realiza WHERE seq_realiza = " & rsReal!seq_realiza
                BG_ExecutaQuery_ADO sSql
            End If
            VerificaAnaliseJaExisteRealiza = 0
        End If
        RsRes.Close
        Set RsRes = Nothing
    Else
        VerificaAnaliseJaExisteRealiza = 0
    End If
    rsReal.Close
    Set rsReal = Nothing
    
Exit Function
TrataErro:
    VerificaAnaliseJaExisteRealiza = 0
    BG_LogFile_Erros "Erro  ao VerificaAnaliseJaExisteRealiza: " & Err.Description, Me.Name, "VerificaAnaliseJaExisteRealiza", True
    Exit Function
    Resume Next
End Function






' --------------------------------------------------------

' VERIFICA SE ANALISE JA FOI MARCADA NOUTRA REQUUISICAO NO PROPRIO DIA

' --------------------------------------------------------
Private Function VerificaAnaliseJaMarcada(ByVal n_req As String, ByVal codAgrup As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim data As Integer
    Dim Data2 As String
    Dim RsPerf As New ADODB.recordset

    If gLAB = "CHVNG" Then
        VerificaAnaliseJaMarcada = False
        Exit Function
    End If
    data = -1
    sSql = "SELECT cod_analise FROM sl_ana_perfis where cod_perfis = (SELECT cod_perfis FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    sSql = sSql & " AND flg_activo = 0)"
    RsPerf.CursorType = adOpenStatic
    RsPerf.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsPerf.Open sSql, gConexao
    If RsPerf.RecordCount > 0 Then
        While Not RsPerf.EOF
            VerificaAnaliseJaMarcada n_req, BL_HandleNull(RsPerf!cod_analise, "")
            RsPerf.MoveNext
        Wend
    Else
        sSql = "SELECT prazo_Val FROM slv_analises WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup) & " AND prazo_val IS NOT NULL "
        rsAna.CursorType = adOpenStatic
        rsAna.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            data = BL_HandleNull(rsAna!prazo_val, 0)
        End If
        rsAna.Close
        If data = -1 Then
            data = 0
        End If
        Data2 = CStr(CDate(dataAct) - data)
        sSql = "SELECT distinct x2.n_Req FROM  sl_requis x2, sl_marcacoes x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
        sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(BL_HandleNull(EcDataPrevista, dataAct))
        sSql = sSql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req   AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
        If n_req <> "" Then
            sSql = sSql & " AND x2.n_req <> " & n_req
        End If
        sSql = sSql & " UNION SELECT distinct x2.n_Req FROM  sl_requis x2, sl_realiza x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
        sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(BL_HandleNull(EcDataPrevista, dataAct))
        sSql = sSql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
        If n_req <> "" Then
            sSql = sSql & " AND x2.n_req <> " & n_req
        End If
        rsAna.CursorType = adOpenStatic
        rsAna.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            VerificaAnaliseJaMarcada = True
            MsgBox "A an�lise " & codAgrup & " j� foi marcada  para o Utente na requisi��o:" & rsAna!n_req & " !"
        Else
            VerificaAnaliseJaMarcada = False
        End If
        rsAna.Close
    End If
    RsPerf.Close
    Set RsPerf = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaAnaliseJaMarcada: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaAnaliseJaMarcada"
    Exit Function
    Resume Next
End Function



Private Sub BtDomicilio_Click()
    On Error GoTo TrataErro
    If FrameDomicilio.Visible = True Then
        FrameDomicilio.Visible = False
    Else
        FrameDomicilio.Visible = True
        FrameDomicilio.top = 1680
        FrameDomicilio.left = 7080
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtDomicilio_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtDomicilio_Click"
    Exit Sub
    Resume Next
End Sub

Sub PreencheCodigoPostal()
    On Error GoTo TrataErro
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostal & "-" & EcCodPostalAux)
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.text = s1
            EcRuaPostal.text = s2
            EcDescrPostal.text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCodigoPostal: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCodigoPostal"
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------

' VERIFICA QUANTOS MEMBROS TEM A ESTRUTURA DE ANALISES

' --------------------------------------------------------
Private Function RetornaQtdFact(cod_agrup As String, cod_efr As String) As Integer
    Dim i As Integer
    Dim cod_ana_gh As String
    Dim flg_conta_membros As Integer
    On Error GoTo TrataErro
    RetornaQtdFact = 1
    If IF_RetornaMapeamentoAna(cod_agrup, cod_efr, cod_ana_gh, flg_conta_membros) = True Then
        If flg_conta_membros = 1 Then
            RetornaQtdFact = 0
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_agrup = cod_agrup And MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
                    RetornaQtdFact = RetornaQtdFact + 1
                End If
            Next
        End If
    End If
Exit Function
TrataErro:
    RetornaQtdFact = 1
    BG_LogFile_Erros "RetornaQtdFact: " & Err.Number & " - " & Err.Description, Me.Name, "RetornaQtdFact"
    Exit Function
    Resume Next
End Function

Private Sub EcAnaEfr_GotFocus()
        cmdOK.Default = False
End Sub

Private Sub EcAnaEfr_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If Len(EcAnaEfr) >= 13 Then
            EcCredAct = EcAnaEfr
        ElseIf (Len(EcAnaEfr)) >= 1 Then
            FGAna.row = FGAna.rows - 1
            LastRowA = FGAna.row
            MarcacaoCodigoBarrasP1 EcAnaEfr
        End If
        EcAnaEfr = ""
        EcAnaEfr.SetFocus
    End If
End Sub

' ------------------------------------------------------------------------------------------------

' VERIFICA SE ANALISE FOI MARCACDA PELO CODIGO DE BARRAS DO P1

' ------------------------------------------------------------------------------------------------
Private Function MarcacaoCodigoBarrasP1(codAna As String) As String
    On Error GoTo TrataErro
    Dim codRubrEfr As String
    Dim rsAnaFact As New ADODB.recordset
    Dim sSql As String
    Dim temp  As String
    Dim i As Integer
    Dim MultAna As String
    If codAna <> "" Then
        codRubrEfr = IF_ObtemRubrParaEFR(EcCodEFR, codAna)
        If codRubrEfr <> "" Then
            sSql = "SELECT x1.cod_ana FROM sl_ana_facturacao x1, slv_analises x2 WHERE replace(x1.cod_ana_gh,'A','') IN " & codRubrEfr
            sSql = sSql & " AND x1.cod_ana = x2.cod_ana and (x2.flg_invisivel = 0 or x2.flg_invisivel is null)"
            sSql = sSql & " AND x2.cod_ana IN (SELECT cod_ana FROM Sl_ana_locais WHERE cod_local = " & gCodLocal & ")"
            sSql = sSql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND x1.cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAnaFact.CursorLocation = adUseServer
            rsAnaFact.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnaFact.Open sSql, gConexao
            If rsAnaFact.RecordCount = 0 Then
                MarcacaoCodigoBarrasP1 = ""
                rsAnaFact.Close
                BG_Mensagem mediMsgBox, "Mapeamento inexistente!", vbOKOnly + vbInformation, "An�lises"
                Set rsAnaFact = Nothing
                Exit Function
            ElseIf rsAnaFact.RecordCount >= 1 Then
                If rsAnaFact.RecordCount > 1 Then
                    'MsgBox codRubrEfr
                    MultAna = "("
                    While Not rsAnaFact.EOF
                        MultAna = MultAna & BL_TrataStringParaBD(rsAnaFact!cod_ana) & ","
                        rsAnaFact.MoveNext
                    Wend
                    MultAna = Mid(MultAna, 1, Len(MultAna) - 1) & ")"
                    AdicionaAnaliseMulti MultAna, BL_HandleNull(EcCredAct, "1")
                Else
                    MultAna = ""
                rsAnaFact.MoveFirst
                MarcacaoCodigoBarrasP1 = BL_HandleNull(rsAnaFact!cod_ana, "")
                AdicionaAnalise MarcacaoCodigoBarrasP1, BL_HandleNull(EcCredAct, "1")
                End If
            End If
            rsAnaFact.Close
            Set rsAnaFact = Nothing
        
        Else
            BG_Mensagem mediMsgBox, "R�brica inexistente!", vbOKOnly + vbInformation, "An�lises"
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "MarcacaoCodigoBarrasP1: " & Err.Number & " - " & Err.Description, Me.Name, "MarcacaoCodigoBarrasP1"
    MarcacaoCodigoBarrasP1 = False
    Exit Function
    Resume Next
End Function


Private Sub AdicionaAnalise(ByVal analise As String, credencial As String)
    On Error GoTo TrataErro
    Dim cod_Agrup_aux As String
    Dim d As String
    If Verifica_Ana_ja_Existe(LastRowA, analise) = False Then
    If Trim(UCase(analise)) <> Trim(UCase(RegistosA(LastRowA).codAna)) Then
        If Insere_Nova_Analise(LastRowA, analise, credencial) = True Then
            cod_Agrup_aux = analise
            d = SELECT_Descr_Ana(cod_Agrup_aux)
            If LastRowA < FGAna.rows Then
                VerificaAnaliseJaMarcada EcNumReq, cod_Agrup_aux
            End If
            'o estado deixa de estar "Sem Analises"
            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
                EcEstadoReq = gEstadoReqEsperaProduto
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
            End If

            If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                'Cria linha vazia
                FGAna.AddItem ""
                CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
            End If
         
            If IdentificaGarrafa() = True Then
                LbGarrafa.caption = "Introduza a identifica��o da garrafa de hemocultura."
                EcGarrafa = ""
                FrameGarrafa.Enabled = True
                FrameGarrafa.Visible = True
                EcGarrafa.SetFocus
                SSTGestReq.Enabled = False
            End If
        End If
    End If
Else
    Beep
    
    BG_Mensagem mediMsgStatus, "An�lise j� indicada!"
End If
FGAna.Col = lColFgAnaCodAna
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaAnalise: " & Err.Description, Me.Name, "AdicionaAnalise", False
    Exit Sub
    Resume Next
End Sub

Private Sub AdicionaAnaliseMulti(multiAna As String, credencial As String)

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "slv_analises_apenas "
    CWhere = " cod_ana in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") AND (flg_invisivel is null or flg_invisivel = 0) AND (inibe_marcacao is null or inibe_marcacao = 0) AND cod_ana IN " & multiAna
    CampoPesquisa = "descr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            FGAna.row = FGAna.rows - 1
            LastRowA = FGAna.row
            AdicionaAnalise BL_SelCodigo("SLV_ANALISES_APENAS", "COD_ANA", "SEQ_ANA", Resultados(i)), credencial
        Next i
        

    End If
End Sub



Public Sub AnexaDocumento(flg As Integer)
    If flg = 1 Then
        EcFlgAnexo = "1"
        BtAnexos.BackColor = vermelhoClaro
    ElseIf flg = 0 Then
        EcFlgAnexo = "0"
        BtAnexos.BackColor = &H8000000F
    ElseIf flg = mediComboValorNull Then
        EcFlgAnexo = ""
        BtAnexos.BackColor = &H8000000F
    End If
End Sub

Private Function RegistaTubosEliminados() As Boolean
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    RegistaTubosEliminados = False
    For i = 1 To TotalTubosEliminados
        TB_ApagaTuboBD BL_HandleNull(tubosEliminados(i).n_req, EcNumReq), tubosEliminados(i).seq_req_tubo
    Next
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    RegistaTubosEliminados = True
Exit Function
TrataErro:
    RegistaTubosEliminados = False
    BG_LogFile_Erros "Erro  RegistaTubosEliminados: " & Err.Description, Me.Name, "RegistaTubosEliminados", False
    Exit Function
    Resume Next
End Function
Private Sub AcrescentaTubosEliminados(n_req As String, seq_req_tubo As Long)
    On Error GoTo TrataErro
    TotalTubosEliminados = TotalTubosEliminados + 1
    ReDim Preserve tubosEliminados(TotalTubosEliminados)
    tubosEliminados(TotalTubosEliminados).n_req = n_req
    tubosEliminados(TotalTubosEliminados).seq_req_tubo = seq_req_tubo
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AcrescentaTubosEliminados: " & Err.Description, Me.Name, "AcrescentaTubosEliminados", False
    Exit Sub
    Resume Next
End Sub



' -------------------------------------------------------------------------------------------

' VERIFICA SE JA EXISTE REGISTO PARA DETERMINADO PERFIL DE MARCACAO NA ESTRUTURA DE RECIBOS

' -------------------------------------------------------------------------------------------
Private Function VerificaPerfilMarcacaoJaMarcado(CodPerfil As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = CodPerfil And RegistosRM(i).ReciboCodAna = CodPerfil Then
            VerificaPerfilMarcacaoJaMarcado = True
            Exit Function
        End If
    Next
    VerificaPerfilMarcacaoJaMarcado = False
    
Exit Function
TrataErro:
    VerificaPerfilMarcacaoJaMarcado = False
    BG_LogFile_Erros "Erro  VerificaPerfilMarcacaoJaMarcado: " & Err.Description, Me.Name, "VerificaPerfilMarcacaoJaMarcado", False
    Exit Function
    Resume Next
End Function



' --------------------------------------------------------

' ADICIONA ANALISE AO RECIBO MANUAL

' --------------------------------------------------------
Private Function AdicionaReciboManual(ByVal flg_manual As Integer, ByVal codEfr As String, ByVal nRec As String, ByVal sRec As String, ByVal CodFacturavel As String, _
                                 ByVal quantidade As Integer, ByVal CodigoPai As String, ByVal codAna As String, ByVal codBarras As Boolean) As Integer
    On Error GoTo TrataErro
    Dim anaFacturar As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim mens As String
    Dim indice As Long
    Dim flg_FdsValFixo As Integer
    Dim AnaRegra As String
    Dim p1 As String
    Dim corP1 As String
    Dim valorUnitEFR As Double
    Dim iAux As Integer
    Dim iAna As Integer
    If CodFacturavel = "" Then Exit Function
    indice = BL_HandleNull(RegistosRM.Count, 0)
    If indice = 0 Then
        CriaNovaClasse RegistosRM, 1, "REC_MAN"
        indice = 1
    End If
    ' -----------------------------------------
    ' ENTIDADE E NUMERO DE DOCUMENTO
    ' -----------------------------------------
    RegistosRM(indice).ReciboEntidade = codEfr
    RegistosRM(indice).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEfr)
    RegistosRM(indice).ReciboNumDoc = nRec
    RegistosRM(indice).ReciboSerieDoc = sRec
    RegistosRM(indice).ReciboCodAna = BL_HandleNull(codAna, CodFacturavel)
    RegistosRM(indice).ReciboCodFacturavel = BL_HandleNull(UCase(CodFacturavel), "0")
    If VerificaCodFacturavel(RegistosRM(indice).ReciboCodFacturavel) = False Then
        RegistosRM(indice).ReciboCodFacturavel = codAna
    End If
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    RegistosRM(indice).ReciboAnaNaoFact = ""

    
    ' -----------------------------------------
    ' TAXA
    ' -----------------------------------------
    codAnaEfr = ""
    descrAnaEfr = ""
    If RegistosRM(indice).ReciboEntidade <> gCodEFRSemCredencial Then
            taxa = IF_RetornaTaxaAnalise(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                         CbTipoUtente, EcUtente, EcDataPrevista, codAnaEfr, descrAnaEfr, False)
    
            taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                         EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, quantidade, "", False, descrAnaEfr, mediComboValorNull)
            RegistosRM(indice).ReciboValorUnitEFR = valorUnitEFR
            taxa = Replace(taxa, ".", ",")
            If Mid(taxa, 1, 1) = "," Then
                taxa = "0" & taxa
            End If
            
            If CkIsento.value = vbChecked And CDbl(taxa) > 0 Then
                taxa = "0,0"
            End If
            
            Select Case taxa
                Case "-1"
                    mens = " An�lise n�o est� mapeada para o FACTUS!"
                    taxaEfr = 0
                Case "-2"
                    mens = "Impossivel abrir conex�o com o FACTUS!"
                    taxaEfr = 0
                Case "-3"
                    mens = "Impossivel seleccionar portaria activa!"
                    taxaEfr = 0
                Case "-4"
                    mens = "An�lise n�o comparticipada pela entidade em causa!"
                    taxaEfr = 0
                Case "-5"
                    mens = "N�o existe tabela codificada para a entidade em causa!"
                    taxaEfr = 0
                Case Else
                    mens = ""
            End Select
    Else
        taxa = "0,0"
    End If
    taxa = BG_CvDecimalParaCalculo(taxa)
    RegistosRM(indice).ReciboCodAnaEFR = codAnaEfr
    RegistosRM(indice).ReciboDescrAnaEFR = descrAnaEfr
    If mens <> "" And gEfrParticular <> mediComboValorNull Then
        BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
        RegistosRM(indice).ReciboTaxa = "0,0"
    Else
        RegistosRM(indice).ReciboTaxa = taxa
    End If
    taxa = BG_CvDecimalParaCalculo(taxa)
    taxaEfr = BG_CvDecimalParaCalculo(taxaEfr)
    
    RegistosRM(indice).ReciboFDS = "0"
    RegistosRM(indice).ReciboCodigoPai = BL_HandleNull(CodigoPai, "")
    RegistosRM(indice).ReciboQuantidade = quantidade
    RegistosRM(indice).ReciboTaxaUnitario = RegistosRM(indice).ReciboTaxa
    RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxaUnitario * RegistosRM(indice).ReciboQuantidade
    RegistosRM(indice).ReciboValorEFR = RegistosRM(indice).ReciboValorUnitEFR * RegistosRM(indice).ReciboQuantidade
    
    'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
    'SE TIVER VALOR FIXO - COLOCA PRE�O A ZERO
    
    If RegistosRM(indice).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
        RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxa * 2 * RegistosRM(indice).ReciboQuantidade
    ElseIf RegistosRM(indice).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
        RegistosRM(indice).ReciboTaxa = "0"
    End If
    
    RegistosRM(indice).ReciboTaxa = Round(RegistosRM(indice).ReciboTaxa, 2)
    RegistosRM(indice).ReciboFlgReciboManual = flg_manual
    RegistosRM(indice).ReciboFlgMarcacaoRetirada = "0"
    
    
    ' -----------------------------------------
    ' P1
    ' -----------------------------------------
    For iAna = 1 To RegistosA.Count
        If RegistosA(iAna).codAna = RegistosRM(indice).ReciboCodFacturavel Then
            p1 = RegistosA(LastRowA).NReqARS
        End If
    Next iAna
    If p1 = "" Then
        If indice = 1 Then
            p1 = 1
            corP1 = "B"
            RegistosRM(indice).ReciboCodMedico = ""
            RegistosRM(indice).ReciboNrBenef = BL_HandleNull(EcNumBenef, "")
        ElseIf indice > 1 Then
            If flg_manual = 0 Then
                If FGAna.row > 1 Then
                    iAux = -1
                    iAux = indice - 1
                    If iAux > -1 Then
                        p1 = BL_HandleNull(Trim(RegistosRM(iAux).ReciboP1), "-1")
                        If Flg_IncrementaP1 = True Then
                            If IsNumeric(p1) Then
                                p1 = p1 + 1
                                corP1 = "B"
                            End If
                        End If
                    End If
                End If
            Else
                p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "-1")
                corP1 = BL_HandleNull(RegistosRM(indice - 1).ReciboCorP1, "B")
            End If
            If p1 = "" Then
                If indice > 1 Then
                    p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "1")
                End If
            End If
                    
                    
            RegistosRM(indice).ReciboCodMedico = RegistosRM(indice - 1).ReciboCodMedico
            RegistosRM(indice).ReciboNrBenef = RegistosRM(indice - 1).ReciboNrBenef
        End If
    End If
    RegistosRM(indice).ReciboP1 = p1
    RegistosRM(indice).ReciboCorP1 = corP1
    RegistosRM(indice).ReciboFlgAutomatico = 0
    If codBarras = True Then
        RegistosRM(indice).flgCodBarras = 1
        If EcCredAct <> "" Then
            RegistosRM(indice).ReciboP1 = EcCredAct
        End If
    Else
        RegistosRM(indice).flgCodBarras = 0
    End If
    'ORDEM DE MARCACAO
    RegistosRM(indice).ReciboOrdemMarcacao = indice
    ' -----------------------------------------
    ' PERFIL MARCACAO - GUARDADO NA TABELA SL_RECIBOS_DET
    ' -----------------------------------------
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    'soliveira lacto **
    If CkIsento.value = vbChecked Then
        RegistosRM(indice).ReciboIsencao = gTipoIsencaoIsento
    Else
        RegistosRM(indice).ReciboIsencao = gTipoIsencaoNaoIsento
    End If
    'RegistosRM(indice).indiceAna = indiceAna
        
    ' -----------------------------------------
    ' ADICIONA ANALISE � ESTRUTURA DE RECIBOS SE NAO FOR ISENTO
    ' -----------------------------------------
    If RegistosRM(indice).ReciboEntidade <> mediComboValorNull Then
        AdicionaAnaliseAoReciboManual indice, False
    End If
    AdicionaReciboManual = indice
    CriaNovaClasse RegistosRM, indice, "REC_MAN"
Exit Function
TrataErro:
    AdicionaReciboManual = -1
    BG_LogFile_Erros "AdicionaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaReciboManual"
    Exit Function
    Resume Next
End Function


Function VerificaCodFacturavel(CodFacturavel As String) As Boolean
    On Error GoTo TrataErro
    Dim RsVerf As ADODB.recordset
    Dim sql As String
    Set RsVerf = New ADODB.recordset
    RsVerf.CursorLocation = adUseServer
    RsVerf.CursorType = adOpenStatic
    sql = "select * from sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(CodFacturavel)
    sql = sql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
    sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsVerf.Open sql, gConexao
    If RsVerf.RecordCount = 0 Then
        VerificaCodFacturavel = False
    Else
        VerificaCodFacturavel = True
    End If
    RsVerf.Close
    Set RsVerf = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaCodFacturavel: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaCodFacturavel"
    VerificaCodFacturavel = False
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' QUANDO ADICIONA UMA NOVA ANALISE, ADICIONA TAMBEM RESPECTIVA TAXA NA ESTRUTURA DE RECIBOS

' -----------------------------------------------------------------------------------------------

Private Sub AdicionaAnaliseAoReciboManual(indice As Long, MudaEstadoPerdidas As Boolean)
    On Error GoTo TrataErro
    Dim i As Long
    Dim Flg_inseriuEntidade As Boolean
    Dim Flg_EntidadeJaEmitida As Boolean
    Flg_inseriuEntidade = False
    
    ' ------------------------------------------------------------------------------------------
    ' SE TAXA FOR 0 NAO E NECESSARIO ACRESCENTAR
    ' ------------------------------------------------------------------------------------------

    If RegistosRM(indice).ReciboCodFacturavel <> RegistosRM(indice).ReciboCodAna Then
        For i = 1 To indice
            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(indice).ReciboCodFacturavel Then
                If RegistosRM(i).ReciboCodAna <> RegistosRM(indice).ReciboCodAna Then
                    Exit Sub
                End If
            End If
        Next
    End If
    
    If RegistosR.Count = 0 Then
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
    Flg_EntidadeJaEmitida = False
    For i = 1 To RegistosR.Count
        ' ------------------------------------------------------------------------------------------
        ' ENTIDADE JA EXISTE NA ESTRUTURA
        ' ------------------------------------------------------------------------------------------
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboNulo Or (RegistosR(i).Estado = gEstadoReciboPerdido And MudaEstadoPerdidas = True) Then
                RegistosR(i).ValorPagar = RegistosR(i).ValorPagar + Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2)
                RegistosR(i).ValorOriginal = RegistosR(i).ValorOriginal + RegistosRM(indice).ReciboTaxa
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                If RegistosR(i).ValorPagar > 0 Then
                
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                ElseIf RegistosR(i).ValorPagar = 0 Then
                    RegistosR(i).Estado = gEstadoReciboNulo
                End If
                Flg_inseriuEntidade = True
                If RegistosR(i).ValorPagar > 0 Then
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(i).Estado = gEstadoReciboNulo
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
                RegistosR(i).ValorPagar = (RegistosR(i).ValorPagar) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).ValorOriginal = (RegistosR(i).ValorOriginal) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                Flg_inseriuEntidade = True
                Flg_EntidadeJaEmitida = True
            ElseIf RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca Then
                Flg_EntidadeJaEmitida = True
            End If
        End If
    Next
    i = i - 1
    ' ------------------------------------------------------------------------------------------
    ' ENTIDADE NAO EXISTE NA ESTRUTURA
    ' ------------------------------------------------------------------------------------------
    If Flg_inseriuEntidade = False Then
        RegistosR(i).SeqRecibo = RECIBO_RetornaSeqRecibo
        RegistosR(i).NumDoc = 0
        RegistosR(i).SerieDoc = "0"
        RegistosR(i).codEntidade = RegistosRM(indice).ReciboEntidade
        RegistosR(i).DescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).codEmpresa = BL_SelCodigo("SL_EFR", "cod_empresa", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).Caucao = 0
        RegistosR(i).UserEmi = ""
        RegistosR(i).DtEmi = ""
        RegistosR(i).HrEmi = ""
        RegistosR(i).flg_impressao = False
        RegistosR(i).DtPagamento = ""
        RegistosR(i).HrPagamento = ""
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            RegistosR(i).Estado = gEstadoReciboNaoEmitido
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            RegistosR(i).Estado = gEstadoReciboNulo
        ElseIf Flg_EntidadeJaEmitida = True Then
            RegistosR(i).Estado = gEstadoReciboPerdido
        End If
        RegistosR(i).NumAnalises = 1
        RegistosR(i).flg_DocCaixa = 0
        
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AdicionaAnaliseAoReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaAnaliseAoReciboManual"
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' RETORNA O INDICE DA ESTRUTURA DE RECIBOS PARA ANALISE EM CAUSA

' -------------------------------------------------------------------------

Public Function DevIndice(codAna As String) As Long
    Dim i As Integer
    On Error GoTo TrataErro
    DevIndice = -1
    If codAna = "" Then Exit Function
    For i = 1 To RegistosRM.Count - 1
        If RegistosRM(i).ReciboCodFacturavel = codAna And BL_HandleNull(RegistosRM(i).flgCodBarras, 0) = 0 Then
            DevIndice = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevIndice = -1
    BG_LogFile_Erros "Erro  DevIndice: " & Err.Description, Me.Name, "DevIndice", False
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' ELIMINA A TAXA DO RECIBO - APENAS SE O RECIBO AINDA NAO TIVER SIDO EMITIDO

' -----------------------------------------------------------------------------------------------
Private Function EliminaReciboManual(indice As Long, codAna As String, EliminaAnalise As Boolean) As Boolean
    On Error GoTo TrataErro
    Dim i As Long
    Dim sSql As String
    
    EliminaReciboManual = True
    If indice = mediComboValorNull Then
        Exit Function
    End If
    For i = 1 To RegistosR.Count
        If Trim(RegistosRM(indice).ReciboNumDoc) = Trim(RegistosR(i).NumDoc) And Trim(RegistosRM(indice).ReciboSerieDoc) = Trim(RegistosR(i).SerieDoc) And RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboPerdido Or RegistosR(i).Estado = gEstadoReciboNulo Then
                If RegistosR(i).NumAnalises > 0 Then RegistosR(i).NumAnalises = RegistosR(i).NumAnalises - 1
                
                RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosR(i).ValorPagar) - BG_CvDecimalParaCalculo(Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2))
                RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosR(i).ValorOriginal) - BG_CvDecimalParaCalculo(BL_HandleNull(RegistosRM(indice).ReciboTaxa, 0))
                If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises = 0 Then
                    LimpaColeccao RegistosR, i
                    Exit For
                ElseIf RegistosR(i).NumAnalises = 0 Then
                    LimpaColeccao RegistosR, i
                    Exit For
                Else
                    If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises > 0 Then
                        RegistosR(i).Estado = gEstadoReciboNulo
                    End If
                End If

                EliminaReciboManual = True
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = True Then
                EliminaReciboManual = False
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = False Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo para esta an�lise.N�o Pode alterar valor deste campo", vbExclamation, "Altera��o Recibo."
                EliminaReciboManual = False
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "EliminaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "EliminaReciboManual"
    EliminaReciboManual = False
    Exit Function
    Resume Next
End Function



' --------------------------------------------------------

' ALTERA ENTIDADE DE UMA ANALISE NO RECIBO MANUAL

' --------------------------------------------------------
Private Sub AlteraEntidadeReciboManual(linha As Long, codEntidade As String, DescrEFR As String, CodPai As String, linhaAna As Long)
    On Error GoTo TrataErro
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim valorUnitEFR As Double
    Dim mens As String
    Dim codigo As String
    Dim flg_FdsValFixo As Integer
    'codigo = RegistosA(linhaAna).codAna
    
    
    If linha > 0 Then
        If RegistosRM(linha).flgCodBarras = 1 Then
            Exit Sub
        End If
        If EliminaReciboManual(linha, RegistosRM(linha).ReciboCodFacturavel, False) = False Then
            Exit Sub
        End If
    End If
    
    If codEntidade <> "" Then
        If RegistosRM.Count = 0 Or linha = 0 Then
            'linha = RegistosRM.Count
            CriaNovaClasse RegistosRM, linha, "REC_MAN"
        End If
        If RegistosRM(linha).ReciboEntidade = "" And linhaAna > mediComboValorNull Then
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            RegistosRM(linha).ReciboQuantidade = 1
            RegistosRM(linha).ReciboNumDoc = "0"
            RegistosRM(linha).ReciboSerieDoc = "0"
            RegistosRM(linha).ReciboCodFacturavel = RegistosA(linhaAna).codAna
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            RegistosRM(linha).ReciboOrdemMarcacao = 0
            If linha = 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            ElseIf linha > 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            End If
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            If codEntidade = gEfrParticular Then
                RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento
            Else
                RegistosRM(linha).ReciboIsencao = BG_DaComboSel(CbTipoIsencao)
            End If
        Else
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            If RegistosRM(linha).ReciboOrdemMarcacao = 0 Then
                RegistosRM(linha).ReciboOrdemMarcacao = 0
            End If
            
        End If
        ' RECALCULA A TAXA
        codAnaEfr = ""
        If RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento Then
            taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, BL_HandleNull(EcDataChegada, dataAct), codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)
            
            taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                         EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidade, "", False, descrAnaEfr, mediComboValorNull)
            RegistosRM(linha).ReciboValorUnitEFR = valorUnitEFR
            RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
            RegistosRM(linha).ReciboDescrAnaEFR = descrAnaEfr
            
        Else
            taxaEfr = 0
            taxa = 0
        End If
        Select Case taxaEfr
            Case -1
                mens = " An�lise n�o est� mapeada para o FACTUS!"
                taxaEfr = 0
            Case -2
                mens = "Impossivel abrir conex�o com o FACTUS!"
                taxaEfr = 0
            Case -3
                mens = "Impossivel seleccionar portaria activa para a entidade em causa!"
                taxaEfr = 0
            Case -4
                mens = "An�lise n�o comparticipada " & RegistosRM(linha).ReciboCodFacturavel & "!"
                taxaEfr = 0
            Case Else
                mens = ""
        End Select
        
        taxaEfr = BG_CvDecimalParaCalculo(taxaEfr)
        
        RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
        If mens <> "" Then
            BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
             RegistosRM(linha).ReciboTaxa = 0
        Else
            RegistosRM(linha).ReciboTaxa = taxaEfr
        End If
        
        If CDbl(Replace(taxa, ".", ",")) > 0 Then
                RegistosRM(linha).ReciboTaxa = Replace(taxa, ".", ",")
        Else
                RegistosRM(linha).ReciboTaxa = "0,0"
        End If
        
        RegistosRM(linha).ReciboQuantidade = 1
        RegistosRM(linha).ReciboFDS = "0"
        RegistosRM(linha).ReciboCodigoPai = CodPai
        RegistosRM(linha).ReciboTaxaUnitario = RegistosRM(linha).ReciboTaxa
        RegistosRM(linha).ReciboTaxa = Replace(RegistosRM(linha).ReciboTaxaUnitario, ".", ",") * RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboTaxa = Round(RegistosRM(linha).ReciboTaxa, 2)
        
        RegistosRM(linha).ReciboValorEFR = Replace(RegistosRM(linha).ReciboValorUnitEFR, ".", ",") * RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboValorEFR = Round(RegistosRM(linha).ReciboValorEFR, 2)
        RegistosRM(linha).flgCodBarras = 0
        
        'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
        'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
        flg_FdsValFixo = "0"
        If RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
            RegistosRM(linha).ReciboTaxa = RegistosRM(linha).ReciboTaxa * 2
        ElseIf RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
            RegistosRM(linha).ReciboTaxa = "0"
        End If
        
        AdicionaAnaliseAoReciboManual linha, False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraEntidadeReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraEntidadeReciboManual"
    Exit Sub
    Resume Next
End Sub

Public Sub EliminaTuboEstrut()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim flg_temAna As Boolean
    On Error GoTo TrataErro
    
    For i = 1 To gTotalTubos
        If i <= gTotalTubos Then
            flg_temAna = False
            For j = 1 To UBound(MaReq)
                If MaReq(j).indice_tubo = i Then
                    flg_temAna = True
                    Exit For
                End If
            Next j
            'BEDSIDE -> gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado
            If flg_temAna = False And gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado Then
                AcrescentaTubosEliminados EcNumReq.text, gEstruturaTubos(i).seq_req_tubo
                For k = i To gTotalTubos - 1
                    TB_MoveEstrutTubos k + 1, k
                Next
                gTotalTubos = gTotalTubos - 1
                ReDim Preserve gEstruturaTubos(gTotalTubos)
                FgTubos.RemoveItem i
                i = i - 1
            End If
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicaoPrivadoPrivado -> Elimina��o de Tubos: " & Err.Description
    Exit Sub
    Resume Next
End Sub



' --------------------------------------------------------

' UTILIZADORES DOS POSTOS PREENCHE POR DEFEITO A SALA

' --------------------------------------------------------
Private Sub PreencheSalaDefeito()
     On Error GoTo TrataErro
   EcCodSala.Enabled = True
    BtPesquisaSala.Enabled = True
    If gCodSalaAssocUser > 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate True
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheSalaDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheSalaDefeito"
    Exit Sub
    Resume Next
End Sub


Private Sub PesquisaUnidSaude()
     On Error GoTo TrataErro
   
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "desc_u_saude"
    CamposEcran(1) = "desc_u_saude"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_u_saude"
    CamposEcran(2) = "cod_u_saude"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_unid_saude"
    CampoPesquisa = "descr_u_saude"
    CWhere = " (flg_activo = 1 or flg_activo is null) "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY desc_u_saude", " Unidades Sa�de")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodUSaude.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Unidades Sa�de", vbExclamation, "Unidades Sa�de"
        FGAna.SetFocus
    End If
'    ValidarARS EcCodEFR
Exit Sub
TrataErro:
    BG_LogFile_Erros "PesquisaUnidSaude: " & Err.Number & " - " & Err.Description, Me.Name, "PesquisaUnidSaude"
    Exit Sub
    Resume Next
End Sub



Private Sub AlteraUnidadeSaude(cod_u_saude As String, linha As Integer)
    Dim i As Integer
    If BG_Mensagem(mediMsgBox, "Deseja actualizar unidade de sa�de para todas as an�lises?", vbYesNo + vbQuestion, "Unidade de Sa�de") = vbYes Then
        For i = 1 To RegistosA.Count - 1
            If RegistosA(i).codAna <> "" Then
                RegistosA(i).codUnidSaude = cod_u_saude
                RegistosA(i).MedUnidSaude = ""
                FGAna.TextMatrix(i, lColFgAnaUnidSaude) = RegistosA(i).codUnidSaude
                FGAna.TextMatrix(i, lColFgAnaMedSaude) = RegistosA(i).MedUnidSaude
            End If
        Next
    Else
        RegistosA(linha).codUnidSaude = cod_u_saude
        RegistosA(linha).MedUnidSaude = ""
        FGAna.TextMatrix(linha, lColFgAnaUnidSaude) = RegistosA(linha).codUnidSaude
        FGAna.TextMatrix(linha, lColFgAnaMedSaude) = RegistosA(linha).MedUnidSaude
    End If
End Sub

Private Function VerificaAnaMarcada(n_req As Long, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsMarca As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_marcacoes WHERE n_req =" & n_req
    sSql = sSql & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c)
    sSql = sSql & " AND cod_ana_S = " & BL_TrataStringParaBD(cod_ana_s)
    With rsMarca
        .Source = sSql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        .Open sSql, gConexao
    End With
    If rsMarca.RecordCount > 0 Then
        VerificaAnaMarcada = True
    Else
        VerificaAnaMarcada = False
    End If
    rsMarca.Close
    Set rsMarca = Nothing
Exit Function
TrataErro:
    Exit Function
    Resume Next
End Function


' -------------------------------------------------------------------------

' PREENCHE GRELHA DE ANALISES COM INFORMACAO DO PRE�O ASSOCIADO

' -------------------------------------------------------------------------
Private Sub ActualizaLinhasRecibos(linha As Integer)
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    
    If linha > 0 Then
        
        FGAna.TextMatrix(linha, lColFgAnaPreco) = ""
        
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna = RegistosA(linha).codAna Then
                If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                    Exit For
                End If
            End If
        Next
    Else
        For i = 1 To RegistosA.Count - 1
            FGAna.TextMatrix(i, lColFgAnaPreco) = ""
        Next
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                For j = 1 To RegistosA.Count - 1
                    If RegistosRM(i).ReciboCodAna = RegistosA(j).codAna Then
                        FGAna.TextMatrix(j, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                        Exit For
                    End If
                Next
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ActualizarLinhasRecibos: " & Err.Description, Me.Name, "ActualizarLinhasRecibos", False
    Exit Sub
    Resume Next
End Sub

' pferreira 2008.10.09
Sub Grava_Agenda()
    Dim sql As String
    
    If EcTAgenda.text <> "" And EcDataChegada.text = "" Then
        sql = "insert into sl_agenda(n_req,cod_t_agenda,dt_marcacao,flg_activo,cod_local, TIPO_REQ) VALUES (" & _
                EcNumReq.text & "," & BL_HandleNull(EcTAgenda, 0) & "," & BL_TrataDataParaBD(EcDataPrevista.text) & "," & "1" & "," & gCodLocal & ", 'R')"
        BG_ExecutaQuery_ADO sql
    End If
End Sub

' pferreira 2008.10.09
Sub Actualiza_Agenda()
    Dim sql As String
    Dim iReg As Integer
    If EcTAgenda.text <> "" And EcDataChegada.text = "" Then
        sql = "update sl_agenda set dt_marcacao = " & BL_TrataDataParaBD(EcDataPrevista.text) & _
              "where n_req = " & EcNumReq.text & " and tipo_req = 'R' AND  cod_local = " & gCodLocal
        iReg = BG_ExecutaQuery_ADO(sql)
        If iReg = 0 Then
            Grava_Agenda
        End If
    End If
        
End Sub


' -------------------------------------------------------------------------

' AO ADICIONAR ANALISES, ADICIONA Questoes DA ANALISE

' -------------------------------------------------------------------------
Private Sub AdicionaQuestaoAna(cod_ana As String)
    Dim sSql As String
    Dim flg_existe As Boolean
    Dim rsQuest As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT x1.cod_frase, x2.descr_frase FROM sl_ana_questoes x1, sl_dicionario x2 WHERE x1.cod_frase = x2.cod_Frase AND "
    sSql = sSql & " x1.cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsQuest.CursorLocation = adUseServer
    rsQuest.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsQuest.Open sSql, gConexao
    If rsQuest.RecordCount >= 1 Then
        While Not rsQuest.EOF
            flg_existe = False
            For i = 1 To totalQuestao
                If BL_HandleNull(rsQuest!cod_frase, "") = EstrutQuestao(i).cod_questao Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                BG_Mensagem mediMsgBox, BL_HandleNull(rsQuest!descr_frase, ""), vbOKOnly + vbInformation, "Quest�es"
                totalQuestao = totalQuestao + 1
                ReDim Preserve EstrutQuestao(totalQuestao)
                EstrutQuestao(totalQuestao).cod_questao = BL_HandleNull(rsQuest!cod_frase, "")
                EstrutQuestao(totalQuestao).descr_questao = BL_HandleNull(rsQuest!descr_frase, "")
            End If
            rsQuest.MoveNext
        Wend
    End If
    rsQuest.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaQuestaoAna: " & Err.Description, Me.Name, "AdicionaQuestaoAna", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' GRAVA Questoes DA ANALISE

' -------------------------------------------------------------------------
Private Function GravaQuestoesAna(n_req As String) As Boolean
    Dim sSql As String
    Dim i As Integer
    Dim registos As Integer
    On Error GoTo TrataErro
    GravaQuestoesAna = False
    sSql = "DELETE FROM sl_req_questoes WHERE n_req = " & n_req
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalQuestao
        sSql = "INSERT INTO sl_req_questoes (n_req, cod_frase, resposta) VALUES("
        sSql = sSql & n_req & ", " & BL_TrataStringParaBD(EstrutQuestao(i).cod_questao) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutQuestao(i).Resposta) & " )"
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos <= 0 Then
            GoTo TrataErro
        End If
    Next
    GravaQuestoesAna = True
Exit Function
TrataErro:
    GravaQuestoesAna = False
    BG_LogFile_Erros "Erro  GravaQuestoesAna: " & Err.Description, Me.Name, "GravaQuestoesAna", False
    Exit Function
    Resume Next
End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Sub ChangeCheckboxState(ByVal linha As Integer, ByVal coluna As Integer, ByVal EstadoCheck As String)
   'EDGAR.PARADA Glintt-HS-18854 27.03.2018
   If FGAna.TextMatrix(linha, lColFgAnaCred) = "" Or FGAna.TextMatrix(linha, lColFgAnaCred) = "A gerar" Then Exit Sub
    
    With FGAna
        .Col = coluna
        .ColAlignment(lColFgAnaConsenteEnvioPDS) = flexAlignCenterCenter
        .CellFontName = "Wingdings"
        .CellForeColor = vbBlack
        .CellFontSize = 15
        '.Text = EstadoCheck
        .FillStyle = flexFillSingle
    End With
    FGAna.TextMatrix(linha, lColFgAnaConsenteEnvioPDS) = EstadoCheck
End Sub

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Sub CheckAnalisesMesmaCredencial(ByVal EstadoCheck As String)
    Dim credencial As String
    Dim i As Integer
    
    credencial = BL_HandleNull(FGAna.TextMatrix(FGAna.RowSel, lColFgAnaCred), "")
    If credencial = "" Then Exit Sub
    
    For i = 1 To FGAna.rows - 2
        If i <> FGAna.RowSel Then
             If FGAna.TextMatrix(i, lColFgAnaCred) = credencial Then
                Call ChangeCheckboxState(i, lColFgAnaConsenteEnvioPDS, EstadoCheck)
            End If
        End If
    Next

End Sub

'BRUNODSANTOS GLINTT-HS-15750 04.01.2018
Private Sub VerificaAnalisesEviarPDS(ByVal NReq As String)
    Dim i As Integer
      
    For i = 1 To FGAna.rows - 2
        'If (FGAna.TextMatrix(i, lColFgAnaConsenteEnvioPDS) <> "") Then
             Call UpdateAnaAcrescentadasEnvioPDS(NReq, FGAna.TextMatrix(i, lColFgAnaCodAna))
       'End If
    Next
    
End Sub


'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function UpdateAnaAcrescentadasEnvioPDS(ByVal NReq As String, ByVal ArrCodAgrp As String) As Boolean
    Dim sSql As String
    Dim iReg As Long
     
    On Error GoTo TrataErro
    
    sSql = "UPDATE sl_ana_acrescentadas SET flg_enviar_pds = " & ConverteCheckPDS(DevolveEstadoCheckEnvioPDS_FgAna(ArrCodAgrp)) & " WHERE n_req = " & BL_TrataStringParaBD(EcNumReq.text) & " AND cod_agrup = " & BL_TrataStringParaBD(ArrCodAgrp)
    
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        UpdateAnaAcrescentadasEnvioPDS = False
    Else
        UpdateAnaAcrescentadasEnvioPDS = True
    End If
    
    Exit Function
    
TrataErro:
    UpdateAnaAcrescentadasEnvioPDS = False
    BG_LogFile_Erros "Erro  UpdateAnaAcrescentadasEnvioPDS: " & sSql & " " & Err.Description, Me.Name, "UpdateAnaAcrescentadasEnvioPDS"
    Exit Function
    Resume Next
    
End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function ConverteCheckPDS(ByVal estadoCheckPDS As String) As Integer

    If estadoCheckPDS = EstadoCheckboxEnvioPDS_Checked Then
        ConverteCheckPDS = 1
    Else
        ConverteCheckPDS = 0
    End If

End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function DevolveEstadoCheckEnvioPDS_FgAna(ByVal codAna As String) As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    For i = 1 To FGAna.rows - 2
        If FGAna.TextMatrix(i, lColFgAnaCodAna) = codAna Then
            DevolveEstadoCheckEnvioPDS_FgAna = FGAna.TextMatrix(i, lColFgAnaConsenteEnvioPDS)
            Exit For
        End If
    Next
    
    Exit Function
TrataErro:

    DevolveEstadoCheckEnvioPDS_FgAna = ""
End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function DevolveEstadoEnvioPDS(ByVal codAna As String, ByVal NReq As String) As String
    Dim rs As New ADODB.recordset
    Dim sql As String
    DevolveEstadoEnvioPDS = EstadoCheckboxEnvioPDS_Unchecked
    
    sql = "SELECT * FROM sl_ana_acrescentadas WHERE n_req = " & BL_TrataStringParaBD(NReq) & " AND cod_agrup = " & BL_TrataStringParaBD(codAna)
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    
    If rs.RecordCount > 0 Then
        DevolveEstadoEnvioPDS = IIf(BL_HandleNull(rs.Fields("flg_enviar_pds").value, 0) = 1, EstadoCheckboxEnvioPDS_Checked, EstadoCheckboxEnvioPDS_Unchecked)
    End If

    rs.Close
    Set rs = Nothing
End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Sub AtualizaColEnvioPDS()

    If (LastColA = lColFgAnaCred) Then
        If (FGAna.TextMatrix(LastRowA, LastColA) <> "") Then
            Call ChangeCheckboxState(LastRowA, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Unchecked)
        ElseIf (FGAna.TextMatrix(LastRowA, LastColA) = "") Then
            FGAna.TextMatrix(LastRowA, lColFgAnaConsenteEnvioPDS) = ""
        End If
    End If
                                    
End Sub


Sub PreencheFgPedAdiciona()
    Dim i As Integer
    Dim j As Integer
    Dim linhaAtual As Integer
    Dim numAna As Integer
    On Error GoTo TrataErro
    linhaAtual = 1
    For i = 1 To RegistosA.Count - 1
        'If COLH_Existe_Ana_Reutil(RegistosA(i).codAna) = False Then
            FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaCodAna) = RegistosA(i).codAna
            FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaDescrAna) = RegistosA(i).descrAna
            FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaMedico) = EcNomeMedico.text
            FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaPrescricao) = EcPrescricao.text
            FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaDtPedido) = EcDataCriacao.text
            FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaHrPedido) = EcHoraCriacao.text
            FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaProven) = EcDescrProveniencia.text
            FgPedAdiciona.MergeCol(lColFgPedAdicionaMedico) = True
            
            If RegistosA(i).codAna <> "" Then
                linhaAtual = linhaAtual + 1
                FgPedAdiciona.AddItem ""
                numAna = numAna + 1
            End If
        'End If
    Next i
    
    If UBound(anaReutil) > 0 Then
        For j = 1 To UBound(anaReutil)
            If anaReutil(j).n_prescricao <> EcPrescricao Then
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaCodAna) = anaReutil(j).cod_ana
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaDescrAna) = anaReutil(j).descr_ana
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaMedico) = anaReutil(j).nome_med
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaTecnico) = anaReutil(j).nome_tecnico
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaPrescricao) = anaReutil(j).n_prescricao
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaDtPedido) = anaReutil(j).dt_cri
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaHrPedido) = anaReutil(j).hr_cri
                FgPedAdiciona.TextMatrix(linhaAtual, lColFgPedAdicionaProven) = anaReutil(j).descr_proven
                FgPedAdiciona.MergeCol(lColFgPedAdicionaMedico) = True

                linhaAtual = linhaAtual + 1
                FgPedAdiciona.AddItem ""
            End If
        Next j
    End If
        
    LbTotalAnaPed.caption = linhaAtual - 1
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, Me.Name, "PreenchePedidoAdicional", True
    Exit Sub
    Resume Next
End Sub

'NELSONPSILVA CHVNG-7461 29.10.2018
Private Function Req_Existe_Ana_Reutil(ByVal analise As String) As Boolean
    Dim i As Integer
    Req_Existe_Ana_Reutil = False
    For i = 1 To UBound(MaReq)
        If analise = Trim(MaReq(i).cod_agrup) Then
            Req_Existe_Ana_Reutil = True
            Exit Function
        End If
    Next
End Function

Sub AcrescentaAnaReutil()
    Dim j As Integer
    For j = 1 To UBound(anaReutil)
        If Req_Existe_Ana_Reutil(Trim(anaReutil(j).cod_ana)) = False Then
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodAna) = anaReutil(j).cod_ana
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescrAna) = anaReutil(j).descr_ana & " *"
            'FGAna.row = FGAna.rows - 1
            LbTotalAna = LbTotalAna + 1
            CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
            RegistosA(FGAna.rows).Estado = "-1"
            FGAna.AddItem ""
        End If
    Next j
End Sub

Sub RemoveAnaReutil()
 Dim j As Integer
    j = FGAna.rows - 1
    For j = 1 To FGAna.rows - 1
        If COLH_Existe_Ana_Reutil(FGAna.TextMatrix(j, lColFgAnaCodAna)) = True Then
            FGAna.RemoveItem j
            j = j + 1
            LbTotalAna = LbTotalAna - 1
        End If
        
    Next j
End Sub
'NELSONPSILVA CHVNG-7461 29.10.2018
Private Function VerificaColheitaTubos(n_req As String, cod_ana As String) As Boolean
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    Dim sql As String
    Dim rsAnaTubo As New ADODB.recordset
    Dim cod_tubo As String
    Dim i As Integer
    
    On Error GoTo TrataErro
    VerificaColheitaTubos = False
    
    sSql = "SELECT * FROM sl_req_tubo WHERE n_req = " & n_req & " AND dt_eliminacao IS NULL AND dt_colheita IS NOT NULL"
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    
    If rsTubo.RecordCount >= 1 Then
        sql = "SELECT cod_tubo FROM slv_analises WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana)
        rsAnaTubo.CursorLocation = adUseServer
        rsAnaTubo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnaTubo.Open sql, gConexao
        
        If rsAnaTubo.RecordCount >= 1 Then
            cod_tubo = BL_HandleNull(rsAnaTubo!cod_tubo, "")
            For i = 1 To UBound(gEstruturaTubos)
                If cod_tubo = gEstruturaTubos(i).CodTubo Then
                    VerificaColheitaTubos = True
                    Exit Function
                End If
            Next i
        Else
            VerificaColheitaTubos = True
        End If
        rsAnaTubo.Close
        Set rsAnaTubo = Nothing
    Else
        VerificaColheitaTubos = True
    End If
    rsTubo.Close
    Set rsTubo = Nothing
    
Exit Function
TrataErro:
    VerificaColheitaTubos = True
    BG_LogFile_Erros "Erro ao Verificar Colheita de Tubos:" & Err.Description, Me.Name, "VerificaColheitaTubos", True
    Exit Function
    Resume Next
End Function

'tania.oliveira CHUC-12912 12.11.2019
Private Sub EstruturaTubosResumo(requicao As Long)

    Dim sql As String
    Dim RsTubos As ADODB.recordset
    Dim rsAna As ADODB.recordset
    Dim agrupador_estrutura As Long
    Dim Index As Long
    
    Set RsTubos = New ADODB.recordset
    RsTubos.CursorLocation = adUseClient
    RsTubos.CursorType = adOpenStatic
    
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    
    agrupador_estrutura = 1
    
    On Error GoTo TrataErro
    sql = "select x1.seq_req_tubo, x1.cod_tubo, x1.cod_local, x1.n_req, x1.dt_previ, x1.dt_chega, x1.hr_chega, x1.dt_eliminacao, x1.hr_eliminacao, x1.user_eliminacao,"
    sql = sql & " x1.user_chega, x1.local_chega, x1.dt_saida, x1.hr_saida, x1.user_saida, x1.local_saida, x1.dt_colheita, x1.hr_colheita, "
    sql = sql & " x1.mot_novo, x1.id_garrafa,x1.flg_transito, "
    sql = sql & " x2.descr_tubo, x2.cod_etiq, "
    sql = sql & " x3.cod_produto, x3.descr_produto, x1.dt_inicio_Transporte, x1.hr_inicio_transporte, x1.user_inicio_transporte , "
    sql = sql & " x1.dt_fim_Transporte, x1.hr_fim_transporte, x1.user_fim_transporte, x1.user_colheita, x1.estado_tubo, x1.seq_req_tubo_local, x1.flg_nota, x1.etiqueta "
    sql = sql & " FROM sl_req_tubo x1, sl_tubo x2 LEFT OUTER JOIN sl_produto x3 ON x2.cod_prod= x3.cod_produto WHERE n_req =" & requicao & " AND x1.cod_tubo = x2.cod_tubo"
    sql = sql & " ORDER BY cod_tubo"
    
    RsTubos.Open sql, gConexao
    
      While (Not RsTubos.EOF)
        Index = Empty
        
        'NELSONPSILVA CHVNG-7461 29.10.2018 Adicionado a "etiqueta" ? estrutura
        InsereItemEstrutura Index, agrupador_estrutura, BL_HandleNull(RsTubos!n_req, Empty), BL_HandleNull(RsTubos!cod_tubo, Empty), _
                            BL_HandleNull(RsTubos!dt_previ, Empty), BL_HandleNull(RsTubos!dt_chega, Empty), _
                            BL_HandleNull(RsTubos!hr_chega, Empty), BL_HandleNull(RsTubos!user_chega, Empty), _
                            BL_HandleNull(RsTubos!dt_colheita, Empty), BL_HandleNull(RsTubos!hr_colheita, Empty), _
                            BL_HandleNull(RsTubos!dt_eliminacao, Empty), BL_HandleNull(RsTubos!hr_eliminacao, Empty), _
                            BL_HandleNull(RsTubos!user_eliminacao, Empty), BL_HandleNull(RsTubos!local_chega, Empty), _
                            Empty, Empty, BL_HandleNull(RsTubos!descR_tubo, Empty), BL_HandleNull(RsTubos!cod_etiq, Empty), _
                            BL_HandleNull(RsTubos!mot_novo, Empty), Empty, BL_HandleNull(RsTubos!cod_produto, Empty), _
                            BL_HandleNull(RsTubos!descr_produto, Empty), CBool(BL_HandleNull(RsTubos!flg_transito, Empty) = mediSim), 0, _
                            BL_HandleNull(RsTubos!dt_inicio_transporte, Empty), BL_HandleNull(RsTubos!hr_inicio_transporte, Empty), _
                            BL_HandleNull(RsTubos!user_inicio_transporte, Empty), BL_HandleNull(RsTubos!dt_fim_transporte, Empty), _
                            BL_HandleNull(RsTubos!hr_fim_transporte, Empty), BL_HandleNull(RsTubos!user_fim_transporte, Empty), _
                            BL_HandleNull(RsTubos!user_colheita, Empty), Empty, BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), _
                            BL_HandleNull(RsTubos!estado_tubo, mediComboValorNull), BL_HandleNull(RsTubos!seq_req_tubo_local, mediComboValorNull), _
                            BL_HandleNull(RsTubos!flg_nota, 0), BL_HandleNull(RsTubos!etiqueta, Empty)
        
        sql = ConstroiQuery2(requicao, BL_HandleNull(RsTubos!cod_tubo, Empty), BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull))
        rsAna.Open sql, gConexao
        If (rsAna.RecordCount = Empty) Then
            Index = Index + 1
            InsereItemEstrutura Index, agrupador_estrutura, BL_HandleNull(RsTubos!n_req, Empty), BL_HandleNull(RsTubos!cod_tubo, Empty), _
                                BL_HandleNull(RsTubos!dt_previ, Empty), Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, _
                                "N/D", Empty, Empty, Empty, Empty, BL_HandleNull(RsTubos!id_garrafa, Empty), Empty, Empty, Empty, 0, Empty, Empty, _
                                Empty, Empty, Empty, Empty, Empty, Empty, BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), mediComboValorNull, _
                                mediComboValorNull, 0, BL_HandleNull(RsTubos!etiqueta, Empty)
        End If
        While (Not rsAna.EOF)
            
            Index = Index + 1
            InsereItemEstrutura Index, agrupador_estrutura, BL_HandleNull(RsTubos!n_req, Empty), BL_HandleNull(RsTubos!cod_tubo, Empty), _
                                BL_HandleNull(RsTubos!dt_previ, Empty), Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, _
                                BL_HandleNull(rsAna!cod_ana, Empty), BL_HandleNull(rsAna!descr_ana, Empty), _
                                Empty, Empty, Empty, BL_HandleNull(RsTubos!id_garrafa, Empty), Empty, Empty, Empty, BL_HandleNull(rsAna!conta, 0), _
                                Empty, Empty, Empty, Empty, Empty, Empty, Empty, BL_HandleNull(rsAna!cod_local_ana, Empty), _
                                BL_HandleNull(RsTubos!seq_req_tubo, mediComboValorNull), mediComboValorNull, _
                                mediComboValorNull, 0, BL_HandleNull(RsTubos!etiqueta, Empty)
            rsAna.MoveNext
        Wend
        If (rsAna.state = adStateOpen) Then: rsAna.Close
        RsTubos.MoveNext
        agrupador_estrutura = agrupador_estrutura + 1
    Wend
   If (RsTubos.state = adStateOpen) Then: RsTubos.Close
Exit Sub
    
TrataErro:
    sql = ""
    BG_LogFile_Erros "Erro estrutura de tubos resumo: " & Err.Description, Me.Name, "EstruturaTubosResumo", True
    Exit Sub
    Resume Next
End Sub

Private Sub InsereItemEstrutura(Index As Long, agrupador_estrutura As Long, requisicao As Long, codigo_tubo As String, _
                                data_prevista As String, data_chegada As String, hora_chegada As String, _
                                utilizador_chegada As String, data_colheita As String, hora_colheita As String, _
                                data_eliminacao As String, hora_eliminacao As String, utilizador_eliminacao As String, _
                                codigo_local As Integer, codigo_analise As String, descricao_analise As String, _
                                descricao_tubo As String, codigo_etiqueta As String, motivo_eliminacao As String, _
                                numero_garrafa As String, codigo_produto As String, descricao_produto As String, _
                                flag_transito As Boolean, num_obs_ana As Integer, dt_ini_transp As String, hr_ini_transp As String, _
                                user_ini_transp As String, dt_fim_transp As String, hr_fim_transp As String, user_fim_transp As String, _
                                utilizador_colheita As String, cod_local_ana As String, seq_req_tubo As Long, estado_tubo As Integer, _
                                seq_req_tubo_local As Integer, flg_nota As Integer, etiqueta As String)
    Dim i As Integer
    totalTubos = totalTubos + 1
    ReDim Preserve eTubos(totalTubos)
    eTubos(totalTubos).Index = Index
    eTubos(totalTubos).agrupador_estrutura = agrupador_estrutura
    eTubos(totalTubos).requisicao = requisicao
    eTubos(totalTubos).codigo_tubo = codigo_tubo
    eTubos(totalTubos).data_prevista = data_prevista
    eTubos(totalTubos).data_chegada = data_chegada
    eTubos(totalTubos).hora_chegada = hora_chegada
    eTubos(totalTubos).utilizador_chegada = utilizador_chegada
    eTubos(totalTubos).codigo_local = codigo_local
    eTubos(totalTubos).codigo_analise = codigo_analise
    eTubos(totalTubos).data_colheita = data_colheita
    eTubos(totalTubos).hora_colheita = hora_colheita
    eTubos(totalTubos).utilizador_colheita = utilizador_colheita
    eTubos(totalTubos).data_eliminacao = data_eliminacao
    eTubos(totalTubos).hora_eliminacao = hora_eliminacao
    eTubos(totalTubos).utilizador_eliminacao = utilizador_eliminacao
    eTubos(totalTubos).descricao_tubo = descricao_tubo
    eTubos(totalTubos).codigo_etiqueta = codigo_etiqueta
    eTubos(totalTubos).descricao_analise = descricao_analise
    eTubos(totalTubos).motivo_eliminacao = motivo_eliminacao
    eTubos(totalTubos).numero_garrafa = numero_garrafa
    eTubos(totalTubos).codigo_produto = codigo_produto
    eTubos(totalTubos).descricao_produto = descricao_produto
    eTubos(totalTubos).flag_transito = flag_transito
    eTubos(totalTubos).num_obs_ana = num_obs_ana
    eTubos(totalTubos).dt_inicio_transp = dt_ini_transp
    eTubos(totalTubos).hr_inicio_transp = hr_ini_transp
    eTubos(totalTubos).user_inicio_transp = user_ini_transp
    eTubos(totalTubos).dt_fim_transp = dt_fim_transp
    eTubos(totalTubos).hr_fim_transp = hr_fim_transp
    eTubos(totalTubos).user_fim_transp = user_fim_transp
    eTubos(totalTubos).cod_local_ana = cod_local_ana
    eTubos(totalTubos).seq_req_tubo = seq_req_tubo
    eTubos(totalTubos).indiceEstrutTubos = mediComboValorNull
    eTubos(totalTubos).estado_tubo = estado_tubo
    eTubos(totalTubos).seq_req_tubo_local = seq_req_tubo_local
    eTubos(totalTubos).flg_nota = flg_nota
    'NELSONPSILVA CHVNG-7461 29.10.2018
    eTubos(totalTubos).etiqueta = etiqueta
    '
End Sub

Private Function ConstroiQuery2(requisicao As Long, codigo_tubo As String, seq_req_tubo As Long) As String

    Dim ssql As String
    Dim Item As Variant
    Dim rsAna As ADODB.recordset
    Dim devolve_tubo As Boolean
    
    On Error GoTo TrataErro
    Set rsAna = New ADODB.recordset
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    ConstroiQuery2 = "select distinct slv_analises.cod_ana, descr_ana, cod_tubo_sec,"
    ConstroiQuery2 = ConstroiQuery2 & " (Select count(*) from sl_obs_ana_req where slv_analises.cod_ana = sl_obs_ana_req.cod_agrup and sl_obs_ana_req.n_req = " & requisicao & ") conta, sl_ana_acrescentadas.cod_local cod_local_ana"
    ConstroiQuery2 = ConstroiQuery2 & " from slv_analises, sl_ana_acrescentadas where slv_analises.cod_ana in ("
    ssql = "select cod_agrup from sl_marcacoes where n_req = " & requisicao & " AND seq_req_tubo = " & seq_req_tubo
    ssql = ssql & " union select cod_agrup from sl_realiza where n_req = " & requisicao & " AND seq_req_tubo = " & seq_req_tubo
    rsAna.Open ssql, gConexao
    If (rsAna.RecordCount = Empty) Then:  ConstroiQuery2 = ConstroiQuery2 & "null)": Exit Function
    While (Not rsAna.EOF)
        ConstroiQuery2 = ConstroiQuery2 & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_agrup, Empty)) & ","
        devolve_tubo = True
        rsAna.MoveNext
    Wend
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    If (Not devolve_tubo) Then
        ConstroiQuery2 = ConstroiQuery2 & "null)"
    Else
        ConstroiQuery2 = Mid(ConstroiQuery2, 1, Len(ConstroiQuery2) - 1) & ")"
    End If
    ConstroiQuery2 = ConstroiQuery2 & " AND slv_analises.cod_ana = sl_ana_acrescentadas.cod_agrup and sl_ana_acrescentadas.n_req =" & requisicao
    ConstroiQuery2 = ConstroiQuery2 & " AND (flg_eliminada IS NULL or flg_eliminada = 0 )"
    
    Exit Function
    
TrataErro:
    ConstroiQuery2 = ""
    BG_LogFile_Erros "Erro  ConstroiQuery2 : " & Err.Description, Me.Name, "ConstroiQuery2", True
    Exit Function
    Resume Next
    
End Function

Private Sub InicializaEstruturaTubos()
    totalTubos = 0
    ReDim eTubos(totalTubos)
End Sub




VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormVigilanciaEpidimeologica 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormVigilanciaEpidimeologica"
   ClientHeight    =   10065
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   12450
   Icon            =   "FormVigilanciaEpidimeologica.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10065
   ScaleWidth      =   12450
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PicVeYellowSVN 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1200
      Picture         =   "FormVigilanciaEpidimeologica.frx":000C
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   30
      Top             =   9000
      Width           =   255
   End
   Begin VB.PictureBox PicVeRedSNV 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1200
      Picture         =   "FormVigilanciaEpidimeologica.frx":03C7
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   29
      Top             =   8760
      Width           =   255
   End
   Begin VB.PictureBox PicVeGreenSNV 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1200
      Picture         =   "FormVigilanciaEpidimeologica.frx":077F
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   28
      Top             =   8520
      Width           =   255
   End
   Begin VB.CommandButton BtGerarExcel 
      Height          =   615
      Left            =   10680
      Picture         =   "FormVigilanciaEpidimeologica.frx":0B35
      Style           =   1  'Graphical
      TabIndex        =   27
      ToolTipText     =   "Gerar excel"
      Top             =   7200
      Width           =   615
   End
   Begin VB.CommandButton BtEncolherProfiles 
      Height          =   300
      Left            =   480
      Picture         =   "FormVigilanciaEpidimeologica.frx":17FF
      Style           =   1  'Graphical
      TabIndex        =   26
      ToolTipText     =   "Colapsar"
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton BtExpandirProfiles 
      Height          =   300
      Left            =   120
      Picture         =   "FormVigilanciaEpidimeologica.frx":1D89
      Style           =   1  'Graphical
      TabIndex        =   25
      ToolTipText     =   "Expandir Árvore"
      Top             =   1800
      Width           =   375
   End
   Begin VB.TextBox EcSeqRealiza 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   24
      Top             =   9600
      Width           =   615
   End
   Begin VB.CommandButton BtIgnorarVE 
      Height          =   615
      Left            =   9960
      Picture         =   "FormVigilanciaEpidimeologica.frx":2313
      Style           =   1  'Graphical
      TabIndex        =   23
      ToolTipText     =   "Ignorar alerta"
      Top             =   7200
      Width           =   615
   End
   Begin MSComctlLib.ImageList ImageList 
      Left            =   7560
      Top             =   7320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   -2147483643
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":2FDD
            Key             =   "amarelo"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":3577
            Key             =   "snv_vermelho"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":38C9
            Key             =   "snv_amarelo_pendente"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":3C1B
            Key             =   "snv_verde"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":3F6D
            Key             =   "snv_amarelo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":42BF
            Key             =   "grupos"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":4659
            Key             =   "email"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":49F3
            Key             =   "excel"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":4D8D
            Key             =   "rejeitado"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormVigilanciaEpidimeologica.frx":B5EF
            Key             =   "espera"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView Tv 
      Height          =   4935
      Left            =   120
      TabIndex        =   22
      Top             =   2160
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   8705
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.CommandButton BtEnviarEmail 
      Height          =   615
      Left            =   11400
      Picture         =   "FormVigilanciaEpidimeologica.frx":11E51
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Enviar Dados"
      Top             =   7200
      Width           =   615
   End
   Begin VB.PictureBox PicMail 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   840
      Picture         =   "FormVigilanciaEpidimeologica.frx":1275D
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   20
      Top             =   8520
      Width           =   255
   End
   Begin VB.PictureBox PicExcel 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   480
      Picture         =   "FormVigilanciaEpidimeologica.frx":12AE7
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   19
      Top             =   8520
      Width           =   255
   End
   Begin VB.PictureBox PicVeGreen 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      Picture         =   "FormVigilanciaEpidimeologica.frx":12E71
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   18
      Top             =   8520
      Width           =   255
   End
   Begin VB.PictureBox PicVeRed 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      Picture         =   "FormVigilanciaEpidimeologica.frx":196C3
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   17
      Top             =   8760
      Width           =   255
   End
   Begin VB.PictureBox PicVeYellow 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      Picture         =   "FormVigilanciaEpidimeologica.frx":1FF15
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   16
      Top             =   9000
      Width           =   255
   End
   Begin VB.ComboBox CbEstado 
      Height          =   315
      Left            =   7440
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   240
      Width           =   1815
   End
   Begin VB.ListBox EcListaAnalises 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   7440
      TabIndex        =   10
      Top             =   1200
      Width           =   4575
   End
   Begin VB.CommandButton BtPesquisaRapidaAna 
      Height          =   315
      Left            =   12000
      Picture         =   "FormVigilanciaEpidimeologica.frx":2049F
      Style           =   1  'Graphical
      TabIndex        =   13
      ToolTipText     =   "  Pesquisa Rápida de  Análises Simples "
      Top             =   1200
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaRapidaMicrorg 
      Height          =   315
      Left            =   5880
      Picture         =   "FormVigilanciaEpidimeologica.frx":20829
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "  Pesquisa Rápida de  Análises Simples "
      Top             =   1200
      Width           =   375
   End
   Begin VB.TextBox EcDescrEpid 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   720
      Width           =   4095
   End
   Begin VB.ListBox EcListaMicrorg 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1200
      MultiSelect     =   2  'Extended
      TabIndex        =   8
      Top             =   1200
      Width           =   4665
   End
   Begin VB.CommandButton BtPesqRapGrEpid 
      Height          =   315
      Left            =   5880
      Picture         =   "FormVigilanciaEpidimeologica.frx":20BB3
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "  Pesquisa Rápida de  Análises Simples "
      Top             =   720
      Width           =   375
   End
   Begin VB.TextBox EcCodGrEpid 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1200
      TabIndex        =   4
      Top             =   720
      Width           =   615
   End
   Begin MSComCtl2.DTPicker EcDtInicio 
      Height          =   255
      Left            =   1200
      TabIndex        =   0
      Top             =   240
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   450
      _Version        =   393216
      Format          =   163053569
      CurrentDate     =   39588
   End
   Begin MSComCtl2.DTPicker EcDtFim 
      Height          =   255
      Left            =   3120
      TabIndex        =   1
      Top             =   240
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   450
      _Version        =   393216
      Format          =   163053569
      CurrentDate     =   39588
   End
   Begin VB.Label Label1 
      Caption         =   "Estado"
      Height          =   255
      Index           =   1
      Left            =   6600
      TabIndex        =   14
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "An�lises"
      Height          =   255
      Index           =   6
      Left            =   6720
      TabIndex        =   11
      Top             =   1200
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "Microrganismo"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Grupo Epid."
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Da Data "
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "a"
      Height          =   255
      Left            =   2760
      TabIndex        =   2
      Top             =   240
      Width           =   255
   End
End
Attribute VB_Name = "FormVigilanciaEpidimeologica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

' Actualização : 24/04/2002
' Técnico Paulo Costa

' Variáveis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando está a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As adodb.recordset

Dim EstrutGrEpid() As GrEpid
Dim totalGrEpid As Integer

Private Sub BtEncolherProfiles_Click()
BL_expandeNo Tv.SelectedItem, False
End Sub

Private Sub BtEnviarEmail_Click()
    Dim iGrEpid As Integer
    Dim iDeta As Integer
    Dim iRegra As Integer
    Dim iDet As Integer
    Dim seq_ve_bin As String
    Dim flgEncontrou As Boolean
    flgEncontrou = False
    If Tv.Nodes.Count > 0 Then
        If Tv.SelectedItem.Index > mediComboValorNull Then
            For iGrEpid = 1 To totalGrEpid
                If EstrutGrEpid(iGrEpid).linha = Tv.SelectedItem.Index Then
                    flgEncontrou = True
                    Exit For
                End If
                For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                    If EstrutGrEpid(iGrEpid).regras(iRegra).linha = Tv.SelectedItem.Index Then
                        flgEncontrou = True
                        Exit For
                    End If
                    For iDet = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
                        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).linha = Tv.SelectedItem.Index Then
                            flgEncontrou = True
                            Exit For
                        End If
                    Next iDet
                    If flgEncontrou = True Then
                        Exit For
                    End If
                Next iRegra
                If flgEncontrou = True Then
                    Exit For
                End If
            Next iGrEpid
        End If
    End If
    If iGrEpid >= 1 And flgEncontrou = True Then
    
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    'For iGrEpid = iGrEpid To totalGrEpid
    '                For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
    '                   For iDeta = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
    '
    
        'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
                Select Case EstrutGrEpid(iGrEpid).tipo_notificacao
                Case tipo_notificacao.email
                   
                    gMsgTitulo = "Enviar Email "
                    gMsgMsg = "Pretende enviar email para o grupo epidemiol�gico: " & EstrutGrEpid(iGrEpid).descr_gr_epid & "?"
                    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                    If gMsgResp = vbYes Then
                        gMsgTitulo = "Enviar Email "
                        gMsgMsg = "Esta ação vai gerar um excel para cada regra detetada e pode demorar algum tempo. Pretende continuar?"
                        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                        If gMsgResp = vbYes Then
                            FormVigilanciaEpidimeologica.Enabled = False
                            Set gFormActivo = FormVigEpidEmail
                        
                            FormVigEpidEmail.EcPara.text = EstrutGrEpid(iGrEpid).email_to
                            FormVigEpidEmail.EcCC.text = EstrutGrEpid(iGrEpid).email_cc
                            FormVigEpidEmail.EcAssunto.text = "Envio de Resultados: " & EstrutGrEpid(iGrEpid).descr_gr_epid & " " & EcDtInicio.value & " a " & EcDtFim.value
                            If EstrutGrEpid(iGrEpid).flg_html = mediSim Then
                                FormVigEpidEmail.EcMensagem.text = "(HTML)"
                                FormVigEpidEmail.EcMensagem.locked = True
                                FormVigEpidEmail.EcHTML.text = EstrutGrEpid(iGrEpid).corpo_html
                                FormVigEpidEmail.EcDescrImagem.text = EstrutGrEpid(iGrEpid).descr_imagem_anexo
                                If EstrutGrEpid(iGrEpid).descr_imagem_anexo <> "" Then
                                    If Dir(gDirCliente & "\" & EstrutGrEpid(iGrEpid).descr_imagem_anexo) <> "" Then
                                        Kill gDirCliente & "\" & EstrutGrEpid(iGrEpid).descr_imagem_anexo
                                    End If
                                    IM_RetiraImagemBDParaFicheiro gDirCliente & "\" & EstrutGrEpid(iGrEpid).descr_imagem_anexo, "SELECT imagem_anexo FROM sl_cod_gr_epid  WHERE cod_gr_epid = " & EstrutGrEpid(iGrEpid).cod_gr_epid
                                End If
                            Else
                                FormVigEpidEmail.EcMensagem.text = EstrutGrEpid(iGrEpid).email_body
                                FormVigEpidEmail.EcMensagem.locked = False
                            End If
                            
                            For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                                seq_ve_bin = GeraExcel(iGrEpid, iRegra, True)
                                FormVigEpidEmail.EcListaAnexos.AddItem seq_ve_bin
                            Next iRegra
                            FormVigEpidEmail.SetiGrEpid iGrEpid
                            FormVigEpidEmail.Show
                        End If
                    End If
                   
                Case tipo_notificacao.ws_sinave
                   
                    gMsgTitulo = "Enviar Dados SINAVE "
                    gMsgMsg = "Pretende enviar dados para o grupo epidemiológico: " & EstrutGrEpid(iGrEpid).descr_gr_epid & "?"
                    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                    
                    If gMsgResp = vbYes Then
                    
                        For iGrEpid = iGrEpid To totalGrEpid
                             For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                                For iDeta = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
                                    'INVOCA WS SINAVE SE ESTADO PENDENTE
                                    'If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado = gEstadoVePendente And EstrutGrEpid(iGrEpid).tipo_notificacao = tipo_notificacao.ws_sinave Then
                                    If verificaSeqRealiza(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza) Then
                                        SNV_EnviaNotificacaoLaboratorial EstrutGrEpid, iGrEpid, iRegra, iDeta
                                    End If
                                    'End If
                                Next iDeta
                             Next iRegra
                        Next iGrEpid
                    End If
                End Select
            'Next iDeta
        'Next iRegra
    'Next iGrEpid
        '
        'CHSJ-4430
        FuncaoProcurar
        '
    End If
End Sub

Private Sub BtExpandirProfiles_Click()
    BL_expandeNo Tv.SelectedItem, True
End Sub

Private Sub BtGerarExcel_Click()
    Dim iGrEpid As Integer
    Dim iDeta As Integer
    Dim iRegra As Integer
    Dim flgEncontrou As Boolean
    Dim iDet As Integer
    flgEncontrou = False
    If Tv.Nodes.Count > 0 Then
        If Tv.SelectedItem.Index > mediComboValorNull Then
            
            For iGrEpid = 1 To totalGrEpid
                iRegra = 0
                If EstrutGrEpid(iGrEpid).linha = Tv.SelectedItem.Index Then
                    flgEncontrou = True
                    Exit For
                End If
                For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                    iDet = 0
                    If EstrutGrEpid(iGrEpid).regras(iRegra).linha = Tv.SelectedItem.Index Then
                        flgEncontrou = True
                        Exit For
                    End If
                    For iDet = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
                        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).linha = Tv.SelectedItem.Index Then
                            flgEncontrou = True
                            Exit For
                        End If
                    Next iDet
                    If flgEncontrou = True Then
                        Exit For
                    End If
                Next iRegra
                If flgEncontrou = True Then
                    Exit For
                End If
            Next iGrEpid
        End If
    End If
    If flgEncontrou = True And iRegra > 0 Then
        GeraExcel iGrEpid, iRegra, True
    End If
End Sub

Private Function GeraExcel(iGrEpid As Integer, iRegra As Integer, flg_Apagar As Boolean) As String
    Dim iDeta As Integer
    Dim seq_ve_bin As String
    Dim NomeFicheiro As String
    Dim flg_abrirFicheiro As Integer
    Dim xlApp As Object
    Dim xlWB As Object
    On Error GoTo TrataErro
    
    'BRUNODSANTOS CHVNG-7389 - 02.06.2016
    If gUsaVersaoOffice = -1 Then
    
        Dim ExcelVersionReg As String
        Dim ExcelVersion As String
        Dim strArr() As String
        Dim OfficeName As String
        Dim extensaoExcel As String
        
        ExcelVersionReg = BL_RegGetExcelVersion(HKEY_CLASSES_ROOT, "Excel.Application\CurVer", "")
        strArr = Split(ExcelVersionReg, ".")
        ExcelVersion = strArr(2)
        
        If ExcelVersion = "" Then
            BG_LogFile_Erros "Versão do Office não foi encontrada no sistema nem se encontra parametrizada", "GeraExcel", "GeraExcel"
            'BG_Mensagem mediMsgBox, "Versão do Office não foi encontrada no sistema", vbExclamation, "Excel"
        Else
            OfficeName = BL_GetOfficeName(ExcelVersion)
            BG_LogFile_Erros "Versão Excel adquirida automaticamente do sistema -> " & OfficeName, "GeraExcel", "GeraExcel"
        End If
        
    Else
        ExcelVersion = gUsaVersaoOffice
        OfficeName = BL_GetOfficeName(ExcelVersion)
        BG_LogFile_Erros "Versão Excel definida manualmente -> " & OfficeName, "GeraExcel", "GeraExcel"
    End If
    
    If CInt(ExcelVersion) >= 12 Then
        extensaoExcel = ".xlsx"
    Else
        extensaoExcel = ".xls"
    End If
    
    '
    
    flg_abrirFicheiro = 0
    NomeFicheiro = gLAB & "_" & EstrutGrEpid(iGrEpid).descr_gr_epid & "_" & EstrutGrEpid(iGrEpid).regras(iRegra).descr_excel & "_" & Format$(Now, "YYYY_MM_DD_HH_24NN_SS") & extensaoExcel
    NomeFicheiro = Replace(NomeFicheiro, """", "")
    NomeFicheiro = Replace(NomeFicheiro, "/", "")
    NomeFicheiro = Replace(NomeFicheiro, "\", "")
    
    seq_ve_bin = VE_CriaFicheiroExcel(gDirCliente, NomeFicheiro, _
        EstrutGrEpid, iGrEpid, iRegra, ExcelVersion)
    For iDeta = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado = gEstadoVePendente Then
            If Tv.Nodes.Item(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).linha).Image <> "excel" Then
                Tv.Nodes.Item(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).linha).Image = "excel"
            End If
            DoEvents
            EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_ve_bin = seq_ve_bin
            VE_atualizaVigEpidBin EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_vig_epid, _
                                  EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_ve_email, _
                                  EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_ve_bin, _
                                  EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado
        ElseIf EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado = gEstadoVeEnviado Then
            If flg_abrirFicheiro = 0 Then
                gMsgTitulo = "Excel "
                gMsgMsg = "A regra " & EstrutGrEpid(iGrEpid).regras(iRegra).descr_regra & " já tem registos enviados por email. " & vbCrLf
                gMsgMsg = gMsgMsg & " Pretende gerar um excel com todas os detalhes apenas para consulta? "
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp = vbYes Then
                    flg_abrirFicheiro = 1
                Else
                    flg_abrirFicheiro = 2
                End If
            End If
        End If
    Next iDeta
    If flg_Apagar = True And flg_abrirFicheiro <> 1 Then
        Kill gDirCliente & "\" & NomeFicheiro
    ElseIf flg_abrirFicheiro = 1 Then
        Set xlApp = CreateObject("Excel.Application")
        xlApp.Visible = True
        Set xlWB = xlApp.Workbooks.Open(gDirCliente & "\" & NomeFicheiro)
    
    End If
    GeraExcel = seq_ve_bin
Exit Function
TrataErro:
    seq_ve_bin = ""
    Exit Function
    Resume Next
End Function
Private Sub BtIgnorarVE_Click()
    Dim iGrEpid As Integer
    Dim iDeta As Integer
    Dim iRegra As Integer
    If Tv.Nodes.Count > 0 Then
        If Tv.SelectedItem.Index > mediComboValorNull Then
            If UCase(Mid(Tv.SelectedItem.key, 1, 5)) <> UCase("KeyVE") Then
                BG_Mensagem mediMsgBox, "Tem que escolher uma requisição.", vbExclamation, "Vigilância Epidemiológica"
            End If
            For iGrEpid = 1 To totalGrEpid
                For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                    For iDeta = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
                        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).linha = Tv.SelectedItem.Index Then
                            If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado = gEstadoVePendente Then
                                If VE_AtualizaEstado(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_vig_epid, CStr(gEstadoVeRejeitado), _
                                        EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_ve_email, _
                                        EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_ve_bin) = True Then
                                    EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado = gEstadoVeRejeitado
                                    AtualizaTvLinha iGrEpid, iRegra, iDeta
                                    BG_Mensagem mediMsgBox, "Registo rejeitado", vbExclamation, "Vigilância Epidemiológica"
                                End If
                            Else
                                BG_Mensagem mediMsgBox, "Apenas é possível rejeitar registos ainda não enviados e rejeitados", vbExclamation, "Vigilância Epidemiológica"
                            End If
                        End If
                    Next iDeta
                Next iRegra
            Next iGrEpid
        End If
    End If
End Sub

Private Sub BtPesqRapGrEpid_Click()
    PA_PesquisaGruposEpid EcCodGrEpid, EcDescrEpid, CStr(gCodLocal)
End Sub

Private Sub BtPesquisaRapidaAna_Click()
    PA_PesquisaAnaMultiSel EcListaAnalises
End Sub

Private Sub BtPesquisaRapidaMicrorg_Click()
    PA_PesquisaMicrorgMultiSel EcListaMicrorg
End Sub



Private Sub CbEstado_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
    
        CbEstado.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub Eclistaanalises_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaAnalises, KeyCode, Shift
End Sub

Private Sub EcListaMicrorg_KeyDown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaMicrorg, KeyCode, Shift
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Vigil�ncia Epidemiol�gica"
    Me.left = 50
    Me.top = 50
    Me.Width = 12540
    Me.Height = 8430 ' Normal
    
    Set CampoDeFocus = EcCodGrEpid
    
    'BRUNODSANTOS - 05.06.2017 Glintt-HS-15955
    BtGerarExcel.Enabled = False
    '
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar écran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormCodRegrasVigEpid = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    Tv.Nodes.Clear
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    Tv.LineStyle = tvwTreeLines
    Tv.ImageList = ImageList
    
End Sub

Sub PreencheValoresDefeito()
    Dim i As Integer
    BG_PreencheComboBD_ADO "sl_tbf_estado_ve", "cod_estado_ve", "descr_estado_ve", CbEstado, mediAscComboCodigo
    EcDtFim.value = Bg_DaData_ADO
    EcDtInicio.value = EcDtFim.value
    For i = 0 To CbEstado.ListCount - 1
        If CbEstado.ItemData(i) = gEstadoVePendente Then
            CbEstado.ListIndex = i
            Exit For
        End If
    Next i
End Sub

Sub PreencheCampos()
    Dim iGrEpid As Integer
    Dim iDeta As Integer
    Dim iRegra As Integer
    Dim i As Integer
    Dim Estado As String
    Dim texto As String
    'BRUNODSANTOS 07.03.2017 - Glintt-HS-14510
    Dim strAmarelo As String
    Dim sRet As String
    Dim sRetCod As String
    '
    'Me.SetFocus    GX-12184|CHVNG-11074
    Tv.Nodes.Clear
    
    i = 1
    For iGrEpid = 1 To totalGrEpid
        'BRUNODSANTOS 07.03.2017 - Glintt-HS-14510
        If EstrutGrEpid(iGrEpid).tipo_notificacao = tipo_notificacao.ws_sinave Then
            strAmarelo = "snv_amarelo"
        Else
            strAmarelo = "amarelo"
        End If
        '
    
        Dim root As Node
        Set root = Tv.Nodes.Add(, tvwChild, "KeyGE" & EstrutGrEpid(iGrEpid).cod_gr_epid, EstrutGrEpid(iGrEpid).descr_gr_epid, "grupos")
        root.Expanded = True
        EstrutGrEpid(iGrEpid).linha = i
        i = i + 1
        For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
            Set root = Tv.Nodes.Add(EstrutGrEpid(iGrEpid).linha, tvwChild, "KeyDE" & EstrutGrEpid(iGrEpid).cod_gr_epid & "_" & _
                                    EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra, "Regra: " & EstrutGrEpid(iGrEpid).regras(iRegra).descr_regra, strAmarelo)
            EstrutGrEpid(iGrEpid).regras(iRegra).linha = i
            root.Expanded = True
            i = i + 1
            For iDeta = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
                Select Case EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado
                    Case gEstadoVeEnviado
                        Estado = "email"
                    Case gEstadoVePendente
                        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_ve_bin <> "" Then
                                Estado = "excel"
                        Else
                            Estado = "espera"
                        End If
                    Case gEstadoVeRejeitado
                        Estado = "rejeitado"
                End Select
                texto = "Req.: " & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).n_req & " Data: " & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).dt_chega
                texto = texto & " - " & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).descr_ana
                
                
                If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).descr_micro <> "" Then
                    texto = texto & "(" & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).descr_micro & ")"
                End If
                
                 'BRUNODSANTOS 07.03.2017 - Glintt-HS-14510
                If EstrutGrEpid(iGrEpid).tipo_notificacao = tipo_notificacao.ws_sinave Then
                    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).id_sinave <> "" Then
                        sRetCod = SNV_RetornaConteudoTag(SNV_DevolveRespostaFromID(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).id_sinave), "enviado")
                        
                        If sRetCod = "0" Then
                            Estado = "snv_vermelho"
                        Else
                            Estado = "snv_verde"
                        End If
                        
                        sRet = SNV_RetornaConteudoTag(SNV_DevolveRespostaFromID(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).id_sinave), "mensagem_retorno")
                        
                        texto = texto & " - " & sRet
                    Else
                        Select Case EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado
                            Case gEstadoVePendente
                                Estado = "snv_amarelo_pendente"
                            Case gEstadoVeRejeitado
                                Estado = "snv_vermelho"
                        End Select
                    End If
                End If
                '
                
                Set root = Tv.Nodes.Add(EstrutGrEpid(iGrEpid).regras(iRegra).linha, tvwChild, "KeyVE" & EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_vig_epid, texto, Estado)
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).linha = i
                i = i + 1
            Next iDeta
        Next iRegra
    Next iGrEpid
    If totalGrEpid >= 1 Then
        Tv.Nodes(1).Selected = True
    
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
End Sub

Function ValidaCamposEc() As Integer


End Function

Sub FuncaoProcurar()
    Dim ssql As String
    totalGrEpid = 0
    ReDim EstrutGrEpid(0)

    ssql = ConstroiCriterio(True)
    VE_PreencheResultadosAvaliacao ssql, EstrutGrEpid, totalGrEpid
    ssql = ConstroiCriterio(False)
    VE_PreencheResultadosAvaliacao ssql, EstrutGrEpid, totalGrEpid
    PreencheCampos
    BL_ToolbarEstadoN Estado
    BL_FimProcessamento Me
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me

    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me

    End If

End Sub

Sub FuncaoInserir()

End Sub

Sub FuncaoModificar()

End Sub
Sub FuncaoRemover()
    
End Sub

Private Sub EcCodGrEpid_Validate(Cancel As Boolean)
    PA_ValidateGrupoEpid EcCodGrEpid, EcDescrEpid, CStr(gCodLocal)
End Sub
'edgar.parada Glintt-HS-19848
Private Function ConstroiCriterio(micro As Boolean) As String
    Dim ssql As String
    Dim ssql2 As String
    Dim i As Integer
    Dim condicoes As String
    If micro = True Then
        ssql = "SELECT x1.seq_vig_epid, x2.cod_regra_ve, x2.descr_regra_ve, x3.cod_estado_ve, x3.descr_estado_ve, x1.seq_ve_email, x1.seq_ve_bin, "
        ssql = ssql & " x5.cod_ana_s cod_ana, x5.descr_ana_s descr_ana , x6.cod_microrg, x6.descr_microrg, x7.n_req, x7.dt_chega, "
        ssql = ssql & " x8.cod_gr_epid, x8.descr_gr_epid, x8.flg_nome, x8.flg_req, x8.flg_req, x8.flg_dt_chega, x8.flg_proven, "
        ssql = ssql & " x8.flg_t_sit, x8.flg_dt_nasc, x8.flg_sexo, x8.flg_sns, x2.flg_suspeita, x8.email_to, x8.email_cc, x8.email_body,"
        ssql = ssql & " x8.id_lab, x4.seq_realiza, "
        ssql = ssql & " x8.flg_pt, x8.flg_prod, x8.flg_dt_int, x9.cod_carac_micro, x10.descr_carac_micro, x2.flg_mostra_micro, "
        ssql = ssql & " x8.descr_instituicao, x2.descr_excel, x8.flg_idade, x8.flg_inf_complementar, x11.descr_vig_epid, x7.inf_complementar, x8.flg_processo, "
        ssql = ssql & " DBMS_LOB.SUBSTR (x8.corpo_html, 4000, 1) corpo_html, x8.flg_html, x8.descr_imagem_anexo "
        'NELSONPSILVA 27.09.2018 - CHVNG-9363
        ssql = ssql & ", x8.flg_data_geracao, x8.flg_n_admissoes_inst "
        '
        'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
        ssql = ssql & ", x8.tipo_notificacao, x5.flg_envio_num_sns, x5.flg_envio_cod_conv, x1.id_notif_sinave, NULL enviar1res, NULL enviar2res "
        '
        ssql = ssql & " FROM sl_vig_epid x1 LEFT OUTER JOIN sl_microrg x6 ON x1.cod_micro = x6.cod_microrg, sl_cod_regras_ve x2, "
        ssql = ssql & " sl_tbf_estado_ve x3, sl_realiza x4, sl_ana_s x5, sl_requis x7 LEFT OUTER JOIN  sl_tbf_t_sit x11 ON x7.t_sit = x11.cod_t_sit,"
        ssql = ssql & " sl_cod_gr_epid x8, sl_res_micro x9 "
        ssql = ssql & " LEFT OUTER JOIN sl_tbf_carac_micro x10 ON x9.cod_carac_micro = x10.cod_carac_micro "
        'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
        'sSql = sSql & " sl_log_sinave x12"
        '
        ssql = ssql & " WHERE x1.cod_regra_ve = x2.cod_regra_ve AND x1.cod_estado_ve = x3.cod_estado_ve  AND x2.cod_gr_epid = x8.cod_gR_epid "
        ssql = ssql & " AND x1.seq_realiza = x4.seq_realiza AND x4.cod_ana_s = x5.cod_ana_s  AND x4.n_req = x7.n_req "
        ssql = ssql & " AND x4.seq_realiza = x9.seq_realiza AND x9.cod_micro = x1.cod_micro "
        ssql = ssql & " AND (x1.flg_invisivel IS NULL OR x1.flg_invisivel = 0) "
        ssql = ssql & " AND x4.flg_estado IN (" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica) & "," & BL_TrataStringParaBD(gEstadoAnaImpressa) & ")"
        ssql = ssql & " AND x9.flg_ve = 1 "
        'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
        'sSql = sSql & " AND x1.id_notif_sinave = x12.id_sinave"
        '
        
        If gLAB = "CHVNG" Then
           ' sSql = sSql & " AND x7.user_fecho IS NOT NULL "
        End If
    Else
        
        ssql2 = "  SELECT x1.seq_vig_epid, x2.cod_regra_ve, x2.descr_regra_ve, x3.cod_estado_ve, x3.descr_estado_ve, x1.seq_ve_email, x1.seq_ve_bin, "
        ssql2 = ssql2 & " x5.cod_ana_s cod_ana, x5.descr_ana_s descr_ana, null cod_microrg, null descr_microrg, x7.n_req, x7.dt_chega, "
        ssql2 = ssql2 & " x8.cod_gr_epid, x8.descr_gr_epid, x8.flg_nome, x8.flg_req, x8.flg_req, x8.flg_dt_chega, x8.flg_proven, "
        ssql2 = ssql2 & " x8.flg_t_sit, x8.flg_dt_nasc, x8.flg_sexo, x8.flg_sns, x2.flg_suspeita, x8.email_to, x8.email_cc, x8.email_body,"
        ssql2 = ssql2 & " x8.id_lab, x4.seq_realiza, "
        ssql2 = ssql2 & " x8.flg_pt, x8.flg_prod, x8.flg_dt_int,  null cod_carac_micro, null descr_carac_micro, x2.flg_mostra_micro, "
        ssql2 = ssql2 & " x8.descr_instituicao, x2.descr_excel, x8.flg_idade, x8.flg_inf_complementar, x11.descr_vig_epid, x7.inf_complementar, x8.flg_processo, "
        ssql2 = ssql2 & " DBMS_LOB.SUBSTR (x8.corpo_html, 4000, 1) corpo_html, x8.flg_html, x8.descr_imagem_anexo "
        'NELSONPSILVA 27.09.2018 - CHVNG-9363
        ssql2 = ssql2 & ", x8.flg_data_geracao, x8.flg_n_admissoes_inst "
        '
        'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
        ssql2 = ssql2 & ", x8.tipo_notificacao, x5.flg_envio_num_sns, x5.flg_envio_cod_conv, x1.id_notif_sinave, x9.enviar1res, x9.enviar2res"
        '
        ssql2 = ssql2 & " FROM sl_vig_epid x1, sl_cod_regras_ve x2,sl_tbf_estado_ve x3, sl_realiza x4, sl_ana_s x5, sl_requis x7 "
        ssql2 = ssql2 & " LEFT OUTER JOIN  sl_tbf_t_sit x11 ON x7.t_sit = x11.cod_t_sit, sl_cod_gr_epid x8, sl_cod_regras_ve_ana x9"
        'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
        'ssql2 = ssql2 & " sl_log_sinave x12"
        '
        ssql2 = ssql2 & " WHERE x1.cod_regra_ve = x2.cod_regra_ve AND x1.cod_estado_ve = x3.cod_estado_ve  AND x2.cod_gr_epid = x8.cod_gR_epid "
        ssql2 = ssql2 & " AND x1.seq_realiza = x4.seq_realiza AND x4.cod_ana_s = x5.cod_ana_s  AND x4.n_req = x7.n_req "
        ssql2 = ssql2 & " AND (x1.flg_invisivel IS NULL OR x1.flg_invisivel = 0) AND x1.cod_micro IS NULL "
        ssql2 = ssql2 & " AND x4.flg_estado IN (" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica) & "," & BL_TrataStringParaBD(gEstadoAnaImpressa) & ")"
        ssql2 = ssql2 & " AND x9.cod_regra_ve = x1.cod_regra_ve"
        ssql2 = ssql2 & " AND x9.cod_ana_s = x5.cod_ana_s"
        'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
        'ssql2 = ssql2 & " AND x1.id_notif_sinave = x12.id_sinave"
        '
        If gLAB = "CHVNG" Then
           ' ssql2 = ssql2 & " AND x7.user_fecho IS NOT NULL "
        End If
    End If
    
    If EcSeqRealiza.text <> "" Then
        condicoes = condicoes & " AND x1.seq_realiza = " & EcSeqRealiza.text
    Else
        condicoes = condicoes & " AND x7.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    End If
    If EcCodGrEpid.text <> "" Then
        condicoes = condicoes & " AND x2.cod_gr_epid = " & EcCodGrEpid.text
    End If
    If CbEstado.ListIndex > mediComboValorNull Then
        condicoes = condicoes & " AND x3.cod_estado_ve = " & CbEstado.ItemData(CbEstado.ListIndex)
    End If
    If EcListaAnalises.ListCount > 0 Then
        condicoes = condicoes & " AND x5.seq_ana IN ("
        For i = 0 To EcListaAnalises.ListCount - 1
            condicoes = condicoes & EcListaAnalises.ItemData(i) & ","
        Next i
        condicoes = Mid(condicoes, 1, Len(condicoes) - 2) & ") "
    End If
    
    If EcListaMicrorg.ListCount > 0 Then
        condicoes = condicoes & " AND x6.seq_microrg IN ("
        For i = 0 To EcListaMicrorg.ListCount - 1
            condicoes = condicoes & EcListaMicrorg.ItemData(i) & ","
        Next i
        condicoes = Mid(ssql, 1, Len(condicoes) - 1) & ") "
    End If
    If micro = True Then
        ConstroiCriterio = ssql & condicoes & " ORDER BY dt_chega ASC"
    Else
        ConstroiCriterio = ssql2 & condicoes & " ORDER BY dt_chega ASC"
    End If
End Function


Private Sub AtualizaTvLinha(iGrEpid As Integer, iRegra As Integer, iDeta As Integer)
    'BRUNODSANTOS 07.03.2017 - Glintt-HS-14510
    Dim strAmarelo As String
    Dim strVermelho As String
    Dim strVerde As String
    '

    If EstrutGrEpid(iGrEpid).tipo_notificacao = tipo_notificacao.email Then
        strAmarelo = "amarelo"
        strVermelho = "vermelho"
        strVerde = "verde"
    ElseIf EstrutGrEpid(iGrEpid).tipo_notificacao = tipo_notificacao.ws_sinave Then
        strAmarelo = "snv_amarelo"
        strVermelho = "snv_vermelho"
        strVerde = "snv_verde"
    End If

    
    Select Case EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_estado
        Case gEstadoVeEnviado
            Tv.Nodes(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).linha).Image = strVerde
        Case gEstadoVePendente
            Tv.Nodes(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).linha).Image = strVermelho
        Case gEstadoVeRejeitado
            Tv.Nodes(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).linha).Image = strAmarelo
    End Select
End Sub


'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
Private Sub Tv_Click()

    Dim iGrEpid As Integer
    Dim iRegra As Integer
     If Tv.Nodes.Count > 0 Then
         If Tv.SelectedItem.Index > mediComboValorNull Then
            
            For iGrEpid = 1 To totalGrEpid
                If EstrutGrEpid(iGrEpid).linha = Tv.SelectedItem.Index Then
                   If EstrutGrEpid(iGrEpid).tipo_notificacao <> tipo_notificacao.email Then
                       BtGerarExcel.Enabled = False
                   Else
                       BtGerarExcel.Enabled = True
                   End If
                   Exit For
                End If
                For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                    If EstrutGrEpid(iGrEpid).regras(iRegra).linha = Tv.SelectedItem.Index Then
                        If EstrutGrEpid(iGrEpid).tipo_notificacao <> tipo_notificacao.email Then
                            BtGerarExcel.Enabled = False
                        Else
                            BtGerarExcel.Enabled = True
                        End If
                        Exit For
                    End If
                Next iRegra
            Next iGrEpid
            
         End If
     End If
End Sub

Private Sub Tv_DblClick()
    Dim iGrEpid As Integer
    Dim iDeta As Integer
    Dim iRegra As Integer
    Dim iDet As Integer
    Dim flgEncontrou As Boolean
    flgEncontrou = False
    If Tv.Nodes.Count > 0 Then
        If Tv.SelectedItem.Index > mediComboValorNull Then
            For iGrEpid = 1 To totalGrEpid
                iRegra = 0
                If EstrutGrEpid(iGrEpid).linha = Tv.SelectedItem.Index Then
                    flgEncontrou = True
                    Exit For
                End If
                For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
                    iDet = 0
                    If EstrutGrEpid(iGrEpid).regras(iRegra).linha = Tv.SelectedItem.Index Then
                        flgEncontrou = True
                        Exit For
                    End If
                    For iDet = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
                        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).linha = Tv.SelectedItem.Index Then
                            flgEncontrou = True
                            Exit For
                        End If
                    Next iDet
                    If flgEncontrou = True Then
                        Exit For
                    End If
                Next iRegra
                If flgEncontrou = True Then
                    Exit For
                End If
            Next iGrEpid
        End If
    End If
    If iDet > 0 And iRegra > 0 And iGrEpid > 0 And flgEncontrou = True Then
        If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_ve_bin <> "" Then
            BG_Mensagem mediMsgStatus, "A abrir ficheiro excel"
            If VE_RetiraExcelBD(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_ve_bin, True) = True Then
                BG_Mensagem mediMsgStatus, "Ficheiro excel aberto com sucesso."
            Else
                BG_Mensagem mediMsgStatus, "Erro ao abrir ficheiro excel"
            End If
        End If
    End If
End Sub

Public Sub AtualizaSeqVeEmail(iGrEpid As Integer, seq_ve_email As String)
    Dim iRegra As Integer
    Dim iDet As Integer
    For iRegra = 1 To EstrutGrEpid(iGrEpid).totalRegra
        For iDet = 1 To EstrutGrEpid(iGrEpid).regras(iRegra).totDet
            If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_estado = gEstadoVePendente Then
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_estado = gEstadoVeEnviado
                EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_ve_email = seq_ve_email
                VE_atualizaVigEpidEmail EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_vig_epid, _
                        EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_ve_email, _
                        EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).seq_ve_bin, _
                        EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).cod_estado
                If Tv.Nodes.Item(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).linha).Image <> "email" Then
                    Tv.Nodes.Item(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDet).linha).Image = "email"
                End If
                DoEvents
            End If
        Next iDet
    Next iRegra
End Sub
Private Function verificaSeqRealiza(ByVal seqRealiza As String) As Boolean
 Dim rs As New adodb.recordset
 Dim sql As String

 verificaSeqRealiza = False
 
 With rs
 .CursorLocation = adUseClient
 .CursorType = adOpenStatic
 End With
 
 sql = "select * from sl_realiza where seq_realiza = " & BL_TrataStringParaBD(seqRealiza)
 
 rs.Open sql, gConexao
 If rs.RecordCount > 0 Then verificaSeqRealiza = True
 
 If rs.state = adStateOpen Then rs.Close
 
 Set rs = Nothing
 
End Function


VERSION 5.00
Begin VB.Form FormCodUtilizadoresExternos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCodUtilizadoresExternos"
   ClientHeight    =   5910
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7710
   Icon            =   "FormCodUtilizadoresExternos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5910
   ScaleWidth      =   7710
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcConfirmacaoPassword 
      Appearance      =   0  'Flat
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1140
      PasswordChar    =   "*"
      TabIndex        =   35
      Top             =   1200
      Width           =   2115
   End
   Begin VB.CommandButton BtPesquisaEntidade 
      Height          =   315
      Left            =   7200
      Picture         =   "FormCodUtilizadoresExternos.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   33
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Produtos"
      Top             =   1920
      Width           =   375
   End
   Begin VB.TextBox EcCodEntidade 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1140
      TabIndex        =   32
      Top             =   1920
      Width           =   855
   End
   Begin VB.TextBox EcDescrEntidade 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2040
      MultiLine       =   -1  'True
      TabIndex        =   31
      Top             =   1920
      Width           =   5235
   End
   Begin VB.TextBox EcPassword 
      Appearance      =   0  'Flat
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1140
      PasswordChar    =   "*"
      TabIndex        =   27
      Top             =   840
      Width           =   2115
   End
   Begin VB.TextBox EcHoraAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5880
      TabIndex        =   26
      Top             =   6360
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcHoraCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6000
      TabIndex        =   25
      Top             =   6000
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.CheckBox CkInvisivel 
      Appearance      =   0  'Flat
      Caption         =   "Desactivo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3480
      TabIndex        =   24
      Top             =   120
      Width           =   1935
   End
   Begin VB.TextBox EcNome 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1140
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1560
      Width           =   6435
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2100
      TabIndex        =   14
      Top             =   6000
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   13
      Top             =   6480
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2100
      TabIndex        =   12
      Top             =   6120
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   11
      Top             =   6120
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   20
      TabIndex        =   4
      Top             =   4920
      Width           =   7605
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   8
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   7
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   6
         Top             =   195
         Width           =   2175
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   5
         Top             =   480
         Width           =   2175
      End
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      Left            =   0
      TabIndex        =   3
      Top             =   2760
      Width           =   7605
   End
   Begin VB.TextBox EcUsername 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1140
      TabIndex        =   1
      Top             =   480
      Width           =   4035
   End
   Begin VB.TextBox EcSeq 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1140
      TabIndex        =   0
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label 
      Caption         =   "Confirma��o Password"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   36
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Entidade"
      Height          =   255
      Left            =   6720
      TabIndex        =   34
      Top             =   2520
      Width           =   1065
   End
   Begin VB.Label Entidade 
      Caption         =   "Entidade"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   30
      Top             =   2040
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Nome"
      Height          =   255
      Left            =   2400
      TabIndex        =   29
      Top             =   2520
      Width           =   1065
   End
   Begin VB.Label Label 
      Caption         =   "Password"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   28
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Nome"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   23
      Top             =   1680
      Width           =   855
   End
   Begin VB.Label Username 
      Caption         =   "Username"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   22
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label7 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   60
      TabIndex        =   20
      Top             =   2520
      Width           =   615
   End
   Begin VB.Label Label9 
      Caption         =   "Username"
      Height          =   255
      Left            =   780
      TabIndex        =   19
      Top             =   2520
      Width           =   1065
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   375
      Left            =   540
      TabIndex        =   18
      Top             =   6120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   375
      Left            =   420
      TabIndex        =   17
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCriacao"
      Height          =   375
      Left            =   3540
      TabIndex        =   16
      Top             =   6120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataAlteracao"
      Height          =   375
      Left            =   3420
      TabIndex        =   15
      Top             =   6480
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormCodUtilizadoresExternos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'RGONCALVES 15.03.2016 CEDIVET-160
' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As ADODB.recordset



Private Sub BtPesquisaEntidade_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 1000
    
    ChavesPesq(2) = "descr_efr"
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 4500
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_efr"
    CampoPesquisa1 = "descr_efr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Entidade")
    
    mensagem = "N�o foi encontrada nenhuma Entidade."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodEntidade.Text = resultados(1)
            EcDescrEntidade.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodEntidade_Validate(Cancel As Boolean)
    Dim RsDescrEntidade As ADODB.recordset
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodEntidade.Text) <> "" Then
        Set RsDescrEntidade = New ADODB.recordset
        
        With RsDescrEntidade
            .Source = "SELECT cod_efr, descr_efr FROM sl_efr WHERE upper(cod_efr)= " & BL_TrataStringParaBD(UCase(EcCodEntidade.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrEntidade.RecordCount > 0 Then
            EcCodEntidade.Text = "" & RsDescrEntidade!cod_efr
            EcDescrEntidade.Text = "" & RsDescrEntidade!descr_efr
        Else
            Cancel = True
            EcDescrEntidade.Text = ""
            BG_Mensagem mediMsgBox, "A entidade indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrEntidade.Close
        Set RsDescrEntidade = Nothing
    Else
        EcDescrEntidade.Text = ""
    End If
End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If

End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcSeq_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcSeq.Text = UCase(EcSeq.Text)

End Sub

Private Sub EcNome_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcNome_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Codifica��o de utilizadores externos"
    Me.left = 50
    Me.top = 50
    Me.Width = 7800
    Me.Height = 6345 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_cod_utilizadores_externos"
    Set CampoDeFocus = EcUsername
    
    NumCampos = 12
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_util_ext"
    CamposBD(1) = "username"
    CamposBD(2) = "nome"
    CamposBD(3) = "password"
    CamposBD(4) = "cod_entidade"
    CamposBD(5) = "user_cri"
    CamposBD(6) = "dt_cri"
    CamposBD(7) = "hr_cri"
    CamposBD(8) = "user_act"
    CamposBD(9) = "dt_act"
    CamposBD(10) = "hr_act"
    CamposBD(11) = "flg_desactivo"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeq
    Set CamposEc(1) = EcUsername
    Set CamposEc(2) = EcNome
    Set CamposEc(3) = EcPassword
    Set CamposEc(4) = EcCodEntidade
    Set CamposEc(5) = EcUtilizadorCriacao
    Set CamposEc(6) = EcDataCriacao
    Set CamposEc(7) = EcHoraCriacao
    Set CamposEc(8) = EcUtilizadorAlteracao
    Set CamposEc(9) = EcDataAlteracao
    Set CamposEc(10) = EcHoraAlteracao
    Set CamposEc(11) = CkInvisivel
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Username"
    TextoCamposObrigatorios(2) = "Descri��o"
    TextoCamposObrigatorios(3) = "Password"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_util_ext"
    Set ChaveEc = EcSeq
    
    CamposBDparaListBox = Array("seq_util_ext", "username", "nome", "cod_entidade")
    'CamposBDparaListBox = Array("cod_postal", "area_geografica")
    NumEspacos = Array(6, 15, 40, 10)

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormCodPostal = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CkInvisivel.value = vbGrayed
    EcDescrEntidade.Text = ""
    EcConfirmacaoPassword.Text = ""

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()

    DoEvents
    CkInvisivel.value = vbGrayed
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
        EcConfirmacaoPassword.Text = EcPassword.Text
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & " " & EcHoraCriacao
        LaDataAlteracao = EcDataAlteracao & " " & EcHoraAlteracao
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim str_aux1 As String
    Dim str_aux2 As String
    Dim str_aux3 As String
    
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
               
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)

              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        
        BG_PreencheListBoxMultipla_ADO EcLista, _
                                       rs, _
                                       CamposBDparaListBox, _
                                       NumEspacos, _
                                       CamposBD, _
                                       CamposEc, _
                                       "SELECT"
        
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        EcHoraCriacao = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    If VerificaPassword = False Then
        Exit Sub
    End If
    gSQLError = 0
    EcSeq = BL_HandleNull(BG_DaMAX(NomeTabela, "seq_util_ext"), 0) + 1
    Dim value As Integer
    value = CkInvisivel.value
    If value = CheckBoxConstants.vbGrayed Then
        CkInvisivel.value = vbUnchecked
    End If
    EcUsername.Text = Trim(UCase(EcUsername.Text))
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    CkInvisivel.value = value
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcHoraAlteracao = Bg_DaHora_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    If VerificaPassword = False Then
        Exit Sub
    End If
    
    gSQLError = 0
    EcUsername.Text = Trim(UCase(EcUsername.Text))
     
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    Dim iRes As Boolean
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer cancelar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        CkInvisivel.value = vbChecked
        EcDataAlteracao = CDate(Bg_DaData_ADO & " " & Bg_DaHora_ADO)
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If
    
End Sub

Private Function VerificaPassword() As Boolean
    If EcPassword.Text <> EcConfirmacaoPassword.Text Then
        Call BG_Mensagem(mediMsgBox, "A password e a sua confirma��o s�o diferentes.", vbInformation, "SISLAB")
        VerificaPassword = False
    Else
        VerificaPassword = True
    End If
End Function

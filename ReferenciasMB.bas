Attribute VB_Name = "ReferenciasMB"
Option Explicit
Private Type mbFichRef
    seq_utente As Long
    t_utente As String
    Utente As String
    dt_ref As String
    estado_ref As String
    nome_ute As String
    seq_ref_emitida As String
    valor_ref As Double
    valor_ficheiro As Double
    linha_ficheiro As String
    
    FIC_REGCOD As String
    SIS_PRCCOD As String
    LOG_PERN01 As String
    LOG_NUMN01 As String
    SIS_DTHN01 As String
    SIS_PSCMNT As String
    LOG_TARMNT As String
    TRM_EQPTIP  As String
    TRM_IDE As String
    TRM_REGNUM As String
    LOC_TRMABV As String
    EPS_REF As String
    LOG_ENVMRT_EPS As String
    MSG_RESTIP_EPS As String
    MSG_RESNUM As String
    LOG_SIS As String
    filler As String
End Type

Public Type mbFicheiro
    seq_ficheiro As Long
    
    FIC_REGCOD_H As String
    FIC_NOMN01 As String
    COM_NUMN01_ORI As String
    COM_NUMN01_DST As String
    fic_seq As String
    fic_seq_ult As String
    EPS_NUM As String
    EXT_MOECOD As String
    EXT_PCTIV As String
    filler_H As String
    
    referencias() As mbFichRef
    totalRef As Integer
    
    FIC_REGCOD_T As String
    FIC_REGQNT As String
    EPS_TOTMNT_FIC As String
    FIC_TARMNTN01 As String
    FIC_IVAMNTN01 As String
    filler_t As String
End Type

Public Type mbDocumentos
    seq_ref_emitida_fact As Long
    cod_tipo_doc As Integer
    descR_tipo_doc As String
    serie_doc As String
    data_doc As String
    valor As Double
    n_doc As String
End Type

Private Type mbRefEmitidas
    seq_ref_emitida As Long
    cod_local As String
    cod_ent_ref As Integer
    cod_ref_pag As Long
    val_pag As Double
    user_cri As String
    dt_cri As String
    dt_fim_pag As String
    flg_estado As Integer
    user_conc As String
    dt_conc As String
    cod_motivo_conc As String
    
    dt_pagamento As String
    
    user_doc As String
    dt_doc As String
    
    user_doc2 As String
    dt_doc2 As String
    
    seq_fich_conc As Long
    observacao As String
    documentos() As mbDocumentos
    totalDoc As Integer
End Type

Public Type mbUtente
    seq_utente As Long
    t_utente As String
    Utente As String
    nome_ute As String
    dt_nasc_ute As String
    num_cartao_ute As String
    num_cartao_contrib As String
    
    referencias() As mbRefEmitidas
    totalRef As Integer
End Type

' -----------------------------------------------------------------------------------------------------------

' INICIALIZA ESTRUTURAS

' -----------------------------------------------------------------------------------------------------------
Public Sub MB_InicializaEstrut(referenciaMB As mbUtente)
    On Error GoTo TrataErro
    
    referenciaMB.seq_utente = mediComboValorNull
    referenciaMB.dt_nasc_ute = ""
    referenciaMB.t_utente = ""
    referenciaMB.Utente = ""
    referenciaMB.nome_ute = ""
    referenciaMB.num_cartao_ute = ""
    referenciaMB.num_cartao_contrib = ""
    referenciaMB.totalRef = 0
    ReDim referenciaMB.referencias(referenciaMB.totalRef)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "MB_InicializaEstrut: " & Err.Number & " - " & Err.Description, "MB", "MB_InicializaEstrut"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------------------

' INICIALIZA ESTRUTURA DOCUMENTOS

' -----------------------------------------------------------------------------------------------------------
Public Sub MB_InicializaEstrutDoc(documentoMB As mbDocumentos)
    On Error GoTo TrataErro
    
    documentoMB.cod_tipo_doc = mediComboValorNull
    documentoMB.descR_tipo_doc = ""
    documentoMB.n_doc = ""
    documentoMB.seq_ref_emitida_fact = mediComboValorNull
    documentoMB.serie_doc = ""
    documentoMB.data_doc = ""
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "MB_InicializaEstrutDoc: " & Err.Number & " - " & Err.Description, "MB", "MB_InicializaEstrutDoc"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA UTENTE

' -----------------------------------------------------------------------------------------------------------

Public Sub MB_PreencheDadosUtente(referenciaMB As mbUtente, seq_utente As Long, dt_nasc_ute As String, t_utente As String, _
                                  Utente As String, nome_ute As String, num_cartao_ute As String, num_cartao_contrib As String)
    On Error GoTo TrataErro
    
    referenciaMB.seq_utente = seq_utente
    referenciaMB.dt_nasc_ute = dt_nasc_ute
    referenciaMB.t_utente = t_utente
    referenciaMB.Utente = Utente
    referenciaMB.nome_ute = nome_ute
    referenciaMB.num_cartao_ute = num_cartao_ute
    referenciaMB.num_cartao_contrib = num_cartao_contrib
    referenciaMB.totalRef = 0
    ReDim referenciaMB.referencias(referenciaMB.totalRef)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "MB_PreencheDadosUtente: " & Err.Number & " - " & Err.Description, "MB", "MB_PreencheDadosUtente"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA UTENTE

' -----------------------------------------------------------------------------------------------------------

Public Sub MB_AdicionaDocumento(documentosMB() As mbDocumentos, totalDoc As Integer, cod_tipo_doc As Integer, descR_tipo_doc As String, _
                                n_doc As String, seq_ref_emitida_fact As Long, serie_doc As String, data_documento As String, valor As Double)
    On Error GoTo TrataErro
    Dim i As Integer
    i = 0
    totalDoc = totalDoc + 1
    i = 1
    ReDim Preserve documentosMB(totalDoc)
    i = 2
    documentosMB(totalDoc).cod_tipo_doc = cod_tipo_doc
    i = 3
    documentosMB(totalDoc).descR_tipo_doc = descR_tipo_doc
    i = 4
    documentosMB(totalDoc).n_doc = n_doc
    i = 5
    documentosMB(totalDoc).seq_ref_emitida_fact = seq_ref_emitida_fact
    i = 6
    documentosMB(totalDoc).serie_doc = serie_doc
    i = 7
    documentosMB(totalDoc).data_doc = data_documento
    i = 8
    documentosMB(totalDoc).valor = valor
    i = 9
Exit Sub
TrataErro:
    BG_LogFile_Erros i & "MB_AdicionaDocumento: " & Err.Number & " - " & Err.Description, "MB", "MB_AdicionaDocumento"
    Exit Sub
    Resume Next
End Sub



Function MB_GeraReferencia(seq_utente As Long, cod_local As String, valor As Double, dt_venc As String, observacao As String) As Long

    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtSeqUtente As ADODB.Parameter
    Dim PmtCodLocal As ADODB.Parameter
    Dim PmtValor As ADODB.Parameter
    Dim PmtDtVenc As ADODB.Parameter
    Dim PmtUserCri As ADODB.Parameter
    Dim PmtObservacao As ADODB.Parameter
    Dim rsData As ADODB.recordset
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "pck_multibanco.gera_referencia"
        .CommandType = adCmdStoredProc
    End With
    
        Set PmtGera = CmdGera.CreateParameter("RETORNO", adNumeric, adParamOutput, 0)
        PmtGera.Precision = 12
        
        Set PmtSeqUtente = CmdGera.CreateParameter("I_SEQ_UTENTE", adVarChar, adParamInput, 30)
        Set PmtCodLocal = CmdGera.CreateParameter("I_COD_LOCAL", adVarChar, adParamInput, 10)
        Set PmtValor = CmdGera.CreateParameter("I_VALOR", adVarNumeric, adParamInput, 0)
        Set PmtDtVenc = CmdGera.CreateParameter("I_DT_VENC", adDate, adParamInput, 0)
        Set PmtUserCri = CmdGera.CreateParameter("I_USER_CRI", adVarChar, adParamInput, 10)
        Set PmtObservacao = CmdGera.CreateParameter("I_OBSERVACAO", adVarChar, adParamInput, 1000)
        
        CmdGera.Parameters.Append PmtSeqUtente
        CmdGera.Parameters.Append PmtCodLocal
        CmdGera.Parameters.Append PmtValor
        CmdGera.Parameters.Append PmtDtVenc
        CmdGera.Parameters.Append PmtUserCri
        CmdGera.Parameters.Append PmtObservacao
        CmdGera.Parameters.Append PmtGera
        
        CmdGera.Parameters("I_SEQ_UTENTE") = seq_utente
        CmdGera.Parameters("I_COD_LOCAL") = cod_local
        CmdGera.Parameters("I_VALOR") = valor
        CmdGera.Parameters("I_DT_VENC") = BL_HandleNull(dt_venc, Empty)
        CmdGera.Parameters("I_USER_CRI") = CStr(gCodUtilizador)
        CmdGera.Parameters("I_OBSERVACAO") = observacao
        CmdGera.Execute
        MB_GeraReferencia = Trim(CmdGera.Parameters.item("RETORNO").value)
    
    
    Set rsData = Nothing
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtSeqUtente = Nothing
    Set PmtCodLocal = Nothing
    Set PmtValor = Nothing
    Set PmtDtVenc = Nothing
    Set PmtUserCri = Nothing
       
Exit Function
TrataErro:
    MB_GeraReferencia = -1
    BG_LogFile_Erros "MB_GeraReferencia:" & Err.Description, "MB", "MB_GeraReferencia", True
    Exit Function
    Resume Next
End Function



Public Function MB_PreencheReferencia(Utente As mbUtente, seq_ref_emitida As Long, cod_estado As String) As Boolean
    Dim sSql As String
    Dim RsRef As New ADODB.recordset
    On Error GoTo TrataErro
    MB_PreencheReferencia = False
    
    sSql = "SELECT * FROM sl_mb_ref_emitida WHERE seq_utente = " & Utente.seq_utente
    If seq_ref_emitida > 0 Then
        sSql = sSql & " AND seq_ref_emitida = " & seq_ref_emitida
    End If
    If cod_estado <> "" Then
        sSql = sSql & " AND flg_estado = " & cod_estado
    End If
    sSql = sSql & " ORDER BY dt_cri DESC "
    
    RsRef.CursorType = adOpenStatic
    RsRef.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRef.Open sSql, gConexao
    If RsRef.RecordCount >= 1 Then
        While Not RsRef.EOF
            Utente.totalRef = Utente.totalRef + 1
            ReDim Preserve Utente.referencias(Utente.totalRef)
            
            Utente.referencias(Utente.totalRef).seq_ref_emitida = BL_HandleNull(RsRef!seq_ref_emitida, mediComboValorNull)
            Utente.referencias(Utente.totalRef).cod_ent_ref = BL_HandleNull(RsRef!cod_ent_ref, mediComboValorNull)
            Utente.referencias(Utente.totalRef).cod_local = BL_HandleNull(RsRef!cod_local, "")
            Utente.referencias(Utente.totalRef).cod_ref_pag = BL_HandleNull(RsRef!cod_ref_pag, mediComboValorNull)
            Utente.referencias(Utente.totalRef).dt_conc = BL_HandleNull(RsRef!dt_conc, "")
            Utente.referencias(Utente.totalRef).dt_pagamento = BL_HandleNull(RsRef!dt_pagamento, "")
            Utente.referencias(Utente.totalRef).dt_cri = BL_HandleNull(RsRef!dt_cri, "")
            Utente.referencias(Utente.totalRef).dt_fim_pag = BL_HandleNull(RsRef!dt_fim_pag, "")
            Utente.referencias(Utente.totalRef).flg_estado = BL_HandleNull(RsRef!flg_estado, mediComboValorNull)
            Utente.referencias(Utente.totalRef).seq_fich_conc = BL_HandleNull(RsRef!seq_fich_conc, mediComboValorNull)
            Utente.referencias(Utente.totalRef).user_conc = BL_HandleNull(RsRef!user_conc, "")
            Utente.referencias(Utente.totalRef).user_cri = BL_HandleNull(RsRef!user_cri, "")
            Utente.referencias(Utente.totalRef).val_pag = BL_HandleNull(RsRef!val_pag, 0)
            Utente.referencias(Utente.totalRef).observacao = BL_HandleNull(RsRef!observacao, "")
            Utente.referencias(Utente.totalRef).cod_motivo_conc = BL_HandleNull(RsRef!cod_motivo_conc, "")
            
            MB_CarregaImpressaoDocumento Utente.referencias(Utente.totalRef)
            Utente.referencias(Utente.totalRef).totalDoc = 0
            ReDim Utente.referencias(Utente.totalRef).documentos(Utente.referencias(Utente.totalRef).totalDoc)
            
            RsRef.MoveNext
        Wend
    End If
    RsRef.Close
    Set RsRef = Nothing
    MB_PreencheReferencia = True
Exit Function
TrataErro:
    MB_PreencheReferencia = False
    BG_LogFile_Erros "MB_PreencheReferencia:" & Err.Description, "MB", "MB_PreencheReferencia", False
    Exit Function
    Resume Next
End Function


Public Function MB_GravaDocumentos(ref() As mbRefEmitidas, indice As Integer) As Boolean
    Dim sSql As String
    Dim RsRef As New ADODB.recordset
    Dim i As Integer
    
    MB_GravaDocumentos = False
    For i = 1 To ref(indice).totalDoc
        If ref(indice).documentos(i).seq_ref_emitida_fact <= 0 Then
            sSql = "SELECT seq_ref_emitida_fact.nextval proximo FROM dual"
            RsRef.CursorType = adOpenStatic
            RsRef.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            RsRef.Open sSql, gConexao
            If RsRef.RecordCount = 1 Then
                ref(indice).documentos(i).seq_ref_emitida_fact = BL_HandleNull(RsRef!proximo, mediComboValorNull)
            End If
            RsRef.Close
            Set RsRef = Nothing
        End If
        sSql = "INSERT INTO sl_mb_ref_emitida_fact(seq_ref_emitida_fact, seq_ref_emitida, cod_t_doc, serie_doc, n_doc, dt_documento,valor) VALUES("
        sSql = sSql & ref(indice).documentos(i).seq_ref_emitida_fact & ", "
        sSql = sSql & ref(indice).seq_ref_emitida & ", "
        sSql = sSql & ref(indice).documentos(i).cod_tipo_doc & ", "
        sSql = sSql & BL_TrataStringParaBD(ref(indice).documentos(i).serie_doc) & ", "
        sSql = sSql & BL_TrataStringParaBD(ref(indice).documentos(i).n_doc) & ", "
        sSql = sSql & BL_TrataDataParaBD(ref(indice).documentos(i).data_doc) & ", "
        sSql = sSql & Replace(ref(indice).documentos(i).valor, ",", ".") & ") "
        BG_ExecutaQuery_ADO sSql
    Next i
    MB_GravaDocumentos = True
Exit Function
TrataErro:
    MB_GravaDocumentos = False
    BG_LogFile_Erros "MB_GravaDocumentos:" & Err.Description, "MB", "MB_GravaDocumentos", False
    Exit Function
    Resume Next
End Function


Public Function MB_PreencheDocumentos(referencias() As mbRefEmitidas, indice As Integer, seq_ref_emitida_fact As Long, _
                                     cod_t_doc As Integer, serie_doc As String, n_doc As String, dt_doc As String, valor As Double) As Boolean
    On Error GoTo TrataErro
    
    referencias(indice).totalDoc = referencias(indice).totalDoc + 1
    ReDim Preserve referencias(indice).documentos(referencias(indice).totalDoc)
    referencias(indice).documentos(referencias(indice).totalDoc).seq_ref_emitida_fact = seq_ref_emitida_fact
    referencias(indice).documentos(referencias(indice).totalDoc).cod_tipo_doc = cod_t_doc
    referencias(indice).documentos(referencias(indice).totalDoc).descR_tipo_doc = BL_SelCodigo("SL_tbf_tipo_doc", "descr_t_doc", "cod_t_doc", cod_t_doc)
    referencias(indice).documentos(referencias(indice).totalDoc).serie_doc = serie_doc
    referencias(indice).documentos(referencias(indice).totalDoc).n_doc = n_doc
    referencias(indice).documentos(referencias(indice).totalDoc).data_doc = dt_doc
    referencias(indice).documentos(referencias(indice).totalDoc).valor = valor
    MB_PreencheDocumentos = True

Exit Function
TrataErro:
    MB_PreencheDocumentos = False
    BG_LogFile_Erros "MB_PreencheDocumentos:" & Err.Description, "MB", "MB_PreencheDocumentos", False
    Exit Function
    Resume Next
End Function


Public Function MB_TrataFicheiro(NomeFicheiro As String) As Long
    On Error GoTo TrataErro
    Dim rsFich As New ADODB.recordset
    Dim seq_ficheiro_mov As Long
    Dim sSql As String
    Dim i As Integer
    Dim FSO As New FileSystemObject
    Dim TS As TextStream
    Dim Final As String
    
    
    On Error GoTo TrataErro
    MB_TrataFicheiro = mediComboValorNull
    
    sSql = "SELECT seq_ficheiro_mov.nextval proximo FROM dual "
    rsFich.CursorType = adOpenKeyset
    rsFich.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFich.Open sSql, gConexao
    If rsFich.RecordCount = 1 Then
        seq_ficheiro_mov = BL_HandleNull(rsFich!proximo, mediComboValorNull)
    Else
        seq_ficheiro_mov = mediComboValorNull
    End If
    rsFich.Close
    
    If seq_ficheiro_mov > mediComboValorNull Then
        sSql = "INSERT INTO sl_mb_ficheiros_mov (seq_ficheiro_mov,user_cri, dt_cri,fic_seq,fic_seq_ult, flg_tratado) "
        sSql = sSql & " VALUES (" & seq_ficheiro_mov & "," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", sysdate, '000000000','000000000',0)"
        BG_ExecutaQuery_ADO sSql
        
        
        sSql = "SELECT * FROM  sl_mb_ficheiros_mov  WHERE seq_ficheiro_mov  = " & seq_ficheiro_mov
        rsFich.CursorType = adOpenKeyset
        rsFich.LockType = adLockOptimistic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsFich.Open sSql, gConexao
    
        If rsFich.RecordCount = 1 Then
            Set FSO = New FileSystemObject
            Set TS = FSO.OpenTextFile(NomeFicheiro, ForReading)
            Final = TS.ReadAll
            rsFich.Fields("ficheiro").AppendChunk Final
            TS.Close
        End If
        rsFich.Close
    End If
    MB_TrataFicheiro = seq_ficheiro_mov
Exit Function
TrataErro:
    MB_TrataFicheiro = mediComboValorNull
    BG_LogFile_Erros "MB_TrataFicheiro: " & Err.Number & " - " & Err.Description, "MB", "MB_TrataFicheiro"
    Exit Function
    Resume Next
End Function



Public Function MB_PreencheFicheiro(seq_ficheiro_mov As Long, ficheiro As mbFicheiro) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsFich As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_mb_ficheiros_mov  WHERE seq_ficheiro_mov = " & seq_ficheiro_mov
    rsFich.CursorType = adOpenKeyset
    rsFich.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFich.Open sSql, gConexao
    If rsFich.RecordCount = 1 Then
        MB_PreencheFicheiro = MB_PreeencheEstrFicheiro(BL_HandleNull(rsFich!ficheiro, ""), ficheiro, seq_ficheiro_mov)
    End If
    rsFich.Close
    
    sSql = "UPDATE sl_mb_ficheiros_mov SET fic_seq = " & BL_TrataStringParaBD(ficheiro.fic_seq) & ","
    sSql = sSql & " fic_seq_ult = " & BL_TrataStringParaBD(ficheiro.fic_seq_ult) & " WHERE seq_ficheiro_mov = " & seq_ficheiro_mov
    BG_ExecutaQuery_ADO sSql
    
Exit Function
TrataErro:
    MB_PreencheFicheiro = False
    BG_LogFile_Erros "MB_PreencheFicheiro: " & Err.Number & " - " & Err.Description, "MB", "MB_PreencheFicheiro"
    Exit Function
    Resume Next
End Function

Private Function MB_PreeencheEstrFicheiro(conteudiFich As String, ficheiro As mbFicheiro, seq_ficheiro_mov As Long) As Boolean
    Dim lines() As String
    Dim tail As Integer
    Dim i As Integer
    
    lines = Split(conteudiFich, vbCrLf)
    tail = UBound(lines) - 1
    ficheiro.totalRef = 0
    ReDim ficheiro.referencias(0)
    ficheiro.seq_ficheiro = seq_ficheiro_mov
    ficheiro.FIC_REGCOD_H = Mid(lines(0), 1, 1)
    ficheiro.FIC_NOMN01 = Mid(lines(0), 2, 4)
    ficheiro.COM_NUMN01_ORI = Mid(lines(0), 6, 8)
    ficheiro.COM_NUMN01_DST = Mid(lines(0), 14, 8)
    ficheiro.fic_seq = Mid(lines(0), 22, 9)
    ficheiro.fic_seq_ult = Mid(lines(0), 31, 9)
    ficheiro.EPS_NUM = Mid(lines(0), 40, 5)
    ficheiro.EXT_MOECOD = Mid(lines(0), 45, 3)
    ficheiro.EXT_PCTIV = Mid(lines(0), 48, 2)
    ficheiro.filler_H = Mid(lines(0), 50)
    For i = 1 To tail - 1
        ficheiro.totalRef = ficheiro.totalRef + 1
        ReDim Preserve ficheiro.referencias(ficheiro.totalRef)
        If MB_PreeencheEstrFicheiroDet(lines(i), ficheiro.referencias(ficheiro.totalRef)) = False Then
            Exit Function
        End If
        MB_PreencheDadosUtenteFicheiro ficheiro.referencias(ficheiro.totalRef)
    Next i
    ficheiro.FIC_REGCOD_T = Mid(lines(tail), 1, 1)
    ficheiro.FIC_REGQNT = Mid(lines(tail), 2, 8)
    ficheiro.EPS_TOTMNT_FIC = Mid(lines(tail), 10, 17)
    ficheiro.FIC_TARMNTN01 = Mid(lines(tail), 27, 12)
    ficheiro.FIC_IVAMNTN01 = Mid(lines(tail), 39, 12)
    ficheiro.filler_t = Mid(lines(tail), 51)
    MB_PreeencheEstrFicheiro = True
Exit Function
TrataErro:
    MB_PreeencheEstrFicheiro = False
    BG_LogFile_Erros "MB_PreeencheEstrFicheiro: " & Err.Number & " - " & Err.Description, "MB", "MB_PreeencheEstrFicheiro"
    Exit Function
    Resume Next
End Function

Private Function MB_PreeencheEstrFicheiroDet(linha As String, detalhe As mbFichRef) As Boolean
    On Error GoTo TrataErro
    
    detalhe.linha_ficheiro = Trim(linha)
    detalhe.FIC_REGCOD = Mid(linha, 1, 1)
    detalhe.SIS_PRCCOD = Mid(linha, 2, 2)
    detalhe.LOG_PERN01 = Mid(linha, 4, 4)
    detalhe.LOG_NUMN01 = Mid(linha, 8, 8)
    detalhe.SIS_DTHN01 = Mid(linha, 16, 12)
    detalhe.SIS_PSCMNT = Mid(linha, 28, 10)
    detalhe.valor_ficheiro = CDbl(Mid(detalhe.SIS_PSCMNT, 1, 8) & "," & Mid(detalhe.SIS_PSCMNT, 9, 2))
    detalhe.LOG_TARMNT = Mid(linha, 38, 5)
    detalhe.TRM_EQPTIP = Mid(linha, 43, 2)
    detalhe.TRM_IDE = Mid(linha, 45, 10)
    detalhe.TRM_REGNUM = Mid(linha, 55, 5)
    detalhe.LOC_TRMABV = Mid(linha, 60, 15)
    detalhe.EPS_REF = Mid(linha, 75, 9)
    detalhe.LOG_ENVMRT_EPS = Mid(linha, 84, 1)
    detalhe.MSG_RESTIP_EPS = Mid(linha, 85, 1)
    detalhe.MSG_RESNUM = Mid(linha, 86, 12)
    detalhe.LOG_SIS = Mid(linha, 98, 2)
    detalhe.filler = Mid(linha, 100)
    MB_PreeencheEstrFicheiroDet = True
Exit Function
TrataErro:
    MB_PreeencheEstrFicheiroDet = False
    BG_LogFile_Erros "MB_PreeencheEstrFicheiroDet: " & Err.Number & " - " & Err.Description, "MB", "MB_PreeencheEstrFicheiroDet"
    Exit Function
    Resume Next
End Function

Private Function MB_PreencheDadosUtenteFicheiro(detalhe As mbFichRef) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsRef As New ADODB.recordset
    MB_PreencheDadosUtenteFicheiro = False
    
    sSql = "SELECT x1.dt_cri, x1.flg_estado, x2.seq_utente, x2.nome_ute, x2.t_utente, x2.utente, x1.seq_ref_emitida, x1.val_pag FROM sl_mb_ref_emitida x1, sl_identif x2 WHERe x1.seq_utente = x2.seq_utente "
    sSql = sSql & " AND cod_ent_ref = " & BL_TrataStringParaBD(CStr(gEntidadeMultibanco)) & " aND cod_ref_pag = " & BL_TrataStringParaBD(detalhe.EPS_REF)
    RsRef.CursorType = adOpenKeyset
    RsRef.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRef.Open sSql, gConexao
    If RsRef.RecordCount = 1 Then
        detalhe.seq_utente = BL_HandleNull(RsRef!seq_utente, "")
        detalhe.nome_ute = BL_HandleNull(RsRef!nome_ute, "")
        detalhe.t_utente = BL_HandleNull(RsRef!t_utente, "")
        detalhe.Utente = BL_HandleNull(RsRef!Utente, "")
        detalhe.dt_ref = BL_HandleNull(RsRef!dt_cri, "")
        detalhe.estado_ref = BL_HandleNull(RsRef!flg_estado, "")
        detalhe.seq_ref_emitida = BL_HandleNull(RsRef!seq_ref_emitida, "")
        detalhe.valor_ref = BL_HandleNull(RsRef!val_pag, 0)
    Else
        detalhe.seq_utente = ""
        detalhe.nome_ute = ""
        detalhe.t_utente = ""
        detalhe.Utente = ""
        detalhe.dt_ref = ""
        detalhe.estado_ref = ""
        detalhe.valor_ref = 0
    End If
    RsRef.Close
    Set RsRef = Nothing
    MB_PreencheDadosUtenteFicheiro = True
Exit Function
TrataErro:
    MB_PreencheDadosUtenteFicheiro = False
    BG_LogFile_Erros "MB_PreencheDadosUtenteFicheiro: " & Err.Number & " - " & Err.Description, "MB", "MB_PreencheDadosUtenteFicheiro"
    Exit Function
    Resume Next
End Function

Public Function MB_ProcessaFicheiro(ficheiro As mbFicheiro) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim sSql As String
    MB_ProcessaFicheiro = False
    For i = 1 To ficheiro.totalRef
        If ficheiro.referencias(i).seq_ref_emitida = "" Then
            gMsgTitulo = " Ficheiro"
            gMsgMsg = "Existem refer�ncias sem Utente associado. Pretende continuar? "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbNo Then
                MB_ProcessaFicheiro = True
                Exit Function
            Else
                Exit For
            End If
        End If
    Next i
    For i = 1 To ficheiro.totalRef
        If ficheiro.referencias(i).seq_ref_emitida <> "" And ficheiro.referencias(i).estado_ref = "0" Then
            sSql = "UPDATE sl_mb_ref_emitida set user_conc = " & CStr(gCodUtilizador) & ", "
            sSql = sSql & " dt_conc = sysdate " & ","
            sSql = sSql & " dt_pagamento = " & BL_TrataDataParaBD(Mid(ficheiro.referencias(i).SIS_DTHN01, 7, 2) & "-" & Mid(ficheiro.referencias(i).SIS_DTHN01, 5, 2) & "-" & Mid(ficheiro.referencias(i).SIS_DTHN01, 1, 4) & " " & Mid(ficheiro.referencias(i).SIS_DTHN01, 9, 2) & ":" & Mid(ficheiro.referencias(i).SIS_DTHN01, 11, 2)) & ", "
            sSql = sSql & " seq_fich_conc = " & ficheiro.seq_ficheiro & ", "
            sSql = sSql & " cod_motivo_conc = 0 , "
            sSql = sSql & " flg_estado = 1, "
            sSql = sSql & " valor_ficheiro = " & Replace(ficheiro.referencias(i).valor_ficheiro, ",", ".") & ","
            sSql = sSql & " linha_ficheiro = " & BL_TrataStringParaBD(ficheiro.referencias(i).linha_ficheiro)
            sSql = sSql & " WHERE seq_ref_emitida = " & ficheiro.referencias(i).seq_ref_emitida
            If BG_ExecutaQuery_ADO(sSql) <> 1 Then
                GoTo TrataErro
            End If
            ficheiro.referencias(i).estado_ref = mediSim
        End If
        
        ' PROCESSA ERROS
        If ficheiro.referencias(i).seq_ref_emitida <> "" And ficheiro.referencias(i).estado_ref <> "0" Then
            MB_ReportaErroFicheiro ficheiro, i, "Refer�ncia j� paga anteriormente"
        End If
        If ficheiro.referencias(i).valor_ficheiro <> ficheiro.referencias(i).valor_ref Then
            MB_ReportaErroFicheiro ficheiro, i, "Valor do ficheiro diferente do valor da refer�ncia emitida."
        End If
    Next i
    
    sSql = "UPDATE sl_mb_ficheiros_mov set flg_tratado =1 WHERE seq_ficheiro_mov =  " & ficheiro.seq_ficheiro
    If BG_ExecutaQuery_ADO(sSql) <> 1 Then
        GoTo TrataErro
    End If
    MB_ProcessaFicheiro = True
Exit Function
TrataErro:
    MB_ProcessaFicheiro = False
    BG_LogFile_Erros "MB_ProcessaFicheiro: " & Err.Number & " - " & Err.Description, "MB", "MB_ProcessaFicheiro"
    Exit Function
    Resume Next
End Function

Public Function MB_FechoReferenciaManual(cod_motivo_conc As Integer, seq_ref_emitida As Long) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = "UPDATE sl_mb_ref_emitida set user_conc = " & CStr(gCodUtilizador) & ", "
    sSql = sSql & " dt_conc = sysdate " & ","
    sSql = sSql & " dt_pagamento = NULL, "
    sSql = sSql & " seq_fich_conc = NULL , "
    sSql = sSql & " cod_motivo_conc =  " & cod_motivo_conc & ","
    sSql = sSql & " flg_estado = 1 WHERE seq_ref_emitida = " & seq_ref_emitida
    If BG_ExecutaQuery_ADO(sSql) <> 1 Then
        GoTo TrataErro
    End If
    
    MB_FechoReferenciaManual = True
Exit Function
TrataErro:
    MB_FechoReferenciaManual = False
    BG_LogFile_Erros "MB_FechoReferenciaManual: " & Err.Number & " - " & Err.Description, "MB", "MB_FechoReferenciaManual"
    Exit Function
    Resume Next
End Function

Public Function MB_VerificaFicheiroAnteriorTratado(fic_seq_ult As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsRef As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_mb_ficheiros_mov  WHERe fic_seq =  " & BL_TrataStringParaBD(fic_seq_ult) & " AND flg_tratado = 1"
    RsRef.CursorType = adOpenKeyset
    RsRef.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRef.Open sSql, gConexao
    If RsRef.RecordCount >= 1 Then
        MB_VerificaFicheiroAnteriorTratado = True
    Else
        MB_VerificaFicheiroAnteriorTratado = False
    End If
    RsRef.Close
    Set RsRef = Nothing
Exit Function
TrataErro:
    MB_VerificaFicheiroAnteriorTratado = False
    BG_LogFile_Erros "MB_VerificaFicheiroAnteriorTratado: " & Err.Number & " - " & Err.Description, "MB", "MB_VerificaFicheiroAnteriorTratado"
    Exit Function
    Resume Next
End Function

Public Function MB_VerificaFicheiroTratado(fic_seq As String, fic_seq_ult As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsRef As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_mb_ficheiros_mov  WHERe fic_seq =  " & BL_TrataStringParaBD(fic_seq)
    sSql = sSql & " AND fic_seq_ult =  " & BL_TrataStringParaBD(fic_seq_ult) & " AND flg_tratado = 1"
    RsRef.CursorType = adOpenKeyset
    RsRef.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRef.Open sSql, gConexao
    If RsRef.RecordCount >= 1 Then
        gMsgTitulo = " Ficheiro"
        gMsgMsg = "Ficheiro em causa j� foi tratado anteriormente. Pretende continuar? "
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbNo Then
            MB_VerificaFicheiroTratado = False
        Else
            MB_VerificaFicheiroTratado = True
        End If
    Else
        MB_VerificaFicheiroTratado = True
    End If
    RsRef.Close
    Set RsRef = Nothing
Exit Function
TrataErro:
    MB_VerificaFicheiroTratado = False
    BG_LogFile_Erros "MB_VerificaFicheiroTratado: " & Err.Number & " - " & Err.Description, "MB", "MB_VerificaFicheiroTratado"
    Exit Function
    Resume Next
End Function


Public Function MB_RegistaImpressaoDocumento(versao As Integer, seq_ref_emitida As Long, texto As String) As Long
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsFich As New ADODB.recordset
    Dim seq_emissao_doc As Long
    MB_RegistaImpressaoDocumento = mediComboValorNull

    sSql = "SELECT seq_emissao_doc.nextval proximo FROM dual "
    rsFich.CursorType = adOpenKeyset
    rsFich.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFich.Open sSql, gConexao
    If rsFich.RecordCount = 1 Then
        seq_emissao_doc = BL_HandleNull(rsFich!proximo, mediComboValorNull)
    Else
        seq_emissao_doc = mediComboValorNull
    End If
    rsFich.Close
    
    If seq_emissao_doc > mediComboValorNull Then
        sSql = "INSERT INTO sl_mb_emissao_doc (seq_emissao_doc, seq_ref_emitida, versao_documento, user_cri, dt_cri, texto) VALUES("
        sSql = sSql & seq_emissao_doc & "," & seq_ref_emitida & "," & versao & ", " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
        sSql = sSql & "sysdate,"
        sSql = sSql & BL_TrataStringParaBD(texto) & ")"
        BG_ExecutaQuery_ADO sSql
        MB_RegistaImpressaoDocumento = seq_emissao_doc
    End If
Exit Function
TrataErro:
    MB_RegistaImpressaoDocumento = mediComboValorNull
    BG_LogFile_Erros "MB_RegistaImpressaoDocumento: " & Err.Number & " - " & Err.Description, "MB", "MB_RegistaImpressaoDocumento"
    Exit Function
    Resume Next
End Function

Public Function MB_CarregaImpressaoDocumento(referencia As mbRefEmitidas) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsDoc As New ADODB.recordset
    MB_CarregaImpressaoDocumento = False
    
    sSql = "select * from sl_mb_emissao_doc where versao_documento = 1 and seq_ref_emitida = " & referencia.seq_ref_emitida & " ORDER BY dt_cri desc "
    RsDoc.CursorType = adOpenKeyset
    RsDoc.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsDoc.Open sSql, gConexao
    If RsDoc.RecordCount >= 1 Then
        referencia.user_doc = BL_HandleNull(RsDoc!user_cri, "")
        referencia.dt_doc = BL_HandleNull(RsDoc!dt_cri, "")
    Else
        referencia.user_doc = ""
        referencia.dt_doc = ""
    End If
    RsDoc.Close
    If referencia.user_conc <> "" Then
        sSql = "select * from sl_mb_emissao_doc where versao_documento = 2 and seq_ref_emitida = " & referencia.seq_ref_emitida & " ORDER BY dt_cri desc "
        RsDoc.CursorType = adOpenKeyset
        RsDoc.LockType = adLockOptimistic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsDoc.Open sSql, gConexao
        If RsDoc.RecordCount >= 1 Then
            referencia.user_doc2 = BL_HandleNull(RsDoc!user_cri, "")
            referencia.dt_doc2 = BL_HandleNull(RsDoc!dt_cri, "")
        Else
            referencia.user_doc2 = ""
            referencia.dt_doc2 = ""
        End If
        RsDoc.Close
    End If
    MB_CarregaImpressaoDocumento = True
Exit Function
TrataErro:
    MB_CarregaImpressaoDocumento = False
    BG_LogFile_Erros "MB_CarregaImpressaoDocumento: " & Err.Number & " - " & Err.Description, "MB", "MB_CarregaImpressaoDocumento"
    Exit Function
    Resume Next
End Function

Private Function MB_ReportaErroFicheiro(ficheiro As mbFicheiro, indice As Integer, descr_erro As String) As Boolean
    On Error GoTo TrataErro
    Dim seq_ficheiro_erro As Long
    Dim sSql As String
    Dim rsFich  As New ADODB.recordset
    
    sSql = "SELECT seq_ficheiro_erro.NextVal proximo FROM dual "
    rsFich.CursorType = adOpenKeyset
    rsFich.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFich.Open sSql, gConexao
    If rsFich.RecordCount = 1 Then
        seq_ficheiro_erro = BL_HandleNull(rsFich!proximo, mediComboValorNull)
    Else
        seq_ficheiro_erro = mediComboValorNull
    End If
    
    If seq_ficheiro_erro > mediComboValorNull Then
        

        sSql = "INSERT INTO sl_mb_ficheiros_erros (seq_ficheiro_erro, seq_ficheiro_mov, seq_ref_emitida,val_ficheiro, dt_cri, user_cri,descr_erro) "
        sSql = sSql & " VALUES(" & seq_ficheiro_erro & ", " & ficheiro.seq_ficheiro & ", " & ficheiro.referencias(indice).seq_ref_emitida & ", "
        sSql = sSql & Replace(ficheiro.referencias(indice).valor_ficheiro, ",", ".") & ", sysdate, " & BL_TrataStringParaBD(CStr(gCodUtilizador))
        sSql = sSql & "," & BL_TrataStringParaBD(descr_erro) & ")"
        BG_ExecutaQuery_ADO sSql
        MB_ReportaErroFicheiro = True
    End If
    rsFich.Close
Exit Function
TrataErro:
    MB_ReportaErroFicheiro = False
    BG_LogFile_Erros "MB_ReportaErroFicheiro: " & Err.Number & " - " & Err.Description, "MB", "MB_ReportaErroFicheiro"
    Exit Function
    Resume Next
End Function

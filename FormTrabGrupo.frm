VERSION 5.00
Begin VB.Form FormFolhasTrabGrupo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormTrabUtente"
   ClientHeight    =   5145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6285
   Icon            =   "FormTrabGrupo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5145
   ScaleWidth      =   6285
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodGrupo 
      Height          =   285
      Left            =   1920
      TabIndex        =   26
      Top             =   6000
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa "
      Height          =   2895
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   6015
      Begin VB.ListBox EcListaLocalEntrega 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1200
         TabIndex        =   28
         Top             =   2160
         Width           =   4215
      End
      Begin VB.CommandButton BtPesquisaLocalEntrega 
         Height          =   315
         Left            =   5400
         Picture         =   "FormTrabGrupo.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   2160
         Width           =   375
      End
      Begin VB.ComboBox CbGrupo 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   2895
      End
      Begin VB.TextBox EcCodProveniencia 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1200
         TabIndex        =   11
         Top             =   1800
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   1800
         Width           =   3495
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   375
         Left            =   5400
         Picture         =   "FormTrabGrupo.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Pesquisa R�pida de Utentes"
         Top             =   1800
         Width           =   375
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1080
         Width           =   1335
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox EcDtFim 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2760
         TabIndex        =   5
         Top             =   720
         Width           =   1335
      End
      Begin VB.TextBox EcDtIni 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Local Entrega"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   29
         Top             =   2160
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "&Grupo"
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label7 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "De"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "a"
         Height          =   255
         Left            =   2520
         TabIndex        =   4
         Top             =   720
         Width           =   255
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Modelo "
      Height          =   1815
      Left            =   120
      TabIndex        =   15
      Top             =   3120
      Width           =   6015
      Begin VB.CheckBox flg_inf_cli 
         Caption         =   "Informa��o Cl�nica"
         Height          =   285
         Left            =   4080
         TabIndex        =   21
         Top             =   600
         Width           =   1695
      End
      Begin VB.CheckBox flg_sexo 
         Caption         =   "Sexo do Utente"
         Height          =   285
         Left            =   2400
         TabIndex        =   20
         Top             =   600
         Width           =   1635
      End
      Begin VB.CheckBox flg_idade 
         Caption         =   "Idade do Utente"
         Height          =   285
         Left            =   2400
         TabIndex        =   17
         Top             =   270
         Width           =   1635
      End
      Begin VB.CheckBox flg_nome 
         Caption         =   "Nome do Utente"
         Height          =   285
         Left            =   4080
         TabIndex        =   18
         Top             =   240
         Width           =   1635
      End
      Begin VB.CheckBox flg_urgencia 
         Caption         =   "Urg�ncia na Requisi��o"
         Height          =   285
         Left            =   120
         TabIndex        =   19
         Top             =   600
         Width           =   2115
      End
      Begin VB.CheckBox flg_situacao 
         Caption         =   "Situa��o da Requisi��o"
         Height          =   285
         Left            =   120
         TabIndex        =   16
         Top             =   270
         Width           =   2115
      End
      Begin VB.CheckBox ChQuebra 
         Caption         =   "Uma Requisi��o por Folha"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   960
         Width           =   2295
      End
      Begin VB.TextBox EcNumUltRes 
         Height          =   285
         Left            =   3480
         TabIndex        =   24
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "&N�mero de resultados anteriores das an�lises:"
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   23
         Top             =   1320
         Width           =   3435
      End
   End
   Begin VB.Label Label5 
      Caption         =   "EcCodGrupo"
      Height          =   255
      Left            =   840
      TabIndex        =   25
      Top             =   6000
      Width           =   1095
   End
End
Attribute VB_Name = "FormFolhasTrabGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
        
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Public Sub FuncaoImprimir()
        
    Call Imprime_FolhaTrabalhoGrupo
    
End Sub

Sub LimpaCampos()

    Me.SetFocus
    
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    
    flg_idade.value = Unchecked
    flg_inf_cli.value = Unchecked
    flg_nome.value = Unchecked
    flg_sexo.value = Unchecked
    flg_situacao.value = Unchecked
    flg_urgencia.value = Unchecked
    
    ChQuebra.value = Unchecked
    
    EcNumUltRes.Text = ""
    
End Sub

Public Sub ImprimirVerAntes()
    
    Call Imprime_FolhaTrabalhoGrupo

End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Public Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Public Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
    
    EcNumUltRes.Tag = adInteger
    EcNumUltRes.MaxLength = 4
    
    EcCodProveniencia.MaxLength = 5
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10

    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormFolhasTrabGrupo = Nothing
    
    Call BL_FechaPreview("Mapa de Registo")
    
End Sub

Sub Inicializacoes()

    Me.caption = " Mapa de Registo Detalhado"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 6375
    Me.Height = 5145
    
    Call PreencheValoresDefeito
    
    Set CampoDeFocus = CbGrupo
    
End Sub

Private Sub Imprime_FolhaTrabalhoGrupo()
        
    Dim RegReq As ADODB.recordset
    Dim RegCmd As ADODB.recordset
    
    Dim CmdDescrAnalise As ADODB.Command
    Dim CmdDiagnP As ADODB.Command
    Dim CmdDiagnS As ADODB.Command
    Dim CmdTeraMed As ADODB.Command
    Dim CmdProd As ADODB.Command
    
    
    Dim sql As String
    Dim s As String
    Dim i As Integer
    Dim total As Integer
    
    Dim continua As Boolean
        
    'Dados para o Utente
    Dim nome_ute As String
    Dim sexo_ute As String
    Dim N_Utente As String
    Dim N_Processo As String
    Dim Idade_Ute As String
    Dim Diagn_P As String
    Dim Flag_Diagn_P As String
        
    'Dados para a Requisi��o
    Dim descr_t_sit As String
    Dim t_urg As String
    Dim descr_proven As String
    Dim Diagn_S As String
    Dim Tera_Med As String
    Dim Produtos As String
    Dim Flag_Tera_Med As String
    Dim Flag_Diagn_S As String
    Dim Flag_Produtos As String
    
    'Dados para as An�lises
    Dim Cod_Perfil As String
    Dim cod_ana_c As String
    Dim cod_ana_s As String
    Dim Descr_Perfil As String
    Dim Descr_Ana_C As String
    Dim descr_ana_s As String
    
    Dim ActualUtente As String
    Dim ActualReq As String
    Dim ActualAnalise As String
                
    Dim NumResAnt As Integer
        
    '1. Verifica se a Form Preview j� est� aberta
    If BL_PreviewAberto("Mapa de Registo") = True Then Exit Sub
        
    '2. Verifica Campos Obrigat�rios
    
'    If CbGrupo.ListIndex = -1 Then
'        Call BG_Mensagem(mediMsgBox, "Indique o Grupo de Trabalho.", vbOKOnly + vbExclamation, App.ProductName)
'        CbGrupo.SetFocus
'        Exit Sub
'    End If
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    Set RegReq = New ADODB.recordset
    RegReq.CursorLocation = adUseServer
    RegReq.CursorType = adOpenForwardOnly
    RegReq.LockType = adLockReadOnly
    RegReq.ActiveConnection = gConexao
    sql = "SELECT sl_requis.N_req,sl_requis.T_urg,sl_requis.seq_utente," & _
          "sl_tbf_t_sit.Descr_t_sit," & _
          "sl_Proven.Descr_proven," & _
          "sl_identif.Nome_ute,sl_identif.T_utente,sl_identif.utente,sl_identif.Dt_nasc_ute,sl_identif.n_proc_1,sl_identif.n_proc_2," & _
          "sl_tbf_sexo.descr_sexo," & _
          "sl_marcacoes.Cod_perfil,sl_marcacoes.Cod_ana_c,sl_marcacoes.Cod_ana_s,sl_marcacoes.Cod_Agrup,sl_marcacoes.Ord_Ana,sl_marcacoes.Ord_Ana_Perf,sl_marcacoes.Ord_Ana_Compl, sl_marcacoes.ord_marca " & _
          "FROM sl_identif,sl_requis,sl_marcacoes,sl_tbf_t_urg, " & _
          IIf(gSGBD = gInformix, "OUTER sl_tbf_sexo,OUTER sl_proven, OUTER sl_tbf_t_sit ", "sl_tbf_sexo,sl_proven, sl_tbf_t_sit ") & _
          "WHERE " & _
          " sl_identif.Seq_utente=sl_requis.Seq_utente " & _
          " AND sl_requis.N_req=sl_marcacoes.N_req AND sl_requis.t_urg = sl_tbf_t_urg.cod_t_urg AND "
          Select Case gSGBD
              Case gOracle
                  sql = sql & "sl_requis.Cod_proven=sl_proven.Cod_proven(+) AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo(+) AND sl_requis.t_sit = sl_tbf_t_sit.cod_t_sit(+) "
              Case gInformix
                  sql = sql & "sl_requis.Cod_proven=sl_proven.Cod_proven AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo AND sl_requis.t_sit = sl_tbf_t_sit.cod_T_sit "
              Case gSqlServer
                  sql = sql & "sl_requis.Cod_proven*=sl_proven.Cod_proven AND sl_identif.sexo_ute*=sl_tbf_sexo.cod_sexo AND sl_requis.t_sit*= sl_tbf_t_sit.cod_t_sit "
          End Select
    'S� interessam as requisi��es cujas an�lises (pai) sejam as do grupo de an�lises especificado.
    If EcCodGrupo.Text <> "" Then
        sql = sql & " AND sl_marcacoes.Cod_agrup IN (SELECT Cod_ana FROM slv_analises WHERE cod_gr_ana=" & BL_TrataStringParaBD(EcCodGrupo.Text) & " ) "
    End If
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_urg=" & CbUrgencia.ListIndex
    End If
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If EcCodProveniencia.Text <> "" Then
        sql = sql & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    'Tipo de Situa��o preenchido?
    If (CbSituacao.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_sit=" & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    'S� interessam as an�lises filtradas das requisi��es que sejam poss�veis de iniciar (Data e Produtos Chegados!!)
    sql = sql & " AND sl_marcacoes.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
    
    'SE FOI INDICADO ALGUM local de entrega
    If EcListaLocalEntrega.ListCount > 0 Then
        sql = sql & " AND sl_requis.cod_local_entrega IN ("
        For i = 0 To EcListaLocalEntrega.ListCount - 1
            sql = sql & EcListaLocalEntrega.ItemData(i) & ", "
        Next i
        sql = Mid(sql, 1, Len(sql) - 2) & ") "
    End If
    If gLAB = "LJM" Then
    
    sql = sql & " UNION " & _
          "SELECT sl_requis.N_req,sl_requis.T_urg,sl_requis.seq_utente seq_utente," & _
          "sl_tbf_t_sit.Descr_t_sit," & _
          "sl_Proven.Descr_proven," & _
          "sl_identif.Nome_ute,sl_identif.T_utente,sl_identif.utente,sl_identif.Dt_nasc_ute,sl_identif.n_proc_1,sl_identif.n_proc_2," & _
          "sl_tbf_sexo.descr_sexo," & _
          "sl_realiza.Cod_perfil,sl_realiza.Cod_ana_c,sl_realiza.Cod_ana_s,sl_realiza.Cod_Agrup,sl_realiza.Ord_Ana,sl_realiza.Ord_Ana_Perf,sl_realiza.Ord_Ana_Compl, sl_realiza.ord_marca " & _
          "FROM sl_identif,sl_requis,sl_realiza,sl_tbf_t_urg, " & _
          IIf(gSGBD = gInformix, "OUTER sl_tbf_sexo,OUTER sl_proven, OUTER sl_tbf_t_sit ", "sl_tbf_sexo,sl_proven, sl_tbf_t_sit ") & _
          "WHERE " & _
          " sl_identif.Seq_utente=sl_requis.Seq_utente " & _
          " AND sl_requis.N_req=sl_realiza.N_req AND sl_requis.t_urg = sl_tbf_t_urg.cod_t_urg AND "
          Select Case gSGBD
              Case gOracle
                  sql = sql & "sl_requis.Cod_proven=sl_proven.Cod_proven(+) AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo(+) AND sl_requis.t_sit = sl_tbf_t_sit.cod_t_sit(+) "
              Case gInformix
                  sql = sql & "sl_requis.Cod_proven=sl_proven.Cod_proven AND sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo AND sl_requis.t_sit = sl_tbf_t_sit.cod_T_sit "
              Case gSqlServer
                  sql = sql & "sl_requis.Cod_proven*=sl_proven.Cod_proven AND sl_identif.sexo_ute*=sl_tbf_sexo.cod_sexo AND sl_requis.t_sit*= sl_tbf_t_sit.cod_t_sit "
          End Select
    'S� interessam as requisi��es cujas an�lises (pai) sejam as do grupo de an�lises especificado.
    If EcCodGrupo.Text <> "" Then
        sql = sql & " AND sl_realiza.Cod_agrup IN (SELECT Cod_ana FROM slv_analises WHERE cod_gr_ana=" & BL_TrataStringParaBD(EcCodGrupo.Text) & " ) "
    End If
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_urg=" & CbUrgencia.ListIndex
    End If
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If EcCodProveniencia.Text <> "" Then
        sql = sql & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    'Tipo de Situa��o preenchido?
    If (CbSituacao.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_sit=" & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    'S� interessam as an�lises filtradas das requisi��es que sejam poss�veis de iniciar (Data e Produtos Chegados!!)
    sql = sql & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
    
    End If
    'Crit�rio de Ordena��o
    sql = sql & " ORDER by sl_identif.seq_utente,sl_requis.N_req, ord_marca, ord_ana, Cod_Agrup,cod_perfil, ord_ana_perf, cod_ana_c, ord_ana_compl, cod_ana_s "
    RegReq.Source = sql
    RegReq.Open
    
    'Verifica se existem registos para imprimir
    total = RegReq.RecordCount
    If total = 0 Then
        RegReq.Close
        Set RegReq = Nothing
        Call BG_Mensagem(mediMsgBox, "N�o foram encontradas nenhumas an�lises para iniciar um mapa de registo detalhado!", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    End If
        
        
    'Verifica o n� de resultados anteriores a devolver
    If Trim(EcNumUltRes.Text) = "" Or Trim(EcNumUltRes.Text) = "0" Then
        NumResAnt = 0
    Else
        NumResAnt = Trim(EcNumUltRes.Text)
    End If
        
    If gImprimirDestino = 1 Then
        If NumResAnt = 0 Then
            continua = BL_IniciaReport("FolhasTrabalhoGrupo", "Mapa de Registo", crptToPrinter)
        Else
            continua = BL_IniciaReport("FolhasTrabalhoGrupoHist", "Mapa de Registo", crptToPrinter)
        End If
    Else
        If NumResAnt = 0 Then
            continua = BL_IniciaReport("FolhasTrabalhoGrupo", "Mapa de Registo", crptToWindow)
        Else
            continua = BL_IniciaReport("FolhasTrabalhoGrupoHist", "Mapa de Registo", crptToWindow)
        End If
    End If
    If continua = False Then
        RegReq.Close
        Set RegReq = Nothing
        Exit Sub
    End If
    
    
    '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    'Cria as Tabelas
    Call CriaTabelas
    
    
    'DEFINI��O DE COMANDOS:
        
    'Define o Comando das descri��es das an�lises
    Set CmdDescrAnalise = New ADODB.Command
    CmdDescrAnalise.ActiveConnection = gConexao
    CmdDescrAnalise.CommandType = adCmdText
    CmdDescrAnalise.Prepared = True
    CmdDescrAnalise.CommandText = "SELECT descr_ana,abr_ana FROM slv_analises WHERE cod_ana=?"
    CmdDescrAnalise.Parameters.Append CmdDescrAnalise.CreateParameter("COD_ANA", adVarChar, adParamInput, 8)
        
    'Define o Comando para obter os produtos das an�lises para cada requisi��o
    Set CmdProd = New ADODB.Command
    CmdProd.ActiveConnection = gConexao
    CmdProd.CommandType = adCmdText
    s = "SELECT sl_produto.cod_produto,sl_produto.descr_produto,sl_especif.descr_especif FROM sl_produto,sl_req_prod," & IIf(gSGBD = gInformix, "OUTER sl_especif", " sl_especif") & _
          " WHERE sl_req_prod.n_req=? AND sl_produto.cod_produto=sl_req_prod.cod_prod "
    Select Case gSGBD
        Case gOracle
            s = s & "AND sl_produto.cod_especif=sl_especif.cod_especif(+) "
        Case gInformix
            s = s & "AND sl_produto.cod_especif=sl_especif.cod_especif "
        Case gSqlServer
            s = s & "AND sl_produto.cod_especif*=sl_especif.cod_especif "
    End Select
    CmdProd.CommandText = s
    CmdProd.Prepared = True
    CmdProd.Parameters.Append CmdProd.CreateParameter("N_REQ", adInteger, adParamInput, 7)
    
    'Define o Comando dos Diagn�sticos Principais
    Set CmdDiagnP = New ADODB.Command
    CmdDiagnP.ActiveConnection = gConexao
    CmdDiagnP.CommandType = adCmdText
    CmdDiagnP.Prepared = True
    CmdDiagnP.CommandText = "SELECT Descr_Diag " & _
                            "FROM sl_diag,sl_diag_pri " & _
                            "WHERE sl_diag.Cod_diag=sl_diag_pri.Cod_diag AND " & _
                            "sl_diag_pri.seq_utente=?"
    CmdDiagnP.Parameters.Append CmdDiagnP.CreateParameter("SEQ_UTENTE", adInteger, adParamInput, 20)
    
    'Define o Comando da Terap�utica e Medica��o
    Set CmdTeraMed = New ADODB.Command
    CmdTeraMed.ActiveConnection = gConexao
    CmdTeraMed.CommandType = adCmdText
    CmdTeraMed.Prepared = True
    CmdTeraMed.CommandText = "SELECT Descr_tera_med FROM sl_tera_med,sl_tm_ute " & _
                             "WHERE sl_tera_med.Cod_tera_med =sl_tm_ute.Cod_tera_med AND " & _
                             "sl_tm_ute.N_Req=?"
    CmdTeraMed.Parameters.Append CmdTeraMed.CreateParameter("N_REQ", adInteger, adParamInput, 7)
    
    'Define o Comando dos Diagn�sticos Secund�rios
    'Para os Diagn�sticos Secund�rios
    Set CmdDiagnS = New ADODB.Command
    CmdDiagnS.ActiveConnection = gConexao
    CmdDiagnS.CommandType = adCmdText
    CmdDiagnS.Prepared = True
    CmdDiagnS.CommandText = "SELECT Descr_Diag " & _
                            "FROM sl_diag,sl_diag_sec " & _
                            "WHERE sl_diag.Cod_diag=sl_diag_sec.Cod_diag AND " & _
                            "sl_diag_sec.N_req=?"
    CmdDiagnS.Parameters.Append CmdDiagnS.CreateParameter("N_REQ", adInteger, adParamInput, 7)
    
    '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    ActualUtente = ""
    ActualReq = ""
    ActualAnalise = ""
    RegReq.MoveFirst
    For i = 1 To total
        'DADOS PARA A TABELA SL_CR_MREGUTE
        
        'MUDOU DE UTENTE?
        If "" & RegReq!seq_utente <> ActualUtente Then
            ActualUtente = "" & RegReq!seq_utente
            
            'Nome do Utente
            nome_ute = ""
            If flg_nome.value = Checked Then
                nome_ute = "" & RegReq!nome_ute
            End If
            If nome_ute = "" Then
                nome_ute = "Null"
            Else
                nome_ute = BL_TrataStringParaBD(nome_ute)
            End If
            
            'Idade do Utente
            Idade_Ute = ""
            If (flg_idade.value = Checked) And (Not BL_HandleNull(RegReq!dt_nasc_ute, "") = "") Then
                Idade_Ute = BG_CalculaIdade("" & RegReq!dt_nasc_ute)
            End If
            If Idade_Ute = "" Then
                Idade_Ute = "Null"
            End If
            
            'Sexo do Utente
            sexo_ute = ""
            If flg_sexo.value = Checked Then
                sexo_ute = "" & RegReq!descr_sexo
            End If
            If sexo_ute = "" Then
                sexo_ute = "Null"
            Else
                sexo_ute = BL_TrataStringParaBD(sexo_ute)
            End If
            
            'Tipo de Utente
            N_Utente = "" & RegReq!t_utente & "\" & RegReq!Utente
            If N_Utente = "" Then
                N_Utente = "Null"
            Else
                N_Utente = BL_TrataStringParaBD(N_Utente)
            End If
            
            'N� dos Processos
            If BL_HandleNull(RegReq!n_proc_1, "") = "" Then
                If BL_HandleNull(RegReq!n_proc_2, "") = "" Then
                    N_Processo = "Null"
                Else
                    N_Processo = BL_TrataStringParaBD(RegReq!n_proc_2)
                End If
            Else
                If BL_HandleNull(RegReq!n_proc_2, "") = "" Then
                    N_Processo = BL_TrataStringParaBD(RegReq!n_proc_1)
                Else
                    N_Processo = BL_TrataStringParaBD(RegReq!n_proc_1 & "\" & RegReq!n_proc_2)
                End If
            End If
            
            'Diagn�sticos Principais
            Diagn_P = ""
            If flg_inf_cli.value = Checked Then
                CmdDiagnP.Parameters("SEQ_UTENTE").value = "" & RegReq!seq_utente
                Set RegCmd = CmdDiagnP.Execute
                While Not RegCmd.EOF
                    Diagn_P = Diagn_P & Trim(RegCmd!Descr_Diag) & ","
                    RegCmd.MoveNext
                Wend
                Diagn_P = left(Diagn_P, Abs(Len(Diagn_P) - 1))
                RegCmd.Close
                Set RegCmd = Nothing
            End If
            If Diagn_P = "" Then
                Diagn_P = "Null"
                Flag_Diagn_P = "Null"
            Else
                Diagn_P = BL_TrataStringParaBD(Diagn_P)
                Flag_Diagn_P = BL_TrataStringParaBD("S")
            End If
                        
            sql = "INSERT INTO " & "SL_CR_MREGUTE" & gNumeroSessao & _
                         " (SEQ_UTENTE,NOME_UTE,SEXO_UTE,N_UTENTE,N_PROCESSO,IDADE_UTE,DIAGN_P,FLAG_DIAGN_P)" & _
                        " VALUES (" & RegReq!seq_utente & "," & nome_ute & "," & sexo_ute & "," & N_Utente & "," & N_Processo & ",'" & Idade_Ute & "'," & Diagn_P & "," & Flag_Diagn_P & ")"
            BG_ExecutaQuery_ADO (sql)
        End If
            
        'DADOS PARA A TABELA SL_CR_MREGREQ
        
        'MUDOU DE REQUISI��O?
        If "" & RegReq!n_req <> ActualReq Then
            ActualReq = "" & RegReq!n_req
            
            'Informa��o Cl�nica
            Tera_Med = ""
            Diagn_S = ""
            
            If flg_inf_cli.value = Checked Then
                'Terap�uticas e Medica��o
                CmdTeraMed.Parameters("N_REQ").value = "" & RegReq!n_req
                Set RegCmd = CmdTeraMed.Execute
                While Not RegCmd.EOF
                    Tera_Med = Tera_Med & Trim(RegCmd!descr_tera_med)
                    RegCmd.MoveNext
                Wend
                Tera_Med = left(Tera_Med, Abs(Len(Tera_Med) - 1))
                RegCmd.Close
                Set RegCmd = Nothing
            
                'Para os diagn�sticos Secund�rios
                CmdDiagnS.Parameters("N_REQ").value = "" & RegReq!n_req
                Set RegCmd = CmdDiagnS.Execute
                While Not RegCmd.EOF
                    Diagn_S = Diagn_S & Trim(RegCmd!Descr_Diag)
                    RegCmd.MoveNext
                Wend
                Diagn_S = left(Diagn_S, Abs(Len(Diagn_S) - 1))
                RegCmd.Close
                Set RegCmd = Nothing
            End If
            If Tera_Med = "" Then
                Tera_Med = "Null"
                Flag_Tera_Med = "Null"
            Else
                Tera_Med = BL_TrataStringParaBD(Tera_Med)
                Flag_Tera_Med = BL_TrataStringParaBD("S")
            End If
            If Diagn_S = "" Then
                Diagn_S = "Null"
                Flag_Diagn_S = "Null"
            Else
                Diagn_S = BL_TrataStringParaBD(Diagn_S)
                Flag_Diagn_S = BL_TrataStringParaBD("S")
            End If
                
            'Situa��o da Requisi��o
            descr_t_sit = ""
            If flg_situacao.value = Checked Then
                descr_t_sit = "" & RegReq!descr_t_sit
            End If
            If descr_t_sit = "" Then
                descr_t_sit = "Null"
            Else
                descr_t_sit = BL_TrataStringParaBD(descr_t_sit)
            End If
            
            'Urg�ncia da Requisi��o
            t_urg = ""
            If flg_urgencia.value = Checked Then
                t_urg = "" & RegReq!t_urg
            End If
            If t_urg = "" Then
                t_urg = "Null"
            Else
                t_urg = BL_TrataStringParaBD(t_urg)
            End If
            
            'Proveni�ncia da Requisi��o
            descr_proven = "" & RegReq!descr_proven
            If descr_proven = "" Then
                descr_proven = "Null"
            Else
                descr_proven = BL_TrataStringParaBD(descr_proven)
            End If
                
            'PRODUTOS
            'Produtos da requisi��o
            Produtos = ""
            CmdProd.Parameters("N_REQ").value = RegReq!n_req
            Set RegCmd = CmdProd.Execute
            While Not RegCmd.EOF
                Produtos = Produtos & RegCmd!descr_produto & " " & RegCmd!descr_especif & ", "
                RegCmd.MoveNext
            Wend
            If Len(Produtos) <> 0 Then
                Produtos = left(Produtos, Len(Produtos) - 2) & "."
            End If
            RegCmd.Close
            Set RegCmd = Nothing
            If Produtos = "" Then
                Produtos = "Null"
                Flag_Produtos = "Null"
            Else
                Produtos = BL_TrataStringParaBD(Produtos)
                Flag_Produtos = BL_TrataStringParaBD("S")
            End If
            
            sql = "INSERT INTO " & "SL_CR_MREGREQ" & gNumeroSessao & _
                         " (SEQ_UTENTE,N_REQ,DESCR_T_SIT,T_URG,DESCR_PROVEN,DIAGN_S,TERA_MED,PRODUTOS,FLAG_TERA_MED,FLAG_DIAGN_S,FLAG_PRODUTOS) " & _
                         " VALUES (" & RegReq!seq_utente & "," & RegReq!n_req & "," & descr_t_sit & "," & t_urg & "," & descr_proven & "," & Diagn_S & "," & Tera_Med & "," & Produtos & "," & Flag_Tera_Med & "," & Flag_Diagn_S & "," & Flag_Produtos & ")"
            BG_ExecutaQuery_ADO (sql)
        End If
        
        
        'DADOS PARA A TABELA SL_CR_MREGANA
        
        'MUDOU DE AN�LISE?
        If "" & RegReq!n_req & "." & RegReq!Cod_Perfil & "." & RegReq!cod_ana_c & "." & RegReq!cod_ana_s <> ActualAnalise Then
            ActualAnalise = "" & RegReq!n_req & "." & RegReq!Cod_Perfil & "." & RegReq!cod_ana_c & "." & RegReq!cod_ana_s
            
            'Perfil
            Cod_Perfil = "" & RegReq!Cod_Perfil
            Descr_Perfil = ""
            If Cod_Perfil <> "0" Then
                CmdDescrAnalise.Parameters("COD_ANA").value = RegReq!Cod_Perfil
                Set RegCmd = CmdDescrAnalise.Execute
                If Not RegCmd.EOF Then
                    Descr_Perfil = "" & RegCmd!descr_ana
                End If
                RegCmd.Close
                Set RegCmd = Nothing
            End If
            If Descr_Perfil = "" Then
                Descr_Perfil = "Null"
            Else
                Descr_Perfil = BL_TrataStringParaBD(Descr_Perfil)
            End If
            Cod_Perfil = BL_TrataStringParaBD(Cod_Perfil)
            
            'Complexa
            cod_ana_c = "" & RegReq!cod_ana_c
            Descr_Ana_C = ""
            If cod_ana_c <> "0" Then
                CmdDescrAnalise.Parameters("COD_ANA").value = RegReq!cod_ana_c
                Set RegCmd = CmdDescrAnalise.Execute
                If Not RegCmd.EOF Then
                    Descr_Ana_C = "" & RegCmd!descr_ana
                End If
                RegCmd.Close
                Set RegCmd = Nothing
            End If
            If Descr_Ana_C = "" Then
                Descr_Ana_C = "Null"
            Else
                Descr_Ana_C = BL_TrataStringParaBD(Descr_Ana_C)
            End If
            cod_ana_c = BL_TrataStringParaBD(cod_ana_c)
            
            'Simples
            cod_ana_s = "" & RegReq!cod_ana_s
            If cod_ana_s = gGHOSTMEMBER_S Then
                cod_ana_s = "0"
            End If
            descr_ana_s = ""
            If cod_ana_s <> "0" Then
                CmdDescrAnalise.Parameters("COD_ANA").value = "" & RegReq!cod_ana_s
                Set RegCmd = CmdDescrAnalise.Execute
                If Not RegCmd.EOF Then
                    descr_ana_s = "" & RegCmd!descr_ana
                End If
                RegCmd.Close
                Set RegCmd = Nothing
            End If
            If descr_ana_s = "" Then
                descr_ana_s = "Null"
            Else
                descr_ana_s = BL_TrataStringParaBD(descr_ana_s)
            End If
            cod_ana_s = BL_TrataStringParaBD(cod_ana_s)
            
            
            sql = "INSERT INTO " & "SL_CR_MREGANA" & gNumeroSessao & _
                         " (SEQ_UTENTE,N_REQ,COD_PERFIL,COD_ANA_C,COD_ANA_S,DESCR_PERFIL,DESCR_ANA_C,DESCR_ANA_S) " & _
                         " VALUES (" & RegReq!seq_utente & "," & RegReq!n_req & "," & Cod_Perfil & "," & cod_ana_c & "," & cod_ana_s & "," & Descr_Perfil & "," & Descr_Ana_C & "," & descr_ana_s & ")"
            BG_ExecutaQuery_ADO (sql)
        End If
        
        RegReq.MoveNext
    Next i
'
'    'Atribui o resto dos dados ao Report=>F�rmulas e Queries
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    'F�rmulas do Report
    Report.formulas(0) = "NomeGrupo=" & BL_TrataStringParaBD(CbGrupo.Text)
    If ChQuebra.value = Checked Then
        Report.formulas(1) = "QuebraReq='S'"
    Else
        Report.formulas(1) = "QuebraReq=''"
    End If
    
    If NumResAnt <> 0 Then
        sql = "SELECT " & _
              "SL_CR_MREGUTE.SEQ_UTENTE,SL_CR_MREGUTE.NOME_UTE, SL_CR_MREGUTE.SEXO_UTE, SL_CR_MREGUTE.N_UTENTE, SL_CR_MREGUTE.N_PROCESSO, SL_CR_MREGUTE.IDADE_UTE, SL_CR_MREGUTE.DIAGN_P, " & _
              "SL_CR_MREGREQ.N_REQ, SL_CR_MREGREQ.DESCR_T_SIT, SL_CR_MREGREQ.T_URG, SL_CR_MREGREQ.DESCR_PROVEN, SL_CR_MREGREQ.DIAGN_S, SL_CR_MREGREQ.TERA_MED, SL_CR_MREGREQ.PRODUTOS,SL_CR_MREGANA.COD_PERFIL, SL_CR_MREGANA.COD_ANA_C, SL_CR_MREGANA.COD_ANA_S, SL_CR_MREGANA.DESCR_PERFIL, SL_CR_MREGANA.DESCR_ANA_C, SL_CR_MREGANA.DESCR_ANA_S," & _
              "SLV_RESULT_ANT.N_REQ, SLV_RESULT_ANT.DT_VAL, SLV_RESULT_ANT.FLG_UNID_ACT1, SLV_RESULT_ANT.UNID_1_RES1, SLV_RESULT_ANT.UNID_2_RES1, SLV_RESULT_ANT.FLG_UNID_ACT2, SLV_RESULT_ANT.UNID_1_RES2, SLV_RESULT_ANT.UNID_2_RES2," & _
              "SLV_RESULT_VAL.SEQ_REALIZA , SLV_RESULT_VAL.N_Res, SLV_RESULT_VAL.ResNum, SLV_RESULT_VAL.RESFRASE, SLV_RESULT_VAL.QUANTMICRO, SLV_RESULT_VAL.DescrMicro, SLV_RESULT_VAL.Sensib, SLV_RESULT_VAL.CMI, SLV_RESULT_VAL.DescrAntib " & _
              "FROM " & _
              "SL_CR_MREGUTE" & gNumeroSessao & " SL_CR_MREGUTE," & _
              "SL_CR_MREGREQ" & gNumeroSessao & " SL_CR_MREGREQ," & _
              "SL_CR_MREGANA" & gNumeroSessao & " SL_CR_MREGANA," & _
              "SLV_RESULT_ANT SLV_RESULT_ANT, SLV_RESULT_VAL SLV_RESULT_VAL " & _
              "WHERE " & _
              "SL_CR_MREGUTE.SEQ_UTENTE = SL_CR_MREGREQ.SEQ_UTENTE AND " & _
              "SL_CR_MREGREQ.N_REQ = SL_CR_MREGANA.N_REQ AND "
              Select Case gSGBD
                Case gSqlServer
                    sql = sql & "SL_CR_MREGANA.SEQ_UTENTE *= SLV_RESULT_ANT.SEQ_UTENTE AND " & _
                                "SL_CR_MREGANA.COD_PERFIL *= SLV_RESULT_ANT.COD_PERFIL AND " & _
                                "SL_CR_MREGANA.COD_ANA_C *= SLV_RESULT_ANT.COD_ANA_C AND " & _
                                "SL_CR_MREGANA.COD_ANA_S *= SLV_RESULT_ANT.COD_ANA_S " & _
                                " AND (" & _
                                "     SLV_RESULT_ANT.SEQ_REALIZA IS NULL " & _
                                "     OR " & _
                                "     (" & _
                                "      SLV_RESULT_ANT.SEQ_REALIZA IS NOT NULL " & _
                                "      AND " & _
                                "      SLV_RESULT_ANT.SEQ_REALIZA IN " & _
                                "      (SELECT TOP " & NumResAnt & " SEQ_REALIZA " & _
                                "      FROM SLV_RESULT_ANT A " & _
                                "      WHERE " & _
                                "      A.SEQ_UTENTE=SL_CR_MREGANA.SEQ_UTENTE AND " & _
                                "      A.COD_PERFIL=SL_CR_MREGANA.COD_PERFIL AND " & _
                                "      A.COD_ANA_C=SL_CR_MREGANA.COD_ANA_C AND " & _
                                "      A.COD_ANA_S=SL_CR_MREGANA.COD_ANA_S) " & _
                                "     )" & _
                                "    ) " & _
                                " AND SLV_RESULT_ANT.SEQ_REALIZA *= SLV_RESULT_VAL.SEQ_REALIZA "
                        
                Case gInformix
                
                Case gOracle
                    sql = sql & "SL_CR_MREGANA.SEQ_UTENTE = SLV_RESULT_ANT.SEQ_UTENTE(+) AND " & _
                                "SL_CR_MREGANA.COD_PERFIL = SLV_RESULT_ANT.COD_PERFIL(+) AND " & _
                                "SL_CR_MREGANA.COD_ANA_C = SLV_RESULT_ANT.COD_ANA_C(+) AND " & _
                                "SL_CR_MREGANA.COD_ANA_S = SLV_RESULT_ANT.COD_ANA_S(+) " & _
                                " AND (" & _
                                "     SLV_RESULT_ANT.SEQ_REALIZA IS NULL " & _
                                "     OR " & _
                                "     (" & _
                                "      SLV_RESULT_ANT.SEQ_REALIZA IS NOT NULL " & _
                                "      AND " & _
                                "      SLV_RESULT_ANT.SEQ_REALIZA IN " & _
                                "      (SELECT SEQ_REALIZA " & _
                                "      FROM SLV_RESULT_ANT A " & _
                                "      WHERE " & _
                                "      ROWNUM<=" & NumResAnt & " AND " & _
                                "      A.SEQ_UTENTE=SL_CR_MREGANA.SEQ_UTENTE AND " & _
                                "      A.COD_PERFIL=SL_CR_MREGANA.COD_PERFIL AND " & _
                                "      A.COD_ANA_C=SL_CR_MREGANA.COD_ANA_C AND " & _
                                "      A.COD_ANA_S=SL_CR_MREGANA.COD_ANA_S) " & _
                                "     )" & _
                                "    ) " & _
                                " AND SLV_RESULT_ANT.SEQ_REALIZA = SLV_RESULT_VAL.SEQ_REALIZA(+) "
            End Select
            sql = sql & "ORDER BY " & _
                        "SL_CR_MREGUTE.SEQ_UTENTE ASC," & _
                        "SL_CR_MREGREQ.N_REQ ASC," & _
                        "SL_CR_MREGANA.COD_PERFIL ASC," & _
                        "SL_CR_MREGANA.COD_ANA_C ASC," & _
                        "SL_CR_MREGANA.cod_ana_s ASC "
    Else
        sql = "SELECT " & _
              "SL_CR_MREGUTE.SEQ_UTENTE,SL_CR_MREGUTE.NOME_UTE,SL_CR_MREGUTE.SEXO_UTESL_CR_MREGUTE.N_UTENTE,SL_CR_MREGUTE.N_PROCESSO,SL_CR_MREGUTE.IDADE_UTE,SL_CR_MREGUTE.DIAGN_P,SL_CR_MREGUTE.FLAG_DIAGN_P," & _
              "SL_CR_MREGREQ.N_REQ,SL_CR_MREGREQ.DESCR_T_SIT,SL_CR_MREGREQ.T_URG,SL_CR_MREGREQ.DESCR_PROVEN,SL_CR_MREGREQ.DIAGN_S,SL_CR_MREGREQ.TERA_MED,SL_CR_MREGREQ.PRODUTOS,SL_CR_MREGREQ.FLAG_TERA_MED,SL_CR_MREGREQ.FLAG_DIAGN_S,SL_CR_MREGREQ.FLAG_PRODUTOS," & _
              "SL_CR_MREGANA.COD_PERFIL,SL_CR_MREGANA.COD_ANA_C,SL_CR_MREGANA.COD_ANA_S,SL_CR_MREGANA.DESCR_PERFIL,SL_CR_MREGANA.DESCR_ANA_C,SL_CR_MREGANA.Descr_ana_s " & _
              "FROM " & _
              "SL_CR_MREGUTE" & gNumeroSessao & " SL_CR_MREGUTE," & _
              "SL_CR_MREGREQ" & gNumeroSessao & " SL_CR_MREGREQ," & _
              "SL_CR_MREGANA" & gNumeroSessao & " SL_CR_MREGANA " & _
              "WHERE " & _
              "SL_CR_MREGUTE.SEQ_UTENTE = SL_CR_MREGREQ.SEQ_UTENTE AND " & _
              "SL_CR_MREGREQ.N_REQ = SL_CR_MREGANA.N_REQ " & _
              "ORDER BY " & _
              "SL_CR_MREGUTE.SEQ_UTENTE ASC,SL_CR_MREGREQ.N_REQ ASC," & _
              "SL_CR_MREGANA.COD_PERFIL ASC, SL_CR_MREGANA.COD_ANA_C ASC,SL_CR_MREGANA.cod_ana_s Asc "
    End If
    Report.SQLQuery = sql
    Call BL_ExecutaReport
    
    Call BL_RemoveTabela("SL_CR_MREGUTE" & gNumeroSessao)
    Call BL_RemoveTabela("SL_CR_MREGREQ" & gNumeroSessao)
    Call BL_RemoveTabela("SL_CR_MREGANA" & gNumeroSessao)
    
    'Desaloca os comandos e o RecordSet
    Set CmdDescrAnalise = Nothing
    Set CmdDiagnP = Nothing
    Set CmdDiagnS = Nothing
    Set CmdProd = Nothing
    Set CmdTeraMed = Nothing
    
    RegReq.Close
    Set RegReq = Nothing
        
End Sub

Private Sub BtPesquisaUtente_Click()
        
End Sub

Private Sub BtPesquisaProveniencia_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub CbGrupo_Click()
    
    BL_ColocaComboTexto "sl_gr_ana", "seq_gr_ana", "cod_gr_ana", EcCodGrupo, CbGrupo
    
End Sub

Private Sub CbGrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then
        CbGrupo.ListIndex = -1
    End If
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then
        CbSituacao.ListIndex = -1
    End If
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then
        CbUrgencia.ListIndex = -1
    End If
    
End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodProveniencia)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.Text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.Text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()
    
    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
        
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    
End Sub

Private Sub EcDtIni_GotFocus()
    
    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtIni)
    
End Sub

Private Sub EcNumUltRes_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumUltRes)
    
End Sub

Private Sub Form_Activate()
        
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub


Private Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
    
End Sub

Private Sub CriaTabelas()
    
    Dim Utente(8) As DefTable
    Dim req(11) As DefTable
    Dim analises(8) As DefTable
    
    
    'UTENTE
    Utente(1).NomeCampo = "SEQ_UTENTE"
    Utente(1).tipo = "INTEGER"
    Utente(1).tamanho = 20
    
    Utente(2).NomeCampo = "NOME_UTE"
    Utente(2).tipo = "STRING"
    Utente(2).tamanho = 80
    
    Utente(3).NomeCampo = "SEXO_UTE"
    Utente(3).tipo = "STRING"
    Utente(3).tamanho = 15
    
    Utente(4).NomeCampo = "N_UTENTE"
    Utente(4).tipo = "STRING"
    Utente(4).tamanho = 26
    
    Utente(5).NomeCampo = "N_PROCESSO"
    Utente(5).tipo = "STRING"
    Utente(5).tamanho = 41
    
    Utente(6).NomeCampo = "IDADE_UTE"
    Utente(6).tipo = "STRING"
    Utente(6).tamanho = 10
    
    Utente(7).NomeCampo = "DIAGN_P"
    Utente(7).tipo = "STRING"
    Utente(7).tamanho = 1000
    
    Utente(8).NomeCampo = "FLAG_DIAGN_P"
    Utente(8).tipo = "STRING"
    Utente(8).tamanho = 1
    
    
    'REQUISI��ES
    req(1).NomeCampo = "SEQ_UTENTE"
    req(1).tipo = "INTEGER"
    req(1).tamanho = 20
    
    req(2).NomeCampo = "N_REQ"
    req(2).tipo = "INTEGER"
    req(2).tamanho = 7
    
    req(3).NomeCampo = "DESCR_T_SIT"
    req(3).tipo = "STRING"
    req(3).tamanho = 20
    
    req(4).NomeCampo = "T_URG"
    req(4).tipo = "STRING"
    req(4).tamanho = 10
    
    req(5).NomeCampo = "DESCR_PROVEN"
    req(5).tipo = "STRING"
    req(5).tamanho = 80

    req(6).NomeCampo = "DIAGN_S"
    req(6).tipo = "STRING"
    req(6).tamanho = 1000
    
    req(7).NomeCampo = "TERA_MED"
    req(7).tipo = "STRING"
    req(7).tamanho = 1000
    
    req(8).NomeCampo = "PRODUTOS"
    req(8).tipo = "STRING"
    req(8).tamanho = 1000
    
    req(9).NomeCampo = "FLAG_TERA_MED"
    req(9).tipo = "STRING"
    req(9).tamanho = 1
    
    req(10).NomeCampo = "FLAG_DIAGN_S"
    req(10).tipo = "STRING"
    req(10).tamanho = 1
    
    req(11).NomeCampo = "FLAG_PRODUTOS"
    req(11).tipo = "STRING"
    req(11).tamanho = 1
    
    
    'AN�LISES
    analises(1).NomeCampo = "SEQ_UTENTE"
    analises(1).tipo = "INTEGER"
    analises(1).tamanho = 20
    
    analises(2).NomeCampo = "N_REQ"
    analises(2).tipo = "INTEGER"
    analises(2).tamanho = 7
    
    analises(3).NomeCampo = "COD_PERFIL"
    analises(3).tipo = "STRING"
    analises(3).tamanho = 10
    
    analises(4).NomeCampo = "COD_ANA_C"
    analises(4).tipo = "STRING"
    analises(4).tamanho = 10
    
    analises(5).NomeCampo = "COD_ANA_S"
    analises(5).tipo = "STRING"
    analises(5).tamanho = 10
    
    analises(6).NomeCampo = "DESCR_PERFIL"
    analises(6).tipo = "STRING"
    analises(6).tamanho = 80
    
    analises(7).NomeCampo = "DESCR_ANA_C"
    analises(7).tipo = "STRING"
    analises(7).tamanho = 80
    
    analises(8).NomeCampo = "DESCR_ANA_S"
    analises(8).tipo = "STRING"
    analises(8).tamanho = 80
    
    'Cria a Tabela na BD
    Call BL_CriaTabela("SL_CR_MREGUTE" & gNumeroSessao, Utente)
    Call BL_CriaTabela("SL_CR_MREGREQ" & gNumeroSessao, req)
    Call BL_CriaTabela("SL_CR_MREGANA" & gNumeroSessao, analises)
    
End Sub



Private Sub EcListaLocalEntrega_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocalEntrega, KeyCode, Shift
End Sub
Private Sub BtPesquisaLocalEntrega_Click()
    PA_PesquisaLocalEntregaMultiSel EcListaLocalEntrega
End Sub

Attribute VB_Name = "RegistoFaltas"
Sub RegistarFaltas()
    Dim SQL As String
    Dim RsReqProd As ADODB.Recordset
    Dim Continua As Boolean
    
    SQL = "SELECT n_req FROM sl_req_prod WHERE dt_chega is null AND dt_previ<" & BL_TrataDataParaBD(Date)
    Set RsReqProd = New ADODB.Recordset
    RsReqProd.CursorLocation = adUseClient
    RsReqProd.CursorType = adOpenStatic
    RsReqProd.Open SQL, gConexao
    If RsReqProd.RecordCount > 0 Then
    
        'Abre a Common Dialog
        Continua = BL_CaixaImpressao(cdlPortrait)
        If Continua = False Then Exit Sub
    
        Call Cria_TmpRec_RegistarFaltas
    
        'Cancelar as requisi��es cujos produtos deviam ter chegado e n�o chegaram
        While Not RsReqProd.EOF
            'Mudar o estado da requisi��o na tabela sl_requis
            SQL = "UPDATE sl_requis SET estado_req='C' where n_req=" & RsReqProd!N_Req
            BG_ExecutaQuery_ADO SQL
            'criar um registo com a raz�o do cancelamento na tabela sl_req_canc
            SQL = "INSERT INTO sl_req_canc(n_req,dt_canc,hr_canc,t_canc,obs_canc,user_canc) VALUES(" & RsReqProd!N_Req & "," & BL_TrataDataParaBD(Date) & ",'" & Time & "'," & "4" & "," & "''" & "," & gCodUtilizador & ")"
            BG_ExecutaQuery_ADO SQL
            'Insert na tabela tempor�ria para ser usada no Crystal Reports
'            SQL = "INSERT INTO sl_modelo_rf" & gNumeroSessao & "(n_req,empr_id) VALUES(" & RsReqProd!N_Req & "," & "''" & ")"
'            BG_ExecutaQuery_ADO SQL
            RsReqProd.MoveNext
        Wend
        
        Dim Report As CrystalReport
        Set Report = Forms(0).Controls("Report")
        
        Call BL_IniciaReport("RegistarFaltas", crptToWindow)
        Report.SQLQuery = ""
        Report.Action = 1
        Call BL_RemoveTabela("SL_MODELO_RF" & gNumeroSessao)
    End If
End Sub

Sub Cria_TmpRec_RegistarFaltas()

    Dim TmpRec(2) As DefTable
    
    TmpRec(1).NomeCampo = "n_req"
    TmpRec(1).Tipo = "NUMBER"
    
    TmpRec(2).NomeCampo = "empr_id"
    TmpRec(2).Tipo = "STRING"
    TmpRec(2).Tamanho = 8
    
    Call BL_CriaTabela("SL_MODELO_RF" & gNumeroSessao, TmpRec)
End Sub

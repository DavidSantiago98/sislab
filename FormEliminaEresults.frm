VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEliminaEresults 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEliminaEresults"
   ClientHeight    =   3840
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   7425
   Icon            =   "FormEliminaEresults.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3840
   ScaleWidth      =   7425
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbTSit 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   3360
      Locked          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox EcEpisodio 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   6120
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton BtApagar 
      Height          =   615
      Left            =   3000
      Picture         =   "FormEliminaEresults.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Apagar Registos"
      Top             =   3120
      Width           =   1215
   End
   Begin VB.TextBox EcDtChega 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   480
      Width           =   855
   End
   Begin VB.TextBox EcNome 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   3240
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   480
      Width           =   3975
   End
   Begin MSFlexGridLib.MSFlexGrid FgReq 
      Height          =   1935
      Left            =   360
      TabIndex        =   2
      Top             =   960
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   3413
      _Version        =   393216
      Cols            =   3
      BackColorBkg    =   -2147483633
      GridColor       =   8421504
      GridLines       =   2
      BorderStyle     =   0
   End
   Begin VB.TextBox EcNumReq 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Tipo Epis�dio"
      Height          =   255
      Index           =   4
      Left            =   2160
      TabIndex        =   10
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Epis�dio"
      Height          =   255
      Index           =   3
      Left            =   5160
      TabIndex        =   9
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Data Req."
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Nome"
      Height          =   255
      Index           =   1
      Left            =   2160
      TabIndex        =   4
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FormEliminaEresults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim gConexao_eResults As ADODB.Connection
Dim rs As ADODB.Recordset

Private Type doc
    codigo_original As String
    cod_tipo_doc As Integer
    dt_emissao As String
    h_emissao As String
    flg_eliminar As Boolean
End Type
Dim EstrutDoc() As doc
Dim totalEstrutDoc As Long
Dim GL_TIPO_DOC As String

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
        
    BL_FimProcessamento Me
    
End Sub

Sub FuncaoInserir()

End Sub
Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    Set rs = New ADODB.Recordset
    
    If EcNumReq = "" Then Exit Sub
    
    'CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    CriterioTabela = "SELECT x1.n_req, x1.dt_chega, x2.nome_ute, x1.n_epis, x1.t_sit FROM sl_Requis x1, sl_identif x2 "
    CriterioTabela = CriterioTabela & " WHERE x1.n_req = " & EcNumReq & " AND x1.seq_utente = x2.seq_utente "
    
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN Estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        
        BL_FimProcessamento Me
    End If
    
End Sub
Sub PreencheCampos()
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    EcNumReq = BL_HandleNull(rs!n_req, "")
    EcDtChega = BL_HandleNull(rs!Dt_Chega, "")
    EcNome = BL_HandleNull(rs!nome_ute, "")
    EcEpisodio = BL_HandleNull(rs!n_epis, "")
    For i = 0 To CbTSit.ListCount - 1
        If CbTSit.ItemData(i) = BL_HandleNull(rs!t_sit, -1) Then
            CbTSit.ListIndex = i
            Exit For
        End If
    Next
        
    If gLAB = "CITO" Or gLAB = "BIO" Or gLAB = "GM" Or gLAB = "ENZ" Then
        If CbTSit.ListIndex = mediComboValorNull Or EcEpisodio = "" Then Exit Sub
        sSql = "SELECT * FROM er_docs WHERE tipo_episodio = " & BL_TrataStringParaBD(CbTSit) & " AND cod_episodio = " & BL_TrataStringParaBD(EcEpisodio)
        'sSql = sSql & " AND cod_tipo_doc = " & GL_TIPO_DOC
    Else
        sSql = "SELECT * FROM er_docs WHERE codigo_original = " & BL_TrataStringParaBD(EcNumReq) & " AND cod_origem = " & geResultsCodOrigem
    End If
    Set rs = New ADODB.Recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile sSql
    rs.Open sSql, gConexao_eResults
    If rs.RecordCount > 0 Then
        While Not rs.EOF
            totalEstrutDoc = totalEstrutDoc + 1
            ReDim Preserve EstrutDoc(totalEstrutDoc)
            
            EstrutDoc(totalEstrutDoc).cod_tipo_doc = BL_HandleNull(rs!cod_tipo_doc, "")
            EstrutDoc(totalEstrutDoc).codigo_original = BL_HandleNull(rs!codigo_original, "")
            EstrutDoc(totalEstrutDoc).dt_emissao = BL_HandleNull(rs!dt_emissao, "")
            EstrutDoc(totalEstrutDoc).h_emissao = BL_HandleNull(rs!h_emissao, "")
            EstrutDoc(totalEstrutDoc).flg_eliminar = False
            
            FgReq.TextMatrix(totalEstrutDoc, 0) = EstrutDoc(totalEstrutDoc).cod_tipo_doc
            FgReq.TextMatrix(totalEstrutDoc, 1) = EstrutDoc(totalEstrutDoc).codigo_original
            FgReq.TextMatrix(totalEstrutDoc, 2) = EstrutDoc(totalEstrutDoc).dt_emissao
            FgReq.TextMatrix(totalEstrutDoc, 3) = EstrutDoc(totalEstrutDoc).h_emissao
            If EstrutDoc(totalEstrutDoc).flg_eliminar = False Then
                FgReq.TextMatrix(totalEstrutDoc, 4) = "N�o"
            Else
                FgReq.TextMatrix(totalEstrutDoc, 4) = "Sim"
            End If
            FgReq.AddItem ""
            rs.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao preencher campos: " & Err.Number & " (" & Err.Description & ")", Me.Name, "PreencheCampos", True
    Exit Sub
    Resume Next
End Sub

Sub FuncaoModificar()

End Sub
Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

Sub BD_Update()


End Sub


Sub BD_Insert()
End Sub

Sub DefTipoCampos()

    With FgReq
        .Rows = 2
        .FixedRows = 1
        .Cols = 5
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .Gridlines = flexGridFlat
        .GridLinesFixed = flexGridFlat
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 1000
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Tipo Doc."
        .ColWidth(1) = 2000
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "C�digo Doc."
        .ColWidth(2) = 1000
        .ColAlignment(2) = flexAlignLeftCenter
        .TextMatrix(0, 2) = "Dt.Emiss�o"
        .ColWidth(3) = 1000
        .ColAlignment(3) = flexAlignLeftCenter
        .TextMatrix(0, 3) = "Hr.Emiss�o"
        .ColWidth(4) = 1000
        .ColAlignment(4) = flexAlignLeftCenter
        .TextMatrix(0, 4) = "Apagar"
        .Col = 1
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
        
    BG_DefTipoCampoEc_ADO "SL_REQUIS", "n_req", EcNumReq, mediTipoDefeito
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
    Fecha_ConexaoBD_eResults
    Set FormEliminaEresults = Nothing
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "sigla_t_sit", CbTSit
End Sub

Private Sub BtApagar_Click()
    Dim i As Long
    Dim sSql As String
    On Error GoTo TrataErro
    
    For i = 1 To totalEstrutDoc
        If EstrutDoc(i).flg_eliminar = True Then
            sSql = "INSERT INTO sl_eresults_eliminados (n_Req, codigo_original, dt_elim, hr_elim, user_elim) VALUES("
            sSql = sSql & EcNumReq & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutDoc(i).codigo_original) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
            sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
            BG_ExecutaQuery_ADO sSql
            
            sSql = "DELETE FROM er_docs WHERE codigo_original = " & BL_TrataStringParaBD(EstrutDoc(i).codigo_original)
            sSql = sSql & " AND cod_origem = " & geResultsCodOrigem
            gConexao_eResults.Execute sSql
            
        End If
    Next
    FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtApagar_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtApagar_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If EcNumReq <> "" And KeyCode = vbKeyReturn Then
        FuncaoProcurar
    End If
End Sub




Private Sub FgReq_DblClick()
    If FgReq.row >= 1 And FgReq.row <= totalEstrutDoc Then
        If EstrutDoc(FgReq.row).flg_eliminar = True Then
            EstrutDoc(FgReq.row).flg_eliminar = False
        Else
            EstrutDoc(FgReq.row).flg_eliminar = True
        End If
        If EstrutDoc(FgReq.row).flg_eliminar = False Then
            FgReq.TextMatrix(FgReq.row, 4) = "N�o"
        Else
            FgReq.TextMatrix(FgReq.row, 4) = "Sim"
        End If
    End If
End Sub

Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub


Sub Inicializacoes()

    Me.Caption = " Elimina��o de Documentos - eResults"
    Me.Left = 50
    Me.Top = 50
    Me.Width = 7515
    Me.Height = 4230 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    
    NumCampos = 0
    ReDim CamposBD(0)
    ReDim CamposEc(0)
    ReDim TextoCamposObrigatorios(0)
    Abre_ConexaoBD_eResults
    LimpaCampos
    
    If gLAB = "CITO" Then
        GL_TIPO_DOC = "27"
    ElseIf gLAB = "ENZ" Then
        GL_TIPO_DOC = "30"
    ElseIf gLAB = "GM" Then
        GL_TIPO_DOC = "29"
    ElseIf gLAB = "BIO" Then
        GL_TIPO_DOC = "28"
    End If
    
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub


Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
    End If

End Sub
Sub FuncaoEstadoAnterior()
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub
Sub LimpaCampos()
    Dim i As Integer
    'Me.SetFocus

    'BG_LimpaCampo_Todos CamposEc
    EcNumReq = ""
    EcNome = ""
    EcDtChega = ""
    PreencheValoresDefeito
    
    totalEstrutDoc = 0
    ReDim EstrutDoc(0)
    
    FgReq.Rows = 2
    For i = 0 To FgReq.Cols - 1
        FgReq.TextMatrix(1, i) = ""
    Next
    
    CbTSit.ListIndex = -1
    EcEpisodio = ""
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

' -----------------------------------------------------------------------------------------------

' ABRE A CONEXAO AO ERESULTS

' -----------------------------------------------------------------------------------------------
Public Function Abre_ConexaoBD_eResults()
       
    'vari�vel ter� de ser global uma s� vez usada para a aplica��o em geral
    Set gConexao_eResults = New ADODB.Connection
    
    On Error GoTo TrataErro
    
    With gConexao_eResults
        .Provider = "MSDASQL"
        .ConnectionString = "DSN=" & geResultsDSN & ";UID=" & geResultsUTIL & ";PWD=" & geResultsPWD & ";Connect Timeout=90"
        .CommandTimeout = "180"
       .Open
    End With
    
    gConexao_eResults.Execute "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'"
    Abre_ConexaoBD_eResults = 1
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro a Abrir a Base de Dados eResults(" & gSGBD & " - " & gDSN & ") : " & Err.Number & " (" & Err.Description & ")", Me.Name, "Abre_ConexaoBD_eResults", True
    Abre_ConexaoBD_eResults = 0
    Exit Function
    Resume Next
End Function

Function Fecha_ConexaoBD_eResults()

    'liberta a vari�vel de conex�o
    Set gConexao_eResults = Nothing
    
End Function

VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEtiqPreImpressas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEtiqPreImpressas"
   ClientHeight    =   6060
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5610
   Icon            =   "FormEtiqPreImpressas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   5610
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtEtiq 
      Height          =   480
      Left            =   4920
      Picture         =   "FormEtiqPreImpressas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   20
      ToolTipText     =   "Imprimir Etiquetas"
      Top             =   5520
      Width           =   495
   End
   Begin VB.Frame Frame1 
      Height          =   5295
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5415
      Begin VB.CheckBox CkEtiqIntervalo 
         Appearance      =   0  'Flat
         Caption         =   "Imprimir Etiq. Intervalo"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3240
         TabIndex        =   19
         Top             =   2040
         Width           =   2055
      End
      Begin VB.CheckBox CkInversao 
         Appearance      =   0  'Flat
         Caption         =   "Inverter ordem  Impr."
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3240
         TabIndex        =   18
         Top             =   1680
         Width           =   1815
      End
      Begin VB.TextBox EcAux 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   0
         TabIndex        =   17
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
      Begin MSFlexGridLib.MSFlexGrid FgTubos 
         Height          =   2415
         Left            =   240
         TabIndex        =   16
         Top             =   2760
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   4260
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.TextBox EcNumReq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2040
         TabIndex        =   14
         Top             =   2040
         Width           =   975
      End
      Begin VB.TextBox EcNumEtiq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2040
         TabIndex        =   12
         Top             =   1680
         Width           =   975
      End
      Begin VB.TextBox EcCodPosto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   10
         Top             =   1080
         Width           =   855
      End
      Begin VB.TextBox EcDescrPosto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2040
         TabIndex        =   9
         Top             =   1080
         Width           =   2895
      End
      Begin VB.CommandButton BtPesquisaPosto 
         Height          =   315
         Left            =   4920
         Picture         =   "FormEtiqPreImpressas.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1080
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   4920
         Picture         =   "FormEtiqPreImpressas.frx":1260
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox EcDescrServico 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2040
         TabIndex        =   5
         Top             =   600
         Width           =   2895
      End
      Begin VB.TextBox EcCodServico 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox EcNumCopias 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2040
         TabIndex        =   2
         Top             =   2370
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "N�mero de requisi��o"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   2070
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "N�mero de etiquetas"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   1710
         Width           =   2535
      End
      Begin VB.Label Label2 
         Caption         =   "Posto"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   11
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Servi�o"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "N�mero de c�pias"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   2400
         Width           =   1575
      End
   End
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   360
      TabIndex        =   7
      Top             =   3360
      Width           =   255
   End
End
Attribute VB_Name = "FormEtiqPreImpressas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 24/07/2006
' T�cnico

' Vari�veis Globais para este Form.

Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Public CampoActivo As Object
Dim NomeTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

Dim CmdImp As New ADODB.Command
Dim PmtImp As ADODB.Parameter

Private Type tubo
    cod_tubo As String
    descR_tubo As String
    cod_etiq As String
    Copias As Integer
End Type
Dim estrutTubo() As tubo
Dim totalEstrutTubo As Integer

Private Sub BtEtiq_Click()

    If EcNumReq.Text <> "" Then
        EcNumEtiq.Text = 1
    ElseIf EcNumEtiq.Text = "" Then
        BG_Mensagem mediMsgBox, "N�mero de etiquetas obrigat�rio!", vbInformation, "Aten��o"
        Exit Sub
    ElseIf EcNumCopias.Text = "" Then
        BG_Mensagem mediMsgBox, "N�mero de c�pias obrigat�rio!", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Call ImprimeEtiq
End Sub




Private Sub BtPesquisaPosto_Click()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_sala"
    CamposEcran(1) = "descr_sala"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_sala"
    CamposEcran(2) = "cod_sala"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CWhere = "( flg_Colheita is null or flg_colheita = 0) "
    CampoPesquisa = "descr_sala"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Postos Colheita")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodPosto.Text = resultados(2)
            EcCodposto_Validate False
            BtPesquisaPosto.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Postos Codificados", vbExclamation, "Postos"
        EcCodPosto.SetFocus
    End If

End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcAux = BL_HandleNull(EcAux, 0)
        estrutTubo(FGTubos.row).Copias = EcAux
        FGTubos.TextMatrix(FGTubos.row, 1) = EcAux
        FGTubos.SetFocus
    End If
    
    If KeyCode = vbKeyEscape Then
        FGTubos.SetFocus
    End If
        
End Sub

Private Sub EcAux_LostFocus()
    EcAux.Visible = False
    FGTubos.SetFocus
End Sub

Private Sub EcCodPosto_Change()
    If EcCodPosto <> "" Then
        EcCodServico = ""
        EcCodservico_Validate True
        EcCodServico.Enabled = False
        EcDescrServico.Enabled = False
        BtPesquisaProveniencia.Enabled = False
    Else
        EcCodServico.Enabled = True
        EcDescrServico.Enabled = True
        BtPesquisaProveniencia.Enabled = True
    End If
End Sub
Private Sub EcCodServico_click()
    If EcCodServico <> "" Then
        EcCodPosto = ""
        EcCodposto_Validate True
        EcCodPosto.Enabled = False
        EcDescrPosto.Enabled = False
        BtPesquisaPosto.Enabled = False
    Else
        EcCodPosto.Enabled = True
        EcDescrPosto.Enabled = True
        BtPesquisaPosto.Enabled = True
    End If
End Sub

Private Sub FGTubos_DblClick()
    If FGTubos.Col = 1 And FGTubos.row <= totalEstrutTubo Then
        EcAux.left = FGTubos.CellLeft + FGTubos.left
        EcAux.top = FGTubos.CellTop + FGTubos.top
        EcAux.Width = FGTubos.CellWidth + 10
        EcAux = estrutTubo(FGTubos.row).Copias
        EcAux.Visible = True
        EcAux.SetFocus
        Sendkeys ("{END}")
    End If
End Sub

Private Sub FGTubos_KeyDown(KeyCode As Integer, Shift As Integer)
    If FGTubos.row <= totalEstrutTubo And FGTubos.Col = 1 And KeyCode = vbKeyReturn Then
        EcAux.left = FGTubos.CellLeft + FGTubos.left
        EcAux.top = FGTubos.CellTop + FGTubos.top
        EcAux.Width = FGTubos.CellWidth + 10
        EcAux = estrutTubo(FGTubos.row).Copias
        EcAux.Visible = True
        EcAux.SetFocus
        Sendkeys ("{END}")
    End If
End Sub

Sub Form_Load()
    
    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Etiquetas Pr� Impressas"
    Me.left = 440
    Me.top = 20
    Me.Width = 5700
    Me.Height = 6480 ' Normal
    EcDescrServico.Enabled = False
    
    EcNumEtiq.Tag = adInteger
    
    ' Pode ser a mesma impressora...da seroteca
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    
    EcNumCopias.Text = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "N_ETIQ_PRE_IMPRESSAS")
    
    'EcNumCopias.text = "6"
    
    Set CmdImp.ActiveConnection = gConexao
    CmdImp.CommandType = adCmdText
    CmdImp.CommandText = "SELECT MAX_IMPRESSAO FROM sl_cod_salas WHERE cod_sala = ?"
    CmdImp.Prepared = True
    Set PmtImp = CmdImp.CreateParameter("COD_SALA", adVarChar, adParamInput, 8)
    CmdImp.Parameters.Append PmtImp
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

    EcNumEtiq.SetFocus
    
End Sub

Sub EventoUnload()
    
    Set CmdImp = Nothing
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormEtiqPreImpressas = Nothing
End Sub

Sub FuncaoLimpar()

End Sub

Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()
    
End Sub

Sub FuncaoInserir()
 
End Sub

Sub FuncaoModificar()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub ImprimeEtiq()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim valor As String
    Dim prefixo As String
    Dim abrev As String
    Dim qtd As Integer
    Dim descricao As String
    Dim EtqCrystal As String
    On Error GoTo ErrorHandler
    Dim NumReqBar As String
    Dim RsImp As ADODB.recordset
    Dim valores() As String
    If CkInversao.value = vbChecked And EcNumReq = "" Then
        ReDim valores(EcNumEtiq)
        prefixo = BL_SelCodigo("SL_COD_SALAS", "PREFIXO_IMPRESSAO", "COD_SALA", EcCodPosto)
        abrev = BL_SelCodigo("SL_COD_SALAS", "ABREV", "COD_SALA", EcCodPosto)
        For i = 1 To EcNumEtiq
            If gUsaNReqPreImpressa = mediSim Then
                valores(i) = BL_GeraNumero("N_REQUIS")
            Else
                valores(i) = BL_GeraNumeroPreImpressas("SL_COD_SALAS", EcCodPosto)
            End If
        Next
    End If
    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
                
    If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                
    If EtqCrystal = "1" Then
    Else
        For i = 1 To EcNumEtiq
            
            If EcCodPosto <> "" Then
                prefixo = BL_SelCodigo("SL_COD_SALAS", "PREFIXO_IMPRESSAO", "COD_SALA", EcCodPosto)
                abrev = BL_SelCodigo("SL_COD_SALAS", "ABREV", "COD_SALA", EcCodPosto)
                If EcNumReq.Text = "" Then
                    If CkInversao.value = vbChecked Then
                        valor = valores((EcNumEtiq + 1) - i)
                        
                    Else
                        If gUsaNReqPreImpressa = mediSim Then
                            valor = BL_GeraNumero("N_REQUIS")
                        Else
                            valor = BL_GeraNumeroPreImpressas("SL_COD_SALAS", EcCodPosto)
                        End If
                    End If
                Else
                    valor = EcNumReq.Text
                End If
                
                If gLAB = "LHL" Then
                    descricao = abrev
                Else
                    descricao = EcDescrPosto
                End If
                CmdImp.Parameters(0).value = EcCodPosto
                Set RsImp = CmdImp.Execute
                If Not RsImp.EOF Then
                    If CLng(valor) > CLng(BL_HandleNull(RsImp!max_impressao, 0)) And gUsaNReqPreImpressa <> mediSim Then
                        BG_Mensagem mediMsgBox, "Atingiu valor m�ximo de impress�o para o posto " & EcDescrPosto & "! ", vbInformation
                        Exit Sub
                    End If
                End If
            ElseIf EcCodServico <> "" Then
                prefixo = BL_SelCodigo("SL_PROVEN", "PREFIXO_IMPRESSAO", "COD_PROVEN", EcCodServico)
                If gUsaNReqPreImpressa = mediSim Then
                    valor = BL_GeraNumero("N_REQUIS")
                Else
                    valor = BL_GeraNumeroPreImpressas("SL_PROVEN", EcCodServico)
                End If
                descricao = EcDescrServico
            Else
                If EcNumReq <> "" Then
                    valor = EcNumReq.Text
                End If
            End If
            
            'O n�mero de requisi��o tem de ter 7 digitos para as rotinas dos aparelhos
            NumReqBar = ""
            'soliveira ualia 09.02.2009 campo prefixo utilizado para a etiqueta temporaria dos postos
            If prefixo <> "" Then
                valor = UCase(prefixo) & valor
            Else
                For j = 1 To (7 - Len(valor))
                    NumReqBar = NumReqBar & "0"
                Next j
                valor = NumReqBar & valor
            End If
            
            RegistaImprEtiq valor, EcCodPosto, EcCodServico
            For k = 1 To EcNumCopias.Text
                If Not LerEtiqInI Then
                    MsgBox "Ficheiro de inicializa��o de etiquetas Pr�-Impressas ausente!"
                    Exit Sub
                End If
                If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                    MsgBox "Imposs�vel abrir impressora etiquetas."
                    Exit Sub
                End If
                
                Call EtiqPrint(valor, descricao, "", CInt(EcNumCopias.Text))
                
                EtiqClosePrinter
            Next k
            
            ImprimeEtiqTubos valor, descricao
            
            If CkEtiqIntervalo.value = vbChecked Then
                ImprimeEtiqIntervalo
            End If
        Next i
        LimpaCampos
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : ImprimeEtiq (FormEtiqSeroteca) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
            Resume Next
    End Select
End Sub


Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo Erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Preimpressa.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Preimpressa.ini"
    End If
    If gImprimePreImpressasZPL = 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While aux <> "P1" And aux <> "P{QUANTIDADE}" And aux <> "P{NUM_COPIAS}"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function
Erro:
If Err.Number = 55 Then
    Close 1
    Open s For Input As 1
    Resume Next
End If
End Function

Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function

Private Function EtiqPrint(ByVal numero As String, servico As String, tubo As String, quantidade As Integer) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", numero)
    sWrittenData = Replace(sWrittenData, "{NUMERO_BAR}", numero)
    sWrittenData = Replace(sWrittenData, "{SERVICO}", servico)
    sWrittenData = Replace(sWrittenData, "{DESCR_TUBO}", tubo)
    sWrittenData = Replace(sWrittenData, "{QUANTIDADE}", quantidade)
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", quantidade)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function



Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function

Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_proven"
    CamposEcran(1) = "descr_proven"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_proven"
    CamposEcran(2) = "cod_proven"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CampoPesquisa = "descr_sala"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Servi�os")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodServico.Text = resultados(2)
            EcDescrServico.Text = resultados(1)
            BtPesquisaProveniencia.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Servi�os Codificados", vbExclamation, "Servi�os"
        EcCodServico.SetFocus
    End If

End Sub


Public Sub EcCodservico_Validate(Cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodServico.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven, t_sit, t_urg FROM sl_proven WHERE cod_proven='" & Trim(EcCodServico.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodServico.Text = ""
            EcDescrServico.Text = ""
        Else
            EcDescrServico.Text = Tabela!descr_proven
        End If
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrServico.Text = ""
    End If
    
End Sub
Public Sub EcCodposto_Validate(Cancel As Boolean)
    Dim i As Integer
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodPosto.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT * FROM sl_cod_salas WHERE cod_sala='" & Trim(EcCodPosto.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodPosto.Text = ""
            EcDescrPosto.Text = ""
            EcNumCopias = "0"
            EcNumEtiq = "0"
        Else
            EcDescrPosto.Text = BL_HandleNull(Tabela!descr_sala, "")
            EcNumCopias = BL_HandleNull(Tabela!num_copias, BG_DaParamAmbiente_NEW(mediAmbitoGeral, "N_ETIQ_PRE_IMPRESSAS"))
            EcNumEtiq = BL_HandleNull(Tabela!num_impr, "0")
        End If
        
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPosto.Text = ""
        EcNumCopias = "0"
        EcNumEtiq = "0"
    End If
    If EcCodPosto <> "" Then
        CarregaEtiquetas EcCodPosto
    Else
        For i = 1 To totalEstrutTubo
            estrutTubo(i).Copias = 0
            FGTubos.TextMatrix(i, 1) = estrutTubo(i).Copias
        Next
    End If
End Sub


Private Sub LimpaCampos()
    EcCodServico = ""
    EcCodservico_Validate False
    EcCodPosto = ""
    EcCodposto_Validate False
    EcNumEtiq = ""
    CkEtiqIntervalo.value = vbUnchecked
End Sub
Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function


Private Sub DefTipoCampos()
    With FGTubos
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(0) = 3500
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "Tubo"
        
        .ColWidth(1) = 1000
        .Col = 1
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "C�pias"
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
End Sub

Private Sub PreencheValoresDefeito()
    Dim sSql As String
    Dim Tabela As New ADODB.recordset
    On Error GoTo TrataErro
    
    totalEstrutTubo = 0
    ReDim estrutTubo(0)
    sSql = "SELECT * FROM sl_tubo ORDER BY descr_tubo "
    Tabela.CursorType = adOpenStatic
    Tabela.CursorLocation = adUseServer
    Tabela.Open sSql, gConexao
    If Tabela.RecordCount > 0 Then
        While Not Tabela.EOF
            totalEstrutTubo = totalEstrutTubo + 1
            ReDim Preserve estrutTubo(totalEstrutTubo)
            estrutTubo(totalEstrutTubo).cod_tubo = BL_HandleNull(Tabela!cod_tubo, "")
            estrutTubo(totalEstrutTubo).descR_tubo = BL_HandleNull(Tabela!descR_tubo, "")
            estrutTubo(totalEstrutTubo).cod_etiq = BL_HandleNull(Tabela!cod_etiq, "")
            estrutTubo(totalEstrutTubo).Copias = 0
            
            FGTubos.TextMatrix(totalEstrutTubo, 0) = estrutTubo(totalEstrutTubo).descR_tubo
            FGTubos.TextMatrix(totalEstrutTubo, 1) = estrutTubo(totalEstrutTubo).Copias
            FGTubos.AddItem ""
            Tabela.MoveNext
        Wend
    End If
    Tabela.Close
    Set Tabela = Nothing
    CkEtiqIntervalo.value = vbUnchecked
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Valores por Defeito " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValoresDefeito", True
    Exit Sub
    Resume Next
End Sub

Private Sub ImprimeEtiqTubos(valor As String, descricao As String)
    Dim j As Integer
    Dim k As Integer
    Dim valorTubo As String
    On Error GoTo TrataErro
    For j = 1 To totalEstrutTubo
        For k = 1 To estrutTubo(j).Copias
            If Not LerEtiqInI Then
                MsgBox "Ficheiro de inicializa��o de etiquetas Pr�-Impressas ausente!"
                Exit Sub
            End If
            If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                MsgBox "Imposs�vel abrir impressora etiquetas."
                Exit Sub
            End If
            valorTubo = BL_HandleNull(estrutTubo(j).cod_etiq, "") & valor
            Call EtiqPrint(valorTubo, descricao, estrutTubo(j).descR_tubo, estrutTubo(j).Copias)
            
            EtiqClosePrinter
       Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao imprimir etiquetas para tubo " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeEtiqTubos", True
    Exit Sub
    Resume Next
End Sub

Private Sub ImprimeEtiqIntervalo()
    Dim j As Integer
    Dim k As Integer
    Dim valorTubo As String
    On Error GoTo TrataErro
    If Not LerEtiqInI Then
        MsgBox "Ficheiro de inicializa��o de etiquetas Pr�-Impressas ausente!"
        Exit Sub
    End If
    If Not EtiqOpenPrinter(EcPrinterEtiq) Then
        MsgBox "Imposs�vel abrir impressora etiquetas."
        Exit Sub
    End If
    Call EtiqPrint("", "", "", 1)
    EtiqClosePrinter
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao imprimir etiquetas para tubo " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeEtiqTubos", True
    Exit Sub
    Resume Next
End Sub

Private Sub RegistaImprEtiq(n_req As String, cod_sala As String, cod_proven As String)
    On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = "INSERT INTO sl_etiq_salas (n_req, cod_sala, user_cri, dt_cri, hr_cri,cod_proven) VALUES( "
    sSql = sSql & BL_TrataStringParaBD(n_req) & ", " & BL_HandleNull(cod_sala, "NULL") & ", " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(cod_proven) & ")"
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao RegistaImprEtiq" & Err.Number & " - " & Err.Description, Me.Name, "RegistaImprEtiq", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' CARREGA ETIQUETAS

' ------------------------------------------------------------------------------------------------
Public Sub CarregaEtiquetas(cod_sala As String)
    Dim sSql As String
    Dim rsSala As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To totalEstrutTubo
        estrutTubo(i).Copias = 0
        FGTubos.TextMatrix(i, 1) = estrutTubo(i).Copias
    Next
    
    sSql = "SELECT * FROM sl_cod_salas_etiq WHERE cod_sala = " & cod_sala
    Set rsSala = BG_ExecutaSELECT(sSql)
    If rsSala.RecordCount > 0 Then
        While Not rsSala.EOF
            For i = 1 To totalEstrutTubo
                If estrutTubo(i).cod_tubo = BL_HandleNull(rsSala!cod_tubo, "") Then
                    estrutTubo(i).Copias = BL_HandleNull(rsSala!num_copias, 0)
                    FGTubos.TextMatrix(i, 1) = estrutTubo(i).Copias
                End If
            Next
            rsSala.MoveNext
        Wend
    End If
    rsSala.Close
    Set rsSala = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar etiquetas: " & sSql & " " & Err.Description, Me.Name, "CarregaEtiquetas", False
    Exit Sub
    Resume Next
End Sub


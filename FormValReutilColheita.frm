VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormValReutilColheita 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11070
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   11070
   ShowInTaskbar   =   0   'False
   Begin MSFlexGridLib.MSFlexGrid Flex 
      Height          =   375
      Left            =   1200
      TabIndex        =   21
      Top             =   7440
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
      _Version        =   393216
   End
   Begin VB.CommandButton BtValida 
      Height          =   615
      Left            =   9840
      Picture         =   "FormValReutilColheita.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   19
      ToolTipText     =   "Valida Reutiliza��o"
      Top             =   6120
      Width           =   735
   End
   Begin VB.CommandButton BtCancelar 
      Height          =   615
      Left            =   9000
      Picture         =   "FormValReutilColheita.frx":038A
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "Cancelar Reutiliza��o"
      Top             =   6120
      Width           =   735
   End
   Begin VB.PictureBox PicMasc 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   6360
      Picture         =   "FormValReutilColheita.frx":0AF4
      ScaleHeight     =   375
      ScaleWidth      =   375
      TabIndex        =   17
      ToolTipText     =   "Masculino"
      Top             =   1530
      Width           =   375
   End
   Begin VB.CommandButton BtConfirm 
      Height          =   375
      Left            =   6360
      Picture         =   "FormValReutilColheita.frx":125E
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Confirmar Requisi��o"
      Top             =   200
      Width           =   375
   End
   Begin VB.PictureBox PicFem 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   6360
      Picture         =   "FormValReutilColheita.frx":15E8
      ScaleHeight     =   375
      ScaleWidth      =   375
      TabIndex        =   15
      ToolTipText     =   "Feminino"
      Top             =   1540
      Width           =   375
   End
   Begin VB.TextBox Ectutente 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1680
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Data de Nascimento"
      Top             =   1200
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid FgTubos 
      Height          =   3375
      Left            =   360
      TabIndex        =   13
      Top             =   2040
      Width           =   10310
      _ExtentX        =   18177
      _ExtentY        =   5953
      _Version        =   393216
      BackColorBkg    =   -2147483633
      GridLines       =   0
      BorderStyle     =   0
   End
   Begin VB.ComboBox CbMotivo 
      Height          =   315
      Left            =   2400
      TabIndex        =   12
      Top             =   5640
      Width           =   4335
   End
   Begin VB.TextBox EcObs 
      Height          =   675
      Left            =   1800
      TabIndex        =   10
      Top             =   6120
      Width           =   4935
   End
   Begin VB.TextBox EcNome 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1680
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1560
      Width           =   4695
   End
   Begin VB.TextBox EcUtente 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   1200
      Width           =   3735
   End
   Begin VB.TextBox EcNumColheita 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1680
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Data de Nascimento"
      Top             =   720
      Width           =   1095
   End
   Begin VB.ComboBox CbEstadoReq 
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2760
      Style           =   1  'Simple Combo
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   240
      Width           =   3615
   End
   Begin VB.TextBox EcNumReq 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   1680
      TabIndex        =   1
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label LabelConsulta 
      Height          =   1215
      Left            =   7680
      TabIndex        =   20
      Top             =   360
      Width           =   2415
   End
   Begin VB.Label LabelObs 
      Caption         =   "Observa��es"
      Height          =   255
      Left            =   360
      TabIndex        =   11
      Top             =   6120
      Width           =   1335
   End
   Begin VB.Label LabelMotivo 
      Caption         =   "Motivo de n�o reutiliza��o"
      Height          =   375
      Left            =   360
      TabIndex        =   9
      Top             =   5640
      Width           =   1935
   End
   Begin VB.Label LbNome 
      AutoSize        =   -1  'True
      Caption         =   "Nome"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   360
      TabIndex        =   7
      Top             =   1560
      Width           =   405
   End
   Begin VB.Label LbNrDoe 
      AutoSize        =   -1  'True
      Caption         =   "&Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   360
      TabIndex        =   5
      Top             =   1200
      Width           =   465
   End
   Begin VB.Label LbColheita 
      AutoSize        =   -1  'True
      Caption         =   "N� Colheita"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   360
      TabIndex        =   3
      ToolTipText     =   "Data de Nascimento"
      Top             =   720
      Width           =   795
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Requisi��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   900
   End
End
Attribute VB_Name = "FormValReutilColheita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 19/04/2006
' T�cnico Sandra Oliveira
' Vari�veis Globais para este Form.

Dim Estado As Integer
Dim CamposBDparaListBox
Dim NumEspacos
Dim CampoDeFocus As Object


Dim estrutTuboReutil() As tubos_req

Const lColDtColheita = 0
Const lColDtRececao = 1
Const lColAmostra = 2
Const lColAnalise = 3
Const lColEstado = 4
Const lColHemolise = 5
Const lColLipemia = 6
Const lColIctericia = 7
Const lColLocalizacao = 8

Private Enum estado_validacao
    Pendente = 0
    Aceite = 1
    Recusado = 2
End Enum

Private Type interf_tubo
    cod_interf As String
    descr_interf As String
    resultado As String
    seq_req_tubo As String
End Type

Private Type analises
    descr_ana As String
    cod_ana As String
    seq_req_tubo As String
    Localizacao As String
    totalAnalises As Integer
End Type

Private Type tubos_req
    seq_req_tubo As String
    cod_tubo As String
    descR_tubo As String
    dt_chegada As String
    hr_chegada As String
    dt_colheita As String
    hr_colheita As String
    n_req As String
    analise_req() As analises
    interferencia() As interf_tubo
End Type

Dim TotalDadosTubo As Integer
Dim colunas As Integer

Dim estado_reutil As String
Dim dt_val_canc_reutil As String
Dim hr_val_canc_reutil As String
Dim user_val_canc_reutil As String
Dim motivo_n_reutil As String
Dim obs_reutil As String
Dim n_presc As String
Dim cod_local As String
Dim n_req As String
Dim dt_colheita As String
Dim hr_colheita As String
Dim dt_chega As String
Dim hr_chega As String


Private Sub BtCancelar_Click()
    Dim sql As String
    Dim sSql As String
    On Error GoTo TrataErro
    
    Dim trataRequis As trataResultado
    Dim r_seq_req_tubo As Integer
    Dim i As Integer
    Dim codAna() As String
    
    If CbMotivo.ListIndex = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "N�o foi selecionado nenhum motivo para n�o reutiliza��o da(s) amostra(s)!", vbInformation
    Else
        TB_ProcuraTubos Flex, CLng(Trim(EcNumReq.Text)), False
        'trataRequis = BL_TrataReutilizacao(EcNumReq.text, n_presc, estado_validacao.Recusado, Bg_DaData_ADO, Bg_DaHora_ADO, gCodUtilizador, EcObs.text, BG_DaComboSel(CbMotivo), _
        '              BL_HandleNull(cod_local, -1), EcNumColheita.text)
        'BG_BeginTransaction
        For i = 1 To gTotalTubos
            r_seq_req_tubo = TB_InsereTuboBD(mediComboValorNull, n_req, gEstruturaTubos(i).CodTubo, gEstruturaTubos(i).dt_previ, gEstruturaTubos(i).dt_chega, _
                      gEstruturaTubos(i).hr_chega, gEstruturaTubos(i).Garrafa, gEstruturaTubos(i).user_chega, gEstruturaTubos(i).local_chega, codAna, _
                      gEstruturaTubos(i).estado_tubo, mediComboValorNull, "")
                      
            sSql = "UPDATE sl_req_tubo set dt_colheita = " & BL_TrataStringParaBD(gEstruturaTubos(i).dt_colheita) & ", hr_colheita = " & BL_TrataStringParaBD(gEstruturaTubos(i).hr_colheita) & _
            " WHERE seq_req_tubo = " & r_seq_req_tubo
            
            BG_ExecutaQuery_ADO sSql
        Next i
        
            
        sql = "UPDATE sl_req_colheitas set estado_reutil = " & estado_validacao.Recusado & _
        " , dt_val_canc_reutil = " & BL_TrataStringParaBD(Bg_DaData_ADO) & ", hr_val_canc_reutil = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & _
        " , motivo_n_reutil = " & CbMotivo.ListIndex & ", user_val_canc_reutil = " & gCodUtilizador & _
        " , obs_reutil = " & BL_TrataStringParaBD(EcObs.Text) & _
        " WHERE n_req = " & BL_TrataStringParaBD(n_req) & " AND flg_reutil = 1 "

        BG_ExecutaQuery_ADO sql
        
        If gSQLError <> 0 Then
            BG_RollbackTransaction
        'If trataRequis.resultado = "NOK" Or trataRequis.resultado = "" Then
            BG_Mensagem mediMsgBox, "Erro ao cancelar a reutiliza��o da colheita!", vbInformation, "ERRO"
            'GoTo Trata_Erro
            Exit Sub
        Else
            'BG_CommitTransaction
            BG_Mensagem mediMsgBox, "Cancelamento Efetuado! ", vbInformation
            'Foi gerada um nova colheita " & trataRequis.n_colheita & ", para a prescricao " & trataRequis.n_pres & "!"
            FormReutilColheitas.FuncaoProcurar
            Unload Me
        End If
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao cancelar a reutiliza��o da colheita:" & Err.Description, Me.Name, "BtCancelar", True
    BG_RollbackTransaction
    Exit Sub
    Resume Next
End Sub

Private Sub BtValida_Click()
    Dim sql As String
    Dim SQLQuery As String
    On Error GoTo TrataErro
        
        Dim trataRequis As trataResultado
         
        'trataRequis = BL_TrataReutilizacao(EcNumReq.text, n_presc, estado_validacao.Aceite, Bg_DaData_ADO, Bg_DaHora_ADO, gCodUtilizador, EcObs.text, BG_DaComboSel(CbMotivo), _
        '            BL_HandleNull(cod_local, -1), EcNumColheita.text)
         
        BG_BeginTransaction

        SQLQuery = "UPDATE sl_requis set dt_colheita = " & BL_TrataStringParaBD(dt_colheita) & _
        " , hr_colheita = " & BL_TrataStringParaBD(hr_colheita) & ", dt_chega = " & BL_TrataStringParaBD(dt_chega) & ", hr_chega = " & BL_TrataStringParaBD(hr_chega) & _
        " WHERE n_req = " & BL_TrataStringParaBD(n_req)
        
        BG_ExecutaQuery_ADO SQLQuery
        
        sql = "UPDATE sl_req_colheitas set estado_reutil = " & estado_validacao.Aceite & _
        " , dt_val_canc_reutil = " & BL_TrataStringParaBD(Bg_DaData_ADO) & ", hr_val_canc_reutil = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & _
        " , obs_reutil = " & BL_TrataStringParaBD(EcObs.Text) & ", user_val_canc_reutil = " & gCodUtilizador & _
        " WHERE n_req = " & BL_TrataStringParaBD(n_req) & " AND flg_reutil = 1 "

        BG_ExecutaQuery_ADO sql
        
        If gSQLError <> 0 Then
            BG_RollbackTransaction
        'If trataRequis.resultado = "NOK" Or trataRequis.resultado = "" Then
            BG_Mensagem mediMsgBox, "Erro ao validar a reutiliza��o da colheita!", vbInformation, "ERRO"
            'GoTo Trata_Erro
            Exit Sub
        Else
            BG_CommitTransaction
            BG_Mensagem mediMsgBox, "Valida��o Efetuada!", vbInformation
            FormReutilColheitas.FuncaoProcurar
            Unload Me
        End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao validar a reutiliza��o da colheita:" & Err.Description, Me.Name, "BtValida", True
    BG_RollbackTransaction
    Exit Sub
    Resume Next
End Sub


Private Sub EcNumReq_LostFocus()

    'cmdOK.Default = False

End Sub

Sub FuncaoProcurar()

    Dim SelTotal As Boolean
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    Dim dt_colheita As String
    Dim hr_colheita As String
   
    On Error GoTo TrataErro
        
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    
     sSql = "(select req.n_req, ute.seq_utente, ute.t_utente, ute.utente, ute.nome_ute, ute.sexo_ute, req.estado_req, req.dt_colheita, req.hr_colheita, req.dt_chega, req.hr_chega "
     If gRegistaColheitaAtiva = mediSim Then
        sSql = sSql & ", colh.seq_colheita "
     End If
     sSql = sSql & " FROM sl_marcacoes res, sl_requis req, slv_identif ute "
     If gRegistaColheitaAtiva = mediSim Then
        sSql = sSql & ", sl_req_colheitas colh "
     End If
     sSql = sSql & " WHERE "
     sSql = sSql & " ute.seq_utente = req.seq_utente AND req.n_req = res.n_req AND req.estado_req <> 'C' "
     If gRegistaColheitaAtiva = mediSim Then
        sSql = sSql & " AND req.n_req = colh.n_req "
     End If
     sSql = sSql & " AND req.n_req = " & BL_TrataStringParaBD(EcNumReq.Text) & ")"
     sSql = sSql & " UNION"
     sSql = sSql & " (select req.n_req, ute.seq_utente, ute.t_utente, ute.utente, ute.nome_ute, ute.sexo_ute, req.estado_req, req.dt_colheita, req.hr_colheita, req.dt_chega, req.hr_chega"
     If gRegistaColheitaAtiva = mediSim Then
        sSql = sSql & ", colh.seq_colheita "
     End If
     sSql = sSql & " FROM sl_realiza res, sl_requis req, slv_identif ute "
     If gRegistaColheitaAtiva = mediSim Then
        sSql = sSql & ", sl_req_colheitas colh "
     End If
     sSql = sSql & " WHERE "
     sSql = sSql & " ute.seq_utente = req.seq_utente AND req.n_req = res.n_req AND req.estado_req <> 'C' "
     If gRegistaColheitaAtiva = mediSim Then
        sSql = sSql & " AND req.n_req = colh.n_req "
     End If
     sSql = sSql & " AND req.n_req = " & BL_TrataStringParaBD(EcNumReq.Text) & ")"
     sSql = sSql & " ORDER BY n_req desc"

    rsReq.Open sSql, gConexao
    If rsReq.RecordCount > 0 Then
   
            CbEstadoReq.Text = BL_DevolveEstadoReq(BL_HandleNull(rsReq!estado_req, ""))
            EcNumColheita.Text = BL_HandleNull(COLH_DevolveNumColheita(EcNumReq.Text), "")
            Ectutente.Text = BL_HandleNull(rsReq!t_utente, "")
            EcUtente.Text = BL_HandleNull(rsReq!Utente, "")
            EcNome.Text = BL_HandleNull(rsReq!nome_ute, "")
            If BL_HandleNull(rsReq!sexo_ute, "") = gT_Feminino Then
                PicFem.Visible = True
            ElseIf BL_HandleNull(rsReq!sexo_ute, "") = gT_Masculino Then
                PicMasc.Visible = True
            End If
            dt_colheita = BL_HandleNull(rsReq!dt_colheita, "")
            hr_colheita = BL_HandleNull(rsReq!hr_colheita, "")
            dt_chega = BL_HandleNull(rsReq!hr_colheita, "")
            hr_chega = BL_HandleNull(rsReq!hr_colheita, "")
            
            PreencheCampos
            'PreencheAnalises (EcNumReq.Text)
            PreencheTubos (EcNumColheita.Text)
            PreencheFGTubos
    Else
        FgTubos.Visible = False
        'PreencheFGTubo
        FgTubos.Visible = True

        rsReq.Close
        'LimpaFgReqPend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar Requisi��o:" & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheAnaRequis(ByVal colheita As String, ByVal indice As String, ByVal seq_req_tubo As String)

    Dim SQLQuery As String
    Dim rsAna As New ADODB.recordset
    Dim TotalDadosAnalise As Integer
    'TotalDadosAnalise = 0
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic

    ReDim estrutTuboReutil(indice).analise_req(0)
    On Error GoTo TrataErro
    
    SQLQuery = "(SELECT  distinct x5.cod_agrup, (select x2.descr_ana from slv_analises x2 where x5.cod_agrup = x2.cod_ana) AS descr_ana, x5.seq_req_tubo "
    SQLQuery = SQLQuery & " FROM  sl_tubo x3, sl_req_tubo x4, sl_marcacoes x5, sl_req_colheitas x6 "
    SQLQuery = SQLQuery & " WHERE "
    SQLQuery = SQLQuery & " x4.seq_req_tubo = x5.SEQ_REQ_TUBO "
    SQLQuery = SQLQuery & " AND x5.n_req = x6.n_req AND x3.cod_tubo = x4.cod_tubo "
    SQLQuery = SQLQuery & " AND x5.seq_req_tubo = " & BL_TrataStringParaBD(seq_req_tubo) & " AND nvl(x6.estado_reutil,'0') <> '2' "
    SQLQuery = SQLQuery & " AND x6.seq_colheita = " & BL_TrataStringParaBD(colheita) & " AND x6.n_req <> (" & BL_TrataStringParaBD(n_req) & "))"
    SQLQuery = SQLQuery & " UNION"
    SQLQuery = SQLQuery & " (SELECT  distinct x5.cod_agrup, (select x2.descr_ana from slv_analises x2 where x5.cod_agrup = x2.cod_ana) AS descr_ana, x5.seq_req_tubo "
    SQLQuery = SQLQuery & " FROM  sl_tubo x3, sl_req_tubo x4, sl_realiza x5, sl_req_colheitas x6 "
    SQLQuery = SQLQuery & " WHERE "
    SQLQuery = SQLQuery & " x4.seq_req_tubo = x5.SEQ_REQ_TUBO "
    SQLQuery = SQLQuery & " AND x6.n_req = x6.n_req AND x3.cod_tubo = x4.cod_tubo "
    SQLQuery = SQLQuery & " AND x5.seq_req_tubo = " & BL_TrataStringParaBD(seq_req_tubo) & " AND nvl(x6.estado_reutil,'0') <> '2' "
    SQLQuery = SQLQuery & " AND x6.seq_colheita = " & BL_TrataStringParaBD(colheita) & " AND x6.n_req <> (" & BL_TrataStringParaBD(n_req) & "))"
    SQLQuery = SQLQuery & " ORDER BY cod_agrup "
    
    rsAna.Open SQLQuery, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            TotalDadosAnalise = TotalDadosAnalise + 1
            ReDim Preserve estrutTuboReutil(indice).analise_req(TotalDadosAnalise)
            estrutTuboReutil(indice).analise_req(TotalDadosAnalise).cod_ana = BL_HandleNull(rsAna!cod_agrup, "")
            estrutTuboReutil(indice).analise_req(TotalDadosAnalise).descr_ana = BL_HandleNull(rsAna!descr_ana, "")
            estrutTuboReutil(indice).analise_req(TotalDadosAnalise).seq_req_tubo = BL_HandleNull(rsAna!seq_req_tubo, "")

        rsAna.MoveNext
        Wend
    End If

Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, Me.Name, "PreencheReutilRequis", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheTubos(colheita As String)

    Dim SQLQuery As String
    Dim rsTubo As New ADODB.recordset
    Dim TotalDadosAnalise As Integer
    'TotalDadosAnalise = 0
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    
    On Error GoTo TrataErro
    
    SQLQuery = "(select distinct x2.cod_tubo, x1.descr_tubo, x2.dt_chega, x2.hr_chega, x2.dt_colheita, x2.hr_colheita, x2.seq_req_tubo, x2.n_req "
    SQLQuery = SQLQuery & " FROM  sl_tubo x1, sl_req_tubo x2 LEFT JOIN sl_marcacoes x3 "
    SQLQuery = SQLQuery & " ON x2.seq_req_tubo = x3.seq_req_tubo "
    SQLQuery = SQLQuery & " AND x3.n_req = x2.n_req, sl_req_colheitas x4 "
    SQLQuery = SQLQuery & " WHERE "
    SQLQuery = SQLQuery & " x2.cod_tubo = x1.cod_tubo AND x2.n_req = x4.n_req AND nvl(x4.estado_reutil,0) <> '2' "
    SQLQuery = SQLQuery & " AND x4.seq_colheita = " & BL_TrataStringParaBD(colheita) & ")"
    SQLQuery = SQLQuery & " UNION"
    SQLQuery = SQLQuery & " (select distinct x2.cod_tubo, x1.descr_tubo, x2.dt_chega, x2.hr_chega, x2.dt_colheita, x2.hr_colheita, x2.seq_req_tubo, x2.n_req "
    SQLQuery = SQLQuery & " FROM  sl_tubo x1, sl_req_tubo x2 LEFT JOIN sl_realiza x3 "
    SQLQuery = SQLQuery & " ON x2.seq_req_tubo = x3.seq_req_tubo "
    SQLQuery = SQLQuery & " AND x3.n_req = x2.n_req, sl_req_colheitas x4 "
    SQLQuery = SQLQuery & " WHERE "
    SQLQuery = SQLQuery & " x2.cod_tubo = x1.cod_tubo AND x2.n_req = x4.n_req AND nvl(x4.estado_reutil,0) <> '2' "
    SQLQuery = SQLQuery & " AND x2.n_req = " & BL_TrataStringParaBD(colheita) & ")"
    SQLQuery = SQLQuery & " ORDER BY dt_colheita ASC, hr_colheita ASC"
    
    rsTubo.Open SQLQuery, gConexao
    If rsTubo.RecordCount > 0 Then
        While Not rsTubo.EOF
        PreencheTubosRequis BL_HandleNull(rsTubo!cod_tubo, ""), BL_HandleNull(rsTubo!descR_tubo, " "), _
                                   BL_HandleNull(rsTubo!dt_chega, ""), _
                                   BL_HandleNull(rsTubo!hr_chega, ""), _
                                   BL_HandleNull(rsTubo!dt_colheita, ""), BL_HandleNull(rsTubo!hr_colheita, ""), _
                                   BL_HandleNull(rsTubo!seq_req_tubo, ""), BL_HandleNull(rsTubo!n_req, "")

        rsTubo.MoveNext
        Wend
    End If


Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, Me.Name, "PreencheReutilRequis", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheTubosRequis(cod_tubo As String, descR_tubo As String, dt_chega As String, _
                                 hr_chega As String, dt_colheita As String, hr_colheita As String, _
                                 seq_req_tubo As String, n_req As String)
    On Error GoTo TrataErro

    TotalDadosTubo = TotalDadosTubo + 1
    ReDim Preserve estrutTuboReutil(TotalDadosTubo)
    estrutTuboReutil(TotalDadosTubo).cod_tubo = cod_tubo
    estrutTuboReutil(TotalDadosTubo).descR_tubo = descR_tubo
    estrutTuboReutil(TotalDadosTubo).dt_chegada = dt_chega
    estrutTuboReutil(TotalDadosTubo).hr_chegada = hr_chega
    estrutTuboReutil(TotalDadosTubo).dt_colheita = dt_colheita
    estrutTuboReutil(TotalDadosTubo).hr_colheita = hr_colheita
    estrutTuboReutil(TotalDadosTubo).seq_req_tubo = seq_req_tubo
    estrutTuboReutil(TotalDadosTubo).seq_req_tubo = seq_req_tubo
    estrutTuboReutil(TotalDadosTubo).n_req = n_req
        
    PreencheAnaRequis EcNumColheita.Text, TotalDadosTubo, seq_req_tubo
    PreencheTuboInterf estrutTuboReutil(TotalDadosTubo).n_req, TotalDadosTubo, seq_req_tubo
    

Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro ao Preeencher Estrutura An�lises:" & Err.Description, Me.Name, "PreencheReutilRequis", True
    Exit Sub
    Resume Next
End Sub



Private Sub PreencheTuboInterf(ByVal n_req As String, ByVal indice As String, ByVal seq_req_tubo As String)

    Dim Query As String
    Dim rsInterf As New ADODB.recordset
    Dim TotalInterf As Integer
    rsInterf.CursorLocation = adUseServer
    rsInterf.CursorType = adOpenStatic

    ReDim estrutTuboReutil(indice).interferencia(0)
    On Error GoTo TrataErro
    
    Query = "select  x1.cod_interferencia, x1.descr_interferencia, x2.resultado, x2.seq_req_tubo "
    Query = Query & " FROM  sl_interferencias x1, sl_res_interferencias x2, sl_req_tubo x3"
    Query = Query & " WHERE "
    Query = Query & " x3.seq_req_tubo = x2.SEQ_REQ_TUBO "
    Query = Query & " AND x3.n_req = x2.n_req "
    Query = Query & " AND x2.cod_interferencia = x1.cod_interferencia "
    Query = Query & " AND x3.SEQ_REQ_TUBO = " & BL_TrataStringParaBD(seq_req_tubo) & ""
    Query = Query & " AND x2.n_req = " & BL_TrataStringParaBD(n_req) & " "
    Query = Query & " ORDER BY descr_interferencia ASC"
    'query = query & " ORDER BY descr_interferencia DESC, descr_interferencia DESC"
    
    rsInterf.Open Query, gConexao
    If rsInterf.RecordCount > 0 Then
        While Not rsInterf.EOF
        
            TotalInterf = TotalInterf + 1
            ReDim Preserve estrutTuboReutil(indice).interferencia(TotalInterf)
            estrutTuboReutil(indice).interferencia(TotalInterf).cod_interf = BL_HandleNull(rsInterf!cod_Interferencia, "")
            estrutTuboReutil(indice).interferencia(TotalInterf).descr_interf = BL_HandleNull(rsInterf!descr_interferencia, "")
            estrutTuboReutil(indice).interferencia(TotalInterf).resultado = BL_HandleNull(rsInterf!resultado, "")
            estrutTuboReutil(indice).interferencia(TotalInterf).seq_req_tubo = BL_HandleNull(rsInterf!seq_req_tubo, "")
                               
        rsInterf.MoveNext
        Wend
    End If
    
Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro ao Procurar Interfer�ncias:" & Err.Description, Me.Name, "PreencheReutilRequis", True
    Exit Sub
    Resume Next
End Sub


Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormValReutilColheita = Nothing
    FormReutilColheitas.Enabled = True

End Sub

Sub DefTipoCampos()
    
    With FgTubos
        .rows = 9
        .FixedRows = 1
        .Cols = 6 + ContaColunas
        .FixedCols = 0
        .WordWrap = True
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeRestrictColumns
        .PictureType = flexPictureColor
        .RowHeightMin = 390
        
        .TextMatrix(0, lColDtColheita) = "Data colheita"
        .ColWidth(lColDtColheita) = 1100
        .Col = lColDtColheita
        
        .ColAlignment(lColDtColheita) = flexAlignCenterCenter
        
        .TextMatrix(0, lColDtRececao) = "Data rece��o"
        .ColWidth(lColDtRececao) = 1100
        .Col = lColDtRececao
        
        .ColAlignment(lColDtRececao) = flexAlignCenterCenter
        
        .TextMatrix(0, lColAmostra) = "Amostra"
        .ColWidth(lColAmostra) = 1200
        .Col = lColAmostra
        
        
        .TextMatrix(0, lColAnalise) = "An�lise"
        .ColWidth(lColAnalise) = 1400
        .Col = lColAnalise
        
        .TextMatrix(0, lColEstado) = "Estado"
        .ColWidth(lColEstado) = 1000
        .Col = lColEstado
        
        
'        For k = lColEstado + 1 To colunas
'            For z = 1 To estrutAnaliseReutil.totalAnalises
'                .TextMatrix(0, i) = estrutAnaliseReutil(z).interferencia.descr_interf
'                .ColWidth(i) = 1000
'                .Col = i
'                .CellAlignment = flexAlignCenterCenter
'            Next z
'        Next k

        'Next
        'Dim j As Integer
            
        'For j = 1 To UBound(estrutReqReutilCopy(Index).analises)
        '   .TextMatrix(0, 1) = estrutReqReutilCopy(Index).analises(1).interferencias(1).descr_interf
       '
        ColunasDinamicas

        .row = 1
        .Col = 0
    End With
    FgTubos.MergeCol(lColDtColheita) = True
    FgTubos.MergeCol(lColDtRececao) = True
    FgTubos.MergeCol(lColAmostra) = True
    FgTubos.MergeCol(lColEstado) = True
    
End Sub

Sub PreencheValoresDefeito()
    'BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
       
End Sub

Sub Inicializacoes()
    
    Me.caption = "Reutiliza��o de Colheita"
    Me.left = 440
    Me.top = 350
    Me.Width = 11000
    Me.Height = 7500 ' Normal
    'Me.Height = 6330 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
'
'    ' Texto para a mensagem nos campos obrigat�rios
'    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
 '   TextoCamposObrigatorios(0) = "Motivo"
    ReDim reqColh(0)
    TotalReqColh = 0

    Set CampoDeFocus = EcNumReq


End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub


Sub PreencheFGTubos()

    On Error GoTo TrataErro
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim z As Integer
    'LimpaFgTubo
    Dim linhaAtual As Integer
    linhaAtual = 1
    
   For i = 1 To TotalDadosTubo
        FgTubos.TextMatrix(linhaAtual, lColDtColheita) = estrutTuboReutil(i).dt_colheita & vbNewLine & estrutTuboReutil(i).hr_colheita
        FgTubos.TextMatrix(linhaAtual, lColDtRececao) = estrutTuboReutil(i).dt_chegada & vbNewLine & estrutTuboReutil(i).hr_chegada
        FgTubos.TextMatrix(linhaAtual, lColAmostra) = estrutTuboReutil(i).descR_tubo
               
        For j = 1 To UBound(estrutTuboReutil(i).analise_req)
            FgTubos.TextMatrix(linhaAtual, lColDtColheita) = estrutTuboReutil(i).dt_colheita & vbNewLine & estrutTuboReutil(i).hr_colheita
            FgTubos.TextMatrix(linhaAtual, lColDtRececao) = estrutTuboReutil(i).dt_chegada & vbNewLine & estrutTuboReutil(i).hr_chegada
            FgTubos.TextMatrix(linhaAtual, lColAmostra) = estrutTuboReutil(i).descR_tubo
            FgTubos.TextMatrix(linhaAtual, lColAnalise) = estrutTuboReutil(i).analise_req(j).descr_ana
            FgTubos.TextMatrix(linhaAtual, lColEstado) = CbEstadoReq.Text
            
            For z = 1 To UBound(estrutTuboReutil(i).interferencia)
                For k = lColEstado To colunas
                    If FgTubos.TextMatrix(0, k) = estrutTuboReutil(i).interferencia(z).descr_interf Then
                        FgTubos.TextMatrix(linhaAtual, k) = estrutTuboReutil(i).interferencia(z).resultado
                    End If
                Next k
            Next z
            linhaAtual = linhaAtual + 1
            If linhaAtual > 8 Then
                FgTubos.AddItem "", linhaAtual
            End If
        Next j
         

        'FgTubo.TextMatrix(i, lColLocalizacao) = estrutTuboReutil(i).localizacao

    FgTubos.AddItem ""

    Next i
    'FgTubo.row = 1
    'LaProduto.caption = estrutTubos(1).DescrProduto
    
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao preencher FGTubos:" & Err.Description, Me.Name, "PreencheFGTubos", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheCampos()
    
    BtConfirm.Visible = False
    LabelConsulta.Visible = False
          
    BG_PreencheComboBD_ADO "sl_cod_motivo_n_reutil", "cod_motivo", "descr_motivo", CbMotivo
    
    estado_reutil = gPassaParams.Param(0)
    dt_val_canc_reutil = gPassaParams.Param(1)
    hr_val_canc_reutil = gPassaParams.Param(2)
    user_val_canc_reutil = gPassaParams.Param(3)
    motivo_n_reutil = gPassaParams.Param(4)
    obs_reutil = gPassaParams.Param(5)
    n_presc = gPassaParams.Param(6)
    cod_local = gPassaParams.Param(7)
    n_req = gPassaParams.Param(8)

    If estado_reutil = "Validado" Then
      BtConfirm.Visible = True
      EcObs.Enabled = False
      CbMotivo.Visible = False
      BtCancelar.Visible = False
      BtValida.Visible = False
      LabelMotivo.Visible = False
      LabelConsulta.Visible = True
      LabelConsulta.caption = "Reutiliza��o Aceite" & vbNewLine & BL_DevolveNomeUtilizadorActual(user_val_canc_reutil) & vbNewLine & "Data: " & dt_val_canc_reutil & " " & _
                              hr_val_canc_reutil & ""
    End If
    
    If estado_reutil = "Recusado" Then
      CbMotivo.Enabled = False
      EcObs.Enabled = False
      BtConfirm.Enabled = False
      BtCancelar.Visible = False
      BtValida.Visible = False
      BtConfirm.Visible = False
      CbMotivo.Text = BL_SelCodigo("sl_cod_motivo_n_reutil", "descr_motivo", "cod_motivo", motivo_n_reutil)
      LabelConsulta.Visible = True
      LabelConsulta.caption = "Reutiliza��o Recusada" & vbNewLine & BL_DevolveNomeUtilizadorActual(user_val_canc_reutil) & vbNewLine & "Data: " & dt_val_canc_reutil & " " & _
                               hr_val_canc_reutil & ""
    End If
    
    
    'EcNumReq.Text = estrutReqReutilCopy(Index).n_req
    'CbEstadoReq.Text = estrutReqReutilCopy(Index).estado_reutil
    'Ectutente.Text = estrutReqReutilCopy(Index).t_utente
    'EcUtente.Text = estrutReqReutilCopy(Index).utente
    'EcNome.Text = estrutReqReutilCopy(Index).nome_ute
     EcObs.Text = obs_reutil
    'EcNumColheita.Text = DevolveNumColheita(estrutReqReutilCopy(Index).n_req)
    'PreencheFGTubo Index
    
      
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao tratar da estrutura:" & Err.Description, Me.Name, "TrataEstrutura", True
    Exit Sub
    Resume Next
End Sub

Public Function DevolveEstadoReq(Estado As String) As String
    Dim sSql As String
    Dim RsEstado As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT * FROM SL_TBF_ESTADO_REQ where cod_estado = " & BL_TrataStringParaBD(Estado)
    RsEstado.CursorType = adOpenStatic
    RsEstado.CursorLocation = adUseServer
    RsEstado.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEstado.Open sSql, gConexao
        If RsEstado.RecordCount > 0 Then
            DevolveEstadoReq = BL_HandleNull(RsEstado!descr_estado_externo, "")
        Else
            DevolveEstadoReq = ""
        End If
    RsEstado.Close
    Set RsEstado = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "DevolveEstadoReq - ERRO: " & Err.Description, Me.Name, "DevolveEstadoReq", True
    DevolveEstadoReq = ""
    Exit Function
    Resume Next
End Function

Sub ColunasDinamicas()

    Dim sql As String
    Dim rsCol As New ADODB.recordset
    Dim i As Integer
    i = lColEstado
    
    On Error GoTo TrataErro
    'sl_res_interferencias x2 WHERE x1.cod_interferencia = x2.cod_interferencia
    sql = "SELECT distinct x1.descr_interferencia FROM sl_interferencias x1 order by x1.descr_interferencia ASC "
    rsCol.CursorType = adOpenStatic
    rsCol.CursorLocation = adUseServer
    rsCol.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rsCol.Open sql, gConexao
        If rsCol.RecordCount > 0 Then
            While Not rsCol.EOF
                 i = i + 1
                FgTubos.TextMatrix(0, i) = BL_HandleNull(rsCol!descr_interferencia, "")
                FgTubos.ColWidth(i) = 1000
                FgTubos.Col = i
                FgTubos.ColAlignment(i) = flexAlignCenterCenter
                FgTubos.MergeCol(i) = True
            rsCol.MoveNext
            
            Wend
                FgTubos.TextMatrix(0, i + 1) = "Localiza��o"
                FgTubos.ColWidth(i + 1) = 1200
                FgTubos.Col = i + 1
                colunas = i
        Else
            FgTubos.TextMatrix(0, lColEstado) = "Localiza��o"
            FgTubos.ColWidth(lColLocalizacao) = 1200
            FgTubos.Col = lColLocalizacao
        End If
    rsCol.Close
    Set rsCol = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao carregar as colunas: " & Err.Description, Me.Name, "ColunasDinamicas", True
    Exit Sub
    Resume Next
End Sub

Private Function ContaColunas() As Integer

    Dim sql As String
    Dim rsConta As New ADODB.recordset
    Dim conta As Integer
    
    On Error GoTo TrataErro
    'sl_res_interferencias x2 WHERE x1.cod_interferencia = x2.cod_interferencia order by descr_interferencia ASC
    sql = "SELECT count(distinct(x1.cod_interferencia)) colunas FROM sl_interferencias x1 "
    rsConta.CursorType = adOpenStatic
    rsConta.CursorLocation = adUseServer
    rsConta.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rsConta.Open sql, gConexao
        If rsConta.RecordCount > 0 Then
            ContaColunas = BL_HandleNull(rsConta!colunas, 0)
        Else
            ContaColunas = 0
        End If
    rsConta.Close
    Set rsConta = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao criar as colunas: " & Err.Description, Me.Name, "ColunasDinamicas", True
    Exit Function
    Resume Next
End Function


VERSION 5.00
Begin VB.Form FormEstatProduto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5295
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7665
   Icon            =   "FormEstatProduto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5295
   ScaleWidth      =   7665
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   3615
      Left            =   120
      TabIndex        =   5
      Top             =   1080
      Width           =   7335
      Begin VB.CheckBox CkRequisicoes 
         Caption         =   "Descriminar Requisi��es"
         Height          =   255
         Left            =   360
         TabIndex        =   27
         Top             =   3240
         Width           =   3255
      End
      Begin VB.CommandButton BtPesquisaGrupo 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatProduto.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   2880
         Width           =   375
      End
      Begin VB.TextBox EcDescrGrupo 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   2880
         Width           =   4095
      End
      Begin VB.TextBox EcCodGrupo 
         Height          =   315
         Left            =   1560
         TabIndex        =   23
         Top             =   2880
         Width           =   735
      End
      Begin VB.ComboBox CbProduto 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   360
         Width           =   1455
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   840
         Width           =   1455
      End
      Begin VB.CheckBox Flg_DescrRelsSC 
         Caption         =   "Descriminar Relat�rios Sem Crescimento"
         Height          =   255
         Left            =   3960
         TabIndex        =   14
         Top             =   3240
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1320
         Width           =   1455
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   1320
         Width           =   1455
      End
      Begin VB.TextBox EcCodEFR 
         Height          =   315
         Left            =   1560
         TabIndex        =   11
         Top             =   2400
         Width           =   735
      End
      Begin VB.TextBox EcDescrEFR 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   2400
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatProduto.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   2400
         Width           =   375
      End
      Begin VB.TextBox EcCodProveniencia 
         Height          =   315
         Left            =   1560
         TabIndex        =   8
         Top             =   1920
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   1920
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatProduto.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1920
         Width           =   375
      End
      Begin VB.Label Label10 
         Caption         =   "&Grupo An�lises"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   26
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Pro&duto"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   21
         Top             =   840
         Width           =   360
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   4440
         TabIndex        =   20
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   19
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   18
         Top             =   2400
         Width           =   495
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   17
         Top             =   1920
         Width           =   975
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   3
         Top             =   360
         Width           =   255
      End
   End
End
Attribute VB_Name = "FormEstatProduto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 09/10/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object


Public rs As ADODB.recordset
Dim CountRegs As Long
'NELSONPSILVA Glintt-HS-18011 09.02.2018
Dim tabela_aux As String
'

Sub Preenche_EstatisticaProduto()
    
    Dim sql As String
    Dim continua As Boolean
            
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estatistica por Produto") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("EstatisticaProduto", "Estatistica por Produto", crptToPrinter)
    Else
        continua = BL_IniciaReport("EstatisticaProduto", "Estatistica por Produto", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    PreencheTabelaTemporaria
    
    If CountRegs = 0 Then
        Call BG_Mensagem(mediMsgBox, "N�o foi seleccionado nenhum produto!", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    End If
 
    BG_BeginTransaction
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    
    Report.SQLQuery = ""
    'Report.SQLQuery = "SELECT SL_CR_ESTATPRODUTO.COD_PROD,SL_CR_ESTATPRODUTO.TIPO_REL,SL_CR_ESTATPRODUTO.UTENTE,SL_CR_ESTATPRODUTO.NOME_UTE,SL_CR_ESTATPRODUTO.REQ_AUX,SL_CR_ESTATPRODUTO.COD_EFR,SL_CR_ESTATPRODUTO.N_BENEF" & _
        " FROM SL_CR_ESTATPRODUTO" & _
        " ORDER BY SL_CR_ESTATPRODUTO.COD_PROD,"
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    If EcCodEFR = "" Then
        Report.formulas(2) = "EFR=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(2) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEFR.Text)
    End If
    If EcCodProveniencia = "" Then
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & EcDescrProveniencia.Text)
    End If
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("Normal")
    Else
        Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    End If
    If CbSituacao.ListIndex = -1 Then
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.Text)
    End If
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
    End If
    If CbProduto.ListIndex = -1 Then
        Report.formulas(7) = "Produtos= " & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(7) = "Produtos= " & BL_TrataStringParaBD("" & CbProduto.Text)
    End If
    If Flg_DescrRelsSC.value = 1 Then
        Report.formulas(8) = "DescrRelsSC=" & BL_TrataStringParaBD(" S")
    Else
        Report.formulas(8) = "DescrRelsSC=" & BL_TrataStringParaBD(" N")
    End If
    If gMultiReport = 1 Then
        Report.formulas(9) = "Cliente=" & BL_TrataStringParaBD("IGM")
    Else
        Report.formulas(9) = "Cliente=" & BL_TrataStringParaBD("Outro")
    End If
    
    If CkRequisicoes.value = vbChecked Then
        Report.formulas(11) = "DescrReq=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(11) = "DescrReq=" & BL_TrataStringParaBD("N")
    End If
    
    If Flg_DescrRelsSC.value = 1 Then
        Report.formulas(10) = "DescrRelsSC=" & BL_TrataStringParaBD(" S")
    Else
        Report.formulas(10) = "DescrRelsSC=" & BL_TrataStringParaBD(" N")
    End If
'
'    Report.SubreportToChange = ""
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
    
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    sql = "DELETE FROM SL_CR_ESTATPRODUTOREL"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    sql = "DELETE FROM SL_CR_ESTATPRODUTO"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro

End Sub

Sub PreencheTabelaTemporaria()

    Dim sql As String
    Dim rsReq As ADODB.recordset
    Dim RsProd As ADODB.recordset
    Dim RsRel As ADODB.recordset
    Dim RsDoc As ADODB.recordset
    Dim SqlDoc As String
    Dim SqlRel, SeqRel, Utente, SqlS, SqlI
    Dim SqlProd As String
    Dim i, n, NRels As Integer
    
    sql = "DELETE from sl_cr_estatproduto"
    BG_ExecutaQuery_ADO sql
    
    SqlI = "INSERT INTO SL_CR_ESTATPRODUTO(N_REQ,UTENTE,NOME_UTE,REQ_AUX,COD_EFR,N_BENEF,COD_PROD,TIPO_REL) "
    If gSGBD = gOracle Then
        SqlS = "select sl_requis.n_req,t_utente || '/' || utente NUtente,nome_ute,req_aux,cod_efr,n_benef,cod_prod," & NRels & _
              " from sl_requis, sl_req_prod, " & tabela_aux & ", sl_realiza, slv_analises where" & _
              " sl_requis.n_req = sl_req_prod.n_req and " & _
              " sl_requis.seq_utente = " & tabela_aux & ".seq_utente AND " & _
              " sl_requis.n_Req= sl_realiza.n_req AND " & _
              " sl_realiza.cod_agrup = slv_analises.cod_ana "
    ElseIf gSGBD = gSqlServer Then
        SqlS = "select  sl_requis.n_req,t_utente + '/' + utente NUtente,nome_ute,req_aux,cod_efr,n_benef,cod_prod," & NRels & _
              " from sl_requis, sl_req_prod, " & tabela_aux & ", sl_realiza, slv_analises where" & _
              " sl_requis.n_req = sl_req_prod.n_req and " & _
              " sl_requis.seq_utente = " & tabela_aux & ".seq_utente AND " & _
              " sl_requis.n_Req= sl_realiza.n_req AND " & _
              " sl_realiza.cod_agrup = slv_analises.cod_ana "
    End If
    SqlS = SqlS & ConstroiCriterio
    If gSGBD = gOracle Then
        SqlS = SqlS & " UNION select sl_requis.n_req,t_utente || '/' || utente NUtente,nome_ute,req_aux,cod_efr,n_benef,cod_prod," & NRels & _
              " from sl_requis, sl_req_prod, " & tabela_aux & ", sl_marcacoes, slv_analises where" & _
              " sl_requis.n_req = sl_req_prod.n_req and " & _
              " sl_requis.seq_utente = " & tabela_aux & ".seq_utente AND " & _
              " sl_requis.n_Req= sl_marcacoes.n_req AND " & _
              " sl_marcacoes.cod_agrup = slv_analises.cod_ana "
    ElseIf gSGBD = gSqlServer Then
        SqlS = SqlS & " UNION select  sl_requis.n_req,t_utente + '/' + utente NUtente,nome_ute,req_aux,cod_efr,n_benef,cod_prod," & NRels & _
              " from sl_requis, sl_req_prod, " & tabela_aux & ", sl_marcacoes, slv_analises where" & _
              " sl_requis.n_req = sl_req_prod.n_req and " & _
              " sl_requis.seq_utente = " & tabela_aux & ".seq_utente AND " & _
              " sl_requis.n_Req= sl_marcacoes.n_req AND " & _
              " sl_marcacoes.cod_agrup = slv_analises.cod_ana "
    End If
    SqlS = SqlS & ConstroiCriterio
    
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    rsReq.Source = SqlS
    rsReq.ActiveConnection = gConexao
    rsReq.Open
    CountRegs = rsReq.RecordCount
    NRels = 0
    BG_ExecutaQuery_ADO SqlI & SqlS
    BG_Trata_BDErro
    
    If gMultiReport <> 1 Then
        
'        NRels = 0
'        BG_ExecutaQuery_ADO SqlI & SqlS
'        BG_Trata_BDErro
    Else
    
        sql = "delete from sl_cr_estatprodutorel"
        BG_ExecutaQuery_ADO sql
        
    End If
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub BtPesquisaProveniencia_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
        
End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, CStr(gCodLocal))
End Sub
      
Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.Text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.Text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Estatistica por Produto"
    Me.left = 540
    Me.top = 450
    Me.Width = 7725
    Me.Height = 5175 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodProveniencia
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If gMultiReport = 1 Then
        Flg_DescrRelsSC.Visible = True
    End If
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormEstatProduto = Nothing
    
    Call BL_FechaPreview("Estatistica por Produto")
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    CbProduto.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    EcDescrSexo.ListIndex = mediComboValorNull
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    EcCodEFR.Text = ""
    EcDescrEFR.Text = ""
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    Flg_DescrRelsSC.value = 0
    EcCodGrupo = ""
    EcDescrGrupo = ""
    
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"
    EcCodEFR.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodProveniencia.MaxLength = 5
    EcCodEFR.MaxLength = 9
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_produto", "seq_produto", "descr_produto", CbProduto
    
    'Preenche Combo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_EstatisticaProduto
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_EstatisticaProduto

End Sub







Private Sub BtPesquisaGrupo_Click()

'    FormPesquisaRapida.InitPesquisaRapida "sl_gr_ana", _
'                        "descr_gr_ana", "seq_gr_ana", _
'                         EcPesqRapGrupo, , "cod_local = " & gCodLocal

    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    ClausulaWhere = "cod_local = " & gCodLocal
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.Text = resultados(1)
            EcDescrGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrupo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE upper(cod_gr_ana)= " & BL_TrataStringParaBD(UCase(EcCodGrupo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrupo.Text = "" & RsDescrGrupo!cod_gr_ana
            EcDescrGrupo.Text = "" & RsDescrGrupo!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrupo.Text = ""
    End If

End Sub


Private Function ConstroiCriterio() As String
    Dim RsProd As New ADODB.recordset
    Dim SqlProd As String
    ConstroiCriterio = ""
    
    'Situa��o preenchida?
    If (EcCodGrupo <> "") Then
        ConstroiCriterio = ConstroiCriterio & " AND slv_analises.cod_gr_ana= " & EcCodGrupo
    End If
    
    'verifica os campos preenchidos
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        ConstroiCriterio = ConstroiCriterio & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'Data preenchida
    ConstroiCriterio = ConstroiCriterio & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
    
    'Situa��o preenchida?
    If (CbSituacao.ListIndex <> -1) Then
        ConstroiCriterio = ConstroiCriterio & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        ConstroiCriterio = ConstroiCriterio & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If EcCodProveniencia.Text <> "" Then
        ConstroiCriterio = ConstroiCriterio & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    
    'C�digo da EFR preenchido?
    If EcCodEFR.Text <> "" Then
        ConstroiCriterio = ConstroiCriterio & " AND sl_requis.Cod_efr=" & BL_TrataStringParaBD(EcCodEFR.Text)
    End If
    
    'C�digo do Produto preenchido?
    If CbProduto.Text <> "" Then
        Set RsProd = New ADODB.recordset
        RsProd.CursorLocation = adUseServer
        RsProd.CursorType = adOpenStatic
        SqlProd = "select cod_produto from sl_produto where descr_produto = " & BL_TrataStringParaBD(CbProduto.Text)
        RsProd.Open SqlProd, gConexao
        If RsProd.RecordCount > 0 Then
            ConstroiCriterio = ConstroiCriterio & " AND sl_req_prod.Cod_prod=" & BL_TrataStringParaBD(RsProd!cod_produto)
        End If
    End If
    

End Function

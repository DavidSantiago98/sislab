Attribute VB_Name = "BibliotecaTraducao"
'MdiFormLoad
'public  IDS_TOOLBAR1_BT_IMPRIMIR = "Imprimir (Teste)"
'public  IDS_TOOLBAR1_BT_IMPRIMIR_VER_ANTES = "Imprimir (Ver Antes)"
'public  IDS_TOOLBAR1_BT_LIMPAR = "Limpar"
'public  IDS_TOOLBAR1_BT_INSERIR = "Validar Inser��o"
'public  IDS_TOOLBAR1_BT_PROCURAR = "Procurar"
'public  IDS_TOOLBAR1_BT_MODIFICAR = "Modificar"
'public  IDS_TOOLBAR1_BT_REMOVER = "Remover"
'public  IDS_TOOLBAR1_BT_REG_ANTERIOR = "Registo Anterior"
'public  IDS_TOOLBAR1_BT_RE_SEGUINTE = "Registo Seguinte"
'public  IDS_TOOLBAR1_BT_ESTADO_ANTERIOR = "Estado Anterior"
'public  IDS_TOOLBAR1_BT_SAIR = "Sair"
'
'public  IDS_IDM_FICHEIRO_MUDAR_SENHA = "Mudar Senha"
'public  IDS_IDM_FICHEIRO_DATA_ACTUAL = "Data Actual"
'public  IDS_IDM_FICHEIRO_CONF_IMPRESSORA = "Configura��o da Impressora"
'public  IDS_IDM_FICHEIRO_IMPR_VER_ANTES = "Imprimir (Ver Antes)"
'public  IDS_IDM_FICHEIRO_IMPRIMIR = "Imprimir"


'Form Codifica��es
Public IDS_MSG_REGISTO_EXISTENTE

Public IDS_MSG_FUNCAO_PROCURAR
Public IDS_MSG_FUNCAO_PROCURAR_TITULO

Public IDS_MSG_FUNCAO_INSERIR
Public IDS_MSG_FUNCAO_INSERIR_TITULO
Public IDS_MSG_FUNCAO_MODIFICAR
Public IDS_MSG_FUNCAO_MODIFICAR_TITULO
Public IDS_MSG_FUNCAO_REMOVER
Public IDS_MSG_FUNCAO_REMOVER_TITULO
Public IDS_MSG_FUNCAO_REMOVER_REG_UNICO

Public IDS_MSG_REGISTO_SEGUINTE
Public IDS_MSG_REGISTO_SEGUINTE_TITULO
Public IDS_MSG_REGISTO_ANTERIOR
Public IDS_MSG_REGISTO_ANTERIOR_TITULO
Public IDS_MSG_IMPRIMIR

Public IDS_MSG_INI_0
Public IDS_MSG_INI_1
Public IDS_MSG_INI_2
Public IDS_MSG_INI_3
Public IDS_MSG_INI_4
Public IDS_MSG_INI_5
Public IDS_MSG_INI_6

Public IDS_MSG_ERRO_BD

Public IDS_MSG_CAMPO_OBRIGATORIO

'Form MDIFormInicio
Public IDS_MSG_MDI_DEMO
Public IDS_MSG_MDI_TIPO_BD
Public IDS_MSG_MDI_ERRO_FATAL
Public IDS_MSG_OPERACAO_N_DISP

Public IDS_MSG_EM_PROCESSAMENTO



Sub BT_CarregaIdiomaIngles()
    IDS_MSG_REGISTO_EXISTENTE = "Record already exists"
    IDS_MSG_FUNCAO_INSERIR = "Are you sure you want to insert the record?"
    IDS_MSG_FUNCAO_INSERIR_TITULO = "Insert"
    IDS_MSG_FUNCAO_PROCURAR = "Record not found!"
    IDS_MSG_FUNCAO_PROCURAR_TITULO = "Search"
    IDS_MSG_FUNCAO_MODIFICAR = "Are you sure you want to update the record?"
    IDS_MSG_FUNCAO_MODIFICAR_TITULO = "Update"
    IDS_MSG_FUNCAO_REMOVER = "Are you sure you want to delete the record?"
    IDS_MSG_FUNCAO_REMOVER_TITULO = "Delete"
    IDS_MSG_FUNCAO_REMOVER_REG_UNICO = "The record you deleted is the only record from the search!"
    IDS_MSG_REGISTO_SEGUINTE = "No more records !"
    IDS_MSG_REGISTO_SEGUINTE_TITULO = "Next Record"
    IDS_MSG_REGISTO_ANTERIOR = "No more records !"
    IDS_MSG_REGISTO_ANTERIOR_TITULO = "Previous Record"
    IDS_MSG_INI_0 = "Are you sure you want to save this information?"
    IDS_MSG_INI_1 = " You must select a file!"
    IDS_MSG_INI_2 = "Exit without save !"
    IDS_MSG_INI_3 = "Are you sure you want to exit?"
    IDS_MSG_INI_4 = "You must restart the application to make the changes take effect !"
    IDS_MSG_INI_5 = "Do you want to exit the application?"
    IDS_MSG_INI_6 = "It is better to terminate the application and restart again!"
    IDS_MSG_ERRO_BD = "Database transaction error!!"
    IDS_MSG_MDI_DEMO = "Demo Version"
    IDS_MSG_MDI_TIPO_BD = "You should indicate the source of the database: Local or Remote"
    IDS_MSG_MDI_ERRO_FATAL = "Fatal Error No. "
    IDS_MSG_IMPRIMIR = "Printing..."
    IDS_MSG_OPERACAO_N_DISP = "Operation not available"
    IDS_MSG_CAMPO_OBRIGATORIO = "Required field"
    IDS_MSG_EM_PROCESSAMENTO = "Processing..."
End Sub
 
Sub BT_CarregaIdiomaPortugues()
    IDS_MSG_REGISTO_EXISTENTE = "Registo j� existe"
    IDS_MSG_FUNCAO_PROCURAR = "N�o foi seleccionado nenhum registo"
    IDS_MSG_FUNCAO_PROCURAR_TITULO = "Procurar"
    IDS_MSG_FUNCAO_INSERIR = "Tem a certeza que quer inserir este registo?"
    IDS_MSG_FUNCAO_INSERIR_TITULO = "Inserir"
    IDS_MSG_FUNCAO_MODIFICAR = "Tem a certeza que quer validar as altera��es?"
    IDS_MSG_FUNCAO_MODIFICAR_TITULO = "Modificar"
    IDS_MSG_FUNCAO_REMOVER = "Tem a certeza que quer apagar o registo?"
    IDS_MSG_FUNCAO_REMOVER_TITULO = "Remover"
    IDS_MSG_FUNCAO_REMOVER_REG_UNICO = "O registo removido era �nico nesta pesquisa !"
    IDS_MSG_REGISTO_SEGUINTE = "N�o existem mais registos !"
    IDS_MSG_REGISTO_SEGUINTE_TITULO = "Registo seguinte"
    IDS_MSG_REGISTO_ANTERIOR = "N�o existem mais registos !"
    IDS_MSG_REGISTO_ANTERIOR_TITULO = "Registo Anterior"
    IDS_MSG_INI_0 = "Tem a certeza que quer guardar estas informa��es?"
    IDS_MSG_INI_1 = "� necess�rio indicar um ficheiro"
    IDS_MSG_INI_2 = "Vai sa�r sem guardar"
    IDS_MSG_INI_3 = "Tem a certeza que quer sa�r"
    IDS_MSG_INI_4 = "Ter� que reinicializar a aplica��o para que as altera��oes efectuadas tenham efeito."
    IDS_MSG_INI_5 = "Quer sa�r j� da aplica��o?"
    IDS_MSG_INI_6 = "Conv�m terminar o que estava a fazer e reinicializar a aplica��o"
    IDS_MSG_ERRO_BD = "Erro de Base de dados!!"
    IDS_MSG_MDI_DEMO = "Vers�o Demo"
    IDS_MSG_MDI_TIPO_BD = "No ficheiro de inicializa��es ter� que indicar se os dados est�o numa Base de Dados Local ou Remota!"
    IDS_MSG_MDI_ERRO_FATAL = "Erro fatal n� "
    IDS_MSG_IMPRIMIR = "A Imprimir..."
    IDS_MSG_OPERACAO_N_DISP = "Opera��o n�o disponivel"
    IDS_MSG_CAMPO_OBRIGATORIO = "Campo obrigat�rio"
    IDS_MSG_EM_PROCESSAMENTO = "Em Processamento..."
End Sub

Sub BT_DefineIdioma()
    BT_CarregaIdiomaPortugues
End Sub



